//
//  DownloadedFiles+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 22/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DownloadedFiles.h"

NS_ASSUME_NONNULL_BEGIN

@interface DownloadedFiles (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *fileName;
@property (nullable, nonatomic, retain) NSString *leadName;

@end

NS_ASSUME_NONNULL_END
