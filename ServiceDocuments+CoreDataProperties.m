//
//  ServiceDocuments+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 20/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ServiceDocuments+CoreDataProperties.h"

@implementation ServiceDocuments (CoreDataProperties)

+ (NSFetchRequest<ServiceDocuments *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ServiceDocuments"];
}

@dynamic userName;
@dynamic companyKey;
@dynamic scheduleStartDate;
@dynamic leadDocumentId;
@dynamic leadId;
@dynamic accountNo;
@dynamic docType;
@dynamic title;
@dynamic fileName;
@dynamic descriptions;
@dynamic isAgreementSigned;
@dynamic signatureLinkforAgree;
@dynamic leadNumber;

@end
