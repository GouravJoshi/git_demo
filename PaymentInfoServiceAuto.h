//
//  PaymentInfoServiceAuto.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentInfoServiceAuto : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "PaymentInfoServiceAuto+CoreDataProperties.h"
