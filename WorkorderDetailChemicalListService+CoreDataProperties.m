//
//  WorkorderDetailChemicalListService+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorkorderDetailChemicalListService+CoreDataProperties.h"

@implementation WorkorderDetailChemicalListService (CoreDataProperties)

@dynamic workorderId;
@dynamic userName;
@dynamic companyKey;
@dynamic chemicalList;
@dynamic otherChemicalList;

@end
