//
//  ServiceAutoCompanyDetails+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ServiceAutoCompanyDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServiceAutoCompanyDetails (CoreDataProperties)

@property (nullable, nonatomic, retain) id companyDetails;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *companyKey;

@end

NS_ASSUME_NONNULL_END
