//
//  EmailDetailServiceAuto+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EmailDetailServiceAuto+CoreDataProperties.h"

@implementation EmailDetailServiceAuto (CoreDataProperties)

@dynamic companyKey;
@dynamic userName;
@dynamic workorderId;
@dynamic woInvoiceMailId;
@dynamic emailId;
@dynamic subject;
@dynamic isMailSent;
@dynamic isCustomerEmail;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic isDefaultEmail;
@dynamic isInvoiceMailSent;

@end
