//
//  OutBox+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 06/08/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OutBox+CoreDataProperties.h"

@implementation OutBox (CoreDataProperties)

@dynamic companKey;
@dynamic datenTime;
@dynamic finalJson;
@dynamic userName;
@dynamic dictData;

@end
