//
//  EquipmentDynamicForm+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 21/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "EquipmentDynamicForm+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface EquipmentDynamicForm (CoreDataProperties)

+ (NSFetchRequest<EquipmentDynamicForm *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrFinalInspection;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *departmentId;
@property (nullable, nonatomic, copy) NSString *empId;
@property (nullable, nonatomic, copy) NSString *isSentToServer;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;//
@property (nullable, nonatomic, copy) NSString *mobileEquipId;
@property (nullable, nonatomic, copy) NSString *woEquipId;

@end

NS_ASSUME_NONNULL_END
