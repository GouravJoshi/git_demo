//
//  UpdateLeadInfoDetails+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 14/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UpdateLeadInfoDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface UpdateLeadInfoDetails (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) id leadInfoDict;

@end

NS_ASSUME_NONNULL_END
