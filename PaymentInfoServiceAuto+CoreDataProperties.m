//
//  PaymentInfoServiceAuto+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 29/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentInfoServiceAuto+CoreDataProperties.h"

@implementation PaymentInfoServiceAuto (CoreDataProperties)

@dynamic woPaymentId;
@dynamic workorderId;
@dynamic paymentMode;
@dynamic paidAmount;
@dynamic checkNo;
@dynamic drivingLicenseNo;
@dynamic expirationDate;
@dynamic checkFrontImagePath;
@dynamic checkBackImagePath;
@dynamic createdDate;
@dynamic createdBy;
@dynamic modifiedDate;
@dynamic modifiedBy;
@dynamic userName;

@end
