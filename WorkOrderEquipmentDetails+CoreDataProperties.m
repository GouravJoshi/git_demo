//
//  WorkOrderEquipmentDetails+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 18/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "WorkOrderEquipmentDetails+CoreDataProperties.h"

@implementation WorkOrderEquipmentDetails (CoreDataProperties)

+ (NSFetchRequest<WorkOrderEquipmentDetails *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WorkOrderEquipmentDetails"];
}

@dynamic arrOfEquipmentList;
@dynamic companyKey;
@dynamic userName;
@dynamic workorderId;

@end
