//
//  SalesAutoModifyDate+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 03/10/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
////

#import "SalesAutoModifyDate.h"

NS_ASSUME_NONNULL_BEGIN

@interface SalesAutoModifyDate (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *modifyDate;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) NSString *leadIdd;

@end

NS_ASSUME_NONNULL_END
