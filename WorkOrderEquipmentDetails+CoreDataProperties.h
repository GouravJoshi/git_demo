//
//  WorkOrderEquipmentDetails+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 18/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "WorkOrderEquipmentDetails+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WorkOrderEquipmentDetails (CoreDataProperties)

+ (NSFetchRequest<WorkOrderEquipmentDetails *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *arrOfEquipmentList;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
