//
//  SalesDynamicInspection+CoreDataProperties.m
//  DPS
//
//  Created by Saavan Patidar on 21/09/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SalesDynamicInspection+CoreDataProperties.h"

@implementation SalesDynamicInspection (CoreDataProperties)

@dynamic arrFinalInspection;
@dynamic companyKey;
@dynamic empId;
@dynamic leadId;
@dynamic userName;
@dynamic isSentToServer;

@end
