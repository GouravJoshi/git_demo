//
//  CustomerAddress+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 28/11/17.
//
//

#import "CustomerAddress+CoreDataProperties.h"

@implementation CustomerAddress (CoreDataProperties)

+ (NSFetchRequest<CustomerAddress *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CustomerAddress"];
}

@dynamic companyKey;
@dynamic userName;
@dynamic accountNo;
@dynamic customerAddress;
@dynamic addressId;

@end
