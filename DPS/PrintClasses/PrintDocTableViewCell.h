//
//  PrintDocTableViewCell.h
//  DPS
//
//  Created by Saavan Patidar on 23/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrintDocTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDocName;

@end
