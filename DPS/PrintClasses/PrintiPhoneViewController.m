//
//  PrintiPhoneViewController.m
//  DPS
//UT-Taco client
//  Created by Saavan Patidar on 14/03/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "PrintiPhoneViewController.h"
#import "AllImportsViewController.h"

@interface PrintiPhoneViewController ()
{
    Global *global;
    NSString *strMainUrl,*strDocPath;
    NSArray *arrOfDocumentList;
}
@end

@implementation PrintiPhoneViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    global = [[Global alloc] init];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];

    BOOL isNet=[global isNetReachable];
    
    if (isNet) {
        
       // [self fetchForPaymentInfo:_strLeadId];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Documents..."];
        
        [self performSelector:@selector(getDocumentList) withObject:nil afterDelay:0.5];
        
        //[self getDocumentList];
        // Do any additional setup after loading the view.
        
        if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            
            if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                self.navigationController.interactivePopGestureRecognizer.enabled = NO;
            }
            [self.navigationController.navigationBar setHidden:YES];
            
        }else{
            
            if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                self.navigationController.interactivePopGestureRecognizer.enabled = NO;
            }
            [self.navigationController.navigationBar setHidden:YES];
            
        }
        
    } else {
        
        [global AlertMethod:Alert :@"Please check your net connection"];
        
        [self performSelector:@selector(backMethod) withObject:nil afterDelay:1.0];
        
    }
    
    _tblViewPrintDocList.rowHeight=UITableViewAutomaticDimension;
    _tblViewPrintDocList.estimatedRowHeight=200;
    _tblViewPrintDocList.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)openPrintOption :(NSString*)strUrlPdf{
    
    if (strUrlPdf.length==0) {
        
        [global AlertMethod:Alert :@"No Document Available"];
        [DejalBezelActivityView removeView];
    } else {

    NSString *strNewString=[strUrlPdf stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];

    strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    NSURL *url = [NSURL URLWithString:strNewString];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    [DejalBezelActivityView removeView];
    if (data==nil) {
        
        [global AlertMethod:Alert :@"No Document Available"];
        
    } else {
        
        [sharingItems addObject:data];
        
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
        
    }
    
    }
}


-(void)openPrintOptioniPad :(NSString*)strUrlPdf{
    
    if (strUrlPdf.length==0) {
        
        [global AlertMethod:Alert :@"No Document Available"];
        [DejalBezelActivityView removeView];
    } else {
        
        NSString *strNewString=[strUrlPdf stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];

        strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        NSURL *url = [NSURL URLWithString:strNewString];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        [DejalBezelActivityView removeView];
        if (data==nil) {
            
            [global AlertMethod:Alert :@"No Document Available"];
            
        } else {
            
            NSArray *items = @[data];
            
            // build an activity view controller
            UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
            
            // and present it
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                [self presentActivityControlleriPad:controller];
            }
            else
            {
                [self presentActivityController:controller];
            }
            
            //[self openPrinter:items];
            
        }
    }
}

-(void)openPrinter: (NSArray *)items
{
    UIPrintInteractionController *pc = [UIPrintInteractionController sharedPrintController];
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.orientation = UIPrintInfoOrientationPortrait;
        //printInfo.jobName = @”Report”;
        pc.printInfo = printInfo;
        //pc.showsPageRange = YES;
        pc.printingItem = items;//[NSURL fileURLWithPath:[self returnFilePath]];
        UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if(!completed && error){
                //NSLog(@"Print failed  domain: %@ error code %ld, error.domain, (long)error.code");.
            }
        };
        [pc presentFromRect:CGRectMake(0, 0, 50, 50) inView:self.view animated:YES completionHandler:completionHandler];
}

- (IBAction)action_PrintAgreement:(id)sender {

    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Agreement..."];
    
    [self performSelector:@selector(printAgreement) withObject:nil afterDelay:0.2];
}

-(void)printAgreement{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@/documents/%@",strMainUrl,_strAgreementName];
    strUrl=[NSString stringWithFormat:@"%@",_strAgreementName];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        [self openPrintOptioniPad:strUrl];
        
    }else{
        
        [self openPrintOption:strUrl];
        
    }

}

- (IBAction)action_PrintProposal:(id)sender {
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Proposal..."];
    
    [self performSelector:@selector(printProposal) withObject:nil afterDelay:0.2];

}

-(void)printProposal{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@/documents/%@",strMainUrl,_strProposalName];
    strUrl=[NSString stringWithFormat:@"%@",_strProposalName];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        [self openPrintOptioniPad:strUrl];
        
    }else{
        
        [self openPrintOption:strUrl];
        
    }

}

- (IBAction)action_PrintInspection:(id)sender {
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading Inspection..."];
    
    [self performSelector:@selector(printInspection) withObject:nil afterDelay:0.2];

}

-(void)printInspection{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@/documents/%@",strMainUrl,_strInspectionName];
    strUrl=[NSString stringWithFormat:@"%@",_strInspectionName];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        [self openPrintOptioniPad:strUrl];
        
    }else{
        
        [self openPrintOption:strUrl];
        
    }

}

- (IBAction)action_Cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)backMethod{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)fetchForPaymentInfo :(NSString*)strLeadId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
        [global AlertMethod:Alert :@"No Document Available"];
        
        [self performSelector:@selector(backMethod) withObject:nil afterDelay:1.0];
        
    }
    else
    {
        
        NSMutableArray *arrAgreement,*arrProposal,*arrInspection;
        
        arrAgreement=[[NSMutableArray alloc]init];
        arrProposal=[[NSMutableArray alloc]init];
        arrInspection=[[NSMutableArray alloc]init];
        
        
        for (int k=0; k<arrAllObj.count; k++) {
            
            NSManagedObject *objTemp=arrAllObj[k];
            
            NSString *strTitle=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"Title"]];
            
            if ([strTitle isEqualToString:@"Agreement"]) {
                
                [arrAgreement addObject:objTemp];
                
            } else if ([strTitle isEqualToString:@"Proposal"]) {
                
                [arrProposal addObject:objTemp];
                
            } else if ([strTitle isEqualToString:@"Inspection"]) {
                
                [arrInspection addObject:objTemp];
                
            }
        }
        
        for (int k1=0; k1<arrAgreement.count; k1++) {
            
            NSManagedObject *objTemp=arrAgreement[k1];
            _strAgreementName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"fileName"]];

        }
        
        for (int k2=0; k2<arrProposal.count; k2++) {
            
            NSManagedObject *objTemp=arrProposal[k2];
            _strProposalName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"fileName"]];
            
        }

        for (int k3=0; k3<arrInspection.count; k3++) {
            
            NSManagedObject *objTemp=arrInspection[k3];
            _strInspectionName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"fileName"]];
            
        }
        
    }
}



- (void)presentActivityControlleriPad:(UIActivityViewController *)controller {
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationFullScreen;//UIModalPresentationPopover;
    
    UILabel *lbl = [[UILabel alloc]init];
    lbl.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 150 , [UIScreen mainScreen].bounds.size.height - 100, 50, 50);
    [self.view addSubview:lbl];
    
    controller.popoverPresentationController.sourceView = lbl;//[[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 150 , [UIScreen mainScreen].bounds.size.height - 150, 50, 50)];
   

    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUnknown;
    
    UINavigationController* nav = [UINavigationController new];
    popController.barButtonItem = nav.navigationItem.leftBarButtonItem;
    

    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
    
    [self presentViewController:controller animated:YES completion:nil];

}


- (void)presentActivityController:(UIActivityViewController *)controller {
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    controller.popoverPresentationController.sourceView = self.view;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUnknown;
    UINavigationController* nav = [UINavigationController new];
    popController.barButtonItem = nav.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
        } else {
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}
-(void)callDelay:(UIActivityViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)getDocumentList{
    
  //  [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Documents..."];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUrl=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
    NSString *strCompanyKey=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

    NSString *stringUrl=[NSString stringWithFormat:@"%@%@%@%@%@",strUrl,UrlSalesDocumentList,strCompanyKey,UrlSalesDocumentListLeadNumber,_strLeadId];
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        
        NSDictionary*ResponseDict;
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:requestLocal returningResponse:&response error:&error];
        
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        [DejalBezelActivityView removeView];

        if (ResponseDict==nil) {
            
            [self goBack];
            
        }else{
            
            arrOfDocumentList=(NSArray*)ResponseDict;
            
            if ([arrOfDocumentList isKindOfClass:[NSArray class]]) {
                
                if (arrOfDocumentList.count==0) {
                    
                    [self goBack];
                    
                } else {
                    
                    [_tblViewPrintDocList reloadData];

                    /*
                    NSMutableArray *arrAgreement,*arrProposal,*arrInspection;
                    
                    arrAgreement=[[NSMutableArray alloc]init];
                    arrProposal=[[NSMutableArray alloc]init];
                    arrInspection=[[NSMutableArray alloc]init];
                    
                    
                    for (int k=0; k<arrOfDocumentList.count; k++) {
                        
                        NSDictionary *objTemp=arrOfDocumentList[k];
                        
                        NSString *strTitle=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"Title"]];
                        
                        if ([strTitle isEqualToString:@"Agreement"]) {
                            
                            [arrAgreement addObject:objTemp];
                            
                        } else if ([strTitle isEqualToString:@"Proposal"]) {
                            
                            [arrProposal addObject:objTemp];
                            
                        } else if ([strTitle isEqualToString:@"Inspection"]) {
                            
                            [arrInspection addObject:objTemp];
                            
                        }
                    }
                    
                    for (int k1=0; k1<arrAgreement.count; k1++) {
                        
                        NSManagedObject *objTemp=arrAgreement[k1];
                        _strAgreementName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"DocumentPath"]];
                        
                    }
                    
                    for (int k2=0; k2<arrProposal.count; k2++) {
                        
                        NSManagedObject *objTemp=arrProposal[k2];
                        _strProposalName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"DocumentPath"]];
                        
                    }
                    
                    for (int k3=0; k3<arrInspection.count; k3++) {
                        
                        NSManagedObject *objTemp=arrInspection[k3];
                        _strInspectionName=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"DocumentPath"]];
                        
                    }
                     
                     */
                    
                }
                
            } else {
                
                [self goBack];
                
            }
            
            
        }
        
        [DejalBezelActivityView removeView];
    }
    @catch (NSException *exception) {
        [global AlertMethod:Alert :Sorry];
        [DejalBezelActivityView removeView];
    }
    @finally {
        [DejalBezelActivityView removeView];
    }
}


-(void)goBack{
    
    [global AlertMethod:Alert :@"No Document Available"];
    
    [self performSelector:@selector(backMethod) withObject:nil afterDelay:1.0];
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOfDocumentList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return tableView.rowHeight;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PrintDocTableViewCell *cell = (PrintDocTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PrintDocTableViewCell" forIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [self configureCellEquipment:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCellEquipment:(PrintDocTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dictData=arrOfDocumentList[indexPath.row];
    cell.lblDocName.text=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Title"]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dictData=arrOfDocumentList[indexPath.row];
    strDocPath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DocumentPath"]];
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Downloading..."];
    [self performSelector:@selector(printDoc) withObject:nil afterDelay:0.2];

}

-(void)printDoc{
    
    NSString *strUrl=[NSString stringWithFormat:@"%@/documents/%@",strMainUrl,strDocPath];
    strUrl=[NSString stringWithFormat:@"%@",strDocPath];
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        [self openPrintOptioniPad:strUrl];
        
    }else{
        
        [self openPrintOption:strUrl];
        
    }
    
}

@end
