//
//  PrintiPhoneViewController.h
//  DPS
//
//  Created by Saavan Patidar on 14/03/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface PrintiPhoneViewController : UIViewController<NSFetchedResultsControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityPaymentInfo;

}
- (IBAction)action_PrintAgreement:(id)sender;
- (IBAction)action_PrintProposal:(id)sender;
- (IBAction)action_PrintInspection:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@property (strong, nonatomic) NSString *strAgreementName;
@property (strong, nonatomic) NSString *strProposalName;
@property (strong, nonatomic) NSString *strInspectionName;
@property (strong, nonatomic) NSString *strLeadId;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) IBOutlet UITableView *tblViewPrintDocList;

@end
