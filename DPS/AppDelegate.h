//
//  AppDelegate.h
//  DPS
//
//  Created by Rakesh Jain on 17/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import <CallKit/CallKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate,NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    AppDelegate *appDelegate1;
    NSEntityDescription *entityOutBox;
    NSManagedObjectContext *context;
    NSManagedObjectContext *context1;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    UIInterfaceOrientationMask orientationLock;

}
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,assign) CGPoint point;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerOutBox;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property (nonatomic, strong) CXCallObserver *callObserver;
- (void)saveOrientation :(UIInterfaceOrientationMask )Orientation;
@property (strong, nonatomic) UINavigationController *navigationController;

@end

