//
//  EquipmentsListTableViewCell.m
//  DPS
//
//  Created by Saavan Patidar on 20/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "EquipmentsListTableViewCell.h"

@implementation EquipmentsListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
