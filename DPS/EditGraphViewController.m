//
//  EditGraphViewController.m
//  DPS
//
//  Created by Saavan Patidar on 09/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//sdfdsf

#import "EditGraphViewController.h"
#import "AllImportsViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface EditGraphViewController ()<CLLocationManagerDelegate>
{
    BOOL isSignImage,isFirstTime,isUploadedGraph;
    UIView *viewForSign;
    NSString *strSignImageName,*strImagePathToEdit,*strGraphImageName,*strGraphCaption,*strGraphDescription;
    NSData *signImageData;
    BOOL image;
    Global *global;
    UITableView *tblData;
    NSMutableArray *arrDataTblView,*arrOfColor;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    
}

@end

@implementation EditGraphViewController

@synthesize mySignatureImage;
@synthesize lastContactPoint1, lastContactPoint2, currentPoint;
@synthesize imageFrame;
@synthesize fingerMoved;
//..........................
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    isFirstTime=NO;
    isUploadedGraph=NO;
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
    
    isFirstTime=[defsImageName boolForKey:@"firstGraphImage"];
    
    if (isFirstTime) {
        
        [defsImageName setBool:NO forKey:@"firstGraphImage"];
        [defsImageName synchronize];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\graphImg%@%@.jpg",strDate,strTime];
//        strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@%@.jpg",strDate,strTime];
//        _imageToEdit=[UIImage imageNamed:@"graph-paper.png"];

        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        BOOL isServiceGraph=[defs boolForKey:@"servicegraph"];
        BOOL isTermiteGraph=[defs boolForKey:@"termitegraph"];
        
        if (isServiceGraph) {
            
        }else if(isTermiteGraph){
            
        }
        else{
            
            strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@%@.jpg",strDate,strTime];
            
        }
        _imageToEdit=[UIImage imageNamed:@"graph-paper.png"];
        
        
        global=[[Global alloc]init];
        
        viewForSign=[[UIView alloc]init];
        viewForSign.frame=CGRectMake(2, _btnCancel.frame.origin.y+_btnCancel.frame.size.height+50, [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);
        
        //....................
#pragma mark- TEMP nilind 8 septs
        
        isSignImage=false;
        mySignatureImage.image = nil;
        
        
        mySignatureImage.image = [UIImage imageNamed:@"graph-paper.png"];
        imageFrame = CGRectMake(0,0,viewForSign.bounds.size.width,viewForSign.bounds.size.height);
        [viewForSign.layer setCornerRadius:5.0f];
        [viewForSign.layer setBorderColor:[UIColor blackColor].CGColor];
        [viewForSign.layer setBorderWidth:0.8f];
        [viewForSign.layer setShadowColor:[UIColor blackColor].CGColor];
        [viewForSign.layer setShadowOpacity:0.3];
        [viewForSign.layer setShadowRadius:3.0];
        [viewForSign.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        //allocate an image view and add to the main view
        mySignatureImage = [[UIImageView alloc] initWithImage:nil];
        mySignatureImage.frame = imageFrame;
        mySignatureImage.backgroundColor = [UIColor whiteColor];
        [viewForSign addSubview:mySignatureImage];
        [self.view addSubview:viewForSign];
        // Do any additional setup after loading the view, typically from a nib.
        
        
        mySignatureImage.image = nil;
        mySignatureImage.image = [UIImage imageNamed:@"graph-paper.png"];
        _imageToEdit=mySignatureImage.image;

    } else {
        
        strImagePathToEdit=[defsImageName valueForKey:@"editImageGraph"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
        UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
        _imageToEdit=imageEdit;
        
        
        global=[[Global alloc]init];
        
        viewForSign=[[UIView alloc]init];
        viewForSign.frame=CGRectMake(2, _btnCancel.frame.origin.y+_btnCancel.frame.size.height+50, [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);
        
        //....................
#pragma mark- TEMP nilind 8 septs
        
        isSignImage=false;
        mySignatureImage.image = nil;
        
        
        mySignatureImage.image = _imageToEdit;
        imageFrame = CGRectMake(0,0,viewForSign.bounds.size.width,viewForSign.bounds.size.height);
        [viewForSign.layer setCornerRadius:5.0f];
        [viewForSign.layer setBorderColor:[UIColor blackColor].CGColor];
        [viewForSign.layer setBorderWidth:0.8f];
        [viewForSign.layer setShadowColor:[UIColor blackColor].CGColor];
        [viewForSign.layer setShadowOpacity:0.3];
        [viewForSign.layer setShadowRadius:3.0];
        [viewForSign.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        //allocate an image view and add to the main view
        mySignatureImage = [[UIImageView alloc] initWithImage:nil];
        mySignatureImage.frame = imageFrame;
        mySignatureImage.backgroundColor = [UIColor whiteColor];
        [viewForSign addSubview:mySignatureImage];
        [self.view addSubview:viewForSign];
        // Do any additional setup after loading the view, typically from a nib.
        
        
        mySignatureImage.image = nil;
        mySignatureImage.image = _imageToEdit;
        // mySignatureImage.contentMode=UIViewContentModeScaleAspectFit;
        //......................................

    }
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- SIGNATURE CODE 8 SEPT
//when one or more fingers touch down in a view or window
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //did our finger moved yet?
    fingerMoved = NO;
    UITouch *touch = [touches anyObject];
    
    //    //just clear the image if the user tapped twice on the screen
    //    if ([touch tapCount] == 2) {
    //        mySignatureImage.image = nil;
    //        return;
    //    }
    
    //we need 3 points of contact to make our signature smooth using quadratic bezier curve
    currentPoint = [touch locationInView:mySignatureImage];
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    lastContactPoint2 = [touch previousLocationInView:mySignatureImage];
    
}


//when one or more fingers associated with an event move within a view or window
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //well its obvious that our finger moved on the screen
    fingerMoved = YES;
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass: [_viewSignature class]]) {
        NSLog(@"finger moved on imageview");
        isSignImage=true;
        //your touch was in a uipickerview ... do whatever you have to do
    }
    //save previous contact locations
    lastContactPoint2 = lastContactPoint1;
    lastContactPoint1 = [touch previousLocationInView:mySignatureImage];
    //save current location
    currentPoint = [touch locationInView:mySignatureImage];
    
    //find mid points to be used for quadratic bezier curve
    CGPoint midPoint1 = [self midPoint:lastContactPoint1 withPoint:lastContactPoint2];
    CGPoint midPoint2 = [self midPoint:currentPoint withPoint:lastContactPoint1];
    
    //create a bitmap-based graphics context and makes it the current context
    UIGraphicsBeginImageContext(imageFrame.size);
    
    //draw the entire image in the specified rectangle frame
    [mySignatureImage.image drawInRect:CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height)];
    
    //set line cap, width, stroke color and begin path
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    float widthValue=[[defs valueForKey:@"widthForLine"] floatValue];
    NSString *strColor=[defs valueForKey:@"colorForLine"];
    
    if (!widthValue) {
        widthValue=3.0f;
    }
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), widthValue);
    
    if ([strColor isEqualToString:@"Red"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
    }else if ([strColor isEqualToString:@"Green"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 1.0, 0.0, 1.0);
    }else if ([strColor isEqualToString:@"Blue"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
    }else if ([strColor isEqualToString:@"White"]) {//White
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 1.0, 1.0, 1.0);
    }else if ([strColor isEqualToString:@"Black"]) {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
    }else {
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 1.0, 1.0);
    }
    
    
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    
    //begin a new new subpath at this point
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), midPoint1.x, midPoint1.y);
    //create quadratic Bézier curve from the current point using a control point and an end point
    CGContextAddQuadCurveToPoint(UIGraphicsGetCurrentContext(),
                                 lastContactPoint1.x, lastContactPoint1.y, midPoint2.x, midPoint2.y);
    
    //set the miter limit for the joins of connected lines in a graphics context
    CGContextSetMiterLimit(UIGraphicsGetCurrentContext(), 2.0);
    
    //paint a line along the current path
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    //set the image based on the contents of the current bitmap-based graphics context
    mySignatureImage.image = UIGraphicsGetImageFromCurrentImageContext();
    
    //remove the current bitmap-based graphics context from the top of the stack
    UIGraphicsEndImageContext();
    
    //lastContactPoint = currentPoint;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // UITouch *touch = [touches anyObject];
    
    //if the finger never moved draw a point
    if(!fingerMoved) {
        // mySignatureImage.image=nil;
        NSLog(@"finger not moved...!!!");
        
    }
}

//calculate midpoint between two points
- (CGPoint) midPoint:(CGPoint )p0 withPoint: (CGPoint) p1 {
    return (CGPoint) {
        (p0.x + p1.x) / 2.0,
        (p0.y + p1.y) / 2.0
    };
}
- (IBAction)actionOnSaveSign:(id)sender
{
    if (mySignatureImage.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please edit to proceed further." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSettingSales"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            strGraphCaption=@"No Caption Available..!!";
            strGraphDescription=@"No Description Available..!!";
            
            [self saveGraphImage];
        }
    }
}
-(void)saveGraphImage{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strImagePathToEdit]];
    signImageData = UIImagePNGRepresentation(mySignatureImage.image);
    [signImageData writeToFile:path atomically:YES];
    
    if (isFirstTime) {
        
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //termitegraph
        BOOL isServiceGraph=[defs boolForKey:@"servicegraph"];
        BOOL isTermiteGraph=[defs boolForKey:@"termitegraph"];
        
        if (isServiceGraph) {
            
            [defs setBool:NO forKey:@"servicegraph"];
            [defs synchronize];
            
            [self saveGraphImageToCoreDataServiceAuto];
            
        }else if (isTermiteGraph){
            
            [defs setBool:NO forKey:@"termitegraph"];
            [defs synchronize];
            // Yaha Termite Graph Save Hoga

            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:strImagePathToEdit forKey:@"graphImagePathTermite"];
            [defs setValue:strGraphCaption forKey:@"graphImageCaption"];
            [defs setValue:strGraphDescription forKey:@"graphImageDescription"];
            [defs setValue:@"strAddGraphImg" forKey:@"strAddGraphImg"];
            [defs synchronize];

            
        }
        else {
            
            [self saveGraphImageToCoreData];
            
        }
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (IBAction)actionOnClearSign:(id)sender
{
    
    mySignatureImage.image = nil;
    
    if (isUploadedGraph) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"UploadedGraphImage"]];
        UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
        
        _imageToEdit=imageEdit;

        mySignatureImage.image = _imageToEdit;
        strSignImageName=nil;
        signImageData=nil;
        isSignImage=false;
        image=YES;
        
    } else {
        
        if (isFirstTime) {
            
            NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
            [formatterDate setDateFormat:@"MMddyyyy"];
            [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
            NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HHmmss"];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *strTime = [formatter stringFromDate:[NSDate date]];
            strImagePathToEdit = [NSString stringWithFormat:@"\\Documents\\UploadImages\\graphImg%@%@.jpg",strDate,strTime];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            
            BOOL isServiceGraph=[defs boolForKey:@"servicegraph"];
            BOOL isTermiteGraph=[defs boolForKey:@"termitegraph"];
            
            if (isServiceGraph) {
                
            }else if(isTermiteGraph){
                
            }
            else{
                
                strImagePathToEdit = [NSString stringWithFormat:@"graphImg%@%@.jpg",strDate,strTime];
                
            }
            // NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
            _imageToEdit=[UIImage imageNamed:@"graph-paper.png"];
            mySignatureImage.image = [UIImage imageNamed:@"graph-paper.png"];
            strSignImageName=nil;
            signImageData=nil;
            isSignImage=false;
            image=YES;
            _imageToEdit=mySignatureImage.image;

        }
        else
        {
            
            NSUserDefaults *defsImageName=[NSUserDefaults standardUserDefaults];
            strImagePathToEdit=[defsImageName valueForKey:@"editImageGraph"];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImagePathToEdit]];
            UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];
            _imageToEdit=imageEdit;
            mySignatureImage.image = _imageToEdit;
            strSignImageName=nil;
            signImageData=nil;
            isSignImage=false;
            image=YES;

        }
        
    }
    
}

-(void)openGallery{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];

}
-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [self resizeImage:chosenImage :@"UploadedGraphImage"];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"UploadedGraphImage"]];
    UIImage* imageEdit = [UIImage imageWithContentsOfFile:path];

    _imageToEdit=imageEdit;
    
    mySignatureImage.image = _imageToEdit;
    
    isUploadedGraph=YES;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)SetGraphImageBack{
    
    mySignatureImage.image = _imageToEdit;
    
}

-(UIImage *)resizeImage:(UIImage *)image1 :(NSString*)imageName
{
    float actualHeight = image1.size.height;
    float actualWidth = image1.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image1 drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
   // NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}


- (IBAction)actionOnExitSign:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_SelectColor:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    arrDataTblView=[[NSMutableArray alloc]init];
    arrOfColor=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObject:@"Red"];
    [arrDataTblView addObject:@"Green"];
    [arrDataTblView addObject:@"Blue"];
    [arrDataTblView addObject:@"Black"];
    [arrDataTblView addObject:@"White"];
    
    UIColor *colorRed=[UIColor redColor];
    UIColor *colorGreen=[UIColor greenColor];
    UIColor *colorBlue=[UIColor blueColor];
    UIColor *colorBlack=[UIColor blackColor];
    UIColor *colorWhite=[UIColor whiteColor];
    
    [arrOfColor addObject:colorRed];
    [arrOfColor addObject:colorGreen];
    [arrOfColor addObject:colorBlue];
    [arrOfColor addObject:colorBlack];
    [arrOfColor addObject:colorWhite];
    
    
    tblData.tag=101;
    [self tableLoad:tblData.tag];
    
}

- (IBAction)action_SelectWidth:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    arrDataTblView=[[NSMutableArray alloc]init];
    
    [arrDataTblView addObject:@"1.0f"];
    [arrDataTblView addObject:@"2.0f"];
    [arrDataTblView addObject:@"3.0f"];
    [arrDataTblView addObject:@"4.0f"];
    [arrDataTblView addObject:@"5.0f"];
    [arrDataTblView addObject:@"6.0f"];
    [arrDataTblView addObject:@"7.0f"];
    
    
    tblData.tag=102;
    [self tableLoad:tblData.tag];
}

-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //[tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- ------------METHODS--------------
//============================================================================
//============================================================================
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    tblData.frame=CGRectMake(2, _btnCancel.frame.origin.y+_btnCancel.frame.size.height+50, [UIScreen mainScreen].bounds.size.width-4, [UIScreen mainScreen].bounds.size.width-4);
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y+tblData.frame.size.height+20, tblData.frame.size.width, 42)];
    
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [btnDone addTarget:self action:@selector(doneMethod) forControlEvents:UIControlEventTouchDown];
    
    [viewBackGround addSubview:btnDone];
    
    
}

-(void)doneMethod{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDataTblView count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                UIColor *color=[arrOfColor objectAtIndex:indexPath.row];
                cell.textLabel.textColor=color;
                if ([[arrDataTblView objectAtIndex:indexPath.row] isEqualToString:@"White"]) {
                    
                    cell.textLabel.textColor=[UIColor blackColor];
                    //  cell.textLabel.backgroundColor=[UIColor blackColor];
                    
                }else{
                    
                    UIColor *color=[arrOfColor objectAtIndex:indexPath.row];
                    cell.textLabel.textColor=color;
                    
                }
                break;
            }
            case 102:
            {
                cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                cell.textLabel.textColor=[UIColor blackColor];
                break;
            }
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:15];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!(arrDataTblView.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                
                NSString *strString=[arrDataTblView objectAtIndex:indexPath.row];
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strString forKey:@"colorForLine"];
                [defs synchronize];
                
                break;
            }
            case 102:
            {
                
                NSString *strString=[arrDataTblView objectAtIndex:indexPath.row];
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setValue:strString forKey:@"widthForLine"];
                [defs synchronize];
                
                break;
            }
                
            default:
                break;
        }
        
    }
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
}

-(void)saveGraphImageToCoreData{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    
    ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
    
    objImageDetail.createdBy=@"";
    objImageDetail.createdDate=@"";
    objImageDetail.descriptionImageDetail=strGraphDescription;
    objImageDetail.leadImageCaption=strGraphCaption;
    objImageDetail.leadImageId=@"";
    objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",strImagePathToEdit];
    objImageDetail.leadImageType=@"Graph";
    objImageDetail.modifiedBy=@"";
    objImageDetail.modifiedDate=[global modifyDate];
    objImageDetail.leadId=_strLeadId;
    objImageDetail.userName=_strUserName;
    objImageDetail.companyKey=_strCompanyKey;
    
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objImageDetail.latitude=latitude;
    objImageDetail.longitude=longitude;

    NSError *error1;
    [context save:&error1];
    
    [global updateSalesModifydate:_strLeadId];

}

-(void)saveGraphImageToCoreDataServiceAuto{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    
    ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
    
    objImageDetail.createdBy=@"";
    objImageDetail.createdDate=@"";
    objImageDetail.imageDescription=strGraphDescription;
    objImageDetail.imageCaption=strGraphCaption;
    objImageDetail.woImageId=@"";
    objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",strImagePathToEdit];
    objImageDetail.woImageType=@"Graph";
    objImageDetail.modifiedBy=@"";
    objImageDetail.modifiedDate=[global modifyDate];
    objImageDetail.workorderId=_strLeadId;
    objImageDetail.userName=_strUserName;
    objImageDetail.companyKey=_strCompanyKey;
    //Lat long code
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objImageDetail.latitude=latitude;
    objImageDetail.longitude=longitude;
    
    NSError *error1;
    [context save:&error1];
    
    NSString *strCurrentDateFormatted = [global modifyDateService];
    [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:_strLeadId :strCurrentDateFormatted];
    [global fetchServicePestWoToUpdateModifyDate:_strLeadId :strCurrentDateFormatted];
    [global fetchServicePestWoToUpdateModifyDateJugad:_strLeadId];
    
}

-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter graph caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter graph description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel & Save Graph" forState:UIControlStateNormal];
    btnDone.titleLabel.numberOfLines=2;
    btnDone.titleLabel.textAlignment=NSTextAlignmentCenter;
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            strGraphCaption=txtFieldCaption.text;
            
        } else {
            
            strGraphCaption=@"No Caption Available..!!";
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            strGraphDescription=txtViewImageDescription.text;
            
        } else {
            
            strGraphDescription=@"No Description Available..!!";
            
        }
        
        [viewBackAlertt removeFromSuperview];
        
        [self saveGraphImage];
        
    } else {
        
        [self AlertViewForImageCaption];
        
    }
    
}

-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    strGraphCaption=@"No Caption Available..!!";
    strGraphDescription=@"No Description Available..!!";
    
    [self saveGraphImage];
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TextView Delegates METHOD-----------------
//============================================================================
//============================================================================


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==1) {
        
        [self openCamera];
        
    } else if (buttonIndex==2){
        
        [self openGallery];
        
    } else{
        
        
        
    }
}

- (IBAction)action_UploadGraph:(id)sender {
    
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Choose option" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    [alert show];
    
}

@end
