//
//  SalesPaymentViewController.m
//  DPS
//
//  Created by Saavan Patidar on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//
//  Saavan Patidar 2021


#import "SalesPaymentViewController.h"
#import "AllImportsViewController.h"

@interface SalesPaymentViewController ()
{
    AppDelegate *appDelegate;
    Global *global;
    UIWebView *webview;
    //VXPResponse *finalResponse;
    NSString *strURL,*strGlobalCardLastFourNo,*strGlobalAmountToBePaid,*strUserName,*strEmployeeNameMain,*strEmpId,*strPaymentreferenceNo,*strAccountNo,*strLeadNo,*strLeadId,*strCompanyKeyy;
    BOOL isForSavedCards,isDefaultSaveCards;
    NSArray *arrOfSavedCardsDetails;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    NSMutableArray *arrOfSavedCardsToSend,*arrOfFinalServices;
    NSIndexPath *indexPathOfButtonBeingEdited;
    NSDictionary *dictResponseElementPaymentSucceess,*dictResponseTransactionId;

}
@end

@implementation SalesPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrOfSavedCardsToSend=[[NSMutableArray alloc]init];
    arrOfFinalServices=[[NSMutableArray alloc]init];

    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strLeadId=[NSString stringWithFormat:@"%@",[defsLead valueForKey:@"LeadId"]];
    strLeadNo=[NSString stringWithFormat:@"%@",[defsLead valueForKey:@"LeadNumber"]];
    strAccountNo =[NSString stringWithFormat:@"%@",[defsLead valueForKey:@"AccountNos"]];//AccountNos

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strEmployeeNameMain=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmpId      =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.CompanyId"];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

    [self salesLeadInfoFetch];
    
    global = [[Global alloc] init];
    
    isForSavedCards=YES;   ///  Save Card Details or Nortt
    [_btnNewCardSaveDetails setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
    
    isDefaultSaveCards=YES;  //Already saved card details wala
    
    [_btnSavedCards setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnNewCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self methodToSetViews];
    [self getSavedCardsDetails];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewSavedCards.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    //============================================================================
    //============================================================================

    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [_scrollVieww addGestureRecognizer:singleTapGestureRecognizer];

    // Do any additional setup after loading the view.
}
//============================================================================
#pragma mark- -------------Tap Method------------------
//============================================================================

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    [viewBackGround removeFromSuperview];
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionBack:(id)sender {
    
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:Alert
                               message:@"Are you sure to cancel transaction"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Yes-Cancel" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              [self dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* No = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:No];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)action_PayNow:(id)sender {
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else{

   // VTPHostConfiguration* hostConfiguration = self.vtpConfiguration.hostConfiguration;
    
    
    [self performSelector:@selector(generatingUrlForPayment) withObject:nil afterDelay:0.2];
        
    }
    
}
/*
-(void)generatingUrlForPayment{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Processing Please Wait..."];

    VXPCredentials* credentials = [VXPCredentials
                                   credentialsWithValues:@"1016502"                                                                accountToken:@"9F46CBF3A695B67A9EE7EB2BE360090FEDD39238B3C0F4FED14F47E7D06E67ABC7C3A101"
                                   acceptorID:@"3928907"];
    
    
    
    VXPApplication* application = [VXPApplication
                                   
                                   applicationWithValues:@"1234"
                                   
                                   applicationName:@"triPOS Sample"
                                   
                                   applicationVersion:@"0.0.0.0"];
    
    
    
    
    
    ////////// Transaction //////////
    
    
    
    VXPTransaction *transaction = [[VXPTransaction alloc] init];
    
    
    
    //    NSString *transactionAmount = [sharedTriPOSConfig.transactionAmount stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
    
    //
    
    //    transaction.TransactionAmount = [NSDecimalNumber decimalNumberWithString:transactionAmount];
    
    
    
    transaction.TransactionAmount = [NSDecimalNumber decimalNumberWithString:strGlobalAmountToBePaid];
    
    
    
    transaction.MarketCode = VXPMarketCodeRetail;
    
    
    
    transaction.ReferenceNumber = [global getReferenceNumber];
    
    strPaymentreferenceNo=transaction.ReferenceNumber;
    
    ////////// VXPTransactionSetup //////////
    
    
    
    VXPTransactionSetup* transactionSetup = [[VXPTransactionSetup alloc]init];
    
    
    
    // set Hosted Payments to a credit card sale
    
    
    
    transactionSetup.TransactionSetupMethod = VXPTransactionSetupMethodCreditCardSale;
    
    
    
    // is the broswer embedded in the app?
    
    
    
    transactionSetup.Embedded = VXPBooleanTypeTrue;
    
    
    
    // return to ReturnURL automatically
    
    
    
    transactionSetup.AutoReturn = VXPBooleanTypeTrue;
    
    
    
    // setup for keyboard entry
    
    
    
    transactionSetup.DeviceInputCode = VXPDeviceInputCodeKeyboard;
    
    
    
    // the transaction result is returned in the return URL query string
    
    
    
    // transactionSetup.ReturnURL =  @"https://localhost/HostedPaymentsResponse";
    
    
    
    //transactionSetup.ReturnURL = @"https://certtransaction.hostedpayments.com";
    
    // transactionSetup.ReturnURL = @"hpresp://localhost";
    
    transactionSetup.ReturnURLTitle = @"peSTream Payment Response";
    
    
    
    // some of following fields are used or not based on the Embedded
    
    // flag
    
    
    
    transactionSetup.CompanyName = @"Quacito LLC";
    
    transactionSetup.WelcomeMessage = @"Welcome to peSTream";
    
    transactionSetup.ProcessTransactionTitle = @"Process";
    
    
    
    ////////// VXPTerminal //////////
    
    
    
    VXPTerminal *terminal = [[VXPTerminal alloc] init];
    
    terminal.LaneNumber = @"1";
    
    terminal.TerminalID = @"TransForm Mobile SDK Express Sample 01";
    
    terminal.CardPresentCode = VXPCardPresentCodePresent;
    
    terminal.CardholderPresentCode = VXPCardHolderPresentCodePresent;
    
    terminal.CVVPresenceCode = VXPCVVPresenceCodeDefault;
    
    terminal.TerminalCapabilityCode = VXPTerminalCapabilityCodeDefault;
    
    terminal.TerminalEnvironmentCode = VXPTerminalEnvironmentCodeDefault;
    
    terminal.MotoECICode = VXPMotoECICodeNotUsed;
    
    terminal.CardInputCode = VXPCardInputCodeManualKeyed;
    
    
    /////  if saved Card is there
    
    VXPPaymentAccount *paymentAccount = [[VXPPaymentAccount alloc]init];
    
    if (isForSavedCards) {
        
       // paymentAccount = [[VXPPaymentAccount alloc]init];
        paymentAccount.PaymentAccountType = VXPPaymentAccountTypeCreditCard;
        paymentAccount.PaymentAccountReferenceNumber = transaction.ReferenceNumber;
        
    }
    
    /////  end
    
    VXPRequest* request  = [VXPRequest requestWithRequestType:VXPRequestTypeTransactionSetup credentials:credentials application:application];
    
    
    request.Transaction = transaction;
    
    request.Terminal = terminal;
    
    request.TransactionSetup = transactionSetup;
    
    /////  if saved Card is there
    if (isForSavedCards) {
        
        request.PaymentAccount = paymentAccount;
        
    }
    ///// end if saved Card is there
    
    
    VXP* vxp = [[VXP alloc] init];
    
    //    vxp.TestCertification = self.vtpConfiguration.applicationConfiguration.mode == VTPApplicationModeTestCertification;
    
    if ([ElementTypeProductionOrtesting isEqualToString:@"Testing"]) {
        
        vxp.TestCertification = YES;

    } else {

        vxp.TestCertification = NO;

    }
    
    
    [vxp sendRequest:request
     
             timeout:10000
     
        autoReversal:YES
     
   completionHandler:^(VXPResponse* response)
     
     {
         
         [self handleExpressResponse:response];
         
     }
     
        errorHandler:^(NSError* error)
     
     {
         
         // [super handleError:error];
         
     }];

}
- (void)handleExpressResponse:(VXPResponse*)response

{
    
    //NSArray* responseArray = @[ @[ @"ExpressResponseMessage", response.ExpressResponseMessage ] ];
    
    finalResponse=response;
    
    NSLog(@"url>>>%@",response.HostedPaymentsUrl);
    
    strURL = (NSString*)response.HostedPaymentsUrl;
    
    // [super updateResponseTableView:responseArray];
    
    
    
    [self performSelectorOnMainThread:@selector(myMethod) withObject:nil waitUntilDone:NO];
    
    
}

-(void)myMethod
{
    
    webview=[[UIWebView alloc]initWithFrame:CGRectMake(0, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height*70/100)];
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,webview.frame.origin.y+webview.frame.size.height+50)];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    [_scrollVieww setContentOffset:bottomOffset animated:YES];

    
    webview.delegate =self;
    
   // webview.scrollView.maximumZoomScale = 0.0;
    
   // webview.scalesPageToFit=YES;
    
   // webview.scrollView.scrollEnabled = NO;
    
  //  webview.scrollView.bounces = NO;
    
    [_scrollVieww addSubview:webview];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:finalResponse.HostedPaymentsUrl];
    
    //Load the request in the UIWebView.
    
    [webview loadRequest:requestObj];
    
    [DejalBezelActivityView activityViewForView:webview withLabel:@"Processing Please Wait..."];
    
   // [DejalBezelActivityView removeView];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView
{

    [DejalBezelActivityView removeView];
    
    NSString *yourstring = [webview stringByEvaluatingJavaScriptFromString:

                            @"document.body.innerText"];

    if ([yourstring isEqualToString:@"Transaction Cancelled"]) {
        
        [webview removeFromSuperview];
        
        [global AlertMethod:Alert :@"You have cancelled the transaction"];
        
        [self methodOnActionSaveCards];
        
    }
    
    NSLog(@">>>>>>%@",yourstring);

}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType

{
    
    if([request.URL.path isEqualToString:@"/mobile/details.aspx"])
        
    {
        
        NSURLComponents *components = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        
        NSArray *queryItems = [components queryItems];
        
        
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        
        
        
        for (NSURLQueryItem *item in queryItems)
            
        {
            
            [dict setObject:[item value] forKey:[item name]];
            
        }
        
        NSLog(@"Dictionary--->%@",dict);
        
        [self afterNewCardPaymentResponse :dict];        
        
        return YES;
        
    }
    return YES;
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Success New Card Payment Methods-----------------
//============================================================================
//============================================================================
-(void)afterNewCardPaymentResponse :(NSDictionary*)dictDataOnResponse{
    
    [webview removeFromSuperview];
    
    dictResponseElementPaymentSucceess=dictDataOnResponse;
    
    NSLog(@"Response on Payment Success Element====%@",dictResponseElementPaymentSucceess);
    
    if ([[dictResponseElementPaymentSucceess valueForKey:@"ExpressResponseMessage"] isEqualToString:@"Approved"]) {
        
        [self SaveTransactionDetail];

    } else {
        
        [global AlertMethod:Alert :ErrorPaymentFailed];
    }
}
*/

//============================================================================
//============================================================================
#pragma mark- ---------------------GET SAVE CARDS METHODS-----------------
//============================================================================
//============================================================================

-(void)getSavedCardsDetails{
    

    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else{
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];

        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",MainUrl,UrlSalesGetSavedCardsDetails,strCoreCompanyId,UrlSalesAccountNo,@"5922262391"];
            
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Processing Please Wait..."];
        
        //============================================================================
        //============================================================================
        
        NSString *strType=@"getSavedCardSales";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         //  arrOfTaskList=response;
                         if (response.count==0) {
                             
//                             UIAlertView *alert =[[UIAlertView alloc]initWithTitle:Alert message:Sorry delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                             [alert show];
                             
                         } else {
                             
                             [self methodGetTransactionId];
                             arrOfSavedCardsDetails=(NSArray*)response;
                             
                         }
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
    }
}

-(void)methodOnOpeningSavedCard{
    
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    
    if (arrOfSavedCardsDetails.count==0) {
        [global AlertMethod:Info :NoDataAvailableee];
    }else{
        tblData.tag=101;
        [self tableLoad:tblData.tag];
    }

}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    [tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
}

//============================================================================
//============================================================================
#pragma mark- ------------METHODS--------------
//============================================================================
//============================================================================
-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        NSLog(@"%lu",(unsigned long)arrOfSavedCardsToSend.count);
        return arrOfSavedCardsToSend.count;
    }else
    return [arrOfSavedCardsDetails count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
- (void)configureCell:(SavedCardsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *dictData=arrOfSavedCardsToSend[indexPath.row];
    
    NSString *strIndex=[dictData valueForKey:@"indexpath"];
    NSString *strAmount=[dictData valueForKey:@"amountPaying"];
    
    if ([strIndex isEqualToString:@"-1"]) {
        
        [cell.btnSelectCards setTitle:@"Select Cards" forState:UIControlStateNormal];

    } else {
        
        int indexxx=[strIndex intValue];
        
        NSDictionary *dictDataa=arrOfSavedCardsDetails[indexxx];
        [cell.btnSelectCards setTitle:[dictDataa valueForKey:@"LastFour"] forState:UIControlStateNormal];
        
    }
    
    cell.txtFieldAmount.text=strAmount;
    cell.txtFieldAmount.tag=indexPath.row;
    cell.txtFieldAmount.delegate=self;
    cell.txtFieldAmount.keyboardType=UIKeyboardTypeDecimalPad;
    
    cell.btnSelectCards.tag=indexPath.row;
    [cell.btnSelectCards addTarget:self
                                   action:@selector(clickSelectCard:) forControlEvents:UIControlEventTouchDown];

    
}

-(void)clickSelectCard:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    indexPathOfButtonBeingEdited = indexpath;
    
    NSDictionary *dictData=arrOfSavedCardsToSend[indexpath.row];
    
    NSString *strIndex=[dictData valueForKey:@"indexpath"];
    
    if ([strIndex isEqualToString:@"-1"]) {
        
        strGlobalCardLastFourNo=@"";
        
    } else {
        
        int indexxx=[strIndex intValue];
        
        NSDictionary *dictDataa=arrOfSavedCardsDetails[indexxx];
        strGlobalCardLastFourNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"LastFour"]];
        
    }
    
    [self methodOnOpeningSavedCard];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1) {
        
        SavedCardsTableViewCell *cell = (SavedCardsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SavedCardsTableViewCell" forIndexPath:indexPath];
        
        // Configure Table View Cell
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }
    else {
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (!(arrOfSavedCardsDetails.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictData=[arrOfSavedCardsDetails objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"LastFour"];
                
                if ([cell.textLabel.text isEqualToString:strGlobalCardLastFourNo]) {
                    
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    
                } else {
                    
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    
                }
                //_Btn_SalesPerson
                break;
            }
            case 102:
            {
                //_Btn_Priority
                NSDictionary *dictData=[arrOfSavedCardsDetails objectAtIndex:indexPath.row];
                cell.textLabel.text=[dictData valueForKey:@"Name"];
                break;
            }
            default:
                break;
        }
        
    }
    if ([UIScreen mainScreen].bounds.size.height==667) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        cell.textLabel.font=[UIFont systemFontOfSize:17];
    }
    else
        cell.textLabel.font=[UIFont systemFontOfSize:15];
    return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==1) {
        
        
        
    }else{
    if (!(arrOfSavedCardsDetails.count==0)) {
        
        NSInteger i;
        i=tblData.tag;
        switch (i)
        {
            case 101:
            {
                NSDictionary *dictData=[arrOfSavedCardsToSend objectAtIndex:indexPathOfButtonBeingEdited.row];
                
                NSDictionary *userDictionary = @{@"indexpath":[NSString stringWithFormat:@"%ld",(long)indexPath.row],
                                                 @"amountPaying":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"amountPaying"]],
                                                 };
                
                [arrOfSavedCardsToSend replaceObjectAtIndex:indexPathOfButtonBeingEdited.row withObject:userDictionary];
                [_tblViewSavedCards reloadData];

                break;
            }
            case 102:
            {
                //NSDictionary *dictData=[arrOfSavedCardsDetails objectAtIndex:indexPath.row];
                break;
            }
                
            default:
                break;
        }
        
    }
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==1) {
        
        return true;
        
    }else
        return false;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag==1) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [arrOfSavedCardsToSend removeObjectAtIndex:indexPath.row];
            
            _tblViewSavedCards.frame=CGRectMake(0, 0, _tblViewSavedCards.frame.size.width, arrOfSavedCardsToSend.count*52);
            
            [_tblViewSavedCards setContentSize:CGSizeMake( _tblViewSavedCards.frame.size.width,arrOfSavedCardsToSend.count*52)];
            
            _btnAddNewRowSavedCard.frame=CGRectMake(_btnAddNewRowSavedCard.frame.origin.x, _tblViewSavedCards.frame.size.height+_tblViewSavedCards.frame.origin.y+10, _btnAddNewRowSavedCard.frame.size.width, _btnAddNewRowSavedCard.frame.size.height);
            
            _btnPayNowSavedCards.frame=CGRectMake(_btnPayNowSavedCards.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnPayNowSavedCards.frame.size.width, _btnPayNowSavedCards.frame.size.height);
            
            _btnCancelSavedCard.frame=CGRectMake(_btnCancelSavedCard.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnCancelSavedCard.frame.size.width, _btnCancelSavedCard.frame.size.height);
            
            CGRect frameForSaveCard=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _btnCancelSavedCard.frame.size.height+_btnCancelSavedCard.frame.origin.y+10);
            [_viewSavedCards setFrame:frameForSaveCard];
            
            //_scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, _scrollVieww.frame.size.width, _viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50);
            
            [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50)];

            [_tblViewSavedCards reloadData];
            
        }
    }
}

-(void)methodToSetViews{
    
    //Yaha Change kiyaa
    CGRect frameForGeneralInfo=CGRectMake(10, 0, [UIScreen mainScreen].bounds.size.width-20, _viewGeneralInfo.frame.size.height);
    [_viewGeneralInfo setFrame:frameForGeneralInfo];
    [_scrollVieww addSubview:_viewGeneralInfo];

    CGRect frameForBillingInfo=CGRectMake(10, _viewGeneralInfo.frame.origin.y+_viewGeneralInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _viewBillinfInfo.frame.size.height);
    [_viewBillinfInfo setFrame:frameForBillingInfo];
    [_scrollVieww addSubview:_viewBillinfInfo];

    
    CGRect frameForPaymentInfo=CGRectMake(10, _viewBillinfInfo.frame.origin.y+_viewBillinfInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _viewPaymentInfo.frame.size.height);
    [_viewPaymentInfo setFrame:frameForPaymentInfo];
    [_scrollVieww addSubview:_viewPaymentInfo];

    
    _tblViewSavedCards.frame=CGRectMake(0, 0, _tblViewSavedCards.frame.size.width, 0);
    
    _btnAddNewRowSavedCard.frame=CGRectMake(_btnAddNewRowSavedCard.frame.origin.x-10, _tblViewSavedCards.frame.size.height+_tblViewSavedCards.frame.origin.y+10, _btnAddNewRowSavedCard.frame.size.width, _btnAddNewRowSavedCard.frame.size.height);
    
    _btnPayNowSavedCards.frame=CGRectMake(_btnPayNowSavedCards.frame.origin.x-10, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnPayNowSavedCards.frame.size.width, _btnPayNowSavedCards.frame.size.height);

    _btnCancelSavedCard.frame=CGRectMake(_btnCancelSavedCard.frame.origin.x-10, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnCancelSavedCard.frame.size.width, _btnCancelSavedCard.frame.size.height);

    CGRect frameForSaveCard=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _viewSavedCards.frame.size.height);
    [_viewSavedCards setFrame:frameForSaveCard];
    [_scrollVieww addSubview:_viewSavedCards];

    _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, _scrollVieww.frame.size.width, _viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50);

    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50)];

}
- (IBAction)action_SavedCards:(id)sender {
    
    if ([_btnSavedCards.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
    }else{
    
    [_viewNewCard removeFromSuperview];
    [_viewSavedCards setHidden:NO];
    [self methodOnActionSaveCards];
    isDefaultSaveCards=YES;
    [_btnSavedCards setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnNewCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
}
-(void)methodOnActionSaveCards{
    
    _tblViewSavedCards.frame=CGRectMake(0, 0, _tblViewSavedCards.frame.size.width, arrOfSavedCardsToSend.count*52);
    
    [_tblViewSavedCards setContentSize:CGSizeMake( _tblViewSavedCards.frame.size.width,arrOfSavedCardsToSend.count*52)];
    
    _btnAddNewRowSavedCard.frame=CGRectMake(_btnAddNewRowSavedCard.frame.origin.x, _tblViewSavedCards.frame.size.height+_tblViewSavedCards.frame.origin.y+10, _btnAddNewRowSavedCard.frame.size.width, _btnAddNewRowSavedCard.frame.size.height);
    
    _btnPayNowSavedCards.frame=CGRectMake(_btnPayNowSavedCards.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnPayNowSavedCards.frame.size.width, _btnPayNowSavedCards.frame.size.height);
    
    _btnCancelSavedCard.frame=CGRectMake(_btnCancelSavedCard.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnCancelSavedCard.frame.size.width, _btnCancelSavedCard.frame.size.height);
    
    CGRect frameForSaveCard=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _btnCancelSavedCard.frame.size.height+_btnCancelSavedCard.frame.origin.y+10);
    [_viewSavedCards setFrame:frameForSaveCard];
    
    //_scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, _scrollVieww.frame.size.width, _viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50);
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50)];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    [_scrollVieww setContentOffset:bottomOffset animated:YES];
    
    [_tblViewSavedCards reloadData];

}
- (IBAction)action_NewCard:(id)sender {
    
    if ([_btnNewCard.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
    }else{
    
    [_viewSavedCards setHidden:YES];
    [self methodCancelSavedCards];
    [self addViewForNewCard];
    isDefaultSaveCards=NO;
    [_btnSavedCards setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNewCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    }
    
}
-(void)addViewForNewCard{
    
    CGRect frameForNewCardInfo=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _viewNewCard.frame.size.height);
    [_viewNewCard setFrame:frameForNewCardInfo];
    [_scrollVieww addSubview:_viewNewCard];

}
- (IBAction)action_AddNewRowSavedCard:(id)sender {
    
    
    NSDictionary *userDictionary = @{@"indexpath":[NSString stringWithFormat:@"%@",@"-1"],
                                     @"amountPaying":[NSString stringWithFormat:@"%@",@"0.00"],
                                     };

    [arrOfSavedCardsToSend addObject:userDictionary];
    
    _tblViewSavedCards.frame=CGRectMake(0, 0, _tblViewSavedCards.frame.size.width, arrOfSavedCardsToSend.count*52);
    
    [_tblViewSavedCards setContentSize:CGSizeMake( _tblViewSavedCards.frame.size.width,arrOfSavedCardsToSend.count*52)];

    _btnAddNewRowSavedCard.frame=CGRectMake(_btnAddNewRowSavedCard.frame.origin.x, _tblViewSavedCards.frame.size.height+_tblViewSavedCards.frame.origin.y+10, _btnAddNewRowSavedCard.frame.size.width, _btnAddNewRowSavedCard.frame.size.height);
    
    _btnPayNowSavedCards.frame=CGRectMake(_btnPayNowSavedCards.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnPayNowSavedCards.frame.size.width, _btnPayNowSavedCards.frame.size.height);
    
    _btnCancelSavedCard.frame=CGRectMake(_btnCancelSavedCard.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnCancelSavedCard.frame.size.width, _btnCancelSavedCard.frame.size.height);
    
    CGRect frameForSaveCard=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _btnCancelSavedCard.frame.size.height+_btnCancelSavedCard.frame.origin.y+10);
    [_viewSavedCards setFrame:frameForSaveCard];
    
    //_scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, _scrollVieww.frame.size.width, _viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50);
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50)];

    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    [_scrollVieww setContentOffset:bottomOffset animated:YES];
    
    [_tblViewSavedCards reloadData];
}

- (IBAction)action_PayNowSavedCards:(id)sender {
    
    if (arrOfSavedCardsToSend.count==0) {
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:Alert message:@"Please select cards and amount" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];

        
    } else {
        
    BOOL yesAlert;
    
    yesAlert=NO;
    
    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"123456789"];
    
    for (int k=0; k<arrOfSavedCardsToSend.count; k++) {
        
        NSDictionary *dictData=arrOfSavedCardsToSend[k];
        
        NSString *strIndex=[dictData valueForKey:@"indexpath"];
        NSString *strAmount=[dictData valueForKey:@"amountPaying"];
        NSRange range1 = [strAmount rangeOfCharacterFromSet:cset];

        if ([strIndex isEqualToString:@"-1"] || [strAmount isEqualToString:@"0.00"] || [strAmount isEqualToString:@""] || (range1.location == NSNotFound)) {
            
            yesAlert=YES;
            
        }
    }
    
    if (yesAlert) {
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:Alert message:@"Please fill all selected card details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];
        
    } else {
        
        [self CheckCreditCardStatus];
        
    }
        
    }
    
}

- (IBAction)action_CancelSavedCards:(id)sender {

    [self methodCancelSavedCards];
    
}

-(void)methodCancelSavedCards{
    
    arrOfSavedCardsToSend=nil;
    
    arrOfSavedCardsToSend=[[NSMutableArray alloc]init];
    
    _tblViewSavedCards.frame=CGRectMake(0, 0, _tblViewSavedCards.frame.size.width, 0);
    
    [_tblViewSavedCards setContentSize:CGSizeMake( _tblViewSavedCards.frame.size.width,arrOfSavedCardsToSend.count*52)];
    
    _btnAddNewRowSavedCard.frame=CGRectMake(_btnAddNewRowSavedCard.frame.origin.x, _tblViewSavedCards.frame.size.height+_tblViewSavedCards.frame.origin.y+10, _btnAddNewRowSavedCard.frame.size.width, _btnAddNewRowSavedCard.frame.size.height);
    
    _btnPayNowSavedCards.frame=CGRectMake(_btnPayNowSavedCards.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnPayNowSavedCards.frame.size.width, _btnPayNowSavedCards.frame.size.height);
    
    _btnCancelSavedCard.frame=CGRectMake(_btnCancelSavedCard.frame.origin.x, _btnAddNewRowSavedCard.frame.size.height+_btnAddNewRowSavedCard.frame.origin.y+10, _btnCancelSavedCard.frame.size.width, _btnCancelSavedCard.frame.size.height);
    
    CGRect frameForSaveCard=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, _btnCancelSavedCard.frame.size.height+_btnCancelSavedCard.frame.origin.y+10);
    [_viewSavedCards setFrame:frameForSaveCard];
    
    //_scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, _scrollVieww.frame.size.width, _viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50);
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewSavedCards.frame.origin.y+_viewSavedCards.frame.size.height+50)];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    [_scrollVieww setContentOffset:bottomOffset animated:YES];
    
    [_tblViewSavedCards reloadData];

}
//============================================================================
//============================================================================
#pragma mark- Text Field Delegate Methods
//============================================================================
//============================================================================
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }else
        return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
//    leftView.backgroundColor = [UIColor clearColor];
//    
//    textField.leftView = leftView;
//    
//    textField.leftViewMode = UITextFieldViewModeAlways;
//    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    NSDictionary *dictData=[arrOfSavedCardsToSend objectAtIndex:textField.tag];
    
    NSDictionary *userDictionary = @{@"indexpath":[NSString stringWithFormat:@"%@",[dictData valueForKey:@"indexpath"]],
                                     @"amountPaying":[NSString stringWithFormat:@"%@",textField.text],
                                     };
    
    [arrOfSavedCardsToSend replaceObjectAtIndex:textField.tag withObject:userDictionary];
    [_tblViewSavedCards reloadData];

}

- (IBAction)action_CheckNewCardSaveCardDetail:(id)sender {
    
    //check_box_1.png
    
    if ([_btnNewCardSaveDetails.currentImage isEqual:[UIImage imageNamed:@"check_box_1.png"]]) {
        isForSavedCards=YES;
        [_btnNewCardSaveDetails setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        isForSavedCards=NO;
        [_btnNewCardSaveDetails setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)action_PayNowNewCard:(id)sender {
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else{
        
        // VTPHostConfiguration* hostConfiguration = self.vtpConfiguration.hostConfiguration;
        
        
        [self performSelector:@selector(generatingUrlForPayment) withObject:nil afterDelay:0.2];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- Payments Methods
//============================================================================
//============================================================================

-(void)CheckCreditCardStatus{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else{
        
        [self performSelector:@selector(methodCheckCreditCardStatus) withObject:nil afterDelay:0.2];
        
    }
}

-(void)methodCheckCreditCardStatus{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Processing Please Wait..."];
    
    NSMutableArray *arrOfCreditCardToCheckstatus=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfSavedCardsToSend.count; k++) {
        
        NSDictionary *dicData=arrOfSavedCardsToSend[k];
        
        int  indexPathh=[[dicData valueForKey:@"indexpath"] intValue];
        
        NSDictionary *dictDataSavedCards=arrOfSavedCardsDetails[indexPathh];
        
        NSDictionary *dict_ToSend = @{@"CardId":[NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"CardId"]],
                                      @"CompanyId":[NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"CompanyId"]],
                                      @"AccountNo":[NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"AccountNo"]],
                                      @"PaymentAccountID": [NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"PaymentAccountID"]],
                                      @"PaymentGateway": @"1",
                                      };

        
        [arrOfCreditCardToCheckstatus addObject:dict_ToSend];
        
    }

    NSDictionary *dict_FinalJson = @{@"CardRequestlst":arrOfCreditCardToCheckstatus,
                                     };

    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_FinalJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_FinalJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Checking Credit Card Status: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesCheckCreditcardStatus];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnSalesPaymentServices:strUrl :@"CheckAllCreditCardForMobile" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     
                     NSArray *arrOfResponseCreditCardStatus=(NSArray*)response;
                     BOOL isExpiredCreditCard;
                     isExpiredCreditCard=NO;
                     
                     for (int k=0; k<arrOfResponseCreditCardStatus.count; k++) {
                         
                         NSDictionary *dictResponseData=arrOfResponseCreditCardStatus[k];
                         
                         BOOL isExpired=[[dictResponseData valueForKey:@"IsExpired"] boolValue];
                         
                         if (isExpired) {
                             
                             isExpiredCreditCard=YES;
                             
                         }
                     }
                     
                     
                     if (!isExpiredCreditCard) {

                         [self methodProcessPaymentForMobile];
                         
                     } else {
                         
                         [DejalBezelActivityView removeView];
                         NSString *strTitle = Alert;
                         NSString *strMsg = ErrorCreditCardStatusFailed;
                         [global AlertMethod:strTitle :strMsg];

                     }
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)methodProcessPaymentForMobile{
 
    NSMutableArray *arrOfCreditCardToCheckstatus=[[NSMutableArray alloc]init];
    
    if (_lblAddress1.text.length==0) {
        
        _lblAddress1.text=@"";
        
    }
    if (_lblCity.text.length==0) {
        
        _lblCity.text=@"";
        
    }
    if (_lblState.text.length==0) {
        
        _lblState.text=@"";
        
    }
    if (_lblZipcode.text.length==0) {
        
        _lblZipcode.text=@"";
        
    }

    NSDictionary *dict_BillingAddress = @{@"BillingAddress1":[NSString stringWithFormat:@"%@",_lblAddress1.text],
                                  @"BillingCity":[NSString stringWithFormat:@"%@",_lblCity.text],
                                  @"BillingState":[NSString stringWithFormat:@"%@",_lblState.text],
                                  @"BillingZipcode": [NSString stringWithFormat:@"%@",_lblZipcode.text],
                                  @"BillingName": [NSString stringWithFormat:@"%@",[matches valueForKey:@"customerName"]],
                                  @"BillingEmail": [NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]],
                                  };

    for (int k=0; k<arrOfSavedCardsToSend.count; k++) {
        
        NSDictionary *dicData=arrOfSavedCardsToSend[k];
        
        int  indexPathh=[[dicData valueForKey:@"indexpath"] intValue];
        
        NSDictionary *dictDataSavedCards=arrOfSavedCardsDetails[indexPathh];
        
        NSDictionary *dict_ToSend = @{@"TransactionId":[NSString stringWithFormat:@"%@",[dictResponseTransactionId valueForKey:@"TransactionId"]],
                                      @"CompanyId":[NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"CompanyId"]],
                                      @"TransactionAmount":[NSString stringWithFormat:@"%@",[dicData valueForKey:@"amountPaying"]],
                                      @"PaymentAccountID": [NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"PaymentAccountID"]],
                                      @"PaymentAccountReferenceNumber": [NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"PaymentReferenceNo"]],
                                      @"ReturnURL": @"",
                                      @"CustomCss": @"",
                                      @"CreatedBy": [NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"CreatedBy"]],
                                      @"IsSaveCard": [NSString stringWithFormat:@"%@",[dictDataSavedCards valueForKey:@"IsSaveCard"]],
                                      @"PaymentGateway": @"1",
                                      @"PaymentMethod": @"1",
                                      @"RequestType": @"2",
                                      @"BillingAddress": dict_BillingAddress,
                                      };
        
        [arrOfCreditCardToCheckstatus addObject:dict_ToSend];
        
    }
    
    NSDictionary *dict_FinalJson = @{@"lstPaymentRequest":arrOfCreditCardToCheckstatus,
                                          };

    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_FinalJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_FinalJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Final Json Card Payment process: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesProcessPayment];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnSalesPaymentServices:strUrl :@"ProcessPaymentForMobile" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {                     
                     
                     /*
                     {
                         AVSResponseCode = "";
                         CVVResponseCode = "";
                         CardLogo = "";
                         ExpressResponseCode = 103;
                         ExpressResponseMessage = "PAYMENT ACCOUNT NOT FOUND";
                         IframeUrl = "";
                         IsSuccess = 0;
                         LastFour = "";
                         PaymentAccountID = "";
                         PaymentAccountReferenceNumber = "";
                         TransactionID = "";
                         TransactionSetupID = "";
                         ValidationCode = "";
                     }
                      */
                     
                     NSArray *arrOfPaymentMobileResponse=(NSArray*)response;
                     
                     [self methodCancelSavedCards];
                     
                     NSString *strTitle = Info;
                     NSString *strMsg = PaymentSuccess;
                     [global AlertMethod:strTitle :strMsg];
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}


//sadfhsadjf

-(void)SaveTransactionDetail{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
        
    }
    else{
        
        [self performSelector:@selector(methodSaveTransactionDetails) withObject:nil afterDelay:0.2];
        
    }
}

-(void)methodSaveTransactionDetails{
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Processing Please Wait..."];
    
    NSDate *currentDate=[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringCurrentDate = [formatter stringFromDate:currentDate];

    NSDictionary *dict_FinalJson = @{@"TransPaymentId":@"",
                                  @"TransactionId":[NSString stringWithFormat:@"%@",[dictResponseElementPaymentSucceess valueForKey:@"TransactionID"]],
                                  @"PaymentGateway":@"Element",
                                  @"PaymentMethod": @"Credit Card",
                                  @"RequestType": @"Setup",
                                  @"RequestParam": @"RequestParam",
                                  @"ResponseParam": @"ResponseParam",
                                  @"IsSuccess": @"true",
                                  @"IsActive": @"true",
                                  @"CreatedDate": @"",
                                  @"CreatedBy": strEmployeeNameMain,
                                  @"ModifiedDate": @"",
                                  @"ModifiedBy": strEmployeeNameMain,
                                  };
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_FinalJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_FinalJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Saving Transaction Details Json: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesSaveTransactionDetail];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnSalesPaymentServices:strUrl :@"SaveTransactionDetail" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     
                     NSString *strTrue=[response valueForKey:@"Response"];
                     
                     if ([strTrue isEqualToString:@"true"]) {
                         
                         [self methodSaveCreditCardDetailsToServer];
                         
                     } else {
                         
                     }
                 }
                 else
                 {
                     [DejalBezelActivityView removeView];
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}

-(void)methodSaveCreditCardDetailsToServer{
    
    NSDictionary *dict_FinalJson = @{@"LastFour":[NSString stringWithFormat:@"%@",[dictResponseElementPaymentSucceess valueForKey:@"LastFour"]],
                                  @"CompanyId":strEmpId,
                                  @"IsActive":@"true",
                                  @"ModifiedBy": strEmployeeNameMain,
                                  @"CreatedBy": strEmployeeNameMain,
                                  @"AccountNo": strAccountNo,
                                  @"PaymentReferenceNo": strPaymentreferenceNo,
                                  @"PaymentAccountID": [NSString stringWithFormat:@"%@",[dictResponseElementPaymentSucceess valueForKey:@"TransactionSetupID"]],
                                  };

    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_FinalJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_FinalJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Final Json Card Payment process: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesAddUpdateCreditCard];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnSalesPaymentServices:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+150)];
                     
                     [self.scrollVieww setContentOffset:CGPointZero animated:YES];
                     
                     NSString *strTitle = Info;
                     NSString *strMsg = PaymentSuccess;
                     [global AlertMethod:strTitle :strMsg];
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}


-(void)methodGetTransactionId{
    
    NSDictionary *dict_FinalJson = @{@"TransactionId":@"",
                                     @"AccountNo":strAccountNo,
                                     @"LeadNo":strLeadNo,
                                     @"PaidAmount": strGlobalAmountToBePaid,
                                     @"CompanyKey": strCompanyKeyy,
                                     @"CreditNotes": @"",
                                     @"SalesCreditMemoServices": arrOfFinalServices,
                                     };
    
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_FinalJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_FinalJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Final Json Get Transaction Id: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlSalesGetTransactionId];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global responseOnSalesPaymentServices:strUrl :@"" :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [DejalBezelActivityView removeView];
                 if (success)
                 {
                     
                     dictResponseTransactionId = response;
                     
                     
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Db Fetch Methods-----------------
//============================================================================
//============================================================================


-(void)salesLeadInfoFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
       
        matches=arrAllObj[0];
        
        _lblAddress1.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingAddress1"]];
        _lblAddress2.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingAddress2"]];
        _lblCity.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingCity"]];
        _lblState.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingState"]];
        _lblZipcode.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingZipcode"]];
        _lblCountry.text=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingCountry"]];
        _lblName.text=[NSString stringWithFormat:@"Name: %@",[matches valueForKey:@"customerName"]];
        _lblEmail.text=[NSString stringWithFormat:@"Email: %@",[matches valueForKey:@"primaryEmail"]];
        _lblContactNO.text=[NSString stringWithFormat:@"Contact No: %@",[matches valueForKey:@"primaryPhone"]];
        
    }
    
    [self fetchFromCoreDataStandard];
}


-(void)fetchFromCoreDataStandard
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObjStandard = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjStandard.count==0)
    {
        
        
    }else
    {
        for (int k=0; k<arrAllObjStandard.count; k++) {
            
            matchesStandard = arrAllObjStandard[k];
            
            NSDictionary *dict_FinalJson = @{@"Price":[matchesStandard valueForKey:@"initialPrice"],
                                             @"ServiceId":[matchesStandard valueForKey:@"serviceId"],
                                             @"ServiceName":[matchesStandard valueForKey:@"serviceSysName"],
                                             @"ServiceSysName": [matchesStandard valueForKey:@"serviceSysName"],
                                             @"ServiceType": @"Standard",
                                             };
            
            [arrOfFinalServices addObject:dict_FinalJson];
        }
    }
    
    [self fetchFromCoreDataNonStandard];
}

-(void)fetchFromCoreDataNonStandard
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObjNonStandard = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjNonStandard.count==0)
    {
        
        
    }else
    {
        for (int k=0; k<arrAllObjNonStandard.count; k++) {
            
            matchesNonStandard = arrAllObjNonStandard[k];
            
            NSDictionary *dict_FinalJson = @{@"Price":[matchesNonStandard valueForKey:@"initialPrice"],
                                             @"ServiceId":@"",
                                             @"ServiceName":[matchesNonStandard valueForKey:@"serviceName"],
                                             @"ServiceSysName": @"",
                                             @"ServiceType": @"NonStandard",
                                             };
            
            [arrOfFinalServices addObject:dict_FinalJson];
        }
    }
    
    [self fetchPaymentInfo];
}

-(void)fetchPaymentInfo{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObjPaymentInfo = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjPaymentInfo.count==0)
    {
        
        strGlobalAmountToBePaid=@"0";
        _lblPaymentAmount.text=[NSString stringWithFormat:@"Payment Amount: $%@",strGlobalAmountToBePaid];
        
    }
    else
    {
        
        matchesPaymentInfo=arrAllObjPaymentInfo[0];
        strGlobalAmountToBePaid=[NSString stringWithFormat:@"%@",[matchesPaymentInfo valueForKey:@"amount"]];
        _lblPaymentAmount.text=[NSString stringWithFormat:@"Payment Amount: $%@",strGlobalAmountToBePaid];
    }
}

@end
