//
//  SavedCardsTableViewCell.m
//  DPS
//
//  Created by Saavan Patidar on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//  Saavan Patidar 2021

#import "SavedCardsTableViewCell.h"

@implementation SavedCardsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
