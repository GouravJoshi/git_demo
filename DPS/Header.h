//
//  Header.h
//  Created by Saavan Patidar Ji 2021
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Patidar 2021
//  Saavan Patidar 2021
//  Saavan 05 Agust 2021
//  Saavan 2021 Latest commit from Nilind Nilind

#ifndef Header_h
#define Header_h

// Staging Url Main ----http://tcrms.stagingsoftware.com/

// Testing Url -------http://tcrs.stagingsoftware.com/

//New Production Server Url ------http://pcrs.pestream.com/
//ElementTypeProductionOrtesting--------  Testing  Production
//GGQ Production--------http://www.gogetquality.com/survey/
//GGQ Staging-----------http://ggq.stagingsoftware.com/

// Testing----- state_list.json
// Staging----- state_list_staging.json
// Production----

//===================================================================================================
//===================================================================================================


//===================================================================================================
//===================================================================================================


#pragma mark- --------- Staging URL's --------------


 #define MainUrl @"https://pcrsstaging.pestream.com/"
 //#define MainUrl @"https://tcrs.pestream.com/"
 #define StateJson @"StateListNew_Production.json" // @"StateListNew_Staging.json"
 #define StateJsonSwift @"StateListNew_Production" // @"StateListNew_Staging"
 #define ElementTypeProductionOrtesting @"Testing"
 #define pushNotificationType @"Testing"
 #define UrlSalesEmail @"http://ggq.stagingsoftware.com/GoHandler.ashx?key="
 #define UrlMainGGQ @"http://ggq.stagingsoftware.com/GoHandler.ashx?"
 #define UrlSendEmailServiceAutomation @"http://ggq.stagingsoftware.com/GoHandler.ashx?key=remindersms&aId="
 #define UrlGetCustomMessage @"http://ggq.stagingsoftware.com/GoHandler.ashx?key=getSms&ComKey="
 #define MainUrlWeb @"https://staging.pestream.com/"
 #define MelissaKey @"TJXAP_9TGVGzzPWOqOZy5O**"
 #define UserCurrentTimeZone @"EST"


#pragma mark- --------- Staging URL's END -------------


#pragma mark- ---------- PRODUCTION URL's ------------

/*
#define MainUrl @"https://pcrs.pestream.com/"   // Live Url
#define ElementTypeProductionOrtesting @"Production"
#define pushNotificationType @"Production"
#define StateJson @"StateListNew_Production.json"
#define StateJsonSwift @"StateListNew_Production"
#define UrlSalesEmail @"http://www.gogetquality.com/survey/GoHandler.ashx?key="
#define UrlMainGGQ @"http://www.gogetquality.com/survey/GoHandler.ashx?"
#define UrlSendEmailServiceAutomation @"http://www.gogetquality.com/survey/GoHandler.ashx?key=remindersms&aId="
#define UrlGetCustomMessage @"http://www.gogetquality.com/survey/GoHandler.ashx?key=getSms&ComKey="
#define MainUrlWeb @"https://pestream.com/"
#define MelissaKey @"TJXAP_9TGVGzzPWOqOZy5O**"
#define UserCurrentTimeZone @"EST"
*/

#pragma mark- -------- PRODUCTION URL's ------------

// Remove firebase upload symbol script while uploading App

#define VersionDate @"12/08/2022"//MM/DD/YYYY//commit
#define VersionNumber @"4.1.57(1)"
#define CountryJson @"Country_List_Testing.json"

//============================================================================
//============================================================================
#pragma mark- ---------------------D2D Flow-----------------
//============================================================================
//============================================================================

#define UrlD2DGetEmployeeBranch @"/api/MobileAppToCore/GetEmployeeBranchByEmployeeId?"
#define UrlD2DDeleteWebLead @"/api/LeadNowAppToSalesProcess/DeleteWebLeadById?webLeadId="
#define UrlD2DDeleteLead @"/api/LeadNowAppToSalesProcess/DeleteLeadByLeadNo?companyKey="
#define UrlEditCustomerAddressNotes_DoorToDoor @"/api/LeadNowAppToSalesProcess/EditCustomerAddressNotes_DoorToDoor"

//============================================================================
//============================================================================
#pragma mark- ---------------------PestPac Payment-----------------
//============================================================================
//============================================================================

#define UrlPestPacAccessToken @"https://is.workwave.com/oauth2/token?scope=openid"
#define UrlPestPacHostedPaymentCharge @"https://api.workwave.com/pestpac/v1/PaymentAccounts/hostedPayments/charge?"
#define PestPacUserName @"nitesh.jain@quacito.com"
#define PestPacPassword @"11802War@200"
#define PestPacCompanyKey @"318784"
#define PestPacClient_Id @"dACr1fE2LSeyNy3A4g1iff4_7PIa"
#define PestPacClient_Secret @"Pt7N4lhf4mKAzxKySbVQ2gfuyzca"
#define PestPacGrant_Type @"password"
#define PestPacApikey @"Pa3TAL333bhJ3E9w3iIh9gWuNs8rnQVR"
#define PestPacUpdatePaymentToServer @"api/MobileToSaleAuto/UpdatePaymentTransactionDetail"
#define UrlPestPacHostedPaymentSaveCard @"https://api.workwave.com/pestpac/v1/PaymentAccounts/hostedPayments/store?"
#define PestPacAddCreditCardToServer @"api/MobileToSaleAuto/UpdateCardInfoDetail"

//============================================================================
//============================================================================
#pragma mark- ---------------------CRM Leadnow portion-----------------
//============================================================================
//============================================================================

//===================================================================================================
//===================================================================================================

#define UrlLogin @"api/Account/Login"

//=====================================================================================================

#define UrlAddUpdateDeviceRegistration @"api/LeadNowAppToSalesProcess/AddUpdateDeviceRegistration"

//=====================================================================================================

#define UrlGetLeaderboardDetails @"/api/Dashboard/GetLeaderboardDetails"

//=====================================================================================================

#define UrlChangePassword @"api/Account/ChangePassword"

//=====================================================================================================

#define UrlGetDashBoardDetails @"/api/Dashboard/GetDashboardDetails?"

//=====================================================================================================

#define UrlGetEmployeeList @"/api/LeadNowAppToSalesProcess/GetAllEmployees?companyKey="

//=====================================================================================================

#define UrlGetLeadDetailMaster @"/api/LeadNowAppToSalesProcess/GetAllMasterAsync_sp?companyKey="

//=====================================================================================================

//api/LeadNowAppToSalesProcess/GetLeadCountReportAsync?employeeId=

#define UrlGetLeadCount @"/api/LeadNowAppToSalesProcess/GetLeadCountReportAsync?employeeId="

//=====================================================================================================

#define UrlGetHistory @"/api/LeadNowAppToSalesProcess/GetEmployeeLeadHistoryAsync?lastDaysCount="

#define UrlGetHistoryAddId @"&employeeId="

//=====================================================================================================
//=====================================================================================================

#define UrlImageUpload @"/api/File/UploadAsync"

//=====================================================================================================
//=====================================================================================================

#define UrlSendLeadToServer @"api/LeadNowAppToSalesProcess/AddLead"

#define UrlSendLeadToServerNew @"api/LeadNowAppToSalesProcess/AddLeadWithDirectScheduleOption"

//=====================================================================================================
//=====================================================================================================

#define UrlGetTaskListGlobal @"/api/LeadNowAppToSalesProcess/GetTasksForEmployeeAsync?employeeId="
#define UrlGetTaskListGlobalcompanyKey @"&companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlAddUpdateTaskGlobal @"/api/LeadNowAppToSalesProcess/AddUpdateTask"
#define UrlAddUpdateTaskGlobalMultiple @"/api/LeadNowAppToSalesProcess/AddUpdateTaskMultiple"

//=====================================================================================================
//=====================================================================================================

#define UrlDeleteTaskGlobal @"/api/LeadNowAppToSalesProcess/DeleteTaskById?leadTaskId="

//=====================================================================================================
//=====================================================================================================

#define UrlGetActivityListGlobal @"/api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsync?refId="
#define UrlGetActivityListGlobalrefType @"&refType=Employee"
#define UrlGetActivityListGlobalcompanykey @"&companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlAddUpdateActivityGlobal @"/api/LeadNowAppToSalesProcess/AddUpdateActivity"

//=====================================================================================================
//=====================================================================================================

#define UrlDeleteActivityGlobal @"/api/LeadNowAppToSalesProcess/DeleteActivityById?activityId="

//=====================================================================================================
//=====================================================================================================

//----------------------------------------------***************--------------------------------------------------------

#define UrlGetTaskList @"/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsync?refId="
#define UrlGetTaskListrefType @"&refType=Lead"
#define UrlGetTaskListcompanyKey @"&companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetActivityList @"/api/LeadNowAppToSalesProcess/GetActivitiesByRefIdAndRefTypeAsync?refId="
#define UrlGetActivityListrefType @"&refType=Lead"
#define UrlGetActivityListcompanyKey @"&companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlAddUpdateActivity @"/api/LeadNowAppToSalesProcess/AddUpdateActivity"
#define UrlAddUpdateActivityMultiple @"/api/LeadNowAppToSalesProcess/AddUpdateActivityMultiple"

//=====================================================================================================
//=====================================================================================================

#define UrlDeleteActivity @"api/LeadNowAppToSalesProcess/AddUpdateLead"

//=====================================================================================================
//=====================================================================================================

#define UrlGetTotalLead @"api/LeadNowAppToSalesProcess/GetAllLeadsForEmployeeAsync?lastModifiedDate="

//=====================================================================================================
//=====================================================================================================

#define UrlToUpdateLeadInfo @"/api/LeadNowAppToSalesProcess/UpdateLead"

//=====================================================================================================
//=====================================================================================================


#define UrlPostComment @"/api/LeadNowAppToSalesProcess/AddCommentToActivity"

//=====================================================================================================
//=====================================================================================================


#define UrlGetServiceAddressesByAccountNo @"/api/LeadNowAppToSalesProcess/GetServiceAddressesByAccountNo?accountNo="

//=====================================================================================================
//=====================================================================================================

#define UrlGetPostAddressesByAccountNo @"/api/LeadNowAppToSalesProcess/SaveCustomerAddressesFromMobile?accountNo="

//=====================================================================================================
//=====================================================================================================


//=====================================================================================================
//=====================================================================================================

#define UrlGetLeadListByAccountNo @"/api/LeadNowAppToSalesProcess/GetLeadsListByAccountNo?CompanyKey="
#define UrlGetLeadListByAccountNoName @"&AccountNo="

//=====================================================================================================
//=====================================================================================================

///api/LeadNowAppToSalesProcess/GetContactListSearch
#define UrlGetContactListSearch @"/api/LeadNowAppToSalesProcess/GetContactListSearch"

//=====================================================================================================
//=====================================================================================================
//GetCrmContactListByAerialDistance

#define UrlGetContactListByAerialDistance @"/api/LeadNowAppToSalesProcess/GetCrmContactListByAerialDistance"

//=====================================================================================================
//=====================================================================================================

#define UrlGetCompanyListByAerialDistance @"/api/LeadNowAppToSalesProcess/GetCrmCompanyListByAerialDistance"

//=====================================================================================================
//=====================================================================================================

#define UrlGetOpportunityListByAerialDistance @"/api/LeadNowAppToSalesProcess/GetOpportunityListByAerialDistance"

//=====================================================================================================
//=====================================================================================================


#define UrlGetWebLeadListByAerialDistance @"/api/LeadNowAppToSalesProcess/GetWebleadListByAerialDistance"

//=====================================================================================================
//=====================================================================================================

#define UrlGetContactDetailByContactId @"/api/LeadNowAppToSalesProcess/GetContactDetailByContactId?CrmContactId="
#define UrlGetContactDetailByContactIdCompanykey @"&CompanyKey="

//=====================================================================================================
//=====================================================================================================

///ess/GetLeadsDetailsByContactId?CrmContactId=40182&CompanyKey=titan
#define UrlGetLeadsDetailsByContactId @"/api/LeadNowAppToSalesProcess/GetLeadsDetailsByContactId?CrmContactId="

//=====================================================================================================
//=====================================================================================================

//rocess/GetOpportunityDetailsByContactId?CrmContactId=40182&CompanyKey=titan
#define UrlGetOpportunityDetailsByContactId @"/api/LeadNowAppToSalesProcess/GetOpportunityDetailsByContactId?CrmContactId="

//=====================================================================================================
//=====================================================================================================

#define UrlGetLeadsDetailsByCompanyId @"/api/LeadNowAppToSalesProcess/GetLeadsDetailsByCrmCompanyId?CrmCompanyId="

//=====================================================================================================
//=====================================================================================================

#define UrlGetOpportunityDetailsByCompanyId @"/api/LeadNowAppToSalesProcess/GetOpportunityDetailsByCrmCompanyId?CrmCompanyId="

//=====================================================================================================
//=====================================================================================================
#define UrlUpdateContactDetailByContactId @"/api/LeadNowAppToSalesProcess/UpdateContactListDetails"

//=====================================================================================================
//=====================================================================================================

#define UpdateCompanyListDetails @"/api/LeadNowAppToSalesProcess/UpdateCompanyListDetails"
//=====================================================================================================
//=====================================================================================================

#define UrlAddNotes @"/api/LeadNowAppToSalesProcess/AddLeadNotes"

//=====================================================================================================
//=====================================================================================================
// GetLeadNotesList?LeadNumber=1&OpportunityNumber=&CompanyKey=titan


#define UrlGetNotes @"/api/LeadNowAppToSalesProcess/GetLeadNotesList?LeadNumber="
#define UrlGetNotesNo @"&OpportunityNumber="

//=====================================================================================================
//=====================================================================================================

#define UrlAddWebLead @"/api/LeadNowAppToSalesProcess/AddWebLead"

//=====================================================================================================


#define UrlAddWebLeadNew @"/api/LeadNowAppToSalesProcess/AddWebLeadv2"

//=====================================================================================================

#define UrlSearchContactNew @"/api/LeadNowAppToSalesProcess/SearchContacts"
//=====================================================================================================

//GetMatchingAccountsToConvertWebLead?webLeadNumber=20&companyKey=titan

#define UrlGetMatchingAccountsToConvertWebLead @"/api/LeadNowAppToSalesProcess/GetMatchingAccountsToConvertWebLead?webLeadNumber="

//=====================================================================================================
//=====================================================================================================
#define URLConvertToOpportunity @"/api/LeadNowAppToSalesProcess/ConvertWebLeadToOpportunity"

//=====================================================================================================
//=====================================================================================================

#define URLEditLeadDetails @"/api/LeadNowAppToSalesProcess/UpdateLeadsDetailsByContactId"

//=====================================================================================================
//=====================================================================================================

#define UrlGetWebLeadsByEmployee @"/api/LeadNowAppToSalesProcess/GetWebLeadsDetailsByEmployeeNo?CompanyKey="
#define UrlGetEmployeeNo @"&EmployeeNo="

//=====================================================================================================
//=====================================================================================================

#define UrlGetEmployeeWorkingTime @"api/MobileToServiceAuto/GetEmployeeWorkingTime"

//=====================================================================================================
//=====================================================================================================

#define URLWebLeadReleaseOrFollowUp @"api/LeadNowAppToSalesProcess/WebLeadReleaseOrFollowUp"

//=====================================================================================================
//=====================================================================================================

#define URLGrabWebLead @"api/LeadNowAppToSalesProcess/GrabWebLead?WebLeadId="

//=====================================================================================================
//=====================================================================================================

#define URLDeadWebLead @"api/LeadNowAppToSalesProcess/MarkWebLeadAsDead?WebLeadId="

//=====================================================================================================
//=====================================================================================================

#define URLGetStastics @"api/LeadNowAppToSalesProcess/GetWebLeadOpportunityDataForMobile?companyKey="


//=====================================================================================================
//=====================================================================================================
//tcrs.pestream.com//api/EmployeeBlockTime/GetEmployeeBlockTimesForMobile?companykey=automation&employeeno=2702545468

#define URLGetEmployeeBlockTime @"api/EmployeeBlockTime/GetEmployeeBlockTimesForMobile?companykey="
#define URLGetEmployeeBlockTimeEmployeeNo @"&employeeno="
#define URLGetEmployeeBlockTime_SP @"api/EmployeeBlockTime/GetEmployeeBlockTimesForMobile_SP?companykey="


//=====================================================================================================
//=====================================================================================================

////api/MobileAppToCore/ManageWorkOrderOnMyWay

#define URLUpdateOnMyWayLifeStyle @"api/MobileAppToCore/ManageWorkOrderOnMyWay"


//=====================================================================================================
//=====================================================================================================

//=====================================================================================================
//=====================================================================================================
//https://pcrs.pestream.com/api/Account/SendForgotPasswordLinkByEmail

#define URLForgotPass @"api/Account/SendForgotPasswordLinkByEmail"

//=====================================================================================================
//=====================================================================================================

#pragma marks   ---------------------------------Service Automation--------------------------------

//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------

//Service Automations


//============================================================================
//============================================================================
#pragma mark- ---------------------Service Automation-----------------
//============================================================================
//============================================================================

//=====================================================================================================
//=====================================================================================================

#define UrlAudioUploadService @"/api/File/UploadAudioAsync"

//=====================================================================================================
//=====================================================================================================

#define UrlGetMasterServiceAutomation @"/api/MobileToServiceAuto/GetMastersByCompanyKey?CompanyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetMasterEquipmentDynamicForm @"/api/MobileToServiceAuto/GetEquipmentInspectionMastersByCompanykey?CompanyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetMasterProductChemicalsServiceAutomation @"/api/MobileToServiceAuto/GetAllProductByCompany?CompanyId="

//=====================================================================================================
//=====================================================================================================

#define UrlGetTotalWorkOrdersServiceAutomation @"/api/MobileToServiceAuto/GetWorkOrderByEmployee?CompanyKey="
#define UrlGetTotalWorkOrdersServiceAutomation_SP @"/api/MobileToServiceAuto/GetWorkOrderByEmployee_sp?CompanyKey="
// 03 August Service Basic Api Implementation

#define UrlGetTotalWorkOrdersServiceAutomation_SPV1 @"/api/MobileToServiceAuto/GetWorkOrderByEmployee_spV1"
#define UrlGetTotalWorkOrdersServiceAutomation_BasicInfo @"/api/MobileToServiceAuto/GetWorkOrdersBasicInfoByEmployee?CompanyKey="

#define UrlGetTotalWorkOrdersServiceAutomationEmployeeNo @"&EmployeeNo="

//=====================================================================================================
//=====================================================================================================
///old wali update method
///api/MobileToServiceAuto/UpdateWorkorderDetail

#define UrlUpdateWorkorderDetail @"/api/MobileToServiceAuto/UpdateWorkorderDetailNew"

//=====================================================================================================
//=====================================================================================================

#define UrlResendMailService @"/api/MobileToServiceAuto/MobileResendInvoiceMail"

//=====================================================================================================
//=====================================================================================================

#define UrlUploadBeforeAfter @"/api/File/UploadImageAsync"

//=====================================================================================================
//=====================================================================================================

#define UrlUploadTechCustomerSign @"/api/File/UploadSignatureAsync"

//=====================================================================================================
//=====================================================================================================

#define UrlServiceDynamicForm @"api/Mobiletoserviceauto/GetJsonFormsByCompanyId?CompanyKey="
#define UrlServiceDynamicFormWorkOrderID @"&WorkorderId="
#define UrlServiceDynamicFormDepartmentId @"&DepartmentId="




//=====================================================================================================
//=====================================================================================================

#define UrlServiceNotesHistory @"/api/LeadNote//GetLeadNotesByAccountNoForMobile?accountNo="

//=====================================================================================================
//=====================================================================================================

#define UrlServiceNotesHistoryNewSales @"/api/LeadNote//GetLeadNotesByAccountNoForMobile?accountNo=%@&companyKey=%@"


//=====================================================================================================
//=====================================================================================================

#define UrlAddNotesSalesNew @"/api/LeadNowAppToSalesProcess/AddUpdateNoteRefTypeRefNo"

//=====================================================================================================
//=====================================================================================================

///Api/MobileToServiceAuto/GetCustomerDocumentsByAccountNo?CompanyKey
#define UrlServiceDocuments @"api/MobileToServiceAuto/GetCustomerDocumentsByAccountNo?CompanyKey="
#define UrlServiceDocumentsAccountNo @"&AccountNo="

//=====================================================================================================
//=====================================================================================================

#define UrlServiceDynamicFormSubmission @"api/Mobiletoserviceauto/UpdateJsonFormsByCompanyId"

//=====================================================================================================
//=====================================================================================================
#define UrlEquipmentDynamicFormSubmission @"api/Mobiletoserviceauto/UpdateEquipmentJsonFormsByCompanyId"

//=====================================================================================================
//=====================================================================================================
///api/MobileToServiceAuto/GetItemInventoryDetailByItemNumber?companyId=3062&itemnumber=898888


//=====================================================================================================
//=====================================================================================================

#define UrlGetItemInventoryDetailByItemNumber @"api/MobileToServiceAuto/GetItemInventoryDetailByItemNumber?companyId="
#define Urlitemnumber @"&itemnumber="

//=====================================================================================================
//=====================================================================================================

//============================================================================
//============================================================================
#pragma mark- --------------------- Sales Automation-----------------
//============================================================================
//============================================================================

#define UrlSalesImageUpload @"/api/File/UploadImageAsync"//UploadImageAsync
#define UrlSalesSignatureImageUpload @"/api/File/UploadSignatureAsync"
#define UrlAudioUploadAsync @"/api/File/UploadAudioAsync"//UploadImageAsync
#define UrlServicePestDocUploadAsync @"/api/File/UploadServiceAddressDocAsync"
#define UrlServicePestConditionDocUploadAsync @"/api/File/UploadConditionImageAsync"
#define UrlWdoGraphXmlUploadAsync @"/api/File/UploadGraphXMLFileAsync"
#define UrlWdoProblemImageUploadAsync @"/api/File/UploadWoWdoProblemIdentificationImageAsync"
#define UrlMelissa @"https://property.melissadata.net/v4/WEB/LookupProperty/"
#define UrlServiceGeneralInfoImageUploadAsync @"/api/File/UploadMultipleImageAsync" //ServiceAuto

//=====================================================================================================
//=====================================================================================================

#pragma marks   ---------------------------------Sales Automation--------------------------------

//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------
//--------------------**************-----------------*********************-----------------******************----------


//=====================================================================================================
//=====================================================================================================

#define UrlGetMasterSalesAutomation @"/api/MobileToSaleAuto/GetMastersByCompanyKey?CompanyKey="

//#define UrlGetMasterSalesAutomation_SP @"/api/MobileToSaleAuto/GetMastersByCompanyKey_SP?CompanyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlSalesInfoGeneral @"/api/MobileToSaleAuto/GetLeadByCompanyKey?CompanyKey="
#define UrlSalesInfoGeneral_SP @"/api/MobileToSaleAuto/GetLeadByCompanyKey_SP?CompanyKey="
#define UrlSalesInfoGeneral_SPV1 @"/api/MobileToSaleAuto/GetLeadByCompanyKey_SPV1?CompanyKey="
#define UrlSalesInfoGeneral_BasicInfo @"/api/MobileToSaleAuto/GetAllLeadBasicInfoByCompanyKey?CompanyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlSalesInspection @"/api/MobileToSaleAuto/GetJsonFormList"

//=====================================================================================================
//=====================================================================================================

#define UrlSalesTaxByServiceAddress @"api/MobileToSaleAuto/GetTaxByServiceAddress?CompanyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlSalesInspectionDynamicFormSubmission @"api/MobileToSaleAuto/UpdateJsonFormsByCompanyId"

//=====================================================================================================
//=====================================================================================================

#define UrlGetSalesInspectionDynamicForm @"/api/MobileToSaleAuto/GetJsonFormsByCompanyId?CompanyKey="
#define UrlGetSalesInspectionDynamicFormleadID @"&leadID="
#define UrlGetSalesInspectionDynamicFormInspectionId @"&InspectionId="

//=====================================================================================================
//=====================================================================================================

#define UrlUpdateSalesSurveyStatus @"api/MobileToSaleAuto/UpdateLeadSurveyStatus?"

#define UrlUpdateServiceSurveyStatus @"api/MobiletoServiceAuto/UpdateWorkorderSurveyStatus?"

//=====================================================================================================
//=====================================================================================================

//#define UrlSalesFinalUpdate @"/api/MobileToSaleAuto/UpdateLeadDetail"
//#define UrlSalesFinalUpdate @"/api/MobileToSaleAuto/UpdateMobileLeadDetail_NewV1"// UpdateMobileLeadDetail_New
#define UrlSalesFinalUpdate @"/api/MobileToSaleAuto/UpdateMobileLeadDetail_NewV2"// UpdateMobileLeadDetail_New


//==========================================================================================================
//==========================================================================================================

#define UrlSalesResendEmail @"/api/MobileToSaleAuto/MobileResendAgreementProposalMail"

//==========================================================================================================
//==========================================================================================================

#define UrlSalesSuggestRoute @"/api/MobileAppToCore/GetSuggestedRouteAndOtherRoutesAsync"

//==========================================================================================================
//==========================================================================================================

#define UrlSalesRangeofTime @"/api/MobileAppToCore/GetAllRangeofTimeAsync?CompanyKey="

//==========================================================================================================
//==========================================================================================================

#define UrlSalesViewAppointment @"/api/MobileAppToCore/GetViewAppointmentAsync"

//==========================================================================================================
//==========================================================================================================

#define UrlSalesSaveInitialSetup @"/api/MobileAppToCore/SaveSetup"

//==========================================================================================================
//==========================================================================================================

#define UrlSalesSaveInitialSetupnGenerateWorkOrder @"/api/MobileAppToCore/SaveSetupandGenerateWorkOrder"

//==========================================================================================================
//==========================================================================================================

//#define UrlSalesDynamicFormMasters @"/api/MobileToSaleAuto/GetInspectionMastersByCompanykey?CompanyKey="
#define UrlSalesDynamicFormMasters @"/api/MobileToSaleAuto/GetAllInspectionMastersByCompanykey?CompanyKey="

//==========================================================================================================
//==========================================================================================================

#define UrlServiceDynamicFormMasters @"/api/MobileToServiceAuto/GetInspectionMastersByCompanykeyNew?CompanyKey="

//==========================================================================================================
//==========================================================================================================
//==========================================================================================================
//==========================================================================================================

#define UrlSalesDynamicFormAllLeads @"/api/MobileToSaleAuto/GetMultipleJsonFormsByLeadandCompanyId?CompanyKey="
#define UrlSalesDynamicFormAllLeadsLeadId @"&LeadId="
//==========================================================================================================
//==========================================================================================================

#define UrlServiceDynamicFormAllWorkOrders @"/api/MobileToServiceAuto/GetMultipleJsonFormsByWorkorderandCompanyId?CompanyKey="
#define UrlServiceDynamicFormAllWorkOrdersWorkOrderID @"&WorkorderId="

//==========================================================================================================
//==========================================================================================================

#define UrlEquipmentsDynamicFormAllWorkOrders @"/api/MobileToServiceAuto/GetMultipleEquipmentJsonFormsByWorkorderandCompanyId?CompanyKey="

//==========================================================================================================
//==========================================================================================================

//==========================================================================================================
#define UrlSalesInitialSetupStatusUpdate @"/api/MobileToSaleAuto/UpdateLeadInitialSetup?leadId="
//==========================================================================================================

//==========================================================================================================
#define UrlSalesDocumentList @"/api/MobileToSaleAuto/GetLeadByLeadNo?CompanyKey="
#define UrlSalesDocumentListLeadNumber @"&leadNumber="
//==========================================================================================================

//==========================================================================================================
#define UrlSalesSignedAgreements @"/api/MobileToSaleAuto/GetSignedAgreementLeads"
//==========================================================================================================

//==========================================================================================================
#define UrlSalesSignedAgreementsBadgeCount @"/api/MobileToSaleAuto/GetSignedAgreementCount?lastdate="
#define UrlSalesSignedAgreementsBadgeCountCompanyKey @"&companyKey="
#define UrlSalesSignedAgreementsBadgeCountEmpId @"&employeeId="

#define UrlUploadTargetImage @"/api/File/UploadTargetImageAsync"
#define UrlDownloadTargetImage @"&employeeId="


//=====================================================================================================

#define UrlGetMasterGetUserDefinedFieldsJsonByEntityList @"/api/MobileAppToCore/GetUserDefinedFieldsJsonByEntityList"

//=====================================================================================================

//============================================================================
//============================================================================
#pragma mark- ---------------------WDO FLow-----------------
//============================================================================
//============================================================================

#define UrlDeviceDynamicFormAllWorkOrders @"/api/MobileToServiceAuto/GetMultipleDeviceJsonFormsByWorkorderandCompanyId?CompanyKey="

//==========================================================================================================
//==========================================================================================================

//============================================================================
//============================================================================

#define UrlWdoMaster @"/api/MobileToServiceAuto/GetWDOMastersByCompanyKey?companykey="

//============================================================================
//============================================================================

//=====================================================================================================
//=====================================================================================================
#define UrlDeviceDynamicFormSubmission @"api/Mobiletoserviceauto/UpdateDeviceJsonFormsByCompanyIdForMultiple"

//=====================================================================================================
//=====================================================================================================

//============================================================================
//============================================================================
#pragma mark- ---------------------Element Payment Sales Automation-----------------
//============================================================================
//============================================================================

//==========================================================================================================
#define UrlSalesGetSavedCardsDetails @"/api/Cardmaster/Getcards?companyId="


#define UrlSalesAccountNo @"&accountNo="

//==========================================================================================================

///api/Payment/CheckAllCreditCardForMobile

#define UrlSalesCheckCreditcardStatus @"/api/Payment/CheckAllCreditCardForMobile"

//==========================================================================================================

///api/Payment/ProcessPaymentForMobile

#define UrlSalesProcessPayment @"/api/Payment/ProcessPaymentForMobile"

//==========================================================================================================

///api/CardMaster/AddUpdateCardMaster

#define UrlSalesAddUpdateCreditCard @"/api/CardMaster/AddUpdateCardMaster"

//==========================================================================================================

///api/Payment/SaveTrancation

#define UrlSalesSaveTransactionDetail @"/api/Payment/SaveTrancation"

//==========================================================================================================

///api/SalesAutoToCore/CreateCreditMemoFromSales


#define UrlSalesGetTransactionId @"/api/SalesAutoToCore/CreateCreditMemoFromSales"

//==========================================================================================================

//============================================================================
//============================================================================
#pragma mark- --------------------- Mechanical Flow-----------------
//============================================================================
//============================================================================

//==========================================================================================================

#define UrlMechanicalGetAllMasters @"/api/MobileToServiceAuto/GetPlumbingMasterByCompanyKey?CompanyKey="
#define UrlMechanicalGetAllMastersNew @"/api/MobileToServiceAuto/GetPlumbingMasterNewByCompanyKey?CompanyKey="

//==========================================================================================================

//==========================================================================================================

#define UrlMechanicalGetAllRepair @"/api/MobileToServiceAuto/GetAllRepairByCompanyKey?CompanyKey="

//==========================================================================================================

//==========================================================================================================

#define UrlMechanicalGetHourConfigMaster @"/api/MobileToServiceAuto/GetHourConfigMasterByCompanyKey?CompanyKey="

//==========================================================================================================

//==========================================================================================================

#define UrlMechanicalGetAllLookupPricing @"/api/MobileToServiceAuto/GetAllLookupPricingAsync?CompanyKey="

//==========================================================================================================

#define UrlMechanicalSubWorkOrderStatusUpdate @"/api/MobileToServiceAuto/UpdateSubWOStatusFromGenralInfo?CompanyKey="
#define UrlMechanicalSubWorkOrderTimeIn @"mobileTimeIn="
#define UrlMechanicalSubWorkOrderTimeOut @"mobileTimeOut="
#define UrlMechanicalSubWorkOrderStatusUpdateInspection @"/api/MobileToServiceAuto/UpdateSubWOStatusWhenStartInspention?CompanyKey="

//==========================================================================================================

#define UrlGetTotalWorkOrdersMechanical @"/api/MobileToServiceAuto/GetplumbingWorkOrderByEmployee?CompanyKey="
#define UrlGetTotalWorkOrdersMechanical_SP @"/api/MobileToServiceAuto/GetplumbingWorkOrderByEmployee_SP?CompanyKey="

// 03 August Service Basic Api Implementation

#define UrlGetTotalWorkOrdersMechanical_SPV1 @"/api/MobileToServiceAuto/GetplumbingWorkOrderByEmployee_SPV1"
#define UrlGetTotalWorkOrdersMechanical_SPBasicInfo @"/api/MobileToServiceAuto/GetPlumbingWorkOrdersBasicInfoByEmployee?CompanyKey="


//==========================================================================================================

#define UrlUpdateMechanicalWorkOrder @"/api/MobileToServiceAuto/UpdatePlumbingWorkOrder"


//==========================================================================================================

#define UrlUpdateMechanicalActualHours @"/api/MobileToServiceAuto/ManageSubWOActualHours"

//=====================================================================================================
//=====================================================================================================

#define UrlMechanicalGetTax @"/api/MobileToServiceAuto/GetPlumbingWorkOrderTax?companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetAccountPSPPercentMechanical @"api/MobileToServiceAuto/GetAccountPSPPercent?companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlMechanicalServiceHistory @"/api/MobileToServiceAuto/GetServiceHistoryByAccountNo?Companykey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetPlanMechanical @"api/MobileToServiceAuto/GetAccounPSP?companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlGetDynamicFormMechanical @"api/MobileToServiceAuto/GetMultipleJsonFormsBySubWorkorderandCompanyId?CompanyKey="
#define UrlMechanicalDynamicFormAllSubWorkOrderId @"&SubWorkorderId="

//=====================================================================================================
//=====================================================================================================

#define UrlMechanicalDynamicFormSubmission @"api/Mobiletoserviceauto/UpdateSubWorkorderJsonFormsByCompanyId"

//=====================================================================================================
//=====================================================================================================

#define UrlMechanicalGetUnsignedDocumentList @"/api/MobileToServiceAuto/GetUnsignedAgreementWorkorderNos?CompanyKey="
#define UrlMechanicalGetUnsignedDocumentListWoNos @"&WorkorderNos="

#define UrlMechanicalGetUnsignedDocumentListNew @"/api/MobileToServiceAuto/GetUnsignedAgreementWorkorderNosNew"

//&WorkorderNos

//////////////////////////// PURCHASE ORDER ///////////////////////////////

#define UrlMechanicalToSubmitPurchaseOrder @"/api/MobileToServiceAuto/AddUpdatePurchaseOrder?companyKey="

///////////////////////////////////////////////////////////////////////////


#define UrlQuoteImageUpload @"/api/File/UploadQuoteSignaturesAsync"

//////////////////////////// CREATE QUOTE/////////////////////////////

#define UrlGetCustomerServiceAddress @"/api/MobileToServiceAuto/GetCustomerServiceAddress?companyKey=&accountNumber="

#define URLCREATEQUOTE @"/api/MobileAppToCore/ManageQuote?companyKey="

//ManageQuote?companyKey
///////////////////

//==========================================================================================================

#define UrlGenerateEquipBarcode @"/api/MobileToServiceAuto/ManageAccountEquipmentWithGenerateBarCode?companyKey="

//=====================================================================================================
//=====================================================================================================

//=====================================================================================================
//=====================================================================================================


#define UrlGetServiceAddressesByAccountNo @"/api/LeadNowAppToSalesProcess/GetServiceAddressesByAccountNo?accountNo="
#define UrlGetServiceAddressesByAccountNoNew @"/api/LeadNowAppToSalesProcess/GetServiceAddrContactCompByAccountNo?accountNo="

//=====================================================================================================
//=====================================================================================================

#define UrlGetPostAddressesByAccountNo @"/api/LeadNowAppToSalesProcess/SaveCustomerAddressesFromMobile?accountNo="

//=====================================================================================================
//=====================================================================================================

//=====================================================================================================
//=====================================================================================================

//service.serviceauto.com/api/MobileToServiceAuto/GetEquipmentHistory?companyKey=Automation&equipmentno=vB0Pod3r8W
#define UrlGetEquipmentHistory @"/api/MobileToServiceAuto/GetEquipmentHistory?companyKey="
#define UrlEquipmentno @"&equipmentno="

//=====================================================================================================
//=====================================================================================================

#define UrlGetPOHistory @"/api/MobileToServiceAuto/GetPurchaseOrderListForMobile?CompanyKey="
#define UrlPoEmployeeNo @"&employeeNo="

//=====================================================================================================
//=====================================================================================================
// Quote History

#define UrlGetQuoteHistory @"/api/MobileAppToCore/GetQuoteByEmployeeNoForMobile?CompanyKey="
#define UrlPoEmployeeNo @"&employeeNo="
#define UrlWONumber @"&workOrderNo="


//=====================================================================================================
//=====================================================================================================

#define UrlGetPO @"/api/MobileAppToCore/GetPurchaseOrderListByPurchaseOrderNumber?companyKey="
#define UrlGetPONo @"&poNumber="

//=====================================================================================================
//=====================================================================================================

#define UrlMechanicalUpdateEstTime @"/API/MobileToServiceAuto/UpdateEstimateTime"

//=====================================================================================================
//=====================================================================================================

#define UrlSubmitSubWorkOrder @"/api/MobileToServiceAuto/ManageSubWorkOrder"

//=====================================================================================================
//=====================================================================================================

///API/MobileToServiceAuto/CopaySubWorkOrderAsync?
//=====================================================================================================
//=====================================================================================================

#define UrlCopySubWorkOrder @"/api/MobileToServiceAuto/CopySubWorkOrderAsync?"

//=====================================================================================================
//=====================================================================================================
///Api/MobileToServiceAuto/DeleteWoOtherDoc?woOtherDocId=  UploadWorkOrderOtherDocForMobileAsync
//=====================================================================================================
//=====================================================================================================

#define UrlDeleteMechanicalWoDocument @"/api/MobileToServiceAuto/DeleteWoOtherDoc?woOtherDocId="

//=====================================================================================================
//=====================================================================================================

//=====================================================================================================
//=====================================================================================================

#define UrlUploadMechanicalWoDocument @"/api/File/UploadWorkOrderOtherDocForMobileAsyncNew"

//=====================================================================================================
//=====================================================================================================
//////////////////////////// CREATE WORK ORDER/////////////////////////////

#define URLSEARCHACCOUNT @"/api/MobileAppToCore/SearchAccountForMobileApp"
#define URLGetSERVICEADDRESS @"api/MobileToServiceAuto/GetCustomerServiceAddress"
#define URLGetHourConfigByAccountForMobileAsync @"/api/MobileToServiceAuto/GetHourConfigByAccountForMobileAsync"

#define URLGETALLRANGEOFTIME @"api/MobileAppToCore/GetAllRangeofTimeAsync"
#define URLGETACCOUNTPASTDUEBYACCOUNTNO @"api/MobileAppToCore/GetAccountPastDueByAccountNo"
#define URLGETALLRANGEOFTIME @"api/MobileAppToCore/GetAllRangeofTimeAsync"
#define URLGetRoutesByCustomerServiceAddressAndDateNew  @"api/MobileAppToCore/GetRoutesByCustomerServiceAddressAndDateNew"

#define URLCreatePlumbingWorkOrderForMobile @"api/MobileAppToCore/CreatePlumbingWorkOrderForMobile"
///////////////////////////////////////////////////////////////////////////

//URLCUSTOMERSIGNATUREUPLOAD
#define URLCUSTOMERSIGNATUREUPLOAD @"api/File/UploadSignatureAsync"

//=====================================================================================================
//=====================================================================================================

#define UrlResendMailMechanical @"/api/MobileToServiceAuto/PlumbingMobileResendInvoiceMail"

//=====================================================================================================
//=====================================================================================================

#define UrlGetPlumbingMasterForAccountAddress @"/api/MobileToServiceAuto/GetPlumbingMasterForAccountAddress?companyKey="

//=====================================================================================================
//=====================================================================================================

#define UrlSyncSubWoEmployeeWorkingTimeToServer @"api/MobileToServiceAuto/ManageSubWOActualHoursAfterCompleted?subWorkOrderId="

//=====================================================================================================
//=====================================================================================================


//============================================================================
//============================================================================
#pragma mark- ---------------------Alert Text Messages------------------------
//============================================================================
//============================================================================

#define Alert @"Alert!"
#define AlertDelete @"Delete?"
#define AlertDeleteMsg @"Doing this will delete image permanently."
#define AlertDeleteDataMsg @ "Are you sure you want to delete?"
#define AlertChangeReolveStatusMsg @"Are you sure to change resolve status.?"
#define AlertChangeStatusMsg @"Are you sure to change status.?"
#define ChooseOptionMsg @"Please Select an Option"
#define Info @"Information"
#define InfoWdoServiceProposalDataSynced @"WDO SERVICE PROPSAL Synced Successfully"
#define InfoWdoServiceReportDataSynced @"WDO SERVICE REPORT Synced Successfully"
#define InfoNPMAServiceProposalDataSynced @"NPMA SERVICE PROPSAL Synced Successfully"
#define InfoNPMAServiceReportDataSynced @"NPMA SERVICE REPORT Synced Successfully"
#define InfoDataSynced @"Data synced successfully"
#define InfoDataSaved @"Data saved successfully"
#define ValidPass @"Please enter valid password"
#define PassWordValid @"1. Passwords must be at least 6 characters and at most 30 characters.\n2. Passwords must have at least one non letter or digit character(!@#$&*).\n3. Passwords must have at least one digit/number ('0'-'9').\n4. Passwords must have at least one uppercase ('A'-'Z').\n5. Passwords must have at least one lowercase ('a'-'z')."
#define InfoComingSoon @"Coming soon..."
#define ErrorServerResponse @"There was a problem connecting to peSTream, Please check your connection and try again"
#define ErrorInternetMsg @"Please check your internet connection and relaunch the app."
#define ErrorInternetMsgPayment @"To Continue For Payment via Credit Card, Please check your internet connection and relaunch the app."
#define Sorry @"Sorry,something went wrong. Please try again later."
#define SorryEmail @"Unable to Send Mail. Please try again later."
#define PaymentSuccess @"Thank you.\n Your transaction is successfull."
#define ErrorCreditCardStatusFailed @"Credit Cards Expired"
#define ErrorPaymentFailed @"Something went wrong. Unable to complete payment process"
#define InvalidLogin @"Invalid login attempt."
#define failMailSend @"Unable to send mail.Please try again later."
#define SuccessMailSend @"Email sent Successfully."
#define failSMSSend @"Unable to send SMS. Please try again later."
#define SuccessSMSSend @"SMS sent Successfully."
#define AlertSelectCustomMsg @"Before sending SMS.Please select Custom Message from Setting Menu."
#define NoBeforeImg @"No images available to show."
#define NoPreview @"No Preview available to show."
#define imageThree @"You have already attached max limit of Photo."
#define NoStatus @"No Status Available.\n Please check either Status are set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoCustomMsg @"No Custom Message Available.\n Please check either Custom Message are set on Web or your internet connection and relaunch the App"
#define NoService @"No Services Available.\n Please check either Services are set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoUrgency @"No Urgency Available.\n Please check either Urgency are set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoDataAvailable @"No Data Available.\n Please check either Data is set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoDataAvailableEquipName @"No Data Available for the Category and Equipment Selected.\n Please try other combination"
#define alertCatnEquipName @"Please select Category and Equipment both."
#define NoDataAvailableReset @"No Data Available.\n Please check either Reset Reasons are set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoDataAvailableServiceJobDescriptions @"No Data Available.\n Please check either Service Job Descriptions are set on Web, if set on Web check your internet connection and Sync Masters from Menu options."
#define NoPOCAvailableee @"No POC Available"
#define NoDataAvailableee @"No Data Available"
#define NoDataAvailableContactOnMap @"No Data Available. Please try increasing Radius."
#define NoNotesDataAvailableee @"Notes history not available"
#define unableCopySWO @"Unable to copy sub work order. Please try again"
#define alertStartJob @"Please start job to continue"
#define alertClientApproval @"Please take client approval first"
#define ReasonClientApproval @"Completed Approved"
#define ReasonOnRoute @"Completed OnRoute"
#define ReasonInspection @"Completed Inspection"
#define ReasonRunning @"Completed Running"
#define NoDataAvailableCustomerAddress @"No Address Available."
#define TaxCode @"Please select tax code"
#define alertActivityLogType @"Please select activity type"
#define alertActivityAgenda @"Please enter agenda"
#define alertOrderByName @"Please enter ordered by name"
#define Alert_FirstName @"Please enter first name"
#define Alert_CompanyName @"Please enter company name"
#define Alert_ValidNo @"Please enter valid no's"

#define AlertAudioVideoPermission @"If Audio Permission not allowed, Please go to settings and allow, If already allowed click on cancel"
//@"Audio Permission not allowed.Please go to settings and allow"

//============================================================================
#pragma mark- ---------------------CRM New portion-----------------
//============================================================================

#define UrlGetAllWebLead @"/api/LeadNowAppToSalesProcess/GetWebLeadsDetailsByEmployeeNoV2?CompanyKey="

#define UrlGetAllOpportunityLead @"/api/LeadNowAppToSalesProcess/GetAllLeadsForEmployeeAsyncV2?lastModifiedDate="

#define UrlGetAllNotes @"/api/LeadNowAppToSalesProcess/GetLeadNotesListV2?refid="

#define UrlGetAllDocuments @"/api/LeadNowAppToSalesProcess/GetDocumentsByRefIdAndRefType?refId="

#define UrlAddDocumentsByRefIdAndRefType @"/api/LeadNowAppToSalesProcess/AddDocumentsByRefIdAndRefType"

#define UrlGetTimeline @"/api/LeadNowAppToSalesProcess/GetTimelineEntities"

#define UrlGetCompanyBasicInfo @"/api/LeadNowAppToSalesProcess/GetCrmCompanyBasicInfoById?CrmCompanyId="

#define UrlGetCrmContactAssociation @"/api/LeadNowAppToSalesProcess/GetCrmContactAssociation?CrmContactId="

#define UrlDissociateContact @"/api/LeadNowAppToSalesProcess/DissociateContactWithRefTypeRefId?CrmContactId="

#define UrlDissociateCompany @"/api/LeadNowAppToSalesProcess/DissociateCrmCompanyWithRefTypeRefId?CrmCompanyId="

#define UrlAddCrmContacts @"/api/LeadNowAppToSalesProcess/CreateContactAssociateCrmCompany"

#define UrlSearchCrmContacts @"/api/LeadNowAppToSalesProcess/SearchCrmContact"

#define UrlSearchCrmCompanies @"/api/LeadNowAppToSalesProcess/SearchCrmCompany"
#define UrlSearchCrmCompaniesV2 @"/api/LeadNowAppToSalesProcess/SearchCrmCompanyV2"

#define UrlAddCrmCompanies @"/api/LeadNowAppToSalesProcess/CreateCrmCompanyAssociateContact"
#define UrlAddCrmCompaniesV2 @"/api/LeadNowAppToSalesProcess/CreateCrmCompanyAssociateContactV2"

#define UrlAddNotesNew @"/api/LeadNowAppToSalesProcess/AddUpdateNoteRefTypeRefId"

#define UrlAssociateContact @"/api/LeadNowAppToSalesProcess/AssociateContactWithRefTypeRefId?CrmContactId="

#define UrlAssociateCompany @"/api/LeadNowAppToSalesProcess/AssociateCrmCompanyWithRefTypeRefId?CrmCompanyId="

#define UrlGetContactBasicInfo @"/api/LeadNowAppToSalesProcess/GetCrmContactBasicInfoById?CrmContactId="

#define UrlSearchCrmAccount @"/api/LeadNowAppToSalesProcess/SearchAccount"

#define UrlGetAssociatedEntitiesForTaskActivityAssociations @"api/LeadNowAppToSalesProcess/GetAssociatedEntitiesForTaskActivityAssociations?"

#define UrlGetActivityComments @"api/LeadNowAppToSalesProcess/GetActivityComments?ActivityId="

#define UrlGetTaskById @"/api/LeadNowAppToSalesProcess/GetTaskById?LeadTaskId="

#define UrlGetServiceAddressesByAccountId @"/api/LeadNowAppToSalesProcess/GetServiceAddressesByAccountId?AccountId="

#define UrlUploadContactProfileImage @"/api/File/UploadCrmContactImagesAsync"

#define UrlUploadCompanyProfileImage @"/api/File/UploadCrmCompanyImagesAsync"

#define UrlScheduleNowByLeadId @"/api/LeadNowAppToSalesProcess/ScheduleLeadById"

#define UrlAddLeadOpportunity @"/api/LeadNowAppToSalesProcess/AddLeadOpportunity"

#define UrlConvertWebLeadToOpportunityV2 @"/api/LeadNowAppToSalesProcess/ConvertWebLeadToOpportunityV2"

#define UrlServiceHistory_LoadData @"/api/MobileToServiceAuto/GetWorkOrderLoadDataExtSer?Type="
#define UrlServiceHistory_LoadDataV2 @"/api/MobileToServiceAuto/GetWorkOrderLoadDataExtSer?Type="
#define UrlNpmaTermiteServiceHistory_LoadDataV2 @"/api/MobileToServiceAuto/GetNPMAWorkOrderLoadDataExtSer?Type="

#define UrlGetAddressesByCrmContactId @"/api/LeadNowAppToSalesProcess/GetCrmContactAddress?CrmContactId="

#define UrlGetAddressesByCrmCompanyId @"/api/LeadNowAppToSalesProcess/GetCrmCompanyAddress?CrmCompanyId="

#define UrlGetDashBoardDetailsNew @"/api/Dashboard/GetEmployeePerformanceDetails?EmployeeId="

#define UrlGetDashBoardDetailsChartData @"/api/Dashboard/GetSoldProposedChartDetails?EmployeeId="

#define UrlUploadXMLSales @"/api/File/UploadGraphXMLFileAsync"

#define AlertAppointmentCompleted @"Appointment completed successfully"
#define AlertAppointmentSynced @"Appointment synced successfully"


#pragma mark- --------- New Sales Flow --------------

 #define UrlGetChemicalSensitiveContactListByAddress @"/api/crmcontact/GetChemicalSensitiveContactListByAddress?"



#endif /* Header_h */

/*
 
 // UserDefaults Refresh Contacts on contact list :- "RefreshContacts_ContactList"
 // UserDefaults Refresh Contacts on contact Details View :- RefreshContacts_ContactDetails
 // UserDefaults Refresh Contacts on contact Associate View :- RefreshContacts_AssociateContactList
 
 // UserDefaults Refresh Company on Company list :- "RefreshCompany_CompanyList"
 // UserDefaults Refresh Company on Company Details View :- RefreshCompany_CompanyDetails
 // UserDefaults Refresh Company on Company Associate View :- RefreshCompany_AssociateCompanyList
 
 */
