//
//  EquipmentsListTableViewCell.h
//  DPS
//
//  Created by Saavan Patidar on 20/03/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquipmentsListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *lblCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblEquipName;
@property (strong, nonatomic) IBOutlet UILabel *lblArea;
@property (strong, nonatomic) IBOutlet UILabel *lblAssociatedwith;
@property (strong, nonatomic) IBOutlet UILabel *lblWarrantyDate;
@property (strong, nonatomic) IBOutlet UILabel *lblInstalledOnDate;
@property (strong, nonatomic) IBOutlet UILabel *lblLastServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lblNextServiceDate;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveServiceDate;
@property (strong, nonatomic) IBOutlet UIButton *BtnAdd;
@property (strong, nonatomic) IBOutlet UIButton *BtnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnServiceDateToSave;
@property (strong, nonatomic) IBOutlet UILabel *lblSerialNo;
@property (strong, nonatomic) IBOutlet UILabel *lblModelNo;
@property (strong, nonatomic) IBOutlet UILabel *lblManufacturer;
@property (strong, nonatomic) IBOutlet UIButton *BtnBarcode;
@property (strong, nonatomic) IBOutlet UIButton *BtnBarcodePrint;
@property (strong, nonatomic) IBOutlet UIButton *btnViewMoreEquip;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBarcode;

@end
