//
//  LeadNowMainVC.swift
//  DPS
//
//  Created by INFOCRATS on 08/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class LeadNowMainVC: UIViewController {
    
    @IBOutlet weak var NewLeadView: UIView!
    @IBOutlet weak var AssignedLeadView: UIView!
    @IBOutlet weak var SubmittedLeadView: UIView!
    
    
    @IBOutlet weak var lblNewLeadCount: UILabel!
    @IBOutlet weak var lblAssignedLeadCount: UILabel!
    @IBOutlet weak var lblSubmittedLeadCount: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    
    
    var dictLoginData = NSDictionary()
    var strCompanyKey = String()
    var strUserName = String()
    var strEmpID = String()
    var strEmpName = String()
    var strEmpNumber = String()
    
    var arrOfLeadNow = LeadNowModel()
    var arrOfPropertyType = [[String: Any]]()
    var arrOfUrgentType = [[String: Any]]()
    var arrofNewLeads = [LeadsModel]()
    
    var isSearching = Bool()
    var searchedText = ""
    var isFrom = ""
    private lazy var NewLeadNowVC: NewLeadNowVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "RuchikaLeadNow", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "NewLeadNowVC") as! NewLeadNowVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var AssignedLeadNowVC: AssignedLeadNowVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "RuchikaLeadNow", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "AssignedLeadNowVC") as! AssignedLeadNowVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        isFrom = "New"
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("MoveToLeadMainVC"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        isFrom = "New"

        NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationThroughSearch(notification:)), name: Notification.Name("MoveToLeadMainVCThroughSearch"), object: nil)
        
        
        NewLeadView.backgroundColor = UIColor.appYellow
        AssignedLeadView.backgroundColor = UIColor.clear
        SubmittedLeadView.backgroundColor = UIColor.clear
        
        updateView(indexButton: 0)
    }
    
    
    
    //MARK: - IBActions
    @IBAction func btnNewLeadAction(_ sender: Any) {
        updateView(indexButton: 0)
        isFrom = "New"
        if isSearching == true{
            
            NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType, "searchText": searchedText])

        }
        else{
           NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
        }
        NewLeadView.backgroundColor = UIColor.appYellow
        AssignedLeadView.backgroundColor = UIColor.clear
        SubmittedLeadView.backgroundColor = UIColor.clear
        
    }
    
    @IBAction func btnAssignedLeadAction(_ sender: Any) {
        updateView(indexButton: 1)
        isFrom = "Assigned"
        if isSearching == true{
            
            NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

            
            NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

        }
        else{
        NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
        }
        NewLeadView.backgroundColor = UIColor.clear
        AssignedLeadView.backgroundColor = UIColor.appYellow
        SubmittedLeadView.backgroundColor = UIColor.clear
    }
    
    @IBAction func btnSubmittedLeadAction(_ sender: Any) {
        updateView(indexButton: 2)
        isFrom = "Submitted"
        if isSearching == true{
            
            NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

            
            NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

        }
        else{
        NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVC"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType ])
        }
        NewLeadView.backgroundColor = UIColor.clear
        AssignedLeadView.backgroundColor = UIColor.clear
        SubmittedLeadView.backgroundColor = UIColor.appYellow
    }
    
    
    //MARK: - Custom Functions
    func getSearchedData(strSearchedtext : String){
        
        print(strSearchedtext)
        arrofNewLeads.removeAll()
        var intNewLeadsCount = Int()
        var intAssignedLeadsCount = Int()
        var intSubmittedLeadsCount  = Int()
        
        NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

        NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

        
        let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
        if arrOfPropertyType.count > 0 || arrOfUrgentType.count > 0{
            
            for i in 0..<arrOfPropertyType.count{
                
                let obj = arrOfPropertyType[i]
                
                let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                
                let tempNew = arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}

                intNewLeadsCount = intNewLeadsCount + tempNew.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
                let tempAssigned = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}
                
                intAssignedLeadsCount = intAssignedLeadsCount + tempAssigned.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
                let tempSubmitted =  arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId }
                
                intSubmittedLeadsCount = intSubmittedLeadsCount + tempSubmitted.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  || $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
            }
            
            
            for i in 0..<arrOfUrgentType.count{
                let obj = arrOfUrgentType[i]
                let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                
                
                let tempNew = arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId}
                
                intNewLeadsCount = intNewLeadsCount + tempNew.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  && $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  && $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  && $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
                let tempAssigned = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}
                
                intAssignedLeadsCount = intAssignedLeadsCount +  tempAssigned.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  && $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  && $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  && $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
                let tempSubmitted = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId }
                
                intSubmittedLeadsCount = intSubmittedLeadsCount +  tempSubmitted.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  && $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  && $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  && $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                
            }
            
            lblNewLeadCount.text = String(intNewLeadsCount)
            lblAssignedLeadCount.text = String(intAssignedLeadsCount)
            lblSubmittedLeadCount.text = String(intSubmittedLeadsCount)
            
        }
        else{
            let tempNew =  arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased()}
            lblNewLeadCount.text = String(tempNew.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  || $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count)
            
            let tempAssigned = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID}
            
            lblAssignedLeadCount.text = String(tempAssigned.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  || $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count)
            
            let tempSubmitted = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}
            lblSubmittedLeadCount.text =  String(tempSubmitted.filter{ $0.strLeadName == strSearchedtext || $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased())  || $0.strLastName.lowercased().contains(strSearchedtext.lowercased())  || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count)
        }
        
    }
    func getLeadsCount(){
        
        arrofNewLeads.removeAll()
        var intNewLeadsCount = Int()
        var intAssignedLeadsCount = Int()
        var intSubmittedLeadsCount  = Int()
        
        let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
        if arrOfPropertyType.count > 0 || arrOfUrgentType.count > 0{
            
            for i in 0..<arrOfPropertyType.count{
                
                let obj = arrOfPropertyType[i]
                
                let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                
                intNewLeadsCount = intNewLeadsCount + arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}.count
                
                intAssignedLeadsCount = intAssignedLeadsCount + arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}.count
                
                intSubmittedLeadsCount = intSubmittedLeadsCount + arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId }.count
                
            }
            
            
            for i in 0..<arrOfUrgentType.count{
                let obj = arrOfUrgentType[i]
                let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                
                
                intNewLeadsCount = intNewLeadsCount + arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId}.count
                
                intAssignedLeadsCount = intAssignedLeadsCount +  arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}.count
                
                intSubmittedLeadsCount = intSubmittedLeadsCount + arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}.count
                
            }
            
            lblNewLeadCount.text = String(intNewLeadsCount)
            lblAssignedLeadCount.text = String(intAssignedLeadsCount)
            lblSubmittedLeadCount.text = String(intSubmittedLeadsCount)
            
        }
        else{
            
            if arrOfLeadNow.strTotalNewcount == ""{
                lblNewLeadCount.text = "0"
            }
            else{
                lblNewLeadCount.text = arrOfLeadNow.strTotalNewcount
            }
            
            if arrOfLeadNow.strTotalAssignedToCount == ""{
                lblAssignedLeadCount.text = "0"
            }
            else{
                lblAssignedLeadCount.text = arrOfLeadNow.strTotalAssignedToCount
            }
            
            lblSubmittedLeadCount.text =  String(arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}.count)  //arrOfLeadNow.strTotalSubmittedByCount
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        if notification.userInfo != nil{
            let dict = notification.userInfo
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            arrOfPropertyType = dict?["PropertyType"] as! [[String: Any]]
            arrOfUrgentType = dict?["UrgencyType"] as! [[String: Any]]
            
            getLeadsCount()
        }
    }
    
    
    @objc func methodOfReceivedNotificationThroughSearch(notification: Notification) {
        if notification.userInfo != nil{
            let dict = notification.userInfo
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            
            let arrOfPropertyTypeTemp = dict?["PropertyType"] as? [[String: Any]]
            let arrOfUrgentTypeTemp = dict?["UrgencyType"] as? [[String: Any]]
            
            arrOfPropertyType =  arrOfPropertyTypeTemp ?? []
            arrOfUrgentType = arrOfUrgentTypeTemp ?? []
            
            let strSearchedtext = dict?["searchText"] as? String ?? ""
            
            //getLeadsCount()
            if strSearchedtext != ""{
                getSearchedData(strSearchedtext: strSearchedtext)
                isSearching = true
                searchedText = strSearchedtext
            }
            else{
                getLeadsCount()
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": ""])
                
                NotificationCenter.default.post(name: Notification.Name("MoveToLeadAssignedVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : isFrom, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": ""])

                
                isSearching = false
                searchedText = ""
            }
        }
    }
    //MARK: - Add child and remove VC
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    private func updateView(indexButton: Int) {
        if indexButton == 0 {
            remove(asChildViewController: AssignedLeadNowVC)
            add(asChildViewController: NewLeadNowVC)
            
        }
        else if indexButton == 1 {
            remove(asChildViewController: NewLeadNowVC)
            remove(asChildViewController: AssignedLeadNowVC)
            add(asChildViewController: AssignedLeadNowVC)
            
        }
        else if indexButton == 2 {
            remove(asChildViewController: NewLeadNowVC)
            remove(asChildViewController: AssignedLeadNowVC)
            add(asChildViewController: AssignedLeadNowVC)
            
        }
    }
    
}

