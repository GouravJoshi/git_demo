//
//  LeadNowModel.swift
//  DPS
//
//  Created by INFOCRATS on 15/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class LeadNowModel: NSObject {
    
    var arrOfLeads = [LeadsModel]()
    var strTotalAssignedToCount = ""
    var strTotalSubmittedByCount = ""
    var strTotalNewcount = ""
    var arrOfAssignedToCount = [LeadAssignedToCountModel]()
    var arrOfSubmittedByCount = [LeadSubmittedByCountModel]()
    
}

class LeadsModel: NSObject{
    
    var strLeadId = ""
    var strLeadNumber = ""
    var strLeadName = ""
    var strAssignedToId = ""
    var strAssignedToName = ""
    var strAssignedType = ""
    var strFirstName  = ""
    var strMiddleName = ""
    var strLastName  = ""
    var strStatusId  = ""
    var strStatusName = ""
    var strStatusSysName = ""
    var strCompanyName = ""
    var strCreatedDate = ""
    var strSubmittedByName = ""
    var strSubmittedById = ""
    var strAddressPropertyName = ""
    var strAddressPropertyTypeId = ""
    var strUrgencyId = ""
    var strUrgencyName = ""
    
}

class LeadAssignedToCountModel : NSObject
{
   var strStatusID = ""
   var strStatusName = ""
   var strCount = ""
}


class LeadSubmittedByCountModel : NSObject
{
   var strStatusID = ""
   var strStatusName = ""
   var strCount = ""
}
