//
//  LeadNowParser.swift
//  DPS
//
//  Created by INFOCRATS on 15/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class LeadNowParser: NSObject {

    class func parseLeadNowAPIJSON(response : NSDictionary, completionHandler: @escaping (LeadNowModel) -> Void) {
        
        let leadNowModel = LeadNowModel()
        var objOfLeads = [LeadsModel]()
        var objOfAssignedToCount = [LeadAssignedToCountModel]()
        var objOfSubmittedToCount = [LeadSubmittedByCountModel]()
        
        let tempResponse =  response["data"] as! [String:Any]
        print(tempResponse)
        let objLeadList = tempResponse["Leads"] as! [[String:Any]]
        
        for i in 0..<objLeadList.count{
            let lead = LeadsModel()
            
            lead.strLeadId = String(describing: objLeadList[i]["LeadId"] as? NSNumber ?? 0)
            lead.strLeadNumber = objLeadList[i]["LeadNumber"] as? String ?? ""
            lead.strLeadName = objLeadList[i]["LeadName"] as? String ?? ""
            lead.strAssignedToId = String(describing: objLeadList[i]["AssignedToId"] as? NSNumber ?? 0)
            lead.strAssignedToName = objLeadList[i]["AssignedToName"] as? String ?? ""
            lead.strAssignedType = objLeadList[i]["AssignedType"] as? String ?? ""
            lead.strFirstName = objLeadList[i]["FirstName"] as? String ?? ""
            lead.strMiddleName = objLeadList[i]["MiddleName"] as? String ?? ""
            lead.strLastName = objLeadList[i]["LastName"] as? String ?? ""
            lead.strStatusId = String(describing: objLeadList[i]["StatusId"] as? NSNumber ?? 0)
            lead.strStatusName = objLeadList[i]["StatusName"] as? String ?? ""
            lead.strStatusSysName = objLeadList[i]["StatusSysName"] as? String ?? ""
            lead.strCompanyName = objLeadList[i]["CompanyName"] as? String ?? ""
            lead.strCreatedDate = objLeadList[i]["CreatedDate"] as? String ?? ""
            lead.strSubmittedByName = objLeadList[i]["SubmittedByName"] as? String ?? ""
            lead.strSubmittedById = String(describing: objLeadList[i]["SubmittedById"] as? NSNumber ?? 0)
            lead.strAddressPropertyName = objLeadList[i]["AddressPropertyName"] as? String ?? ""
            lead.strAddressPropertyTypeId = String(describing: objLeadList[i]["AddressPropertyTypeId"] as? NSNumber ?? 0)
            lead.strUrgencyId = String(describing: objLeadList[i]["UrgencyId"] as? NSNumber ?? 0)
            lead.strUrgencyName = objLeadList[i]["UrgencyName"] as? String ?? ""

            objOfLeads.append(lead)
        }
        
        
        let objAssignedToCountList = tempResponse["AssignedToCount"] as! [[String:Any]]
        for j in 0..<objAssignedToCountList.count{
            let objAssigned = LeadAssignedToCountModel()
            
            objAssigned.strStatusID =  String(describing: objAssignedToCountList[j]["StatusID"] as? NSNumber ?? 0)
            objAssigned.strStatusName = objAssignedToCountList[j]["statusName"] as? String ?? ""
            objAssigned.strCount =  String(describing: objAssignedToCountList[j]["count"] as? NSNumber ?? 0)
            
            objOfAssignedToCount.append(objAssigned)
        }
        
        let objSubmittedToCountList = tempResponse["SubmittedByCount"] as! [[String:Any]]
        for k in 0..<objSubmittedToCountList.count{
            let objSubmitted = LeadSubmittedByCountModel()
            
            objSubmitted.strStatusID =  String(describing: objSubmittedToCountList[k]["StatusID"] as? NSNumber ?? 0)
            objSubmitted.strStatusName = objSubmittedToCountList[k]["statusName"] as? String ?? ""
            objSubmitted.strCount =  String(describing: objSubmittedToCountList[k]["count"] as? NSNumber ?? 0)
            
            objOfSubmittedToCount.append(objSubmitted)
        }
        
        
        leadNowModel.arrOfLeads = objOfLeads
        leadNowModel.strTotalAssignedToCount = String(describing:  tempResponse["TotalAssignedToCount"] as? NSNumber ?? 0)
        leadNowModel.strTotalNewcount = String(describing: tempResponse["TotalNewcount"] as? NSNumber ?? 0)
        leadNowModel.strTotalSubmittedByCount = String(describing: tempResponse["TotalSubmittedByCount"] as? NSNumber ?? 0)
        leadNowModel.arrOfAssignedToCount = objOfAssignedToCount
        leadNowModel.arrOfSubmittedByCount = objOfSubmittedToCount
        
        completionHandler(leadNowModel)
    }
}
