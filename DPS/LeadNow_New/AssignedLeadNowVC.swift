//
//  AssignedLeadNowVC.swift
//  DPS
//
//  Created by INFOCRATS on 08/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import Charts

class AssignedLeadNowVC: UIViewController {
    
    
    //MARK: - IBOUtlets
    @IBOutlet weak var btnSummary: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var heightConstraintForCollectionView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForBarChartView: NSLayoutConstraint!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!

    
    //MARK: - Variables
    
    var arrOfLeadNow = LeadNowModel()
    var isFromWhichTab = ""
    var arrOfChartTitle = [String]()
    var arrOfChartValues = [Double]()
    var arrOfPropertyType = [[String: Any]]()
    var arrOfUrgentType = [[String: Any]]()
    
    var dictLoginData = NSDictionary()
    var strEmpID = String()

    var isSearching = Bool()
    var searchedText = ""
    var isSummarySelected = false
    private lazy var NewLeadNowVC: NewLeadNowVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "RuchikaLeadNow", bundle: Bundle.main)
        
        var viewController = storyboard.instantiateViewController(withIdentifier: "NewLeadNowVC") as! NewLeadNowVC
        
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    //MARK: - View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnSummary.layer.cornerRadius = btnSummary.frame.size.height/2
        btnDetails.layer.cornerRadius = btnDetails.frame.size.height/2
        
        isSummarySelected = true
        collectionView.delegate = self
        collectionView.dataSource = self
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        self.tblView.tableFooterView = UIView()

        if DeviceType.IS_IPAD{
            heightConstraintForBarChartView.constant = 500
        }
        else{
            heightConstraintForBarChartView.constant = 310
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("MoveToLeadAssignedVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationThroughSearch(notification:)), name: Notification.Name("MoveToLeadAssignedVCThroughSearch"), object: nil)

        
    }
    
    //MARK: - IBActions
    @IBAction func btnSummaryAction(_ sender: Any) {
        
        btnDetails.backgroundColor = UIColor.white
        btnDetails.titleLabel?.textColor = UIColor.darkGray
        
        btnSummary.backgroundColor = UIColor.appYellow
        btnSummary.titleLabel?.textColor = UIColor.white
        
        isSummarySelected = true

        if arrOfLeadNow.arrOfLeads.count == 0 {
            
            tblView.isHidden = true
            collectionView.isHidden = true
            barChartView.isHidden = true
            
            lblNoData.isHidden = false
            lblNoData.text = "No Data Availalble"
            
        }
        else{
            
            tblView.isHidden = false
            collectionView.isHidden = false
            barChartView.isHidden = false
            
            lblNoData.isHidden = true
            lblNoData.text = ""
        }
        remove(asChildViewController: NewLeadNowVC)
    }
    
    @IBAction func btnDetailAction(_ sender: Any) {
        
        isSummarySelected = false

        btnSummary.backgroundColor = UIColor.white
        btnSummary.titleLabel?.textColor = UIColor.darkGray
        
        btnDetails.backgroundColor = UIColor.appYellow
        btnDetails.titleLabel?.textColor = UIColor.white
        
        if arrOfLeadNow.arrOfLeads.count == 0 {
            
            tblView.isHidden = true
            collectionView.isHidden = true
            barChartView.isHidden = true
            
            lblNoData.isHidden = false
            lblNoData.text = "No Data Availalble"
            
        }
        else{
            
            add(asChildViewController: NewLeadNowVC)
            
            if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType, "searchText": searchedText])

                }
                else{
                NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
            else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

                }
                else{
                NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Submitted", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
            

            self.barChartView.isHidden = true
            self.collectionView.isHidden = true
            self.tblView.isHidden = true

            
            lblNoData.isHidden = true
            lblNoData.text = ""

        }
            
       
    }
    
    //MARK: - Custom Functions
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        arrOfChartValues.removeAll()
        arrOfChartTitle.removeAll()
        arrOfPropertyType.removeAll()
        arrOfUrgentType.removeAll()
        
        btnDetails.backgroundColor = UIColor.white
        btnDetails.titleLabel?.textColor = UIColor.darkGray
        
        btnSummary.backgroundColor = UIColor.appYellow
        btnSummary.titleLabel?.textColor = UIColor.white
        
        if notification.userInfo != nil{
            let dict = notification.userInfo
            
            arrOfPropertyType = dict?["PropertyType"] as! [[String: Any]]
            arrOfUrgentType = dict?["UrgencyType"] as! [[String: Any]]
            
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            if arrOfLeadNow.arrOfLeads.count == 0 {
                
                tblView.isHidden = true
                collectionView.isHidden = true
                barChartView.isHidden = true
                
                lblNoData.isHidden = false
                lblNoData.text = "No Data Availalble"
                
            }
            else{
                let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
                
                tblView.isHidden = false
                collectionView.isHidden = false
                barChartView.isHidden = false
                
                lblNoData.isHidden = true
                lblNoData.text = ""
                
                
                if arrOfPropertyType.count > 0 || arrOfUrgentType.count > 0{
                    
                    
                    if (dict?["FromTab"] as? String ?? "").lowercased() == "Assigned".lowercased(){
                        isFromWhichTab = "Assigned"
                        
                        for i in 0..<arrOfLeadNow.arrOfAssignedToCount.count{
                            
                        //    if arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased() != "New".lowercased(){
                                
                                let strStatusName = arrOfLeadNow.arrOfAssignedToCount[i].strStatusName
                                
                                var chartIndexCount = 0
                                for j in 0..<arrOfPropertyType.count{
                                    
                                    let obj = arrOfPropertyType[j]
                                    
                                    let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                                    
                                    chartIndexCount = chartIndexCount + arrOfLeadsTemp.filter{
                                        $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId }.count
                                    
                                }
                                
                                
                                for i in 0..<arrOfUrgentType.count{
                                    let obj = arrOfUrgentType[i]
                                    let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                                    
                                    chartIndexCount = chartIndexCount + arrOfLeadsTemp.filter{
                                        $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId}.count
                                    
                                }
                                
                                arrOfChartTitle.append(strStatusName)
                                arrOfChartValues.append(Double(chartIndexCount))
                           // }
                            
                        }
                        
                        setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                        
                    }
                    
                    else if (dict?["FromTab"] as? String ?? "").lowercased() == "Submitted".lowercased(){
                        isFromWhichTab = "Submitted"
                        
                        for i in 0..<arrOfLeadNow.arrOfSubmittedByCount.count{
                            
                         //   if arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName.lowercased() != "New".lowercased(){
                                
                                let strStatusName = arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName
                                
                                var chartIndexCount = 0
                                for j in 0..<arrOfPropertyType.count{
                                    
                                    let obj = arrOfPropertyType[j]
                                    
                                    let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                                    
                                    chartIndexCount = chartIndexCount +  arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strAddressPropertyTypeId == tempAddressPropertyId}.count
                                    
                                }
                                
                                
                                for i in 0..<arrOfUrgentType.count{
                                    let obj = arrOfUrgentType[i]
                                    let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                                    
                                    chartIndexCount = chartIndexCount + arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strUrgencyId == tempUrgencyId}.count
                                    
                                }
                                
                                arrOfChartTitle.append(strStatusName)
                                arrOfChartValues.append(Double(chartIndexCount))
                           // }
                            
                         }
                        
                        if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                          setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                        }
                        
                    }
                }
                else{
                    if (dict?["FromTab"] as? String ?? "").lowercased() == "Assigned".lowercased(){
                        isFromWhichTab = "Assigned"
                        
                        for i in 0..<arrOfLeadNow.arrOfAssignedToCount.count{
                           // if arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased() != "New".lowercased(){
                                arrOfChartValues.append(Double(arrOfLeadNow.arrOfAssignedToCount[i].strCount ) ?? 0.0)
                                arrOfChartTitle.append(arrOfLeadNow.arrOfAssignedToCount[i].strStatusName)
                                
                           // }
                        }
                        if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                          setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                        }
                    }
                    else if (dict?["FromTab"] as? String ?? "").lowercased() == "Submitted".lowercased(){
                        isFromWhichTab = "Submitted"
                        
                        for i in 0..<arrOfLeadNow.arrOfSubmittedByCount.count{
                         //   if arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName.lowercased() != "New".lowercased(){
                                
                                arrOfChartValues.append(Double(arrOfLeadNow.arrOfSubmittedByCount[i].strCount ) ?? 0.0)
                                arrOfChartTitle.append(arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName)
                                
                        //    }
                        }
                        if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                          setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                        }
                    }
                }
                
                
                if isSummarySelected == true{
                btnDetails.backgroundColor = UIColor.clear
                btnDetails.tintColor = UIColor.lightText
                btnSummary.backgroundColor = UIColor.appYellow
                btnSummary.tintColor = UIColor.white
                self.barChartView.isHidden = false
                self.collectionView.isHidden = false
                self.tblView.isHidden = false
                remove(asChildViewController: NewLeadNowVC)

                if arrOfChartTitle.count > 0{
                    collectionView.reloadData()
                }
                }
                else{
                btnDetails.backgroundColor = UIColor.clear
                btnDetails.tintColor = UIColor.lightText
                btnSummary.backgroundColor = UIColor.appYellow
                btnSummary.tintColor = UIColor.white
                isSummarySelected = true
                self.barChartView.isHidden = false
                self.collectionView.isHidden = false
                self.tblView.isHidden = false
                addChild(NewLeadNowVC)
     
                
                if arrOfChartTitle.count > 0{
                   collectionView.reloadData()
                }
                }
            }
        }
    }
    
    @objc func methodOfReceivedNotificationThroughSearch(notification: Notification) {
        arrOfChartValues.removeAll()
        arrOfChartTitle.removeAll()
        arrOfPropertyType.removeAll()
        arrOfUrgentType.removeAll()
        
        
        if notification.userInfo != nil{
            let dict = notification.userInfo
            
            arrOfPropertyType = dict?["PropertyType"] as? [[String: Any]] ?? []
            arrOfUrgentType = dict?["UrgencyType"] as? [[String: Any]] ?? []
            
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            let strSearchedtext = dict?["searchText"] as? String ?? ""
            let fromtab =  dict?["FromTab"] as? String ?? ""
            
            if arrOfLeadNow.arrOfLeads.count == 0 {
                
                tblView.isHidden = true
                collectionView.isHidden = true
                barChartView.isHidden = true
                
                lblNoData.isHidden = false
                lblNoData.text = "No Data Availalble"
                
            }
            else{
                let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
                
                tblView.isHidden = false
                collectionView.isHidden = false
                barChartView.isHidden = false
                
                lblNoData.isHidden = true
                lblNoData.text = ""
                
                if strSearchedtext != ""{
                    isSearching = true
                    searchedText = strSearchedtext
                    
                    if arrOfPropertyType.count > 0 || arrOfUrgentType.count > 0{
                        
                        
                        if (dict?["FromTab"] as? String ?? "").lowercased() == "Assigned".lowercased(){
                            isFromWhichTab = "Assigned"
                            
                            for i in 0..<arrOfLeadNow.arrOfAssignedToCount.count{
                                
                                //if arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased() != "New".lowercased(){
                                    
                                    let strStatusName = arrOfLeadNow.arrOfAssignedToCount[i].strStatusName
                                    
                                    var chartIndexCount = 0
                                    for j in 0..<arrOfPropertyType.count{
                                        
                                        let obj = arrOfPropertyType[j]
                                        
                                        let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                                        
                                        
                                        let temp =  arrOfLeadsTemp.filter{
                                            $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}
                                        
                                        let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                                        
                                        chartIndexCount = chartIndexCount + finalCount
                                        
                                    }
                                    
                                    
                                    for i in 0..<arrOfUrgentType.count{
                                        let obj = arrOfUrgentType[i]
                                        let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                                        
                                        let temp =  arrOfLeadsTemp.filter{
                                            $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId}
                                        
                                        let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                                        
                                        chartIndexCount = chartIndexCount + finalCount
                                        
                                    }
                                    
                                    arrOfChartTitle.append(strStatusName)
                                    arrOfChartValues.append(Double(chartIndexCount))
                              //  }
                                
                            }
                            
                            if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                              setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                            }
                        }
                        
                        else if (dict?["FromTab"] as? String ?? "").lowercased() == "Submitted".lowercased(){
                            isFromWhichTab = "Submitted"
                            
                            for i in 0..<arrOfLeadNow.arrOfSubmittedByCount.count{
                                
                               // if arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName.lowercased() != "New".lowercased(){
                                    
                                    let strStatusName = arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName
                                    
                                    var chartIndexCount = 0
                                    for j in 0..<arrOfPropertyType.count{
                                        
                                        let obj = arrOfPropertyType[j]
                                        
                                        let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                                        
                                        let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strAddressPropertyTypeId == tempAddressPropertyId}
                                        
                                        let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                                        
                                            
                                        chartIndexCount = chartIndexCount + finalCount
                                        
                                    }
                                    
                                    
                                    for i in 0..<arrOfUrgentType.count{
                                        let obj = arrOfUrgentType[i]
                                        let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                                        
                                        let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strUrgencyId == tempUrgencyId}
                                        
                                        let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count

                                        
                                        chartIndexCount = chartIndexCount + finalCount
                                        
                                    }
                                    
                                    arrOfChartTitle.append(strStatusName)
                                    arrOfChartValues.append(Double(chartIndexCount))
                              //  }
                                
                            }
                            
                            if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                              setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                            }
                        }
                    }
                    else{
                        if (dict?["FromTab"] as? String ?? "").lowercased() == "Assigned".lowercased(){
                            isFromWhichTab = "Assigned"
                            
                            for i in 0..<arrOfLeadNow.arrOfAssignedToCount.count{
                              //  if arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased() != "New".lowercased(){
                                    
                                    let temp = arrOfLeadsTemp.filter{
                                       $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased()}
                                    
                                    let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                                    
                                    arrOfChartValues.append(Double(finalCount))
                                    arrOfChartTitle.append(arrOfLeadNow.arrOfAssignedToCount[i].strStatusName)
                                    
                            //    }
                            }
                            if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                              setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                            }
                        }
                        else if (dict?["FromTab"] as? String ?? "").lowercased() == "Submitted".lowercased(){
                            isFromWhichTab = "Submitted"
                            
                            for i in 0..<arrOfLeadNow.arrOfSubmittedByCount.count{
                             //   if arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName.lowercased() != "New".lowercased(){
                                    
                                    let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == arrOfLeadNow.arrOfAssignedToCount[i].strStatusName.lowercased()}
                                    
                                    let finalCount = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}.count
                                    
                                    arrOfChartValues.append(Double(finalCount))
                                    arrOfChartTitle.append(arrOfLeadNow.arrOfSubmittedByCount[i].strStatusName)
                                    
                             //   }
                            }
                            if arrOfChartValues.max() != 0.0 || arrOfChartValues.max() != 0{
                              setChart(dataPoints: arrOfChartTitle, values: arrOfChartValues)
                            }
                        }
                    }
                    
                    
                    if isSummarySelected == true{
                    btnDetails.backgroundColor = UIColor.clear
                    btnDetails.tintColor = UIColor.lightText
                    btnSummary.backgroundColor = UIColor.appYellow
                    btnSummary.tintColor = UIColor.white
                    self.barChartView.isHidden = false
                    self.collectionView.isHidden = false
                    self.tblView.isHidden = false
                    remove(asChildViewController: NewLeadNowVC)

                    if arrOfChartTitle.count > 0{
                        collectionView.reloadData()
                    }
                    }
                    else{
                        btnSummary.backgroundColor = UIColor.white
                        btnSummary.titleLabel?.textColor = UIColor.darkGray
                        
                        btnDetails.backgroundColor = UIColor.appYellow
                        btnDetails.titleLabel?.textColor = UIColor.white
                        
                        self.barChartView.isHidden = true
                        self.collectionView.isHidden = true
                        self.tblView.isHidden = true
                        addChild(NewLeadNowVC)

                    }
                }
                else{
                    
                    methodOfReceivedNotification(notification: notification)
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : fromtab, "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": ""])
                    isSearching = false
                    searchedText = ""
                }
                
            }
        }
    }
    
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
}


extension AssignedLeadNowVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrOfChartTitle.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeadNowSummaryCollectionCell", for: indexPath) as! LeadNowSummaryCollectionCell
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "LeadNowSummaryCollectionCell", for: indexPath as IndexPath) as! LeadNowSummaryCollectionCell
            
            cell.lblSummaryTitle.text = "Total"
            if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                if arrOfUrgentType.count > 0 || arrOfPropertyType.count > 0 {
                    cell.lblSummaryCount.text = String(Int(arrOfChartValues.sum()))
                }
                else{
                    cell.lblSummaryCount.text = String(Int(arrOfChartValues.sum()))
                    //arrOfLeadNow.strTotalAssignedToCount
                }
            }
            else if  isFromWhichTab.lowercased() == "Submitted".lowercased(){
                if arrOfUrgentType.count > 0 || arrOfPropertyType.count > 0 {
                    cell.lblSummaryCount.text = String(Int(arrOfChartValues.sum()))
                }
                else{
                    cell.lblSummaryCount.text = String(Int(arrOfChartValues.sum())) //arrOfLeadNow.strTotalSubmittedByCount
                }
            }
            
            return cell
        }
        else{
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "LeadNowSummaryCollectionCell", for: indexPath as IndexPath) as! LeadNowSummaryCollectionCell
            
            if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                
                cell.lblSummaryTitle.text = arrOfChartTitle[indexPath.row - 1]
                cell.lblSummaryCount.text = String(Int(arrOfChartValues[indexPath.row - 1]))
                
            }
            else if  isFromWhichTab.lowercased() == "Submitted".lowercased(){
                
                cell.lblSummaryTitle.text = arrOfChartTitle[indexPath.row - 1]
                cell.lblSummaryCount.text = String(Int(arrOfChartValues[indexPath.row - 1]))
            }
            
            return cell
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        btnSummary.backgroundColor = UIColor.white
        btnSummary.titleLabel?.textColor = UIColor.darkGray
        
        btnDetails.backgroundColor = UIColor.appYellow
        btnDetails.titleLabel?.textColor = UIColor.white
        
        isSummarySelected = false
        add(asChildViewController: NewLeadNowVC)
        self.barChartView.isHidden = true
        self.collectionView.isHidden = true
        self.tblView.isHidden = true
        if isFromWhichTab.lowercased() == "Assigned".lowercased(){
            if indexPath.row == 0{
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType, "searchText": searchedText])

                }
                else{
                   NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": "Total" , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
            else{
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": arrOfChartTitle[indexPath.row - 1] , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType, "searchText": searchedText])

                }
                else{
                NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Assigned", "CollectionStautsName": arrOfChartTitle[indexPath.row - 1] , "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
        }
        else if  isFromWhichTab.lowercased() == "Submitted".lowercased(){
            if indexPath.row == 0{
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Submitted", "CollectionStautsName": "Total", "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

                }
                else{
                NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Submitted", "CollectionStautsName": "Total", "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
            else{
                if isSearching == true{
                    NotificationCenter.default.post(name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Submitted", "CollectionStautsName": arrOfChartTitle[indexPath.row - 1], "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType , "searchText": searchedText])

                }
                else{
                NotificationCenter.default.post(name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil, userInfo: ["LeadNow" : self.arrOfLeadNow, "FromTab" : "Submitted", "CollectionStautsName": arrOfChartTitle[indexPath.row - 1], "PropertyType": arrOfPropertyType, "UrgencyType": arrOfUrgentType])
                }
            }
        }
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = CGFloat()
        var height = CGFloat()

        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            width = self.collectionView.frame.size.width/CGFloat(3.7)
            height = 75
            heightConstraintForCollectionView.constant = height * 2 + 10
            
        case .pad:
            width = self.collectionView.frame.size.width/CGFloat(4.4)
            height = 90
            heightConstraintForCollectionView.constant = height * 2 + 50

        default:
            break
        }
        
        return CGSize(width: width, height: height)
    }
    
}

extension AssignedLeadNowVC: ChartViewDelegate {
    
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        barChartView.noDataText = "No Data avaialble"
        
        // Prevent from setting an empty data set to the chart (crashes)
        guard dataPoints.count > 0 else { return }
        
        var dataEntries = [BarChartDataEntry]()
        
        for i in 0..<dataPoints.count {
            let entry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(entry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Units Sold")
        chartDataSet.drawValuesEnabled = false
        chartDataSet.colors = [UIColor.red]
        chartDataSet.colors = [UIColor.appThemeColor]
        chartDataSet.stackLabels = dataPoints
        chartDataSet.highlightEnabled = false
        chartDataSet.highlightAlpha = 1
        let chartData = BarChartData(dataSet: chartDataSet)
        barChartView.data = chartData
    
        //Animation bars
        barChartView.animate(xAxisDuration: 0.0, yAxisDuration: 1.0, easingOption: .easeInCubic)
        
        //remove decimal digits
        barChartView.leftAxis.valueFormatter = CustomLabelsYAxisValueFormatter()
        barChartView.leftAxis.granularity = 1
        
        // X axis configurations
        barChartView.xAxis.granularityEnabled = false
        barChartView.xAxis.granularity = 1
        barChartView.xAxis.drawAxisLineEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.labelFont = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 16) : UIFont.systemFont(ofSize: 7)
        barChartView.xAxis.labelTextColor = UIColor.darkGray
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
        barChartView.xAxis.granularity = 1
        
        // Right axis configurations
        barChartView.rightAxis.drawAxisLineEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawLabelsEnabled = false
        
        // Other configurations
        barChartView.highlightPerDragEnabled = false
        barChartView.chartDescription?.text = ""
        barChartView.legend.enabled = false
        barChartView.pinchZoomEnabled = false
        barChartView.doubleTapToZoomEnabled = false
        barChartView.scaleYEnabled = false
        
        barChartView.drawMarkers = true
        
        let l = barChartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 40
        l.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 18) : UIFont.systemFont(ofSize: 12)
        l.xEntrySpace = 30
        
        
    }
}


final class CustomLabelsYAxisValueFormatter : IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let intVal = Int(value)
        return "\(intVal)"
    }
}
