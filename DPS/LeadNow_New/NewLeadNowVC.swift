//
//  NewLeadNowVC.swift
//  DPS
//
//  Created by INFOCRATS on 08/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewLeadNowVC: UIViewController {
    
    //MARK: - IBOUtlets
    weak var delegateTaskAssociate: AddTaskAssociateProtocol?
    weak var delegateActivityAssociate: AddActivityAssociateProtocol?
    var selectionTag = Int()

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!

    //MARK: - variables
    
    var arrOfLeadNow = LeadNowModel()
    var arrofNewLeads = [LeadsModel]()
    var strStatusName = ""
    var isFromWhichTab = ""
    
    var dictLoginData = NSDictionary()
    var strEmpID = String()
    var arrOfPropertyType = [[String: Any]]()
    var arrOfUrgentType = [[String: Any]]()
    var strTypeOfAssociations = String()
    var isSearching = Bool()
    var searchedText = ""

    //MARK: - View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.tblView.tableFooterView = UIView()

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("MoveToLeadNewVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationCollectionViewDidSelect(notification:)), name: Notification.Name("MoveFromAssignedCollectionDidSelect"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationThroughSearch(notification:)), name: Notification.Name("MoveToLeadNewVCThroughSearch"), object: nil)

    }
    
    //MARK: - Call Web Service
    
    @objc func methodOfReceivedNotificationCollectionViewDidSelect(notification: Notification) {
        if notification.userInfo != nil{
            let dict = notification.userInfo
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            
            
            if (dict?["FromTab"] as? String ?? "").lowercased() == "Assigned".lowercased(){
                isFromWhichTab = "Assigned"
            }
            else  if (dict?["FromTab"] as? String ?? "").lowercased() == "Submitted".lowercased(){
                isFromWhichTab = "Submitted"
            }
            
            strStatusName = dict?["CollectionStautsName"] as? String ?? ""
            arrOfPropertyType = dict?["PropertyType"] as! [[String: Any]]
            arrOfUrgentType = dict?["UrgencyType"] as! [[String: Any]]

            getListOfNewLeads()
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        if notification.userInfo != nil{
            let dict = notification.userInfo
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            if (dict?["FromTab"] as? String ?? "").lowercased() == "New".lowercased(){
                isFromWhichTab = "New"
            }
            arrOfPropertyType = dict?["PropertyType"] as! [[String: Any]]
            arrOfUrgentType = dict?["UrgencyType"] as! [[String: Any]]

            getListOfNewLeads()
        }
    }
    
    @objc func methodOfReceivedNotificationThroughSearch(notification: Notification) {
        if notification.userInfo != nil{
            let dict = notification.userInfo
            arrOfLeadNow = dict?["LeadNow"] as! LeadNowModel
            
            let arrOfPropertyTypeTemp = dict?["PropertyType"] as? [[String: Any]] ?? []
            let arrOfUrgentTypeTemp = dict?["UrgencyType"] as? [[String: Any]] ?? []
            
            isFromWhichTab = dict?["FromTab"] as? String ?? ""
            arrOfPropertyType =  arrOfPropertyTypeTemp
            arrOfUrgentType = arrOfUrgentTypeTemp
            strStatusName = dict?["CollectionStautsName"] as? String ?? ""

            let strSearchedtext = dict?["searchText"] as? String ?? ""
            
            //getLeadsCount()
            if strSearchedtext != ""{
                getListOfSearchedNewLeads(strSearchedtext: strSearchedtext)
                isSearching = true
                searchedText = strSearchedtext
            }
            else{
                getListOfNewLeads()
                isSearching = false
                searchedText = ""
            }
        }
    }
   
    func getListOfSearchedNewLeads(strSearchedtext: String){
        arrofNewLeads.removeAll()
        let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
        
        
            if arrOfPropertyType.count == 0 || arrOfUrgentType.count == 0{
                if isFromWhichTab.lowercased() == "New".lowercased(){
                    let temp = arrOfLeadsTemp.filter{ $0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID}
                    
                    arrofNewLeads = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased()) }
                }
                else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                    
                    if strStatusName != ""{
                        if strStatusName.lowercased() == "Total".lowercased(){
                            let temp = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID}
                            
                            arrofNewLeads = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                            
                        }
                        else{
                            let  temp = arrOfLeadsTemp.filter{
                                $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()}
                                    
                            arrofNewLeads = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                        }
                    }
                    else{
                        let temp =  arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID }
                        
                        arrofNewLeads = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased()) }
                    }
                    
                }
                else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                    
                    if strStatusName != ""{
                        
                        if strStatusName.lowercased() == "Total".lowercased(){
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}
                                
                            arrofNewLeads = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                            
                        }
                        else{
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()}
                                
                            arrofNewLeads = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                        }
                    }
                    else{
                        let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}
                        
                        arrofNewLeads = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                    }
                }
            }
            else{
                for i in 0..<arrOfPropertyType.count{
                    let obj = arrOfPropertyType[i]
                    let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                    
                    if isFromWhichTab.lowercased() == "New".lowercased(){
                        
                        let temp =  arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID}
                        
                        
                        let finalDict =  temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                        
                        
                        arrofNewLeads.append(contentsOf: finalDict)
                        
                    }
                    
                    else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                        
                        if strStatusName != ""{
                            if strStatusName.lowercased() == "Total".lowercased(){
                                
                                let temp = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}
                                
                                let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                arrofNewLeads.append(contentsOf: finalDict)
                                
                            }
                            else{
                                
                                let temp =  arrOfLeadsTemp.filter{
                                    $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}
                                
                                let finalDict = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                arrofNewLeads.append(contentsOf: finalDict)
                                
                            }
                        }
                        else{
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId}
                            
                            let finalDict = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                            
                            arrofNewLeads.append(contentsOf: finalDict)
                            
                        }
                        
                    }
                    else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                        
                        if strStatusName != ""{
                            
                            if strStatusName.lowercased() == "Total".lowercased(){
                                
                                let temp =  arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId == tempAddressPropertyId }
                                
                                let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                
                                arrofNewLeads.append(contentsOf: finalDict)
                                
                            }
                            else{
                                
                                let temp =  arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strAddressPropertyTypeId == tempAddressPropertyId}
                                
                                let finalDict =
                                    temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                
                                arrofNewLeads.append(contentsOf: finalDict)
                                
                            }
                        }
                        else{
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId }
                            
                            let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                            
                            arrofNewLeads.append(contentsOf: finalDict)
                            
                        }
                    }
                }
                
                
                for i in 0..<arrOfUrgentType.count{
                    let obj = arrOfUrgentType[i]
                    let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                    
                    if isFromWhichTab.lowercased() == "New".lowercased(){
                        let temp = arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID}
                        
                        let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                        
                        arrofNewLeads.append(contentsOf: finalDict)
                        

                    }
                    
                    else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                        
                        if strStatusName != ""{
                            if strStatusName.lowercased() == "Total".lowercased(){
                                
                                let temp = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}
                                    
                                let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                
                                arrofNewLeads.append(contentsOf: finalDict)

                                
                            }
                            else{
                                
                                let temp =  arrOfLeadsTemp.filter{
                                    $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId}
                                    
                                    let finalDict =  temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                arrofNewLeads.append(contentsOf: finalDict)

                            }
                        }
                        else{
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}

                                
                                let finalDict = temp.filter{$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                            

                            arrofNewLeads.append(contentsOf: finalDict)

                        }
                        
                    }
                    else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                        
                        if strStatusName != ""{
                            
                            if strStatusName.lowercased() == "Total".lowercased(){
                                
                                let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}
                                    
                                    let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                arrofNewLeads.append(contentsOf: finalDict)

                                
                            }
                            else{
                                
                                let temp =  arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strUrgencyId.lowercased() == tempUrgencyId}
                                    
                                let finalDict = temp.filter{ $0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                                
                                arrofNewLeads.append(contentsOf: finalDict)

                            }
                        }
                        else{
                            
                            let temp = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId}

                            let finalDict  = temp.filter {$0.strLeadName.lowercased().contains(strSearchedtext.lowercased())  || $0.strFirstName.lowercased().contains(strSearchedtext.lowercased()) || $0.strLastName.lowercased().contains(strSearchedtext.lowercased()) || $0.strMiddleName.lowercased().contains(strSearchedtext.lowercased())}
                        
                        arrofNewLeads.append(contentsOf: finalDict)

                        }
                    }
                }
            }
            
        if arrofNewLeads.count > 0{
            tblView.isHidden = false
            tblView.reloadData()
            lblNoData.isHidden = true
            lblNoData.text = ""
        }
        else{
            tblView.isHidden = true
            lblNoData.isHidden = false
            lblNoData.text = "No Data available"

        }
    }
    
    func getListOfNewLeads(){
        
        arrofNewLeads.removeAll()
        let arrOfLeadsTemp = arrOfLeadNow.arrOfLeads
        
        
            if arrOfPropertyType.count == 0 && arrOfUrgentType.count == 0{
                if isFromWhichTab.lowercased() == "New".lowercased(){
                    arrofNewLeads = arrOfLeadsTemp.filter{ $0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID}
                }
                else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                    
                    if strStatusName != ""{
                        if strStatusName.lowercased() == "Total".lowercased(){
                            arrofNewLeads = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID }
                            
                        }
                        else{
                            arrofNewLeads = arrOfLeadsTemp.filter{
                                $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()}
                        }
                    }
                    else{
                        arrofNewLeads = arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID }
                    }
                    
                }
                else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                    
                    if strStatusName != ""{
                        
                        if strStatusName.lowercased() == "Total".lowercased(){
                            arrofNewLeads = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}
                            
                        }
                        else{
                            arrofNewLeads = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()}
                        }
                    }
                    else{
                        arrofNewLeads = arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID}
                    }
                }
            }
            else{
                for i in 0..<arrOfPropertyType.count{
                    let obj = arrOfPropertyType[i]
                    let tempAddressPropertyId = String(describing:  obj["AddressPropertyTypeId"] as! NSNumber)
                    
                    if isFromWhichTab.lowercased() == "New".lowercased(){
                        arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId  && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID})
                    }
                    
                    else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                        
                        if strStatusName != ""{
                            if strStatusName.lowercased() == "Total".lowercased(){
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId })
                                
                            }
                            else{
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{
                                                        $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId})
                            }
                        }
                        else{
                            arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId})
                        }
                        
                    }
                    else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                        
                        if strStatusName != ""{
                            
                            if strStatusName.lowercased() == "Total".lowercased(){
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId == tempAddressPropertyId })
                                
                            }
                            else{
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strAddressPropertyTypeId == tempAddressPropertyId})
                            }
                        }
                        else{
                            arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strAddressPropertyTypeId.lowercased() == tempAddressPropertyId})
                        }
                    }
                }
                
                
                for i in 0..<arrOfUrgentType.count{
                    let obj = arrOfUrgentType[i]
                    let tempUrgencyId = String(describing:  obj["UrgencyId"] as! NSNumber)
                    
                    if isFromWhichTab.lowercased() == "New".lowercased(){
                        arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{$0.strStatusSysName.lowercased() == "New".lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId  && $0.strAssignedType.lowercased() == "".lowercased() && $0.strAssignedToId != strEmpID})
                    }
                    
                    else if isFromWhichTab.lowercased() == "Assigned".lowercased(){
                        
                        if strStatusName != ""{
                            if strStatusName.lowercased() == "Total".lowercased(){
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId})
                                
                            }
                            else{
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{
                                                        $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased() && $0.strUrgencyId.lowercased() == tempUrgencyId})
                            }
                        }
                        else{
                            arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strAssignedType.lowercased() == "Individual".lowercased() && $0.strAssignedToId == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId})
                        }
                        
                    }
                    else if isFromWhichTab.lowercased() == "Submitted".lowercased(){
                        
                        if strStatusName != ""{
                            
                            if strStatusName.lowercased() == "Total".lowercased(){
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId})
                                
                            }
                            else{
                                arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID && $0.strStatusName.lowercased() == strStatusName.lowercased()  && $0.strUrgencyId.lowercased() == tempUrgencyId})
                            }
                        }
                        else{
                            arrofNewLeads.append(contentsOf: arrOfLeadsTemp.filter{ $0.strSubmittedById == strEmpID  && $0.strUrgencyId.lowercased() == tempUrgencyId})
                        }
                    }
                }
            }
            
        if arrofNewLeads.count > 0{
            tblView.isHidden = false
            tblView.reloadData()
            lblNoData.isHidden = true
            lblNoData.text = ""
        }
        else{
            tblView.isHidden = true
            lblNoData.isHidden = false
            lblNoData.text = "No Data available"

        }
        
        
    }
}

extension NewLeadNowVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrofNewLeads.count > 0{
        return arrofNewLeads.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if arrofNewLeads.count > 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadNowNewCell", for: indexPath as IndexPath) as! LeadNowNewCell
            
            
            cell.lblLeadTitle.text = arrofNewLeads[indexPath.row].strLeadName
            
            let rangeTime =  getTotalTimeAfterCreated(strdate: arrofNewLeads[indexPath.row].strCreatedDate)
            
            if(rangeTime.count != 0){
                cell.lblLeadDuration.text  =  rangeTime
            }
            cell.lblLeadDuration.text = rangeTime //String(arrofNewLeads[indexPath.row].strCreatedDate.split(separator: "T")[0])
            cell.lblLeadSubTitle.text = arrofNewLeads[indexPath.row].strFirstName + " " + arrofNewLeads[indexPath.row].strLastName
            
            cell.lblLeadNowCircle.text = "\u{2022}"
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let isForAssociation = nsud.bool(forKey: "fromWebLeadAssociation")
        if isForAssociation {
            
            nsud.set(false, forKey: "fromWebLeadAssociation")
            nsud.synchronize()
            
            var dictData = [AnyHashable: Any]()
            strTypeOfAssociations = nsud.string(forKey: "strTypeOfAssociationsFromWebLead") ?? ""
 
            
            nsud.set("", forKey: "strTypeOfAssociationsFromWebLead")
            nsud.synchronize()

            if strTypeOfAssociations == "Activity" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedActivity_Notification"), object: nil, userInfo: dictData)
                
            } else if strTypeOfAssociations == "Task"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedTask_Notification"), object: nil, userInfo: dictData)
                
            } else if strTypeOfAssociations == "Company"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedCompany_Notification"), object: nil, userInfo: dictData )
                
            } else if strTypeOfAssociations == "Contact"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedContact_Notification"), object: nil, userInfo: dictData )
                
            } else if strTypeOfAssociations == "Account"{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAccount_Notification"), object: nil, userInfo: dictData)
                
            }
            //-------Association Task Activity------
            else if(strTypeOfAssociations == "AddActivityCRM_Associations"){
                
                dictData = ["leadId": arrofNewLeads[indexPath.row].strLeadId,
                            "leadName": arrofNewLeads[indexPath.row].strLeadName]
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddActivityCRM_AssociationsFromNewLeadVC"), object: nil, userInfo: dictData)

                //self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictData as NSDictionary, tag: selectionTag)
            }
            else if(strTypeOfAssociations == "AddTaskCRM_Associations"){
                
                dictData = ["leadId": arrofNewLeads[indexPath.row].strLeadId,
                            "leadName": arrofNewLeads[indexPath.row].strLeadName]
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddTaskCRM_AssociationsFromNewLeadVC"), object: nil, userInfo: dictData)

                //self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictData as NSDictionary, tag: 2)
            }

            self.navigationController?.popViewController(animated: false)
            
        } else {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
            
            vc.strReftype = "weblead"
            vc.strRefIdNew = arrofNewLeads[indexPath.row].strLeadId
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}


class LeadNowNewCell: UITableViewCell
{
    @IBOutlet weak var lblLeadTitle: UILabel!
    @IBOutlet weak var lblLeadDuration: UILabel!
    @IBOutlet weak var lblLeadSubTitle: UILabel!
    @IBOutlet weak var lblLeadMarker: UILabel!
    @IBOutlet weak var lblLeadNowCircle: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        lblLeadNowCircle.layer.cornerRadius =  4.0

        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

