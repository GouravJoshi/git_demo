//
//  LeadNowSummaryCollectionCell.swift
//  DPS
//
//  Created by INFOCRATS on 10/09/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class LeadNowSummaryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSummaryTitle: UILabel!
    @IBOutlet weak var lblSummaryCount: UILabel!
    @IBOutlet weak var btnCollectionClick: UIButton!

}
