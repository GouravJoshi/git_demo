//
//  ChangePasswordView.m
//  DPS
//
//  Created by Rakesh Jain on 01/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "ChangePasswordView.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"

#import "DejalActivityView.h"
#import "LoginViewController.h"

@interface ChangePasswordView ()
{
    Global *global;
}

@end

@implementation ChangePasswordView

- (void)viewDidLoad {
    [super viewDidLoad];
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark TouchesBegan Method
//============================================================================
//============================================================================

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_txt_NewPassword resignFirstResponder];
    [_txt_OldPassword resignFirstResponder];
    [_txt_ConfirmPassword resignFirstResponder];
    if (self.view.frame.origin.y==-90.0) {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 90.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}

//============================================================================
//============================================================================
#pragma mark TextField Delegate Method
//============================================================================
//============================================================================

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }

        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 50) ? NO : YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txt_OldPassword)
    {
        [_txt_NewPassword becomeFirstResponder];
        
    }
    else if (textField==_txt_NewPassword)
    {
        [_txt_ConfirmPassword becomeFirstResponder];
    }
    else if (textField==_txt_ConfirmPassword)
    {
        [_txt_ConfirmPassword resignFirstResponder];
    }
    
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    
    
    textField.leftView = leftView;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

    if(textField==_txt_OldPassword)
    {
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDelegate:self];
//        [UIView setAnimationDuration:0.5];
//        [UIView setAnimationBeginsFromCurrentState:YES];
//        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 90.0), self.view.frame.size.width, self.view.frame.size.height);
//        [UIView commitAnimations];
    }
    if(textField==_txt_NewPassword)
    {
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDelegate:self];
//        [UIView setAnimationDuration:0.5];
//        [UIView setAnimationBeginsFromCurrentState:YES];
//        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 90.0), self.view.frame.size.width, self.view.frame.size.height);
//        [UIView commitAnimations];
    }
    if(textField==_txt_ConfirmPassword)
    {
        if (self.view.frame.origin.y==0.0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 90.0), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==_txt_ConfirmPassword)
    {
        if (self.view.frame.origin.y==-90.0) {

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 90.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
        }
    }
}

//============================================================================
//============================================================================
#pragma mark--- Action Buttons Methods
//============================================================================
//============================================================================

-(IBAction)hideKeyBoard:(id)sender{
    [self resignFirstResponder];
}

- (IBAction)action_Back:(id)sender {
    
}
- (IBAction)action_ChangePassword:(id)sender {
    [self ChangePasswordMethod];
}
- (IBAction)action_Help:(id)sender {
    
    [global displayAlertController:Info :PassWordValid :self];
    
}

//============================================================================
//============================================================================
#pragma mark--- Methods
//============================================================================
//============================================================================
-(NSString *)validationCheck
{   //passwordForMatch
    
    //NSUserDefaults *defsPassOld=[NSUserDefaults standardUserDefaults];
   // NSString *strPasss=[defsPassOld valueForKey:@"passwordForMatch"];
    
    NSString *errorMessage;
    if (_txt_OldPassword.text.length==0) {
        errorMessage = @"Please enter Old Password";
    } else if (_txt_NewPassword.text.length==0){
        errorMessage = @"Please enter New Password";
    } else if (_txt_ConfirmPassword.text.length==0){
        errorMessage = @"Please enter Confirm Password";
    } else if (!(_txt_NewPassword.text.length>=6)){// && _txt_NewPassword.text.length<=10
        errorMessage = @"Password must be atleast 6 characters";
    }
//    else if (![[strPasss stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] isEqualToString:[_txt_OldPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]){
//        errorMessage = @"Old Password did not match.";
//    }
    else if (![[_txt_NewPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] isEqualToString:[_txt_ConfirmPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]){
        errorMessage = @"New Password and Confirm Password did not match.";
    }
    return errorMessage;
}

-(void)goToDashBoard{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"YesLoggedIn"];
    [defs setBool:YES forKey:@"fromChangePass"];
    [defs synchronize];

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    LoginViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

-(void)ChangePasswordMethod{
    NSString *errorMessage=[self validationCheck];
    if (errorMessage) {
        NSString *strTitle = Alert;
        [global AlertMethod:strTitle :errorMessage];
        [DejalBezelActivityView removeView];
    } else {
        
        if (self.view.frame.origin.y==-90.0) {
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationBeginsFromCurrentState:YES];
            self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 90.0), self.view.frame.size.width, self.view.frame.size.height);
            [UIView commitAnimations];
        }

        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Changing Password..."];

        //============================================================================
        //============================================================================

       // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSString *strUsername,*strCompanyKeyy;
        NSUserDefaults *defsUSername=[NSUserDefaults standardUserDefaults];
        strUsername=[defsUSername valueForKey:@"usernameChangePass"];//CompanyKeyChangePass
        strCompanyKeyy=[defsUSername valueForKey:@"CompanyKeyChangePass"];
        if (strUsername.length==0) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];

            [self goToDashBoard];
        }else if(strCompanyKeyy.length==0){

            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            [self goToDashBoard];

        }
        else{
            
        //============================================================================
        //============================================================================

            
            NSUserDefaults *defsConfirmPass=[NSUserDefaults standardUserDefaults];
            [defsConfirmPass setValue:_txt_ConfirmPassword.text forKey:@"confirmPass"];
            [defsConfirmPass synchronize];
            
        NSArray *objects = [NSArray arrayWithObjects:[_txt_OldPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
            [_txt_NewPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
            strUsername,
            strCompanyKeyy,
            @"Employee",
            [_txt_ConfirmPassword.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],nil];
        
        NSArray *keys = [NSArray arrayWithObjects:@"OldPassword",
                         @"NewPassword",
                         @"EmailOrUserName",
                         @"CompanyKey",
                         @"LoginUserType",
                         @"ConfirmPassword", nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",MainUrl,UrlChangePassword];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrlPostMethod:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         _txt_ConfirmPassword.text=@"";
                         _txt_NewPassword.text=@"";
                         
                         if ([response isKindOfClass:[NSDictionary class]]) {
                             
                             NSArray *arrKeys = [response allKeys];
                             
                             if ([arrKeys containsObject:@"IsSucceeded"]) {
                                 
                                 BOOL isSuccess = [[response valueForKey:@"IsSucceeded"] boolValue];
                                 
                                 if (isSuccess) {
                                     
                                     [self AfterServerResponse:response];
                                     
                                 } else {
                                     
                                     if ([arrKeys containsObject:@"Errors"]) {
                                         
                                         NSArray *arrError = [response valueForKey:@"Errors"];
                                         
                                         if (arrError.count>0) {
                                             
                                             NSString *strMsgs = arrError[0];
                                             NSString *strTitle = Alert;
                                             [global AlertMethod:strTitle :strMsgs];
                                             
                                         } else {
                                             
                                             NSString *strTitle = Alert;
                                             [global AlertMethod:strTitle :ValidPass];
                                             
                                         }
                                         
                                     }else{
                                         
                                         NSString *strTitle = Alert;
                                         [global AlertMethod:strTitle :ValidPass];
                                         
                                     }
                                     
                                 }
                                 
                             } else {
                                 
                                 NSString *strTitle = Alert;
                                 [global AlertMethod:strTitle :ValidPass];
                                 
                             }
                             
                         } else {
                             
                             NSString *strTitle = Alert;
                             [global AlertMethod:strTitle :ValidPass];
                             
                         }
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         [global AlertMethod:strTitle :ValidPass];
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
        
    }
    
    }
}

//============================================================================
//============================================================================
#pragma mark--- AfterServerResponse
//============================================================================
//============================================================================

-(void)AfterServerResponse :(NSDictionary *)ResponseDict{
    if (ResponseDict.count==0) {
        NSString *strTitle = Alert;
        NSString *strMsg = ErrorInternetMsg;
        [global AlertMethod:strTitle :strMsg];
        [DejalBezelActivityView removeView];
    } else {
        NSString *strError =([[ResponseDict valueForKey:@"Error"] isKindOfClass:[NSNull class]]) ? @"" : [ResponseDict valueForKey:@"Error"];
        if (strError.length==0) {
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:NO forKey:@"YesChangePassword"];
            [defs synchronize];
            [self goToDashBoard];
            [DejalBezelActivityView removeView];
        } else {
            //[self goToDashBoard];
            [DejalBezelActivityView removeView];
        }
    }
}

@end
