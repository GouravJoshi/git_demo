//
//  AllImportsViewController.h
//  DPS
//
//  Created by Saavan Patidar on 21/04/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+globalColor.h"
#import "FTIndicator.h"

#import "LoginViewController.h"
#import "AppointmentView.h"
#import "TaskList.h"
#import "SettingsView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "EXPhotoViewer.h"
#import "AddLead+CoreDataProperties.h"
#import "AddLead.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "HistoryView.h"
#import "GeneralInfoAppointmentView.h"
#import "SalesAutomationInspection.h"
#import "ServiceDetailAppointmentView.h"
#import "GeneralInfoAppointmentView.h"
#import "BeginAuditView.h"
#import "InvoiceAppointmentView.h"
#import "SalesAutomationAgreementProposal.h"
#import "SaleAutomationGeneralInfo.h"
#import "InitialSetUp.h"
#import <sys/utsname.h>
#include <sys/param.h>
#include <sys/mount.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "SalesPaymentViewController.h"

#import "MTBBarcodeScanner.h"
#import "TermiteSelectStateViewController.h"
#import "TermiteInspectionTexasViewController.h"
#import "TermiteSignViewController.h"
#import "Global.h"
#import "TexasTermiteServiceDetail+CoreDataProperties.h"
#import "TexasTermiteServiceDetail+CoreDataClass.h"
#import "GraphImageTermiteViewController.h"
#import "CustomerAddress+CoreDataProperties.h"
#import "CustomerAddress+CoreDataClass.h"
#import "ImageDetailsServiceAuto+CoreDataClass.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ImageDetailsServiceAuto+CoreDataClass.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ScannerViewController.h"
#import "QuoteViewController.h"
#import "CreateWorkOrder2_iPadViewController.h"
#import "CreateWorkOrder_iPadViewController.h"

//#import <triPOSMobileSDK/triPOSMobileSDK.h>
#import "SavedCardsTableViewCell.h"
#import "GeneralInfoImagesCollectionViewCell.h"
#import "CustomImageFlowLayout.h"
#import "EditGraphViewController.h"
#import "GraphImagePreviewViewController.h"
#import "LoginViewControlleriPad.h"
#import "DLPieChart.h"

#import "ChangePasswordViewiPad.h"
#import "AppointmentViewiPad.h"
#import "GeneralInfoAppointmentViewiPad.h"
#import "InvoiceAppointmentViewiPad.h"
#import "ImagePreviewGeneralInfoAppointmentViewiPad.h"
#import "ServiceSendMailViewControlleriPad.h"
#import "ServiceSignViewControlleriPad.h"
#import "ServiceDocumentsViewControlleriPad.h"
#import "ServiceDetailAppointmentViewiPad.h"
#import "EditImageViewControlleriPad.h"
#import "EditGraphViewControlleriPad.h"
#import "GraphImagePreviewViewControlleriPad.h"
#import "HistoryViewiPad.h"
#import "SettingsViewiPad.h"
#import "RecordAudioViewiPad.h"
#import "OutBoxViewiPad.h"
#import "GenerateWorkOrder.h"
#import "EquipmentsViewControlleriPad.h"
#import "BeginAuditViewiPad.h"
#import "ServiceSendMailViewController.h"
//SalesiPad Import
#import "SaleAutomationGeneralInfoiPad.h"
#import "SalesAutomationInspectioniPad.h"
#import "SalesAutomationSelectServiceiPad.h"
#import "AddStandardServiceiPad.h"
#import "AddNonStandardServicesiPad.h"
#import "AddPlusServiceiPad.h"
#import "SalesAutomationServiceSummaryiPad.h"
#import "SalesAutomationAgreementProposaliPad.h"
#import "GenerateWorkOrderiPad.h"
#import "InitialSetUpiPad.h"
#import "ServiceSendMailViewControlleriPad.h"
#import "SignViewControlleriPad.h"
#import "SendMailViewControlleriPad.h"
#import "ImagePreviewSalesAutoiPad.h"
#import "LeadAppliedDiscounts+CoreDataProperties.h"
#import "LeadAppliedDiscounts+CoreDataClass.h"

//Clark Pest Sales
#import "ClarkPestSalesSelectServiceiPad.h"
#import "ClarkPestSalesServiceSummaryiPad.h"
#import "ClarkPestSalesAgreementProposaliPad.h"
#import "ClarkPestConfigureProposaliPad.h"


#import "ClarkPestSelectServiceTableViewCell.h"

#import "ClarkPestSalesSelectServiceiPhone.h"
#import "ClarkPestSalesAgreementProposaliPhone.h"
#import "ClarkPestConfigureProposaliPhone.h"

// Clark Pest Data Base

#import "LeadCommercialScopeExtDc+CoreDataClass.h"
#import "LeadCommercialScopeExtDc+CoreDataProperties.h"

#import "LeadCommercialTargetExtDc+CoreDataClass.h"
#import "LeadCommercialTargetExtDc+CoreDataProperties.h"

#import "LeadCommercialDiscountExtDc+CoreDataClass.h"
#import "LeadCommercialDiscountExtDc+CoreDataProperties.h"

#import "LeadCommercialInitialInfoExtDc+CoreDataClass.h"
#import "LeadCommercialInitialInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialMaintInfoExtDc+CoreDataClass.h"
#import "LeadCommercialMaintInfoExtDc+CoreDataProperties.h"

#import "LeadCommercialTermsExtDc+CoreDataClass.h"
#import "LeadCommercialTermsExtDc+CoreDataProperties.h"

#import "LeadCommercialDetailExtDc+CoreDataClass.h"
#import "LeadCommercialDetailExtDc+CoreDataProperties.h"

#import "LeadMarketingContentExtDc+CoreDataClass.h"
#import "LeadMarketingContentExtDc+CoreDataProperties.h"
//Mechanical Info

#import "MechanicalGeneralInfoViewController.h"
#import "MechanicalGeneralSubWorkOrderTableViewCell.h"
#import "RKCustomButton.h"
#import "RKTagsView.h"
#import "MechanicalSubWorkOrder+CoreDataClass.h"
#import "MechanicalSubWorkOrder+CoreDataClass.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "EmailDetailServiceAuto.h"
#import "EmailDetailServiceAuto+CoreDataProperties.h"
#import "MechanicalSubWorkOrderActualHoursDcs+CoreDataClass.h"
#import "MechanicalSubWorkOrderActualHoursDcs+CoreDataProperties.h"
#import "MechanicalSubWOTechHelperDcs+CoreDataClass.h"
#import "MechanicalSubWOTechHelperDcs+CoreDataProperties.h"
#import "MechanicalSubWOIssueRepairDcs+CoreDataClass.h"
#import "MechanicalSubWOIssueRepairDcs+CoreDataProperties.h"
#import "MechanicalSubWOIssueRepairDcs+CoreDataProperties.h"
#import "MechannicalSubWOIssueRepairLaborDcs+CoreDataClass.h"
#import "MechannicalSubWOIssueRepairLaborDcs+CoreDataProperties.h"
#import "MechanicalSubWorkOrderIssueDcs+CoreDataClass.h"
#import "MechanicalSubWorkOrderIssueDcs+CoreDataProperties.h"
#import "MechanicalSubWOIssueRepairPartDcs+CoreDataClass.h"
#import "MechanicalSubWOIssueRepairPartDcs+CoreDataProperties.h"
#import "MechanicalSubWorkOrderNoteDcs+CoreDataClass.h"
#import "MechanicalSubWorkOrderNoteDcs+CoreDataProperties.h"
#import "MechanicalSubWorkOrderDetailsViewControlleriPad.h"
#import "MechanicalServiceIssueTableViewCell.h"
#import "MechanicalNotesTableViewCell.h"
#import "MechanicalClientApprovalViewController.h"
#import "MechanicalInvoiceViewController.h"
#import "MechanicalStartRepairViewController.h"
#import "AddLaboriPadViewController.h"
#import "AddLaborTableViewCell.h"
#import "AddPartsiPadViewController.h"
#import "AddPartsTableViewCell.h"
#import "MechanicalSubWorkOrderDetailsViewControllerTMiPadViewController.h"
#import "ServiceDocumentsViewController.h"
#import "MechanicalStartRepairTM.h"
#import "SendMailMechanicaliPadViewController.h"
#import "MechanicalSubWOPaymentDetailDcs+CoreDataClass.h"
#import "MechanicalSubWOPaymentDetailDcs+CoreDataProperties.h"
#import "SyncMechanicalViewController.h"
#import "ServiceHistoryMechanical.h"
#import "ServiceHistoryTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ServiceDynamicForm+CoreDataProperties.h"
#import "ServiceDynamicForm+CoreDataClass.h"
#import "MechanicalDynamicForm+CoreDataClass.h"
#import "MechanicalDynamicForm+CoreDataProperties.h"
#import "MechanicalDynamicViewController.h"
#import "ChemicalListPreview.h"
#import "EquipmentsViewController.h"
#import "EditImageViewController.h"
#import "ActivityList.h"
#import "ActivityList+CoreDataProperties.h"
#import "WoOtherDocuments+CoreDataClass.h"
#import "WoOtherDocuments+CoreDataProperties.h"
#import "ServiceGraphCollectionViewCell.h"
#import "CreditCardIntegration.h"
#import "ServiceNotesHistoryViewController.h"
#import "UIView+Toast.h"
#import "PurchaseOrderViewController.h"
#import "PurchaseOrderGroupViewController.h"
#import "MechanicalPurchaseOrderGeneric+CoreDataClass.h"
#import "MechanicalPurchaseOrderGeneric+CoreDataProperties.h"
#import "MechanicalPurchaseOrderStandard+CoreDataClass.h"
#import "MechanicalPurchaseOrderStandard+CoreDataProperties.h"
#import "MechanicalEquipmentsViewControlleriPad.h"
#import "MechanicalWoEquipment+CoreDataClass.h"
#import "MechanicalWoEquipment+CoreDataProperties.h"
#import "PrintiPhoneViewController.h"
#import "EquipmentHistoryTableViewCell.h"
#import "EquipmentHistoryViewController.h"
#import "PoHistoryViewController.h"
#import "PoHistoryTableViewCell.h"
#import "BillingAddressPOCDetailDcs+CoreDataClass.h"
#import "BillingAddressPOCDetailDcs+CoreDataProperties.h"
#import "ServiceAddressPOCDetailDcs+CoreDataClass.h"
#import "ServiceAddressPOCDetailDcs+CoreDataProperties.h"
#import "CreateSubWorkOrderViewControlleriPad.h"
#import "QuoteHistoryViewController.h"
#import "DrawingBoardViewController.h"
#import "GraphDrawingBoardViewController.h"
#import "ReceivedPOViewController.h"
#import "SubWOCompleteTimeExtSerDcs+CoreDataClass.h"
#import "SubWOCompleteTimeExtSerDcs+CoreDataProperties.h"
#import "SubWoEmployeeWorkingTimeExtSerDcs+CoreDataClass.h"
#import "SubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h"
#import "TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataClass.h"
#import "TempSubWoEmployeeWorkingTimeExtSerDcs+CoreDataProperties.h"
#import "EmpTimeSheetViewController.h"
#import "PrintDocTableViewCell.h"
#import "MechanicalDocumentsViewController.h"
#import "WoOtherDocExtSerDcs+CoreDataProperties.h"
#import "WoOtherDocExtSerDcs+CoreDataClass.h"
#import "ACFloatingTextField.h"

#import "NotesViewControlleriPhone.h"
#import "WebLeads+CoreDataClass.h"
#import "WebLeads+CoreDataProperties.h"
#import "MapAnnotations.h"

//#import "UpdateLeadViewControlleriPad.h"
#import "WeeklyTimeSheetViewControlleriPad.h"

//Termite Image
#import "ImageDetailsTermite+CoreDataProperties.h"
#import "ImageDetailsTermite+CoreDataClass.h"
#import "ImagePreviewTermiteViewController.h"
#import "ClockInOutViewController.h"
#import "ClockInOutViewControlleriPad.h"

#import "LeadAgreementChecklistSetups+CoreDataProperties.h"
#import "LeadAgreementChecklistSetups+CoreDataClass.h"

#import "ElectronicAuthorization_iPad.h"

#import "ElectronicAuthorizedForm+CoreDataClass.h"
#import "ElectronicAuthorizedForm+CoreDataProperties.h"
#import "ChemicalSensitivityList.h"

// Akshay Start //
#import "AccountDiscountExtSerDcs+CoreDataProperties.h"
#import "AccountDiscountExtSerDcs+CoreDataClass.h"

#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataProperties.h"
#import "WorkOrderAppliedDiscountExtSerDcs+CoreDataClass.h"
// Akshay End //

#import "NotesViewController.h"
#import "StatisticsViewController.h"
#import "tblCell.h"
#import "StatisticsSettingsViewController.h"
#import "MechanicalWOs+CoreDataClass.h"
#import "MechanicalWOs+CoreDataProperties.h"

#import "InspectionVC.h"
#import "AudioPlayerVC.h"
#import "ServiceSignViewController.h"
#import "DeviceDynamicInspectionVC.h"
#import "GlobalSyncViewController.h"
#import "GlobalSyncViewControlleriPad.h"


#import "LeadContactDetailExtSerDc+CoreDataClass.h"
#import "LeadContactDetailExtSerDc+CoreDataProperties.h"

#import "RenewalServiceTableViewCell.h"
#import "RenewalServiceExtDcs+CoreDataClass.h"
#import "RenewalServiceExtDcs+CoreDataProperties.h"

#import "TaskAppointmentVC+CoreDataClass.h"
#import "EmployeeBlockTime+CoreDataClass.h"


#import "ElectronicAuthorization_iPhone.h"


@interface AllImportsViewController : UIViewController

@end
