//
//  WoOtherDocuments+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 18/09/17.
//
//

#import "WoOtherDocuments+CoreDataProperties.h"

@implementation WoOtherDocuments (CoreDataProperties)

+ (NSFetchRequest<WoOtherDocuments *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WoOtherDocuments"];
}

@dynamic isActive;
@dynamic isChecked;
@dynamic isDefault;
@dynamic otherDocSysName;
@dynamic otherDocumentPath;
@dynamic title;
@dynamic woOtherDocumentId;
@dynamic workorderId;
@dynamic userName;
@dynamic companyKey;

@end
