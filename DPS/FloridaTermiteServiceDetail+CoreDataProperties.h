//
//  FloridaTermiteServiceDetail+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 09/01/18.
//
//

#import "FloridaTermiteServiceDetail+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FloridaTermiteServiceDetail (CoreDataProperties)

+ (NSFetchRequest<FloridaTermiteServiceDetail *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *floridaTermiteServiceDetail;
@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
