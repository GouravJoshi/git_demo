//
//  FloridaTermiteServiceDetail+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 09/01/18.
//
//

#import "FloridaTermiteServiceDetail+CoreDataProperties.h"

@implementation FloridaTermiteServiceDetail (CoreDataProperties)

+ (NSFetchRequest<FloridaTermiteServiceDetail *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FloridaTermiteServiceDetail"];
}

@dynamic floridaTermiteServiceDetail;
@dynamic companyKey;
@dynamic userName;
@dynamic workorderId;

@end
