//
//  InvoiceAppointmentView.m
//  DPS
//
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "InvoiceAppointmentView.h"
#import "AllImportsViewController.h"
#import "ServiceDetailAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceSignViewController.h"
#import "RecordAudioView.h"
#import "ImagePreviewGeneralInfoAppointmentView.h"
#import "BeginAuditView.h"
#import "ServiceSendMailViewController.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "ServiceDocumentsViewController.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "EquipmentsViewController.h"
#import "EquipmentListInvoiceViewTableViewCell.h"
#import "EditImageViewController.h"
#import "CreditCardIntegration.h"
#import <CoreLocation/CoreLocation.h>
#import "TermiteInspectionFloridaViewController.h"
#import "DPS-Swift.h"

@interface InvoiceAppointmentView ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate>
{
    Global *global;
    UIView *MainViewForm,*ViewFormSections,*viewForm,*viewSection;
    NSMutableArray *arrayOfButtonsMain;
    NSMutableArray *arrOFImagesName;
    NSString *strEmpID,*strCompanyKey,*strCompanyId,*strServiceUrlMain,*strUserName,*strGlobalPaymentMode;
    
    //Change for Chemical List
    
    UIView *MainViewFormChemical,*ViewFormSectionsChemical,*viewFormChemical,*viewSectionChemical;
    UIButton *BtnMainViewChemical;
    
    //New change on 24 august 2016 for dynamic form
    NSMutableArray *arrayViewsChemical,*arrayProductName,*arrayOfUnit,*arrayOfTarget;
    NSMutableArray *arrayOfButtonsMainChemical;
    NSMutableArray *arrOfHeightsViewsChemical;
    NSMutableArray *arrOfYAxisViewsChemical;
    NSMutableArray *arrOfButtonImagesChemical;
    NSMutableArray *arrOfIndexesChemical;
    NSMutableArray *arrOfControlssMainChemical,*arrOfAfterImagesAll,*arrOfEquipmentList,*arrOfImagenameCollewctionView;
    NSString *strWorkOrderId,*strGlobalTechSignImage,*strGlobalCustSignImage,*strGlobalAudio,*strGlobalWorkOrderStatus,*strGlobalWoEquipIdToFetchDynamicData;
    BOOL isCustomerImage,isCustomerNotPrsent,yesEditedSomething;
    
    BOOL noAmount,isWoEquipIdPrsent,isNoServiceJobDescriptions;
    UIDatePicker *pickerDate;
    NSString *strGlobalDateToShow,*strGlobalDatenTime;
    UIView *viewBackGround;
    UIView *viewForDate;
    BOOL isPaymentInfo,isCheckFrontImage,isFromBeforeImage;
    int btnTagCheckImage;
    NSMutableArray *arrOfCheckBackImage,*arrOFEquipmentDynamicDataFromDb,*arrOfAppMethodNameToShowOnClick,*arrOfTargetNameToShowOnClick,*arrOfHeightsForEquipmentsCell,*arrOfMoreOrLess,*arrOfImageCaption,*arrOfImageDescription;
    NSDictionary *dictDeptNameByKey;
    float heightForServiceJobDescriptions;
    
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    BOOL isPreSetSignGlobal;
    
    //Graph Image
    NSMutableArray *arrOfImagenameCollewctionViewGraph,*arrOfImageCaptionGraph,*arrOfImageDescriptionGraph,*arrOfBeforeImageAllGraph;
    
    //termite
    NSMutableDictionary *dictGlobalTermiteTexas;
    NSString *strGlobalBuyersSign,*strGlobalInspectorSign,*strGlobalApplicatorSign,*strGlobalBuyersSignPurchaser,*strGlobalGraphImage,*strGlobalBuyersSign7B,*strCreatedBy,*strDateInspectiondate,*strDate,*strDateTreatmentByInspectionCompany,*strDatePosted,*strDateOfPurchaserOfProperty,*strGraphImageCaption,*strGraphImageDescription,*strDepartmentType;
    
    
    NSMutableArray *arrImageLattitude,*arrImageLongitude,*arrImageGraphLattitude,*arrImageGraphLongitude;
    
    //Nilind
    //Nilind 07 Jun ImageCaption
    // UITextView *txtFieldCaption;
    // UITextView *txtViewImageDescription;
    //UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaptionFor7A,*arrOfImageDescriptionFor7A,*arrOfImageCaptionFor8,*arrOfImageDescriptionFor8;
    NSMutableArray *arrNoImage,*arrImageLattitudeFor7A,*arrImageLongitudeFor7A;
    NSMutableArray *arrNoImageCollectionView2,*arrImageLattitudeFor8,*arrImageLongitudeFor8;
    
    
    //For Florida
    NSMutableDictionary *dictGlobalTermiteFlorida;
    
    NSString *strGlobalSignOfAgent;
    NSString * strDateOfInspectiondate,*strDateOfLicenceeAgent,*strInspectionDate;
    
}

@end

@implementation InvoiceAppointmentView

- (void)viewDidLoad
{
    
    _btnSave.layer.cornerRadius = 10;
    _btnSave.layer.borderWidth = 1;
    _btnSave.layer.borderColor = UIColor.clearColor.CGColor;
    _btnSave.layer.masksToBounds = false;
    _btnSave.layer.shadowColor = UIColor.darkGrayColor.CGColor;
    _btnSave.layer.shadowOffset = CGSizeMake(1, 1);
    _btnSave.layer.shadowOpacity = 0.5;
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    if (isNoServiceJobDescriptions) {
        
        heightForServiceJobDescriptions=0;
        [_viewServiceJobDescriptions setHidden:YES];
        
    } else {
        
        [_viewServiceJobDescriptions setHidden:NO];
        heightForServiceJobDescriptions=_viewServiceJobDescriptions.frame.size.height;
        
    }
    
    //Nilind
    noAmount=NO;
    isCheckFrontImage=YES;
    isNoServiceJobDescriptions=NO;
    isWoEquipIdPrsent=NO;
    btnTagCheckImage=1000;
    arrOfCheckBackImage=[[NSMutableArray alloc]init];
    arrOfEquipmentList=[[NSMutableArray alloc]init];
    arrOFEquipmentDynamicDataFromDb=[[NSMutableArray alloc]init];
    arrOfAppMethodNameToShowOnClick=[[NSMutableArray alloc]init];
    arrOfTargetNameToShowOnClick=[[NSMutableArray alloc]init];
    arrOfMoreOrLess=[[NSMutableArray alloc]init];
    arrOfImageCaption=[[NSMutableArray alloc]init];
    arrOfImageDescription=[[NSMutableArray alloc]init];
    arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
    
    //Graph
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
    arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
    
    //Graph
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    arrImageLattitude=[[NSMutableArray alloc]init];
    arrImageLongitude=[[NSMutableArray alloc]init];
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    //Graph
    _viewGraphImage.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _viewGraphImage.layer.borderWidth=1.0;
    _viewGraphImage.layer.cornerRadius=5.0;
    
    yesEditedSomething=NO;
    
    [super viewDidLoad];
    
    [_buttonImg1 setHidden:YES];
    [_buttonImg2 setHidden:YES];
    [_buttonImg3 setHidden:YES];
    
    //============================================================================
    //============================================================================
    isCustomerNotPrsent=NO;
    
    global = [[Global alloc] init];
    [self getClockStatus];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    arrayOfButtonsMain=[[NSMutableArray alloc]init];
    arrOFImagesName=[[NSMutableArray alloc] init];
    arrOfAfterImagesAll=[[NSMutableArray alloc] init];
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    strWorkOrderId=[defsLead valueForKey:@"LeadId"];
    
    _lbl_AccountNos.text=[defsLead valueForKey:@"lblName"];
    
    NSString *strThirdPartyAccountNo=[defsLead valueForKey:@"lblThirdPartyAccountNo"];
    if (strThirdPartyAccountNo.length>0) {
        _lbl_AccountNos.text=strThirdPartyAccountNo;
    }
    
    [self fetchWorkOrderDetails];
    
    // strDepartmentType=@"Termite";
    if ([strDepartmentType isEqualToString:@"Termite"])
    {
        if ([_strTermiteType isEqualToString:@"florida"])
        {
            [self getDataFrmDBFlorida];
            [self methodSetAllViewsFlorida];
        }
        else
        {
            [self getDataFrmDB];
            [self methodSetAllViews];
        }
        
    }else{
        
        [self setDynamicData];
        
    }
    [self setEditableOrNot];
    _txtView_TechComment.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_TechComment.layer.borderWidth=1.0;
    
    _txtView_BillingAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_BillingAddress.layer.borderWidth=1.0;
    
    _txtView_OfficeNotes.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtView_OfficeNotes.layer.borderWidth=1.0;
    
    _txt_ServiceAddress.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txt_ServiceAddress.layer.borderWidth=1.0;
    
    //Nilind 23 Jan
    
    _txtView_TechComment.layer.cornerRadius=5.0;
    _txtView_BillingAddress.layer.cornerRadius=5.0;
    _txtView_OfficeNotes.layer.cornerRadius=5.0;
    _txt_ServiceAddress.layer.cornerRadius=5.0;
    
    //End
    
    
    // Do any additional setup after loading the view.
    
    for (int j=0; j<_arrAllObjImageDetail.count; j++) {
        
        matchesImageDetail=_arrAllObjImageDetail[j];
        
        NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
        
        if ([woImageType isEqualToString:@"After"]) {
            
            NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
            NSString *userName=[matchesImageDetail valueForKey:@"userName"];
            NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
            NSString *woImageId=[matchesImageDetail valueForKey:@"woImageId"];
            NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
            NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
            NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
            NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
            NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
            NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
            
            
            NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                       companyKey.length==0 ? @"" : companyKey,
                                       userName.length==0 ? @"" : userName,
                                       workorderId.length==0 ? @"" : workorderId,
                                       woImageId.length==0 ? @"" : woImageId,
                                       woImagePath.length==0 ? @"" : woImagePath,
                                       woImageType.length==0 ? @"" : woImageType,
                                       createdDate.length==0 ? @"" : createdDate,
                                       createdBy.length==0 ? @"" : createdBy,
                                       modifiedDate.length==0 ? @"" : modifiedDate,
                                       modifiedBy.length==0 ? @"" : modifiedBy,
                                       lattitude.length==0 ? @"" : lattitude,
                                       longitude.length==0 ? @"" : longitude,nil];
            NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                     @"companyKey",
                                     @"userName",
                                     @"workorderId",
                                     @"woImageId",
                                     @"woImagePath",
                                     @"woImageType",
                                     @"createdDate",
                                     @"createdBy",
                                     @"modifiedDate",
                                     @"modifiedBy",
                                     @"latitude",
                                     @"longitude",nil];
            
            NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
            
            [arrOfAfterImagesAll addObject:dict_ToSendLeadInfo];
            
            NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
            
            if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            } else {
                
                [arrOfImageCaption addObject:strImageCaption];
                
            }
            
            
            NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
            
            if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            } else {
                
                [arrOfImageDescription addObject:strImageDescription];
                
            }
            
            //Lat long
            
            NSString *strLat,*strLong;
            strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
            strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
            if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                
                [arrImageLattitude addObject:@""];
                
            } else {
                
                [arrImageLattitude addObject:strLat];
                
            }
            if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                
                [arrImageLongitude addObject:@""];
                
            } else {
                
                [arrImageLongitude addObject:strLong];
                
            }
            
            
            
        }
    }
    [self defaultViewSettingForAmount];
    //Nilind  23 Feb
    
    NSString* checkFrontImagePath=[_paymentInfoDetail valueForKey:@"checkFrontImagePath"];
    if (checkFrontImagePath.length>0) {
        [arrOFImagesName addObject:checkFrontImagePath];
    }
    NSString* checkBackImagePath=[_paymentInfoDetail valueForKey:@"checkBackImagePath"];
    if (checkBackImagePath.length>0) {
        [arrOfCheckBackImage addObject:checkBackImagePath];
    }
    
    //End
    
    
    //    UITapGestureRecognizer *singleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedScrollView)];
    //    singleTap3.numberOfTapsRequired = 1;
    //    [_scrollVieww setUserInteractionEnabled:YES];
    //    [_scrollVieww addGestureRecognizer:singleTap3];
    
    
    _textView_ServiceJobDescriptions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _textView_ServiceJobDescriptions.layer.borderWidth=1.0;
    _textView_ServiceJobDescriptions.layer.cornerRadius=5.0;
    
    
    _txtViewTermsnConditions.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewTermsnConditions.layer.borderColor=[[UIColor whiteColor] CGColor];
    _txtViewTermsnConditions.layer.borderWidth=1.0;
    _txtViewTermsnConditions.layer.cornerRadius=5.0;
    
    //[self fetchWoEquipmentFromDB];
    
    //Nilind 06 Dec Termite
    
    arrNoImage=[[NSMutableArray alloc]init];
    arrNoImageCollectionView2=[[NSMutableArray alloc]init];
    arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
    arrOfImageDescriptionFor7A=[[NSMutableArray alloc]init];
    arrOfImageCaptionFor8=[[NSMutableArray alloc]init];
    arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
    //arrImagePath=[[NSMutableArray alloc]init];
    arrImageLattitudeFor7A=[[NSMutableArray alloc]init];
    arrImageLongitudeFor7A=[[NSMutableArray alloc]init];
    arrImageLattitudeFor8=[[NSMutableArray alloc]init];
    arrImageLongitudeFor8=[[NSMutableArray alloc]init];
    [self fetchImageDetailFromDataBaseForSectionView7A_8];
    
    //End
    
    NSUserDefaults *defLbl = [NSUserDefaults standardUserDefaults];
    _lblTitleHeader.text = [defLbl valueForKey:@"lblName"];
    
}

-(void)tapDetectedScrollView{
    
    [_txtView_TechComment resignFirstResponder];
    [_txtAmount resignFirstResponder];
    [_txtCheckNo resignFirstResponder];
    [_txtView_OfficeNotes resignFirstResponder];
    [_txt_DrivingLicenseNo resignFirstResponder];
    [_txtAmountSingle resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [txtFieldCaption resignFirstResponder];
    [txtViewImageDescription resignFirstResponder];
    [_txtView_TechComment resignFirstResponder];
    [_txtAmount resignFirstResponder];
    [_txtCheckNo resignFirstResponder];
    [_txtView_OfficeNotes resignFirstResponder];
    [_txt_DrivingLicenseNo resignFirstResponder];
    [_txtAmountSingle resignFirstResponder];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    
    
    
    //Graph
    [self fetchGraphImageDetailFromDataBase];
    
    [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
    
    //    if (isNoServiceJobDescriptions) {
    //
    //        heightForServiceJobDescriptions=-50;
    //        [_viewServiceJobDescriptions setHidden:YES];
    //
    //    } else {
    //
    //        heightForServiceJobDescriptions=_viewServiceJobDescriptions.frame.size.height;
    //
    //    }
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        
        /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        
        
        ////Akshay 22 may 2019
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        ////
        
    } else {
        
        NSString *strIns,*strCust;
        //    BOOL chkFromSign;
        //    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        //    chkFromSign=[defs boolForKey:@"fromInspectorSign"];
        //    if (chkFromSign==YES)
        //    {
        //        UIImage *imageSign;
        //        imageSign=[self loadImage];
        //        _imgViewInspectorSign.image=imageSign;
        //    }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        strIns=[defs valueForKey:@"fromInspectorSignService"];
        strCust=[defs valueForKey:@"fromCustomerSignService"];
        
        //    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        
        
        
        if ([strIns isEqualToString:@"fromInspectorSignService"])
        {
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePath"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            _imgView_TechSign.image=imageSign;
            strGlobalTechSignImage=strImageName;
            [defs setValue:@"abc" forKey:@"fromInspectorSignService"];
            [defs setValue:@"abc" forKey:@"imagePath"];
            [defs synchronize];
        }
        
        if ([strCust isEqualToString:@"fromCustomerSignService"])
        {
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePath"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            strGlobalCustSignImage=strImageName;
            _imgView_CustomerSign.image=imageSign;
            [defs setValue:@"xyz" forKey:@"fromCustomerSignService"];
            [defs setValue:@"xyz" forKey:@"imagePath"];
            [defs synchronize];
            
        }
        
        BOOL yesAudioAvailable=[defs boolForKey:@"yesAudio"];
        if (yesAudioAvailable) {
            
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            strGlobalAudio=[defs valueForKey:@"AudioNameService"];
            
            if (strGlobalAudio.length>0) {
                [_workOrderDetail setValue:strGlobalAudio forKey:@"audioFilePath"];
            }else{
                [_workOrderDetail setValue:@"" forKey:@"audioFilePath"];
            }
            NSError *error;
            [context save:&error];
            
        }
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfAfterImagesAll.count; k++) {
                
                NSDictionary *dictdat=arrOfAfterImagesAll[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrOfAfterImagesAll[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                
                // NSString *strImageName=[dictdat valueForKey:@"woImagePath"];
                
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrOfAfterImagesAll[k]];
                        
                    }
                }
                //            if (yesFoundName) {
                //
                //                [arrTempBeforeImage addObject:arrOfBeforeImageAll[k]];
                //
                //            }
            }
            if (!(arrTempBeforeImage.count==0)) {
                // arrOfAfterImagesAll=nil;
                // arrOfAfterImagesAll=[[NSMutableArray alloc]init];
                [arrOfAfterImagesAll removeObjectsInArray:arrTempBeforeImage];
            }
            
            yesEditedSomething=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew synchronize];
            
            //[self saveImageToCoreData];;
            
        }
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageCaptionGraph=[defsSS boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            
            yesEditedSomething=YES;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            
            //[self saveImageToCoreData];;
            
        }
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageDescriptionGraph=[defsSS boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            
            yesEditedSomething=YES;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            
            //[self saveImageToCoreData];;
            
        }
        
        //Change in image Captions
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaption=nil;
            arrOfImageCaption=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaption addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
            //[self saveImageToCoreData];;
            
        }
        
        //Change in image Description
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescription=nil;
            arrOfImageDescription=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescription addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
            //[self saveImageToCoreData];;
            
        }
        
        [self downloadingImagesThumbNailCheck];
        
        //Graph
        [self downloadGraphImage];
        
        NSString *strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"isCustomerNotPresent"]];
        if (isCustomerNotPrsent) {
            
            isCustomerNotPrsent=YES;
            
            [_btnCustNotPrsent setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            _const_ViewCust_Top.constant=0;
            _const_ViewCustSign_H.constant=0;
            
            _imgView_CustomerSign.image=[UIImage imageNamed:@"NoImage.jpg"];
            
            [_lbl_CustomerSign setHidden:YES];
            [_btn_CustSignn setHidden:YES];
            [_imgView_CustomerSign setHidden:YES];
            if (_const_AfterImage_BtoImgView.constant==-120) {
                
                _const_SavenContinue_B.constant=275;
                
            } else {
                
                _const_SavenContinue_B.constant=175;
                
            }
            
            [_viewCustomerSignature setHidden:YES];
            
            _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, 0);
            
            
        } else {
            
            isCustomerNotPrsent=NO;
            
            [_btnCustNotPrsent setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            _const_ViewCust_Top.constant=1;
            _const_ViewCustSign_H.constant=138;
            
            [_lbl_CustomerSign setHidden:NO];
            [_btn_CustSignn setHidden:NO];
            [_imgView_CustomerSign setHidden:NO];
            if (_const_AfterImage_BtoImgView.constant==-120) {
                _const_SavenContinue_B.constant=175;
            }else{
                _const_SavenContinue_B.constant=61;
            }
            
            
            [_viewCustomerSignature setHidden:NO];
            
            _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
            
        }
        _txt_Customer.inputView=[[UIView alloc]init];
        _txt_Customer.delegate = self;
        _txt_PrimaryEmail.inputView=[[UIView alloc]init];
        _txt_PrimaryEmail.delegate = self;
        
    }
    
}

-(void)methodAfterSomeTime{
    
    _viewTermsnConditions.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height-160, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewPaymentInfo.frame.size.height+_viewPaymentInfo.frame.origin.y+200+_viewTermsnConditions.frame.size.height)];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
#pragma mark- Actions Buttons
//============================================================================

- (IBAction)action_BeforeImageNew:(id)sender{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    GlobalImageVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objByProductVC.strHeaderTitle = @"After";
    objByProductVC.strWoId = strWorkOrderId;
    objByProductVC.strModuleType = @"ServicePestFlow";
    if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"])
    {
        objByProductVC.strWoStatus = @"Completed";
    }else{
        objByProductVC.strWoStatus = @"InComplete";
    }
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}
- (IBAction)action_GraphImageNew:(id)sender{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    GlobalImageVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GlobalImageVC"];
    objByProductVC.strHeaderTitle = @"Graph";
    objByProductVC.strWoId = strWorkOrderId;
    objByProductVC.strModuleType = @"ServicePestFlow";
    if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"])
    {
        objByProductVC.strWoStatus = @"Completed";
    }else{
        objByProductVC.strWoStatus = @"InComplete";
    }
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
}


- (IBAction)action_Back:(id)sender {
    
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    [defsBack setBool:YES forKey:@"fromInvoiceAppointment"];
    [defsBack synchronize];
    if (LandingSong.isPlaying)
    {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        [LandingSong stop];
    }
    
    [self saveCommentDetailsOnBack];
    
    if ([strDepartmentType isEqualToString:@"Termite"])
    {
        
        if([_strTermiteType isEqualToString:@"florida"])
        {
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[TermiteInspectionFloridaViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            TermiteInspectionFloridaViewController *myController = (TermiteInspectionFloridaViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
        }
        else
        {
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[TermiteInspectionTexasViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            TermiteInspectionTexasViewController *myController = (TermiteInspectionTexasViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
        }
        
    }else{
        
        
        NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
        
        BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
        isEquipEnabled=NO;
        if (isEquipEnabled) {
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[EquipmentsViewController class]]) {
                    index=k1;
                    //break;
                }
            }
            EquipmentsViewController *myController = (EquipmentsViewController *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
        }else{  // ServiceDetailAppointmentView
            
            
            int index = 0;
            NSArray *arrstack=self.navigationController.viewControllers;
            for (int k1=0; k1<arrstack.count; k1++) {
                if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailAppointmentView class]]) {
                    index=k1;
                    //break;
                }
            }
            ServiceDetailAppointmentView *myController = (ServiceDetailAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
            [self.navigationController popToViewController:myController animated:NO];
            
            /*
            if ([strWorkOrderId isEqualToString: @""]) {

                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailAppointmentView class]]) {
                        index=k1;
                        //break;
                    }
                }
                ServiceDetailAppointmentView *myController = (ServiceDetailAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
            }else{
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailAppointmentView class]]) {
                        index=k1;
                        //break;
                    }
                }
                ServiceDetailAppointmentView *myController = (ServiceDetailAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
                
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                ServiceDetailVC *myController = (ServiceDetailVC *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
             
                
            }
            */
            
//            int index = 0;
//            NSArray *arrstack=self.navigationController.viewControllers;
//            for (int k1=0; k1<arrstack.count; k1++) {
//                if ([[arrstack objectAtIndex:k1] isKindOfClass:[ServiceDetailVC class]]) {
//                    index=k1;
//                    //break;
//                }
//            }
//            ServiceDetailVC *myController = (ServiceDetailVC *)[self.navigationController.viewControllers objectAtIndex:index];
//            // myController.typeFromBack=_lbl_LeadInfo_Status.text;
//            [self.navigationController popToViewController:myController animated:NO];
            
        }
    }
    
}
- (IBAction)action_FinalSaveContinue:(id)sender {
    
    if (LandingSong.isPlaying)
    {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        [LandingSong stop];
    }
    /*if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
     {
     
     
     
     }
     else
     {
     //[self saveImageToCoreData];;
     [self finalSavePaymentInfo];
     [self savingWorkOrderDetails];
     }
     */ //orignal
#pragma mark- BY NILIND
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }
    else
    {
        //Nilind
        
        if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"])
        {
            
            
        }
        else
            
        {
            
            //START new change for email valid overall
            BOOL isEmailValid;
            isEmailValid=YES;
            
            
            //END new change for email valid overall
            
            
            
            if (noAmount)
            {
                
                if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
                    
                    if (_txtCheckNo.text.length==0) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                        
                    }
                    //                    else if (_txt_DrivingLicenseNo.text.length==0) {
                    //
                    //                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                        [alert show];
                    //
                    //                    }
                    //                    else if (_btnCheckExpDate.titleLabel.text.length==0) {
                    //
                    //                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                        [alert show];
                    //
                    //                    }else if (arrOFImagesName.count==0) {
                    //
                    //                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                        [alert show];
                    //
                    //                    }else if (arrOfCheckBackImage.count==0) {
                    //
                    //                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    //                        [alert show];
                    //
                    //                    }
                    else{
                        
                        
                        if (yesEditedSomething) {
                            NSLog(@"Yes Something Edited and Finaly Saved");
                            [self updateModifyDate];
                            
                        }
                        //[self saveImageToCoreData];;
                        [self savingWorkOrderDetails];
                        
                        [self finalSavePaymentInfo];
                        
                    }
                }
                else
                {
                    
                    
                    if (yesEditedSomething) {
                        NSLog(@"Yes Something Edited and Finaly Saved");
                        [self updateModifyDate];
                        
                        
                    }
                    //[self saveImageToCoreData];;
                    [self savingWorkOrderDetails];
                    
                    [self finalSavePaymentInfo];
                    
                }
            } else {
                
                BOOL yesZero;
                yesZero=NO;
                
                NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"123456789"];
                
                if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
                    NSRange range = [_txtAmount.text rangeOfCharacterFromSet:cset];
                    if (range.location == NSNotFound) {
                        // no ( or ) in the string
                        yesZero=YES;
                    } else {
                        // ( or ) are present
                        yesZero=NO;
                    }
                }else{
                    
                    NSRange range1 = [_txtAmountSingle.text rangeOfCharacterFromSet:cset];
                    if (range1.location == NSNotFound) {
                        // no ( or ) in the string
                        yesZero=YES;
                    } else {
                        // ( or ) are present
                        yesZero=NO;
                    }
                    
                }
                
                if ((_txtAmount.text.length==0) && (_txtAmountSingle.text.length==0)) {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter amount to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }else if (yesZero){
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter non zero amount to continue" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                else {
                    
                    if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
                        
                        
                        
                        if (_txtCheckNo.text.length==0) {
                            
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                            [alert show];
                            
                        }
                        //                        else if (_txt_DrivingLicenseNo.text.length==0) {
                        //
                        //                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        //                            [alert show];
                        //
                        //                        }
                        //                        else if (_btnCheckExpDate.titleLabel.text.length==0) {
                        //
                        //                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        //                            [alert show];
                        //
                        //                        }else if (arrOFImagesName.count==0) {
                        //
                        //                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        //                            [alert show];
                        //
                        //                        }else if (arrOfCheckBackImage.count==0) {
                        //
                        //                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        //                            [alert show];
                        //
                        //                        }
                        else{
                            
                            if (yesEditedSomething) {
                                NSLog(@"Yes Something Edited and Finaly Saved");
                                [self updateModifyDate];
                                
                                
                            }
                            //[self saveImageToCoreData];;
                            [self finalSavePaymentInfo];
                            [self savingWorkOrderDetails];
                            
                        }
                    }
                    else
                    {
                        
                        if (yesEditedSomething)
                        {
                            NSLog(@"Yes Something Edited and Finaly Saved");
                            [self updateModifyDate];
                            
                            
                        }
                        //[self saveImageToCoreData];;
                        [self finalSavePaymentInfo];
                        [self savingWorkOrderDetails];
                        
                        
                    }
                }
                
            }
            
        }
        
        
        //End
#pragma mark- temp comment
        //  //[self saveImageToCoreData];;
        // [self finalSavePaymentInfo];
        // [self savingWorkOrderDetails];
        
    }
}

- (IBAction)action_TechSign:(id)sender {
    if (LandingSong.isPlaying)
    {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        [LandingSong stop];
    }
    
    isCustomerImage=NO;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromTechService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    ServiceSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewController"];
    // objSignViewController.strType=@"TechService";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
}

- (IBAction)action_CustomerSign:(id)sender {
    if (LandingSong.isPlaying)
    {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        [LandingSong stop];
    }
    
    if (isCustomerNotPrsent) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please deselect Customer Not Present" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    } else {
        
        isCustomerImage=YES;
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"fromCustService" forKey:@"signatureType"];
        [defs synchronize];
        
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        ServiceSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"ServiceSignViewController"];
        // objSignViewController.strType=@"CustService";
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        
    }
}
- (IBAction)action_CustNotPrsent:(id)sender {
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [_btnCustNotPrsent imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        isCustomerNotPrsent=YES;
        
        [_btnCustNotPrsent setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        _const_ViewCust_Top.constant=0;
        _const_ViewCustSign_H.constant=0;
        
        _imgView_CustomerSign.image=[UIImage imageNamed:@"NoImage.jpg"];
        
        [_lbl_CustomerSign setHidden:YES];
        [_btn_CustSignn setHidden:YES];
        [_imgView_CustomerSign setHidden:YES];
        if (_const_AfterImage_BtoImgView.constant==-120) {
            
            _const_SavenContinue_B.constant=275;
            
        } else {
            
            _const_SavenContinue_B.constant=175;
            
        }
        
        
        _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
        
        
        _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, 0);
        
        [_viewCustomerSignature setHidden:YES];
        
        _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
        
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
        
    }
    else{
        
        
        isCustomerNotPrsent=NO;
        
        [_btnCustNotPrsent setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        
        _const_ViewCust_Top.constant=1;
        _const_ViewCustSign_H.constant=138;
        
        [_lbl_CustomerSign setHidden:NO];
        [_btn_CustSignn setHidden:NO];
        [_imgView_CustomerSign setHidden:NO];
        if (_const_AfterImage_BtoImgView.constant==-120) {
            _const_SavenContinue_B.constant=175;
        }else{
            _const_SavenContinue_B.constant=61;
        }
        
        _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
        
        
        [_viewCustomerSignature setHidden:NO];
        
        _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
        
        
        _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
        
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
        
    }
}

- (IBAction)action_RecordAudio:(id)sender {
    
    LandingSong=nil;
    if (LandingSong.isPlaying)
    {
        
    }
    else
    {
        [LandingSong stop];
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isfirstTimeAudio=[defs boolForKey:@"firstAudio"];
    
    if (isfirstTimeAudio) {
        
        [defs setBool:NO forKey:@"firstAudio"];
        [defs synchronize];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        RecordAudioView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
        objByProductVC.strFromWhere=@"ServiceInvoice";
        [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
        
        
    } else {
        
        BOOL isAudioPermissionAvailable=[global isAudioPermissionAvailable];
        
        if (isAudioPermissionAvailable) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            RecordAudioView
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
            objByProductVC.strFromWhere=@"ServiceInvoice";
            [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
            
            
        }else{
            
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:AlertAudioVideoPermission//@"Audio Permission not allowed.Please go to settings and allow"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                      
                                      
                                  }];
            [alert addAction:yes];
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                     if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                         [[UIApplication sharedApplication] openURL:url];
                                     } else {
                                         
                                         //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         //                                     [alert show];
                                         
                                     }
                                     
                                 }];
            [alert addAction:no];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}
- (IBAction)action_Play:(id)sender {
    
    if ([_btnPlayAudio.titleLabel.text isEqualToString:@"Play"]) {
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        BOOL yesAudio=[defs boolForKey:@"yesAudio"];
        if (yesAudio) {
            [_btnPlayAudio setTitle:@"Stop" forState:UIControlStateNormal];
            _lblAudioStatus.text=@"Audio Available";
            //[defs setBool:NO forKey:@"yesAudio"];
            [defs synchronize];
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            NSString *strAudioName=[defs valueForKey:@"AudioNameService"];
            [self playLandingAudio:strAudioName];
        }else{
            _lblAudioStatus.text=@"Audio Not Available";
            [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        }
    } else {
        [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
        // [LandingSong stop];
        LandingSong=nil;
        if (LandingSong.isPlaying)
        {
            
        }
        else
        {
            [LandingSong stop];
        }
    }
}


- (IBAction)action_SelectAudioOptions:(id)sender {
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Choose Options"
                               message:@""
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Record-Audio" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
                                  
                              }else{
                                  
                                  LandingSong=nil;
                                  if (LandingSong.isPlaying)
                                  {
                                      
                                  }
                                  else
                                  {
                                      [LandingSong stop];
                                  }
                                  
                                  NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                  
                                  BOOL isfirstTimeAudio=[defs boolForKey:@"firstAudio"];
                                  
                                  if (isfirstTimeAudio) {
                                      
                                      [defs setBool:NO forKey:@"firstAudio"];
                                      [defs synchronize];
                                      
                                      UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                               bundle: nil];
                                      RecordAudioView
                                      *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
                                      objByProductVC.strFromWhere=@"ServiceInvoice";
                                      [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
                                      
                                      
                                  } else {
                                      
                                      BOOL isAudioPermissionAvailable=[global isAudioPermissionAvailable];
                                      
                                      if (isAudioPermissionAvailable) {
                                          
                                          UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                                   bundle: nil];
                                          RecordAudioView
                                          *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"RecordAudioView"];
                                          objByProductVC.strFromWhere=@"ServiceInvoice";
                                          [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
                                          
                                          
                                      }else{
                                          
                                          UIAlertController *alert= [UIAlertController
                                                                     alertControllerWithTitle:@"Alert"
                                                                     message:AlertAudioVideoPermission//@"Audio Permission not allowed.Please go to settings and allow"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                          
                                          UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction * action)
                                                                {
                                                                    
                                                                    
                                                                    
                                                                }];
                                          [alert addAction:yes];
                                          UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * action)
                                                               {
                                                                   
                                                                   if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                                       NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                       [[UIApplication sharedApplication] openURL:url];
                                                                   } else {
                                                                       
                                                                       //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                                       //                                     [alert show];
                                                                       
                                                                   }
                                                                   
                                                               }];
                                          [alert addAction:no];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                  }
                              }
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Play-Audio" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if ([_btnPlayAudio.titleLabel.text isEqualToString:@"Play"]) {
                                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                 BOOL yesAudio=[defs boolForKey:@"yesAudio"];
                                 if (yesAudio) {
                                     [_btnPlayAudio setTitle:@"Stop" forState:UIControlStateNormal];
                                     _lblAudioStatus.text=@"Audio Available";
                                     //[defs setBool:NO forKey:@"yesAudio"];
                                     [defs synchronize];
                                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                     NSString *strAudioName=[defs valueForKey:@"AudioNameService"];
                                     [self playLandingAudio:strAudioName];
                                 }else{
                                     _lblAudioStatus.text=@"Audio Not Available";
                                     [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
                                 }
                             } else {
                                 [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
                                 // [LandingSong stop];
                                 LandingSong=nil;
                                 if (LandingSong.isPlaying)
                                 {
                                     
                                 }
                                 else
                                 {
                                     [LandingSong stop];
                                 }
                             }
                             
                         }];
    [alert addAction:no];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 
                             }];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}



- (IBAction)action_History:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Task-Activity"
                                                             bundle: nil];
    ServiceHistory_iPhoneVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistory_iPhoneVC"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
    /*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceHistoryMechanical
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceHistoryMechanicaliPhone"];
    [self.navigationController pushViewController:objByProductVC animated:NO];*/
    
}

//============================================================================
#pragma mark- ------------CORE DATA IMAGE SAVE------------------
//============================================================================
-(void)saveImageToCoreData
{
    //[self deleteAfterImagesBeforeSaving];
    
    //Graph
    //[self deleteGraphImagesBeforeSaving];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrOfAfterImagesAll.count; k++)
    {
        
        if ([arrOfAfterImagesAll[k] isKindOfClass:[NSString class]]) {
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfAfterImagesAll objectAtIndex:k]];
            objImageDetail.woImageType=@"After";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            
            NSError *error1;
            [context save:&error1];
            
        }else{
            
            // Image Detail Entity
            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
            
            ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
            
            NSDictionary *dictData=[arrOfAfterImagesAll objectAtIndex:k];
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"After";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaption objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescription objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            
            NSError *error1;
            [context save:&error1];
            
            
        }
    }
    
    
    //Graph
    for (int k=0; k<arrOfImagenameCollewctionViewGraph.count; k++)
    {
        // Image Detail Entity
        entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
        
        ImageDetailsServiceAuto *objImageDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
        
        if ([arrOfImagenameCollewctionViewGraph[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrOfImagenameCollewctionViewGraph objectAtIndex:k]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            NSDictionary *dictData=[arrOfImagenameCollewctionViewGraph objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"Graph";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionGraph objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionGraph objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageGraphLattitude objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageGraphLongitude objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    //........................................................................
}

-(void)deleteAfterImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"After"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

/*- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 {
 yesEditedSomething=YES;
 NSLog(@"Yes Edited Something In Db");
 NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
 [formatterDate setDateFormat:@"MMddyyyy"];
 [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"HHmmss"];
 [formatter setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strTime = [formatter stringFromDate:[NSDate date]];
 NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
 
 //    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
 //    [self saveImage:image :strImageNamess];
 //    if (isCustomerImage) {
 //        strGlobalCustSignImage=strImageNamess;
 //        [arrOFImagesName addObject:strImageNamess];
 //    } else {
 //        strGlobalTechSignImage=strImageNamess;
 //        [arrOFImagesName addObject:strImageNamess];
 //    }
 
 [arrOfAfterImagesAll addObject:strImageNamess];
 UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
 
 [self resizeImage:chosenImage :strImageNamess];
 
 // [self saveImage:chosenImage :strImageNamess];
 
 [picker dismissViewControllerAnimated:YES completion:NULL];
 
 
 }*/

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    //    if (screenWidth == 320)
    //    {
    //        actualWidth =1000;
    //        actualHeight =1000;
    //    }
    //    else if (screenWidth == 375)
    //    {
    //        actualWidth =365;
    //        actualHeight =365;
    //    }
    //    else if (screenWidth == 414)
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //    else
    //    {
    //        actualWidth =404;
    //        actualHeight =404;
    //    }
    //
    //    actualWidth =1000;
    //    actualHeight =1000;
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //  NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveImage: (UIImage*)image :(NSString*)imageName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSData* data = UIImagePNGRepresentation(image);
    [data writeToFile:path atomically:YES];
}

- (IBAction)action_AfterImage:(id)sender
{
    btnTagCheckImage=102;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
-(void)openGalleryy{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}
-(void)openCamera{
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    [self presentViewController:imagePickController animated:YES completion:nil];
    
}

//============================================================================
#pragma mark- Action Sheet and Image Picker
//============================================================================
/*
 -(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
 {
 if (buttonIndex ==10)
 {
 NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
 
 
 NSArray *arrOfImagesDetail=arrOfAfterImagesAll;
 
 arrOfBeforeImages=[[NSMutableArray alloc]init];
 
 for (int k=0; k<arrOfImagesDetail.count; k++) {
 
 NSDictionary *dictData=arrOfImagesDetail[k];
 
 if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
 
 [arrOfBeforeImages addObject:dictData];
 
 }else{
 [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
 }
 }
 
 arrOfImagesDetail=arrOfBeforeImages;
 
 if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
 [global AlertMethod:Info :NoBeforeImg];
 }else if (arrOfImagesDetail.count==0){
 [global AlertMethod:Info :NoBeforeImg];
 }
 else {
 NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
 for (int k=0; k<arrOfImagesDetail.count; k++) {
 if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
 NSDictionary *dict=arrOfImagesDetail[k];
 [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
 }else{
 [arrOfImagess addObject:arrOfImagesDetail[k]];
 }
 }
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
 bundle: nil];
 ImagePreviewGeneralInfoAppointmentView
 *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
 objByProductVC.arrOfImages=arrOfImagess;
 objByProductVC.indexOfImage=@"7";
 objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
 //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
 [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
 
 }
 }
 else if (buttonIndex == 0)
 {
 
 if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
 {
 }
 else
 {
 if (arrOfAfterImagesAll.count<3)
 {
 
 NSLog(@"The CApture Image.");
 
 BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
 
 if (isCameraPermissionAvailable) {
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 
 }else{
 
 UIAlertController *alert= [UIAlertController
 alertControllerWithTitle:@"Alert"
 message:@"Camera Permission not allowed.Please go to settings and allow"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 
 
 }];
 [alert addAction:yes];
 UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
 [[UIApplication sharedApplication] openURL:url];
 } else {
 
 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 //                                     [alert show];
 
 }
 
 }];
 [alert addAction:no];
 [self presentViewController:alert animated:YES completion:nil];
 }
 }
 else
 {
 
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
 [alert show];
 
 }
 }
 }
 else if (buttonIndex == 1)
 {
 if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
 }else{
 
 if (arrOfAfterImagesAll.count<3)
 {
 
 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
 
 if (isCameraPermissionAvailable) {
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 
 }else{
 
 UIAlertController *alert= [UIAlertController
 alertControllerWithTitle:@"Alert"
 message:@"Gallery Permission not allowed.Please go to settings and allow"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 
 
 }];
 [alert addAction:yes];
 UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
 [[UIApplication sharedApplication] openURL:url];
 } else {
 
 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 //                                     [alert show];
 
 }
 
 }];
 [alert addAction:no];
 [self presentViewController:alert animated:YES completion:nil];
 }
 }else{
 
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
 [alert show];
 
 }
 }
 }
 }
 */
- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

//============================================================================
#pragma mark- Play Audio
//============================================================================

-(void)playLandingAudio :(NSString *)strAudioName
{
    
    strAudioName=[global strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    
    [global playAudio:path :self.view];
    
    /*
     // NSString *sounfiles1=[[NSBundle mainBundle]pathForResource:@"Flag_Intro Audio" ofType:@"mp3"];
     NSURL *url=[NSURL fileURLWithPath:path];
     LandingSong=[[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
     LandingSong.delegate = self;
     [LandingSong play];
     if (LandingSong.isPlaying)
     {
     
     }
     else
     {
     [LandingSong stop];
     }
     
     */
    
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    [_btnPlayAudio setTitle:@"Play" forState:UIControlStateNormal];
    
}



//============================================================================
#pragma mark- Load Image
//============================================================================

- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

//============================================================================
#pragma mark- setDynamicData Method
//============================================================================

-(void)setDynamicData
{
    
    BOOL isMainValueThere;
    
    isMainValueThere=NO;
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(10, 0, self.view.frame.size.width-20, _view_CustomerInfo.frame.size.height);
    frameFor_view_CustomerInfo.origin.x=10;
    frameFor_view_CustomerInfo.origin.y=0;
    
    [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
    
    [_scrollVieww addSubview:_view_CustomerInfo];
    
    
    // _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.frame.size.height+_view_CustomerInfo.frame.size.height);
    
    //[_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.contentSize.height+_view_CustomerInfo.frame.size.height)];
    
    //============================================================================
    //============================================================================
    
    
    CGFloat scrollViewHeight=0.0;
    
    NSDictionary *dictMain=_dictJsonDynamicForm;
    
    UIButton *BtnMainView;
    
    if (!MainViewForm.frame.size.height) {
        BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, _view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height, [UIScreen mainScreen].bounds.size.width, 42)];
    }
    else{
        BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, MainViewForm.frame.origin.y+MainViewForm.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 42)];
    }
    //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
    BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
    
    [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];
    
    
#pragma mark- -------------------BY NILIND 23 FEB--------------------------
    
    //Nilind 23 Feb
    
    // [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];//dictDeptNameByKey
    NSString *strDept=[dictDeptNameByKey valueForKey:[dictMain valueForKey:@"DepartmentSysName"]];
    if ([strDept isEqual:nil]||[strDept isEqualToString:@""])
    {
        strDept=@"No Department Available";
    }
    [BtnMainView setTitle:strDept forState:UIControlStateNormal];
    //End
    
    
    // UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
    // imgView.image=[UIImage imageNamed:@"minus_icon.png"];
    
    //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
    //  [BtnMainView addSubview:imgView];
    
    [_scrollVieww addSubview:BtnMainView];
    
    [arrayOfButtonsMain addObject:BtnMainView];
    
    // Form Creation
    MainViewForm=[[UIView alloc]init];
    
    MainViewForm.backgroundColor=[UIColor clearColor];
    
    MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 1000);
    
    [_scrollVieww addSubview:MainViewForm];
    
    NSArray *arrSections=[dictMain valueForKey:@"Sections"];
    
    
    int yAxisViewFormSections=0.0;
    int heightVIewFormSections=0.0;
    int heightMainViewFormSections=0.0;
    
    
    for (int j=0; j<arrSections.count; j++)
    {
        NSDictionary *dictSection=[arrSections objectAtIndex:j];
        NSArray *arrControls=[dictSection valueForKey:@"Controls"];
        
        // Section Creation Start
        heightVIewFormSections=0.0;
        ViewFormSections=[[UIView alloc]init];
        ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width, 400);
        
        ViewFormSections.backgroundColor=[UIColor whiteColor];
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
        lblTitleSection.textAlignment=NSTextAlignmentCenter;
        [lblTitleSection setAdjustsFontSizeToFitWidth:YES];
        [ViewFormSections addSubview:lblTitleSection];
        
        ViewFormSections.layer.borderWidth = 1.5f;
        ViewFormSections.layer.cornerRadius = 4.0f;
        ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
        
        [MainViewForm addSubview:ViewFormSections];
        
        CGFloat xPositionOfControls= 0.0;
        CGFloat yPositionOfControls = 0.0;
        CGFloat heightOfControls= 0.0;
        CGFloat widthOfControls= 0.0;
        
        BOOL isValuePrsent;
        isValuePrsent=NO;
        
        int viewHeight = 0;
        
        NSMutableArray *arrOfValuepresent=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrControls.count; k++)
        {
            NSDictionary *dictControls=[arrControls objectAtIndex:k];
            UILabel *lblTitleControl=[[UILabel alloc]init];
            lblTitleControl.backgroundColor=[UIColor blackColor];
            lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width, 30);
            lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
            lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
            lblTitleControl.textAlignment=NSTextAlignmentCenter;
            // [ViewFormSections addSubview:lblTitleControl];
            
            NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
            if (strIndexOfValue.length==0) {
                // isValuePrsent=NO;
            } else {
                
                
                
                isValuePrsent=YES;
                NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                
                if (!yPositionOfControls) {
                    yPositionOfControls=40;
                }
                
                UIView *view = [[UIView alloc] init];
                CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                [view setFrame:buttonFrame];
                view.backgroundColor=[UIColor whiteColor];
                
                CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                [lbl setAdjustsFontSizeToFitWidth:YES];
                [lbl setNumberOfLines:100000];
                lbl.font = [UIFont systemFontOfSize:15];
                lbl.text =[dictControls valueForKey:@"Label"];
                [view addSubview:lbl];
                
                NSString *heightttt=[NSString stringWithFormat:@"%f",s.height];
                
                // [arrOfValuepresent addObject:heightttt];
                
                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-textbox"]) {
                    
                    //     CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                    UITextView *lblItem = [[UITextView alloc] init];
                    lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                    [lblItem setFont:[UIFont systemFontOfSize:15]];
                    //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                    lblItem.text =[dictControls valueForKey:@"Value"];
                    lblItem.editable=NO;
                    [view addSubview:lblItem];
                    //  view.backgroundColor=[UIColor redColor];
                    
                    int tempHeight=lblItem.frame.size.height;
                    
                    viewHeight=viewHeight+tempHeight;
                    
                    //[view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lblItem.frame.size.height+lbl.frame.size.height+15)];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                    
                    heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                    [arrOfValuepresent addObject:heightttt];
                    
                }
                else if([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                    
                    NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                    NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                    NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                    
                    for (int k=0; k<arrOfValues.count; k++) {
                        
                        for (int l=0; l<arrOfOptions.count; l++) {
                            
                            NSDictionary *dictData=arrOfOptions[l];
                            NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                            
                            if ([strValue isEqualToString:arrOfValues[k]]) {
                                //Name
                                if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-dropdown"]) {
                                    [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                    
                                } else {
                                    [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                    
                                }
                            }
                        }
                    }
                    
                    NSString *strValue=[dictControls valueForKey:@"Label"];
                    strValue =[strValue stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    // NSString *strTextView=[[dictControls valueForKey:@"Value"] stringByReplacingOccurrencesOfString:@"," withString:@", \n \n"];
                    
                    NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                    
                    CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lblItem = [[UILabel alloc] init];
                    
                    if (s11.height<30) {
                        s11.height=30;
                    }
                    
                    lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                    [lblItem setFont:[UIFont systemFontOfSize:15]];
                    //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                    [lblItem setNumberOfLines:100000];
                    lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                    
                    //  lblItem.backgroundColor=[UIColor grayColor];
                    
                    [view addSubview:lblItem];
                    
                    int tempHeight=lblItem.frame.size.height;
                    
                    viewHeight=viewHeight+tempHeight;
                    
                    //[view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lblItem.frame.size.height+lbl.frame.size.height+15)];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                    
                    heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                    [arrOfValuepresent addObject:heightttt];
                    
                }
                else {
                    
                    CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([global globalWidthDevice], MAXFLOAT)];
                    UILabel *lblItem = [[UILabel alloc] init];
                    
                    if (s11.height<30) {
                        s11.height=30;
                    }
                    
                    lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                    [lblItem setFont:[UIFont systemFontOfSize:15]];
                    //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                    [lblItem setNumberOfLines:100000];
                    lblItem.text =[dictControls valueForKey:@"Value"];
                    
                    // lblItem.backgroundColor=[UIColor grayColor];
                    
                    [view addSubview:lblItem];
                    
                    int tempHeight=lblItem.frame.size.height;
                    
                    viewHeight=viewHeight+tempHeight;
                    
                    //[view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lblItem.frame.size.height+lbl.frame.size.height+15)];
                    
                    [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, CGRectGetMaxY(lblItem.frame))];
                    
                    heightttt=[NSString stringWithFormat:@"%f",lblItem.frame.size.height+lbl.frame.size.height+15];
                    [arrOfValuepresent addObject:heightttt];
                    
                }
                
                //                CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                //                UILabel *lblItem = [[UILabel alloc] init];
                //                lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width, s11.height);
                //                [lblItem setFont:[UIFont systemFontOfSize:15]];
                //                //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                //                [lblItem setNumberOfLines:2];
                //                lblItem.text =[dictControls valueForKey:@"Value"];
                //                [view addSubview:lblItem];
                //
                //                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, lbl.frame.size.height+12+20)];
                
                heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                
                yPositionOfControls=yPositionOfControls+view.frame.size.height+10;
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                
                [ViewFormSections addSubview:view];
                
            }
        }
        
        if (!isValuePrsent) {
            
            [BtnMainView removeFromSuperview];
            [arrayOfButtonsMain removeLastObject];
            ViewFormSections.frame=CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, ViewFormSections.frame.size.height-400);
            [lblTitleSection removeFromSuperview];
            //isMainValueThere=NO;
        }
        else{
            isMainValueThere=YES;
            int heightTemporay=0;
            
            for (int k=0; k<arrOfValuepresent.count; k++) {
                
                NSString *strValue=arrOfValuepresent[k];
                
                heightTemporay=heightTemporay+[strValue intValue];
                
            }
            
            viewHeight=40+heightTemporay+(int)arrOfValuepresent.count*10;//Label Title ki height ==40 and labeltitilsect
            
            [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, viewHeight+35)];
            
            heightMainViewFormSections=viewHeight+35;
            
            MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width, heightMainViewFormSections+35);
            
            yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+45;
            
        }
    }
    
    if (!(arrSections.count==0)) {
        
        // scrollViewHeight=scrollViewHeight+MainViewForm.frame.size.height;
        
        CGFloat heightTemp=MainViewForm.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewForm.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        _scrollVieww.backgroundColor=[UIColor clearColor];
        
    }else{
        [BtnMainView removeFromSuperview];
        [arrayOfButtonsMain removeLastObject];
        [ViewFormSections removeFromSuperview];
    }
    
    if (!isMainValueThere) {
        
        [BtnMainView removeFromSuperview];
        [arrayOfButtonsMain removeLastObject];
        [ViewFormSections removeFromSuperview];
        [MainViewForm removeFromSuperview];
        [ViewFormSections setFrame:CGRectMake(0, 0, 0, 0)];
        [MainViewForm setFrame:CGRectMake(0, 0, 0, 0)];
        
    }
    //   _scrollVieww.frame=CGRectMake(_scrollVieww.frame.origin.x, _scrollVieww.frame.origin.y, [UIScreen mainScreen].bounds.size.width-2, _scrollVieww.frame.size.height+_view_CustomerInfo.frame.size.height+scrollViewHeight+100);
    
    //  [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_scrollVieww.contentSize.height+_view_CustomerInfo.frame.size.height+scrollViewHeight+100)];
    
    // [MainViewForm setBackgroundColor:[UIColor redColor]];
    
    [self fetchWoEquipmentFromDB];
    
    [self dynamicChemicalList];
}
-(void)dynamicChemicalList
{
    
    NSMutableArray *arrOfUnitMaster=[[NSMutableArray alloc]init];  //=[_dictOfChemicalList valueForKey:@"UnitMasterInfo"];
    NSMutableArray *arrOfTargetMaster=[[NSMutableArray alloc]init];//[_dictOfChemicalList valueForKey:@"TargetMasterInfo"];
    NSMutableArray *arrOfAppMethodInfoMaster=[[NSMutableArray alloc]init];//[_dictOfChemicalList valueForKey:@"AppMethodInfo"];
    
    
    NSMutableArray *arrOfCombineChemicalList=[[NSMutableArray alloc]init];
    
    NSString *hopProfile = @"ProductName";
    
    NSSortDescriptor *hopProfileDescriptor =
    [[NSSortDescriptor alloc] initWithKey:hopProfile
                                ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObjects:hopProfileDescriptor, nil];
    NSArray *sortedArrayOfDictionaries1 = [_arrChemicalList sortedArrayUsingDescriptors:descriptors];
    NSArray *sortedArrayOfDictionaries2 = [_arrOfChemicalListOther sortedArrayUsingDescriptors:descriptors];
    
    [arrOfCombineChemicalList addObjectsFromArray:sortedArrayOfDictionaries1];
    [arrOfCombineChemicalList addObjectsFromArray:sortedArrayOfDictionaries2];
    
    NSArray *arrCombined=(NSArray*)arrOfCombineChemicalList;
    
    NSArray *arrOfProductMaster=arrCombined;
    
    for (int k=0; k<arrOfProductMaster.count; k++) {
        
        NSDictionary *dictData=arrOfProductMaster[k];
        
        [arrOfUnitMaster addObject:[dictData valueForKey:@"UnitId"]];
        
        if (dictData[@"Targets"])
            [arrOfTargetMaster addObject:[dictData valueForKey:@"Targets"]];
        else
            [arrOfTargetMaster addObject:[dictData valueForKey:@"TargetId"]];
        
        
        if (dictData[@"ApplicationMethods"])
            [arrOfAppMethodInfoMaster addObject:[dictData valueForKey:@"ApplicationMethods"]];
        else
            [arrOfAppMethodInfoMaster addObject:[dictData valueForKey:@"ApplicationMethodId"]];
        
    }
    
    /*
     NSMutableArray *arrTempValues=[[NSMutableArray alloc]init];
     
     //arrOfUnitMaster
     for (int k=0; k<arrOfUnitMaster.count; k++) {
     
     NSDictionary *dictData=arrOfUnitMaster[k];
     NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
     
     if ([strDepartmentIddd isEqualToString:_strDepartmentIdd]) {
     
     [arrTempValues addObject:dictData];
     
     }
     }
     arrOfUnitMaster=arrTempValues;
     arrTempValues=nil;
     arrTempValues=[[NSMutableArray alloc]init];
     
     
     //arrOfTargetMaster
     for (int k=0; k<arrOfTargetMaster.count; k++) {
     
     NSDictionary *dictData=arrOfTargetMaster[k];
     NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
     
     if ([strDepartmentIddd isEqualToString:_strDepartmentIdd]) {
     
     [arrTempValues addObject:dictData];
     
     }
     }
     arrOfTargetMaster=arrTempValues;
     arrTempValues=nil;
     arrTempValues=[[NSMutableArray alloc]init];
     
     
     //arrOfAppMethodInfoMaster
     for (int k=0; k<arrOfAppMethodInfoMaster.count; k++) {
     
     NSDictionary *dictData=arrOfAppMethodInfoMaster[k];
     NSString *strDepartmentIddd=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]];
     
     if ([strDepartmentIddd isEqualToString:_strDepartmentIdd]) {
     
     [arrTempValues addObject:dictData];
     
     }
     }
     arrOfAppMethodInfoMaster=arrTempValues;
     arrTempValues=nil;
     arrTempValues=[[NSMutableArray alloc]init];
     
     */
    
    
    //            //Temporary HAi Comment KArna Yad SE
    //            arrResponseInspection=[[NSMutableArray alloc]init];
    //            NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    //            NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"ChemicalList.json"];
    //            NSError * error;
    //            NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    //            NSDictionary *arrOfCountry = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    //            dictOfChemicalList=arrOfCountry;
    //            dictChemicalList=dictOfChemicalList;
    //            arrOfProductMaster=[dictOfChemicalList valueForKey:@"ProductMasterInfo"];
    
    CGFloat scrollViewHeight=0.0;
    
    arrOfIndexesChemical =[[NSMutableArray alloc]init];
    arrOfControlssMainChemical =[[NSMutableArray alloc]init];
    
    CGFloat yXisForBtnCheckUncheck=0.0;
    if (ViewFormSections.frame.size.height==0) {
        
        yXisForBtnCheckUncheck=_view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+20;
        
        
    } else {
        
        yXisForBtnCheckUncheck=_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+MainViewForm.frame.size.height+20+50;
        
    }
    scrollViewHeight=MainViewForm.frame.size.height;
    
    for (int i=0; i<arrOfProductMaster.count; i++)
    {
        NSDictionary *dictProductMasterDetail=[arrOfProductMaster objectAtIndex:i];
        if (!MainViewFormChemical.frame.size.height) {
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck, 30, 30)];
        }
        else{
            BtnMainViewChemical=[[UIButton alloc]initWithFrame:CGRectMake(20, yXisForBtnCheckUncheck, 30, 30)];
        }
        
        BtnMainViewChemical.tag=i+5555;
        //  [BtnMainViewChemical addTarget:self action:@selector(actionOpenCloseChemicalList:) forControlEvents:UIControlEventTouchDown];
        // BtnMainViewChemical.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        NSString *strProductId=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductId"]];
        
        if ([strProductId isEqualToString:@"(null)"] || (strProductId.length==0) || (strProductId==nil)) {
            
            strProductId=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductName"]];
            
        }
        
        BOOL isMatchedProductId;
        isMatchedProductId=NO;
        
        [BtnMainViewChemical setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        
        [_scrollVieww addSubview:BtnMainViewChemical];
        
        [arrayOfButtonsMainChemical addObject:BtnMainViewChemical];
        
        // Form Creation
        MainViewFormChemical=[[UIView alloc]init];
        
        MainViewFormChemical.backgroundColor=[UIColor clearColor];
        
        MainViewFormChemical.frame=CGRectMake(0, yXisForBtnCheckUncheck+BtnMainViewChemical.frame.size.height+10, [UIScreen mainScreen].bounds.size.width, 150);
        
        MainViewFormChemical.tag=i+9999;
        MainViewFormChemical.backgroundColor=[UIColor clearColor];
        
        [_scrollVieww addSubview:MainViewFormChemical];
        
        UILabel *lblTitleSection=[[UILabel alloc]init];
        lblTitleSection.backgroundColor=[UIColor lightGrayColor];
        lblTitleSection.layer.borderWidth = 1.5f;
        lblTitleSection.layer.cornerRadius = 4.0f;
        lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
        
        lblTitleSection.frame=CGRectMake(BtnMainViewChemical.frame.origin.x+BtnMainViewChemical.frame.size.width+5, BtnMainViewChemical.frame.origin.y, [UIScreen mainScreen].bounds.size.width-BtnMainViewChemical.frame.origin.x-BtnMainViewChemical.frame.size.width-15, 30);
        
        lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductName"]] ;
        lblTitleSection.textAlignment=NSTextAlignmentLeft;
        
        [arrayProductName addObject:lblTitleSection];
        
        [_scrollVieww addSubview:lblTitleSection];
        
        NSString *strProductAmountt,*strProductPercentt,*strUnitt,*strTargett,*strUnittNameShow,*strTargetNameShow,*strEPAReg,*strMethodInfo,*strMethodInfoToShow;
        
        
        strProductAmountt=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        strProductPercentt=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        strUnitt=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"UnitId"]];
        strTargett=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"Targets"]];
        strEPAReg=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"EPARegNumber"]];
        strMethodInfo=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ApplicationMethods"]];
        
        if ([strEPAReg isEqualToString:@"(null)"] || [strEPAReg isKindOfClass:[NSNull class]]) {
            
            strEPAReg=@"";
            
        }
        
        for (int j=0; j<arrOfUnitMaster.count; j++) {
            
            NSDictionary *dict=arrOfUnitMaster[j];
            NSString *strUId=[NSString stringWithFormat:@"%@",arrOfUnitMaster[j]];
            // NSString *strUnitNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"UnitName"]];
            if ([strUId isEqualToString:strUnitt]) {
                
                strUnittNameShow=strUId;
                
            }
            
        }
        
        for (int j=0; j<arrOfTargetMaster.count; j++) {
            
            NSDictionary *dict=arrOfTargetMaster[j];
            NSString *strTId=[NSString stringWithFormat:@"%@",arrOfTargetMaster[j]];
            //  NSString *strTargetNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TargetName"]];
            if ([strTId isEqualToString:strTargett]) {
                
                strTargetNameShow=strTId;
                
            }
            
        }
        
        
        for (int j=0; j<arrOfAppMethodInfoMaster.count; j++) {
            
            NSDictionary *dict=arrOfAppMethodInfoMaster[j];
            NSString *strApplicationMethodId=[NSString stringWithFormat:@"%@",arrOfAppMethodInfoMaster[j]];
            //  NSString *strApplicationMethodIdNamee=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ApplicationMethodName"]];
            if ([strApplicationMethodId isEqualToString:strMethodInfo]) {
                
                strMethodInfoToShow=strApplicationMethodId;
                
            }
            
        }
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt EPA Reg Number -----------------
        //============================================================================
        //============================================================================
        UIView *viewwEPA=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        UITextField *txtFieldEPA=[[UITextField alloc]init];
        txtFieldEPA.placeholder=@"EPAReg #";
        txtFieldEPA.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"EPARegNumber"]];
        txtFieldEPA.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldEPA.layer.borderWidth = 1.5f;
        txtFieldEPA.layer.cornerRadius = 4.0f;
        txtFieldEPA.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldEPA.tag=i+25000;
        txtFieldEPA.inputView=viewwEPA;
        [txtFieldEPA setEnabled:NO];
        txtFieldEPA.delegate=self;
        txtFieldEPA.frame=CGRectMake(BtnMainViewChemical.frame.origin.x-10, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldEPA];
        
        yXisForBtnCheckUncheck=yXisForBtnCheckUncheck+200;
        
        //============================================================================
        //============================================================================
#pragma mark----------------------Txt Percentage-----------------
        //============================================================================
        //============================================================================
        
        UITextField *txtFieldPercentage=[[UITextField alloc]init];
        txtFieldPercentage.placeholder=@"Percentage";
        txtFieldPercentage.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"DefaultPercentage"]];
        txtFieldPercentage.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldPercentage.layer.borderWidth = 1.5f;
        txtFieldPercentage.layer.cornerRadius = 4.0f;
        txtFieldPercentage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldPercentage.tag=i+30000;
        txtFieldPercentage.inputView=viewwEPA;
        txtFieldPercentage.delegate=self;
        [txtFieldPercentage setEnabled:NO];
        txtFieldPercentage.frame=CGRectMake(txtFieldEPA.frame.origin.x+txtFieldEPA.frame.size.width+5, 10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldPercentage];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Txt Amount-----------------
        //============================================================================
        //============================================================================
        UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        UITextField *txtFieldAmount=[[UITextField alloc]init];
        txtFieldAmount.placeholder=@"Amount";
        txtFieldAmount.text=[NSString stringWithFormat:@"%@",[dictProductMasterDetail valueForKey:@"ProductAmount"]];
        txtFieldAmount.keyboardType=UIKeyboardTypeNumberPad;
        txtFieldAmount.layer.borderWidth = 1.5f;
        txtFieldAmount.layer.cornerRadius = 4.0f;
        txtFieldAmount.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtFieldAmount.tag=i+25000;
        txtFieldAmount.inputView=vieww;
        txtFieldAmount.delegate=self;
        [txtFieldAmount setEnabled:NO];
        txtFieldAmount.frame=CGRectMake(txtFieldEPA.frame.origin.x, txtFieldEPA.frame.origin.y+txtFieldEPA.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        [MainViewFormChemical addSubview:txtFieldAmount];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Unit-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnUnit=[[UIButton alloc]init];
        
        btnUnit.frame=CGRectMake(txtFieldPercentage.frame.origin.x, txtFieldPercentage.frame.origin.y+txtFieldPercentage.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfUnitIdToFetchUnitName=[_dictOfChemicalList valueForKey:@"UnitMasterInfo"];
        
        for (int s=0; s<arrOfUnitIdToFetchUnitName.count; s++) {
            
            NSDictionary *dictData=arrOfUnitIdToFetchUnitName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitId"]];
            if ([strUnitIdToCheck isEqualToString:strUnittNameShow]) {
                
                strUnittNameShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"UnitName"]];
                break;
                
            }
        }
        
        [btnUnit setTitle:strUnittNameShow forState:UIControlStateNormal];
        [btnUnit setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnUnit.tag=i+15000;
        // [btnUnit addTarget:self action:@selector(actionUnitList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfUnit addObject:btnUnit];
        [MainViewFormChemical addSubview:btnUnit];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn App Method-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnAppMethod=[[UIButton alloc]init];
        
        btnAppMethod.frame=CGRectMake(txtFieldAmount.frame.origin.x, txtFieldAmount.frame.origin.y+txtFieldAmount.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfAppMethodIdToFetchAppMethodName=[_dictOfChemicalList valueForKey:@"AppMethodInfo"];
        
        NSMutableArray *arrTempValueAppMethod=[[NSMutableArray alloc]init];
        
        NSArray *objValueControls=[NSArray arrayWithObjects:
                                   @"0",
                                   @"Select Method",
                                   @"00001",nil];
        
        NSArray *objKeyControls=[NSArray arrayWithObjects:
                                 @"ApplicationMethodId",
                                 @"ApplicationMethodName",
                                 @"DepartmentId",nil];
        
        NSDictionary *dictAddExtra=[[NSDictionary alloc] initWithObjects:objValueControls forKeys:objKeyControls];
        
        // [arrTempValueAppMethod addObject:dictAddExtra];
        
        [arrTempValueAppMethod addObjectsFromArray:arrOfAppMethodIdToFetchAppMethodName];
        
        arrOfAppMethodIdToFetchAppMethodName=arrTempValueAppMethod;
        
        NSMutableArray *arrTempssNew=[[NSMutableArray alloc]init];
        NSArray *arrValuesAppMethodss=[strMethodInfoToShow componentsSeparatedByString:@","];
        
        for (int s=0; s<arrOfAppMethodIdToFetchAppMethodName.count; s++) {
            
            NSDictionary *dictData=arrOfAppMethodIdToFetchAppMethodName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodId"]];
            if ([arrValuesAppMethodss containsObject:strUnitIdToCheck]) {
                //strUnitIdToCheck
                strMethodInfoToShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodName"]];
                [arrTempssNew addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ApplicationMethodName"]]];
                //  break;
                
            }
        }
        
        strMethodInfoToShow=[arrTempssNew componentsJoinedByString:@","];
        
        if (strMethodInfoToShow.length==0) {
            strMethodInfoToShow=@"N/A";
        }
        
        [arrOfAppMethodNameToShowOnClick addObject:strMethodInfoToShow];
        
        [btnAppMethod setTitle:strMethodInfoToShow forState:UIControlStateNormal];
        [btnAppMethod setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnAppMethod.tag=i+20000;
        [btnAppMethod addTarget:self action:@selector(actionAppMethodList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTarget addObject:btnAppMethod];
        [MainViewFormChemical addSubview:btnAppMethod];
        
        
        //============================================================================
        //============================================================================
#pragma mark- ---------------------Btn Target-----------------
        //============================================================================
        //============================================================================
        
        UIButton *btnTarget=[[UIButton alloc]init];
        
        btnTarget.frame=CGRectMake(btnUnit.frame.origin.x, btnUnit.frame.origin.y+btnUnit.frame.size.height+10, [UIScreen mainScreen].bounds.size.width/2-10, 30);
        
        NSArray *arrOfTargetIdToFetchTargetName=[_dictOfChemicalList valueForKey:@"TargetMasterInfo"];
        
        NSMutableArray *arrTempss=[[NSMutableArray alloc]init];
        NSArray *arrValuesTargets=[strTargetNameShow componentsSeparatedByString:@","];
        for (int s=0; s<arrOfTargetIdToFetchTargetName.count; s++) {
            
            NSDictionary *dictData=arrOfTargetIdToFetchTargetName[s];
            
            NSString *strUnitIdToCheck=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetId"]];
            if ([arrValuesTargets containsObject:strUnitIdToCheck]) {
                //strUnitIdToCheck
                strTargetNameShow=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetName"]];
                [arrTempss addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"TargetName"]]];
                //  break;
                
            }
        }
        
        strTargetNameShow=[arrTempss componentsJoinedByString:@","];
        
        if (strTargetNameShow.length==0) {
            strTargetNameShow=@"N/A";
        }
        [arrOfTargetNameToShowOnClick addObject:strTargetNameShow];
        
        [btnTarget setTitle:strTargetNameShow forState:UIControlStateNormal];
        [btnTarget setBackgroundColor:[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1]];
        btnTarget.tag=i+40000;
        [btnTarget addTarget:self action:@selector(actionTargetList:) forControlEvents:UIControlEventTouchDown];
        [arrayOfTarget addObject:btnTarget];
        [MainViewFormChemical addSubview:btnTarget];
        
        
        // scrollViewHeight=scrollViewHeight+MainViewFormChemical.frame.size.height;
        
        [arrayViewsChemical addObject:MainViewFormChemical];
        
        CGFloat heightTemp=MainViewFormChemical.frame.size.height;
        
        NSString *strTempHeight=[NSString stringWithFormat:@"%f",heightTemp];
        
        CGFloat YAxisTemp=MainViewFormChemical.frame.origin.y;
        
        NSString *strTempYAxis=[NSString stringWithFormat:@"%f",YAxisTemp];
        
        [arrOfHeightsViewsChemical addObject:strTempHeight];
        
        [arrOfYAxisViewsChemical addObject:strTempYAxis];
        
    }
    _scrollVieww.backgroundColor=[UIColor clearColor];
    
    //Showing EquipmentView Here
    
    CGRect frameFor_viewPaymentInfo;
    
    NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
    
    BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
    
    if (isEquipEnabled) {
        
        /*
         for (int k=0; k<_arrOfEquipmentLists.count; k++) {
         
         NSDictionary *dictData=_arrOfEquipmentLists[k];
         NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsServiceStatus"]];
         if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
         
         [arrOfEquipmentList addObject:dictData];
         
         }
         }
         _arrOfEquipmentLists=arrOfEquipmentList;
         */
        
        [arrOfEquipmentList addObjectsFromArray:_arrOfEquipmentLists];
        
        //Yaha Check Lagana Hai
        
        if (arrOfEquipmentList.count==0) {
            
            frameFor_viewPaymentInfo=CGRectMake(10, 200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
            frameFor_viewPaymentInfo.origin.x=10;
            frameFor_viewPaymentInfo.origin.y=200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20;
            
            
        } else {
            
            frameFor_viewPaymentInfo=CGRectMake(10, 200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
            frameFor_viewPaymentInfo.origin.x=10;
            frameFor_viewPaymentInfo.origin.y=200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20;
            
            _tblview_EquipmentList.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, arrOfEquipmentList.count*365);
            
            _view_EquipmentList.frame=CGRectMake(0, frameFor_viewPaymentInfo.origin.y,[UIScreen mainScreen].bounds.size.width, arrOfEquipmentList.count*365);
            
            frameFor_viewPaymentInfo.origin.y=_view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+10;
            
            [_scrollVieww addSubview:_view_EquipmentList];
            
            // [_tblview_EquipmentList reloadData];
        }
        
        
    }else{
        
        
        frameFor_viewPaymentInfo=CGRectMake(10, 200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameFor_viewPaymentInfo.origin.x=10;
        frameFor_viewPaymentInfo.origin.y=200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20;
        
        
    }
    
    [_paymentTypeView setFrame:frameFor_viewPaymentInfo];
    
    
    //Change For Service job descriptions
    
    [_scrollVieww addSubview:_paymentTypeView];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    [_scrollVieww addSubview:_viewServiceJobDescriptions];
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    
    [_scrollVieww addSubview:_viewPaymentInfo];
    
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    [_scrollVieww addSubview:_view_TechSignature];
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    [_scrollVieww addSubview:_viewCustomerSignature];
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    [_scrollVieww addSubview:_view_SavenContinue];
    
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *contextNew = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [_txtViewTermsnConditions.text boundingRectWithSize:constraint
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:_txtViewTermsnConditions.font}
                                                                     context:contextNew].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    if (size.height<85) {
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, 100);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, 85+40);
        
        
    }else{
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, size.height+40);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, size.height+40);
        
    }
    
    [_scrollVieww addSubview:_viewTermsnConditions];
    
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y)];
    
    
    //Change For Fetching Workorder equipIdss
    NSUserDefaults *defsEquipNew=[NSUserDefaults standardUserDefaults];
    
    BOOL isEquipEnabledNew=[defsEquipNew boolForKey:@"isEquipmentEnabled"];
    
    if (isEquipEnabledNew) {
        
        for (int k=0; k<_arrOfEquipmentLists.count; k++) {
            
            NSDictionary *dictDataEquip=_arrOfEquipmentLists[k];
            NSString *strWoEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"WOEquipmentId"]];
            if (strWoEquipIdd.length==0) {
                
                NSString *strMobileEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"MobileUniqueId"]];
                strGlobalWoEquipIdToFetchDynamicData=strMobileEquipIdd;
                isWoEquipIdPrsent=NO;
            } else {
                isWoEquipIdPrsent=YES;
                strGlobalWoEquipIdToFetchDynamicData=strWoEquipIdd;
            }
            
            [self FetchFromCoreDataToShowEquipmentDynamic];
            
        }
        
        [self findHeightForRowsAtIndexPaths];
        
    }
    //Change For Fetching Workorder equipIdss ENDED HERE
    
}

-(void)actionTargetList:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    int taggg=btn.tag;//40000
    
    NSString *strToShow=[NSString stringWithFormat:@"%@",arrOfTargetNameToShowOnClick[taggg-40000]];
    
    if (![strToShow isEqualToString:@"N/A"]) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Selected Targets"
                                   message:strToShow
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
-(void)actionAppMethodList:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    int taggg=btn.tag;//20000
    
    NSString *strToShow=[NSString stringWithFormat:@"%@",arrOfAppMethodNameToShowOnClick[taggg-20000]];
    
    if (![strToShow isEqualToString:@"N/A"]) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Selected Methods"
                                   message:strToShow
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)hideTextFields
{
    UIView *vieww=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
    _txt_Customer.inputView=vieww;
    _txt_AccountNo.inputView=vieww;
    _txt_Technician.inputView=vieww;
    _txt_WorkorderNo.inputView=vieww;
    _txt_PrimaryEmail.inputView=vieww;
    _txt_PrimaryPhone.inputView=vieww;
    _txt_SecondaryEmail.inputView=vieww;
    _txt_SecondaryPhone.inputView=vieww;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    /* if([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
     {
     return false;
     }
     else
     {
     return true;
     }*/
    if (textField.tag==7) {
        //Image Caption
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }else  if (textField.tag==8) {
        //Image Description
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 250) ? NO : YES;
    }else  if(textField.tag==2)
    {
        return false;
    }
    else
    {
        return true;
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag==10000) {
        
        [_scrollVieww setContentOffset:CGPointMake(0,_amountViewSingle.frame.origin.y+textField.frame.origin.y-50) animated:YES];
        
    } else {
        
        [_scrollVieww setContentOffset:CGPointMake(0,_checkVieww.frame.origin.y+textField.frame.origin.y-50) animated:YES];
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.tag==10000) {
        
        [_lblTotalPaid setHidden:NO];
        _strPaidAmount=textField.text;
        _lblTotalPaid.text=[NSString stringWithFormat:@"Total Paid Amount: $%@",_strPaidAmount];
        
    }else if(textField.tag==10001){
        
        [_lblTotalPaid setHidden:NO];
        _strPaidAmount=textField.text;
        _lblTotalPaid.text=[NSString stringWithFormat:@"Total Paid Amount: $%@",_strPaidAmount];
        
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//============================================================================
//============================================================================
#pragma mark- ---------------------TEXT VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(void)textViewDidBeginEditing:(UITextView *)textField{
    
    //[_scrollVieww setContentOffset:CGPointMake(0, _viewPaymentInfo.frame.origin.y+textField.frame.origin.y-50) animated:YES];
    // NSLog(@"Position of textview===-----------%f",_viewPaymentInfo.frame.origin.y+textField.frame.origin.y-50);
    
}

-(void)textViewDidEndEditing:(UITextView *)textField
{
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    yesEditedSomething=YES;
    NSLog(@"Yes Edited Something In Db");
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

- (IBAction)action_Refresh:(id)sender {
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        if([UIScreen mainScreen].bounds.size.height == 480.0)
        {
            //move to your iphone5 xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView" bundle:nil];
            [self.navigationController pushViewController:bav animated:YES];
        }
        else{
            //move to your iphone4s xib
            BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iphone5" bundle:nil];
            [self.navigationController pushViewController:bav animated:YES];
        }
    }
    else{
        //BeginAuditView_iPad
        BeginAuditView *bav = [[BeginAuditView alloc] initWithNibName:@"BeginAuditView_iPad" bundle:nil];
        [self.navigationController pushViewController:bav animated:NO];
    }
    
}

//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    if (imagee == nil){
        
    }else{
        
    }
    
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent");
}


//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    NSData *audioData;
    
    audioData = [NSData dataWithContentsOfFile:path];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,UrlImageUpload];
    
    // setting up the request object now
    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
    [request1 setURL:[NSURL URLWithString:urlString]];
    [request1 setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request1 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Audio Sent");
    }
    NSLog(@"Audio Sent");
}
-(void)fetchWorkOrderDetails{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    //strWorkOrderId=@"201";
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderId];
    
    [requestNewService setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        _workOrderDetail=arrAllObj[0];
        [self showValues];
        
        strDepartmentType=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"departmentType"]];
        
        NSUserDefaults *defsSign=[NSUserDefaults standardUserDefaults];
        
        BOOL isPreSetSign=[defsSign boolForKey:@"isPreSetSignService"];
        
        NSString *strSignUrl=[defsSign valueForKey:@"ServiceTechSignPath"];
        
        if ((isPreSetSign) && (strSignUrl.length>0) && !([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)) {
            
            strSignUrl=[strSignUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            NSString *result;
            NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
            }else{
                result=strSignUrl;
            }
            
            strGlobalTechSignImage=result;
            
            [self downloadingImagessTechnicianPreSet:strSignUrl];
            
            [_btn_TechSignn setEnabled:NO];
            
            isPreSetSignGlobal=YES;
            
        } else {
            
            isPreSetSignGlobal=NO;
            
            [_btn_TechSignn setEnabled:YES];
            
            if([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
            {
                [_btn_TechSignn setEnabled:NO];
            }
            NSString *technicianImage=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"technicianSignaturePath"]];
            if (technicianImage.length>0) {
                
                NSLog(@"TechSign====%@",technicianImage);
                
                NSString *strIsPresetWO=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"isEmployeePresetSignature"]];
                
                
                
                if ([strIsPresetWO caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsPresetWO isEqualToString:@"1"]) {
                    
                    //downloadingImagessTechnicianIfPreset
                    [self downloadingImagessTechnicianIfPreset:technicianImage];
                    
                }else{
                    
                    [self downloadingImagessTechnician:technicianImage];
                    
                }
                
                
                strGlobalTechSignImage=technicianImage;
                
            }
            
        }
        
        NSString *customerImage=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"customerSignaturePath"]];
        
        if (customerImage.length>0) {
            
            NSLog(@"customerImage====%@",customerImage);
            
            [self downloadingImagess:customerImage];
            
            strGlobalCustSignImage=customerImage;
            
        }else{
            
        }
        NSString *strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"isCustomerNotPresent"]];
        
        //for service job description start
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [[_workOrderDetail valueForKey:@"serviceJobDescription"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        _textView_ServiceJobDescriptions.attributedText = attributedString;
        
        //_textView_ServiceJobDescriptions.text=[_workOrderDetail valueForKey:@"serviceJobDescription"];
        
        if (_textView_ServiceJobDescriptions.text.length==0) {
            
            heightForServiceJobDescriptions=0;
            [_viewServiceJobDescriptions setHidden:YES];
            
        } else {
            
            [_viewServiceJobDescriptions setHidden:NO];
            heightForServiceJobDescriptions=_viewServiceJobDescriptions.frame.size.height;
            
            NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
            
            isNoServiceJobDescriptions=[defsBack boolForKey:@"isNoServiceJobDescription"];
            
            if (isNoServiceJobDescriptions) {
                
                heightForServiceJobDescriptions=0;
                [_viewServiceJobDescriptions setHidden:YES];
                
            }
            else{
                
                CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, CGFLOAT_MAX);
                CGSize size;
                
                NSStringDrawingContext *contextNew = [[NSStringDrawingContext alloc] init];
                CGSize boundingBox = [_textView_ServiceJobDescriptions.text boundingRectWithSize:constraint
                                                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                                                      attributes:@{NSFontAttributeName:_textView_ServiceJobDescriptions.font}
                                                                                         context:contextNew].size;
                
                size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
                
                if (size.height<90) {
                    heightForServiceJobDescriptions=90+70+30;
                }else{
                    
                    _textView_ServiceJobDescriptions.frame=CGRectMake(_textView_ServiceJobDescriptions.frame.origin.x, _textView_ServiceJobDescriptions.frame.origin.y, _textView_ServiceJobDescriptions.frame.size.width, size.height+40);
                    heightForServiceJobDescriptions=size.height+100;
                    
                }
            }
        }
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
        
        if ([dictdata isKindOfClass:[NSString class]]) {
            
            //[global AlertMethod:Alert :NoDataAvailableServiceJobDescriptions];
            
        }else{
            
            NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceJobDescriptionTemplate"];
            if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
                
                for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
                    
                    NSDictionary *dictData=[arrOfServiceJobDescriptionTemplate objectAtIndex:k];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"ServiceJobDescriptionId"]] isEqualToString:[_workOrderDetail valueForKey:@"serviceJobDescriptionId"]]) {
                        
                        [_btnServiceJobDescriptionTitle setTitle:[dictData valueForKey:@"Title"] forState:UIControlStateNormal];
                        
                    }
                }
            }
        }
        
        
        //for service job description start
        
        if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
            
            isCustomerNotPrsent=YES;
            
            [_btnCustNotPrsent setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            _const_ViewCust_Top.constant=0;
            _const_ViewCustSign_H.constant=0;
            
            _imgView_CustomerSign.image=[UIImage imageNamed:@"NoImage.jpg"];
            
            [_lbl_CustomerSign setHidden:YES];
            [_btn_CustSignn setHidden:YES];
            [_imgView_CustomerSign setHidden:YES];
            if (_const_AfterImage_BtoImgView.constant==-120) {
                
                _const_SavenContinue_B.constant=275;
                
            } else {
                
                _const_SavenContinue_B.constant=175;
                
            }
            
            // [_btnServiceJobDescriptionTitle setEnabled:NO];
            [_textView_ServiceJobDescriptions setEditable:NO];
            
        } else {
            
            isCustomerNotPrsent=NO;
            
            [_btnCustNotPrsent setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            _const_ViewCust_Top.constant=1;
            _const_ViewCustSign_H.constant=138;
            
            [_lbl_CustomerSign setHidden:NO];
            [_btn_CustSignn setHidden:NO];
            [_imgView_CustomerSign setHidden:NO];
            if (_const_AfterImage_BtoImgView.constant==-120) {
                _const_SavenContinue_B.constant=175;
            }else{
                _const_SavenContinue_B.constant=61;
            }
            
            // [_btnServiceJobDescriptionTitle setEnabled:NO];
            [_textView_ServiceJobDescriptions setEditable:NO];
            
        }
        
        NSString *audioPath=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"audioFilePath"]];
        
        if (audioPath.length>0) {
            
            if ([audioPath isEqualToString:@"(null)"]) {
                
                strGlobalAudio=@"";
                
            } else {
                
                NSLog(@"audioPath====%@",audioPath);
                strGlobalAudio=audioPath;
                [self downloadingAudio:audioPath];
                
            }
            
        }
        
        float total=[[_workOrderDetail valueForKey:@"invoiceAmount"] floatValue]+[[_workOrderDetail valueForKey:@"previousBalance"] floatValue]+[[_workOrderDetail valueForKey:@"tax"] floatValue];
        NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", total];
        _lblChargesCurrentService.text=[NSString stringWithFormat:@"Charges for current services: $%@",[_workOrderDetail valueForKey:@"invoiceAmount"]];
        _lblTax.text=[NSString stringWithFormat:@"Tax: $%@",[_workOrderDetail valueForKey:@"tax"]];
        _lblTotaldue.text=[NSString stringWithFormat:@"Total Due: $%@",formattedNumber];//[matchesWorkOrder valueForKey:@"workorderId"];
        
    }
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictdata=[defsLogindDetail valueForKey:@"MasterServiceAutomation"];
    NSArray *arrOfServiceJobDescriptionTemplate=[dictdata valueForKey:@"ServiceReportTermsAndConditions"];
    
    NSString *strTremsnConditions;
    if ([arrOfServiceJobDescriptionTemplate isKindOfClass:[NSArray class]]) {
        
        for (int k=0; k<arrOfServiceJobDescriptionTemplate.count; k++) {
            
            
            NSDictionary *dictData=arrOfServiceJobDescriptionTemplate[k];
            
            if ([[NSString stringWithFormat:@"%@",[dictData valueForKey:@"DepartmentId"]] isEqualToString:[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"departmentId"]]]) {
                
                strTremsnConditions=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SR_TermsAndConditions"]];
                
            }
        }
        
    }else{
        
        strTremsnConditions=@"N/A";
        
    }
    
    
    NSAttributedString *attributedStringTermsnConditions = [[NSAttributedString alloc]
                                                            initWithData: [strTremsnConditions dataUsingEncoding:NSUnicodeStringEncoding]
                                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                            documentAttributes: nil
                                                            error: nil
                                                            ];
    
    _txtViewTermsnConditions.attributedText=attributedStringTermsnConditions;
    
    
    NSAttributedString *attributedStringTermsnConditionsNew = [[NSAttributedString alloc]
                                                               initWithData: [[self getHTML] dataUsingEncoding:NSUnicodeStringEncoding]
                                                               options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                               documentAttributes: nil
                                                               error: nil
                                                               ];
    
    _txtViewTermsnConditions.attributedText=attributedStringTermsnConditionsNew;
    
}

-(void)viewTermsHeight :(NSString*)strTremsnConditions{
    
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *contextNew = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [strTremsnConditions boundingRectWithSize:constraint
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:@{NSFontAttributeName:_txtViewTermsnConditions.font}
                                                           context:contextNew].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    if (size.height<85) {
        
        
        
    }else{
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, size.height+40);
        
        _viewTermsnConditions.frame=CGRectMake(_viewTermsnConditions.frame.origin.x, _viewTermsnConditions.frame.origin.y, _viewTermsnConditions.frame.size.width, size.height+50);
        
        [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
        
    }
    
}

- (NSString *)getHTML {
    NSDictionary *exportParams = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
    NSData *htmlData = [_txtViewTermsnConditions.attributedText dataFromRange:NSMakeRange(0, _txtViewTermsnConditions.attributedText.length) documentAttributes:exportParams error:nil];
    return [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
}

-(void)showValues
{
    
    _txt_AccountNo.text=[_workOrderDetail valueForKey:@"accountNo"];
    
    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"thirdPartyAccountNo"]];
    if (strThirdPartyAccountNo.length>0) {
        _txt_AccountNo.text=[NSString stringWithFormat:@"%@",strThirdPartyAccountNo];
    }
    
    _txt_WorkorderNo.text=[_workOrderDetail valueForKey:@"workOrderNo"];
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"firstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"lastName"]];
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    
    _txt_Customer.text=strFullName;
    _txt_PrimaryPhone.text=[_workOrderDetail valueForKey:@"primaryPhone"];
    _txt_SecondaryPhone.text=[_workOrderDetail valueForKey:@"secondaryPhone"];
    _txt_Cell.text=[_workOrderDetail valueForKey:@"cellNo"];
    _txt_PrimaryEmail.text=[_workOrderDetail valueForKey:@"primaryEmail"];
    _txt_SecondaryEmail.text=[_workOrderDetail valueForKey:@"secondaryEmail"];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    if([NSString stringWithFormat:@"%@",[defs valueForKey:@"inspectorNameService"]].length==0)
    {
        _txt_Technician.text=@"N/A";
    }
    else
    {
        _txt_Technician.text=[defs valueForKey:@"inspectorNameService"];
    }
    
    // _txt_Technician.text=[_workOrderDetail valueForKey:@"technicianComment"];
    
    NSString *strBillingcountry=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"billingCountry"]];
    
    if ([strBillingcountry caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strBillingcountry=@"United States";
    }
    
    NSString *strbillingAddress2=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"billingAddress2"]];
    if (strbillingAddress2.length==0) {
        
        _txtView_BillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[_workOrderDetail valueForKey:@"billingAddress1"],[_workOrderDetail valueForKey:@"billingCity"],[_workOrderDetail valueForKey:@"billingState"],strBillingcountry,[_workOrderDetail valueForKey:@"billingZipcode"]];
        
    } else {
        
        _txtView_BillingAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[_workOrderDetail valueForKey:@"billingAddress1"],[_workOrderDetail valueForKey:@"billingAddress2"],[_workOrderDetail valueForKey:@"billingCity"],[_workOrderDetail valueForKey:@"billingState"],strBillingcountry,[_workOrderDetail valueForKey:@"billingZipcode"]];
        
    }
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"serviceCountry"]];
    
    if ([strServiceAddress caseInsensitiveCompare:@"United States"] == NSOrderedSame) {
        
    }else{
        strServiceAddress=@"United States";
    }
    
    NSString *strServiceAddress2=[NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"billingAddress2"]];
    if (strServiceAddress2.length==0) {
        
        _txt_ServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@",[_workOrderDetail valueForKey:@"servicesAddress1"],[_workOrderDetail valueForKey:@"serviceCity"],[_workOrderDetail valueForKey:@"serviceState"],strServiceAddress,[_workOrderDetail valueForKey:@"serviceZipcode"]];
        
        
    }else{
        
        _txt_ServiceAddress.text=[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",[_workOrderDetail valueForKey:@"servicesAddress1"],[_workOrderDetail valueForKey:@"serviceAddress2"],[_workOrderDetail valueForKey:@"serviceCity"],[_workOrderDetail valueForKey:@"serviceState"],strServiceAddress,[_workOrderDetail valueForKey:@"serviceZipcode"]];
        
        
    }
    
    //   _txtView_BillingAddress.text=[_workOrderDetail valueForKey:@"BillingAddress1"];
    //    _txt_ServiceAddress.text=[_workOrderDetail valueForKey:@"ServicesAddress1"];
    
    _txtView_OfficeNotes.text=[_workOrderDetail valueForKey:@"officeNotes"];
    _txtView_TechComment.text=[_workOrderDetail valueForKey:@"technicianComment"];
    _lblTimeinn.text=[NSString stringWithFormat:@"Time In: %@",[_workOrderDetail valueForKey:@"timeIn"]];
    _lblTimeOut.text=[NSString stringWithFormat:@"Time Out: %@",[_workOrderDetail valueForKey:@"timeOut"]];
    
    _lblTimeinn.text=[NSString stringWithFormat:@"Time In: %@",[global changeDateToFormattedDateNew:[_workOrderDetail valueForKey:@"timeIn"]]];
    
    _lblTimeOut.text=[NSString stringWithFormat:@"Time Out: %@",[global changeDateToFormattedDateNew:[_workOrderDetail valueForKey:@"timeOut"]]];
    
    if ([_lblTimeOut.text isEqualToString:@"Time Out: (null)"]) {
        
        _lblTimeOut.text=@"Time Out: N/A";
        
    }
    
    if ([_strPaymentModes caseInsensitiveCompare:@"NoCharge"] == NSOrderedSame || [_strPaymentModes caseInsensitiveCompare:@"Bill"] == NSOrderedSame || [_strPaymentModes caseInsensitiveCompare:@"PaymentPending"] == NSOrderedSame || [_strPaymentModes caseInsensitiveCompare:@"AutoChargeCustomer"] == NSOrderedSame) {
        [_lblTotalPaid setHidden:YES];
        
    } else {
        [_lblTotalPaid setHidden:NO];
        _lblTotalPaid.text=[NSString stringWithFormat:@"Total Paid Amount: $%@",_strPaidAmount];//[_paymentInfoDetail valueForKey:@"paidAmount"]
        
    }
    
    if ([_strPaymentModes caseInsensitiveCompare:@"NoCharge"] == NSOrderedSame) {
        _strPaymentModes=@"No Charge";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame) {
        _strPaymentModes=@"Credit Card";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"AutoChargeCustomer"] == NSOrderedSame) {
        _strPaymentModes=@"Auto Charge Customer";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"PaymentPending"] == NSOrderedSame) {
        _strPaymentModes=@"Payment Pending";
    }
    
    _lblPaymentType.text=[NSString stringWithFormat:@"Payment Type: %@",_strPaymentModes];//[_paymentInfoDetail valueForKey:@"paymentMode"]
    
    
    strGlobalWorkOrderStatus=[_workOrderDetail valueForKey:@"workorderStatus"];
    
    if (!([[_workOrderDetail valueForKey:@"workorderStatus"] caseInsensitiveCompare:@"InComplete"] == NSOrderedSame)) {
        
        [_txtView_TechComment setEditable:NO];
        [_txtView_OfficeNotes setEditable:NO];
    } else {
        
        [_txtView_TechComment setEditable:YES];
        [_txtView_OfficeNotes setEditable:YES];
    }
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
        
        [_btnAddGraphhh setEnabled:NO];
        [_btnCustNotPrsent setEnabled:NO];
        [_btn_CustSignn setEnabled:NO];
        [_btn_TechSignn setEnabled:NO];
        // [_btn_AfterImage setEnabled:NO];
        [_btn_RecordAudio setEnabled:NO];
        [_btn_SavenContinue setEnabled:NO];
        
        //Nilind 23 Feb
        
        [_btnCash setEnabled:NO];
        [_btnCheck setEnabled:NO];
        [_btnCreditCard setEnabled:NO];
        [_btnBill setEnabled:NO];
        [_btnAutoCharger setEnabled:NO];
        [_btnPaymentPending setEnabled:NO];
        [_btnNoCharge setEnabled:NO];
        [_txtAmount setEnabled:NO];
        [_txt_DrivingLicenseNo setEnabled:NO];
        [_txtCheckNo setEnabled:NO];
        [_txtAmountSingle setEnabled:NO];
        
        
        //End
        
        [_btnHiddenCash setEnabled:NO];
        [_btnHiddenCheck setEnabled:NO];
        [_btnHiddenCreditCard setEnabled:NO];
        [_btnHiddenBill setEnabled:NO];
        [_btnHiddenAutoChargeCustomer setEnabled:NO];
        [_btnHiddenPaymentPending setEnabled:NO];
        [_btnHiddenNoCharge setEnabled:NO];
        
        //
        
        [_btnCashNew setEnabled:NO];
        [_btnCheckNew setEnabled:NO];
        [_btnCreditCardNew setEnabled:NO];
        [_btnBillNew setEnabled:NO];
        [_btnAutoChargeCustomerNew setEnabled:NO];
        [_btnPaymentPendingNew setEnabled:NO];
        [_btnNoChargeNew setEnabled:NO];
        
        
        
    }
    else
    {
        [_btnAddGraphhh setEnabled:YES];
        [_btnCustNotPrsent setEnabled:YES];
        [_btn_CustSignn setEnabled:YES];
        [_btn_TechSignn setEnabled:YES];
        //[_btn_AfterImage setEnabled:YES];
        [_btn_RecordAudio setEnabled:YES];
        [_btn_SavenContinue setEnabled:YES];
        
        //Nilind 23 Feb
        
        [_btnCash setEnabled:YES];
        [_btnCheck setEnabled:YES];
        [_btnCreditCard setEnabled:YES];
        [_btnBill setEnabled:YES];
        [_btnAutoCharger setEnabled:YES];
        [_btnPaymentPending setEnabled:YES];
        [_btnNoCharge setEnabled:YES];
        [_txtAmount setEnabled:YES];
        [_txt_DrivingLicenseNo setEnabled:YES];
        [_txtCheckNo setEnabled:YES];
        [_txtAmountSingle setEnabled:YES];
        
        //End
        
        [_btnHiddenCash setEnabled:YES];
        [_btnHiddenCheck setEnabled:YES];
        [_btnHiddenCreditCard setEnabled:YES];
        [_btnHiddenBill setEnabled:YES];
        [_btnHiddenAutoChargeCustomer setEnabled:YES];
        [_btnHiddenPaymentPending setEnabled:YES];
        [_btnHiddenNoCharge setEnabled:YES];
        
        
        //
        [_btnCashNew setEnabled:YES];
        [_btnCheckNew setEnabled:YES];
        [_btnCreditCardNew setEnabled:YES];
        [_btnBillNew setEnabled:YES];
        [_btnAutoChargeCustomerNew setEnabled:YES];
        [_btnPaymentPendingNew setEnabled:YES];
        [_btnNoChargeNew setEnabled:YES];
        
    }
}


//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================

-(void)downloadingImagess :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImage :str];
    } else {
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImage :str];
                
            });
            
        });
    }
}

-(void)ShowFirstImage :(NSString*)str{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgView_CustomerSign.image=[self loadImage:result];
    
}
-(void)downloadingImagessTechnician :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}
-(void)downloadingImagessTechnicianIfPreset :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@//Documents//%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTech :(NSString*)str{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgView_TechSign.image=[self loadImage:result];
    
}
-(void)downloadingImagessTechnicianPreSet :(NSString*)str{
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [strGlobalTechSignImage rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [strGlobalTechSignImage substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=strGlobalTechSignImage;
    }
    if (result.length==0) {
        NSRange equalRange = [strGlobalTechSignImage rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strGlobalTechSignImage substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@",str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :strGlobalTechSignImage];
    } else {
        
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :strGlobalTechSignImage];
                
            });
        });
        
    }
}

-(void)ShowFirstImageTechPreSet :(NSString*)str{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    _imgView_TechSign.image=[self loadImage:result];
    
}

- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}


//============================================================================
//============================================================================
#pragma mark -----------------------Download Audio-------------------------------
//============================================================================
//============================================================================

-(void)downloadingAudio :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        // [self ShowFirstImage :str];
        _lblAudioStatus.text=@"Audio Available";
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs setBool:YES forKey:@"yesAudio"];
        [defs synchronize];
        
    } else {
        
        NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        
        // NSURL *url = [NSURL URLWithString:strNewString];
        
        NSURL *photoURL = [NSURL URLWithString:strNewString];
        NSData *audioData = [NSData dataWithContentsOfURL:photoURL];
        [self saveAudioAfterDownload1:audioData :result];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:result forKey:@"AudioNameService"];
        [defs setBool:YES forKey:@"yesAudio"];
        [defs synchronize];
        
        _lblAudioStatus.text=@"Audio Available";
    }
}
- (void)saveAudioAfterDownload1: (NSData*)audioData :(NSString*)strAudioName {
    
    strAudioName=[global strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",strAudioName]];
    
    [audioData writeToFile:path atomically:YES];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Save Work Order Detail--------------------
//============================================================================
//============================================================================

-(void)savingWorkOrderDetails{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    BOOL isCompulsory=[defsss boolForKey:@"isCompulsoryAfterImageService"];
    if (isCompulsory) {
        
        NSArray *arrOfAfterImageLocal = [self fetchAfterImageDB];
        
        if (arrOfAfterImageLocal.count==0) {
            
            isCompulsory=YES;
            
        } else {
            
            isCompulsory=NO;
            
        }
        
    } else {
        
        isCompulsory=NO;
    }
    
    
    if (isCompulsory) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take atleast one after image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    } else {
        
        
        if (isCustomerNotPrsent) {
            
            if (strGlobalTechSignImage.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }else{
                
                if (strGlobalTechSignImage.length>0) {
                    [_workOrderDetail setValue:strGlobalTechSignImage forKey:@"technicianSignaturePath"];
                }
                //            if (strGlobalCustSignImage.length>0) {
                //                [_workOrderDetail setValue:strGlobalCustSignImage forKey:@"customerSignaturePath"];
                //            }
                if (strGlobalAudio.length>0) {
                    [_workOrderDetail setValue:strGlobalAudio forKey:@"audioFilePath"];
                }else{
                    [_workOrderDetail setValue:@"" forKey:@"audioFilePath"];
                }
                
                [_workOrderDetail setValue:_txtView_TechComment.text forKey:@"technicianComment"];
                [_workOrderDetail setValue:_txtView_OfficeNotes.text forKey:@"officeNotes"];
                NSDate *currentDate=[NSDate date];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                [formatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
                [_workOrderDetail setValue:stringCurrentDate forKey:@"timeOut"];
                
                //Nilind 08 Dec
                CLLocationCoordinate2D coordinate = [global getLocation] ;
                NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
                NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
                [_workOrderDetail setValue:latitude forKey:@"timeOutLatitude"];
                [_workOrderDetail setValue:longitude forKey:@"timeOutLongitude"];
                
                //End
                
                
                if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"InComplete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"InComplete"] == NSOrderedSame)
                {
                    
                    [_workOrderDetail setValue:@"0" forKey:@"isResendInvoiceMail"];
                    
                }
                
                [_workOrderDetail setValue:@"Completed" forKey:@"workorderStatus"];
                //[_workOrderDetail setValue:@"InComplete" forKey:@"workorderStatus"];
                
                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                BOOL isElementIntegration=[[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"] boolValue];
                
                if (isElementIntegration) {
                    
                    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Credit Card"] == NSOrderedSame || [strGlobalPaymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame)
                    {
                        
                        // if credit card so wo status incomplete and will complete after payment done
                        [_workOrderDetail setValue:@"InComplete" forKey:@"workorderStatus"];
                        
                    }
                    
                }
                if (isPreSetSignGlobal) {
                    
                    [_workOrderDetail setValue:@"true" forKey:@"isEmployeePresetSignature"];
                    
                } else {
                    
                    [_workOrderDetail setValue:@"false" forKey:@"isEmployeePresetSignature"];
                    
                }
                
                if (isCustomerNotPrsent) {
                    
                    [_workOrderDetail setValue:@"true" forKey:@"isCustomerNotPresent"];
                    
                } else {
                    
                    [_workOrderDetail setValue:@"false" forKey:@"isCustomerNotPresent"];
                    
                }
                
                if (yesEditedSomething) {
                    NSLog(@"Yes Edited Something In Db");
                    
                    [_workOrderDetail setValue:@"yes" forKey:@"zSync"];
                    
                }
                NSError *error;
                [context save:&error];
                
                //                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                //                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                //                BOOL isElementIntegration=[[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"] boolValue];
                
                if (isElementIntegration) {
                    
                    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Credit Card"] == NSOrderedSame || [strGlobalPaymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame)
                    {
                        [self sendToCreditCardView];
                        
                    }
                    else
                    {
                        [self goToSendEmailView];
                    }
                    
                } else {
                    
                    [self goToSendEmailView];
                    
                }
                
            }
            
            
        } else {
            if (strGlobalTechSignImage.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Sign the Work Order" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                
            }else if (strGlobalCustSignImage.length==0) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please take the Customer Signature" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            }else{
                
                if (strGlobalTechSignImage.length>0) {
                    [_workOrderDetail setValue:strGlobalTechSignImage forKey:@"technicianSignaturePath"];
                }
                if (strGlobalCustSignImage.length>0) {
                    [_workOrderDetail setValue:strGlobalCustSignImage forKey:@"customerSignaturePath"];
                }
                if (strGlobalAudio.length>0) {
                    [_workOrderDetail setValue:strGlobalAudio forKey:@"audioFilePath"];
                }else{
                    [_workOrderDetail setValue:@"" forKey:@"audioFilePath"];
                }
                [_workOrderDetail setValue:_txtView_TechComment.text forKey:@"technicianComment"];
                [_workOrderDetail setValue:_txtView_OfficeNotes.text forKey:@"officeNotes"];
                NSDate *currentDate=[NSDate date];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                [formatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
                [_workOrderDetail setValue:stringCurrentDate forKey:@"timeOut"];
                
                //Nilind 08 Dec
                CLLocationCoordinate2D coordinate = [global getLocation] ;
                NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
                NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
                [_workOrderDetail setValue:latitude forKey:@"timeOutLatitude"];
                [_workOrderDetail setValue:longitude forKey:@"timeOutLongitude"];
                
                //End
                
                if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"InComplete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"InComplete"] == NSOrderedSame)
                {
                    
                    [_workOrderDetail setValue:@"0" forKey:@"isResendInvoiceMail"];
                    
                }
                
                [_workOrderDetail setValue:@"Completed" forKey:@"workorderStatus"];
                //[_workOrderDetail setValue:@"InComplete" forKey:@"workorderStatus"];
                
                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                BOOL isElementIntegration=[[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"] boolValue];
                
                if (isElementIntegration) {
                    
                    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Credit Card"] == NSOrderedSame || [strGlobalPaymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame)
                    {
                        
                        // if credit card so wo status incomplete and will complete after payment done
                        [_workOrderDetail setValue:@"InComplete" forKey:@"workorderStatus"];
                        
                    }
                    
                }
                
                if (isPreSetSignGlobal) {
                    
                    [_workOrderDetail setValue:@"true" forKey:@"isEmployeePresetSignature"];
                    
                } else {
                    
                    [_workOrderDetail setValue:@"false" forKey:@"isEmployeePresetSignature"];
                    
                }
                
                if (isCustomerNotPrsent) {
                    
                    [_workOrderDetail setValue:@"true" forKey:@"isCustomerNotPresent"];
                    
                } else {
                    
                    [_workOrderDetail setValue:@"false" forKey:@"isCustomerNotPresent"];
                    
                }
                if (yesEditedSomething) {
                    NSLog(@"Yes Edited Something In Db");
                    
                    [_workOrderDetail setValue:@"yes" forKey:@"zSync"];
                    
                }
                NSError *error;
                [context save:&error];
                
                //                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                //                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                //                BOOL isElementIntegration=[[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsElementIntegration"] boolValue];
                
                if (isElementIntegration) {
                    
                    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Credit Card"] == NSOrderedSame || [strGlobalPaymentMode caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame)
                    {
                        
                        [self sendToCreditCardView];
                        
                    }
                    else
                    {
                        [self goToSendEmailView];
                    }
                    
                } else {
                    
                    [self goToSendEmailView];
                    
                }
                
            }
        }
    }
}

-(void)goToSendEmailView{
    
    NSString *strTimeInFromDb = [NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"timeIn"]];
    if ([strTimeInFromDb isEqualToString:@""] || (strTimeInFromDb.length==0) || [strTimeInFromDb isEqualToString:@"(null)"]) {
        
        [self addPickerViewForTimeIn:@"sendmail"];
        
    } else {
        
        strGlobalWorkOrderStatus=@"InComplete";
        
        if (yesEditedSomething) {
            NSLog(@"Yes Edited Something In Db");
            
            [self updateModifyDate];
            
        }
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
        
        NSString *strValue;
        
        if (isCustomerNotPrsent) {
            strValue=@"no";
        } else {
            strValue=@"yes";
        }
        
        objSendMail.isCustomerPresent=strValue;
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }
    
}
-(void)updateModifyDate{
    
    //============================================================================
    //============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
    //============================================================================
    //============================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:strWorkOrderId :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}

- (IBAction)action_ServiceDocumentss:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceDocumentsViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceDocumentsViewController"];
    objByProductVC.strAccountNo=[_workOrderDetail valueForKey:@"accountNo"];
    [self.navigationController presentViewController:objByProductVC animated:NO completion:nil];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Action Button ImageView-------------------------------
//============================================================================
//============================================================================

- (IBAction)action_ButtonImg1:(id)sender {
    
    [self goingToPreview : @"0"];
    
}

- (IBAction)action_ButtonImg2:(id)sender {
    
    
    [self goingToPreview : @"1"];
    
    
}

- (IBAction)action_ButtonImg3:(id)sender {
    
    [self goingToPreview : @"2"];
    
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Download Image-------------------------------
//============================================================================
//============================================================================
-(void)downloadingImagesThumbNailCheck{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfAfterImagesAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        //[global AlertMethod:Info :NoBeforeImg];
        _const_AfterImage_BtoImgView.constant=-120;//14
        _const_AfterImage_B.constant=10;//128
        _const_SavenContinue_B.constant=175;
        [_buttonImg1 setHidden:YES];
        [_buttonImg2 setHidden:YES];
        [_buttonImg3 setHidden:YES];
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [_imageCollectionView reloadData];
        
        
    }else if (arrOfImagesDetail.count==0){
        //[global AlertMethod:Info :NoBeforeImg];
        _const_AfterImage_BtoImgView.constant=-120;//14
        _const_AfterImage_B.constant=10;//128
        _const_SavenContinue_B.constant=175;
        [_buttonImg1 setHidden:YES];
        [_buttonImg2 setHidden:YES];
        [_buttonImg3 setHidden:YES];
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [_imageCollectionView reloadData];
        
        
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        if (arrOfImagess.count==1) {
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:YES];
            [_buttonImg3 setHidden:YES];
        } else if (arrOfImagess.count==2){
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:YES];
        }else{
            [_buttonImg1 setHidden:NO];
            [_buttonImg2 setHidden:NO];
            [_buttonImg3 setHidden:NO];
        }
        _const_AfterImage_BtoImgView.constant=14;//14
        _const_AfterImage_B.constant=128;//128
        _const_SavenContinue_B.constant=61;
        
        
        arrOfImagenameCollewctionView=nil;
        arrOfImagenameCollewctionView=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionView addObjectsFromArray:arrOfImagess];
        [_imageCollectionView reloadData];
        
        [self downloadImages:arrOfImagess];
        
        //        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        //        dispatch_async(myQueue, ^{
        //
        //            [self downloadImages:arrOfImagess];
        //
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                // Update the UI
        //            });
        //        });
        
    }
}

-(void)downloadImages :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImage:str:k];
        } else {
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            // NSURL *url = [NSURL URLWithString:strNewString];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1: image : result : k];
            
        }
        
        [_imageCollectionView reloadData];
    }
    
    
}
-(void)ShowFirstImage :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    [self ShowFirstImage : name : indexx];
    
}

//============================================================================
//============================================================================
#pragma mark -----------------------Load Image-------------------------------
//============================================================================
//============================================================================

- (UIImage*)loadImage :(NSString*)name :(int)indexxx {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    if (indexxx==0) {
        
        [_buttonImg1 setImage:image forState:UIControlStateNormal];
        
    } else if(indexxx==1) {
        
        [_buttonImg2 setImage:image forState:UIControlStateNormal];
        
        
    }else{
        
        [_buttonImg3 setImage:image forState:UIControlStateNormal];
        
    }
    if (image==nil) {
        // [global AlertMethod:Info :NoPreview];
    }
    return image;
}


-(void)goingToPreview :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfAfterImagesAll;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewGeneralInfoAppointmentView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaption;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescription;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

//============================================================================
//============================================================================
#pragma mark- -----Payment Type Radio Button Methods-----------------
//============================================================================
//============================================================================

- (IBAction)action_Cash:(id)sender {
    
    [self changePaymentMode:@"Cash"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCashSetting];
    
}
-(void)btnCashSetting{
    
    strGlobalPaymentMode=@"Cash";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self amountSingleViewSettings];
    
}
- (IBAction)action_Check:(id)sender {
    [self changePaymentMode:@"Check"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCheckSetting];
    
}
-(void)btnCheckSetting{
    
    strGlobalPaymentMode=@"Check";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self CheckViewSettings];
    
}
- (IBAction)action_CreditCard:(id)sender {
    
    [self changePaymentMode:@"CreditCard"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnCreditCArdSetting];
    
}

-(void)btnCreditCArdSetting{
    
    strGlobalPaymentMode=@"CreditCard";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self amountSingleViewSettings];
    
}
- (IBAction)action_Bill:(id)sender {
    
    [self changePaymentMode:@"Bill"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnBillSetting];
}

-(void)btnBillSetting{
    
    strGlobalPaymentMode=@"Bill";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}
- (IBAction)action_AutoChargerCust:(id)sender {
    
    [self changePaymentMode:@"AutoChargeCustomer"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnAutoChargeSetting];
    
}

-(void)btnAutoChargeSetting{
    
    strGlobalPaymentMode=@"AutoChargeCustomer";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}
- (IBAction)action_PaymentPending:(id)sender {
    
    [self changePaymentMode:@"PaymentPending"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnPaymentPendingSetting];
    
}

-(void)btnPaymentPendingSetting{
    
    strGlobalPaymentMode=@"PaymentPending";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
    
}
- (IBAction)action_NoCharge:(id)sender {
    
    [self changePaymentMode:@"NoCharge"];
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    [self btnNoChargeSetting];
    
}

-(void)btnNoChargeSetting{
    
    strGlobalPaymentMode=@"NoCharge";
    
    [_btnCash setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnBill setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnPaymentPending setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnCreditCard setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnAutoCharger setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNoCharge setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
    [self noAmountViewSettings];
    
}


- (IBAction)action_CheckExpDate:(id)sender
{
    
    [self tapDetectedScrollView];
    [_txtAmount resignFirstResponder];
    [_txtAmountSingle resignFirstResponder];
    [_txt_DrivingLicenseNo resignFirstResponder];
    [_txtCheckNo resignFirstResponder];
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    int taggggs=-7;
    strGlobalDateToShow=_btnCheckExpDate.titleLabel.text;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [self addPickerViewDateTo : taggggs];
}
//**************************************
#pragma mark- Amount And Check View Settings according To Radio Selected
//**************************************

-(void)CheckViewSettings
{
    
    noAmount=NO;
    
    
    [_amountViewSingle removeFromSuperview];
    _checkVieww.backgroundColor=[UIColor clearColor];
    
    _checkVieww.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _checkVieww.frame.size.height);
    [_scrollVieww addSubview:_checkVieww];
    
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _checkVieww.frame.origin.y+_checkVieww.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _checkVieww.frame.origin.y+_checkVieww.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10+_checkVieww.frame.size.height,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    
    _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
    
    //End
    
    
}

-(void)amountSingleViewSettings
{
    
    noAmount=NO;
    
    [_checkVieww removeFromSuperview];
    //Nilind
    //Nilind 22 Feb
    _amountViewSingle.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _amountViewSingle.frame.size.height);
    [_scrollVieww addSubview:_amountViewSingle];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10+_amountViewSingle.frame.size.height,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10+_amountViewSingle.frame.size.height,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10+_amountViewSingle.frame.size.height,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    
    _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
    //End
    
}

-(void)noAmountViewSettings
{
    noAmount=YES;
    [_checkVieww removeFromSuperview];
    [_amountViewSingle removeFromSuperview];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
        
    }
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    
    _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+20, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y+300)];
}
//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo:(int)intTagg
{
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //  [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strGlobalDateToShow.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strGlobalDateToShow];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    btnClose.tag=intTagg;
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    btnDone.tag=intTagg;
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    
    yesEditedSomething=YES;
    NSLog(@"Yes Something Edited");
    UIButton *btn=(UIButton*)sender;
    int taggggs=btn.tag ;
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString  *strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        [_btnCheckExpDate setTitle:strDate forState:UIControlStateNormal];
        strGlobalDateToShow=strDate;
        [viewForDate removeFromSuperview];
        [viewBackGround removeFromSuperview];
    }
    else
    {
        
        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
        
        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending)
        {
            
            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
            
        }else
        {
            [_btnCheckExpDate setTitle:strDate forState:UIControlStateNormal];
            strGlobalDateToShow=strDate;
            [viewForDate removeFromSuperview];
            [viewBackGround removeFromSuperview];
        }
    }
    
    
}
//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}
#pragma mark- Single Tap Methods
//============================================================================
//============================================================================
-(void)tapDetectedOnBackGroundView{
    [viewBackGround removeFromSuperview];
}
-(void)defaultViewSettingForAmount
{
    _txtAmountSingle.text=_strPaidAmount;
    strGlobalPaymentMode=_strPaymentModes;
    if ([strGlobalPaymentMode caseInsensitiveCompare:@"Cash"] == NSOrderedSame || strGlobalPaymentMode.length==0)
    {
        
        [self performSelector:@selector(btnCashSetting) withObject:nil afterDelay:0.0];
        // [self btnCashSetting];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"Credit Card"] == NSOrderedSame)
    {
        
        [self performSelector:@selector(btnCreditCArdSetting) withObject:nil afterDelay:0.0];
        //   [self btnCreditCArdSetting];
        
    }else if ([strGlobalPaymentMode caseInsensitiveCompare:@"No Charge"] == NSOrderedSame){
        
        [self performSelector:@selector(btnNoChargeSetting) withObject:nil afterDelay:0.0];
        // [self btnNoChargeSetting];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"Check"] == NSOrderedSame)
    {
        
        [self performSelector:@selector(btnCheckSetting) withObject:nil afterDelay:0.0];
        _txtAmount.text=_strPaidAmount;
        _txtCheckNo.text=[_paymentInfoDetail valueForKey:@"checkNo"];
        _txt_DrivingLicenseNo.text=[_paymentInfoDetail valueForKey:@"drivingLicenseNo"];
        [_btnCheckExpDate setTitle:[global changeDateToFormattedDateNewEXPDATE:[_paymentInfoDetail valueForKey:@"expirationDate"]]
                          forState:UIControlStateNormal] ;
        //[self btnCheckSetting];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"Payment Pending"] == NSOrderedSame){
        
        [self performSelector:@selector(btnPaymentPendingSetting) withObject:nil afterDelay:0.0];
        //  [self btnPaymentPending];
        
    }
    else if ([strGlobalPaymentMode caseInsensitiveCompare:@"Auto Charge Customer"] == NSOrderedSame){
        
        [self performSelector:@selector(btnAutoChargeSetting) withObject:nil afterDelay:0.0];
        // [self btnAutoChargeSetting];
        
        
    }
    else {
        
        [self performSelector:@selector(btnBillSetting) withObject:nil afterDelay:0.0];
        // [self btnBillSetting];
        
    }
    
}
//**************************************
#pragma mark- Image Selecting
//**************************************

- (IBAction)action_CheckFrontImage:(id)sender {
    
    // isFromBeforeImage=NO;
    isCheckFrontImage=YES;
    btnTagCheckImage=0;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
- (IBAction)action_CheckBackImage:(id)sender {
    
    //isFromBeforeImage=NO;
    isCheckFrontImage=NO;
    btnTagCheckImage=0;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Preview Image", @"Capture New", @"Gallery", nil];
    
    [actionSheet showInView:self.view];
    
}
/*
 -(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
 {
 if (btnTagCheckImage==0)
 {
 
 }
 else if (btnTagCheckImage==1)
 {
 }
 else
 {
 if (buttonIndex ==0)
 {
 if (isCheckFrontImage) {
 
 if (arrOFImagesName.count==0) {
 
 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alert show];
 
 }else{
 
 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
 [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
 [defs setBool:NO forKey:@"forCheckBackImageDelete"];
 [defs synchronize];
 
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
 bundle: nil];
 ImagePreviewGeneralInfoAppointmentView
 *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
 objByProductVC.arrOfImages=arrOFImagesName;
 objByProductVC.indexOfImage=@"0";
 objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
 //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
 [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
 }
 
 } else {
 
 if (arrOfCheckBackImage.count==0) {
 
 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alert show];
 
 }else{
 
 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
 [defs setBool:YES forKey:@"forCheckBackImageDelete"];
 [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
 [defs synchronize];
 
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
 bundle: nil];
 ImagePreviewGeneralInfoAppointmentView
 *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
 objByProductVC.arrOfImages=arrOfCheckBackImage;
 objByProductVC.indexOfImage=@"0";
 objByProductVC.statusOfWorkOrder=strWorkOrderStatuss;
 //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
 [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
 }
 }
 }
 else if (buttonIndex == 1)
 {
 NSArray *arrTemp;
 if (isCheckFrontImage) {
 
 arrTemp=arrOFImagesName;
 
 }else{
 
 arrTemp=arrOfCheckBackImage;
 
 }
 
 if (arrTemp.count>=1) {
 
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 [alert show];
 
 } else {
 NSLog(@"The CApture Image.");
 
 BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
 
 if (isCameraPermissionAvailable) {
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 
 }else{
 
 UIAlertController *alert= [UIAlertController
 alertControllerWithTitle:@"Alert"
 message:@"Camera Permission not allowed.Please go to settings and allow"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 
 
 }];
 [alert addAction:yes];
 UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
 [[UIApplication sharedApplication] openURL:url];
 } else {
 
 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 //                                     [alert show];
 
 }
 
 }];
 [alert addAction:no];
 [self presentViewController:alert animated:YES completion:nil];
 }
 
 }
 }
 else if (buttonIndex == 2)
 {
 NSArray *arrTemp;
 if (isCheckFrontImage) {
 
 arrTemp=arrOFImagesName;
 
 }else{
 
 arrTemp=arrOfCheckBackImage;
 
 }
 
 if (arrTemp.count>=1) {
 
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 [alert show];
 
 } else {
 
 BOOL isCameraPermissionAvailable=[global isGalleryPermission];
 
 if (isCameraPermissionAvailable) {
 
 UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
 imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
 imagePickController.delegate=(id)self;
 imagePickController.allowsEditing=TRUE;
 [self presentViewController:imagePickController animated:YES completion:nil];
 
 
 }else{
 
 UIAlertController *alert= [UIAlertController
 alertControllerWithTitle:@"Alert"
 message:@"Gallery Permission not allowed.Please go to settings and allow"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 
 
 }];
 [alert addAction:yes];
 UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
 [[UIApplication sharedApplication] openURL:url];
 } else {
 
 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 //                                     [alert show];
 
 }
 
 }];
 [alert addAction:no];
 [self presentViewController:alert animated:YES completion:nil];
 }
 
 }
 }
 }
 }*/


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (btnTagCheckImage==0)
    {
        
        if (buttonIndex ==0)
        {
            if (isCheckFrontImage) {
                
                if (arrOFImagesName.count==0) {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else{
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setBool:YES forKey:@"forCheckFrontImageDelete"];
                    [defs setBool:NO forKey:@"forCheckBackImageDelete"];
                    [defs synchronize];
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                             bundle: nil];
                    ImagePreviewGeneralInfoAppointmentView
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
                    objByProductVC.arrOfImages=arrOFImagesName;
                    objByProductVC.indexOfImage=@"0";
                    objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
                    //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
                    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
                }
                
            }
            else {
                
                if (arrOfCheckBackImage.count==0) {
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No Image Available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }else{
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setBool:YES forKey:@"forCheckBackImageDelete"];
                    [defs setBool:NO forKey:@"forCheckFrontImageDelete"];
                    [defs synchronize];
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                             bundle: nil];
                    ImagePreviewGeneralInfoAppointmentView
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
                    objByProductVC.arrOfImages=arrOfCheckBackImage;
                    objByProductVC.indexOfImage=@"0";
                    objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//strWorkOrderStatuss;
                    //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
                    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
                }
            }
        }
        else if (buttonIndex==1)
        {
            NSArray *arrTemp;
            if (isCheckFrontImage) {
                
                arrTemp=arrOFImagesName;
                
            }else{
                
                arrTemp=arrOfCheckBackImage;
                
            }
            
            if (arrTemp.count>=1) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check front image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            } else {
                NSLog(@"The CApture Image.");
                
                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                    
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Camera Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if ( ( (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             } else {
                                                 
                                                 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                 //                                     [alert show];
                                                 
                                             }
                                             
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }
        else if (buttonIndex == 2)
        {
            NSArray *arrTemp;
            if (isCheckFrontImage) {
                
                arrTemp=arrOFImagesName;
                
            }else{
                
                arrTemp=arrOfCheckBackImage;
                
            }
            
            if (arrTemp.count>=1) {
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You can take only one check back image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
            } else {
                
                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                    
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if ( ((&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             } else {
                                                 
                                                 //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                 //                                     [alert show];
                                                 
                                             }
                                             
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }
    }
    else
    {
        if (buttonIndex ==10)
        {
            NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
            
            
            NSArray *arrOfImagesDetail=arrOfAfterImagesAll;
            
            arrOfBeforeImages=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfImagesDetail.count; k++) {
                
                NSDictionary *dictData=arrOfImagesDetail[k];
                
                if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                    
                    [arrOfBeforeImages addObject:dictData];
                    
                }else{
                    [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
                }
            }
            
            arrOfImagesDetail=arrOfBeforeImages;
            
            if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
                [global AlertMethod:Info :NoBeforeImg];
            }else if (arrOfImagesDetail.count==0){
                [global AlertMethod:Info :NoBeforeImg];
            }
            else {
                NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
                for (int k=0; k<arrOfImagesDetail.count; k++) {
                    if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict=arrOfImagesDetail[k];
                        [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
                    }else{
                        [arrOfImagess addObject:arrOfImagesDetail[k]];
                    }
                }
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                         bundle: nil];
                ImagePreviewGeneralInfoAppointmentView
                *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewGeneralInfoAppointmentView"];
                objByProductVC.arrOfImages=arrOfImagess;
                objByProductVC.indexOfImage=@"7";
                objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
                //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
                [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
                
            }
        }
        else if (buttonIndex == 0)
        {
            
            if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
            {
            }
            else
            {
                
                
                if (arrOfAfterImagesAll.count<10)
                {
                    
                    NSLog(@"The CApture Image.");
                    
                    BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                    
                    if (isCameraPermissionAvailable) {
                        
                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                        {
                            
                            [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                            
                        }else{
                            
                            UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                            imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                            imagePickController.delegate=(id)self;
                            imagePickController.allowsEditing=TRUE;
                            [self presentViewController:imagePickController animated:YES completion:nil];
                            
                        }
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Camera Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                                                  
                                                  
                                                  
                                              }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 
                                                 if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                     [[UIApplication sharedApplication] openURL:url];
                                                 } else {
                                                     
                                                     //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                     //                                     [alert show];
                                                     
                                                 }
                                                 
                                             }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
                else
                {
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alert show];
                    
                }
            }
        }
        else if (buttonIndex == 1)
        {
            if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
            }else{
                
                if (arrOfAfterImagesAll.count<10)
                {
                    
                    BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                    
                    if (isCameraPermissionAvailable) {
                        
                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                        {
                            
                            [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                            
                        }else{
                            
                            UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                            imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                            imagePickController.delegate=(id)self;
                            imagePickController.allowsEditing=TRUE;
                            [self presentViewController:imagePickController animated:YES completion:nil];
                            
                        }
                        
                    }else{
                        
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Alert"
                                                   message:@"Gallery Permission not allowed.Please go to settings and allow"
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action)
                                              {
                                                  
                                                  
                                                  
                                              }];
                        [alert addAction:yes];
                        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 
                                                 if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                     [[UIApplication sharedApplication] openURL:url];
                                                 } else {
                                                     
                                                     //                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"Unable to redirect on settings.Please follow following Steps-Go To Settings--Pestream--Camera--Switch On" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                                     //                                     [alert show];
                                                     
                                                 }
                                                 
                                             }];
                        [alert addAction:no];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }else{
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alert show];
                    
                }
            }
        }
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (btnTagCheckImage==0)
    {
        // isFromImagePicker=YES;
        yesEditedSomething=YES;
        NSLog(@"Yes Something Edited");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\CheckImg%@%@.jpg",strDate,strTime];
        
        UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
        
        [self resizeImage:image :strImageNamess];
        
        //[self saveImage:image :strImageNamess];
        
        if (isCheckFrontImage)
        {
            
            [arrOFImagesName addObject:strImageNamess];
            
        }
        else
        {
            
            [arrOfCheckBackImage addObject:strImageNamess];
            
        }
        
        [self.navigationController dismissViewControllerAnimated: YES completion: nil];
        
        
    }
    else
    {
        yesEditedSomething=YES;
        NSLog(@"Yes Edited Something In Db");
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess = [NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        //    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
        //    [self saveImage:image :strImageNamess];
        //    if (isCustomerImage) {
        //        strGlobalCustSignImage=strImageNamess;
        //        [arrOFImagesName addObject:strImageNamess];
        //    } else {
        //        strGlobalTechSignImage=strImageNamess;
        //        [arrOFImagesName addObject:strImageNamess];
        //    }
        
        [arrOfAfterImagesAll addObject:strImageNamess];
        
        [arrOfImagenameCollewctionView addObject:strImageNamess];
        [_imageCollectionView reloadData];
        
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        
        [self resizeImage:chosenImage :strImageNamess];
        // [self saveImage:chosenImage :strImageNamess];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //Lat long code
        
        CLLocationCoordinate2D coordinate = [global getLocation] ;
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [arrImageLattitude addObject:latitude];
        [arrImageLongitude addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption) {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
            ////[self saveImageToCoreData];;
            
        }
        
        

        
    }
    
}

//Change for Image Caption

-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
}
-(void)saveMethodForCaption :(id)sender{
    
    if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
        
        if (txtFieldCaption.text.length>0) {
            
            [arrOfImageCaption addObject:txtFieldCaption.text];
            
        } else {
            
            [arrOfImageCaption addObject:@"No Caption Available..!!"];
            
        }
        
        
        if (txtViewImageDescription.text.length>0) {
            
            [arrOfImageDescription addObject:txtViewImageDescription.text];
            
        } else {
            
            [arrOfImageDescription addObject:@"No Description Available..!!"];
            
        }
        
        [viewBackAlertt removeFromSuperview];
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
        // [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
        ////[self saveImageToCoreData];;
        
        
    } else {
        
        [self AlertViewForImageCaption];
        
        //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
        
    }
    
}
-(void)cancelMethodForCaption :(id)sender{
    
    [viewBackAlertt removeFromSuperview];
    
    [arrOfImageCaption addObject:@"No Caption Available..!!"];
    [arrOfImageDescription addObject:@"No Description Available..!!"];
    
    CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
    // [_scrollVieww setContentOffset:bottomOffset animated:YES];
    
    ////[self saveImageToCoreData];;
    
}

-(void)alertToEnterImageCaption{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                              message: @""
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Caption Here...";
        textField.tag=7;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter Image Description Here...";
        textField.tag=8;
        textField.delegate=self;
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * txtHistoricalDays = textfields[0];
        UITextField * txtImageDescriptions = textfields[1];
        if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
            
            if (txtHistoricalDays.text.length>0) {
                
                [arrOfImageCaption addObject:txtHistoricalDays.text];
                
            } else {
                
                [arrOfImageCaption addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtImageDescriptions.text.length>0) {
                
                [arrOfImageDescription addObject:txtImageDescriptions.text];
                
            } else {
                
                [arrOfImageDescription addObject:@"No Description Available..!!"];
                
            }
            
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _viewPaymentInfo.frame.origin.y);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
        } else {
            
            [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [arrOfImageCaption addObject:@"No Caption Available..!!"];
        [arrOfImageDescription addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _viewPaymentInfo.frame.origin.y);
        [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark- SAVE PAYMENT DETAIL
-(void)finalSavePaymentInfo
{
    
    if ([strGlobalPaymentMode isEqualToString:@"Check"])
    {
        
        
        if (_txtCheckNo.text.length==0)
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter Check #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        //        else if (_txt_DrivingLicenseNo.text.length==0)
        //        {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Driving License #" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }
        //        else if (_btnCheckExpDate.titleLabel.text.length==0)
        //        {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Check Expiration Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }else if (arrOFImagesName.count==0) {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Front Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }else if (arrOfCheckBackImage.count==0) {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Upload Check Back Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //            [alert show];
        //
        //        }
        else{
            
            if (isPaymentInfo) {
                
                NSString *strTypePaymentToSend;
                
                if ([strGlobalPaymentMode isEqualToString:@"Credit Card"]) {
                    
                    strTypePaymentToSend=@"CreditCard";
                    
                } else if ([strGlobalPaymentMode isEqualToString:@"No Charge"]){
                    
                    strTypePaymentToSend=@"NoCharge";
                    
                }
                else if ([strGlobalPaymentMode isEqualToString:@"Auto Charge Customer"]){
                    
                    strTypePaymentToSend=@"AutoChargeCustomer";
                    
                }
                else if ([strGlobalPaymentMode isEqualToString:@"Payment Pending"]){
                    
                    strTypePaymentToSend=@"PaymentPending";
                    
                }else{
                    
                    strTypePaymentToSend=strGlobalPaymentMode;
                    
                }
                [_paymentInfoDetail setValue:strTypePaymentToSend forKey:@"paymentMode"];
                [_paymentInfoDetail setValue:_txtAmount.text forKey:@"paidAmount"];
                [_paymentInfoDetail setValue:_txtCheckNo.text forKey:@"checkNo"];
                [_paymentInfoDetail setValue:_txt_DrivingLicenseNo.text forKey:@"drivingLicenseNo"];
                [_paymentInfoDetail setValue:_btnCheckExpDate.titleLabel.text forKey:@"expirationDate"];
                if (!(arrOFImagesName.count==0)) {
                    [_paymentInfoDetail setValue:arrOFImagesName[0] forKey:@"checkFrontImagePath"];
                }
                if (!(arrOfCheckBackImage.count==0)) {
                    [_paymentInfoDetail setValue:arrOfCheckBackImage[0] forKey:@"checkBackImagePath"];
                }
                
                
                NSError *error;
                [context save:&error];
                
            }
            else
            {
                
                [self setPaymentInfoFirstTime:@"Check"];
                
            }
        }
    }
    else
    {
        if (isPaymentInfo)
        {
            
            
            NSString *strTypePaymentToSend;
            
            if ([strGlobalPaymentMode isEqualToString:@"Credit Card"]) {
                
                strTypePaymentToSend=@"CreditCard";
                
            } else if ([strGlobalPaymentMode isEqualToString:@"No Charge"]){
                
                strTypePaymentToSend=@"NoCharge";
                
            }
            else if ([strGlobalPaymentMode isEqualToString:@"Auto Charge Customer"]){
                
                strTypePaymentToSend=@"AutoChargeCustomer";
                
            }
            else if ([strGlobalPaymentMode isEqualToString:@"Payment Pending"]){
                
                strTypePaymentToSend=@"PaymentPending";
                
            }else{
                
                strTypePaymentToSend=strGlobalPaymentMode;
                
            }
            
            [_paymentInfoDetail setValue:strTypePaymentToSend forKey:@"paymentMode"];
            [_paymentInfoDetail setValue:_txtAmountSingle.text forKey:@"paidAmount"];
            NSError *error;
            [context save:&error];
            
        }
        else
        {
            
            [self setPaymentInfoFirstTime:@"NoCheck"];
            
        }
    }
}
//============================================================================
//============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
//============================================================================
//============================================================================


-(void)setPaymentInfoFirstTime :(NSString*)type{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    
    PaymentInfoServiceAuto *objPaymentInfoServiceAuto = [[PaymentInfoServiceAuto alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
    
    objPaymentInfoServiceAuto.woPaymentId=@"";
    
    objPaymentInfoServiceAuto.workorderId=strWorkOrderId;
    
    objPaymentInfoServiceAuto.paymentMode=strGlobalPaymentMode;
    
    if ([type isEqualToString:@"Check"]) {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmount.text;
        objPaymentInfoServiceAuto.checkNo=_txtCheckNo.text;
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=_btnCheckExpDate.titleLabel.text;
        if (!(arrOFImagesName.count==0)) {
            objPaymentInfoServiceAuto.checkFrontImagePath=arrOFImagesName[0];
        }else{
            objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        }
        if (!(arrOfCheckBackImage.count==0)) {
            objPaymentInfoServiceAuto.checkBackImagePath=arrOfCheckBackImage[0];
        }else{
            objPaymentInfoServiceAuto.checkBackImagePath=@"";
        }
    } else {
        
        objPaymentInfoServiceAuto.paidAmount=_txtAmountSingle.text;
        objPaymentInfoServiceAuto.checkNo=@"";
        objPaymentInfoServiceAuto.drivingLicenseNo=@"";
        objPaymentInfoServiceAuto.expirationDate=@"";
        objPaymentInfoServiceAuto.checkFrontImagePath=@"";
        objPaymentInfoServiceAuto.checkBackImagePath=@"";
        
    }
    
    objPaymentInfoServiceAuto.createdDate=@"";
    
    objPaymentInfoServiceAuto.createdBy=@"";
    
    objPaymentInfoServiceAuto.modifiedDate=@"";
    
    objPaymentInfoServiceAuto.modifiedBy=@"";
    
    objPaymentInfoServiceAuto.userName=strUserName;
    
    NSError *error;
    [context save:&error];
    
}

-(void)fetchDepartmentNameBySysName
{
    
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    
    
    //Branch Master Fetch
    
    NSArray *arrBranchMaster;
    arrBranchMaster=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    
    NSMutableArray *arrDeptVal,*arrDeptKey;
    arrDeptVal=[[NSMutableArray alloc]init];
    arrDeptKey=[[NSMutableArray alloc]init];
    
    NSDictionary *dictMasterKeyValue;
    for (int i=0; i<arrBranchMaster.count; i++)
    {
        NSDictionary *dict=[arrBranchMaster objectAtIndex:i];
        NSArray *arrDepartments=[dict valueForKey:@"Departments"];
        for (int k=0; k<arrDepartments.count; k++)
        {
            NSDictionary *dict1=[arrDepartments objectAtIndex:k];
            [arrDeptVal addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"Name"]]];
            [arrDeptKey addObject:[NSString stringWithFormat:@"%@",[dict1 valueForKey:@"SysName"]]];
        }
        
    }
    dictMasterKeyValue=[NSDictionary dictionaryWithObjects:arrDeptVal forKeys:arrDeptKey];
    dictDeptNameByKey=dictMasterKeyValue;
    NSLog(@"dictMasterKeyValue>>%@",dictMasterKeyValue);
}

-(void)changePaymentMode :(NSString*)str{
    
    _strPaymentModes=str;
    
    if ([_strPaymentModes caseInsensitiveCompare:@"NoCharge"] == NSOrderedSame) {
        _strPaymentModes=@"No Charge";
        [_lblTotalPaid setHidden:YES];
        //_lblTotalPaid.text=@"";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"CreditCard"] == NSOrderedSame) {
        _strPaymentModes=@"Credit Card";
        [_lblTotalPaid setHidden:NO];
    }else if ([_strPaymentModes caseInsensitiveCompare:@"AutoChargeCustomer"] == NSOrderedSame) {
        _strPaymentModes=@"Auto Charge Customer";
        [_lblTotalPaid setHidden:YES];
        // _lblTotalPaid.text=@"";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"PaymentPending"] == NSOrderedSame) {
        _strPaymentModes=@"Payment Pending";
        [_lblTotalPaid setHidden:YES];
        //_lblTotalPaid.text=@"";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"Bill"] == NSOrderedSame) {
        _strPaymentModes=@"Bill";
        [_lblTotalPaid setHidden:YES];
        // _lblTotalPaid.text=@"";
    }else if ([_strPaymentModes caseInsensitiveCompare:@"Cash"] == NSOrderedSame) {
        _strPaymentModes=@"Cash";
        _strPaidAmount=_txtAmountSingle.text;
        [_lblTotalPaid setHidden:NO];
        _lblTotalPaid.text=_lblTotalPaid.text=[NSString stringWithFormat:@"Total Paid Amount: $%@",_strPaidAmount];;
    }else if ([_strPaymentModes caseInsensitiveCompare:@"Check"] == NSOrderedSame) {
        _strPaymentModes=@"Check";
        _strPaidAmount=_txtAmount.text;
        [_lblTotalPaid setHidden:NO];
        _lblTotalPaid.text=_lblTotalPaid.text=[NSString stringWithFormat:@"Total Paid Amount: $%@",_strPaidAmount];;
    }
    
    _lblPaymentType.text=[NSString stringWithFormat:@"Payment Type: %@",_strPaymentModes];//[_paymentInfoDetail valueForKey:@"paymentMode"]
    
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOfEquipmentList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *strType=arrOfMoreOrLess[indexPath.row];
    
    if ([strType isEqualToString:@"More"]) {
        
        int height=[arrOfHeightsForEquipmentsCell[indexPath.row] intValue];
        // return height+365;//return tableView.rowHeight;
        
        NSDictionary *dictDataEquip=arrOfEquipmentList[indexPath.row];
        NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
        if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
            
            return height+365;
            
        }else{
            
            return 0;
            
        }
        
        
    } else {
        
        NSDictionary *dictDataEquip=arrOfEquipmentList[indexPath.row];
        NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
        if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
            
            return 365;
            
        }else{
            
            return 0;
            
        }
        
        //   return 365;//return tableView.rowHeight;
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EquipmentListInvoiceViewTableViewCell *cell = (EquipmentListInvoiceViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"EquipmentListInvoiceViewTableViewCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(EquipmentListInvoiceViewTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictDataEquip=arrOfEquipmentList[indexPath.row];
    
    cell.lbl_Category.text=[NSString stringWithFormat:@"Category:%@",[dictDataEquip valueForKey:@"CategoryName"]];
    cell.lbl_Area.text=[NSString stringWithFormat:@"Area:%@",[dictDataEquip valueForKey:@"AreaName"]];
    cell.lbl_EquipName.text=[NSString stringWithFormat:@"Equip.Name(#):%@(%@)",[dictDataEquip valueForKey:@"AreaName"],[dictDataEquip valueForKey:@"EquipmentNumber"]];
    cell.lbl_ServiceDate.text=[NSString stringWithFormat:@"Service Date:%@",[dictDataEquip valueForKey:@"strServiceDate"]];
    cell.lbl_WarrantyDate.text=[NSString stringWithFormat:@"Warranty Date:%@",[dictDataEquip valueForKey:@"strWarrantyDate"]];
    cell.lbl_AssociatedWith.text=[NSString stringWithFormat:@"AssociatedWith:%@",[dictDataEquip valueForKey:@"strAssociatedWith"]];
    cell.lbl_InstallDate.text=[NSString stringWithFormat:@"Installed On:%@",[dictDataEquip valueForKey:@"strInstalledOn"]];
    cell.lbl_LastServiceDate.text=[NSString stringWithFormat:@"Last Serviced:%@",[dictDataEquip valueForKey:@"strLastServiceDate"]];
    cell.lbl_NextServiceDate.text=[NSString stringWithFormat:@"Next Service:%@",[dictDataEquip valueForKey:@"strNextServiceDate"]];
    cell.lblSerialNo.text=[NSString stringWithFormat:@"Serial #:%@",[dictDataEquip valueForKey:@"SerialNumber"]];
    cell.lblModelNo.text=[NSString stringWithFormat:@"Model #:%@",[dictDataEquip valueForKey:@"ModelNumber"]];
    cell.lblManufacturer.text=[NSString stringWithFormat:@"Manufacturer:%@",[dictDataEquip valueForKey:@"Manufacturer"]];
    cell.btnMorenLess.tag=indexPath.row;
    
    NSString *strType=arrOfMoreOrLess[indexPath.row];
    
    if ([strType isEqualToString:@"More"]) {
        
        [cell.btnMorenLess setTitle:@"Show Less" forState:UIControlStateNormal];
        
    } else {
        
        [cell.btnMorenLess setTitle:@"Show More" forState:UIControlStateNormal];
        
    }
    
    [cell.btnMorenLess addTarget:self action:@selector(methodShowMorenLess:) forControlEvents:UIControlEventTouchDown];
    
    
    NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
    if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
        
        [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    UIView *MainViewFormNew;
    
    //For Dynamic View
    
    //============================================================================
    //============================================================================
    NSArray *arrDynamicData=arrOFEquipmentDynamicDataFromDb[indexPath.row];
    int  val;
    val=0;
    
    int constantHeighttt=80.0;
    int BtnMainViewKiHeight=0.0;
    
    CGFloat scrollViewHeight=0.0;
    
    NSDictionary *dictMain=arrOFEquipmentDynamicDataFromDb[indexPath.row];;
    
    if ([dictMain isKindOfClass:[NSString class]]) {
        
        //        [arrOfHeightsForEquipmentsCell addObject:[NSNumber numberWithInt: 0]];
        //        [arrOfMoreOrLess addObject:@"More"];
        
    } else {
        
        UIButton *BtnMainView;
        
        if (!MainViewFormNew.frame.size.height) {
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width-20, 42)];
        }
        else{
            BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, BtnMainViewKiHeight-25, [UIScreen mainScreen].bounds.size.width-20, 42)];
        }
        //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
        BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
        
        [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];
        
        BtnMainView.titleLabel.numberOfLines=2;
        
        //    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
        //    imgView.image=[UIImage imageNamed:@"minus_icon.png"];
        
        //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
        //  [BtnMainView addSubview:imgView];
        
        [cell.viewBackgroundEquipDynamicForm addSubview:BtnMainView];
        
        [arrayOfButtonsMain addObject:BtnMainView];
        
        // Form Creation
        MainViewFormNew=[[UIView alloc]init];
        
        MainViewFormNew.backgroundColor=[UIColor clearColor];
        
        MainViewFormNew.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, 1000);
        
        [cell.viewBackgroundEquipDynamicForm addSubview:MainViewFormNew];
        
        NSArray *arrSections=[dictMain valueForKey:@"Sections"];
        
        
        int yAxisViewFormSections=0.0;
        int heightVIewFormSections=0.0;
        int heightMainViewFormSections=0.0;
        
        BOOL isValuePrsent,isValuePrsentForSection;
        isValuePrsent=NO;
        isValuePrsentForSection=NO;
        
        for (int j=0; j<arrSections.count; j++)
        {
            NSDictionary *dictSection=[arrSections objectAtIndex:j];
            NSArray *arrControls=[dictSection valueForKey:@"Controls"];
            
            // Section Creation Start
            heightVIewFormSections=0.0;
            ViewFormSections=[[UIView alloc]init];
            ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width-20, 400);
            
            ViewFormSections.backgroundColor=[UIColor whiteColor];
            UILabel *lblTitleSection=[[UILabel alloc]init];
            lblTitleSection.backgroundColor=[UIColor lightGrayColor];
            lblTitleSection.layer.borderWidth = 1.5f;
            lblTitleSection.layer.cornerRadius = 4.0f;
            lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
            
            lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-20, 30);
            lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
            lblTitleSection.textAlignment=NSTextAlignmentCenter;
            [ViewFormSections addSubview:lblTitleSection];
            
            ViewFormSections.layer.borderWidth = 1.5f;
            ViewFormSections.layer.cornerRadius = 4.0f;
            ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
            
            [MainViewFormNew addSubview:ViewFormSections];
            
            CGFloat xPositionOfControls= 0.0;
            CGFloat yPositionOfControls = 0.0;
            CGFloat heightOfControls= 0.0;
            CGFloat widthOfControls= 0.0;
            
            NSMutableArray *arrOfControlsValurPrsent=[[NSMutableArray alloc]init];
            
            
            for (int k=0; k<arrControls.count; k++)
            {
                NSDictionary *dictControls=[arrControls objectAtIndex:k];
                UILabel *lblTitleControl=[[UILabel alloc]init];
                lblTitleControl.backgroundColor=[UIColor blackColor];
                lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width-20, 30);
                lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
                lblTitleControl.textAlignment=NSTextAlignmentCenter;
                // [ViewFormSections addSubview:lblTitleControl];
                
                NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                if (strIndexOfValue.length==0)
                {
                    // isValuePrsent=NO;
                }
                else
                {
                    
                    [arrOfControlsValurPrsent arrayByAddingObject:@"1"];
                    
                    isValuePrsent=YES;
                    isValuePrsentForSection=YES;
                    NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                    
                    if (!yPositionOfControls)
                    {
                        yPositionOfControls=40;
                    }
                    
                    UIView *view = [[UIView alloc] init];
                    CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                    [view setFrame:buttonFrame];
                    view.backgroundColor=[UIColor whiteColor];
                    
                    CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                    [lbl setAdjustsFontSizeToFitWidth:YES];
                    [lbl setNumberOfLines:8];
                    lbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
                    lbl.text =[dictControls valueForKey:@"Label"];
                    [view addSubview:lbl];
                    
                    
                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]) {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                        UITextView *lblItem = [[UITextView alloc] init];
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        lblItem.editable=NO;
                        [view addSubview:lblItem];
                        //  view.backgroundColor=[UIColor redColor];
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lbl.frame.size.height+12+20+50)];
                        
                    }
                    else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]){
                        
                        NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                        NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                        NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                        
                        for (int k=0; k<arrOfValues.count; k++) {
                            
                            for (int l=0; l<arrOfOptions.count; l++) {
                                
                                NSDictionary *dictData=arrOfOptions[l];
                                NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                                
                                if ([strValue isEqualToString:arrOfValues[k]]) {
                                    
                                    if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]) {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                        
                                    } else {
                                        [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                        
                        CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:1000];
                        lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        
                    }
                    else
                    {
                        
                        CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                        UILabel *lblItem = [[UILabel alloc] init];
                        if (s11.height<30) {
                            
                            s11.height=30;
                            
                        }
                        lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                        [lblItem setFont:[UIFont systemFontOfSize:15]];
                        //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                        [lblItem setNumberOfLines:1000];
                        lblItem.text =[dictControls valueForKey:@"Value"];
                        [view addSubview:lblItem];
                        
                        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                        
                    }
                    
                    heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                    
                    yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                    
                    [ViewFormSections addSubview:view];
                    
                }
            }
            
            if (!isValuePrsent)
            {
                
                BtnMainViewKiHeight=85;
                
                MainViewFormNew.frame=CGRectMake(0, MainViewFormNew.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, 0);
                constantHeighttt=80;
                
                // [BtnMainView removeFromSuperview];
                if (arrayOfButtonsMain.count==0) {
                    
                } else {
                    int countt=(int)arrayOfButtonsMain.count;
                    if (countt==0) {
                        UIButton *btnTempNew=arrayOfButtonsMain[countt];
                        [btnTempNew setHidden:YES];
                    } else {
                        UIButton *btnTempNew=arrayOfButtonsMain[countt-1];
                        [btnTempNew setHidden:YES];
                    }
                    [arrayOfButtonsMain removeLastObject];
                }
                [ViewFormSections removeFromSuperview];
            }
            else
            {
                isValuePrsent=NO;
                
                [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
                
                heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;//10
                
                MainViewFormNew.frame=CGRectMake(0, MainViewFormNew.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                
                BtnMainViewKiHeight=MainViewForm.frame.origin.y+MainViewFormNew.frame.size.height;
                
                yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
                
                constantHeighttt=MainViewFormNew.frame.size.height+constantHeighttt;
                
            }
        }
        
    }
    
    if ([arrDynamicData isKindOfClass:[NSString class]]) {
        
        [MainViewFormNew removeFromSuperview];
        [cell.viewBackgroundEquipDynamicForm setFrame:CGRectMake(0, 0, 0, 0)];
        
    }else  if (arrDynamicData.count==0) {
        [MainViewFormNew removeFromSuperview];
        [cell.viewBackgroundEquipDynamicForm setFrame:CGRectMake(0, 0, 0, 0)];
    }else{
        [cell.viewBackgroundEquipDynamicForm setFrame:CGRectMake(cell.viewBackgroundEquipDynamicForm.frame.origin.x, cell.lblManufacturer.frame.origin.y+cell.lblManufacturer.frame.size.height, [UIScreen mainScreen].bounds.size.width-20, MainViewFormNew.frame.size.height)];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Cell Button Methods-----------------
//============================================================================
//============================================================================

-(void)methodShowMorenLess:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSString *strType=arrOfMoreOrLess[btn.tag];
    
    if ([strType isEqualToString:@"More"]) {
        
        [self setDynamicHeightsForEquipmentOnClickOfMorenLessButton:@"Less" :(int)btn.tag];
        
        [arrOfMoreOrLess replaceObjectAtIndex:btn.tag withObject:@"Less"];
        
    } else {
        
        [self setDynamicHeightsForEquipmentOnClickOfMorenLessButton:@"More" :(int)btn.tag];
        
        [arrOfMoreOrLess replaceObjectAtIndex:btn.tag withObject:@"More"];
        
    }
    
    //    NSDictionary *dictDataEquip=arrOfEquipmentList[btn.tag];
    //    NSString *strWoEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"WOEquipmentId"]];
    //    if (strWoEquipIdd.length==0) {
    //
    //        NSString *strMobileEquipIdd=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"MobileUniqueId"]];
    //        strGlobalWoEquipIdToFetchDynamicData=strMobileEquipIdd;
    //        isWoEquipIdPrsent=NO;
    //    } else {
    //        isWoEquipIdPrsent=YES;
    //        strGlobalWoEquipIdToFetchDynamicData=strWoEquipIdd;
    //    }
    //
    //    [self FetchFromCoreDataToShowEquipmentDynamic];
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [_tblview_EquipmentList reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    
}
//    [self findHeightForRowsAtIndexPaths];

-(void)FetchFromCoreDataToShowEquipmentDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate;
    if (isWoEquipIdPrsent) {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    } else {
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND mobileEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    }
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@ AND woEquipId = %@",strWorkOrderId,strUsername,strGlobalWoEquipIdToFetchDynamicData];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        [arrOFEquipmentDynamicDataFromDb addObject:@""];
    }
    else
    {
        matches=arrAllObj[0];
        NSArray *arrTemp = [matches valueForKey:@"arrFinalInspection"];
        NSDictionary *dictDataService;
        if ([arrTemp isKindOfClass:[NSDictionary class]]) {
            
            dictDataService=(NSDictionary*)arrTemp;
            
        } else {
            
            dictDataService=arrTemp[0];
            
        }
        [arrOFEquipmentDynamicDataFromDb addObject:dictDataService];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
    
    
}

-(void)setDynamicHeightsForEquipment{
    
    int sum=0;
    for(int x=0; x < [arrOfHeightsForEquipmentsCell count]; x++)
    {
        
        NSDictionary *dictDataEquip=arrOfEquipmentList[x];
        NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
        if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
            
            sum += [[arrOfHeightsForEquipmentsCell objectAtIndex:x] intValue];
            
            
        }else{
            
            //return 0;
            
        }
        
        // sum += [[arrOfHeightsForEquipmentsCell objectAtIndex:x] intValue];
    }
    
    NSMutableArray *arrTemps=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfEquipmentList.count; k++) {
        
        NSDictionary *dictDataEquip=arrOfEquipmentList[k];
        NSString *strServiceStatus=[NSString stringWithFormat:@"%@",[dictDataEquip valueForKey:@"IsServiceStatus"]];
        if([strServiceStatus isEqualToString:@"true"] || [strServiceStatus isEqualToString:@"1"]){
            
            [arrTemps addObject:@"1"];
            
        }else{
            
            //return 0;
            
        }
        
        
    }
    
    _tblview_EquipmentList.frame=CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, arrTemps.count*365+sum);
    
    _view_EquipmentList.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y,[UIScreen mainScreen].bounds.size.width, arrTemps.count*365+sum+50);
    
    CGRect frameFor_viewPaymentInfo;
    
    
    if (arrOfEquipmentList.count==0) {
        
        NSMutableArray *arrOfCombineChemicalList=[[NSMutableArray alloc]init];
        
        NSString *hopProfile = @"ProductName";
        
        NSSortDescriptor *hopProfileDescriptor =
        [[NSSortDescriptor alloc] initWithKey:hopProfile
                                    ascending:YES];
        
        NSArray *descriptors = [NSArray arrayWithObjects:hopProfileDescriptor, nil];
        NSArray *sortedArrayOfDictionaries1 = [_arrChemicalList sortedArrayUsingDescriptors:descriptors];
        NSArray *sortedArrayOfDictionaries2 = [_arrOfChemicalListOther sortedArrayUsingDescriptors:descriptors];
        
        [arrOfCombineChemicalList addObjectsFromArray:sortedArrayOfDictionaries1];
        [arrOfCombineChemicalList addObjectsFromArray:sortedArrayOfDictionaries2];
        
        NSArray *arrCombined=(NSArray*)arrOfCombineChemicalList;
        
        NSArray *arrOfProductMaster=arrCombined;
        
        frameFor_viewPaymentInfo=CGRectMake(10, 200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameFor_viewPaymentInfo.origin.x=10;
        frameFor_viewPaymentInfo.origin.y=200*arrOfProductMaster.count+100+_view_CustomerInfo.frame.origin.y+ViewFormSections.frame.origin.y+_view_CustomerInfo.frame.size.height+ViewFormSections.frame.size.height+20;
        
        
    } else {
        
        frameFor_viewPaymentInfo=CGRectMake(10, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
        frameFor_viewPaymentInfo.origin.x=10;
        frameFor_viewPaymentInfo.origin.y=_view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+20;
        
    }
    [_paymentTypeView setFrame:frameFor_viewPaymentInfo];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    _viewTermsnConditions.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height-160, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewPaymentInfo.frame.size.height+_viewPaymentInfo.frame.origin.y+_viewTermsnConditions.frame.size.height)];
    
}
-(void)setDynamicHeightsForEquipmentOnClickOfMorenLessButton :(NSString*)strType :(int)indexpath{
    
    int sum=0;
    
    sum = [[arrOfHeightsForEquipmentsCell objectAtIndex:indexpath] intValue];
    
    
    /*
     for(int x=0; x<[arrOfHeightsForEquipmentsCell count]; x++)
     {
     if (x==indexpath) {
     
     if ([strType isEqualToString:@"More"]) {
     
     sum += [[arrOfHeightsForEquipmentsCell objectAtIndex:x] intValue];
     
     } else {
     
     sum -= [[arrOfHeightsForEquipmentsCell objectAtIndex:x] intValue];
     
     }
     
     } else {
     
     sum += [[arrOfHeightsForEquipmentsCell objectAtIndex:x] intValue];
     
     }
     }
     */
    
    if ([strType isEqualToString:@"More"]) {
        
        _tblview_EquipmentList.frame=CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, _tblview_EquipmentList.frame.size.height+sum);
        
        _view_EquipmentList.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y,[UIScreen mainScreen].bounds.size.width, _view_EquipmentList.frame.size.height+sum);
        
    } else {
        
        _tblview_EquipmentList.frame=CGRectMake(0, 50, [UIScreen mainScreen].bounds.size.width, _tblview_EquipmentList.frame.size.height-sum);
        
        _view_EquipmentList.frame=CGRectMake(0, _view_EquipmentList.frame.origin.y,[UIScreen mainScreen].bounds.size.width, _view_EquipmentList.frame.size.height-sum);
        
    }
    
    
    CGRect frameFor_viewPaymentInfo;
    
    frameFor_viewPaymentInfo=CGRectMake(10, _view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+20, self.view.frame.size.width-20, _paymentTypeView.frame.size.height);
    frameFor_viewPaymentInfo.origin.x=10;
    frameFor_viewPaymentInfo.origin.y=_view_EquipmentList.frame.origin.y+_view_EquipmentList.frame.size.height+20;
    
    [_paymentTypeView setFrame:frameFor_viewPaymentInfo];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    _viewTermsnConditions.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height-160, [UIScreen mainScreen].bounds.size.width-20, _viewTermsnConditions.frame.size.height);
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewPaymentInfo.frame.size.height+_viewPaymentInfo.frame.origin.y+_viewTermsnConditions.frame.size.height)];
    
}

-(void)findHeightForRowsAtIndexPaths{
    
    arrOfHeightsForEquipmentsCell=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOFEquipmentDynamicDataFromDb.count; k++) {
        
        //============================================================================
        //============================================================================
        NSArray *arrDynamicData=arrOFEquipmentDynamicDataFromDb[k];
        int  val;
        val=0;
        
        int constantHeighttt=80.0;
        int BtnMainViewKiHeight=0.0;
        
        CGFloat scrollViewHeight=0.0;
        
        NSDictionary *dictMain=arrOFEquipmentDynamicDataFromDb[k];
        
        if ([dictMain isKindOfClass:[NSString class]]) {
            
            [arrOfHeightsForEquipmentsCell addObject:[NSNumber numberWithInt: 0]];
            [arrOfMoreOrLess addObject:@"More"];
            
        } else {
            
            
            
            UIButton *BtnMainView;
            
            if (!MainViewForm.frame.size.height) {
                BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width-20, 42)];
            }
            else{
                BtnMainView=[[UIButton alloc]initWithFrame:CGRectMake(0, BtnMainViewKiHeight-25, [UIScreen mainScreen].bounds.size.width-20, 42)];
            }
            //  [BtnMainView addTarget:self action:@selector(actionOpenCloseMAinView:) forControlEvents:UIControlEventTouchDown];
            BtnMainView.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];//[UIColor blackColor];
            
            // [BtnMainView setTitle:[dictMain valueForKey:@"DepartmentSysName"] forState:UIControlStateNormal];
            
            //    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 40, 40)];
            //    imgView.image=[UIImage imageNamed:@"minus_icon.png"];
            
            //scrollViewHeight=scrollViewHeight+BtnMainView.frame.size.height;
            //  [BtnMainView addSubview:imgView];
            
            
            [arrayOfButtonsMain addObject:BtnMainView];
            
            // Form Creation
            MainViewForm=[[UIView alloc]init];
            
            MainViewForm.backgroundColor=[UIColor clearColor];
            
            MainViewForm.frame=CGRectMake(0, BtnMainView.frame.origin.y+BtnMainView.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, 1000);
            
            NSArray *arrSections=[dictMain valueForKey:@"Sections"];
            
            
            int yAxisViewFormSections=0.0;
            int heightVIewFormSections=0.0;
            int heightMainViewFormSections=0.0;
            
            BOOL isValuePrsent,isValuePrsentForSection;
            isValuePrsent=NO;
            isValuePrsentForSection=NO;
            
            for (int j=0; j<arrSections.count; j++)
            {
                NSDictionary *dictSection=[arrSections objectAtIndex:j];
                NSArray *arrControls=[dictSection valueForKey:@"Controls"];
                
                // Section Creation Start
                heightVIewFormSections=0.0;
                ViewFormSections=[[UIView alloc]init];
                ViewFormSections.frame=CGRectMake(0, yAxisViewFormSections, [UIScreen mainScreen].bounds.size.width-20, 400);
                
                ViewFormSections.backgroundColor=[UIColor whiteColor];
                UILabel *lblTitleSection=[[UILabel alloc]init];
                lblTitleSection.backgroundColor=[UIColor lightGrayColor];
                lblTitleSection.layer.borderWidth = 1.5f;
                lblTitleSection.layer.cornerRadius = 4.0f;
                lblTitleSection.layer.borderColor = [[UIColor grayColor] CGColor];
                
                lblTitleSection.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-20, 30);
                lblTitleSection.text=[NSString stringWithFormat:@"%@",[dictSection valueForKey:@"Name"]] ;
                lblTitleSection.textAlignment=NSTextAlignmentCenter;
                [ViewFormSections addSubview:lblTitleSection];
                
                ViewFormSections.layer.borderWidth = 1.5f;
                ViewFormSections.layer.cornerRadius = 4.0f;
                ViewFormSections.layer.borderColor = [[UIColor grayColor] CGColor];
                
                [MainViewForm addSubview:ViewFormSections];
                
                CGFloat xPositionOfControls= 0.0;
                CGFloat yPositionOfControls = 0.0;
                CGFloat heightOfControls= 0.0;
                CGFloat widthOfControls= 0.0;
                
                NSMutableArray *arrOfControlsValurPrsent=[[NSMutableArray alloc]init];
                
                
                for (int k=0; k<arrControls.count; k++)
                {
                    NSDictionary *dictControls=[arrControls objectAtIndex:k];
                    UILabel *lblTitleControl=[[UILabel alloc]init];
                    lblTitleControl.backgroundColor=[UIColor blackColor];
                    lblTitleControl.frame=CGRectMake(0, 30*k+35, [UIScreen mainScreen].bounds.size.width-20, 30);
                    lblTitleControl.text=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Label"]] ;
                    lblTitleControl.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
                    lblTitleControl.textAlignment=NSTextAlignmentCenter;
                    // [ViewFormSections addSubview:lblTitleControl];
                    
                    NSString *strIndexOfValue=[NSString stringWithFormat:@"%@",[dictControls valueForKey:@"Value"]];
                    if (strIndexOfValue.length==0)
                    {
                        // isValuePrsent=NO;
                    }
                    else
                    {
                        
                        [arrOfControlsValurPrsent arrayByAddingObject:@"1"];
                        
                        isValuePrsent=YES;
                        isValuePrsentForSection=YES;
                        NSArray *arrOfIndexChecked=[strIndexOfValue componentsSeparatedByString:@","];
                        
                        if (!yPositionOfControls)
                        {
                            yPositionOfControls=40;
                        }
                        
                        UIView *view = [[UIView alloc] init];
                        CGRect buttonFrame = CGRectMake(0, yPositionOfControls,heightOfControls, widthOfControls);
                        [view setFrame:buttonFrame];
                        view.backgroundColor=[UIColor whiteColor];
                        
                        CGSize s = [[dictControls valueForKey:@"Label"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width-30, s.height)];
                        [lbl setAdjustsFontSizeToFitWidth:YES];
                        [lbl setNumberOfLines:8];
                        lbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
                        lbl.text =[dictControls valueForKey:@"Label"];
                        [view addSubview:lbl];
                        
                        
                        if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-textarea"]) {
                            
                            CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                            UITextView *lblItem = [[UITextView alloc] init];
                            lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, 65);
                            [lblItem setFont:[UIFont systemFontOfSize:15]];
                            //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                            lblItem.text =[dictControls valueForKey:@"Value"];
                            lblItem.editable=NO;
                            [view addSubview:lblItem];
                            //  view.backgroundColor=[UIColor redColor];
                            
                            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, lbl.frame.size.height+12+20+50)];
                            
                        }
                        else if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-checkbox"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio-combo"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-radio"] || [[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]){
                            
                            NSArray *arrOfOptions=[dictControls valueForKey:@"Options"];
                            NSArray *arrOfValues=[[dictControls valueForKey:@"Value"] componentsSeparatedByString:@","];
                            NSMutableArray *arrOfValuesToSet=[[NSMutableArray alloc]init];
                            
                            for (int k=0; k<arrOfValues.count; k++) {
                                
                                for (int l=0; l<arrOfOptions.count; l++) {
                                    
                                    NSDictionary *dictData=arrOfOptions[l];
                                    NSString *strValue=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Value"]];
                                    
                                    if ([strValue isEqualToString:arrOfValues[k]]) {
                                        
                                        if ([[dictControls valueForKey:@"Element"] isEqualToString:@"el-multi-select"]) {
                                            [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]]];
                                            
                                        } else {
                                            [arrOfValuesToSet addObject:[NSString stringWithFormat:@"%@",[dictData valueForKey:@"Label"]]];
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            NSString *strTextView=[arrOfValuesToSet componentsJoinedByString:@"\n \n"];
                            
                            CGSize s11 = [strTextView sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                            UILabel *lblItem = [[UILabel alloc] init];
                            if (s11.height<30) {
                                
                                s11.height=30;
                                
                            }
                            lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                            [lblItem setFont:[UIFont systemFontOfSize:15]];
                            //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                            [lblItem setNumberOfLines:1000];
                            lblItem.text =strTextView;//[dictControls valueForKey:@"Value"];
                            [view addSubview:lblItem];
                            
                            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                            
                        }
                        else
                        {
                            
                            CGSize s11 = [[dictControls valueForKey:@"Value"] sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, MAXFLOAT)];
                            UILabel *lblItem = [[UILabel alloc] init];
                            if (s11.height<30) {
                                
                                s11.height=30;
                                
                            }
                            lblItem.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y+lbl.frame.size.height+10, lbl.frame.size.width-20, s11.height);
                            [lblItem setFont:[UIFont systemFontOfSize:15]];
                            //  lblItem.center=CGPointMake(lblItem.center.x, btn.center.y);
                            [lblItem setNumberOfLines:1000];
                            lblItem.text =[dictControls valueForKey:@"Value"];
                            [view addSubview:lblItem];
                            
                            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, [UIScreen mainScreen].bounds.size.width, s11.height+12+20)];
                            
                        }
                        
                        heightVIewFormSections=view.frame.size.height+heightVIewFormSections;
                        
                        yPositionOfControls=view.frame.origin.y+view.frame.size.height+10;
                        
                        [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, lbl.frame.size.height+12+10)];
                        
                        [ViewFormSections addSubview:view];
                        
                    }
                }
                
                if (!isValuePrsent)
                {
                    
                    BtnMainViewKiHeight=85;
                    
                    MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, 0);
                    constantHeighttt=80;
                    
                    // [BtnMainView removeFromSuperview];
                    if (arrayOfButtonsMain.count==0) {
                        
                    } else {
                        int countt=(int)arrayOfButtonsMain.count;
                        if (countt==0) {
                            UIButton *btnTempNew=arrayOfButtonsMain[countt];
                            [btnTempNew setHidden:YES];
                        } else {
                            UIButton *btnTempNew=arrayOfButtonsMain[countt-1];
                            [btnTempNew setHidden:YES];
                        }
                        [arrayOfButtonsMain removeLastObject];
                    }
                    [ViewFormSections removeFromSuperview];
                }
                else
                {
                    isValuePrsent=NO;
                    
                    [ViewFormSections setFrame:CGRectMake(ViewFormSections.frame.origin.x, ViewFormSections.frame.origin.y, ViewFormSections.frame.size.width, heightVIewFormSections+arrControls.count*10+50)];
                    
                    heightMainViewFormSections=heightMainViewFormSections+heightVIewFormSections+arrControls.count*10+50;//10
                    
                    MainViewForm.frame=CGRectMake(0, MainViewForm.frame.origin.y, [UIScreen mainScreen].bounds.size.width-20, heightMainViewFormSections+60);
                    
                    BtnMainViewKiHeight=MainViewForm.frame.origin.y+MainViewForm.frame.size.height;
                    
                    yAxisViewFormSections=ViewFormSections.frame.origin.y+ViewFormSections.frame.size.height+10;
                    
                    constantHeighttt=MainViewForm.frame.size.height+constantHeighttt;
                    
                }
            }
            if (arrDynamicData.count==0) {
                // [MainViewForm removeFromSuperview];
            }
            
            int heightToSave=MainViewForm.frame.size.height;
            [arrOfHeightsForEquipmentsCell addObject:[NSNumber numberWithInt: heightToSave]];
            [arrOfMoreOrLess addObject:@"More"];
            
        }
        
    }
    
    [self setDynamicHeightsForEquipment];
    
    [_tblview_EquipmentList reloadData];
    
}
-(void)saveCommentDetailsOnBack{
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
    {
        
        
        
    }else{
        
        [_workOrderDetail setValue:_txtView_TechComment.text forKey:@"technicianComment"];
        [_workOrderDetail setValue:_txtView_OfficeNotes.text forKey:@"officeNotes"];
        NSError *error;
        [context save:&error];
        
    }
    
}
- (IBAction)action_ServiceJobDescriptionTitle:(id)sender{
    
    if ([_btnServiceJobDescriptionTitle.titleLabel.text isEqualToString:@"Select"]) {
        
    }else
    {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:_btnServiceJobDescriptionTitle.titleLabel.text
                                   message:@""
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


//============================================================================
//============================================================================
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //Graph
    if (collectionView==_collectionViewGraph) {
        
        return arrOfImagenameCollewctionViewGraph.count;
        
    }
    else if (collectionView==_collectionView7A)
    {
        return arrNoImage.count;
    }
    else if (collectionView==_collectionView8)
    {
        return arrNoImageCollectionView2.count;
    }
    else
    {
        
        return arrOfImagenameCollewctionView.count;
        
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView==_imageCollectionView)
    {
        
        
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        
        NSString *str = [arrOfImagenameCollewctionView objectAtIndex:indexPath.row];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        return cell;
        
    }
    else if (collectionView == _collectionView7A)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
            //str=[arrNoImage objectAtIndex:indexPath.row];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else if (collectionView == _collectionView8)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
            
            // str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        }
        else
        {
            str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
    else{
        
        //Graph
        static NSString *identifier = @"ServiceGraphCollectionViewCell";
        
        ServiceGraphCollectionViewCell *cell = (ServiceGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        //    cell.selected=YES;
        //    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        return cell;
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView==_collectionViewGraph) {
        
        //Graph
        NSString *str;
        if ([[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictDataa=[arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
            str = [dictDataa valueForKey:@"woImagePath"];
            
        } else {
            
            str = [arrOfImagenameCollewctionViewGraph objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewGraph : strIndex];
            
        }
        
    }
    else if (collectionView==_collectionView7A)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewFor7A : strIndex];
            
        }
    }
    else if(collectionView==_collectionView8)
    {
        
        NSString *str;
        NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewFor8 : strIndex];
            
        }
        
    }
    else {
        
        NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        [self goingToPreview : strIndex];
        
    }
    
}
#pragma mark- ----------- CREDIT CARD METHOD --------------

-(void)sendToCreditCardView
{
    
    NSString *strTimeInFromDb = [NSString stringWithFormat:@"%@",[_workOrderDetail valueForKey:@"timeIn"]];
    
    if ([strTimeInFromDb isEqualToString:@""] || (strTimeInFromDb.length==0) || [strTimeInFromDb isEqualToString:@"(null)"]) {
        
        [self addPickerViewForTimeIn:@"sendcreditcardview"];
        
    } else {
        
        if (yesEditedSomething) {
            NSLog(@"Yes Edited Something In Db");
            
            [self updateModifyDate];
            
        }
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [global AlertMethod:@"ALert" :ErrorInternetMsgPayment];
            [DejalActivityView removeView];
        }
        else
        {
            UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
            CreditCardIntegration *objCreditCard=[storyBoard instantiateViewControllerWithIdentifier:@"CreditCardIntegration"];
            objCreditCard.strAmount=_txtAmountSingle.text;
            objCreditCard.strGlobalWorkOrderId=strWorkOrderId;
            objCreditCard.strTypeOfService=@"service";
            objCreditCard.workOrderDetailNew=_workOrderDetail;
            NSString *strValue;
            
            if (isCustomerNotPrsent) {
                strValue=@"no";
            } else {
                strValue=@"yes";
            }
            
            objCreditCard.isCustomerPresent=strValue;
            
            
            [self.navigationController pushViewController:objCreditCard animated:NO];
        }
        
    }
    
}
//Nilind 26 Sept
//Graph
-(void)deleteGraphImagesBeforeSaving{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",strWorkOrderId,@"Graph"];
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

//Graph
-(void)fetchGraphImageDetailFromDataBase{
    
    arrImageGraphLattitude=[[NSMutableArray alloc]init];
    arrImageGraphLongitude=[[NSMutableArray alloc]init];
    
    arrOfBeforeImageAllGraph=nil;
    arrOfBeforeImageAllGraph=[[NSMutableArray alloc]init];
    
    arrOfImagenameCollewctionViewGraph=nil;
    arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //  NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    NSArray *arrAllObjImageDetailNew = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetailNew count] == 0)
    {
        
        
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetailNew.count; j++) {
            
            NSManagedObject *matchesImageDetailNew=arrAllObjImageDetailNew[j];
            NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"Graph"]) {
                
                NSString *companyKey=[matchesImageDetailNew valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetailNew valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetailNew valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetailNew valueForKey:@"woImageId"];
                NSString *woImagePath=[matchesImageDetailNew valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetailNew valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetailNew valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetailNew valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetailNew valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetailNew valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetailNew valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetailNew valueForKey:@"longitude"];
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrOfBeforeImageAllGraph addObject:dict_ToSendLeadInfo];
                [arrOfImagenameCollewctionViewGraph addObject:dict_ToSendLeadInfo];
                
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionGraph addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionGraph addObject:strImageCaption];
                    
                }
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionGraph addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionGraph addObject:strImageDescription];
                    
                }
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetailNew valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLattitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLattitude addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageGraphLongitude addObject:@""];
                    
                } else {
                    
                    [arrImageGraphLongitude addObject:strLong];
                    
                }
                
                
            }
        }
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(NSArray*)fetchAfterImageDB{

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && woImageType = %@",strWorkOrderId, @"After"];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    NSArray *arrAllObjImageDetailNew = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetailNew count] == 0)
    {
        
        
        
    }
    else
    {
        
        
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrAllObjImageDetailNew;
    
}

//Graph
-(void)downloadGraphImage
{
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        
    }else if (arrOfImagesDetail.count==0){
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        arrOfImagenameCollewctionViewGraph=nil;
        arrOfImagenameCollewctionViewGraph=[[NSMutableArray alloc]init];
        [arrOfImagenameCollewctionViewGraph addObjectsFromArray:arrOfImagess];
        
        [self downloadImagesGraph:arrOfImagess];
        
    }
}

//Graph
//For Graph Download Code
//============================================================================
//============================================================================
#pragma mark -----------------------Download Graph Images-------------------------------
//============================================================================
//============================================================================

-(void)downloadImagesGraph :(NSArray*)arrOfImagesDownload{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str = [arrOfImagesDownload objectAtIndex:k];
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists) {
            [self ShowFirstImageGraph:str:k];
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload1Graph: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    [_collectionViewGraph reloadData];
}

-(void)ShowFirstImageGraph :(NSString*)str :(int)indexxx{
    
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [self loadImage : result : indexxx];
}
- (void)saveImageAfterDownload1Graph: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
    if (indexx==arrOfImagenameCollewctionViewGraph.count-1) {
        
        [_collectionViewGraph reloadData];
        
    }
    
    [_collectionViewGraph reloadData];
    
    //    [self ShowFirstImage : name : indexx];
    
}

//Graph
-(void)goingToPreviewGraph :(NSString*)indexxx{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrOfBeforeImageAllGraph;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImagePreviewViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImagePreviewViewController"];
        objImagePreviewSalesAuto.arrOfImages=arrOfImagess;
        objImagePreviewSalesAuto.indexOfImage=indexxx;
        objImagePreviewSalesAuto.strLeadId=strWorkOrderId;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrOfImageCaptionGraph;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrOfImageDescriptionGraph;
        objImagePreviewSalesAuto.strFromWhere=@"Service";
        objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",strGlobalWorkOrderStatus];
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
    }
}

//Graph
- (IBAction)action_GraphImage:(id)sender {
    
    [_collectionViewGraph reloadData];
    
    CGRect frameFor_view_BeforeImageInfo=CGRectMake(0, _viewFooter.frame.origin.y-_viewGraphImage.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewGraphImage.frame.size.height);
    [_viewGraphImage setFrame:frameFor_view_BeforeImageInfo];
    [self.view addSubview:_viewGraphImage];
    
}
//Graph
- (IBAction)action_AddGraph:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"firstGraphImage"];
    [defs setBool:YES forKey:@"servicegraph"];
    [defs setBool:YES forKey:@"isForNewGraph"];// Akshay 21 May 2019
    [defs synchronize];
    
    /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
     EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
     objSignViewController.strLeadId=strWorkOrderId;
     objSignViewController.strCompanyKey=strCompanyKey;
     objSignViewController.strUserName=strUserName;
     [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
    
    
    ////Akshay 22 may 2019
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
    objSignViewController.strLeadId=strWorkOrderId;
    objSignViewController.strCompanyKey=strCompanyKey;
    objSignViewController.strUserName=strUserName;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    ////
}
//Graph
- (IBAction)action_CloseGraphView:(id)sender {
    
    [_viewGraphImage removeFromSuperview];
    
}


-(void)fetchWoEquipmentFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextWoEquipment = [appDelegate managedObjectContext];
    entityWoEquipment=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:contextWoEquipment];
    requestWoEquipment = [[NSFetchRequest alloc] init];
    [requestWoEquipment setEntity:entityWoEquipment];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestWoEquipment setPredicate:predicate];
    
    sortDescriptorWoEquipment = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWoEquipment = [NSArray arrayWithObject:sortDescriptorWoEquipment];
    
    [requestWoEquipment setSortDescriptors:sortDescriptorsWoEquipment];
    
    self.fetchedResultsControllerWoEquipmentService = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWoEquipment managedObjectContext:contextWoEquipment sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWoEquipmentService setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWoEquipmentService performFetch:&error];
    arrAllObjWoEquipment = [self.fetchedResultsControllerWoEquipmentService fetchedObjects];
    if ([arrAllObjWoEquipment count] == 0)
    {
        
    }
    else
    {
        matchesWoEquipment=arrAllObjWoEquipment[0];
        
        _arrOfEquipmentLists=[[NSMutableArray alloc]init];
        
        [_arrOfEquipmentLists addObjectsFromArray:[matchesWoEquipment valueForKey:@"arrOfEquipmentList"]];
        
        
    }
    
    // [self addingEquipmentView];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


//============================================================================
#pragma mark- Fetch From DB Values For Termite Static Form
//============================================================================

-(void)getDataFrmDB{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        [self showValuesSavedInDb];
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"];
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"]
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        
        NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        dictDataTermite=[self nestedDictionaryByReplacingNullsWithNil:dictDataTermite];
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        [dictGlobalTermiteTexas addEntriesFromDictionary:dictDataTermite];
        [self showValuesSavedInDb];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(void)showValuesSavedInDb{
    
    //([[dictData valueForKey:@"ModifiedDate"] isKindOfClass:[NSNull class]]) ? @"" : [dictData valueForKey:@"ModifiedDate"];
    
    _txt_InspectedAddress.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    _txt_City.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"City"]];
    _txt_ZipCode.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ZipCode"]];
    _txt_1A.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyName"]];
    _txt_1B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SPCSBuisnessLicenseNo"]];
    _txt_1C.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyAddress"]];
    _txt_ZipCodeView4.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyZipCode"]];
    _txt_CityView4.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyCity"]];
    _txt_State.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyState"]];
    _txt_TelephoneNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TelephoneNo"]];
    _txt_1D.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectorName"]];
    _txt_2.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CaseNumber"]];
    _txt_3.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionDate"]];
    _txt_PersonPurchasingInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonName_PurchasingInspection"]];
    
    _txt_4A_Other.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonOther_PurchasingInspection"]];
    
    strDateInspectiondate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionDate"]];
    
    if (strDateInspectiondate.length==0) {
        
        [_btnInspectionDate setTitle:@"Select Date" forState:UIControlStateNormal];
        
    } else {
        
        [_btnInspectionDate setTitle:strDateInspectiondate forState:UIControlStateNormal];
        [_btnInspectionDate setTitle:[self getDateInDayFormat:strDateInspectiondate] forState:UIControlStateNormal];
        
        
    }
    
    _txt_4B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Owner_Seller"]];
    _txt_5.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_5"]];
    _txt_6BSpecify.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Specify"]];
    _txt_7BSpecify.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Specify"]];
    _txt_8E.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionRevealsVisibleEvidence_Specify"]];
    _txt_8F.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ExplanationOfSignsIOfPreviousTreatment"]];
    _txt_8G.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"VisibleEvidenceOf"]];
    _txt_8GD.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"VisibleEvidenceObesrvedAreas"]];
    _txt_9.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"MechanicallyCorrectedByInspectingCompany_Specify"]];
    _txt_9BSpecifyReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PreventiveTreatment_Specify"]];
    _txt_9B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    _txt_10A.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforWoodDestroyingInsects"]];
    _txt_DateOfTreatment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOFTreatment_InspectingCompany"]];
    
    strDateTreatmentByInspectionCompany=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOFTreatment_InspectingCompany"]];
    
    if (strDateTreatmentByInspectionCompany.length==0) {
        
        [_btn_DateTreatmentByInspectionCompany setTitle:@"Select Date" forState:UIControlStateNormal];
        
    } else {
        
        [_btn_DateTreatmentByInspectionCompany setTitle:strDateTreatmentByInspectionCompany forState:UIControlStateNormal];
        [_btn_DateTreatmentByInspectionCompany setTitle:[self getDateInDayFormat:strDateTreatmentByInspectionCompany] forState:UIControlStateNormal];
        
        
    }
    
    _txt_CommonNameOfInsect.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CommonNameOfInsect"]];
    _txt_NameOfPesticide.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"NameOfPesticide_Bait_Or_Other"]];
    _txt_ListInsects.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ListInsects"]];
    _txt_AdditionalComments.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"AdditionalComments_TexasOffiicial_WoodDestroyingInsect"]];
    _txt_12B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ListNoOfPages"]];
    _txt_ApplicatorLicenseNumber.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_LicenseNumber"]];
    _txt_Specifys_TexasOffiicial_WoodDestroyingInsect.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Specifys_TexasOffiicial_WoodDestroyingInsect"]];
    
    
    NSString *strbtnChkBoxA=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_A"]];
    if([strbtnChkBoxA isEqualToString:@"true"] || [strbtnChkBoxA isEqualToString:@"1"]){
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxB=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_B"]];
    if([strbtnChkBoxB isEqualToString:@"true"] || [strbtnChkBoxB isEqualToString:@"1"]){
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxC=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_B"]];
    if([strbtnChkBoxC isEqualToString:@"true"] || [strbtnChkBoxC isEqualToString:@"1"]){
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxD=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_D"]];
    if([strbtnChkBoxD isEqualToString:@"true"] || [strbtnChkBoxD isEqualToString:@"1"]){
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxE=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_E"]];
    if([strbtnChkBoxE isEqualToString:@"true"] || [strbtnChkBoxE isEqualToString:@"1"]){
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxF=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_F"]];
    if([strbtnChkBoxF isEqualToString:@"true"] || [strbtnChkBoxF isEqualToString:@"1"]){
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxG=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_G"]];
    if([strbtnChkBoxG isEqualToString:@"true"] || [strbtnChkBoxG isEqualToString:@"1"]){
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxH=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_H"]];
    if([strbtnChkBoxH isEqualToString:@"true"] || [strbtnChkBoxH isEqualToString:@"1"]){
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxI=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_I"]];
    if([strbtnChkBoxI isEqualToString:@"true"] || [strbtnChkBoxI isEqualToString:@"1"]){
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxJ=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_J"]];
    if([strbtnChkBoxJ isEqualToString:@"true"] || [strbtnChkBoxJ isEqualToString:@"1"]){
        
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_1E=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_Technician"]];
    if([strbtnRadio_1_1E isEqualToString:@"Certified Applicator"]){
        
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_1E isEqualToString:@"Technician"]){
        
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    NSString *strPersonType_PurchasingInspection=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonType_PurchasingInspection"]];
    if([strPersonType_PurchasingInspection isEqualToString:@"Seller"]){
        
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Agent"]){
        
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Buyer"]){
        
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Management Co."]){
        
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Other"]){
        
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    NSString *strbtnChkBox_1_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"]];
    if([strbtnChkBox_1_4C isEqualToString:@"true"] || [strbtnChkBox_1_4C isEqualToString:@"1"]){
        
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_2_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_PurchaseService"]];
    if([strbtnChkBox_2_4C isEqualToString:@"true"] || [strbtnChkBox_2_4C isEqualToString:@"1"]){
        
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_3_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Seller"]];
    if([strbtnChkBox_3_4C isEqualToString:@"true"] || [strbtnChkBox_3_4C isEqualToString:@"1"]){
        
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_4_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Agent"]];
    if([strbtnChkBox_4_4C isEqualToString:@"true"] || [strbtnChkBox_4_4C isEqualToString:@"1"]){
        
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_5_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Buyer"]];
    if([strbtnChkBox_5_4C isEqualToString:@"true"] || [strbtnChkBox_5_4C isEqualToString:@"1"]){
        
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_6A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsPropertyObstrected_Or_Inaccessible"]];
    if([strbtnRadio_1_6A isEqualToString:@"true"] || [strbtnRadio_1_6A isEqualToString:@"1"]){
        
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_6A isEqualToString:@"false"] || [strbtnRadio_1_6A isEqualToString:@"0"]){
        
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    //Obstrected_Or_Inaccessible_SlightCornerCracks
    
    NSString *strbtnChkBox_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SlightCornerCracks"]];
    if([strbtnChkBox_6B isEqualToString:@"true"] || [strbtnChkBox_6B isEqualToString:@"1"]){
        
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_1_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Attic"]];
    if([strbtnChkBox_1_6B isEqualToString:@"true"] || [strbtnChkBox_1_6B isEqualToString:@"1"]){
        
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_InsulatedAreaOfAttic"]];
    if([strbtnChkBox_2_6B isEqualToString:@"true"] || [strbtnChkBox_2_6B isEqualToString:@"1"]){
        
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_PlumbingAreas"]];
    if([strbtnChkBox_3_6B isEqualToString:@"true"] || [strbtnChkBox_3_6B isEqualToString:@"1"]){
        
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_PlanterBoxAbuttingStructure"]];
    if([strbtnChkBox_4_6B isEqualToString:@"true"] || [strbtnChkBox_4_6B isEqualToString:@"1"]){
        
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_5_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Deck"]];
    if([strbtnChkBox_5_6B isEqualToString:@"true"] || [strbtnChkBox_5_6B isEqualToString:@"1"]){
        
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_6_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SubFloors"]];
    if([strbtnChkBox_6_6B isEqualToString:@"true"] || [strbtnChkBox_6_6B isEqualToString:@"1"]){
        
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_7_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SlabJoints"]];
    if([strbtnChkBox_7_6B isEqualToString:@"true"] || [strbtnChkBox_7_6B isEqualToString:@"1"]){
        
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_8_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_CrawlSpace"]];
    if([strbtnChkBox_8_6B isEqualToString:@"true"] || [strbtnChkBox_8_6B isEqualToString:@"1"]){
        
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_9_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SoilGradeTooHigh"]];
    if([strbtnChkBox_9_6B isEqualToString:@"true"] || [strbtnChkBox_9_6B isEqualToString:@"1"]){
        
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_10_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_HeavyFollage"]];
    if([strbtnChkBox_10_6B isEqualToString:@"true"] || [strbtnChkBox_10_6B isEqualToString:@"1"]){
        
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_11_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Eaves"]];
    if([strbtnChkBox_11_6B isEqualToString:@"true"] || [strbtnChkBox_11_6B isEqualToString:@"1"]){
        
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_12_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Weepholes"]];
    if([strbtnChkBox_12_6B isEqualToString:@"true"] || [strbtnChkBox_12_6B isEqualToString:@"1"]){
        
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_13_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Other"]];
    if([strbtnChkBox_13_6B isEqualToString:@"true"] || [strbtnChkBox_13_6B isEqualToString:@"1"]){
        
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_7A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_7A isEqualToString:@"true"] || [strbtnRadio_1_7A isEqualToString:@"1"]){
        
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_7A isEqualToString:@"false"] || [strbtnRadio_1_7A isEqualToString:@"0"]){
        
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_SlightCornerCracks"]];
    if([strbtnChkBox_1_7B isEqualToString:@"true"] || [strbtnChkBox_1_7B isEqualToString:@"1"]){
        
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodtoGroundContact"]];
    if([strbtnChkBox_2_7B isEqualToString:@"true"] || [strbtnChkBox_2_7B isEqualToString:@"1"]){
        
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_FormBoardsLeftInPlace"]];
    if([strbtnChkBox_3_7B isEqualToString:@"true"] || [strbtnChkBox_3_7B isEqualToString:@"1"]){
        
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_ExcessiveMoisture"]];
    if([strbtnChkBox_4_7B isEqualToString:@"true"] || [strbtnChkBox_4_7B isEqualToString:@"1"]){
        
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_5_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_DebrisunderoraroundStructure"]];
    if([strbtnChkBox_5_7B isEqualToString:@"true"] || [strbtnChkBox_5_7B isEqualToString:@"1"]){
        
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_6_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_FootingTooLowOrSoilLineTooHigh"]];
    if([strbtnChkBox_6_7B isEqualToString:@"true"] || [strbtnChkBox_6_7B isEqualToString:@"1"]){
        
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_7_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodRot"]];
    if([strbtnChkBox_7_7B isEqualToString:@"true"] || [strbtnChkBox_7_7B isEqualToString:@"1"]){
        
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_8_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_HeavyFollage"]];
    if([strbtnChkBox_8_7B isEqualToString:@"true"] || [strbtnChkBox_8_7B isEqualToString:@"1"]){
        
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_9_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Plonterboxabuttingstructure"]];
    if([strbtnChkBox_9_7B isEqualToString:@"true"] || [strbtnChkBox_9_7B isEqualToString:@"1"]){
        
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_10_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodPileInContactwithStructure"]];
    if([strbtnChkBox_10_7B isEqualToString:@"true"] || [strbtnChkBox_10_7B isEqualToString:@"1"]){
        
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_11_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodFenceinContactwithStrucuture"]];//ConduciveConditions_WoodFenceinContactwithStrucuture
    if([strbtnChkBox_11_7B isEqualToString:@"true"] || [strbtnChkBox_11_7B isEqualToString:@"1"]){
        
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_12_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_InsufficientVentillation"]];
    if([strbtnChkBox_12_7B isEqualToString:@"true"] || [strbtnChkBox_12_7B isEqualToString:@"1"]){
        
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_13_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Other"]];
    if([strbtnChkBox_13_7B isEqualToString:@"true"] || [strbtnChkBox_13_7B isEqualToString:@"1"]){
        
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    //8
    
    NSString *strbtnRadio_1_8_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8A_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8A_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8A_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8B_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8B_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8B_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8C_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8C_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8C_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8D_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_ActiveInfestation"]];
    if([strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8D_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_PreviousInfestation"]];
    if([strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8D_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_PreviousTreatment"]];
    if([strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8E_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_ActiveInfestation"]];
    if([strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8E_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_PreviousInfestation"]];
    if([strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8E_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_PreviousTreatment"]];
    if([strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_9=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"MechanicallyCorrectedByInspectingCompany"]];
    if([strbtnRadio_1_9 isEqualToString:@"true"] || [strbtnRadio_1_9 isEqualToString:@"1"]){
        
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9 isEqualToString:@"false"] || [strbtnRadio_1_9 isEqualToString:@"0"]){
        
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_9A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CorrectiveTreatment"]];
    if([strbtnRadio_1_9A isEqualToString:@"true"] || [strbtnRadio_1_9A isEqualToString:@"1"]){
        
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9A isEqualToString:@"false"] || [strbtnRadio_1_9A isEqualToString:@"0"]){
        
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_9B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsPreventiveTreatment"]];//IsPreventiveTreatment
    if([strbtnRadio_1_9B isEqualToString:@"true"] || [strbtnRadio_1_9B isEqualToString:@"1"]){
        
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9B isEqualToString:@"false"] || [strbtnRadio_1_9B isEqualToString:@"0"]){
        
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Partial"]];
    if([strbtnChkBox_1_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_1_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Spot"]];
    if([strbtnChkBox_2_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_2_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Bait"]];
    if([strbtnChkBox_3_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_3_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Other"]];
    if([strbtnChkBox_4_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_4_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_1_10A_Drywood=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforDrywoodTermites_Full"]];
    if([strbtnChkBox_1_10A_Drywood isEqualToString:@"true"] || [strbtnChkBox_1_10A_Drywood isEqualToString:@"1"]){
        
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_10A_Drywood=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforDrywoodTermites_Limited"]];
    if([strbtnChkBox_2_10A_Drywood isEqualToString:@"true"] || [strbtnChkBox_2_10A_Drywood isEqualToString:@"1"]){
        
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_10A_Contract=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"]];
    if([strbtnRadio_1_10A_Contract isEqualToString:@"true"] || [strbtnRadio_1_10A_Contract isEqualToString:@"1"]){
        
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_10A_Contract isEqualToString:@"false"] || [strbtnRadio_1_10A_Contract isEqualToString:@"0"]){
        
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ElectricBreakerBox_12A"]];
    if([strbtnChkBox_1_12A isEqualToString:@"true"] || [strbtnChkBox_1_12A isEqualToString:@"1"]){
        
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"WaterHeaterCloset_12A"]];
    if([strbtnChkBox_2_12A isEqualToString:@"true"] || [strbtnChkBox_2_12A isEqualToString:@"1"]){
        
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BathTrapAccess_12A"]];
    if([strbtnChkBox_3_12A isEqualToString:@"true"] || [strbtnChkBox_3_12A isEqualToString:@"1"]){
        
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BeneathTheKitchenSink_12A"]];
    if([strbtnChkBox_4_12A isEqualToString:@"true"] || [strbtnChkBox_4_12A isEqualToString:@"1"]){
        
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    //DatePosted_12B
    
    strDatePosted=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DatePosted_12B"]];
    
    if (strDatePosted.length==0) {
        
        [_btn_DatePosted setTitle:@"Select Date" forState:UIControlStateNormal];
        
    } else {
        
        [_btn_DatePosted setTitle:strDatePosted forState:UIControlStateNormal];
        
        [_btn_DatePosted setTitle:[self getDateInDayFormat:strDatePosted] forState:UIControlStateNormal];
        
        
    }//DateOfPurchaserOfProperty
    
    strDateOfPurchaserOfProperty=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOfPurchaserOfProperty"]];
    
    if (strDateOfPurchaserOfProperty.length==0) {
        
        [_btn_DateOfPurchaserOfProperty setTitle:@"Select Date" forState:UIControlStateNormal];
        
    } else {
        
        [_btn_DateOfPurchaserOfProperty setTitle:strDateOfPurchaserOfProperty forState:UIControlStateNormal];
        [_btn_DateOfPurchaserOfProperty setTitle:[self getDateInDayFormat:strDateOfPurchaserOfProperty] forState:UIControlStateNormal];
        
        
    }//DateOfPurchaserOfProperty
    
    //Buyers Initials Image Download  BuyersInitials_SignaturePath_7B
    
    NSString *strBuyersInitials_SignaturePath_10B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
    
    if (!(strBuyersInitials_SignaturePath_10B.length==0)) {
        
        [self downloadBuyersInitials:strBuyersInitials_SignaturePath_10B :_imgViewBuyerIntials];
        strGlobalBuyersSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
        
    }else{
        _imgViewBuyerIntials.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSign=@"";
        
    }
    
    NSString *strGraphPath__TexasOffiicial_WoodDestroyingInsect=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
    
    if (!(strGraphPath__TexasOffiicial_WoodDestroyingInsect.length==0)) {
        
        [self downloadGraph:strGraphPath__TexasOffiicial_WoodDestroyingInsect];
        strGlobalGraphImage=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
        strGraphImageCaption=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Graph_Caption"]];
        strGraphImageDescription=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Graph_Description"]];
        
    }else{
        
        [_btnAddGraphImg setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        strGlobalGraphImage=@"";
        strGraphImageCaption=@"";
        strGraphImageDescription=@"";
    }
    
    NSString *strInspector_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
    
    if (!(strInspector_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strInspector_SignaturePath :_imgViewInspectorSign];
        strGlobalInspectorSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
        
    }else{
        
        _imgViewInspectorSign.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalInspectorSign=@"";
        
    }
    
    
    NSString *strCertifiedApplicator_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
    
    if (!(strCertifiedApplicator_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strCertifiedApplicator_SignaturePath :_imgViewCertifiedApplicatorSign];
        strGlobalApplicatorSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
        
    }else{
        
        _imgViewCertifiedApplicatorSign.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalApplicatorSign=@"";
        
    }
    
    
    NSString *strSignatureOfPurchaserOfProperty_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
    
    if (!(strSignatureOfPurchaserOfProperty_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strSignatureOfPurchaserOfProperty_SignaturePath :_imgViewBuyerIntialsPurchaser];
        strGlobalBuyersSignPurchaser=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
        
    }else{
        
        _imgViewBuyerIntialsPurchaser.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSignPurchaser=@"";
        
    }
    
    NSString *strBuyersInitials_SignaturePath_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
    
    if (!(strBuyersInitials_SignaturePath_7B.length==0)) {
        
        [self downloadBuyersInitials:strBuyersInitials_SignaturePath_7B :_imgView_BuyerInitial_7B];
        strGlobalBuyersSign7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
        
    }else{
        
        _imgView_BuyerInitial_7B.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSign7B=@"";
        
    }
    
}

-(void)downloadBuyersInitials :(NSString*)str :(UIImageView*)imgView{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str :imgView];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str :imgView];
                
            });
        });
        
    }
}


-(void)ShowFirstImageTech :(NSString*)str :(UIImageView*)imgView{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    imgView.image=[self loadImage:result];
    
}

-(void)downloadGraph :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowImageGraph :str :_btnAddGraphImg];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowImageGraph :str :_btnAddGraphImg];
                
            });
        });
        
    }
}

-(void)ShowImageGraph :(NSString*)str :(UIButton*)imgView{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [imgView setImage:[self loadImage:result] forState:UIControlStateNormal];
    //imgView.image=[self loadImage:result];
    
}


-(void)methodSetAllViews{
    
    
    [_btnInspectionDate.layer setCornerRadius:5.0f];
    [_btnInspectionDate.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnInspectionDate.layer setBorderWidth:0.8f];
    [_btnInspectionDate.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnInspectionDate.layer setShadowOpacity:0.3];
    [_btnInspectionDate.layer setShadowRadius:3.0];
    [_btnInspectionDate.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DatePosted.layer setCornerRadius:5.0f];
    [_btn_DatePosted.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DatePosted.layer setBorderWidth:0.8f];
    [_btn_DatePosted.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DatePosted.layer setShadowOpacity:0.3];
    [_btn_DatePosted.layer setShadowRadius:3.0];
    [_btn_DatePosted.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DateTreatmentByInspectionCompany.layer setCornerRadius:5.0f];
    [_btn_DateTreatmentByInspectionCompany.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DateTreatmentByInspectionCompany.layer setBorderWidth:0.8f];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowOpacity:0.3];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowRadius:3.0];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DateOfPurchaserOfProperty.layer setCornerRadius:5.0f];
    [_btn_DateOfPurchaserOfProperty.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DateOfPurchaserOfProperty.layer setBorderWidth:0.8f];
    [_btn_DateOfPurchaserOfProperty.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DateOfPurchaserOfProperty.layer setShadowOpacity:0.3];
    [_btn_DateOfPurchaserOfProperty.layer setShadowRadius:3.0];
    [_btn_DateOfPurchaserOfProperty.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(10, 0, self.view.frame.size.width-20, _view_CustomerInfo.frame.size.height);
    frameFor_view_CustomerInfo.origin.x=10;
    frameFor_view_CustomerInfo.origin.y=0;
    
    [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
    
    [_scrollVieww addSubview:_view_CustomerInfo];
    
    CGRect frameFor_View1=CGRectMake(0, _view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view1.frame.size.height);
    [_view1 setFrame:frameFor_View1];
    
    [_scrollVieww addSubview:_view1];
    
    CGRect frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view2.frame.size.height);
    [_view2 setFrame:frameFor_View2];
    
    [_scrollVieww addSubview:_view2];
    
    CGRect frameFor_View3=CGRectMake(0, _view2.frame.origin.y+_view2.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view3.frame.size.height);
    [_view3 setFrame:frameFor_View3];
    
    [_scrollVieww addSubview:_view3];
    
    
    CGRect frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view4.frame.size.height);
    [_view4 setFrame:frameFor_View4];
    
    [_scrollVieww addSubview:_view4];
    
    CGRect frameFor_View5=CGRectMake(0, _view4.frame.origin.y+_view4.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5.frame.size.height);
    [_view5 setFrame:frameFor_View5];
    
    [_scrollVieww addSubview:_view5];
    
    CGRect frameFor_View5Sub=CGRectMake(0, _view5.frame.origin.y+_view5.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5Sub.frame.size.height);
    [_view5Sub setFrame:frameFor_View5Sub];
    
    [_scrollVieww addSubview:_view5Sub];
    
    
    CGRect frameFor_View6=CGRectMake(0, _view5Sub.frame.origin.y+_view5Sub.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view6.frame.size.height);
    [_view6 setFrame:frameFor_View6];
    
    [_scrollVieww addSubview:_view6];
    
    CGRect frameFor_View7=CGRectMake(0, _view6.frame.origin.y+_view6.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view7.frame.size.height);
    [_view7 setFrame:frameFor_View7];
    
    [_scrollVieww addSubview:_view7];
    
    CGRect frameFor_View8=CGRectMake(0, _view7.frame.origin.y+_view7.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view8.frame.size.height);
    [_view8 setFrame:frameFor_View8];
    
    [_scrollVieww addSubview:_view8];
    
    CGRect frameFor_View9=CGRectMake(0, _view8.frame.origin.y+_view8.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view9.frame.size.height);
    [_view9 setFrame:frameFor_View9];
    
    [_scrollVieww addSubview:_view9];
    
    CGRect frameFor_View10=CGRectMake(0, _view9.frame.origin.y+_view9.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view10.frame.size.height);
    [_view10 setFrame:frameFor_View10];
    
    [_scrollVieww addSubview:_view10];
    
    CGRect frameFor_View11=CGRectMake(0, _view10.frame.origin.y+_view10.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view11.frame.size.height);
    [_view11 setFrame:frameFor_View11];
    
    [_scrollVieww addSubview:_view11];
    
    CGRect frameFor_View12=CGRectMake(0, _view11.frame.origin.y+_view11.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view12.frame.size.height);
    [_view12 setFrame:frameFor_View12];
    
    [_scrollVieww addSubview:_view12];
    
    CGRect frameFor_View13=CGRectMake(0, _view12.frame.origin.y+_view12.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view13.frame.size.height);
    [_view13 setFrame:frameFor_View13];
    
    [_scrollVieww addSubview:_view13];
    
    //  [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_view13.frame.size.height+_view13.frame.origin.y)];
    
    //CGRect frameFor_viewPaymentInfo=CGRectMake(0, _view12.frame.origin.y+_view12.frame.size.height, [UIScreen mainScreen].bounds.size.width,_paymentTypeView.frame.size.height);
    CGRect frameFor_viewPaymentInfo=CGRectMake(0, _view13.frame.origin.y+_view13.frame.size.height, [UIScreen mainScreen].bounds.size.width,_paymentTypeView.frame.size.height);
    
    [_paymentTypeView setFrame:frameFor_viewPaymentInfo];
    
    
    //Change For Service job descriptions
    
    [_scrollVieww addSubview:_paymentTypeView];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    [_scrollVieww addSubview:_viewServiceJobDescriptions];
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    [_scrollVieww addSubview:_viewPaymentInfo];
    
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    [_scrollVieww addSubview:_view_TechSignature];
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    [_scrollVieww addSubview:_viewCustomerSignature];
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    [_scrollVieww addSubview:_view_SavenContinue];
    
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *contextNew = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [_txtViewTermsnConditions.text boundingRectWithSize:constraint
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:_txtViewTermsnConditions.font}
                                                                     context:contextNew].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    if (size.height<85) {
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, 100);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, 85+40);
        
        
    }else{
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, size.height+40);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, size.height+40);
        
    }
    
    [_scrollVieww addSubview:_viewTermsnConditions];
    
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y)];
    
}
//============================================================================
#pragma mark- BY NILIND
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
/*
 - (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
 {
 if (collectionView==_collectionView7A)
 {
 return arrNoImage.count;
 }
 else
 {
 return arrNoImageCollectionView2.count;
 }
 
 }
 
 
 - (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
 {
 if (collectionView == _collectionView7A)
 {
 static NSString *identifier = @"CellCollectionService";
 
 GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
 
 
 NSString *str ;
 
 NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 //str=[arrNoImage objectAtIndex:indexPath.row];
 }
 else
 {
 str=[arrNoImage objectAtIndex:indexPath.row];
 
 }
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound)
 {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 
 UIImage* image = [UIImage imageWithContentsOfFile:path];
 
 if (image==nil) {
 
 cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
 
 } else {
 
 cell.imageBefore.image = image;
 }
 
 
 return cell;
 }
 else
 {
 static NSString *identifier = @"CellCollectionService";
 
 GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
 
 
 NSString *str ;
 
 NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 
 // str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
 }
 else
 {
 str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
 
 }
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound)
 {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 
 UIImage* image = [UIImage imageWithContentsOfFile:path];
 
 if (image==nil) {
 
 cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
 
 } else {
 
 cell.imageBefore.image = image;
 }
 
 
 return cell;
 }
 
 }
 
 - (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
 {
 
 if (collectionView==_collectionView7A)
 {
 NSString *str ;
 
 NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 }
 else
 {
 str=[arrNoImage objectAtIndex:indexPath.row];
 
 }
 
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 
 UIImage* image = [UIImage imageWithContentsOfFile:path];
 
 if (image==nil) {
 
 
 
 }else{
 
 NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
 [self goingToPreviewFor7A : strIndex];
 
 }
 }
 else if(collectionView==_collectionView8)
 {
 
 NSString *str;
 NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 }
 else
 {
 str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
 
 }
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 
 UIImage* image = [UIImage imageWithContentsOfFile:path];
 
 if (image==nil) {
 
 
 
 }else{
 
 NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
 [self goingToPreviewFor8 : strIndex];
 
 }
 
 }
 }*/
-(void)goingToPreviewFor7A :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewTermiteViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewTermiteViewController"];
        objByProductVC.strForView=@"for7A";
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaptionFor7A;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;//fromInvoice
        objByProductVC.strFromInvoice=@"fromInvoice";
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescriptionFor7A;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}
-(void)goingToPreviewFor8 :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImageCollectionView2;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewTermiteViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewTermiteViewController"];
        objByProductVC.strForView=@"for8";
        
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaptionFor8;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        objByProductVC.strFromInvoice=@"fromInvoice";
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescriptionFor8;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

- (IBAction)actionOnAddImageForCollectionView7A:(id)sender
{
    _collectionView7A.tag=0;
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Capture New", @"Gallery", nil];
        
        [actionSheet showInView:self.view];
        
        
    }
}
-(IBAction)actionOnAddImageForCollectionView8:(id)sender
{
    _collectionView7A.tag=1;
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Capture New", @"Gallery", nil];
        
        [actionSheet showInView:self.view];
        
        
    }
}
//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (_collectionView8.tag==0)
//    {
//        if (buttonIndex == 0)
//        {
//            NSLog(@"The CApture Image.");
//
//
//            if (arrNoImage.count<10)
//            {
//                NSLog(@"The CApture Image.");
//
//                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
//
//                if (isCameraPermissionAvailable) {
//
//                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//                    {
//
//                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
//
//                    }else{
//
//                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
//                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
//                        imagePickController.delegate=(id)self;
//                        imagePickController.allowsEditing=TRUE;
//                        [self presentViewController:imagePickController animated:YES completion:nil];
//
//                    }
//                }else{
//
//                    UIAlertController *alert= [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:@"Camera Permission not allowed.Please go to settings and allow"
//                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction * action)
//                                          {
//
//
//
//                                          }];
//                    [alert addAction:yes];
//                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action)
//                                         {
//
//                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
//                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//                                                 [[UIApplication sharedApplication] openURL:url];
//                                             } else {
//
//                                             }
//                                         }];
//                    [alert addAction:no];
//                    [self presentViewController:alert animated:YES completion:nil];
//                }
//
//            }
//            else
//            {
//                //chkForDuplicateImageSave=YES;
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//            }
//            //............
//
//        }
//        else if (buttonIndex == 1)
//        {
//            NSLog(@"The Gallery.");
//            if (arrNoImage.count<10)
//            {
//                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
//
//                if (isCameraPermissionAvailable) {
//
//                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//                    {
//
//                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
//
//                    }else{
//
//                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
//                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//                        imagePickController.delegate=(id)self;
//                        imagePickController.allowsEditing=TRUE;
//                        [self presentViewController:imagePickController animated:YES completion:nil];
//
//                    }
//                }else{
//
//                    UIAlertController *alert= [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
//                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction * action)
//                                          {
//
//
//
//                                          }];
//                    [alert addAction:yes];
//                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action)
//                                         {
//
//                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
//                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//                                                 [[UIApplication sharedApplication] openURL:url];
//                                             }
//                                             else
//                                             {
//
//
//
//                                             }
//
//                                         }];
//                    [alert addAction:no];
//                    [self presentViewController:alert animated:YES completion:nil];
//                }
//            }
//            else
//            {
//                //chkForDuplicateImageSave=YES;
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//            }
//        }
//    }
//    else
//    {
//        if (buttonIndex == 0)
//        {
//            NSLog(@"The CApture Image.");
//
//
//            if (arrNoImageCollectionView2.count<10)
//            {
//                NSLog(@"The CApture Image.");
//
//                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
//
//                if (isCameraPermissionAvailable) {
//
//                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//                    {
//
//                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
//
//                    }else{
//
//                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
//                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
//                        imagePickController.delegate=(id)self;
//                        imagePickController.allowsEditing=TRUE;
//                        [self presentViewController:imagePickController animated:YES completion:nil];
//
//                    }
//                }else{
//
//                    UIAlertController *alert= [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:@"Camera Permission not allowed.Please go to settings and allow"
//                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction * action)
//                                          {
//
//
//
//                                          }];
//                    [alert addAction:yes];
//                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action)
//                                         {
//
//                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
//                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//                                                 [[UIApplication sharedApplication] openURL:url];
//                                             } else {
//
//                                             }
//                                         }];
//                    [alert addAction:no];
//                    [self presentViewController:alert animated:YES completion:nil];
//                }
//
//            }
//            else
//            {
//                //chkForDuplicateImageSave=YES;
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//            }
//            //............
//
//        }
//        else if (buttonIndex == 1)
//        {
//            NSLog(@"The Gallery.");
//            if (arrNoImageCollectionView2.count<10)
//            {
//                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
//
//                if (isCameraPermissionAvailable) {
//
//                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//                    {
//
//                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
//
//                    }else{
//
//                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
//                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//                        imagePickController.delegate=(id)self;
//                        imagePickController.allowsEditing=TRUE;
//                        [self presentViewController:imagePickController animated:YES completion:nil];
//
//                    }
//                }else{
//
//                    UIAlertController *alert= [UIAlertController
//                                               alertControllerWithTitle:@"Alert"
//                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
//                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction * action)
//                                          {
//
//
//
//                                          }];
//                    [alert addAction:yes];
//                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
//                                                               handler:^(UIAlertAction * action)
//                                         {
//
//                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
//                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//                                                 [[UIApplication sharedApplication] openURL:url];
//                                             }
//                                             else
//                                             {
//
//
//
//                                             }
//
//                                         }];
//                    [alert addAction:no];
//                    [self presentViewController:alert animated:YES completion:nil];
//                }
//            }
//            else
//            {
//                //chkForDuplicateImageSave=YES;
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 3 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//            }
//        }
//    }
//}

#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------
/*
 - (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 {
 if (_collectionView7A.tag==0)
 {
 // isEditedInSalesAuto=YES;
 NSLog(@"Database edited");
 
 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
 [defs setBool:NO forKey:@"backfromDynamicView"];
 [defs synchronize];
 
 NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
 [formatterDate setDateFormat:@"MMddyyyy"];
 [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"HHmmss"];
 [formatter setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strTime = [formatter stringFromDate:[NSDate date]];
 NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
 [arrNoImage addObject:strImageNamess];
 UIImage *image = info[UIImagePickerControllerEditedImage];
 // [self saveImage:chosenImage :strImageNamess];
 
 //[self resizeImage:chosenImage :strImageNamess];
 
 [picker dismissViewControllerAnimated:YES completion:NULL];
 
 //imageCaption
 
 NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
 
 BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
 
 if (yesImageCaption)
 {
 
 [self alertViewCustom];
 
 }
 else
 {
 
 [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
 [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
 
 }
 //Lat long code
 
 CLLocationCoordinate2D coordinate = [global getLocation] ;
 NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
 NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
 [arrImageLattitudeFor7A addObject:latitude];
 [arrImageLongitudeFor7A addObject:longitude];
 
 [self resizeImage:image :strImageNamess];
 //[_collectionView7A reloadData];
 }
 else
 {
 // isEditedInSalesAuto=YES;
 _collectionView8.tag=1;
 
 NSLog(@"Database edited");
 
 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
 [defs setBool:NO forKey:@"backfromDynamicView"];
 [defs synchronize];
 
 NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
 [formatterDate setDateFormat:@"MMddyyyy"];
 [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"HHmmss"];
 [formatter setTimeZone:[NSTimeZone localTimeZone]];
 NSString *strTime = [formatter stringFromDate:[NSDate date]];
 NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
 [arrNoImageCollectionView2 addObject:strImageNamess];
 UIImage *image = info[UIImagePickerControllerEditedImage];
 // [self saveImage:chosenImage :strImageNamess];
 
 //[self resizeImage:chosenImage :strImageNamess];
 
 [picker dismissViewControllerAnimated:YES completion:NULL];
 
 //imageCaption
 
 NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
 
 BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
 
 if (yesImageCaption)
 {
 
 [self alertViewCustom];
 
 } else {
 
 [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
 [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
 
 }
 CLLocationCoordinate2D coordinate = [global getLocation] ;
 NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
 NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
 [arrImageLattitudeFor8 addObject:latitude];
 [arrImageLongitudeFor8 addObject:longitude];
 
 [self resizeImage:image :strImageNamess];
 //[_collectionView8 reloadData];
 }
 }
 */
/*- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
 {
 [picker dismissViewControllerAnimated:YES completion:NULL];
 }*/


//============================================================================
#pragma mark- ---------------------FETCH IMAGE TERMITE FROM CORE DATA ---------------------
//============================================================================

-(void)fetchImageDetailFromDataBaseForSectionView7A_8
{
    // NSMutableArray *arrImageLongitude,*arrImageLattitude;
    // arrImageLongitude=[[NSMutableArray alloc]init];
    // arrImageLattitude=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetailsTermite];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfTermiteImages;
    arrOfTermiteImages=[[NSMutableArray alloc]init];
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"TexasTermite_Section7A"])
            {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woTexasTermiteImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionFor7A addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionFor7A addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitudeFor7A addObject:@""];
                    
                } else {
                    
                    [arrImageLattitudeFor7A addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitudeFor7A addObject:@""];
                    
                } else {
                    
                    [arrImageLongitudeFor7A addObject:strLong];
                    
                }
                
            }
            else if ([woImageType isEqualToString:@"TexasTermite_Section8"])
            {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woTexasTermiteImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrNoImageCollectionView2 addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionFor8 addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionFor8 addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitudeFor8 addObject:@""];
                    
                } else {
                    
                    [arrImageLattitudeFor8 addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitudeFor8 addObject:@""];
                    
                }
                else
                {
                    
                    [arrImageLongitudeFor8 addObject:strLong];
                    
                }
                
            }
            [arrOfTermiteImages addObject:[matchesImageDetail valueForKey:@"woImagePath"]];
        }
    }
    [self downloadImages7ATermite:arrOfTermiteImages];
    
    // [self downloadImages7ATermite:arrNoImage];
    //[self downloadImages8Termite:arrNoImageCollectionView2];
    [_collectionView7A reloadData];
    [_collectionView8 reloadData];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    //[self downloadImagesGraphs:arrOfImagenameCollewctionView];
    
}
//============================================================================
#pragma mark- ------------CORE DATA TERMITE IMAGE SAVE------------------
//============================================================================
-(void)saveImageTermiteToCoreData
{
    //[self deleteBeforeImagesBeforeSaving];
    //Graph
    //[self deleteGraphImagesBeforeSaving];
    [self deleteImageTermiteFromCoreData];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrNoImage.count; k++)
    {
        
        // Image Detail Entity
        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
        
        ImageDetailsTermite *objImageDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
        if ([arrNoImage[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
            objImageDetail.woImageType=@"TexasTermite_Section7A";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor7A objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor7A objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor7A objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor7A objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
        else
        {
            
            NSDictionary *dictData=[arrNoImage objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"TexasTermite_Section7A";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor7A objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor7A objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor7A objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor7A objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    
    //For Section 7 Image
    
    for (int k=0; k<arrNoImageCollectionView2.count; k++)
    {
        // Image Detail Entity
        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
        ImageDetailsTermite *objImageDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
        
        if ([arrNoImageCollectionView2[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrNoImageCollectionView2 objectAtIndex:k]];
            objImageDetail.woImageType=@"TexasTermite_Section8";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor8 objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor8  objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor8  objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor8  objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            NSDictionary *dictData=[arrNoImageCollectionView2 objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"TexasTermite_Section8";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=strWorkOrderId;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor8 objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor8 objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor8 objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor8 objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    //........................................................................
}
//============================================================================
#pragma mark- ------------DELETE CORE DATA TERMITE IMAGE ------------------
//============================================================================
-(void)deleteImageTermiteFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete ImageDetailsTermite
    
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context]];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",_strWorkOrder,@"Graph"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [allData setPredicate:predicate];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
//============================================================================
#pragma mark- ------------DOWNLOAD TERMITE IMAGE ------------------
//============================================================================

-(void)downloadImages7ATermite :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
        } else {
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                
                NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                
                NSURL *photoURL = [NSURL URLWithString:strNewString];
                NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
                UIImage *image = [UIImage imageWithData:photoData];
                [self saveImageAfterDownload7A: image : result : k];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            });
        }
    }
    //[_collectionView7A reloadData];
}
- (void)saveImageAfterDownload7A: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}
/*
 -(void)downloadImages8Termite :(NSArray*)arrOfImagesDownload
 {
 for (int k=0; k<arrOfImagesDownload.count; k++) {
 
 NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
 NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 }
 else
 {
 str=[arrOfImagesDownload objectAtIndex:k];
 }
 
 NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
 NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
 NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
 
 NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
 if (fileExists)
 {
 } else {
 
 dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
 dispatch_async(myQueue, ^{
 
 NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
 
 NSURL *photoURL = [NSURL URLWithString:strNewString];
 NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
 UIImage *image = [UIImage imageWithData:photoData];
 [self saveImageAfterDownload8: image : result : k];
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 });
 });
 }
 }
 [_collectionView8 reloadData];
 }
 - (void)saveImageAfterDownload8: (UIImage*)image :(NSString*)name :(int)indexx{
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
 NSData  *Data = UIImagePNGRepresentation(image);
 [Data writeToFile:path atomically:YES];
 
 }*/
- (IBAction)actionOnNotesHistory:(id)sender
{
    //Nilind
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    ServiceNotesHistoryViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ServiceNotesHistoryViewController"];
    objByProductVC.strTypeOfService=@"service";
    [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    
    
    //End
}
//Nilind 15 Dec
-(NSString *)getDateInDayFormat:(NSString*)dateString
{
    //Getting date from string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateString];
    // converting into our required date format
    [dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}

#pragma mark- --------Fetch For Florida -------------------
//============================================================================
#pragma mark- Fetch From DB Values
//============================================================================

-(void)getDataFrmDBFlorida
{
    
    NSLog(@"Work Order Id===%@",strWorkOrderId);
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailFlorida;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    requestTermiteFlorida = [[NSFetchRequest alloc] init];
    [requestTermiteFlorida setEntity:entityTermiteFlorida];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestTermiteFlorida setPredicate:predicate];
    
    sortDescriptorTermiteFlorida = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsTermiteFlorida = [NSArray arrayWithObject:sortDescriptorTermiteFlorida];
    
    [requestTermiteFlorida setSortDescriptors:sortDescriptorsTermiteFlorida];
    
    fetchedResultsControllerWorkOrderDetailFlorida = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTermiteFlorida managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailFlorida setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailFlorida performFetch:&error];
    arrAllObjTermiteFlorida = [fetchedResultsControllerWorkOrderDetailFlorida fetchedObjects];
    if ([arrAllObjTermiteFlorida count] == 0)
    {
        //matchesWorkOrder=nil;
        //[self setFloridaTermiteFormifBlank];
        [self showValuesSavedInDbFlorida];
        
    }
    else
    {
        matchesWorkOrder=arrAllObjTermiteFlorida[0];
        dictGlobalTermiteFlorida=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteFlorida=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"floridaTermiteServiceDetail"];
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[matchesWorkOrder valueForKey:@"floridaTermiteServiceDetail"]
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        
        NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        dictDataTermite=[self nestedDictionaryByReplacingNullsWithNil:dictDataTermite];
        dictGlobalTermiteFlorida=[[NSMutableDictionary alloc]init];
        [dictGlobalTermiteFlorida addEntriesFromDictionary:dictDataTermite];
        [self showValuesSavedInDbFlorida];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)showValuesSavedInDbFlorida
{
    //[self fetchWorkOrderFromDataBase];
    
    //For View1
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strInspectionCompanyName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyName"]];
    if ([strInspectionCompanyName isEqualToString:@"" ]||strInspectionCompanyName.length==0||[strInspectionCompanyName isEqualToString:@"(null)"])
    {
        _txtInspectionCompany.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        [dictGlobalTermiteFlorida setValue:_txtInspectionCompany.text forKey:@"InspectionCompanyName"];
        
        
    }
    else
    {
        _txtInspectionCompany.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyName"]];
        
    }
    
    _txtBusinessLicenseNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"BusinessLicenseNumber"]];
    
    
    NSString *strInspectionCompanyAddress=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionCompanyAddress"]];
    
    if ([strInspectionCompanyAddress isEqualToString:@"" ]||strInspectionCompanyAddress.length==0||[strInspectionCompanyAddress isEqualToString:@"(null)"])
    {
        _txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];
        [dictGlobalTermiteFlorida setValue:_txtCompanyAddress.text forKey:@"CompanyAddress"];
        
        
    }
    else
    {
        _txtCompanyAddress.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyAddress"]];
    }
    
    
    NSString *strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyCity"]];
    if ([strInspectionCompanyCity isEqualToString:@"" ]||strInspectionCompanyCity.length==0||[strInspectionCompanyCity isEqualToString:@"(null)"])
    {
        strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyCity forKey:@"CompanyCity"];
    }
    else
    {
        strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyCity"]];
        
    }
    
    NSString *strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyState"]];
    if ([strInspectionCompanyState isEqualToString:@"" ]||strInspectionCompanyState.length==0||[strInspectionCompanyState isEqualToString:@"(null)"])
    {
        strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyState forKey:@"CompanyState"];
    }
    else
    {
        strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyState"]];
        
    }
    
    NSString *strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyZipCode"]];
    if ([strInspectionCompanyZipCode isEqualToString:@"" ]||strInspectionCompanyZipCode.length==0||[strInspectionCompanyZipCode isEqualToString:@"(null)"])
    {
        strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];
        [dictGlobalTermiteFlorida setValue:strInspectionCompanyZipCode forKey:@"CompanyZipCode"];
    }
    else
    {
        strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CompanyZipCode"]];
    }
    
    //_txtComapnyCityStateZipcode.text=[NSString stringWithFormat:@"%@, %@, %@",strInspectionCompanyCity,strInspectionCompanyState,strInspectionCompanyZipCode];
    _txtComapnyCity.text=[NSString stringWithFormat:@"%@",strInspectionCompanyCity];
    _txtComapnyState.text=[NSString stringWithFormat:@"%@",strInspectionCompanyState];
    _txtComapnyZipcode.text=[NSString stringWithFormat:@"%@",strInspectionCompanyZipCode];
    
    //_txtDateOfInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionDate"]];
    
    
    NSString *strInspectionTelephoneNo=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"PhoneNumber"]];
    if ([strInspectionTelephoneNo isEqualToString:@"" ]||strInspectionTelephoneNo.length==0||[strInspectionTelephoneNo isEqualToString:@"(null)"])
    {
        _txtPhoneNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.PrimaryPhone"]];
        [dictGlobalTermiteFlorida setValue:_txtPhoneNo.text forKey:@"PhoneNumber"];
        
        
    }
    else
    {
        _txtPhoneNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"PhoneNumber"]];
        
    }
    
    NSString *strInspectionInspectorName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectorName"]];
    if ([strInspectionInspectorName isEqualToString:@"" ]||strInspectionInspectorName.length==0||[strInspectionInspectorName isEqualToString:@"(null)"])
    {
        _txt1InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
        [dictGlobalTermiteFlorida setValue:_txt1InspectorNameAndIdentificationNo.text forKey:@"InspectorName"];
    }
    else
    {
        _txt1InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectorName"]];
    }
    
    
    _txt2InspectorNameAndIdentificationNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"IdentificationCardNumber"]];
    
    
    strDateOfInspectiondate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DateOfInspection"]];
    /*
     if (strDateOfInspectiondate.length==0) {
     //currentDate
     [_btnDateOfInspection setTitle:[self currentDate] forState:UIControlStateNormal];
     
     [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"DateOfInspection"];
     
     } else {
     
     strDateOfInspectiondate=[self changeDateToFormattedDate:strDateOfInspectiondate];
     
     [_btnDateOfInspection setTitle:strDateOfInspectiondate forState:UIControlStateNormal];
     
     }
     */
    strDateOfInspectiondate=[self changeDateToFormattedDate:strDateOfInspectiondate];
    [_btnDateOfInspection setTitle:strDateOfInspectiondate forState:UIControlStateNormal];
    
    _txtAddressOfPropoertyInspected.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"AddressOfPropertyInspected"]];
    
    
    _txtStructurePropertyInspected.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"StructureOnPropertyInspected"]];
    
    _txtInspectionAndReportRequestedBy.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionandReportRequestedBy"]];
    _txtInspectionAndReportRequestedTo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"ReportSentToRequestor"]];
    
    //View 1 End
    
    
    
    //For View 2
    
    
    NSString *strbtnChkBoxA=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NoVisibleSignOfWDOObserved"]];
    if([strbtnChkBoxA isEqualToString:@"true"] || [strbtnChkBoxA isEqualToString:@"1"]){
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxB=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"VisibleEvidenceOfWDOObserved"]];
    if([strbtnChkBoxB isEqualToString:@"true"] || [strbtnChkBoxB isEqualToString:@"1"]){
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxLiveWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"LiveWDO"]];
    if([strbtnChkBoxLiveWDO isEqualToString:@"true"] || [strbtnChkBoxLiveWDO isEqualToString:@"1"]){
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxLiveWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtLiveWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"LiveWDO_Text"]];
    
    
    
    
    NSString *strbtnChkBoxEvidenceOfWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidenceOfWDO"]];
    if([strbtnChkBoxEvidenceOfWDO isEqualToString:@"true"] || [strbtnChkBoxEvidenceOfWDO isEqualToString:@"1"]){
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxEvidenceOfWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtViewEvidenceOfWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidenceOfWDO_Text"]];
    
    
    
    NSString *strbtnChkBoxDamageCausedbyWDO=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DamageCausedbyWDO"]];
    if([strbtnChkBoxDamageCausedbyWDO isEqualToString:@"true"] || [strbtnChkBoxDamageCausedbyWDO isEqualToString:@"1"]){
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxDAMAGEcausedbyWDO setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtViewDAMAGEcausedbyWDO.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DamageCausedbyWDO_Text"]];
    
    //View 2 End
    
    
    //For View 3
    
    
    NSString *strbtnChkBoxAttic=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic"]];
    if([strbtnChkBoxAttic isEqualToString:@"true"] || [strbtnChkBoxAttic isEqualToString:@"1"]){
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxAttic setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldAtticSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic_SpecificAreas"]];
    _txtViewAtticReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Attic_Reason"]];
    
    
    NSString *strbtnChkBoxInterior=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior"]];
    if([strbtnChkBoxInterior isEqualToString:@"true"] || [strbtnChkBoxInterior isEqualToString:@"1"]){
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxInterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldInteriorSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior_SpecificAreas"]];
    _txtViewInteriorReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Interior_Reason"]];
    
    
    
    NSString *strbtnChkBoxExterior=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior"]];
    if([strbtnChkBoxExterior isEqualToString:@"true"] || [strbtnChkBoxExterior isEqualToString:@"1"]){
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxExterior setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldExteriorSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior_SpecificAreas"]];
    _txtViewExteriorReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Exterior_Reason"]];
    
    
    NSString *strbtnChkBoxCrawlPlace=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace"]];
    if([strbtnChkBoxCrawlPlace isEqualToString:@"true"] || [strbtnChkBoxCrawlPlace isEqualToString:@"1"]){
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxCrawlPlace setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldCrawlPlaceSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace_SpecificAreas"]];
    _txViewCrawlPlaceReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CrawlSpace_Reason"]];
    
    
    NSString *strbtnChkBoxJ=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other"]];
    if([strbtnChkBoxJ isEqualToString:@"true"] || [strbtnChkBoxJ isEqualToString:@"1"]){
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnCheckBoxOther setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldOtherSpecificArea.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other_SpecificAreas"]];
    _txtViewOtherReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Other_Reason"]];
    
    //View 3 End
    
    
    
    //View 4
    
    NSString *strbtnRadioEvidencOfPreviousTreatmentObserved=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidencOfPreviousTreatmentObserved"]];
    if([strbtnRadioEvidencOfPreviousTreatmentObserved isEqualToString:@"true"] || [strbtnRadioEvidencOfPreviousTreatmentObserved isEqualToString:@"1"])
    {
        
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnRadioEvidenceForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioEvidencForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    _txtFieldForEvidence.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"EvidencOfPreviousTreatmentObserved_Text"]];
    
    
    _txtFieldForNoticeOfInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NoticeOfInspection"]];
    
    
    
    
    NSString *strbtnRadioTimeOfInspection=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"TimeOfInspection"]];
    if([strbtnRadioTimeOfInspection isEqualToString:@"true"] || [strbtnRadioTimeOfInspection isEqualToString:@"1"])
    {
        
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [_btnRadioCompnayStructureForNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadioCompanyStructureForYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    _txtFieldForCommonNameOfOrganism.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"CommonNameOfOrganisationTreated"]];
    _txtFieldForNameOfPesticideUsed.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"NameOfPesticideUsed"]];
    _txtFieldForTermsAndCondition.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"TermsandConditionsOfTreatment"]];
    
    
    NSString *strbtnChkBoxWholeStructure=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"WholeStructure"]];
    if([strbtnChkBoxWholeStructure isEqualToString:@"true"] || [strbtnChkBoxWholeStructure isEqualToString:@"1"]){
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxWholeStructure setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxSpotTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"WholeStructure"]];
    if([strbtnChkBoxSpotTreatment isEqualToString:@"true"] || [strbtnChkBoxSpotTreatment isEqualToString:@"1"]){
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxSpotTreatment setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    _txtFieldForMethodOfTreatment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SpotTreatment_Text"]];
    _txtFieldForSpecifyTreatmentNotice.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SpecifyTreatmentNoticeLocation"]];
    
    //View 4 End
    
    
    //View 5
    
    _txtViewForComment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"Comments"]];
    
    
    
    
    //DateOfPurchaserOfProperty
    
    
    NSString *strSignOfAgentPath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SignatureOfLicenseOrAgent"]];
    
    if (!(strSignOfAgentPath.length==0)) {
        
        [self downloadBuyersInitials:strSignOfAgentPath :_imgViewSignatureofLicenseAgent];
        strGlobalSignOfAgent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"SignatureOfLicenseOrAgent"]];
        
    }else{
        
        _imgViewSignatureofLicenseAgent.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalSignOfAgent=@"";
        
    }
    
    
    strInspectionDate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"InspectionDate"]];
    
    /* if (strInspectionDate.length==0) {
     //currentDate
     [_btnInspectionDate setTitle:[self currentDate] forState:UIControlStateNormal];
     
     [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"InspectionDate"];
     
     } else {
     
     strInspectionDate=[self changeDateToFormattedDate:strInspectionDate];
     
     [_btnInspectionDate setTitle:strInspectionDate forState:UIControlStateNormal];
     
     }
     */
    strInspectionDate=[self changeDateToFormattedDate:strInspectionDate];
    [_btnInspectionDateFloridaView5 setTitle:strInspectionDate forState:UIControlStateNormal];
    
    strDateOfLicenceeAgent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"DateOfLicenseOrAgent"]];
    
    /*if (strDateOfLicenceeAgent.length==0) {
     //currentDate
     [_btnDateOfLicenceeAgent setTitle:[self currentDate] forState:UIControlStateNormal];
     
     [dictGlobalTermiteFlorida setValue:[self currentDate] forKey:@"DateOfLicenseOrAgent"];
     
     } else {
     
     strDateOfLicenceeAgent=[self changeDateToFormattedDate:strDateOfLicenceeAgent];
     
     [_btnDateOfLicenceeAgent setTitle:strDateOfLicenceeAgent forState:UIControlStateNormal];
     
     }*/
    strDateOfLicenceeAgent=[self changeDateToFormattedDate:strDateOfLicenceeAgent];
    [_btnDateOfLicenceeAgent setTitle:strDateOfLicenceeAgent forState:UIControlStateNormal];
    
    _txtFieldAddressOfPropertyInspectedView5.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteFlorida valueForKey:@"AddressOfPropertInspected"]];
    
    
}


- (IBAction)actionOnBtnDateOfLicenceeAgent:(id)sender {
}
-(void)setTextViewColor:(UITextView*)textView
{
    textView.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    textView.layer.borderWidth=1.0;
    textView.layer.cornerRadius=5.0;
    
}
-(void)setButtonwColor:(UIButton*)btn
{
    [btn.layer setCornerRadius:5.0f];
    [btn.layer setBorderColor:[UIColor blackColor].CGColor];
    [btn.layer setBorderWidth:0.8f];
    [btn.layer setShadowColor:[UIColor blackColor].CGColor];
    [btn.layer setShadowOpacity:0.3];
    [btn.layer setShadowRadius:3.0];
    [btn.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}
-(void)methodSetAllViewsFlorida
{
    
    [self setTextViewColor:_txtViewDAMAGEcausedbyWDO];
    [self setTextViewColor:_txtViewForComment];
    [self setTextViewColor:_txtViewAtticReason];
    [self setTextViewColor:_txtViewOtherReason];
    [self setTextViewColor:_txtViewExteriorReason];
    [self setTextViewColor:_txtViewInteriorReason];
    [self setTextViewColor:_txtViewEvidenceOfWDO];
    [self setTextViewColor:_txViewCrawlPlaceReason];
    
    /*[_btnDateOfInspection.layer setCornerRadius:5.0f];
     [_btnDateOfInspection.layer setBorderColor:[UIColor blackColor].CGColor];
     [_btnDateOfInspection.layer setBorderWidth:0.8f];
     [_btnDateOfInspection.layer setShadowColor:[UIColor blackColor].CGColor];
     [_btnDateOfInspection.layer setShadowOpacity:0.3];
     [_btnDateOfInspection.layer setShadowRadius:3.0];
     [_btnDateOfInspection.layer setShadowOffset:CGSizeMake(2.0, 2.0)];*/
    [self setButtonwColor:_btnDateOfInspection];
    [self setButtonwColor:_btnInspectionDateFloridaView5];
    [self setButtonwColor:_btnDateOfLicenceeAgent];
    
    CGRect frameFor_view_CustomerInfo=CGRectMake(10, 0, self.view.frame.size.width-20, _view_CustomerInfo.frame.size.height);
    frameFor_view_CustomerInfo.origin.x=10;
    frameFor_view_CustomerInfo.origin.y=0;
    
    [_view_CustomerInfo setFrame:frameFor_view_CustomerInfo];
    
    [_scrollVieww addSubview:_view_CustomerInfo];
    
    CGRect frameFor_View1=CGRectMake(0, _view_CustomerInfo.frame.origin.y+_view_CustomerInfo.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view1Florida.frame.size.height);
    [_view1Florida setFrame:frameFor_View1];
    
    [_scrollVieww addSubview:_view1Florida];
    
    CGRect frameFor_View2;
    if ([UIScreen mainScreen].bounds.size.height<570)
    {
        frameFor_View2=CGRectMake(0, _view1Florida.frame.origin.y+_view1Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view2Florida.frame.size.height);
    }
    else
    {
        frameFor_View2=CGRectMake(0, _view1Florida.frame.origin.y+_view1Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,1840+20);
    }
    
    [_view2Florida setFrame:frameFor_View2];
    
    [_scrollVieww addSubview:_view2Florida];
    
    CGRect frameFor_View3=CGRectMake(0, _view2Florida.frame.origin.y+_view2Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view3Florida.frame.size.height);
    [_view3Florida setFrame:frameFor_View3];
    
    
    
    [_scrollVieww addSubview:_view3Florida];
    
    
    CGRect frameFor_View4=CGRectMake(0, _view3Florida.frame.origin.y+_view3Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view4Florida.frame.size.height);
    [_view4Florida setFrame:frameFor_View4];
    
    [_scrollVieww addSubview:_view4Florida];
    
    CGRect frameFor_View5=CGRectMake(0, _view4Florida.frame.origin.y+_view4Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5Florida.frame.size.height);
    [_view5Florida setFrame:frameFor_View5];
    
    [_scrollVieww addSubview:_view5Florida];
    
    
    
    //[_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_view5.frame.size.height+_view5.frame.origin.y)];
    CGRect frameFor_viewPaymentInfo=CGRectMake(0, _view5Florida.frame.origin.y+_view5Florida.frame.size.height, [UIScreen mainScreen].bounds.size.width,_paymentTypeView.frame.size.height);
    
    [_paymentTypeView setFrame:frameFor_viewPaymentInfo];
    
    
    //Change For Service job descriptions
    
    [_scrollVieww addSubview:_paymentTypeView];
    
    _viewServiceJobDescriptions.frame=CGRectMake(0, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width, heightForServiceJobDescriptions);
    
    [_scrollVieww addSubview:_viewServiceJobDescriptions];
    
    if (heightForServiceJobDescriptions==0) {
        
        _viewPaymentInfo.frame=CGRectMake(10, _paymentTypeView.frame.origin.y+_paymentTypeView.frame.size.height+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
        
    } else {
        
        _viewPaymentInfo.frame=CGRectMake(10, _viewServiceJobDescriptions.frame.origin.y+heightForServiceJobDescriptions+10,self.view.frame.size.width-20, _viewPaymentInfo.frame.size.height);
        
    }
    
    [_scrollVieww addSubview:_viewPaymentInfo];
    
    
    _view_TechSignature.frame=CGRectMake(10, _viewPaymentInfo.frame.origin.y+_viewPaymentInfo.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_TechSignature.frame.size.height);
    
    [_scrollVieww addSubview:_view_TechSignature];
    
    _viewCustomerSignature.frame=CGRectMake(10, _view_TechSignature.frame.origin.y+_view_TechSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _viewCustomerSignature.frame.size.height);
    
    [_scrollVieww addSubview:_viewCustomerSignature];
    
    _view_SavenContinue.frame=CGRectMake(10, _viewCustomerSignature.frame.origin.y+_viewCustomerSignature.frame.size.height+10,[UIScreen mainScreen].bounds.size.width-20, _view_SavenContinue.frame.size.height);
    
    [_scrollVieww addSubview:_view_SavenContinue];
    
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *contextNew = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [_txtViewTermsnConditions.text boundingRectWithSize:constraint
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:_txtViewTermsnConditions.font}
                                                                     context:contextNew].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    if (size.height<85) {
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, 100);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, 85+40);
        
        
    }else{
        
        _txtViewTermsnConditions.frame=CGRectMake(_txtViewTermsnConditions.frame.origin.x, _txtViewTermsnConditions.frame.origin.y, _txtViewTermsnConditions.frame.size.width, size.height+40);
        
        _viewTermsnConditions.frame=CGRectMake(10, _view_SavenContinue.frame.origin.y+_view_SavenContinue.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, size.height+40);
        
    }
    
    [_scrollVieww addSubview:_viewTermsnConditions];
    
    
    [_scrollVieww setContentSize:CGSizeMake( [UIScreen mainScreen].bounds.size.width-2,_viewTermsnConditions.frame.size.height+_viewTermsnConditions.frame.origin.y)];
    
}
-(void)setEditableOrNot
{
    [_txtFieldForInspectionDateForView5 setEnabled:NO];
    [_txtDateOfInspection setEnabled:NO];
    [_txtFieldForInspectionDateForView5 setEnabled:NO];
    [_txtFieldDateForView5 setEnabled:NO];
    
    
    [_txtInspectionCompany setEnabled:NO];
    
    // [self setTextDisable:_txtInspectionCompany];
    
    [_txtBusinessLicenseNo setEnabled:NO];
    
    // [self setTextDisable:_txtBusinessLicenseNo];
    
    [_txtCompanyAddress setEnabled:NO];
    [_txtComapnyCity setEnabled:NO];
    [_txtComapnyState setEnabled:NO];
    [_txtComapnyZipcode setEnabled:NO];
    [_txtPhoneNo setEnabled:NO];
    [_txt1InspectorNameAndIdentificationNo setEnabled:NO];
    [_txt2InspectorNameAndIdentificationNo setEnabled:NO];
    [_btnDateOfInspection setEnabled:NO];
    [_txtAddressOfPropoertyInspected setEnabled:NO];
    [_txtStructurePropertyInspected setEnabled:NO];
    [_txtInspectionAndReportRequestedBy setEnabled:NO];
    [_txtInspectionAndReportRequestedTo setEnabled:NO];
    [_btnCheckBoxA setEnabled:NO];
    [_btnCheckBoxB setEnabled:NO];
    [_txtLiveWDO setEnabled:NO];
    [_btnCheckBoxEvidenceOfWDO setEnabled:NO];
    [_txtViewEvidenceOfWDO setEditable:NO];
    [_btnChkBoxDAMAGEcausedbyWDO setEnabled:NO];
    [_txtViewDAMAGEcausedbyWDO setEditable:NO];
    [_btnCheckBoxAttic setEnabled:NO];
    [_txtFieldAtticSpecificArea setEnabled:NO];
    [_txtViewAtticReason setEditable:NO];
    [_btnCheckBoxInterior setEnabled:NO];
    [_txtFieldInteriorSpecificArea setEnabled:NO];
    [_btnCheckBoxExterior setEnabled:NO];
    [_txtFieldExteriorSpecificArea setEnabled:NO];
    [_txtViewExteriorReason setEditable:NO];
    [_btnCheckBoxCrawlPlace setEnabled:NO];
    [_txtFieldCrawlPlaceSpecificArea setEnabled:NO];
    
    [_txViewCrawlPlaceReason setEditable:NO];
    [_btnCheckBoxOther setEnabled:NO];
    [_txtFieldOtherSpecificArea setEnabled:NO];
    [_txtViewOtherReason setEditable:NO];
    [_btnRadioEvidencForYes setEnabled:NO];
    [_btnRadioEvidenceForNo setEnabled:NO];
    [_txtFieldForEvidence setEnabled:NO];
    [_txtFieldForNoticeOfInspection setEnabled:NO];
    [_btnRadioCompanyStructureForYes setEnabled:NO];
    [_btnRadioCompnayStructureForNo setEnabled:NO];
    [_txtFieldForCommonNameOfOrganism setEnabled:NO];
    [_txtFieldForNameOfPesticideUsed setEnabled:NO];
    [_txtFieldForTermsAndCondition setEnabled:NO];
    [_btnChkBoxWholeStructure setEnabled:NO];
    [_btnChkBoxSpotTreatment setEnabled:NO];
    
    [_txtViewForComment setEditable:NO];
    [_btnSignatureLicenseAgent setEnabled:NO];
    [_btnInspectionDate setEnabled:NO];
    [_btnDateOfLicenceeAgent setEnabled:NO];
    [_txtFieldAddressOfPropertyInspectedView5 setEnabled:NO];
    
    
}
-(void)setTextDisable:(UITextField*)textField
{
    textField.inputView = [[UIView alloc] init];
    textField.delegate = self;
    
}

-(NSString*)changeDateToFormattedDate :(NSString*)strDateBeingConverted{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    //[dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;
    
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

//----
#pragma mark- Method if Time In in Nil
//----

-(void)addPickerViewForTimeIn :(NSString*)strGoToWhichView
{
    
    [_workOrderDetail setValue:@"InComplete" forKey:@"workorderStatus"];
    NSError *error;
    [context save:&error];
    
    pickerDate=[[UIDatePicker alloc]init];
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    //UIDatePickerModeTime
        pickerDate.datePickerMode = UIDatePickerModeDateAndTime;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    singleTap1.numberOfTapsRequired = 1;
    [viewBackGround setUserInteractionEnabled:YES];
    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"Please select start time to complete work order";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.numberOfLines=2;
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    if ([strGoToWhichView isEqualToString:@"sendmail"]) {
        btnDone.tag=1111111111;
    } else {
        btnDone.tag=1111111112;
    }
    
    [btnDone setTitle:@"Save" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewForDate addSubview:btnDone];
    
    [btnDone addTarget: self action: @selector(setDateOnDoneTimeIn:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

-(void)setDateOnDoneTimeIn :(id)sender
{
    
    UIButton *btn=(UIButton*)sender;
    int taggggs=(int)btn.tag ;
    
    yesEditedSomething=YES;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString  *strDateTimeIn = [dateFormat stringFromDate:pickerDate.date];
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSString *strMsgs=[NSString stringWithFormat:@"Are you sure %@ is your start time for work order..??",strDateTimeIn];
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm...???"
                               message:strMsgs
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Save & Continue" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              [viewBackGround removeFromSuperview];
                              
                              [_workOrderDetail setValue:strDateTimeIn forKey:@"timeIn"];
                              [_workOrderDetail setValue:latitude forKey:@"timeInLatitude"];
                              [_workOrderDetail setValue:longitude forKey:@"timeInLongitude"];
                              [_workOrderDetail setValue:@"Completed" forKey:@"workorderStatus"];
                              
                              NSError *error;
                              [context save:&error];
                              
                              if (taggggs==1111111111) {
                                  
                                  //SendMail Pe Jana Hai
                                  
                                  [self goToSendEmailView];
                                  
                              } else {
                                  
                                  //Credit Card  Pe Jana Hai
                                  
                                  [self sendToCreditCardView];
                                  
                              }
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Change start time" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                         }];
    [alert addAction:no];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}



@end
