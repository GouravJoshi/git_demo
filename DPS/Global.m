//
//  Global.m
//  CaptureTheirFlag
//  Saavan Test
//  Created by Rakesh Jain on 30/09/15.
//  Copyright © 2015 Sankalp. All rights reserved.
//
//  Saavan Patidar
//  Saavan Patidar
//  Saavan Patidar
//  2021
//


#import "Header.h"
#import "Global.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "AddLead+CoreDataProperties.h"
#import "AddLead.h"
#import "OutBox+CoreDataProperties.h"
#import "OutBox.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AllImportsViewController.h"
#import <MessageUI/MessageUI.h>
#import <AVKit/AVKit.h>
#import "DPS-Swift.h"

@implementation Global
{
    NSMutableData *responseData;
    NSDictionary *ResponseDict;
    NSMutableDictionary *dictFinal,*dictSubWorkOrderDetails,*dictSubWorkOrderIssuesDetails,*dictSubWorkOrderIssuesRepairDetails;
    NSMutableArray *arrOfSubWorkOrderDb,*arrOfSubWorkOrderIssuesDb,*arrOfSubWorkOrderIssuesRepairDb;
    
    NSString *strWorkOrderType;
    
    NSEntityDescription *entitySubWorkOrderIssues,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHrs,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderIssuesRepairLabour,*entitySubWorkOrderMechanicalEquipment,*entityMechanicalSubWorkOrder;
    
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestSubWorkOrderMechanicalEquipment;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorSubWorkOrderMechanicalEquipment;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsSubWorkOrderMechanicalEquipment;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour,*matchesSubWorkOrderMechanicalEquipment;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour,*arrAllObjSubWorkOrderMechanicalEquipment;

}

//============================================================================
//============================================================================
#pragma mark--- Alert View Method
//============================================================================
//============================================================================

-(void)AlertMethod :(NSString *)strMsgTitle :(NSString *)strMsg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strMsgTitle message:strMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)displayAlertController :(NSString *)strMsgTitle :(NSString *)strMsg :(UIViewController*)view
{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:strMsgTitle
                               message:strMsg
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    [view presentViewController:alert animated:YES completion:nil];
    
}

-(void)AlertMethodNew :(NSString *)strMsgTitle :(NSString *)strMsg :(UIViewController*)view
{
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strMsgTitle message:strMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //    [alert show];
    
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Info"
                               message:@"Notifications are not allowed.Please go to settings and allow"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Later" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                 [[UIApplication sharedApplication] openURL:url];
                             } else {
                                 
                                 
                             }
                             
                         }];
    [alert addAction:no];
    [view presentViewController:alert animated:YES completion:nil];
    
    
}

//============================================================================
//============================================================================
#pragma mark--- Get Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallback)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlock)callback
{
    getServerResponseForUrlCallback = callback;
    [self onBackendResponse:nil withSuccess:YES error:nil withUrl:url withType:Type];
}

- (void)onBackendResponse:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString*)strType
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60000.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
//    if ([strType isEqualToString:@"TotalWorkOrderServiceAutomation"]) {
//
//        [request addValue:@"true" forHTTPHeaderField:@"IsCorporateUser"];
//
//    }
    
    
    if ([strType isEqualToString:@"TotalLead"] || [strType isEqualToString:@"LeadCount"] || [strType isEqualToString:@"History"] || [strType isEqualToString:@"ResetStatus"]) {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
        NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
        NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
        NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
        NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
        NSString *strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
        [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
        [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
        [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
        [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
        [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
        
        //        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        //
        //        sessionConfig.timeoutIntervalForRequest = 5000.0;
        //        sessionConfig.timeoutIntervalForResource = 5000.0;
        
        // request.timeoutInterval=20000.0;
        
    }
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSString * returnString;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        //ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        if([strType isEqualToString:@"ResetStatus"])
        {
            returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        }
        else
        {
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        }
        
        if ([strType isEqualToString:@"MechanicalSubWorkOrderStatusNew"]) {
            
            //Yaha Dispacth Ka Response hai getMatchingAccounts
            
        }
        else if([strType isEqualToString:@"ResetStatus"])
        {

            if ([strType isEqualToString:@"ResetStatus"]) {
                
                if ([returnString isEqualToString:@"true"]) {
                    
                    ResponseDict = @{@"Response":@"true",
                                                     };

                } else {

                    ResponseDict = @{@"Response":@"false",
                                                     };

                }
            }
        }
        else {
            // MechanicalSubWorkOrderStatusDispatch  @"getAddresss"
            if ([strType isEqualToString:@"getActivity"] || [strType isEqualToString:@"getTask"] || [strType isEqualToString:@"salesInspection"] || [strType isEqualToString:@"serviceInspection"] || [strType isEqualToString:@"getMasterProductChemicalsServiceAutomation"] || [strType isEqualToString:@"getDocumentService"] || [strType isEqualToString:@"getSavedCardSales"] || [strType isEqualToString:@"getAddresss"] || [strType isEqualToString:@"getLeadOrOpportunity"] || [strType isEqualToString:@"getMatchingAccounts"] || [strType isEqualToString:@"getWeeklyTimeSheet"]||[strType isEqualToString:@"TotalLead"])
            {
                if (ResponseDict.count==0)
                {
                    
                } else {
                    
                    if ([strType isEqualToString:@"getLeadOrOpportunity"])
                    {

                        
                        
                    }
                    else if ([strType isEqualToString:@"TotalLead"]) {
                        
                        
                        
                    }
                    else{
                        
                        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                        [dictTempResponse setObject:ResponseDict forKey:@"response"];
                        NSDictionary *dict=[[NSDictionary alloc]init];
                        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                        ResponseDict =dictData;
                    }

                    
                    if ([strType isEqualToString:@"getMasterProductChemicalsServiceAutomation"]) {
                        
                        NSUserDefaults *defsMaster=[NSUserDefaults standardUserDefaults];
                        [defsMaster setObject:ResponseDict forKey:@"getMasterProductChemicalsServiceAutomation"];
                        [defsMaster synchronize];
                        
                    }
                }
            }else
            {
                ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            }

        }
    }
    @catch (NSException *exception) {
        
        [self AlertMethod:Alert :Sorry];
        
    }
    @finally {
        
    }
    getServerResponseForUrlCallback(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Post Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallbackPostMethod)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrlPostMethod:(NSString *)url :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback
{
    getServerResponseForUrlCallbackPostMethod = callback;
    [self onBackendResponsePostMethod:nil withSuccess:YES error:nil withUrl:url withData:requestDataa];
}

- (void)onBackendResponsePostMethod:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:0 forHTTPHeaderField:@"BranchId"];
    [request addValue:@"" forHTTPHeaderField:@"BranchSysName"];
    [request addValue:@"true" forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:@"" forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSString *strException;
        @try {
            NSArray *arr=[ResponseDict valueForKey:@"Errors"];
            if (!(arr.count==0)) {
                strException=arr[0];
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (strException.length==0) {
            
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
        } else {
            if ([strException isEqualToString:@"Incorrect password."])
            {
                
            }else if ([strException isEqualToString:@"Passwords must have at least one non letter or digit character. Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z')."]){
                
                
                
            }
            else{
                
                ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                
            }
        }
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackPostMethod(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Post Method Call Back Sales Payments Methods
//============================================================================
//============================================================================

void(^responseOnSalesPaymentServices)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)responseOnSalesPaymentServices:(NSString *)url :(NSString *)strTypeSales :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlockSalesPayment)callback
{
    responseOnSalesPaymentServices = callback;
    [self onBackendResponseSalesPayment:nil withSuccess:YES error:nil withUrl:url withType:strTypeSales withData:requestDataa];
}

- (void)onBackendResponseSalesPayment:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString *)strTypeSales withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        
        NSError *error = nil;
        NSString *returnString;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if ([strTypeSales isEqualToString:@"SaveTransactionDetail"]) {
            
            returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        } else {
            
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];

        }
        
        NSString *strException;
        
        if ([strTypeSales isEqualToString:@"SaveTransactionDetail"]) {
            
            if ([returnString isEqualToString:@"true"]) {
                
                ResponseDict = @{@"Response":@"true",
                                                 };

            } else {

                ResponseDict = @{@"Response":@"false",
                                                 };

            }

            
        }else{
            
        if (strException.length==0) {
            
            if ([strTypeSales isEqualToString:@"ProcessPaymentForMobile"] || [strTypeSales isEqualToString:@"CheckAllCreditCardForMobile"]) {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
            }else{
                
                  ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                
            }
            
        } else {
            
            if ([strTypeSales isEqualToString:@"ProcessPaymentForMobile"] || [strTypeSales isEqualToString:@"CheckAllCreditCardForMobile"]) {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
            }else{
 
                  ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                
            }
        }
      }
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    responseOnSalesPaymentServices(success, ResponseDict, error);
}


//============================================================================
//============================================================================
#pragma mark--- Asynchronus Get Method Call Back
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallbackAsynchronus)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrlGetAynchronusMethod:(NSString *)url :(NSString *)type withCallback:(WebServiceCompletionBlockGetAsynchronus)callback
{
    getServerResponseForUrlCallbackAsynchronus = callback;
    if ([type isEqualToString:@"getEmployeeList"])
    {
        [self onBackendResponseAsynchronusgetEmployeeList:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getLeadDetailMaster"])
    {
        [self onBackendResponseAsynchronusgetLeadDetailMaster:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getLeadCount"])
    {
        [self onBackendResponseAsynchronusgetgetLeadCount:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterServiceAutomation"])
    {
        [self onBackendResponseAsynchronusgetMasterServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }//onBackendResponseAsynchronusgetMasterEquipmentsDynamicForm
    else if ([type isEqualToString:@"getMasterEquipmentsDynamicForm"])
    {
        [self onBackendResponseAsynchronusgetMasterEquipmentsDynamicForm:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterSalesAutomation"])
    {
        [self onBackendResponseAsynchronusgetMasterSalesAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    
    else if ([type isEqualToString:@"getMasterProductChemicalsServiceAutomation"])
    {
        [self onBackendResponseAsynchronusgetProductChemicalsServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"SendEmailServiceAutomation"])
    {
        [self onBackendResponseAsynchronusgSendEmailServiceAutomation:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"GetCustomMessage"])
    {
        [self onBackendResponseAsynchronusgGetCustomMessage:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"SendSurveyStatus"])
    {
        [self onBackendResponseAsynchronusgSendSurveyStatus:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"MastersSalesDynamic"])
    {
        [self onBackendResponseAsynchronusgetMasterSalesAutomationDynamicForm:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"MastersServiceDynamic"])
    {
        [self onBackendResponseAsynchronusgetMasterServiceAutomationDynamicForm:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"SalesAllDynamicForm"])
    {
        [self onBackendResponseSynchronusSalesAutoDynamicFormAll:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"ServiceAllDynamicForm"])
    {
        [self onBackendResponseSynchronusServiceAutoDynamicFormAll:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"EquipmentsAllDynamicForm"])
    {
        [self onBackendResponseSynchronusServiceAutoDynamicFormAllEquipments:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterAllMechanical"])
    {
        [self onBackendResponseAsynchronusgetMasterRepairs:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterHourConfig"])
    {
        [self onBackendResponseAsynchronusgetMasterHourConfig:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else if ([type isEqualToString:@"getMasterLookupPricing"])
    {
        [self onBackendResponseAsynchronusgetMasterLookupPricing:nil withSuccess:YES error:nil withUrl:url :type];
    }
    else{
        [self onBackendResponseAsynchronus:nil withSuccess:YES error:nil withUrl:url :type];
    }
}

- (void)onBackendResponseAsynchronus:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             //NSLog(@"Response on Asynchronus = = = =  = %@",ResponseDict);
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- getLeadDetailMaster
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetLeadDetailMaster:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setValue:ResponseDict forKey:@"LeadDetailMaster"];
                 [defs synchronize];
                 AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                 [defaultAf setLeadDetails:jsonData];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on getLeadDeatilMaster = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- getEmployeeList
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetEmployeeList:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict.count==0) {
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 
                 if (strException.length==0) {
                     
                     NSUserDefaults *defsResponse=[NSUserDefaults standardUserDefaults];
                     [defsResponse setObject:dictData forKey:@"EmployeeList"];
                     [defsResponse synchronize];
                     
                 } else {
                     ResponseDict=nil;
                 }
                 
                 NSLog(@"Response on getEmployeeList = = = =  = %@",dictData);
                 
             }
         }];
        
        //        NSError *error = nil;
        //        NSURLResponse * response = nil;
        //        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //        NSData* jsonData = [NSData dataWithData:data];
        //        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- GetLeadCount
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetgetLeadCount:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             NSArray *arrOfLeadStatusMasters =[ResponseDict valueForKey:@"LeadStatusMasters"];
             //             arrOfLeadStatusMasters =[NSArray arrayWithObjects:@"test._-&/",
             //                                      @"Open",
             //                                      @"New",
             //                                      @"Sold",
             //                                      @"Deferred",
             //                                      @"Complete",
             //                                      nil];
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                 NSArray *sortDescriptorsNew = [NSArray arrayWithObject:brandDescriptor];
                 arrOfLeadStatusMasters = [arrOfLeadStatusMasters sortedArrayUsingDescriptors:sortDescriptorsNew];
                 
                 NSMutableArray *arrDelete=[[NSMutableArray alloc]init];
                 
                 for (int k=0; k<arrOfLeadStatusMasters.count; k++) {
                     
                     NSDictionary *dictData=arrOfLeadStatusMasters[k];
                     
                     BOOL isActive=[[dictData valueForKey:@"IsActive"] boolValue];
                     
                     if (!isActive) {
                         
                         [arrDelete addObject:dictData];
                         
                     }
                     
                 }
                 NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                 
                 [arrTemp addObjectsFromArray:arrOfLeadStatusMasters];
                 
                 if (!(arrDelete.count==0)) {
                     [arrTemp removeObjectsInArray:arrDelete];
                 }
                 
                 arrOfLeadStatusMasters=arrTemp;
                 
                 NSUserDefaults *defLeadStatusMasters =[NSUserDefaults standardUserDefaults];
                 [defLeadStatusMasters setObject:arrOfLeadStatusMasters forKey:@"LeadStatusMasters"];
                 [defLeadStatusMasters setObject:ResponseDict forKey:@"TotalLeadCountResponse"];
                 [defLeadStatusMasters synchronize];
                 
                 
                 //  Web Lead Status Master
                 
                 NSArray *arrOfLeadStatusMasters1 =[ResponseDict valueForKey:@"WebLeadStatusMasters"];
                 
                 NSSortDescriptor *brandDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                 NSArray *sortDescriptorsNew1 = [NSArray arrayWithObject:brandDescriptor1];
                 arrOfLeadStatusMasters1 = [arrOfLeadStatusMasters1 sortedArrayUsingDescriptors:sortDescriptorsNew1];
                 NSMutableArray *arrDelete1=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOfLeadStatusMasters1.count; k++) {
                     
                     NSDictionary *dictData1=arrOfLeadStatusMasters1[k];
                     
                     BOOL isActive1=[[dictData1 valueForKey:@"IsActive"] boolValue];
                     
                     if (!isActive1) {
                         
                         [arrDelete1 addObject:dictData1];
                         
                     }
                     
                 }
                 NSMutableArray *arrTempLocal1=[[NSMutableArray alloc]init];
                 
                 [arrTempLocal1 addObjectsFromArray:arrOfLeadStatusMasters1];
                 
                 if (!(arrDelete1.count==0)) {
                     [arrTempLocal1 removeObjectsInArray:arrDelete1];
                 }
                 
                 arrOfLeadStatusMasters1=arrTempLocal1;
                 
                 NSUserDefaults *defLeadStatusMasters1 =[NSUserDefaults standardUserDefaults];
                 [defLeadStatusMasters1 setObject:arrOfLeadStatusMasters1 forKey:@"WebLeadStatusMasters"];
                 [defLeadStatusMasters1 synchronize];
                 
                 // End Web Lead Status Masters
                 
             } else {
                 ResponseDict=nil;
             }
             
             
             NSLog(@"Response on lead count = = = =  = %@",ResponseDict);
         }];
        
        //        NSError *error = nil;
        //        NSURLResponse * response = nil;
        //        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //        NSData* jsonData = [NSData dataWithData:data];
        //        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             NSLog(@"Response on getMasterServiceAutomation = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master MechanicalFlow Repairs
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterRepairs:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict==nil) {
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;

             }
             

          //   ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterAllMechanical"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:@"MastersSync" object:self];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"MastersSyncClick" object:self];

             NSLog(@"Response on MasterRepairs = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Mechanical Flow  Hours Config
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterHourConfig:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             ResponseDict =dictData;

           //  ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterHourConfig"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on getMaster HourConfig = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Lookup pricing
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterLookupPricing:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             ResponseDict =dictData;

//             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterLookupPricing"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on LookupPricing Masters = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Equipments Dynamic Form
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterEquipmentsDynamicForm:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict==nil) {
                 
             } else {

             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             ResponseDict =dictData;
                 
             }
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterEquipmentsDynamicForm"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             NSLog(@"Response on MasterEquipmentsDynamicForm = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Service Automation DynamicForm
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterServiceAutomationDynamicForm:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setObject:ResponseDict forKey:@"MasterServiceAutomationDynamicForm"];
            [defs synchronize];
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response on MasterServiceAutomationDynamicForm = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get All Leads Dynamic Form
//============================================================================
//============================================================================

- (void)onBackendResponseSynchronusSalesAutoDynamicFormAll:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response on SalesAllDynamicForm = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Get All WorkOrder Dynamic Form
//============================================================================
//============================================================================

- (void)onBackendResponseSynchronusServiceAutoDynamicFormAll:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if (ResponseDict.count==0) {
            
        } else {
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            ResponseDict =dictData;

        }
        
//        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
//        [dictTempResponse setObject:ResponseDict forKey:@"response"];
//        NSDictionary *dict=[[NSDictionary alloc]init];
//        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
//        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
//        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response on ServiceAllDynamicForm = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Get All Equipment Dynamic Form
//============================================================================
//============================================================================

- (void)onBackendResponseSynchronusServiceAutoDynamicFormAllEquipments:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response on ServiceAllDynamicForm = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Sales Automation DynamicForm
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterSalesAutomationDynamicForm:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setObject:ResponseDict forKey:@"MasterSalesAutomationDynamicForm"];
            [defs synchronize];
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response on MasterSalesAutomationDynamicForm = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Sales Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetMasterSalesAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
            
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterSalesAutomation"];
                 [defs synchronize];
                 AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                 [defaultAf setMasterDetails:jsonData];
                 
             } else {
                 ResponseDict=nil;
             }
             
             
             NSLog(@"Response on MasterSalesAutomation = = = =  = %@",ResponseDict);
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Master Product Chemicals Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgetProductChemicalsServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"ProductChemicalsServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
             
             NSLog(@"Response on getProductChemicalsServiceAutomation = = = =  = %@",ResponseDict);
         }];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Send Email Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgSendEmailServiceAutomation:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"Successful"]) {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
        } else {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
        }
        NSLog(@"Response on Send Email Service Automation = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}
//============================================================================
//============================================================================
#pragma mark--- Send Email Service Automation
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgSendSurveyStatus:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             
             NSString *returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             if ([returnString isEqualToString:@"Success"]) {
                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
             } else {
                 ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
             }
             
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    
    
    /*
     @try {
     NSError *error = nil;
     NSURLResponse * response = nil;
     NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
     NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
     if ([returnString isEqualToString:@"Success"]) {
     ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
     } else {
     ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
     }
     NSLog(@"Response on Send Email Service Automation = = = =  = %@",ResponseDict);
     
     }
     @catch (NSException *exception) {
     [self AlertMethod:Alert :Sorry];
     }
     @finally {
     }
     */
    
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Get Custom Message
//============================================================================
//============================================================================

- (void)onBackendResponseAsynchronusgGetCustomMessage:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //    [request setHTTPMethod:@"GET"];
    //        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if (ResponseDict==nil) {
            
            ResponseDict=nil;
            
        } else {
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            ResponseDict=dictData;
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defsResponse=[NSUserDefaults standardUserDefaults];
                [defsResponse setObject:dictData forKey:@"CustomMessageData"];
                [defsResponse synchronize];
                
            } else {
                ResponseDict=nil;
            }
        }
        
        NSLog(@"Response on Get Custom Message = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackAsynchronus(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- Save to Core Data Add Lead Info
//============================================================================
//============================================================================

void(^saveToCoreDataAddLeadCallback)(BOOL success, NSError *error);

// --------------
- (void)saveToCoreDataLeadInfo:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback
{
    saveToCoreDataAddLeadCallback = callback;
    [self onSavingDataAddLead:nil withSuccess:YES error:nil withUrl:url withtype:type withDictData:dictData];
}

- (void)onSavingDataAddLead:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withtype:(NSString*)type withDictData:(NSDictionary*)dictData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddLead=[NSEntityDescription entityForName:@"AddLead" inManagedObjectContext:context];
    
    AddLead *objAddLead = [[AddLead alloc]initWithEntity:entityAddLead insertIntoManagedObjectContext:context];
    if ([type isEqualToString:@"CustomerExtDc"]) {
        objAddLead.customerExtDcDict=stringUrl;
        NSError *error1;
        [context save:&error1];
    } else if ([type isEqualToString:@"SelectingService"]) {
        requestNew = [[NSFetchRequest alloc] init];
        [requestNew setEntity:entityAddLead];
        //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
        //[request setPredicate:predicate];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"companyId" ascending:NO];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNew setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerAddLead = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerAddLead setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [self.fetchedResultsControllerAddLead performFetch:&error];
        arrAllObj = [self.fetchedResultsControllerAddLead fetchedObjects];
        if ([arrAllObj count] == 0)
        {
            
        }
        else
        {
            matches = arrAllObj[1];
        }
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        } else {
        }
        [matches setValue:stringUrl forKey:@"leadServiceExtDcsArray"];
        [matches setValue:[dictData valueForKey:@"CompanyId"] forKey:@"CompanyId"];
        [matches setValue:[dictData valueForKey:@"CompanyKey"] forKey:@"CompanyKey"];
        [matches setValue:[dictData valueForKey:@"CustomerId"] forKey:@"CustomerId"];
        [matches setValue:[dictData valueForKey:@"CreatedBy"] forKey:@"CreatedBy"];
        [matches setValue:[dictData valueForKey:@"CreatedDateTime"] forKey:@"CreatedDateTime"];
        [matches setValue:[dictData valueForKey:@"IsAgreementGenerated"] forKey:@"IsAgreementGenerated"];
        [matches setValue:[dictData valueForKey:@"LeadId"] forKey:@"LeadId"];
        [matches setValue:[dictData valueForKey:@"PocId"] forKey:@"PocId"];
        [matches setValue:[dictData valueForKey:@"PrimaryServiceId"] forKey:@"PrimaryServiceId"];
        [matches setValue:[dictData valueForKey:@"UserName"] forKey:@"UserName"];
        NSError *error2 = nil;
        // Save the object to persistent store
        if (![context save:&error2]) {
            NSLog(@"Can't Save! %@ %@", error2, [error2 localizedDescription]);
        }
    }
    saveToCoreDataAddLeadCallback(success, error);
}

//============================================================================
//============================================================================
#pragma mark--- Delete Data AddLead
//============================================================================
//============================================================================

-(void)deleteDatabaseBeforeAdding
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddLead=[NSEntityDescription entityForName:@"AddLead" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"AddLead" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}


//============================================================================
//============================================================================
#pragma mark--- Save Data OutBox
//============================================================================
//============================================================================

//============================================================================
//============================================================================
#pragma mark--- Save to Core Data Add Lead Info
//============================================================================
//============================================================================

void(^saveToCoreDataOutBoxCallback)(BOOL success, NSError *error);

// --------------
- (void)saveToCoreDataOutBox:(NSString *)url :(NSString *)type :(NSDictionary *)dictData withCallback:(CoreDataAddleadsSaveBlock)callback
{
    saveToCoreDataOutBoxCallback = callback;
    [self onSavingDataOutBox:nil withSuccess:YES error:nil withUrl:url withtype:type withDictData:dictData];
}

- (void)onSavingDataOutBox:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withtype:(NSString*)type withDictData:(NSDictionary*)dictData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityOutBox=[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context];
    
    OutBox *objOutBox = [[OutBox alloc]initWithEntity:entityOutBox insertIntoManagedObjectContext:context];
    objOutBox.finalJson=stringUrl;
    NSError *error1;
    [context save:&error1];
    saveToCoreDataOutBoxCallback(success, error);
}

//============================================================================
//============================================================================
#pragma mark--- Sent Lead To Server OutBox
//============================================================================
//============================================================================

void(^getServerResponseForSendLeadToServer)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSendLeadToServer:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSString*)Type withCallback:(SendLeadToServerPostCompletionBlock)callback
{
    getServerResponseForSendLeadToServer = callback;
    [self onBackendResponseSendLeadToServer:nil withSuccess:YES error:nil withUrl:url withData:requestDataa withIndex:indexToDletee withType:Type];
}

- (void)onBackendResponseSendLeadToServer:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData withIndex:(NSString*)indexToDelete withType:(NSString*)Typeee
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:6000.0];
    [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
    NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
    NSString *strEmployeeEmail =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    NSString *strSalesProcessCompanyId;
    if ([Typeee isEqualToString:@"serviceOrder"]) {
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
        
    }else{
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        
    }
    
    NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
    [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
    [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
    [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
    //[request addValue:strEmployeeName forHTTPHeaderField:@"CreatedBy"];
    //[request addValue:strCoreCompanyId forHTTPHeaderField:@"CreatedBy"];//CompanyId
    [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CompanyId"];
    [request addValue:strEmployeeEmail forHTTPHeaderField:@"EmployeeEmail"];
    if ([Typeee isEqualToString:@"serviceOrder"]) {
        
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"ServiceAutoCompanyId"];
        
    }else{
        
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        
    }
    [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        if ([Typeee isEqualToString:@"leadFromOutbox"]) {
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
             {
                 NSData* jsonData = [NSData dataWithData:data];
                 
               //  NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                 
                 if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                 {
                     
                     ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];

                     
                 }else{
                     
                     ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];

                     NSString *StrAccountNoss=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"MobileAccountId"]];
                     if (StrAccountNoss.length==0) {
                         
                         StrAccountNoss=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"AccountNo"]];
                         
                     }
                     NSArray *arrOfCustomerAddress=(NSArray*)[ResponseDict valueForKey:@"ServiceAddresses"];
                     
                     if (arrOfCustomerAddress.count>0) {

                     [self saveToCoreDataAddress:arrOfCustomerAddress :StrAccountNoss];
                         
                     }

                 }

                 NSLog(@"Response ===========  %@",ResponseDict);
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 
                 
                 if (strException.length==0) {
                     
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Opportunity has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     
                     [self deleteFromOutBoxCoreData:indexToDelete];
                     
                 } else {
                     
                     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     
                 }
                 
             }];
            
        }else if ([Typeee isEqualToString:@"ActualHrsdsfg"]){
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
             {
                 NSData* jsonData = [NSData dataWithData:data];
                 ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                 ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 
                 if (strException.length==0) {
                     
                     
                 } else {
                     
                     
                 }
                 
             }];

            
        }else if ([Typeee isEqualToString:@"resendMailService"]){
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            
            BOOL boolResponse = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] boolValue];
            
            if (boolResponse) {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg", nil];
                
            } else {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg", nil];
                
            }

            
        }else if ([Typeee isEqualToString:@"CrmContactsNew"]){
            
            /*NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            
            BOOL boolResponse = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] boolValue];
            
            if (boolResponse) {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg", nil];
                
            } else {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg", nil];
                
            }*/
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            //ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[self nestedDictionaryByReplacingNullsWithNilReplicaForArray:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            
            if (dictData.count > 0) {
                
                NSArray *arrResponse = (NSArray*)dictData;
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg",arrResponse,@"Response", nil];
                
            } else {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg",@"",@"Response", nil];
                
            }
            
        }else if ([Typeee isEqualToString:@"CrmCompanyV2"]){
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            /*NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[self nestedDictionaryByReplacingNullsWithNilReplicaForArray:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];*/
            
            if (ResponseDict.count > 0) {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg",ResponseDict,@"Response", nil];
                
            } else {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg",@"",@"Response", nil];
                
            }
            
        }else if ([Typeee isEqualToString:@"CrmCompaniesNew"]){
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            
            NSString *strResponse = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            if (strResponse.length > 0) {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg",strResponse,@"CrmCompanyId", nil];
                
            } else {
                
                ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg",@"",@"CrmCompanyId", nil];
                
            }
            
            
        }else {
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSLog(@"Main Response On Uploading....:----%@",ResponseDict);
            
        }
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForSendLeadToServer(success, ResponseDict, error);
}

//============================================================================
//============================================================================
#pragma mark--- -----------------deleting From OutBox Core Data-------------------
//============================================================================
//============================================================================

-(void)deleteFromOutBoxCoreData :(NSString *)strDateTime{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context]];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"datenTime=%@",strDateTime];
    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [context executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [context  deleteObject:car];
        NSError *saveError = nil;
        [context save:&saveError];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OutBoxRefresh" object:self];
    
}
-(void)deleteFromOutBoxCoreDataOffline :(NSString *)strDateTime{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context]];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"datenTime=%@",strDateTime];
    [allData setPredicate:p];
    NSError * error = nil;
    NSArray *Data = [context executeFetchRequest:allData error:&error];
    for (NSManagedObject * car in Data) {
        [context  deleteObject:car];
        NSError *saveError = nil;
        [context save:&saveError];
    }
}

//============================================================================
//============================================================================
#pragma mark--- -----------------Null Conversion-------------------
//============================================================================
//============================================================================

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        
        //NSLog(@"---%@-----%@",key,object);
        
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}


-(NSDictionary *) nestedDictionaryByReplacingNullsWithNilReplicaForArray:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        
       // NSLog(@"---%@-----%@",key,object);
        
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNilReplicaForArray:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNilReplicaForArray:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
                if([record isKindOfClass:[NSArray class]])
                {
                    [nullFreeRecords addObject:record];
                }
                if([record isKindOfClass:[NSString class]])
                {
                    [nullFreeRecords addObject:record];
                }
                if([record isKindOfClass:[NSNumber class]])
                {
                    [nullFreeRecords addObject:record];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

/*
- (NSString *)getIPAddress {
    
    NSString *address = @"0.0.0.0";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

 */
//============================================================================
//============================================================================
#pragma mark ---------------- Date Change Method--------------------------
//============================================================================
//============================================================================
-(NSString *)ChangeTimeFromTwToTFHrsFormat :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"hh:mm a"];//07:00 pm
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSDate *)changeStringTimeToDate :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];//07:00 pm
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    return newTime;
}

-(NSDate *)changeStringDateToDate :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];//07:00 pm
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    return newTime;
}

-(NSDate *)convertDateToTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//07:00 pm
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    NSDate* newTimee = [dateFormatter dateFromString:finalTime];
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }

    return newTimee;
}

-(NSDate *)convertStringDateToDateForPestFlow :(NSString*)strDateToConvert :(NSString*)strTimeZone{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//07:00 pm
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    NSDate* newTimee = [dateFormatter dateFromString:finalTime];
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    return newTimee;
}
-(NSDate *)convertStringDateToDateForCrmNewFlow :(NSString*)strDateToConvert :(NSString*)strDateFormatToConvert :(NSString*)strTimeZone{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//07:00 pm
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:strDateFormatToConvert];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    NSDate* newTimee = [dateFormatter dateFromString:finalTime];
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    return newTimee;
}

-(NSString *)latestStringDateToStringFormatted :(NSString*)strDateToConvert :(NSString*)strDateFormatToConvert :(NSString*)strTimeZone{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];//07:00 pm
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
    [dateFormatter setDateFormat:strDateFormatToConvert];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    NSDate* newTimee = [dateFormatter dateFromString:finalTime];
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    if (newTime==nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];//07:00 pm
        newTime = [dateFormatter dateFromString:strDateToConvert];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        [dateFormatter setDateFormat:strDateFormatToConvert];
        finalTime = [dateFormatter stringFromDate:newTime];
        newTimee = [dateFormatter dateFromString:finalTime];
    }
    return finalTime;
}

-(NSString *)ChangeDateToLocalDate :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
-(NSString *)ChangeDateToLocalDateFull :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOther :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];

    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];

    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];

    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherTaskDue :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}


-(NSString *)ChangeDateToLocalDueTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}
-(NSString *)ChangeDateToLocalActivityTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}
-(NSString *)ChangeDateToLocalDueTime24Hr :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"HH:mm"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}
-(NSString *)ChangeDateMechanicalEquipment :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
 
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherSaavan :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

//Nilind 5 Nov
-(NSString *)ChangeDateToLocalDateOtherLeadView :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];  //MM/dd/yyyy hh:mm a   //MM/dd/yyyy HH:mm
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

//...................................
-(NSString *)ChangeDateToLocalDateOtherServiceAppointments :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
-(NSString *)ChangeDateToLocalDateOtherServiceAppointmentsUpdated :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherTimeAlso :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherSep :(NSString*)strDateToConvert{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    if ([finalTime isEqualToString:@""]) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateOtherNew :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}

-(NSString*)changeDateToFormattedDateNew :(NSString*)strDateBeingConverted{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;

}
//============================================================================
//============================================================================
#pragma mark--- Sent Lead To Server OutBox Offline
//============================================================================
//============================================================================

void(^getServerResponseForSendLeadToServerOffline)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSendLeadToServerOffline:(NSString *)url :(NSData *)requestDataa :(NSDictionary *)dictData :(NSString*)indexToDletee :(NSDictionary *)dictDataAllValues withCallback:(SendLeadToServerPostCompletionBlockOffline)callback
{
    getServerResponseForSendLeadToServerOffline = callback;
    [self onBackendResponseSendLeadToServerOffline:nil withSuccess:YES error:nil withUrl:url withData:requestDataa withIndex:indexToDletee withDataValues:dictDataAllValues];
}

- (void)onBackendResponseSendLeadToServerOffline:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData withIndex:(NSString*)indexToDelete withDataValues:(NSDictionary*)dictLoginData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"HrmsCompanyId"]];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CreatedBy"]];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CoreCompanyId"]];
    NSString *strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"SalesProcessCompanyId"]];
    NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CompanyKey"]];
    
    [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
    [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
    [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
    [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
    [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
    [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];
    [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
    [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
             {
                 
                 ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];

                 
             }else{
                 
                 ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
                 
                 NSString *StrAccountNoss=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"MobileAccountId"]];
                 if (StrAccountNoss.length==0) {
                     
                     StrAccountNoss=[NSString stringWithFormat:@"%@",[ResponseDict valueForKey:@"AccountNo"]];
                     
                 }
                 NSArray *arrOfCustomerAddress=(NSArray*)[ResponseDict valueForKey:@"ServiceAddresses"];
                 
                 if (arrOfCustomerAddress.count>0) {

                 [self saveToCoreDataAddress:arrOfCustomerAddress :StrAccountNoss];
                     
                 }

                 
             }
             NSLog(@"Response ===========  %@",ResponseDict);
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (strException.length==0) {
                 
                 //                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Lead has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 //                 [alert show];
                 
                 [self deleteFromOutBoxCoreDataOffline:indexToDelete];
                 
             } else {
                 
                 //                 UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 //                 [alert show];
                 
             }
             
         }];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForSendLeadToServerOffline(success, ResponseDict, error);
}


-(void)saveToCoreDataAddress :(NSArray*)arrOfAddressListToSave :(NSString*)strAccountNoss{
    
    //==================================================================================
    //==================================================================================
    [self deleteFromCoreDataCustomerAddress :strAccountNoss];
    //==================================================================================
    //==================================================================================
    
    NSEntityDescription *entityAddAddress;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
    
    CustomerAddress *objTaskList = [[CustomerAddress alloc]initWithEntity:entityAddAddress insertIntoManagedObjectContext:context];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    objTaskList.userName=strUserName;
    objTaskList.companyKey=strCompanyKey;
    objTaskList.customerAddress=arrOfAddressListToSave;
    objTaskList.accountNo=strAccountNoss;
    if (arrOfAddressListToSave.count>0) {
        
        NSError *error1;
        [context save:&error1];
        
    }
    
}

-(void)deleteFromCoreDataCustomerAddress :(NSString*)strAccountNoss{
    
    NSEntityDescription *entityAddAddress;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
    
    //  Delete Student Data
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@",strAccountNoss];
    [allData setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}

//============================================================================
//============================================================================
#pragma mark--- Sales Dynamic Json Call Back
//============================================================================
//============================================================================

void(^getServerResponseForDynamicJsonUrlCallbackPostMethod)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForSalesDynamicJson:(NSString *)url :(NSData *)requestDataa withCallback:(SalesDynamicJsonPostCompletionBlock)callback
{
    getServerResponseForDynamicJsonUrlCallbackPostMethod = callback;
    [self onBackendResponseDynamicJsonPostMethod:nil withSuccess:YES error:nil withUrl:url withData:requestDataa];
}

- (void)onBackendResponseDynamicJsonPostMethod:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        //        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
        //                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
        //         {
        //             NSData* jsonData = [NSData dataWithData:data];
        //             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        //         }];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        // NSData* jsonData = [NSData dataWithData:data];
        NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"Successful"] || [returnString isEqualToString:@"Success"]) {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"",@"ReturnMsg", nil];
        } else {
            ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:returnString,@"ReturnMsg", nil];
        }
        NSLog(@"Response on Send Sale Automation Dynamic Json = = = =  = %@",ResponseDict);
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForDynamicJsonUrlCallbackPostMethod(success, ResponseDict, error);
}
//============================================================================
#pragma mark--- CHANGE BY NILIND
//============================================================================


-(void)updateLeadStatus:(NSString *)strLeadId
{

appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
context = [appDelegate managedObjectContext];
entityLeadDetailss=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
requestLeadDetailss = [[NSFetchRequest alloc] init];
[requestLeadDetailss setEntity:entityLeadDetailss];
NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
[requestLeadDetailss setPredicate:predicate];
sortDescriptorLeadDetailss = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
sortDescriptorsLeadDetailss = [NSArray arrayWithObject:sortDescriptorLeadDetailss];
[requestLeadDetailss setSortDescriptors:sortDescriptorsLeadDetailss];
self.fetchedResultsControllerLeadDetailss = [[NSFetchedResultsController alloc] initWithFetchRequest:requestLeadDetailss managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
[self.fetchedResultsControllerLeadDetailss setDelegate:self];
// Perform Fetch
NSError *error = nil;
[self.fetchedResultsControllerLeadDetailss performFetch:&error];
arrAllObjLeadDetailss = [self.fetchedResultsControllerLeadDetailss fetchedObjects];
if ([arrAllObjLeadDetailss count] == 0)
{
}
else
{
  for (int k=0; k<arrAllObjLeadDetailss.count; k++) {
      matchesLeadDetailss=arrAllObjLeadDetailss[k];
      [matchesLeadDetailss setValue:@"Complete" forKey:@"statusSysName"];
      [matchesLeadDetailss setValue:@"Won" forKey:@"stageSysName"];

      //Final Save
      NSError *error;
      [context save:&error];
  }
}
if (error) {
  NSLog(@"Unable to execute fetch request.");
  NSLog(@"%@, %@", error, error.localizedDescription);
} else {
  
}
}

//Nilind 24 Sept
//============================================================================
#pragma mark- MODIFY DATE
//============================================================================
-(NSString *)modifyDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}
-(NSString *)modifyDateService
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    [dateFormatter setTimeZone:inputTimeZone];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString* finalTime = [dateFormatter stringFromDate:[NSDate date]];
    return finalTime;
}

//..........Nilind 5 Oct.....................................


-(void)updateSalesModifydate:(NSString *)strLeadId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadIdd=%@",strLeadId];
    [requestModifyDate setPredicate:predicate];
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjModifyDate.count; k++) {
            
            matchesModifyDate=arrAllObjModifyDate[k];
            NSDate *currentDate=[NSDate date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
            [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
            NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
            [matchesModifyDate setValue:stringCurrentDate forKey:@"modifyDate"];
            
            NSDate* newTime = [formatter dateFromString:[NSString stringWithFormat:@"%@",stringCurrentDate]];
            
            [self updateLeadModifydate:strLeadId :newTime];
            [self updateSalesZSYNC:strLeadId :@"yes"];

            
//            [matchesModifyDate setValue:newTime forKey:@"dateModified"];
            
            
            //Final Save
            NSError *error;
            [context save:&error];
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}
-(void)updateLeadModifydate:(NSString *)strLeadId :(NSDate*)dateToSet
{
    AppDelegate *appDelegateLeadDate;
    NSEntityDescription *entityLeadDetailDate;
    NSManagedObjectContext *contextLeadDate;
    NSFetchRequest *requestNewLeadDate;
    NSSortDescriptor *sortDescriptorLeadDate;
    NSArray *sortDescriptorsLeadDate;
    NSManagedObject *matchesLeadDate;
    NSArray *arrAllObjModifyDateLeadDate;
    
    
    appDelegateLeadDate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextLeadDate = [appDelegateLeadDate managedObjectContext];
    entityLeadDetailDate=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:contextLeadDate];
    
    requestNewLeadDate = [[NSFetchRequest alloc] init];
    [requestNewLeadDate setEntity:entityLeadDetailDate];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestNewLeadDate setPredicate:predicate];
    sortDescriptorLeadDate = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsLeadDate = [NSArray arrayWithObject:sortDescriptorLeadDate];
    
    [requestNewLeadDate setSortDescriptors:sortDescriptorsLeadDate];
    
    NSFetchedResultsController *fetchedResultsControllerModifyDateLeadDate;
    
    fetchedResultsControllerModifyDateLeadDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewLeadDate managedObjectContext:contextLeadDate sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerModifyDateLeadDate setDelegate:self];
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerModifyDateLeadDate performFetch:&error];
    
    arrAllObjModifyDateLeadDate = [fetchedResultsControllerModifyDateLeadDate fetchedObjects];
    if ([arrAllObjModifyDateLeadDate count] == 0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjModifyDateLeadDate.count; k++) {
            
            matchesLeadDate=arrAllObjModifyDateLeadDate[k];
            [matchesLeadDate setValue:dateToSet forKey:@"dateModified"];
            
            //Final Save
            NSError *error;
            [contextLeadDate save:&error];
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}


-(void)updateSalesZSYNC:(NSString *)strLeadId :(NSString *)strYesNo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetailss=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestLeadDetailss = [[NSFetchRequest alloc] init];
    [requestLeadDetailss setEntity:entityLeadDetailss];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadId];
    [requestLeadDetailss setPredicate:predicate];
    sortDescriptorLeadDetailss = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsLeadDetailss = [NSArray arrayWithObject:sortDescriptorLeadDetailss];
    [requestLeadDetailss setSortDescriptors:sortDescriptorsLeadDetailss];
    self.fetchedResultsControllerLeadDetailss = [[NSFetchedResultsController alloc] initWithFetchRequest:requestLeadDetailss managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerLeadDetailss setDelegate:self];
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerLeadDetailss performFetch:&error];
    arrAllObjLeadDetailss = [self.fetchedResultsControllerLeadDetailss fetchedObjects];
    if ([arrAllObjLeadDetailss count] == 0)
    {
    }
    else
    {
        for (int k=0; k<arrAllObjLeadDetailss.count; k++) {
            matchesLeadDetailss=arrAllObjLeadDetailss[k];
            [matchesLeadDetailss setValue:strYesNo forKey:@"zSync"];
            //Final Save
            NSError *error;
            [context save:&error];
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

//...........................................................
//===========================================================

-(BOOL)isCameraPermissionAvailable{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        
        return true;
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
            }
        }];
        return false;
        
    } else {
        // impossible, unknown authorization status
        
        return false;
        
    }
}
-(BOOL)isGalleryPermission{
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    if (status != ALAuthorizationStatusAuthorized) {
        //show alert for asking the user to give permission
        return false;
    }else
        return true;
}
-(BOOL)isAudioPermissionAvailable{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        
        return true;
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        
        return false;
        
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", AVMediaTypeAudio);
            } else {
                NSLog(@"Not granted access to %@", AVMediaTypeAudio);
            }
        }];
        
        return false;
        
    } else {
        // impossible, unknown authorization status
        
        return false;
        
    }
}

-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
{
    
    NSString *strType=text;
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MM/dd/yyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss A"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
    text=[NSString stringWithFormat:@"%@ %@",strDate,strTime];
    
    UIFont *font = [UIFont boldSystemFontOfSize:17];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    
    //CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGRect rect = CGRectMake(image.size.width-180, image.size.height-30, 180, 30);
    
    if ([strType isEqualToString:@"Sign"]) {

        [[UIColor blackColor] set];

    } else {
        
        [[UIColor whiteColor] set];

    }
    
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(NSString *)RemovingWhiteSpaces :(NSString*)strStringToRemove{
    
    NSString *strTxtUserName=[strStringToRemove stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]];

    return strTxtUserName;
    
}
-(BOOL)isGreaterDate :(NSDate*)fromDate :(NSDate*)toDate{
    
    NSComparisonResult result = [fromDate compare:toDate];
    if(result == NSOrderedDescending)
    {
        return YES;
    }
    else if(result == NSOrderedAscending)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)autoDismissAlerView :(UIAlertView*)alertView{
    
    [self performSelector:@selector(autoDismissAlerViewWithDelay:) withObject:alertView afterDelay:0.5];
    
}
-(void)autoDismissAlerViewWithDelay :(UIAlertView*)alertView{
    
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
}
-(BOOL)isStringNullNilBlank:(NSString *)strString{
    
    if ([strString isKindOfClass:[NSString class]]) {
        if ([strString isEqualToString:@"(null)"] || (strString.length==0) || (strString==nil)) {
            
            NSLog(@"String Is equal to ====%@",strString);
            
            return true;
        }else{
            return false;
        }
    }else if (strString==nil){
        return true;
    }else if (![strString isKindOfClass:[NSString class]]){
        return true;
    }else{
        return false;
    }
}

-(NSDictionary *) nestedDictionaryByReplacingNSDATEwithStringDate:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSDate class];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(NSString *)getIPAddress {
    
    NSString *strIPaddress;
    
    const NSString *WIFI_IF = @"en0";
    NSArray *KNOWN_WIRED_IFS = @[@"en2",@"en3",@"en4"];
    NSArray *KNOWN_CELL_IFS = @[@"pdp_ip0",@"pdp_ip1",@"pdp_ip2",@"pdp_ip3"];
    
    const NSString *UNKNOWN_IP_ADDRESS = @"0.0.0.0";
    
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithDictionary:@{@"wireless":UNKNOWN_IP_ADDRESS,
                                                                                     @"wired":UNKNOWN_IP_ADDRESS,
                                                                                     @"cell":UNKNOWN_IP_ADDRESS}];
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if (temp_addr->ifa_addr == NULL) {
                continue;
            }
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:WIFI_IF]) {
                    // Get NSString from C String
                    [addresses setObject:[NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)] forKey:@"wireless"];
                    
                }
                // Check if interface is a wired connection
                if([KNOWN_WIRED_IFS containsObject:[NSString stringWithUTF8String:temp_addr->ifa_name]]) {
                    [addresses setObject:[NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)] forKey:@"wired"];
                }
                // Check if interface is a cellular connection
                if([KNOWN_CELL_IFS containsObject:[NSString stringWithUTF8String:temp_addr->ifa_name]]) {
                    [addresses setObject:[NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)] forKey:@"cell"];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    
    NSString *strCellAddress =[addresses valueForKey:@"cell"];
    NSString *strWiredAddress =[addresses valueForKey:@"wired"];
    NSString *strWirelessAddress =[addresses valueForKey:@"wireless"];
    
    NSLog(@"address is====%@",addresses);
    
    if (strCellAddress.length>0 && !([strCellAddress isEqualToString:@"0.0.0.0"])) {
        
        strIPaddress=strCellAddress;
        
    } else if (strWiredAddress.length>0 && !([strWiredAddress isEqualToString:@"0.0.0.0"])) {
        
        strIPaddress=strWiredAddress;
        
    } else if (strWirelessAddress.length>0 && !([strWirelessAddress isEqualToString:@"0.0.0.0"])){
        
        strIPaddress=strWirelessAddress;
    } else {
        
        strIPaddress=@"0.0.0.0";
        
    }
    
    NSLog(@"IP Address is====%@",strIPaddress);
    
    return strIPaddress;
}

-(NSString *)getReferenceNumber
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString* referenceNumber = [dateFormatter stringFromDate:[NSDate date]];
    int r = (arc4random() % 99999) + 10000;
    referenceNumber=[NSString stringWithFormat:@"%@%d",referenceNumber,r];
    return referenceNumber;
    
}


-(NSString *)getReferenceNumberNew
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString* referenceNumber = [dateFormatter stringFromDate:[NSDate date]];
    referenceNumber=[NSString stringWithFormat:@"%@",referenceNumber];
    return referenceNumber;
    
}

-(NSString *)strCurrentDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy hh:mm a";
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;

}

-(NSString *)strGetCurrentDateInFormat : (NSString*)strFormat{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = strFormat;//@"MM/dd/yyyy hh:mm a";
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;

}

-(NSString *)strCurrentDateFormatted :(NSString*)strFormat :(NSString*)strTimeZone{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = strFormat;
    if (strTimeZone.length>0) {
        
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:strTimeZone]];
        
    }
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
    
}

-(NSString *)getEmployeeNameViaId :(NSString *)strEmpId{
    
    NSString *strEmployeeNameViaId;
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    if (!(arrOfEmployee.count==0)) {
        
        for (int k=0; k<arrOfEmployee.count; k++) {
            NSDictionary *dictDataId=arrOfEmployee[k];
            NSString *strId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"EmployeeNo"]];
            if ([strId isEqualToString:[NSString stringWithFormat:@"%@",strEmpId]]) {
                strEmployeeNameViaId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"FullName"]];
            }
        }
        
    }
    
    if (strEmployeeNameViaId.length==0) {
        strEmployeeNameViaId=@"";
    }

    return strEmployeeNameViaId;
    
}

-(NSString *)getEmployeeNameViaEMPId :(NSString *)strEmpId{
    
    NSString *strEmployeeNameViaId;
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    if (!(arrOfEmployee.count==0)) {
        
        for (int k=0; k<arrOfEmployee.count; k++) {
            NSDictionary *dictDataId=arrOfEmployee[k];
            NSString *strId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"EmployeeNo"]];
            if ([strId isEqualToString:[NSString stringWithFormat:@"%@",strEmpId]]) {
                strEmployeeNameViaId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"FullName"]];
            }
        }
        
    }
    
    if (strEmployeeNameViaId.length==0) {
        strEmployeeNameViaId=@"N/A";
        strEmployeeNameViaId=@"";
    }
    
    return strEmployeeNameViaId;
    
}

-(NSString *)getEmployeeNameViaEmployeeID :(NSString *)strEmpId{
    
    NSString *strEmployeeNameViaId;
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
    NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
    
    if (!(arrOfEmployee.count==0)) {
        
        for (int k=0; k<arrOfEmployee.count; k++) {
            NSDictionary *dictDataId=arrOfEmployee[k];
            NSString *strId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"EmployeeId"]];
            if ([strId isEqualToString:[NSString stringWithFormat:@"%@",strEmpId]]) {
                strEmployeeNameViaId=[NSString stringWithFormat:@"%@",[dictDataId valueForKey:@"FullName"]];
            }
        }
        
    }
    
    if (strEmployeeNameViaId.length==0) {
        strEmployeeNameViaId=@"N/A";
        strEmployeeNameViaId=@"";
    }
    
    return strEmployeeNameViaId;
    
}
#pragma mark - padding in textfield
+(void)addPaddingView:(UITextField*)textField
{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,5, textField.frame.size.height)];
    
    paddingView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
}

-(NSDate *)ChangeDateToLocalDateMechanical :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    
    return newTime;
}
-(NSDate *)ChangeDateToLocalDateMechanicalUTC :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    
    
    return newTime;
}

-(NSDate *)ChangeDateToLocalDateMechanicalTimeOut :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    return newTime;
}

-(NSDate *)getOnlyDate :(NSDate*)strDateToConvert{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *theDate = [dateFormat stringFromDate:strDateToConvert];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate* newTime = [dateFormatter dateFromString:theDate];
    
    return newTime;
    
}

-(NSString *)getOnlyDateNeww :(NSDate*)strDateToConvert{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *theDate = [dateFormat stringFromDate:strDateToConvert];
    return theDate;
}

-(NSDate *)getOnlyTime :(NSDate*)strDateToConvert{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *theDate = [dateFormat stringFromDate:strDateToConvert];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:theDate];
    
    return newTime;
    
}

-(NSString *)getStartTimeofDay :(NSDate*)strDateToConvert{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *theDate = [dateFormat stringFromDate:strDateToConvert];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:theDate];
    
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];

    return finalTime;
    
}

-(NSString *)ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
   // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTimeFromDate :(NSDate*)strDateToConvert{
    
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:strDateToConvert];
    
    return finalTime;
}

-(NSString *)ChangeDateToLocalDateMechanicalParts :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];

    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}


-(NSString *)changeDtaeEmpTimeSheet :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'Z'"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss z"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}

-(NSString *)strBlockDateTime :(NSString*)strDate{
 
    NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* newTime1 = [dateFormatter1 dateFromString:strDate];
    
    //Add the following line to display the time in the local time zone
    [dateFormatter1 setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSString* finalTime1 = [dateFormatter1 stringFromDate:newTime1];
    
    return finalTime1;
    
}

-(NSString *)ChangeDateToLocalDateMechanicalAllDates :(NSString*)strDateToConvert{
    //timeZoneWithAbbreviation:@"EST" 2020-06-02T02:25:15.883
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    
    return finalTime;
}


-(NSString *)ChangeDateToLocalDateMechanicalAllDatesEST :(NSString*)strDateToConvert{
    //timeZoneWithAbbreviation:@"EST"
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    
    return finalTime;
}

-(NSString *)strCurrentDateFormattedForMechanical{
    
    /*
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
    */
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;

}

-(NSString *)strCurrentDateFormattedForMechanicalNewLocalTime{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
    
}

-(int)ChangeTimeMechanical :(NSString*)strDateToConvert{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm:ss";
    
    if (strDateToConvert.length==5) {
        
        formatter.dateFormat = @"HH:mm";
        
    }
    NSDate *timeDate = [formatter dateFromString:strDateToConvert];
    
    formatter.dateFormat = @"HH";
    int hours = [[formatter stringFromDate:timeDate] intValue];
    formatter.dateFormat = @"mm";
    int minutes = [[formatter stringFromDate:timeDate] intValue];
    formatter.dateFormat = @"ss";
    int seconds = [[formatter stringFromDate:timeDate] intValue];
    //int timeInSeconds = seconds + minutes * 60 + hours * 3600;
    int timeInSeconds = minutes * 60 + hours * 3600;
    return timeInSeconds;
}

-(int)ChangeTimeMechanicalLabor :(NSString*)strDateToConvert{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    NSDate *timeDate = [formatter dateFromString:strDateToConvert];
    
    formatter.dateFormat = @"HH";
    int hours = [[formatter stringFromDate:timeDate] intValue];
    formatter.dateFormat = @"mm";
    int minutes = [[formatter stringFromDate:timeDate] intValue];
    int timeInSeconds = minutes * 60 + hours * 3600;
    
    return timeInSeconds;
}

-(int)ChangeTimeMechanicalInvoice :(NSString*)strDateToConvert{
    
    NSArray *arry=[strDateToConvert componentsSeparatedByString:@":"];
    
    NSString *strFirst=arry[0];
    NSString *strSecond=arry[1];
    
    int hours = [strFirst intValue];
    int minutes = [strSecond intValue];
    int timeInSeconds = minutes * 60 + hours * 3600;
    
    return timeInSeconds;
    
}

-(NSString *)ChangeTimeToHrnMin :(NSString*)strTimeToConvert{
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strTimeToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"HH:mm"];
        newTime = [dateFormatter dateFromString:strTimeToConvert];
        
    }
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strTimeToConvert;
        
    }
    return finalTime;
}
-(BOOL)isNetReachable{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        return NO;
        
    }
    else
    {
        
        return YES;
        
    }

}


-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate :(NSString*)strWorkOrderId{

    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;

    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;

    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:[self modifyDateService] forKey:@"modifiedFormatedDate"];
        [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
        
        NSString *strDateModified = [self modifyDateService];
        [matchesWorkOrder setValue:strDateModified forKey:@"modifiedFormatedDate"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",strDateModified]];
        [matchesWorkOrder setValue:newTime forKey:@"dateModified"];
        
        
        NSError *error1;
        [context1 save:&error1];

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderFromDataBaseForServiceToUpdateModifydate :(NSString*)strWorkOrderId :(NSString*)strCurrentDateToSet{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",strCurrentDateToSet]];
        [matchesWorkOrder setValue:newTime forKey:@"dateModified"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchServicePestWoToUpdateModifyDate :(NSString*)strWorkOrderId :(NSString*)strCurrentDateToSet{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",strCurrentDateToSet]];
        [matchesWorkOrder setValue:newTime forKey:@"dateModified"];
        [matchesWorkOrder setValue:@"yes" forKey:@"zSync"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderAndSaveOrUpdateData :(NSString*)strWorkOrderId :(NSString*)strKeyName :(NSString*)strValue {
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:strValue forKey:strKeyName];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchServicePestWoToUpdateModifyDateJugad :(NSString*)strWorkOrderId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        [matchesWorkOrder setValue:[self modifyDateService] forKey:@"modifyDate"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderFromDbToUpdateSurveyStatus :(NSString*)strWorkOrderId :(NSString*)strSurveyStatus{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:strSurveyStatus forKey:@"surveyStatus"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(BOOL)isMoreThenTwoMB :(NSURL*)myURL :(int )sizee{
 
    NSData *data = [NSData dataWithContentsOfURL:myURL];
    
    NSString *strDataInKb = [[NSByteCountFormatter new] stringFromByteCount:data.length];
    
    float dataInMB = [strDataInKb floatValue]/1024;
    
    if (dataInMB>sizee) {
        
        return true;
        
    } else {
     
        return false;
        
    }
    
}

-(NSString*)strDocNameFromPath :(NSString*)strPath{
 
    NSString *result;
    NSRange equalRange = [strPath rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [strPath substringFromIndex:equalRange.location + equalRange.length];
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"'\'" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"\\\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"//" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"///" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"////" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
        NSRange equalRange = [strPath rangeOfString:@"\\\\\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [strPath substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    if (result.length==0) {
       
        result = strPath;
    }
    return result;
}

-(BOOL)isSurveyCompletedService :(NSString*)strWorkOrderId{
    
    BOOL isComplete;
    isComplete=NO;
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        isComplete=NO;
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSString *strStatus=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"surveyStatus"]];
        
        if ([strStatus isEqualToString:@"Completed"] || [strStatus isEqualToString:@"Complete"]) {
            
            isComplete=YES;
            
        } else {
            
            isComplete=NO;
            
        }
        
    }
    
    return isComplete;
}

-(void)fetchLeadFromDbToUpdateSurveyStatus :(NSString*)strWorkOrderId :(NSString*)strSurveyStatus :(NSString*)strStageType{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:strSurveyStatus forKey:@"surveyStatus"];
        
        if ([strStageType isEqualToString:@"forSendProposal"])
        {
            
            [matchesWorkOrder setValue:@"Open" forKey:@"statusSysName"];
            [matchesWorkOrder setValue:@"Proposed" forKey:@"stageSysName"];
            
        }
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(BOOL)isSurveyCompletedSales :(NSString*)strWorkOrderId {
    
    BOOL isComplete;
    isComplete=NO;
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        isComplete=NO;
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSString *strStatus=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"surveyStatus"]];
        
        if ([strStatus isEqualToString:@"Completed"] || [strStatus isEqualToString:@"Complete"]) {
            
            isComplete=YES;
            
        } else {
            
            isComplete=NO;
            
        }
        
    }
    
    return isComplete;
}

-(NSManagedObject*)fetchMechanicalWorkOrderObj :(NSString*)strWorkOrderId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
    }
    
    return matchesWorkOrder;
    
}


-(NSManagedObject*)fetchServiceWorkOrderObjToFindStatus :(NSString*)strWorkOrderId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
    }
    
    return matchesWorkOrder;
    
}

-(NSManagedObject*)fetchMechanicalSubWorkOrderObj :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesSubWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    //subWorkOrderId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesSubWorkOrder=arrAllObjWorkOrder[0];
    }
    
    return matchesSubWorkOrder;
    
}

-(NSString *)strBoolValueFromServer :(NSString*)strValueComing{
    
    NSString *strReturn;
    if ([strValueComing caseInsensitiveCompare:@"true"] == NSOrderedSame || [strValueComing caseInsensitiveCompare:@"1"] == NSOrderedSame) {
        
        strReturn=@"true";
        
    } else {
        
        strReturn=@"false";
        
    }
    
    return strReturn;
    
}

-(NSString *)ChangeDateFormatOnGeneralInf :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //2018-03-05T05:29:00
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];

    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }

    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}

-(NSString *)ChangeDateFormatOnGeneralInfNew :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //2018-03-05T05:29:00
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}

-(NSString *)ChangeDateFormatOnGeneralInfoTime :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatter dateFromString:strDateToConvert];
        
    }
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=strDateToConvert;
        
    }
    return finalTime;
}


-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateClockStatus :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)status{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:status forKey:@"clockStatus"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderFromDataBaseForMechanicalToUpdateAdditionalInfo :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)strAdditionalInfo{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        [matchesWorkOrder setValue:strAdditionalInfo forKey:@"inspectionresults"];
        
        NSError *error1;
        [context1 save:&error1];

        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Additional Info Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [alert show];
        
        [self autoDismissAlerView:alert];

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(BOOL)isNumberOnly :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck{
    
    if ([string isEqualToString:@""]) {
        
        return true;
        
    } else {
        
        if (!(txtLength+(string.length - range.length) <= maxValue)) {
            
            return false;
            
        } else {
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            
            NSRange range = [string rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound) {
                // no ( or ) in the string
                return false;
            } else {
                // ( or ) are present
                return true;
            }
        }
    }
}

-(BOOL)isHowMuchLength :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength{
    
    if ([string isEqualToString:@""]) {
        
        return true;
        
    } else {
        
        if (!(txtLength+(string.length - range.length) <= maxValue)) {
            
            return false;
            
        } else {
            
            return true;

        }
    }
}

-(BOOL)isNumbernDecimalOnly :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck{
    
    if ([string isEqualToString:@""]) {
        
        return true;
        
    } else {
        
        if (!(txtLength+(string.length - range.length) <= maxValue)) {
            
            return false;
            
        } else {
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:stringTocheck];
            
            NSRange range = [string rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound) {
                // no ( or ) in the string
                return false;
            } else {
                // ( or ) are present
                return true;
            }
        }
    }
}

-(NSString *)getTaxForMechanicalService:(NSString *)strServiceUrl :(NSString *)strCompanyKeyMechanical :(NSString *)strWorkorderIdMechanical
{
    NSString *strTax;
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
        
    {
//        [DejalActivityView removeView];
//        [DejalBezelActivityView removeView];

        return @"0";
    }
    else
    {
        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&workOrderId=%@",strServiceUrl,UrlMechanicalGetTax,strCompanyKeyMechanical,strWorkorderIdMechanical];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        
        NSURL *url = [NSURL URLWithString:strUrl];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSError *error = nil;
        
        NSURLResponse * response = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
//            [DejalActivityView removeView];
//            [DejalBezelActivityView removeView];

            return @"0";
        }
        else
        {
            NSString *responseString1    = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Tax calculation value on mechanical responseString %@",responseString1);
            
            strTax=responseString1;
            
//            [DejalActivityView removeView];
//            [DejalBezelActivityView removeView];

            return responseString1;
            
        }
        
    }
}

-(NSString *)getMemberShipCHargeForMechanical:(NSString *)strServiceUrl :(NSString *)strCompanyKeyMechanical :(NSString *)strDepartment :(NSString *)strAccountNo
{
    NSString *strTax;
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
        
    {
        //        [DejalActivityView removeView];
        //        [DejalBezelActivityView removeView];
        
        return @"0";
    }
    else
    {
        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&departmentSysName=%@&accountNo=%@",strServiceUrl,UrlGetAccountPSPPercentMechanical,strCompanyKeyMechanical,strDepartment,strAccountNo];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        
        NSURL *url = [NSURL URLWithString:strUrl];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSError *error = nil;
        
        NSURLResponse * response = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            //            [DejalActivityView removeView];
            //            [DejalBezelActivityView removeView];
            
            return @"0";
        }
        else
        {
            NSString *responseString1    = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"responseString %@",responseString1);
            
            strTax=responseString1;
            
            //            [DejalActivityView removeView];
            //            [DejalBezelActivityView removeView];
            
            return responseString1;
            
        }
        
    }
}


-(BOOL)isNumberOnlyWithDecimal :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck :(NSString*)stringFullText{
    stringFullText = [stringFullText stringByReplacingCharactersInRange:range withString:string];
    
    if ([string isEqualToString:@""]) {
        
        return true;
        
    } else {
        
        if (!(txtLength+(string.length - range.length) <= maxValue)) {
            
            return false;
            
        } else {
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:stringTocheck];
            
            NSRange range = [string rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound) {
                // no ( or ) in the string
                return false;
            }
            else
            {
                
                NSArray *sep = [stringFullText componentsSeparatedByString:@"."];
                if([sep count] >= 2 )
                {
                    if ([sep count]>=3)
                    {
                        return NO;
                    }
                    //if ([string rangeOfString:@"."].location !=NSNotFound && [stringFullText rangeOfString:string].location !=NSNotFound)
                    /* if ([string rangeOfString:@"."].location !=NSNotFound )
                     {
                     
                     return NO;
                     }*/
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    NSLog(@"Length %lu",(unsigned long)[sepStr length]);
                    if ([sepStr length]>=3)
                    {
                        return NO;
                    }
                    else
                    {
                        return YES;
                    }
                    //return !([sepStr length]>=2);
                }
                return YES;
                
                
            }
        }
    }
}

-(BOOL)isNumberOnlyWithDecimalNew :(NSString*)string :(NSRange)range :(int)maxValue :(int)txtLength :(NSString*)stringTocheck :(NSString*)stringFullText{
    //stringFullText = [stringFullText stringByReplacingCharactersInRange:range withString:string];
    
    if ([string isEqualToString:@""]) {
        
        return true;
        
    } else {
        
        if (!(txtLength+(string.length - range.length) <= maxValue)) {
            
            return false;
            
        } else {
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:stringTocheck];
            
            NSRange range = [string rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound) {
                // no ( or ) in the string
                return false;
            }
            else
            {
                
                NSArray *sep = [stringFullText componentsSeparatedByString:@"."];
                if([sep count] >= 2 )
                {
                    if ([sep count]>=3)
                    {
                        return NO;
                    }
                    //if ([string rangeOfString:@"."].location !=NSNotFound && [stringFullText rangeOfString:string].location !=NSNotFound)
                    /* if ([string rangeOfString:@"."].location !=NSNotFound )
                     {
                     
                     return NO;
                     }*/
                    NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
                    NSLog(@"Length %lu",(unsigned long)[sepStr length]);
                    if ([sepStr length]>=3)
                    {
                        return NO;
                    }
                    else
                    {
                        return YES;
                    }
                    //return !([sepStr length]>=2);
                }
                return YES;
                
                
            }
        }
    }
}
-(NSString*)changeDateToFormattedDateNewEXPDATE :(NSString*)strDateBeingConverted{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;
    
}

-(void)getUnsignedDocumentsWoNosNew :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyKeyy {
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrOfWoIdss=[[NSMutableArray alloc]init];
    
    arrOfWoIdss = [defsss objectForKey:@"WorkOrderNosUnsignedList"];
    
    if (arrOfWoIdss.count>0) {
        
        NSString *strCombinedWoIdss=[arrOfWoIdss componentsJoinedByString:@","];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalGetUnsignedDocumentList,strCompanyKeyy,UrlMechanicalGetUnsignedDocumentListWoNos,strCombinedWoIdss];
        
        NSLog(@"Get Unsigned Document URl-----%@",strUrl);
        
        // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Service Appointments..."];
        
        //============================================================================
        //============================================================================
        
        NSURL *url = [NSURL URLWithString:strUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60000.0];
        [request setHTTPMethod:@"GET"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //Nilind 1 Feb
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             NSString *returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             //NSDictionary *strResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if ([returnString isEqualToString:@""]) {
                 
                 
                 
             } else {
                 
                 NSArray *arrOfUnsignedDocuments=[returnString componentsSeparatedByString:@","];
                 
                 NSLog(@"list being save===%@",arrOfUnsignedDocuments);
                 
                 NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
                 [defsss setObject:arrOfUnsignedDocuments forKey:@"ListOfUnsignedDocs"];
                 [defsss synchronize];
                 
             }
             NSLog(@"List Of Unsigned Documents===%@",returnString);
         }];
        
    }
    
}

-(void)getUnsignedDocumentsWoNos :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyKeyy{
    
    NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrOfWoIdss=[[NSMutableArray alloc]init];
    
    arrOfWoIdss = [defsss objectForKey:@"WorkOrderNosUnsignedList"];
    
    if (arrOfWoIdss.count>0) {
        
        //CompanyKey WorkOrderNos
        
        NSString *strCombinedWoIdss=[arrOfWoIdss componentsJoinedByString:@","];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlMechanicalGetUnsignedDocumentListNew];
        
        
        NSArray *objects = [NSArray arrayWithObjects:strCompanyKeyy,
                            strCombinedWoIdss,
                            nil];
        
        NSArray *keys = [NSArray arrayWithObjects:@"CompanyKey",
                         @"WorkOrderNos",
                         nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSLog(@"Get Unsigned Document URl-----%@",strUrl);
        
        // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Service Appointments..."];
        
        //============================================================================
        //============================================================================
        
        NSURL *url = [NSURL URLWithString:strUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60000.0];
        [request setHTTPMethod:@"POST"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //Nilind 1 Feb
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];
        //ENd
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSData* jsonData = [NSData dataWithData:data];
             NSString *returnString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             //NSDictionary *strResponse = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if ([returnString isEqualToString:@""]) {
                 
                 
                 
             } else {
                 
                 NSArray *arrOfUnsignedDocuments=[returnString componentsSeparatedByString:@","];
                 
                 NSLog(@"list being save===%@",arrOfUnsignedDocuments);
                 
                 NSUserDefaults *defsss=[NSUserDefaults standardUserDefaults];
                 [defsss setObject:arrOfUnsignedDocuments forKey:@"ListOfUnsignedDocs"];
                 [defsss synchronize];
                 
             }
             NSLog(@"List Of Unsigned Documents===%@",returnString);
         }];
        
    }
    
}

-(BOOL)runningInBackground
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    return state == UIApplicationStateBackground;
}

-(BOOL)runningInForeground
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    return state == UIApplicationStateActive;
}

-(BOOL)runningInInActiveState
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    return state == UIApplicationStateInactive;
}

-(BOOL)checkImage :(UIButton*)btnImgToCheck{
    
    BOOL isTrue;
    isTrue=NO;
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"uncheck.png"];
    
    UIImage *img = [btnImgToCheck imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        isTrue=YES;
        
    }
    else
    {
        isTrue=NO;
        
    }
    
    return isTrue;
    
}

-(BOOL)checkImageRadio :(UIButton*)btnImgToCheck{
    
    BOOL isTrue;
    isTrue=NO;
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"RadioButton-Unselected.png"];
    
    UIImage *img = [btnImgToCheck imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        isTrue=YES;
        
    }
    else
    {
        isTrue=NO;
        
    }
    
    return isTrue;
    
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager;
    locationManager = [[CLLocationManager alloc]init] ;
    locationManager.delegate = self;
    //[locationManager requestAlwaysAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    [locationManager stopUpdatingLocation];
    
    return coordinate;
    
}


-(NSDictionary*)getInventoryDetailsByItemNo :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyIddd :(NSString*)strItemNumber{
    
    NSDictionary *dictDataInventory;
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlGetItemInventoryDetailByItemNumber,strCompanyIddd,Urlitemnumber,strItemNumber];
    
    NSLog(@"Get InventoryDetails URl-----%@",strUrl);
    
   // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Item Details..."];
    
    //============================================================================
    //============================================================================
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60000.0];
    [request setHTTPMethod:@"GET"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    NSError *error = nil;
    NSURLResponse * response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSData* jsonData = [NSData dataWithData:data];
    dictDataInventory = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    return dictDataInventory;
}


void(^responseOnCustomerAddress)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)responseOnCustomerAddress:(NSString *)url :(NSString *)strTypeSales :(NSString *)strMode withCallback:(WebServicePostCompletionBlockCustomerAddress)callback
{
    responseOnCustomerAddress = callback;
    if ([strMode isEqualToString:@"BackGround"]) {
        
        [self sendCustomerAddressBackGround:nil withSuccess:YES error:nil withUrl:url withType:strTypeSales];

    } else {
        
        [self sendCustomerAddress:nil withSuccess:YES error:nil withUrl:url withType:strTypeSales];

    }
}

- (void)sendCustomerAddress:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString *)strAccountNo
{
    
    NSMutableArray *arrOfAddress=[[NSMutableArray alloc]init];
    NSEntityDescription *entityAddAddress;
    NSArray *arrrAllObjAddress;
    NSManagedObject *matchesAddress;
    NSData *requestData;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityAddAddress];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",strAccountNo,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerAddCustomerAddress = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAddCustomerAddress setDelegate:self];
    
    // Perform Fetch
    NSError *error111 = nil;
    [self.fetchedResultsControllerAddCustomerAddress performFetch:&error111];
    arrrAllObjAddress = [self.fetchedResultsControllerAddCustomerAddress fetchedObjects];
    if ([arrrAllObjAddress count] == 0)
    {
        
    }
    else
    {
        
        arrOfAddress=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrrAllObjAddress.count; k++) {
            
            matchesAddress=arrrAllObjAddress[k];
            
            [arrOfAddress addObjectsFromArray:[matchesAddress valueForKey:@"customerAddress"]];
            
        }
        
    }
    if (error111) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error111, error111.localizedDescription);
    } else {
    }

    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    
  //  if (!(arrOfAddress.count==0)) {
        
        [dictData setObject:arrOfAddress forKey:@"customer_addresses"];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrOfAddress])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrOfAddress options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Customer Address JSON: %@", jsonString);
            }
        }
        
        requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURL *url = [NSURL URLWithString:stringUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:200.0];
        [request setHTTPMethod:@"POST"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //Nilind 1 Feb
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        
        NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
        NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
        NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
        NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
        NSString *strEmployeeEmail =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
        NSString *strSalesProcessCompanyId;
        //    if ([Typeee isEqualToString:@"serviceOrder"]) {
        //        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
        //
        //    }else{
        //        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        //
        //    }
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
        [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
        [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
        [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
        [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
        [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];//CompanyId
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CompanyId"];
        [request addValue:strEmployeeEmail forHTTPHeaderField:@"EmployeeEmail"];
        //    if ([Typeee isEqualToString:@"serviceOrder"]) {
        //
        //        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"ServiceAutoCompanyId"];
        //
        //    }else{
        //
        //        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        //
        //    }
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];
        
        @try {
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            // ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            ResponseDict =dictData;
            
            
            NSArray *arrOfAddressListToSave;
            
            arrOfAddressListToSave=(NSArray*)ResponseDict;
            
            if ([arrOfAddressListToSave isKindOfClass:[NSArray class]]) {
                
                NSLog(@"NSArray");
                NSLog(@"Response on uploading Customer Address:----%@",ResponseDict);
                
                //First Delete Existing From Db
                
                //  Delete Student Data
                NSFetchRequest *allData = [[NSFetchRequest alloc] init];
                [allData setEntity:[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context]];
                [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
                NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@",strAccountNo];
                [allData setPredicate:predicate];
                
                NSError * error1 = nil;
                NSArray * Data = [context executeFetchRequest:allData error:&error1];
                //error handling goes here
                for (NSManagedObject * data in Data) {
                    [context deleteObject:data];
                }
                NSError *saveError = nil;
                [context save:&saveError];
                
                
                //And then save customer address to Local Db
                
                appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                context = [appDelegate managedObjectContext];
                entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
                
                CustomerAddress *objTaskList = [[CustomerAddress alloc]initWithEntity:entityAddAddress insertIntoManagedObjectContext:context];
                
                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                NSString *strUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
                NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
                NSString *strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                
                objTaskList.userName=strUserName;
                objTaskList.companyKey=strCompanyKey;
                objTaskList.customerAddress=arrOfAddressListToSave;
                objTaskList.accountNo=strAccountNo;
                NSError *error11;
                [context save:&error11];

            } else {
                
                NSLog(@"NSString");

            }
            

            
        }
        @catch (NSException *exception) {
            [self AlertMethod:Alert :Sorry];
        }
        @finally {
        }

//    }else{
//
//        ResponseDict = @"";
//
//
//    }
    
    responseOnCustomerAddress(success, ResponseDict, error);
    
}


- (void)sendCustomerAddressBackGround:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withType:(NSString *)strAccountNo
{
    
    NSMutableArray *arrOfAddress=[[NSMutableArray alloc]init];
    NSEntityDescription *entityAddAddress;
    NSArray *arrrAllObjAddress;
    NSManagedObject *matchesAddress;
    NSData *requestData;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityAddAddress];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@ AND userName = %@",strAccountNo,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerAddCustomerAddress = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAddCustomerAddress setDelegate:self];
    
    // Perform Fetch
    NSError *error111 = nil;
    [self.fetchedResultsControllerAddCustomerAddress performFetch:&error111];
    arrrAllObjAddress = [self.fetchedResultsControllerAddCustomerAddress fetchedObjects];
    if ([arrrAllObjAddress count] == 0)
    {
        
    }
    else
    {
        
        arrOfAddress=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrrAllObjAddress.count; k++) {
            
            matchesAddress=arrrAllObjAddress[k];
            
            [arrOfAddress addObjectsFromArray:[matchesAddress valueForKey:@"customerAddress"]];
            
        }
        
    }
    if (error111) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error111, error111.localizedDescription);
    } else {
    }
    
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    
  //  if (!(arrOfAddress.count==0)) {
        
        [dictData setObject:arrOfAddress forKey:@"customer_addresses"];
        
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrOfAddress])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrOfAddress options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Final Customer Address JSON: %@", jsonString);
            }
        }
        
        requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURL *url = [NSURL URLWithString:stringUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:200.0];
        [request setHTTPMethod:@"POST"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //Nilind 1 Feb
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        
        NSString *strHrmsCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.HrmsCompanyId"]];
        NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
        NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeId"]];
        NSString *strCreatedBy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"CreatedBy"]];
        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
        NSString *strEmployeeEmail =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEmail"]];
        NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
        NSString *strSalesProcessCompanyId;
        //    if ([Typeee isEqualToString:@"serviceOrder"]) {
        //        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ServiceAutoCompanyId"]];
        //
        //    }else{
        //        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        //
        //    }
        strSalesProcessCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
        NSString *strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
        [request addValue:strHrmsCompanyId forHTTPHeaderField:@"HrmsCompanyId"];
        [request addValue:[self getIPAddress] forHTTPHeaderField:@"IpAddress"];
        [request addValue:strEmployeeNumber forHTTPHeaderField:@"EmployeeNumber"];
        [request addValue:strEmployeeId forHTTPHeaderField:@"EmployeeId"];
        [request addValue:strCreatedBy forHTTPHeaderField:@"CreatedBy"];
        [request addValue:strEmployeeName forHTTPHeaderField:@"EmployeeName"];
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CoreCompanyId"];//CompanyId
        [request addValue:strCoreCompanyId forHTTPHeaderField:@"CompanyId"];
        [request addValue:strEmployeeEmail forHTTPHeaderField:@"EmployeeEmail"];
        //    if ([Typeee isEqualToString:@"serviceOrder"]) {
        //
        //        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"ServiceAutoCompanyId"];
        //
        //    }else{
        //
        //        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        //
        //    }
        [request addValue:strSalesProcessCompanyId forHTTPHeaderField:@"SalesProcessCompanyId"];
        [request addValue:strCompanyKey forHTTPHeaderField:@"CompanyKey"];
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];
        
        @try {
            
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
             {
                 NSData* jsonData = [NSData dataWithData:data];
                 ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
//                 NSString *strException;
//                 @try {
//                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
//                 } @catch (NSException *exception) {
//
//                 } @finally {
//
//                 }
//                 if (strException.length==0) {
                 
                     NSArray *arrOfAddressListToSave;
                     
                     arrOfAddressListToSave=(NSArray*)ResponseDict;
                 
                 if ([arrOfAddressListToSave isKindOfClass:[NSArray class]]) {
                     
                     NSLog(@"Response on uploading Customer Address:----%@",ResponseDict);
                     
                     //First Delete Existing From Db
                     
                     //  Delete Student Data
                     NSFetchRequest *allData = [[NSFetchRequest alloc] init];
                     [allData setEntity:[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context]];
                     [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
                     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"accountNo = %@",strAccountNo];
                     [allData setPredicate:predicate];
                     
                     NSError * error1 = nil;
                     NSArray * Data = [context executeFetchRequest:allData error:&error1];
                     //error handling goes here
                     for (NSManagedObject * data in Data) {
                         [context deleteObject:data];
                     }
                     NSError *saveError = nil;
                     [context save:&saveError];
                     
                     
                     //And then save customer address to Local Db
                     NSEntityDescription *entityAddAddress;
                     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                     context = [appDelegate managedObjectContext];
                     entityAddAddress=[NSEntityDescription entityForName:@"CustomerAddress" inManagedObjectContext:context];
                     
                     CustomerAddress *objTaskList = [[CustomerAddress alloc]initWithEntity:entityAddAddress insertIntoManagedObjectContext:context];
                     
                     NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                     NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                     NSString *strUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
                     NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
                     NSString *strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                     
                     objTaskList.userName=strUserName;
                     objTaskList.companyKey=strCompanyKey;
                     objTaskList.customerAddress=arrOfAddressListToSave;
                     objTaskList.accountNo=strAccountNo;
                     NSError *error11;
                     [context save:&error11];
                     
                 }
                 
                 
//                 }else{
//
//                     ResponseDict = @"";
//
//                 }
             }];

            
            
        }
        @catch (NSException *exception) {
            [self AlertMethod:Alert :Sorry];
        }
        @finally {
        }
        
//    }else{
//
//        ResponseDict = @"";
//
//
//    }
    
    responseOnCustomerAddress(success, ResponseDict, error);
    
}


// Get Hours Config From Master on basis of different conditions

-(NSMutableArray*)getHoursConfiFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderAccNo :(NSString*)strWorkOrderAddressId :(NSString*)strWorkOrderAddressSubType{
    
    if ([strWorkOrderAddressId isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressId=@"";
        
    }
    if ([strWorkOrderAddressSubType isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressSubType=@"";
        
    }
    if ([strWorkOrderAccNo isEqualToString:@"(null)"]) {
        
        strWorkOrderAccNo=@"";
        
    }

    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"HourConfigMasterExtSerDc"];
    
    NSMutableArray *arrOfHoursConfig=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfStatus.count; k++) {
        
        NSDictionary *dictDataa=arrOfStatus[k];
        //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
        //   if (isActive) {
        
        // Changes for fetching Via company dept account address
        
        
        NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
        NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
        NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
        
        
        if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId]) {
            
            [arrOfHoursConfig addObject:dictDataa];
            
        }
    }
    
    if (arrOfHoursConfig.count==0) {
        
        for (int k=0; k<arrOfStatus.count; k++) {
            
            NSDictionary *dictDataa=arrOfStatus[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""]) {
                
                [arrOfHoursConfig addObject:dictDataa];
                
            }
        }
        
    }
    
    if (arrOfHoursConfig.count==0) {
        
        for (int k=0; k<arrOfStatus.count; k++) {
            
            NSDictionary *dictDataa=arrOfStatus[k];
            //   BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
            //   if (isActive) {
            
            // Changes for fetching Via company dept account address
            
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            NSString *strLocalAddressSubType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AddressType"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""] && [strLocalAddressSubType isEqualToString:strWorkOrderAddressSubType]) {
                
                [arrOfHoursConfig addObject:dictDataa];
                
            }
        }
        
    }
    
    return arrOfHoursConfig;
}


//

-(NSString*)logicForFetchingMultiplier :(NSString*)strPriceToLookUp :(NSString*)strCategorySysName :(NSString*)strType :(NSMutableArray*)arrOfPriceLookup{
    
    NSString *strMultiplier;
    
    NSMutableArray *arrTempPriceLookUp=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfPriceLookup.count; k++) {
        
        NSDictionary *dictDataPrice=arrOfPriceLookup[k];
        
        NSString *strFrom=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeFrom"]];
        NSString *strTo=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"RangeTo"]];
        
        int c=[strPriceToLookUp intValue];
        
        int a=[strFrom intValue];
        int b=[strTo intValue];
        
        if(c >= a && c <= b){
            
            // strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
            
            //  break;
            
            NSString *strisStandard=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"IsStandard"]];
            
            
            if ([strType isEqualToString:@"Standard"]) {
                
                if ([strisStandard isEqualToString:@"true"] || [strisStandard isEqualToString:@"True"] || [strisStandard isEqualToString:@"1"]) {
                    
                    [arrTempPriceLookUp addObject:dictDataPrice];
                    
                    
                }
            } else {
                
                if ([strisStandard isEqualToString:@"false"] || [strisStandard isEqualToString:@"False"] || [strisStandard isEqualToString:@"0"]) {
                    
                    [arrTempPriceLookUp addObject:dictDataPrice];
                    
                    
                }
            }
            
        }
        
    }
    
    
    for (int k1=0; k1<arrTempPriceLookUp.count; k1++) {
        
        NSDictionary *dictDataPrice=arrTempPriceLookUp[k1];
        
        NSString *strCatSysnameLocal=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"CategorySysName"]];
        if ([strCatSysnameLocal isEqualToString:strCategorySysName]) {
            
            strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
            
            break;
            
        }else{
            
            //strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
            
        }
        
    }
    
    if (strMultiplier.length==0) {
        
        
        for (int k1=0; k1<arrTempPriceLookUp.count; k1++) {
            
            NSDictionary *dictDataPrice=arrTempPriceLookUp[k1];
            
            NSString *strCatSysnameLocal=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"CategorySysName"]];
            
            if ([strType isEqualToString:@"Standard"]) {
                
                if ([strCatSysnameLocal isEqualToString:@""]) {
                    strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
                    break;
                }
                
            } else {
                
                if ([strCatSysnameLocal isEqualToString:@""] && ([strCategorySysName isEqualToString:@""] || (strCategorySysName.length==0))) {
                    strMultiplier=[NSString stringWithFormat:@"%@",[dictDataPrice valueForKey:@"Multiplier"]];
                    break;
                }
                
            }
            
        }
        
    }

    if (strMultiplier.length==0) {
        
        strMultiplier=@"1";
        
    }
    
    return strMultiplier;
}


-(NSMutableArray*)getPricLookupFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderAccNo :(NSString*)strWorkOrderAddressId :(NSString*)strWorkOrderAddressSubType :(NSString*)strCategorySysName{
    
    if ([strWorkOrderAddressId isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressId=@"";
        
    }
    if ([strWorkOrderAddressSubType isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressSubType=@"";
        
    }
    if ([strWorkOrderAccNo isEqualToString:@"(null)"]) {
        
        strWorkOrderAccNo=@"";
        
    }
    if ([strCategorySysName isEqualToString:@"(null)"]) {
        
        strCategorySysName=@"";
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"LookupPricingExtSerDc"];
    
    NSMutableArray *arrOfPriceLookup=[[NSMutableArray alloc]init];
    
    
    if (strCategorySysName.length==0) {
        
        if ((strWorkOrderAccNo.length>0) && (strWorkOrderAddressId.length>0)) {
            
            for (int k=0; k<arrOfStatus.count; k++) {
                
                NSDictionary *dictDataa=arrOfStatus[k];
                //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                //        if (isActive) {
                
                NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                NSString *strLocalCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategorySysName"]];
                
                if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId] && [strLocalCategorySysName isEqualToString:@""]) {
                    
                    [arrOfPriceLookup addObject:dictDataa];
                    
                }
                
            }
            
        }
        
        if ((strWorkOrderAccNo.length>0) && (arrOfPriceLookup.count==0)) {
            
                for (int k=0; k<arrOfStatus.count; k++) {
                    
                    NSDictionary *dictDataa=arrOfStatus[k];
                    //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                    //        if (isActive) {
                    
                    NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                    NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                    NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                    NSString *strLocalCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategorySysName"]];
                    
                    if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""] && [strLocalCategorySysName isEqualToString:@""]) {
                        
                        [arrOfPriceLookup addObject:dictDataa];
                        
                    }
                    
                }
            
        }
        
        if (arrOfPriceLookup.count==0) {
            
            for (int k=0; k<arrOfStatus.count; k++) {
                
                NSDictionary *dictDataa=arrOfStatus[k];
                //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                //        if (isActive) {
                
                NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                NSString *strLocalCategorySysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategorySysName"]];

                if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""] && [strLocalCategorySysName isEqualToString:@""]) {
                    
                    [arrOfPriceLookup addObject:dictDataa];
                    
                }
                
            }
            
        }
        
    } else {
        
        
        if ((strWorkOrderAccNo.length>0) && (strWorkOrderAddressId.length>0)) {
            
            for (int k=0; k<arrOfStatus.count; k++) {
                
                NSDictionary *dictDataa=arrOfStatus[k];
                //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                //        if (isActive) {
                
                NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                
                if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:strWorkOrderAddressId]) {
                    
                    [arrOfPriceLookup addObject:dictDataa];
                    
                }
                
            }
            
        }
        
        if ((strWorkOrderAccNo.length>0) && (arrOfPriceLookup.count==0)) {
            
            for (int k=0; k<arrOfStatus.count; k++) {
                
                NSDictionary *dictDataa=arrOfStatus[k];
                //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                //        if (isActive) {
                
                NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                
                if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:strWorkOrderAccNo] && [strLocalAddressId isEqualToString:@""]) {
                    
                    [arrOfPriceLookup addObject:dictDataa];
                    
                }
                
            }
            
        }
        
        if (arrOfPriceLookup.count==0) {
            
            for (int k=0; k<arrOfStatus.count; k++) {
                
                NSDictionary *dictDataa=arrOfStatus[k];
                //        BOOL isActive=[[dictDataa valueForKey:@"IsActive"] boolValue];
                //        if (isActive) {
                
                NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
                NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
                NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
                
                if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""]) {
                    
                    [arrOfPriceLookup addObject:dictDataa];
                    
                }
                
            }
            
        }
        
    }
    
    return arrOfPriceLookup;
    
}


-(NSMutableArray*)getPartsMaster :(NSString*)strDepartMentSysName :(NSString*)strCategoryMasterId{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictDetailsMastersMechanical=[defsLead valueForKey:@"MasterServiceAutomation"];
    
    NSArray *arrOfAreaMaster=[dictDetailsMastersMechanical valueForKey:@"InventoryItems"];
    NSMutableArray *arrOfPartsGlobalMasters=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfAreaMaster.count; k++) {
        
        NSDictionary *dictDataa=arrOfAreaMaster[k];
        
        NSString *strDepartmentSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
        NSString *strIsActive=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"IsActive"]];
        NSString *strCategoryMasterIdToCheck=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CategoryId"]];

        if ((([strIsActive isEqualToString:@"true"] || [strIsActive isEqualToString:@"1"]) && [strDepartMentSysName isEqualToString:strDepartmentSysName])) {
            
            if (strCategoryMasterId.length==0) {
                
                [arrOfPartsGlobalMasters addObject:dictDataa];
                
                
            } else {
                
                if ([strCategoryMasterId isEqualToString:strCategoryMasterIdToCheck]) {
                    
                    [arrOfPartsGlobalMasters addObject:dictDataa];
                    
                } else {
                    
                    
                    
                }
                
            }

            
        }
    }
    
    return arrOfPartsGlobalMasters;
    
}

-(float)fetchPartDetailViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters{
    
    NSString *strLookupPrice;
    
    for (int k=0; k<arrOfPartsGlobalMasters.count; k++) {
        
        NSDictionary *dictData=arrOfPartsGlobalMasters[k];
        
        NSString *strPartCodeComing=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([strPartCodeComing isEqualToString:strPartCode]) {
            
            strLookupPrice=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"BestPrice"]];
            
            break;
            
        }
    }
    
    float lookUpPrice=[strLookupPrice floatValue];
    
    return lookUpPrice;
    
}

-(NSString*)fetchPartCategorySysNameDetailViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters{
    
    NSString *strCategorySysNameLocal;
    
    for (int k=0; k<arrOfPartsGlobalMasters.count; k++) {
        
        NSDictionary *dictData=arrOfPartsGlobalMasters[k];
        
        NSString *strPartCodeComing=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([strPartCodeComing isEqualToString:strPartCode]) {
            
            strCategorySysNameLocal=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"CategorySysName"]];
            
            break;
            
        }
    }
    
    return strCategorySysNameLocal;
    
}

-(NSDictionary*)fetchPartDetailDictionaryViaPartCode :(NSString*)strPartCode :(NSMutableArray*)arrOfPartsGlobalMasters{
    
    NSDictionary *dictDataPartss;
    
    for (int k=0; k<arrOfPartsGlobalMasters.count; k++) {
        
        NSDictionary *dictData=arrOfPartsGlobalMasters[k];
        
        NSString *strPartCodeComing=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"SysName"]];
        
        if ([strPartCodeComing isEqualToString:strPartCode]) {
            
            dictDataPartss=dictData;
            
            break;
            
        }
    }
    
    return dictDataPartss;
    
}


-(NSMutableArray*)getSalesTaxFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderPropertyType{
    
    if ([strWorkOrderPropertyType isEqualToString:@"(null)"]) {
        
        strWorkOrderPropertyType=@"";
        
    }
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"SalesTaxConfigExtSerDc"];
    
    NSMutableArray *arrOfPriceLookup=[[NSMutableArray alloc]init];

    if ([arrOfStatus isKindOfClass:[NSArray class]]) {
        
        for (int k=0; k<arrOfStatus.count; k++) {
            
            NSDictionary *dictDataa=arrOfStatus[k];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalWorkOrderPropertyType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"PropertyType"]];
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            
            if ([strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalWorkOrderPropertyType isEqualToString:strWorkOrderPropertyType] && [strLocalCompanyKey isEqualToString:strCompanyKey]) {
                
                [arrOfPriceLookup addObject:dictDataa];
                
            }
            
        }
        
    }
    
    return arrOfPriceLookup;
    
}

//===================================================================================================
//===================================================================================================

#pragma mark - ================= New Calculation For TM TotalLabor Hours Run Time ===================

//===================================================================================================
//===================================================================================================


-(float)totalPriceCalcualtionForLabor :(NSString*)strFromDate :(NSString*)strToDate :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder{
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    //---->>>>
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *ToDateToCheck = [[NSDate alloc] init];
    ToDateToCheck = [self ChangeDateToLocalDateMechanicalTimeOut:strToDate];
    
    NSDate *FromDateToCheck = [[NSDate alloc] init];
    FromDateToCheck = [self ChangeDateToLocalDateMechanicalTimeOut:strFromDate];
    
    float TotalPrice = 0.0;////
    
    if (!(arrOfHoursConfig.count==0)) {
        

        BOOL isSameDate=[self isSameDay:ToDateToCheck otherDay:FromDateToCheck];
        
        if([[self getOnlyDate:ToDateToCheck] compare:[self getOnlyDate:FromDateToCheck]] == NSOrderedSame) {
            
            isSameDate=YES;
            
        }else{
            
            isSameDate=NO;
            
        }

        if (isSameDate)
        {
            
            // To check If Holiday Date
            
            BOOL isHolidayDate=[self isHoliDayDate:FromDateToCheck];
            
            if (isHolidayDate) {
                
                // Total Price Logic To fetch From Master
                
                NSTimeInterval secondsBetween = [ToDateToCheck timeIntervalSinceDate:FromDateToCheck];
                
                int hrs = secondsBetween;
                
                int seconds = hrs % 60;
                int minutes = (hrs / 60) % 60;
                int hours = hrs / 3600;
                NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                float totalTime=0.0;
                actualHrsInSec=hours*3600;
                actualMinInSec=minutes*60;
                actualMinInSecSec=seconds*60;
                totalTime=actualHrsInSec+actualMinInSec+seconds;
                totalTime=totalTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder)
                    {
                        
                        strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                        
                    }

                }
                
                TotalPrice = totalTime*strStdPrice;
                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                
            } else {
                
                // Fetching After Hour Rates
                
                NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                
                NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                
                //---->>>> Yaha Par After Hour Rates Ko Order By Karna Hai Ascending Mai(chote se bada mtlb----8,9,10,11,12 etc etc....)
                
                if (!(arrOfAfterHourRates.count==0)) {
                    
                    NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[0];
                    
                    NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                    
                    NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                    
                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                    
                    if ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedAscending) {
                        
                        //Setting StartDate and To date Coming From Db to New Object
                        
                        NSDate *NewStartTime=timeFromDBTimeIn;
                        NSDate *NewEndTime=timeToDBTimeOut;
                        
                        //Ascending mtlb first wala bada descending mtlb second wala bada
                        if (([fromTimeAfterHoursRateMaster compare: timeFromDBTimeIn] == NSOrderedDescending) && ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedAscending)) {
                            
                            NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0;
                            
                            if (!(arrOfHoursConfig.count==0)) {

                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];

                            }
                            
                            TotalPrice = totalTime*strStdPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                            NewStartTime=fromTimeAfterHoursRateMaster;
                            NewEndTime=timeToDBTimeOut;
                            
                        }else if (([fromTimeAfterHoursRateMaster compare: timeFromDBTimeIn] == NSOrderedAscending) || ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedSame)){
                            
                            NewStartTime=timeFromDBTimeIn;
                            
                        }
                        
                        // For Loop on After Hours Rate
                        
                        for (int k=0; k<arrOfAfterHourRates.count; k++) {
                            
                            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k];
                            
                            NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                            
                            NSString *strToTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                            
                            NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                            
                            NSDate *toTimeAfterHoursRateMaster =[self changeStringTimeToDate:strToTime];// Master se aaya hua from time
                            
                            if (([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedAscending) && ([toTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending)) {
                                
                                if ([fromTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending) {
                                    
                                    NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:NewStartTime];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                    
                                    TotalPrice = totalTime*strStdPrice+TotalPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                    NewStartTime=fromTimeAfterHoursRateMaster;
                                    
                                    
                                }
                                
                                NSTimeInterval diff = [toTimeAfterHoursRateMaster timeIntervalSinceDate:NewStartTime];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                
                                TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                NewStartTime=toTimeAfterHoursRateMaster;
                                
                            }else if (([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedDescending) || ([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedSame)){
                                
                                NSTimeInterval diff = [NewEndTime timeIntervalSinceDate:NewStartTime];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                
                                TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                NewStartTime=toTimeAfterHoursRateMaster;
                                
                                break;
                                
                            }
                        }
                        //
                        if ([NewEndTime compare: NewStartTime] == NSOrderedDescending) {
                            
                            
                            NSTimeInterval diff = [NewEndTime timeIntervalSinceDate:NewStartTime];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0;
                            
                            if (!(arrOfHoursConfig.count==0)) {

                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                
                            }
                            
                            TotalPrice = totalTime*strStdPrice+TotalPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                        }
                        
                    }else{
                        
                        NSTimeInterval diff = [timeToDBTimeOut timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                        
                        int hrs = diff;
                        int seconds = hrs % 60;
                        int minutes = (hrs / 60) % 60;
                        int hours = hrs / 3600;
                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                        float totalTime=0.0;
                        actualHrsInSec=hours*3600;
                        actualMinInSec=minutes*60;
                        actualMinInSecSec=seconds*60;
                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                        totalTime=totalTime/3600;
                        
                        float strStdPrice=0.0;
                        
                        if (!(arrOfHoursConfig.count==0)) {
                            
                            NSDictionary *dictDataHours=arrOfHoursConfig[0];
                            
                            strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                            
                        }

                        TotalPrice = totalTime*strStdPrice+TotalPrice;
                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                    }
                }else{
                    // if after hour not exist
//                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
//
//                    NSDate *timeToDBTimeOut =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    //Change on 27 August as blank dynamic employee sheet was coming on invoice due to minus
                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                    
                    NSTimeInterval diff = [timeToDBTimeOut timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                    
                    int hrs = diff;
                    int seconds = hrs % 60;
                    int minutes = (hrs / 60) % 60;
                    int hours = hrs / 3600;
                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                    float totalTime=0.0;
                    actualHrsInSec=hours*3600;
                    actualMinInSec=minutes*60;
                    actualMinInSecSec=seconds*60;
                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                    totalTime=totalTime/3600;
                    
                    float strStdPrice=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                        
                    }

                    TotalPrice = totalTime*strStdPrice+TotalPrice;
                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                }
            }
        }else{
            
            NSDate *StartDate = FromDateToCheck;
            
            while (([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedAscending) || ([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedSame)) {
                
                NSString *strEndTimeOfDay=[self getStartTimeofDay:StartDate];
                
                strEndTimeOfDay=[strEndTimeOfDay stringByAppendingString:@" 23:59:59"];
                
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSDate* newTime = [dateFormatter dateFromString:strEndTimeOfDay];
                NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
                [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
                //---->>>>
                NSDate *EndDate=[[NSDate alloc]init];
                
                EndDate = [dateFormatterNew dateFromString:finalTime];
                
                if ([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedSame) {
                    
                    EndDate=ToDateToCheck;
                    
                }
                    //Calculating Price
                    
                    // To check If Holiday Date
                    
                    BOOL isHolidayDate=[self isHoliDayDate:StartDate];
                    
                    if (isHolidayDate) {
                        
                        // Total Price Logic To fetch From Master
                        
                      //  NSTimeInterval secondsBetween = [StartDate timeIntervalSinceDate:EndDate];
                        NSTimeInterval secondsBetween = [EndDate timeIntervalSinceDate:StartDate];

                        int hrs = secondsBetween;
                        
                        int seconds = hrs % 60;
                        int minutes = (hrs / 60) % 60;
                        int hours = hrs / 3600;
                        NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                        float totalTime=0.0;
                        actualHrsInSec=hours*3600;
                        actualMinInSec=minutes*60;
                        actualMinInSecSec=seconds*60;
                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                        totalTime=totalTime/3600;
                        
                        float strStdPrice=0.0;
                        
                        if (!(arrOfHoursConfig.count==0)) {

                            NSDictionary *dictDataHours=arrOfHoursConfig[0];
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                            
                            if (!isStandardSubWorkOrder)
                            {
                                
                                strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                                
                            }
                            
                        }
                        
                        TotalPrice = totalTime*strStdPrice+TotalPrice;
                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                    }else{
                        
                        // Fetching After Hour Rates
                        
                        NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                        
                        NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                        
                        //---->>>> Yaha Par After Hour Rates Ko Order By Karna Hai Ascending Mai(chote se bada mtlb----8,9,10,11,12 etc etc....)
                        
                        if (!(arrOfAfterHourRates.count==0)) {
                            
                            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[0];
                            
                            NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                            
                            NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                            
                            NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                            
                            //NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                            
                            if ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:EndDate]] == NSOrderedAscending) {
                                
                                //Setting StartDate and To date Coming From Db to New Object
                                
                                NSDate *NewStartTime=timeFromDBTimeIn;
                                NSDate *NewEndTime=[self getOnlyTime:EndDate];
                                
                                if (([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedDescending) && ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:EndDate]] == NSOrderedAscending)) {
                                    
                                    NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=0.0;
                                    
                                    if (!(arrOfHoursConfig.count==0)) {

                                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                        
                                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                        
                                    }
                                    
                                    TotalPrice = totalTime*strStdPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                    NewStartTime=fromTimeAfterHoursRateMaster;
                                    NewEndTime=[self getOnlyTime:EndDate];
                                    
                                }else if (([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedAscending) || ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedSame)){
                                    
                                    NewStartTime=[self getOnlyTime:StartDate];
                                    
                                }
                                
                                // For Loop on After Hours Rate
                                
                                for (int k=0; k<arrOfAfterHourRates.count; k++) {
                                    
                                    NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k];
                                    
                                    NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                                    
                                    NSString *strToTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    
                                    NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                                    
                                    NSDate *toTimeAfterHoursRateMaster =[self changeStringTimeToDate:strToTime];// Master se aaya hua from time
                                    
                                    if (([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedAscending) && ([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewStartTime]] == NSOrderedDescending)) {
                                        
                                        if ([fromTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending) {
                                            
                                            NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                            
                                            int hrs = diff;
                                            int seconds = hrs % 60;
                                            int minutes = (hrs / 60) % 60;
                                            int hours = hrs / 3600;
                                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                            float totalTime=0.0;
                                            actualHrsInSec=hours*3600;
                                            actualMinInSec=minutes*60;
                                            actualMinInSecSec=seconds*60;
                                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                                            totalTime=totalTime/3600;
                                            
                                            float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                            
                                            TotalPrice = totalTime*strStdPrice+TotalPrice;
                                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                            NewStartTime=fromTimeAfterHoursRateMaster;
                                            
                                        }
                                        
                                        NSTimeInterval diff = [toTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                        
                                        int hrs = diff;
                                        int seconds = hrs % 60;
                                        int minutes = (hrs / 60) % 60;
                                        int hours = hrs / 3600;
                                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                        float totalTime=0.0;
                                        actualHrsInSec=hours*3600;
                                        actualMinInSec=minutes*60;
                                        actualMinInSecSec=seconds*60;
                                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                                        totalTime=totalTime/3600;
                                        
                                        float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                        
                                        TotalPrice = totalTime*strStdPrice+TotalPrice;
                                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                        NewStartTime=toTimeAfterHoursRateMaster;
                                        
                                    }else if (([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedDescending) || ([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedSame)){
                                        
                                        NSTimeInterval diff = [[self getOnlyTime:NewEndTime] timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                        
                                        int hrs = diff;
                                        int seconds = hrs % 60;
                                        int minutes = (hrs / 60) % 60;
                                        int hours = hrs / 3600;
                                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                        float totalTime=0.0;
                                        actualHrsInSec=hours*3600;
                                        actualMinInSec=minutes*60;
                                        actualMinInSecSec=seconds*60;
                                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                                        totalTime=totalTime/3600;
                                        
                                        float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                        
                                        TotalPrice = totalTime*strStdPrice+TotalPrice;
                                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                        NewStartTime=toTimeAfterHoursRateMaster;
                                        
                                        break;
                                        
                                    }
                                }
                                //
                                if ([[self getOnlyTime:NewEndTime] compare: [self getOnlyTime:NewStartTime]] == NSOrderedDescending) {
                                    
                                    
                                    NSTimeInterval diff = [[self getOnlyTime:NewEndTime] timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=0.0;
                                    
                                    if (!(arrOfHoursConfig.count==0)) {

                                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                        
                                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                        
                                    }
                                    
                                    TotalPrice = totalTime*strStdPrice+TotalPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                                }
                                
                            }else{
                                
                                NSTimeInterval diff = [[self getOnlyTime:EndDate] timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=0.0;
                                
                                if (!(arrOfHoursConfig.count==0)) {

                                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                    
                                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                    
                                }
                                
                                TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                            }
                        }else{
                            
                            // if after hour not exist
                           // NSDate *timeFromDBTimeIn =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                            
                            //NSDate *timeToDBTimeOut =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                            
                            NSTimeInterval diff = [[self getOnlyTime:EndDate] timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0;
                            
                            if (!(arrOfHoursConfig.count==0)) {

                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                
                            }
                            
                            TotalPrice = totalTime*strStdPrice+TotalPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);

                            
                        }
                        
                    }
                    
               // }
                
                int daysToAdd = 1;
                StartDate = [StartDate dateByAddingTimeInterval:daysToAdd*24*60*60];
                
                NSString *strStartTimeOfDay1=[self ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTimeFromDate:StartDate];
                
                strStartTimeOfDay1=[strStartTimeOfDay1 stringByAppendingString:@" 00:00:00"];
                
                NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSDate* newTime1 = [dateFormatter1 dateFromString:strStartTimeOfDay1];
                NSDateFormatter* dateFormatterNew1 = [[NSDateFormatter alloc] init];
                [dateFormatterNew1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatterNew1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSString* finalTime1 = [dateFormatterNew1 stringFromDate:newTime1];
                //---->>>>
                StartDate=[[NSDate alloc]init];
                
                StartDate = [dateFormatterNew dateFromString:finalTime1];
                
                //---->>>>
                StartDate=StartDate;

            }
            
        }//End Different Date
    }
    
    return TotalPrice;
    
}

-(BOOL)isHoliDayDate :(NSDate*)dateToCheck{
    
    BOOL isHolidayy;
    
    isHolidayy=NO;
    
    NSUserDefaults *defrs=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictData=[defrs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfHolidayDate=[dictData valueForKey:@"CompanyHolidaysMasterExtSerDc"];
    
    for(int i=0;i<arrOfHolidayDate.count;i++)
    {
        
        NSDictionary *dictHolidayData=arrOfHolidayDate[i];
        
        NSString *strHolidayDateInMaster=[NSString stringWithFormat:@"%@",[dictHolidayData valueForKey:@"HolidayDate"]];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//2017-12-28T00:00:00
        NSDate* newTime = [dateFormatter dateFromString:strHolidayDateInMaster];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString* finalTime = [dateFormatter stringFromDate:newTime];
        NSDate* holidayedateInMaster = [dateFormatter dateFromString:finalTime];
        
        BOOL isSameDate=[self isSameDay:holidayedateInMaster otherDay:dateToCheck];
        
        if (isSameDate) {
            
            isHolidayy=YES;
            
            break;
            
        }else{
            
            isHolidayy=NO;
            
        }
        
    }
    
    return isHolidayy;
    
}


//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

//============================================================================
//============================================================================
#pragma mark- ------------CLOCK STATUS METHODS--------------
//============================================================================
//============================================================================

-(NSString*)fetchClockInOutDetailCoreData
{
    // visibility and Inspector alag se save krna he
    NSString *strEmployeeId,*strWorkingStatusNew;
    strWorkingStatusNew=@"";
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeId    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    //**** For Lead Detail *****//
    appDelegateClock = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    contextClock = [appDelegateClock managedObjectContext];
    
    NSFetchRequest *requestClock = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:contextClock];
    [requestClock setEntity:entityClockInOutDetail];
    
    NSPredicate *predicateClock =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];
    [requestClock setPredicate:predicateClock];
    
    
    sortDescriptorClock = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptorsClock = [NSArray arrayWithObject:sortDescriptorClock];
    
    [requestClock setSortDescriptors:sortDescriptorsClock];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestClock managedObjectContext:contextClock sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObjClock = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    if (arrAllObjClock.count==0)
    {
        
    }
    else
    {
        
        matchesClock=[arrAllObjClock lastObject];
        
        strWorkingStatusNew=[NSString stringWithFormat:@"%@",[matchesClock valueForKey:@"workingStatus"]];
    }
    return strWorkingStatusNew;
}
-(NSString*)getCurrentTimerOfClock
{
    NSString *strClockUrl,*strEmployeeId,*strCurrentWorkingStatus;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeId    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strClockUrl= [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"]];
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [self AlertMethod:@"ALert" :ErrorInternetMsg];
        //[DejalActivityView removeView];
    }
    else
    {
        
        @try {
            
            NSString *strUrl;
            strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeClockInOut/EmployeeClockInOutForDisplayExt"];
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"EmployeeId",
                 nil];
            value=[NSArray arrayWithObjects:
                   strEmployeeId,
                   nil];
            
            NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
            
            NSError *errorr;
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
            
            NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"JSON FOR CURRENT TIMER CLOCK %@",jsonString1);
            
            NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
            
            NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
            
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            //Nilind 1 Feb
            [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
            [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
            //ENd
            
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody: requestData];
            
            NSError *error = nil;
            NSURLResponse * response = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSData* jsonData = [NSData dataWithData:data];
            
            strCurrentWorkingStatus=@"";
            if (jsonData==nil)
            {
            }
            else
            {
                
                // NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                
                NSError* error;
                NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                           options:kNilOptions
                                                                             error:&error];
                
                if ([dictionary isKindOfClass:[NSDictionary class]]) {
                    
                    
                    
                } else {
                    
                    if (dictionary.count==0 || dictionary==nil)
                    {
                        
                        
                    }
                    else
                    {
                        NSLog(@"CURRENT TIMER Clock Reponse %@",dictionary);
                        strCurrentWorkingStatus=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
                        
                        NSString *strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];
                        if (strTimeOut.length>10)
                        {
                            strCurrentWorkingStatus=@"EndDay";
                        }
                    }
                    
                }
                
            }
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
        
        //[DejalActivityView removeView];
    }
    return strCurrentWorkingStatus;
}


-(void)fetchSubWorkOrderFromDBToUpdateStatus :(NSString*)strWorkOrderId :(NSString*)strType{
    
    BOOL isCompleted=YES;
    
    NSEntityDescription *entityMechanicalSubWorkOrder;
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSManagedObject *matchesMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        isCompleted=YES;
        
    }
    else
    {
        
        NSMutableArray *arrOfSubWorkOrder=nil;
        arrOfSubWorkOrder=[[NSMutableArray alloc]init];
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            NSString *strStatusSubWorkOrder=[NSString stringWithFormat:@"%@",[matchesMechanicalSubWorkOrder valueForKey:@"subWOStatus"]];
            
            if ([strStatusSubWorkOrder isEqualToString:@"Completed"]) {
                
                
            }else if([strStatusSubWorkOrder isEqualToString:@"CompletePending"]){
                
                strType=@"CompletePending";
                
            }
            else{
                
                isCompleted=NO;
                
            }
        }
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
    
    if (isCompleted) {
        
        if ([strType isEqualToString:@"CompletePending"]) {
            
            [self fetchWorkOrderFromDataBaseForMechanical:strWorkOrderId :@"CompletePending"];

        } else {

            [self fetchWorkOrderFromDataBaseForMechanical:strWorkOrderId :@"Completed"];

        }
        
    } else {
        
        [self fetchWorkOrderFromDataBaseForMechanical:strWorkOrderId :@"Incomplete"];
        
    }
    
}


-(void)fetchWorkOrderFromDataBaseForMechanical :(NSString*)strWorkOrderId :(NSString*)status{
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;

    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        [matchesWorkOrder setValue:status forKey:@"workorderStatus"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(NSString*)strEmpBranchID{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmpBranchID =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchId"]];
    return strEmpBranchID;
    
}

-(NSString*)strEmpBranchSysName{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmpBranchID =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchSysName"]];
    
    NSLog(@"Employee Branch SysName is = = = =  = = =  %@",strEmpBranchID);
    
    return strEmpBranchID;
    
}
-(NSString*)strEmpLoginId{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmpBranchID =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    
    NSLog(@"Employee Branch SysName is = = = =  = = =  %@",strEmpBranchID);
    
    return strEmpBranchID;
    
}
-(NSString*)strIsCorporateUser{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strIsCorporateUser =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"IsCorporateUser"]];
    
    if ([strIsCorporateUser isEqualToString:@"1"] || [strIsCorporateUser isEqualToString:@"true"] ||[strIsCorporateUser isEqualToString:@"True"]) {
        
        strIsCorporateUser=@"true";
        
    } else {
        
        strIsCorporateUser=@"false";
        
    }
    strIsCorporateUser=@"False";
    return strIsCorporateUser;
    
}
-(NSString*)strClientTimeZone{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strClientTimeZone =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"ClientTimeZone"]];
    return strClientTimeZone;
    
}
-(void)calling :(NSString*)strCellNo
{
    
    NSString *newString = [[strCellNo componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    
    NSString *strCellNoToCall=[NSString stringWithFormat:@"tel:%@",newString];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strCellNoToCall]];
    
}

-(void)emailComposer :(NSString*)stEmailIDs :(NSString*)strSubject  :(NSString*)strBody :(UIViewController*)controller{
    
    if (strSubject.length==0) {
        
        strSubject=@"";
        
    }if (strBody.length==0) {
        
        strBody=@"";
        
    }
    
    NSArray *arrOfemails=[stEmailIDs componentsSeparatedByString:@","];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:strSubject];
        [mail setMessageBody:strBody isHTML:NO];
        [mail setToRecipients:arrOfemails];
        [controller presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        [self AlertMethod:@"Alert" :@"Please configure your email-id in device to send email" ];
    }
}



- (void) mailComposeController:(MFMailComposeViewController *)controller    didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(void)redirectOnAppleMap :(UIViewController*)viewToShowOn :(NSString*) addressStrTo{
    
    if ([addressStrTo isEqualToString:@"N/A"] || [addressStrTo isEqualToString:@""] || (addressStrTo.length==0)) {
        
        [self AlertMethod:Alert :@"No address available"];
        
    } else {
        
        addressStrTo=[addressStrTo stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        // Latitude changes
        CLLocationCoordinate2D coordinate = [self getLocation];
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        
        if (latitude.length==0) {
            
            latitude = @"29.548760";  // Quacito LLC address
            
        }
        
        if (longitude.length==0) {
            
            longitude = @"-98.495670"; // Quacito LLC address
            
        }
        
        NSString *url;
        
        url=[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@",latitude,longitude,addressStrTo];
        
        NSURL *URL = [NSURL URLWithString:url];
        
        if (![[UIApplication sharedApplication] canOpenURL:URL]) {
            
            url=[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@",latitude,longitude,addressStrTo];
            
            NSURL *URL = [NSURL URLWithString:url];
            
            if (URL) {
                if ([SFSafariViewController class] != nil) {
                    SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
                    sfvc.delegate = (id)self;
                    [viewToShowOn presentViewController:sfvc animated:YES completion:nil];
                } else {
                    if (![[UIApplication sharedApplication] openURL:URL]) {
                    }
                }
            } else {
                // will have a nice alert displaying soon.
            }
        }else{
            
            [[UIApplication sharedApplication] openURL:URL];
            
        }
        
    }

}

-(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    //key=AIzaBcDeFgHiJkLmNoPqRsTuVwXyZ
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    
    if (latitude != 0 && longitude!= 0) {
        NSString *nativeMapScheme = @"maps.apple.com";
        NSString* url = [NSString stringWithFormat:@"http://%@/maps?q=%f,%f", nativeMapScheme, latitude,longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }else{
        
        [self AlertMethod:Alert :@"Unable to locate location \n1. Please check your network connection. \n2. Please check if your address is correct or not. \n3. Enable location in settings."];
        
    }
    return center;
}


-(void)playAudio :(NSString*)path :(UIView*)viewToPlayAudio{

    /*NSURL *url=[NSURL fileURLWithPath:path];
    movieplayer=[[MPMoviePlayerController alloc] initWithContentURL:url];
    movieplayer.controlStyle = MPMovieControlStyleDefault;
    movieplayer.shouldAutoplay = YES;
    [viewToPlayAudio addSubview:movieplayer.view];
    [movieplayer setFullscreen:YES animated:YES];*/

    moviePlayer1 = [[AVPlayerViewController alloc] init];
    moviePlayer1.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:path]];
    moviePlayer1.entersFullScreenWhenPlaybackBegins = true;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [viewToPlayAudio.window.rootViewController presentViewController:moviePlayer1 animated:YES completion:^{
          [moviePlayer1.player play];
    }];
    [moviePlayer1.player play];

    
}

#pragma marks Avaudio player

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *) player successfully:(BOOL)flag
{
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Finished Playing" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    
    
}

-(NSString*)strNameBackSlashIssue :(NSString*)strAudioName{
    
    NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
    }
    
    NSRange equalRange1 = [strAudioName rangeOfString:@"\\\\" options:NSBackwardsSearch];
    if (equalRange1.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange1.location + equalRange1.length];
    }
    
    NSRange equalRange2 = [strAudioName rangeOfString:@"//" options:NSBackwardsSearch];
    if (equalRange2.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange2.location + equalRange2.length];
    }
    
    NSRange equalRange3 = [strAudioName rangeOfString:@"////" options:NSBackwardsSearch];
    if (equalRange3.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange3.location + equalRange3.length];
    }
    
    NSRange equalRange4 = [strAudioName rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange4.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange4.location + equalRange4.length];
    }
    
    NSRange equalRange5 = [strAudioName rangeOfString:@"///" options:NSBackwardsSearch];
    if (equalRange5.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange5.location + equalRange5.length];
    }
    
    return strAudioName;

}

-(NSAttributedString*)getUnderLineAttributedString:(NSString*)strText
{
    
    if(![strText isKindOfClass:[NSString class]])
    {
        strText=@"";
    }
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:strText];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    
    return attributeString;
    
}

-(NSMutableAttributedString*)removeUnderLineAttributedString:(UIButton*)strText
{
    NSMutableAttributedString *attrStr = [[strText attributedTitleForState:UIControlStateNormal] mutableCopy];//or whatever the state you want
    [attrStr enumerateAttributesInRange:NSMakeRange(0, [attrStr length])
                                options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
                             usingBlock:^(NSDictionary *attributes, NSRange range, BOOL *stop)
    {
        NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
        [mutableAttributes removeObjectForKey:NSUnderlineStyleAttributeName];
        [attrStr setAttributes:mutableAttributes range:range];
    }];
    return attrStr;
}


-(BOOL)fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted :(NSString*)strEmployeeNo :(NSString*)strSubWorkOrderId{
    
    NSUserDefaults *defsRunning =[NSUserDefaults standardUserDefaults];
    [defsRunning setObject:@"" forKey:@"RunningSubWorkOrder"];
    [defsRunning setValue:@"" forKey:@"RunningSubWorkOrderNo"];
    [defsRunning synchronize];
    
    BOOL isStarted;
    isStarted=NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    NSEntityDescription *entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    NSFetchRequest *requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo = %@",strEmployeeNo];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"employeeNo" ascending:NO];
    NSArray *sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        isStarted=NO;
        
    }
    else
    {
        
        NSMutableArray *arrOfRunningSubWorkOrderNo = [[NSMutableArray alloc]init];
        NSString *strSubWorkOderNoRunning;
        strSubWorkOderNoRunning=@"";
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            NSManagedObject *matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWoStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOStatus"]];//subWorkOrderId   employeeNo
            NSString *subWorkOrderId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderId"]];//subWorkOrderId   employeeNo
            NSString *strSubWoClockStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"clockStatus"]];//subWorkOrderId   employeeNo

            if ([strSubWoStatus isEqualToString:@"Completed"] || [strSubWoStatus isEqualToString:@"CompletePending"] || [strSubWoStatus isEqualToString:@"New"] || [strSubWoStatus isEqualToString:@"Not Started"] || [strSubWoStatus isEqualToString:@"Dispatch"] || [subWorkOrderId isEqualToString:strSubWorkOrderId] ) {
                
                
                
            }else{
                
                if ([strSubWoClockStatus caseInsensitiveCompare:@"Start"] == NSOrderedSame) {
                    
                    isStarted=YES;
                    
                    strSubWorkOderNoRunning =[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderNo"]];
                    NSString *strWorkOderNoRunning =[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"workOrderNo"]];

                    //NSString *strCombinedStringToShow =[NSString stringWithFormat:@"WO #: %@, Sub WO #: %@",strWorkOderNoRunning,strSubWorkOderNoRunning];
                    
                    NSString *strCombinedStringToShow =[NSString stringWithFormat:@"%@",strWorkOderNoRunning];

                    if ([arrOfRunningSubWorkOrderNo containsObject:strCombinedStringToShow]) {
                        
                        
                        
                    } else {
                        
                        [arrOfRunningSubWorkOrderNo addObject:strCombinedStringToShow];
                        
                    }
                    
                    
                    //break;

                } else {
                    
                    
                }
            }
        }
        
        if (arrOfRunningSubWorkOrderNo.count>0) {
            
            NSUserDefaults *defsRunning =[NSUserDefaults standardUserDefaults];
            [defsRunning setObject:arrOfRunningSubWorkOrderNo forKey:@"RunningSubWorkOrder"];
            [defsRunning setValue:strSubWorkOderNoRunning forKey:@"RunningSubWorkOrderNo"];
            [defsRunning synchronize];
            
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
    
    return isStarted;
}


-(NSMutableArray*)fetchRunningSubWorkOrder :(NSString*)strEmployeeNo :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId{
    
    NSMutableArray *arrOfTempSubWorkOrder = [NSMutableArray new];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    NSEntityDescription *entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    NSFetchRequest *requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo = %@ && workorderId = %@",strEmployeeNo,strWorkOrderId];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"employeeNo" ascending:NO];
    NSArray *sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        
        
    }
    else
    {
        
        NSString *strSubWorkOderNoRunning;
        strSubWorkOderNoRunning=@"";
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            NSManagedObject *matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSString *strSubWoStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOStatus"]];//subWorkOrderId   employeeNo
            NSString *subWorkOrderId=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderId"]];//subWorkOrderId   employeeNo
            NSString *strSubWoClockStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"clockStatus"]];//subWorkOrderId   employeeNo
            
            if ([strSubWoStatus isEqualToString:@"Completed"] || [strSubWoStatus isEqualToString:@"CompletePending"] || [strSubWoStatus isEqualToString:@"New"] || [strSubWoStatus isEqualToString:@"Not Started"] || [strSubWoStatus isEqualToString:@"Dispatch"] || [subWorkOrderId isEqualToString:strSubWorkOrderId] ) {
                
                
                
            }else{
                
                if ([strSubWoClockStatus caseInsensitiveCompare:@"Start"] == NSOrderedSame) {
                    
                    strSubWorkOderNoRunning =[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderNo"]];
                    
                    if ([arrOfTempSubWorkOrder containsObject:strSubWorkOderNoRunning]) {
                        
                        
                        
                    } else {
                        
                        [arrOfTempSubWorkOrder addObject:strSubWorkOderNoRunning];
                        
                    }
                    
                } else {
                    
                    
                }
            }
        }
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
    
    return arrOfTempSubWorkOrder;
}

-(NSString*)getEmployeeDeatils{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strDetails =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    return strDetails;
    
}

-(BOOL)isCompletedSatusMechanical :(NSString*)strStatus{
    
    BOOL isCompleted;
    isCompleted=NO;
    
    if ([strStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame) {
        isCompleted=YES;
    } else if ([strStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame){
        isCompleted=YES;
    } else if ([strStatus caseInsensitiveCompare:@"CompletePending"] == NSOrderedSame){
        isCompleted=YES;
    } else if ([strStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame){
        isCompleted=YES;
    }else{
        isCompleted=NO;
    }
    return isCompleted;
}



//===================================================================================================
//===================================================================================================

#pragma mark - ================= Employee Time Sheet Slot Wise Changes ===================

//===================================================================================================
//===================================================================================================


-(NSMutableArray*)employeeTimeSheetSlotWiseTimeEntry :(NSString*)strFromDate :(NSString*)strToDate :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId :(NSString*)subWorkOrderActualHourId {
    
    
    NSArray *arrOfHelper=[self fetchSubWorkOrderHelperFromDataBaseForSlotWiseTimeEntry:strWorkOrderId :strSubWorkOrderId];

    NSMutableArray *arrOfTimeSlots=[[NSMutableArray alloc]init];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    //---->>>>
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *ToDateToCheck = [[NSDate alloc] init];
    ToDateToCheck = [self ChangeDateToLocalDateMechanicalTimeOut:strToDate];
    
    NSDate *FromDateToCheck = [[NSDate alloc] init];
    FromDateToCheck = [self ChangeDateToLocalDateMechanicalTimeOut:strFromDate];
    
    float TotalPrice = 0.0;////
    
    if (!(arrOfHoursConfig.count==0)) {
        
        
        BOOL isSameDate=[self isSameDay:ToDateToCheck otherDay:FromDateToCheck];
        
        if([[self getOnlyDate:ToDateToCheck] compare:[self getOnlyDate:FromDateToCheck]] == NSOrderedSame) {
            
            isSameDate=YES;
            
        }else{
            
            isSameDate=NO;
            
        }
        
        if (isSameDate)
        {
            
            // To check If Holiday Date
            
            BOOL isHolidayDate=[self isHoliDayDate:FromDateToCheck];
            
            if (isHolidayDate) {
                
                // Total Price Logic To fetch From Master
                
                NSTimeInterval secondsBetween = [ToDateToCheck timeIntervalSinceDate:FromDateToCheck];
                
                int hrs = secondsBetween;
                
                int seconds = hrs % 60;
                int minutes = (hrs / 60) % 60;
                int hours = hrs / 3600;
                NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                float totalTime=0.0;
                actualHrsInSec=hours*3600;
                actualMinInSec=minutes*60;
                actualMinInSecSec=seconds*60;
                totalTime=actualHrsInSec+actualMinInSec+seconds;
                totalTime=totalTime/3600;
                
                float strStdPrice=0.0,strStdPriceLabor=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                    strStdPriceLabor=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];

                    
//                    if (!isStandardSubWorkOrder)
//                    {
//
//                        strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
//
//                    }
                    
                }
                
                //TotalPrice = totalTime*strStdPrice;
                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                
                
                float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                TotalPrice = [strRoundedValue intValue]*strStdPrice;

                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                
                //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];

                
                // Change for Labor Type
                
                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                
                if (isMechanicLocal) {
                    // is labor so pass labor price
                    
                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                    
                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    
                } else {
                    // is Helper so pass helper price
                    
                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                    
                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    
                }
                
                // Fetch All Helper for particular subworkorder
                
                for (int kk=0; kk<arrOfHelper.count; kk++) {
                    
                    NSManagedObject *objTemp=arrOfHelper[kk];
                    
                    strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                    strEmployeeName =[NSString stringWithFormat:@"%@",@""];

                    
                    // Change for Labor Type
                    
                    BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                    
                    if (isMechanicLocal) {
                        // is labor so pass labor price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        
                    } else {
                        // is Helper so pass helper price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        
                    }
                    
                }
                
                
            } else {
                
                // Fetching After Hour Rates
                
                NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                
                NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                
                //---->>>> Yaha Par After Hour Rates Ko Order By Karna Hai Ascending Mai(chote se bada mtlb----8,9,10,11,12 etc etc....)
                
                if (!(arrOfAfterHourRates.count==0)) {
                    
                    NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[0];
                    
                    NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                    
                    NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                    
                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                    
                    if ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedAscending) {
                        
                        //Setting StartDate and To date Coming From Db to New Object
                        
                        NSDate *NewStartTime=timeFromDBTimeIn;
                        NSDate *NewEndTime=timeToDBTimeOut;
                        
                        //Ascending mtlb first wala bada descending mtlb second wala bada
                        if (([fromTimeAfterHoursRateMaster compare: timeFromDBTimeIn] == NSOrderedDescending) && ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedAscending)) {
                            
                            NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0,strStdPriceLabor=0.0;
                            NSString *strTitle,*strTimeSlot;
                            
                            if (!(arrOfHoursConfig.count==0)) {
                                
                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                                strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                            }
                            
                            //TotalPrice = totalTime*strStdPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                            
                            
                            float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                            NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                            TotalPrice = [strRoundedValue intValue]*strStdPrice;

                            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                            
                            //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                            NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                            NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                            
                            
                            // Change for Labor Type
                            
                            BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                            
                            if (isMechanicLocal) {
                                // is labor so pass labor price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            } else {
                                // is Helper so pass helper price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            }
                            
                            // Fetch All Helper for particular subworkorder
                            
                            for (int kk=0; kk<arrOfHelper.count; kk++) {
                                
                                NSManagedObject *objTemp=arrOfHelper[kk];
                                
                                strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                }
                                
                            }

                            NewStartTime=fromTimeAfterHoursRateMaster;
                            NewEndTime=timeToDBTimeOut;
                            
                        }else if (([fromTimeAfterHoursRateMaster compare: timeFromDBTimeIn] == NSOrderedAscending) || ([fromTimeAfterHoursRateMaster compare: timeToDBTimeOut] == NSOrderedSame)){
                            
                            NewStartTime=timeFromDBTimeIn;
                            
                        }
                        
                        // For Loop on After Hours Rate
                        
                        for (int k=0; k<arrOfAfterHourRates.count; k++) {
                            
                            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k];
                            
                            NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                            
                            NSString *strToTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                            
                            NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                            
                            NSDate *toTimeAfterHoursRateMaster =[self changeStringTimeToDate:strToTime];// Master se aaya hua from time
                            
                            if (([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedAscending) && ([toTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending)) {
                                
                                if ([fromTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending) {
                                    
                                    NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:NewStartTime];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                    
                                    //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                    
                                    //,strStdPriceLabor=0.0;
                                    NSString *strTitle,*strTimeSlot;
                                    float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                    strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                                    
                                    float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                    NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                    TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                    
                                    //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                    NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    }
                                    
                                    // Fetch All Helper for particular subworkorder
                                    
                                    for (int kk=0; kk<arrOfHelper.count; kk++) {
                                        
                                        NSManagedObject *objTemp=arrOfHelper[kk];
                                        
                                        strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                        strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                        
                                        
                                        // Change for Labor Type
                                        
                                        BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                        
                                        if (isMechanicLocal) {
                                            // is labor so pass labor price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        } else {
                                            // is Helper so pass helper price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        }
                                        
                                    }

                                    NewStartTime=fromTimeAfterHoursRateMaster;
                                    
                                    
                                }
                                
                                NSTimeInterval diff = [toTimeAfterHoursRateMaster timeIntervalSinceDate:NewStartTime];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                
                                //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                
                                NSString *strTitle,*strTimeSlot;
                                float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                                
                                float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                
                                //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                }
                                
                                // Fetch All Helper for particular subworkorder
                                
                                for (int kk=0; kk<arrOfHelper.count; kk++) {
                                    
                                    NSManagedObject *objTemp=arrOfHelper[kk];
                                    
                                    strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                    strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    }
                                    
                                }

                                NewStartTime=toTimeAfterHoursRateMaster;
                                
                            }else if (([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedDescending) || ([toTimeAfterHoursRateMaster compare: NewEndTime] == NSOrderedSame)){
                                
                                NSTimeInterval diff = [NewEndTime timeIntervalSinceDate:NewStartTime];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                
                                //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                
                                
                                NSString *strTitle,*strTimeSlot;
                                float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                                float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;
                                
                                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                
                                //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                }
                                
                                // Fetch All Helper for particular subworkorder
                                
                                for (int kk=0; kk<arrOfHelper.count; kk++) {
                                    
                                    NSManagedObject *objTemp=arrOfHelper[kk];
                                    
                                    strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                    strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strFromDate :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    }
                                    
                                }

                                NewStartTime=toTimeAfterHoursRateMaster;
                                
                                break;
                                
                            }
                        }
                        //
                        if ([NewEndTime compare: NewStartTime] == NSOrderedDescending) {
                            
                            
                            NSTimeInterval diff = [NewEndTime timeIntervalSinceDate:NewStartTime];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0,strStdPriceLabor=0.0;
                            NSString *strTitle,*strTimeSlot;

                            if (!(arrOfHoursConfig.count==0)) {
                                
                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                                strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                            }
                            
                            //TotalPrice = totalTime*strStdPrice+TotalPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                            
                            
                            float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                            NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                            TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                            
                            //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                            NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                            NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                            

                            // Change for Labor Type

                            BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                            
                            if (isMechanicLocal) {
                                // is labor so pass labor price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            } else {
                                // is Helper so pass helper price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            }
                            
                            
                            // Fetch All Helper for particular subworkorder
                            
                            for (int kk=0; kk<arrOfHelper.count; kk++) {
                                
                                NSManagedObject *objTemp=arrOfHelper[kk];
                                
                                strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                
                                
                                // Change for Labor Type

                                BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                }
                                
                            }

                        }
                        
                    }else{
                        
                        NSTimeInterval diff = [timeToDBTimeOut timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                        
                        int hrs = diff;
                        int seconds = hrs % 60;
                        int minutes = (hrs / 60) % 60;
                        int hours = hrs / 3600;
                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                        float totalTime=0.0;
                        actualHrsInSec=hours*3600;
                        actualMinInSec=minutes*60;
                        actualMinInSecSec=seconds*60;
                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                        totalTime=totalTime/3600;
                        
                        float strStdPrice=0.0,strStdPriceLabor=0.0;
                        NSString *strTitle,*strTimeSlot;
                        
                        if (!(arrOfHoursConfig.count==0)) {
                            
                            NSDictionary *dictDataHours=arrOfHoursConfig[0];
                            
                            strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                            strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                            strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                            strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                        }
                        
                        //TotalPrice = totalTime*strStdPrice+TotalPrice;
                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                        
                        float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                        NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :totalTime*60]];
                        TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                        
                        //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                        NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                        
                        
                        // Change for Labor Type
                        
                        BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                        
                        if (isMechanicLocal) {
                            // is labor so pass labor price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        } else {
                            // is Helper so pass helper price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        }
                        
                        // Fetch All Helper for particular subworkorder
                        
                        for (int kk=0; kk<arrOfHelper.count; kk++) {
                            
                            NSManagedObject *objTemp=arrOfHelper[kk];
                            
                            strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                            strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                            
                            
                            // Change for Labor Type
                            
                            BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                            
                            if (isMechanicLocal) {
                                // is labor so pass labor price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                
                            } else {
                                // is Helper so pass helper price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                
                            }
                            
                        }

                    }
                }else{
                    // if after hour not exist
//                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
//
//                    NSDate *timeToDBTimeOut =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    //Change on 27 August as blank dynamic employee sheet was coming on invoice due to minus

                    NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                    
                    NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                    
                    NSTimeInterval diff = [timeToDBTimeOut timeIntervalSinceDate:timeFromDBTimeIn];//Subtract
                    
                    int hrs = diff;
                    int seconds = hrs % 60;
                    int minutes = (hrs / 60) % 60;
                    int hours = hrs / 3600;
                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                    float totalTime=0.0;
                    actualHrsInSec=hours*3600;
                    actualMinInSec=minutes*60;
                    actualMinInSecSec=seconds*60;
                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                    totalTime=totalTime/3600;
                    
                    float strStdPrice=0.0,strStdPriceLabor=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                        strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                    }
                    
                    
                    float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                    NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                    
                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                    
                    //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                    NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                    
                    
                    // Change for Labor Type
                    
                    BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                    
                    if (isMechanicLocal) {
                        // is labor so pass labor price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    } else {
                        // is Helper so pass helper price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    }
                    
                    // Fetch All Helper for particular subworkorder
                    
                    for (int kk=0; kk<arrOfHelper.count; kk++) {
                        
                        NSManagedObject *objTemp=arrOfHelper[kk];
                        
                        strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                        strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                        

                        
                        // Change for Labor Type
                        
                        BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                        
                        if (isMechanicLocal) {
                            // is labor so pass labor price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            
                        } else {
                            // is Helper so pass helper price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strFromDate :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            
                        }
                        
                    }

                    TotalPrice = totalTime*strStdPrice+TotalPrice;
                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                    
                }
            }
        }else{
            
            NSDate *StartDate = FromDateToCheck;
            
            while (([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedAscending) || ([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedSame)) {
                
                NSString *strDateToSet=[NSString stringWithFormat:@"%@",StartDate];
                
                NSString *strEndTimeOfDay=[self getStartTimeofDay:StartDate];
                
                strEndTimeOfDay=[strEndTimeOfDay stringByAppendingString:@" 23:59:59"];
                
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSDate* newTime = [dateFormatter dateFromString:strEndTimeOfDay];
                NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
                [dateFormatterNew setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
                //---->>>>
                NSDate *EndDate=[[NSDate alloc]init];
                
                EndDate = [dateFormatterNew dateFromString:finalTime];
                
                if ([[self getOnlyDate:StartDate] compare: [self getOnlyDate:ToDateToCheck]] == NSOrderedSame) {
                    
                    EndDate=ToDateToCheck;
                    
                }
                //Calculating Price
                
                // To check If Holiday Date
                
                BOOL isHolidayDate=[self isHoliDayDate:StartDate];
                
                if (isHolidayDate) {
                    
                    // Total Price Logic To fetch From Master
                    
                    //  NSTimeInterval secondsBetween = [StartDate timeIntervalSinceDate:EndDate];
                    NSTimeInterval secondsBetween = [EndDate timeIntervalSinceDate:StartDate];
                    
                    int hrs = secondsBetween;
                    
                    int seconds = hrs % 60;
                    int minutes = (hrs / 60) % 60;
                    int hours = hrs / 3600;
                    NSLog(@"Total time %@",[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]);
                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                    float totalTime=0.0;
                    actualHrsInSec=hours*3600;
                    actualMinInSec=minutes*60;
                    actualMinInSecSec=seconds*60;
                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                    totalTime=totalTime/3600;
                    
                    float strStdPrice=0.0,strStdPriceLabor=0.0;
                    
                    if (!(arrOfHoursConfig.count==0)) {
                        
                        NSDictionary *dictDataHours=arrOfHoursConfig[0];
                        
                        strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                        strStdPriceLabor=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];

//                        if (!isStandardSubWorkOrder)
//                        {
//
//                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
//
//                        }
                        
                    }
                    
                    //TotalPrice = totalTime*strStdPrice+TotalPrice;
                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                    
                    
                    float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                    NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                    TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                    
                    //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                    NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                    
                    
                    // Change for Labor Type
                    
                    BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                    
                    if (isMechanicLocal) {
                        // is labor so pass labor price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    } else {
                        // is Helper so pass helper price
                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                        
                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                    }
                    
                    
                    // Fetch All Helper for particular subworkorder
                    
                    for (int kk=0; kk<arrOfHelper.count; kk++) {
                        
                        NSManagedObject *objTemp=arrOfHelper[kk];
                        
                        strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                        strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                        
                        // Change for Labor Type
                        
                        BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                        
                        if (isMechanicLocal) {
                            // is labor so pass labor price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            
                        } else {
                            // is Helper so pass helper price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"3" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            
                        }
                        
                    }

                }else{
                    
                    // Fetching After Hour Rates
                    
                    NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                    
                    NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                    
                    //---->>>> Yaha Par After Hour Rates Ko Order By Karna Hai Ascending Mai(chote se bada mtlb----8,9,10,11,12 etc etc....)
                    
                    if (!(arrOfAfterHourRates.count==0)) {
                        
                        NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[0];
                        
                        NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                        
                        NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                        
                        NSDate *timeFromDBTimeIn =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                        
                        NSDate *timeToDBTimeOut =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                        
                        if ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:EndDate]] == NSOrderedAscending) {
                            
                            //Setting StartDate and To date Coming From Db to New Object
                            
                            NSDate *NewStartTime=timeFromDBTimeIn;
                            NSDate *NewEndTime=[self getOnlyTime:EndDate];
                            
                            if (([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedDescending) && ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:EndDate]] == NSOrderedAscending)) {
                                
                                NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=0.0,strStdPriceLabor=0.0;
                                NSString *strTitle,*strTimeSlot;
                                
                                if (!(arrOfHoursConfig.count==0)) {
                                    
                                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                    
                                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                    strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                                    strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                                }
                                
                                //TotalPrice = totalTime*strStdPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                
                                
                                float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                TotalPrice = [strRoundedValue intValue]*strStdPrice;

                                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                
                                //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                }
                                
                                // Fetch All Helper for particular subworkorder
                                
                                for (int kk=0; kk<arrOfHelper.count; kk++) {
                                    
                                    NSManagedObject *objTemp=arrOfHelper[kk];
                                    
                                    strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                    strEmployeeName =[NSString stringWithFormat:@"%@",@""];

                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    }
                                    
                                }

                                NewStartTime=fromTimeAfterHoursRateMaster;
                                NewEndTime=[self getOnlyTime:EndDate];
                                
                            }else if (([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedAscending) || ([fromTimeAfterHoursRateMaster compare: [self getOnlyTime:StartDate]] == NSOrderedSame)){
                                
                                NewStartTime=[self getOnlyTime:StartDate];
                                
                            }
                            
                            // For Loop on After Hours Rate
                            
                            for (int k=0; k<arrOfAfterHourRates.count; k++) {
                                
                                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k];
                                
                                NSString *strFromTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"FromTime"]];
                                
                                NSString *strToTime =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                
                                NSDate *fromTimeAfterHoursRateMaster =[self changeStringTimeToDate:strFromTime];// Master se aaya hua from time
                                
                                NSDate *toTimeAfterHoursRateMaster =[self changeStringTimeToDate:strToTime];// Master se aaya hua from time
                                
                                if (([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedAscending) && ([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewStartTime]] == NSOrderedDescending)) {
                                    
                                    if ([fromTimeAfterHoursRateMaster compare: NewStartTime] == NSOrderedDescending) {
                                        
                                        NSTimeInterval diff = [fromTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                        
                                        int hrs = diff;
                                        int seconds = hrs % 60;
                                        int minutes = (hrs / 60) % 60;
                                        int hours = hrs / 3600;
                                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                        float totalTime=0.0;
                                        actualHrsInSec=hours*3600;
                                        actualMinInSec=minutes*60;
                                        actualMinInSecSec=seconds*60;
                                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                                        totalTime=totalTime/3600;
                                        
                                        float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                        
                                        //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                        
                                        
                                        NSString *strTitle,*strTimeSlot;
                                        float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                        strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                        strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                        
                                        
                                        float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                        NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                        TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                        
                                        //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                        NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                        
                                        
                                        // Change for Labor Type
                                        
                                        BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                        
                                        if (isMechanicLocal) {
                                            // is labor so pass labor price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        } else {
                                            // is Helper so pass helper price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        }
                                        
                                        // Fetch All Helper for particular subworkorder
                                        
                                        for (int kk=0; kk<arrOfHelper.count; kk++) {
                                            
                                            NSManagedObject *objTemp=arrOfHelper[kk];
                                            
                                            strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                            strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                            
                                            // Change for Labor Type
                                            
                                            BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                            
                                            if (isMechanicLocal) {
                                                // is labor so pass labor price
                                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                                
                                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                                
                                            } else {
                                                // is Helper so pass helper price
                                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                                
                                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                                
                                            }
                                            
                                        }

                                        NewStartTime=fromTimeAfterHoursRateMaster;
                                        
                                    }
                                    
                                    NSTimeInterval diff = [toTimeAfterHoursRateMaster timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                    
                                    //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                    
                                    
                                    NSString *strTitle,*strTimeSlot;
                                    float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                    strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    
                                    
                                    float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                    NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                    TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                    
                                    //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                    NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    }
                                    
                                    // Fetch All Helper for particular subworkorder
                                    
                                    for (int kk=0; kk<arrOfHelper.count; kk++) {
                                        
                                        NSManagedObject *objTemp=arrOfHelper[kk];
                                        
                                        strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                        strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                        
                                        
                                        // Change for Labor Type
                                        
                                        BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                        
                                        if (isMechanicLocal) {
                                            // is labor so pass labor price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        } else {
                                            // is Helper so pass helper price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        }
                                        
                                    }

                                    NewStartTime=toTimeAfterHoursRateMaster;
                                    
                                }else if (([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedDescending) || ([toTimeAfterHoursRateMaster compare: [self getOnlyTime:NewEndTime]] == NSOrderedSame)){
                                    
                                    NSTimeInterval diff = [[self getOnlyTime:NewEndTime] timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                    
                                    int hrs = diff;
                                    int seconds = hrs % 60;
                                    int minutes = (hrs / 60) % 60;
                                    int hours = hrs / 3600;
                                    int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                    float totalTime=0.0;
                                    actualHrsInSec=hours*3600;
                                    actualMinInSec=minutes*60;
                                    actualMinInSecSec=seconds*60;
                                    totalTime=actualHrsInSec+actualMinInSec+seconds;
                                    totalTime=totalTime/3600;
                                    
                                    float strStdPrice=[[dictDataAfterHoursRate valueForKey:@"LaborPrice"] floatValue];
                                    
                                    //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                    NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                    
                                    
                                    NSString *strTitle,*strTimeSlot;
                                    float strStdPriceLabor=[[dictDataAfterHoursRate valueForKey:@"HelperPrice"] floatValue];
                                    strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    
                                    
                                    float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                    NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                    TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                    
                                    //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                    NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    }
                                    
                                    // Fetch All Helper for particular subworkorder
                                    
                                    for (int kk=0; kk<arrOfHelper.count; kk++) {
                                        
                                        NSManagedObject *objTemp=arrOfHelper[kk];
                                        
                                        strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                        strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                        

                                        // Change for Labor Type
                                        
                                        BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                        
                                        if (isMechanicLocal) {
                                            // is labor so pass labor price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        } else {
                                            // is Helper so pass helper price
                                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"2" :strDateToSet :strTitle :strTimeSlot :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                            
                                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                            
                                        }
                                        
                                    }

                                    NewStartTime=toTimeAfterHoursRateMaster;
                                    
                                    break;
                                    
                                }
                            }
                            //
                            if ([[self getOnlyTime:NewEndTime] compare: [self getOnlyTime:NewStartTime]] == NSOrderedDescending) {
                                
                                
                                NSTimeInterval diff = [[self getOnlyTime:NewEndTime] timeIntervalSinceDate:[self getOnlyTime:NewStartTime]];//Subtract
                                
                                int hrs = diff;
                                int seconds = hrs % 60;
                                int minutes = (hrs / 60) % 60;
                                int hours = hrs / 3600;
                                int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                                float totalTime=0.0;
                                actualHrsInSec=hours*3600;
                                actualMinInSec=minutes*60;
                                actualMinInSecSec=seconds*60;
                                totalTime=actualHrsInSec+actualMinInSec+seconds;
                                totalTime=totalTime/3600;
                                
                                float strStdPrice=0.0,strStdPriceLabor=0.0;
                                NSString *strTitle,*strTimeSlot;
                                
                                if (!(arrOfHoursConfig.count==0)) {
                                    
                                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                    
                                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                    strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                                    strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                                }
                                
                                //TotalPrice = totalTime*strStdPrice+TotalPrice;
                                NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                                
                                float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                                NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                                TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                                NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                                NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                                
                                //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                                NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                                NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                                
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                }
                                
                                // Fetch All Helper for particular subworkorder
                                
                                for (int kk=0; kk<arrOfHelper.count; kk++) {
                                    
                                    NSManagedObject *objTemp=arrOfHelper[kk];
                                    
                                    strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                    strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                    
                                    
                                    // Change for Labor Type
                                    
                                    BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                    
                                    if (isMechanicLocal) {
                                        // is labor so pass labor price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    } else {
                                        // is Helper so pass helper price
                                        NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                        
                                        [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                        
                                    }
                                    
                                }

                            }
                            
                        }else{
                            
                            NSTimeInterval diff = [[self getOnlyTime:EndDate] timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                            
                            int hrs = diff;
                            int seconds = hrs % 60;
                            int minutes = (hrs / 60) % 60;
                            int hours = hrs / 3600;
                            int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                            float totalTime=0.0;
                            actualHrsInSec=hours*3600;
                            actualMinInSec=minutes*60;
                            actualMinInSecSec=seconds*60;
                            totalTime=actualHrsInSec+actualMinInSec+seconds;
                            totalTime=totalTime/3600;
                            
                            float strStdPrice=0.0,strStdPriceLabor=0.0;
                            NSString *strTitle,*strTimeSlot;
                            
                            if (!(arrOfHoursConfig.count==0)) {
                                
                                NSDictionary *dictDataHours=arrOfHoursConfig[0];
                                
                                strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                                strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                                strTitle=[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                strTimeSlot=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];

                            }
                            
                            //TotalPrice = totalTime*strStdPrice+TotalPrice;
                            NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                            
                            
                            float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                            NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                            TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                            
                            //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                            NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                            NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                            
                            
                            // Change for Labor Type
                            
                            BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                            
                            if (isMechanicLocal) {
                                // is labor so pass labor price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            } else {
                                // is Helper so pass helper price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                            }
                            
                            // Fetch All Helper for particular subworkorder
                            
                            for (int kk=0; kk<arrOfHelper.count; kk++) {
                                
                                NSManagedObject *objTemp=arrOfHelper[kk];
                                
                                strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                                strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                                
                                
                                // Change for Labor Type
                                
                                BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                                
                                if (isMechanicLocal) {
                                    // is labor so pass labor price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                } else {
                                    // is Helper so pass helper price
                                    NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                    
                                    [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                    
                                }
                                
                            }

                        }
                    }else{
                        
                        // if after hour not exist
                        //NSDate *timeFromDBTimeIn =[self convertDateToTime:strToDate];//DB Mai jo Save hai vo wala time
                        
                        //NSDate *timeToDBTimeOut =[self convertDateToTime:strFromDate];//DB Mai jo Save hai vo wala time
                        
                        NSTimeInterval diff = [[self getOnlyTime:EndDate] timeIntervalSinceDate:[self getOnlyTime:StartDate]];//Subtract
                        
                        int hrs = diff;
                        int seconds = hrs % 60;
                        int minutes = (hrs / 60) % 60;
                        int hours = hrs / 3600;
                        int actualHrsInSec,actualMinInSec,actualMinInSecSec;
                        float totalTime=0.0;
                        actualHrsInSec=hours*3600;
                        actualMinInSec=minutes*60;
                        actualMinInSecSec=seconds*60;
                        totalTime=actualHrsInSec+actualMinInSec+seconds;
                        totalTime=totalTime/3600;
                        
                        float strStdPrice=0.0,strStdPriceLabor=0.0;
                        
                        if (!(arrOfHoursConfig.count==0)) {
                            
                            NSDictionary *dictDataHours=arrOfHoursConfig[0];
                            
                            strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                            strStdPriceLabor=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];

                        }
                        
                        //TotalPrice = totalTime*strStdPrice+TotalPrice;
                        NSLog(@"Total Time====%f, Total Price====%f, strPrice===%f",totalTime,TotalPrice,strStdPrice);
                        float TimeInSecond=actualHrsInSec+actualMinInSec+seconds;
                        NSString *strRoundedValue=[NSString stringWithFormat:@"%d",[self roundedOffTimeInMin:TimeInSecond :TotalPrice]];
                        TotalPrice = [strRoundedValue intValue]*strStdPrice+TotalPrice;

                        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
                        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
                        
                        //NSString *strEmployeeId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
                        NSString *strEmployeeNo =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
                        NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
                        
                        
                        // Change for Labor Type
                        
                        BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderId];
                        
                        if (isMechanicLocal) {
                            // is labor so pass labor price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        } else {
                            // is Helper so pass helper price
                            NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                            
                            [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                        }
                        
                        // Fetch All Helper for particular subworkorder
                        
                        for (int kk=0; kk<arrOfHelper.count; kk++) {
                            
                            NSManagedObject *objTemp=arrOfHelper[kk];
                            
                            strEmployeeNo =[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
                            strEmployeeName =[NSString stringWithFormat:@"%@",@""];
                            

                            // Change for Labor Type
                            
                            BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderId :strEmployeeNo];
                            
                            if (isMechanicLocal) {
                                // is labor so pass labor price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPrice] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                
                            } else {
                                // is Helper so pass helper price
                                NSArray *arrTempSlots=[self saveSlotWiseTime:strWorkOrderId :@"1" :strDateToSet :@"" :@"" :strSubWorkOrderId :subWorkOrderActualHourId :[self getReferenceNumber] :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :[NSString stringWithFormat:@"%f",strStdPriceLabor] :strRoundedValue :strEmployeeName :strEmployeeNo];
                                
                                [arrOfTimeSlots addObjectsFromArray:arrTempSlots];
                                
                            }
                            
                        }

                        
                    }
                    
                }
                
                // }
                
                int daysToAdd = 1;
                StartDate = [StartDate dateByAddingTimeInterval:daysToAdd*24*60*60];
                
                NSString *strStartTimeOfDay1=[self ChangeDateToLocalDateMechanicalTimeOutToGetStartAndEndTimeFromDate:StartDate];
                
                strStartTimeOfDay1=[strStartTimeOfDay1 stringByAppendingString:@" 00:00:00"];
                
                NSDateFormatter* dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatter1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSDate* newTime1 = [dateFormatter1 dateFromString:strStartTimeOfDay1];
                NSDateFormatter* dateFormatterNew1 = [[NSDateFormatter alloc] init];
                [dateFormatterNew1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                [dateFormatterNew1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                NSString* finalTime1 = [dateFormatterNew1 stringFromDate:newTime1];
                //---->>>>
                StartDate=[[NSDate alloc]init];
                
                StartDate = [dateFormatterNew dateFromString:finalTime1];
                
                //---->>>>
                StartDate=StartDate;
                
            }
            
        }//End Different Date
    }
    
    return arrOfTimeSlots;
    
}


-(NSMutableArray*)saveSlotWiseTime :(NSString*)strWorkOrderId :(NSString*)workingType :(NSString*)workingDate :(NSString*)timeSlotTitle :(NSString*)timeSlot :(NSString*)subWorkOrderId :(NSString*)subWorkOrderActualHourId :(NSString*)subWOEmployeeWorkingTimeId :(NSString*)actualAmt :(NSString*)actualDurationInMin :(NSString*)billableAmt :(NSString*)billableDurationInMin :(NSString*)strEmployeeNameComing :(NSString*)strEmployeeNoComing{
    
    workingDate=[self changeDtaeEmpTimeSheet:workingDate];
    
    strEmployeeNameComing=@"";
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmployeeName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeName"]];
    NSString *strDate =[self strCurrentDate];

    float actualAmountComingForOneMin = [actualAmt floatValue]/60;
    float billableAmountComingForOneMin = [actualAmt floatValue]/60;

    actualAmt = [NSString stringWithFormat:@"%f",[actualDurationInMin floatValue]*actualAmountComingForOneMin];
    billableAmt = [NSString stringWithFormat:@"%f",[billableDurationInMin floatValue]*billableAmountComingForOneMin];

    
    NSArray *objValue=[NSArray arrayWithObjects:
                       actualAmt,
                       actualDurationInMin,
                       billableAmt,
                       billableDurationInMin,
                       strEmployeeName,
                       @"Mobile",
                       strDate,
                       strEmployeeNameComing,
                       strEmployeeNoComing,
                       @"true",
                       @"true",
                       strEmployeeName,
                       strDate,
                       subWOEmployeeWorkingTimeId,
                       subWorkOrderActualHourId,
                       subWorkOrderId,
                       timeSlot,
                       timeSlotTitle,
                       workingDate,
                       workingType,
                       strWorkOrderId,nil];
    
    NSArray *objKey=[NSArray arrayWithObjects:
                     @"actualAmt",
                     @"actualDurationInMin",
                     @"billableAmt",
                     @"billableDurationInMin",
                     @"createdBy",//USerName
                     @"createdByDevice",
                     @"createdDate",
                     @"employeeName",//USerName employee no for which we are saving data
                     @"employeeNo",//employee no for which we are saving data
                     @"isActive",
                     @"isApprove",
                     @"modifiedBy",//USerName
                     @"modifiedDate",
                     @"subWOEmployeeWorkingTimeId",// dynamic create
                     @"subWorkOrderActualHourId",
                     @"subWorkOrderId",
                     @"timeSlot",// holiday null and STandard mai null----after hours mai jo bhi aata vo
                     @"timeSlotTitle",// holiday null and STandard mai null----after hours mai jo bhi aata vo
                     @"workingDate",
                     @"workingType",
                     @"workorderId",nil];
    
    
    NSDictionary *dict_ToSend=[[NSDictionary alloc] initWithObjects:objValue forKeys:objKey];

    NSMutableArray *arrSlots=[[NSMutableArray alloc]init];
    
    [arrSlots addObject:dict_ToSend];
    
    return arrSlots;
    
}

-(NSArray*)fetchSubWorkOrderHelperFromDataBaseForSlotWiseTimeEntry :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    NSArray *arrOfHelper= [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrOfHelper;
    
}

-(int)roundedOffTimeInMin :(float)TimeInSecond :(float)totalTime{
    
    float timeInMinutes =TimeInSecond/60;
    
    NSString *strTotalTime = [NSString stringWithFormat:@"%f",timeInMinutes];
    
    totalTime=[strTotalTime floatValue];
    
    NSRange equalRange = [strTotalTime rangeOfString:@"." options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        strTotalTime = [strTotalTime substringFromIndex:equalRange.location+1];
    }
    
    int roundedValue;
    
    if (TimeInSecond>30) {
        
        roundedValue = ceil(totalTime); NSLog(@"%d",roundedValue);
        
    } else {
        
        roundedValue = floor(totalTime); NSLog(@"%d",roundedValue);
        
    }
    
    return roundedValue;
    
}


-(NSString*)methodToCalculateLaborPriceGlobal :(NSString *)strTimeEntered :(BOOL)isLaborType :(NSArray*)arrOfHoursConfig :(BOOL)isStandardSubWorkOrder :(BOOL)isHoliday :(NSString*)strAfterHrsDuration {
    
    isStandardSubWorkOrder=NO;
    
    strTimeEntered=[self getHrAndMinsGlobal:strTimeEntered];
    
    NSString *strAmount;
    BOOL isWarranty=NO;
    
    if (strTimeEntered.length==5) {
        
        NSDictionary *dictDataAfterHourRateToBeUsed=[[NSDictionary alloc]init];
        
        if (!isWarranty) {
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[self ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"StdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                //strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR;
                                NSString *strToTimeAHR;
                                if (tempSplittedArray.count==2) {
                                    
                                    strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                    strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                    strFromTimeAHR=[strFromTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    strToTimeAHR=[strToTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    
                                }else{
                                    
                                    strFromTimeAHR=@"zxc";
                                    strToTimeAHR=@"zxx";
                                }
                                
                                //strFromTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                //strToTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                NSArray *arrTempDictKey=[dictDataAfterHourRateToBeUsed allKeys];
                                
                                if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]]) || (arrTempDictKey.count==0) || (![arrTempDictKey containsObject:@"ToTime"])) {
                                    
                                    
                                    
                                } else {
                                    
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborPrice"] floatValue];
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[self roundedOffValuess:hrsTime]*strStdPrice;
                
                strAmount=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                
                // Change to fetch afetr rates config
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    if (strAfterHrsDuration.length==0) {
                        
                        //strStdPrice=0.0;
                        
                    } else {
                        
                        NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                        
                        
                        NSString *strFromTimeAHR;
                        NSString *strToTimeAHR;
                        if (tempSplittedArray.count==2) {
                            
                            strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                            strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                            strFromTimeAHR=[strFromTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                            strToTimeAHR=[strToTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                            
                        }else{
                            
                            strFromTimeAHR=@"zxc";
                            strToTimeAHR=@"zxx";
                        }
                        
                        //strFromTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                        //strToTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                        
                        
                        NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                        
                        for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                            
                            NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                            
                            NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                            NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                            
                            if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                
                                dictDataAfterHourRateToBeUsed=dictDataAHC;
                                break;
                                
                            }
                            
                        }
                    }
                }
                    
                //Logic For Price Calculation
                
                float hrsTime=[self ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperPrice"] floatValue];
                            
                        } else {
                            
                            NSArray *arrTempDictKey=[dictDataAfterHourRateToBeUsed allKeys];
                            
                            if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]]) || (arrTempDictKey.count==0) || (![arrTempDictKey containsObject:@"ToTime"])) {
                                
                                
                                
                            } else {
                                
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                                
                            }
                            
                            //strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperPrice"] floatValue];
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[self roundedOffValuess:hrsTime]*strStdPrice;
                
                strAmount=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
        }else{
            
            // Is Warranty Checked Hai
            
            if (isLaborType) {
                
                //Logic For Price Calculation
                
                float hrsTime=[self ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"WrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        // Change for holiday and After hours change
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayLaborWrntyPrice"] floatValue];
                            
                        } else {
                            
                            // AfterHourRateConfigDcs k change krna hai yaha p strAfterHrsDuration  07:00 pm - 07:30 pm
                            
                            if (strAfterHrsDuration.length==0) {
                                
                                //strStdPrice=0.0;
                                
                            } else {
                                
                                NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                                
                                NSString *strFromTimeAHR;
                                NSString *strToTimeAHR;
                                if (tempSplittedArray.count==2) {
                                    
                                    strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                                    strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                                    strFromTimeAHR=[strFromTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    strToTimeAHR=[strToTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    
                                }else{
                                    
                                    strFromTimeAHR=@"zxc";
                                    strToTimeAHR=@"zxx";
                                }
                                
                                //strFromTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                                //strToTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                                
                                
                                NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                                    
                                    NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                                    
                                    NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                                    NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                                    
                                    if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                        
                                        dictDataAfterHourRateToBeUsed=dictDataAHC;
                                        break;
                                        
                                    }
                                    
                                }
                                
                                NSArray *arrTempDictKey=[dictDataAfterHourRateToBeUsed allKeys];
                                
                                if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]]) || (arrTempDictKey.count==0) || (![arrTempDictKey containsObject:@"ToTime"])) {
                                    
                                    
                                    
                                } else {
                                    
                                strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"LaborWrntyPrice"] floatValue];
                                    
                                }
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[self roundedOffValuess:hrsTime]*strStdPrice;

                strAmount=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
                
            } else {
                
                // Change to fetch afetr rates config
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    if (strAfterHrsDuration.length==0) {
                        
                        //strStdPrice=0.0;
                        
                    } else {
                        
                        NSArray* tempSplittedArray = [strAfterHrsDuration componentsSeparatedByString: @"-"];
                        
                        NSString *strFromTimeAHR;
                        NSString *strToTimeAHR;
                        if (tempSplittedArray.count==2) {
                            
                            strFromTimeAHR=[tempSplittedArray objectAtIndex: 0];
                            strToTimeAHR=[tempSplittedArray objectAtIndex: 1];
                            strFromTimeAHR=[strFromTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                            strToTimeAHR=[strToTimeAHR stringByReplacingOccurrencesOfString:@" " withString:@""];
                            
                        }else{
                            
                            strFromTimeAHR=@"zxc";
                            strToTimeAHR=@"zxx";
                        }
                        
                        //strFromTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strFromTimeAHR];
                        //strToTimeAHR=[self ChangeTimeFromTwToTFHrsFormat:strToTimeAHR];
                        
                        
                        NSArray *arrOfAfterHourRateConfigDcs=[dictDataHours valueForKey:@"AfterHourRateConfigDcs"];
                        
                        for (int k1=0; k1<arrOfAfterHourRateConfigDcs.count; k1++) {
                            
                            NSDictionary *dictDataAHC=arrOfAfterHourRateConfigDcs[k1];
                            
                            NSString *strFromTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"FromTime"]];
                            NSString *strToTimeAHRToCompare=[NSString stringWithFormat:@"%@",[dictDataAHC valueForKey:@"ToTime"]];
                            
                            if ([strFromTimeAHRToCompare isEqualToString:strFromTimeAHR] && [strToTimeAHRToCompare isEqualToString:strToTimeAHR]) {
                                
                                dictDataAfterHourRateToBeUsed=dictDataAHC;
                                break;
                                
                            }
                            
                        }
                    }
                }
                
                
                //Logic For Price Calculation
                
                float hrsTime=[self ChangeTimeMechanicalLabor:strTimeEntered];
                
                hrsTime=hrsTime/3600;
                
                float strStdPrice=0.0;
                
                if (!(arrOfHoursConfig.count==0)) {
                    
                    NSDictionary *dictDataHours=arrOfHoursConfig[0];
                    
                    strStdPrice=[[dictDataHours valueForKey:@"HelperWrntyStdPrice"] floatValue];
                    
                    if (!isStandardSubWorkOrder) {
                        
                        if (isHoliday) {
                            
                            strStdPrice=[[dictDataHours valueForKey:@"HolidayHelperWrntyPrice"] floatValue];
                            
                        } else {
                            
                            NSArray *arrTempDictKey=[dictDataAfterHourRateToBeUsed allKeys];
                            
                            if ((dictDataAfterHourRateToBeUsed==nil) || ([dictDataAfterHourRateToBeUsed isKindOfClass:[NSNull class]]) || (arrTempDictKey.count==0) || (![arrTempDictKey containsObject:@"ToTime"])) {
                                
                                
                                
                            } else {
                                
                            strStdPrice=[[dictDataAfterHourRateToBeUsed valueForKey:@"HelperWrntyPrice"] floatValue];
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                //float totalPriceLabour=hrsTime*strStdPrice;
                
                float totalPriceLabour=[self roundedOffValuess:hrsTime]*strStdPrice;

                strAmount=[NSString stringWithFormat:@"%.02f",totalPriceLabour];
            }
            
            
        }
        
    }else{
        
        strAmount=@"";
        
    }
    return strAmount;
}

-(void)saveEmployeeTimeSheetSlotWise :(NSArray*)arrEmpSlots{
    
    
    for (int k=0; k<arrEmpSlots.count; k++) {
        
        NSDictionary *dictData=arrEmpSlots[k];
        
        NSArray *arrTempp=[dictData valueForKey:@"EmployeeList"];
        
        for (int i=0; i<arrTempp.count; i++) {
            
            NSDictionary *dictDataaa=arrTempp[i];
            
            NSString *strBillableAmt=[dictDataaa valueForKey:@"billableAmt"];
            NSString *strActualAmt=[dictDataaa valueForKey:@"actualAmt"];
            NSString *strBillableDuration=[dictDataaa valueForKey:@"billableDurationInMin"];
            NSString *strActualDuration=[dictDataaa valueForKey:@"actualDurationInMin"];
            
            float BillAmount=[strBillableAmt floatValue];
            float ActualAmount=[strActualAmt floatValue];
            float BillDuration=[strBillableDuration floatValue];
            float ActualDuration=[strActualDuration floatValue];
            
            if ((BillAmount>0) || (ActualAmount>0) || (BillDuration>0) || (ActualDuration>0)) {
                
                entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                
                SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
                
                
                NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrTempp[i];
                
                objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWOEmployeeWorkingTimeId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workorderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderActualHourId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeNo"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeName"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlotTitle"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlot"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualDurationInMin"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualAmt"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableDurationInMin"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableAmt"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingType"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isApprove"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isActive"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdByDevice"]];
                
                // Change for Labor Type
                
                if ([[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"Helper"]] isEqualToString:@"Yes"]) {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"True";
                    
                } else {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"False";
                    
                }
                
                NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
                
            }
            
        }
        
    }

}


// changes for Background Saving Emp Sheet Data on Start Repair Screen
//changes for employee sheet

-(NSArray*)fetchSubWorkOrderActualHrsFromDataBaseForEmployeeTimeSheet :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
    
    NSEntityDescription *entityMechanicalSubWorkOrderActualHrs;
    NSFetchRequest *requestSubWorkOrderActualHrs;
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs;
    NSArray *sortDescriptorsSubWorkOrderActualHrs;
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strID=[defs valueForKey:@"subWOActualHourId"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strID];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrActualHours = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if (error) {
        
        
    } else {
        
        
    }
    
    return arrActualHours;
    
}

-(NSMutableArray*)createEmpSheetDataInBackground :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal :(NSArray*)arrOfHoursConfig :(NSString*)strEmpID :(NSString*)strEmployeeNoLoggedIn :(NSString*)strEmpName :(NSString*)strFromWhere :(NSString*)strCurrentTime{
    
    NSMutableArray *arrOfTimeSlotsEmployeeWise,*arrOfHeaderTitleForSlots,*arrOfEmployeesAssignedInSubWorkOrder,*arrOfWorkingDate;
    
    // change for Employee Time Sheet
    
    NSArray *arrActualHoursTemp=[self fetchSubWorkOrderActualHrsFromDataBaseForEmployeeTimeSheet :strWorkOrderId :strSubWorkOrderIdGlobal];
    
    if (arrActualHoursTemp.count>0) {
        
        NSManagedObject *objTemp=arrActualHoursTemp[0];
        
        arrOfTimeSlotsEmployeeWise=[[NSMutableArray alloc]init];
        
        NSString *strCurrentDateToCheck=[self strCurrentDateFormattedForMechanical];
        
        if ([strFromWhere isEqualToString:@"StartRepairBackground"]) {
            
            strCurrentDateToCheck=strCurrentTime;
            
        } else if ([strFromWhere isEqualToString:@"Background"]){
            
            strCurrentDateToCheck=strCurrentTime;
            
        }
        
        NSArray *arrOfSlots = [self employeeTimeSheetSlotWiseTimeEntry:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"mobileTimeIn"]] :strCurrentDateToCheck :arrOfHoursConfig :false :strWorkOrderId :strSubWorkOrderIdGlobal :[objTemp valueForKey:@"subWOActualHourId"]];
        
        [arrOfTimeSlotsEmployeeWise addObjectsFromArray:arrOfSlots];
        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrOfTimeSlotsEmployeeWise])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrOfTimeSlotsEmployeeWise options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"BackGround On Start Repair Employee Time Sheet Array Json object: %@", jsonString);
            }
        }
        
        
        // Getting header for Slots
        
        arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
        arrOfEmployeesAssignedInSubWorkOrder=[[NSMutableArray alloc]init];
        
        /*
        [arrOfHeaderTitleForSlots addObject:@"Standard"];
        
        if (arrOfHoursConfig.count>0) {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                
                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                [arrOfHeaderTitleForSlots addObject:strTitle];
            }
            
            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                
                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                
                NSString *strSequenceNo =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"SequenceNo"]];
                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                int sequenceNo=[strSequenceNo intValue];
                
                if (arrOfHeaderTitleForSlots.count>sequenceNo) {
                    
                    [arrOfHeaderTitleForSlots replaceObjectAtIndex:sequenceNo withObject:strTitle];
                    
                }
            }

        }
        [arrOfHeaderTitleForSlots addObject:@"Holiday"];
        */
        
        [arrOfHeaderTitleForSlots addObject:@"Standard"];
        NSMutableArray *arrOfSequenceNo=[[NSMutableArray alloc]init];

        if (arrOfHoursConfig.count>0) {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                
                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                // NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                
                NSString *strTitle =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                NSString *strSequenceNo =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"SequenceNo"]];
                
                [arrOfHeaderTitleForSlots addObject:strTitle];
                [arrOfSequenceNo addObject:strSequenceNo];
                
            }
            
        }
        
        [arrOfHeaderTitleForSlots addObject:@"Holiday"];
        
        NSArray *arrTempp = [self arrayInAscendingOrder:arrOfSequenceNo];
        
        arrOfSequenceNo = nil;
        arrOfSequenceNo = [[NSMutableArray alloc]init];
        
        [arrOfSequenceNo addObjectsFromArray:arrTempp]; // 205 510 1001
        
        
        if (arrOfHoursConfig.count>0) {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
            for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                
                NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                
                NSString *strSequenceNo =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"SequenceNo"]];
                NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                //NSString *strTitle =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                
                //int sequenceNo=[strSequenceNo intValue];
                
                int indexxContains = (int)[arrOfSequenceNo indexOfObject:strSequenceNo];
                
                //if (arrOfHeaderTitleForSlots.count>sequenceNo) {
                
                [arrOfHeaderTitleForSlots replaceObjectAtIndex:indexxContains+1 withObject:strTitle];
                
                //}
            }
        }

        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        
        NSDictionary *dictEmployeeList=[defsLogindDetail valueForKey:@"EmployeeList"];
        
        if ([dictEmployeeList isKindOfClass:[NSString class]]) {
            
            
        } else {
            
            if (!(dictEmployeeList.count==0)) {
                NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
                for (int k=0; k<arrOfEmployee.count; k++) {
                    NSDictionary *dictDatata=arrOfEmployee[k];
                    long long  longEmployeeId=[[dictDatata valueForKey:@"EmployeeId"] longLongValue];
                    long long  longEmployeeIdToCheck=[strEmpID longLongValue];
                    if (longEmployeeId==longEmployeeIdToCheck) {
                        [arrOfEmployeesAssignedInSubWorkOrder addObject:dictDatata];
                    }
                }
            }
            
        }
        
        NSArray *arrOfHelper=[self fetchSubWorkOrderHelperFromDataBaseForSlotWiseTimeEntry:strWorkOrderId :strSubWorkOrderIdGlobal];
        
        NSMutableArray *arrOfEmpIdss=[[NSMutableArray alloc]init];
        
        for (int k2=0; k2<arrOfHelper.count; k2++) {
            
            NSDictionary *dictDataHelper=arrOfHelper[k2];
            
            [arrOfEmpIdss addObject:[NSString stringWithFormat:@"%@",[dictDataHelper valueForKey:@"employeeNo"]]];
            
        }
        
        if ([dictEmployeeList isKindOfClass:[NSString class]]) {
            
            
        } else {
            
            if (!(dictEmployeeList.count==0)) {
                NSArray *arrOfEmployee=(NSArray*)dictEmployeeList;
                for (int k=0; k<arrOfEmployee.count; k++) {
                    NSDictionary *dictDatata=arrOfEmployee[k];
                    NSString *strEmpIdsss=[NSString stringWithFormat:@"%@",[dictDatata valueForKey:@"EmployeeNo"]];
                    if ([arrOfEmpIdss containsObject:strEmpIdsss]) {
                        [arrOfEmployeesAssignedInSubWorkOrder addObject:dictDatata];
                    }
                }
            }
        }
        
    }
    
    
    // Logic to fetch Unique Working Date in Employee SLot Table
    
    arrOfWorkingDate=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrWorkingDateTemp=[[NSMutableArray alloc]init];
    
    for (int k2=0; k2<arrOfTimeSlotsEmployeeWise.count; k2++) {
        
        NSDictionary *dictDataTemp=arrOfTimeSlotsEmployeeWise[k2];
        
        NSString *strWorkingDate=[NSString stringWithFormat:@"%@",[dictDataTemp valueForKey:@"workingDate"]];
        
        if (![arrWorkingDateTemp containsObject:strWorkingDate]) {
            
            [arrOfWorkingDate addObject:dictDataTemp];
            [arrWorkingDateTemp addObject:strWorkingDate];
        }
        
    }
    
    NSMutableArray *arrOfMainTempEmployeeTimeSlot=[[NSMutableArray alloc]init];
    
    // Logic for creating final Array list for Employee Time Slot Wise
    
    for (int q=0; q<arrOfEmployeesAssignedInSubWorkOrder.count; q++) {
        
        
        for (int q1=0; q1<arrOfWorkingDate.count; q1++) {
            
            NSMutableDictionary *dictOfMainObj=[[NSMutableDictionary alloc]init];
            
            NSDictionary *dictObjEmp=arrOfEmployeesAssignedInSubWorkOrder[q];
            
            NSString *strEmployeeNoInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"EmployeeNo"]];
            NSString *strEmployeeNameInsideLoop=[NSString stringWithFormat:@"%@",[dictObjEmp valueForKey:@"FullName"]];
            
            [dictOfMainObj setValue:strEmployeeNoInsideLoop forKey:@"EmployeeNo"];
            [dictOfMainObj setValue:strEmployeeNameInsideLoop forKey:@"EmployeeName"];
            
            // Change for Labor Type
            
            if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoLoggedIn]) {
                
                BOOL isMechanicLocal = [self isMechanicTypeForLoggedInUser:strWorkOrderId :strSubWorkOrderIdGlobal];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"No" forKey:@"Helper"];
                
                
            } else {
                
                BOOL isMechanicLocal = [self isMechanicType:strWorkOrderId :strSubWorkOrderIdGlobal :strEmployeeNoInsideLoop];
                
                if (isMechanicLocal) {
                    
                    [dictOfMainObj setValue:@"No" forKey:@"Helper"];
                    
                } else {
                    
                    [dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                    
                }
                
                //[dictOfMainObj setValue:@"Yes" forKey:@"Helper"];
                
            }
            
            NSMutableArray *arrOfTempEmployeeListFiltered=[[NSMutableArray alloc]init];
            
            NSDictionary *dictObjWorkingDate=arrOfWorkingDate[q1];
            
            NSString *strSubWorkOrderActualHourIdInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"subWorkOrderActualHourId"]];
            NSString *strWorkingDateInsideLoop=[NSString stringWithFormat:@"%@",[dictObjWorkingDate valueForKey:@"workingDate"]];
            
            [dictOfMainObj setValue:strSubWorkOrderActualHourIdInsideLoop forKey:@"ActualHrId"];
            [dictOfMainObj setValue:strWorkingDateInsideLoop forKey:@"WorkingDate"];
            
            for (int q2=0; q2<arrOfHeaderTitleForSlots.count; q2++) {
                
                NSString *strHeaderTitleInsideLoop=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[q2]];
                
                NSMutableDictionary *dictTemoObjEmployeeWiseSlots=[[NSMutableDictionary alloc]init];
                
                for (int q3=0; q3<arrOfTimeSlotsEmployeeWise.count; q3++) {
                    
                    NSDictionary *dictObjTimeSlotsEmployeeWise=arrOfTimeSlotsEmployeeWise[q3];
                    
                    NSString *strEmployeeNoTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"employeeNo"]];
                    NSString *strWorkingDateTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingDate"]];
                    NSString *strActualHrIdTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"subWorkOrderActualHourId"]];
                    NSString *strWorkingTypeTimeSlotsEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"workingType"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlotTitle"]];
                    NSString *strtimeSlotTitleEmployeeWiseInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"timeSlot"]];

                    BOOL isMatchecd = [self isSlotMatched:strtimeSlotTitleEmployeeWiseInsideLoop :strtimeSlotTitleEmployeeWiseInsideLoopForLeaving1and3 :strHeaderTitleInsideLoop];
                    
                    
                    if ([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Standard"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"]) {
                        
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                        }
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && [strHeaderTitleInsideLoop isEqualToString:@"Holiday"] &&[strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                        }
                        
                    }else if([strEmployeeNoInsideLoop isEqualToString:strEmployeeNoTimeSlotsEmployeeWiseInsideLoop] && [strWorkingDateInsideLoop isEqualToString:strWorkingDateTimeSlotsEmployeeWiseInsideLoop] && [strSubWorkOrderActualHourIdInsideLoop isEqualToString:strActualHrIdTimeSlotsEmployeeWiseInsideLoop] && isMatchecd &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"1"] &&![strWorkingTypeTimeSlotsEmployeeWiseInsideLoop isEqualToString:@"3"]){
                        
                        if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                            
                            [dictTemoObjEmployeeWiseSlots addEntriesFromDictionary:dictObjTimeSlotsEmployeeWise];
                            
                        } else {
                            
                            NSString *strActualAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoop=[NSString stringWithFormat:@"%@",[dictObjTimeSlotsEmployeeWise valueForKey:@"billableDurationInMin"]];
                            
                            NSString *strActualAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualAmt"]];
                            NSString *strActualDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"actualDurationInMin"]];
                            NSString *strBillableAmtInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableAmt"]];
                            NSString *strBillableDurationInMinInsideLoopAlreadyPresent=[NSString stringWithFormat:@"%@",[dictTemoObjEmployeeWiseSlots valueForKey:@"billableDurationInMin"]];
                            
                            float actualAmtTotal=[strActualAmtInsideLoop floatValue] + [strActualAmtInsideLoopAlreadyPresent floatValue];
                            float actualDurationInMinTotal=[strActualDurationInMinInsideLoop floatValue] + [strActualDurationInMinInsideLoopAlreadyPresent floatValue];
                            float billableAmtTotal=[strBillableAmtInsideLoop floatValue] + [strBillableAmtInsideLoopAlreadyPresent floatValue];
                            float billableDurationInMinTotal=[strBillableDurationInMinInsideLoop floatValue] + [strBillableDurationInMinInsideLoopAlreadyPresent floatValue];
                            
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualAmtTotal] forKey:@"actualAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",actualDurationInMinTotal] forKey:@"actualDurationInMin"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableAmtTotal] forKey:@"billableAmt"];
                            [dictTemoObjEmployeeWiseSlots setObject:[NSString stringWithFormat:@"%f",billableDurationInMinTotal] forKey:@"billableDurationInMin"];
                            
                        }
                        
                    }
                    
                }
                
                // if dictTemoObjEmployeeWiseSlots is null to apna pura data banaunga mai
                
                
                if ([strFromWhere isEqualToString:@"StartRepairBackground"]) {
                    
                    
                } else if ([strFromWhere isEqualToString:@"Background"]){
                    
                    
                } else{
                    
                    if ([dictTemoObjEmployeeWiseSlots count] < 1) {
                        
                        NSString *strDate =[self strCurrentDate];
                        
                        [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"actualAmt"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"actualDurationInMin"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"0.0" forKey:@"billableAmt"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"0" forKey:@"billableDurationInMin"];
                        [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"createdBy"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"Mobile" forKey:@"createdByDevice"];
                        [dictTemoObjEmployeeWiseSlots setObject:strDate forKey:@"createdDate"];
                        //[dictTemoObjEmployeeWiseSlots setObject:strEmployeeNameInsideLoop forKey:@"employeeName"];
                        [dictTemoObjEmployeeWiseSlots setObject:strEmployeeNoInsideLoop forKey:@"employeeNo"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isActive"];
                        [dictTemoObjEmployeeWiseSlots setObject:@"true" forKey:@"isApprove"];
                        [dictTemoObjEmployeeWiseSlots setObject:strEmpName forKey:@"modifiedBy"];
                        [dictTemoObjEmployeeWiseSlots setObject:strDate forKey:@"modifiedDate"];
                        [dictTemoObjEmployeeWiseSlots setObject:[self getReferenceNumber] forKey:@"subWOEmployeeWorkingTimeId"];
                        [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderActualHourIdInsideLoop forKey:@"subWorkOrderActualHourId"];
                        [dictTemoObjEmployeeWiseSlots setObject:strSubWorkOrderIdGlobal forKey:@"subWorkOrderId"];
                        [dictTemoObjEmployeeWiseSlots setObject:strWorkingDateInsideLoop forKey:@"workingDate"];
                        [dictTemoObjEmployeeWiseSlots setObject:strWorkOrderId forKey:@"workorderId"];
                        
                        if ([strHeaderTitleInsideLoop isEqualToString:@"Standard"]) {
                            
                            [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                            [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                            [dictTemoObjEmployeeWiseSlots setObject:@"1" forKey:@"workingType"];
                            
                        } else if ([strHeaderTitleInsideLoop isEqualToString:@"Holiday"]){
                            
                            [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlot"];
                            [dictTemoObjEmployeeWiseSlots setObject:@"" forKey:@"timeSlotTitle"];
                            [dictTemoObjEmployeeWiseSlots setObject:@"3" forKey:@"workingType"];
                            
                        } else{
                            
                            NSString *strSlotsToSet,*strTitleSlotToSet;
                            
                            if (arrOfHoursConfig.count>0) {
                                
                                NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
                                
                                NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
                                
                                for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
                                    
                                    NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
                                    NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                                    
                                    NSString *strTimeSlotsNew =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                    
                                    BOOL isMatched = [self isSlotMatched:strTitle :strTimeSlotsNew :strHeaderTitleInsideLoop];

                                    if (isMatched) {
                                        
                                        strSlotsToSet=[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
                                        strTitleSlotToSet = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];

                                        break;
                                    }
                                }
                            }
                            
                            [dictTemoObjEmployeeWiseSlots setObject:strSlotsToSet forKey:@"timeSlot"];
                            [dictTemoObjEmployeeWiseSlots setObject:strTitleSlotToSet forKey:@"timeSlotTitle"];
                            [dictTemoObjEmployeeWiseSlots setObject:@"2" forKey:@"workingType"];
                            
                        }
                        
                    }

                }
                
                if (dictTemoObjEmployeeWiseSlots.count>1) {
                    
                    [arrOfTempEmployeeListFiltered addObject:dictTemoObjEmployeeWiseSlots];
                    
                }
                
            }
            
            
            [dictOfMainObj setValue:arrOfTempEmployeeListFiltered forKey:@"EmployeeList"];
            
            [arrOfMainTempEmployeeTimeSlot addObject:dictOfMainObj];
            
        }
    }
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:arrOfMainTempEmployeeTimeSlot])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:arrOfMainTempEmployeeTimeSlot options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"BackGround On Start Repair Employee Time Sheet arrOfMainTempEmployeeTimeSlot Json object final to set on view: %@", jsonString);
        }
    }
    // fixing 22 jan
    if ([strFromWhere isEqualToString:@"StartRepairBackground"]) {
        
        [self saveEmployeeTimeSheetSlotWiseTempTable:arrOfMainTempEmployeeTimeSlot];

    } else if ([strFromWhere isEqualToString:@"Background"]){
        
        [self saveEmployeeTimeSheetSlotWise:arrOfMainTempEmployeeTimeSlot];

    } else{
        
    }
    
    return arrOfMainTempEmployeeTimeSlot;
    
}

-(void)saveEmployeeTimeSheetSlotWiseTempTable :(NSArray*)arrEmpSlots{
    
    for (int k=0; k<arrEmpSlots.count; k++) {
        
        NSDictionary *dictData=arrEmpSlots[k];
        
        NSArray *arrTempp=[dictData valueForKey:@"EmployeeList"];
        
        for (int i=0; i<arrTempp.count; i++) {
            
            NSDictionary *dictDataaa=arrTempp[i];
            
            NSString *strBillableAmt=[dictDataaa valueForKey:@"billableAmt"];
            NSString *strActualAmt=[dictDataaa valueForKey:@"actualAmt"];
            NSString *strBillableDuration=[dictDataaa valueForKey:@"billableDurationInMin"];
            NSString *strActualDuration=[dictDataaa valueForKey:@"actualDurationInMin"];

            float BillAmount=[strBillableAmt floatValue];
            float ActualAmount=[strActualAmt floatValue];
            float BillDuration=[strBillableDuration floatValue];
            float ActualDuration=[strActualDuration floatValue];

            if ((BillAmount>0) || (ActualAmount>0) || (BillDuration>0) || (ActualDuration>0)) {
                
                entitySubWoEmployeeWorkingTimeExtSerDcsTemp=[NSEntityDescription entityForName:@"TempSubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                
                TempSubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[TempSubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcsTemp insertIntoManagedObjectContext:context];
                
                
                NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrTempp[i];
                
                objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWOEmployeeWorkingTimeId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workorderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"subWorkOrderActualHourId"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeNo"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"employeeName"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlotTitle"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"timeSlot"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualDurationInMin"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"actualAmt"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableDurationInMin"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"billableAmt"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"workingType"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isApprove"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"isActive"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedDate"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"modifiedBy"]];
                objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"createdByDevice"]];
                
                // Change for Labor Type
                
                if ([[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"Helper"]] isEqualToString:@"Yes"]) {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"True";
                    
                } else {
                    
                    objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=@"False";
                    
                }
                
                NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
                
            }
            
        }
        
    }
    
}


-(NSMutableArray*)arrOfHeaderTitleGlobal :(NSMutableArray*)arrOfHoursConfig{
    
    /*
    NSMutableArray *arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfGlobalDynamicEmpSheetFinal.count; k++) {
        
        NSManagedObject *objTemp=arrOfGlobalDynamicEmpSheetFinal[k];
        
        NSString *strworkingType=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"workingType"]];
        NSString *strTitleSlotsComing=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"timeSlotTitle"]];
        
        NSString *strTitleToSet;
        if ([strworkingType isEqualToString:@"1"]) {
            strTitleToSet=@"Standard";
        } else if ([strworkingType isEqualToString:@"2"]){
            strTitleToSet=strTitleSlotsComing;
        } else if ([strworkingType isEqualToString:@"3"]){
            strTitleToSet=@"Holiday";
        }
        
        if(strTitleToSet.length==0){
            
            strTitleToSet=@"N/A";
            
        }
        
        if (![arrOfHeaderTitleForSlots containsObject:strTitleToSet]) {
            
            [arrOfHeaderTitleForSlots addObject:strTitleToSet];
            
        }
        
    }

     */
    
    // Getting header for Slots
    NSMutableArray *arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfSequenceNo=[[NSMutableArray alloc]init];

    arrOfHeaderTitleForSlots=[[NSMutableArray alloc]init];
    
    [arrOfHeaderTitleForSlots addObject:@"Standard"];
    
    if (arrOfHoursConfig.count>0) {
        
        NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
        
        NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
        
        for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
            
            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
           // NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];

            NSString *strTitle =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
            NSString *strSequenceNo =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"SequenceNo"]];

            [arrOfHeaderTitleForSlots addObject:strTitle];
            [arrOfSequenceNo addObject:strSequenceNo];
            
        }
        
    }
    
    [arrOfHeaderTitleForSlots addObject:@"Holiday"];

    NSArray *arrTempp = [self arrayInAscendingOrder:arrOfSequenceNo];
    
    arrOfSequenceNo = nil;
    arrOfSequenceNo = [[NSMutableArray alloc]init];
    
    [arrOfSequenceNo addObjectsFromArray:arrTempp]; // 205 510 1001
    
    
    if (arrOfHoursConfig.count>0) {
        
        NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
        
        NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
        
        for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
            
            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
            
            NSString *strSequenceNo =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"SequenceNo"]];
            //NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
            NSString *strTitle =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
            
            //int sequenceNo=[strSequenceNo intValue];
            
            int indexxContains = (int)[arrOfSequenceNo indexOfObject:strSequenceNo];
            
            //if (arrOfHeaderTitleForSlots.count>sequenceNo) {
                
                [arrOfHeaderTitleForSlots replaceObjectAtIndex:indexxContains+1 withObject:strTitle];
                
            //}
        }
    }

    return arrOfHeaderTitleForSlots;
    
}

-(NSArray*)arrayInAscendingOrder :(NSArray*)arrSequenceNo{
    
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self"
                                                                ascending: YES];
    return [arrSequenceNo sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    
}

-(NSString*)slotHeaderTitleFromTimeSlot :(NSArray*)arrOfHoursConfig :(NSString*)strSlots{
    
    NSString *strTitleGlobal;
    
    if (arrOfHoursConfig.count>0) {
        
        NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
        
        NSArray *arrOfAfterHourRates=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
        
        for (int k1=0; k1<arrOfAfterHourRates.count; k1++) {
            
            NSDictionary *dictDataAfterHoursRate=arrOfAfterHourRates[k1];
            // NSString *strTitle =[NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
            
            NSString *strTitle =[NSString stringWithFormat:@"%@ - %@",[dictDataAfterHoursRate valueForKey:@"FromTime"],[dictDataAfterHoursRate valueForKey:@"ToTime"]];
            
            if ([strSlots isEqualToString:strTitle]) {
                
                strTitleGlobal = [NSString stringWithFormat:@"%@",[dictDataAfterHoursRate valueForKey:@"Title"]];
                
                if (strTitleGlobal.length==0) {
                    
                    strTitleGlobal = strTitle;
                    
                }
                
                break;
            }
            
        }
        
    }
    
    return strTitleGlobal;
    
}


-(UIView*)createDynamicEmpSheet :(NSArray*)arrOfGlobalDynamicEmpSheetFinal :(NSArray*)arrOfHeaderTitleForSlots :(UIScrollView*)scrollViewEmpTimeSheet{
    
    //    arrOfGlobalDynamicEmpSheetFinal=[[NSMutableArray alloc]init];
    //    [arrOfGlobalDynamicEmpSheetFinal addObjectsFromArray:arrOfEmpSheet];
    
    CGFloat scrollViewHeight=0.0;
    CGFloat scrollViewWidth=0.0;
    
    scrollViewHeight=arrOfGlobalDynamicEmpSheetFinal.count*60+60;
    scrollViewWidth=arrOfHeaderTitleForSlots.count*100;
    
    //scrollViewEmpTimeSheet=[[UIScrollView alloc]initWithFrame:CGRectMake(scrollViewEmpTimeSheet.frame.origin.x, scrollViewEmpTimeSheet.frame.origin.y, scrollViewWidth, scrollViewHeight)];
    
    UIView *viewEmpSheetOnScroll=[[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollViewEmpTimeSheet.frame.size.width, scrollViewEmpTimeSheet.frame.size.height)];
    
    CGFloat xAxisHeaderTitle=210;
    
    for (int i=0; i<arrOfHeaderTitleForSlots.count; i++) {
        
        UILabel *lblTitleHeader=[[UILabel alloc]init];
        lblTitleHeader.backgroundColor=[UIColor clearColor];
        lblTitleHeader.layer.borderWidth = 1.5f;
        lblTitleHeader.layer.cornerRadius = 4.0f;
        lblTitleHeader.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleHeader.frame=CGRectMake(xAxisHeaderTitle, 0, 100, 50);
        lblTitleHeader.font=[UIFont systemFontOfSize:22];
        
        if (i==0) {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",@"Regular"];
            
        } else {
            
            lblTitleHeader.text=[NSString stringWithFormat:@"%@",arrOfHeaderTitleForSlots[i]];
            
        }
        
        lblTitleHeader.textAlignment=NSTextAlignmentCenter;
        lblTitleHeader.numberOfLines=700;
        [lblTitleHeader setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleHeader];
        xAxisHeaderTitle=xAxisHeaderTitle+100;
        
    }
    
    CGFloat yAxis=60;
    
    for (int k=0; k<arrOfGlobalDynamicEmpSheetFinal.count; k++) {
        
        CGFloat xAxis=210;
        
        NSDictionary *dictDataEmpSheet=arrOfGlobalDynamicEmpSheetFinal[k];
        
        UILabel *lblTitleEmpName=[[UILabel alloc]init];
        lblTitleEmpName.backgroundColor=[UIColor clearColor];
        lblTitleEmpName.layer.borderWidth = 1.5f;
        lblTitleEmpName.layer.cornerRadius = 4.0f;
        lblTitleEmpName.layer.borderColor = [[UIColor grayColor] CGColor];
        lblTitleEmpName.frame=CGRectMake(10, yAxis, 200, 50);
        lblTitleEmpName.font=[UIFont systemFontOfSize:22];
        lblTitleEmpName.text=[NSString stringWithFormat:@"%@",[dictDataEmpSheet valueForKey:@"EmployeeName"]];
        lblTitleEmpName.textAlignment=NSTextAlignmentCenter;
        lblTitleEmpName.numberOfLines=700;
        [lblTitleEmpName setAdjustsFontSizeToFitWidth:YES];
        
        [viewEmpSheetOnScroll addSubview:lblTitleEmpName];
        
        
        NSArray *arrEmpData=[dictDataEmpSheet valueForKey:@"EmployeeList"];
        
        for (int k1=0; k1<arrEmpData.count; k1++) {
            
            NSDictionary *dictDataaaa=arrEmpData[k1];
            CGRect txtViewFrame = CGRectMake(xAxis, yAxis,100,50);
            UITextField *txtView= [[UITextField alloc] init];// billableDurationInMin
            txtView.text = [self getHrAndMinsGlobal:[dictDataaaa valueForKey:@"actualDurationInMin"]];
            txtView.text = [self getHrAndMinsGlobal:[dictDataaaa valueForKey:@"billableDurationInMin"]];
            txtView.frame =txtViewFrame;
            txtView.layer.borderWidth = 1.5f;
            txtView.layer.cornerRadius = 4.0f;
            txtView.layer.borderColor = [[UIColor grayColor] CGColor];
            [txtView setBackgroundColor:[UIColor clearColor]];
            txtView.font=[UIFont systemFontOfSize:20];
            txtView.delegate=self;
            
            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            leftView.backgroundColor = [UIColor clearColor];
            txtView.leftView = leftView;
            txtView.leftViewMode = UITextFieldViewModeAlways;
            txtView.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            
            txtView.tag=k*1000+k1+500;
            
            [viewEmpSheetOnScroll addSubview:txtView];
            
            xAxis=xAxis+100;
            
        }
        
        yAxis=yAxis+60;
        
    }
    
    return viewEmpSheetOnScroll;
    
}

-(NSString*)getHrAndMinsGlobal:(NSString*)strMins
{
    int seconds = (int)[strMins integerValue]*60;
    int minutes = (seconds / 60) % 60;
    int hours = seconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d",hours,minutes];
    
}

-(NSString*)convertIntoJson :(NSDictionary*)dictData :(NSArray*)arrData :(NSString*)strType :(NSString*)strLogMsg{
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    
    if ([strType isEqualToString:@"array"]) {
        if ([NSJSONSerialization isValidJSONObject:arrData])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrData options:NSJSONWritingPrettyPrinted error:&errorNew];
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            }
        }
    } else {
        if ([NSJSONSerialization isValidJSONObject:dictData])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:dictData options:NSJSONWritingPrettyPrinted error:&errorNew];
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            }
        }
    }
    
    NSLog(@"json data being converted for %@=======%@",strLogMsg,jsonString);
    
    return jsonString;
}

-(CGFloat)globalWidthDevice{
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width-30;
    
    return width;
    
}


// changes for copying sub work order

-(void)fetchSubWorkOrderFromDBToCopy :(NSString*)strWorkOrderIdToCopy :(NSString*)strSubWorkOrderIdToCopy{
    
    dictFinal = [[NSMutableDictionary alloc]init];
    
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToCopy];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictFinal setObject:arrEmailDetail forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderDetails=[[NSMutableDictionary alloc]init];
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            NSManagedObject *objTempSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            //subWorkOrderId
            
            //strWorkOrderType
            strWorkOrderType=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"subWOType"]];
            
            NSString *strStatus=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"subWOStatus"]];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrder entity] attributesByName] allKeys];
            
            NSString *strCustomerSign;//=[objTempSubWorkOrder valueForKey:@"CustomerSignaturePath"];
            NSString *strTechSign;//=[objTempSubWorkOrder valueForKey:@"TechnicianSignaturePath"];
            
            BOOL isCompletedStatusMechanical=[self isCompletedSatusMechanical:strStatus];
            
            
            if (isCompletedStatusMechanical) {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"completeSWO_CustSignPath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"completeSWO_TechSignPath"];
                
            } else {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"customerSignaturePath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"technicianSignaturePath"];
                
            }
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            dictSubWorkOrderDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchSubWorkOrderIssuesFromDB:[objTempSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            [arrOfSubWorkOrderDb addObject:dictSubWorkOrderDetails];
        }
        
        [dictFinal setObject:arrOfSubWorkOrderDb forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}


-(void)fetchSubWorkOrderIssuesFromDB :(NSString*)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderIssueDcs"];
        
        
    }
    else
    {
        
        arrOfSubWorkOrderIssuesDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssues entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            if ([strWorkOrderType isEqualToString:@"TM"]) {
                
                [dictSubWorkOrderIssuesDetails setObject: [dictSubWorkOrderIssuesDetails objectForKey: @"subWOIssueRepairPartDcs"] forKey: @"SubWOIssueRepairDcs"];
                
                [dictSubWorkOrderIssuesDetails removeObjectForKey: @"subWOIssueRepairPartDcs"];
                
                [self fetchSubWorkOrderIssuesPartsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            } else {
                
                [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            }
            
            //Yaha Par Db Se Fetch Karna Issues k Repairs
            //  [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
            
            [arrOfSubWorkOrderIssuesDb addObject:dictSubWorkOrderIssuesDetails];
        }
        
        // Yaha Par nootes helper hrs sab set honge
        [dictSubWorkOrderDetails setObject:arrOfSubWorkOrderIssuesDb forKey:@"SubWorkOrderIssueDcs"];
        
        [self fetchSubWorkOrderHelperFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWorkOrderNotesFromDB :strSubWorkOrderIdToFetch];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@ && subWorkOrderIssueId = %@",strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderIssuesRepairDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesRepairDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepair entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesRepairDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchSubWorkOrderIssuesRepairPartsFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [self fetchSubWorkOrderIssuesRepairLabourFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [arrOfSubWorkOrderIssuesRepairDb addObject:dictSubWorkOrderIssuesRepairDetails];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfSubWorkOrderIssuesRepairDb forKey:@"SubWOIssueRepairDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@ && subWorkOrderIssueId = %@",strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@ && issueRepairId = %@",strSubWorkOrderIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    else
    {
        NSMutableArray *arrOfLaborTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairLabor=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairLabor entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempLabor = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfLaborTemp addObject:dictTempLabor];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfLaborTemp forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderHelperFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWOTechHelperDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
            
            NSManagedObject *objTempHelper=arrAllObjSubWorkOrderHelper[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempHelper entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempHelper = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempHelper addObject:dictTempHelper];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempHelper forKey:@"SubWOTechHelperDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderNotesFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    requestSubWorkOrderNotes = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderNotes setEntity:entityMechanicalSubWorkOrderNotes];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderNotes setPredicate:predicate];
    
    sortDescriptorSubWorkOrderNotes = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsSubWorkOrderNotes = [NSArray arrayWithObject:sortDescriptorSubWorkOrderNotes];
    
    [requestSubWorkOrderNotes setSortDescriptors:sortDescriptorsSubWorkOrderNotes];
    
    self.fetchedResultsControllerSubWorkOrderNotes = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderNotes managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderNotes setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderNotes performFetch:&error];
    arrAllObjSubWorkOrderNotes = [self.fetchedResultsControllerSubWorkOrderNotes fetchedObjects];
    if ([arrAllObjSubWorkOrderNotes count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderNoteDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderNotes.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWorkOrderNotes[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempNotes addObject:dictTempNotes];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"SubWorkOrderNoteDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)copySubWorkOrder :(NSString*)strServiceUrlMainServiceAutomation :(NSString*)strCompanyKey :(NSString*)strIdWorkOrder :(NSString*)strIdSubWorkOrder :(NSString*)strLoggedInUserName{
    
    [self fetchSubWorkOrderFromDBToCopy:strIdWorkOrder :strIdSubWorkOrder];
    
    [dictSubWorkOrderDetails setObject:@"" forKey:@"subWorkOrderId"];
    [dictSubWorkOrderDetails setObject:@"" forKey:@"subWorkOrderNo"];
    [dictSubWorkOrderDetails setObject:strCompanyKey forKey:@"companyKey"];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictSubWorkOrderDetails])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictSubWorkOrderDetails options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Copy Sub Work Order Json: %@", jsonString);
        }
    }

    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

   // NSString *strUrl = [NSString stringWithFormat:@"%@%@companyKey=%@&subWorkOrderId=%@&loginUserName=%@",strServiceUrlMainServiceAutomation,UrlCopySubWorkOrder,strCompanyKey,strIdSubWorkOrder,strLoggedInUserName];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSubmitSubWorkOrder];

    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];

    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
             NSDictionary *ResponseDictLocal;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDictLocal = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDictLocal==nil) || ResponseDictLocal.count==0) {
                 
                 [self AlertMethod:Alert :unableCopySWO];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalCopySubWorkOrder" object:self];

                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDictLocal forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDictLocal =dictData;
                 
//                 NSString *strException;
//                 @try {
//                     strException=[ResponseDictLocal valueForKey:@"ExceptionMessage"];
//                 } @catch (NSException *exception) {
//
//                 } @finally {
//
//                 }
//                 if (![strException isKindOfClass:[NSString class]]) {
//
//                     strException=@"";
//
//                 }
//
//                 if (strException.length==0) {
                 
                     NSString *strStatus=[NSString stringWithFormat:@"%@",[ResponseDictLocal valueForKey:@"status"]];
                     NSString *strMsgResponse=[NSString stringWithFormat:@"%@",[ResponseDictLocal valueForKey:@"Message"]];
                     
                     if ([strStatus isEqualToString:@"1"] || [strStatus caseInsensitiveCompare:@"true"] == NSOrderedSame) {
                         
                         NSDictionary *dictSubWorKorder=[ResponseDictLocal valueForKey:@"SubWorkOrderExtSerDc"];
                         
                         NSArray *arrSubWorKorderIssues=[dictSubWorKorder valueForKey:@"SubWorkOrderIssueDcs"];
                         
                         NSArray *arrSubWorkOrderHelper=[dictSubWorKorder valueForKey:@"SubWOTechHelperDcs"];
                         
                         NSArray *arrSubWorkOrderNotes=[dictSubWorKorder valueForKey:@"SubWorkOrderNoteDcs"];

                         [self saveSubWorkOrderInDb :dictSubWorKorder :arrSubWorKorderIssues :arrSubWorkOrderHelper : arrSubWorkOrderNotes :strIdWorkOrder :strCompanyKey];
                         
                         [self AlertMethod:Alert :@"Successfully copied"];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalCopySubWorkOrder" object:self];

                         
//                     } else {
//
//                         //[DejalBezelActivityView removeView];
//
//                         [self AlertMethod:Alert :strMsgResponse];
//
//                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalCopySubWorkOrder" object:self];
//
//
//                     }
                     
                 } else {
                     
                     ResponseDictLocal=nil;
                     [self AlertMethod:Alert :unableCopySWO];
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalCopySubWorkOrder" object:self];

                 }
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        [self AlertMethod:Alert :Sorry];
        //[DejalBezelActivityView removeView];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MechanicalCopySubWorkOrder" object:self];

    }
    @finally {
    }
    
}


-(void)saveSubWorkOrderInDb :(NSDictionary*)dictOfSubWorkOrderInfo :(NSArray*)arrOfSubWorkOrderIssuesInfo :(NSArray*)arrOfSubWorkOrderHelper :(NSArray*)arrOfSubWorkOrderNotes :(NSString*)strWorkOrderId :(NSString*)strCompanyKey{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    
    MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
    
    objSubWorkOrder.workorderId=strWorkOrderId;
    
    NSManagedObject *objWorkOrder=[self fetchMechanicalWorkOrderObj:strWorkOrderId];
    
    objSubWorkOrder.departmentSysName =[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"departmentSysName"]];
    objSubWorkOrder.companyKey=strCompanyKey;
    objSubWorkOrder.accountNo=[NSString stringWithFormat:@"%@",[objWorkOrder valueForKey:@"accountNo"]];;
    objSubWorkOrder.actualTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTime"]];
    objSubWorkOrder.amtPaid=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AmtPaid"]];
    objSubWorkOrder.companyKey=strCompanyKey;
    objSubWorkOrder.completeSWO_CustSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_CustSignPath"]];
    objSubWorkOrder.completeSWO_IsCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_IsCustomerNotPresent"]];
    objSubWorkOrder.completeSWO_TechSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_TechSignPath"]];
    objSubWorkOrder.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrder.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrder.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CustomerSignaturePath"]];
    objSubWorkOrder.diagnosticChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiagnosticChargeMasterId"]];
    objSubWorkOrder.discountAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiscountAmt"]];
    objSubWorkOrder.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"EmployeeNo"]];
    objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
    objSubWorkOrder.invoicePayType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoicePayType"]];
    objSubWorkOrder.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsActive"]]];
    objSubWorkOrder.isActive=@"true";
    objSubWorkOrder.isClientApprovalReq=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApprovalReq"]]];
    objSubWorkOrder.isClientApproved=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApproved"]]];
    objSubWorkOrder.isCompleted=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCompleted"]]];
    objSubWorkOrder.isCustomerNotPresent=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCustomerNotPresent"]]];
    objSubWorkOrder.isStdPrice=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsStdPrice"]]];
    objSubWorkOrder.jobDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"JobDesc"]];
    objSubWorkOrder.laborPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborPercent"]];
    objSubWorkOrder.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrder.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrder.otherDiagnosticChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherDiagnosticChargesAmt"]];
    objSubWorkOrder.otherTripChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherTripChargesAmt"]];
    objSubWorkOrder.partPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPercent"]];
    objSubWorkOrder.priceNotToExceed=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PriceNotToExceed"]];
    objSubWorkOrder.serviceDateTime=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceDateTime"]]];
    objSubWorkOrder.serviceStartEndTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartEndTime"]];
    objSubWorkOrder.serviceStartStartTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartStartTime"]];
    objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
    
    objSubWorkOrder.subWorkOrderIssueId=@"";
    objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
    objSubWorkOrder.subWOStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOStatus"]];
    objSubWorkOrder.subWOType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
    objSubWorkOrder.taxAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TaxAmt"]];
    objSubWorkOrder.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TechnicianSignaturePath"]];
    objSubWorkOrder.totalApprovedAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalApprovedAmt"]];
    objSubWorkOrder.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTime"]];
    objSubWorkOrder.totalEstimationTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTimeInt"]];
    objSubWorkOrder.tripChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargeMasterId"]];
    objSubWorkOrder.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedByDevice"]];
    
    objSubWorkOrder.pSPDiscount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPDiscount"]];
    
    objSubWorkOrder.pSPCharges=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPCharges"]];
    
    objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
    
    objSubWorkOrder.accountPSPId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AccountPSPId"]];
    
    objSubWorkOrder.pSPMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPMasterId"]];
    
    objSubWorkOrder.inspectionresults=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"Inspectionresults"]];
    objSubWorkOrder.propertyType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PropertyType"]];
    
    objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
    objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
    objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
    objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
    objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
    objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
    objSubWorkOrder.afterHrsDuration=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AfterHrsDuration"]];
    objSubWorkOrder.isHolidayHrs=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsHolidayHrs"]];
    objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
    objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
    objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
    objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
    objSubWorkOrder.miscChargeAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MiscChargeAmt"]];
    objSubWorkOrder.actualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTimeInt"]];
    
    objSubWorkOrder.isCreateWOforInCompleteIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCreateWOforInCompleteIssue"]];
    objSubWorkOrder.isSendDocWithOutSign=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsSendDocWithOutSign"]];
    objSubWorkOrder.calculatedActualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CalculatedActualTimeInt"]];
    objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
    objSubWorkOrder.clockStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ClockStatus"]];
    objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
    
    //setting null values to blank
    
    objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
    objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
    objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
    objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
    objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
    objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
    objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
    objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
    objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
    objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
    objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
    objSubWorkOrder.tripChargesQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargesQty"]];

    
    
    NSError *error222;
    [context save:&error222];
    
    if ([arrOfSubWorkOrderIssuesInfo isKindOfClass:[NSArray class]]) {
        
        for (int k=0; k<arrOfSubWorkOrderIssuesInfo.count; k++) {
            
            NSDictionary *dictSubworkorderIssues=arrOfSubWorkOrderIssuesInfo[k];
            
            [self saveSUbWorkOrderIssues :dictSubworkorderIssues :strWorkOrderId];
            
        }
        
    }
    
    if ([arrOfSubWorkOrderHelper isKindOfClass:[NSArray class]]) {

        for (int k1=0; k1<arrOfSubWorkOrderHelper.count; k1++) {
            
            NSDictionary *dictSubworkOrderHelper=arrOfSubWorkOrderHelper[k1];
            
            [self saveSUbWorkOrderHelper :dictSubworkOrderHelper :strWorkOrderId];
            
        }
        
    }
    
    if ([arrOfSubWorkOrderNotes isKindOfClass:[NSArray class]]) {

        for (int k2=0; k2<arrOfSubWorkOrderNotes.count; k2++) {
            
            NSDictionary *dictSubworkOrderNotes=arrOfSubWorkOrderNotes[k2];
            
            [self saveSUbWorkOrderHelper :dictSubworkOrderNotes :strWorkOrderId];
            
        }
        
    }

}

-(void)saveSUbWorkOrderIssues :(NSDictionary*)dictOfSubWorkOrderIssuesInfo :(NSString*)strWorkOrderId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];//ServiceDynamicForm
    
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entitySubWorkOrderIssues insertIntoManagedObjectContext:context];
    
    objSubWorkOrderIssues.workorderId=strWorkOrderId;
    objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
    objSubWorkOrderIssues.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderIssues.serviceIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ServiceIssue"]];
    objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Priority"]];
    objSubWorkOrderIssues.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"IsActive"]]];
    objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrderIssues.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrderIssues.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedByDevice"]];
    
    objSubWorkOrderIssues.equipmentCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCode"]];
    objSubWorkOrderIssues.equipmentName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentName"]];
    objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SerialNumber"]];
    objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModelNumber"]];
    objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Manufacturer"]];
    objSubWorkOrderIssues.equipmentCustomName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCustomName"]];
    objSubWorkOrderIssues.equipmentNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentNo"]];
    
    NSError *error2;
    [context save:&error2];
    
    NSDictionary *dictOfSubWorkOrderInfo = dictOfSubWorkOrderIssuesInfo;
    
    NSString *strWOTYPE=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
    
    if ([strWOTYPE isEqualToString:@"TM"]) {
        
        
        //Yaha Parr Issues Repairs parts Save karna hai
        
        //============================================================================
        //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Parts Time And Material Save --------------------------
        //============================================================================
        //============================================================================
        
        //                            entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
        //
        //                            MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
        
        //SubWorkOrderIssuesRepairsParts
        if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"];
            
            for (int k2=0; k2<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k2++) {
                entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                
                MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                
                NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k2];
                
                objSubWorkOrderIssuesRepairsParts.workorderId=strWorkOrderId;
                objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];
                objSubWorkOrderIssuesRepairsParts.issueRepairId=@"";//[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                objSubWorkOrderIssuesRepairsParts.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                objSubWorkOrderIssuesRepairsParts.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                objSubWorkOrderIssuesRepairsParts.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                objSubWorkOrderIssuesRepairsParts.isDefault=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                objSubWorkOrderIssuesRepairsParts.isWarranty=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                objSubWorkOrderIssuesRepairsParts.installationDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                objSubWorkOrderIssuesRepairsParts.isCompleted=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]]];
                objSubWorkOrderIssuesRepairsParts.customerFeedback=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];
                
                
                
                NSError *error5;
                [context save:&error5];
                
            }
            
        }
        
        
    } else {
        
        //Yaha Par SubWork Order Repairs add honge
        
        //============================================================================
        //============================================================================
#pragma mark- @@@@@@@@------------------ Sub Work Order Issues Repairs Save --------------------------
        //============================================================================
        //============================================================================
        
        //                            entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
        //
        //                            MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
        
        //SubWorkOrderIssuesRepairs
        if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"] isKindOfClass:[NSArray class]])
        {
            
        }
        else
        {
            NSArray *arrOfSubWorkOrderIssuesRepairsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"];
            
            for (int k3=0; k3<arrOfSubWorkOrderIssuesRepairsFromServer.count; k3++) {
                
                entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
                
                MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepair insertIntoManagedObjectContext:context];
                
                NSDictionary *dictOfSubWorkOrderIssuesRepairsInfo = arrOfSubWorkOrderIssuesRepairsFromServer[k3];
                
                objSubWorkOrderIssuesRepairs.workorderId=strWorkOrderId;
                objSubWorkOrderIssuesRepairs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                objSubWorkOrderIssuesRepairs.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IssueRepairId"]];
                objSubWorkOrderIssuesRepairs.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                objSubWorkOrderIssuesRepairs.repairMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairMasterId"]];
                objSubWorkOrderIssuesRepairs.repairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairLaborId"]];
                objSubWorkOrderIssuesRepairs.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsActive"]]];
                objSubWorkOrderIssuesRepairs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedBy"]];
                objSubWorkOrderIssuesRepairs.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedDate"]]];
                objSubWorkOrderIssuesRepairs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedBy"]];
                objSubWorkOrderIssuesRepairs.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedDate"]]];
                objSubWorkOrderIssuesRepairs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedByDevice"]];
                objSubWorkOrderIssuesRepairs.repairName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairName"]];
                objSubWorkOrderIssuesRepairs.repairDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairDesc"]];
                objSubWorkOrderIssuesRepairs.costAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CostAdjustment"]];
                objSubWorkOrderIssuesRepairs.isWarranty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsWarranty"]];
                objSubWorkOrderIssuesRepairs.isCompleted=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsCompleted"]]];
                objSubWorkOrderIssuesRepairs.customerFeedback=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CustomerFeedback"]]];
                
                objSubWorkOrderIssuesRepairs.partCostAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"PartCostAdjustment"]];
                objSubWorkOrderIssuesRepairs.nonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdRepairAmt"]];
                objSubWorkOrderIssuesRepairs.nonStdPartAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdPartAmt"]];
                objSubWorkOrderIssuesRepairs.nonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdLaborAmt"]];
                objSubWorkOrderIssuesRepairs.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"Qty"]];
                
                NSError *error4;
                [context save:&error4];
                
                
                //Yaha Parr Issues Repairs parts Save karna hai
                
                //============================================================================
                //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Parts Save --------------------------
                //============================================================================
                //============================================================================
                
                //                                    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                //
                //                                    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                
                //SubWorkOrderIssuesRepairsParts
                if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"];
                    
                    for (int k4=0; k4<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k4++) {
                        
                        entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairParts insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k4];
                        
                        objSubWorkOrderIssuesRepairsParts.workorderId=strWorkOrderId;
                        objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];
                        objSubWorkOrderIssuesRepairsParts.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                        objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                        objSubWorkOrderIssuesRepairsParts.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderIssuesRepairsParts.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderIssuesRepairsParts.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                        objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                        objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                        objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                        objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                        objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                        objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                        objSubWorkOrderIssuesRepairsParts.isDefault=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                        objSubWorkOrderIssuesRepairsParts.isWarranty=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                        objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                        objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                        objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                        objSubWorkOrderIssuesRepairsParts.installationDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                        objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                        objSubWorkOrderIssuesRepairsParts.isCompleted=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]];
                        objSubWorkOrderIssuesRepairsParts.customerFeedback=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                        objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                        objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                        objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];
                        
                        
                        
                        NSError *error5;
                        [context save:&error5];
                        
                    }
                    
                }
                
                
                
                //Yaha Parr Issues Repairs parts Save karna hai
                
                
                //============================================================================
                //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Labours Save --------------------------
                //============================================================================
                //============================================================================
                
                //                                    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                //
                //                                    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
                
                //SubWorkOrderIssues
                if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"];
                    
                    for (int k5=0; k5<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k5++) {
                        
                        entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                        
                        MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entitySubWorkOrderIssuesRepairLabour insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k5];
                        
                        objSubWorkOrderIssuesRepairsLabour.workorderId=strWorkOrderId;
                        objSubWorkOrderIssuesRepairsLabour.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                        objSubWorkOrderIssuesRepairsLabour.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                        objSubWorkOrderIssuesRepairsLabour.issueRepairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairLaborId"]];
                        objSubWorkOrderIssuesRepairsLabour.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                        objSubWorkOrderIssuesRepairsLabour.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborType"]];
                        objSubWorkOrderIssuesRepairsLabour.laborCost=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCost"]];
                        
                        objSubWorkOrderIssuesRepairsLabour.laborHours=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborHours"]];
                        
                        objSubWorkOrderIssuesRepairsLabour.laborCostType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCostType"]];
                        objSubWorkOrderIssuesRepairsLabour.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrderIssuesRepairsLabour.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrderIssuesRepairsLabour.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrderIssuesRepairsLabour.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrderIssuesRepairsLabour.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrderIssuesRepairsLabour.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                        objSubWorkOrderIssuesRepairsLabour.laborDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborDescription"]];
                        objSubWorkOrderIssuesRepairsLabour.isWarranty=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                        objSubWorkOrderIssuesRepairsLabour.isDefault=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                        
                        NSError *error6;
                        [context save:&error6];
                        
                    }
                }
            }
        }
    }
}

-(void)saveSUbWorkOrderHelper :(NSDictionary*)dictOfSubWorkOrderHelperInfo :(NSString*)strWorkOrderId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    
    MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
    
    //NSDictionary *dictOfSubWorkOrderHelperInfo = arrOfSubWorkOrderHelperFromServer[k6];
    
    objSubWorkOrderHelper.workorderId=strWorkOrderId;
    objSubWorkOrderHelper.subWorkOrderTechnicianId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"SubWorkOrderTechnicianId"]];
    objSubWorkOrderHelper.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"EmployeeNo"]];
    objSubWorkOrderHelper.isActive=[self strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"IsActive"]]];
    objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedBy"]];
    objSubWorkOrderHelper.createdDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedDate"]]];
    objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedBy"]];
    objSubWorkOrderHelper.modifiedDate=[self ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedDate"]]];
    objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedByDevice"]];
    
    NSError *error2;
    [context save:&error2];
    
}


-(BOOL)isSlotMatched :(NSString*)strSlot1 :(NSString*)strSlotTitle1 :(NSString*)strSlotsToMatch{
    
    BOOL isMatched = NO;
    
    if ([strSlot1 isEqualToString:strSlotsToMatch]) {
        
        isMatched=YES;
        
    } else if ([strSlotTitle1 isEqualToString:strSlotsToMatch]){
        
        isMatched=YES;
        
    } else{
        
        isMatched=NO;
        
    }
    
    return isMatched;
}

-(NSString*)strStatNameFromID :(NSString*)strId{
    
    NSString *strName=@"N/A";
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:StateJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    for (int k=0; k<arrOfState.count; k++) {
        
        NSDictionary *dictData =arrOfState[k];
        
        NSString *strID = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"StateId"]];
        
        if ([strID isEqualToString:strId]) {
            
            strName = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"Name"]];
            
            break;
            
        }
        
    }
    
    return strName;
}

-(NSString*)strCombinedAddress :(NSDictionary*)dictData{
    
    dictData = [self nestedDictionaryByReplacingNullsWithNil:dictData];
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"Address1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"Address1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"Address1"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"AddressLine1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"AddressLine1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"AddressLine1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServicesAddress1"]) {

        

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]].length==0)) {

            

            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]]];

            

        }

        

    }
    
    if ([arrTempKeys containsObject:@"Address2"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"Address2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"Address2"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"AddressLine2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"AddressLine2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"AddressLine2"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"CityName"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"CityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"City"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"City"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"City"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"StateId"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateId"]]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"CountryName"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"Zipcode"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"Zipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"Zipcode"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"ZipCode"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ZipCode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ZipCode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"Zip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"Zip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"Zip"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"address1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"address1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"address1"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"addressLine1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"addressLine1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"addressLine1"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"address2"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"address2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"address2"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"addressLine2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"addressLine2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"addressLine2"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"cityName"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"cityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"cityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"city"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"city"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"city"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"stateId"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"stateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"stateId"]]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"countryName"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"countryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"zipcode"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"zipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"zipcode"]]];
            
        }
        
    }

    if ([arrTempKeys containsObject:@"zip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"zip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"zip"]]];
            
        }
        
    }
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[self getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}


-(NSString*)strFloridaAddress :(NSDictionary*)dictData{
    
    dictData = [self nestedDictionaryByReplacingNullsWithNil:dictData];
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"CityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"CityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CityName"]]];
            
        }
        
    }
    if ([arrTempKeys containsObject:@"StateName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"StateName"]]];
            
        }
        
    }
        
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        
    }
    
    if ([arrTempKeys containsObject:@"ZipCode"]) {

        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ZipCode"]].length==0)) {

            if (strCombinedAddress.length > 0) {
                
                strCombinedAddress = [NSString stringWithFormat:@"%@ %@",strCombinedAddress,[dictData valueForKey:@"ZipCode"]];
                
            }

        }

    }
    
    return strCombinedAddress;
    
}

/// Service
-(NSString*)strCombinedAddressService :(NSDictionary*)dictData{
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"ServiceAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServicesAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]].length==0)) {

            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServicesAddress1"]]];

        }

    }
    
    if ([arrTempKeys containsObject:@"ServiceAddress2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceAddress2"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCity"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCity"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCity"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceStateId"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceStateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceStateId"]]]];
            
        }
        
    }
    else if ([arrTempKeys containsObject:@"ServiceState"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceState"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceState"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceCountryName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceCountryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceZipcode"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZipcode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"ServiceZip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"ServiceZip"]]];
            
        }
        
    }
    
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[self getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}

//Billing
-(NSString*)strCombinedAddressBilling :(NSDictionary*)dictData{
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"BillingAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingAddress1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingAddress1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingAddress2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingAddress2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingAddress2"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingCityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingCityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingCityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingCity"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingCity"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingCity"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingStateId"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingStateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingStateId"]]]];
            
        }
        
    }
    else if ([arrTempKeys containsObject:@"billingState"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingState"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingState"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingCountryName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingCountryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingZipcode"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingZipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingZipcode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"BillingZip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingZip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"BillingZip"]]];
            
        }
        
    }
    
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[self getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}
-(NSString*)strFullName :(NSDictionary*)dictData{
    
    NSMutableArray *arrFullName=[[NSMutableArray alloc] init];
    
    NSString *strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
    NSString *strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
    NSString *strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
    
    if ((strFN.length>0) && ![strFN isEqualToString:@"(null)"] &&![strFN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strFN];
        
    }else{
     
        strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"firstName"]];
        
        if ((strFN.length>0) && ![strFN isEqualToString:@"(null)"] &&![strFN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strFN];
            
        }
        
    }
        
    if ((strMN.length>0) && ![strMN isEqualToString:@"(null)"] &&![strMN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strMN];
        
    }else{
     
        strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"middleName"]];
        
        if ((strMN.length>0) && ![strMN isEqualToString:@"(null)"] &&![strMN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strMN];
            
        }
        
    }
    if ((strLN.length>0) && ![strLN isEqualToString:@"(null)"] && ![strLN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strLN];
        
    }else{
        
        strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"lastName"]];
        
        if ((strLN.length>0) && ![strLN isEqualToString:@"(null)"] && ![strLN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strLN];
            
        }
    }
    
    NSString *strFullName;
    
    if (arrFullName.count>0) {
        
        strFullName = [arrFullName componentsJoinedByString:@" "];

    }else{
        
        strFullName = @"";

    }
    
    return strFullName;
}

-(NSString*)strFullNameFromCoreDB :(NSManagedObject*)dictData{
    
    NSMutableArray *arrFullName=[[NSMutableArray alloc] init];
    
//    NSString *strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"FirstName"]];
//    NSString *strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"MiddleName"]];
//    NSString *strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"LastName"]];
    
    NSString *strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"firstName"]];
    NSString *strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"middleName"]];
    NSString *strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"lastName"]];
    
    if ((strFN.length>0) && ![strFN isEqualToString:@"(null)"] &&![strFN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strFN];
        
    }else{
     
        strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"firstName"]];
        
        if ((strFN.length>0) && ![strFN isEqualToString:@"(null)"] &&![strFN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strFN];
            
        }
        
    }
        
    if ((strMN.length>0) && ![strMN isEqualToString:@"(null)"] &&![strMN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strMN];
        
    }else{
     
        strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"middleName"]];
        
        if ((strMN.length>0) && ![strMN isEqualToString:@"(null)"] &&![strMN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strMN];
            
        }
        
    }
    if ((strLN.length>0) && ![strLN isEqualToString:@"(null)"] && ![strLN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strLN];
        
    }else{
        
        strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"lastName"]];
        
        if ((strLN.length>0) && ![strLN isEqualToString:@"(null)"] && ![strLN isEqualToString:@"<null>"]) {
            
            [arrFullName addObject:strLN];
            
        }
    }
    
    NSString *strFullName;
    
    if (arrFullName.count>0) {
        
        strFullName = [arrFullName componentsJoinedByString:@" "];

    }else{
        
        strFullName = @"";

    }
    
    return strFullName;
}

-(NSArray*)arrFilter :(NSArray*)arrToFilter :(NSString*)strDateParam :(BOOL)ascendingYes{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: strDateParam ascending: ascendingYes];
    NSArray *sortedArray = [arrToFilter sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return sortedArray;
    
}

-(BOOL)isMechanicType :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal :(NSString*)strEmpNo{
    
    BOOL isMechanic;
    isMechanic = NO;
    
    NSMutableArray *arrOfSubWorkServiceHelper;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && employeeNo = %@",strWorkOrderId,strSubWorkOrderIdGlobal,strEmpNo];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        
    }
    else
    {
        arrOfSubWorkServiceHelper=nil;
        arrOfSubWorkServiceHelper=[[NSMutableArray alloc]init];
        
        NSManagedObject *matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[0];
        
        NSString *strlaborType=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderHelper valueForKey:@"laborType"]];
        
        if ([strlaborType isEqualToString:@"L"] || [strlaborType isEqualToString:@""] || (strlaborType.length==0)) {

            isMechanic = YES;
            
        } else {
            
            isMechanic = NO;
            
        }
        
//        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
//
//           NSManagedObject *matchesSubWorkOrderHelper=arrAllObjSubWorkOrderHelper[k];
//
//            NSString *strIsActive=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderHelper valueForKey:@"isActive"]];
//
//            if ([strIsActive caseInsensitiveCompare:@"true"] == NSOrderedSame || [strIsActive isEqualToString:@"1"]) {
//
//                [arrOfSubWorkServiceHelper addObject:matchesSubWorkOrderHelper];
//
//            }
//
//        }
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return isMechanic;
    
}

//MechanicalSubWorkOrder
-(BOOL)isMechanicTypeForLoggedInUser :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
    
    BOOL isMechanic;
    isMechanic = NO;
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailsNew;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetailsNew = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailsNew setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailsNew performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetailsNew fetchedObjects];

    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        NSManagedObject *matchesSubWorkOrderHelper=arrAllObjWorkOrder[0];
        
        NSString *strlaborType=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderHelper valueForKey:@"laborType"]];
        
        if ([strlaborType isEqualToString:@"L"] || [strlaborType isEqualToString:@""] || (strlaborType.length==0)) {
            
            isMechanic = YES;
            
        } else {
            
            isMechanic = NO;
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return isMechanic;
    
}

-(NSString *)ChangeDateToLocalDateOtherWithTimeNilind :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}


-(void)deleteDataFromCoreData :(NSEntityDescription*)entityDescription :(NSString*)strMechanicalWoId  :(NSString*)strEntityForName{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityDescription=[NSEntityDescription entityForName:strEntityForName inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:strEntityForName inManagedObjectContext:context]];
    
    NSPredicate *predicate ;
    // WorkOrderAppliedDiscountExtSerDcs
    if ([strEntityForName isEqualToString:@"WoOtherDocExtSerDcs"] || [strEntityForName isEqualToString:@"WorkOrderAppliedDiscountExtSerDcs"]) {
        
        predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strMechanicalWoId];
        
    } else {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strMechanicalWoId];
        
    }
    
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


//NIlind

void(^getSynchronousServerResponseForUrlCallback)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getSynchronousServerResponseForUrl:(NSString *)url :(NSString *)Type withCallback:(WebServiceCompletionBlockSynchronous)callback
{
    getSynchronousServerResponseForUrlCallback = callback;
    [self onGetSynchronousResponse:nil withSuccess:YES error:nil withUrl:url :Type];
}
- (void)onGetSynchronousResponse:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl :(NSString *)type
{
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
        [dictTempResponse setObject:ResponseDict forKey:@"response"];
        NSDictionary *dict=[[NSDictionary alloc]init];
        dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
        NSMutableDictionary *dictData=[dict valueForKey:@"response"];
        ResponseDict =dictData;
        
        
        NSString *strException;
        @try {
            strException=[ResponseDict valueForKey:@"ExceptionMessage"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        if (![strException isKindOfClass:[NSString class]]) {
            
            strException=@"";
            
        }
        
        if (strException.length==0) {
            
            
            
        } else {
            ResponseDict=nil;
        }
        
        NSLog(@"Response = = = =  = %@",ResponseDict);
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getSynchronousServerResponseForUrlCallback(success, ResponseDict, error);
}

-(NSString*)checkIfStringNull :(NSString*)strValueToCheck{
    
    NSString *value;
    
    if ([strValueToCheck isEqualToString:@"(null)"] || [strValueToCheck isEqualToString:@""] || strValueToCheck.length==0 || [strValueToCheck isEqualToString:@"<null>"]) {
        
        value = @"";
        
    }else{
        
        value = strValueToCheck;
        
    }
    
    return value;
    
}



-(NSString *)ChangeDateToLocalDateAkshay :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    
    if (finalTime.length==0)
    {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        //Add the following line to display the time in the local time zone
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0) {
        
        finalTime=@"";
        
    }
    return finalTime;
}

-(NSString *)ChangeDateToLocalTimeAkshay :(NSString*)strDateToConvert type:(NSString*) strType
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    [dateFormatter setDateFormat:@"HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    //Add the following line to display the time in the local time zone
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    
    if ([strType isEqualToString:@"time"])
    {
        [dateFormatter setDateFormat:@"hh:mm a"];
        
    }
    else
    {
        [dateFormatter setDateFormat:@"HH:mm"];
        
    }
    
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0)
    {
        finalTime=@"";
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"HH:mm:ss.SSS'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        if ([strType isEqualToString:@"time"])
        {
            
            [dateFormatter setDateFormat:@"hh:mm a"];
        }
        else
        {
            [dateFormatter setDateFormat:@"HH:mm"];
        
        }
        finalTime = [dateFormatter stringFromDate:newTime];
    }
    
    if (finalTime.length==0)
    {
        finalTime=@"";
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        if ([strType isEqualToString:@"time"])
        {
            [dateFormatter setDateFormat:@"hh:mm a"];
        }
        else
        {
            [dateFormatter setDateFormat:@"HH:mm"];
            
        }
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0)
    {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        [dateFormatter setDateFormat:@"HH:mm:ss.SSS"];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        if ([strType isEqualToString:@"time"])
        {
            [dateFormatter setDateFormat:@"hh:mm a"];
        }
        else
        {
            [dateFormatter setDateFormat:@"HH:mm"];
            
        }
        finalTime = [dateFormatter stringFromDate:newTime];
        
    }
    if (finalTime.length==0)
    {
        finalTime=@"";
        
    }
    
    return finalTime;
    
}



-(NSString *)getBranchNameFromSysNam:(NSString*)strBranchSysName
{
    NSString *strBranchName=@"";
    NSUserDefaults *userDef=[NSUserDefaults standardUserDefaults];
    NSArray *arrayBranch = [[userDef objectForKey:@"LeadDetailMaster"] valueForKey:@"BranchMastersAll"];
    
    for (int k=0; k<arrayBranch.count; k++)
    {
        
        NSDictionary *dictDataa=arrayBranch[k];
        if ([[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"SysName"]]isEqualToString:strBranchSysName])
        {
            strBranchName =[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"Name"]];
            break;
        }
        
    }
    return strBranchName;
}

#pragma mark- -------NILIND------

-(NSString*)getYesterdayDate: (NSString*)strCurrentDate
{
    NSTimeInterval secondsInHours = -(23*60*60 + 59*60 + 59);
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDate] :secondsInHours];
    return newUpdatedTime;
}
-(NSString*)getYesterdayDateEST: (NSString*)strCurrentDate
{
    NSTimeInterval secondsInHours = -(23*60*60 + 59*60 + 59);
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDateEST] :secondsInHours];
    return newUpdatedTime;
}
-(NSMutableDictionary*)getCureentMonthDate
{
    
    NSMutableDictionary *dictDataTemp = [[NSMutableDictionary alloc]init];
    
    NSString *strCurrentDate=[self getCurrentDate];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];// you can use your format.
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSDate *startDateMonth = [calendar dateFromComponents:components];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictDataTemp setObject:dateStartMonth forKey:@"StartDate"];
    
    
    //End Date Month
    NSCalendar* calendarEndDay = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsEndDay = [calendarEndDay components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:today];
    
    [componentsEndDay setDay:dayRange.length];
    [componentsEndDay setHour:23];
    [componentsEndDay setMinute:59];
    [componentsEndDay setSecond:59];
    
    NSDate *endDateMonth = [calendarEndDay dateFromComponents:componentsEndDay];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictDataTemp setObject:dateEndMonth forKey:@"EndDate"];
    
    return dictDataTemp;
    
}
-(NSMutableDictionary*)getCureentMonthDateEST
{
    
    NSMutableDictionary *dictDataTemp = [[NSMutableDictionary alloc]init];
    
    NSString *strCurrentDate=[self getCurrentDateEST];
    NSDateFormatter *dateFormatNew=[[NSDateFormatter alloc]init];
    [dateFormatNew setDateFormat:@"MM/dd/yyyy"];
    dateFormatNew.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSDate *today = [dateFormatNew dateFromString:strCurrentDate];
    NSLog(@"Today date is %@",today);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];// you can use your format.
    dateFormat.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSDate *startDateMonth = [calendar dateFromComponents:components];
    NSDateFormatter *dateFormat_Start = [[NSDateFormatter alloc] init];
    [dateFormat_Start setDateFormat:@"MM/dd/yyyy"];
    dateFormat_Start.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSString* dateStartMonth = [dateFormat stringFromDate:startDateMonth];
    [dictDataTemp setObject:dateStartMonth forKey:@"StartDate"];
    
    
    //End Date Month
    NSCalendar* calendarEndDay = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* componentsEndDay = [calendarEndDay components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:today];
    
    [componentsEndDay setDay:dayRange.length];
    [componentsEndDay setHour:23];
    [componentsEndDay setMinute:59];
    [componentsEndDay setSecond:59];
    
    NSDate *endDateMonth = [calendarEndDay dateFromComponents:componentsEndDay];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"MM/dd/yyyy"];
    dateFormat_End.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSString* dateEndMonth = [dateFormat stringFromDate:endDateMonth];
    [dictDataTemp setObject:dateEndMonth forKey:@"EndDate"];
    
    return dictDataTemp;
    
}
-(NSString *)getCurrentDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    //formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
}
-(NSString *)getCurrentDateEST
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM/dd/yyyy";
    formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
    NSString *stringCurrentDate = [formatter stringFromDate:[NSDate date]];
    return stringCurrentDate;
}
-(NSString*)getStartDate: (NSString*)strCurrentDate :(NSInteger)noOfDays
{
    NSTimeInterval secondsInHours = - (23*60*60 + 59*60 + 59) * noOfDays;
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDate] :secondsInHours];
    return newUpdatedTime;
}
-(NSString*)getEndDate: (NSString*)strCurrentDate :(NSInteger)noOfDays
{
    NSTimeInterval secondsInHours = (23*60*60 + 59*60 + 59) * (noOfDays+1);
    NSString* newUpdatedTime=[self updateHours:[self getCurrentDate] :secondsInHours];
    return newUpdatedTime;
}
-(NSString*)updateHours: (NSString*)strMaxDate : (NSTimeInterval)secondsInHours
{
    NSDateFormatter *dateForMateNew=[[NSDateFormatter alloc]init];
    [dateForMateNew setDateFormat:@"MM/dd/yyyy"];
    
    NSDate *dateEightHoursAhead = [[dateForMateNew dateFromString:strMaxDate] dateByAddingTimeInterval:secondsInHours];
    
    NSString* newTime = [dateForMateNew stringFromDate:dateEightHoursAhead];
    
    NSDate *dateNew=[dateForMateNew dateFromString:newTime];
    
    NSString* strPayPeriodDate=[dateForMateNew stringFromDate:dateNew];
    
    return strPayPeriodDate;
}

-(BOOL)alertForTaxCode :(NSString*)strTaxCodeSelected{
    
    BOOL isRequired;
    
    isRequired = YES;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    BOOL isTaxCodeReq = [defs boolForKey:@"TaxCodeReq"];
    
    if (isTaxCodeReq) {
        
        if (strTaxCodeSelected.length==0) {
            
            isRequired = YES;
            
        } else {
            
            isRequired = NO;
            
        }
        
    } else {
        
        isRequired = NO;
        
    }
    
    return isRequired;
    
}

-(void)defaultSelectedStatus{
 
    BOOL downloaded2 = [[NSUserDefaults standardUserDefaults] boolForKey: @"firstInstall_5March2019"];
    if (!downloaded2) {
        
        NSUserDefaults *defsLeadLocal=[NSUserDefaults standardUserDefaults];
        NSArray *arrOfLeadStatusMastersLocal=[defsLeadLocal valueForKey:@"LeadStatusMasters"];
        NSMutableArray *arrOfStatusLocal =[[NSMutableArray alloc]init];
        
        for (int k=0;k<arrOfLeadStatusMastersLocal.count; k++) {
            NSDictionary *dictDataLocal=arrOfLeadStatusMastersLocal[k];
            [arrOfStatusLocal addObject:[dictDataLocal valueForKey:@"StatusName"]];
        }
        
        [defsLeadLocal setObject:arrOfStatusLocal forKey:@"SelectedStatusValues"];
        [defsLeadLocal synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"firstInstall_5March2019"];
    }
    
}

-(float)roundedOffValuess :(float)hrsTime{
 
    NSString *strTempHrs = [NSString stringWithFormat:@"%.02f",hrsTime];
    
    float tempHrsTime = [strTempHrs floatValue];
    
    return tempHrsTime;
    
}


//============================================================================
//============================================================================
#pragma mark--- Change Password Call Back Method
//============================================================================
//============================================================================

void(^getServerResponseForUrlCallbackPostMethodChangePass)(BOOL success, NSDictionary *response, NSError *error);

// --------------
- (void)getServerResponseForUrlPostMethodChangePass:(NSString *)url :(NSData *)requestDataa withCallback:(WebServicePostCompletionBlock)callback
{
    getServerResponseForUrlCallbackPostMethodChangePass = callback;
    [self onBackendResponsePostMethodChangePass:nil withSuccess:YES error:nil withUrl:url withData:requestDataa];
}

- (void)onBackendResponsePostMethodChangePass:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSData*)requestData
{
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:0 forHTTPHeaderField:@"BranchId"];
    [request addValue:@"" forHTTPHeaderField:@"BranchSysName"];
    [request addValue:@"true" forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:@"" forHTTPHeaderField:@"ClientTimeZone"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    @try {
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
    getServerResponseForUrlCallbackPostMethodChangePass(success, ResponseDict, error);
}



-(void)fetchIfEmailIdExistInDB :(NSString*)strEmailId :(NSString*)strType :(NSString*)strWOID{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"emailId = %@ && workorderId = %@",strEmailId,strWOID];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //workorderId
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    if (Data.count>0) {
        
        for (NSManagedObject * data in Data) {
            
            if ([strType isEqualToString:@"Billing"]) {
                
                [data setValue:@"true" forKey:@"isMailSent"];
                [data setValue:@"true" forKey:@"isInvoiceMailSent"];
                
            }  else {
                
                [data setValue:@"true" forKey:@"isMailSent"];
                [data setValue:@"false" forKey:@"isInvoiceMailSent"];
                
            }
            // BillingService
            if ([strType isEqualToString:@"BillingService"]) {
                
                [data setValue:@"true" forKey:@"isMailSent"];
                [data setValue:@"true" forKey:@"isInvoiceMailSent"];
                
            }
            
            
        }
        
    } else {
        
        //Email Detail Entity
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
        objEmailDetail.createdBy=@"0";;
        
        objEmailDetail.createdDate=@"0";
        
        objEmailDetail.emailId=strEmailId;
        
        objEmailDetail.isCustomerEmail=@"true";
        
        objEmailDetail.woInvoiceMailId=@"";
        
        objEmailDetail.modifiedBy=@"";
        
        objEmailDetail.modifiedDate=[self modifyDate];
        
        objEmailDetail.subject=@"";
        
        objEmailDetail.workorderId=strWOID;
        
        
        if ([strType isEqualToString:@"Billing"]) {
            
            objEmailDetail.isMailSent=@"true";
            objEmailDetail.isInvoiceMailSent=@"true";
            
        }  else {
            
            objEmailDetail.isMailSent=@"true";
            objEmailDetail.isInvoiceMailSent=@"false";
            
        }
    }
    

    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(BOOL)emailAlreadyExists :(NSString*)strEmailId :(NSString*)strWOID{
    
    BOOL isExistMailInDB = NO;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"emailId = %@ && workorderId = %@",strEmailId,strWOID];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //workorderId
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    
    if (Data.count>0) {
        
        isExistMailInDB = YES;
        
    } else {
        
        isExistMailInDB = NO;
        
    }
    
    return isExistMailInDB;
    
}

-(NSArray *)getEmployeeBlockTime:(NSString *)strUrls :(NSString *)strCompanyKey :(NSString *)strEmpNo
{
    
    NSArray *arrOfBlockTime;
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
        
    {
        return arrOfBlockTime;
    }
    else
    {
        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strUrls,URLGetEmployeeBlockTime_SP,strCompanyKey,URLGetEmployeeBlockTimeEmployeeNo,strEmpNo];
        
        //strUrl = @"http://tcrs.pestream.com//api/EmployeeBlockTime/GetEmployeeBlockTimesForMobile?companykey=automation&employeeno=2702545468";
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        
        NSURL *url = [NSURL URLWithString:strUrl];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSError *error = nil;
        
        NSURLResponse * response = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            return arrOfBlockTime;
        }
        else
        {
            
            NSData* jsonData = [NSData dataWithData:data];
            arrOfBlockTime = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if ([arrOfBlockTime isKindOfClass:[NSArray class]]) {
                
                if (arrOfBlockTime.count>0) {
                    
                    
                    NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                    [dictTempResponse setObject:arrOfBlockTime forKey:@"response"];
                    NSDictionary *dict=[[NSDictionary alloc]init];
                    dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                    arrOfBlockTime=[dict valueForKey:@"response"];
                    
                    NSUserDefaults *blockDefs = [NSUserDefaults standardUserDefaults];
                    
                    [blockDefs setObject:arrOfBlockTime forKey:@"BlockTime"];
                    
                    [blockDefs synchronize];
                    
                }else {
                    
                    NSArray *arrBlank;
                    
                    NSUserDefaults *blockDefs = [NSUserDefaults standardUserDefaults];
                    
                    [blockDefs setObject:arrBlank forKey:@"BlockTime"];
                    
                    [blockDefs synchronize];
                    
                }
                
            }else {
                
                NSArray *arrBlank;
                
                NSUserDefaults *blockDefs = [NSUserDefaults standardUserDefaults];
                
                [blockDefs setObject:arrBlank forKey:@"BlockTime"];
                
                [blockDefs synchronize];
                
            }
            
            return arrOfBlockTime;
            
        }
        
    }
}


-(void)getPlumbingMasterForAccountAddress :(NSString*)strServiceUrlMain :(NSString*)companyKey :(NSDictionary*)dict_ToSend{

    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Get Plumbing Master For Account Address: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@",strServiceUrlMain,UrlGetPlumbingMasterForAccountAddress,companyKey];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];
    
    @try {
        
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             
             if ((ResponseDict==nil) || ResponseDict.count==0) {
                 
                 //[self displayAlertController:Alert :NoDataAvailableee :self];
                 [DejalBezelActivityView removeView];
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
             }
             
         }];
    }
    @catch (NSException *exception) {
        
        //[self displayAlertController:Alert :Sorry :self];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
    
}


-(void)updateStatusOnMyWayLifeStyle :(NSString*)strServiceUrlMain :(NSDictionary*)dict_ToSend{
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict_ToSend])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"On My Way Status Life Style----: %@", jsonString);
        }
    }
    
    NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMain,URLUpdateOnMyWayLifeStyle];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"POST"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    [requestLocal setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [requestLocal setHTTPBody:requestData];
    
    @try {
        
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             //NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             NSDictionary *dictTemp = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if ([dictTemp isKindOfClass:[NSDictionary class]]) {
                 
                 NSLog(@"Response on On My Way Life Style-------%@",dictTemp);
                 
             }
             /*
             BOOL boolResponse = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] boolValue];
             
             if (boolResponse) {
                 
                 //ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"ReturnMsg", nil];
                 
             } else {
                 
                 //ResponseDict=[[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"ReturnMsg", nil];
                 
             }*/
             
         }];
    }
    @catch (NSException *exception) {
        
        //[self displayAlertController:Alert :Sorry :self];
        [DejalBezelActivityView removeView];
        
    }
    @finally {
    }
    
}

-(NSString *)getSignedAgreementBadgeCount
{
    NSString *strTax;
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
        
    {
        return @"0";
    }
    else
    {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strEmpId =[dictLoginData valueForKey:@"EmployeeId"];

        NSString *strMainUrl =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"];
        NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
        
        NSString *strLastDate = [defsLogindDetail valueForKey:@"LastDateSignedAgreements"];

        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",strMainUrl,UrlSalesSignedAgreementsBadgeCount,strLastDate,UrlSalesSignedAgreementsBadgeCountCompanyKey,strCompanyKey,UrlSalesSignedAgreementsBadgeCountEmpId,strEmpId];
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        
        NSURL *url = [NSURL URLWithString:strUrl];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSError *error = nil;
        
        NSURLResponse * response = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            return @"0";
        }
        else
        {
            NSString *responseString1    = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Signed Agreement Badge Count %@",responseString1);
            
            strTax=responseString1;
            
            if (responseString1.length>10) {
                
                responseString1 = @"0";
                
            }
        
            return responseString1;
            
        }
        
    }
}

-(NSString*)strReplacedEmail :(NSString*)strStringToBeReplaced{
 
    strStringToBeReplaced = [strStringToBeReplaced stringByReplacingOccurrencesOfString:@";" withString:@","];
    
    strStringToBeReplaced =  [strStringToBeReplaced stringByReplacingOccurrencesOfString:@":" withString:@","];
    
    return strStringToBeReplaced;
    
}

-(BOOL)validEmail :(NSString*)strEmail{

    BOOL isValid;
    isValid = YES;
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if (![emailPredicate evaluateWithObject:strEmail])
    {
        
        isValid = NO;
        
    }
    
    return isValid;
    
}


-(BOOL)checkIfCommaSepartedEmailsAreValid :(NSString*)strEmail{
 
    BOOL isValid;
    isValid = YES;
    
    strEmail = [strEmail stringByReplacingOccurrencesOfString:@";" withString:@","];
    strEmail = [strEmail stringByReplacingOccurrencesOfString:@":" withString:@","];
    
    NSArray *arrTemp = [strEmail componentsSeparatedByString:@","];
    
    for (int k=0; k<arrTemp.count; k++) {
        
        NSString *str = arrTemp[k];
        
        BOOL isValidEmail = [self validEmail:str];
        
        if (!isValidEmail) {
            
             isValid = NO;
            
        }
        
    }
    
    return isValid;
    
}

-(NSString*)fetchSubWorkOrderActualHrsFromDataBaseForActualHourId :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
    
    NSEntityDescription *entityMechanicalSubWorkOrderActualHrs;
    NSFetchRequest *requestSubWorkOrderActualHrs;
    NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs;
    NSArray *sortDescriptorsSubWorkOrderActualHrs;
    NSString *strActualHourId;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    NSArray *arrActualHours = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if (arrActualHours.count==0) {
        
        strActualHourId = @"";
        
    } else {
        
        NSManagedObject *objTemp = arrActualHours[arrActualHours.count-1];
        
        strActualHourId = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"subWOActualHourId"]];
        
    }
    
    return strActualHourId;
    
}

-(NSString*)fetchWorkOrderFromDataBaseForMechanicalToFetchWorkOrderId :(NSString*)strWorkOrderNo{
    
    NSString *strId;
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderNo = %@",strWorkOrderNo];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        strId = @"";
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        strId = [NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"workorderId"]];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return strId;
    
}

-(NSManagedObject*)fetchMechanicalSubWorkOrderToStopJob :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesSubWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    //subWorkOrderId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesSubWorkOrder=arrAllObjWorkOrder[0];
        [matchesSubWorkOrder setValue:@"Stop" forKey:@"clockStatus"];
        NSError *error2;
        [context1 save:&error2];
        
    }
    
    return matchesSubWorkOrder;
    
}

-(NSString *)convertDate :(NSString*)strDateToConvert
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-dd-MM"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }

     if (newTime==nil) {

            NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
            [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
            newTime = [dateFormatterNew dateFromString:strDateToConvert];

        }
    if (newTime==nil) {

           NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
           [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
           //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
           [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
           newTime = [dateFormatterNew dateFromString:strDateToConvert];

       }
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"MM/dd/yyyy"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
    
    return finalTime;
}

-(NSMutableDictionary*)dateStartEndSales{
    
    NSDate *fromDateMinimum,*toDateMaximum;
    NSString *strfromDateMinimum,*strtoDateMaximum;

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSString*strConfigFromDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.FromDate"]];
    
    NSString*strConfigFromDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.FromDate"]];
    
    NSString *strConfigTooDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ToDate"]];
    
    NSString *strConfigTooDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ToDate"]];
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    [dateFormat setTimeZone:outputTimeZone];
    NSDate *fromDateSalesAuto=[dateFormat dateFromString:strConfigFromDateSalesAuto];
    NSDate *fromDateServiceAuto=[dateFormat dateFromString:strConfigFromDateServiceAuto];
    NSDate *tooDateSalesAuto=[dateFormat dateFromString:strConfigTooDateSalesAuto];
    NSDate *tooDateServiceAuto=[dateFormat dateFromString:strConfigTooDateServiceAuto];
    
    BOOL isFromDateSalesGreater=[self isGreaterDate:fromDateSalesAuto :fromDateServiceAuto];
    BOOL isTooDateSalesGreater=[self isGreaterDate:tooDateSalesAuto :tooDateServiceAuto];
    
    if (!isFromDateSalesGreater) {
        fromDateMinimum=fromDateSalesAuto;
        strfromDateMinimum = strConfigFromDateSalesAuto;
    } else {
        fromDateMinimum=fromDateServiceAuto;
        strfromDateMinimum = strConfigFromDateServiceAuto;
    }
    
    if (isTooDateSalesGreater) {
        toDateMaximum=tooDateSalesAuto;
        strtoDateMaximum = strConfigTooDateSalesAuto;
    } else {
        toDateMaximum=tooDateServiceAuto;
        strtoDateMaximum = strConfigTooDateServiceAuto;
    }
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:strfromDateMinimum forKey:@"fromDateMinimum"];
    [dictTemp setValue:strtoDateMaximum forKey:@"toDateMaximum"];
    
    return dictTemp;
    
}

// akshay 10/12/2019
-(NSString *)convertTime :(NSString*)strDateToConvert
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-dd-MM"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"MM/dd/yyyy HH:mm"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"HH:mm:ss"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {
        
        NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
        [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [dateFormatterNew setDateFormat:@"HH:mm:ss.SSS"];
        newTime = [dateFormatterNew dateFromString:strDateToConvert];
        
    }
    if (newTime==nil) {

           NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
           [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
           //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
           [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
           newTime = [dateFormatterNew dateFromString:strDateToConvert];

       }
    NSDateFormatter* dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatterNew setDateFormat:@"hh:mm a"];
    // newTime = [dateFormatterNew dateFromString:strDateToConvert];
    NSString* finalTime = [dateFormatterNew stringFromDate:newTime];
    
    return finalTime;
}

-(void)setDefaultNonBillableFromTodate{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:[self getCurrentDate] forKey:@"fromdateNonBillableiPad"];
    [defs setValue:[self getCurrentDate] forKey:@"todateNonBillableiPad"];
    [defs synchronize];
    
}


-(UIImage *)resizeImageGlobal:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([self drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

-(UIImage *)resizeImageGloballl:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation([self drawText:@"Saavan" inImage:img], compressionQuality);
    
    return [UIImage imageWithData:imageData];
    
}

-(NSData *)resizeImageToData:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation([self drawText:@"Saavan" inImage:img], compressionQuality);
    
    return imageData;
    
}


-(void)playAudioGlobal :(NSString*)strAudioName :(UIView*)viewToPlay{
 
    strAudioName=[self strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    
    [self playAudio:path :viewToPlay];
    
}

-(NSString*)returnAudioPath :(NSString*)strAudioName{
    
    strAudioName=[self strNameBackSlashIssue:strAudioName];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    
    return path;
    
}


-(NSArray*)fetchSubWorkOrderHelperFromDataBaseForMechanical :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderId,strSubWorkOrderIdGlobal];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    NSArray *arrHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrHelper count] == 0)
    {
        
    }
    else
    {
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return arrHelper;
    
}

-(BOOL)isAlreadyHelperInDb : (NSString*)strHelperIdGlobal :(NSString*)strWorkOrderId :(NSString*)strSubWorkOrderIdGlobal{
 
    BOOL isAlreadyThere;
    
    isAlreadyThere=NO;
    
    NSArray *arrOfSubWorkServiceHelper = [self fetchSubWorkOrderHelperFromDataBaseForMechanical:strWorkOrderId :strSubWorkOrderIdGlobal];
    
    for (int k=0; k<arrOfSubWorkServiceHelper.count; k++) {
        
        NSManagedObject *objTemp=arrOfSubWorkServiceHelper[k];
        
        NSString *strHelperId=[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"employeeNo"]];
        
        if ([strHelperIdGlobal isEqualToString:strHelperId]) {
            
            isAlreadyThere=YES;
            break;
            
        }
        
    }
    
    return isAlreadyThere;
    
}

-(NSString *)getCompanyKey
{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
//    NSString *strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
//    NSString *strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
//    NSString *strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
//    NSString *strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];

    return strCompanyKey;
    
}

-(NSString *)getUserName
{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    //    NSString *strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    //    NSString *strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
        NSString *strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    //    NSString *strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    
    return strUserName;
    
}

-(NSString *)getCompanyId
{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    //    NSString *strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    //    NSString *strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    NSString *strCompanyId       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    
    //NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    //    NSString *strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    
    return strCompanyId;
    
}

-(NSString *)getEmployeeId
{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    NSString *strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    //    NSString *strEmployeeNoLoggedIn=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    //NSString *strCompanyId       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyId"]];
    
    //NSString *strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    //    NSString *strEmpName          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    
    return strEmpID;
    
}

-(NSString *)getEmployeeBranchSysName
{
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLead valueForKey:@"LoginDetails"];
    
    //NSString *strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    NSString *strEmployeeBranchSysName=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchSysName"]];

    
    return strEmployeeBranchSysName;
    
}

-(NSString*)strFullNameForNSManagedObject :(NSManagedObject*)dictData{
    
    NSMutableArray *arrFullName=[[NSMutableArray alloc] init];
    
    NSString *strFN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"firstName"]];
    NSString *strMN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"middleName"]];
    NSString *strLN = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"lastName"]];
    
    if ((strFN.length>0) && ![strFN isEqualToString:@"(null)"] &&![strFN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strFN];
        
    }
    if ((strMN.length>0) && ![strMN isEqualToString:@"(null)"] &&![strMN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strMN];
        
    }
    if ((strLN.length>0) && ![strLN isEqualToString:@"(null)"] && ![strLN isEqualToString:@"<null>"]) {
        
        [arrFullName addObject:strLN];
        
    }
    
    NSString *strFullName;
    
    if (arrFullName.count>0) {
        
        strFullName = [arrFullName componentsJoinedByString:@" "];
        
    }else{
        
        strFullName = @"N/A";
        
    }
    
    return strFullName;
}


-(NSString*)strCombinedAddressServiceForNSManagedObject :(NSManagedObject*)managedObjectData
{
    
    NSArray *keys = [[[managedObjectData entity] attributesByName] allKeys];
    NSDictionary *dictData = [managedObjectData dictionaryWithValuesForKeys:keys];
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"serviceAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceAddress1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceAddress1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"servicesAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"servicesAddress1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"servicesAddress1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceAddress2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceAddress2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceAddress2"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceCityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceCityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceCityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceCity"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceCity"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceCity"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceStateId"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceStateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceStateId"]]]];
            
        }
        
    }else if ([arrTempKeys containsObject:@"serviceState"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceState"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceState"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceCountryName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceCountryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceZipcode"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceZipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceZipcode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"serviceZip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceZip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"serviceZip"]]];
            
        }
        
    }
    
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[self getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}

//Billing
-(NSString*)strCombinedAddressBillingForNSManagedObject :(NSManagedObject*)managedObjectData
{
    NSArray *keys = [[[managedObjectData entity] attributesByName] allKeys];
    NSDictionary *dictData = [managedObjectData dictionaryWithValuesForKeys:keys];
    
    NSArray *arrTempKeys = [dictData allKeys];
    
    NSMutableArray *arrOfTempAddress=[[NSMutableArray alloc]init];
    
    if ([arrTempKeys containsObject:@"billingAddress1"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingAddress1"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingAddress1"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingAddress2"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingAddress2"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingAddress2"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingCityName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingCityName"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingCityName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingCity"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingCity"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingCity"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingStateId"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingStateId"]].length==0)) {
            
            [arrOfTempAddress addObject:[self strStatNameFromID:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingStateId"]]]];
            
        }
        
    }
    else if ([arrTempKeys containsObject:@"billingState"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingState"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingState"]]];
            
        }
        
    }
    
    
    
    
    if ([arrTempKeys containsObject:@"billingCountryName"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingCountryName"]].length==0)) {
            
            //[arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"CountryName"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingZipcode"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingZipcode"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingZipcode"]]];
            
        }
        
    }
    
    if ([arrTempKeys containsObject:@"billingZip"]) {
        
        if (!([NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingZip"]].length==0)) {
            
            [arrOfTempAddress addObject:[NSString stringWithFormat:@"%@", [dictData valueForKey:@"billingZip"]]];
            
        }
        
    }
    
    NSString *strCombinedAddress=@"";
    
    if (arrOfTempAddress.count>0) {
        
        strCombinedAddress=[arrOfTempAddress componentsJoinedByString:@", "];
        //[self getLocationFromAddressString:strCombinedAddress];
        
    }
    
    return strCombinedAddress;
    
}

-(NSArray*)arrOfFilteredData :(NSArray*)arrData :(NSString*)strId :(NSString*)strIdType{
    
    NSArray *arrTemp;
    
    NSMutableArray *arrTempLocal = [[NSMutableArray alloc]init];
    
    for (int k=0; k<arrData.count; k++) {
        
        NSDictionary *dicctData = arrData[k];
        
        NSString *strBranchId = [NSString stringWithFormat:@"%@",[dicctData valueForKey:strIdType]];
        
        if ([strBranchId isEqualToString:strId]) {
            
            [arrTempLocal addObject:dicctData];
            
        }
        
    }
    
    arrTemp = arrTempLocal;
    
    return arrTemp;
    
}

-(NSArray *)getCategoryDeptWiseGlobal
{
    NSDictionary  *dictSalesLeadMaster;
    NSUserDefaults *defs2=[NSUserDefaults standardUserDefaults];
    dictSalesLeadMaster=[defs2 valueForKey:@"LeadDetailMaster"];
    NSArray *arrDeptName=[dictSalesLeadMaster valueForKey:@"BranchMasters"];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSString *strBranchSysName;
    strBranchSysName = [self getEmployeeBranchSysName];//[defs valueForKey:@"branchSysName"];
    NSMutableArray *arrDepartment;
    arrDepartment=[[NSMutableArray alloc]init];
    
    for (int i=0;i<arrDeptName.count; i++)
    {
        NSDictionary *dictBranch=[arrDeptName objectAtIndex:i];
        NSArray *arry=[dictBranch valueForKey:@"Departments"];
        for (int j=0; j<arry.count; j++)
        {
            NSDictionary *dict=[arry objectAtIndex:j];
            if ([strBranchSysName isEqualToString:[dictBranch valueForKey:@"SysName"]])
            {
                NSString *strIsActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
                if ([strIsActive isEqualToString:@"false"]||[strIsActive isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    [arrDepartment addObject:dict];
                }
            }
        }
    }
    
    NSMutableArray *arrCategoryBranchWise;
    arrCategoryBranchWise=[[NSMutableArray alloc]init];
    NSUserDefaults *defsNew=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defsNew valueForKey:@"MasterSalesAutomation"];
    NSArray *arrCtgryNew=[dictMasters valueForKey:@"Categories"];
    
    for (int i=0; i<arrDepartment.count; i++)
    {
        NSDictionary *dict=[arrDepartment objectAtIndex:i];
        NSString *strDeptSysName;
        strDeptSysName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
        for (int j=0; j<arrCtgryNew.count; j++)
        {
            NSDictionary *dictNew=[arrCtgryNew objectAtIndex:j];
            
            if ([[dictNew valueForKey:@"DepartmentSysName"] isEqualToString:strDeptSysName])
            {
                if ([[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"true"]||[[NSString stringWithFormat:@"%@",[dictNew valueForKey:@"IsActive"] ]isEqualToString:@"True"])
                {
                    
                    [arrCategoryBranchWise addObject:dictNew];
                }
                
            }
        }
    }
    return arrCategoryBranchWise;
}

-(NSString*)getTotalPrice :(NSMutableArray*)arrPricing {
 
    double finalTotal = 0.0;
    
    for (int k=0; k<arrPricing.count; k++) {
        
        NSManagedObject *objTemp = arrPricing[k];
        
        NSString *strTotal = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"total"] ];
        
        double totalvalue = [strTotal doubleValue];
        
        finalTotal = finalTotal+totalvalue;
        
    }
    
    NSString *strFinalTotal = [NSString stringWithFormat:@"%.2f",finalTotal];
    
    return strFinalTotal;
    
}
    
- (NSDictionary *)getInspectorDetails:(NSString *)strInspectorId
{
    NSDictionary *dictInspectorDetails;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSArray* arrEmployee=[defs valueForKey:@"EmployeeList"];
    
    if ([arrEmployee isEqual:@""]|| arrEmployee.count==0 || [arrEmployee isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"dsfdsfdsfdsfdsfdsf");
    }
    else
    {
        for (int i=0; i<arrEmployee.count; i++)
        {
            dictInspectorDetails=[arrEmployee objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",[dictInspectorDetails valueForKey:@"EmployeeId"]]isEqualToString:strInspectorId])
            {
                NSString *str;
                str=[dictInspectorDetails valueForKey:@"FullName"];
                break;
                /*  NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
                 [defs setObject:dictInspectorDetails forKey:@"inspectorDetail"];
                 [defs synchronize];*/
            }
        }
    }
    
    return dictInspectorDetails;
}

-(NSString*)fetchLeadStatus :(NSString*)strWdoLeadId{
    
    NSString *strStatus = @"";
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesSubWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    //subWorkOrderId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@",strWdoLeadId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        strStatus = @"";
        
    }
    else
    {
        
        matchesSubWorkOrder=arrAllObjWorkOrder[0];
        
        NSString *strStatusNameLocal = [NSString stringWithFormat:@"%@",[matchesSubWorkOrder valueForKey:@"statusSysName"]];
        NSString *strStageNameLocal = [NSString stringWithFormat:@"%@",[matchesSubWorkOrder valueForKey:@"stageSysName"]];
        
        if ([strStageNameLocal caseInsensitiveCompare:@"won"] == NSOrderedSame && [strStatusNameLocal caseInsensitiveCompare:@"complete"] == NSOrderedSame) {
            
            strStatus = @"complete";
            
        } else {
            
            strStatus = @"";
            
        }
        
    }
    
    return strStatus;
    
}

-(NSString*)strTechSign :(NSString*)strSignUrl{
 
    NSString *result;
    NSRange equalRange = [strSignUrl rangeOfString:@"Documents" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [strSignUrl substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=strSignUrl;
    }
    return result;
}

-(void)updateWoVersionNo :(NSString*)strWorkOrderId{

    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
        NSString *VersionDatee = VersionDate;
        NSString *VersionNumberr = VersionNumber;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                                encoding:NSUTF8StringEncoding];
        
        [matchesWorkOrder setValue:VersionDatee forKey:@"versionDate"];
        [matchesWorkOrder setValue:VersionNumberr forKey:@"versionNumber"];
        [matchesWorkOrder setValue:DeviceVersion forKey:@"deviceVersion"];
        [matchesWorkOrder setValue:DeviceName forKey:@"deviceName"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    
}

-(void)updateLeadVersion :(NSString*)strLeadId{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadId];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
        NSString *VersionDatee = VersionDate;
        NSString *VersionNumberr = VersionNumber;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                                encoding:NSUTF8StringEncoding];
        
        [matchesWorkOrder setValue:VersionDatee forKey:@"versionDate"];
        [matchesWorkOrder setValue:VersionNumberr forKey:@"versionNumber"];
        [matchesWorkOrder setValue:DeviceVersion forKey:@"deviceVersion"];
        [matchesWorkOrder setValue:DeviceName forKey:@"deviceName"];
        
        NSError *error1;
        [context1 save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)updateWdoNpmaIsMailSent :(NSString*)strWoIdd{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWoIdd];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        for (int i=0; i<arrAllObjWorkOrder.count; i++) {
            
            matchesWorkOrder=arrAllObjWorkOrder[i];
            [matchesWorkOrder setValue:@"false" forKey:@"isMailSent"];

            NSError *error1;
            [context1 save:&error1];
            
        }

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)updateWoIsMailSent :(NSString*)strWoIdd :(NSString*)strTrueFalse{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWoIdd];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        
        for (int i=0; i<arrAllObjWorkOrder.count; i++) {
            
            matchesWorkOrder=arrAllObjWorkOrder[i];
            [matchesWorkOrder setValue:strTrueFalse forKey:@"isMailSent"];

            NSError *error1;
            [context1 save:&error1];
            
        }

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)removeImageFromDocumentDirectory:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
        NSLog(@"Deleted Image From Path -:%@ ",filePath);
        
    }
    else
    {
       
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        
    }
    
}

-(CGSize)getHeightForString :(NSString*)strString :(int)fontSize :(CGFloat)widthh{
 
    CGSize s = [strString sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(widthh, MAXFLOAT)];
    
    return s;
    
}
-(NSDate*)getDueDate:(NSString*)strDueDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateDuee = [dateFormat dateFromString:strDueDate];
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss.SSS"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    return dateDuee;
}


-(NSDate*)getDueDateNeww:(NSString*)strDueDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *dateDuee = [dateFormat dateFromString:strDueDate];
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    if(dateDuee == nil)
    {
        [dateFormat setDateFormat:@"MM/dd/yyyy HH:mm:ss.SSS"];
        dateDuee = [dateFormat dateFromString:strDueDate];
    }
    return dateDuee;
}

-(NSDate*)getCurrentDateAkshay
{
    return [NSDate date];
}
-(NSString *)getModifiedDate{
    
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strModifiedDate = [formatterDate stringFromDate:[NSDate date]];
    return strModifiedDate;
    
}
-(NSDate*)getCompareDate
{
    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MM/dd/yyyy"];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterDate1 = [[NSDateFormatter alloc] init];
    [formatterDate1 setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [formatterDate dateFromString:strDate];
    
    return date;
}

-(NSString*)splitString :(NSString*)str :(NSString*)strSplitBy{
    
    NSString *strLast;
    
    NSArray *arrTemp = [str componentsSeparatedByString:strSplitBy];
    
    if (arrTemp.count > 0) {
        
        strLast = [arrTemp lastObject];
        
        return strLast;
        
    }else{
        
        return strLast;
        
    }
    
}

-(NSManagedObject*)fetchLeadDetailFromDb :(NSString*)strLeadIdLocal{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    entityWorkOrder=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIdLocal];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    return matchesWorkOrder;
    
}
-(NSString*)getSourceNameMultiple :(NSArray*)arrSourceIdSelected :(NSArray*)arrayOfSource{
    
    NSString *str = @"";

    NSMutableArray *arrSourceSelectedSysName = [[NSMutableArray alloc]init];
     
     if (![arrSourceIdSelected isKindOfClass:[NSArray class]]) {
         
         arrSourceIdSelected = nil;
         
     }
     
     if (arrSourceIdSelected.count>0) {
         
         for (int k=0; k<arrSourceIdSelected.count; k++) {
             
             [arrSourceSelectedSysName addObject:[NSString stringWithFormat:@"%@",arrSourceIdSelected[k]]];
             
         }

     }
     
     NSMutableArray *arrDataTblView = [[NSMutableArray alloc]init];
     
     [arrDataTblView addObjectsFromArray:arrayOfSource];
     
     if(arrSourceSelectedSysName.count>0)
     {
         for (int k1=0; k1<arrSourceSelectedSysName.count; k1++) {
             
             NSString *strLocalSysNameSelected = [NSString stringWithFormat:@"%@",arrSourceSelectedSysName[k1]];
             
             for (int k2=0; k2<arrDataTblView.count; k2++) {
                 
                 NSDictionary *dictDataLocal=arrDataTblView[k2];
                 
                 NSString *strLocalSysName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"SourceId"]];
                 NSString *strLocalName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"Name"]];
                 
                 if ([strLocalSysNameSelected isEqualToString:strLocalSysName]) {
                     
                     if(str.length>0)
                     {
                         str = [NSString stringWithFormat:@"%@, %@",str,strLocalName];
                     }
                     else
                     {
                         str = [NSString stringWithFormat:@"%@",strLocalName];
                     }
                     
                 }
                 
             }
             
         }
         
     }
         return str;
    
}

-(NSString*)getBranchNameMultiple :(NSArray*)arrBranchSysName {
 
    NSString *str = @"";
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSArray *arrayBranch = [[defsLogindDetail objectForKey:@"LeadDetailMaster"] valueForKey:@"BranchMastersAll"];
    
    NSMutableArray *arrDataTblView = [[NSMutableArray alloc]init];
    NSMutableArray *arrOfBranchSysNameSelected = [[NSMutableArray alloc]init];

    [arrDataTblView addObjectsFromArray:arrayBranch];
    
    
    if (arrBranchSysName.count>0) {
        
        //[arrOfBranchSysNameSelected addObjectsFromArray:arrBranchSysName];
        
        for (int k=0; k<arrBranchSysName.count; k++) {
            
            [arrOfBranchSysNameSelected addObject:[NSString stringWithFormat:@"%@",arrBranchSysName[k]]];
            
        }
        
    }
    
    
    if(arrOfBranchSysNameSelected.count>0)
    {
        for (int k1=0; k1<arrOfBranchSysNameSelected.count; k1++) {
            
            NSString *strLocalSysNameSelected = arrOfBranchSysNameSelected[k1];
            
            for (int k2=0; k2<arrDataTblView.count; k2++) {
                
                NSDictionary *dictDataLocal=arrDataTblView[k2];
                
                NSString *strLocalSysName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"SysName"]];
                NSString *strLocalName = [NSString stringWithFormat:@"%@",[dictDataLocal valueForKey:@"Name"]];
                
                if ([strLocalSysNameSelected isEqualToString:strLocalSysName]) {
                    
                    if(str.length>0)
                    {
                        str = [NSString stringWithFormat:@"%@,%@",str,strLocalName];
                    }
                    else
                    {
                        str = [NSString stringWithFormat:@"%@",strLocalName];
                    }
                    
                }
                
            }
            
        }
        
    }
    
    return str;
    
}
-(NSMutableAttributedString *)getAttributedString : (NSString *)strHtmlDesc WithFontStyle : (UIFont*)fontStyle
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithData:[strHtmlDesc dataUsingEncoding:NSUTF8StringEncoding]
                                                                              options:@{
                                                                                  NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                   documentAttributes:nil error:nil];
    [text addAttributes:@{NSFontAttributeName: fontStyle} range:NSMakeRange(0, text.length)];
    
    return text;
    
}

-(void)checkAppVersionLogic : (UIViewController *)viewToShowAlert
{

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    BOOL IsDeviceVersionForceUpdate=[[dictLoginData valueForKey:@"DeviceVersionForceUpdate"] boolValue];
    
    if (IsDeviceVersionForceUpdate) {
        
        NSString *strBundleIdentifier=@"com.infocrats.Pestream";
        
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", strBundleIdentifier]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [NSURLConnection sendAsynchronousRequest:request
         
                                           queue:[NSOperationQueue mainQueue]
         
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   
                                   if (!error) {
                                       
                                       NSError* parseError;
                                       
                                       NSDictionary *appMetadataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                                       
                                       NSArray *resultsArray = (appMetadataDictionary)?[appMetadataDictionary objectForKey:@"results"]:nil;
                                       
                                       NSDictionary *resultsDic = [resultsArray firstObject];
                                       
                                       if (resultsDic) {
                                           
                                           // compare version with your apps local version
                                           
                                           NSString *iTunesVersion = [resultsDic objectForKey:@"version"];
                                           
                                           NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)@"CFBundleShortVersionString"];
                                           
                                           if (iTunesVersion && [appVersion compare:iTunesVersion] != NSOrderedDescending&&[appVersion compare:iTunesVersion] != NSOrderedSame) {{
                                               
                                               UIAlertController * alert=   [UIAlertController
                                                                             
                                                                             alertControllerWithTitle:@"Update"
                                                                             
                                                                             message:@"A new version of this app is available."
                                                                             
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                               
                                               
                                               UIAlertAction* updateBtn = [UIAlertAction
                                                                           
                                                                           actionWithTitle:@"Update"
                                                                           
                                                                           style:UIAlertActionStyleDefault
                                                                           
                                                                           handler:^(UIAlertAction * action)
                                                                           
                                                                           {
                                                                               
                                                                               //                                                                                                                                                      NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/in/app/apple-store/id%@?mt=8",strAppStoreId];
                                                                               
                                                                               NSString *iTunesLink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/in/app/id1174328672?mt=8"];
                                                                               
                                                                               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                                           }];
                                               
                                               [alert addAction:updateBtn];
                                               
                                               [viewToShowAlert presentViewController:alert animated:YES completion:nil];
                                               
                                           }}
                                       }
                                   } else {
                                       // error occurred with http(s) request
                                       NSLog(@"error occurred communicating with iTunes");
                                   }
                               }];
        
    }
    
}

-(void)diskSpaceCheck : (UIViewController *)viewToShowAlert{
    
    float freeSpace = 0.0f;
    NSError *error = nil;
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:documentsDirectory error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemFreeSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];
        //NSLog(@"%@",fileSystemFreeSizeInBytes);
        freeSpace = [fileSystemFreeSizeInBytes floatValue];
    } else {
        //Handle error
    }
    //NSLog(@"Free Space===%f",freeSpace/(1024 * 1024));
    
    if (freeSpace/(1024 * 1024)<100) {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Storage Almost Full"
                                   message:@"Please Manage your storage as it is almost full."
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                                  
                              }];
        [alert addAction:yes];
        [viewToShowAlert presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(BOOL)logicForDownloadingMasters{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL yesFromAppDelegate=[defs boolForKey:@"fromAppDelegate"];
    
    if (yesFromAppDelegate) {
        
        NSUserDefaults *defsDate=[NSUserDefaults standardUserDefaults];
        
        NSString *strDateSaved=[NSString stringWithFormat:@"%@",[defsDate valueForKey:@"DateDownloadMasters"]];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate* ReadingDate = [dateFormatter dateFromString:strDateSaved];
        NSString *strCurrentDate = [dateFormatter stringFromDate:[NSDate date]];
        NSDate* CurrentDate = [dateFormatter dateFromString:strCurrentDate];
        
        NSComparisonResult result = [ReadingDate compare:CurrentDate];
        
        BOOL toFecthMasters;
        toFecthMasters=NO;
        
        if(result == NSOrderedDescending)
        {
            NSLog(@"strDateSaved is Greater");//
            toFecthMasters=YES;
        }
        else if(result == NSOrderedAscending)
        {
            NSLog(@"CurrentDate is greater");//
            toFecthMasters=YES;
        }
        else
        {
            NSLog(@"Equal date");//
            toFecthMasters=NO;
        }
        if (ReadingDate==nil) {
            
            toFecthMasters=YES;
            
        }
        
        if (toFecthMasters) {
            
            [defsDate setValue:strCurrentDate forKey:@"DateDownloadMasters"];
            [defsDate synchronize];
            
            return true;
            
        }else{
            
            return false;

        }
    }else{
     
        return false;
        
    }
    
}

-(void)setCrmMastersInUserDefaults :(NSArray*)arrOfLeadStatusMasters :(NSDictionary*)responseDictLocal{
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
    NSArray *sortDescriptorsNew = [NSArray arrayWithObject:brandDescriptor];
    arrOfLeadStatusMasters = [arrOfLeadStatusMasters sortedArrayUsingDescriptors:sortDescriptorsNew];
    NSMutableArray *arrDelete=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfLeadStatusMasters.count; k++) {
        
        NSDictionary *dictData=arrOfLeadStatusMasters[k];
        
        BOOL isActive=[[dictData valueForKey:@"IsActive"] boolValue];
        
        if (!isActive) {
            
            [arrDelete addObject:dictData];
            
        }
        
    }
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    
    [arrTemp addObjectsFromArray:arrOfLeadStatusMasters];
    
    if (!(arrDelete.count==0)) {
        [arrTemp removeObjectsInArray:arrDelete];
    }
    
    arrOfLeadStatusMasters=arrTemp;
    
    NSUserDefaults *defLeadStatusMasters =[NSUserDefaults standardUserDefaults];
    [defLeadStatusMasters setObject:arrOfLeadStatusMasters forKey:@"LeadStatusMasters"];
    [defLeadStatusMasters setObject:responseDictLocal forKey:@"TotalLeadCountResponse"];
    [defLeadStatusMasters synchronize];
    
    
    //  Web Lead Status Master
    
    NSArray *arrOfLeadStatusMasters1 =[responseDictLocal valueForKey:@"WebLeadStatusMasters"];
    
    NSSortDescriptor *brandDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
    NSArray *sortDescriptorsNew1 = [NSArray arrayWithObject:brandDescriptor1];
    arrOfLeadStatusMasters1 = [arrOfLeadStatusMasters1 sortedArrayUsingDescriptors:sortDescriptorsNew1];
    NSMutableArray *arrDelete1=[[NSMutableArray alloc]init];
    for (int k=0; k<arrOfLeadStatusMasters1.count; k++) {
        
        NSDictionary *dictData1=arrOfLeadStatusMasters1[k];
        
        BOOL isActive1=[[dictData1 valueForKey:@"IsActive"] boolValue];
        
        if (!isActive1) {
            
            [arrDelete1 addObject:dictData1];
            
        }
        
    }
    NSMutableArray *arrTempLocal1=[[NSMutableArray alloc]init];
    
    [arrTempLocal1 addObjectsFromArray:arrOfLeadStatusMasters1];
    
    if (!(arrDelete1.count==0)) {
        [arrTempLocal1 removeObjectsInArray:arrDelete1];
    }
    
    arrOfLeadStatusMasters1=arrTempLocal1;
    
    NSUserDefaults *defLeadStatusMasters1 =[NSUserDefaults standardUserDefaults];
    [defLeadStatusMasters1 setObject:arrOfLeadStatusMasters1 forKey:@"WebLeadStatusMasters"];
    [defLeadStatusMasters1 synchronize];
    
    // End Web Lead Status Masters
    
    // kch gadbad thi do baar service call ho rahoi thi crm master ki to maine ek kr di hai
    
    NSUserDefaults *defs123=[NSUserDefaults standardUserDefaults];
    [defs123 setValue:responseDictLocal forKey:@"LeadDetailMaster"];
    [defs123 synchronize];
    //MARK: - Need TO Change
    //AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
   // [defaultAf setLeadDetails:defs123];
    
}

-(NSDictionary*)convertNullArray :(NSDictionary*)dictLocalResponse{
 
    NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
    [dictTempResponse setObject:dictLocalResponse forKey:@"response"];
    NSDictionary *dict=[[NSDictionary alloc]init];
    dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
    NSMutableDictionary *dictData=[dict valueForKey:@"response"];
    dictLocalResponse =dictData;
    
    return dictLocalResponse;
    
}

-(NSDictionary*)convertNullArrayNew :(NSDictionary*)dictLocalResponse{
 
    NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
    [dictTempResponse setObject:dictLocalResponse forKey:@"response"];
    NSDictionary *dict=[[NSDictionary alloc]init];
    dict=[self nestedDictionaryByReplacingNullsWithNilReplicaForArray:dictLocalResponse];
    NSMutableDictionary *dictData=[dict valueForKey:@"response"];
    dictLocalResponse =dictData;
    
    return dictLocalResponse;
    
}

-(NSMutableDictionary*)dictMenuOptions{
 
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSArray *slideMenuArray;
    NSArray *slideMenuIconsArray;
    
        slideMenuArray=@[@"Profile",
                         @"Dashboard",
                         @"Appointment",
                         @"Manage Opportunity/Lead",
                         @"Signed Agreement",
                         @"Task",
                         @"Activity",
                         @"Contacts List",
                         @"Settings",
                         @"Opportunity/Lead History",
                         @"Sync Masters",
                         @"Day Clock In/Out",
                         @"Statistics",
                         @"Nearby",
                         @"Change Password",
                         @"View schedule",
                         @"Switch Branch",
                         @"Logout"];
        
        slideMenuIconsArray=@[@"edit_profile_nav_icon2.png",
                              @"dashboard.png",
                              @"appointment.png",
                              @"leads.png",
                              @"signed_agreements.png",
                              @"task1.png",
                              @"activity.png",
                              @"contact_list.png",
                              @"services.png",
                              @"lead_history_icon.png",
                              @"sync master.png",
                              @"clock",
                              @"statistics.png",
                              @"nearby.png",
                              @"change_password.png",
                              @"clock",
                              @"services.png",
                              @"logout.png"];
    
    NSMutableDictionary *dictTemp;
    dictTemp=[[NSMutableDictionary alloc]init];
    
    [dictTemp setObject: slideMenuArray forKey: @"slideMenuArray"];
    [dictTemp setObject: slideMenuIconsArray forKey: @"slideMenuIconsArray"];

    return dictTemp;
    
}

-(void)checkIfAppUpdateAvilable {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"AppUpdateAvailable"];
    [defs synchronize];
    
    NSString *strBundleIdentifier=@"com.infocrats.Pestream";
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", strBundleIdentifier]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
     
                                       queue:[NSOperationQueue mainQueue]
     
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (!error) {
                                   
                                   NSError* parseError;
                                   
                                   NSDictionary *appMetadataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                                   
                                   NSArray *resultsArray = (appMetadataDictionary)?[appMetadataDictionary objectForKey:@"results"]:nil;
                                   
                                   NSDictionary *resultsDic = [resultsArray firstObject];
                                   
                                   if (resultsDic) {
                                       
                                       // compare version with your apps local version
                                       
                                       NSString *iTunesVersion = [resultsDic objectForKey:@"version"];
                                       
                                       NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)@"CFBundleShortVersionString"];
                                       
                                       if (iTunesVersion && [appVersion compare:iTunesVersion] != NSOrderedDescending&&[appVersion compare:iTunesVersion] != NSOrderedSame) {{
                                           
                                           NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                           [defs setBool:YES forKey:@"AppUpdateAvailable"];
                                           [defs synchronize];
                                           
                                       }}
                                   }
                               } else {
                                   // error occurred with http(s) request
                                   NSLog(@"error occurred communicating with iTunes");
                               }
                           }];
    
}




//============================================================================
//============================================================================
#pragma mark--- New DashBoard Swift To Obkective C
//============================================================================
//============================================================================


//============================================================================
//============================================================================
#pragma mark- CRM MASTERS
//============================================================================
//============================================================================

-(void)getLeadCountNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime CRM Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             NSArray *arrOfLeadStatusMasters =[ResponseDict valueForKey:@"LeadStatusMasters"];
             
             NSArray *arrAllKeyPresentInDictResponse = [ResponseDict allKeys];
             if ([arrAllKeyPresentInDictResponse containsObject:@"ExceptionMessage"]) {
                 NSLog(@"Exception Something went wrong------");
             } else {
                 NSLog(@"Response recieved successfully-------");
             }
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 strException=@"";
             }
             if (strException.length==0) {
                 NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                 NSArray *sortDescriptorsNew = [NSArray arrayWithObject:brandDescriptor];
                 arrOfLeadStatusMasters = [arrOfLeadStatusMasters sortedArrayUsingDescriptors:sortDescriptorsNew];
                 NSMutableArray *arrDelete=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOfLeadStatusMasters.count; k++) {
                     
                     NSDictionary *dictData=arrOfLeadStatusMasters[k];
                     
                     BOOL isActive=[[dictData valueForKey:@"IsActive"] boolValue];
                     
                     if (!isActive) {
                         
                         [arrDelete addObject:dictData];
                         
                     }
                     
                 }
                 NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                 
                 [arrTemp addObjectsFromArray:arrOfLeadStatusMasters];
                 
                 if (!(arrDelete.count==0)) {
                     [arrTemp removeObjectsInArray:arrDelete];
                 }
                 
                 arrOfLeadStatusMasters=arrTemp;
                 
                 NSUserDefaults *defLeadStatusMasters =[NSUserDefaults standardUserDefaults];
                 [defLeadStatusMasters setObject:arrOfLeadStatusMasters forKey:@"LeadStatusMasters"];
                 [defLeadStatusMasters setObject:ResponseDict forKey:@"TotalLeadCountResponse"];
                 [defLeadStatusMasters synchronize];
                 
                 
                 //  Web Lead Status Master
                 
                 NSArray *arrOfLeadStatusMasters1 =[ResponseDict valueForKey:@"WebLeadStatusMasters"];
                 
                 NSSortDescriptor *brandDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                 NSArray *sortDescriptorsNew1 = [NSArray arrayWithObject:brandDescriptor1];
                 arrOfLeadStatusMasters1 = [arrOfLeadStatusMasters1 sortedArrayUsingDescriptors:sortDescriptorsNew1];
                 NSMutableArray *arrDelete1=[[NSMutableArray alloc]init];
                 for (int k=0; k<arrOfLeadStatusMasters1.count; k++) {
                     
                     NSDictionary *dictData1=arrOfLeadStatusMasters1[k];
                     
                     BOOL isActive1=[[dictData1 valueForKey:@"IsActive"] boolValue];
                     
                     if (!isActive1) {
                         
                         [arrDelete1 addObject:dictData1];
                         
                     }
                     
                 }
                 NSMutableArray *arrTempLocal1=[[NSMutableArray alloc]init];
                 
                 [arrTempLocal1 addObjectsFromArray:arrOfLeadStatusMasters1];
                 
                 if (!(arrDelete1.count==0)) {
                     [arrTempLocal1 removeObjectsInArray:arrDelete1];
                 }
                 
                 arrOfLeadStatusMasters1=arrTempLocal1;
                 
                 NSUserDefaults *defLeadStatusMasters1 =[NSUserDefaults standardUserDefaults];
                 [defLeadStatusMasters1 setObject:arrOfLeadStatusMasters1 forKey:@"WebLeadStatusMasters"];
                 [defLeadStatusMasters1 synchronize];
                 
                 // End Web Lead Status Masters
                 
                 // kch gadbad thi do baar service call ho rahoi thi crm master ki to maine ek kr di hai
                 
                 NSUserDefaults *defs123=[NSUserDefaults standardUserDefaults];
                 [defs123 setValue:ResponseDict forKey:@"LeadDetailMaster"];
                 [defs123 synchronize];
                 AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                 [defaultAf setLeadDetails:jsonData];
                 
             } else {
                 ResponseDict=nil;
             }
            
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}


// --------------

void(^getDataFromSwiftCallback)(BOOL success, NSDictionary *response, NSError *error);

- (void)getDataFromSwift:(NSString *)url :(NSString *)requestDataa withCallback:(swiftWebServicePostCompletionBlock)callback
{
    getDataFromSwiftCallback = callback;
    [self ongetDataFromSwiftResponse:nil withSuccess:YES error:nil withUrl:url withData:requestDataa];
}

- (void)ongetDataFromSwiftResponse:(NSDictionary *)response withSuccess:(BOOL)success error:(NSError *)error withUrl:(NSString*)stringUrl withData:(NSString*)requestData
{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    [requestLocal addValue:[self strEmpLoginId] forHTTPHeaderField:@"EmployeeId"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
     {
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
        NSLog(@"executionTime Objective C -------%@--------- API ======== %f", stringUrl , executionTime);
        
        NSData* jsonData = [NSData dataWithData:data];
        
        if ([requestData isEqualToString:@"Employee"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Employee List API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (ResponseDict.count==0) {
                
            } else {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                
                NSString *strException;
                @try {
                    strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                if (![strException isKindOfClass:[NSString class]]) {
                    
                    strException=@"";
                    
                }
                if (strException.length==0) {
                    
                    NSUserDefaults *defsResponse=[NSUserDefaults standardUserDefaults];
                    [defsResponse setObject:dictData forKey:@"EmployeeList"];
                    [defsResponse synchronize];
                    
                } else {
                    ResponseDict=nil;
                }
            }
            
        }else if ([requestData isEqualToString:@"CRM"]) {
            
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime CRM Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSArray *arrOfLeadStatusMasters =[ResponseDict valueForKey:@"LeadStatusMasters"];
            
            NSArray *arrAllKeyPresentInDictResponse = [ResponseDict allKeys];
            if ([arrAllKeyPresentInDictResponse containsObject:@"ExceptionMessage"]) {
                NSLog(@"Exception Something went wrong------");
            } else {
                NSLog(@"Response recieved successfully-------");
            }
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
            }
            if (![strException isKindOfClass:[NSString class]]) {
                strException=@"";
            }
            if (strException.length==0) {
                NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                NSArray *sortDescriptorsNew = [NSArray arrayWithObject:brandDescriptor];
                arrOfLeadStatusMasters = [arrOfLeadStatusMasters sortedArrayUsingDescriptors:sortDescriptorsNew];
                NSMutableArray *arrDelete=[[NSMutableArray alloc]init];
                for (int k=0; k<arrOfLeadStatusMasters.count; k++) {
                    
                    NSDictionary *dictData=arrOfLeadStatusMasters[k];
                    
                    BOOL isActive=[[dictData valueForKey:@"IsActive"] boolValue];
                    
                    if (!isActive) {
                        
                        [arrDelete addObject:dictData];
                        
                    }
                    
                }
                NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
                
                [arrTemp addObjectsFromArray:arrOfLeadStatusMasters];
                
                if (!(arrDelete.count==0)) {
                    [arrTemp removeObjectsInArray:arrDelete];
                }
                
                arrOfLeadStatusMasters=arrTemp;
                
                NSUserDefaults *defLeadStatusMasters =[NSUserDefaults standardUserDefaults];
                [defLeadStatusMasters setObject:arrOfLeadStatusMasters forKey:@"LeadStatusMasters"];
                [defLeadStatusMasters setObject:ResponseDict forKey:@"TotalLeadCountResponse"];
                [defLeadStatusMasters synchronize];
                
                
                //  Web Lead Status Master
                
                NSArray *arrOfLeadStatusMasters1 =[ResponseDict valueForKey:@"WebLeadStatusMasters"];
                
                NSSortDescriptor *brandDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"OrderBy" ascending:YES];
                NSArray *sortDescriptorsNew1 = [NSArray arrayWithObject:brandDescriptor1];
                arrOfLeadStatusMasters1 = [arrOfLeadStatusMasters1 sortedArrayUsingDescriptors:sortDescriptorsNew1];
                NSMutableArray *arrDelete1=[[NSMutableArray alloc]init];
                for (int k=0; k<arrOfLeadStatusMasters1.count; k++) {
                    
                    NSDictionary *dictData1=arrOfLeadStatusMasters1[k];
                    
                    BOOL isActive1=[[dictData1 valueForKey:@"IsActive"] boolValue];
                    
                    if (!isActive1) {
                        
                        [arrDelete1 addObject:dictData1];
                        
                    }
                    
                }
                NSMutableArray *arrTempLocal1=[[NSMutableArray alloc]init];
                
                [arrTempLocal1 addObjectsFromArray:arrOfLeadStatusMasters1];
                
                if (!(arrDelete1.count==0)) {
                    [arrTempLocal1 removeObjectsInArray:arrDelete1];
                }
                
                arrOfLeadStatusMasters1=arrTempLocal1;
                
                NSUserDefaults *defLeadStatusMasters1 =[NSUserDefaults standardUserDefaults];
                [defLeadStatusMasters1 setObject:arrOfLeadStatusMasters1 forKey:@"WebLeadStatusMasters"];
                [defLeadStatusMasters1 synchronize];
                
                // End Web Lead Status Masters
                
                // kch gadbad thi do baar service call ho rahoi thi crm master ki to maine ek kr di hai
                
                NSUserDefaults *defs123=[NSUserDefaults standardUserDefaults];
                [defs123 setValue:ResponseDict forKey:@"LeadDetailMaster"];
                [defs123 synchronize];
                AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                [defaultAf setLeadDetails:jsonData];
                
            } else {
                ResponseDict=nil;
            }
           
        }else if ([requestData isEqualToString:@"Sales"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Sales Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"MasterSalesAutomation"];
                [defs synchronize];
                AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                [defaultAf setMasterDetails:jsonData];
            } else {
                ResponseDict=nil;
            }

        }else if ([requestData isEqualToString:@"SalesDynamic"]) {
            
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Sales Dynamic Form Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (ResponseDict.count > 0) {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
                NSString *strException;
                @try {
                    strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                if (![strException isKindOfClass:[NSString class]]) {
                    
                    strException=@"";
                    
                }
                
                if (strException.length==0) {
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setObject:ResponseDict forKey:@"MasterSalesAutomationDynamicForm"];
                    [defs synchronize];
                    
                } else {
                    ResponseDict=nil;
                }
                
            }else {
                ResponseDict=nil;
            }

        }else if ([requestData isEqualToString:@"Service"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Service Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"MasterServiceAutomation"];
                [defs synchronize];
                
            } else {
                ResponseDict=nil;
            }
        }else if ([requestData isEqualToString:@"ServiceDynamic"]) {
            
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Service Dynamic Form Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (ResponseDict.count > 0) {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
                
                NSString *strException;
                @try {
                    strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                if (![strException isKindOfClass:[NSString class]]) {
                    
                    strException=@"";
                    
                }
                
                if (strException.length==0) {
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setObject:ResponseDict forKey:@"MasterServiceAutomationDynamicForm"];
                    [defs synchronize];
                    
                } else {
                    ResponseDict=nil;
                }
                
            }
        }else if ([requestData isEqualToString:@"ServiceProduct"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Service Product Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                //getMasterProductChemicalsServiceAutomation
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"ProductChemicalsServiceAutomation"];
                [defs setObject:ResponseDict forKey:@"getMasterProductChemicalsServiceAutomation"];
                [defs synchronize];
                
            } else {
                ResponseDict=nil;
            }

        }else if ([requestData isEqualToString:@"ServiceEquipment"]) {
            
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Service Equipemnet Master API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (ResponseDict==nil) {
                
            } else {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
            }
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"MasterEquipmentsDynamicForm"];
                [defs synchronize];
                
            } else {
                ResponseDict=nil;
            }
            
        }else if ([requestData isEqualToString:@"PlumbingMaster"]) {
            
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (ResponseDict==nil) {
                
            } else {
                
                NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                [dictTempResponse setObject:ResponseDict forKey:@"response"];
                NSDictionary *dict=[[NSDictionary alloc]init];
                dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                ResponseDict =dictData;
                
            }
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"MasterAllMechanical"];
                [defs synchronize];
                
                // Check which payment is set
                
                NSArray *arrY = [ResponseDict allKeys];
                
                if ([arrY containsObject:@"CompanyPaymentModeExtSerDc"]) {
                    
                    NSArray *arrCompanyPaymentModeExtSerDc = [ResponseDict valueForKey:@"CompanyPaymentModeExtSerDc"];
                    
                    for (int k=0; k<arrCompanyPaymentModeExtSerDc.count; k++) {
                        
                        NSDictionary *dictData = arrCompanyPaymentModeExtSerDc[k];
                        
                        NSString *strIsActivee = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"IsDefault"]];
                        
                        if ([strIsActivee isEqualToString:@"1"] || [strIsActivee isEqualToString:@"True"] || [strIsActivee isEqualToString:@"true"]) {
                            
                            NSString *strPaymentMode = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"DefaultLabel"]];
                            
                            NSUserDefaults *defsPaymentMode=[NSUserDefaults standardUserDefaults];
                            [defsPaymentMode setObject:strPaymentMode forKey:@"CompanyPaymentMode"];
                            [defsPaymentMode synchronize];
                            
                            break;
                            
                        }
                        
                    }
                    
                }
                
                
            } else {
                ResponseDict=nil;
            }
            
        }else if ([requestData isEqualToString:@"WDOMaster"]) {
            
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            //NSLog(@"Response on WDO Master = = = =  = %@",ResponseDict);

            if ([ResponseDict isKindOfClass:[NSDictionary class]]) {
                
                NSArray *arrOfKeys = [ResponseDict allKeys];
                
                if ([arrOfKeys containsObject:@"LetterTemplateMaster"]) {
                    
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setObject:ResponseDict forKey:@"MasterWDO"];
                    [defs synchronize];
                    
                }
                
            }
            
        }else if ([requestData isEqualToString:@"EmployeePerformanceDetails"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Employee Performance Details API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"DashBoardPerformance"];
                [defs synchronize];
                
            } else {
                ResponseDict=nil;
            }

        }else if ([requestData isEqualToString:@"EmployeeChartDetails"]) {
           
           NSDate *methodFinish = [NSDate date];
           NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
           NSLog(@"executionTime Employee Chart Details API = %f", executionTime);
           
            NSDictionary *ResponseDict;
            NSData* jsonData = [NSData dataWithData:data];
            ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
            
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setObject:ResponseDict forKey:@"DashBoardPerformanceChart"];
                [defs synchronize];
                
            } else {
                ResponseDict=nil;
            }

        }
        
        if (ResponseDict.count > 0) {
            
            NSMutableDictionary *dictDataTemp = [[NSMutableDictionary alloc] init];
            [dictDataTemp setValue:ResponseDict forKey:@"data"];
            
            //NSLog(@"Response dictDataTemp ===%@",dictDataTemp);

            //data
            getDataFromSwiftCallback(success, dictDataTemp, error);

        } else {
            
            NSMutableDictionary *dictDataTemp = [[NSMutableDictionary alloc] init];
            [dictDataTemp setValue:Sorry forKey:@"message"];
            
            //message
            getDataFromSwiftCallback(success, dictDataTemp, error);

        }
         
     }];

}

-(void)getEmployeeListNew :(NSString*)stringUrl{

    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
            
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Employee List API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict.count==0) {
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 
                 NSString *strException;
                 @try {
                     strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                 } @catch (NSException *exception) {
                     
                 } @finally {
                     
                 }
                 if (![strException isKindOfClass:[NSString class]]) {
                     
                     strException=@"";
                     
                 }
                 if (strException.length==0) {
                     
                     NSUserDefaults *defsResponse=[NSUserDefaults standardUserDefaults];
                     [defsResponse setObject:dictData forKey:@"EmployeeList"];
                     [defsResponse synchronize];
                     
                 } else {
                     ResponseDict=nil;
                 }
             }
             
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}


-(NSMutableDictionary*) dictOfDeviceInfo {
    
    NSString *DeviceVersion = [[UIDevice currentDevice] systemVersion];
    NSString *VersionDatee = VersionDate;
    NSString *VersionNumberr = VersionNumber;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *DeviceName=[NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:DeviceName forKey:@"DeviceName"];
    [dictTemp setValue:VersionNumberr forKey:@"VersionNumberr"];
    [dictTemp setValue:VersionDatee forKey:@"VersionDatee"];
    [dictTemp setValue:DeviceVersion forKey:@"DeviceVersion"];
    [dictTemp setValue:[self strCurrentDate] forKey:@"CurrentDate"];
    [dictTemp setValue:[self getUserName] forKey:@"UserName"];
    [dictTemp setValue:[self getCompanyKey] forKey:@"CompanyKey"];
    [dictTemp setValue:[self getCompanyId] forKey:@"CompanyId"];
    [dictTemp setValue:[self strEmpBranchID] forKey:@"BranchID"];

    return dictTemp;
    
}

//============================================================================
//============================================================================
#pragma mark- SERVICE MASTERS
//============================================================================
//============================================================================


-(void)getMasterServiceAutoNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
            
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Service Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

-(void)getServiceDynamicFormMasterNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Service Dynamic Form Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             ResponseDict =dictData;
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterServiceAutomationDynamicForm"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }

         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

-(void)getProductChemicalNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
            
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Service Product Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 //getMasterProductChemicalsServiceAutomation
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"ProductChemicalsServiceAutomation"];
                 [defs setObject:ResponseDict forKey:@"getMasterProductChemicalsServiceAutomation"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }

         }];
        
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

-(void)getMasterEquipmentsNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Service Equipemnet Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             if (ResponseDict==nil) {
                 
             } else {
                 
                 NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
                 [dictTempResponse setObject:ResponseDict forKey:@"response"];
                 NSDictionary *dict=[[NSDictionary alloc]init];
                 dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
                 NSMutableDictionary *dictData=[dict valueForKey:@"response"];
                 ResponseDict =dictData;
                 
             }
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterEquipmentsDynamicForm"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }
             
         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

//============================================================================
//============================================================================
#pragma mark- SALES MASTERS
//============================================================================
//============================================================================

-(void)getSalesMasterNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
            
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Sales Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             ResponseDict=[self nestedDictionaryByReplacingNullsWithNil:ResponseDict];
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterSalesAutomation"];
                 [defs synchronize];
                 AppUserDefaults * defaultAf = [[AppUserDefaults alloc]init];
                 [defaultAf setMasterDetails:jsonData];
             } else {
                 ResponseDict=nil;
             }

         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

-(NSArray*)filterDataAfterMergingAppointments :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrAllObj{
    
    NSMutableArray *arrTodayBlockDates = [[NSMutableArray alloc]init];
    
    [arrTodayBlockDates addObjectsFromArray:arrOfEmpBlockTime];
    
    NSMutableArray *arrTemp =[[NSMutableArray alloc]init];
    NSArray *arrTempFiltered =[[NSArray alloc]init];
    
    //[arrTemp addObjectsFromArray:arrAllObj];
    
    for (int k=0; k<arrTodayBlockDates.count; k++) {
        
        [arrTemp addObject:arrTodayBlockDates[k]];
        
    }
    for (int k=0; k<arrAllObj.count; k++) {
        
        [arrTemp addObject:arrAllObj[k]];
        
    }
    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
    
    if (isSortByScheduleDate) {
        
        BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
        
        if (isSortByScheduleDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :NO];
            
        }
        
    } else {
        
        BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
        
        if (isSortByModifiedDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        }
        
    }
    
    arrAllObj = nil;
    
    arrAllObj = arrTempFiltered;
    
    return arrAllObj;
    
}



-(void)getDynamicFormSalesAutoMasterNew :(NSString*)stringUrl{
    
    NSDate *methodStart = [NSDate date];

    NSURL *url = [NSURL URLWithString:stringUrl];
    NSMutableURLRequest *requestLocal = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [requestLocal setHTTPMethod:@"GET"];
    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestLocal addValue:[self strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [requestLocal addValue:[self strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [requestLocal addValue:[self strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [requestLocal addValue:[self strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    [requestLocal addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [requestLocal addValue:[self getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [requestLocal addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    
    @try {
        
        [NSURLConnection sendAsynchronousRequest:requestLocal queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
         {
             
            NSDate *methodFinish = [NSDate date];
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
            NSLog(@"executionTime Sales Dynamic Form Master API = %f", executionTime);
            
             NSDictionary *ResponseDict;
             NSData* jsonData = [NSData dataWithData:data];
             ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
             
             NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
             [dictTempResponse setObject:ResponseDict forKey:@"response"];
             NSDictionary *dict=[[NSDictionary alloc]init];
             dict=[self nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
             NSMutableDictionary *dictData=[dict valueForKey:@"response"];
             ResponseDict =dictData;
             
             
             NSString *strException;
             @try {
                 strException=[ResponseDict valueForKey:@"ExceptionMessage"];
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             if (![strException isKindOfClass:[NSString class]]) {
                 
                 strException=@"";
                 
             }
             
             if (strException.length==0) {
                 
                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                 [defs setObject:ResponseDict forKey:@"MasterSalesAutomationDynamicForm"];
                 [defs synchronize];
                 
             } else {
                 ResponseDict=nil;
             }

         }];
    }
    @catch (NSException *exception) {
        [self AlertMethod:Alert :Sorry];
    }
    @finally {
    }
}

-(void)DeleteFromCoreDataMechancialWOs
{
    NSEntityDescription *entitytotalLeads;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalLeads=[NSEntityDescription entityForName:@"MechanicalWOs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"MechanicalWOs" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
    }
}

-(NSDate*)getCurrentSystemTime
{
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    return [outputFormatter dateFromString:newDateString];
}

-(BOOL)isGenerateWoMechanical{

    // IsCreateWoAppAnyTime if Set Yes can create Wo change by saavan on 31st july 2019
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    BOOL isCreateWoAnyTimeAppp = [defs boolForKey:@"IsCreateWoAppAnyTime"];
    
    if (isCreateWoAnyTimeAppp) {
        
        return true;
        
    } else {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
        NSString *dayName = [dateFormatter stringFromDate:[NSDate date]];
        NSArray *arrayBusinessHr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"MasterSalesAutomation"] valueForKey:@"CompanyBusinessHour"];
        NSDictionary *dictLoginDetails = [[NSUserDefaults standardUserDefaults] objectForKey:@"LoginDetails"];
        
        if(arrayBusinessHr.count>0)
        {
            BOOL showAlert;
            showAlert = YES;
            
            for(NSDictionary *dict in arrayBusinessHr)
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"WeekDays"]] isEqualToString:dayName])
                {
                    if([[dict valueForKey:@"CompanyBusinessHourEmployees"] count]>0)
                    {
                        for(NSDictionary *dictEmp in [dict valueForKey:@"CompanyBusinessHourEmployees"])
                        {
                            if([[dictLoginDetails valueForKey:@"EmployeeId"] intValue]==[[dictEmp valueForKey:@"EmployeeId"] intValue])
                            {
                                NSDate *systemCurrentTime = [self getCurrentSystemTime];
                                
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                [formatter setDateFormat:@"HH:mm:ss"];
                                
                                NSDate *fromTime= [formatter dateFromString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"BusinessHoursFrom"]]];
                                
                                NSDate *toTime = [formatter dateFromString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"BusinessHoursTo"]]];
                                
                                NSComparisonResult result1 = [systemCurrentTime compare:fromTime];
                                
                                NSComparisonResult result2 = [systemCurrentTime compare:toTime];
                                
                                if(result1 == NSOrderedDescending || result2 == NSOrderedAscending )
                                {
                                    
                                    showAlert = NO;
                                    // go to create work order
                                    
                                    return true;
                                }
                                else
                                {
                                    
                                    // can not create work order
                                    return false;
                                }
                            }
                        }
                    }
                    else
                    {
                        // can not create work order
                        return false;
                    }
                }
            }
            
            if (showAlert) {
                
                return false;
                
            }
            
            return showAlert;
            
        }
        else
        {
            // can not create workorder
            return false;
        }
        
    }
    
}

-(NSArray*)arrPO :(NSDictionary*)dictData{
    
    NSArray *arrTemp = (NSArray*)dictData;
    
    return arrTemp;
    
}

-(BOOL)isEaMasterAvailable {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSDictionary* dictMasters=[defs valueForKey:@"MasterSalesAutomation"];
    NSDictionary *dictHTMLData = [dictMasters valueForKey:@"ElectronicAuthorizationFormMaster"];
    if([dictHTMLData isKindOfClass:[NSDictionary class]])
    {
        
        return true;
        
    }else{
        
        return false;
        
    }
    
}
-(float)heightOfAttrbuitedText:(NSAttributedString *)attrStr width:(CGFloat )width{

    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return rect.size.height;
}
@end
