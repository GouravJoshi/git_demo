//
//  AppointmentAllView.m
//  DPS
//
//  Created by Rakesh Jain on 21/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Patidar 2021
//  Saavan Patidar

#import "AppointmentAllView.h"

#import "AppointmentView.h"
#import "AllAppointmentTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "GeneralInfoAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import <MessageUI/MessageUI.h>
#import "SaleAutomationGeneralInfo.h"
#import "AppDelegate.h"
#import "DPS-Swift.h"

//#import "SalesAutomationAppoint+CoreDataProperties.h"
//#import "SalesAutomationAppoint.h"

@interface AppointmentAllView ()<UITableViewDataSource,UITableViewDelegate>
{
    Global *global;
    NSMutableArray *arrOfTotalWorkerOrder;
    NSString *strCompanyKeyy,*strServiceUrlMainServiceAutomation,*strEmpId;
    int countForServiceOrder;
}

@end

@implementation AppointmentAllView

- (void)viewDidLoad {
    
    //Testing
    //Testing
    //Testing 123 123 123123
    
    [super viewDidLoad];
    
    countForServiceOrder=-1;
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpId      =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.CompanyId"];
    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];

    //============================================================================
    //============================================================================

    _tblViewAllApponitments.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewAllApponitments.rowHeight=UITableViewAutomaticDimension;
    _tblViewAllApponitments.estimatedRowHeight=200;
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    // Adding the swipe gesture on image view
    [self.view addGestureRecognizer:swipeRight];
    // Do any additional setup after loading the view.
    [self fetchSalesAndServiceWorkOrdersFromCoreData];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
#pragma mark- Button Action METHODS
//============================================================================

- (IBAction)actionBack:(id)sender {
//    for (UIViewController *controller in self.navigationController.viewControllers)
//    {
//        if ([controller isKindOfClass:[DashBoardView class]])
//        {
//            [self.navigationController popToViewController:controller animated:NO];
//            break;
//        }
//    }

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"DashBoard"
                                                             bundle: nil];
    DashBoardNew_iPhoneVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
    [self.navigationController pushViewController:objByProductVC animated:NO];
}

- (IBAction)actionUpcoming:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[AppointmentView class]])
        {
            [self.navigationController popToViewController:controller animated:NO];
            break;
        }
    }

//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//    AppointmentView
//    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
//    [self.navigationController pushViewController:objByProductVC animated:NO];
}
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {

        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[AppointmentView class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                break;
            }
        }

//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                 bundle: nil];
//        AppointmentView
//        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
//        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
    else
    {
    }
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    NSArray *sections = [self.fetchedResultsController sections];
    //    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    //    // totalCount=[sectionInfo numberOfObjects];
    //    return [sectionInfo numberOfObjects];
    return arrOfTotalWorkerOrder.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AllAppointmentTableViewCell *cell = (AllAppointmentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AllAppointmentCell" forIndexPath:indexPath];
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(AllAppointmentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    // Fetch Record
    //  cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    if (!(countForServiceOrder==-1)) {
        
    if (indexPath.row<countForServiceOrder) {

    NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:indexPath.row];
    NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
    
    cell.lbl_WorkOrderNo.text=[NSString stringWithFormat:@"Wo#:%@",[dictofWorkorderDetail valueForKey:@"WorkOrderNo"]];
    
    cell.lbl_ServiceName.text=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"Services"]];
    
    cell.lbl_Status.text=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"WorkorderStatus"]];
    
    cell.lbl_DAte.text=[global ChangeDateToLocalDate:[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleOnStartDateTime"]]];
    
    cell.lbl_Name.text=[NSString stringWithFormat:@"%@ %@",[dictofWorkorderDetail valueForKey:@"FirstName"],[dictofWorkorderDetail valueForKey:@"LastName"]];
    
    cell.lbl_Type.text=[NSString stringWithFormat:@"%@",@"Service"];
    
    cell.lbl_AccNo.text=[NSString stringWithFormat:@"Ac#:%@",[dictofWorkorderDetail valueForKey:@"AccountNo"]];
    
    cell.lbl_Address.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[dictofWorkorderDetail valueForKey:@"BillingAddress1"],[dictofWorkorderDetail valueForKey:@"BillingCity"],[dictofWorkorderDetail valueForKey:@"BillingState"],[dictofWorkorderDetail valueForKey:@"BillingZipcode"]];
    
    cell.lbl_Invoice.text=[NSString stringWithFormat:@"$%@",[dictofWorkorderDetail valueForKey:@"InvoiceAmount"]];
    
    //WorkorderStatus Services
    //AccountNo  ScheduleOnStartDateTime  InvoiceAmount FirstName LastName BillingAddress1 BillingCity BillingState BillingZipcode

    cell.sendSMS.tag=indexPath.row;
    cell.sendEmail.tag=indexPath.row;
    cell.sendMapView.tag=indexPath.row;
    [cell.sendSMS addTarget:self action:@selector(sendSMSs:) forControlEvents:UIControlEventTouchDown];
    [cell.sendEmail addTarget:self action:@selector(sendEmail:) forControlEvents:UIControlEventTouchDown];
    [cell.sendMapView addTarget:self action:@selector(sendToMapView:) forControlEvents:UIControlEventTouchDown];
        }
        else{
           cell.lbl_Type.text=[NSString stringWithFormat:@"%@",@"Sales"]; 
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!(countForServiceOrder==-1)) {
        
        if (indexPath.row<countForServiceOrder) {

    NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:indexPath.row];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    GeneralInfoAppointmentView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GeneralInfoAppointmentView"];
    objByProductVC.dictOfWorkOrders=dictOfWorkOrderExtSerDcs;
    [self.navigationController pushViewController:objByProductVC animated:NO];
        }
        else
        {
            
            NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:indexPath.row];
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                     bundle: nil];
            SaleAutomationGeneralInfo
            *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SaleAutomationGeneralInfo"];
            objByProductVC.dictSaleAuto=dictOfWorkOrderExtSerDcs;
            [self.navigationController pushViewController:objByProductVC animated:NO];

        }
    }
}
//This function is where all the magic happens
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    //1. Setup the CATransform3D structure
//    CATransform3D rotation;
//    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
//    rotation.m34 = 1.0/ -600;
//    
//    
//    //2. Define the initial state (Before the animation)
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//    
//    cell.layer.transform = rotation;
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//    
//    
//    //3. Define the final state (After the animation) and commit the animation
//    [UIView beginAnimations:@"rotation" context:NULL];
//    [UIView setAnimationDuration:0.8];
//    cell.layer.transform = CATransform3DIdentity;
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
//    
//}

//============================================================================
#pragma mark- METHODS Button Actions
//============================================================================

-(void)sendSMSs:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm??"
                               message:@"Do you want to send SMS"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             UIButton *btn = (UIButton *)sender;
                             if (!(countForServiceOrder==-1)) {
                                 
                                 if (btn.tag<countForServiceOrder) {
                                     
                                     NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
                                     NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
                                     NSString *strNumber=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"PrimaryPhone"]];
                                     
                                     NSArray *recipients = [NSArray arrayWithObjects:strNumber, nil];
                                     
                                     [self sendNumber:recipients :dictofWorkorderDetail];
                                 }
                                 else{
                                     
                                     //Sales Wala YAha Karna hai
                                 }
                             }
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)sendEmail:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm??"
                               message:@"Do you want to send Email"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email..."];
                             
                             UIButton *btn = (UIButton *)sender;
                             
                             if (!(countForServiceOrder==-1)) {
                                 
                                 if (btn.tag<countForServiceOrder) {
                                     
                                     NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
                                     NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
                                     
                                     NSString *strUrl=[NSString stringWithFormat:@"%@%@&CompanyKey=%@",UrlSendEmailServiceAutomation,[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"WorkorderId"]],strCompanyKeyy];
                                     //============================================================================
                                     //============================================================================
                                     
                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                         
                                         [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendEmailServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [DejalBezelActivityView removeView];
                                                  if (success)
                                                  {
                                                      NSString *returnString =[response valueForKey:@"ReturnMsg"];
                                                      if ([returnString isEqualToString:@"Successful"]) {
                                                          [global AlertMethod:Info :SuccessMailSend];
                                                      } else {
                                                          [global AlertMethod:Info :SuccessMailSend];
                                                      }
                                                  }
                                                  else
                                                  {
                                                      NSString *strTitle = Alert;
                                                      NSString *strMsg = Sorry;
                                                      [global AlertMethod:strTitle :strMsg];
                                                  }
                                              });
                                          }];
                                     });
                                 }
                                 else
                                 {
                                     //Yaha Par sales auto ki karna hai
                                 }
                             }
                             //============================================================================
                             //============================================================================
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)sendToMapView:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    if (!(countForServiceOrder==-1)) {
        
        if (btn.tag<countForServiceOrder) {
            
           // NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
           // NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
        }
        else{
            //Yaha PAr Sales Auto Ka Karna hai
        }
    }
    
    // NSString *url=[NSString stringWithFormat:@"https://maps.google.com/maps?f=q&t=m&q=%@,%@",[dictofWorkorderDetail valueForKey:@"ServiceAddressLatitude"],[dictofWorkorderDetail valueForKey:@"ServiceAddressLongitude"]];
    
    NSString *url=@"https://maps.google.com/maps?f=q&t=m&q=22.730287,75.8763365";
    
    NSURL *URL = [NSURL URLWithString:url];
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}


//============================================================================
#pragma mark- METHODS Fetch Data from DB
//============================================================================

-(void)FetchFromCoreDataTotalWorkOrdersServiceAutomation :(NSString*)strTypefLead{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitytotalWorkOrders];
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"empId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerTotalWorkOrders = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTotalWorkOrders setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTotalWorkOrders performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerTotalWorkOrders fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        
        matches = arrAllObj[0];
        NSDictionary *dictOfWorkOrderss=[matches valueForKey:@"dictOfWorkOrders"];
        arrOfTotalWorkerOrder=[dictOfWorkOrderss valueForKey:@"WorkOrderExtSerDcs"];
        [_tblViewAllApponitments reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


//============================================================================
//============================================================================

#pragma mark- Merging Sales And Service Work Orders

//============================================================================
//============================================================================


-(void)fetchSalesAndServiceWorkOrdersFromCoreData{
    
    arrOfTotalWorkerOrder=[[NSMutableArray alloc]init];
    
    //-----------------*****************-----------------Service WALA--------------****************************
    
    //============================================================================
    //============================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitytotalWorkOrders];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"empId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerTotalWorkOrders = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTotalWorkOrders setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTotalWorkOrders performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerTotalWorkOrders fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        
        matches = arrAllObj[0];
        NSDictionary *dictOfWorkOrderss=[matches valueForKey:@"dictOfWorkOrders"];
        arrOfTotalWorkerOrder=[dictOfWorkOrderss valueForKey:@"WorkOrderExtSerDcs"];
        
        NSMutableArray *arrtempWorkOrder=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfTotalWorkerOrder.count; k++) {
            
            NSDictionary *dictOfWorkOrderExtSerDcs =arrOfTotalWorkerOrder[k];
            NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
            NSString *strDateToConvert=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleEndDateTime"]];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* EndDate = [dateFormatter dateFromString:strDateToConvert];
            
            NSComparisonResult result = [[NSDate date] compare:EndDate];
            if(result == NSOrderedDescending)
            {
                NSLog(@"strLocal is later than Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            else if(result == NSOrderedAscending)
            {
                NSLog(@"Current is later than strLocal");
            }
            else
            {
                NSLog(@"strLocal is equal to Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            
        }
        
        //  arrOfTotalWorkerOrder=arrtempWorkOrder;
        
        [arrOfTotalWorkerOrder addObjectsFromArray:arrtempWorkOrder];
        
        if (!(arrOfTotalWorkerOrder.count==0)) {
            
            countForServiceOrder=arrOfTotalWorkerOrder.count;
            
        }

        // [_tblViewAppointment reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    
    
    //-----------------*****************-----------------Sales WALA--------------****************************
    
    //============================================================================
    //============================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesInfo=[NSEntityDescription entityForName:@"SalesAutomationAppoint" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"companyKey = %@",strCompanyKeyy];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        NSArray *arrOfTotalWorkOrderService;
        
        matches=arrAllObj[0];
        
        NSDictionary *dictOfWorkOrderssSales = [matches valueForKey:@"appointResponseDict"];
        
        arrOfTotalWorkOrderService=[dictOfWorkOrderssSales valueForKey:@"LeadExtSerDcs"];
        
        NSMutableArray *arrtempWorkOrder=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfTotalWorkOrderService.count; k++) {
            
            NSDictionary *dictOfWorkOrderExtSerDcs =arrOfTotalWorkOrderService[k];
            NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"LeadDetail"];
            NSString *strDateToConvert=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleEndDateTime"]];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* EndDate = [dateFormatter dateFromString:strDateToConvert];
            
            NSComparisonResult result = [[NSDate date] compare:EndDate];
            if(result == NSOrderedDescending)
            {
                NSLog(@"strLocal is later than Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkOrderService[k]];
            }
            else if(result == NSOrderedAscending)
            {
                NSLog(@"Current is later than strLocal");
            }
            else
            {
                NSLog(@"strLocal is equal to Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkOrderService[k]];
            }
            
        }
        //arrOfTotalWorkerOrder=arrtempWorkOrder;
        
          [arrOfTotalWorkerOrder addObjectsFromArray:arrtempWorkOrder];
        
        [_tblViewAllApponitments reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}


//============================================================================
//============================================================================
#pragma mark- ----------------Message Composer Methods----------------
//============================================================================
//============================================================================

-(void)sendNumber:(NSArray*)arrOfnumber :(NSDictionary*)dictofWorkorderDetail{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *strOld= [def valueForKey:@"CustomMessage"];
    
    if (strOld.length==0) {
        [global AlertMethod:Info :AlertSelectCustomMsg];
    }
    else{
        
        //    NSString *strFinalMsg= [strOld stringByReplacingOccurrencesOfString:@"##CustomerName##" withString:[[dictofWorkorderDetail valueForKey:@"Details"] valueForKey:@"FirstName"]];
        //
        //    NSString *strTech= [strFinalMsg stringByReplacingOccurrencesOfString:@"##TechName##" withString:[[dictofWorkorderDetail valueForKey:@"Details"] valueForKey:@"FirstName"]];
        //
        //    NSString *strTime = [self calculateTimeBetweenDistane:[NSString stringWithFormat:@"%f,%f",22.7253,75.8655] :[NSString stringWithFormat:@"%f,%f",22.9600,76.0600]];
        //
        //    NSString  *strFinalMsgF= [strTech stringByReplacingOccurrencesOfString:@"##Time## min" withString:strTime];
        //    NSString *strFinalMsgLast= [strFinalMsgF stringByReplacingOccurrencesOfString:@"##EprofileUrl##" withString:[dictofWorkorderDetail  valueForKey:@"SpecialInstruction"]];
        
        [self sendSMS:arrOfnumber :@"Testing"];
        
    }
}

- (void)sendSMS:(NSArray *)recipients :(NSString*)message
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = message;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Info message:@"Your device does not support sending Text Message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [global AlertMethod:Alert :failSMSSend];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [global AlertMethod:Info :SuccessSMSSend];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


//============================================================================
//============================================================================
#pragma mark- ----------------Calculate Time Between Distane Methods----------------
//============================================================================
//============================================================================

-(NSString *)calculateTimeBetweenDistane : (NSString *)strLat : (NSString *)strLong
{
    NSString *LocationUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%@",strLat,strLong];
    // NSLog(@"Location URL:%@",LocationUrl);
    
    NSURL *finalurl = [NSURL URLWithString: LocationUrl];
    
    NSData *data = [NSData dataWithContentsOfURL:finalurl];
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions error:&error];
    
    
    NSArray *arrayRows = [json valueForKey:@"rows"];
    NSString *strTime;
    for (NSDictionary *dict in arrayRows) {
        NSArray *arrayElement =[dict valueForKey:@"elements"];
        NSDictionary *dicttemp = [arrayElement objectAtIndex:0];
        // NSLog(@"Final URL = %@",[[dicttemp valueForKey:@"duration"] valueForKey:@"text"]);
        strTime =[[dicttemp valueForKey:@"duration"] valueForKey:@"text"];
    }
    return strTime;
}

@end
