//
//  GeneralInfoiPadVC.swift
//  DPS
//  peSTream
//  Created by Akshay Hastekar on 27/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class GeneralInfoiPadVC: UIViewController
{
    
    // MARK: -  ----- Outlet  -----
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnClock: UIButton!
    
    @IBOutlet weak var btnServiceDocs: UIButton!
    
    @IBOutlet weak var btnGeneralInfo: UIButton!
    
    @IBOutlet weak var btnProblemIdentification: UIButton!
    
    @IBOutlet weak var btnConfigureProposal: UIButton!
    
    @IBOutlet weak var btnAgreement: UIButton!
    
    @IBOutlet weak var btnFinalizeReport: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    // MARK: -  ----- View Customer Info Outlet  -----
    
    @IBOutlet weak var btnShowCustomerInfo: UIButton!
    
    @IBOutlet weak var btnEditCustomerInfo: UIButton!
    @IBOutlet weak var viewCustomerInfo: CardView!
    @IBOutlet weak var const_ViewCustomerInfo_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_ViewEditContactDetail_CuastomerInfo_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_ViewUnEditView_CustomerInfo_H: NSLayoutConstraint!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var btnNoEmail: UIButton!
    @IBOutlet weak var txtFldPrimaryEmailCustInfo: UITextField!
    @IBOutlet weak var txtFldSecondaryEmailCustInfo: UITextField!
    @IBOutlet weak var txtFldPrimaryPhoneCustInfo: UITextField!
    @IBOutlet weak var txtFldSecondaryPhoneCustInfo: UITextField!
    @IBOutlet weak var txtFldCellNoCustInfo: UITextField!
    @IBOutlet weak var txtViewAccountAlert: UITextView!
    
    @IBOutlet weak var btnPrimaryEmailCustInfo: UIButton!
    @IBOutlet weak var btnSecondaryEmailCustInfo: UIButton!
    @IBOutlet weak var btnPrimaryPhoneCustInfo: UIButton!
    @IBOutlet weak var btnSecondaryPhoneCustInfo: UIButton!
    @IBOutlet weak var btnCellNoCustInfo: UIButton!
    @IBOutlet weak var btnAddressCustomerInfo: UIButton!
    @IBOutlet weak var lblNoEmail: UILabel!
    // MARK: -  ----- View Work Order Info Outlet  -----
    @IBOutlet weak var viewWorkOrderInfo: CardView!
    @IBOutlet weak var btnShowWorkOrderInfo: UIButton!
    @IBOutlet weak var const_ViewWorkOrderInfo_H: NSLayoutConstraint!
    
    @IBOutlet weak var lblAccNoWorkOrderInfo: UILabel!
    @IBOutlet weak var btnCollectPayment: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblServices: UILabel!
    @IBOutlet weak var lblEmployeeNo: UILabel!
    @IBOutlet weak var lblEmployeeName: UILabel!
    @IBOutlet weak var lblKeyMap: UILabel!
    @IBOutlet weak var lblRoutNo: UILabel!
    @IBOutlet weak var lblRoutName: UILabel!
    @IBOutlet weak var lblRoutAlert: UILabel!
    @IBOutlet weak var lblScheduleStartDateTime: UILabel!
    @IBOutlet weak var lblScheduleEndDateTime: UILabel!
    @IBOutlet weak var lblEarliestStartTime: UILabel!
    @IBOutlet weak var lblLatestStartTime: UILabel!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblToTime: UILabel!
    
    
    
    
    // MARK: -  ----- View Billing Contact & Address Outlet  -----
    
    @IBOutlet weak var viewBillingContactAddress: CardView!
    @IBOutlet weak var btnShowBillingContactAddress: UIButton!
    @IBOutlet weak var constViewBillingContactAddress_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnEditBillingContact: UIButton!
    @IBOutlet weak var btnSelectBillingContactAddress: UIButton!
    @IBOutlet weak var lblContactNameBillingContact: UILabel!
    @IBOutlet weak var lblMapCodeBillingContact: UILabel!
    @IBOutlet weak var lblPrimaryEmailBillingContact: UILabel!
    @IBOutlet weak var lblSecondaryEmailBillingContact: UILabel!
    @IBOutlet weak var lblPrimaryPhoneBillingContact: UILabel!
    @IBOutlet weak var lblSecondaryPhoneBillingContact: UILabel!
    @IBOutlet weak var btnAddressBillingContact: UIButton!
    @IBOutlet weak var lblCountyBillingContact: UILabel!
    @IBOutlet weak var lblSchoolDistrictBillingContact: UILabel!
    
    @IBOutlet weak var btnPrimaryEmailBillingContact: UIButton!
    @IBOutlet weak var btnSecondaryEmailBillingContact: UIButton!
    @IBOutlet weak var btnPrimaryPhoneBillingContact: UIButton!
    @IBOutlet weak var btnSecondaryPhoneBillingContact: UIButton!
    @IBOutlet weak var const_BtnBillingContact_H: NSLayoutConstraint!
    
    
    // MARK: -  ----- View Service Contact & Address Outlet  -----
    @IBOutlet weak var viewServiceContactAddress: CardView!
    @IBOutlet weak var btnShowServiceContactAddress: UIButton!
    @IBOutlet weak var constViewServiceContactAddress_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnEditServiceContact: UIButton!
    @IBOutlet weak var imgViewServiceContactAddress: UIImageView!
    @IBOutlet weak var btnUploadEditImageServiceContact: UIButton!
    
    @IBOutlet weak var btnSelectServiceContactAddress: UIButton!
    @IBOutlet weak var lblContactNameServiceContact: UILabel!
    @IBOutlet weak var lblMapCodeServiceContact: UILabel!
    @IBOutlet weak var lblPrimaryEmailServiceContact: UILabel!
    @IBOutlet weak var lblSecondaryEmailServiceContact: UILabel!
    @IBOutlet weak var lblPrimaryPhoneServiceContact: UILabel!
    @IBOutlet weak var lblSecondaryPhoneServiceContact: UILabel!
    @IBOutlet weak var btnAddressServiceContact: UIButton!
    @IBOutlet weak var lblCountyServiceContact: UILabel!
    @IBOutlet weak var lblSchoolDistrictServiceContact: UILabel!
    
    
    @IBOutlet weak var btnPrimaryPhoneServiceContact: UIButton!
    @IBOutlet weak var btnPrimaryEmailServiceContact: UIButton!
    
    @IBOutlet weak var constBtnServiceContact_H: NSLayoutConstraint!
    // MARK: -  ----- View Upload Image Outlet  -----
    
    @IBOutlet weak var viewUploadImage: CardView!
    @IBOutlet weak var btnShowUploadImage: UIButton!
    @IBOutlet weak var constViewUploadImage_H: NSLayoutConstraint!
    
    
    // MARK: -  ----- View Other Outlet  -----
    
    @IBOutlet weak var viewOther: CardView!
    @IBOutlet weak var btnShowViewOther: UIButton!
    @IBOutlet weak var const_ViewOther_H: NSLayoutConstraint!
    @IBOutlet weak var txtViewServiceDescription: UITextView!
    @IBOutlet weak var txtViewServiceInstruction: UITextView!
    @IBOutlet weak var txtViewAttribute: UITextView!
    @IBOutlet weak var txtViewTarget: UITextView!
    @IBOutlet weak var txtViewSetupInstruction: UITextView!
    
    // MARK: -  ----- View Technician Comment Outlet  -----
    
    @IBOutlet weak var viewTechnicianComment: CardView!
    @IBOutlet weak var btnShowViewTechnicianComment: UIButton!
    @IBOutlet weak var constViewTechnicianComment_H: NSLayoutConstraint!
    @IBOutlet weak var txtViewTechnicianComment: UITextView!
    @IBOutlet weak var btnServiceStatus: UIButton!
    @IBOutlet weak var btnResetReason: UIButton!
    @IBOutlet weak var viewResetReason: UIView!
    @IBOutlet weak var constViewResetReason: NSLayoutConstraint!
    @IBOutlet weak var txtViewResetReason: UITextView!
    
    // MARK: -  ----- View Time Range Outlet -----
    
    @IBOutlet weak var viewTimeRange: CardView!
    @IBOutlet weak var btnShowTimeRange: UIButton!
    @IBOutlet weak var const_ViewTimeRange_H: NSLayoutConstraint!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var lblStartTimeValue: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    
    
    // MARK: -  ----- Warning Message -----
    @IBOutlet weak var viewWarning: UIView!
    @IBOutlet weak var lbl_WarningActionTitle: UILabel!
    @IBOutlet weak var lbl_WarningActionValue: UILabel!
    @IBOutlet weak var lbl_PricingNoteTitle: UILabel!
    @IBOutlet weak var lbl_PricingNoteValue: UILabel!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strServicesStatus = String()
    var strLeadIdGlobal = String()
    var strFromWhere = String()

    // MARK: -  ----- Variable Declaration -----
    
    // MARK: -  --- String ---
    
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strResetId = String()
    var strTimeIn = String()
    var strServicePocId = String()
    var strBillingPocId = String()
    
    // MARK: -  --- Array ---
    
    var arrData = NSMutableArray()
    var arrServiceAddressContactDetails = NSMutableArray()
    var arrBillingAddressContactDetails = NSMutableArray()
    
    // MARK: -  --- Dictionary ---
    
    var dictLoginData = NSDictionary()
    var dictServiceContactData = NSDictionary()
    var dictBillingContactData = NSDictionary()
    
    // MARK: -  --- Bool ---
    
    var isNoEmail = Bool()
    var isResetReason = Bool()
    var yesEditedSomething = Bool()
    var isCollectPayment = Bool()
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            // Condition For Edit From Pending Approval
            
            if strFromWhere == "clickedOnEditFromPendingApproval" {
                
                nsud.setValue("yes", forKey: "clickedOnEditFromPendingApproval")
                nsud.synchronize()
                
            }else{
                
                nsud.setValue("no", forKey: "clickedOnEditFromPendingApproval")
                nsud.synchronize()
                
            }
            
            isNoEmail = false
            isResetReason = false
            yesEditedSomething = false
            isCollectPayment = false
            
            strResetId = ""
            strServicePocId = ""
            strBillingPocId = ""
            strLeadIdGlobal = ""
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
            
            strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
            
            strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            
            strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
            
            if strServiceReportType == "CompanyEmail"
            {
                strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
            }
            else
            {
                strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
            }
            
            
            fetchServiceAddressContactDetails()
            fetchBillingAddressContactDetails()
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
            
            if arryOfData.count > 0 {
                
                objWorkorderDetail = arryOfData[0] as! NSManagedObject
                
                assignValues()
                
                // Set Default Flow type for WDO Opp. to Commercial
                
                let departmentType = "\(objWorkorderDetail.value(forKey: "departmentType")!)"
                let serviceState = "\(objWorkorderDetail.value(forKey: "serviceState")!)"
                
                if departmentType == "Termite" && serviceState == "California" {
                    
                    let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
                    
                    // relatedOpportunityNo  === leadNumber
                    
                    self.updateWdoOppFlowType(strLeadNoL: strWoLeadNo)
                                    
                    //Show Warning message
                    ShowWarinigAlertMessage(isShow: false)
                    
                    //---For Show status
                    let wdoWorkflowStageId =  "\(objWorkorderDetail.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(objWorkorderDetail.value(forKey: "wdoWorkflowStageId") ?? "")"
                    let wdoWorkflowStatus = "\(objWorkorderDetail.value(forKey: "wdoWorkflowStatus") ?? "")"
                    if(wdoWorkflowStageId.count == 0 && wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased()){

                        ShowWarinigAlertMessage(isShow: true)
                        
                    }
                    //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                    if checkRedirectSignApprovalOrNot(obj: objWorkorderDetail, checkflowStageType: true, checkflowStatus: true) {
                        
                        ShowWarinigAlertMessage(isShow: true)

                    }
                    
                }
                
                //---For Show status
                let wdoWorkflowStageId =  "\(objWorkorderDetail.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(objWorkorderDetail.value(forKey: "wdoWorkflowStageId") ?? "")"
                let wdoWorkflowStatus = "\(objWorkorderDetail.value(forKey: "wdoWorkflowStatus") ?? "")"
                if(wdoWorkflowStageId.count == 0 && wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased()){
                    
                    nsud.setValue("yes", forKey: "isInspectorCorrectionStage")
                    nsud.synchronize()
                    
                }else{
                    
                    nsud.setValue("no", forKey: "isInspectorCorrectionStage")
                    nsud.synchronize()
                    
                }
                
            }else{
                
                
            }
            
            //btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            btnShowCustomerInfo.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowWorkOrderInfo.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowBillingContactAddress.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowServiceContactAddress.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowUploadImage.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowViewOther.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowViewTechnicianComment.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            btnShowTimeRange.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            
            //end
            
            setBorderAtLoad()
            
            downloadGraphXML()
            
            // Saving Version No
            global.updateWoVersionNo(strWoId as String)
            global.updateLeadVersion(strLeadIdGlobal as String)
            
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            editCustomerInfo()
            getClockStatus()
            methodForDisable()
            
            if (strWorkOrderStatus == "Reset" || strWorkOrderStatus == "reset")
            {
                
                btnSave.isEnabled = false
                
            }
            
            btnCollectPayment.isEnabled = false
            
        }
    
        
    }
    
    func checkRedirectSignApprovalOrNot(obj : NSManagedObject , checkflowStageType : Bool , checkflowStatus : Bool) -> Bool {
        //---------By navin Approval check
        
        var aryApprovalCheckList = NSMutableArray()
        aryApprovalCheckList =  AppointmentVC().getApprovalCheckList().mutableCopy() as! NSMutableArray

        let departmentType = "\(obj.value(forKey: "departmentType")!)"
        let serviceState = "\(obj.value(forKey: "serviceState")!)"
        if departmentType == "Termite" && serviceState == "California" {
            
            if (DeviceType.IS_IPAD && (departmentType == "Termite" && serviceState == "California") && aryApprovalCheckList.count != 0)  {
                
                //---For Show status
                let wdoWorkflowStageId = "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")"
                let wdoWorkflowStatus = "\(obj.value(forKey: "wdoWorkflowStatus") ?? "")"
               
                //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                if(wdoWorkflowStageId.count > 0){
                    let aryApprovalCheckListFilterObj = aryApprovalCheckList.filter { (task) -> Bool in
                        return ("\((task as! NSDictionary).value(forKey: "ApprovalCheckListId")!)".contains(wdoWorkflowStageId)  ) }
                    if aryApprovalCheckListFilterObj.count != 0 {
                        let objApprovalCheckList = aryApprovalCheckListFilterObj[0]as! NSDictionary
                        
                        let strWdoWorkflowStageType = "\(objApprovalCheckList.value(forKey: "WdoWorkflowStageType") ?? "")"
                        
                        if(checkflowStageType && checkflowStatus){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased()  && wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased() {
                                return true
                                
                            }
                        }
                        
                        else if(checkflowStageType){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased() {
                                return true
                                
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    // MARK: -  ------------------------------ Action ------------------------------
    @IBAction func actionOnSeeMore(_ sender: Any)
    {
      
        if (isInternetAvailable()){
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPad" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_SeeMore_AlerMessageVC") as? WDO_SeeMore_AlerMessageVC
            vc?.workOrderId = strWoId as String
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
    

    
    @IBAction func actionOnBack(_ sender: Any)
    {
        //dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnServiceDoc(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        goToServiceDocument()
        
    }
    
    @IBAction func actionOnClock(_ sender: Any)
    {

        self.view.endEditing(true)

        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.view.endEditing(true)

        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.view.endEditing(true)

        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.view.endEditing(true)

        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.view.endEditing(true)

        self.goToGlobalmage(strType: "Graph")
        
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.view.endEditing(true)

        self.goToNotesHistory()
        
    }
    
    
    @IBAction func actionOnSave(_ sender: Any)
    {

        self.view.endEditing(true)

        saveData()
        
    }
    
    // MARK: -  -----Actions View Customer Info   -----
    
    @IBAction func actionShowCustomerInfo(_ sender: Any)
    {
        
        showHideCustomerInfo()
        
    }
    
    @IBAction func actionOnNoEmail(_ sender: Any)
    {
        if (btnNoEmail.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            
            isNoEmail = true
            
            txtFldPrimaryEmailCustInfo.text = ""
            
            btnPrimaryEmailCustInfo.setTitle(txtFldPrimaryEmailCustInfo.text, for: .normal)
            
        }
        else
        {
            btnNoEmail.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            isNoEmail = false
            
        }
    }
    
    @IBAction func actionOnPrimaryEmailCustInfo(_ sender: Any)
    {
        if (txtFldPrimaryEmailCustInfo.text?.count)! > 0
        {
            global.emailComposer(txtFldPrimaryEmailCustInfo.text, "", "", self)
        }
    }
    
    @IBAction func actionOnSecondaryEmailCustInfo(_ sender: Any)
    {
        if (txtFldSecondaryEmailCustInfo.text?.count)! > 0
        {
            global.emailComposer(txtFldSecondaryEmailCustInfo.text, "", "", self)
        }
        
    }
    
    @IBAction func actionOnPrimaryPhoneCustInfo(_ sender: Any)
    {
        if ((txtFldPrimaryPhoneCustInfo.text?.count)!>0)
        {
            global.calling(txtFldPrimaryPhoneCustInfo.text)
            
        }
    }
    
    @IBAction func actionOnSecondaryPhoneCustInfo(_ sender: Any)
    {
        if ((txtFldSecondaryPhoneCustInfo.text?.count)!>0)
        {
            global.calling(txtFldSecondaryPhoneCustInfo.text)
            
        }
    }
    
    @IBAction func actionOnCellNoCustInfo(_ sender: Any)
    {
        if ((txtFldCellNoCustInfo.text?.count)!>0)
        {
            global.calling(txtFldCellNoCustInfo.text)
            
        }
    }
    
    @IBAction func actionOnAddressCustomerInfo(_ sender: Any)
    {
        
        self.view.endEditing(true)

        global.redirect(onAppleMap: self, btnAddressCustomerInfo.titleLabel?.text)
    }
    
    @IBAction func actionOnEditCustomerInfo(_ sender: Any)
    {
        
        if (btnEditCustomerInfo.currentImage == UIImage(named: "edit_ipad")!)
        {
            txtFldPrimaryEmailCustInfo.isHidden = false
            txtFldSecondaryEmailCustInfo.isHidden = false
            txtFldPrimaryPhoneCustInfo.isHidden = false
            txtFldSecondaryPhoneCustInfo.isHidden = false
            txtFldCellNoCustInfo.isHidden = false
            
            btnPrimaryEmailCustInfo.isHidden = true
            btnSecondaryEmailCustInfo.isHidden = true
            btnPrimaryPhoneCustInfo.isHidden = true
            btnSecondaryPhoneCustInfo.isHidden = true
            btnCellNoCustInfo.isHidden = true
            
            btnUploadEditImageServiceContact.isEnabled = true
            btnNoEmail.isEnabled = true
            
            btnEditCustomerInfo.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            // btnUploadEditImageServiceContact.isHidden = false
            
            const_ViewUnEditView_CustomerInfo_H.constant = 0
            const_ViewEditContactDetail_CuastomerInfo_H.constant = 260
            const_ViewCustomerInfo_H.constant = 495 - 130
        }
        else
        {
            txtFldPrimaryEmailCustInfo.isHidden = true
            txtFldSecondaryEmailCustInfo.isHidden = true
            txtFldPrimaryPhoneCustInfo.isHidden = true
            txtFldSecondaryPhoneCustInfo.isHidden = true
            txtFldCellNoCustInfo.isHidden = true
            
            btnPrimaryEmailCustInfo.isHidden = false
            btnSecondaryEmailCustInfo.isHidden = false
            btnPrimaryPhoneCustInfo.isHidden = false
            btnSecondaryPhoneCustInfo.isHidden = false
            btnCellNoCustInfo.isHidden = false
            //btnUploadEditImageServiceContact.isHidden = true
            
            btnUploadEditImageServiceContact.isEnabled = false
            btnNoEmail.isEnabled = false
            
            btnEditCustomerInfo.setImage(UIImage(named: "edit_ipad"), for: .normal)
            
            const_ViewUnEditView_CustomerInfo_H.constant = 130
            const_ViewEditContactDetail_CuastomerInfo_H.constant = 0
            const_ViewCustomerInfo_H.constant = 495 - 260
            
            btnPrimaryEmailCustInfo.setTitle( "\(txtFldPrimaryEmailCustInfo.text ?? "")", for: .normal)
            
            btnSecondaryEmailCustInfo.setTitle( "\(txtFldSecondaryEmailCustInfo.text ?? "")", for: .normal)
            
            btnPrimaryPhoneCustInfo.setTitle( "\(txtFldPrimaryPhoneCustInfo.text ?? "")", for: .normal)
            
            btnSecondaryPhoneCustInfo.setTitle( "\(txtFldSecondaryPhoneCustInfo.text ?? "")", for: .normal)
            
            btnCellNoCustInfo.setTitle( "\(txtFldCellNoCustInfo.text ?? "")", for: .normal)
            
        }
        
    }
    func editCustomerInfo()
    {
        
        
        btnEditCustomerInfo.setImage(UIImage(named: "edit_ipad"), for: .normal)
        txtFldPrimaryEmailCustInfo.isHidden = true
        txtFldSecondaryEmailCustInfo.isHidden = true
        txtFldPrimaryPhoneCustInfo.isHidden = true
        txtFldSecondaryPhoneCustInfo.isHidden = true
        txtFldCellNoCustInfo.isHidden = true
        
        btnPrimaryEmailCustInfo.isHidden = false
        btnSecondaryEmailCustInfo.isHidden = false
        btnPrimaryPhoneCustInfo.isHidden = false
        btnSecondaryPhoneCustInfo.isHidden = false
        btnCellNoCustInfo.isHidden = false
        
        // btnUploadEditImageServiceContact.isHidden = true
        
        btnUploadEditImageServiceContact.isEnabled = false
        btnNoEmail.isEnabled = false
        
        
        
        const_ViewEditContactDetail_CuastomerInfo_H.constant = 0
        btnEditCustomerInfo.setImage(UIImage(named: "edit_ipad"), for: .normal)
        const_ViewUnEditView_CustomerInfo_H.constant = 130
        const_ViewCustomerInfo_H.constant = 495 - 260
        
        
        
        
        const_BtnBillingContact_H.constant = 0
        btnEditBillingContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
        constViewBillingContactAddress_H.constant = 0 + 190
        
        constBtnServiceContact_H.constant = 0
        btnEditServiceContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
        constViewServiceContactAddress_H.constant = 0 + 190
        
        
        // setColorBorderForView(item: btnSelectBillingContactAddress)
        // setColorBorderForView(item: btnSelectServiceContactAddress)
        
        buttonRound(sender: btnSelectBillingContactAddress)
        buttonRound(sender: btnSelectServiceContactAddress)
        
        
    }
    // MARK: -  -----Actions View Work Order Info   -----
    
    @IBAction func actionShowWorkOrderInfo(_ sender: Any)
    {

        self.view.endEditing(true)

        showHideWorkOrderInfo()
        
    }
    
    @IBAction func actionOnCollectPayment(_ sender: Any)
    {
        
        /*
        if (btnCollectPayment.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            isCollectPayment = true
            
        }
        else
        {
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            isCollectPayment = false
            
        }
 
 */
        
    }
    
    // MARK: -  -----Actions View Billing Contact & Address   -----
    
    @IBAction func actionOnEditBillingContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        if (btnEditBillingContact.currentImage == UIImage(named: "edit_ipad")!)
        {
            const_BtnBillingContact_H.constant = 49
            btnEditBillingContact.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 0 + 190 + 49
            
            
            
        }
        else
        {
            const_BtnBillingContact_H.constant = 0
            btnEditBillingContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 0 + 190
            
            
        }
        
    }
    
    
    @IBAction func actionShowBillingContactAddress(_ sender: Any)
    {
        
        self.view.endEditing(true)

        showHideBillingContactAddress()
        
    }
    
    @IBAction func actionOnSelectBillingContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        let arrOfData = arrBillingAddressContactDetails
        
        if arrOfData.count > 0
        {
            btnSelectBillingContactAddress.tag = 43
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No record found", viewcontrol: self)
            const_BtnBillingContact_H.constant = 0
            btnEditBillingContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 0 + 190
        }
        
        
    }
    
    @IBAction func actionOnPrimaryEmailBillingContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /*if (lblPrimaryEmailBillingContact.text?.count)! > 0
         {
         global.emailComposer(lblPrimaryEmailBillingContact.text, "", "", self)
         }*/
        
        let strEmail = "\(btnPrimaryEmailBillingContact.titleLabel?.text ?? "")" as String
        
        if (strEmail.count>0)
        {
            global.emailComposer(strEmail, "", "", self)
            
        }
    }
    
    @IBAction func actionOnSecondaryEmailBililngContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /* if (lblSecondaryEmailBillingContact.text?.count)! > 0
         {
         global.emailComposer(lblSecondaryEmailBillingContact.text, "", "", self)
         }*/
        let strEmail = "\(btnSecondaryEmailBillingContact.titleLabel?.text ?? "")" as String
        
        if (strEmail.count>0)
        {
            global.emailComposer(strEmail, "", "", self)
            
        }
        
        
    }
    
    @IBAction func actionOnPrimaryPhoneBillingContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        // global.emailComposer(lblPrimaryPhoneBillingContact.text, "", "", self)
        
        let strNo = "\(btnPrimaryPhoneBillingContact.titleLabel?.text ?? "")" as String
        
        if (strNo.count>0)
        {
            global.calling(strNo)
            
        }
        
    }
    
    @IBAction func actionOnSecondaryPhoneBillingContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        // global.emailComposer(lblSecondaryPhoneBillingContact.text, "", "", self)
        
        /* let strNo = "\(btnSecondaryPhoneBillingContact.titleLabel?.text ?? "")" as String
         
         if (strNo.count>0)
         {
         global.calling(strNo)
         
         }*/
        
    }
    
    @IBAction func actionOnAddressBillingContact(_ sender: Any)
    {

        self.view.endEditing(true)

        global.redirect(onAppleMap: self, btnAddressBillingContact.titleLabel?.text)
    }
    
    // MARK: -  -----Actions View Service Contact & Address   -----
    
    @IBAction func actionOnEditServiceContact(_ sender: Any)
    {
        if (btnEditServiceContact.currentImage == UIImage(named: "edit_ipad")!)
        {
            
            constBtnServiceContact_H.constant = 49
            btnEditServiceContact.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 0 + 190 + 49
            
            
            
        }
        else
        {
            constBtnServiceContact_H.constant = 0
            btnEditServiceContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 0 + 190
            
            
        }
    }
    
    
    @IBAction func actionOnUploadEditImageServiceContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            //  BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        
        /* alert.popoverPresentationController?.sourceView = self.view
         
         self.present(alert, animated: true, completion: {
         
         
         })*/
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = alert.popoverPresentationController
            {
                currentPopoverpresentioncontroller.sourceView = btnUploadEditImageServiceContact
                currentPopoverpresentioncontroller.sourceRect = btnUploadEditImageServiceContact.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up
                self.present(alert, animated: true, completion: nil)
            }
                
            else{
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
    }
    @IBAction func actionShowServiceContactAddress(_ sender: Any)
    {
        
        self.view.endEditing(true)

        showHideServiceContactAddress()
        
    }
    
    @IBAction func actionOnSelectServiceContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        let arrOfData = arrServiceAddressContactDetails
        if arrOfData.count > 0
        {
            btnSelectServiceContactAddress.tag = 40
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
            
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No record found", viewcontrol: self)
            
            constBtnServiceContact_H.constant = 0
            btnEditServiceContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 0 + 190
        }
    }
    
    @IBAction func actionOnPrimaryEmailServiceContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /* if (lblPrimaryEmailServiceContact.text?.count)! > 0
         {
         global.emailComposer(lblPrimaryEmailServiceContact.text, "", "", self)
         }*/
        let strEmail = "\(btnPrimaryEmailServiceContact.titleLabel?.text ?? "")" as String
        
        if (strEmail.count>0)
        {
            global.emailComposer(strEmail, "", "", self)
            
        }
    }
    
    @IBAction func actionOnSecondaryEmailServiceContact(_ sender: Any)
    {
        /* if (lblSecondaryEmailServiceContact.text?.count)! > 0
         {
         global.emailComposer(lblSecondaryEmailServiceContact.text, "", "", self)
         }*/
        
        /* let strEmail = "\(btnPrimaryEmailServiceContact.titleLabel?.text ?? "")" as String
         
         if (strEmail.count>0)
         {
         global.emailComposer(strEmail, "", "", self)
         
         }*/
        
    }
    
    @IBAction func actionOnPrimaryPhoneServiceContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /*  if ((lblPrimaryPhoneServiceContact.text?.count)!>0)
         {
         global.calling(lblPrimaryPhoneServiceContact.text)
         
         }*/
        let strNo = "\(btnPrimaryPhoneServiceContact.titleLabel?.text ?? "")" as String
        
        if (strNo.count>0)
        {
            global.calling(strNo)
            
        }
        
    }
    
    @IBAction func actionOnSecondaryPhoneServiceContact(_ sender: Any)
    {
        /*  if ((lblSecondaryPhoneServiceContact.text?.count)!>0)
         {
         global.calling(lblSecondaryPhoneServiceContact.text)
         
         }*/
        
        /*let strNo = "\(btnSecondaryPhoneServiceContact.titleLabel?.text ?? "")" as String
         
         if (strNo.count>0)
         {
         global.calling(strNo)
         
         }*/
        
    }
    
    @IBAction func actionOnAddressServiceContact(_ sender: Any)
    {
        
        self.view.endEditing(true)

        global.redirect(onAppleMap: self, btnAddressServiceContact.titleLabel?.text)
        
    }
    
    // MARK: -  -----Actions View Upload Image   -----
    
    
    @IBAction func actionShowUploadImage(_ sender: Any)
    {
        
        self.view.endEditing(true)

        showHideUploadImage()
        
    }
    
    
    // MARK: -  -----Actions View Other  -----
    
    @IBAction func actionShowOther(_ sender: Any)
    {
        
        self.view.endEditing(true)

        showHideOther()
        
    }
    
    
    // MARK: -  -----Actions View Technician Comment   -----
    
    @IBAction func actionShowTechnicianComment(_ sender: Any)
    {
        
        self.view.endEditing(true)

        showHideTechnicianComment()
        
    }
    
    @IBAction func actionOnServiceStatus(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /* let arrOfData = arrData
         btnServiceStatus.tag = 42
         gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")*/
        
        let alert = UIAlertController(title: "", message: "Please choose status", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Incomplete", style: .default , handler:{ (UIAlertAction)in
            
            self.btnServiceStatus.setTitle("Incomplete", for: .normal)
            self.showHideResetReason()
            self.isResetReason = false
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Reset", style: .default , handler:{ (UIAlertAction)in
            
            self.btnServiceStatus.setTitle("Reset", for: .normal)
            self.showHideResetReason()
            self.isResetReason = true
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            self.showHideResetReason()
            
        }))
        /* alert.popoverPresentationController?.sourceView = self.view
         
         self.present(alert, animated: true, completion: {
         
         
         })*/
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = alert.popoverPresentationController
            {
                currentPopoverpresentioncontroller.sourceView = btnServiceStatus
                currentPopoverpresentioncontroller.sourceRect = btnServiceStatus.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.down
                self.present(alert, animated: true, completion: nil)
            }
                
            else{
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func actionOnResetReason(_ sender: Any)
    {
        
        self.view.endEditing(true)

        if (nsud.value(forKey: "MasterServiceAutomation") != nil) {
            
            let dictDetailServiceMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            
            if nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                
                if dictDetailServiceMaster.value(forKey: "ResetReasons") is NSArray {
                    
                    let arrOfLogType = (dictDetailServiceMaster.value(forKey: "ResetReasons") as! NSArray).mutableCopy()as! NSMutableArray
                    
                    if arrOfLogType.count > 0
                    {
                        let arrOfData = arrOfLogType
                        if arrOfData.count > 0
                        {
                            btnResetReason.tag = 41
                            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                        }
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)
                    }
                    
                }
                
            }
            
        }else{

            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data found", viewcontrol: self)

        }
        
    }
    
    // MARK: -  -----Actions View Time Range    -----
    
    @IBAction func actionReset(_ sender: Any)
    {
        
        self.view.endEditing(true)

        viewResetReason.isHidden = false
        
       // self.strServicesStatus = "Reset"
        viewResetReason.frame = self.view.frame
        self.view.addSubview(self.viewResetReason)
        
    }
    @IBAction func actionCancleResetReason(_ sender: UIButton)
    {
        
        if(sender.tag == 1){ // For Reset Click
            
            isResetReason = true
            self.strServicesStatus = "Reset"

            saveData()
            
        }else{
            
            btnResetReason.setTitle("---Select---", for: .normal)
            strResetId = ""
            txtViewResetReason.text = ""
            self.strServicesStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
            isResetReason = false
            self.viewResetReason.removeFromSuperview()

        }
        
        //self.viewResetReason.removeFromSuperview()
        
    }
    
    @IBAction func actionShowTimeRange(_ sender: Any)
    {
        
        showHideTimeRange()
        
    }
    
    @IBAction func actionOnStartTime(_ sender: Any)
    {
        
        self.view.endEditing(true)

        /* btnStartTime.tag = 10
         self.gotoDatePickerView(sender: sender as! UIButton, strType: "DateTime")*/
        
        let str = "\(lblStartTimeValue.titleLabel?.text ?? "")"
        
        if str == ""
        {
            getStartTimeIn()
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to reset start time", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes-Reset", style: .default , handler:{ (nil)in
                
                self.getStartTimeIn()
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (nil)in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    func getStartTimeIn()
    {
        yesEditedSomething = true
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var str = String()
        str = dateFormatter.string(from: date)
        lblStartTimeValue.setTitle(str, for: .normal)
    }
    
    // MARK: -  ----- Function  -----
    
    func ShowWarinigAlertMessage(isShow : Bool)  {
        
        //WdoPricingApprovalLatestDeclinednote
        //WdoWorkOrderApprovalLatestDeclinednote
        
        if isShow{
            lbl_WarningActionTitle.numberOfLines = 0
            lbl_WarningActionValue.numberOfLines = 0
            lbl_PricingNoteTitle.numberOfLines = 0
            lbl_PricingNoteValue.numberOfLines = 0
            if("\(objWorkorderDetail.value(forKey: "wdoWorkOrderApprovalLatestDeclinednote") ?? "")") == ""{
                lbl_WarningActionTitle.text = ""
                lbl_WarningActionValue.text = ""
            }else{
                lbl_WarningActionTitle.text = "\nWarning: Action Required"
                lbl_WarningActionValue.text = "\(objWorkorderDetail.value(forKey: "wdoWorkOrderApprovalLatestDeclinednote")!)"
            }
            
            if("\(objWorkorderDetail.value(forKey: "wdoPricingApprovalLatestDeclinednote") ?? "")") == ""{
                lbl_PricingNoteTitle.text = ""
                lbl_PricingNoteValue.text = ""
            }else{
                lbl_PricingNoteTitle.text = "Pricing Note"
                lbl_PricingNoteValue.text = "\(objWorkorderDetail.value(forKey: "wdoPricingApprovalLatestDeclinednote")!)\n"
            }
            
            if(lbl_WarningActionTitle.text == "" && lbl_PricingNoteTitle.text == ""){
                viewWarning.isHidden = true
            }else{
                viewWarning.isHidden = false
            }
            
        }else{
            
            lbl_WarningActionTitle.text = ""
            lbl_WarningActionValue.text = ""
            lbl_PricingNoteTitle.text = ""
            lbl_PricingNoteValue.text = ""

            viewWarning.isHidden = true
            
        }
        
    }
    
    
    func updateWdoOppFlowType(strLeadNoL : String)
    {
        // Update Opp Flow Type to Commercial for WDO Opp always.
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("flowType")
        
        arrOfValues.add("Commercial")
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strLeadNoL), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
            // Updated SuccessFully
            
        }
        
    }

    func saveData() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
        {
            self.goToInspectionView()
        }
        else
        {
            trimTextField()
            
            if ((global.checkIfCommaSepartedEmailsAreValid(txtFldPrimaryEmailCustInfo.text!) == false) && ((txtFldPrimaryEmailCustInfo.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid Primary Email", viewcontrol: self)
            }
            else if ((global.checkIfCommaSepartedEmailsAreValid(txtFldSecondaryEmailCustInfo.text!) == false) && ((txtFldSecondaryEmailCustInfo.text?.count)!>0 ) )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid Secondary Email", viewcontrol: self)
            }
            else if("\(lblStartTimeValue.titleLabel?.text ?? "")" == "")
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter the Start Time", viewcontrol: self)
            }
            else
            {
                
                if isResetReason == true
                {
                    //  let strResetReason = "\(btnResetReason.titleLabel?.text ?? "")"
                    
                    if (strResetId == "")
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select reset reason", viewcontrol: self)
                    }
                    else
                    {
                        
                        if checkImage() == true
                        {
                            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one before image", viewcontrol: self)
                        }
                        else
                        {
                            
                            updateWorkOrderDetail()
                            saveEmailToCoreData()
                            saveEmailToCoreDataSales()
                            self.goToSendMailOnReset()
                            
                        }
                    }
                }
                else
                {
                    if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one before image", viewcontrol: self)
                    }
                    else
                    {
                        updateWorkOrderDetail()
                        saveEmailToCoreData()
                         saveEmailToCoreDataSales()
                        self.goToInspectionView()
                    }
                }
                
            }
            
        }
        
    }
    
    func goToSendMailOnReset() {
        
        var image :UIImage?
        let currentLayer = UIApplication.shared.keyWindow!.layer
        let currentScale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(currentLayer.frame.size, false, currentScale);
        guard let currentContext = UIGraphicsGetCurrentContext() else {return}
        currentLayer.render(in: currentContext)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.viewResetReason.removeFromSuperview()
        
        nsud.set(true, forKey: "isFromSurveyToSendEmail")
        nsud.synchronize()
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewControlleriPad") as? ServiceSendMailViewControlleriPad
        testController?.fromWhere = "WDO"
        testController?.strWoIdd = strWoId as String
        testController?.strBackRoundImage = image
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    
    func checkImage() -> Bool
    {
        let chkImage = nsud.bool(forKey: "isCompulsoryBeforeImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "Before"))
            
            if arryOfData.count > 0
            {
                
                return false
                
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    
    func getClockStatus()
    {
        var strClockStatus = String()
        
        if !isInternetAvailable()
        {
            
            strClockStatus = global.fetchClockInOutDetailCoreData()
            
            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
            {
                btnClock.setImage(UIImage(named: "white"), for: .normal)
            }
            else if strClockStatus == "WorkingTime"
            {
                btnClock.setImage(UIImage(named: "green"), for: .normal)
                
            }
            else if strClockStatus == "BreakTime"
            {
                btnClock.setImage(UIImage(named: "orange"), for: .normal)
            }
            else
            {
                btnClock.setImage(UIImage(named: "red"), for: .normal)
                
            }
        }
        else
        {
            DispatchQueue.global(qos: .background).async
                {
                    strClockStatus = self.global.getCurrentTimerOfClock()
                    
                    DispatchQueue.main.async
                        {
                            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
                            {
                                self.btnClock.setImage(UIImage(named: "white"), for: .normal)
                            }
                            else if strClockStatus == "WorkingTime"
                            {
                                self.btnClock.setImage(UIImage(named: "green"), for: .normal)
                                
                            }
                            else if strClockStatus == "BreakTime"
                            {
                                self.btnClock.setImage(UIImage(named: "orange"), for: .normal)
                            }
                            else
                            {
                                self.btnClock.setImage(UIImage(named: "red"), for: .normal)
                                
                            }
                            
                    }
            }
        }
        
    }
    
    
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
            
        }
        else
        {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func saveEmailToCoreData()
    {
        
        let strEmailPrimary = txtFldPrimaryEmailCustInfo.text
        
        let strEmailScondary = txtFldSecondaryEmailCustInfo.text
        
        
        var arrEmailPrimary = strEmailPrimary!.components(separatedBy: ",") as NSArray
        
        //let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        arrEmailPrimary = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrEmailPrimary)")
        
        
        var arrEmailSecondary = strEmailScondary!.components(separatedBy: ",") as NSArray
        
        arrEmailSecondary = arrEmailSecondary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        let arrEmailToSave = NSMutableArray()
        
        
        for i in 0 ..< arrEmailPrimary.count
        {
            let strPrimaryEmail = "\(arrEmailPrimary[i])"
            var chkEmailExist = Bool()
            chkEmailExist = false
            
            for j in 0 ..< arryOfData.count
            {
                
                let  objEmailDetail = arryOfData[j] as! NSManagedObject
                let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                if strEmailId == strPrimaryEmail
                {
                    chkEmailExist = true
                    break
                }
                else
                {
                    chkEmailExist = false
                }
            }
            
            if chkEmailExist == false
            {
                arrEmailToSave.add(strPrimaryEmail)
            }
            
        }
        for i in 0 ..< arrEmailSecondary.count
        {
            let strSecondaryEmail = "\(arrEmailSecondary[i])"
            
            if strSecondaryEmail.count > 0
            {
                var chkEmailExist = Bool()
                chkEmailExist = false
                
                for j in 0 ..< arryOfData.count
                {
                    
                    let  objEmailDetail = arryOfData[j] as! NSManagedObject
                    let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                    if strEmailId == strSecondaryEmail
                    {
                        chkEmailExist = true
                        break
                    }
                    else
                    {
                        chkEmailExist = false
                    }
                }
                
                if chkEmailExist == false
                {
                    arrEmailToSave.add(strSecondaryEmail)
                }
            }
        }
        
        //NSArray *uniqueArray = [[NSSet setWithArray:duplicateArray] allObjects];
        
        /*   let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
         print("Unique array \(conciseUniqueValues)")*/
        
        //conciseUniqueValues = [5, 6, 9, 7, 4]
        
        for i in 0 ..< arrEmailToSave.count
        {
            let strEmailToSave = "\(arrEmailToSave[i])"
            
            if strEmailToSave.count > 0
            {
                
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                arrOfKeys = ["createdBy",
                             "createdDate",
                             "emailId",
                             "isCustomerEmail",
                             "isMailSent",
                             "companyKey",
                             "modifiedBy",
                             "modifiedDate",
                             "subject",
                             "userName",
                             "workorderId",
                             "woInvoiceMailId"]
                
                arrOfValues = ["0",
                               "0",
                               strEmailToSave,
                               "true",
                               "true",
                               strCompanyKey,
                               "",
                               global.getCurrentDate(),
                               "",
                               strUserName,
                               strWoId,
                               ""]
                
                saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            
        }
        
        
    }
    func saveEmailToCoreDataSales()
    {
        
        let strEmailPrimary = txtFldPrimaryEmailCustInfo.text
        
        let strEmailScondary = txtFldSecondaryEmailCustInfo.text
        
        
        var arrEmailPrimary = strEmailPrimary!.components(separatedBy: ",") as NSArray
        
        //let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        arrEmailPrimary = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        print("Unique array \(arrEmailPrimary)")
        
        
        var arrEmailSecondary = strEmailScondary!.components(separatedBy: ",") as NSArray
        
        arrEmailSecondary = arrEmailSecondary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdGlobal))
        
        let arrEmailToSave = NSMutableArray()
        
        
        for i in 0 ..< arrEmailPrimary.count
        {
            let strPrimaryEmail = "\(arrEmailPrimary[i])"
            var chkEmailExist = Bool()
            chkEmailExist = false
            
            for j in 0 ..< arryOfData.count
            {
                
                let  objEmailDetail = arryOfData[j] as! NSManagedObject
                let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                if strEmailId == strPrimaryEmail
                {
                    chkEmailExist = true
                    break
                }
                else
                {
                    chkEmailExist = false
                }
            }
            
            if chkEmailExist == false
            {
                arrEmailToSave.add(strPrimaryEmail)
            }
            
        }
        for i in 0 ..< arrEmailSecondary.count
        {
            let strSecondaryEmail = "\(arrEmailSecondary[i])"
            
            if strSecondaryEmail.count > 0
            {
                var chkEmailExist = Bool()
                chkEmailExist = false
                
                for j in 0 ..< arryOfData.count
                {
                    
                    let  objEmailDetail = arryOfData[j] as! NSManagedObject
                    let strEmailId = "\(objEmailDetail.value(forKey: "emailId")!)"
                    if strEmailId == strSecondaryEmail
                    {
                        chkEmailExist = true
                        break
                    }
                    else
                    {
                        chkEmailExist = false
                    }
                }
                
                if chkEmailExist == false
                {
                    arrEmailToSave.add(strSecondaryEmail)
                }
            }
        }
        
        //NSArray *uniqueArray = [[NSSet setWithArray:duplicateArray] allObjects];
        
        /*   let conciseUniqueValues = arrEmailPrimary.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
         print("Unique array \(conciseUniqueValues)")*/
        
        //conciseUniqueValues = [5, 6, 9, 7, 4]
        
        for i in 0 ..< arrEmailToSave.count
        {
            let strEmailToSave = "\(arrEmailToSave[i])"
            
            if strEmailToSave.count > 0
            {
                
                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                
                arrOfKeys = ["createdBy",
                             "createdDate",
                             "emailId",
                             "isCustomerEmail",
                             "isMailSent",
                             "companyKey",
                             "modifiedBy",
                             "modifiedDate",
                             "subject",
                             "userName",
                             "leadId",
                             "leadMailId"]
                
                arrOfValues = ["0",
                               "0",
                               strEmailToSave,
                               "true",
                               "true",
                               strCompanyKey,
                               "",
                               global.getCurrentDate(),
                               "",
                               strUserName,
                               strLeadIdGlobal,
                               ""]
                
                saveDataInDB(strEntity: "EmailDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            
        }
        
        
    }
    func changeStringDateToGivenFormat(strDate: String , strRequiredFormat: String) -> String
    {
        let strTimeIn  = strDate
        
        var dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date1 = Date()
        var strDateFinal = String()
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        }
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        else
        {
            dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        }
        
        
        if dateFormatter.date(from: strTimeIn) != nil
        {
            date1 = Date()
            strDateFinal = String()
            date1 = dateFormatter.date(from: strTimeIn)!
            dateFormatter.dateFormat = strRequiredFormat
            strDateFinal = dateFormatter.string(from: date1)
            return strDateFinal
        }
        
        return ""
        
    }
    
    
    
    func fetchServiceAddressContactDetails()
    {
        //ServiceAddressPOCDetailDcs
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ServiceAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0
        {
            
            let  objServiceAddressContactDetails = arryOfData[0] as! NSManagedObject
            
            arrServiceAddressContactDetails = objServiceAddressContactDetails.value(forKey: "pocDetails")  as! NSMutableArray
            
        }
        else
        {
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, assignstrMessage: alertNoWo, viewcontrol: self)
            
            //self.back()
            
        }
    }
    func fetchBillingAddressContactDetails()
    {
        //ServiceAddressPOCDetailDcs
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "BillingAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0
        {
            let  objBillingAddressContactDetails = arryOfData[0] as! NSManagedObject

            arrBillingAddressContactDetails = objBillingAddressContactDetails.value(forKey: "pocDetails") as!  NSMutableArray
            
        }
        else
        {
            
            // showAlertWithoutAnyAction(strtitle: alertMessage, assignstrMessage: alertNoWo, viewcontrol: self)
            
            //self.back()
            
        }
    }
    
    
    func goToInspectionView()
    {
        
        let isNPMATermite = "\(objWorkorderDetail.value(forKey: "isNPMATermite")!)"

        if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
            
            self.goToNPMAInspectionView()
            
        }else{
            
            nsud.set(false, forKey: "yesupdatedWdoInspection")
            nsud.set(false, forKey: "loadeDataRefreshConfigureProposalView")
            nsud.set(false, forKey: "loadeDataRefreshAgreementView")
            nsud.set(false, forKey: "loadeDataRefreshFinalizeView")
            nsud.synchronize()
            
            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "ProblemIdentification_WDOVC") as? ProblemIdentification_WDOVC
            testController?.strWoId = strWoId
            self.navigationController?.pushViewController(testController!, animated: false)
            
        }
        
    }
    
    func goToNPMAInspectionView()
    {
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Inspection_iPadVC") as? NPMA_Inspection_iPadVC
        testController?.strWoId = strWoId
        self.navigationController?.pushViewController(testController!, animated: false)
        
    }
    
    func goToServiceDocument() {
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    func assignValues()
    {
        print("WorkOrderDetails \(objWorkorderDetail)")
        
        // Putting Wdo Lead Is In DB
        
        let strDepartmentTypeLocal = "\(objWorkorderDetail.value(forKey: "departmentType") ?? "")"
        let strServiceStateLocal = "\(objWorkorderDetail.value(forKey: "serviceState") ?? "")"
        
        if strDepartmentTypeLocal == "Termite" || strServiceStateLocal == "California" {
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                //LeadId  defsLeadName leadName
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                let strWoLeadId = "\(match.value(forKey: "leadId")!)"
                let strLeadName = "\(match.value(forKey: "leadName")!)"

                strLeadIdGlobal = strWoLeadId
                
                nsud.set(strWoLeadId, forKey: "WdoLeadId")
                nsud.set(strWoLeadId, forKey: "LeadId")
                nsud.set(strLeadName, forKey: "defsLeadName")

                nsud.synchronize()
                
                // Save isWdoLead to true in DB
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("isWdoLead")
                arrOfValues.add("true")
                
                let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    
                }
                
            }
            
        }
        
        strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        nsud.set(strWorkOrderStatus, forKey: "workOrderStatus")
        nsud.synchronize()
        
        strServicePocId = "\(objWorkorderDetail.value(forKey: "servicePOCId") ?? "")"
        strBillingPocId = "\(objWorkorderDetail.value(forKey: "billingPOCId") ?? "")"
        
        
        let strIsCollectPayment = "\(objWorkorderDetail.value(forKey: "isCollectPayment") ?? "")"
        
        if strIsCollectPayment == "1" || strIsCollectPayment == "true" || strIsCollectPayment == "True" {
            
            isCollectPayment = true
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            
        }
        else
        {
            
            isCollectPayment = false
            btnCollectPayment.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            
        }
        
        
        
        //Service Address Image Download
        strServiceAddImageName = "\(objWorkorderDetail.value(forKey: "serviceAddressImagePath") ?? "")"
        
        if strServiceAddImageName.count > 0
        {
            downloadServiceAddImage()
        }
        
        btnAddressCustomerInfo.setTitle( global.strCombinedAddressService(for: objWorkorderDetail), for: .normal)
        
        
        lblCustomerName.text = global.strFullName(for: objWorkorderDetail)//"\(objWorkorderDetail.value(forKey: "") ?? "")"
        
        lblCompanyName.text = "\(objWorkorderDetail.value(forKey: "companyName") ?? "N/A")"
        
        lblAccNoWorkOrderInfo.text = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        
        
        txtFldPrimaryEmailCustInfo.text = "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")"
        btnPrimaryEmailCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")", for: .normal)
        
        
        txtFldSecondaryEmailCustInfo.text = "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")"
        btnSecondaryEmailCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "secondaryEmail") ?? "")", for: .normal)
        
        
        
        txtFldPrimaryPhoneCustInfo.text = "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")"
        btnPrimaryPhoneCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")", for: .normal)
        
        
        txtFldSecondaryPhoneCustInfo.text = "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")"
        btnSecondaryPhoneCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "secondaryPhone") ?? "")", for: .normal)
        
        txtFldCellNoCustInfo.text = "\(objWorkorderDetail.value(forKey: "cellNo") ?? "")"
        btnCellNoCustInfo.setTitle( "\(objWorkorderDetail.value(forKey: "cellNo") ?? "")", for: .normal)
        
        lblCategory.text = "\(objWorkorderDetail.value(forKey: "categoryName") ?? "")"
        
        lblServices.text = "\(objWorkorderDetail.value(forKey: "services") ?? "")"
        
        lblEmployeeNo.text = "\(objWorkorderDetail.value(forKey: "employeeNo") ?? "")"
        
        lblEmployeeName.text = strEmpName
        
        lblEmployeeNo.text = strEmpName + " " + "(#" + "\(lblEmployeeNo.text ?? "")" + ")"
        
        
        
        lblKeyMap.text = "\(objWorkorderDetail.value(forKey: "keyMap") ?? "")"
        
        lblRoutNo.text = "\(objWorkorderDetail.value(forKey: "routeNo") ?? "")"
        
        lblRoutName.text = "\(objWorkorderDetail.value(forKey: "routeName") ?? "")"
        
        lblRoutNo.text =  "\(lblRoutName.text ?? "")" + " " + "(#" + "\(lblRoutNo.text ?? "")" + ")"
        
        
        
        lblRoutAlert.text = ""
        
        lblScheduleStartDateTime.text = "\(objWorkorderDetail.value(forKey: "scheduleOnStartDateTime") ?? "")"
        
        lblScheduleEndDateTime.text = "\(objWorkorderDetail.value(forKey: "scheduleOnEndDateTime") ?? "")"
        
        lblEarliestStartTime.text = "\(objWorkorderDetail.value(forKey: "earliestStartTimeStr") ?? "")"
        
        lblLatestStartTime.text = "\(objWorkorderDetail.value(forKey: "latestStartTimeStr") ?? "")"
        
        lblFromTime.text = "" //"\(objWorkorderDetail.value(forKey: "EmployeeNo") ?? "")"
        
        lblToTime.text = "" //"\(objWorkorderDetail.value(forKey: "EmployeeNo") ?? "")"
        
        
        
        //Billing
        
        var strBillingContactName = String()
        strBillingContactName = "\(objWorkorderDetail.value(forKey: "billingFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "billingLastName") ?? "")"
        
        lblContactNameBillingContact.text = strBillingContactName
        
        //  lblMapCodeBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingMapCode") ?? "")"
        
        //  lblPrimaryEmailBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingPrimaryEmail") ?? "")"
        //      lblSecondaryEmailBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingSecondaryEmail") ?? "")"
        
        // lblPrimaryPhoneBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingPrimaryPhone") ?? "")"
        //      lblSecondaryPhoneBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingSecondaryPhone") ?? "")"
        btnAddressBillingContact.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
        
        //     lblCountyBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingCounty") ?? "")"
        //      lblSchoolDistrictBillingContact.text = "\(objWorkorderDetail.value(forKey: "BillingSchoolDistrict") ?? "")"
        
        
        
        btnPrimaryEmailBillingContact.setTitle( "\(objWorkorderDetail.value(forKey: "billingPrimaryEmail") ?? "")", for: .normal)
        //btnSecondaryEmailBillingContact.setTitle( lblSecondaryEmailBillingContact.text, for: .normal)
        btnPrimaryPhoneBillingContact.setTitle("\(objWorkorderDetail.value(forKey: "billingPrimaryPhone") ?? "")", for: .normal)
        
        //btnSecondaryPhoneBillingContact.setTitle(lblSecondaryPhoneBillingContact.text, for: .normal)
        
        
        
        
        //Service
        
        var strServiceContactName = String()
        strServiceContactName = "\(objWorkorderDetail.value(forKey: "serviceFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceLastName") ?? "")"
        lblContactNameServiceContact.text = strServiceContactName
        
        //   lblMapCodeServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServiceMapCode") ?? "")"
        //    lblPrimaryEmailServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServicePrimaryEmail") ?? "")"
        //    lblSecondaryEmailServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServiceSecondaryEmail") ?? "")"
        //   lblPrimaryPhoneServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServicePrimaryPhone") ?? "")"
        //       lblSecondaryPhoneServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServiceSecondaryPhone") ?? "")"
        btnAddressServiceContact.setTitle( global.strCombinedAddressService(for: objWorkorderDetail), for: .normal)
        
        //   lblCountyServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServiceCounty") ?? "")"
        //   lblSchoolDistrictServiceContact.text = "\(objWorkorderDetail.value(forKey: "ServiceSchoolDistrict") ?? "")"
        
        
        btnPrimaryEmailServiceContact.setTitle( "\(objWorkorderDetail.value(forKey: "servicePrimaryEmail") ?? "")", for: .normal)
        // btnSecondaryEmailServiceContact.setTitle( lblSecondaryEmailServiceContact.text, for: .normal)
        btnPrimaryPhoneServiceContact.setTitle("\(objWorkorderDetail.value(forKey: "servicePrimaryPhone") ?? "")", for: .normal)
        
        //btnSecondaryPhoneServiceContact.setTitle(lblSecondaryPhoneServiceContact.text, for: .normal)
        
        
        
        
        txtViewServiceDescription.text  = "\(objWorkorderDetail.value(forKey: "serviceDescription") ?? "")"
        
        txtViewServiceInstruction.text = "\(objWorkorderDetail.value(forKey: "serviceInstruction") ?? "")"
        
        txtViewTarget.text = "\(objWorkorderDetail.value(forKey: "targets") ?? "")"
        
        txtViewAttribute.text = "\(objWorkorderDetail.value(forKey: "atributes") ?? "")"
        
        txtViewSetupInstruction.text = "\(objWorkorderDetail.value(forKey: "specialInstruction") ?? "")"
        
        txtViewTechnicianComment.text = "\(objWorkorderDetail.value(forKey: "technicianComment") ?? "")"
        
        //btnServiceStatus.setTitle("\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")", for: .normal) //workorderStatus
        strServicesStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        // lblStartTimeValue.setTitle("\(objWorkorderDetail.value(forKey: "TimeIn") ?? "")", for: .normal)
        
        
        let strTimeIn  = "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
        
        lblStartTimeValue.setTitle(changeStringDateToGivenFormat(strDate: strTimeIn, strRequiredFormat: "MM/dd/yyyy hh:mm a"), for: .normal)
        
        
        
        
        
        
        nsud.set("", forKey: "lblThirdPartyAccountNo")
        nsud.synchronize()
        
        var strThirdPartyAccountNo = String()
        strThirdPartyAccountNo = "\(objWorkorderDetail.value(forKey: "thirdPartyAccountNo") ?? "")"
        if strThirdPartyAccountNo.count > 0
        {
            lblTitle.text = "A/C #: " + strThirdPartyAccountNo + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
            
            nsud.set(lblTitle.text, forKey: "lblThirdPartyAccountNo")
            nsud.synchronize()
        }
        else
        {
            lblTitle.text = "A/C #: " + "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")" + " Order #: " + "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        }
        
        nsud.set(lblTitle.text, forKey: "lblName")
        nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "workOrderAccountNo")
        nsud.set("\(objWorkorderDetail.value(forKey: "accountNo")!)", forKey: "AccNoService")
        nsud.set("\(objWorkorderDetail.value(forKey: "departmentId")!)", forKey: "DepartmentIdService")
        nsud.synchronize()
        
        
        // For Reset
        
        strResetId = "\(objWorkorderDetail.value(forKey: "resetId") ?? "")"

        if strResetId.count > 0 {
            
            if nsud.value(forKey: "MasterServiceAutomation") != nil {
                
                if nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                    
                    let dictDetailServiceMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
                    
                    if dictDetailServiceMaster.value(forKey: "ResetReasons") is NSArray {
                        
                        let arrOfLogType = dictDetailServiceMaster.value(forKey: "ResetReasons") as! NSArray
                        //strResetId = "\(objWorkorderDetail.value(forKey: "ResetId") ?? "")"
                        if arrOfLogType.count > 0
                        {
                            for i in 0 ..< arrOfLogType.count
                            {
                                let dict = arrOfLogType[i] as! NSDictionary
                                
                                if strResetId == "\(dict.value(forKey: "ResetId")!)"
                                {
                                    btnResetReason.setTitle(dict["ResetReason"] as? String, for: .normal)
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        
        if(strWorkOrderStatus.caseInsensitiveCompare("Reset") == ComparisonResult.orderedSame)
        {
            isResetReason = true
        }
        else if(strWorkOrderStatus.caseInsensitiveCompare("InComplete") == ComparisonResult.orderedSame)
        {
            isResetReason = false
        }
        else
        {
            isResetReason = false
        }
        
        
        let strWoNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        
        if nsud.value(forKey: "ListOfUnsignedDoc") is NSArray
        {
            let arrOfUnsignedDocs = nsud.value(forKey: "ListOfUnsignedDoc") as! NSArray
            
            if arrOfUnsignedDocs .contains(strWoNo)
            {
                btnServiceDocs.setImage(UIImage(named: "document_3"), for: .normal)
            }
            else
            {
                btnServiceDocs.setImage(UIImage(named: "document_2"), for: .normal)
                
            }
        }
        else
        {
            btnServiceDocs.setImage(UIImage(named: "document_2"), for: .normal)
            
        }
        
        if arrBillingAddressContactDetails.count != 0
        {
            for item in arrBillingAddressContactDetails
            {
                let dict = item as! NSDictionary
                
                let strId = "\(dict.value(forKey: "ContactId") ?? "")"
                
                if strBillingPocId == strId
                {
                    let strName = "\(dict.value(forKey: "FirstName") ?? "")" + " " + "\(dict.value(forKey: "MiddleName") ?? "")" + " " + "\(dict.value(forKey: "LastName") ?? "")"
                    btnSelectBillingContactAddress.setTitle(strName, for: .normal)
                    
                    break
                }
                
            }
        }
        
        if arrServiceAddressContactDetails.count != 0
        {
            for item in arrServiceAddressContactDetails
            {
                let dict = item as! NSDictionary
                
                let strId = "\(dict.value(forKey: "ContactId") ?? "")"
                
                if strServicePocId == strId
                {
                    
                    let strName = "\(dict.value(forKey: "FirstName") ?? "")" + " "  + "\(dict.value(forKey: "MiddleName") ?? "")" + " " + "\(dict.value(forKey: "LastName") ?? "")"
                    btnSelectServiceContactAddress.setTitle(strName, for: .normal)
                    
                    break
                }
                
            }
        }
        
        txtViewAccountAlert.text = "\(objWorkorderDetail.value(forKey: "accountDescription") ?? "")"
        
    }
    func downloadGraphXML() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
            
            var strURL = String()
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            
            strURL = strURL + "\(xmlFileName)"
            
            strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
            
            DispatchQueue.global(qos: .background).async {
                
                let url = URL(string:strURL)
                let data = try? Data(contentsOf: url!)
                
                if data != nil && (data?.count)! > 0 {
                    
                    DispatchQueue.main.async {
                        
                        savepdfDocumentDirectory(strFileName: xmlFileName, dataa: data!)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func downloadServiceAddImage()
    {
        
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strServiceAddImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strServiceAddImageName)
        
        if isImageExists!
        {
            imgViewServiceContactAddress.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strServiceAddImageUrl = String()
            
            strServiceAddImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //let strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            var strUrl = String()//= strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            
            if strServiceAddImageName.contains("Documents")
            {
                strUrl = strServiceAddImageUrl  + strServiceAddImageName
            }
            else
            {
                strUrl = strServiceAddImageUrl + "//Documents/" + strServiceAddImageName
            }
            
            //strUrl  = strUrl.replacingOccurrences(of: "\\", with: "/")
            //strUrl = replaceBackSlasheFromUrl(strUrl: strUrl)
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
            
            
            let strUrlwithString = strUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""

            

            //let nsUrl = URL(string: strUrl)
            let nsUrl = URL(string: strUrlwithString)


            imgViewServiceContactAddress.load(url: nsUrl! as URL , strImageName: strServiceAddImageName)
        }
        
    }
    func methodForDisable()
    {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
        {
            txtViewAccountAlert.isEditable = false
            txtViewServiceDescription.isEditable = false
            txtViewServiceInstruction.isEditable = false
            txtViewSetupInstruction.isEditable = false
            txtViewAttribute.isEditable = false
            txtViewTechnicianComment.isEditable = false
            txtViewTarget.isEditable = false
            
            /* txtFldPrimaryEmailCustInfo.isHidden = true
             txtFldSecondaryEmailCustInfo.isHidden = true
             txtFldPrimaryPhoneCustInfo.isHidden = true
             txtFldSecondaryPhoneCustInfo.isHidden = true
             txtFldCellNoCustInfo.isHidden = true
             
             btnPrimaryEmailCustInfo.isHidden = false
             btnSecondaryEmailCustInfo.isHidden = false
             btnPrimaryPhoneCustInfo.isHidden = false
             btnSecondaryPhoneCustInfo.isHidden = false
             btnCellNoCustInfo.isHidden = false*/
            
            btnNoEmail.isEnabled = false
            btnCollectPayment.isEnabled = false
            
            //  btnSelectServiceContactAddress.isEnabled = false
            //  btnSelectBillingContactAddress.isEnabled = false
            
            btnUploadEditImageServiceContact.isEnabled = false
            // btnServiceStatus.isEnabled = false
            btnResetReason.isEnabled = false
            lblStartTimeValue.isEnabled = false
            
            btnEditCustomerInfo.isEnabled = false
            btnEditBillingContact.isEnabled = false
            btnEditServiceContact.isEnabled = false
            btnReset.isEnabled = false
            btnStartTime.isEnabled = false

            
        }
        else
        {
            
            btnStartTime.isEnabled = true
            btnReset.isEnabled = true
            txtViewAccountAlert.isEditable = false
            txtViewServiceDescription.isEditable = false
            txtViewServiceInstruction.isEditable = false
            txtViewSetupInstruction.isEditable = false
            txtViewAttribute.isEditable = false
            txtViewTechnicianComment.isEditable = true
            txtViewTarget.isEditable = false
            
            
            /*txtFldPrimaryEmailCustInfo.isHidden = false
             txtFldSecondaryEmailCustInfo.isHidden = false
             txtFldPrimaryPhoneCustInfo.isHidden = false
             txtFldSecondaryPhoneCustInfo.isHidden = false
             txtFldCellNoCustInfo.isHidden = false
             
             btnPrimaryEmailCustInfo.isHidden = true
             btnSecondaryEmailCustInfo.isHidden = true
             btnPrimaryPhoneCustInfo.isHidden = true
             btnSecondaryPhoneCustInfo.isHidden = true
             btnCellNoCustInfo.isHidden = true*/
            
            btnNoEmail.isEnabled = true
            btnCollectPayment.isEnabled = true
            // btnSelectServiceContactAddress.isEnabled = true
            // btnSelectBillingContactAddress.isEnabled = true
            btnUploadEditImageServiceContact.isEnabled = true
            // btnServiceStatus.isEnabled = true
            btnResetReason.isEnabled = true
            lblStartTimeValue.isEnabled = true
            
            
            btnEditCustomerInfo.isEnabled = true
            btnEditBillingContact.isEnabled = true
            btnEditServiceContact.isEnabled = true
        }
        /* txtFldPrimaryEmailCustInfo.isHidden = true
         txtFldSecondaryEmailCustInfo.isHidden = true
         txtFldPrimaryPhoneCustInfo.isHidden = true
         txtFldSecondaryPhoneCustInfo.isHidden = true
         txtFldCellNoCustInfo.isHidden = true
         
         btnPrimaryEmailCustInfo.isHidden = false
         btnSecondaryEmailCustInfo.isHidden = false
         btnPrimaryPhoneCustInfo.isHidden = false
         btnSecondaryPhoneCustInfo.isHidden = false
         btnCellNoCustInfo.isHidden = false*/
        
    }
    func updateWorkOrderDetail()
    {
        strTimeIn = lblStartTimeValue.titleLabel?.text ?? ""
        strTimeIn = changeStringDateToGivenFormat(strDate: strTimeIn, strRequiredFormat: "MM/dd/yyyy HH:mm:ss")
        
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        //Key Array
        arrOfKeys.add("PrimaryEmail")
        arrOfKeys.add("SecondaryEmail")
        arrOfKeys.add("PrimaryPhone")
        arrOfKeys.add("SecondaryPhone")
        arrOfKeys.add("CellNo")
        //arrOfKeys.add("AccountAlert")
        
        arrOfKeys.add("ServiceDescription")
        arrOfKeys.add("Targets")
        arrOfKeys.add("Atributes")
        arrOfKeys.add("ServiceInstruction")
        arrOfKeys.add("SpecialInstruction")
        arrOfKeys.add("TechnicianComment")
        arrOfKeys.add("SpecialInstruction")
        arrOfKeys.add("ServiceAddressImagePath")
        arrOfKeys.add("TimeIn")
        arrOfKeys.add("resetId")
        arrOfKeys.add("resetDescription")
        arrOfKeys.add("workorderStatus")
        arrOfKeys.add("billingPOCId")
        arrOfKeys.add("ServicePOCId")
        
        arrOfKeys.add("isCollectPayment")
        
        
        //Value Array
        
        arrOfValues.add(txtFldPrimaryEmailCustInfo.text ?? "")
        arrOfValues.add(txtFldSecondaryEmailCustInfo.text ?? "")
        arrOfValues.add(txtFldPrimaryPhoneCustInfo.text ?? "")
        arrOfValues.add(txtFldSecondaryPhoneCustInfo.text ?? "")
        arrOfValues.add(txtFldCellNoCustInfo.text ?? "")
        //arrOfValues.add(txtViewAccountAlert.text ?? "")
        
        arrOfValues.add(txtViewServiceDescription.text ?? "")
        arrOfValues.add(txtViewTarget.text ?? "")
        arrOfValues.add(txtViewAttribute.text ?? "")
        arrOfValues.add(txtViewServiceInstruction.text ?? "")
        arrOfValues.add(txtViewServiceInstruction.text ?? "")
        arrOfValues.add(txtViewTechnicianComment.text ?? "")
        arrOfValues.add(txtViewSetupInstruction.text ?? "")
        arrOfValues.add(strServiceAddImageName )
        arrOfValues.add(strTimeIn)
        arrOfValues.add(strResetId)
        arrOfValues.add(txtViewResetReason.text ?? "")
        arrOfValues.add("\(strServicesStatus)")
        arrOfValues.add(strBillingPocId)
        arrOfValues.add(strServicePocId)
        
        if isCollectPayment {
            
            arrOfValues.add("true")
            
        } else {
            
            arrOfValues.add("false")
            
        }
        
        //Service POC
        if dictServiceContactData.count > 0
        {
            
            arrOfKeys.add("ServiceFirstName")
            arrOfKeys.add("ServiceMiddleName")
            arrOfKeys.add("ServiceLastName")
            
            arrOfKeys.add("ServicePrimaryEmail")
            arrOfKeys.add("ServiceSecondaryEmail")
            
            arrOfKeys.add("ServicePrimaryPhone")
            arrOfKeys.add("ServiceSecondaryPhone")
            
            arrOfValues.add("\(dictServiceContactData.value(forKey: "FirstName") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "MiddleName") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "LastName") ?? "")")
            
            arrOfValues.add("\(dictServiceContactData.value(forKey: "PrimaryEmail") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "SecondaryEmail") ?? "")")
            
            arrOfValues.add("\(dictServiceContactData.value(forKey: "PrimaryPhone") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "SecondaryPhone") ?? "")")
            
        }
        
        //Billing POC
        
        if dictBillingContactData.count > 0
        {
            
            arrOfKeys.add("BillingFirstName")
            arrOfKeys.add("BillingMiddleName")
            arrOfKeys.add("BillingLastName")
            
            arrOfKeys.add("BillingPrimaryEmail")
            arrOfKeys.add("BillingSecondaryEmail")
            
            arrOfKeys.add("BillingPrimaryPhone")
            arrOfKeys.add("BillingSecondaryPhone")
            
            arrOfValues.add("\(dictBillingContactData.value(forKey: "FirstName") ?? "")")
            arrOfValues.add("\(dictBillingContactData.value(forKey: "MiddleName") ?? "")")
            arrOfValues.add("\(dictBillingContactData.value(forKey: "LastName") ?? "")")
            
            
            arrOfValues.add("\(dictServiceContactData.value(forKey: "PrimaryEmail") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "SecondaryEmail") ?? "")")
            
            arrOfValues.add("\(dictServiceContactData.value(forKey: "PrimaryPhone") ?? "")")
            arrOfValues.add("\(dictServiceContactData.value(forKey: "SecondaryPhone") ?? "")")
            
        }
        
        let dictToSend = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
        
        print(dictToSend)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            if yesEditedSomething
            {
                
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func trimTextField()
    {
        txtFldPrimaryEmailCustInfo.text = txtFldPrimaryEmailCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtFldSecondaryEmailCustInfo.text = txtFldSecondaryEmailCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtFldPrimaryPhoneCustInfo.text = txtFldPrimaryPhoneCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtFldSecondaryPhoneCustInfo.text = txtFldSecondaryPhoneCustInfo.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
    }
    func tempDict()
    {
        arrData = NSMutableArray()
        
        let objValue = ["Jon","1"]
        let objKey = ["Name","IsActive"]
        
        let dict_ToSend = NSDictionary(objects:objValue, forKeys:objKey as [NSCopying]) as Dictionary
        
        let objValue1 = ["Rock","1"]
        let objKey1 = ["Name","IsActive"]
        
        let dict_ToSend1 = NSDictionary(objects:objValue1, forKeys:objKey1 as [NSCopying]) as Dictionary
        
        let objValue2 = ["Bravo","1"]
        let objKey2 = ["Name","IsActive"]
        
        let dict_ToSend2 = NSDictionary(objects:objValue2, forKeys:objKey2 as [NSCopying]) as Dictionary
        
        arrData.add(dict_ToSend)
        arrData.add(dict_ToSend1)
        arrData.add(dict_ToSend2)
        
        
    }
    func setBorderAtLoad()
    {
        buttonRound(sender: btnSave)
        buttonRound(sender: btnUploadEditImageServiceContact)
        
        showHideCustomerInfo()
        showHideWorkOrderInfo()
        showHideBillingContactAddress()
        showHideServiceContactAddress()
        showHideUploadImage()
        showHideOther()
        showHideTechnicianComment()
        showHideTimeRange()
        
        /* setColorBorderForView(item: txtFldPrimaryEmailCustInfo)
         setColorBorderForView(item: txtFldSecondaryEmailCustInfo)
         setColorBorderForView(item: txtFldPrimaryPhoneCustInfo)
         setColorBorderForView(item: txtFldSecondaryPhoneCustInfo)
         setColorBorderForView(item: txtFldCellNoCustInfo)*/
        
        
        setColorBorderForView(item: txtViewAccountAlert)
        setColorBorderForView(item: txtViewServiceDescription)
        setColorBorderForView(item: txtViewServiceInstruction)
        setColorBorderForView(item: txtViewTarget)
        setColorBorderForView(item: txtViewAttribute)
        setColorBorderForView(item: txtViewSetupInstruction)
        setColorBorderForView(item: txtViewTechnicianComment)
        setColorBorderForView(item: txtViewResetReason)
        
        setColorBorderForView(item: lblStartTimeValue)
        setColorBorderForView(item: btnStartTime)
        
        //setColorBorderForView(item: btnSelectBillingContactAddress)
        
        //  setColorBorderForView(item: btnSelectServiceContactAddress)
        
        setColorBorderForView(item: btnResetReason)
        setColorBorderForView(item: btnReset)
        
        // setColorBorderForView(item: btnServiceStatus)
        //setColorBorderForView(item: btnSave)
        
        
        
        
        /* setColorBorderForView(item: btnPrimaryEmailCustInfo)
         setColorBorderForView(item: btnSecondaryEmailCustInfo)
         setColorBorderForView(item: btnPrimaryPhoneCustInfo)
         setColorBorderForView(item: btnSecondaryPhoneCustInfo)
         setColorBorderForView(item: btnCellNoCustInfo)*/
        
        txtFldCellNoCustInfo.setLeftPaddingPoints(5)
        txtFldPrimaryEmailCustInfo.setLeftPaddingPoints(5)
        txtFldSecondaryEmailCustInfo.setLeftPaddingPoints(5)
        txtFldPrimaryPhoneCustInfo.setLeftPaddingPoints(5)
        txtFldSecondaryPhoneCustInfo.setLeftPaddingPoints(5)
        
        
        
        
    }
    func setColorBorder(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 123.0 / 255, green: 166.0 / 255, blue: 208.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strLeadIdGlobal as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    func showHideCustomerInfo()
    {
        /* if btnShowCustomerInfo.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
         {
         btnShowCustomerInfo.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
         const_ViewCustomerInfo_H.constant = 0 + 222 //495
         viewCustomerInfo.isHidden = false
         
         }
         else
         {
         btnShowCustomerInfo.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
         const_ViewCustomerInfo_H.constant = 222  //495
         viewCustomerInfo.isHidden = false
         }*/
        /* if (btnEditCustomerInfo.currentImage == UIImage(named: "edit_ipad")!)
         {
         const_ViewEditContactDetail_CuastomerInfo_H.constant = 0
         const_ViewCustomerInfo_H.constant = 495 - 260
         }
         else
         {
         const_ViewEditContactDetail_CuastomerInfo_H.constant = 260
         const_ViewCustomerInfo_H.constant = 495
         }*/
        
    }
    func showHideWorkOrderInfo()
    {
        if btnShowWorkOrderInfo.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowWorkOrderInfo.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            const_ViewWorkOrderInfo_H.constant = 0 + 441
            viewWorkOrderInfo.isHidden = false
            
        }
        else
        {
            btnShowWorkOrderInfo.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            const_ViewWorkOrderInfo_H.constant = 441
            viewWorkOrderInfo.isHidden = false
            
        }
        
    }
    func showHideBillingContactAddress()
    {
        if btnShowBillingContactAddress.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowBillingContactAddress.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 0 + 190
            viewBillingContactAddress.isHidden = false
            
        }
        else
        {
            btnShowBillingContactAddress.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 190
            
            viewBillingContactAddress.isHidden = false
            
        }
        
    }
    func showHideServiceContactAddress()
    {
        if btnShowServiceContactAddress.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowServiceContactAddress.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 0 + 190
            viewServiceContactAddress.isHidden = false
            
            
        }
        else
        {
            btnShowServiceContactAddress.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 190
            viewServiceContactAddress.isHidden = false
            
            
        }
        
    }
    func showHideUploadImage()
    {
        if btnShowUploadImage.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowUploadImage.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            constViewUploadImage_H.constant = 0
            viewUploadImage.isHidden = false
            
        }
        else
        {
            btnShowUploadImage.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            constViewUploadImage_H.constant = 100 - 100
            viewUploadImage.isHidden = false
            
        }
    }
    func showHideOther()
    {
        if btnShowViewOther.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowViewOther.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            const_ViewOther_H.constant = 0
            viewOther.isHidden = true
            
        }
        else
        {
            btnShowViewOther.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            const_ViewOther_H.constant = 965
            viewOther.isHidden = false
            
        }
    }
    func showHideTechnicianComment()
    {
        if btnShowViewTechnicianComment.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowViewTechnicianComment.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            constViewTechnicianComment_H.constant = 0 + 170
            // showHideResetReason()
            viewTechnicianComment.isHidden = false
            viewResetReason.isHidden = true
            
        }
        else
        {
            btnShowViewTechnicianComment.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            
            constViewTechnicianComment_H.constant = 170
            //  showHideResetReason()
            viewTechnicianComment.isHidden = false
            
        }
    }
    func showHideResetReason()
    {
        let strServicStatus = "\(strServicesStatus)"
        
        if strServicStatus == "Reset"
        {
            constViewResetReason.constant = 260
            viewResetReason.isHidden = false
            constViewTechnicianComment_H.constant = 530
        }
        else
        {
            constViewResetReason.constant = 0
            viewResetReason.isHidden = true
            constViewTechnicianComment_H.constant = 530 - 260
        }
        
    }
    func showHideTimeRange()
    {
        if btnShowTimeRange.currentImage == UIImage(named: "close_arrow_ipad") //show_icon
        {
            btnShowTimeRange.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            const_ViewTimeRange_H.constant = 0 + 80
            //viewTimeRange.isHidden = true
            viewTimeRange.isHidden = false
            
        }
        else
        {
            btnShowTimeRange.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            const_ViewTimeRange_H.constant = 80
            viewTimeRange.isHidden = false
            
        }
    }
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        //vc.arySelectedSource = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            //vc.handleSelection = self
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func isValidEmail(checkString : String) -> Bool
    {
        /* var stricterFilter = Bool()
         stricterFilter = false
         let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$" as String
         
         let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$" as String
         
         let emailRegex = stricterFilter ? stricterFilterString : laxString
         
         var emailTest = NSPredicate()
         
         emailTest = NSPredicate(format: "SELF MATCHES = %@", emailRegex)
         
         return emailTest.evaluate(with: checkString)*/
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: checkString)
        
        
    }
    func alertForPrimaryEmailSaving()
    {
        
    }
    /*
     
     -(void)alertForPrimaryEmailSaving{
     
     [viewBackGround removeFromSuperview];
     [tblData removeFromSuperview];
     
     NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
     
     UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Email-Id"
     message: strMessage
     preferredStyle:UIAlertControllerStyleAlert];
     [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
     textField.placeholder = @"Enter Email-Id Here...";
     textField.tag=7;
     textField.delegate=self;
     textField.textColor = [UIColor blackColor];
     textField.clearButtonMode = UITextFieldViewModeWhileEditing;
     textField.borderStyle = UITextBorderStyleRoundedRect;
     textField.keyboardType=UIKeyboardTypeDefault;
     }];
     //    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
     //        textField.placeholder = @"password";
     //        textField.textColor = [UIColor blueColor];
     //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
     //        textField.borderStyle = UITextBorderStyleRoundedRect;
     //        textField.secureTextEntry = YES;
     //    }];
     [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     NSArray * textfields = alertController.textFields;
     UITextField * txtHistoricalDays = textfields[0];
     if (txtHistoricalDays.text.length>0) {
     
     txtHistoricalDays.text =  [txtHistoricalDays.text stringByTrimmingCharactersInSet:
     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
     
     NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
     NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
     
     if (![emailPredicate evaluateWithObject:txtHistoricalDays.text])
     {
     
     [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }else if ([txtHistoricalDays.text containsString:@" "])
     {
     
     [self performSelector:@selector(AlertViewForNonValidPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }
     else{
     
     if ([arrOfPrimaryEmailNew containsObject:txtHistoricalDays.text]) {
     
     [self performSelector:@selector(AlertViewForAlreadyExitPrimaryEmail) withObject:nil afterDelay:0.2];
     
     } else {
     
     [_btn_NoEmail setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
     isNoEmail=NO;
     [arrOfPrimaryEmailNew addObject:txtHistoricalDays.text];
     
     [arrOfPrimaryEmailAlreadySaved addObject:txtHistoricalDays.text];
     
     NSString *strMessage=[arrOfPrimaryEmailNew componentsJoinedByString:@","];
     
     [_btnPrimaryEmail setTitle:strMessage forState:UIControlStateNormal];
     
     [self tablViewForPrimaryEmailIds];
     
     }
     }
     
     } else {
     
     [self performSelector:@selector(AlertViewForEmptyPrimaryEmail) withObject:nil afterDelay:0.2];
     
     }
     }]];
     [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     
     [self tablViewForPrimaryEmailIds];
     
     }]];
     [self presentViewController:alertController animated:YES completion:nil];
     
     }
     */
    
    
    
    
    
    
}

// MARK: --------------------- Tableview Delegate --------------

extension GeneralInfoiPadVC : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int)
    {
        if tag == 43
        {
            yesEditedSomething = true
            dictBillingContactData = dictData
            
            strBillingPocId = "\((dictBillingContactData.value(forKey: "ContactId") ?? ""))"
            
            
            btnSelectBillingContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            
            let strName = "\(dictBillingContactData.value(forKey: "FirstName") ?? "")" + " " + "\(dictBillingContactData.value(forKey: "MiddleName") ?? "")" + " " + "\(dictBillingContactData.value(forKey: "LastName") ?? "")"
            
            btnSelectBillingContactAddress.setTitle(strName, for: .normal)
            
            lblContactNameBillingContact.text = strName
            
            
            //lblPrimaryEmailBillingContact.text = "\(dictBillingContactData.value(forKey: "PrimaryEmail") ?? "") "
            
            
            btnAddressBillingContact.setTitle( global.strCombinedAddressBilling(for: objWorkorderDetail), for: .normal)
            
            btnPrimaryEmailBillingContact.setTitle( "\(dictBillingContactData.value(forKey: "PrimaryEmail") ?? "") ", for: .normal)
            
            btnPrimaryPhoneBillingContact.setTitle("\(dictBillingContactData.value(forKey: "PrimaryPhone") ?? "") ", for: .normal)
            
            
            //  lblSecondaryEmailBillingContact.text = "\(dictBillingContactData.value(forKey: "SecondaryEmail") ?? "") "
            //  btnSecondaryEmailBillingContact.setTitle( "\(dictBillingContactData.value(forKey: "SecondaryEmail") ?? "") ", for: .normal)
            
            
            
            
            //lblPrimaryPhoneBillingContact.text = "\(dictBillingContactData.value(forKey: "PrimaryPhone") ?? "") "
            
            
            //  lblSecondaryPhoneBillingContact.text = "\(dictBillingContactData.value(forKey: "SecondaryPhone") ?? "") "
            //  btnSecondaryPhoneBillingContact.setTitle("\(dictBillingContactData.value(forKey: "SecondaryPhone") ?? "") ", for: .normal)
            
            
            //btnAddressBillingContact.setTitle(global.strCombinedAddressBilling(dictBillingContactData), for: .normal)
            //
            
            const_BtnBillingContact_H.constant = 0
            btnEditBillingContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewBillingContactAddress_H.constant = 0 + 190
            
            
            
        }
        else if tag == 40
        {
            yesEditedSomething = true
            dictServiceContactData = dictData
            
            strServicePocId = "\((dictServiceContactData.value(forKey: "ContactId") ?? ""))"
            
            //btnSelectServiceContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            
            btnSelectServiceContactAddress.setTitle(dictData["Name"] as? String, for: .normal)
            
            let strName = "\(dictServiceContactData.value(forKey: "FirstName") ?? "") " + " " + "\(dictServiceContactData.value(forKey: "MiddleName") ?? "") " + " " +  "\(dictServiceContactData.value(forKey: "LastName") ?? "")"
            
            btnSelectServiceContactAddress.setTitle(strName, for: .normal)
            
            lblContactNameServiceContact.text = strName
            
            btnAddressServiceContact.setTitle( global.strCombinedAddressService(for: objWorkorderDetail), for: .normal)
            
            btnPrimaryEmailBillingContact.setTitle( "\(dictServiceContactData.value(forKey: "PrimaryEmail") ?? "") ", for: .normal)
            
            btnPrimaryPhoneServiceContact.setTitle("\(dictServiceContactData.value(forKey: "PrimaryPhone") ?? "") ", for: .normal)
            //btnPrimaryPhoneServiceContact.setTitle(lblPrimaryPhoneServiceContact.text, for: .normal)
            
            //btnPrimaryEmailBillingContact.setTitle(lblPrimaryPhoneServiceContact.text, for: .normal)
            
            
            // lblPrimaryEmailServiceContact.text = "\(dictBillingContactData.value(forKey: "PrimaryEmail") ?? "") "
            //  lblSecondaryEmailServiceContact.text = "\(dictServiceContactData.value(forKey: "SecondaryEmail") ?? "") "
            //  lblPrimaryPhoneServiceContact.text = "\(dictServiceContactData.value(forKey: "PrimaryPhone") ?? "") "
            //   lblSecondaryPhoneServiceContact.text = "\(dictServiceContactData.value(forKey: "SecondaryPhone") ?? "") "
            
            
            //btnSecondaryPhoneServiceContact.setTitle(lblSecondaryPhoneServiceContact.text, for: .normal)
            
            //btnAddressServiceContact.setTitle(global.strCombinedAddressBilling(dictServiceContactData), for: .normal)
            constBtnServiceContact_H.constant = 0
            btnEditServiceContact.setImage(UIImage(named: "edit_ipad"), for: .normal)
            constViewServiceContactAddress_H.constant = 0 + 190
            
        }
        else if tag == 41
        {
            yesEditedSomething = true
            btnResetReason.setTitle(dictData["ResetReason"] as? String, for: .normal)
            strResetId = "\(dictData.value(forKey: "ResetId") ?? "")"
            
            isResetReason = true
            
        }
        else if tag == 42
        {
            // btnServiceStatus.setTitle(dictData["Name"] as? String, for: .normal)
            // btnResetReason.setTitle(dictData["Name"] as? String, for: .normal)
        }
    }
    
    
    
}

// MARK: --------------------- DatePicker Delegate --------------

extension GeneralInfoiPadVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if tag == 10
        {
            
            lblStartTimeValue.setTitle(strDate, for: .normal)
            //strFromTimeNonBillable = strDate
            yesEditedSomething = true
            
        }
    }
    
    
}

// MARK: --------------------- ImageView Delegate --------------


extension GeneralInfoiPadVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            imgViewServiceContactAddress.contentMode = .scaleAspectFit
            imgViewServiceContactAddress.image = pickedImage
            //strServiceAddImageName = "\\Documents\\UploadImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg"
            
            strServiceAddImageName = "\\Documents\\CustomerAddressImages\\Img"  + "ServiceAddImg" + "\(getUniqueValueForId())" + ".jpg" // UploadImages
            
            saveImageDocumentDirectory(strFileName: strServiceAddImageName, image: pickedImage)
            yesEditedSomething = true
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: --------------------- Text View Delegate --------------

extension GeneralInfoiPadVC:UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        yesEditedSomething = true
        return true
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        yesEditedSomething = true

    }
}
// MARK: --------------------- Text Field Delegate --------------

extension GeneralInfoiPadVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if ( textField == txtFldPrimaryEmailCustInfo || textField == txtFldSecondaryEmailCustInfo )
        {
            yesEditedSomething = true
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 200)
            
        }
            
        else if ( textField == txtFldPrimaryPhoneCustInfo || textField == txtFldSecondaryPhoneCustInfo || textField == txtFldCellNoCustInfo )
        {
            yesEditedSomething = true
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
        }
        else
        {
            
            return true
            
        }
    }
    
}

