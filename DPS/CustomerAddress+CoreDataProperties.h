//
//  CustomerAddress+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 28/11/17.
//
//

#import "CustomerAddress+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CustomerAddress (CoreDataProperties)

+ (NSFetchRequest<CustomerAddress *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *accountNo;
@property (nullable, nonatomic, retain) NSObject *customerAddress;//addressId
@property (nullable, nonatomic, retain) NSObject *addressId;

@end

NS_ASSUME_NONNULL_END
