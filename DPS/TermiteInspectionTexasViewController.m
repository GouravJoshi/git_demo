//
//  TermiteInspectionViewController.m
//  DPS
//
//  Created by Saavan Patidar on 08/11/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "TermiteInspectionTexasViewController.h"
#import "AllImportsViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface TermiteInspectionTexasViewController ()<CLLocationManagerDelegate>
{
    Global *global;
    NSString *strGlobalBuyersSign,*strGlobalInspectorSign,*strGlobalApplicatorSign,*strGlobalBuyersSignPurchaser,*strGlobalGraphImage,*strGlobalBuyersSign7B,*strEmpID,*strCompanyKey,*strCompanyId,*strUserName,*strCreatedBy,*strDateInspectiondate,*strDate,*strDateTreatmentByInspectionCompany,*strDatePosted,*strDateOfPurchaserOfProperty,*strGraphImageCaption,*strGraphImageDescription,*strGlobalWorkOrderStatus;
    NSMutableDictionary *dictGlobalTermiteTexas;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    BOOL yesEditedSomething;
    
    //Nilind
    //Nilind 07 Jun ImageCaption
    UITextView *txtFieldCaption;
    UITextView *txtViewImageDescription;
    UIView *viewBackAlertt;
    NSMutableArray *arrOfImageCaptionFor7A,*arrOfImageDescriptionFor7A,*arrOfImageCaptionFor8,*arrOfImageDescriptionFor8;
    NSMutableArray *arrNoImage,*arrImageLattitudeFor7A,*arrImageLongitudeFor7A;
    NSMutableArray *arrNoImageCollectionView2,*arrImageLattitudeFor8,*arrImageLongitudeFor8;
}

@end

@implementation TermiteInspectionTexasViewController

- (void)viewDidLoad {
    
    yesEditedSomething=NO;
    
    strGlobalWorkOrderStatus=_strWorkOrderStatus;
    global=[[Global alloc]init];
    [self getClockStatus];
    
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    [pickerDate setMinimumDate:[NSDate date]];
    
    //============================================================================
#pragma mark- collection view change
    arrNoImage=[[NSMutableArray alloc]init];
    arrNoImageCollectionView2=[[NSMutableArray alloc]init];
    
    
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    [super viewDidLoad];
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    _lblAccountNo.text=[defsLead valueForKey:@"lblName"];
    
    NSString *strThirdPartyAccountNo=[defsLead valueForKey:@"lblThirdPartyAccountNo"];
    if (strThirdPartyAccountNo.length>0) {
        _lblAccountNo.text=strThirdPartyAccountNo;
    }
    
    [self methodSetAllViews];
    
    // Do any additional setup after loading the view.
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        
    }
    
    [self.navigationController.navigationBar setHidden:YES];
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strEmpID          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strCreatedBy          =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"CreatedBy"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strCompanyId      =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.SalesProcessCompanyId"]];
    
    [self getDataFrmDB];
    
    [self fetchImageDetailFromDataBase];
    
    //Nilind 06 Dec
    
    arrNoImage=[[NSMutableArray alloc]init];
    arrNoImageCollectionView2=[[NSMutableArray alloc]init];
    arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
    arrOfImageDescriptionFor7A=[[NSMutableArray alloc]init];
    arrOfImageCaptionFor8=[[NSMutableArray alloc]init];
    arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
    //arrImagePath=[[NSMutableArray alloc]init];
    arrImageLattitudeFor7A=[[NSMutableArray alloc]init];
    arrImageLongitudeFor7A=[[NSMutableArray alloc]init];
    arrImageLattitudeFor8=[[NSMutableArray alloc]init];
    arrImageLongitudeFor8=[[NSMutableArray alloc]init];
    [self fetchImageDetailFromDataBaseForSectionView7A_8];
    
    //End
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defsClock=[NSUserDefaults standardUserDefaults];
    
    if([defsClock boolForKey:@"fromClockInOut"]==YES)
    {
        [self getClockStatus];
        [defsClock setBool:NO forKey:@"fromClockInOut"];
        [defsClock synchronize];
    }
    NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
    
    BOOL isForEditImage=[defsBack boolForKey:@"yesEditImage"];
    
    if (isForEditImage)
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"yesEditImage"];
        [defs synchronize];
        /*  UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
         EditImageViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditImageViewController"];
         [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
        
        
        //Akshay 22 may 2019
        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
        [def setBool:YES forKey:@"isFromTermiteTexas"];
        [def synchronize];
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        DrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
        [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
        ////
        
    }
    else
    {
        
        NSUserDefaults *defsBack=[NSUserDefaults standardUserDefaults];
        if ([defsBack boolForKey:@"fromInvoiceAppointment"]==YES)
        {
            arrNoImage=[[NSMutableArray alloc]init];
            arrNoImageCollectionView2=[[NSMutableArray alloc]init];
            arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
            arrOfImageDescriptionFor7A=[[NSMutableArray alloc]init];
            arrOfImageCaptionFor8=[[NSMutableArray alloc]init];
            arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
            //arrImagePath=[[NSMutableArray alloc]init];
            arrImageLattitudeFor7A=[[NSMutableArray alloc]init];
            arrImageLongitudeFor7A=[[NSMutableArray alloc]init];
            arrImageLattitudeFor8=[[NSMutableArray alloc]init];
            arrImageLongitudeFor8=[[NSMutableArray alloc]init];
            
            [self fetchImageDetailFromDataBaseForSectionView7A_8];
            [defsBack setBool:NO forKey:@"fromInvoiceAppointment"];
            [defsBack synchronize];
        }
        
        NSString *strBuyersInitials,*strInspSign,*strApplicatorSign,*strBuyersInitialsPurchaser,*strAddGraphImg,*strBuyersInitials7B;
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        strBuyersInitials=[defs valueForKey:@"strBuyersInitials"];
        strBuyersInitials7B=[defs valueForKey:@"strBuyersInitials7B"];
        strInspSign=[defs valueForKey:@"strInspSign"];
        strApplicatorSign=[defs valueForKey:@"strApplicatorSign"];
        strBuyersInitialsPurchaser=[defs valueForKey:@"strBuyersInitialsPurchaser"];
        strAddGraphImg=[defs valueForKey:@"strAddGraphImg"];
        
        
        if ([strBuyersInitials isEqualToString:@"strBuyersInitials"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePathTermite"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            _imgViewBuyerIntials.image=imageSign;
            strGlobalBuyersSign=strImageName;
            [defs setValue:@"" forKey:@"strBuyersInitials"];
            [defs setValue:@"" forKey:@"imagePathTermite"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalBuyersSign forKey:@"BuyersInitials_SignaturePath_10B"];
        }
        
        if ([strBuyersInitials7B isEqualToString:@"strBuyersInitials7B"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePathTermite"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            _imgView_BuyerInitial_7B.image=imageSign;
            strGlobalBuyersSign7B=strImageName;
            [defs setValue:@"" forKey:@"strBuyersInitials7B"];
            [defs setValue:@"" forKey:@"imagePathTermite"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalBuyersSign7B forKey:@"BuyersInitials_SignaturePath_7B"];
        }
        
        if ([strInspSign isEqualToString:@"strInspSign"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePathTermite"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            strGlobalInspectorSign=strImageName;
            _imgViewInspectorSign.image=imageSign;
            [defs setValue:@"" forKey:@"strInspSign"];
            [defs setValue:@"" forKey:@"imagePathTermite"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalInspectorSign forKey:@"Inspector_SignaturePath"];
        }
        
        if ([strApplicatorSign isEqualToString:@"strApplicatorSign"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePathTermite"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            strGlobalApplicatorSign=strImageName;
            _imgViewCertifiedApplicatorSign.image=imageSign;
            [defs setValue:@"" forKey:@"strApplicatorSign"];
            [defs setValue:@"" forKey:@"imagePathTermite"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalApplicatorSign forKey:@"CertifiedApplicator_SignaturePath"];
        }
        
        if ([strBuyersInitialsPurchaser isEqualToString:@"strBuyersInitialsPurchaser"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"imagePathTermite"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            strGlobalBuyersSignPurchaser=strImageName;
            _imgViewBuyerIntialsPurchaser.image=imageSign;
            [defs setValue:@"" forKey:@"strBuyersInitialsPurchaser"];
            [defs setValue:@"" forKey:@"imagePathTermite"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalBuyersSignPurchaser forKey:@"SignatureOfPurchaserOfProperty_SignaturePath"];
        }
        
        if ([strAddGraphImg isEqualToString:@"strAddGraphImg"])
        {
            NSString *strImageName;
            strImageName=[defs valueForKey:@"graphImagePathTermite"];
            strGraphImageCaption=[defs valueForKey:@"graphImageCaption"];
            strGraphImageDescription=[defs valueForKey:@"graphImageDescription"];
            UIImage *imageSign;
            imageSign=[self loadImage:strImageName];
            strGlobalGraphImage=strImageName;
            _imgViewAddGraph.image=imageSign;
            [_btnAddGraphImg setImage:imageSign forState:UIControlStateNormal];
            [defs setValue:@"" forKey:@"strAddGraphImg"];
            [defs setValue:@"" forKey:@"graphImagePathTermite"];
            [defs setValue:@"" forKey:@"graphImageCaption"];
            [defs setValue:@"" forKey:@"graphImageDescription"];
            [defs synchronize];
            [dictGlobalTermiteTexas setValue:strGlobalGraphImage forKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"];
            [dictGlobalTermiteTexas setValue:strGraphImageCaption forKey:@"Graph_Caption"];
            [dictGlobalTermiteTexas setValue:strGraphImageDescription forKey:@"Graph_Description"];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageCaptionGraph=[defs boolForKey:@"yesEditedImageCaptionGraph"];
        if (yesEditedImageCaptionGraph) {
            NSMutableArray *arrOfImageCaptionGraph;
            arrOfImageCaptionGraph=nil;
            arrOfImageCaptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageCaptionGraph"]];
            [defsnew setObject:nil forKey:@"imageCaptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaptionGraph"];
            [defsnew synchronize];
            strGraphImageCaption=arrOfImageCaptionGraph[0];
            [dictGlobalTermiteTexas setValue:strGraphImageCaption forKey:@"Graph_Caption"];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        //Graph
        //Change For Image Description  yesEditedImageDescription
        BOOL yesEditedImageDescriptionGraph=[defs boolForKey:@"yesEditedImageDescriptionGraph"];
        if (yesEditedImageDescriptionGraph) {
            NSMutableArray *arrOfImageDescriptionGraph;
            arrOfImageDescriptionGraph=nil;
            arrOfImageDescriptionGraph=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionGraph addObjectsFromArray:[defsnew objectForKey:@"imageDescriptionGraph"]];
            [defsnew setObject:nil forKey:@"imageDescriptionGraph"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescriptionGraph"];
            [defsnew synchronize];
            strGraphImageDescription=arrOfImageDescriptionGraph[0];
            [dictGlobalTermiteTexas setValue:strGraphImageDescription forKey:@"Graph_Description"];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        BOOL yesDeletedGraphImage=[defs boolForKey:@"yesDeleteGraphTermite"];
        if (yesDeletedGraphImage) {
            
            [_btnAddGraphImg setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
            strGlobalGraphImage=@"";
            strGraphImageCaption=@"";
            strGraphImageDescription=@"";
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setBool:NO forKey:@"yesDeleteGraphTermite"];
            [defsnew synchronize];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        //For Image Delete 7A
        
        
        NSUserDefaults *defsSS=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImage=[defsSS boolForKey:@"yesDeletedImageInPreview"];
        if (yesFromDeleteImage) {
            
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            [defsSS setBool:NO forKey:@"yesDeletedImageInPreview"];
            [defsSS synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImage.count; k++) {
                
                NSDictionary *dictdat=arrNoImage[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrNoImage[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                
                // NSString *strImageName=[dictdat valueForKey:@"woImagePath"];
                
                NSArray *arrOfImagesLeft=[defsSS objectForKey:@"DeletedImages"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImage[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImage removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImage=nil;
                arrNoImage=[[NSMutableArray alloc]init];
            }
            
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [defsnew setObject:nil forKey:@"DeletedImages"];
            [defsnew synchronize];
            
            
            //Nilind 26 Dec
            //For Caption Delete
            
            NSLog(@"Caption 7A >>%@",arrOfImageCaptionFor7A);
            NSLog(@"Description 7A >>%@",arrOfImageDescriptionFor7A);
            
            NSArray *arrOfImagesLeftCaption=[defsSS objectForKey:@"captionFor7A"];
            arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
            for (int l=0; l<arrOfImagesLeftCaption.count; l++)
            {
                [arrOfImageCaptionFor7A addObject:[arrOfImagesLeftCaption objectAtIndex:l]];
            }
            NSArray *arrOfImagesLeftDescription=[defsSS objectForKey:@"descriptionFor7A"];
            arrOfImageDescriptionFor7A=[[NSMutableArray alloc]init];
            for (int l=0; l<arrOfImagesLeftDescription.count; l++)
            {
                [arrOfImageDescriptionFor7A addObject:[arrOfImagesLeftDescription objectAtIndex:l]];
            }
            //End
            
            
            [self saveImageTermiteToCoreData];
            
            
        }
        //For Image Caption 7A
        
        BOOL yesEditedImageCaption=[defsSS boolForKey:@"yesEditedImageCaption"];
        if (yesEditedImageCaption) {
            
            yesEditedSomething=YES;
            arrOfImageCaptionFor7A=nil;
            arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionFor7A addObjectsFromArray:[defsnew objectForKey:@"imageCaption"]];
            [defsnew setObject:nil forKey:@"imageCaption"];
            [defsnew setBool:NO forKey:@"yesEditedImageCaption"];
            [defsnew synchronize];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        
        //For Image Description 7A
        
        BOOL yesEditedImageDescription=[defsSS boolForKey:@"yesEditedImageDescription"];
        if (yesEditedImageDescription) {
            
            yesEditedSomething=YES;
            arrOfImageDescriptionFor8=nil;
            arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnew=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionFor8 addObjectsFromArray:[defsnew objectForKey:@"imageDescription"]];
            [defsnew setObject:nil forKey:@"imageDescription"];
            [defsnew setBool:NO forKey:@"yesEditedImageDescription"];
            [defsnew synchronize];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        
        //For Image Delete 8
        
        NSUserDefaults *defsSSFor8=[NSUserDefaults standardUserDefaults];
        BOOL yesFromDeleteImageFor8=[defsSSFor8 boolForKey:@"yesDeletedImageInPreviewFor8"];
        if (yesFromDeleteImageFor8) {
            
            yesEditedSomething=YES;
            NSLog(@"Yes Edited Something In Db");
            [defsSSFor8 setBool:NO forKey:@"yesDeletedImageInPreviewFor8"];
            [defsSSFor8 synchronize];
            
            NSMutableArray *arrTempBeforeImage=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrNoImageCollectionView2.count; k++) {
                
                NSDictionary *dictdat=arrNoImageCollectionView2[k];
                
                NSString *strImageName;
                
                if ([dictdat isKindOfClass:[NSString class]]) {
                    
                    strImageName=arrNoImageCollectionView2[k];
                    
                } else {
                    
                    strImageName=[dictdat valueForKey:@"woImagePath"];
                    
                }
                
                // NSString *strImageName=[dictdat valueForKey:@"woImagePath"];
                
                NSArray *arrOfImagesLeft=[defsSSFor8 objectForKey:@"DeletedImagesFor8"];
                BOOL yesFoundName;
                yesFoundName=NO;
                
                for (int j=0; j<arrOfImagesLeft.count; j++) {
                    
                    NSString *strImageNameleftToCompare=arrOfImagesLeft[j];
                    
                    if ([strImageName isEqualToString:strImageNameleftToCompare]) {
                        
                        [arrTempBeforeImage addObject:arrNoImageCollectionView2[k]];
                        
                    }
                }
                
            }
            if (!(arrTempBeforeImage.count==0))
            {
                
                [arrNoImageCollectionView2 removeObjectsInArray:arrTempBeforeImage];
            }
            else if((arrTempBeforeImage.count==0))
            {
                arrNoImageCollectionView2=nil;
                arrNoImageCollectionView2=[[NSMutableArray alloc]init];
            }
            
            NSUserDefaults *defsnewFor8=[NSUserDefaults standardUserDefaults];
            [defsnewFor8 setObject:nil forKey:@"DeletedImagesFor8"];
            [defsnewFor8 synchronize];
            
            //Nilind 26 Dec
            
            //For Caption Delete
            
            NSLog(@"Caption For8 >>%@",arrOfImageCaptionFor8);
            NSLog(@"Description For8 >>%@",arrOfImageDescriptionFor8);
            
            NSArray *arrOfImagesLeftCaption=[defsSS objectForKey:@"captionFor8"];
            arrOfImageCaptionFor8=[[NSMutableArray alloc]init];
            for (int l=0; l<arrOfImagesLeftCaption.count; l++)
            {
                [arrOfImageCaptionFor8 addObject:[arrOfImagesLeftCaption objectAtIndex:l]];
            }
            NSArray *arrOfImagesLeftDescription=[defsSS objectForKey:@"descriptionFor8"];
            arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
            for (int l=0; l<arrOfImagesLeftDescription.count; l++)
            {
                [arrOfImageDescriptionFor8 addObject:[arrOfImagesLeftDescription objectAtIndex:l]];
            }
            //End
            
            [self saveImageTermiteToCoreData];
            
        }
        //For Image Caption 8
        
        BOOL yesEditedImageCaptionFor8=[defsSSFor8 boolForKey:@"yesEditedImageCaptionFor8"];
        if (yesEditedImageCaptionFor8) {
            
            yesEditedSomething=YES;
            arrOfImageCaptionFor7A=nil;
            arrOfImageCaptionFor7A=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnewFor8=[NSUserDefaults standardUserDefaults];
            [arrOfImageCaptionFor7A addObjectsFromArray:[defsnewFor8 objectForKey:@"imageCaptionFor8"]];
            [defsnewFor8 setObject:nil forKey:@"imageCaptionFor8"];
            [defsnewFor8 setBool:NO forKey:@"yesEditedImageCaptionFor8"];
            [defsnewFor8 synchronize];
            
            [self saveImageTermiteToCoreData];
            
        }
        
        
        //For Image Description 8
        BOOL yesEditedImageDescriptionFor8=[defsSSFor8 boolForKey:@"yesEditedImageDescriptionFor8"];
        if (yesEditedImageDescriptionFor8) {
            
            yesEditedSomething=YES;
            arrOfImageDescriptionFor8=nil;
            arrOfImageDescriptionFor8=[[NSMutableArray alloc]init];
            NSUserDefaults *defsnewFor8=[NSUserDefaults standardUserDefaults];
            [arrOfImageDescriptionFor8 addObjectsFromArray:[defsnewFor8 objectForKey:@"imageDescriptionFor8"]];
            [defsnewFor8 setObject:nil forKey:@"imageDescriptionFor8"];
            [defsnewFor8 setBool:NO forKey:@"yesEditedImageDescriptionFor8"];
            [defsnewFor8 synchronize];
            
            [self saveImageTermiteToCoreData];
            
        }
        
    }
    
    [self setEditableOrNot];
    [_collectionView8 reloadData];
    [_collectionView7A reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//============================================================================
#pragma mark- Fetch From DB Values
//============================================================================

-(void)getDataFrmDB{
    
    NSLog(@"Work Order Id===%@",_strWorkOrder);
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        //matchesWorkOrder=nil;
        [self setTexasTermiteFormifBlank];
        [self showValuesSavedInDb];
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"];
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"]
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        
        NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        dictDataTermite=[self nestedDictionaryByReplacingNullsWithNil:dictDataTermite];
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        [dictGlobalTermiteTexas addEntriesFromDictionary:dictDataTermite];
        [self showValuesSavedInDb];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary
{
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    [sourceDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        object = [sourceDictionary objectForKey:key];
        if([object isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *innerDict = object;
            [replaced setObject:[self nestedDictionaryByReplacingNullsWithNil:innerDict] forKey:key];
            
        }
        else if([object isKindOfClass:[NSArray class]]){
            NSMutableArray *nullFreeRecords = [NSMutableArray array];
            for (id record in object) {
                
                if([record isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *nullFreeRecord = [self nestedDictionaryByReplacingNullsWithNil:record];
                    [nullFreeRecords addObject:nullFreeRecord];
                }
            }
            [replaced setObject:nullFreeRecords forKey:key];
        }
        else
        {
            if(object == nul) {
                [replaced setObject:blank forKey:key];
            }
        }
    }];
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(void)setTexasTermiteFormifBlank{
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"TermiteTexasStaticForm.json"];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSDictionary *dictOfTermiteDate = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
    dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
    [dictGlobalTermiteTexas addEntriesFromDictionary:dictOfTermiteDate];
    
    [dictGlobalTermiteTexas setValue:_strWorkOrder forKey:@"WorkorderId"];
    [dictGlobalTermiteTexas setValue:@"" forKey:@"TexasTermiteServiceDetailID"];
    [dictGlobalTermiteTexas setValue:[global strCurrentDate] forKey:@"CreatedDate"];
    [dictGlobalTermiteTexas setValue:strCreatedBy forKey:@"CreatedBy"];
    [dictGlobalTermiteTexas setValue:[global strCurrentDate] forKey:@"ModifiedDate"];
    [dictGlobalTermiteTexas setValue:strCreatedBy forKey:@"ModifiedBy"];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictGlobalTermiteTexas])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictGlobalTermiteTexas options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"Final Service Automation JSON: %@", jsonString);
        }
    }
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    //Equipment Detail Entity
    entityTermiteTexas=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    
    TexasTermiteServiceDetail *objImagesDetail = [[TexasTermiteServiceDetail alloc]initWithEntity:entityTermiteTexas insertIntoManagedObjectContext:context1];
    
    objImagesDetail.companyKey=@"";
    
    objImagesDetail.userName=@"";
    
    objImagesDetail.workorderId=_strWorkOrder;
    
    objImagesDetail.texasTermiteServiceDetail=dictDataTermite;
    
    NSError *error1;
    [context1 save:&error1];
    
    
    [self getDataFrmDB];
}

-(NSString*)changeDateToFormattedDate :(NSString*)strDateBeingConverted{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;
    
}

-(void)showValuesSavedInDb
{
    [self fetchWorkOrderFromDataBase];
    //([[dictData valueForKey:@"ModifiedDate"] isKindOfClass:[NSNull class]]) ? @"" : [dictData valueForKey:@"ModifiedDate"];
    
    _txt_InspectedAddress.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    _txt_City.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"City"]];
    _txt_ZipCode.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ZipCode"]];
    
    //Nilind 11 Dec
    NSString *strInspectedAdd;
    strInspectedAdd=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    if ([strInspectedAdd isEqualToString:@"" ]||strInspectedAdd.length==0||[strInspectedAdd isEqualToString:@"(null)"])
    {
        _txt_InspectedAddress.text=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"servicesAddress1"]];
        [dictGlobalTermiteTexas setValue:_txt_InspectedAddress.text forKey:@"InspectedAddress"];
        
    }
    else
    {
        _txt_InspectedAddress.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    }
    
    
    NSString *strInspectedCity;
    strInspectedCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"City"]];
    if ([strInspectedCity isEqualToString:@"" ]||strInspectedCity.length==0||[strInspectedCity isEqualToString:@"(null)"])
    {
        _txt_City.text=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"serviceCity"]];
        [dictGlobalTermiteTexas setValue:_txt_City.text forKey:@"City"];
        
    }
    else
    {
        _txt_City.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"City"]];
    }
    
    NSString *strInspectedZipcode;
    strInspectedZipcode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ZipCode"]];
    if ([strInspectedZipcode isEqualToString:@"" ]||strInspectedZipcode.length==0||[strInspectedZipcode isEqualToString:@"(null)"])
    {
        _txt_ZipCode.text=[NSString stringWithFormat:@"%@",[matchesWorkOrderNew valueForKey:@"serviceZipcode"]];
        [dictGlobalTermiteTexas setValue:_txt_ZipCode.text forKey:@"ZipCode"];
        
        
    }
    else
    {
        _txt_ZipCode.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ZipCode"]];
        
    }
    //End
    
    //Nilind
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    BOOL isBranchAddress = false;
        
    if ([[dictLoginData valueForKey:@"Branch"] isKindOfClass:[NSDictionary class]]) {
                
        NSString *strBranch = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"]];
        
        if (strBranch.length > 0) {
            
            isBranchAddress = true;
            
        }else{
            
            isBranchAddress = false;
            
        }
        
    } else {
        
        isBranchAddress = false;
        
    }
    
    NSString *strInspectionCompanyName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyName"]];
    if ([strInspectionCompanyName isEqualToString:@"" ]||strInspectionCompanyName.length==0||[strInspectionCompanyName isEqualToString:@"(null)"])
    {
        _txt_1A.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Name"]];
        [dictGlobalTermiteTexas setValue:_txt_1A.text forKey:@"InspectionCompanyName"];
        
        
    }
    else
    {
        _txt_1A.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyName"]];
        
    }
    
    _txt_1B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SPCSBuisnessLicenseNo"]];
    
    
    NSString *strInspectionCompanyAddress=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyAddress"]];
    if ([strInspectionCompanyAddress isEqualToString:@"" ]||strInspectionCompanyAddress.length==0||[strInspectionCompanyAddress isEqualToString:@"(null)"])
    {
        
        if (isBranchAddress) {
            
            NSString *strBranchAddressLine2 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine2"]];
            
            if (strBranchAddressLine2.length > 0) {
                
                _txt_1C.text=[NSString stringWithFormat:@"%@, %@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"],[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine2"]];
                
            } else {
                
                _txt_1C.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchAddressLine1"]];

            }

        } else {
            
            NSString *strCompanyAddressLine2 = [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine2"]];
            
            if (strCompanyAddressLine2.length > 0) {
                
                _txt_1C.text=[NSString stringWithFormat:@"%@, %@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"],[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine2"]];
                
            } else {
                
                _txt_1C.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];

            }
                        
        }
        
        //_txt_1C.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyAddressLine1"]];
        [dictGlobalTermiteTexas setValue:_txt_1C.text forKey:@"InspectionCompanyAddress"];
        
        
    }
    else
    {
        _txt_1C.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyAddress"]];
    }
    
    
    NSString *strInspectionCompanyZipCode=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyZipCode"]];
    if ([strInspectionCompanyZipCode isEqualToString:@"" ]||strInspectionCompanyZipCode.length==0||[strInspectionCompanyZipCode isEqualToString:@"(null)"])
    {
        
        if (isBranchAddress) {
            
            _txt_ZipCodeView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchZipCode"]];

        } else {
            
            _txt_ZipCodeView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];

        }
        
        //_txt_ZipCodeView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.ZipCode"]];
        [dictGlobalTermiteTexas setValue:_txt_ZipCodeView4.text forKey:@"InspectionCompanyZipCode"];
        
        
    }
    else
    {
        _txt_ZipCodeView4.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyZipCode"]];
        
    }
    
    
    NSString *strInspectionCompanyCity=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyCity"]];
    if ([strInspectionCompanyCity isEqualToString:@"" ]||strInspectionCompanyCity.length==0||[strInspectionCompanyCity isEqualToString:@"(null)"])
    {
        
        if (isBranchAddress) {
            
            _txt_CityView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchCity"]];

        } else {
            
            _txt_CityView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
            
        }
        
        //_txt_CityView4.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CityName"]];
        [dictGlobalTermiteTexas setValue:_txt_CityView4.text forKey:@"InspectionCompanyCity"];
        
        
    }
    else
    {
        _txt_CityView4.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyCity"]];
        
    }
    
    
    NSString *strInspectionCompanyState=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyState"]];
    if ([strInspectionCompanyState isEqualToString:@"" ]||strInspectionCompanyState.length==0||[strInspectionCompanyState isEqualToString:@"(null)"])
    {
        if (isBranchAddress) {
            
            _txt_State.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Branch.BranchState"]];

        } else {
            
            _txt_State.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
            
        }
        //_txt_State.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.StateName"]];
        [dictGlobalTermiteTexas setValue:_txt_State.text forKey:@"InspectionCompanyState"];
        
    }
    else
    {
        _txt_State.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionCompanyState"]];
        
    }
    
    
    NSString *strInspectionTelephoneNo=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TelephoneNo"]];
    if ([strInspectionTelephoneNo isEqualToString:@"" ]||strInspectionTelephoneNo.length==0||[strInspectionTelephoneNo isEqualToString:@"(null)"])
    {
        _txt_TelephoneNo.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.PrimaryPhone"]];
        [dictGlobalTermiteTexas setValue:_txt_TelephoneNo.text forKey:@"TelephoneNo"];
        
        
    }
    else
    {
        _txt_TelephoneNo.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TelephoneNo"]];
        
    }
    
    NSString *strInspectionInspectorName=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectorName"]];
    if ([strInspectionInspectorName isEqualToString:@"" ]||strInspectionInspectorName.length==0||[strInspectionInspectorName isEqualToString:@"(null)"])
    {
        _txt_1D.text=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
        [dictGlobalTermiteTexas setValue:_txt_1D.text forKey:@"InspectorName"];
        
        
    }
    else
    {
        _txt_1D.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectorName"]];
        
    }
    
    //ENd
    
    _txt_2.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CaseNumber"]];
    _txt_3.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionDate"]];
    _txt_PersonPurchasingInspection.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonName_PurchasingInspection"]];
    
    strDateInspectiondate=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionDate"]];
    
    if (strDateInspectiondate.length==0) {
        //currentDate
        [_btnInspectionDate setTitle:[self currentDate] forState:UIControlStateNormal];
        
        [dictGlobalTermiteTexas setValue:[self currentDate] forKey:@"InspectionDate"];
        
    } else {
        
        strDateInspectiondate=[self changeDateToFormattedDate:strDateInspectiondate];
        
        [_btnInspectionDate setTitle:strDateInspectiondate forState:UIControlStateNormal];
        
    }
    
    _txt_4A_Other.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonOther_PurchasingInspection"]];
    _txt_4B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Owner_Seller"]];
    _txt_5.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_5"]];
    _txt_6BSpecify.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Specify"]];
    _txt_7BSpecify.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Specify"]];
    _txt_8E.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectionRevealsVisibleEvidence_Specify"]];
    _txt_8F.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ExplanationOfSignsIOfPreviousTreatment"]];
    _txt_8G.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"VisibleEvidenceOf"]];
    _txt_8GD.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"VisibleEvidenceObesrvedAreas"]];
    _txt_9.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"MechanicallyCorrectedByInspectingCompany_Specify"]];
    _txt_9BSpecifyReason.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PreventiveTreatment_Specify"]];
    _txt_9B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"InspectedAddress"]];
    _txt_10A.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforWoodDestroyingInsects"]];
    _txt_DateOfTreatment.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOFTreatment_InspectingCompany"]];
    
    strDateTreatmentByInspectionCompany=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOFTreatment_InspectingCompany"]];
    
    if (strDateTreatmentByInspectionCompany.length==0) {
        
        [_btn_DateTreatmentByInspectionCompany setTitle:[self currentDate] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:[self currentDate] forKey:@"DateOFTreatment_InspectingCompany"];
        
    } else {
        
        strDateTreatmentByInspectionCompany=[self changeDateToFormattedDate:strDateTreatmentByInspectionCompany];
        [_btn_DateTreatmentByInspectionCompany setTitle:strDateTreatmentByInspectionCompany forState:UIControlStateNormal];
        
    }
    
    _txt_CommonNameOfInsect.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CommonNameOfInsect"]];
    _txt_NameOfPesticide.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"NameOfPesticide_Bait_Or_Other"]];
    _txt_ListInsects.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ListInsects"]];
    _txt_AdditionalComments.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"AdditionalComments_TexasOffiicial_WoodDestroyingInsect"]];
    _txt_12B.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ListNoOfPages"]];
    _txt_ApplicatorLicenseNumber.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_LicenseNumber"]];
    _txt_Specifys_TexasOffiicial_WoodDestroyingInsect.text=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Specifys_TexasOffiicial_WoodDestroyingInsect"]];
    
    
    NSString *strbtnChkBoxA=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_A"]];
    if([strbtnChkBoxA isEqualToString:@"true"] || [strbtnChkBoxA isEqualToString:@"1"]){
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxB=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_B"]];
    if([strbtnChkBoxB isEqualToString:@"true"] || [strbtnChkBoxB isEqualToString:@"1"]){
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxC=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_B"]];
    if([strbtnChkBoxC isEqualToString:@"true"] || [strbtnChkBoxC isEqualToString:@"1"]){
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxD=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_D"]];
    if([strbtnChkBoxD isEqualToString:@"true"] || [strbtnChkBoxD isEqualToString:@"1"]){
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxE=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_E"]];
    if([strbtnChkBoxE isEqualToString:@"true"] || [strbtnChkBoxE isEqualToString:@"1"]){
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxF=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_F"]];
    if([strbtnChkBoxF isEqualToString:@"true"] || [strbtnChkBoxF isEqualToString:@"1"]){
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxG=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_G"]];
    if([strbtnChkBoxG isEqualToString:@"true"] || [strbtnChkBoxG isEqualToString:@"1"]){
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxH=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_H"]];
    if([strbtnChkBoxH isEqualToString:@"true"] || [strbtnChkBoxH isEqualToString:@"1"]){
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxI=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_I"]];
    if([strbtnChkBoxI isEqualToString:@"true"] || [strbtnChkBoxI isEqualToString:@"1"]){
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBoxJ=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ScopeOfInspection_J"]];
    if([strbtnChkBoxJ isEqualToString:@"true"] || [strbtnChkBoxJ isEqualToString:@"1"]){
        
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_1E=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_Technician"]];
    if([strbtnRadio_1_1E isEqualToString:@"Certified Applicator"]){
        
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_1E isEqualToString:@"Technician"]){
        
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    NSString *strPersonType_PurchasingInspection=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"PersonType_PurchasingInspection"]];
    if([strPersonType_PurchasingInspection isEqualToString:@"Seller"]){
        
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Agent"]){
        
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Buyer"]){
        
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Management Co."]){
        
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strPersonType_PurchasingInspection isEqualToString:@"Other"]){
        
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    
    NSString *strbtnChkBox_1_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"]];
    if([strbtnChkBox_1_4C isEqualToString:@"true"] || [strbtnChkBox_1_4C isEqualToString:@"1"]){
        
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_2_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_PurchaseService"]];
    if([strbtnChkBox_2_4C isEqualToString:@"true"] || [strbtnChkBox_2_4C isEqualToString:@"1"]){
        
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_3_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Seller"]];
    if([strbtnChkBox_3_4C isEqualToString:@"true"] || [strbtnChkBox_3_4C isEqualToString:@"1"]){
        
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_4_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Agent"]];
    if([strbtnChkBox_4_4C isEqualToString:@"true"] || [strbtnChkBox_4_4C isEqualToString:@"1"]){
        
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    NSString *strbtnChkBox_5_4C=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ReportForwardedTo_Buyer"]];
    if([strbtnChkBox_5_4C isEqualToString:@"true"] || [strbtnChkBox_5_4C isEqualToString:@"1"]){
        
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_6A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsPropertyObstrected_Or_Inaccessible"]];
    if([strbtnRadio_1_6A isEqualToString:@"true"] || [strbtnRadio_1_6A isEqualToString:@"1"]){
        
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_6A isEqualToString:@"false"] || [strbtnRadio_1_6A isEqualToString:@"0"]){
        
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    //Obstrected_Or_Inaccessible_SlightCornerCracks
    
    NSString *strbtnChkBox_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SlightCornerCracks"]];
    if([strbtnChkBox_6B isEqualToString:@"true"] || [strbtnChkBox_6B isEqualToString:@"1"]){
        
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_1_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Attic"]];
    if([strbtnChkBox_1_6B isEqualToString:@"true"] || [strbtnChkBox_1_6B isEqualToString:@"1"]){
        
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_InsulatedAreaOfAttic"]];
    if([strbtnChkBox_2_6B isEqualToString:@"true"] || [strbtnChkBox_2_6B isEqualToString:@"1"]){
        
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_PlumbingAreas"]];
    if([strbtnChkBox_3_6B isEqualToString:@"true"] || [strbtnChkBox_3_6B isEqualToString:@"1"]){
        
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_PlanterBoxAbuttingStructure"]];
    if([strbtnChkBox_4_6B isEqualToString:@"true"] || [strbtnChkBox_4_6B isEqualToString:@"1"]){
        
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_5_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Deck"]];
    if([strbtnChkBox_5_6B isEqualToString:@"true"] || [strbtnChkBox_5_6B isEqualToString:@"1"]){
        
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_6_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SubFloors"]];
    if([strbtnChkBox_6_6B isEqualToString:@"true"] || [strbtnChkBox_6_6B isEqualToString:@"1"]){
        
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_7_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SlabJoints"]];
    if([strbtnChkBox_7_6B isEqualToString:@"true"] || [strbtnChkBox_7_6B isEqualToString:@"1"]){
        
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_8_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_CrawlSpace"]];
    if([strbtnChkBox_8_6B isEqualToString:@"true"] || [strbtnChkBox_8_6B isEqualToString:@"1"]){
        
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_9_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_SoilGradeTooHigh"]];
    if([strbtnChkBox_9_6B isEqualToString:@"true"] || [strbtnChkBox_9_6B isEqualToString:@"1"]){
        
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_10_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_HeavyFollage"]];
    if([strbtnChkBox_10_6B isEqualToString:@"true"] || [strbtnChkBox_10_6B isEqualToString:@"1"]){
        
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_11_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Eaves"]];
    if([strbtnChkBox_11_6B isEqualToString:@"true"] || [strbtnChkBox_11_6B isEqualToString:@"1"]){
        
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_12_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Weepholes"]];
    if([strbtnChkBox_12_6B isEqualToString:@"true"] || [strbtnChkBox_12_6B isEqualToString:@"1"]){
        
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_13_6B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Obstrected_Or_Inaccessible_Other"]];
    if([strbtnChkBox_13_6B isEqualToString:@"true"] || [strbtnChkBox_13_6B isEqualToString:@"1"]){
        
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_7A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_7A isEqualToString:@"true"] || [strbtnRadio_1_7A isEqualToString:@"1"]){
        
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_7A isEqualToString:@"false"] || [strbtnRadio_1_7A isEqualToString:@"0"]){
        
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_SlightCornerCracks"]];
    if([strbtnChkBox_1_7B isEqualToString:@"true"] || [strbtnChkBox_1_7B isEqualToString:@"1"]){
        
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodtoGroundContact"]];
    if([strbtnChkBox_2_7B isEqualToString:@"true"] || [strbtnChkBox_2_7B isEqualToString:@"1"]){
        
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_FormBoardsLeftInPlace"]];
    if([strbtnChkBox_3_7B isEqualToString:@"true"] || [strbtnChkBox_3_7B isEqualToString:@"1"]){
        
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_ExcessiveMoisture"]];
    if([strbtnChkBox_4_7B isEqualToString:@"true"] || [strbtnChkBox_4_7B isEqualToString:@"1"]){
        
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_5_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_DebrisunderoraroundStructure"]];
    if([strbtnChkBox_5_7B isEqualToString:@"true"] || [strbtnChkBox_5_7B isEqualToString:@"1"]){
        
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_6_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_FootingTooLowOrSoilLineTooHigh"]];
    if([strbtnChkBox_6_7B isEqualToString:@"true"] || [strbtnChkBox_6_7B isEqualToString:@"1"]){
        
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_7_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodRot"]];
    if([strbtnChkBox_7_7B isEqualToString:@"true"] || [strbtnChkBox_7_7B isEqualToString:@"1"]){
        
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_8_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_HeavyFollage"]];
    if([strbtnChkBox_8_7B isEqualToString:@"true"] || [strbtnChkBox_8_7B isEqualToString:@"1"]){
        
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_9_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Plonterboxabuttingstructure"]];
    if([strbtnChkBox_9_7B isEqualToString:@"true"] || [strbtnChkBox_9_7B isEqualToString:@"1"]){
        
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_10_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodPileInContactwithStructure"]];
    if([strbtnChkBox_10_7B isEqualToString:@"true"] || [strbtnChkBox_10_7B isEqualToString:@"1"]){
        
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_11_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_WoodFenceinContactwithStrucuture"]];//ConduciveConditions_WoodFenceinContactwithStrucuture
    if([strbtnChkBox_11_7B isEqualToString:@"true"] || [strbtnChkBox_11_7B isEqualToString:@"1"]){
        
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_12_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_InsufficientVentillation"]];
    if([strbtnChkBox_12_7B isEqualToString:@"true"] || [strbtnChkBox_12_7B isEqualToString:@"1"]){
        
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_13_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ConduciveConditions_Other"]];
    if([strbtnChkBox_13_7B isEqualToString:@"true"] || [strbtnChkBox_13_7B isEqualToString:@"1"]){
        
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    //8
    
    NSString *strbtnRadio_1_8_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsConduciveConditions"]];
    if([strbtnRadio_1_8_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8A_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8A_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8A_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8A_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SubterraneanTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8A_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8B_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8B_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8B_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8B_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8B_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DrywoodTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8B_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8C_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_ActiveInfestation"]];
    if([strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8C_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8C_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_PreviousInfestation"]];
    if([strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8C_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8C_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"FormosanTermites_PreviousTreatment"]];
    if([strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8C_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8D_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_ActiveInfestation"]];
    if([strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8D_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8D_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_PreviousInfestation"]];
    if([strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8D_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8D_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CarpenterAnts_PreviousTreatment"]];
    if([strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8D_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_8E_ActiveInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_ActiveInfestation"]];
    if([strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"true"] || [strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"false"] || [strbtnRadio_1_8E_ActiveInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8E_PreviousInfestation=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_PreviousInfestation"]];
    if([strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"true"] || [strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"false"] || [strbtnRadio_1_8E_PreviousInfestation isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_8E_PreviousTreatment=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"OtherWoodDestroyingInsects_PreviousTreatment"]];
    if([strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"true"] || [strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"1"]){
        
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"false"] || [strbtnRadio_1_8E_PreviousTreatment isEqualToString:@"0"]){
        
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_9=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"MechanicallyCorrectedByInspectingCompany"]];
    if([strbtnRadio_1_9 isEqualToString:@"true"] || [strbtnRadio_1_9 isEqualToString:@"1"]){
        
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9 isEqualToString:@"false"] || [strbtnRadio_1_9 isEqualToString:@"0"]){
        
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnRadio_1_9A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CorrectiveTreatment"]];
    if([strbtnRadio_1_9A isEqualToString:@"true"] || [strbtnRadio_1_9A isEqualToString:@"1"]){
        
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9A isEqualToString:@"false"] || [strbtnRadio_1_9A isEqualToString:@"0"]){
        
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_9B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"IsPreventiveTreatment"]];//IsPreventiveTreatment
    if([strbtnRadio_1_9B isEqualToString:@"true"] || [strbtnRadio_1_9B isEqualToString:@"1"]){
        
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_9B isEqualToString:@"false"] || [strbtnRadio_1_9B isEqualToString:@"0"]){
        
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Partial"]];
    if([strbtnChkBox_1_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_1_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Spot"]];
    if([strbtnChkBox_2_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_2_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Bait"]];
    if([strbtnChkBox_3_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_3_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_10A_Subterranean=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforSubterraneanTermites_Other"]];
    if([strbtnChkBox_4_10A_Subterranean isEqualToString:@"true"] || [strbtnChkBox_4_10A_Subterranean isEqualToString:@"1"]){
        
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_1_10A_Drywood=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforDrywoodTermites_Full"]];
    if([strbtnChkBox_1_10A_Drywood isEqualToString:@"true"] || [strbtnChkBox_1_10A_Drywood isEqualToString:@"1"]){
        
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_10A_Drywood=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"TreatingforDrywoodTermites_Limited"]];
    if([strbtnChkBox_2_10A_Drywood isEqualToString:@"true"] || [strbtnChkBox_2_10A_Drywood isEqualToString:@"1"]){
        
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnRadio_1_10A_Contract=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"]];
    if([strbtnRadio_1_10A_Contract isEqualToString:@"true"] || [strbtnRadio_1_10A_Contract isEqualToString:@"1"]){
        
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else if([strbtnRadio_1_10A_Contract isEqualToString:@"false"] || [strbtnRadio_1_10A_Contract isEqualToString:@"0"]){
        
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }else{
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
    
    NSString *strbtnChkBox_1_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"ElectricBreakerBox_12A"]];
    if([strbtnChkBox_1_12A isEqualToString:@"true"] || [strbtnChkBox_1_12A isEqualToString:@"1"]){
        
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_2_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"WaterHeaterCloset_12A"]];
    if([strbtnChkBox_2_12A isEqualToString:@"true"] || [strbtnChkBox_2_12A isEqualToString:@"1"]){
        
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_3_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BathTrapAccess_12A"]];
    if([strbtnChkBox_3_12A isEqualToString:@"true"] || [strbtnChkBox_3_12A isEqualToString:@"1"]){
        
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *strbtnChkBox_4_12A=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BeneathTheKitchenSink_12A"]];
    if([strbtnChkBox_4_12A isEqualToString:@"true"] || [strbtnChkBox_4_12A isEqualToString:@"1"]){
        
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        
    }else{
        
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        
    }
    
    //DatePosted_12B
    
    strDatePosted=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DatePosted_12B"]];
    
    if (strDatePosted.length==0) {
        
        [_btn_DatePosted setTitle:[self currentDate] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:[self currentDate] forKey:@"DatePosted_12B"];
        
        
    } else {
        
        strDatePosted=[self changeDateToFormattedDate:strDatePosted];
        
        [_btn_DatePosted setTitle:strDatePosted forState:UIControlStateNormal];
        
    }//DateOfPurchaserOfProperty
    
    strDateOfPurchaserOfProperty=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"DateOfPurchaserOfProperty"]];
    
    if (strDateOfPurchaserOfProperty.length==0) {
        
        [_btn_DateOfPurchaserOfProperty setTitle:[self currentDate] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:[self currentDate] forKey:@"DateOfPurchaserOfProperty"];
        
    } else {
        
        strDateOfPurchaserOfProperty=[self changeDateToFormattedDate:strDateOfPurchaserOfProperty];
        [_btn_DateOfPurchaserOfProperty setTitle:strDateOfPurchaserOfProperty forState:UIControlStateNormal];
        
    }//DateOfPurchaserOfProperty
    
    //Buyers Initials Image Download  BuyersInitials_SignaturePath_7B
    
    NSString *strBuyersInitials_SignaturePath_10B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
    
    if (!(strBuyersInitials_SignaturePath_10B.length==0)) {
        
        [self downloadBuyersInitials:strBuyersInitials_SignaturePath_10B :_imgViewBuyerIntials];
        strGlobalBuyersSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
        
    }else{
        _imgViewBuyerIntials.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSign=@"";
        
    }
    
    NSString *strGraphPath__TexasOffiicial_WoodDestroyingInsect=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
    
    if (!(strGraphPath__TexasOffiicial_WoodDestroyingInsect.length==0)) {
        
        [self downloadGraph:strGraphPath__TexasOffiicial_WoodDestroyingInsect];
        strGlobalGraphImage=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
        strGraphImageCaption=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Graph_Caption"]];
        strGraphImageDescription=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Graph_Description"]];
        
    }else{
        
        [_btnAddGraphImg setImage:[UIImage imageNamed:@"NoImage.jpg"] forState:UIControlStateNormal];
        strGlobalGraphImage=@"";
        strGraphImageCaption=@"";
        strGraphImageDescription=@"";
    }
    
    NSString *strInspector_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
    
    if (!(strInspector_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strInspector_SignaturePath :_imgViewInspectorSign];
        strGlobalInspectorSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
        
    }else{
        
        _imgViewInspectorSign.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalInspectorSign=@"";
        
    }
    
    
    NSString *strCertifiedApplicator_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
    
    if (!(strCertifiedApplicator_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strCertifiedApplicator_SignaturePath :_imgViewCertifiedApplicatorSign];
        strGlobalApplicatorSign=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
        
    }else{
        
        _imgViewCertifiedApplicatorSign.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalApplicatorSign=@"";
        
    }
    
    
    NSString *strSignatureOfPurchaserOfProperty_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
    
    if (!(strSignatureOfPurchaserOfProperty_SignaturePath.length==0)) {
        
        [self downloadBuyersInitials:strSignatureOfPurchaserOfProperty_SignaturePath :_imgViewBuyerIntialsPurchaser];
        strGlobalBuyersSignPurchaser=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
        
    }else{
        
        _imgViewBuyerIntialsPurchaser.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSignPurchaser=@"";
        
    }
    
    NSString *strBuyersInitials_SignaturePath_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
    
    if (!(strBuyersInitials_SignaturePath_7B.length==0)) {
        
        [self downloadBuyersInitials:strBuyersInitials_SignaturePath_7B :_imgView_BuyerInitial_7B];
        strGlobalBuyersSign7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
        
    }else{
        
        _imgView_BuyerInitial_7B.image=[UIImage imageNamed:@"NoImage.jpg"];
        strGlobalBuyersSign7B=@"";
        
    }
    
}

-(void)setEditableOrNot{
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        [_txt_InspectedAddress setEnabled:NO];
        [_txt_City setEnabled:NO];
        [_txt_ZipCode setEnabled:NO];
        [_txt_1A setEnabled:NO];
        [_txt_1B setEnabled:NO];
        [_txt_1C setEnabled:NO];
        [_txt_ZipCodeView4 setEnabled:NO];
        [_txt_CityView4 setEnabled:NO];
        [_txt_State setEnabled:NO];
        [_txt_TelephoneNo setEnabled:NO];
        [_txt_1D setEnabled:NO];
        [_txt_2 setEnabled:NO];
        [_txt_3 setEnabled:NO];
        [_txt_4B setEnabled:NO];
        [_txt_5 setEnabled:NO];
        [_txt_6BSpecify setEnabled:NO];
        [_txt_7BSpecify setEnabled:NO];
        [_txt_8F setEnabled:NO];
        [_txt_8G setEnabled:NO];
        [_txt_8GD setEnabled:NO];
        [_txt_9 setEnabled:NO];
        [_txt_9BSpecifyReason setEnabled:NO];
        [_txt_9B setEnabled:NO];
        [_txt_10A setEnabled:NO];
        [_txt_DateOfTreatment setEnabled:NO];
        [_txt_CommonNameOfInsect setEnabled:NO];
        [_txt_NameOfPesticide setEnabled:NO];
        [_txt_ListInsects setEnabled:NO];
        [_txt_12B setEnabled:NO];
        [_txt_ApplicatorLicenseNumber setEnabled:NO];
        [_btnChkBoxA setEnabled:NO];
        [_btnChkBoxB setEnabled:NO];
        [_btnChkBoxC setEnabled:NO];
        [_btnChkBoxD setEnabled:NO];
        [_btnChkBoxE setEnabled:NO];
        [_btnChkBoxF setEnabled:NO];
        [_btnChkBoxG setEnabled:NO];
        [_btnChkBoxH setEnabled:NO];
        [_btnChkBoxI setEnabled:NO];
        [_btnChkBoxJ setEnabled:NO];
        [_btnRadio_1_1E setEnabled:NO];
        [_btnRadio_2_1E setEnabled:NO];
        [_btnRadio_1_4A setEnabled:NO];
        [_btnRadio_2_4A setEnabled:NO];
        [_btnRadio_3_4A setEnabled:NO];
        [_btnRadio_4_4A setEnabled:NO];
        [_btnRadio_5_4A setEnabled:NO];
        [_btnChkBox_1_4C setEnabled:NO];
        [_btnChkBox_2_4C setEnabled:NO];
        [_btnChkBox_3_4C setEnabled:NO];
        [_btnChkBox_4_4C setEnabled:NO];
        [_btnChkBox_5_4C setEnabled:NO];
        [_btnInspectionDate setEnabled:NO];
        [_btnChkBoxA setEnabled:NO];
        [_btnRadio_1_6A setEnabled:NO];
        [_btnRadio_2_6A setEnabled:NO];
        [_btnChkBox_6B setEnabled:NO];
        [_btnChkBox_1_6B setEnabled:NO];
        [_btnChkBox_2_6B setEnabled:NO];
        [_btnChkBox_3_6B setEnabled:NO];
        [_btnChkBox_4_6B setEnabled:NO];
        [_btnChkBox_5_6B setEnabled:NO];
        [_btnChkBox_6_6B setEnabled:NO];
        [_btnChkBox_7_6B setEnabled:NO];
        [_btnChkBox_8_6B setEnabled:NO];
        [_btnChkBox_9_6B setEnabled:NO];
        [_btnChkBox_10_6B setEnabled:NO];
        [_btnChkBox_11_6B setEnabled:NO];
        [_btnChkBox_12_6B setEnabled:NO];
        [_btnChkBox_13_6B setEnabled:NO];
        [_btnRadio_1_7A setEnabled:NO];
        [_btnRadio_2_7A setEnabled:NO];
        [_btnChkBox_1_7B setEnabled:NO];
        [_btnChkBox_2_7B setEnabled:NO];
        [_btnChkBox_3_7B setEnabled:NO];
        [_btnChkBox_4_7B setEnabled:NO];
        [_btnChkBox_5_7B setEnabled:NO];
        [_btnChkBox_6_7B setEnabled:NO];
        [_btnChkBox_7_7B setEnabled:NO];
        [_btnChkBox_8_7B setEnabled:NO];
        [_btnChkBox_9_7B setEnabled:NO];
        [_btnChkBox_10_7B setEnabled:NO];
        [_btnChkBox_11_7B setEnabled:NO];
        [_btnChkBox_12_7B setEnabled:NO];
        [_btnChkBox_13_7B setEnabled:NO];
        [_btnChkBoxA setEnabled:NO];
        [_btnRadio_1_8_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_8A_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8A_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8A_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8A_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8A_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8A_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_8B_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8B_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8B_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8B_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8B_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8B_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_8C_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8C_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8C_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8C_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8C_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8C_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_8D_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8D_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8D_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8D_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8D_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8D_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_8E_ActiveInfestation setEnabled:NO];
        [_btnRadio_2_8E_ActiveInfestation setEnabled:NO];
        [_btnRadio_1_8E_PreviousInfestation setEnabled:NO];
        [_btnRadio_2_8E_PreviousInfestation setEnabled:NO];
        [_btnRadio_1_8E_PreviousTreatment setEnabled:NO];
        [_btnRadio_2_8E_PreviousTreatment setEnabled:NO];
        [_btnRadio_1_9 setEnabled:NO];
        [_btnRadio_2_9 setEnabled:NO];
        [_btnRadio_1_9A setEnabled:NO];
        [_btnRadio_2_9A setEnabled:NO];
        [_btnRadio_1_9B setEnabled:NO];
        [_btnRadio_2_9B setEnabled:NO];
        [_btnChkBox_1_10A_Subterranean setEnabled:NO];
        [_btnChkBox_2_10A_Subterranean setEnabled:NO];
        [_btnChkBox_3_10A_Subterranean setEnabled:NO];
        [_btnChkBox_4_10A_Subterranean setEnabled:NO];
        [_btnChkBox_1_10A_Drywood setEnabled:NO];
        [_btnChkBox_2_10A_Drywood setEnabled:NO];
        [_btnRadio_1_10A_Contract setEnabled:NO];
        [_btnRadio_2_10A_Contract setEnabled:NO];
        [_btnBuyerIntials setEnabled:NO];
        [_btnAddGraph setEnabled:NO];
        [_btnInspectorSignature setEnabled:NO];
        [_btnAddGraphImg setEnabled:NO];
        [_btnCertifiedApplicatorSign setEnabled:NO];
        [_btnBuyerIntialsPurchaser setEnabled:NO];
        [_btnChkBox_1_12A setEnabled:NO];
        [_btnChkBox_2_12A setEnabled:NO];
        [_btnChkBox_3_12A setEnabled:NO];
        [_btnChkBox_4_12A setEnabled:NO];
        [_btn_DatePosted setEnabled:NO];
        [_btnBuyersInitial_7B setEnabled:NO];
        [_txt_4A_Other setEnabled:NO];
        
    } else {
        
        [_txt_4A_Other setEnabled:YES];
        [_txt_InspectedAddress setEnabled:YES];
        [_txt_City setEnabled:YES];
        [_txt_ZipCode setEnabled:YES];
        [_txt_1A setEnabled:YES];
        [_txt_1B setEnabled:YES];
        [_txt_1C setEnabled:YES];
        [_txt_ZipCodeView4 setEnabled:YES];
        [_txt_CityView4 setEnabled:YES];
        [_txt_State setEnabled:YES];
        [_txt_TelephoneNo setEnabled:YES];
        [_txt_1D setEnabled:YES];
        [_txt_2 setEnabled:YES];
        [_txt_3 setEnabled:YES];
        [_txt_4B setEnabled:YES];
        [_txt_5 setEnabled:YES];
        [_txt_6BSpecify setEnabled:YES];
        [_txt_7BSpecify setEnabled:YES];
        [_txt_8F setEnabled:YES];
        [_txt_8G setEnabled:YES];
        [_txt_8GD setEnabled:YES];
        [_txt_9 setEnabled:YES];
        [_txt_9BSpecifyReason setEnabled:YES];
        [_txt_9B setEnabled:YES];
        [_txt_10A setEnabled:YES];
        [_txt_DateOfTreatment setEnabled:YES];
        [_txt_CommonNameOfInsect setEnabled:YES];
        [_txt_NameOfPesticide setEnabled:YES];
        [_txt_ListInsects setEnabled:YES];
        [_txt_12B setEnabled:YES];
        [_txt_ApplicatorLicenseNumber setEnabled:YES];
        [_btnChkBoxA setEnabled:YES];
        [_btnChkBoxB setEnabled:YES];
        [_btnChkBoxC setEnabled:YES];
        [_btnChkBoxD setEnabled:YES];
        [_btnChkBoxE setEnabled:YES];
        [_btnChkBoxF setEnabled:YES];
        [_btnChkBoxG setEnabled:YES];
        [_btnChkBoxH setEnabled:YES];
        [_btnChkBoxI setEnabled:YES];
        [_btnChkBoxJ setEnabled:YES];
        [_btnRadio_1_1E setEnabled:YES];
        [_btnRadio_2_1E setEnabled:YES];
        [_btnRadio_1_4A setEnabled:YES];
        [_btnRadio_2_4A setEnabled:YES];
        [_btnRadio_3_4A setEnabled:YES];
        [_btnRadio_4_4A setEnabled:YES];
        [_btnRadio_5_4A setEnabled:YES];
        [_btnChkBox_1_4C setEnabled:YES];
        [_btnChkBox_2_4C setEnabled:YES];
        [_btnChkBox_3_4C setEnabled:YES];
        [_btnChkBox_4_4C setEnabled:YES];
        [_btnChkBox_5_4C setEnabled:YES];
        [_btnInspectionDate setEnabled:YES];
        [_btnChkBoxA setEnabled:YES];
        [_btnRadio_1_6A setEnabled:YES];
        [_btnRadio_2_6A setEnabled:YES];
        [_btnChkBox_6B setEnabled:YES];
        [_btnChkBox_1_6B setEnabled:YES];
        [_btnChkBox_2_6B setEnabled:YES];
        [_btnChkBox_3_6B setEnabled:YES];
        [_btnChkBox_4_6B setEnabled:YES];
        [_btnChkBox_5_6B setEnabled:YES];
        [_btnChkBox_6_6B setEnabled:YES];
        [_btnChkBox_7_6B setEnabled:YES];
        [_btnChkBox_8_6B setEnabled:YES];
        [_btnChkBox_9_6B setEnabled:YES];
        [_btnChkBox_10_6B setEnabled:YES];
        [_btnChkBox_11_6B setEnabled:YES];
        [_btnChkBox_12_6B setEnabled:YES];
        [_btnChkBox_13_6B setEnabled:YES];
        [_btnRadio_1_7A setEnabled:YES];
        [_btnRadio_2_7A setEnabled:YES];
        [_btnChkBox_1_7B setEnabled:YES];
        [_btnChkBox_2_7B setEnabled:YES];
        [_btnChkBox_3_7B setEnabled:YES];
        [_btnChkBox_4_7B setEnabled:YES];
        [_btnChkBox_5_7B setEnabled:YES];
        [_btnChkBox_6_7B setEnabled:YES];
        [_btnChkBox_7_7B setEnabled:YES];
        [_btnChkBox_8_7B setEnabled:YES];
        [_btnChkBox_9_7B setEnabled:YES];
        [_btnChkBox_10_7B setEnabled:YES];
        [_btnChkBox_11_7B setEnabled:YES];
        [_btnChkBox_12_7B setEnabled:YES];
        [_btnChkBox_13_7B setEnabled:YES];
        [_btnChkBoxA setEnabled:YES];
        [_btnRadio_1_8_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_8A_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8A_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8A_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8A_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8A_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8A_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_8B_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8B_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8B_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8B_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8B_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8B_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_8C_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8C_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8C_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8C_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8C_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8C_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_8D_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8D_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8D_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8D_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8D_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8D_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_8E_ActiveInfestation setEnabled:YES];
        [_btnRadio_2_8E_ActiveInfestation setEnabled:YES];
        [_btnRadio_1_8E_PreviousInfestation setEnabled:YES];
        [_btnRadio_2_8E_PreviousInfestation setEnabled:YES];
        [_btnRadio_1_8E_PreviousTreatment setEnabled:YES];
        [_btnRadio_2_8E_PreviousTreatment setEnabled:YES];
        [_btnRadio_1_9 setEnabled:YES];
        [_btnRadio_2_9 setEnabled:YES];
        [_btnRadio_1_9A setEnabled:YES];
        [_btnRadio_2_9A setEnabled:YES];
        [_btnRadio_1_9B setEnabled:YES];
        [_btnRadio_2_9B setEnabled:YES];
        [_btnChkBox_1_10A_Subterranean setEnabled:YES];
        [_btnChkBox_2_10A_Subterranean setEnabled:YES];
        [_btnChkBox_3_10A_Subterranean setEnabled:YES];
        [_btnChkBox_4_10A_Subterranean setEnabled:YES];
        [_btnChkBox_1_10A_Drywood setEnabled:YES];
        [_btnChkBox_2_10A_Drywood setEnabled:YES];
        [_btnRadio_1_10A_Contract setEnabled:YES];
        [_btnRadio_2_10A_Contract setEnabled:YES];
        [_btnBuyerIntials setEnabled:YES];
        [_btnAddGraph setEnabled:YES];
        [_btnInspectorSignature setEnabled:YES];
        [_btnAddGraphImg setEnabled:YES];
        [_btnCertifiedApplicatorSign setEnabled:YES];
        [_btnBuyerIntialsPurchaser setEnabled:YES];
        [_btnChkBox_1_12A setEnabled:YES];
        [_btnChkBox_2_12A setEnabled:YES];
        [_btnChkBox_3_12A setEnabled:YES];
        [_btnChkBox_4_12A setEnabled:YES];
        [_btn_DatePosted setEnabled:YES];
        [_btnBuyersInitial_7B setEnabled:YES];
        
    }
    
}

//============================================================================
#pragma mark- Load Image
//============================================================================

- (UIImage*)loadImage:(NSString *)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)methodSetAllViews{
    
    
    [_btnInspectionDate.layer setCornerRadius:5.0f];
    [_btnInspectionDate.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btnInspectionDate.layer setBorderWidth:0.8f];
    [_btnInspectionDate.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btnInspectionDate.layer setShadowOpacity:0.3];
    [_btnInspectionDate.layer setShadowRadius:3.0];
    [_btnInspectionDate.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DatePosted.layer setCornerRadius:5.0f];
    [_btn_DatePosted.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DatePosted.layer setBorderWidth:0.8f];
    [_btn_DatePosted.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DatePosted.layer setShadowOpacity:0.3];
    [_btn_DatePosted.layer setShadowRadius:3.0];
    [_btn_DatePosted.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DateTreatmentByInspectionCompany.layer setCornerRadius:5.0f];
    [_btn_DateTreatmentByInspectionCompany.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DateTreatmentByInspectionCompany.layer setBorderWidth:0.8f];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowOpacity:0.3];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowRadius:3.0];
    [_btn_DateTreatmentByInspectionCompany.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [_btn_DateOfPurchaserOfProperty.layer setCornerRadius:5.0f];
    [_btn_DateOfPurchaserOfProperty.layer setBorderColor:[UIColor blackColor].CGColor];
    [_btn_DateOfPurchaserOfProperty.layer setBorderWidth:0.8f];
    [_btn_DateOfPurchaserOfProperty.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btn_DateOfPurchaserOfProperty.layer setShadowOpacity:0.3];
    [_btn_DateOfPurchaserOfProperty.layer setShadowRadius:3.0];
    [_btn_DateOfPurchaserOfProperty.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    CGRect frameFor_View1=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,_view1.frame.size.height);
    [_view1 setFrame:frameFor_View1];
    
    [_scrollVieww addSubview:_view1];
    
    CGRect frameFor_View2=CGRectMake(0, _view1.frame.origin.y+_view1.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view2.frame.size.height);
    [_view2 setFrame:frameFor_View2];
    
    [_scrollVieww addSubview:_view2];
    
    CGRect frameFor_View3=CGRectMake(0, _view2.frame.origin.y+_view2.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view3.frame.size.height);
    [_view3 setFrame:frameFor_View3];
    
    [_scrollVieww addSubview:_view3];
    
    
    CGRect frameFor_View4=CGRectMake(0, _view3.frame.origin.y+_view3.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view4.frame.size.height);
    [_view4 setFrame:frameFor_View4];
    
    [_scrollVieww addSubview:_view4];
    
    CGRect frameFor_View5=CGRectMake(0, _view4.frame.origin.y+_view4.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5.frame.size.height);
    [_view5 setFrame:frameFor_View5];
    
    [_scrollVieww addSubview:_view5];
    
    CGRect frameFor_View5Sub=CGRectMake(0, _view5.frame.origin.y+_view5.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view5Sub.frame.size.height);
    [_view5Sub setFrame:frameFor_View5Sub];
    
    [_scrollVieww addSubview:_view5Sub];
    
    
    CGRect frameFor_View6=CGRectMake(0, _view5Sub.frame.origin.y+_view5Sub.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view6.frame.size.height);
    [_view6 setFrame:frameFor_View6];
    
    [_scrollVieww addSubview:_view6];
    
    CGRect frameFor_View7=CGRectMake(0, _view6.frame.origin.y+_view6.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view7.frame.size.height);
    [_view7 setFrame:frameFor_View7];
    
    [_scrollVieww addSubview:_view7];
    
    CGRect frameFor_View8=CGRectMake(0, _view7.frame.origin.y+_view7.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view8.frame.size.height);
    [_view8 setFrame:frameFor_View8];
    
    [_scrollVieww addSubview:_view8];
    
    CGRect frameFor_View9=CGRectMake(0, _view8.frame.origin.y+_view8.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view9.frame.size.height);
    [_view9 setFrame:frameFor_View9];
    
    [_scrollVieww addSubview:_view9];
    
    CGRect frameFor_View10=CGRectMake(0, _view9.frame.origin.y+_view9.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view10.frame.size.height);
    [_view10 setFrame:frameFor_View10];
    
    [_scrollVieww addSubview:_view10];
    
    CGRect frameFor_View11=CGRectMake(0, _view10.frame.origin.y+_view10.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view11.frame.size.height);
    [_view11 setFrame:frameFor_View11];
    
    [_scrollVieww addSubview:_view11];
    
    CGRect frameFor_View12=CGRectMake(0, _view11.frame.origin.y+_view11.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view12.frame.size.height);
    [_view12 setFrame:frameFor_View12];
    
    [_scrollVieww addSubview:_view12];
    
    
    CGRect frameFor_View13=CGRectMake(0, _view12.frame.origin.y+_view12.frame.size.height, [UIScreen mainScreen].bounds.size.width,_view13.frame.size.height);
    [_view13 setFrame:frameFor_View13];
    //_viewForcollectionView7A.backgroundColor=[UIColor redColor];
    //_viewForcollectionView8.backgroundColor=[UIColor yellowColor];
    [_scrollVieww addSubview:_view13];
    
    //Nilind 06 Dec
    /* CGRect frameFor_View14=CGRectMake(0, _view13.frame.origin.y+_viewForcollectionView7A.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForcollectionView7A.frame.size.height);
     [_viewForcollectionView7A setFrame:frameFor_View14];
     
     [_scrollVieww addSubview:_viewForcollectionView7A];
     
     _viewForcollectionView7A.backgroundColor=[UIColor redColor];
     _viewForcollectionView8.backgroundColor=[UIColor yellowColor];
     
     
     CGRect frameFor_View15=CGRectMake(0, _viewForcollectionView7A.frame.origin.y+_viewForcollectionView8.frame.size.height, [UIScreen mainScreen].bounds.size.width,_viewForcollectionView8.frame.size.height);
     [_viewForcollectionView8 setFrame:frameFor_View15];
     
     [_scrollVieww addSubview:_viewForcollectionView8];
     
     [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_viewForcollectionView8.frame.size.height+_viewForcollectionView8.frame.origin.y)];*/
    
    //End
    
    [_scrollVieww setContentSize:CGSizeMake( _scrollVieww.frame.size.width,_view13.frame.size.height+_view13.frame.origin.y)];
    
}

- (IBAction)action_Back:(id)sender {
    
    int index = 0;
    NSArray *arrstack=self.navigationController.viewControllers;
    for (int k1=0; k1<arrstack.count; k1++) {
        if ([[arrstack objectAtIndex:k1] isKindOfClass:[TermiteSelectStateViewController class]]) {
            index=k1;
            //break;
        }
    }
    TermiteSelectStateViewController *myController = (TermiteSelectStateViewController *)[self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:myController animated:NO];
    
    
}

//===========================================
//===========================================
#pragma mark - View 2 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnChkBoxA:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxA];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_A"];
        
    }
    else
    {
        
        [_btnChkBoxA setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_A"];
        
    }
    
}


- (IBAction)action_btnChkBoxB:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxB];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_B"];
        
    }
    else
    {
        
        [_btnChkBoxB setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_B"];
        
    }
    
}


- (IBAction)action_btnChkBoxC:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxC];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_C"];
        
    }
    else
    {
        
        [_btnChkBoxC setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_C"];
        
    }
    
}


- (IBAction)action_btnChkBoxD:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxD];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_D"];
        
    }
    else
    {
        
        [_btnChkBoxD setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_D"];
        
    }
    
}


- (IBAction)action_btnChkBoxE:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxE];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_E"];
        
    }
    else
    {
        
        [_btnChkBoxE setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_E"];
        
    }
    
}


- (IBAction)action_btnChkBoxF:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxF];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_F"];
        
    }
    else
    {
        
        [_btnChkBoxF setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_F"];
        
    }
    
}

//===========================================
//===========================================
#pragma mark - View 3 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnChkBoxG:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxG];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_G"];
        
    }
    else
    {
        
        [_btnChkBoxG setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_G"];
        
    }
    
}


- (IBAction)action_btnChkBoxH:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxH];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_H"];
        
    }
    else
    {
        
        [_btnChkBoxH setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_H"];
        
    }
    
}


- (IBAction)action_btnChkBoxI:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxI];
    
    if(isCompare==true)
    {
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_I"];
        
    }
    else
    {
        
        [_btnChkBoxI setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_I"];
        
    }
    
}


- (IBAction)action_btnChkBoxJ:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBoxJ];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ScopeOfInspection_J"];
        
    }
    else
    {
        
        [_btnChkBoxJ setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ScopeOfInspection_J"];
        
    }
    
}

//===========================================
//===========================================
#pragma mark - View 4 Action Methodss
//===========================================
//===========================================


- (IBAction)action_btnRadio_1_1E:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_1E];
    
    if(isCompare==true)
    {
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Certified Applicator" forKey:@"CertifiedApplicator_Technician"];
        
    }
    else
    {
        
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CertifiedApplicator_Technician"];
    }
    
}


- (IBAction)action_btnRadio_2_1E:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_1E];
    
    if(isCompare==true)
    {
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Technician" forKey:@"CertifiedApplicator_Technician"];
    }
    else
    {
        
        [_btnRadio_2_1E setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CertifiedApplicator_Technician"];
    }
    
}


- (IBAction)action_btnRadio_1_4A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_4A];
    
    if(isCompare==true)
    {
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Seller" forKey:@"PersonType_PurchasingInspection"];
        
    }
    else
    {
        
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"PersonType_PurchasingInspection"];
    }
    
}


- (IBAction)action_btnRadio_2_4A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_4A];
    
    if(isCompare==true)
    {
        
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Agent" forKey:@"PersonType_PurchasingInspection"];
    }
    else
    {
        
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"PersonType_PurchasingInspection"];
    }
    
}


- (IBAction)action_btnRadio_3_4A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_3_4A];
    
    if(isCompare==true)
    {
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Buyer" forKey:@"PersonType_PurchasingInspection"];
    }
    else
    {
        
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"PersonType_PurchasingInspection"];
    }
    
}


- (IBAction)action_btnRadio_4_4A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_4_4A];
    
    if(isCompare==true)
    {
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Management Co." forKey:@"PersonType_PurchasingInspection"];
    }
    else
    {
        
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"PersonType_PurchasingInspection"];
    }
    
}


- (IBAction)action_btnRadio_5_4A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_5_4A];
    
    if(isCompare==true)
    {
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_3_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_4_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"Other" forKey:@"PersonType_PurchasingInspection"];
    }
    else
    {
        
        [_btnRadio_5_4A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"PersonType_PurchasingInspection"];
    }
    
}

- (IBAction)action_btnChkBox_1_4C:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_4C];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        //[dictGlobalTermiteTexas setValue:@"true" forKey:@""];
        
        //Nilind 15 Dec
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"];
        
    }
    else
    {
        
        [_btnChkBox_1_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        //Nilind 15
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"];
        
    }
    
}

- (IBAction)action_btnChkBox_2_4C:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_4C];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        //[dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"];
        //ReportForwardedTo_PurchaseService
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_PurchaseService"];
        
    }
    else
    {
        
        [_btnChkBox_2_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        //[dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_TitleCompany_Or_Mortgage"];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_PurchaseService"];
        
    }
    
}


- (IBAction)action_btnChkBox_3_4C:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_3_4C];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_Seller"];
    }
    else
    {
        
        [_btnChkBox_3_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_Seller"];
    }
    
}

- (IBAction)action_btnChkBox_4_4C:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_4_4C];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_Agent"];
    }
    else
    {
        
        [_btnChkBox_4_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_Agent"];
    }
    
}

- (IBAction)action_btnChkBox_5_4C:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_5_4C];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ReportForwardedTo_Buyer"];
    }
    else
    {
        
        [_btnChkBox_5_4C setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ReportForwardedTo_Buyer"];
    }
    
}

- (IBAction)action_InspectionDate:(id)sender {
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=108;
    [self addPickerViewDateTo :strDateInspectiondate];
    
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

/*
 -(void)addPickerViewDateTo :(NSString*)strDateString
 {
 pickerDate=[[UIDatePicker alloc]init];
     pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
 pickerDate.frame=CGRectMake(0,0, 320, 350);
 // [pickerDate setMinimumDate:[NSDate date]];
 
 if (!(strDateString.length==0)) {
 
 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
 [dateFormat setDateFormat:@"MM/dd/yyyy"];
 NSDate *dateToSett = [dateFormat dateFromString:strDateString];
 
 if (dateToSett==nil) {
 
 }else
 {
 
 pickerDate.date =dateToSett;
 
 }
 
 }
 
     pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
 [viewForDate setHidden:NO];
 
 viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
 viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
 [self.view addSubview: viewBackGround];
 
 //============================================================================
 //============================================================================
 
 //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
 //    singleTap1.numberOfTapsRequired = 1;
 //    [viewBackGround setUserInteractionEnabled:YES];
 //    [viewBackGround addGestureRecognizer:singleTap1];
 
 
 viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
 [viewBackGround addSubview: viewForDate];
 
 viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
 viewForDate.layer.cornerRadius=20.0;
 viewForDate.clipsToBounds=YES;
 [viewForDate.layer setBorderWidth:2.0];
 // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
 [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
 UILabel *lblTitle;
 lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
 
 lblTitle.text=@"SELECT DATE";
 lblTitle.font=[UIFont boldSystemFontOfSize:20];
 lblTitle.textAlignment=NSTextAlignmentCenter;
 lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
 [viewForDate addSubview:lblTitle];
 UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
 [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
 [viewForDate addSubview:lblLineUp];
 
 UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
 [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
 [viewForDate addSubview:lblLine];
 
 UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
 if([UIScreen mainScreen].bounds.size.height==736)
 {
 btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
 }
 else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
 {
 btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
 }
 //btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-100, lblLine.frame.origin.y+5, 100, 40)];
 
 if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
 
 btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
 
 }
 
 if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
 
 btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
 
 }
 
 [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
 
 [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 
 
 //    btnClose.layer.cornerRadius=10.0;
 //    btnClose.clipsToBounds=YES;
 //    [btnClose.layer setBorderWidth:2.0];
 //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
 
 btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
 
 [viewForDate addSubview:btnClose];
 
 UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
 
 if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
 {
 btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,40)];
 }
 
 if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
 
 btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
 
 }
 
 if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
 
 btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
 
 }
 
 [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
 [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 
 //    btnDone.layer.cornerRadius=10.0;
 //    btnDone.clipsToBounds=YES;
 //    [btnDone.layer setBorderWidth:2.0];
 //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
 btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
 
 [viewForDate addSubview:btnDone];
 [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
 [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
 
 pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
 [viewForDate addSubview:pickerDate];
 }
 */


// Akshay 8 may 2019, above same method commented
-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    // [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strDateString.length==0)) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        
        if (dateToSett==nil) {
            
        }else
        {
            pickerDate.date =dateToSett;
        }
    }
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    //    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
    //    singleTap1.numberOfTapsRequired = 1;
    //    [viewBackGround setUserInteractionEnabled:YES];
    //    [viewBackGround addGestureRecognizer:singleTap1];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:18];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    
    float btnWidth = (viewForDate.frame.size.width-60)/3; //60 i.e 20 leading, 20 trailing,10-10 space between buttons
    
    // adding close button
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(20, lblLine.frame.origin.y+10, btnWidth, 35)];
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    btnClose.titleLabel.font = [UIFont systemFontOfSize:16];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    
    // adding clear button
    UIButton *btnClear=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnClose.frame)+10, btnClose.frame.origin.y, btnWidth, 35)];
    
    [btnClear setTitle:@"CLEAR" forState:UIControlStateNormal];
    
    btnClear.titleLabel.font = [UIFont systemFontOfSize:16];
    
    [btnClear setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClear.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClear];
    
    
    // adding done button
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(btnClear.frame)+10, btnClose.frame.origin.y, btnWidth,35)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    
    btnDone.titleLabel.font = [UIFont systemFontOfSize:16];
    
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    
    
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    [btnClear addTarget: self action: @selector(dismissPickerSheetAndRemoveDate:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}

- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSInteger i;
    i=tblData.tag;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    pickerDate.minimumDate=[NSDate date];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    
    //    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    //    if (yesSameDay) {
    //
    //        [self settingDate:i];
    //
    //    }else{
    //        NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
    //
    //        if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
    //
    //            [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
    //
    //        }else{
    
    // [self settingDate:i];
    
    if (i==108) {
        
        [_btnInspectionDate setTitle:strDate forState:UIControlStateNormal];
        strDateInspectiondate=strDate;
        [dictGlobalTermiteTexas setValue:strDateInspectiondate forKey:@"InspectionDate"];
        
        
        [_btnInspectionDate setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
        
        
    } else if (i==109){
        
        [_btn_DateTreatmentByInspectionCompany setTitle:strDate forState:UIControlStateNormal];
        strDateTreatmentByInspectionCompany=strDate;
        [dictGlobalTermiteTexas setValue:strDateTreatmentByInspectionCompany forKey:@"DateOFTreatment_InspectingCompany"];
        [_btn_DateTreatmentByInspectionCompany setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
        
    } else if (i==110){
        
        [_btn_DatePosted setTitle:strDate forState:UIControlStateNormal];
        strDatePosted=strDate;
        [dictGlobalTermiteTexas setValue:strDatePosted forKey:@"DatePosted_12B"];
        [_btn_DatePosted setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
        
    } else if (i==111){
        
        [_btn_DateOfPurchaserOfProperty setTitle:strDate forState:UIControlStateNormal];
        strDateOfPurchaserOfProperty=strDate;
        [dictGlobalTermiteTexas setValue:strDateOfPurchaserOfProperty forKey:@"DateOfPurchaserOfProperty"];
        [_btn_DateOfPurchaserOfProperty setTitle:[self getDateInDayFormat:strDate] forState:UIControlStateNormal];
        
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
    // }
    //    }
}
-(void)settingDate :(NSInteger)tag{
    
    if (tag==108)
    {
        if ([_btnInspectionDate.titleLabel.text isEqualToString:@"Select Date"]) {
            [_btnInspectionDate setTitle:strDate forState:UIControlStateNormal];
            strDateInspectiondate=strDate;
        } else {
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MM/dd/yyyy"];
            [_btnInspectionDate setTitle:strDate forState:UIControlStateNormal];
            strDateInspectiondate=strDate;
        }
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}

//===========================================
//===========================================
#pragma mark - View 5 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnRadio_1_6A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_6A];
    
    if(isCompare==true)
    {
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"IsPropertyObstrected_Or_Inaccessible"];
    }
    else
    {
        
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsPropertyObstrected_Or_Inaccessible"];
    }
    
}


- (IBAction)action_btnRadio_2_6A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_6A];
    
    if(isCompare==true)
    {
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"IsPropertyObstrected_Or_Inaccessible"];
    }
    else
    {
        
        [_btnRadio_2_6A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsPropertyObstrected_Or_Inaccessible"];
    }
    
}

- (IBAction)action_btnChkBox_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_SlightCornerCracks"];
    }
    else
    {
        
        [_btnChkBox_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_SlightCornerCracks"];
    }
    
}

- (IBAction)action_btnChkBox_1_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_Attic"];
    }
    else
    {
        
        [_btnChkBox_1_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_Attic"];
    }
    
}


- (IBAction)action_btnChkBox_2_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_InsulatedAreaOfAttic"];
    }
    else
    {
        
        [_btnChkBox_2_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_InsulatedAreaOfAttic"];
    }
    
}


- (IBAction)action_btnChkBox_3_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_3_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_PlumbingAreas"];
    }
    else
    {
        
        [_btnChkBox_3_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_PlumbingAreas"];
    }
    
}


- (IBAction)action_btnChkBox_4_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_4_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_PlanterBoxAbuttingStructure"];
    }
    else
    {
        
        [_btnChkBox_4_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_PlanterBoxAbuttingStructure"];
    }
    
}


- (IBAction)action_btnChkBox_5_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_5_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_Deck"];
    }
    else
    {
        
        [_btnChkBox_5_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_Deck"];
    }
    
}


- (IBAction)action_btnChkBox_6_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_6_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_SubFloors"];
    }
    else
    {
        
        [_btnChkBox_6_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_SubFloors"];
    }
    
}


- (IBAction)action_btnChkBox_7_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_7_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_SlabJoints"];
    }
    else
    {
        
        [_btnChkBox_7_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_SlabJoints"];
    }
    
}


- (IBAction)action_btnChkBox_8_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_8_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_CrawlSpace"];
    }
    else
    {
        
        [_btnChkBox_8_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_CrawlSpace"];
    }
    
}

- (IBAction)action_btnChkBox_9_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_9_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_SoilGradeTooHigh"];
    }
    else
    {
        
        [_btnChkBox_9_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_SoilGradeTooHigh"];
    }
    
}

- (IBAction)action_btnChkBox_10_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_10_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_HeavyFollage"];
    }
    else
    {
        
        [_btnChkBox_10_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_HeavyFollage"];
    }
    
}

- (IBAction)action_btnChkBox_11_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_11_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_Eaves"];
    }
    else
    {
        
        [_btnChkBox_11_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_Eaves"];
    }
    
}

- (IBAction)action_btnChkBox_12_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_12_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_Weepholes"];
    }
    else
    {
        
        [_btnChkBox_12_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_Weepholes"];
    }
    
}


- (IBAction)action_btnChkBox_13_6B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_13_6B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"Obstrected_Or_Inaccessible_Other"];
    }
    else
    {
        
        [_btnChkBox_13_6B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"Obstrected_Or_Inaccessible_Other"];
    }
    
}


- (IBAction)action_btnRadio_1_7A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_7A];
    
    if(isCompare==true)
    {
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"IsConduciveConditions"];
    }
    else
    {
        
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsConduciveConditions"];
        
    }
    
}


- (IBAction)action_btnRadio_2_7A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_7A];
    
    if(isCompare==true)
    {
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"IsConduciveConditions"];
    }
    else
    {
        
        [_btnRadio_2_7A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsConduciveConditions"];
    }
    
}


- (IBAction)action_btnChkBox_1_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_SlightCornerCracks"];
    }
    else
    {
        
        [_btnChkBox_1_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_SlightCornerCracks"];
    }
    
}


- (IBAction)action_btnChkBox_2_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_WoodtoGroundContact"];
    }
    else
    {
        
        [_btnChkBox_2_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_WoodtoGroundContact"];
    }
    
}


- (IBAction)action_btnChkBox_3_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_3_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_FormBoardsLeftInPlace"];
    }
    else
    {
        
        [_btnChkBox_3_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_FormBoardsLeftInPlace"];
    }
    
}


- (IBAction)action_btnChkBox_4_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_4_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_ExcessiveMoisture"];
    }
    else
    {
        
        [_btnChkBox_4_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_ExcessiveMoisture"];
    }
    
}


- (IBAction)action_btnChkBox_5_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_5_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_DebrisunderoraroundStructure"];
    }
    else
    {
        
        [_btnChkBox_5_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_DebrisunderoraroundStructure"];
    }
    
}


- (IBAction)action_btnChkBox_6_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_6_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_FootingTooLowOrSoilLineTooHigh"];
    }
    else
    {
        
        [_btnChkBox_6_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_FootingTooLowOrSoilLineTooHigh"];
    }
    
}

- (IBAction)action_btnChkBox_7_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_7_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_WoodRot"];
    }
    else
    {
        
        [_btnChkBox_7_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_WoodRot"];
    }
    
}

- (IBAction)action_btnChkBox_8_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_8_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_HeavyFollage"];
    }
    else
    {
        
        [_btnChkBox_8_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_HeavyFollage"];
    }
    
}

- (IBAction)action_btnChkBox_9_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_9_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_Plonterboxabuttingstructure"];
    }
    else
    {
        
        [_btnChkBox_9_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_Plonterboxabuttingstructure"];
    }
    
}

- (IBAction)action_btnChkBox_10_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_10_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_WoodPileInContactwithStructure"];
    }
    else
    {
        
        [_btnChkBox_10_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ConduciveConditions_WoodPileInContactwithStructure"];
    }
    
}

- (IBAction)action_btnChkBox_11_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_11_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_WoodFenceinContactwithStrucuture"];
    }
    else
    {
        
        [_btnChkBox_11_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_WoodFenceinContactwithStrucuture"];
    }
    
}

- (IBAction)action_btnChkBox_12_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_12_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_InsufficientVentillation"];
    }
    else
    {
        
        [_btnChkBox_12_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_InsufficientVentillation"];
    }
    
}

- (IBAction)action_btnChkBox_13_7B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_13_7B];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_Other"];
    }
    else
    {
        
        [_btnChkBox_13_7B setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ConduciveConditions_Other"];
    }
    
}


//===========================================
//===========================================
#pragma mark - View 6 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnRadio_1_8_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
}


- (IBAction)action_btnRadio_2_8_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_btnRadio_2_8_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
}


- (IBAction)action_btnRadio_1_8_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
}


- (IBAction)action_btnRadio_2_8_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_btnRadio_2_8_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        
    }
    
}


- (IBAction)action_btnRadio_1_8A_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8A_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8A_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8A_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8A_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8A_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8A_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"SubterraneanTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8A_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8A_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"SubterraneanTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8A_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_PreviousInfestation"];
    }
    
}

- (IBAction)action_btnRadio_1_8A_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8A_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"SubterraneanTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_PreviousTreatment"];
    }
    
}

- (IBAction)action_btnRadio_2_8A_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8A_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"SubterraneanTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_2_8A_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"SubterraneanTermites_PreviousTreatment"];
    }
    
}

- (IBAction)action_btnRadio_1_8B_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8B_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"DrywoodTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_ActiveInfestation"];
    }
    
}

- (IBAction)action_btnRadio_2_8B_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8B_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"DrywoodTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8B_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8B_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8B_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"DrywoodTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8B_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8B_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"DrywoodTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8B_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8B_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8B_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"DrywoodTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_PreviousTreatment"];
    }
    
}


- (IBAction)action_btnRadio_2_8B_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8B_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"DrywoodTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_2_8B_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DrywoodTermites_PreviousTreatment"];
        
    }
    
}


//===========================================
//===========================================
#pragma mark - View 7 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnRadio_1_8C_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8C_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"FormosanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8C_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8C_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"FormosanTermites_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8C_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8C_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8C_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"FormosanTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8C_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8C_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"FormosanTermites_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8C_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_PreviousInfestation"];
        
    }
    
}


- (IBAction)action_btnRadio_1_8C_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8C_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"FormosanTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_PreviousTreatment"];
    }
    
}


- (IBAction)action_btnRadio_2_8C_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8C_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"FormosanTermites_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_2_8C_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"FormosanTermites_PreviousTreatment"];
    }
    
}


- (IBAction)action_btnRadio_1_8D_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8D_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"CarpenterAnts_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8D_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8D_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"CarpenterAnts_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8D_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8D_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8D_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"CarpenterAnts_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8D_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8D_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"CarpenterAnts_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8D_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_PreviousInfestation"];
    }
    
}

- (IBAction)action_btnRadio_1_8D_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8D_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"CarpenterAnts_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_PreviousTreatment"];
    }
    
}

- (IBAction)action_btnRadio_2_8D_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8D_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"CarpenterAnts_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_2_8D_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CarpenterAnts_PreviousTreatment"];
    }
    
}

- (IBAction)action_btnRadio_1_8E_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8E_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"OtherWoodDestroyingInsects_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_ActiveInfestation"];
    }
    
}

- (IBAction)action_btnRadio_2_8E_ActiveInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8E_ActiveInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"OtherWoodDestroyingInsects_ActiveInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8E_ActiveInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_ActiveInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8E_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8E_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"OtherWoodDestroyingInsects_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_2_8E_PreviousInfestation:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8E_PreviousInfestation];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"OtherWoodDestroyingInsects_PreviousInfestation"];
    }
    else
    {
        
        [_btnRadio_2_8E_PreviousInfestation setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_PreviousInfestation"];
    }
    
}


- (IBAction)action_btnRadio_1_8E_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_8E_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"OtherWoodDestroyingInsects_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_PreviousTreatment"];
    }
    
}


- (IBAction)action_btnRadio_2_8E_PreviousTreatment:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_8E_PreviousTreatment];
    
    if(isCompare==true)
    {
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"OtherWoodDestroyingInsects_PreviousTreatment"];
    }
    else
    {
        
        [_btnRadio_2_8E_PreviousTreatment setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"OtherWoodDestroyingInsects_PreviousTreatment"];
    }
    
}



//===========================================
//===========================================
#pragma mark - View 8 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnRadio_1_9:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_9];
    
    if(isCompare==true)
    {
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"MechanicallyCorrectedByInspectingCompany"];
    }
    else
    {
        
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"MechanicallyCorrectedByInspectingCompany"];
    }
    
}


- (IBAction)action_btnRadio_2_9:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_9];
    
    if(isCompare==true)
    {
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"MechanicallyCorrectedByInspectingCompany"];
    }
    else
    {
        
        [_btnRadio_2_9 setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"MechanicallyCorrectedByInspectingCompany"];
    }
    
}


- (IBAction)action_btnRadio_1_9A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_9A];
    
    if(isCompare==true)
    {
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"CorrectiveTreatment"];
    }
    else
    {
        
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CorrectiveTreatment"];
    }
    
}


- (IBAction)action_btnRadio_2_9A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_9A];
    
    if(isCompare==true)
    {
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"CorrectiveTreatment"];
    }
    else
    {
        
        [_btnRadio_2_9A setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CorrectiveTreatment"];
        
    }
    
}


- (IBAction)action_btnRadio_1_9B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_9B];
    
    if(isCompare==true)
    {
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"IsPreventiveTreatment"];
    }
    else
    {
        
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsPreventiveTreatment"];
        
    }
    
}


- (IBAction)action_btnRadio_2_9B:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_9B];
    
    if(isCompare==true)
    {
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"IsPreventiveTreatment"];
    }
    else
    {
        
        [_btnRadio_2_9B setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"IsPreventiveTreatment"];
    }
    
}

//===========================================
//===========================================
#pragma mark - View 9 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnChkBox_1_10A_Subterranean:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_10A_Subterranean];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforSubterraneanTermites_Partial"];
    }
    else
    {
        
        [_btnChkBox_1_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforSubterraneanTermites_Partial"];
    }
    
}


- (IBAction)action_btnChkBox_2_10A_Subterranean:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_10A_Subterranean];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforSubterraneanTermites_Spot"];
    }
    else
    {
        
        [_btnChkBox_2_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforSubterraneanTermites_Spot"];
    }
    
}


- (IBAction)action_btnChkBox_3_10A_Subterranean:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_3_10A_Subterranean];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforSubterraneanTermites_Bait"];
    }
    else
    {
        
        [_btnChkBox_3_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforSubterraneanTermites_Bait"];
    }
    
}


- (IBAction)action_btnChkBox_4_10A_Subterranean:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_4_10A_Subterranean];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforSubterraneanTermites_Other"];
    }
    else
    {
        
        [_btnChkBox_4_10A_Subterranean setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforSubterraneanTermites_Other"];
    }
    
}


- (IBAction)action_btnChkBox_1_10A_Drywood:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_10A_Drywood];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforDrywoodTermites_Full"];
    }
    else
    {
        
        [_btnChkBox_1_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforDrywoodTermites_Full"];
    }
    
}


- (IBAction)action_btnChkBox_2_10A_Drywood:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_10A_Drywood];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"TreatingforDrywoodTermites_Limited"];
    }
    else
    {
        
        [_btnChkBox_2_10A_Drywood setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"TreatingforDrywoodTermites_Limited"];
    }
    
}


- (IBAction)action_btnRadio_1_10A_Contract:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_1_10A_Contract];
    
    if(isCompare==true)
    {
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"];
    }
    else
    {
        
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"];
    }
    
}


- (IBAction)action_btnRadio_2_10A_Contract:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImageRadio:_btnRadio_2_10A_Contract];
    
    if(isCompare==true)
    {
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
        [_btnRadio_1_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"];
    }
    else
    {
        
        [_btnRadio_2_10A_Contract setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"CompanyContractOrWarranty_WoodDestroyingInsects"];
        
    }
    
}

- (IBAction)action_btn_DateTreatmentByInspectionCompany:(id)sender {
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=109;
    [self addPickerViewDateTo :strDateTreatmentByInspectionCompany];
    
}

//===========================================
//===========================================
#pragma mark - View 10 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnBuyerIntials:(id)sender {
    yesEditedSomething=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strBuyersInitials";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}


- (IBAction)action_btnAddGraph:(id)sender {
    yesEditedSomething=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"firstGraphImage"];
    [defs setBool:NO forKey:@"servicegraph"];
    [defs setBool:YES forKey:@"termitegraph"];
    [defs setBool:YES forKey:@"isFromTermiteTexas"];// Akshay 20 May 2019
    [defs synchronize];
    
    // commented by Akshay 20 may 2019
    /* UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
     EditGraphViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"EditGraphViewController"];
     //    objSignViewController.strLeadId=strWorkOrderId;
     //    objSignViewController.strCompanyKey=strCompanyKey;
     //    objSignViewController.strUserName=strUserName;
     [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];*/
    
    
    // Akshay 20 may 2019 ///
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    GraphDrawingBoardViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"GraphDrawingBoardViewController"];
    objSignViewController.strLeadId=[matchesWorkOrder valueForKey:@"workorderId"];
    objSignViewController.strCompanyKey=strCompanyKey;
    objSignViewController.strUserName=strUserName;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    /////////////
}


- (IBAction)action_btnInspectorSignature:(id)sender {
    yesEditedSomething=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strInspSign";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

- (IBAction)action_ButtonAddGraphimg:(id)sender {
    yesEditedSomething=YES;
    UIImage *imageToCheckFor = [UIImage imageNamed:@"NoImage.jpg"];
    
    UIImage *img = [_btnAddGraphImg imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        
        
    }
    else
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        GraphImageTermiteViewController
        *objImagePreviewSalesAuto = [mainStoryboard instantiateViewControllerWithIdentifier:@"GraphImageTermiteViewController"];
        
        NSMutableArray *arrGraphImg=[[NSMutableArray alloc]initWithObjects:strGlobalGraphImage, nil];
        NSMutableArray *arrGraphImgCaption=[[NSMutableArray alloc]initWithObjects:strGraphImageCaption, nil];
        NSMutableArray *arrGraphImgDescription=[[NSMutableArray alloc]initWithObjects:strGraphImageDescription, nil];
        
        objImagePreviewSalesAuto.arrOfImages=arrGraphImg;
        objImagePreviewSalesAuto.indexOfImage=@"0";
        objImagePreviewSalesAuto.strLeadId=_strWorkOrder;
        objImagePreviewSalesAuto.strUserName=strUserName;
        objImagePreviewSalesAuto.strCompanyKey=strCompanyKey;
        objImagePreviewSalesAuto.arrOfImageCaptionsSaved=arrGraphImgCaption;
        objImagePreviewSalesAuto.arrOfImageDescriptionSaved=arrGraphImgDescription;
        objImagePreviewSalesAuto.strFromWhere=@"Service";
        objImagePreviewSalesAuto.statusOfWorkOrder=[NSString stringWithFormat:@"%@",strGlobalWorkOrderStatus];
        [self.navigationController presentViewController:objImagePreviewSalesAuto animated:YES completion:nil];
        
    }
    
}


//===========================================
//===========================================
#pragma mark - View 11 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnCertifiedApplicatorSign:(id)sender {
    yesEditedSomething=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strApplicatorSign";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}


- (IBAction)action_btnBuyerIntialsPurchaser:(id)sender {
    yesEditedSomething=YES;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strBuyersInitialsPurchaser";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}


- (IBAction)action_btnChkBox_1_12A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_1_12A];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"ElectricBreakerBox_12A"];
    }
    else
    {
        
        [_btnChkBox_1_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"ElectricBreakerBox_12A"];
    }
    
}


- (IBAction)action_btnChkBox_2_12A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_2_12A];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"WaterHeaterCloset_12A"];
    }
    else
    {
        
        [_btnChkBox_2_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"WaterHeaterCloset_12A"];
    }
    
}


- (IBAction)action_btnChkBox_3_12A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_3_12A];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"BathTrapAccess_12A"];
    }
    else
    {
        
        [_btnChkBox_3_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"BathTrapAccess_12A"];
    }
    
}


- (IBAction)action_btnChkBox_4_12A:(id)sender {
    yesEditedSomething=YES;
    BOOL isCompare =  [global checkImage:_btnChkBox_4_12A];
    
    if(isCompare==true)
    {
        // check_box_1.png
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_2.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"true" forKey:@"BeneathTheKitchenSink_12A"];
    }
    else
    {
        
        [_btnChkBox_4_12A setImage:[UIImage imageNamed:@"check_box_1.png"] forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"false" forKey:@"BeneathTheKitchenSink_12A"];
    }
    
}

- (IBAction)action_BtnDatePosted:(id)sender {
    yesEditedSomething=YES;
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=110;
    [self addPickerViewDateTo :strDatePosted];
    
}

//===========================================
//===========================================
#pragma mark - View 12 Action Methodss
//===========================================
//===========================================

- (IBAction)action_btnFinalSavenContinue:(id)sender {
    
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
        
        [self goToInvoiceView];
        
        
    }else{
        
        if ((strGlobalBuyersSign.length==0) || (strGlobalInspectorSign.length==0) || (strGlobalApplicatorSign.length==0) || (strGlobalBuyersSignPurchaser.length==0) || (strGlobalBuyersSign7B.length==0)) {
            
            [global AlertMethod:Alert :@"Please complete signatures to continue"];
            
        } else {
            
            if ([[dictGlobalTermiteTexas valueForKey:@"Graph_Caption"] isEqualToString:@"No Caption Available..!!"]) {
                
                [dictGlobalTermiteTexas setValue:@"" forKey:@"Graph_Caption"];
                
            }
            
            if ([[dictGlobalTermiteTexas valueForKey:@"Graph_Description"] isEqualToString:@"No Description Available..!!"]) {
                
                [dictGlobalTermiteTexas setValue:@"" forKey:@"Graph_Description"];
                
            }
            
            if (yesEditedSomething) {
                
                [self updateModifyDate];
                
            }
            
            [dictGlobalTermiteTexas setValue:[global strCurrentDate] forKey:@"ModifiedDate"];
            
            //Nilind
            
            //End
            
            
            NSError *errorNew = nil;
            NSData *json;
            NSString *jsonString;
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:dictGlobalTermiteTexas])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:dictGlobalTermiteTexas options:NSJSONWritingPrettyPrinted error:&errorNew];
                
                // If no errors, let's view the JSON
                if (json != nil && errorNew == nil)
                {
                    jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    NSLog(@"Final Service Automation JSON: %@", jsonString);
                }
            }
            
            NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dictDataTermite = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            [matchesWorkOrder setValue:dictDataTermite forKey:@"texasTermiteServiceDetail"];
            NSError *error1;
            [context1 save:&error1];
            
            [self goToInvoiceView];
            
        }
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
//============================================================================
//============================================================================

-(void)updateModifyDate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityModifyDateServiceAuto];
    
    // NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    // NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        
        NSString *strCurrentDateFormatted = [global modifyDateService];
        
        [matchesServiceModifyDate setValue:strCurrentDateFormatted forKey:@"modifyDate"];
        
        [global fetchWorkOrderFromDataBaseForServiceToUpdateModifydate:_strWorkOrder :strCurrentDateFormatted];
        
        //Final Save
        NSError *error;
        [context save:&error];
    }
}

-(void)goToInvoiceView{
    
    [self fetchWorkOrderFromDataBase];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                             bundle: nil];
    InvoiceAppointmentView
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InvoiceAppointmentView"];
    
    //    if (finalJsonDict.count==0) {
    //        if (arrResponseInspection.count==0) {
    //
    //        } else {
    //
    //            objByProductVC.dictJsonDynamicForm=[arrResponseInspection objectAtIndex:0];
    //
    //        }
    //
    //
    //    } else {
    //        objByProductVC.dictJsonDynamicForm=finalJsonDict;
    //
    //    }
    objByProductVC.workOrderDetail=matchesWorkOrderNew;
    //    objByProductVC.paymentInfoDetail=matchesPaymentInfo;
    //    objByProductVC.dictOfChemicalList=dictChemicalList;
    if ([strGlobalWorkOrderStatus isEqualToString:@"Complete"] || [strGlobalWorkOrderStatus isEqualToString:@"Completed"])
    {
        //        objByProductVC.arrOfChemicalListOther=arrOfWorkOrderOtherChemicalList;
        //        objByProductVC.arrOfChemicalList=arrOfWorkOrderProductDetails;
        //        objByProductVC.arrChemicalList=arrOfWorkOrderProductDetails;//[dictChemicalList valueForKey:@""];
        
    }
    else
    {
        
        //        objByProductVC.arrOfChemicalListOther=arrOfSelectedChemicalToSendToNextViewOther;
        //        objByProductVC.arrOfChemicalList=arrOfSelectedChemicalToSendToNextView;
        //        objByProductVC.arrChemicalList=arrOfSelectedChemicalToSendToNextView;//[dictChemicalList valueForKey:@""];
        
    }
    //    objByProductVC.strPaymentModes=strGlobalPaymentMode;
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    objByProductVC.strDepartmentIdd=[NSString stringWithFormat:@"%@",[defsLead valueForKey:@"strDepartmentId"]];
    ;
    //    if ([strGlobalPaymentMode isEqualToString:@"Check"]) {
    //        objByProductVC.strPaidAmount=_txtAmount.text;
    //
    //    } else {
    //        objByProductVC.strPaidAmount=_txtAmountSingle.text;
    //
    //    }
    //
    objByProductVC.arrAllObjImageDetail=arrAllObjImageDetail;
    [self saveImageTermiteToCoreData];
    [self.navigationController pushViewController:objByProductVC animated:NO];
    
}
-(void)fetchWorkOrderFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrder=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    self.fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [self.fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
    }
    else
    {
        matchesWorkOrderNew=arrAllObjWorkOrder[0];
    }
    
}


-(void)fetchImageDetailFromDataBase{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetail];
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderId,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        matchesImageDetail=arrAllObjImageDetail[0];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)downloadBuyersInitials :(NSString*)str :(UIImageView*)imgView{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowFirstImageTech :str :imgView];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowFirstImageTech :str :imgView];
                
            });
        });
        
    }
}


-(void)ShowFirstImageTech :(NSString*)str :(UIImageView*)imgView{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    imgView.image=[self loadImage:result];
    
}

- (void)saveImageAfterDownload1: (UIImage*)image :(NSString*)name {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}


- (IBAction)action_btnBuyersInitial_7B:(id)sender {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:@"fromCustService" forKey:@"signatureType"];
    [defs synchronize];
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    TermiteSignViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"TermiteSignViewController"];
    objSignViewController.strType=@"strBuyersInitials7B";
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

-(void)downloadGraph :(NSString*)str{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    
    //str=[str stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (fileExists) {
        [self ShowImageGraph :str :_btnAddGraphImg];
    } else {
        
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            // NSURL *url = [NSURL URLWithString:strNewString];
            strNewString = [strNewString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload1:image :result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self ShowImageGraph :str :_btnAddGraphImg];
                
            });
        });
        
    }
}

-(void)ShowImageGraph :(NSString*)str :(UIButton*)imgView{
    NSString *result;
    NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        result = [str substringFromIndex:equalRange.location + equalRange.length];
    }else{
        result=str;
    }
    if (result.length==0) {
        NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }
    }
    [imgView setImage:[self loadImage:result] forState:UIControlStateNormal];
    //imgView.image=[self loadImage:result];
    
}

//============================================================================
//============================================================================
#pragma mark- Text Field Delegate Methods
//============================================================================
//============================================================================

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
    leftView.backgroundColor = [UIColor clearColor];
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    yesEditedSomething=YES;
    
    if (textField==_txt_InspectedAddress) {
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectedAddress"];
        
    }else if (textField==_txt_City){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"City"];
        
    }else if (textField==_txt_ZipCode){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ZipCode"];
        
    }
    else if (textField==_txt_1A){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionCompanyName"];
        
    }
    else if (textField==_txt_1B){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"SPCSBuisnessLicenseNo"];
        
    }
    else if (textField==_txt_1C){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionCompanyAddress"];
        
    }
    else if (textField==_txt_ZipCodeView4){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionCompanyZipCode"];
        
    }
    else if (textField==_txt_CityView4){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionCompanyCity"];
        
    }
    else if (textField==_txt_State){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionCompanyState"];
        
    }
    else if (textField==_txt_TelephoneNo){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"TelephoneNo"];
        
    }
    else if (textField==_txt_1D){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectorName"];
        
    }
    else if (textField==_txt_2){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"CaseNumber"];
        
    }
    else if (textField==_txt_3){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionDate"];
        
    }
    else if (textField==_txt_4A_Other){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"PersonOther_PurchasingInspection"];
        
    }
    else if (textField==_txt_4B){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"Owner_Seller"];
        
    }
    else if (textField==_txt_5){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ScopeOfInspection_5"];
        
    }
    else if (textField==_txt_6BSpecify){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"Obstrected_Or_Inaccessible_Specify"];
        
    }
    else if (textField==_txt_7BSpecify){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ConduciveConditions_Specify"];
        
    }
    else if (textField==_txt_8E){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectionRevealsVisibleEvidence_Specify"];
        
    }
    else if (textField==_txt_8F){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ExplanationOfSignsIOfPreviousTreatment"];
        
    }
    else if (textField==_txt_8G){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"VisibleEvidenceOf"];
        
    }
    else if (textField==_txt_8GD){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"VisibleEvidenceObesrvedAreas"];
        
    }
    else if (textField==_txt_9){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"MechanicallyCorrectedByInspectingCompany_Specify"];
        
    }
    else if (textField==_txt_9BSpecifyReason){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"PreventiveTreatment_Specify"];
        
    }
    else if (textField==_txt_9B){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"InspectedAddress"];
        
    }
    else if (textField==_txt_10A){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"TreatingforWoodDestroyingInsects"];
        
    }
    else if (textField==_txt_DateOfTreatment){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"DateOFTreatment_InspectingCompany"];
        
    }
    else if (textField==_txt_CommonNameOfInsect){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"CommonNameOfInsect"];
        
    }
    else if (textField==_txt_NameOfPesticide){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"NameOfPesticide_Bait_Or_Other"];
        
    }
    else if (textField==_txt_ListInsects){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ListInsects"];
        
    }
    else if (textField==_txt_AdditionalComments){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"AdditionalComments_TexasOffiicial_WoodDestroyingInsect"];
        
    }
    else if (textField==_txt_12B){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"ListNoOfPages"];
        
    }
    else if (textField==_txt_ApplicatorLicenseNumber){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"CertifiedApplicator_LicenseNumber"];
        
    }
    else if (textField==_txt_Specifys_TexasOffiicial_WoodDestroyingInsect){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"Specifys_TexasOffiicial_WoodDestroyingInsect"];
        
    }
    else if (textField==_txt_PersonPurchasingInspection){
        
        [dictGlobalTermiteTexas setValue:textField.text forKey:@"PersonName_PurchasingInspection"];
        
    }
    
    [textField resignFirstResponder];
}


//============================================================================
//============================================================================
#pragma mark- ---------------------TABLE VIEW DELEGATE METHODS-----------------
//============================================================================
//============================================================================
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIScreen mainScreen].bounds.size.height==667) {
        return 50;
    }else if ([UIScreen mainScreen].bounds.size.height==736) {
        return 50;
    }
    else
        return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"cell";
    UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

- (IBAction)action_btn_DateOfPurchaserOfProperty:(id)sender {
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=111;
    [self addPickerViewDateTo :strDateOfPurchaserOfProperty];
    
}


//============================================================================
#pragma mark- BY NILIND
#pragma mark- ---------------------COLLECTION VIEW DELEGATE METHODS-----------------
//============================================================================
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView==_collectionView7A)
    {
        return arrNoImage.count;
    }
    else
    {
        return arrNoImageCollectionView2.count;
    }
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _collectionView7A)
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
            //str=[arrNoImage objectAtIndex:indexPath.row];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    else
    {
        static NSString *identifier = @"CellCollectionService";
        
        GeneralInfoImagesCollectionViewCell *cell = (GeneralInfoImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
            
            // str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        }
        else
        {
            str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound)
        {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            cell.imageBefore.image = [UIImage imageNamed:@"NoImage.jpg"];
            
        } else {
            
            cell.imageBefore.image = image;
        }
        
        
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView==_collectionView7A)
    {
        NSString *str ;
        
        NSDictionary *dictdat=[arrNoImage objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrNoImage objectAtIndex:indexPath.row];
            
        }
        
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewFor7A : strIndex];
            
        }
    }
    else
    {
        
        NSString *str;
        NSDictionary *dictdat=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrNoImageCollectionView2 objectAtIndex:indexPath.row];
            
        }
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        
        UIImage* image = [UIImage imageWithContentsOfFile:path];
        
        if (image==nil) {
            
            
            
        }else{
            
            NSString *strIndex=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [self goingToPreviewFor8 : strIndex];
            
        }
        
    }
}
-(void)goingToPreviewFor7A :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImage;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewTermiteViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewTermiteViewController"];
        objByProductVC.strForView=@"for7A";
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaptionFor7A;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescriptionFor7A;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}
-(void)goingToPreviewFor8 :(NSString*)indexxx
{
    
    NSMutableArray *arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    NSArray *arrOfImagesDetail=arrNoImageCollectionView2;
    
    arrOfBeforeImages=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrOfImagesDetail.count; k++) {
        
        if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dictData=arrOfImagesDetail[k];
            
            [arrOfBeforeImages addObject:dictData];
            
        }else{
            
            [arrOfBeforeImages addObject:arrOfImagesDetail[k]];
            
        }
        
    }
    
    arrOfImagesDetail=arrOfBeforeImages;
    
    if ([arrOfImagesDetail isKindOfClass:[NSString class]]) {
        [global AlertMethod:Info :NoBeforeImg];
    }else if (arrOfImagesDetail.count==0){
        [global AlertMethod:Info :NoBeforeImg];
    }
    else {
        NSMutableArray *arrOfImagess=[[NSMutableArray alloc]init];
        for (int k=0; k<arrOfImagesDetail.count; k++) {
            if ([arrOfImagesDetail[k] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict=arrOfImagesDetail[k];
                [arrOfImagess addObject:[dict valueForKey:@"woImagePath"]];
            }else{
                
                [arrOfImagess addObject:arrOfImagesDetail[k]];
                
            }
        }
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ImagePreviewTermiteViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ImagePreviewTermiteViewController"];
        objByProductVC.strForView=@"for8";
        
        objByProductVC.arrOfImages=arrOfImagess;
        objByProductVC.indexOfImage=indexxx;
        objByProductVC.arrOfImageCaptionsSaved=arrOfImageCaptionFor8;
        objByProductVC.statusOfWorkOrder=strGlobalWorkOrderStatus;
        objByProductVC.arrOfImageDescriptionSaved=arrOfImageDescriptionFor8;
        //  objByProductVC.dictOfWorkOrdersImagePreview=_dictOfWorkOrders;
        [self.navigationController presentViewController:objByProductVC animated:YES completion:nil];
    }
}

- (IBAction)actionOnAddImageForCollectionView7A:(id)sender
{
    _collectionView7A.tag=0;
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Capture New", @"Gallery", nil];
        
        [actionSheet showInView:self.view];
        
        
    }
}
-(IBAction)actionOnAddImageForCollectionView8:(id)sender
{
    _collectionView7A.tag=1;
    if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert!!" :@"Not allowed"];
    }
    else
    {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Make your selection"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Capture New", @"Gallery", nil];
        
        [actionSheet showInView:self.view];
        
        
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_collectionView7A.tag==0) //For View 7
    {
        if (buttonIndex == 0)
        {
            NSLog(@"The CApture Image.");
            
            
            if (arrNoImage.count<10)
            {
                NSLog(@"The CApture Image.");
                
                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Camera Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             } else {
                                                 
                                             }
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else
            {
                //chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            //............
            
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"The Gallery.");
            if (arrNoImage.count<10)
            {
                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             }
                                             else
                                             {
                                                 
                                                 
                                                 
                                             }
                                             
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                //chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    else  //For View 8
    {
        if (buttonIndex == 0)
        {
            NSLog(@"The CApture Image.");
            
            
            if (arrNoImageCollectionView2.count<10)
            {
                NSLog(@"The CApture Image.");
                
                BOOL isCameraPermissionAvailable=[global isCameraPermissionAvailable];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openCamera) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypeCamera;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Camera Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             } else {
                                                 
                                             }
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else
            {
                //chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            //............
            
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"The Gallery.");
            if (arrNoImageCollectionView2.count<10)
            {
                BOOL isCameraPermissionAvailable=[global isGalleryPermission];
                
                if (isCameraPermissionAvailable) {
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        
                        [self performSelector:@selector(openGalleryy) withObject:nil afterDelay:0.1];
                        
                    }else{
                        
                        UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
                        imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                        imagePickController.delegate=(id)self;
                        imagePickController.allowsEditing=TRUE;
                        [self presentViewController:imagePickController animated:YES completion:nil];
                        
                    }
                }else{
                    
                    UIAlertController *alert= [UIAlertController
                                               alertControllerWithTitle:@"Alert"
                                               message:@"Gallery Permission not allowed.Please go to settings and allow"
                                               preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              
                                              
                                              
                                          }];
                    [alert addAction:yes];
                    UIAlertAction* no = [UIAlertAction actionWithTitle:@"Go-To Settings" style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action)
                                         {
                                             
                                             if (/* DISABLES CODE */ (/* DISABLES CODE */ (&UIApplicationOpenSettingsURLString)) != NULL) {
                                                 NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                 [[UIApplication sharedApplication] openURL:url];
                                             }
                                             else
                                             {
                                                 
                                                 
                                                 
                                             }
                                             
                                         }];
                    [alert addAction:no];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                //chkForDuplicateImageSave=YES;
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Max 10 no. of images can be choosen" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
}
#pragma mark- ---------------------CAMERA DELEGATE METHOD---------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (_collectionView7A.tag==0)
    {
        yesEditedSomething=YES;
        NSLog(@"Database edited");
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        NSString  *strImageNamess =[NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];// [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
        //[NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime]
        [arrNoImage addObject:strImageNamess];
        UIImage *image = info[UIImagePickerControllerEditedImage];
        // [self saveImage:chosenImage :strImageNamess];
        
        //[self resizeImage:chosenImage :strImageNamess];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        //Lat long code
              
              CLLocationCoordinate2D coordinate = [global getLocation] ;
              NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
              NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
              [arrImageLattitudeFor7A addObject:latitude];
              [arrImageLongitudeFor7A addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption)
        {
            
            [self alertViewCustom];
            
        }
        else
        {
            
            [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
            [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
            
            [self saveImageTermiteToCoreData];
            
        }
      
        
        [self resizeImage:image :strImageNamess];
       // [_collectionView7A reloadData];
    }
    else
    {
        yesEditedSomething=YES;
        _collectionView8.tag=1;
        
        NSLog(@"Database edited");
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"backfromDynamicView"];
        [defs synchronize];
        
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateFormat:@"MMddyyyy"];
        [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HHmmss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *strTime = [formatter stringFromDate:[NSDate date]];
        //NSString  *strImageNamess = [NSString stringWithFormat:@"Img%@%@.jpg",strDate,strTime];
        NSString  *strImageNamess =[NSString stringWithFormat:@"\\Documents\\UploadImages\\Img%@%@.jpg",strDate,strTime];
        
        [arrNoImageCollectionView2 addObject:strImageNamess];
        UIImage *image = info[UIImagePickerControllerEditedImage];
        // [self saveImage:chosenImage :strImageNamess];
        
        //[self resizeImage:chosenImage :strImageNamess];
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        CLLocationCoordinate2D coordinate = [global getLocation] ;
               NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
               NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
               [arrImageLattitudeFor8 addObject:latitude];
               [arrImageLongitudeFor8 addObject:longitude];
        
        //imageCaption
        
        NSUserDefaults *defsCaption=[NSUserDefaults standardUserDefaults];
        
        BOOL yesImageCaption=[defsCaption boolForKey:@"imageCaptionSetting"];
        
        if (yesImageCaption)
        {
            
            [self alertViewCustom];
            
        } else {
            
            [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
            [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
            
            [self saveImageTermiteToCoreData];
            
        }
       
        
        [self resizeImage:image :strImageNamess];
        //[_collectionView8 reloadData];
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
-(void)alertViewCustom{
    
    
    viewBackAlertt=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackAlertt.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    // viewBackAlertt.alpha=0.5f;
    [self.view addSubview:viewBackAlertt];
    
    
    UIView *viewAlertt=[[UIView alloc]initWithFrame:CGRectMake(10, 85, [UIScreen mainScreen].bounds.size.width-20, 250)];
    viewAlertt.backgroundColor=[UIColor whiteColor];
    [viewBackAlertt addSubview:viewAlertt];
    
    
    viewAlertt.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    viewAlertt.layer.borderWidth=2.0;
    viewAlertt.layer.cornerRadius=5.0;
    
    UILabel *lblCaption=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewAlertt.bounds.size.width-20, 30)];
    
    lblCaption.text=@"Enter image caption below...";
    
    [viewAlertt addSubview:lblCaption];
    
    txtFieldCaption=[[UITextView alloc]initWithFrame:CGRectMake(10, lblCaption.frame.origin.y+35, viewAlertt.bounds.size.width-20, 50)];
    // txtFieldCaption.placeholder = @"Enter Caption Here...";
    txtFieldCaption.tag=7;
    txtFieldCaption.delegate=self;
    txtFieldCaption.textColor = [UIColor blackColor];
    txtFieldCaption.font=[UIFont systemFontOfSize:16];
    // txtFieldCaption.clearButtonMode = UITextFieldViewModeWhileEditing;
    // txtFieldCaption.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldCaption.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtFieldCaption];
    
    txtFieldCaption.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtFieldCaption.layer.borderWidth=1.0;
    txtFieldCaption.layer.cornerRadius=5.0;
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(10, txtFieldCaption.frame.origin.y+55, viewAlertt.bounds.size.width-20, 30)];
    
    lbl.text=@"Enter image description below...";
    
    [viewAlertt addSubview:lbl];
    
    txtViewImageDescription=[[UITextView alloc]initWithFrame:CGRectMake(10, lbl.frame.origin.y+35, viewAlertt.bounds.size.width-20, 100)];
    txtViewImageDescription.tag=8;
    txtViewImageDescription.delegate=self;
    txtViewImageDescription.font=[UIFont systemFontOfSize:16];
    txtViewImageDescription.textColor = [UIColor blackColor];
    txtViewImageDescription.keyboardType=UIKeyboardTypeDefault;
    [viewAlertt addSubview:txtViewImageDescription];
    
    txtViewImageDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    txtViewImageDescription.layer.borderWidth=1.0;
    txtViewImageDescription.layer.cornerRadius=5.0;
    
    UIButton *btnSave=[[UIButton alloc]initWithFrame:CGRectMake(viewAlertt.frame.origin.x+10, viewAlertt.frame.origin.y+255, viewAlertt.frame.size.width/2-20, 40)];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnSave];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnSave.frame.origin.x+btnSave.frame.size.width+10, btnSave.frame.origin.y, viewAlertt.frame.size.width/2-10,40)];
    [btnDone setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    [viewBackAlertt addSubview:btnDone];
    
    [txtFieldCaption becomeFirstResponder];
    
    [btnSave addTarget: self action: @selector(saveMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    [btnDone addTarget: self action: @selector(cancelMethodForCaption:)forControlEvents: UIControlEventTouchDown];
    
}
-(void)saveMethodForCaption :(id)sender
{
    if (_collectionView7A.tag==0)
    {
        if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
            
            if (txtFieldCaption.text.length>0) {
                
                [arrOfImageCaptionFor7A addObject:txtFieldCaption.text];
                
            } else {
                
                [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtViewImageDescription.text.length>0) {
                
                [arrOfImageDescriptionFor7A addObject:txtViewImageDescription.text];
                
            } else {
                
                [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
                
            }
            
            [viewBackAlertt removeFromSuperview];
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
            [self saveImageTermiteToCoreData];
            
        } else {
            
            [self AlertViewForImageCaption];
            
            //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }
    else
    {
        if ((txtFieldCaption.text.length>0) || (txtViewImageDescription.text.length>0)) {
            
            if (txtFieldCaption.text.length>0) {
                
                [arrOfImageCaptionFor8 addObject:txtFieldCaption.text];
                
            } else {
                
                [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
                
            }
            
            
            if (txtViewImageDescription.text.length>0) {
                
                [arrOfImageDescriptionFor8 addObject:txtViewImageDescription.text];
                
            } else {
                
                [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
                
            }
            
            [viewBackAlertt removeFromSuperview];
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
            [self saveImageTermiteToCoreData];
            
        } else {
            
            [self AlertViewForImageCaption];
            
            //[self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
            
        }
    }
    
}
-(void)cancelMethodForCaption :(id)sender
{
    
    if (_collectionView7A.tag==0)
    {
        [viewBackAlertt removeFromSuperview];
        
        [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
        [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
        [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
        [self saveImageTermiteToCoreData];
        
    }
    else
    {
        [viewBackAlertt removeFromSuperview];
        
        [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
        [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
        
        CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
        [_scrollVieww setContentOffset:bottomOffset animated:YES];
        
        [self saveImageTermiteToCoreData];
        
    }
    
}

-(void)alertToEnterImageCaption
{
    if (_collectionView7A.tag==0)
    {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Enter Caption Here...";
            textField.tag=7;
            textField.delegate=self;
            textField.textColor = [UIColor blackColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.keyboardType=UIKeyboardTypeDefault;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Enter Image Description Here...";
            textField.tag=8;
            textField.delegate=self;
            textField.textColor = [UIColor blackColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.keyboardType=UIKeyboardTypeDefault;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * txtHistoricalDays = textfields[0];
            UITextField * txtImageDescriptions = textfields[1];
            if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
                
                if (txtHistoricalDays.text.length>0) {
                    
                    [arrOfImageCaptionFor7A addObject:txtHistoricalDays.text];
                    
                } else {
                    
                    [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
                    
                }
                
                
                if (txtImageDescriptions.text.length>0) {
                    
                    [arrOfImageDescriptionFor7A addObject:txtImageDescriptions.text];
                    
                } else {
                    
                    [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
                    
                }
                
                CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
                [_scrollVieww setContentOffset:bottomOffset animated:YES];
                
                
            } else {
                
                [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
                
            }
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
            [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
            
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Image Caption"
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Enter Caption Here...";
            textField.tag=7;
            textField.delegate=self;
            textField.textColor = [UIColor blackColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.keyboardType=UIKeyboardTypeDefault;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Enter Image Description Here...";
            textField.tag=8;
            textField.delegate=self;
            textField.textColor = [UIColor blackColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.keyboardType=UIKeyboardTypeDefault;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * txtHistoricalDays = textfields[0];
            UITextField * txtImageDescriptions = textfields[1];
            if ((txtHistoricalDays.text.length>0) || (txtImageDescriptions.text.length>0)) {
                
                if (txtHistoricalDays.text.length>0) {
                    
                    [arrOfImageCaptionFor8 addObject:txtHistoricalDays.text];
                    
                } else {
                    
                    [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
                    
                }
                
                
                if (txtImageDescriptions.text.length>0) {
                    
                    [arrOfImageDescriptionFor8 addObject:txtImageDescriptions.text];
                    
                } else {
                    
                    [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
                    
                }
                
                CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
                [_scrollVieww setContentOffset:bottomOffset animated:YES];
                
                
            } else {
                
                [self performSelector:@selector(AlertViewForImageCaption) withObject:nil afterDelay:0.2];
                
            }
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
            [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
            
            CGPoint bottomOffset = CGPointMake(0, _scrollVieww.contentSize.height - _scrollVieww.bounds.size.height);
            [_scrollVieww setContentOffset:bottomOffset animated:YES];
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
    
}

-(void)AlertViewForImageCaption{
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Alert"
                               message:@"Please enter something to add"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                              // [self alertToEnterImageCaption];
                              
                          }];
    [alert addAction:yes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(UIImage *)resizeImage:(UIImage *)image :(NSString*)imageName
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 1000;//actualHeight/1.5;
    float maxWidth = 1000;//actualWidth/1.5;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    
    //NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    
    NSData *imageData = UIImageJPEGRepresentation([global drawText:@"Saavan" inImage:img], compressionQuality);
    
    UIGraphicsEndImageContext();
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",imageName]];
    [imageData writeToFile:path atomically:YES];
    
    return [UIImage imageWithData:imageData];
    
}

//============================================================================
#pragma mark- ---------------------FETCH IMAGE TERMITE FROM CORE DATA ---------------------
//============================================================================

-(void)fetchImageDetailFromDataBaseForSectionView7A_8
{
    // NSMutableArray *arrImageLongitude,*arrImageLattitude;
    // arrImageLongitude=[[NSMutableArray alloc]init];
    // arrImageLattitude=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetailsTermite];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfTermiteImages;
    arrOfTermiteImages=[[NSMutableArray alloc]init];
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObjImageDetail = [self.fetchedResultsControllerImageDetail fetchedObjects];
    if ([arrAllObjImageDetail count] == 0)
    {
        
    }
    else
    {
        for (int j=0; j<arrAllObjImageDetail.count; j++) {
            
            matchesImageDetail=arrAllObjImageDetail[j];
            NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
            
            if ([woImageType isEqualToString:@"TexasTermite_Section7A"])
            {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woTexasTermiteImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrNoImage addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionFor7A addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionFor7A addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionFor7A addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionFor7A addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitudeFor7A addObject:@""];
                    
                } else {
                    
                    [arrImageLattitudeFor7A addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitudeFor7A addObject:@""];
                    
                } else {
                    
                    [arrImageLongitudeFor7A addObject:strLong];
                    
                }
                
            }
            else if ([woImageType isEqualToString:@"TexasTermite_Section8"])
            {
                
                NSString *companyKey=[matchesImageDetail valueForKey:@"companyKey"];
                NSString *userName=[matchesImageDetail valueForKey:@"userName"];
                NSString *workorderId=[matchesImageDetail valueForKey:@"workorderId"];
                NSString *woImageId=[matchesImageDetail valueForKey:@"woTexasTermiteImageId"];
                NSString *woImagePath=[matchesImageDetail valueForKey:@"woImagePath"];
                NSString *woImageType=[matchesImageDetail valueForKey:@"woImageType"];
                NSString *createdDate=[matchesImageDetail valueForKey:@"createdDate"];
                NSString *createdBy=[matchesImageDetail valueForKey:@"createdBy"];
                NSString *modifiedDate=[matchesImageDetail valueForKey:@"modifiedDate"];
                NSString *modifiedBy=[matchesImageDetail valueForKey:@"modifiedBy"];
                NSString *lattitude=[matchesImageDetail valueForKey:@"latitude"];
                NSString *longitude=[matchesImageDetail valueForKey:@"longitude"];
                
                NSArray *objValueLeadInfo=[NSArray arrayWithObjects:
                                           companyKey.length==0 ? @"" : companyKey,
                                           userName.length==0 ? @"" : userName,
                                           workorderId.length==0 ? @"" : workorderId,
                                           woImageId.length==0 ? @"" : woImageId,
                                           woImagePath.length==0 ? @"" : woImagePath,
                                           woImageType.length==0 ? @"" : woImageType,
                                           createdDate.length==0 ? @"" : createdDate,
                                           createdBy.length==0 ? @"" : createdBy,
                                           modifiedDate.length==0 ? @"" : modifiedDate,
                                           modifiedBy.length==0 ? @"" : modifiedBy,
                                           lattitude.length==0 ? @"" : lattitude,
                                           longitude.length==0 ? @"" : longitude,nil];
                
                NSArray *objKeyLeadInfo=[NSArray arrayWithObjects:
                                         @"companyKey",
                                         @"userName",
                                         @"workorderId",
                                         @"woImageId",
                                         @"woImagePath",
                                         @"woImageType",
                                         @"createdDate",
                                         @"createdBy",
                                         @"modifiedDate",
                                         @"modifiedBy",
                                         @"latitude",
                                         @"longitude",nil];
                
                NSDictionary *dict_ToSendLeadInfo=[[NSDictionary alloc] initWithObjects:objValueLeadInfo forKeys:objKeyLeadInfo];
                
                [arrNoImageCollectionView2 addObject:dict_ToSendLeadInfo];
                //[arrOfImageCaption addObject:@"No Caption Available..!!"];
                
                NSString *strImageCaption=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageCaption"]];
                
                if ((strImageCaption.length==0) || [strImageCaption isEqualToString:@"(null)"]) {
                    
                    [arrOfImageCaptionFor8 addObject:@"No Caption Available..!!"];
                    
                } else {
                    
                    [arrOfImageCaptionFor8 addObject:strImageCaption];
                    
                }
                
                
                NSString *strImageDescription=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"imageDescription"]];
                
                if ((strImageDescription.length==0) || [strImageDescription isEqualToString:@"(null)"]) {
                    
                    [arrOfImageDescriptionFor8 addObject:@"No Description Available..!!"];
                    
                } else {
                    
                    [arrOfImageDescriptionFor8 addObject:strImageDescription];
                    
                }
                
                
                //Lat long
                
                NSString *strLat,*strLong;
                strLat=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"latitude"]];
                strLong=[NSString stringWithFormat:@"%@",[matchesImageDetail valueForKey:@"longitude"]];
                if ((strLat.length==0) || [strLat isEqualToString:@"(null)"]) {
                    
                    [arrImageLattitudeFor8 addObject:@""];
                    
                } else {
                    
                    [arrImageLattitudeFor8 addObject:strLat];
                    
                }
                if ((strLong.length==0) || [strLong isEqualToString:@"(null)"]) {
                    
                    [arrImageLongitudeFor8 addObject:@""];
                    
                }
                else
                {
                    
                    [arrImageLongitudeFor8 addObject:strLong];
                    
                }
                
            }
            [arrOfTermiteImages addObject:[matchesImageDetail valueForKey:@"woImagePath"]];
        }
    }
    [self downloadImages7ATermite:arrOfTermiteImages];
    
    // [self downloadImages7ATermite:arrNoImage];
    //[self downloadImages8Termite:arrNoImageCollectionView2];
    [_collectionView7A reloadData];
    [_collectionView8 reloadData];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    //[self downloadImagesGraphs:arrOfImagenameCollewctionView];
    
}
//============================================================================
#pragma mark- ------------CORE DATA TERMITE IMAGE SAVE------------------
//============================================================================
-(void)saveImageTermiteToCoreData
{
    //[self deleteBeforeImagesBeforeSaving];
    //Graph
    //[self deleteGraphImagesBeforeSaving];
    [self deleteImageTermiteFromCoreData];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    for (int k=0; k<arrNoImage.count; k++)
    {
        
        // Image Detail Entity
        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
        
        ImageDetailsTermite *objImageDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
        if ([arrNoImage[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrNoImage objectAtIndex:k]];
            objImageDetail.woImageType=@"TexasTermite_Section7A";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=_strWorkOrder;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor7A objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor7A objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor7A objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor7A objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
        else
        {
            
            NSDictionary *dictData=[arrNoImage objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.companyKey=strCompanyKey;
            objImageDetail.userName=strUserName;
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"TexasTermite_Section7A";
            objImageDetail.modifiedBy=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=_strWorkOrder;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor7A objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor7A objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor7A objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor7A objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    
    
    //For Section 7 Image
    
    for (int k=0; k<arrNoImageCollectionView2.count; k++)
    {
        // Image Detail Entity
        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
        ImageDetailsTermite *objImageDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
        
        if ([arrNoImageCollectionView2[k] isKindOfClass:[NSString class]])
        {
            
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[arrNoImageCollectionView2 objectAtIndex:k]];
            objImageDetail.woImageType=@"TexasTermite_Section8";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=_strWorkOrder;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor8 objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor8  objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            
            //Latlong code
            
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor8  objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor8  objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
        }
        else
        {
            NSDictionary *dictData=[arrNoImageCollectionView2 objectAtIndex:k];
            objImageDetail.createdBy=@"";
            objImageDetail.createdDate=@"";
            objImageDetail.imageDescription=@"";
            objImageDetail.woTexasTermiteImageId=@"";
            objImageDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"woImagePath"]];
            objImageDetail.woImageType=@"TexasTermite_Section8";
            objImageDetail.modifiedBy=@"";
            objImageDetail.modifiedDate=[global modifyDate];
            objImageDetail.workorderId=_strWorkOrder;
            objImageDetail.userName=strUserName;
            objImageDetail.companyKey=strCompanyKey;
            
            NSString *strImageCaptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageCaptionFor8 objectAtIndex:k]];
            if ([strImageCaptionToSet isEqualToString:@"No Caption Available..!!"]) {
                
                objImageDetail.imageCaption=@"";
                
            } else {
                
                objImageDetail.imageCaption=strImageCaptionToSet;
                
            }
            
            NSString *strImageDescriptionToSet=[NSString stringWithFormat:@"%@",[arrOfImageDescriptionFor8 objectAtIndex:k]];
            if ([strImageDescriptionToSet isEqualToString:@"No Description Available..!!"]) {
                
                objImageDetail.imageDescription=@"";
                
            } else {
                
                objImageDetail.imageDescription=strImageDescriptionToSet;
                
            }
            //Latlong code
            NSString *strlat=[NSString stringWithFormat:@"%@",[arrImageLattitudeFor8 objectAtIndex:k]];
            objImageDetail.latitude=strlat;
            NSString *strlong=[NSString stringWithFormat:@"%@",[arrImageLongitudeFor8 objectAtIndex:k]];
            objImageDetail.longitude=strlong;
            
            NSError *error1;
            [context save:&error1];
            
        }
    }
    [_collectionView7A reloadData];
    [_collectionView8 reloadData];
    //........................................................................
}
//============================================================================
#pragma mark- ------------DELETE CORE DATA TERMITE IMAGE ------------------
//============================================================================
-(void)deleteImageTermiteFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete ImageDetailsTermite
    
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    
    [allData setEntity:[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context]];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woImageType = %@",_strWorkOrder,@"Graph"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",_strWorkOrder];
    
    [allData setPredicate:predicate];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
//============================================================================
#pragma mark- ------------DOWNLOAD TERMITE IMAGE ------------------
//============================================================================

-(void)downloadImages7ATermite :(NSArray*)arrOfImagesDownload
{
    
    for (int k=0; k<arrOfImagesDownload.count; k++) {
        
        NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
        NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
        if([dictdat isKindOfClass:[NSDictionary class]])
        {
            str=[dictdat valueForKey:@"woImagePath"];
        }
        else
        {
            str=[arrOfImagesDownload objectAtIndex:k];
        }
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
        NSString *result;
        NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            result = [str substringFromIndex:equalRange.location + equalRange.length];
        }else{
            result=str;
        }
        if (result.length==0) {
            NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
            if (equalRange.location != NSNotFound) {
                result = [str substringFromIndex:equalRange.location + equalRange.length];
            }
        }
        // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (fileExists)
        {
        } else {
            
            /*dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
             dispatch_async(myQueue, ^{
             
             NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
             
             NSURL *photoURL = [NSURL URLWithString:strNewString];
             NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
             UIImage *image = [UIImage imageWithData:photoData];
             [self saveImageAfterDownload7A: image : result : k];
             
             dispatch_async(dispatch_get_main_queue(), ^{
             
             });
             
             });*/
            NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
            
            NSURL *photoURL = [NSURL URLWithString:strNewString];
            NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
            UIImage *image = [UIImage imageWithData:photoData];
            [self saveImageAfterDownload7A: image : result : k];
        }
    }
    //[_collectionView7A reloadData];
}
- (void)saveImageAfterDownload7A: (UIImage*)image :(NSString*)name :(int)indexx{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
    NSData  *Data = UIImagePNGRepresentation(image);
    [Data writeToFile:path atomically:YES];
    
}
/*
 -(void)downloadImages8Termite :(NSArray*)arrOfImagesDownload
 {
 for (int k=0; k<arrOfImagesDownload.count; k++) {
 
 NSString *str;//=[arrOfImagesDownload objectAtIndex:k];
 NSDictionary *dictdat=[arrOfImagesDownload objectAtIndex:k];
 if([dictdat isKindOfClass:[NSDictionary class]])
 {
 str=[dictdat valueForKey:@"woImagePath"];
 }
 else
 {
 str=[arrOfImagesDownload objectAtIndex:k];
 }
 
 NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
 NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
 NSString *strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
 NSString *result;
 NSRange equalRange = [str rangeOfString:@"/" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }else{
 result=str;
 }
 if (result.length==0) {
 NSRange equalRange = [str rangeOfString:@"\\" options:NSBackwardsSearch];
 if (equalRange.location != NSNotFound) {
 result = [str substringFromIndex:equalRange.location + equalRange.length];
 }
 }
 // NSString *strUrl = [NSString stringWithFormat:@"%@//Documents/UploadImages/%@",strServiceUrlMainServiceAutomation,str];
 
 NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,str];
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",result]];
 BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
 if (fileExists)
 {
 } else {
 
 dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
 dispatch_async(myQueue, ^{
 
 NSString *strNewString=[strUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
 
 NSURL *photoURL = [NSURL URLWithString:strNewString];
 NSData *photoData = [NSData dataWithContentsOfURL:photoURL];
 UIImage *image = [UIImage imageWithData:photoData];
 [self saveImageAfterDownload8: image : result : k];
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 });
 });
 }
 }
 [_collectionView8 reloadData];
 }
 - (void)saveImageAfterDownload8: (UIImage*)image :(NSString*)name :(int)indexx{
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString* path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@",name]];
 NSData  *Data = UIImagePNGRepresentation(image);
 [Data writeToFile:path atomically:YES];
 
 }*/
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    yesEditedSomething=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        [txtFieldCaption resignFirstResponder];
        [txtViewImageDescription resignFirstResponder];
        
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    return YES;
}
//Nilind 15 Dec
-(NSString *)getDateInDayFormat:(NSString*)dateString
{
    //Getting date from string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateString];
    // converting into our required date format
    [dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}
//Nilind 15 Dec
-(NSString *)currentDate
{
    //Getting date from string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [NSDate date];
    // converting into our required date format
    [dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    NSLog(@"date is %@", reqDateString);
    return reqDateString;
}
#pragma mark- CLOCK DATA FETCH

-(void)getClockStatus
{
    global = [[Global alloc] init];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        NSString *strClockStatus=[global fetchClockInOutDetailCoreData];
        
        if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
        {
            [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
        }
        else if ([strClockStatus isEqualToString:@"WorkingTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
            
        }
        else if ([strClockStatus isEqualToString:@"BreakTime"])
        {
            [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
            
        }
        else
        {
            [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
            
        }
        
    }
    else
    {
        //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSString *strClockStatus= [global getCurrentTimerOfClock];
                           NSLog(@"%@",strClockStatus);
                           
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               
                               if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
                               }
                               else if ([strClockStatus isEqualToString:@"WorkingTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
                                   
                               }
                               else if ([strClockStatus isEqualToString:@"BreakTime"])
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
                                   
                               }
                               else
                               {
                                   [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
                                   
                               }
                               
                               
                           });
                       });
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         NSString *strClockStatus= [global getCurrentTimerOfClock];
         NSLog(@"%@",strClockStatus);
         if ([strClockStatus isEqualToString:@"<null>"]||[strClockStatus isEqualToString:@"0"]||[strClockStatus isEqualToString:@""])
         {
         [_btnClock setImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
         }
         else if ([strClockStatus isEqualToString:@"WorkingTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"green"] forState:UIControlStateNormal];
         
         }
         else if ([strClockStatus isEqualToString:@"BreakTime"])
         {
         [_btnClock setImage:[UIImage imageNamed:@"orange"] forState:UIControlStateNormal];
         
         }
         else
         {
         [_btnClock setImage:[UIImage imageNamed:@"red"] forState:UIControlStateNormal];
         
         }
         
         
         });*/
    }
}
- (IBAction)actionOnClock:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        ClockInOutViewController
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"ClockInOutViewController"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}

// Akshay 8 may 2019
-(void)dismissPickerSheetAndRemoveDate:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
    NSInteger i=tblData.tag;
    if(i == 109)
    {
        [_btn_DateTreatmentByInspectionCompany setTitle:@"---Select Date---" forState:UIControlStateNormal];
        [dictGlobalTermiteTexas setValue:@"" forKey:@"DateOFTreatment_InspectingCompany"];
    }
}

@end
