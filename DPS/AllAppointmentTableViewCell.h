//
//  AllAppointmentTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 22/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  2021
//  Saavan Patidar
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface AllAppointmentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *sendSMS;
@property (strong, nonatomic) IBOutlet UIButton *sendEmail;
@property (strong, nonatomic) IBOutlet UIButton *sendMapView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_WorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ServiceName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Type;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Invoice;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_DAte;

@end
