//
//  FloridaTermiteServiceDetail+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 09/01/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface FloridaTermiteServiceDetail : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FloridaTermiteServiceDetail+CoreDataProperties.h"
