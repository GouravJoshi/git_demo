//
//  EditOpportunityVC.swift
//  DPS Test
//
//  Created by Akshay Hastekar on 26/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

// MARK: -----------------------------------Tableview Protocol-----------------------------------

//protocol refreshEditopportunity : class{
//
//    func refreshViewOnEditOpportunitySelection(dictData : NSDictionary ,tag : Int)
//
//}

// MARK:- -----------------------------------Tableview Protocol Delegate Methods -----------------------------------

extension EditOpportunityVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        if tag == 1 {
            
            //            btn_State.setTitle(dictData["Name"] as? String, for: .normal)
            //            strStateId = "\(dictData.value(forKeyPath: "StateId")!)"
            
        } else if tag == 2{
            
            btn_Urgency.setTitle(dictData["Name"] as? String, for: .normal)
            strUrgencyId = "\(dictData.value(forKeyPath: "UrgencyId")!)"
            
        }else if tag == 3{
            
            btn_PrimaryReasonForCall.setTitle(dictData["Name"] as? String, for: .normal)
            strPrimaryReasonForCallDepSysName = "\(dictData.value(forKeyPath: "DepartmentSysName")!)"
            strServiceCategoryID = "\(dictData.value(forKeyPath: "CategoryId")!)"
            
            btn_Service.setTitle("----Select----", for: .normal)
            
            
            
        }else if tag == 4{
            //SourceId
            let arySource = dictData.object(forKey: "source")as! NSArray
            print(arySource)
            var strName = ""
            arrSourceId = NSMutableArray()
            
            arrSelectedSource = NSMutableArray()
            
            arrSelectedSource = arySource.mutableCopy() as! NSMutableArray
            
            for item in arySource{
                strName = "\(strName),\((item as AnyObject).value(forKey: "Name")!)"
                arrSourceId.add("\((item as AnyObject).value(forKey: "SourceId")!)")
            }
            if strName == ""
            {
                strName = "----Select----"
            }
            if strName.count != 0{
                strName = String(strName.dropFirst())
            }
            btn_Source.setTitle(strName, for: .normal)
            print(arrSourceId)
            print(strName)
            
            
        }else if tag == 5{
            
            btn_Service.setTitle(dictData["Name"] as? String, for: .normal)
            strServiceId = "\(dictData.value(forKeyPath: "ServiceId")!)"
            
        }else if tag == 6{
            
            btn_OpportunityType.setTitle(dictData["Name"] as? String, for: .normal)
            strOpportunityType = "\(dictData.value(forKeyPath: "Name")!)"
            
        }else if tag == 7{
            
            btn_SubmittedBy.setTitle(dictData["FullName"] as? String, for: .normal)
            strSubmittedById = "\(dictData.value(forKeyPath: "EmployeeId")!)"
            
        }else if tag == 8{
            
            btn_CompanySIze.setTitle(dictData["Name"] as? String, for: .normal)
            strCompanySizeId = "\(dictData.value(forKeyPath: "SizeId")!)"
            
        }else if tag == 9{
            //IndustryId
            btn_Industry.setTitle(dictData["Name"] as? String, for: .normal)
            strIndustryId = "\(dictData.value(forKeyPath: "IndustryId")!)"
            
        }else if tag == 14{
            
            btn_InsideSalesPerson.setTitle(dictData["FullName"] as? String, for: .normal)
            strInsideSalesPersonId = "\(dictData.value(forKeyPath: "EmployeeId")!)"
            
        }else if tag == 17{
            
            if dictData.count == 0 {
                
                btn_SelectServiceAddress.setTitle("----Select----", for: .normal)
                
                // Setting values from Address
                
                txtFld_ServiceAddressLine1.text = ""
                txtFld_ServiceAddressLine2.text = ""
                txtFld_ServiceCity.text = ""
                txtFld_ServiceZipCode.text = ""
                txtFld_ServiceCounty.text = ""
                txtFld_ServiceSchoolDistrict.text = ""
                txtFld_ServiceMapCode.text = ""
                txtFld_ServiceGateCode.text = ""
                strStateIdService = ""
                strServiceLocationId = ""
                btn_ServiceState.setTitle("----Select----", for: .normal)
                
                
            }else{
                
                btn_SelectServiceAddress.setTitle(Global().strCombinedAddress((dictData as! [AnyHashable : Any])), for: .normal)
                
                // Setting values from Address
                
                txtFld_ServiceAddressLine1.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Address1")!)")
                txtFld_ServiceAddressLine2.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Address2")!)")
                txtFld_ServiceCity.text = checkNullValue(str: "\(dictData.value(forKeyPath: "CityName")!)")
                txtFld_ServiceZipCode.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Zipcode")!)")
                txtFld_ServiceCounty.text = checkNullValue(str: "\(dictData.value(forKeyPath: "County")!)")
                txtFld_ServiceSchoolDistrict.text = checkNullValue(str: "\(dictData.value(forKeyPath: "SchoolDistrict")!)")
                txtFld_ServiceMapCode.text = checkNullValue(str: "\(dictData.value(forKeyPath: "MapCode")!)")
                txtFld_ServiceGateCode.text = checkNullValue(str: "\(dictData.value(forKeyPath: "GateCode")!)")
                strStateIdService = checkNullValue(str: "\(dictData.value(forKeyPath: "StateId")!)")
                btn_ServiceState.setTitle(dictData["StateName"] as? String, for: .normal)
                
                strServiceLocationId = checkNullValue(str: "\(dictData.value(forKeyPath: "CustomerAddressId")!)")
            }
            
        }else if tag == 18{
            
            btn_ServiceState.setTitle(dictData["Name"] as? String, for: .normal)
            strStateIdService = "\(dictData.value(forKeyPath: "StateId")!)"
            
        }else if tag == 19{
            
            btn_ServiceAddressSubtype.setTitle(dictData["Name"] as? String, for: .normal)
            strServiceAddressSubType = "\(dictData.value(forKeyPath: "Name")!)"
            
        }else if tag == 20{
            
            btn_ServiceTaxcode.setTitle(dictData["Name"] as? String, for: .normal)
            strServiceTaxCode = "\(dictData.value(forKeyPath: "SysName")!)"
            
        }else if tag == 21{
            
            if dictData.count == 0{
                
                btn_SelectBillingAddress.setTitle("----Select----", for: .normal)
                
                // Setting values from Address
                
                txtFld_BillingAddressLine1.text = ""
                txtFld_BillingAddressLine2.text = ""
                txtFld_BillingCity.text = ""
                txtFld_BillingZipCode.text = ""
                txtFld_BillingCounty.text = ""
                txtFld_BillingSchoolDistrict.text = ""
                txtFld_BillingMapCode.text = ""
                strStateIdBilling = ""
                strBillingLocationId = ""
                btn_BillingState.setTitle("----Select----", for: .normal)
                
            }else {
                
                btn_SelectBillingAddress.setTitle(Global().strCombinedAddress((dictData as! [AnyHashable : Any])), for: .normal)
                
                // Setting values from Address
                
                txtFld_BillingAddressLine1.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Address1")!)")
                txtFld_BillingAddressLine2.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Address2")!)")
                txtFld_BillingCity.text = checkNullValue(str: "\(dictData.value(forKeyPath: "CityName")!)")
                txtFld_BillingZipCode.text = checkNullValue(str: "\(dictData.value(forKeyPath: "Zipcode")!)")
                txtFld_BillingCounty.text = checkNullValue(str: "\(dictData.value(forKeyPath: "County")!)")
                txtFld_BillingSchoolDistrict.text = checkNullValue(str: "\(dictData.value(forKeyPath: "SchoolDistrict")!)")
                txtFld_BillingMapCode.text = checkNullValue(str: "\(dictData.value(forKeyPath: "MapCode")!)")
                strStateIdBilling = checkNullValue(str: "\(dictData.value(forKeyPath: "StateId")!)")
                btn_BillingState.setTitle(dictData["StateName"] as? String, for: .normal)
                
                strBillingLocationId = checkNullValue(str: "\(dictData.value(forKeyPath: "CustomerAddressId")!)")
                
            }
            
        }else if tag == 22{
            
            btn_BillingState.setTitle(dictData["Name"] as? String, for: .normal)
            strStateIdBilling = "\(dictData.value(forKeyPath: "StateId")!)"
            
        }else if tag == 23{
            
            btnFieldSalesPerson.setTitle(dictData["FullName"] as? String, for: .normal)
            strFieldSalesPersonId = "\(dictData.value(forKeyPath: "EmployeeId")!)"
            
        }
    }
}

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

//protocol datePickerView : class{
//
//    func setDateOnSelction(strDate : String ,tag : Int)
//
//}
extension EditOpportunityVC: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if tag == 10 {
            
            btn_FollowUpDate.setTitle(strDate, for: .normal)
            strFollowUpDate = strDate
            
        } else if tag == 11 {
            
            btn_GrabbedDate.setTitle(strDate, for: .normal)
            
        }else if tag == 12 {
            
            btn_ExpectedClosingDate.setTitle(strDate, for: .normal)
            strExpectedClosingDate = strDate
            
            
        }else if tag == 13 {
            
            btn_SubmittedDate.setTitle(strDate, for: .normal)
            
        }else if tag == 16 {
            
            btn_Time.setTitle(strDate, for: .normal)
            
            
            
            let dateString = getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: strDate)
            
            if(dateString.count != 0)
            {
                strTime = dateString
            }
            
        }else if tag == 15 {
            
            btn_ScheduleDate.setTitle(strDate, for: .normal)
            strScheduleDate = strDate
        }
    }
}

/*extension EditOpportunityVC: datePickerView {
 
 func setDateOnSelction(strDate: String, tag: Int) {
 
 self.view.endEditing(true)
 
 if tag == 10 {
 
 btn_FollowUpDate.setTitle(strDate, for: .normal)
 strFollowUpDate = strDate
 
 } else if tag == 11 {
 
 btn_GrabbedDate.setTitle(strDate, for: .normal)
 
 }else if tag == 12 {
 
 btn_ExpectedClosingDate.setTitle(strDate, for: .normal)
 strExpectedClosingDate = strDate
 
 
 }else if tag == 13 {
 
 btn_SubmittedDate.setTitle(strDate, for: .normal)
 
 }else if tag == 16 {
 
 btn_Time.setTitle(strDate, for: .normal)
 
 
 
 let dateString = getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: strDate)
 
 if(dateString.count != 0)
 {
 strTime = dateString
 }
 
 }else if tag == 15 {
 
 btn_ScheduleDate.setTitle(strDate, for: .normal)
 strScheduleDate = strDate
 }
 }
 }*/
class EditOpportunityVC: UIViewController {
    
    // MARK: - -----------------------------------Global Variables-----------------------------------
    
    var strStateId = String()
    var strStateIdService = String()
    var strStateIdBilling = String()
    var strUrgencyId = String()
    var strServiceId = String()
    var strSourceId = String()
    var strSubmittedById = String()
    var strOpportunityType = String()
    var strIndustryId = String()
    var strPrimaryReasonForCallDepSysName = String()
    var strInsideSalesPersonId = String()
    var strFieldSalesPersonId = String()
    var strServiceAddressSubType = String()
    var strServiceTaxCode = String()
    var strCompanySizeId = String()
    var strBranchNameGlobal = String()
    var strDepartmentNameGlobal = String()
    var arrOfServiceAddress = NSMutableArray()
    var arrOfBillingAddress = NSMutableArray()
    var arrSourceIds = NSMutableArray()
    @objc var dictOpportunityDetail = NSDictionary()
    
    var strFollowUpDate = String()
    var strExpectedClosingDate = String()
    var strTime = String()
    var strScheduleDate = String()
    var strServiceaddressId = String()
    var strServiceLocationId = String()
    var strBillingLocationId = String()
    var arrSourceId = NSMutableArray()
    var strCRMContactId = String()
    var strEmployeeId = String()
    var strOpportunityId = String()
    var arrSelectedSource = NSMutableArray()
    var strServiceCategoryID = String()
    var dictSelectedServiceCategory = NSDictionary()
    var strFromWhere = String()
    // MARK: - -----------------------------------IBoutlets-----------------------------------
    
    @IBOutlet var view_CustomerInfo: UIView!
    @IBOutlet var view_AccountInfo: UIView!
    @IBOutlet var view_BillingAddress: UIView!
    @IBOutlet var view_ServiceAddress: UIView!
    @IBOutlet var view_OpportunityInfo: UIView!
    @IBOutlet var view_OpportunitySchedule: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var view_UpdateButon: UIView!
    @IBOutlet var btn_Country: UIButton!
    @IBOutlet var btn_State: UIButton!
    @IBOutlet var btn_Urgency: UIButton!
    @IBOutlet var btn_Service: UIButton!
    @IBOutlet var btn_OpportunityType: UIButton!
    @IBOutlet var btn_SubmittedBy: UIButton!
    @IBOutlet var btn_SalesAppDate: UIButton!
    @IBOutlet var btn_CompanySIze: UIButton!
    @IBOutlet var btn_Industry: UIButton!
    @IBOutlet var btn_InsideSalesPerson: UIButton!
    @IBOutlet var btn_ScheduleDate: UIButton!
    @IBOutlet var btn_FollowUpDate: UIButton!
    @IBOutlet var btn_GrabbedDate: UIButton!
    @IBOutlet var btn_SubmittedDate: UIButton!
    @IBOutlet var btn_ExpectedClosingDate: UIButton!
    @IBOutlet var btn_ScheduleOpportunity: UIButton!
    @IBOutlet var btn_SelectBillingAddress: UIButton!
    @IBOutlet var btn_BillingCountry: UIButton!
    @IBOutlet var btn_BillingState: UIButton!
    @IBOutlet var btn_SelectServiceAddress: UIButton!
    @IBOutlet var btn_ServiceCountry: UIButton!
    @IBOutlet var btn_ServiceState: UIButton!
    @IBOutlet var btn_Time: UIButton!
    @IBOutlet var txtView_TechNote: UITextView!
    @IBOutlet var btn_UpdateOpportunity: UIButton!
    @IBOutlet var btn_Source: UIButton!
    @IBOutlet var btn_PrimaryReasonForCall: UIButton!
    @IBOutlet var btn_ServiceAddressSubtype: UIButton!
    @IBOutlet var btn_ServiceTaxcode: UIButton!
    @IBOutlet var txtView_Description: UITextView!
    @IBOutlet var txtView_AccountAlert: UITextView!
    @IBOutlet var txtView_ServiceNotes: UITextView!
    @IBOutlet var txtView_ServiceDirection: UITextView!
    @IBOutlet var btnFieldSalesPerson: UIButton!
    @IBOutlet var view_BillingAddressTitle: UIView!
    @IBOutlet var btn_SameAsServiceAddress: UIButton!
    @IBOutlet var btn_IsTaxExempt: UIButton!
    @IBOutlet var btn_ScheduleNow: UIButton!
    @IBOutlet var txtFld_TaxExemption: ACFloatingTextField!
    @IBOutlet var lbl_OpportunityName: UILabel!
    @IBOutlet var lbl_AccountName: UILabel!
    @IBOutlet var lbl_CompanyName: UILabel!
    @IBOutlet var lbl_BranchName: UILabel!
    @IBOutlet var lbl_Status: UILabel!
    @IBOutlet var lbl_Stage: UILabel!
    @IBOutlet var txtFld_FN: ACFloatingTextField!
    @IBOutlet var txtFld_MN: ACFloatingTextField!
    @IBOutlet var txtFld_LN: ACFloatingTextField!
    @IBOutlet var txtFld_PrimaryPhone: ACFloatingTextField!
    @IBOutlet var txtFld_Cell: ACFloatingTextField!
    @IBOutlet var txtFld_SecondaryPhone: ACFloatingTextField!
    @IBOutlet var txtFld_PrimaryEmail: ACFloatingTextField!
    @IBOutlet var txtFld_SecondaryEmail: ACFloatingTextField!
    @IBOutlet var txtFld_BillingAddressLine1: ACFloatingTextField!
    @IBOutlet var txtFld_BillingAddressLine2: ACFloatingTextField!
    @IBOutlet var txtFld_BillingCity: ACFloatingTextField!
    @IBOutlet var txtFld_BillingZipCode: ACFloatingTextField!
    @IBOutlet var txtFld_BillingCounty: ACFloatingTextField!
    @IBOutlet var txtFld_BillingSchoolDistrict: ACFloatingTextField!
    @IBOutlet var txtFld_BillingMapCode: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceAddressLine1: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceAddressLine2: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceCity: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceZipCode: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceCounty: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceSchoolDistrict: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceMapCode: ACFloatingTextField!
    @IBOutlet var txtFld_ServiceGateCode: ACFloatingTextField!
    @IBOutlet var txtFld_ProjectedAmount: ACFloatingTextField!
    @IBOutlet var txtFld_ProposedAmount: ACFloatingTextField!
    @IBOutlet var txtFld_EstimatedDuration: ACFloatingTextField!
    @IBOutlet var txtFld_DriveTime: ACFloatingTextField!
    @IBOutlet weak var progressConfidenceLvel: UISlider!
    @IBOutlet weak var lblProgreeConfidenceValue: UILabel!
    
    // MARK: - -----------------------------------View Life Cycle-----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // do blank initially
        lbl_OpportunityName.text = ""
        lbl_AccountName.text = ""
        lbl_CompanyName.text = ""
        lbl_BranchName.text = ""
        lbl_Status.text = ""
        lbl_Stage.text = ""
        
        progressConfidenceLvel.tintColor = UIColor.theme()
        
        self.addViewsOnLoad()
        self.borderColor()
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
        
        btn_SameAsServiceAddress.setImage(UIImage(named: "check_box_1.png"), for: .normal)
        btn_ScheduleNow.setImage(UIImage(named: "check_box_1.png"), for: .normal)
        btn_IsTaxExempt.setImage(UIImage(named: "check_box_1.png"), for: .normal)
        txtFld_TaxExemption.isHidden = true
        
        /*
         let dict = dictOpportunityDetail.value(forKey: "Account") as! NSDictionary
         let leadData = dictOpportunityDetail.value(forKey: "LeadContact") as! NSDictionary
         strCRMContactId = "\(leadData.value(forKey: "CrmContactId")!)"
         let userDefault = UserDefaults.standard
         let dictLogin = userDefault.value(forKey: "LoginDetails") as! NSDictionary
         strEmployeeId = "\(dictLogin.value(forKey: "EmployeeId")!)"
         strOpportunityId = "\(dictOpportunityDetail.value(forKey: "LeadId")!)"
         strBranchNameGlobal = "\(dictOpportunityDetail.value(forKey: "BranchSysName")!)"
         self.getAddress(strAccountNo: dict.value(forKey: "AccountNo") as! String)
         */
        
        /*let allKey = dictOpportunityDetail.allKeys as NSArray
         let allValues = dictOpportunityDetail.allValues as NSArray
         let allKeyNew = NSMutableArray()
         for item in allKey
         {
         var str = item as! String
         str = str.lowerCasedFirstLetter()//lowercased()
         allKeyNew.add(str)
         }
         let dict_ToSend1 = NSDictionary.init(objects: allValues as! [Any], forKeys: allKeyNew as! [NSCopying])
         
         print(dict_ToSend1)
         print(dictOpportunityDetail)
         dictOpportunityDetail = dict_ToSend1*/
        
        var dict = NSDictionary()
        dict = getLowerCasedDictionary(dictDetail: dictOpportunityDetail)
        dictOpportunityDetail = dict
        print(dictOpportunityDetail)
        
        
        strCRMContactId = "\(dictOpportunityDetail.value(forKey: "crmContactId") ?? "")"
        let userDefault = UserDefaults.standard
        let dictLogin = userDefault.value(forKey: "LoginDetails") as! NSDictionary
        strEmployeeId = "\(dictLogin.value(forKey: "EmployeeId")!)"
        strOpportunityId = "\(dictOpportunityDetail.value(forKey: "opportunityId")!)"
        strBranchNameGlobal = "\(dictOpportunityDetail.value(forKey: "branchSysName")!)"
        self.getAddress(strAccountNo: "\(dictOpportunityDetail.value(forKey: "accountNo") ?? "")")
        
        
        // self.showValues()
    }
    
    // MARK: - -----------------------------------Actions-----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_State(_ sender: Any) {
        
        /*let testController = storyboardLeadProspect.instantiateViewController(withIdentifier: "EditOpportunityCRM_VC") as! EditOpportunityCRM_VC
        testController.dictOpportunity = dictOpportunityDetail
        self.present(testController, animated: false, completion: nil)*/

        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
            do {
                
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                let stateList = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: stateList , strTitle: "")
                
            } catch {
                
                Global().displayAlertController(Alert, NoDataAvailableee, self)
                
            }
        }
        
        
    }
    
    @IBAction func action_SelectBillingAddress(_ sender: Any) {
        
        if(arrOfBillingAddress.count > 1)
        {
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfBillingAddress , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_SelectBillingState(_ sender: Any) {
        
        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
            do {
                
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                let stateList = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: stateList , strTitle: "")
                
            } catch {
                
                Global().displayAlertController(Alert, NoDataAvailableee, self)
                
            }
        }
        
        
    }
    
    @IBAction func action_SelectServiceAddress(_ sender: Any) {
        
        if(arrOfServiceAddress.count > 1)
        {
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfServiceAddress , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_ServiceState(_ sender: Any) {
        
        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
            do {
                
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                let stateList = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: stateList , strTitle: "")
                
            } catch {
                
                Global().displayAlertController(Alert, NoDataAvailableee, self)
                
            }
        }
        
        
    }
    
    @IBAction func action_Urgency(_ sender: Any) {
            
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "UrgencyMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_OpportunityType(_ sender: Any) {
        
        let myArrayOfDict: NSMutableArray = [
            ["Name": "Residential"]
            ,   ["Name": "Commercial"]
        ]
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: myArrayOfDict , strTitle: "")
        
    }
    
    @IBAction func action_Service(_ sender: Any) {
        
        
        if(strPrimaryReasonForCallDepSysName.count != 0)
        {
            
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            // strDepartmentNameGlobal = "testdep"
            strDepartmentNameGlobal = strPrimaryReasonForCallDepSysName
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfFilteredData) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle:"Alert!" , strMessage: "Please select primary reason for call", viewcontrol: self)
        }
        
    }
    
    @IBAction func action_SubmittedBy(_ sender: Any) {
        
        // EmployeeList
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_SalesAppDate(_ sender: Any) {
    }
    
    @IBAction func action_Industry(_ sender: Any) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "IndustryMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_ScheduleDate(_ sender: UIButton)
    {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    
    @IBAction func action_GrabbedDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    
    @IBAction func action_ExpectedClosingDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    
    @IBAction func action_CompanySize(_ sender: Any) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "SizeMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_InsideSalesPerson(_ sender: Any) {
        
        // EmployeeList
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_FollowUpDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    
    @IBAction func action_SubmittedDate(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    
    @IBAction func action_Time(_ sender: UIButton) {
        
        self.gotoDatePickerView(sender: sender, strType: "Time")
        
    }
    
    @IBAction func action_ScheduleOpportunity(_ sender: Any) {
    }
    
    @IBAction func action_UpdateOpportunity(_ sender: Any) {
        
        if isInternetAvailable() == true
        {// call api to update opportunity
            trimTextField()
            let strMsg = validateFields()
            if(strMsg == "")
            {
                updateOpportunity()
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMsg, viewcontrol: self)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    @IBAction func action_Source(_ sender: UIButton) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
        let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender , aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_PrimaryReasonForCall(_ sender: Any) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictLeadDetailMaster.count != 0)
        {
            let arrOfData = (dictLeadDetailMaster.value(forKey: "Categories") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            //            strDepartmentNameGlobal = "testdep"
            strDepartmentNameGlobal = "\(dictOpportunityDetail.value(forKey: "DepartmentSysName")!)"
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfFilteredData) , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_ServiceAddressSubType(_ sender: Any) {
        
        let myArrayOfDict: NSMutableArray = [
            ["Name": "Residential"]
            ,   ["Name": "Commercial"]
        ]
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: myArrayOfDict , strTitle: "")
        
    }
    
    @IBAction func action_ServiceTaxCode(_ sender: Any) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
        //let strBranchName = Global().getBranchName(fromSysNam: strBranchNameGlobal)
        
        
        if(dictLeadDetailMaster.count != 0)
        {
            let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchNameGlobal ) , strTitle: "")
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
        
    }
    
    @IBAction func action_FieldSalesPerson(_ sender: Any) {
        
        // EmployeeList
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData) , strTitle: "")
        
    }
    
    @IBAction func action_SameAsServiceAddress(_ sender: UIButton) {
        
        if sender.currentImage ==  UIImage(named: "check_box_1.png") {
            
            btn_SameAsServiceAddress.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            
            self.sameAsServiceAddressChecked()
            
        } else {
            
            btn_SameAsServiceAddress.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            
            self.sameAsServiceAddressUnChecked()
            
        }
        
    }
    
    @IBAction func action_IsTaxExempt(_ sender: UIButton) {
        
        if sender.currentImage ==  UIImage(named: "check_box_1.png") {
            
            btn_IsTaxExempt.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            txtFld_TaxExemption.isHidden = false
            
        } else {
            
            btn_IsTaxExempt.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            txtFld_TaxExemption.isHidden = true
            
        }
        
    }
    
    @IBAction func action_ScheduleNow(_ sender: UIButton) {
        
        if sender.currentImage ==  UIImage(named: "check_box_1.png") {
            
            btn_ScheduleNow.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            
        } else {
            
            btn_ScheduleNow.setImage(UIImage(named: "check_box_1.png"), for: .normal)
            
        }
        
    }
    @IBAction func actionOnSliderProgress(_ sender: UISlider)
    {
        let currentValue = (Float(sender.value)) * 100
        
        let val = String(format: "%.00f",currentValue)
        
        lblProgreeConfidenceValue.text = "\(val)%"
        
    }
    
    
    // MARK: - -----------------------------------Functions-----------------------------------
    
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        vc.arySelectedItems = arrSelectedSource
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleSelectionOnEditOpportunity = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func addViewsOnLoad() {
        
        //Adding AccountInfoView
        
        let frameFor_view_AccountInfo = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: view_AccountInfo.frame.size.height)
        view_AccountInfo.frame = frameFor_view_AccountInfo
        scrollView.addSubview(view_AccountInfo)
        
        //Adding CustomerInfoView
        
        let frameFor_view_CustomerInfo = CGRect(x: 0, y: view_AccountInfo.frame.origin.y + view_AccountInfo.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_CustomerInfo.frame.size.height)
        view_CustomerInfo.frame = frameFor_view_CustomerInfo
        scrollView.addSubview(view_CustomerInfo)
        
        //Adding OpportunityInfoView
        
        let frameFor_view_OpportunityInfo = CGRect(x: 0, y: view_CustomerInfo.frame.origin.y + view_CustomerInfo.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_OpportunityInfo.frame.size.height)
        view_OpportunityInfo.frame = frameFor_view_OpportunityInfo
        scrollView.addSubview(view_OpportunityInfo)
        
        //Adding OpportunityScheduleInfoView
        
        let frameFor_view_OpportunityScheduleInfo = CGRect(x: 0, y: view_OpportunityInfo.frame.origin.y + view_OpportunityInfo.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_OpportunitySchedule.frame.size.height)
        view_OpportunitySchedule.frame = frameFor_view_OpportunityScheduleInfo
        scrollView.addSubview(view_OpportunitySchedule)
        
        //Adding ServiceAddressView
        
        let frameFor_view_ServiceAddress = CGRect(x: 0, y: view_OpportunitySchedule.frame.origin.y + view_OpportunitySchedule.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_ServiceAddress.frame.size.height)
        view_ServiceAddress.frame = frameFor_view_ServiceAddress
        scrollView.addSubview(view_ServiceAddress)
        
        //Adding BillingAddressView Title
        
        let frameFor_view_BillingAddressTitle = CGRect(x: 0, y: view_ServiceAddress.frame.origin.y + view_ServiceAddress.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_BillingAddressTitle.frame.size.height)
        view_BillingAddressTitle.frame = frameFor_view_BillingAddressTitle
        scrollView.addSubview(view_BillingAddressTitle)
        
        //Adding BillingAddressView
        
        let frameFor_view_BillingAddress = CGRect(x: 0, y: view_BillingAddressTitle.frame.origin.y + view_BillingAddressTitle.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_BillingAddress.frame.size.height)
        view_BillingAddress.frame = frameFor_view_BillingAddress
        scrollView.addSubview(view_BillingAddress)
        
        
        //Adding UpdateButton
        
        let frameFor_view_UpdateButton = CGRect(x: 0, y: view_BillingAddress.frame.origin.y + view_BillingAddress.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height)
        view_UpdateButon.frame = frameFor_view_UpdateButton
        scrollView.addSubview(view_UpdateButon)
        
        
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height + view_UpdateButon.frame.origin.y + 120)
        
    }
    
    func borderColor() {
        
        txtView_Description.layer.cornerRadius = 5.0
        txtView_Description.layer.borderColor = UIColor.theme()?.cgColor
        txtView_Description.layer.borderWidth = 0.8
        txtView_Description.layer.shadowColor = UIColor.theme()?.cgColor
        txtView_Description.layer.shadowOpacity = 0.3
        txtView_Description.layer.shadowRadius = 3.0
        txtView_Description.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        
        txtView_AccountAlert.layer.cornerRadius = 5.0
        txtView_AccountAlert.layer.borderColor = UIColor.theme()?.cgColor
        txtView_AccountAlert.layer.borderWidth = 0.8
        txtView_AccountAlert.layer.shadowColor = UIColor.theme()?.cgColor
        txtView_AccountAlert.layer.shadowOpacity = 0.3
        txtView_AccountAlert.layer.shadowRadius = 3.0
        txtView_AccountAlert.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        txtView_ServiceNotes.layer.cornerRadius = 5.0
        txtView_ServiceNotes.layer.borderColor = UIColor.theme()?.cgColor
        txtView_ServiceNotes.layer.borderWidth = 0.8
        txtView_ServiceNotes.layer.shadowColor = UIColor.theme()?.cgColor
        txtView_ServiceNotes.layer.shadowOpacity = 0.3
        txtView_ServiceNotes.layer.shadowRadius = 3.0
        txtView_ServiceNotes.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        txtView_ServiceDirection.layer.cornerRadius = 5.0
        txtView_ServiceDirection.layer.borderColor = UIColor.theme()?.cgColor
        txtView_ServiceDirection.layer.borderWidth = 0.8
        txtView_ServiceDirection.layer.shadowColor = UIColor.theme()?.cgColor
        txtView_ServiceDirection.layer.shadowOpacity = 0.3
        txtView_ServiceDirection.layer.shadowRadius = 3.0
        txtView_ServiceDirection.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        txtView_TechNote.layer.cornerRadius = 5.0
        txtView_TechNote.layer.borderColor = UIColor.theme()?.cgColor
        txtView_TechNote.layer.borderWidth = 0.8
        txtView_TechNote.layer.shadowColor = UIColor.theme()?.cgColor
        txtView_TechNote.layer.shadowOpacity = 0.3
        txtView_TechNote.layer.shadowRadius = 3.0
        txtView_TechNote.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        //        btn_Country.layer.cornerRadius = 5.0
        //        btn_Country.layer.borderColor = UIColor.theme()?.cgColor
        //        btn_Country.layer.borderWidth = 0.8
        //        btn_Country.layer.shadowColor = UIColor.theme()?.cgColor
        //        btn_Country.layer.shadowOpacity = 0.3
        //        btn_Country.layer.shadowRadius = 3.0
        //        btn_Country.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        //        btn_State.layer.cornerRadius = 5.0
        //        btn_State.layer.borderColor = UIColor.theme()?.cgColor
        //        btn_State.layer.borderWidth = 0.8
        //        btn_State.layer.shadowColor = UIColor.theme()?.cgColor
        //        btn_State.layer.shadowOpacity = 0.3
        //        btn_State.layer.shadowRadius = 3.0
        //        btn_State.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_Urgency.layer.cornerRadius = 5.0
        btn_Urgency.layer.borderColor = UIColor.theme()?.cgColor
        btn_Urgency.layer.borderWidth = 0.8
        btn_Urgency.layer.shadowColor = UIColor.theme()?.cgColor
        btn_Urgency.layer.shadowOpacity = 0.3
        btn_Urgency.layer.shadowRadius = 3.0
        btn_Urgency.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_Service.layer.cornerRadius = 5.0
        btn_Service.layer.borderColor = UIColor.theme()?.cgColor
        btn_Service.layer.borderWidth = 0.8
        btn_Service.layer.shadowColor = UIColor.theme()?.cgColor
        btn_Service.layer.shadowOpacity = 0.3
        btn_Service.layer.shadowRadius = 3.0
        btn_Service.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_OpportunityType.layer.cornerRadius = 5.0
        btn_OpportunityType.layer.borderColor = UIColor.theme()?.cgColor
        btn_OpportunityType.layer.borderWidth = 0.8
        btn_OpportunityType.layer.shadowColor = UIColor.theme()?.cgColor
        btn_OpportunityType.layer.shadowOpacity = 0.3
        btn_OpportunityType.layer.shadowRadius = 3.0
        btn_OpportunityType.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_SubmittedBy.layer.cornerRadius = 5.0
        btn_SubmittedBy.layer.borderColor = UIColor.theme()?.cgColor
        btn_SubmittedBy.layer.borderWidth = 0.8
        btn_SubmittedBy.layer.shadowColor = UIColor.theme()?.cgColor
        btn_SubmittedBy.layer.shadowOpacity = 0.3
        btn_SubmittedBy.layer.shadowRadius = 3.0
        btn_SubmittedBy.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        //        btn_SalesAppDate.layer.cornerRadius = 5.0
        //        btn_SalesAppDate.layer.borderColor = UIColor.theme()?.cgColor
        //        btn_SalesAppDate.layer.borderWidth = 0.8
        //        btn_SalesAppDate.layer.shadowColor = UIColor.theme()?.cgColor
        //        btn_SalesAppDate.layer.shadowOpacity = 0.3
        //        btn_SalesAppDate.layer.shadowRadius = 3.0
        //        btn_SalesAppDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_CompanySIze.layer.cornerRadius = 5.0
        btn_CompanySIze.layer.borderColor = UIColor.theme()?.cgColor
        btn_CompanySIze.layer.borderWidth = 0.8
        btn_CompanySIze.layer.shadowColor = UIColor.theme()?.cgColor
        btn_CompanySIze.layer.shadowOpacity = 0.3
        btn_CompanySIze.layer.shadowRadius = 3.0
        btn_CompanySIze.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_Industry.layer.cornerRadius = 5.0
        btn_Industry.layer.borderColor = UIColor.theme()?.cgColor
        btn_Industry.layer.borderWidth = 0.8
        btn_Industry.layer.shadowColor = UIColor.theme()?.cgColor
        btn_Industry.layer.shadowOpacity = 0.3
        btn_Industry.layer.shadowRadius = 3.0
        btn_Industry.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_InsideSalesPerson.layer.cornerRadius = 5.0
        btn_InsideSalesPerson.layer.borderColor = UIColor.theme()?.cgColor
        btn_InsideSalesPerson.layer.borderWidth = 0.8
        btn_InsideSalesPerson.layer.shadowColor = UIColor.theme()?.cgColor
        btn_InsideSalesPerson.layer.shadowOpacity = 0.3
        btn_InsideSalesPerson.layer.shadowRadius = 3.0
        btn_InsideSalesPerson.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ScheduleDate.layer.cornerRadius = 5.0
        btn_ScheduleDate.layer.borderColor = UIColor.theme()?.cgColor
        btn_ScheduleDate.layer.borderWidth = 0.8
        btn_ScheduleDate.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ScheduleDate.layer.shadowOpacity = 0.3
        btn_ScheduleDate.layer.shadowRadius = 3.0
        btn_ScheduleDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_FollowUpDate.layer.cornerRadius = 5.0
        btn_FollowUpDate.layer.borderColor = UIColor.theme()?.cgColor
        btn_FollowUpDate.layer.borderWidth = 0.8
        btn_FollowUpDate.layer.shadowColor = UIColor.theme()?.cgColor
        btn_FollowUpDate.layer.shadowOpacity = 0.3
        btn_FollowUpDate.layer.shadowRadius = 3.0
        btn_FollowUpDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_GrabbedDate.layer.cornerRadius = 5.0
        btn_GrabbedDate.layer.borderColor = UIColor.theme()?.cgColor
        btn_GrabbedDate.layer.borderWidth = 0.8
        btn_GrabbedDate.layer.shadowColor = UIColor.theme()?.cgColor
        btn_GrabbedDate.layer.shadowOpacity = 0.3
        btn_GrabbedDate.layer.shadowRadius = 3.0
        btn_GrabbedDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_SubmittedDate.layer.cornerRadius = 5.0
        btn_SubmittedDate.layer.borderColor = UIColor.theme()?.cgColor
        btn_SubmittedDate.layer.borderWidth = 0.8
        btn_SubmittedDate.layer.shadowColor = UIColor.theme()?.cgColor
        btn_SubmittedDate.layer.shadowOpacity = 0.3
        btn_SubmittedDate.layer.shadowRadius = 3.0
        btn_SubmittedDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ExpectedClosingDate.layer.cornerRadius = 5.0
        btn_ExpectedClosingDate.layer.borderColor = UIColor.theme()?.cgColor
        btn_ExpectedClosingDate.layer.borderWidth = 0.8
        btn_ExpectedClosingDate.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ExpectedClosingDate.layer.shadowOpacity = 0.3
        btn_ExpectedClosingDate.layer.shadowRadius = 3.0
        btn_ExpectedClosingDate.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_Time.layer.cornerRadius = 5.0
        btn_Time.layer.borderColor = UIColor.theme()?.cgColor
        btn_Time.layer.borderWidth = 0.8
        btn_Time.layer.shadowColor = UIColor.theme()?.cgColor
        btn_Time.layer.shadowOpacity = 0.3
        btn_Time.layer.shadowRadius = 3.0
        btn_Time.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        //        btn_ScheduleOpportunity.layer.cornerRadius = 5.0
        //        btn_ScheduleOpportunity.layer.borderColor = UIColor.themeColorBlack()?.cgColor
        //        btn_ScheduleOpportunity.layer.borderWidth = 0.8
        //        btn_ScheduleOpportunity.layer.shadowColor = UIColor.themeColorBlack()?.cgColor
        //        btn_ScheduleOpportunity.layer.shadowOpacity = 0.3
        //        btn_ScheduleOpportunity.layer.shadowRadius = 3.0
        //        btn_ScheduleOpportunity.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_SelectBillingAddress.layer.cornerRadius = 5.0
        btn_SelectBillingAddress.layer.borderColor = UIColor.theme()?.cgColor
        btn_SelectBillingAddress.layer.borderWidth = 0.8
        btn_SelectBillingAddress.layer.shadowColor = UIColor.theme()?.cgColor
        btn_SelectBillingAddress.layer.shadowOpacity = 0.3
        btn_SelectBillingAddress.layer.shadowRadius = 3.0
        btn_SelectBillingAddress.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_SelectServiceAddress.layer.cornerRadius = 5.0
        btn_SelectServiceAddress.layer.borderColor = UIColor.theme()?.cgColor
        btn_SelectServiceAddress.layer.borderWidth = 0.8
        btn_SelectServiceAddress.layer.shadowColor = UIColor.theme()?.cgColor
        btn_SelectServiceAddress.layer.shadowOpacity = 0.3
        btn_SelectServiceAddress.layer.shadowRadius = 3.0
        btn_SelectServiceAddress.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_BillingCountry.layer.cornerRadius = 5.0
        btn_BillingCountry.layer.borderColor = UIColor.theme()?.cgColor
        btn_BillingCountry.layer.borderWidth = 0.8
        btn_BillingCountry.layer.shadowColor = UIColor.theme()?.cgColor
        btn_BillingCountry.layer.shadowOpacity = 0.3
        btn_BillingCountry.layer.shadowRadius = 3.0
        btn_BillingCountry.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ServiceCountry.layer.cornerRadius = 5.0
        btn_ServiceCountry.layer.borderColor = UIColor.theme()?.cgColor
        btn_ServiceCountry.layer.borderWidth = 0.8
        btn_ServiceCountry.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ServiceCountry.layer.shadowOpacity = 0.3
        btn_ServiceCountry.layer.shadowRadius = 3.0
        btn_ServiceCountry.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_BillingState.layer.cornerRadius = 5.0
        btn_BillingState.layer.borderColor = UIColor.theme()?.cgColor
        btn_BillingState.layer.borderWidth = 0.8
        btn_BillingState.layer.shadowColor = UIColor.theme()?.cgColor
        btn_BillingState.layer.shadowOpacity = 0.3
        btn_BillingState.layer.shadowRadius = 3.0
        btn_BillingState.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ServiceState.layer.cornerRadius = 5.0
        btn_ServiceState.layer.borderColor = UIColor.theme()?.cgColor
        btn_ServiceState.layer.borderWidth = 0.8
        btn_ServiceState.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ServiceState.layer.shadowOpacity = 0.3
        btn_ServiceState.layer.shadowRadius = 3.0
        btn_ServiceState.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_UpdateOpportunity.layer.cornerRadius = 5.0
        btn_UpdateOpportunity.layer.borderColor = UIColor.themeColorBlack()?.cgColor
        btn_UpdateOpportunity.layer.borderWidth = 0.8
        btn_UpdateOpportunity.layer.shadowColor = UIColor.themeColorBlack()?.cgColor
        btn_UpdateOpportunity.layer.shadowOpacity = 0.3
        btn_UpdateOpportunity.layer.shadowRadius = 3.0
        btn_UpdateOpportunity.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_Source.layer.cornerRadius = 5.0
        btn_Source.layer.borderColor = UIColor.theme()?.cgColor
        btn_Source.layer.borderWidth = 0.8
        btn_Source.layer.shadowColor = UIColor.theme()?.cgColor
        btn_Source.layer.shadowOpacity = 0.3
        btn_Source.layer.shadowRadius = 3.0
        btn_Source.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_PrimaryReasonForCall.layer.cornerRadius = 5.0
        btn_PrimaryReasonForCall.layer.borderColor = UIColor.theme()?.cgColor
        btn_PrimaryReasonForCall.layer.borderWidth = 0.8
        btn_PrimaryReasonForCall.layer.shadowColor = UIColor.theme()?.cgColor
        btn_PrimaryReasonForCall.layer.shadowOpacity = 0.3
        btn_PrimaryReasonForCall.layer.shadowRadius = 3.0
        btn_PrimaryReasonForCall.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ServiceAddressSubtype.layer.cornerRadius = 5.0
        btn_ServiceAddressSubtype.layer.borderColor = UIColor.theme()?.cgColor
        btn_ServiceAddressSubtype.layer.borderWidth = 0.8
        btn_ServiceAddressSubtype.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ServiceAddressSubtype.layer.shadowOpacity = 0.3
        btn_ServiceAddressSubtype.layer.shadowRadius = 3.0
        btn_ServiceAddressSubtype.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btn_ServiceTaxcode.layer.cornerRadius = 5.0
        btn_ServiceTaxcode.layer.borderColor = UIColor.theme()?.cgColor
        btn_ServiceTaxcode.layer.borderWidth = 0.8
        btn_ServiceTaxcode.layer.shadowColor = UIColor.theme()?.cgColor
        btn_ServiceTaxcode.layer.shadowOpacity = 0.3
        btn_ServiceTaxcode.layer.shadowRadius = 3.0
        btn_ServiceTaxcode.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        btnFieldSalesPerson.layer.cornerRadius = 5.0
        btnFieldSalesPerson.layer.borderColor = UIColor.theme()?.cgColor
        btnFieldSalesPerson.layer.borderWidth = 0.8
        btnFieldSalesPerson.layer.shadowColor = UIColor.theme()?.cgColor
        btnFieldSalesPerson.layer.shadowOpacity = 0.3
        btnFieldSalesPerson.layer.shadowRadius = 3.0
        btnFieldSalesPerson.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
    }
    
    func getAddress(strAccountNo : String) {
        
        let defsLogindDetail = UserDefaults.standard
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") as? String
        var strCompanyKey: String? = nil
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            strCompanyKey = "\(value)"
        }
        
        var strUrl = ""
        
        strUrl = "\(strUrlMain ?? "")\(UrlGetServiceAddressesByAccountNoNew)\(strAccountNo)\(UrlGetActivityListcompanyKey)\(strCompanyKey ?? "")"
        
        //DejalBezelActivityView.init(for: self.view, withLabel: "Fetching Address...", width: 200)
        
        FTIndicator.showProgress(withMessage: "Loading.....", userInteractionEnable: false)
        
        WebService.callAPIBYGET(parameter: NSDictionary(), url: strUrl) { (Response, Status) in
            
            
            DispatchQueue.main.async{
                FTIndicator.dismissProgress()
                
            }
            if(Status){
                
                
                // ServiceAddresses
                
                let dictAddress = Response["data"] as! NSDictionary
                
                self.arrOfServiceAddress.add("----Select----")
                self.arrOfBillingAddress.add("----Select----")
                
                self.arrOfServiceAddress.addObjects(from: (dictAddress.value(forKey: "ServiceAddresses") as! [Any]))
                
                self.arrOfBillingAddress.addObjects(from: (dictAddress.value(forKey: "BillingAddresses") as! [Any]))
                
                self.showValues()
                
            }
            else
            {
                self.showValues()
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, try again later.", viewcontrol: self)
            }
            
        }
        
    }
    
    func sameAsServiceAddressChecked() {
        
        view_BillingAddress.removeFromSuperview()
        
        //Adding BillingAddressView Title
        
        let frameFor_view_BillingAddressTitle = CGRect(x: 0, y: view_ServiceAddress.frame.origin.y + view_ServiceAddress.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_BillingAddressTitle.frame.size.height)
        view_BillingAddressTitle.frame = frameFor_view_BillingAddressTitle
        scrollView.addSubview(view_BillingAddressTitle)
        
        //Adding UpdateButton
        
        let frameFor_view_UpdateButton = CGRect(x: 0, y: view_BillingAddressTitle.frame.origin.y + view_BillingAddressTitle.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height)
        view_UpdateButon.frame = frameFor_view_UpdateButton
        //scrollView.addSubview(view_UpdateButon)
        
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height + view_UpdateButon.frame.origin.y + 120)
        
    }
    
    func sameAsServiceAddressUnChecked() {
        
        //Adding BillingAddressView
        
        let frameFor_view_BillingAddress = CGRect(x: 0, y: view_BillingAddressTitle.frame.origin.y + view_BillingAddressTitle.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_BillingAddress.frame.size.height)
        view_BillingAddress.frame = frameFor_view_BillingAddress
        scrollView.addSubview(view_BillingAddress)
        
        //Adding UpdateButton
        
        let frameFor_view_UpdateButton = CGRect(x: 0, y: view_BillingAddress.frame.origin.y + view_BillingAddress.frame.size.height, width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height)
        view_UpdateButon.frame = frameFor_view_UpdateButton
        //scrollView.addSubview(view_UpdateButon)
        
        
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: view_UpdateButon.frame.size.height + view_UpdateButon.frame.origin.y + 120)
        
    }
    
    func showValues() {
        
        lbl_OpportunityName.text = "\(dictOpportunityDetail.value(forKey: "opportunityName")!)(# : \(dictOpportunityDetail.value(forKey: "opportunityNumber")!))"
        
        lbl_AccountName.text = "Account # : \((dictOpportunityDetail.value(forKey: "accountNo") ?? ""))"
        txtView_AccountAlert.text = "\(dictOpportunityDetail.value(forKey: "opportunityDescription") ?? "")"
        
        
        
        if dictOpportunityDetail.value(forKey: "accountCompany") is NSDictionary {
            
            let dict = dictOpportunityDetail.value(forKey: "accountCompany") as! NSDictionary
            
            lbl_CompanyName.text = "Company : \(dict.value(forKey: "Name")!)"
        }
        // showing branch name
        
        if(strBranchNameGlobal.count != 0)
        {
            lbl_BranchName.text = "Branch : "+Global().getBranchName(fromSysNam: strBranchNameGlobal)
        }
        else
        {
            lbl_BranchName.text = "Branch : "+"N/A"
        }
        
        lbl_Stage.text = "Stage : \(dictOpportunityDetail.value(forKeyPath: "opportunityStage")!)"
        
        lbl_Status.text = "Status : \(dictOpportunityDetail.value(forKeyPath: "opportunityStatus")!)"
        
        if( lbl_Status.text == "Complete")
        {
            btn_ScheduleNow.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            btn_ScheduleNow.isUserInteractionEnabled = false
        }
        
        txtFld_ProposedAmount.isUserInteractionEnabled = false
        btn_GrabbedDate.isUserInteractionEnabled = false
        btn_SubmittedDate.isUserInteractionEnabled = false
        txtView_TechNote.isUserInteractionEnabled = false
        
        
        txtFld_FN.text = "\(dictOpportunityDetail.value(forKey: "firstName") ?? "")"
        txtFld_MN.text = "\(dictOpportunityDetail.value(forKey: "middleName")!)"
        txtFld_LN.text = "\(dictOpportunityDetail.value(forKey: "lastName") ?? "")"
        txtFld_PrimaryPhone.text = "\(dictOpportunityDetail.value(forKey: "primaryPhone") ?? "")"
        txtFld_PrimaryEmail.text = Global().strReplacedEmail("\(dictOpportunityDetail.value(forKey: "primaryEmail") ?? "" )")
        txtFld_SecondaryEmail.text = Global().strReplacedEmail("\(dictOpportunityDetail.value(forKey: "secondaryEmail") ?? "")")
        txtFld_SecondaryPhone.text = "\(dictOpportunityDetail.value(forKey: "secondaryPhone") ?? "")"
        txtFld_Cell.text = "\(dictOpportunityDetail.value(forKey: "cell") ?? "")"
        
        
        // showing Opportunity Schedule
        if(("\(dictOpportunityDetail.value(forKey: "opportunityStatus")!)" == "Open") && "\(dictOpportunityDetail.value(forKey: "opportunityStage")!)" == "Scheduled")
        {
            btn_ScheduleNow.setImage(UIImage(named: "check_box_2.png"), for: .normal)
        }
        
        if strFromWhere == "opportunityDetailVC"
        {
            if(("\(dictOpportunityDetail.value(forKey: "fieldSalesPersonId")!)").count > 0)
            {
                // EmployeeList
                let defsLogindDetail = UserDefaults.standard
                let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count != 0)
                {
                    for item in arrOfData
                    {
                        let dict = (item as AnyObject) as! NSDictionary
                        if("\(dictOpportunityDetail.value(forKey: "fieldSalesPersonId")!)" == "\(dict.value(forKey: "EmployeeId")!)")
                        {
                            btnFieldSalesPerson.setTitle(dict["FullName"] as? String, for: .normal)
                            strFieldSalesPersonId = "\(dictOpportunityDetail.value(forKey: "fieldSalesPersonId")!)"
                        }
                    }
                }
            }
        }
        else
        {
            if(("\(dictOpportunityDetail.value(forKey: "fieldSalesPerson")!)").count > 0)
            {
                // EmployeeList
                let defsLogindDetail = UserDefaults.standard
                let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count != 0)
                {
                    for item in arrOfData
                    {
                        let dict = (item as AnyObject) as! NSDictionary
                        if("\(dictOpportunityDetail.value(forKey: "fieldSalesPersonId")!)" == "\(dict.value(forKey: "EmployeeId")!)")
                        {
                            btnFieldSalesPerson.setTitle(dict["FullName"] as? String, for: .normal)
                            strFieldSalesPersonId = "\(dictOpportunityDetail.value(forKey: "fieldSalesPerson")!)"
                        }
                    }
                }
            }
        }
        
        // showing schedule date
        
        if(("\(dictOpportunityDetail.value(forKey: "scheduleDate")!)").count > 0)
        {
            // let strDate = Global().changeDate(toLocalDate: "\(dictOpportunityDetail.value(forKey: "ScheduleDate")!)")
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "scheduleDate")!)")
            
            
            if(strDate?.count != 0)
            {
                btn_ScheduleDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "scheduleDate")!)"), for: .normal)
                strScheduleDate = strDate!
            }
        }
        
        
        // showing schedule time
        
        if(("\(dictOpportunityDetail.value(forKey: "scheduleTime")!)").count > 0)
        {
            // let strTm = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "ScheduleTime")!)")
            
            let strTm = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "scheduleTime")!)", type: "time")
            
            if(strTm?.count != 0)
            {
                btn_Time.setTitle(strTm!, for: .normal)
                
                let dateString = getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: strTm!)
                
                if(dateString.count != 0)
                {
                    strTime = dateString
                }
            }
        }
        
        
        // showing estimated duration
        if(("\(dictOpportunityDetail.value(forKey: "totalEstimationTime")!)").count > 0)
        {
            
            let strEstTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "totalEstimationTime")!)", type: "")
            
            if(strEstTime?.count != 0)
            {
                txtFld_EstimatedDuration.text = strEstTime!
                //"\(dictOpportunityDetail.value(forKey: "TotalEstimationTime")!)"
            }
        }
        
        
        // showing drive time
        if(("\(dictOpportunityDetail.value(forKey: "driveTime")!)").count > 0)
        {
            
            let strDriveTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "driveTime")!)", type: "")
            
            if(strDriveTime?.count != 0)
            {
                txtFld_DriveTime.text = strDriveTime!
                
                //  "\(dictOpportunityDetail.value(forKey: "DriveTime")!)"
                
            }
            
        }
        
        
        // showing service address
        if dictOpportunityDetail.value(forKey: "serviceAddress") is NSDictionary
        {
            let dictServiceAddress = dictOpportunityDetail.value(forKey: "serviceAddress") as! NSDictionary
            
            
            txtFld_ServiceAddressLine1.text = "\(dictServiceAddress.value(forKey: "Address1")!)"
            
            txtFld_ServiceAddressLine2.text = "\(dictServiceAddress.value(forKey: "Address2")!)"
            
            txtFld_ServiceCity.text = "\(dictServiceAddress.value(forKey: "CityName")!)"
            
            txtFld_ServiceZipCode.text = "\(dictServiceAddress.value(forKey: "Zipcode")!)"
            
            
            //txtFld_ServiceMapCode.text = "\(dictServiceAddress.value(forKey: "mapCode")!)"
            
            //txtFld_ServiceGateCode.text = "\(dictServiceAddress.value(forKey: "gateCode")!)"
            
            //txtFld_ServiceCounty.text = "\(dictServiceAddress.value(forKey: "County")!)"
            
            //txtFld_ServiceSchoolDistrict.text = "\(dictServiceAddress.value(forKey: "SchoolDistrict")!)"
            
            let stateNameService = "\(Global().strStatName(fromID: "\((dictServiceAddress.value(forKeyPath: "ServiceAddress.StateId") ?? ""))") ?? "")"
            
            
            if stateNameService.count > 0
            {
                btn_ServiceState.setTitle("\(stateNameService)", for: .normal)
                strStateIdService = "\(dictServiceAddress.value(forKey: "StateId")!)"
            }
            
            
            /*if(("\(dictServiceAddress.value(forKey: "addressSubType")!)").count != 0)
             {
             btn_ServiceAddressSubtype.setTitle("\(dictServiceAddress.value(forKey: "addressSubType")!)", for: .normal)
             strServiceAddressSubType = "\(dictServiceAddress.value(forKey: "addressSubType")!)"
             }*/
            
        }
        
        if(("\(dictOpportunityDetail.value(forKey: "notes")!)").count != 0)
        {
            txtView_ServiceNotes.text = "\(dictOpportunityDetail.value(forKey: "notes")!)"
        }
        if(("\(dictOpportunityDetail.value(forKey: "direction")!)").count != 0)
        {
            txtView_ServiceDirection.text = "\(dictOpportunityDetail.value(forKey: "direction")!)"
        }
        
        // shwing is tax exemp
        
        if(("\(dictOpportunityDetail.value(forKey: "isTaxExempt")!)").count > 0)
        {
            if(("\(dictOpportunityDetail.value(forKey: "isTaxExempt")!)" == "true") || ("\(dictOpportunityDetail.value(forKey: "isTaxExempt")!)" == "True") || ("\(dictOpportunityDetail.value(forKey: "isTaxExempt")!)" == "TRUE") || ("\(dictOpportunityDetail.value(forKey: "isTaxExempt")!)" == "1"))
            {
                btn_IsTaxExempt.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                txtFld_TaxExemption.isHidden = false
                
                txtFld_TaxExemption.text = "\(dictOpportunityDetail.value(forKey: "taxExemptionNo")!)"
            }
            else
            {
                btn_IsTaxExempt.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                txtFld_TaxExemption.isHidden = true
            }
        }
        
        if(("\(dictOpportunityDetail.value(forKey: "serviceLocationId")!)").count > 0)
        {
            if(arrOfServiceAddress.count != 0 )
            {
                for item in arrOfServiceAddress
                {
                    if((item as AnyObject) is String)
                    {
                        
                    }
                    else
                    {
                        let dict = (item as AnyObject) as! NSDictionary
                        
                        if("\(dictOpportunityDetail.value(forKey: "serviceLocationId")!)" == "\(dict.value(forKey: "CustomerAddressId")!)")
                        {
                            btn_SelectServiceAddress.setTitle(Global().strCombinedAddress((dict as! [AnyHashable : Any])), for: .normal)
                            
                            strServiceLocationId = "\(dictOpportunityDetail.value(forKey: "serviceLocationId")!)"
                            break
                            
                        }
                    }
                    
                }
            }
        }
        
        //Nilind
        if(("\(dictOpportunityDetail.value(forKey: "taxSysName")!)").count != 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            //let strBranchName = Global().getBranchName(fromSysNam: strBranchNameGlobal)
            
            if(dictLeadDetailMaster.count != 0)
            {
                let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                
                //  strBranchNameGlobal = "Plumbing Branch"
                
                let arrayTaxCode = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchNameGlobal)
                
                if(arrayTaxCode.count > 0)
                {
                    for item in arrayTaxCode
                    {
                        let dictTaxCode = (item as AnyObject) as! NSDictionary
                        if("\(dictOpportunityDetail.value(forKey: "taxSysName")!)" == "\(dictTaxCode.value(forKey: "SysName")!)")
                        {
                            btn_ServiceTaxcode.setTitle(dictTaxCode["Name"] as? String, for: .normal)
                            
                            strServiceTaxCode = "\(dictTaxCode.value(forKey: "SysName")!)"
                            break
                        }
                    }
                }
            }
        }
        //End
        
        
        
        // showing if billing address is same as service address
        if ("\(dictOpportunityDetail.value(forKey: "billingAddressSameAsService")!)" == "true" || "\(dictOpportunityDetail.value(forKey: "billingAddressSameAsService")!)" == "True" || "\(dictOpportunityDetail.value(forKey: "billingAddressSameAsService")!)" == "TRUE" || "\(dictOpportunityDetail.value(forKey: "billingAddressSameAsService")!)" == "1")
        {
            btn_SameAsServiceAddress.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            self.sameAsServiceAddressChecked()
        }
        
        // showing billing address
        if dictOpportunityDetail.value(forKey: "billingAddress") is NSDictionary
        {
            let dictBillingAddress = dictOpportunityDetail.value(forKey: "billingAddress") as! NSDictionary
            
            txtFld_BillingAddressLine1.text = "\(dictBillingAddress.value(forKey: "Address1")!)"
            
            txtFld_BillingAddressLine2.text = "\(dictBillingAddress.value(forKey: "Address2")!)"
            
            txtFld_BillingCity.text = "\(dictBillingAddress.value(forKey: "CityName")!)"
            
            txtFld_BillingZipCode.text = "\(dictBillingAddress.value(forKey: "Zipcode")!)"
            
            //txtFld_BillingCounty.text = "\(dictBillingAddress.value(forKey: "county")!)"
            
            //txtFld_BillingMapCode.text = "\(dictBillingAddress.value(forKey: "mapCode")!)"
            
            //txtFld_BillingMapCode.text = "\(dictBillingAddress.value(forKey: "schoolDistrict")!)"
            let stateNameBilling = "\(Global().strStatName(fromID: "\((dictBillingAddress.value(forKeyPath: "billingAddress.StateId") ?? ""))") ?? "")"
            
            
            if stateNameBilling.count > 0
            {
                btn_BillingState.setTitle("\(stateNameBilling)", for: .normal)
                strStateIdBilling = "\(dictBillingAddress.value(forKey: "StateId")!)"
            }
            
            
            //btn_BillingCountry.setTitle("\(dictBillingAddress.value(forKey: "CountryName")!)", for: .normal)
            
            
            
        }
        
        if(("\(dictOpportunityDetail.value(forKey: "billingAddress")!)").count > 0)
        {
            if(arrOfBillingAddress.count != 0 )
            {
                for item in arrOfBillingAddress
                {
                    if((item as AnyObject) is String)
                    {
                        
                    }
                    else{
                        
                        let dict = (item as AnyObject) as! NSDictionary
                        
                        if("\(dictOpportunityDetail.value(forKey: "billingLocationId")!)" == "\(dict.value(forKey: "CustomerAddressId")!)")
                        {
                            btn_SelectBillingAddress.setTitle(Global().strCombinedAddress((dict as! [AnyHashable : Any])), for: .normal)
                            
                            strBillingLocationId = "\(dictOpportunityDetail.value(forKey: "billingLocationId")!)"
                            
                        }
                    }
                }
            }
        }
        
        //-------- Show Urgency ----------//
        if(("\(dictOpportunityDetail.value(forKey: "urgency")!)").count != 0)
        {
            btn_Urgency.setTitle("\(dictOpportunityDetail.value(forKey: "urgency")!)", for: .normal)
            strUrgencyId = "\(dictOpportunityDetail.value(forKeyPath: "urgencyId")!)"
        }
        
        //-------- Show Source ----------//SourceIdsList
        
        
        
        
        if dictOpportunityDetail.value(forKey: "sourceIdsList") is NSArray
        {
            let arrMultipleSource = dictOpportunityDetail.value(forKey: "sourceIdsList") as! NSArray
            
            if(arrMultipleSource.count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                let arrTempSource = NSMutableArray()
                var strSourceName = ""
                for souceIdFromServer in arrMultipleSource
                {
                    for souceIdLocal in arrOfData
                    {
                        let dict = (souceIdLocal as AnyObject) as! NSDictionary
                        if("\(souceIdFromServer)" == "\(dict.value(forKey: "SourceId")!)")
                        {
                            arrSourceId.add("\(souceIdFromServer)")
                            arrTempSource.add(dict)
                        }
                    }
                }
                
                if(arrTempSource.count > 0)
                {
                    for item in arrTempSource{
                        strSourceName = "\(strSourceName),\((item as AnyObject).value(forKey: "Name")!)"
                    }
                    if strSourceName == ""
                    {
                        strSourceName = "----Select----"
                    }
                    else{
                        strSourceName = String(strSourceName.dropFirst())
                    }
                    
                    btn_Source.setTitle(strSourceName, for: .normal)
                    arrSelectedSource = arrTempSource
                }
            }
        }
        
        //-------- Show Primary reason for call (i.e service category id) ----------//
        if(("\(dictOpportunityDetail.value(forKey: "serviceCategoryId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictLeadDetailMaster.count != 0)
            {
                let arrOfData = (dictLeadDetailMaster.value(forKey: "Categories") as! NSArray).mutableCopy() as! NSMutableArray
                
                var arrOfFilteredData = returnFilteredArray(array: arrOfData)
                
                // strDepartmentNameGlobal = "testdep"
                
                strDepartmentNameGlobal = "\(dictOpportunityDetail.value(forKey: "departmentSysName")!)"
                
                
                
                //  arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "departmentSysName" , strParameterValue :strDepartmentNameGlobal )
                arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
                
                
                
                for item in arrOfFilteredData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    
                    if("\(dictOpportunityDetail.value(forKey: "serviceCategoryId")!)" == "\(dict.value(forKey: "CategoryId")!)")
                    {
                        btn_PrimaryReasonForCall.setTitle(dict["Name"] as? String, for: .normal)
                        strServiceCategoryID = "\(dict.value(forKeyPath: "CategoryId")!)"
                        strPrimaryReasonForCallDepSysName = "\(dict.value(forKeyPath: "DepartmentSysName")!)"
                        break
                    }
                }
            }
        }
        
        //-------- Show Service ----------//
        
        if(("\(dictOpportunityDetail.value(forKey: "serviceId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            if(arrOfFilteredData.count != 0)
            {
                
                for item in arrOfFilteredData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    if("\(dictOpportunityDetail.value(forKey: "serviceId")!)" == "\(dict.value(forKey: "ServiceId")!)")
                    {
                        btn_Service.setTitle(dict["Name"] as? String, for: .normal)
                        strServiceId = "\(dict.value(forKeyPath: "ServiceId")!)"
                        break
                    }
                }
            }
        }
        
        
        //-------- Show Opportunity Type ----------//
        
        if(("\(dictOpportunityDetail.value(forKey: "opportunityType")!)").count > 0)
        {
            btn_OpportunityType.setTitle("\(dictOpportunityDetail.value(forKey: "opportunityType")!)", for: .normal)
            strOpportunityType = "\(dictOpportunityDetail.value(forKey: "opportunityType")!)"
        }
        else
        {
            btn_OpportunityType.setTitle("Residential", for: .normal)
            strOpportunityType = "Residential"
        }
        
        
        //-------- Show Submitted By ----------//
        
        let defsLogindDetail = UserDefaults.standard
        let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
        
        for item in arrOfData
        {
            if("\(dictOpportunityDetail.value(forKey: "submittedById")!)" == "\((item as AnyObject).value(forKey: "EmployeeId")!)")
            {
                btn_SubmittedBy.setTitle("\((item as AnyObject).value(forKeyPath: "FullName")!)", for: .normal)//
                
                strSubmittedById = "\((item as AnyObject).value(forKeyPath: "EmployeeId")!)"
                break
            }
        }
        //End
        
        
        
        
        
        
        // Showing Company Size
        if(("\(dictOpportunityDetail.value(forKey: "sizeId")!)").count > 0)
        {
            btn_CompanySIze.setTitle(dictOpportunityDetail["companySize"] as? String, for: .normal)
            strCompanySizeId = "\(dictOpportunityDetail.value(forKeyPath: "sizeId")!)"
        }
        
        
        // Showing Industry
        
        if(("\(dictOpportunityDetail.value(forKey: "industryId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "TotalLeadCountResponse") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "IndustryMasters") as! NSArray)
            
            for item in arrOfData
            {
                if("\(dictOpportunityDetail.value(forKey: "industryId")!)" == "\((item as AnyObject).value(forKey: "IndustryId")!)")
                {
                    btn_Industry.setTitle("\((item as AnyObject).value(forKey: "Name")!)", for: .normal)
                    
                    strIndustryId = "\(dictOpportunityDetail.value(forKey: "industryId")!)"
                    break
                }
            }
        }
        
        // Showing FollowUpDate
        if(("\(dictOpportunityDetail.value(forKey: "followUpDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "followUpDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_FollowUpDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "followUpDate")!)"), for: .normal)
                strFollowUpDate = strDate!
            }
        }
        
        // Showing GrabbedDate
        if(("\(dictOpportunityDetail.value(forKey: "grabbedDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "grabbedDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_GrabbedDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "grabbedDate")!)"), for: .normal)
                
            }
            
        }
        
        // Showing Expected Closing Date
        if(("\(dictOpportunityDetail.value(forKey: "expectedClosingDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "expectedClosingDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_ExpectedClosingDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "expectedClosingDate")!)"), for: .normal)
                
                strExpectedClosingDate = strDate!
            }
            
        }
        
        // Showing Submitted Date
        if(("\(dictOpportunityDetail.value(forKey: "submittedDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "submittedDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_SubmittedDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "submittedDate")!)"), for: .normal)
                // strSubmittedById = strDate!
            }
            
            
        }
        
        // Showing Inside Sales Person
        if strFromWhere == "opportunityDetailVC"
        {
            if(("\(dictOpportunityDetail.value(forKey: "insideSalesPersonId")!)").count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
                
                for item in arrOfData
                {
                    let dictItem = (item as AnyObject) as! NSDictionary
                    
                    if("\(dictOpportunityDetail.value(forKey: "insideSalesPersonId")!)" == "\(dictItem.value(forKey: "EmployeeId")!)")
                    {
                        btn_InsideSalesPerson.setTitle(dictItem["FullName"] as? String, for: .normal)
                        strInsideSalesPersonId = "\(dictItem.value(forKeyPath: "EmployeeId")!)"
                    }
                }
            }
        }
        else
        {
            if(("\(dictOpportunityDetail.value(forKey: "insideSalesPerson")!)").count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
                
                for item in arrOfData
                {
                    let dictItem = (item as AnyObject) as! NSDictionary
                    
                    //if("\(dictOpportunityDetail.value(forKey: "insideSalesPerson")!)" == "\(dictItem.value(forKey: "EmployeeId")!)")
                    if("\(dictOpportunityDetail.value(forKey: "insideSalesPersonId")!)" == "\(dictItem.value(forKey: "EmployeeId")!)")
                        
                    {
                        btn_InsideSalesPerson.setTitle(dictItem["FullName"] as? String, for: .normal)
                        strInsideSalesPersonId = "\(dictItem.value(forKeyPath: "EmployeeId")!)"
                    }
                }
            }
        }
        
        
        
        
        // Showing Tech Note
        if(("\(dictOpportunityDetail.value(forKey: "techNote")!)").count > 0)
        {
            txtView_TechNote.text = "\(dictOpportunityDetail.value(forKey: "techNote")!)"
        }
        
        
        // Showing Description
        
        if(("\(dictOpportunityDetail.value(forKey: "opportunityDescription")!)").count > 0)
        {
            txtView_Description.text = "\(dictOpportunityDetail.value(forKey: "opportunityDescription")!)"
        }
        
        // Showing Projected Amount
        
        if(("\(dictOpportunityDetail.value(forKey: "opportunityValue")!)").count > 0)
        {
            txtFld_ProjectedAmount.text = "\(dictOpportunityDetail.value(forKey: "opportunityValue")!)"
        }
        
        /* let currentValue = Float("\((dictOpportunityDetail.value(forKey: "confidenceValue") ?? ""))") ?? 0 * 100
         
         let val = String(format: "%.00f",currentValue)
         
         lblProgreeConfidenceValue.text = "\(val)%"
         progressConfidenceLvel.value = Float("\((dictOpportunityDetail.value(forKey: "confidenceValue") ?? ""))") ?? 0*/ //ConfidenceLevel
        
        let currentValue = Float("\((dictOpportunityDetail.value(forKey: "confidenceLevel") ?? ""))") ?? 0 * 100
        
        
        
        
        let val = String(format: "%.00f",currentValue)
        
        lblProgreeConfidenceValue.text = "\(val)%"
        progressConfidenceLvel.value = Float("\(currentValue/100)") ?? 0
        
        //progressConfidenceLvel.value = Float("\((dictOpportunityDetail.value(forKey: "confidenceLevel") ?? ""))") ?? 0
        
    }
    func showValuesOld() {
        
        lbl_OpportunityName.text = "\(dictOpportunityDetail.value(forKey: "Name")!)(# : \(dictOpportunityDetail.value(forKey: "LeadNumber")!))"
        
        if dictOpportunityDetail.value(forKey: "Account") is NSDictionary {
            
            let dict = dictOpportunityDetail.value(forKey: "Account") as! NSDictionary
            
            lbl_AccountName.text = "Account # : \(dict.value(forKey: "AccountNo")!)"
            
            if(("\(dict.value(forKey: "Description")!)").count != 0)
            {
                txtView_AccountAlert.text = "\(dict.value(forKey: "Description")!)"
            }
        }
        
        if dictOpportunityDetail.value(forKey: "AccountCompany") is NSDictionary {
            
            let dict = dictOpportunityDetail.value(forKey: "AccountCompany") as! NSDictionary
            
            lbl_CompanyName.text = "Company : \(dict.value(forKey: "Name")!)"
        }
        // showing branch name
        if(strBranchNameGlobal.count != 0)
        {
            lbl_BranchName.text = "Branch : "+Global().getBranchName(fromSysNam: strBranchNameGlobal)
        }
        else
        {
            lbl_BranchName.text = "Branch : "+"N/A"
        }
        
        lbl_Stage.text = "Stage : \(dictOpportunityDetail.value(forKeyPath: "Stage")!)"
        
        lbl_Status.text = "Status : \(dictOpportunityDetail.value(forKeyPath: "Status")!)"
        
        if( lbl_Status.text == "Complete")
        {
            btn_ScheduleNow.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            btn_ScheduleNow.isUserInteractionEnabled = false
        }
        
        txtFld_ProposedAmount.isUserInteractionEnabled = false
        btn_GrabbedDate.isUserInteractionEnabled = false
        btn_SubmittedDate.isUserInteractionEnabled = false
        txtView_TechNote.isUserInteractionEnabled = false
        
        
        if dictOpportunityDetail.value(forKey: "LeadContact") is NSDictionary {
            
            let dict = dictOpportunityDetail.value(forKey: "LeadContact") as! NSDictionary
            
            txtFld_FN.text = "\(dict.value(forKey: "FirstName")!)"
            txtFld_MN.text = "\(dict.value(forKey: "MiddleName")!)"
            txtFld_LN.text = "\(dict.value(forKey: "LastName")!)"
            
            txtFld_PrimaryPhone.text = "\(dict.value(forKey: "PrimaryPhone")!)"
            txtFld_PrimaryEmail.text = Global().strReplacedEmail("\(dict.value(forKey: "PrimaryEmail")!)")
            txtFld_SecondaryEmail.text = Global().strReplacedEmail("\(dict.value(forKey: "SecondaryEmail")!)")
            txtFld_SecondaryPhone.text = "\(dict.value(forKey: "SecondaryPhone")!)"
            
            if let val = dict["CellPhone1"] {
                // now val is not nil and the Optional has been unwrapped, so use it
                txtFld_Cell.text = "\(val)"
            }
            
            
            
        }
        // showing Opportunity Schedule
        if(("\(dictOpportunityDetail.value(forKey: "Status")!)" == "Open") && "\(dictOpportunityDetail.value(forKey: "Stage")!)" == "Scheduled")
        {
            btn_ScheduleNow.setImage(UIImage(named: "check_box_2.png"), for: .normal)
        }
        
        if(("\(dictOpportunityDetail.value(forKey: "FieldSalesPerson")!)").count > 0)
        {
            // EmployeeList
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            if(arrOfData.count != 0)
            {
                for item in arrOfData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    if("\(dictOpportunityDetail.value(forKey: "FieldSalesPerson")!)" == "\(dict.value(forKey: "EmployeeId")!)")
                    {
                        btnFieldSalesPerson.setTitle(dict["FullName"] as? String, for: .normal)
                        strFieldSalesPersonId = "\(dictOpportunityDetail.value(forKey: "FieldSalesPerson")!)"
                    }
                }
            }
        }
        // showing schedule date
        
        if(("\(dictOpportunityDetail.value(forKey: "ScheduleDate")!)").count > 0)
        {
            // let strDate = Global().changeDate(toLocalDate: "\(dictOpportunityDetail.value(forKey: "ScheduleDate")!)")
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "ScheduleDate")!)")
            
            
            if(strDate?.count != 0)
            {
                btn_ScheduleDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "ScheduleDate")!)"), for: .normal)
                strScheduleDate = strDate!
            }
        }
        
        
        // showing schedule time
        
        if(("\(dictOpportunityDetail.value(forKey: "ScheduleTime")!)").count > 0)
        {
            // let strTm = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "ScheduleTime")!)")
            
            let strTm = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "ScheduleTime")!)", type: "time")
            
            if(strTm?.count != 0)
            {
                btn_Time.setTitle(strTm!, for: .normal)
                
                let dateString = getTimeStringIn24HrsFormatFrom12HrsFormat(timeToConvert: strTm!)
                
                if(dateString.count != 0)
                {
                    strTime = dateString
                }
            }
        }
        
        
        // showing estimated duration
        if(("\(dictOpportunityDetail.value(forKey: "TotalEstimationTime")!)").count > 0)
        {
            
            let strEstTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "TotalEstimationTime")!)", type: "")
            
            if(strEstTime?.count != 0)
            {
                txtFld_EstimatedDuration.text = strEstTime!
                //"\(dictOpportunityDetail.value(forKey: "TotalEstimationTime")!)"
            }
        }
        
        
        // showing drive time
        if(("\(dictOpportunityDetail.value(forKey: "DriveTime")!)").count > 0)
        {
            
            let strDriveTime = Global().changeDate(toLocalTimeAkshay: "\(dictOpportunityDetail.value(forKey: "DriveTime")!)", type: "")
            
            if(strDriveTime?.count != 0)
            {
                txtFld_DriveTime.text = strDriveTime!
                
                //  "\(dictOpportunityDetail.value(forKey: "DriveTime")!)"
                
            }
            
        }
        
        
        // showing service address
        if dictOpportunityDetail.value(forKey: "ServiceLocationDc") is NSDictionary
        {
            let dictServiceAddress = dictOpportunityDetail.value(forKey: "ServiceLocationDc") as! NSDictionary
            
            
            txtFld_ServiceAddressLine1.text = "\(dictServiceAddress.value(forKey: "Address1")!)"
            
            txtFld_ServiceAddressLine2.text = "\(dictServiceAddress.value(forKey: "Address2")!)"
            
            txtFld_ServiceCity.text = "\(dictServiceAddress.value(forKey: "CityName")!)"
            
            txtFld_ServiceZipCode.text = "\(dictServiceAddress.value(forKey: "Zipcode")!)"
            
            
            txtFld_ServiceMapCode.text = "\(dictServiceAddress.value(forKey: "MapCode")!)"
            
            txtFld_ServiceGateCode.text = "\(dictServiceAddress.value(forKey: "GateCode")!)"
            
            txtFld_ServiceCounty.text = "\(dictServiceAddress.value(forKey: "County")!)"
            
            txtFld_ServiceSchoolDistrict.text = "\(dictServiceAddress.value(forKey: "SchoolDistrict")!)"
            
            if(("\(dictServiceAddress.value(forKey: "StateName")!)").count != 0)
            {
                btn_ServiceState.setTitle("\(dictServiceAddress.value(forKey: "StateName")!)", for: .normal)
                strStateIdService = "\(dictServiceAddress.value(forKey: "StateId")!)"
            }
            
            
            // btn_ServiceCountry.setTitle("\(dictServiceAddress.value(forKey: "CountryName")!)", for: .normal)
            
            if(("\(dictServiceAddress.value(forKey: "AddressSubType")!)").count != 0)
            {
                btn_ServiceAddressSubtype.setTitle("\(dictServiceAddress.value(forKey: "AddressSubType")!)", for: .normal)
                strServiceAddressSubType = "\(dictServiceAddress.value(forKey: "AddressSubType")!)"
            }
            
            if(("\(dictServiceAddress.value(forKey: "TaxSysName")!)").count != 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
                //let strBranchName = Global().getBranchName(fromSysNam: strBranchNameGlobal)
                
                if(dictLeadDetailMaster.count != 0)
                {
                    let arrOfData = (dictLeadDetailMaster.value(forKey: "TaxMaster") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    //  strBranchNameGlobal = "Plumbing Branch"
                    
                    let arrayTaxCode = returnFilteredArrayValues(array : arrOfData , strParameterName : "BranchSysName" , strParameterValue :strBranchNameGlobal)
                    
                    if(arrayTaxCode.count > 0)
                    {
                        for item in arrayTaxCode
                        {
                            let dictTaxCode = (item as AnyObject) as! NSDictionary
                            if("\(dictServiceAddress.value(forKey: "TaxSysName")!)" == "\(dictTaxCode.value(forKey: "SysName")!)")
                            {
                                btn_ServiceTaxcode.setTitle(dictTaxCode["Name"] as? String, for: .normal)
                                
                                strServiceTaxCode = "\(dictTaxCode.value(forKey: "SysName")!)"
                                break
                            }
                        }
                    }
                }
            }
            
            
            if(("\(dictOpportunityDetail.value(forKey: "ServiceLocationId")!)").count > 0)
            {
                if(arrOfServiceAddress.count != 0 )
                {
                    for item in arrOfServiceAddress
                    {
                        if((item as AnyObject) is String)
                        {
                            
                        }
                        else
                        {
                            let dict = (item as AnyObject) as! NSDictionary
                            
                            if("\(dictOpportunityDetail.value(forKey: "ServiceLocationId")!)" == "\(dict.value(forKey: "CustomerAddressId")!)")
                            {
                                btn_SelectServiceAddress.setTitle(Global().strCombinedAddress((dict as! [AnyHashable : Any])), for: .normal)
                                
                                strServiceLocationId = "\(dictOpportunityDetail.value(forKey: "ServiceLocationId")!)"
                                break
                                
                            }
                        }
                        
                    }
                }
            }
            
            if(("\(dictServiceAddress.value(forKey: "Notes")!)").count != 0)
            {
                txtView_ServiceNotes.text = "\(dictServiceAddress.value(forKey: "Notes")!)"
            }
            if(("\(dictServiceAddress.value(forKey: "Direction")!)").count != 0)
            {
                txtView_ServiceDirection.text = "\(dictServiceAddress.value(forKey: "Direction")!)"
            }
            
            // shwing is tax exemp
            
            if(("\(dictServiceAddress.value(forKey: "IsTaxExempt")!)").count > 0)
            {
                if(("\(dictServiceAddress.value(forKey: "IsTaxExempt")!)" == "true") || ("\(dictServiceAddress.value(forKey: "IsTaxExempt")!)" == "True") || ("\(dictServiceAddress.value(forKey: "IsTaxExempt")!)" == "TRUE") || ("\(dictServiceAddress.value(forKey: "IsTaxExempt")!)" == "1"))
                {
                    btn_IsTaxExempt.setImage(UIImage(named: "check_box_2.png"), for: .normal)
                    txtFld_TaxExemption.isHidden = false
                    
                    txtFld_TaxExemption.text = "\(dictServiceAddress.value(forKey: "TaxExemptionNo")!)"
                }
                else
                {
                    btn_IsTaxExempt.setImage(UIImage(named: "check_box_1.png"), for: .normal)
                    txtFld_TaxExemption.isHidden = true
                }
            }
        }
        
        // showing if billing address is same as service address
        if ("\(dictOpportunityDetail.value(forKey: "BillingAddressSameAsService")!)" == "true" || "\(dictOpportunityDetail.value(forKey: "BillingAddressSameAsService")!)" == "True" || "\(dictOpportunityDetail.value(forKey: "BillingAddressSameAsService")!)" == "TRUE" || "\(dictOpportunityDetail.value(forKey: "BillingAddressSameAsService")!)" == "1")
        {
            btn_SameAsServiceAddress.setImage(UIImage(named: "check_box_2.png"), for: .normal)
            self.sameAsServiceAddressChecked()
        }
        
        // showing billing address
        if dictOpportunityDetail.value(forKey: "BillingLocationDc") is NSDictionary
        {
            let dictBillingAddress = dictOpportunityDetail.value(forKey: "BillingLocationDc") as! NSDictionary
            
            txtFld_BillingAddressLine1.text = "\(dictBillingAddress.value(forKey: "Address1")!)"
            
            txtFld_BillingAddressLine2.text = "\(dictBillingAddress.value(forKey: "Address2")!)"
            
            txtFld_BillingCity.text = "\(dictBillingAddress.value(forKey: "CityName")!)"
            
            txtFld_BillingZipCode.text = "\(dictBillingAddress.value(forKey: "Zipcode")!)"
            
            txtFld_BillingCounty.text = "\(dictBillingAddress.value(forKey: "County")!)"
            
            txtFld_BillingMapCode.text = "\(dictBillingAddress.value(forKey: "MapCode")!)"
            
            txtFld_BillingMapCode.text = "\(dictBillingAddress.value(forKey: "SchoolDistrict")!)"
            
            if(("\(dictBillingAddress.value(forKey: "StateName")!)").count != 0)
            {
                btn_BillingState.setTitle("\(dictBillingAddress.value(forKey: "StateName")!)", for: .normal)
                strStateIdBilling = "\(dictBillingAddress.value(forKey: "StateId")!)"
            }
            
            
            //btn_BillingCountry.setTitle("\(dictBillingAddress.value(forKey: "CountryName")!)", for: .normal)
            
            
            if(("\(dictOpportunityDetail.value(forKey: "BillingLocationDc")!)").count > 0)
            {
                if(arrOfBillingAddress.count != 0 )
                {
                    for item in arrOfBillingAddress
                    {
                        if((item as AnyObject) is String)
                        {
                            
                        }
                        else{
                            
                            let dict = (item as AnyObject) as! NSDictionary
                            
                            if("\(dictOpportunityDetail.value(forKey: "BillingLocationId")!)" == "\(dict.value(forKey: "CustomerAddressId")!)")
                            {
                                btn_SelectBillingAddress.setTitle(Global().strCombinedAddress((dict as! [AnyHashable : Any])), for: .normal)
                                
                                strBillingLocationId = "\(dictOpportunityDetail.value(forKey: "BillingLocationId")!)"
                                
                            }
                        }
                    }
                }
            }
        }
        
        //-------- Show Urgency ----------//
        if(("\(dictOpportunityDetail.value(forKey: "UrgencyName")!)").count != 0)
        {
            btn_Urgency.setTitle("\(dictOpportunityDetail.value(forKey: "UrgencyName")!)", for: .normal)
            strUrgencyId = "\(dictOpportunityDetail.value(forKeyPath: "UrgencyId")!)"
        }
        
        //-------- Show Source ----------//SourceIdsList
        
        
        if dictOpportunityDetail.value(forKey: "SourceIdsList") is NSArray
        {
            let arrMultipleSource = dictOpportunityDetail.value(forKey: "SourceIdsList") as! NSArray
            
            if(arrMultipleSource.count > 0)
            {
                let defsLogindDetail = UserDefaults.standard
                let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                let arrTempSource = NSMutableArray()
                var strSourceName = ""
                for souceIdFromServer in arrMultipleSource
                {
                    for souceIdLocal in arrOfData
                    {
                        let dict = (souceIdLocal as AnyObject) as! NSDictionary
                        if("\(souceIdFromServer)" == "\(dict.value(forKey: "SourceId")!)")
                        {
                            arrSourceId.add("\(souceIdFromServer)")
                            arrTempSource.add(dict)
                        }
                    }
                }
                
                if(arrTempSource.count > 0)
                {
                    for item in arrTempSource{
                        strSourceName = "\(strSourceName),\((item as AnyObject).value(forKey: "Name")!)"
                    }
                    if strSourceName == ""
                    {
                        strSourceName = "----Select----"
                    }
                    else{
                        strSourceName = String(strSourceName.dropFirst())
                    }
                    
                    btn_Source.setTitle(strSourceName, for: .normal)
                    arrSelectedSource = arrTempSource
                }
            }
        }
        
        //-------- Show Primary reason for call (i.e service category id) ----------//
        if(("\(dictOpportunityDetail.value(forKey: "ServiceCategoryId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictLeadDetailMaster.count != 0)
            {
                let arrOfData = (dictLeadDetailMaster.value(forKey: "Categories") as! NSArray).mutableCopy() as! NSMutableArray
                
                var arrOfFilteredData = returnFilteredArray(array: arrOfData)
                
                // strDepartmentNameGlobal = "testdep"
                
                strDepartmentNameGlobal = "\(dictOpportunityDetail.value(forKey: "DepartmentSysName")!)"
                
                
                
                arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
                
                
                for item in arrOfFilteredData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    
                    if("\(dictOpportunityDetail.value(forKey: "ServiceCategoryId")!)" == "\(dict.value(forKey: "CategoryId")!)")
                    {
                        btn_PrimaryReasonForCall.setTitle(dict["Name"] as? String, for: .normal)
                        strServiceCategoryID = "\(dict.value(forKeyPath: "CategoryId")!)"
                        strPrimaryReasonForCallDepSysName = "\(dict.value(forKeyPath: "DepartmentSysName")!)"
                        break
                    }
                }
            }
        }
        
        //-------- Show Service ----------//
        
        if(("\(dictOpportunityDetail.value(forKey: "PrimaryServiceId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "ServiceMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            var arrOfFilteredData = returnFilteredArray(array: arrOfData)
            
            //strDepartmentNameGlobal = "testdep"
            
            arrOfFilteredData = returnFilteredArrayValues(array : arrOfFilteredData , strParameterName : "DepartmentSysName" , strParameterValue :strDepartmentNameGlobal )
            
            if(arrOfFilteredData.count != 0)
            {
                
                for item in arrOfFilteredData
                {
                    let dict = (item as AnyObject) as! NSDictionary
                    if("\(dictOpportunityDetail.value(forKey: "PrimaryServiceId")!)" == "\(dict.value(forKey: "ServiceId")!)")
                    {
                        btn_Service.setTitle(dict["Name"] as? String, for: .normal)
                        strServiceId = "\(dict.value(forKeyPath: "ServiceId")!)"
                        break
                    }
                }
            }
        }
        
        //-------- Show Opportunity Type ----------//
        
        if(("\(dictOpportunityDetail.value(forKey: "FlowType")!)").count > 0)
        {
            btn_OpportunityType.setTitle("\(dictOpportunityDetail.value(forKey: "FlowType")!)", for: .normal)
            strOpportunityType = "\(dictOpportunityDetail.value(forKey: "FlowType")!)"
        }
        else
        {
            btn_OpportunityType.setTitle("Residential", for: .normal)
            strOpportunityType = "Residential"
        }
        //-------- Show Submitted By ----------//
        
        if dictOpportunityDetail.value(forKey: "SubmittedByDetailExtDc") is NSDictionary {
            
            let dict = dictOpportunityDetail.value(forKey: "SubmittedByDetailExtDc") as! NSDictionary
            
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrOfData
            {
                if("\(dict.value(forKey: "SubmittedById")!)" == "\((item as AnyObject).value(forKey: "EmployeeId")!)")
                {
                    btn_SubmittedBy.setTitle("\((item as AnyObject).value(forKeyPath: "FullName")!)", for: .normal)//
                    
                    strSubmittedById = "\((item as AnyObject).value(forKeyPath: "EmployeeId")!)"
                    break
                }
            }
        }
        
        // Showing Company Size
        if(("\(dictOpportunityDetail.value(forKey: "sizeId")!)").count > 0)
        {
            btn_CompanySIze.setTitle(dictOpportunityDetail["companySize"] as? String, for: .normal)
            strCompanySizeId = "\(dictOpportunityDetail.value(forKeyPath: "sizeId")!)"
        }
        
        
        // Showing Industry
        
        if(("\(dictOpportunityDetail.value(forKey: "IndustryId")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "TotalLeadCountResponse") as! NSDictionary
            let arrOfData = (dictLeadDetailMaster.value(forKey: "IndustryMasters") as! NSArray)
            
            for item in arrOfData
            {
                if("\(dictOpportunityDetail.value(forKey: "IndustryId")!)" == "\((item as AnyObject).value(forKey: "IndustryId")!)")
                {
                    btn_Industry.setTitle("\((item as AnyObject).value(forKey: "Name")!)", for: .normal)
                    
                    strIndustryId = "\(dictOpportunityDetail.value(forKey: "IndustryId")!)"
                    break
                }
            }
        }
        
        // Showing FollowUpDate
        if(("\(dictOpportunityDetail.value(forKey: "FollowUpDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "FollowUpDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_FollowUpDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "FollowUpDate")!)"), for: .normal)
                strFollowUpDate = strDate!
            }
        }
        
        // Showing GrabbedDate
        if(("\(dictOpportunityDetail.value(forKey: "DateGrabbed")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "DateGrabbed")!)")
            
            if(strDate?.count != 0)
            {
                btn_GrabbedDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "DateGrabbed")!)"), for: .normal)
                
            }
            
        }
        
        // Showing Expected Closing Date
        if(("\(dictOpportunityDetail.value(forKey: "CloseDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "CloseDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_ExpectedClosingDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "CloseDate")!)"), for: .normal)
                
                strExpectedClosingDate = strDate!
            }
            
        }
        
        // Showing Submitted Date
        if(("\(dictOpportunityDetail.value(forKey: "SubmittedDate")!)").count > 0)
        {
            let strDate = Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "SubmittedDate")!)")
            
            if(strDate?.count != 0)
            {
                btn_SubmittedDate.setTitle(Global().changeDate(toLocalDateAkshay: "\(dictOpportunityDetail.value(forKey: "SubmittedDate")!)"), for: .normal)
                // strSubmittedById = strDate!
            }
            
            
        }
        
        // Showing Inside Sales Person
        if(("\(dictOpportunityDetail.value(forKey: "InsideSalesPerson")!)").count > 0)
        {
            let defsLogindDetail = UserDefaults.standard
            let arrOfData = (defsLogindDetail.value(forKey: "EmployeeList") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrOfData
            {
                let dictItem = (item as AnyObject) as! NSDictionary
                
                if("\(dictOpportunityDetail.value(forKey: "InsideSalesPerson")!)" == "\(dictItem.value(forKey: "EmployeeId")!)")
                {
                    btn_InsideSalesPerson.setTitle(dictItem["FullName"] as? String, for: .normal)
                    strInsideSalesPersonId = "\(dictItem.value(forKeyPath: "EmployeeId")!)"
                }
            }
        }
        
        // Showing Tech Note
        if(("\(dictOpportunityDetail.value(forKey: "TechNote")!)").count > 0)
        {
            txtView_TechNote.text = "\(dictOpportunityDetail.value(forKey: "TechNote")!)"
        }
        
        
        // Showing Description
        
        if(("\(dictOpportunityDetail.value(forKey: "Description")!)").count > 0)
        {
            txtView_Description.text = "\(dictOpportunityDetail.value(forKey: "Description")!)"
        }
        
        // Showing Projected Amount
        
        if(("\(dictOpportunityDetail.value(forKey: "LeadValue")!)").count > 0)
        {
            txtFld_ProjectedAmount.text = "\(dictOpportunityDetail.value(forKey: "LeadValue")!)"
        }
    }
    
    
    // MARK: - -----------------------------------Validation Methods-----------------------------------
    
    func validateFields() -> String
    {
        var strMessage = ""
        
        let isTaxCodeReq = Global().alert(forTaxCode: strServiceTaxCode)
        
        if(txtFld_FN.text?.count == 0)
        {
            strMessage = "Please enter first name"
            return strMessage
        }
        if(txtFld_LN.text?.count == 0)
        {
            strMessage = "Please enter last name"
            return strMessage
        }
        
        if(txtFld_PrimaryPhone.text?.count != 0)
        {
            if(txtFld_PrimaryPhone.text!.count < 10)
            {
                strMessage = "Primary phone should be minimum 10 characters long "
                return strMessage
            }
        }
        
        if(txtFld_Cell.text?.count != 0)
        {
            if(txtFld_Cell.text!.count < 10)
            {
                strMessage = "Cell number should be minimum 10 characters long "
                return strMessage
            }
        }
        
        if(txtFld_SecondaryPhone.text?.count != 0)
        {
            if(txtFld_SecondaryPhone.text!.count < 10)
            {
                strMessage = "Secondary phone should be minimum 10 characters long "
                return strMessage
            }
        }
        
        if(txtFld_PrimaryEmail.text?.count != 0)
        {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtFld_PrimaryEmail.text!)
            
            if !isValid {
                strMessage = "Please enter valid primary email"
                return strMessage
            }
            
            /*
             if(validateEmail(email: txtFld_PrimaryEmail.text!) == false)
             {
             strMessage = "Please enter valid primary email"
             return strMessage
             }
             */
            
        }
        if(txtFld_SecondaryEmail.text?.count != 0)
        {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtFld_SecondaryEmail.text!)
            
            if !isValid {
                
                strMessage = "Please enter valid secondary email"
                return strMessage
                
            }
            /*
             if(validateEmail(email: txtFld_SecondaryEmail.text!) == false)
             {
             strMessage = "Please enter valid secondary email"
             return strMessage
             }
             */
            
        }
        
        if(btn_OpportunityType.titleLabel?.text == "----Select----")
        {
            strMessage = "Please select opportunity type"
            return strMessage
        }
        if(btn_SubmittedBy.titleLabel?.text == "----Select----")
        {
            strMessage = "Please select submitted by"
            return strMessage
        }
        
        if( btn_ScheduleNow.currentImage ==  UIImage(named: "check_box_2.png"))
        {
            if(btnFieldSalesPerson.titleLabel?.text == "----Select----")
            {
                strMessage = "Please select field sales person"
                return strMessage
            }
            if(btn_ScheduleDate.titleLabel?.text == "----Select----")
            {
                strMessage = "Please select schedule date"
                return strMessage
            }
            if(btn_Time.titleLabel?.text == "----Select----")
            {
                strMessage = "Please select time"
                return strMessage
            }
            if(txtFld_ServiceAddressLine1.text?.count == 0)
            {
                strMessage = "Please enter service address line1"
                return strMessage
            }
            
            if(btn_ServiceState.titleLabel?.text == "----Select----")
            {
                strMessage = "Please select service state"
                return strMessage
            }
            if(txtFld_ServiceCity.text?.count == 0)
            {
                strMessage = "Please enter service address city"
                return strMessage
            }
            if(txtFld_ServiceZipCode.text?.count == 0)
            {
                strMessage = "Please enter service address zipcode"
                return strMessage
            }
            if(isTaxCodeReq)
            {
                strMessage = TaxCode
                return strMessage
            }
            if(btn_SameAsServiceAddress.currentImage == UIImage(named: "check_box_1.png"))
            {
                if(txtFld_BillingAddressLine1.text?.count == 0)
                {
                    strMessage = "Please enter billing address line1"
                    return strMessage
                }
                
                if(btn_BillingState.titleLabel?.text == "----Select----")
                {
                    strMessage = "Please select billing address state"
                    return strMessage
                }
                if(txtFld_BillingCity.text?.count == 0)
                {
                    strMessage = "Please enter billing address city"
                    return strMessage
                }
                if(txtFld_BillingZipCode.text?.count == 0)
                {
                    strMessage = "Please enter billing address zipcode"
                    return strMessage
                }
            }
        }
        else // check if service address line1 field of address is filled then all fields are compulsary to be filled
        {
            if(txtFld_ServiceAddressLine1.text?.count != 0  )//|| txtFld_ServiceCity.text?.count != 0 ||  txtFld_ServiceZipCode.text?.count != 0 || btn_ServiceState.titleLabel?.text != "----Select----"
            {
                if(txtFld_ServiceAddressLine1.text?.count == 0)
                {
                    strMessage = "Please enter service address line1"
                    return strMessage
                }
                
                if(btn_ServiceState.titleLabel?.text == "----Select----")
                {
                    strMessage = "Please select service address state"
                    return strMessage
                }
                if(txtFld_ServiceCity.text?.count == 0)
                {
                    strMessage = "Please enter service address city"
                    return strMessage
                }
                if(txtFld_ServiceZipCode.text?.count == 0)
                {
                    strMessage = "Please enter service address zipcode"
                    return strMessage
                }
                if(isTaxCodeReq)
                {
                    strMessage = TaxCode
                    return strMessage
                }
            }
            
            if(btn_SameAsServiceAddress.currentImage == UIImage(named: "check_box_1.png"))
            {
                // check if billing address line1 field of address is filled then all fields are compulsary to be filled
                
                if(txtFld_BillingAddressLine1.text?.count != 0  )
                {
                    if(txtFld_BillingAddressLine1.text?.count == 0)
                    {
                        strMessage = "Please enter billing address line1"
                        return strMessage
                    }
                    if(btn_BillingState.titleLabel?.text == "----Select----")
                    {
                        strMessage = "Please select billing address state"
                        return strMessage
                    }
                    if(txtFld_BillingCity.text?.count == 0)
                    {
                        strMessage = "Please enter billing address city"
                        return strMessage
                    }
                    if(txtFld_BillingZipCode.text?.count == 0)
                    {
                        strMessage = "Please enter billing address zipcode"
                        return strMessage
                    }
                }
            }
            
        }
        
        if(txtFld_EstimatedDuration.text?.count != 0)
        {
            if(txtFld_EstimatedDuration.text!.contains(":"))
            {
                let arrDuration = txtFld_EstimatedDuration.text?.components(separatedBy: ":")
                
                if(Int(arrDuration![0])! > 24)
                {
                    strMessage = "Please enter estimation duration hrs less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDuration![0])! == 24 && Int(arrDuration![1])! > 0)
                {
                    strMessage = "Please enter estimation duration less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDuration![1])! > 60)
                {
                    strMessage = "Please enter estimation duration minutes less than 60 minutes"
                    return strMessage
                }
                
            }
            else
            {
                if(Int(txtFld_EstimatedDuration.text!)! > 24)
                {
                    strMessage = "Please enter estimation duration less than 24 hrs"
                    return strMessage
                }
            }
        }
        
        if(txtFld_DriveTime.text?.count != 0)
        {
            if(txtFld_DriveTime.text!.contains(":"))
            {
                let arrDriveTime = txtFld_DriveTime.text?.components(separatedBy: ":")
                
                if(Int(arrDriveTime![0])! > 24)
                {
                    strMessage = "Please enter drive time hrs less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDriveTime![0])! == 24 && Int(arrDriveTime![1])! > 0)
                {
                    strMessage = "Please enter drive time less than 24 hrs"
                    return strMessage
                }
                if(Int(arrDriveTime![1])! > 60)
                {
                    strMessage = "Please enter drive time minutes less than 60 minutes"
                    return strMessage
                }
            }
            else
            {
                if(Int(txtFld_DriveTime.text!)! > 24)
                {
                    strMessage = "Please enter drive time less than 24 hrs"
                    return strMessage
                }
            }
        }
        return strMessage
    }
    
    // MARK: - -----------------------------------Update Opportunity-----------------------------------
    func updateOpportunity()
    {
        FTIndicator.showProgress(withMessage: "Loading.....", userInteractionEnable: false)
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
            
        }
        
        var strUrl =  ""
        
        let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") as? String
        
        
        strUrl = strUrlMain! + "api/LeadNowAppToSalesProcess/EditOpportunity"
        
        let Url = String(format: strUrl)
        
        guard
            
            let serviceUrl = URL(string: Url) else { return }
        
        
        let dictJson = NSMutableDictionary()
        
        dictJson.setValue(strCompanyKey, forKey: "CompanyKey")
        
        dictJson.setValue(strCRMContactId, forKey: "CrmContactId")
        
        dictJson.setValue(strOpportunityId, forKey: "OpportunityId")
        
        dictJson.setValue(strEmployeeId, forKey: "EmployeeId")
        
        dictJson.setValue(((txtFld_FN.text == "") ? "" :  txtFld_FN.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "FirstName")
        
        dictJson.setValue(((txtFld_MN.text == "") ? "" :  txtFld_MN.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "MiddleName")
        
        dictJson.setValue(((txtFld_LN.text == "") ? "" :  txtFld_LN.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "LastName")
        
        dictJson.setValue(((txtFld_PrimaryPhone.text == "") ? "" : txtFld_PrimaryPhone.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "PrimaryPhone")
        
        dictJson.setValue(((txtFld_Cell.text == "") ? "" : txtFld_Cell.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "CellPhone")
        
        dictJson.setValue(((txtFld_SecondaryPhone.text == "") ? "" : txtFld_SecondaryPhone.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "SecondaryPhone")
        
        
        dictJson.setValue(((txtFld_PrimaryEmail.text == "") ? "" : txtFld_PrimaryEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "PrimaryEmail")
        
        dictJson.setValue(((txtFld_SecondaryEmail.text == "") ? "" : txtFld_SecondaryEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "SecondaryEmail")
        
        dictJson.setValue(( (txtFld_ProjectedAmount.text == "") ? "" : txtFld_ProjectedAmount.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ProjectedAmount")
        
        dictJson.setValue(strUrgencyId, forKey: "PriorityId")// urgency id
        
        dictJson.setValue(arrSourceId, forKey: "SourceIds")
        
        dictJson.setValue(strServiceCategoryID, forKey: "ServiceCategoryId")// primary reason of call
        
        dictJson.setValue(strServiceId, forKey: "PrimaryServiceId")// service id
        
        dictJson.setValue(strOpportunityType, forKey: "FlowType")// opportunity type
        
        dictJson.setValue(strSubmittedById, forKey: "SubmittedBy")
        
        dictJson.setValue(strCompanySizeId, forKey: "CompanySizeId")
        
        dictJson.setValue(strIndustryId, forKey: "IndustryId")
        
        dictJson.setValue(strFollowUpDate, forKey: "FollowUpDate")
        
        dictJson.setValue(strExpectedClosingDate, forKey: "CloseDate")
        
        dictJson.setValue(strInsideSalesPersonId, forKey: "InsideSalesPerson")
        
        dictJson.setValue(((txtView_Description.text == "") ? "" :  txtView_Description.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "Description")
        
        dictJson.setValue(((txtView_AccountAlert.text == "") ? "" :  txtView_AccountAlert.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "AccountAlert")
        
        dictJson.setValue(((btn_ScheduleNow.currentImage == UIImage(named: "check_box_2.png") ? "true" : "false")), forKey: "ScheduleOpportunity")
        
        dictJson.setValue(strFieldSalesPersonId, forKey: "FieldSalesPerson")
        
        dictJson.setValue(strScheduleDate, forKey: "ScheduleDate")
        
        dictJson.setValue(strTime, forKey: "ScheduleTime")
        
        
        dictJson.setValue(((txtFld_EstimatedDuration.text == "") ? "" :  txtFld_EstimatedDuration.text!.trimmingCharacters(in: CharacterSet.whitespaces)+":00"), forKey: "TotalEstimationTime")
        
        dictJson.setValue(((txtFld_DriveTime.text == "") ? "" :  txtFld_DriveTime.text!.trimmingCharacters(in: CharacterSet.whitespaces)+":00"), forKey: "DriveTime")
        
        
        dictJson.setValue(strServiceAddressSubType, forKey: "AddressSubType")
        
        dictJson.setValue(strServiceLocationId, forKey: "ServiceLocationId")
        
        dictJson.setValue(((txtFld_ServiceAddressLine1.text == "") ? "" :  txtFld_ServiceAddressLine1.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceAddress1")
        
        dictJson.setValue(((txtFld_ServiceAddressLine2.text == "") ? "" :  txtFld_ServiceAddressLine2.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceAddress2")
        
        dictJson.setValue("1", forKey: "ServiceCountryId")
        
        dictJson.setValue(strStateIdService, forKey: "ServiceStateId")
        
        dictJson.setValue(((txtFld_ServiceCity.text == "") ? "" :  txtFld_ServiceCity.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceCity")
        
        dictJson.setValue(((txtFld_ServiceZipCode.text == "") ? "" :  txtFld_ServiceZipCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceZipcode")
        
        dictJson.setValue(( (txtFld_ServiceCounty.text == "") ? "" :  txtFld_ServiceCounty.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceCounty")
        
        dictJson.setValue(((txtFld_ServiceSchoolDistrict.text == "") ? "" :  txtFld_ServiceSchoolDistrict.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceSchoolDistrict")
        
        dictJson.setValue(((txtFld_ServiceMapCode.text == "") ? "" :  txtFld_ServiceMapCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceMapCode")
        
        dictJson.setValue(((txtFld_ServiceGateCode.text == "") ? "" :  txtFld_ServiceGateCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceGateCode")
        
        dictJson.setValue(((txtView_ServiceNotes.text == "") ? "" :  txtView_ServiceNotes.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceNotes")
        
        dictJson.setValue(((txtView_ServiceDirection.text == "") ? "" :  txtView_ServiceDirection.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceDirection")
        
        dictJson.setValue(strServiceTaxCode, forKey: "ServiceTaxSysName")
        
        dictJson.setValue(((btn_IsTaxExempt.currentImage == UIImage(named: "check_box_2.png") ? "true" : "false")), forKey: "ServiceIsTaxExempt")
        
        dictJson.setValue(((txtFld_TaxExemption.text == "") ? "" :  txtFld_TaxExemption.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "ServiceTaxExemptionNo")
        
        dictJson.setValue(((btn_SameAsServiceAddress.currentImage == UIImage(named: "check_box_2.png") ? "true" : "false")), forKey: "BillingAddressSameAsServiceAddress")
        
        dictJson.setValue(strBillingLocationId, forKey: "BillingLocationId")
        
        dictJson.setValue(( (txtFld_BillingAddressLine1.text == "") ? "" :  txtFld_BillingAddressLine1.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingAddress1")
        
        dictJson.setValue(((txtFld_BillingAddressLine2.text == "") ? "" :  txtFld_BillingAddressLine2.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingAddress2")
        
        dictJson.setValue("1", forKey: "BillingCountryId")
        
        dictJson.setValue(strStateIdBilling, forKey: "BillingStateId")
        
        dictJson.setValue(( (txtFld_BillingCity.text == "") ? "" :  txtFld_BillingCity.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingCity")
        
        dictJson.setValue(((txtFld_BillingZipCode.text == "") ? "" :  txtFld_BillingZipCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingZipcode")
        
        dictJson.setValue(((txtFld_BillingCounty.text == "") ? "" :  txtFld_BillingCounty.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingCounty")
        
        dictJson.setValue(((txtFld_BillingSchoolDistrict.text == "") ? "" :  txtFld_BillingSchoolDistrict.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingSchoolDistrict")
        
        dictJson.setValue(((txtFld_BillingMapCode.text == "") ? "" :  txtFld_BillingMapCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)), forKey: "BillingMapCode")
        
        //confidenceValue ConfidenceLevel
        /*var strConfidence = lblProgreeConfidenceValue.text ?? "00"
         strConfidence = strConfidence.replacingOccurrences(of: "%", with: "")
         dictJson.setValue("\(strConfidence)", forKey: "ConfidenceValue")*/
        
        var strConfidence = lblProgreeConfidenceValue.text ?? "00"
        strConfidence = strConfidence.replacingOccurrences(of: "%", with: "")
        dictJson.setValue("\(strConfidence)", forKey: "Confidence")//ConfidenceLevel
        
        
        let parameterDictionary = dictJson
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dictJson, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        
        print(jsonString)
        
        var request = URLRequest(url: serviceUrl)
        
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.addValue(Global().strEmpBranchID(), forHTTPHeaderField: "BranchId")
        
        request.addValue(Global().strEmpBranchSysName(), forHTTPHeaderField: "BranchSysName")
        
        request.addValue(Global().strIsCorporateUser(), forHTTPHeaderField: "IsCorporateUser")
        
        request.addValue(Global().strClientTimeZone(), forHTTPHeaderField: "ClientTimeZone")
        
        
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.addValue(Global().getIPAddress(), forHTTPHeaderField: "VisitorIP")
        
        request.addValue("IOS", forHTTPHeaderField: "Browser")
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async{
                FTIndicator.dismissProgress()
                
            }
            
            if let response = response {
                
                print(response)
            }
            
            if let data = data
            {
                do
                {
                    let strResponse = String(decoding: data, as: UTF8.self)
                    
                    if((strResponse == "true") || (strResponse == "True") || (strResponse == "TRUE"))
                    {
                        
                        DispatchQueue.main.async{
                            
                            let alertController = UIAlertController(title: "Message", message: "Opportunity Updated Successfully.", preferredStyle:UIAlertController.Style.alert)
                            
                            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
                            { action -> Void in
                                
                                if nsud.bool(forKey: "fromNearByOpportunity") == true
                                {
                                    
                                }
                                else
                                {
                                    let userDef = UserDefaults.standard
                                    userDef.set(true, forKey: "isFromEditOpprtunityVC")
                                    userDef.set(true, forKey: "forEditOpportunity")
                                    userDef.synchronize()
                                }
                                
                                
                                self.dismiss(animated: false)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async{
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, please try again later.", viewcontrol: self)
                        }
                    }
                }
                catch
                {
                    print(error)
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Something went wrong, please try again later.", viewcontrol: self)
                }
            }
            }.resume()
    }
    
    func trimTextField()
    {
        txtFld_PrimaryEmail.text = txtFld_PrimaryEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        txtFld_SecondaryEmail.text = txtFld_SecondaryEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
}

// MARK: - -----------------------------------Text Field Delegates Methods-----------------------------------

extension EditOpportunityVC : UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtFld_EstimatedDuration {
            
            return validateTimeFormat(textField: textField, range: range, string: string as NSString)
            
        } else if textField == txtFld_DriveTime {
            
            return validateTimeFormat(textField: textField, range: range, string: string as NSString)
            
        } else if ( textField == txtFld_PrimaryPhone || textField == txtFld_SecondaryPhone || textField == txtFld_Cell ){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
        } else if ( textField == txtFld_PrimaryEmail || textField == txtFld_SecondaryEmail){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        } else if ( textField == txtFld_ProposedAmount || textField == txtFld_ProjectedAmount){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
            
        } else if ( textField == txtFld_ServiceZipCode || textField == txtFld_BillingZipCode){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
            
        } else if ( textField == txtFld_FN || textField == txtFld_MN || textField == txtFld_LN){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 100)
            
        } else if ( textField == txtFld_ServiceAddressLine1 || textField == txtFld_ServiceAddressLine2 || textField == txtFld_BillingAddressLine1 || textField == txtFld_BillingAddressLine2){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 500)
            
        } else if ( textField == txtFld_ServiceCity || textField == txtFld_ServiceCounty || textField == txtFld_ServiceSchoolDistrict || textField == txtFld_ServiceMapCode || textField == txtFld_ServiceGateCode){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 300)
            
        } else if ( textField == txtFld_BillingCity || textField == txtFld_BillingCounty || textField == txtFld_BillingSchoolDistrict || textField == txtFld_BillingMapCode){
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 300)
            
        }
        else if(textField == txtFld_TaxExemption)
        {
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 10)
        }
        else {
            
            return true
            
        }
    }
}
extension Dictionary where Key == String {
    
    subscript(caseInsensitive key: Key) -> Value? {
        get {
            if let k = keys.first(where: { $0.caseInsensitiveCompare(key) == .orderedSame }) {
                return self[k]
            }
            return nil
        }
        set {
            if let k = keys.first(where: { $0.caseInsensitiveCompare(key) == .orderedSame }) {
                self[k] = newValue
            } else {
                self[key] = newValue
            }
        }
    }
    
}
extension String
{
    //    func lowerCasedFirstLetter() -> String {
    //        return prefix(1).lowercased() + dropFirst()
    //    }
    //    func capitalizingFirstLetter() -> String {
    //        return prefix(1).capitalized + dropFirst()
    //    }
    
    //    mutating func capitalizeFirstLetter() {
    //        self = self.capitalizingFirstLetter()
    //    }
}
