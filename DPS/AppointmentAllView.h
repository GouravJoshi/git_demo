//
//  AppointmentAllView.h
//  DPS
//
//  Created by Rakesh Jain on 21/06/16.
//  Copyright © 2016 Saavan. All rights reserved.


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface AppointmentAllView : UIViewController<NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entitytotalWorkOrders;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
     NSEntityDescription *entitySalesInfo;
}

- (IBAction)actionBack:(id)sender;
- (IBAction)actionUpcoming:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblViewAllApponitments;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerTotalWorkOrders;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;

@end
