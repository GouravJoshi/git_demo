//
//  ClockInOutViewControlleriPad.m
//  DPS
//
//  Created by Rakesh Jain on 25/01/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ClockInOutViewControlleriPad.h"
#import "ClockInOutTableViewCell.h"
#import "AllImportsViewController.h"
#import "DPS-Swift.h"


@interface ClockInOutViewControlleriPad ()
{
    NSMutableArray *arrDate,*arrStartTime,*arrStopTime,*arrOfDepartMents,*arrOfSlots;
    UIView *viewBackGround,*viewBackGroundForNonBillableTime;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    NSString *strTitile,*strDate,*strForFromTime,*strForToTime,*strFromTime,*strToTime,*strFromTimeNonBillable,*strToTimeNonBillable;
    Global *global;
    
    //Start Timer Code
    NSTimer *stopTimer;
    NSDate *startDate;
    BOOL running;
    NSString *strTimerOnStop;
    
    //dfsgsdfgdfsgsdf
    int count;
    NSTimer *myTimer;
    
    NSString *strClockInOutId,*strTimeIn,*strTimeOut,*strWorkingStatusNew,*strReason,*strDescrption, *strEmployeeId,*strCompanyKeyNew,*strUserNameNew;
    NSString *strClockUrl,*strEmployeeNo;
    NSMutableArray *arrResponse;
    
    //NonBillable
    NSMutableArray *arrResponseNonBillable;
    NSDate *dateOnFirstTime;
    NSString*strDepartmentName;
    NSMutableDictionary *dictSlotData;
    
    BOOL isEditNonBillable;
    NSString *strNonBillabelTimeId;
    NSDictionary *dictSlotDataEdited;
    
    NSString *strDateEdited;
    
}@end

@implementation ClockInOutViewControlleriPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isEditNonBillable = NO;
    strNonBillabelTimeId = @"";
    strDateEdited = @"";
    
    arrResponse=[[NSMutableArray alloc]init];
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeId    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strCompanyKeyNew=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserNameNew    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    strClockUrl= [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"]];
    [self deleteClockRecordExceptToday];
    global=[[Global alloc]init];
    
    
    
    arrDate=[[NSMutableArray alloc]init];
    arrStartTime=[[NSMutableArray alloc]init];
    arrStopTime=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    _txtViewNonBillableDescription.text=@"";
    _txtViewNonBillableDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewNonBillableDescription.layer.borderWidth=1.0;
    _txtViewNonBillableDescription.layer.cornerRadius=5.0;
    
    _txtBreakDescription.text=@"";
    _txtBreakDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBreakDescription.layer.borderWidth=1.0;
    _txtBreakDescription.layer.cornerRadius=5.0;
    
    _txtBreakReason.text=@"";
    _txtBreakReason.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBreakReason.layer.borderWidth=1.0;
    _txtBreakReason.layer.cornerRadius=5.0;
    
    
    //Timer Code
    
    /* running = NO;
     startDate = [NSDate date];*/
    running = NO;
    count = 0;
    _lblTime.text = @"00:00:00";
    //End
    
    
    
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    // _lblTime.text = [dateFormatter stringFromDate:now];
    [self getDate];
    //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    
    
    
    
    NSString *strTitle;
    strTitle=_btnStartDay.currentTitle;
    
    if ([strTitle isEqualToString:@"Start Day"])
    {
        
        _constTakeBreak_H.constant=0;
    }
    else
    {
        _constTakeBreak_H.constant=50;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [DejalBezelActivityView removeView];
        [self getCurrentTimerOfClock];
    });
    [self getUpdatedRecords];
    _btnSelectDepartment.layer.cornerRadius = 4.0;
    _btnSelectDepartment.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _btnSelectDepartment.layer.borderWidth = 1.0;
    _btnSelectTimeSlot.layer.cornerRadius = 4.0;
    _btnSelectTimeSlot.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _btnSelectTimeSlot.layer.borderWidth = 1.0;
    _btnSelectDepartment.layer.cornerRadius = 4.0;
    _btnSelectDepartment.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _btnSelectDepartment.layer.borderWidth = 1.0;
    _btnSelectTimeSlot.layer.cornerRadius = 4.0;
    _btnSelectTimeSlot.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    _btnSelectTimeSlot.layer.borderWidth = 1.0;
    
    _viewForData.layer.cornerRadius = 8.0;
    _tvForListData.tableFooterView = [UIView new];
    _tvForListData.estimatedRowHeight = 50.0;
    
    
    NSArray *arrayBranch = [[defsLogindDetail objectForKey:@"LeadDetailMaster"] valueForKey:@"BranchMastersAll"];
    NSString *strEmployeeBranchId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchId"]];
    for (int k=0; k<arrayBranch.count; k++) {
        NSDictionary *dictBranchData = arrayBranch[k];
        NSString *strLocalBranchId=[NSString stringWithFormat:@"%@",[dictBranchData valueForKey:@"BranchMasterId"]];
        if ([strLocalBranchId isEqualToString:strEmployeeBranchId]) {
            [arrOfDepartMents removeAllObjects];
            arrOfDepartMents = [[NSMutableArray alloc]init];
            for(NSDictionary *dict in [[arrayBranch objectAtIndex:k] valueForKey:@"Departments"])
            {
                if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]]isEqualToString:@"1"])
                {
                    [arrOfDepartMents addObject:dict];
                }
            }
            break;
        }
    }
    
    
}


// Get Hours Config From Master

-(NSMutableArray*)getHoursConfiFromMaster :(NSString*)strCompanyKey :(NSString*)strDepartMentSysName :(NSString*)strWorkOrderAccNo :(NSString*)strWorkOrderAddressId :(NSString*)strWorkOrderAddressSubType{
    
    if ([strWorkOrderAddressId isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressId=@"";
        
    }
    if ([strWorkOrderAddressSubType isEqualToString:@"(null)"]) {
        
        strWorkOrderAddressSubType=@"";
        
    }
    if ([strWorkOrderAccNo isEqualToString:@"(null)"]) {
        
        strWorkOrderAccNo=@"";
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    //@"MasterAllMechanical"
    NSDictionary *dictMaster=[defs valueForKey:@"MasterAllMechanical"];
    NSArray *arrOfStatus=[dictMaster valueForKey:@"HourConfigMasterExtSerDc"];
    
    NSMutableArray *arrOfHoursConfig=[[NSMutableArray alloc]init];
    
    if (arrOfHoursConfig.count==0) {
        
        for (int k=0; k<arrOfStatus.count; k++) {
            
            NSDictionary *dictDataa=arrOfStatus[k];
            
            NSString *strLocalCompanyKey=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"CompanyKey"]];
            NSString *strLocalDeptSysName=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"DepartmentSysName"]];
            NSString *strLocalAccountNo=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AccountNumber"]];
            NSString *strLocalAddressId=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"ServiceAddressId"]];
            NSString *strLocalAddressSubType=[NSString stringWithFormat:@"%@",[dictDataa valueForKey:@"AddressType"]];
            
            
            if ([strLocalCompanyKey isEqualToString:strCompanyKey] && [strLocalDeptSysName isEqualToString:strDepartMentSysName] && [strLocalAccountNo isEqualToString:@""] && [strLocalAddressId isEqualToString:@""] && [strLocalAddressSubType isEqualToString:strWorkOrderAddressSubType]) {
                
                [arrOfHoursConfig addObject:dictDataa];
                
            }
        }
        
    }
    
    return arrOfHoursConfig;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-  ------------- TABLE VIEW DELEGAT METHOD -------------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblData)
    {
        return arrResponse.count;
        
    }else if (tableView==_tvForListData)
    {
        if (_tvForListData.tag == 1) {
            return arrOfDepartMents.count;
        }else{
            return arrOfSlots.count;
        }
        
    }
    else //tblDataWorkOrderDetails
    {
        return arrResponseNonBillable.count;
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblData)
    {
        static NSString *identifier=@"ClockInOutTableViewCell";
        ClockInOutTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell==nil)
        {
            cell=[[ClockInOutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrResponse objectAtIndex:indexPath.row];
        
        if(indexPath.row==0)
        {
            cell.lblDate.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkingStatus"]];
            //cell.lblDate.textAlignment=NSTextAlignmentCenter;
            cell.lblTimeIn.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
            //cell.lblTimeIn.textAlignment=NSTextAlignmentCenter;
            
            cell.lblTimeOut.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]];
            //cell.lblTimeOut.textAlignment=NSTextAlignmentCenter;
            
            
            cell.lblDate.font=[UIFont boldSystemFontOfSize:20];
            cell.lblTimeIn.font=[UIFont boldSystemFontOfSize:20];
            cell.lblTimeOut.font=[UIFont boldSystemFontOfSize:20];
            
        }
        else
        {
            cell.lblDate.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkingStatus"]];
            
            
            NSString *strTimeInCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]]];//TimeIn
            if (strTimeInCell.length==0||[strTimeInCell isEqualToString:@"<null>"]||[strTimeInCell isEqual:NULL])
            {
                strTimeInCell=@"";
            }
            
            cell.lblTimeIn.text=strTimeInCell;
            
            NSString *strTimeOutCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]]];
            if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
            {
                strTimeOutCell=@"";
            }
            cell.lblTimeOut.text=strTimeOutCell;
            
            cell.lblDate.font=[UIFont systemFontOfSize:18];
            cell.lblTimeIn.font=[UIFont systemFontOfSize:18];
            cell.lblTimeOut.font=[UIFont systemFontOfSize:18];
            
            //Temp
            
            NSString *strValue=[self calcaulteTimeInterval:strTimeInCell :strTimeOutCell];
            double secondsBetween=[strValue doubleValue];
            int hr = secondsBetween/60/60;
            
            int min =secondsBetween/60;
            int sec = secondsBetween;
            //int mSec = secondsBetween % 100;
            
            if (sec >= 60) {
                sec = sec % 60;
            }
            if (min >= 60) {
                min = min % 60;
            }
            NSString *strDuration;
            strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
            //cell.lblDate.text=strDuration;
            //End
        }
        return cell;
    }else if (tableView==_tvForListData)
    {
        static NSString *identifier=@"DepartmentORSlotCell";
        ClockInOutTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell==nil)
        {
            cell=[[ClockInOutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (_tvForListData.tag == 1)
        { // For DepartMent
            NSDictionary *dict=[arrOfDepartMents objectAtIndex:indexPath.row];
            cell.lblTitleOnTable.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
        }
        else
        {  // For Slot
            NSDictionary *dict=[arrOfSlots objectAtIndex:indexPath.row];
            
            cell.lblTitleOnTable.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            
            if ([NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]].length == 0 || [[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]] isEqualToString:@""])
            {
                
                NSString *str1 = [self string24HrDateTo12HrStringAM_PM:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromTime"]]];
                
                NSString *str2 = [self string24HrDateTo12HrStringAM_PM:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToTime"]]];
                
                cell.lblTitleOnTable.text = [NSString stringWithFormat:@"%@ - %@",str1,str2];
            }
            else
            {
                cell.lblTitleOnTable.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];
            }
        }
        
        
        return  cell;
    }
    else
    {
        static NSString *identifier=@"WorkOrderDetail";
        ClockInOutTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell==nil)
        {
            cell=[[ClockInOutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrResponseNonBillable objectAtIndex:indexPath.row];
        
        if(indexPath.row==0)
        {
            cell.lblWONo.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WO#"]];
            //cell.lblWONo.textAlignment=NSTextAlignmentCenter;
            cell.lblTimeIn.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
            //cell.lblTimeIn.textAlignment=NSTextAlignmentCenter;
            
            cell.lblTimeOut.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]];
            //cell.lblTimeOut.textAlignment=NSTextAlignmentCenter;
            
            
            cell.lblWONo.font=[UIFont boldSystemFontOfSize:20];
            cell.lblTimeIn.font=[UIFont boldSystemFontOfSize:20];
            cell.lblTimeOut.font=[UIFont boldSystemFontOfSize:20];
            
        }
        else
        {
            NSString *strWoNonBillable;
            strWoNonBillable=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WO#"]];
            if (strWoNonBillable.length==0||[strWoNonBillable isEqualToString:@"<null>"]||[strWoNonBillable isEqual:NULL]||[strWoNonBillable isEqualToString:@"(null)"])
            {
               // strWoNonBillable=@"WO #";
                strWoNonBillable=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]];

            }
            
            cell.lblWONo.text=strWoNonBillable;
            
            //NSString *strTimeInCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];
            NSString *strTimeInCell=[self changeDateToFormattedDateNewLocal:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];

            
            if (strTimeInCell.length==0||[strTimeInCell isEqualToString:@"<null>"]||[strTimeInCell isEqual:NULL])
            {
                strTimeInCell=@"";
                
                
            }
            if ([strTimeInCell isEqualToString:@"(null)"])
            {
                strTimeInCell=[dict valueForKey:@"TimeIn"];
            }
            cell.lblTimeIn.text=strTimeInCell;
            
            //NSString *strTimeOutCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
            
             NSString *strTimeOutCell=[self changeDateToFormattedDateNewLocal:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
            
            if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
            {
                strTimeOutCell=@"";
                
            }
            if ([strTimeOutCell isEqualToString:@"(null)"])
            {
                strTimeOutCell=[dict valueForKey:@"TimeOut"];
            }
            cell.lblTimeOut.text=strTimeOutCell;
            
            if([UIScreen mainScreen].bounds.size.height<600)
            {
                cell.lblWONo.font=[UIFont systemFontOfSize:9];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:9];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:9];
                
            }
            else
            {
                cell.lblWONo.font=[UIFont systemFontOfSize:18];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:18];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:18];
            }
            
        }
        return cell;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblData)
    {
        if (indexPath.row==0)
        {
            return 65;
        }
        else
        {
            return tableView.rowHeight;
        }
    }else if (tableView==_tvForListData)
    {
        return  UITableViewAutomaticDimension;
    }
    else
    {
        if (indexPath.row==0)
        {
            return 65;
        }
        else
        {
            return tableView.rowHeight;
        }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _tvForListData) {
        if (_tvForListData.tag == 1) { // For Department
            
            NSDictionary *dict=[arrOfDepartMents objectAtIndex:indexPath.row];
            [_btnSelectDepartment setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]] forState:UIControlStateNormal];
            strDepartmentName = [NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]];
            [_viewForTableView removeFromSuperview];
            
            [_btnSelectTimeSlot setTitle:@"Select" forState:UIControlStateNormal];
            
        }
        else
        { // For Slot
            NSDictionary *dict=[arrOfSlots objectAtIndex:indexPath.row];
            
            if ([NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]].length == 0 || [[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]] isEqualToString:@""])
            {
                NSString *str1 = [self string24HrDateTo12HrStringAM_PM:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromTime"]]];
                
                NSString *str2 = [self string24HrDateTo12HrStringAM_PM:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToTime"]]];
                
                
                 [_btnSelectTimeSlot setTitle:[NSString stringWithFormat:@"%@ - %@",str1,str2] forState:UIControlStateNormal];
            }
            else
            {
                 [_btnSelectTimeSlot setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]] forState:UIControlStateNormal];
            }
            
           
            
            
            dictSlotData = [NSMutableDictionary new];
            dictSlotData = (NSMutableDictionary*)dict;
            [_viewForTableView removeFromSuperview];
            
        }
    }
}
#pragma mark-  ------------- ALL ACTION METHODS -------------


- (IBAction)actionOnStartDay:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        BOOL chkForCurrentDate;
        chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
        if (chkForCurrentDate==YES)
        {
            NSString *strTitle;
            strTitle=_btnStartDay.currentTitle;
            
            if (running == NO)
            {
                
                [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                
                running = YES;
                
                if (myTimer == nil)
                {
                    
                    //myTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                    
                    
                }
                
                strTimeIn=[self getCurrentTime];
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self apiForClockInOutDetail:@"WorkingTime" TimeIn:strTimeIn TimeOut:@""];
                    
                });
                
                _constTakeBreak_H.constant=50;
            }
            else
            {
                // [_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
                [_btnStartDay setEnabled:NO];
                _constTakeBreak_H.constant=0;
                strTimeOut=[self getCurrentTime];
                [self fetchClockInOutDetailCoreData];
                [self updateClockInOutDetailCoreData];
                strWorkingStatusNew=@"EndDay";
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [DejalBezelActivityView removeView];
                    [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
                });
                
                [self stopTimer];
                
            }
        }
        else
        {
            [self endDayFunctionality];
        }
    }//
}
- (IBAction)actionOnTakeBreak:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        
    }
    else
    {
        BOOL chkForCurrentDate;
        chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
        if (chkForCurrentDate==YES)
        {
            NSString *strTitle;
            strTitle=_btnBreak.currentTitle;
            
            if ([strTitle isEqualToString:@"Take Break"])
            {
                viewBackGroundForNonBillableTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                _viewForBreak.frame=CGRectMake(_viewForBreak.frame.origin.x, ([UIScreen mainScreen].bounds.size.height-_viewForBreak.frame.size.height)/2, viewBackGroundForNonBillableTime.frame.size.width, _viewForBreak.frame.size.height);
                viewBackGroundForNonBillableTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
                [viewBackGroundForNonBillableTime addSubview:_viewForBreak];
                [self.view addSubview:viewBackGroundForNonBillableTime];
                
            }
            else
            {
                _btnStartDay.enabled=YES;
                
                strTimeOut=[self getCurrentTime];
                [_btnBreak setTitle:@"Take Break" forState:UIControlStateNormal];
                [self fetchClockInOutDetailCoreData];
                [self updateClockInOutDetailCoreData];
                
                strWorkingStatusNew=@"WorkingTime";
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[DejalBezelActivityView removeView];
                    
                    [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
                    
                });
                //Start Time
                
                if (running == NO)
                {
                    running = YES;
                    
                    if (myTimer == nil)
                    {
                        
                        //myTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        
                        
                    }
                }
                
                
            }
        }
        else
        {
            [self endDayFunctionality];
        }
    }
    
}
- (IBAction)actionOnBtnFromTime:(id)sender
{
    strTitile=@"SELECT TIME";
    strForFromTime=@"fromTime";
    [self addPickerViewDateTo];
}
- (IBAction)actionOnBtnToTime:(id)sender
{
    strTitile=@"SELECT TIME";
    strForFromTime=@"ToTime";
    
    [self addPickerViewDateTo];
}
- (IBAction)actionOnNonBillableTime:(id)sender
{
    
    [self displayNonBillableView];
}
-(void)displayNonBillableView
{
    viewBackGroundForNonBillableTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _viewForAddNonBillableTime.frame=CGRectMake(viewBackGroundForNonBillableTime.frame.origin.x, ([UIScreen mainScreen].bounds.size.height-_viewForAddNonBillableTime.frame.size.height)/2, viewBackGroundForNonBillableTime.frame.size.width, _viewForAddNonBillableTime.frame.size.height);
    viewBackGroundForNonBillableTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [viewBackGroundForNonBillableTime addSubview:_viewForAddNonBillableTime];
    
    [self.view addSubview:viewBackGroundForNonBillableTime];
}
- (IBAction)actionOnSaveForNonBillableTime:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setDateFormat:@"hh:mm:a"];
    
    [dateFormat setDateFormat:@"hh:mm a"];

    
    NSDate *date1 = [dateFormat dateFromString:strFromTime];
    NSDate *date2 = [dateFormat dateFromString:strToTime];
    if ([strDepartmentName length] == 0)
    {
        [global AlertMethod:@"Alert" :@"Please select department"];
    }
    else if ([_btnSelectTimeSlot.currentTitle isEqualToString:@"Select"] || _btnSelectTimeSlot.titleLabel.text.length==0)//([dictSlotData count] == 0)
    {
        [global AlertMethod:@"Alert" :@"Please select slot"];
    }
    else if (strFromTime.length==0||[strFromTime isEqualToString:@""]||[_btnFromTime.currentTitle isEqualToString:@"From Time"])
    {
        [global AlertMethod:@"Alert" :@"Please choose From Time"];
    }
    else if (strToTime.length==0||[strToTime isEqualToString:@""]||[_btnToTime.currentTitle isEqualToString:@"To Time"])
    {
        [global AlertMethod:@"Alert" :@"Please choose To Time"];
    }
    else if ([_txtViewNonBillableDescription.text isEqualToString:@""])
    {
        [global AlertMethod:@"Alert" :@"Please add descrption"];
    }
    else if ([date1 compare:date2] == NSOrderedDescending)
    {
        [global AlertMethod:@"Alert" :@"To Time should be greater than From Time"];
    }
    else if ([date1 compare:date2] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert" :@"From Time and TO Time can not be same "];
    }
    else
    {
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [global AlertMethod:@"ALert" :ErrorInternetMsg];
            [DejalActivityView removeView];
        }
        else
        {
            
            if (arrResponseNonBillable.count==1)
            {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [DejalBezelActivityView removeView];
                    [self apiForNonBillableTimeDetail];
                     //[global displayAlertController:Alert :@"Time Range not exist." :self];
                    if (isEditNonBillable == YES)
                    {
                        isEditNonBillable = NO;
                    }
                    
                    strFromTime=@"";
                    strToTime=@"";
                    _txtViewNonBillableDescription.text=@"";
                    [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
                    [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
                    [_btnSelectDepartment setTitle:@"Select" forState:UIControlStateNormal];
                    [_btnSelectTimeSlot setTitle:@"Select" forState:UIControlStateNormal];
                });
                

                [viewBackGroundForNonBillableTime removeFromSuperview];
                [_viewForAddNonBillableTime removeFromSuperview];
                
              
                
                
            }
            else
            {
                
                if ([self isValidTimeToAdd] == YES)
                {
                    
                    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [DejalBezelActivityView removeView];
                        [self apiForNonBillableTimeDetail];
                        //[global displayAlertController:Alert :@"Time Range not exist." :self];
                        strFromTime=@"";
                        strToTime=@"";
                        _txtViewNonBillableDescription.text=@"";
                        [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
                        [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
                        [_btnSelectDepartment setTitle:@"Select" forState:UIControlStateNormal];
                        [_btnSelectTimeSlot setTitle:@"Select" forState:UIControlStateNormal];
                        
                    });
                    

                    [viewBackGroundForNonBillableTime removeFromSuperview];
                    [_viewForAddNonBillableTime removeFromSuperview];
                    
                    /*if (isEditNonBillable == YES)
                    {
                        isEditNonBillable = NO;
                    }*/
                }
                else
                {
                    
                    [global displayAlertController:Alert :@"Time Range already exist." :self];
                    
                }
                
            }
            
        }
    }
}

- (IBAction)actionOnCancelForNonBillableTime:(id)sender
{
    dictSlotData = [NSMutableDictionary new];
    strDepartmentName = @"";
    strFromTime=@"";
    strToTime=@"";
    [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
    [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
    _txtViewNonBillableDescription.text=@"";
    [_btnSelectDepartment setTitle:@"Select" forState:UIControlStateNormal];
    [_btnSelectTimeSlot setTitle:@"Select" forState:UIControlStateNormal];
    [viewBackGroundForNonBillableTime removeFromSuperview];
    [_viewForAddNonBillableTime removeFromSuperview];
    
    if (isEditNonBillable == YES)
    {
        isEditNonBillable = NO;
    }
    
}
- (IBAction)actionOnSaveForBreak:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [_btnBreak setTitle:@"End Break" forState:UIControlStateNormal];
        _btnStartDay.enabled=NO;
        strReason=_txtBreakReason.text;
        strDescrption=_txtBreakDescription.text;
        
        strTimeOut=[self getCurrentTime];
        [self fetchClockInOutDetailCoreData];
        [self updateClockInOutDetailCoreData];
        strWorkingStatusNew=@"BreakTime";
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //[DejalBezelActivityView removeView];
            
            [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
            
        });
        [self stopTimer];
        strReason=@"";
        strDescrption=@"";
        _txtBreakDescription.text=@"";
        [viewBackGroundForNonBillableTime removeFromSuperview];
        [_viewForBreak removeFromSuperview];
    }
    
}

- (IBAction)actionOnCancelForBreak:(id)sender
{
    strReason=@"";
    strDescrption=@"";
    _txtBreakDescription.text=@"";
    [viewBackGroundForNonBillableTime removeFromSuperview];
    [_viewForBreak removeFromSuperview];
    
}
- (IBAction)actionOnBack:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromClockInOut"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- ---------------  ALL API METHODS ---------------

-(void)apiForClockInOutDetail: (NSString*)strWorkingStatus TimeIn:(NSString *)timeIn TimeOut:(NSString*)timeOut
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeClockInOut/AddUpdateEmployeeClockInOutExt"];
        
        if([strClockInOutId isEqual:nil]|| strClockInOutId.length==0)
        {
            strClockInOutId=@"";
        }
        if([_btnStartDay.currentTitle isEqualToString:@"End Break"])
        {
            strClockInOutId=@"";
            
        }
        if([strReason isEqual:nil]|| strReason.length==0)
        {
            strReason=@"";
        }
        if([strDescrption isEqual:nil]|| strDescrption.length==0)
        {
            strDescrption=@"";
        }
        
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"EmployeeClockInOutId",
             @"EmployeeId",
             @"WorkingStatus",
             @"TimeIn",
             @"TimeOut",
             @"Reason",
             @"Description",
             @"IsActive",
             @"CreatedBy",
             @"CreatedDate",
             @"CompanyKey",
             nil];
        value=[NSArray arrayWithObjects:
               strClockInOutId,
               strEmployeeId,
               strWorkingStatus,
               timeIn,
               timeOut,
               strReason,
               strDescrption,
               @"true",
               strEmployeeId,
               [global strCurrentDate],
               strCompanyKeyNew,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
        
        //End
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                NSLog(@"Clock Reponse %@",dictionary);
            }
            
            strClockInOutId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeClockInOutId"]];
            strTimeIn=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeIn"]];
            strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];
            
            NSString *statusWorkingTime=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
            
            /*if([_btnStartDay.currentTitle isEqualToString:@"End Day"])
             {
             
             }
             else
             {*/
            [self saveClockInOutDetailCoreData :strClockInOutId :statusWorkingTime];
            [self getUpdatedRecords];
            
            // }
            
        }
        [DejalActivityView removeView];
    }
}
-(void)apiForNonBillableTimeDetail
{
    if (isEditNonBillable== NO)
    {
        strNonBillabelTimeId = @"";
    }
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        NSArray *key,*value;
        NSString *strUrl;
        if (isEditNonBillable == NO)
        {
            NSString *strTitleTemp =[dictSlotData valueForKey:@"Title"];
            
            if ([strTitleTemp isEqualToString:@"Regular"])
            {
                
            }
            else if ([strTitleTemp isEqualToString:@"Holiday"])
            {
                
            }
            else
            {
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"HH:mm:ss"];
                NSDate *dateFromTimeTemp = [dateFormat dateFromString:[dictSlotData valueForKey:@"FromTime"]];
                NSDate *dateToTimeTemp = [dateFormat dateFromString:[dictSlotData valueForKey:@"ToTime"]];
                
                NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
                [dateFormatNew setDateFormat:@"hh:mm a"];
                NSString *strFromTimeTemp = [dateFormatNew stringFromDate:dateFromTimeTemp];
                NSString *strToTimeTemp = [dateFormatNew stringFromDate:dateToTimeTemp];
                strFromTimeTemp=[strFromTimeTemp lowercaseString];
                strToTimeTemp=[strToTimeTemp lowercaseString];
                strTitleTemp = [NSString stringWithFormat:@"%@ - %@",strFromTimeTemp,strToTimeTemp];
                
            }
            
            strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeNonBillableTime/AddUpdateEmployeeNonBillableTimeExt"];
            
            key=[NSArray arrayWithObjects:
                 @"EmployeeNonBillableTimeId",
                 @"EmployeeId",
                 @"FromDate",
                 @"ToDate",
                 @"Title",
                 @"Description",
                 @"IsActive",
                 @"EmployeeDc",
                 @"CreatedDate",
                 @"CreatedBy",
                 @"ModifiedDate",
                 @"ModifiedBy",
                 @"DepartmentSysName",
                 @"TimeSlot",
                 @"TimeSlotTitle",
                 @"QBPayrollItemName",
                 nil];
            value=[NSArray arrayWithObjects:
                   @"",
                   strEmployeeId,
                   strFromTimeNonBillable,
                   strToTimeNonBillable,
                   @"Nonbillable Reason",
                   _txtViewNonBillableDescription.text,
                   @"true",
                   @"",
                   [global strCurrentDate],
                   strEmployeeId,
                   @"",
                   @"",
                   strDepartmentName,
                   strTitleTemp,
                   [NSString stringWithFormat:@"%@",[dictSlotData valueForKey:@"Title"]],
                   [NSString stringWithFormat:@"%@",[dictSlotData valueForKey:@"QBPayrollItemName"]],
                   nil];
        }
        else //EDITED
        {
            
            if (strFromTimeNonBillable.length>15)
            {
                strFromTimeNonBillable = [self stringDateToAM_PM:strFromTimeNonBillable];
                
            }
            if (strToTimeNonBillable.length>15)
            {
                strToTimeNonBillable = [self stringDateToAM_PM:strToTimeNonBillable];
                
            }
            
            
            NSString *strTitleTemp =[dictSlotData valueForKey:@"Title"];
            NSString *strTimeSlot,*strTimeSlotTitle,*strQBPayrollItemName;
            
            if ([dictSlotData isKindOfClass:[NSMutableDictionary class]] || [dictSlotData isKindOfClass:[NSDictionary class]] || dictSlotData.count>0)
            {
                if ([strTitleTemp isEqualToString:@"Regular"])
                {
                    
                }
                else if ([strTitleTemp isEqualToString:@"Holiday"])
                {
                    
                }
                else
                {
                    
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"HH:mm:ss"];
                    NSDate *dateFromTimeTemp = [dateFormat dateFromString:[dictSlotData valueForKey:@"FromTime"]];
                    NSDate *dateToTimeTemp = [dateFormat dateFromString:[dictSlotData valueForKey:@"ToTime"]];
                    
                    NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
                    [dateFormatNew setDateFormat:@"hh:mm a"];
                    NSString *strFromTimeTemp = [dateFormatNew stringFromDate:dateFromTimeTemp];
                    NSString *strToTimeTemp = [dateFormatNew stringFromDate:dateToTimeTemp];
                    
                    strFromTimeTemp=[strFromTimeTemp lowercaseString];
                    strToTimeTemp=[strToTimeTemp lowercaseString];
                    strTitleTemp = [NSString stringWithFormat:@"%@ - %@",strFromTimeTemp,strToTimeTemp];
                    
                    
                    
                }
                
                strTimeSlot = strTitleTemp;
                strTimeSlotTitle =[NSString stringWithFormat:@"%@",[dictSlotData valueForKey:@"Title"]];
                
                strQBPayrollItemName =  [NSString stringWithFormat:@"%@",[dictSlotData valueForKey:@"QBPayrollItemName"]];
            }
            else
            {
                strTimeSlot = [NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"TimeSlot"]];
                strTimeSlotTitle =[NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"TimeSlotTitle"]];
                strQBPayrollItemName = [NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"QBPayrollItemName"]];
            }
            
            strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeNonBillableTime/AddUpdateEmployeeNonBillableTimeExt"];
            //NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"EmployeeNonBillableTimeId",
                 @"EmployeeId",
                 @"FromDate",
                 @"ToDate",
                 @"Title",
                 @"Description",
                 @"IsActive",
                 @"EmployeeDc",
                 @"CreatedDate",
                 @"CreatedBy",
                 @"ModifiedDate",
                 @"ModifiedBy",
                 @"DepartmentSysName",
                 @"TimeSlot",
                 @"TimeSlotTitle",
                 @"QBPayrollItemName",
                 nil];
            value=[NSArray arrayWithObjects:
                   [NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"EmployeeNonBillableTimeId"]],
                   [NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"EmployeeId"]],
                   strFromTimeNonBillable,
                   strToTimeNonBillable,
                   @"Nonbillable Reason",
                   _txtViewNonBillableDescription.text,
                   @"true",
                   @"",
                   [global strCurrentDate],
                   [NSString stringWithFormat:@"%@",[dictSlotDataEdited valueForKey:@"EmployeeId"]],
                   @"",
                   @"",
                   strDepartmentName,
                   strTimeSlot,
                   strTimeSlotTitle,
                   strQBPayrollItemName,
                   nil];
        }
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR Non Billable CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
        
        //End
        
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                NSLog(@"Non Billable Clock Reponse %@",dictionary);
            }
            
            [self getUpdatedRecords];
        }
        [DejalActivityView removeView];
        isEditNonBillable = NO;
    }
}

-(void)getCurrentTimerOfClock
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        //http://service.hrms.com/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=Titan&employeeNo=325350&filterDate=1/10/2018
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeClockInOut/EmployeeClockInOutForDisplayExt"];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"EmployeeId",
             nil];
        value=[NSArray arrayWithObjects:
               strEmployeeId,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR CURRENT TIMER CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
        
        //End
        
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                
                NSLog(@"CURRENT TIMER Clock Reponse %@",dictionary);
                NSString *strCurrentWorkingStatus,*strCurrentWorkingTimeInSeconds;
                strCurrentWorkingStatus=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
                strCurrentWorkingTimeInSeconds=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingTimeInSeconds"]];
                strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];
                
                //Temp
                int countStart=0;
                
                NSArray *arrTemp=[dictionary valueForKey:@"DayWiseClockInOutExtSerDcs"];
                if ([arrTemp isKindOfClass:[NSArray class]])
                {
                    for(int i=0;i<arrTemp.count;i++)
                    {
                        NSDictionary *dict=[arrTemp objectAtIndex:i];
                        NSString *strDate1,*strDate2;
                        strDate1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeInStr"]];
                        strDate2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOutStr"]];
                        
                        NSString *strValue=[self calcaulteTimeIntervalNew:strDate1 :strDate2];
                        double secondsBetween=[strValue doubleValue];
                        countStart=countStart+secondsBetween;
                        
                    }
                }
                
                //End
                //Current Time
                //count=[strCurrentWorkingTimeInSeconds intValue];
                count=countStart;
                
                int hr = count/60/60;
                int min =count/60;
                int sec = count;
                if (sec >= 60) {
                    sec = sec % 60;
                }
                if (min >= 60) {
                    min = min % 60;
                }
                _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
                //End
                
                
                if ([strCurrentWorkingStatus isEqualToString:@"<null>"]||[strCurrentWorkingStatus isEqualToString:@"0"])
                {
                    [_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
                }
                else if ([strCurrentWorkingStatus isEqualToString:@"WorkingTime"])
                {
                    if(strTimeOut.length>10)
                    {
                        _btnStartDay.enabled=NO;
                        
                        [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                        
                    }
                    
                    else
                    {
                        _btnStartDay.enabled=YES;
                        [_btnBreak setTitle:@"Take Break" forState:UIControlStateNormal];
                        [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                        
                        running = YES;
                        if (myTimer == nil)
                        {
                            myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        }
                        
                    }
                    
                }
                else if ([strCurrentWorkingStatus isEqualToString:@"BreakTime"])
                {
                    
                    [_btnBreak setTitle:@"End Break" forState:UIControlStateNormal];
                    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                    _btnStartDay.enabled=NO;
                    
                    /*running = YES;
                     if (myTimer == nil)
                     {
                     myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                     }*/
                    
                }
                else if ([strCurrentWorkingStatus isEqualToString:@"EndDay"])
                {
                    _btnStartDay.enabled=NO;
                    
                    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                }
                NSString *strTitle;
                strTitle=_btnStartDay.currentTitle;
                
                if ([strTitle isEqualToString:@"Start Day"])
                {
                    
                    _constTakeBreak_H.constant=0;
                }
                else
                {
                    _constTakeBreak_H.constant=50;
                    
                    if ([strTitle isEqualToString:@"End Day"])
                    {
                        if(strTimeOut.length>10)
                        {
                            _constTakeBreak_H.constant=0;
                            
                        }
                    }
                    
                }
                NSString *strClockId;
                strClockId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeClockInOutId"]];
                strEmployeeId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeId"]];
                strWorkingStatusNew=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
                strTimeIn=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeIn"]];
                if ([strTimeIn isEqualToString:@"<null>"])
                {
                    strTimeIn=@"";
                }
                strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];
                if ([strTimeOut isEqualToString:@"<null>"])
                {
                    strTimeOut=@"";
                }
                strReason=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"Reason"]];
                if ([strReason isEqualToString:@"<null>"])
                {
                    strReason=@"";
                }
                strDescrption=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"Description"]];
                if ([strDescrption isEqualToString:@"<null>"])
                {
                    strDescrption=@"";
                }
                // Seperate Code
                
                if ([strClockId isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    [self fetchAndUpdateClockInOutDetailForCurrentTimeCalculation:strEmployeeId:strClockId];
                    
                }
                
                
            }
            
        }
        [DejalActivityView removeView];
    }
}
-(void)getUpdatedClockInOutDate
{
    //http://thrs.stagingsoftware.com//api/EmployeeClockInOut/GetClockInOutByEmployeeNoExt/?companyKey=Automation&employeeNo=2702545469&filterDate=1/22/2018
    
    NSString *strUrlwithString=[NSString stringWithFormat:@"%@/api/EmployeeClockInOut/GetClockInOutByEmployeeNoExt/?companyKey=%@&employeeNo=%@&&filterDate=%@",strClockUrl,strCompanyKeyNew,strEmployeeNo,[self getDateToday]];
    
    NSString *strType=@"";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrlwithString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    //End
    
    @try
    {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict;
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if(ResponseDict.count==0)
        {
        }
        else
        {
            arrResponse=[[NSMutableArray alloc]init];
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"WorkingStatus",
                 @"TimeIn",
                 @"TimeOut",
                 nil];
            value=[NSArray arrayWithObjects:
                   @"Working Status",
                   @"Time In",
                   @"Time Out",
                   nil];
            NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
            
            NSLog(@"Response clock data >> %@",ResponseDict);
            [arrResponse addObject:dict];
            NSArray *tempArr=(NSArray*)ResponseDict;
            for (int i=0; i<tempArr.count; i++)
            {
                [arrResponse addObject:[tempArr objectAtIndex:i]];
            }
        }
        [DejalBezelActivityView removeView];
        NSLog(@"Response Final clock data >> %@",arrResponse);
        [_tblData reloadData];
        
        _const_TblClockIn_H.constant=50*arrResponse.count+20;
        _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
        
        
        _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
        [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
        
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        
    }
    @finally
    {
        
    }
}
-(void)getDetailNonBillabelTime
{
    //http://service.hrms.com/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=Titan&employeeNo=325350&filterDate=1/10/2018
    
    
    NSString *strUrlwithString=[NSString stringWithFormat:@"%@/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=%@&employeeNo=%@&&filterDate=%@",strClockUrl,strCompanyKeyNew,strEmployeeNo,[self getDateToday]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrlwithString]
 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];
    
    //End
    
    @try
    {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict;
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if(ResponseDict.count==0)
        {
        }
        else
        {
            arrResponseNonBillable=[[NSMutableArray alloc]init];
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"WO#",
                 @"TimeIn",
                 @"TimeOut",
                 nil];
            value=[NSArray arrayWithObjects:
                   @"WO #",
                   @"Time In",
                   @"Time Out",
                   nil];
            NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
            
            NSLog(@"Response clock data >> %@",ResponseDict);
            [arrResponseNonBillable addObject:dict];
            NSArray *tempArr=(NSArray*)ResponseDict;
            for (int i=0; i<tempArr.count; i++)
            {
                [arrResponseNonBillable addObject:[tempArr objectAtIndex:i]];
            }
        }
        [self fetchTodaysCompleteWorkOrder];
        [DejalBezelActivityView removeView];
        NSLog(@"Response Final clock data >> %@",arrResponseNonBillable);
        
        [_tblData reloadData];
        [_tblDataWorkOrderDetails reloadData];
        
        _const_TblClockIn_H.constant=50*arrResponse.count+20;//50*
        _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
        
        
        _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
        [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
        
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        
    }
    @finally
    {
        
    }
}

#pragma mark- ---------- CORE DATA METHODS ---------------

-(void)saveClockInOutDetailCoreData:(NSString*)strClockId :(NSString*)strWorkingStatus
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityClockInOutDetail=[NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    
    ClockInOutDetail *objClockInOutDetail = [[ClockInOutDetail alloc]initWithEntity:entityClockInOutDetail insertIntoManagedObjectContext:context];
    
    objClockInOutDetail.employeeId=strEmployeeId;
    objClockInOutDetail.companyKey=strCompanyKeyNew;
    objClockInOutDetail.userName=strUserNameNew;
    objClockInOutDetail.employeeClockInOutId=strClockId;
    objClockInOutDetail.workingStatus=strWorkingStatus;
    objClockInOutDetail.timeIn=strTimeIn;
    objClockInOutDetail.timeOut=strTimeOut;
    objClockInOutDetail.reason=strReason;
    objClockInOutDetail.descriptionDetail=strDescrption;
    objClockInOutDetail.isActive=@"true";
    objClockInOutDetail.createdBy=strEmployeeId;
    objClockInOutDetail.createdDate=[global strCurrentDate];
    
    NSError *error1;
    [context save:&error1];
}
-(void)updateClockInOutDetailCoreData
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeClockInOutId=%@",strClockInOutId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeClockInOutId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        [matches setValue:strTimeOut forKey:@"timeOut"];
        [context save:&error1];
    }
}

-(void)fetchClockInOutDetailCoreData
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=[arrAllObj lastObject];
        strClockInOutId=[NSString stringWithFormat:@"%@",[matches valueForKey:@"employeeClockInOutId"]];
        strTimeIn=[NSString stringWithFormat:@"%@",[matches valueForKey:@"timeIn"]];
        strWorkingStatusNew=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workingStatus"]];
        //[matches setValue:[self getCurrentTime] forKey:@"timeOut"];
        //[context save:&error1];
    }
}
-(void)fetchAndUpdateClockInOutDetailForCurrentTimeCalculation:(NSString*)strEmpIdId :(NSString*)strClockId
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@ AND employeeClockInOutId =%@",strEmpIdId,strClockId];//employeeClockInOutId
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [self saveClockInOutDetailCoreData:strClockId :strWorkingStatusNew];
    }
    else
    {
        matches=arrAllObj[0];
        
        [matches setValue:strTimeIn forKey:@"timeIn"];
        [matches setValue:strTimeOut forKey:@"timeOut"];
        [matches setValue:strReason forKey:@"reason"];
        [matches setValue:strDescrption forKey:@"descriptionDetail"];
        [matches setValue:strWorkingStatusNew forKey:@"workingStatus"];
        [context save:&error1];
    }
    
}
-(void)deleteClockRecordExceptToday
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];//employeeClockInOutId
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        //Temp
        NSString *strCurrentDate;
        strCurrentDate=[NSString stringWithFormat:@"%@",[data valueForKey:@"createdDate"]];
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        NSDate *date=[dateFormatter dateFromString:strCurrentDate];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString *strDateData=[dateFormatter stringFromDate:date];
        
        NSString *strCurrentDateTime=[dateFormatter stringFromDate:[NSDate date]];
        if (![strDateData isEqualToString:strCurrentDateTime])
        {
            NSLog(@"Previous date data deleted");
            [context deleteObject:data];
            // break;
        }
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)fetchTodaysCompleteWorkOrder
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName=%@ AND employeeNo=%@",strUserNameNew,strEmployeeNo];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObjWorkOrder = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObjWorkOrder.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjWorkOrder.count; k++)
        {
            matchesWorkOrder=arrAllObjWorkOrder[k];
            NSString *strWOStatus,*strDateWO;//"2018-02-07T05:40:24.337"
            
            strWOStatus=[matchesWorkOrder valueForKey:@"workorderStatus"];
            strDateWO=[matchesWorkOrder valueForKey:@"timeIn"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//07:00 pm
            NSDate* newTime = [dateFormatter dateFromString:strDateWO];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            strDateWO = [dateFormatter stringFromDate:newTime];
            
            NSDate *dateWO=[dateFormatter dateFromString:strDateWO];
            
            NSDate *dateToday = [NSDate date];
            
            
            BOOL temp=[self isSameDay:dateWO otherDay:dateToday];
            //if ([dateWO compare:dateToday] == NSOrderedSame)
            
            if (temp==YES)
            {
                if ([strWOStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWOStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strWOStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                    NSLog(@"Today Completed WO Matched");
                    
                    NSArray *key,*value;
                    key=[NSArray arrayWithObjects:
                         @"WO#",
                         @"TimeIn",
                         @"TimeOut",
                         nil];
                    NSString *str1Wo,*str2TimeIn,*str3TimeOut;
                    str1Wo=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"workOrderNo"]]];
                    str2TimeIn=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"timeIn"]]];
                    str3TimeOut=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"timeOut"]]];
                    value=[NSArray arrayWithObjects:
                           str1Wo,
                           str2TimeIn,
                           str3TimeOut,
                           nil];
                    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
                    [arrResponseNonBillable addObject:dict];
                    
                }
            }
            
        }
    }
    [_tblDataWorkOrderDetails reloadData];
    
}

#pragma mark- --------------------- LOCAL METHODS -----------------

-(void)endDayFunctionality
{
    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
    [_btnStartDay setEnabled:NO];
    _constTakeBreak_H.constant=0;
    strTimeOut=[self getCurrentTime];
    [self fetchClockInOutDetailCoreData];
    [self updateClockInOutDetailCoreData];
    strWorkingStatusNew=@"EndDay";
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // [DejalBezelActivityView removeView];
        [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
    });
    [self stopTimer];
}

-(void)getUpdatedRecords
{
    arrResponse=[[NSMutableArray alloc]init];
    NSArray *key,*value;
    key=[NSArray arrayWithObjects:
         @"WorkingStatus",
         @"TimeIn",
         @"TimeOut",
         nil];
    value=[NSArray arrayWithObjects:
           @"Working Status",
           @"Time In",
           @"Time Out",
           nil];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
    [arrResponse addObject:dict];
    
    arrResponseNonBillable=[[NSMutableArray alloc]init];
    NSArray *keyNonBillable,*valueNonBillable;
    keyNonBillable=[NSArray arrayWithObjects:
                    @"WO#",
                    @"TimeIn",
                    @"TimeOut",
                    nil];
    valueNonBillable=[NSArray arrayWithObjects:
                      @"WO #",
                      @"Time In",
                      @"Time Out",
                      nil];
    NSDictionary *dictNonBillable = [[NSDictionary alloc] initWithObjects:valueNonBillable forKeys:keyNonBillable];
    [arrResponseNonBillable addObject:dictNonBillable];
    
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Data"];
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         [self getCurrentTimerOfClock];
         });*/
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self getUpdatedClockInOutDate];
        });
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self getDetailNonBillabelTime];
        });
        
    }
    [_tblDataWorkOrderDetails reloadData];
    
    _const_TblClockIn_H.constant=50*arrResponse.count+20;
    _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
    
    
    _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
    [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
}

/*-(void)updateTimer
 {
 NSDate *currentDate = [NSDate date];
 NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
 NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"HH:mm:ss"];
 [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
 NSString *timeString=[dateFormatter stringFromDate:timerDate];
 strTimerOnStop=timeString;
 _lblTime.text = timeString;
 }*/

//Nilind 15 Dec

#pragma mark- ------------ DATE PICKER METHOD ------------

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=strTitile;
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
   // [dateFormat setDateFormat:@"hh:mm:a"];
    
    [dateFormat setDateFormat:@"hh:mm a"];
    
    if ([strForFromTime isEqualToString:@"fromTime"])
    {
        
        strDate = [dateFormat stringFromDate:pickerDate.date];
        strFromTime=strDate;
        [_btnFromTime setTitle:strDate forState:UIControlStateNormal];
        
        
        NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
        
        //[dateFormatNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
        [dateFormatNew setDateFormat:@"hh:mm a"];
        
        NSString *strNewDate=[dateFormatNew stringFromDate:pickerDate.date];
        strFromTimeNonBillable=strNewDate;
        NSLog(@"%@",strNewDate);
        
        
    }
    else
    {
        strDate = [dateFormat stringFromDate:pickerDate.date];
        strToTime=strDate;
        [_btnToTime setTitle:strDate forState:UIControlStateNormal];
        
        
        NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
        //[dateFormatNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
        [dateFormatNew setDateFormat:@"hh:mm a"];
        
        NSString *strNewDate=[dateFormatNew stringFromDate:pickerDate.date];
        strToTimeNonBillable=strNewDate;
        NSLog(@"%@",strNewDate);
        
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

#pragma mark- -------- DATE TIME METHOD -------------
-(void)getDate
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMM, yyyy";  // very simple format  "8:47:22 AM"
    }
    dateOnFirstTime=now;
    _lblDate.text = [dateFormatter stringFromDate:now];
    
}
- (NSString*)getCurrentTime
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        
    }
    NSString *strCurrentDateTime=[dateFormatter stringFromDate:now];
    return  strCurrentDateTime;
}
- (NSString*)getDateToday
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        
    }
    NSString *strCurrentDateTime=[dateFormatter stringFromDate:now];
    return  strCurrentDateTime;
}
- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}
#pragma mark- -------- TIMER METHOD -------------



- (void)stopTimer {
    
    running = NO;
    [myTimer invalidate];
    myTimer = nil;
    //[_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
    
}
- (void)updateTimer {
    BOOL chkForCurrentDate;
    chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
    if (chkForCurrentDate==YES)
    {
        /*count++;
         int hr = floor(count/100/60/60);
         
         int min = floor(count/100/60);
         int sec = floor(count/100);
         int mSec = count % 100;
         
         if (sec >= 60) {
         sec = sec % 60;
         }
         if (min >= 60) {
         min = min % 60;
         }*/
        count++;
        int hr = count/60/60;
        
        int min =count/60;
        int sec = count;
        int mSec = count % 100;
        
        if (sec >= 60) {
            sec = sec % 60;
        }
        if (min >= 60) {
            min = min % 60;
        }
        _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
        
        //_lblTime.text = [NSString stringWithFormat:@"%02d:%02d",min,sec];
    }
    else
    {
        [self endDayFunctionality];
    }
    
    
}
-(NSString*)calcaulteTimeInterval:(NSString*)strTimeInCell :(NSString *)strTimeOutCell
{
    NSString *strDuration;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
    {
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
            [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
            
        }
        
        strTimeOutCell=[dateFormatter stringFromDate:now];
    }
    NSDate *date2=[dateFormatter dateFromString:strTimeOutCell];
    NSDate *date1=[dateFormatter dateFromString:strTimeInCell];
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    NSLog(@"%f",secondsBetween);
    /* int hr = secondsBetween/60/60;
     
     int min =secondsBetween/60;
     int sec = secondsBetween;
     //int mSec = secondsBetween % 100;
     
     if (sec >= 60) {
     sec = sec % 60;
     }
     if (min >= 60) {
     min = min % 60;
     }
     strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];*/
    strDuration=[NSString stringWithFormat:@"%f",secondsBetween];
    return strDuration;
}
-(NSString*)calcaulteTimeIntervalNew:(NSString*)strTimeInCell :(NSString *)strTimeOutCell
{
    NSString *strDuration;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
    {
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
            [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
            
        }
        
        strTimeOutCell=[dateFormatter stringFromDate:now];
    }
    NSDate *date2=[dateFormatter dateFromString:strTimeOutCell];
    NSDate *date1=[dateFormatter dateFromString:strTimeInCell];
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    NSLog(@"%f",secondsBetween);
    /* int hr = secondsBetween/60/60;
     
     int min =secondsBetween/60;
     int sec = secondsBetween;
     //int mSec = secondsBetween % 100;
     
     if (sec >= 60) {
     sec = sec % 60;
     }
     if (min >= 60) {
     min = min % 60;
     }
     strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];*/
    strDuration=[NSString stringWithFormat:@"%f",secondsBetween];
    return strDuration;
}

- (void)updateTimerNew {
    
    count++;
    int hr = count/60/60;
    
    int min =count/60;
    int sec = count;
    int mSec = count % 100;
    
    if (sec >= 60) {
        sec = sec % 60;
    }
    if (min >= 60) {
        min = min % 60;
    }
    
    //[_btnTemp setTitle:[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec] forState:UIControlStateNormal];
    /*count++;
     int hr = floor(count/100/60/60);
     
     int min = floor(count/100/60);
     int sec = floor(count/100);
     int mSec = count % 100;
     
     if (sec >= 60) {
     sec = sec % 60;
     }
     if (min >= 60) {
     min = min % 60;
     }*/
    
    _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
    
    
}

/*-(void)tapDetectedOnBackGroundView
 {
 [viewForDate removeFromSuperview];
 [viewBackGround removeFromSuperview];
 }*/


- (IBAction)actionOnBtnTemp:(id)sender
{
    
    if (running == NO)
    {
        
        running = YES;
        
        if (myTimer == nil)
        {
            
            myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerNew) userInfo:nil repeats:YES];
            
        }
    }
    else
    {
        [self stopTimer];
        
    }
}
#pragma mark- -----------Text Field Delegate Method --------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    //[self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //[self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [self.view endEditing:YES];
    return YES;
}

#pragma mark- -----------Text View Delegate Method --------------

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    //isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        
        [_txtViewNonBillableDescription resignFirstResponder];
        [_txtBreakDescription resignFirstResponder];
        return NO;
    }
    
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    //[self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //[self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [self.view endEditing:YES];
    
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
}
#pragma mark-  ------------- Select Department / Slot----------

- (IBAction)actionOnSelectDepartment:(UIButton *)sender {
    
    if (arrOfDepartMents.count != 0) {
        self.tvForListData.tag = 1;
        
        _viewForTableView.frame = self.view.frame;
        int height = (int) arrOfDepartMents.count * 50;
        if (height > 450) {
            _viewForData.frame = CGRectMake(10, self.view.frame.size.height/2 - 200 , self.view.frame.size.width - 20, 420 );
        }else{
            _viewForData.frame = CGRectMake(10, self.view.frame.size.height/2 - height/2 , self.view.frame.size.width - 20, height + 20 );
        }
        [self.view addSubview:_viewForTableView];
        self.tvForListData.delegate = self;
        self.tvForListData.dataSource = self;
        [self.tvForListData reloadData];
    }else{
        
        [global displayAlertController:Alert :NoDataAvailableee :self];
        
    }
    
}

- (IBAction)actionOnSelectTimeSlot:(UIButton *)sender {
    
    if (strDepartmentName.length==0) {
        
        [global displayAlertController:Alert :@"Please select department" :self];
        
    } else {
        
        // Fetch Slots
        
        arrOfSlots = nil;
        
        arrOfSlots = [[NSMutableArray alloc]init];
        
        NSMutableArray *arrOfHoursConfig=nil;
        arrOfHoursConfig=[[NSMutableArray alloc]init];
        arrOfHoursConfig=[global getHoursConfiFromMaster:strCompanyKeyNew :strDepartmentName :@"" :@"" :@"Residential"];
        //New Change 27 Aug 2019
        NSString *strQbPayrollItemForRegular, *strQbPayrollItemForHoliday;
        strQbPayrollItemForRegular = @"Regular";
        strQbPayrollItemForHoliday = @"Holiday";

        if (arrOfHoursConfig.count>0)
        {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            NSArray *arrAfterHourRateConfigDcs = [dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
            NSString *strRegular = [NSString stringWithFormat:@"%@",[dictDataHourConfig valueForKey:@"StdQBPayrollItemName"]];
            
            NSString *strHoliday = [NSString stringWithFormat:@"%@",[dictDataHourConfig valueForKey:@"HolidayQBPayrollItemName"]];
            
            if (![strRegular isEqualToString:@""] || strRegular.length>0)
            {
                strQbPayrollItemForRegular = strRegular ;
            }
            if (![strHoliday isEqualToString:@""] || strHoliday.length>0)
            {
                strQbPayrollItemForHoliday = strHoliday ;
            }
            
            if (arrAfterHourRateConfigDcs.count>0)
            {
               /* NSDictionary *dict = [arrAfterHourRateConfigDcs objectAtIndex:0];
                NSString *strRegular = [NSString stringWithFormat:@"%@",[dict valueForKey:@"StdQBPayrollItemName"]];
                
                NSString *strHoliday = [NSString stringWithFormat:@"%@",[dict valueForKey:@"HolidayQBPayrollItemName"]];
                
                if (![strRegular isEqualToString:@""] || strRegular.length>0)
                {
                    strQbPayrollItemForRegular = strRegular ;
                }
                if (![strHoliday isEqualToString:@""] || strHoliday.length>0)
                {
                    strQbPayrollItemForHoliday = strHoliday ;
                }*/
            }
            
        }
       //End
        
        NSArray *objValue=[NSArray arrayWithObjects:
                           @"Regular",
                           @"Regular",
                           strQbPayrollItemForRegular,
                           @"Regular",nil];
        
        NSArray *objKey=[NSArray arrayWithObjects:
                         @"TimeSlot",
                         @"TimeSlotTitle",
                         @"QBPayrollItemName",
                         @"Title",nil];
        
        NSDictionary *dict_ToSend=[[NSDictionary alloc] initWithObjects:objValue forKeys:objKey];
        
        NSArray *objValue1=[NSArray arrayWithObjects:
                            @"Holiday",
                            @"Holiday",
                            strQbPayrollItemForHoliday,
                            @"Holiday",nil];
        
        NSArray *objKey1=[NSArray arrayWithObjects:
                          @"TimeSlot",
                          @"TimeSlotTitle",
                          @"QBPayrollItemName",
                          @"Title",nil];
        
        NSDictionary *dict_ToSend1=[[NSDictionary alloc] initWithObjects:objValue1 forKeys:objKey1];
        
        if (arrOfHoursConfig.count>0) {
            
            NSDictionary *dictDataHourConfig=arrOfHoursConfig[0];
            
            [arrOfSlots addObject:dict_ToSend];
            //Nilind 13 Aug
            NSArray *arrAfterHourRateConfigDcs = [dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            for (int i=0; i< arrAfterHourRateConfigDcs.count;i++)
            {
                NSDictionary *dict = [arrAfterHourRateConfigDcs objectAtIndex:i];
                if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]]isEqualToString:@"1"])
                {
                    [arrOfSlots addObject:dict];

                }
            }
            //End
           //Old// [arrOfSlots addObjectsFromArray:[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"]];
            
            [arrOfSlots addObject:dict_ToSend1];
            
            //arrOfSlots=[dictDataHourConfig valueForKey:@"AfterHourRateConfigDcs"];
            
        } else {
            
            arrOfSlots = nil;
            
            arrOfSlots = [[NSMutableArray alloc]init];
            
            [arrOfSlots addObject:dict_ToSend];
            
            [arrOfSlots addObject:dict_ToSend1];
            
        }
        
        if (arrOfSlots.count != 0) {
            self.tvForListData.tag = 2;
            _viewForTableView.frame = self.view.frame;
            int height =  (int) arrOfSlots.count * 50;
            if (height > 450) {
                _viewForData.frame = CGRectMake(10, self.view.frame.size.height/2 - 200 , self.view.frame.size.width - 20, 420 );
            }else{
                _viewForData.frame = CGRectMake(10, self.view.frame.size.height/2 - height/2 , self.view.frame.size.width - 20, height + 20 );
            }
            [self.view addSubview:_viewForTableView];
            self.tvForListData.delegate = self;
            self.tvForListData.dataSource = self;
            [self.tvForListData reloadData];
        }else{
            
            [global displayAlertController:Alert :NoDataAvailableee :self];
            
        }
    }
    
}
- (IBAction)actionOnButtonTransprant:(id)sender {
    [_viewForTableView  removeFromSuperview];
}

-(BOOL)isValidTimeToAdd
{
    
    NSLog(@"%@",arrResponseNonBillable);
    
    
    
    NSString *strCurrentDate = [global getCurrentDate];
    if (isEditNonBillable)
    {
        strCurrentDate = strDateEdited;
    }
    //strCurrentDate = strDateEdited;
    
    NSString *strFromDateNew = [NSString stringWithFormat:@"%@ %@",strCurrentDate,strFromTime];
    
    NSString *strToDateNew = [NSString stringWithFormat:@"%@ %@",strCurrentDate,strToTime];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
   // [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];

    
    NSDate *date1 = [dateFormat dateFromString:strFromDateNew];
    
    NSDate *date2 = [dateFormat dateFromString:strToDateNew];
    
    
    
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    
    NSString *str1 = [dateFormat stringFromDate:date1];
    
    NSString *str2 = [dateFormat stringFromDate:date2];
    
    
    NSDate *dateSelectedFromTime = [dateFormat dateFromString:str1];
    
    NSDate *dateSelectedToTime = [dateFormat dateFromString:str2];
    
    
    
    
    
    
    BOOL chkValidToAdd;
    
    chkValidToAdd = NO;
    
    
    
    for (int i=1; i<arrResponseNonBillable.count; i++)
        
    {
        
        
        NSDictionary *dict = [arrResponseNonBillable objectAtIndex:i];
        
        if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeNonBillableTimeId"]] isEqualToString:strNonBillabelTimeId])
        {
            
            chkValidToAdd = YES;
            continue;
        }
        
        //2019-07-12T15:18:00
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        
        
        NSString *strFromDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];
        
        
        
        NSString *strToDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
        
        
        
        NSDate *fromDateInResponse,*toDateInResponse;
        
        fromDateInResponse = [dateFormat dateFromString:strFromDate];
        
        toDateInResponse = [dateFormat dateFromString:strToDate];
        
        
        
        //strFromDate = [dateFormat stringFromDate:fromDateInResponse];
        
        // strToDate = [dateFormat stringFromDate:toDateInResponse];
        
        
        
        //if ((([fromDateInResponse compare:dateSelectedFromTime] == NSOrderedAscending)&&([dateSelectedFromTime compare:toDateInResponse] == NSOrderedAscending)) || (([fromDateInResponse compare:dateSelectedToTime] == NSOrderedAscending)&&([dateSelectedToTime compare:toDateInResponse] == NSOrderedAscending)))
      
        
        
         if(((([dateSelectedFromTime compare:fromDateInResponse]==NSOrderedAscending)||([dateSelectedFromTime compare:toDateInResponse]==NSOrderedAscending))&& (([fromDateInResponse compare:dateSelectedToTime]==NSOrderedAscending)||([toDateInResponse compare:dateSelectedToTime]==NSOrderedAscending))))
            
        {
            
            NSLog(@"Time slot not available");
            
            chkValidToAdd = NO;
            
            break;
            
        }
        
        else
            
        {
            
            NSLog(@"Time slot available");
            
            if(([fromDateInResponse compare:dateSelectedFromTime]==NSOrderedSame)&& !([toDateInResponse compare:dateSelectedToTime]==NSOrderedSame))
            {
                chkValidToAdd = false;
                break;
            }
            
            chkValidToAdd = YES;
            
        }
        
        
        
        
    }
    
    
    
    
    
    return chkValidToAdd;
    
}

-(NSString *)ChangeDateToLocalDateClock :(NSString*)strDateToConvert
{
    
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    //Add the following line to display the time in the local time zone
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        
        
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    
    
    if (finalTime.length==0)
        
    {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
    }
    
    return finalTime;
    
}
/*
 -(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 
 if (editingStyle == UITableViewCellEditingStyleDelete)
 {
 
 
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:@"Alert!"
 message:@"Are you sure want to delete"
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alert dismissViewControllerAnimated:YES completion:nil];
 }];
 UIAlertAction* cancel = [UIAlertAction
 actionWithTitle:@"Cancel"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 
 [alert dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alert addAction:ok];
 [alert addAction:cancel];
 
 [self presentViewController:alert animated:YES completion:nil];
 
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert)
 {
 NSLog(@"edit click");
 }
 
 }
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return YES if you want the specified item to be editable.
    
    if(tableView == _tblData || tableView == _tvForListData)
    {
        return NO;
    }
    else
    {
       NSDictionary * dict = [arrResponseNonBillable objectAtIndex:indexPath.row];

        
        if (indexPath.row == 0)
        {
            return NO;
        }
        else
        {
            if([[NSString stringWithFormat:@"%@",[dict valueForKey:@"SupervisorStatus"]] caseInsensitiveCompare:@"Approved"]==NSOrderedSame)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
    }
}
-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *rowActionEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"EDIT" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                           {
                                               isEditNonBillable = YES;
                                               dictSlotDataEdited = [arrResponseNonBillable objectAtIndex:indexPath.row];
                                               
                                               NSLog(@"%@",[arrResponseNonBillable objectAtIndex:indexPath.row]);
                                               [self editActionNonBilliable:[arrResponseNonBillable objectAtIndex:indexPath.row]];
                                               
                                               
                                           }];
    
    UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"DELETE" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 UIAlertController * alert=   [UIAlertController
                                                                               alertControllerWithTitle:@"Alert!"
                                                                               message:@"Are you sure want to delete"
                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                 
                                                 UIAlertAction* ok = [UIAlertAction
                                                                      actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action)
                                                                      {
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                                                 UIAlertAction* cancel = [UIAlertAction
                                                                          actionWithTitle:@"Cancel"
                                                                          style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action)
                                                                          {
                                                                              
                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                              
                                                                          }];
                                                 
                                                 [alert addAction:ok];
                                                 [alert addAction:cancel];
                                                 
                                                 [self presentViewController:alert animated:YES completion:nil];
                                                 
                                             }];
    
    rowActionDelete.backgroundColor=[UIColor redColor];
    rowActionEdit.backgroundColor=[UIColor grayColor];
    //return @[rowActionDelete,rowActionEdit];
    return @[rowActionEdit];
}
-(void)editActionNonBilliable:(NSDictionary *)dictForEdit
{
    
    
    strDepartmentName = [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"DepartmentSysName"]];
    
    for (int i=0; i<arrOfDepartMents.count; i++)
    {
        NSDictionary *dict=[arrOfDepartMents objectAtIndex:i];
        
        if ([strDepartmentName isEqualToString:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SysName"]]])
        {
            [_btnSelectDepartment setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]] forState:UIControlStateNormal];
            break;
            
        }
    }
    
    strNonBillabelTimeId = [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"EmployeeNonBillableTimeId"]];
    
    strEmployeeId = [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"EmployeeId"]];
    
    
    /* NSString *strTimeSlotTemp = [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"TimeSlot"]];
     
     
     for (int i=0; i<arrOfSlots.count; i++)
     {
     NSMutableDictionary *dict=[arrOfSlots objectAtIndex:i];
     
     if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]] isEqualToString:strTimeSlotTemp])
     {
     [_btnSelectTimeSlot setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"Title"]] forState:UIControlStateNormal];
     dictSlotData = dict;
     break;
     }
     }*/
    
    [_btnSelectTimeSlot setTitle:[NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"TimeSlotTitle"]] forState:UIControlStateNormal];//Time Slot
    
    if ([NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"TimeSlotTitle"]].length == 0 || [[NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"TimeSlotTitle"]] isEqualToString:@""])
    {
        [_btnSelectTimeSlot setTitle:[NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"TimeSlot"]] forState:UIControlStateNormal];//Time Slot

    }
    
    
    NSString *strFromTimeTemp =[self ChangeDateToLocalDateClock: [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"FromDate"]]];
    
    
    
    [_btnFromTime setTitle:[self stringDateToAM_PM:strFromTimeTemp] forState:UIControlStateNormal];
    
    
    NSString *strToTimeTemp = [self ChangeDateToLocalDateClock: [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"ToDate"]]];
    
    [_btnToTime setTitle:[self stringDateToAM_PM:strToTimeTemp] forState:UIControlStateNormal];
    
    strFromTimeNonBillable = strFromTimeTemp;
    strToTimeNonBillable = strToTimeTemp;
    
    strFromTime = [self stringDateToAM_PM:strFromTimeTemp];//strFromTimeTemp;
    strToTime = [self stringDateToAM_PM:strToTimeTemp];//strToTimeTemp;
    
    _txtViewNonBillableDescription.text = [NSString stringWithFormat:@"%@",[dictForEdit valueForKey:@"Description"]];
    
    
    [self displayNonBillableView];
    
    
    //temp Monday check krna 19 Aug ko
    
    strDateEdited = [self stringToEditedDate:strFromTimeTemp];
    
    //end
}
-(NSString*)stringDateToAM_PM:(NSString *)strDateTime
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    NSDate *date1 = [dateFormat dateFromString:strDateTime];
    
   // [dateFormat setDateFormat:@"hh:mm:a"];
    
    [dateFormat setDateFormat:@"hh:mm a"];

    
    strDateTime = [dateFormat stringFromDate:date1];
    
    return strDateTime;
    
}
-(NSString*)string24HrDateTo12HrStringAM_PM:(NSString *)strDateTime
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"HH:mm:ss"];
    
    NSDate *date1 = [dateFormat dateFromString:strDateTime];
    
    // [dateFormat setDateFormat:@"hh:mm:a"];
    
    [dateFormat setDateFormat:@"hh:mm a"];
    
    
    strDateTime = [dateFormat stringFromDate:date1];
    
    return strDateTime;
    
}
-(NSString*)stringToEditedDate:(NSString *)strDateTime
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    NSDate *date1 = [dateFormat dateFromString:strDateTime];
    
    // [dateFormat setDateFormat:@"hh:mm:a"];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    
    strDateTime = [dateFormat stringFromDate:date1];
    
    return strDateTime;
    
}

- (IBAction)action_NonBillableHours:(id)sender{
    
    [self goToNonBillableHours];
    
}

-(void)goToNonBillableHours{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MechanicaliPad"
                                                             bundle: nil];
    NonBillableiPadVC
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"NonBillableiPadVC"];
    //[self.navigationController pushViewController:objByProductVC animated:NO];
    [self presentViewController:objByProductVC animated:false completion:nil];
    
}

-(NSString*)changeDateToFormattedDateNewLocal :(NSString*)strDateBeingConverted{
    //2017-11-29T00:00:00
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate* newTime = [dateFormatter dateFromString:strDateBeingConverted];
    
    if (newTime==nil) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        newTime = [dateFormatter dateFromString:strDateBeingConverted];
    }
    
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    
    //New change for Formatted Date
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"hh:mm a"];

    finalTime = [dateFormatter stringFromDate:newTime];
    
    if (finalTime.length==0) {
        finalTime=strDateBeingConverted;
    }
    
    return finalTime;
    
}
@end

