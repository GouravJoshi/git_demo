//
//  NonBillableiPadVC.swift
//  DPS
//
//  Created by Saavan Patidar on 01/08/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit
import Alamofire


class NonBillableiPadVC: UIViewController {

    // MARK: - -----------------------------------IBOutlets-----------------------------------

    @IBOutlet var tblViewNonBillableHours: UITableView!

    // MARK: - -----------------------------------Global Variables-----------------------------------
    var arrayReponse = NSArray ()
    var refresher = UIRefreshControl()

    
    // MARK: - -----------------------------------View Life Cycle-----------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        
        self.tblViewNonBillableHours!.addSubview(refresher)
        
        refresher.attributedTitle = NSAttributedString(string: "Pull To Refresh")
        
        tblViewNonBillableHours.estimatedRowHeight = 50.0
        tblViewNonBillableHours.tableFooterView = UIView()
        
        // setting Todays date default
        
        Global().setDefaultNonBillableFromTodate()

        self.callAPI()

        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        if (nsud.value(forKey: "isChangedFilterDate") != nil) {
            
            let isChangedDate = nsud.bool(forKey: "isChangedFilterDate")
            
            if isChangedDate {
                
                nsud.set(false, forKey: "isChangedFilterDate")
                nsud.synchronize()
                self.callAPI()
                
            }
            
        }
        
    }

    // MARK: - -----------------------------------Actions-----------------------------------
    
    @IBAction func action_Back(_ sender: Any) {
        
        dismiss(animated: false)
        
    }
    
    @IBAction func action_Filter(_ sender: Any) {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let objVC = storyboardIpad.instantiateViewController(withIdentifier: "FilterViewLead") as? FilterViewLead
        objVC?.strType = "NonBillableiPad"
        self.present(objVC!, animated: false, completion: nil)
        
    }
    
    // MARK: - -----------------------------------Functions-----------------------------------
    
    @objc func RefreshloadData() {
        self.callAPI()
    }
    
    func callAPI() {
        
        
        if !isInternetAvailable()
            
        {
            Global().displayAlertController(alertMessage, "\(ErrorInternetMsg)", self)
        }
        else
        {
            
            let strFromDate = nsud.value(forKey: "fromdateNonBillableiPad")
            let strToDate = nsud.value(forKey: "todateNonBillableiPad")
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
           var strCompanyKey = String()
           var strEmployeeNumber = String()

           if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
           
               strCompanyKey = "\(value)"
           
           }
            
            if let value = dictLoginData.value(forKeyPath: "EmployeeNumber") {
                
                strEmployeeNumber = "\(value)"
                
            }
            // /api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoForPastDateExt/?companyKey=titan&employeeNo=E4307&&fromDate=05/24/2019&toDate=05/25/2019
            
            var strUrl =  ""
            
            let strUrlMain = dictLoginData.value(forKeyPath: "Company.CompanyConfig.HrmsServiceModule.ServiceUrl") as? String
            
            strUrl = "\(strUrlMain ?? "")\("/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoForPastDateExt/?companyKey=")\(strCompanyKey)\("&employeeNo=")\(strEmployeeNumber)\("&fromDate=")\(strFromDate ?? Global().getCurrentDate())\("&toDate=")\(strToDate ?? Global().getCurrentDate())"
            
            //strUrl = strUrlMain! + "/api/EmployeeClockInOut/GetClockInOutByEmployeeNoExt/?companyKey=" + strCompanyKey + "&employeeNo=" + strEmployeeNumber + "&filterDate=" + strFromDate
            
            FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
                
                DispatchQueue.main.async{
                    
                    FTIndicator.dismissProgress()
                    self.refresher.endRefreshing()
                    
                }
                if(Status){
                    
                    //let dictAddress = Response["data"] as! NSDictionary
                    
                    let arrData = Response["data"] as! NSArray
                    
                    //let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self) {
                        
                        self.arrayReponse = arrData
                        
                        if self.arrayReponse.count == 0 {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                            
                        }
                        
                        self.tblViewNonBillableHours.reloadData()
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                else
                {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            }
            
        }
        


    }
    
    
    func goToUpdateBillableHours(dict: NSDictionary , array: NSArray)
    {
    
        let mainStoryboard = UIStoryboard(name: "MechanicaliPad", bundle: nil)
        let objVC = mainStoryboard.instantiateViewController(withIdentifier: "UpdateNonBillableHours") as? UpdateNonBillableHours
        objVC?.arrResponseNonBillable = array
        objVC?.dictNonBillabel = dict
        
        self.present(objVC!, animated: true, completion: nil)
        

    }

}


// MARK: - -------------------------------------------UITableViewDelegate-------------------------------------------

extension NonBillableiPadVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return arrayReponse.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
        
    }
    
    

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var dict = NSDictionary()
        dict  = self.arrayReponse.object(at: section) as! NSDictionary
        
        let strHeader = dict.value(forKey: "DateSlot")
        

        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        vw.backgroundColor = UIColor.lightTextColorTimeSheet()
        
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = strHeader as? String
        lblHeader.font = UIFont.systemFont(ofSize: 20)
        vw.addSubview(lblHeader)
        
        return vw
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var dictData = NSDictionary()
        var array = NSArray()
        dictData  = self.arrayReponse.object(at: section) as! NSDictionary
        array  = dictData.value(forKey: "EmployeeNonBillableTimeExtSerDcss") as! NSArray
        
        return array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NonBillableiPadVC", for: indexPath as IndexPath) as! tblCell
        tblViewNonBillableHours.separatorColor = UIColor.theme()
        
        var dictData = NSDictionary()
        var array = NSArray()
        dictData  = self.arrayReponse.object(at: indexPath.section) as! NSDictionary
        array  = dictData.value(forKey: "EmployeeNonBillableTimeExtSerDcss") as! NSArray
        
        var dict = NSDictionary()
        dict  = array.object(at: indexPath.row) as! NSDictionary
        // FromDate  ToDate  2019-08-01T17:29:00  TimeSlotTitle Description
        
        //cell.lbl_NonBillableFromTime.text = dict.value(forKey: "FromDate") as? String
        //cell.lbl_NonBillableToTime.text = dict.value(forKey: "ToDate") as? String
        //cell.lbl_NonBillableTimeSlot.text = dict.value(forKey: "TimeSlot") as? String
        
        let strDescriptions = (dict.value(forKey: "Description") as? String)!
        
        if strDescriptions.count == 0 {
            
            cell.lbl_NonBillableDescription.text = "N/A"
            
        }else {
            
            cell.lbl_NonBillableDescription.text = strDescriptions

        }
        
        let startTime = dateTimeConvertor(str: "\(dict.value(forKey: "FromDate")!)", formet: "yyyy-MM-dd'T'HH:mm:ss" , strFormatToBeConverted: "hh:mm a")
        let endTime = dateTimeConvertor(str: "\(dict.value(forKey: "ToDate")!)", formet: "yyyy-MM-dd'T'HH:mm:ss" , strFormatToBeConverted: "hh:mm a")

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let dateNSDate = dateFormatter.date(from: startTime)
        let currentDate = dateFormatter.date(from: endTime)
        let timeInterval = currentDate!.timeIntervalSince(dateNSDate!)
        
        let hours = Int(timeInterval) / 3600
        let minutes = Int(timeInterval) / 60 % 60
        //let seconds = Int(timeInterval) % 60
        
       // let stringDuration = String(format: "%02i:%02i:%02i", hours, minutes, seconds)
        let stringDuration = String(format: "%02i:%02i", hours, minutes)

        //cell.lbl_NonBillableDuration.text = stringDuration
        //cell.lbl_NonBillableFromTime.text = startTime
        //cell.lbl_NonBillableToTime.text = endTime
        
        cell.lbl_NonBillableTimeSlotDuration.text = (dict.value(forKey: "TimeSlot") as? String)! + " - " + stringDuration + " hrs"
        cell.lbl_NonBillableTimeInOut.text = "Time In - Out: " + startTime + " - " + endTime

        
        return cell
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
     {
        
        var dictData = NSDictionary()
        var array = NSArray()
        dictData  = self.arrayReponse.object(at: indexPath.section) as! NSDictionary
        array  = dictData.value(forKey: "EmployeeNonBillableTimeExtSerDcss") as! NSArray
        
        var dict = NSDictionary()
        dict  = array.object(at: indexPath.row) as! NSDictionary
        
        let strSupervisorStatus = dict.value(forKey: "SupervisorStatus") as! String
        
        if strSupervisorStatus == "Approved" {
            
            return false
            
        } else {
            
            return true
            
        }

    }
    
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
           
            var dict = NSDictionary()
            var dict1 = NSDictionary()

            var array = NSArray()
            dict  = self.arrayReponse.object(at: indexPath.section) as! NSDictionary
            array  = dict.value(forKey: "EmployeeNonBillableTimeExtSerDcss") as! NSArray
            dict1  = array.object(at: indexPath.row) as! NSDictionary

            self.goToUpdateBillableHours(dict: dict1, array: array)
            
        })
        
        return [editAction]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.rowHeight
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
//        var dict = NSDictionary()
//        dict  = arrayReponse.object(at: indexPath.row) as! NSDictionary
        
    }
    
    func reloadWithAnimation() {
        
        tblViewNonBillableHours.reloadData()
        let tableViewHeight = tblViewNonBillableHours.bounds.size.height
        let cells = tblViewNonBillableHours.visibleCells
        
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.5, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }

}
