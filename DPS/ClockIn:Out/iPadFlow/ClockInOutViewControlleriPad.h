//
//  ClockInOutViewControlleriPad.h
//  DPS
//  Created by Rakesh Jain on 25/01/18.
//  Copyright © 2018 Saavan. All rights reserved. CHANGE
// sdfgdsfg

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ClockInOutDetail+CoreDataClass.h"
#import "ClockInOutDetail+CoreDataProperties.h"
@interface ClockInOutViewControlleriPad : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityClockInOutDetail,*entityWorkOderDetailServiceAuto;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches,*matchesWorkOrder;
    NSArray *arrAllObj,*arrAllObjWorkOrder;
    
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerClockInOutDetail,*fetchedResultsControllerServiceAutomation;

- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
@property (weak, nonatomic) IBOutlet UITableView *tblDataWorkOrderDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnStartDay;
- (IBAction)actionOnStartDay:(id)sender;

- (IBAction)actionOnTakeBreak:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBreak;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTakeBreak_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblClockIn_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblWorkOrder_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constContentView_H;
- (IBAction)actionOnNonBillableTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNonBillabelTime;

//View NonBillable
@property (strong, nonatomic) IBOutlet UIView *viewForAddNonBillableTime;
@property (weak, nonatomic) IBOutlet UIButton *btnFromTime;
- (IBAction)actionOnBtnFromTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnToTime;
- (IBAction)actionOnBtnToTime:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNonBillableDescription;
- (IBAction)actionOnSaveForNonBillableTime:(id)sender;
- (IBAction)actionOnCancelForNonBillableTime:(id)sender;

// View For Break sdfgdfs sdefgsdf sdefgdfs sdfgdfsg
@property (strong, nonatomic) IBOutlet UIView *viewForBreak;
@property (weak, nonatomic) IBOutlet UITextField *txtBreakReason;
@property (weak, nonatomic) IBOutlet UITextView *txtBreakDescription;
- (IBAction)actionOnSaveForBreak:(id)sender;
- (IBAction)actionOnCancelForBreak:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTemp;
- (IBAction)actionOnBtnTemp:(id)sender;
//Updated by Navin 20/11/2018
//Updated by Navin 20/11/2018
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDepartment;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectTimeSlot;
- (IBAction)actionOnSelectDepartment:(UIButton *)sender;
- (IBAction)actionOnSelectTimeSlot:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *viewForTableView;
- (IBAction)actionOnButtonTransprant:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForData;
@property (weak, nonatomic) IBOutlet UITableView *tvForListData;
- (IBAction)action_NonBillableHours:(id)sender;
-(NSString *)ChangeDateToLocalDateClock :(NSString*)strDateToConvert;
-(NSString*)stringDateToAM_PM : (NSString *)strDateTime;
@end

