//
//  ClockInOutViewController.m
//  DPS
//
//  Created by Rakesh Jain on 21/12/17.
//  Copyright © 2017 Saavan. All rights reserved. changes
//

#import "ClockInOutViewController.h"
#import "ClockInOutTableViewCell.h"
#import "AllImportsViewController.h"
@interface ClockInOutViewController ()
{
    NSMutableArray *arrDate,*arrStartTime,*arrStopTime;
    UIView *viewBackGround,*viewBackGroundForNonBillableTime;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    NSString *strTitile,*strDate,*strForFromTime,*strForToTime,*strFromTime,*strToTime,*strFromTimeNonBillable,*strToTimeNonBillable;
    Global *global;

    //Start Timer Code
    NSTimer *stopTimer;
    NSDate *startDate;
    BOOL running;
    NSString *strTimerOnStop;
    
    
    int count;
    NSTimer *myTimer;
    
    NSString *strClockInOutId,*strTimeIn,*strTimeOut,*strWorkingStatusNew,*strReason,*strDescrption, *strEmployeeId,*strCompanyKeyNew,*strUserNameNew;
    NSString *strClockUrl,*strEmployeeNo;
    NSMutableArray *arrResponse;
    
    //NonBillable
    NSMutableArray *arrResponseNonBillable;
    NSDate *dateOnFirstTime;


}
@end

@implementation ClockInOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrResponse=[[NSMutableArray alloc]init];
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strEmployeeId    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strCompanyKeyNew=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strUserNameNew    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strClockUrl= [NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.HrmsServiceModule.ServiceUrl"]];
    [self deleteClockRecordExceptToday];
    [self fetchClockInOutDetailInOfflineCase];
    //[self fetchTodaysCompleteWorkOrder];
    global=[[Global alloc]init];

    arrDate=[[NSMutableArray alloc]init];
    arrStartTime=[[NSMutableArray alloc]init];
    arrStopTime=[[NSMutableArray alloc]init];

    // Do any additional setup after loading the view.
    _txtViewNonBillableDescription.text=@"";
    _txtViewNonBillableDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtViewNonBillableDescription.layer.borderWidth=1.0;
    _txtViewNonBillableDescription.layer.cornerRadius=5.0;
    
    _txtBreakDescription.text=@"";
    _txtBreakDescription.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBreakDescription.layer.borderWidth=1.0;
    _txtBreakDescription.layer.cornerRadius=5.0;

    _txtBreakReason.text=@"";
    _txtBreakReason.layer.borderColor=[[UIColor colorWithRed:123.0f/255 green:166.0f/255 blue:208.0f/255 alpha:1] CGColor];
    _txtBreakReason.layer.borderWidth=1.0;
    _txtBreakReason.layer.cornerRadius=5.0;

    //Timer Code
    
   /* running = NO;
    startDate = [NSDate date];*/
    running = NO;
    count = 0;
    _lblTime.text = @"00:00:00";
    //End


    
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
    }
   // _lblTime.text = [dateFormatter stringFromDate:now];
    [self getDate];
    //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    
    

  
    NSString *strTitle;
    strTitle=_btnStartDay.currentTitle;
    
    if ([strTitle isEqualToString:@"Start Day"])
    {
        
        _constTakeBreak_H.constant=0;
    }
    else
    {
        _constTakeBreak_H.constant=35;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [DejalBezelActivityView removeView];
        [self getCurrentTimerOfClock];
    });
    [self getUpdatedRecords];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-  ------------- TABLE VIEW DELEGAT METHOD -------------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblData)
    {
        return arrResponse.count;
 
    }
    else //tblDataWorkOrderDetails
    {
        return arrResponseNonBillable.count;

    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblData)
    {
        static NSString *identifier=@"ClockInOutTableViewCell";
        ClockInOutTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell==nil)
        {
            cell=[[ClockInOutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrResponse objectAtIndex:indexPath.row];

        if(indexPath.row==0)
        {
            cell.lblDate.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkingStatus"]];
            cell.lblDate.textAlignment=NSTextAlignmentCenter;
            cell.lblTimeIn.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
            cell.lblTimeIn.textAlignment=NSTextAlignmentCenter;

            cell.lblTimeOut.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]];
            cell.lblTimeOut.textAlignment=NSTextAlignmentCenter;


            cell.lblDate.font=[UIFont boldSystemFontOfSize:12];
            cell.lblTimeIn.font=[UIFont boldSystemFontOfSize:12];
            cell.lblTimeOut.font=[UIFont boldSystemFontOfSize:12];

        }
        else
        {
            cell.lblDate.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WorkingStatus"]];

            
            NSString *strTimeInCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]]];//TimeIn
            if (strTimeInCell.length==0||[strTimeInCell isEqualToString:@"<null>"]||[strTimeInCell isEqual:NULL])
            {
                strTimeInCell=@"";
            }
            
            cell.lblTimeIn.text=strTimeInCell;
            
            NSString *strTimeOutCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]]];
            if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
            {
                strTimeOutCell=@"";
            }
            cell.lblTimeOut.text=strTimeOutCell;

            
            if([UIScreen mainScreen].bounds.size.height<600)
            {
                cell.lblDate.font=[UIFont systemFontOfSize:9];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:9];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:9];

            }
            else
            {
                cell.lblDate.font=[UIFont systemFontOfSize:12];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:12];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:12];

            }
            //Temp

            NSString *strValue=[self calcaulteTimeInterval:strTimeInCell :strTimeOutCell];
            double secondsBetween=[strValue doubleValue];
            int hr = secondsBetween/60/60;
            
            int min =secondsBetween/60;
            int sec = secondsBetween;
            //int mSec = secondsBetween % 100;
            
            if (sec >= 60) {
                sec = sec % 60;
            }
            if (min >= 60) {
                min = min % 60;
            }
            NSString *strDuration;
            strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
            //cell.lblDate.text=strDuration;
            //End
        }
        return cell;
    }
    else
    {
        static NSString *identifier=@"WorkOrderDetail";
        ClockInOutTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell==nil)
        {
            cell=[[ClockInOutTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSDictionary *dict=[arrResponseNonBillable objectAtIndex:indexPath.row];
        
        if(indexPath.row==0)
        {
            cell.lblWONo.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WO#"]];
            //cell.lblWONo.textAlignment=NSTextAlignmentCenter;
            cell.lblTimeIn.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
            cell.lblTimeIn.textAlignment=NSTextAlignmentCenter;
            
            cell.lblTimeOut.text=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOut"]];
            cell.lblTimeOut.textAlignment=NSTextAlignmentCenter;
            
            
                cell.lblWONo.font=[UIFont boldSystemFontOfSize:12];
                cell.lblTimeIn.font=[UIFont boldSystemFontOfSize:12];
                cell.lblTimeOut.font=[UIFont boldSystemFontOfSize:12];
            
        }
        else
        {
            NSString *strWoNonBillable;
            strWoNonBillable=[NSString stringWithFormat:@"%@",[dict valueForKey:@"WO#"]];
            if (strWoNonBillable.length==0||[strWoNonBillable isEqualToString:@"<null>"]||[strWoNonBillable isEqual:NULL]||[strWoNonBillable isEqualToString:@"(null)"])
            {
                strWoNonBillable=@"WO #";
            }
            
            cell.lblWONo.text=strWoNonBillable;
            
            //cell.lblWONo.textAlignment=NSTextAlignmentCenter;
            NSString *strTimeInCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];
            if (strTimeInCell.length==0||[strTimeInCell isEqualToString:@"<null>"]||[strTimeInCell isEqual:NULL])
            {
                strTimeInCell=@"";
                
                
            }
            if ([strTimeInCell isEqualToString:@"(null)"])
            {
                strTimeInCell=[dict valueForKey:@"TimeIn"];
            }
            cell.lblTimeIn.text=strTimeInCell;
            
            NSString *strTimeOutCell=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
            if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
            {
                strTimeOutCell=@"";
               
            }
            if ([strTimeOutCell isEqualToString:@"(null)"])
            {
                strTimeOutCell=[dict valueForKey:@"TimeOut"];
            }
            cell.lblTimeOut.text=strTimeOutCell;
            
            if([UIScreen mainScreen].bounds.size.height<600)
            {
                cell.lblWONo.font=[UIFont systemFontOfSize:9];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:9];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:9];
                
            }
            else
            {
                cell.lblWONo.font=[UIFont systemFontOfSize:12];
                cell.lblTimeIn.font=[UIFont systemFontOfSize:12];
                cell.lblTimeOut.font=[UIFont systemFontOfSize:12];
            }
            
        }
        return cell;

    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblData)
    {
        if (indexPath.row==0)
        {
            return 50;
        }
        else
        {
            return tableView.rowHeight;
        }
    }
    else
    {
        if (indexPath.row==0)
        {
            return 50;
        }
        else
        {
            return tableView.rowHeight;
        }
    }
}
#pragma mark-  ------------- ALL ACTION METHODS -------------


- (IBAction)actionOnStartDay:(id)sender
{
  
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        BOOL chkForCurrentDate;
        chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
        if (chkForCurrentDate==YES)
        {
            NSString *strTitle;
            strTitle=_btnStartDay.currentTitle;
            
            if (running == NO)
            {
                
                [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                
                running = YES;
                
                if (myTimer == nil)
                {
                    
                    //myTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                    
                    
                }
                
                strTimeIn=[self getCurrentTime];
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self apiForClockInOutDetail:@"WorkingTime" TimeIn:strTimeIn TimeOut:@""];
                    
                });
                
                _constTakeBreak_H.constant=35;
            }
            else
            {
                // [_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
                [_btnStartDay setEnabled:NO];
                _constTakeBreak_H.constant=0;
                strTimeOut=[self getCurrentTime];
                [self fetchClockInOutDetailCoreData];
                [self updateClockInOutDetailCoreData];
                strWorkingStatusNew=@"EndDay";
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [DejalBezelActivityView removeView];
                    [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
                });
                
                [self stopTimer];
                
            }
        }
        else
        {
            [self endDayFunctionality];
        }
    }//
}
-(void)endDayFunctionality
{
    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
    [_btnStartDay setEnabled:NO];
    _constTakeBreak_H.constant=0;
    strTimeOut=[self getCurrentTime];
    [self fetchClockInOutDetailCoreData];
    [self updateClockInOutDetailCoreData];
    strWorkingStatusNew=@"EndDay";
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // [DejalBezelActivityView removeView];
        [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
    });
    [self stopTimer];
}
- (IBAction)actionOnTakeBreak:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        
    }
    else
    {
        BOOL chkForCurrentDate;
        chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
        if (chkForCurrentDate==YES)
        {
            NSString *strTitle;
            strTitle=_btnBreak.currentTitle;
            
            if ([strTitle isEqualToString:@"Take Break"])
            {
                viewBackGroundForNonBillableTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                _viewForBreak.frame=CGRectMake(_viewForBreak.frame.origin.x, ([UIScreen mainScreen].bounds.size.height-_viewForBreak.frame.size.height)/2, viewBackGroundForNonBillableTime.frame.size.width, _viewForBreak.frame.size.height);
                viewBackGroundForNonBillableTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
                [viewBackGroundForNonBillableTime addSubview:_viewForBreak];
                [self.view addSubview:viewBackGroundForNonBillableTime];
                
            }
            else
            {
                _btnStartDay.enabled=YES;
                
                strTimeOut=[self getCurrentTime];
                [_btnBreak setTitle:@"Take Break" forState:UIControlStateNormal];
                [self fetchClockInOutDetailCoreData];
                [self updateClockInOutDetailCoreData];
                
                strWorkingStatusNew=@"WorkingTime";
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[DejalBezelActivityView removeView];
                    
                    [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
                    
                });
                //Start Time
                
                if (running == NO)
                {
                    running = YES;
                    
                    if (myTimer == nil)
                    {
                        
                        //myTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        
                        
                    }
                }
                
                
            }
        }
        else
        {
            [self endDayFunctionality];
        }
    }
    
}
- (IBAction)actionOnBtnFromTime:(id)sender
{
    strTitile=@"SELECT TIME";
    strForFromTime=@"fromTime";
    [self addPickerViewDateTo];
}
- (IBAction)actionOnBtnToTime:(id)sender
{
    strTitile=@"SELECT TIME";
    strForFromTime=@"ToTime";

    [self addPickerViewDateTo];
}
- (IBAction)actionOnNonBillableTime:(id)sender
{
    viewBackGroundForNonBillableTime=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _viewForAddNonBillableTime.frame=CGRectMake(viewBackGroundForNonBillableTime.frame.origin.x, ([UIScreen mainScreen].bounds.size.height-_viewForAddNonBillableTime.frame.size.height)/2, viewBackGroundForNonBillableTime.frame.size.width, _viewForAddNonBillableTime.frame.size.height);
    viewBackGroundForNonBillableTime.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [viewBackGroundForNonBillableTime addSubview:_viewForAddNonBillableTime];
    
    [self.view addSubview:viewBackGroundForNonBillableTime];
}
- (IBAction)actionOnSaveForNonBillableTime:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setDateFormat:@"MM/dd/yyyy"];
    [dateFormat setDateFormat:@"hh:mm:a"];
    
    NSDate *date1 = [dateFormat dateFromString:strFromTime];
    NSDate *date2 = [dateFormat dateFromString:strToTime];
    
    if (strFromTime.length==0||[strFromTime isEqualToString:@""]||[_btnFromTime.currentTitle isEqualToString:@"From Time"])
    {
        [global AlertMethod:@"Alert" :@"Please choose From Time"];
        
    }
    else if (strToTime.length==0||[strToTime isEqualToString:@""]||[_btnToTime.currentTitle isEqualToString:@"To Time"])
    {
        [global AlertMethod:@"Alert" :@"Please choose To Time"];
        
    }
    else if ([_txtViewNonBillableDescription.text isEqualToString:@""])
    {
        [global AlertMethod:@"Alert" :@"Please add descrption"];
        
    }
    else if ([date1 compare:date2] == NSOrderedDescending)
    {
        [global AlertMethod:@"Alert" :@"To Time should be greater than From Time"];
    }
    else if ([date1 compare:date2] == NSOrderedSame)
    {
        [global AlertMethod:@"Alert" :@"From Time and TO Time can not be same "];
    }
    else
    {
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            [global AlertMethod:@"ALert" :ErrorInternetMsg];
            [DejalActivityView removeView];
        }
        else
        {
            
            if (arrResponseNonBillable.count==1) {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [DejalBezelActivityView removeView];
                    [self apiForNonBillableTimeDetail];
                });
                
                strFromTime=@"";
                strToTime=@"";
                _txtViewNonBillableDescription.text=@"";
                [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
                [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
                
                [viewBackGroundForNonBillableTime removeFromSuperview];
                [_viewForAddNonBillableTime removeFromSuperview];
                
            }else{
                
                if ([self isValidTimeToAdd] == YES) {
                    
                    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [DejalBezelActivityView removeView];
                        [self apiForNonBillableTimeDetail];
                    });
                    
                    strFromTime=@"";
                    strToTime=@"";
                    _txtViewNonBillableDescription.text=@"";
                    [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
                    [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
                    
                    [viewBackGroundForNonBillableTime removeFromSuperview];
                    [_viewForAddNonBillableTime removeFromSuperview];
                    
                } else {
                    
                    [global displayAlertController:Alert :@"Time Range already exist." :self];
                    
                }
                
            }

            
        }
    }
}

- (IBAction)actionOnCancelForNonBillableTime:(id)sender
{
    strFromTime=@"";
    strToTime=@"";
    [_btnFromTime setTitle:@"From Time" forState:UIControlStateNormal];
    [_btnToTime setTitle:@"To Time" forState:UIControlStateNormal];
    _txtViewNonBillableDescription.text=@"";
    
    [viewBackGroundForNonBillableTime removeFromSuperview];
    [_viewForAddNonBillableTime removeFromSuperview];
    
}
- (IBAction)actionOnSaveForBreak:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [_btnBreak setTitle:@"End Break" forState:UIControlStateNormal];
        _btnStartDay.enabled=NO;
        strReason=_txtBreakReason.text;
        strDescrption=_txtBreakDescription.text;
        
        strTimeOut=[self getCurrentTime];
        [self fetchClockInOutDetailCoreData];
        [self updateClockInOutDetailCoreData];
        strWorkingStatusNew=@"BreakTime";
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading...."];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //[DejalBezelActivityView removeView];
            
            [self apiForClockInOutDetail:strWorkingStatusNew TimeIn:strTimeIn TimeOut:strTimeOut];
            
        });
        [self stopTimer];
        strReason=@"";
        strDescrption=@"";
        _txtBreakDescription.text=@"";

        [viewBackGroundForNonBillableTime removeFromSuperview];
        [_viewForBreak removeFromSuperview];
    }
    
}

- (IBAction)actionOnCancelForBreak:(id)sender
{
    strReason=@"";
    strDescrption=@"";
    _txtBreakDescription.text=@"";
    [viewBackGroundForNonBillableTime removeFromSuperview];
    [_viewForBreak removeFromSuperview];
    
}
- (IBAction)actionOnBack:(id)sender
{
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromClockInOut"];
    [defs synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- ---------------  ALL API METHODS ---------------

-(void)apiForClockInOutDetail: (NSString*)strWorkingStatus TimeIn:(NSString *)timeIn TimeOut:(NSString*)timeOut
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
       
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeClockInOut/AddUpdateEmployeeClockInOutExt"];
        
        if([strClockInOutId isEqual:nil]|| strClockInOutId.length==0)
        {
            strClockInOutId=@"";
        }
        if([_btnStartDay.currentTitle isEqualToString:@"End Break"])
        {
            strClockInOutId=@"";

        }
        if([strReason isEqual:nil]|| strReason.length==0)
        {
            strReason=@"";
        }
        if([strDescrption isEqual:nil]|| strDescrption.length==0)
        {
            strDescrption=@"";
        }
        
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"EmployeeClockInOutId",
             @"EmployeeId",
             @"WorkingStatus",
             @"TimeIn",
             @"TimeOut",
             @"Reason",
             @"Description",
             @"IsActive",
             @"CreatedBy",
             @"CreatedDate",
             @"CompanyKey",
             nil];
         value=[NSArray arrayWithObjects:
               strClockInOutId,
               strEmployeeId,
               strWorkingStatus,
               timeIn,
               timeOut,
               strReason,
               strDescrption,
               @"true",
               strEmployeeId,
               [global strCurrentDate],
               strCompanyKeyNew,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                NSLog(@"Clock Reponse %@",dictionary);
            }
            
            strClockInOutId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeClockInOutId"]];
            strTimeIn=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeIn"]];
            strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];

            NSString *statusWorkingTime=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
            
           /*if([_btnStartDay.currentTitle isEqualToString:@"End Day"])
           {
              
           }
           else
           {*/
               [self saveClockInOutDetailCoreData :strClockInOutId :statusWorkingTime];
            [self getUpdatedRecords];
               
          // }
           
        }
        [DejalActivityView removeView];
    }
}
-(void)apiForNonBillableTimeDetail
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        //http://service.hrms.com/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=Titan&employeeNo=325350&filterDate=1/10/2018
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeNonBillableTime/AddUpdateEmployeeNonBillableTimeExt"];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"EmployeeNonBillableTimeId",
             @"EmployeeId",
             @"FromDate",
             @"ToDate",
             @"Title",
             @"Description",
             @"IsActive",
             @"EmployeeDc",
             @"CreatedDate",
             @"CreatedBy",
             @"ModifiedDate",
             @"ModifiedBy",
             nil];
        value=[NSArray arrayWithObjects:
               @"",
               strEmployeeId,
               strFromTimeNonBillable,
               strToTimeNonBillable,
               @"Nonbillable Reason",
               _txtViewNonBillableDescription.text,
               @"true",
               @"",
               [global strCurrentDate],
               strEmployeeId,
               @"",
               @"",
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR Non Billable CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End

        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                NSLog(@"Non Billable Clock Reponse %@",dictionary);
            }
            
            [self getUpdatedRecords];
        }
        [DejalActivityView removeView];
    }
}

-(void)getCurrentTimerOfClock
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        //http://service.hrms.com/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=Titan&employeeNo=325350&filterDate=1/10/2018
        NSString *strUrl;
        strUrl=[NSString stringWithFormat:@"%@%@",strClockUrl,@"/api/EmployeeClockInOut/EmployeeClockInOutForDisplayExt"];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
            @"EmployeeId",
             nil];
        value=[NSArray arrayWithObjects:
               strEmployeeId,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"JSON FOR CURRENT TIMER CLOCK %@",jsonString1);
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End

        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
        }
        else
        {
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            
            if (dictionary.count==0 || dictionary==nil) {
                
            }
            else
            {
                
                NSLog(@"CURRENT TIMER Clock Reponse %@",dictionary);
                NSString *strCurrentWorkingStatus,*strCurrentWorkingTimeInSeconds;
                strCurrentWorkingStatus=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
                strCurrentWorkingTimeInSeconds=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingTimeInSeconds"]];
                strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];

                //Temp
                int countStart=0;

                NSArray *arrTemp=[dictionary valueForKey:@"DayWiseClockInOutExtSerDcs"];
                if ([arrTemp isKindOfClass:[NSArray class]])
                {
                    for(int i=0;i<arrTemp.count;i++)
                    {
                        NSDictionary *dict=[arrTemp objectAtIndex:i];
                        NSString *strDate1,*strDate2;
                        strDate1=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeInStr"]];
                        strDate2=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeOutStr"]];
                        
                        NSString *strValue=[self calcaulteTimeIntervalNew:strDate1 :strDate2];
                        double secondsBetween=[strValue doubleValue];
                        countStart=countStart+secondsBetween;
                        
                    }
                }

                //End
                //Current Time
                //count=[strCurrentWorkingTimeInSeconds intValue];
                count=countStart;

                int hr = count/60/60;
                int min =count/60;
                int sec = count;
                if (sec >= 60) {
                    sec = sec % 60;
                }
                if (min >= 60) {
                    min = min % 60;
                }
                _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
                //End
                
                
                if ([strCurrentWorkingStatus isEqualToString:@"<null>"]||[strCurrentWorkingStatus isEqualToString:@"0"])
                {
                    [_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
                }
                else if ([strCurrentWorkingStatus isEqualToString:@"WorkingTime"])
                {
                    if(strTimeOut.length>10)
                    {
                        _btnStartDay.enabled=NO;
                        
                        [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];

                    }
                        
                    else
                    {
                        _btnStartDay.enabled=YES;
                        [_btnBreak setTitle:@"Take Break" forState:UIControlStateNormal];
                        [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                        
                        running = YES;
                        if (myTimer == nil)
                        {
                            myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                        }

                    }
                   
                }
                else if ([strCurrentWorkingStatus isEqualToString:@"BreakTime"])
                {
                    
                    [_btnBreak setTitle:@"End Break" forState:UIControlStateNormal];
                    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                    _btnStartDay.enabled=NO;
                    
                    /*running = YES;
                    if (myTimer == nil)
                    {
                        myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
                    }*/

                }
                else if ([strCurrentWorkingStatus isEqualToString:@"EndDay"])
                {
                    _btnStartDay.enabled=NO;

                    [_btnStartDay setTitle:@"End Day" forState:UIControlStateNormal];
                }
                NSString *strTitle;
                strTitle=_btnStartDay.currentTitle;
                
                if ([strTitle isEqualToString:@"Start Day"])
                {
                    
                    _constTakeBreak_H.constant=0;
                }
                else
                {
                    _constTakeBreak_H.constant=35;
                    
                    if ([strTitle isEqualToString:@"End Day"])
                    {
                        if(strTimeOut.length>10)
                        {
                            _constTakeBreak_H.constant=0;

                        }
                    }
                    
                }
                NSString *strClockId;
                strClockId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeClockInOutId"]];
                strEmployeeId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"EmployeeId"]];
                strWorkingStatusNew=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"WorkingStatus"]];
                strTimeIn=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeIn"]];
                if ([strTimeIn isEqualToString:@"<null>"])
                {
                    strTimeIn=@"";
                }
                strTimeOut=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TimeOut"]];
                if ([strTimeOut isEqualToString:@"<null>"])
                {
                    strTimeOut=@"";
                }
                strReason=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"Reason"]];
                if ([strReason isEqualToString:@"<null>"])
                {
                    strReason=@"";
                }
                strDescrption=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"Description"]];
                if ([strDescrption isEqualToString:@"<null>"])
                {
                    strDescrption=@"";
                }
// Seperate Code
                
                if ([strClockId isEqualToString:@"0"])
                {
                    
                }
                else
                {
                    [self fetchAndUpdateClockInOutDetailForCurrentTimeCalculation:strEmployeeId:strClockId];

                }
                

            }
            
        }
        [DejalActivityView removeView];
    }
}
-(void)getUpdatedClockInOutDate
{
    //http://thrs.stagingsoftware.com//api/EmployeeClockInOut/GetClockInOutByEmployeeNoExt/?companyKey=Automation&employeeNo=2702545469&filterDate=1/22/2018
    
    NSString *strUrlwithString=[NSString stringWithFormat:@"%@/api/EmployeeClockInOut/GetClockInOutByEmployeeNoExt/?companyKey=%@&employeeNo=%@&&filterDate=%@",strClockUrl,strCompanyKeyNew,strEmployeeNo,[self getDateToday]];
    
    NSString *strType=@"";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrlwithString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //End

    @try
    {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict;
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if(ResponseDict.count==0)
        {
        }
        else
        {
            arrResponse=[[NSMutableArray alloc]init];
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"WorkingStatus",
                 @"TimeIn",
                 @"TimeOut",
                 nil];
            value=[NSArray arrayWithObjects:
                   @"Working Status",
                   @"Time In",
                   @"Time Out",
                   nil];
            NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
            
            NSLog(@"Response clock data >> %@",ResponseDict);
            [arrResponse addObject:dict];
            NSArray *tempArr=(NSArray*)ResponseDict;
            for (int i=0; i<tempArr.count; i++)
            {
                [arrResponse addObject:[tempArr objectAtIndex:i]];
            }
        }
        [DejalBezelActivityView removeView];
        NSLog(@"Response Final clock data >> %@",arrResponse);
        [_tblData reloadData];
        
        _const_TblClockIn_H.constant=_tblData.rowHeight*arrResponse.count+20;
        _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
        
        
        _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
        [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
        
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        
    }
    @finally
    {
        
    }
}
-(void)getDetailNonBillabelTime
{
    //http://service.hrms.com/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=Titan&employeeNo=325350&filterDate=1/10/2018
    
    
    NSString *strUrlwithString=[NSString stringWithFormat:@"%@/api/EmployeeNonBillableTime/GetNonBillableTimeByEmployeeNoExt/?companyKey=%@&employeeNo=%@&&filterDate=%@",strClockUrl,strCompanyKeyNew,strEmployeeNo,[self getDateToday]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrlwithString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //End

    
    @try
    {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict;
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if(ResponseDict.count==0)
        {
        }
        else
        {
            arrResponseNonBillable=[[NSMutableArray alloc]init];
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"WO#",
                 @"TimeIn",
                 @"TimeOut",
                 nil];
            value=[NSArray arrayWithObjects:
                   @"WO #",
                   @"Time In",
                   @"Time Out",
                   nil];
            NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
            
            NSLog(@"Response clock data >> %@",ResponseDict);
            [arrResponseNonBillable addObject:dict];
            NSArray *tempArr=(NSArray*)ResponseDict;
            for (int i=0; i<tempArr.count; i++)
            {
                [arrResponseNonBillable addObject:[tempArr objectAtIndex:i]];
            }
        }
        [self fetchTodaysCompleteWorkOrder];
        [DejalBezelActivityView removeView];
        NSLog(@"Response Final clock data >> %@",arrResponseNonBillable);
        
        [_tblData reloadData];
        [_tblDataWorkOrderDetails reloadData];
        
        _const_TblClockIn_H.constant=_tblData.rowHeight*arrResponse.count+20;
        _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
        
        
        _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
        [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
        
    }
    @catch (NSException *exception) {
        
        [global AlertMethod:Alert :Sorry];
        
    }
    @finally
    {
        
    }
}

#pragma mark- ---------- CORE DATA METHODS ---------------

-(void)saveClockInOutDetailCoreData:(NSString*)strClockId :(NSString*)strWorkingStatus
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityClockInOutDetail=[NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    
    ClockInOutDetail *objClockInOutDetail = [[ClockInOutDetail alloc]initWithEntity:entityClockInOutDetail insertIntoManagedObjectContext:context];
    
    objClockInOutDetail.employeeId=strEmployeeId;
    objClockInOutDetail.companyKey=strCompanyKeyNew;
    objClockInOutDetail.userName=strUserNameNew;
    objClockInOutDetail.employeeClockInOutId=strClockId;
    objClockInOutDetail.workingStatus=strWorkingStatus;
    objClockInOutDetail.timeIn=strTimeIn;
    objClockInOutDetail.timeOut=strTimeOut;
    objClockInOutDetail.reason=strReason;
    objClockInOutDetail.descriptionDetail=strDescrption;
    objClockInOutDetail.isActive=@"true";
    objClockInOutDetail.createdBy=strEmployeeId;
    objClockInOutDetail.createdDate=[global strCurrentDate];
    
    NSError *error1;
    [context save:&error1];
}
-(void)updateClockInOutDetailCoreData
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeClockInOutId=%@",strClockInOutId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeClockInOutId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        [matches setValue:strTimeOut forKey:@"timeOut"];
        [context save:&error1];
    }
}

-(void)fetchClockInOutDetailCoreData
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=[arrAllObj lastObject];
        strClockInOutId=[NSString stringWithFormat:@"%@",[matches valueForKey:@"employeeClockInOutId"]];
        strTimeIn=[NSString stringWithFormat:@"%@",[matches valueForKey:@"timeIn"]];
        strWorkingStatusNew=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workingStatus"]];
        //[matches setValue:[self getCurrentTime] forKey:@"timeOut"];
        //[context save:&error1];
    }
}
-(void)fetchAndUpdateClockInOutDetailForCurrentTimeCalculation:(NSString*)strEmpIdId :(NSString*)strClockId
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@ AND employeeClockInOutId =%@",strEmpIdId,strClockId];//employeeClockInOutId
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [self saveClockInOutDetailCoreData:strClockId :strWorkingStatusNew];
    }
    else
    {
        matches=arrAllObj[0];
        
        [matches setValue:strTimeIn forKey:@"timeIn"];
        [matches setValue:strTimeOut forKey:@"timeOut"];
        [matches setValue:strReason forKey:@"reason"];
        [matches setValue:strDescrption forKey:@"descriptionDetail"];
        [matches setValue:strWorkingStatusNew forKey:@"workingStatus"];
        [context save:&error1];
    }
    
}
-(void)fetchClockInOutDetailInOfflineCase
{
    // visibility and Inspector alag se save krna he
    
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityClockInOutDetail= [NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context];
    [request setEntity:entityClockInOutDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];//employeeClockInOutId
    [request setPredicate:predicate];
    
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"employeeId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerClockInOutDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerClockInOutDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerClockInOutDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerClockInOutDetail fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=[arrAllObj objectAtIndex:i];
            NSLog(@"%@",[matches valueForKey:@"timeIn"]);
            NSLog(@"%@",matches );
        }
    }
    
}
-(void)deleteClockRecordExceptToday
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeId=%@",strEmployeeId];//employeeClockInOutId
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setPredicate:predicate];
    [allData setEntity:[NSEntityDescription entityForName:@"ClockInOutDetail" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data)
    {
        //Temp
        NSString *strCurrentDate;
        strCurrentDate=[NSString stringWithFormat:@"%@",[data valueForKey:@"createdDate"]];
        NSDateFormatter *dateFormatter;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        NSDate *date=[dateFormatter dateFromString:strCurrentDate];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString *strDateData=[dateFormatter stringFromDate:date];
        
        NSString *strCurrentDateTime=[dateFormatter stringFromDate:[NSDate date]];
        if (![strDateData isEqualToString:strCurrentDateTime])
        {
            NSLog(@"Previous date data deleted");
            [context deleteObject:data];
           // break;
        }
        
    }
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)fetchTodaysCompleteWorkOrder
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName=%@ AND employeeNo=%@",strUserNameNew,strEmployeeNo];

    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObjWorkOrder = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObjWorkOrder.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjWorkOrder.count; k++)
        {
            matchesWorkOrder=arrAllObjWorkOrder[k];
            NSString *strWOStatus,*strDateWO;//"2018-02-07T05:40:24.337"

            strWOStatus=[matchesWorkOrder valueForKey:@"workorderStatus"];
            strDateWO=[matchesWorkOrder valueForKey:@"timeIn"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];//07:00 pm
            NSDate* newTime = [dateFormatter dateFromString:strDateWO];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            strDateWO = [dateFormatter stringFromDate:newTime];
            
            NSDate *dateWO=[dateFormatter dateFromString:strDateWO];
            
            NSDate *dateToday = [NSDate date];
            
            
            BOOL temp=[self isSameDay:dateWO otherDay:dateToday];
            //if ([dateWO compare:dateToday] == NSOrderedSame)

            if (temp==YES)
            {
                if ([strWOStatus caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strWOStatus caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strWOStatus caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                    NSLog(@"Today Completed WO Matched");
                    
                    NSArray *key,*value;
                    key=[NSArray arrayWithObjects:
                         @"WO#",
                         @"TimeIn",
                         @"TimeOut",
                         nil];
                    NSString *str1Wo,*str2TimeIn,*str3TimeOut;
                    str1Wo=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"workOrderNo"]]];
                    str2TimeIn=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"timeIn"]]];
                    str3TimeOut=[global changeDateToFormattedDateNew:[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"timeOut"]]];
                    value=[NSArray arrayWithObjects:
                           str1Wo,
                           str2TimeIn,
                           str3TimeOut,
                           nil];
                    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
                    [arrResponseNonBillable addObject:dict];
                    
                    }
            }

        }
    }
    [_tblDataWorkOrderDetails reloadData];

}
- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}
#pragma mark- --------------------- LOCAL METHODS -----------------


-(void)getUpdatedRecords
{
    arrResponse=[[NSMutableArray alloc]init];
    NSArray *key,*value;
    key=[NSArray arrayWithObjects:
         @"WorkingStatus",
         @"TimeIn",
         @"TimeOut",
         nil];
    value=[NSArray arrayWithObjects:
           @"Working Status",
           @"Time In",
           @"Time Out",
           nil];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:value forKeys:key];
    [arrResponse addObject:dict];
    
    arrResponseNonBillable=[[NSMutableArray alloc]init];
    NSArray *keyNonBillable,*valueNonBillable;
    keyNonBillable=[NSArray arrayWithObjects:
                    @"WO#",
                    @"TimeIn",
                    @"TimeOut",
                    nil];
    valueNonBillable=[NSArray arrayWithObjects:
                      @"WO #",
                      @"Time In",
                      @"Time Out",
                      nil];
    NSDictionary *dictNonBillable = [[NSDictionary alloc] initWithObjects:valueNonBillable forKeys:keyNonBillable];
    [arrResponseNonBillable addObject:dictNonBillable];
    
    
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Data"];
        
        /* dispatch_async(dispatch_get_main_queue(), ^{
         [DejalBezelActivityView removeView];
         [self getCurrentTimerOfClock];
         });*/
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self getUpdatedClockInOutDate];
        });
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView removeView];
            [self getDetailNonBillabelTime];
        });
        
    }
    [_tblDataWorkOrderDetails reloadData];
    _const_TblClockIn_H.constant=_tblData.rowHeight*arrResponse.count+20;
    _const_TblWorkOrder_H.constant=_tblDataWorkOrderDetails.rowHeight*arrResponseNonBillable.count+20;
    
    
    _constContentView_H.constant=_const_TblWorkOrder_H.constant+_const_TblClockIn_H.constant+_tblDataWorkOrderDetails.frame.origin.y;
    [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,  _constContentView_H.constant+100)];
}

/*-(void)updateTimer
 {
 NSDate *currentDate = [NSDate date];
 NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
 NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"HH:mm:ss"];
 [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
 NSString *timeString=[dateFormatter stringFromDate:timerDate];
 strTimerOnStop=timeString;
 _lblTime.text = timeString;
 }*/

//Nilind 15 Dec

#pragma mark- ------------ DATE PICKER METHOD ------------

-(void)addPickerViewDateTo
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    
    pickerDate.datePickerMode =UIDatePickerModeTime;  //UIDatePickerModeDate;
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=strTitile;
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    [viewForDate addSubview:pickerDate];
}
-(void)setDateOnDone:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setDateFormat:@"HH:mm"];
    [dateFormat setDateFormat:@"hh:mm:a"];
    if ([strForFromTime isEqualToString:@"fromTime"])
    {
        
        strDate = [dateFormat stringFromDate:pickerDate.date];
        strFromTime=strDate;
        [_btnFromTime setTitle:strDate forState:UIControlStateNormal];
        
        
        NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
        
        [dateFormatNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
        
        NSString *strNewDate=[dateFormatNew stringFromDate:pickerDate.date];
        strFromTimeNonBillable=strNewDate;
        NSLog(@"%@",strNewDate);
        
        
    }
    else
    {
        strDate = [dateFormat stringFromDate:pickerDate.date];
        strToTime=strDate;
        [_btnToTime setTitle:strDate forState:UIControlStateNormal];
        
        
        NSDateFormatter *dateFormatNew = [[NSDateFormatter alloc] init];
        [dateFormatNew setDateFormat:@"MM/dd/yyyy hh:mm a"];
        
        NSString *strNewDate=[dateFormatNew stringFromDate:pickerDate.date];
        strToTimeNonBillable=strNewDate;
        NSLog(@"%@",strNewDate);
        
    }
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}

#pragma mark- -------- DATE TIME METHOD -------------
-(void)getDate
{
    
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMM, yyyy";  // very simple format  "8:47:22 AM"
    }
    dateOnFirstTime=now;
    _lblDate.text = [dateFormatter stringFromDate:now];
    
}
- (NSString*)getCurrentTime
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        
    }
    NSString *strCurrentDateTime=[dateFormatter stringFromDate:now];
    return  strCurrentDateTime;
}
- (NSString*)getDateToday
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        
    }
    NSString *strCurrentDateTime=[dateFormatter stringFromDate:now];
    return  strCurrentDateTime;
}
#pragma mark- -------- TIMER METHOD -------------



- (void)stopTimer
{
    
    running = NO;
    [myTimer invalidate];
    myTimer = nil;
    //[_btnStartDay setTitle:@"Start Day" forState:UIControlStateNormal];
    
}
- (void)updateTimer
{
    BOOL chkForCurrentDate;
    chkForCurrentDate=[self isSameDay:dateOnFirstTime otherDay:[NSDate date]];
    if (chkForCurrentDate==YES)
    {
        /*count++;
         int hr = floor(count/100/60/60);
         
         int min = floor(count/100/60);
         int sec = floor(count/100);
         int mSec = count % 100;
         
         if (sec >= 60) {
         sec = sec % 60;
         }
         if (min >= 60) {
         min = min % 60;
         }*/
        count++;
        int hr = count/60/60;
        
        int min =count/60;
        int sec = count;
        int mSec = count % 100;
        
        if (sec >= 60) {
            sec = sec % 60;
        }
        if (min >= 60) {
            min = min % 60;
        }
        _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
        
        //_lblTime.text = [NSString stringWithFormat:@"%02d:%02d",min,sec];
    }
    else
    {
        [self endDayFunctionality];
    }

    
}
-(NSString*)calcaulteTimeInterval:(NSString*)strTimeInCell :(NSString *)strTimeOutCell
{
    NSString *strDuration;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
    {
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
            [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
            
        }
        
        strTimeOutCell=[dateFormatter stringFromDate:now];
    }
    NSDate *date2=[dateFormatter dateFromString:strTimeOutCell];
    NSDate *date1=[dateFormatter dateFromString:strTimeInCell];
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    NSLog(@"%f",secondsBetween);
   /* int hr = secondsBetween/60/60;
    
    int min =secondsBetween/60;
    int sec = secondsBetween;
    //int mSec = secondsBetween % 100;
    
    if (sec >= 60) {
        sec = sec % 60;
    }
    if (min >= 60) {
        min = min % 60;
    }
    strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];*/
    strDuration=[NSString stringWithFormat:@"%f",secondsBetween];
    return strDuration;
}
-(NSString*)calcaulteTimeIntervalNew:(NSString*)strTimeInCell :(NSString *)strTimeOutCell
{
    NSString *strDuration;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    if (strTimeOutCell.length==0||[strTimeOutCell isEqualToString:@"<null>"]||[strTimeOutCell isEqual:NULL])
    {
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            //dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
            [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
            
        }
        
        strTimeOutCell=[dateFormatter stringFromDate:now];
    }
    NSDate *date2=[dateFormatter dateFromString:strTimeOutCell];
    NSDate *date1=[dateFormatter dateFromString:strTimeInCell];
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    NSLog(@"%f",secondsBetween);
    /* int hr = secondsBetween/60/60;
     
     int min =secondsBetween/60;
     int sec = secondsBetween;
     //int mSec = secondsBetween % 100;
     
     if (sec >= 60) {
     sec = sec % 60;
     }
     if (min >= 60) {
     min = min % 60;
     }
     strDuration=[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];*/
    strDuration=[NSString stringWithFormat:@"%f",secondsBetween];
    return strDuration;
}

- (void)updateTimerNew {
    
    count++;
    int hr = count/60/60;
    
    int min =count/60;
    int sec = count;
    int mSec = count % 100;
    
    if (sec >= 60) {
        sec = sec % 60;
    }
    if (min >= 60) {
        min = min % 60;
    }
    
    //[_btnTemp setTitle:[NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec] forState:UIControlStateNormal];
    /*count++;
     int hr = floor(count/100/60/60);
     
     int min = floor(count/100/60);
     int sec = floor(count/100);
     int mSec = count % 100;
     
     if (sec >= 60) {
     sec = sec % 60;
     }
     if (min >= 60) {
     min = min % 60;
     }*/
    
    _lblTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
    
    
}
/*-(void)tapDetectedOnBackGroundView
 {
 [viewForDate removeFromSuperview];
 [viewBackGround removeFromSuperview];
 }*/


- (IBAction)actionOnBtnTemp:(id)sender
{
    
    if (running == NO)
    {
        
        running = YES;
        
        if (myTimer == nil)
        {
            
            myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerNew) userInfo:nil repeats:YES];
            
        }
    }
    else
    {
        [self stopTimer];

    }
}
#pragma mark- -----------Text Field Delegate Method --------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    //[self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //[self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [self.view endEditing:YES];
    return YES;
}

#pragma mark- -----------Text View Delegate Method --------------

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    //isEditedInSalesAuto=YES;
    NSLog(@"Database edited");
    if([text isEqualToString:@"\n"])
    {
        
        [_txtViewNonBillableDescription resignFirstResponder];
        [_txtBreakDescription resignFirstResponder];
        return NO;
    }
    
    
    return textView.text.length + (text.length - range.length) <= 5000;
    
    
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    //[self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    //[self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    [self.view endEditing:YES];
    
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
}
-(BOOL)checkForCurrentDate: (NSDate *)date1 :(NSDate *)date2
{
    BOOL chk;
    
    chk=[self isSameDay:date1 otherDay:date2];
    return chk;
}

-(BOOL)isValidTimeToAdd

{
    
    NSLog(@"%@",arrResponseNonBillable);
    
    
    
    NSString *strCurrentDate = [global getCurrentDate];
    
    NSString *strFromDateNew = [NSString stringWithFormat:@"%@ %@",strCurrentDate,strFromTime];
    
    NSString *strToDateNew = [NSString stringWithFormat:@"%@ %@",strCurrentDate,strToTime];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    NSDate *date1 = [dateFormat dateFromString:strFromDateNew];
    
    NSDate *date2 = [dateFormat dateFromString:strToDateNew];
    
    
    
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    
    NSString *str1 = [dateFormat stringFromDate:date1];
    
    NSString *str2 = [dateFormat stringFromDate:date2];
    
    
    NSDate *dateSelectedFromTime = [dateFormat dateFromString:str1];
    
    NSDate *dateSelectedToTime = [dateFormat dateFromString:str2];
    
    
    
    
    
    
    BOOL chkValidToAdd;
    
    chkValidToAdd = NO;
    
    
    
    for (int i=1; i<arrResponseNonBillable.count; i++)
        
    {
        
        
        
        //2019-07-12T15:18:00
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        
        
        
        
        NSDictionary *dict = [arrResponseNonBillable objectAtIndex:i];
        
        NSString *strFromDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FromDate"]]];
        
        
        
        NSString *strToDate= [self ChangeDateToLocalDateClock:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ToDate"]]];
        
        
        
        NSDate *fromDateInResponse,*toDateInResponse;
        
        fromDateInResponse = [dateFormat dateFromString:strFromDate];
        
        toDateInResponse = [dateFormat dateFromString:strToDate];
        
        
        
        //strFromDate = [dateFormat stringFromDate:fromDateInResponse];
        
        // strToDate = [dateFormat stringFromDate:toDateInResponse];
        
        
        
        if ((([fromDateInResponse compare:dateSelectedFromTime] == NSOrderedAscending)&&([dateSelectedFromTime compare:toDateInResponse] == NSOrderedAscending)) || (([fromDateInResponse compare:dateSelectedToTime] == NSOrderedAscending)&&([dateSelectedToTime compare:toDateInResponse] == NSOrderedAscending)))
            
        {
            
            NSLog(@"Time slot not available");
            
            chkValidToAdd = NO;
            
            break;
            
            
            
        }
        
        else
            
        {
            
            NSLog(@"Time slot available");
            
            
            
            chkValidToAdd = YES;
            
        }
        
        
        
        
    }
    
    
    
    
    
    return chkValidToAdd;
    
}

-(NSString *)ChangeDateToLocalDateClock :(NSString*)strDateToConvert{
    
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    
    
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    
    //Add the following line to display the time in the local time zone
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
    
    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        
        
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    
    
    if (finalTime.length==0)
        
    {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        
        NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
        
        //Add the following line to display the time in the local time zone
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:a"];
        
        finalTime = [dateFormatter stringFromDate:newTime];
        
        
        
    }
    
    if (finalTime.length==0) {
        
        
        
        finalTime=@"";
        
        
        
    }
    
    return finalTime;
    
}
@end
