//
//  ClockInOutViewController.h
//  DPS
//
//  Created by Rakesh Jain on 21/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ClockInOutDetail+CoreDataClass.h"
#import "ClockInOutDetail+CoreDataProperties.h"
@interface ClockInOutViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityClockInOutDetail,*entityWorkOderDetailServiceAuto;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches,*matchesWorkOrder;
    NSArray *arrAllObj,*arrAllObjWorkOrder;

}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerClockInOutDetail,*fetchedResultsControllerServiceAutomation;

- (IBAction)actionOnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
@property (weak, nonatomic) IBOutlet UITableView *tblDataWorkOrderDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnStartDay;
- (IBAction)actionOnStartDay:(id)sender;

- (IBAction)actionOnTakeBreak:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBreak;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTakeBreak_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblClockIn_H;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_TblWorkOrder_H;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constContentView_H;
- (IBAction)actionOnNonBillableTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNonBillabelTime;

//View NonBillable
@property (strong, nonatomic) IBOutlet UIView *viewForAddNonBillableTime;
@property (weak, nonatomic) IBOutlet UIButton *btnFromTime;
- (IBAction)actionOnBtnFromTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnToTime;
- (IBAction)actionOnBtnToTime:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNonBillableDescription;
- (IBAction)actionOnSaveForNonBillableTime:(id)sender;
- (IBAction)actionOnCancelForNonBillableTime:(id)sender;

// View For Break
@property (strong, nonatomic) IBOutlet UIView *viewForBreak;
@property (weak, nonatomic) IBOutlet UITextField *txtBreakReason;
@property (weak, nonatomic) IBOutlet UITextView *txtBreakDescription;
- (IBAction)actionOnSaveForBreak:(id)sender;
- (IBAction)actionOnCancelForBreak:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTemp;
- (IBAction)actionOnBtnTemp:(id)sender;

@end
