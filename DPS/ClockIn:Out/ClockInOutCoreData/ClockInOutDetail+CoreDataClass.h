//
//  ClockInOutDetail+CoreDataClass.h
//  
//
//  Created by Rakesh Jain on 18/01/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClockInOutDetail : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ClockInOutDetail+CoreDataProperties.h"
