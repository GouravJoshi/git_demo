//
//  ClockInOutDetail+CoreDataProperties.m
//  
//
//  Created by Rakesh Jain on 18/01/18.
//
//

#import "ClockInOutDetail+CoreDataProperties.h"

@implementation ClockInOutDetail (CoreDataProperties)

+ (NSFetchRequest<ClockInOutDetail *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ClockInOutDetail"];
}

@dynamic companyKey;
@dynamic userName;
@dynamic employeeClockInOutId;
@dynamic employeeId;
@dynamic workingStatus;
@dynamic timeIn;
@dynamic timeOut;
@dynamic reason;
@dynamic descriptionDetail;
@dynamic isActive;
@dynamic createdBy;
@dynamic createdDate;

@end
