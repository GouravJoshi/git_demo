//
//  ClockInOutDetail+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 18/01/18.
//
//

#import "ClockInOutDetail+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ClockInOutDetail (CoreDataProperties)

+ (NSFetchRequest<ClockInOutDetail *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *employeeClockInOutId;
@property (nullable, nonatomic, copy) NSString *employeeId;
@property (nullable, nonatomic, copy) NSString *workingStatus;
@property (nullable, nonatomic, copy) NSString *timeIn;
@property (nullable, nonatomic, copy) NSString *timeOut;
@property (nullable, nonatomic, copy) NSString *reason;
@property (nullable, nonatomic, copy) NSString *descriptionDetail;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;

@end

NS_ASSUME_NONNULL_END
