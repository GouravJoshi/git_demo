//
//  ClockInOutTableViewCell.h
//  DPS
/////
//  Created by Rakesh Jain on 21/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
////

#import <UIKit/UIKit.h>

@interface ClockInOutTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeIn;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeOut;
@property (weak, nonatomic) IBOutlet UILabel *lblWONo;
@end
