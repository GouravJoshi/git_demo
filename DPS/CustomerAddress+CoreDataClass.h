//
//  CustomerAddress+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 28/11/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface CustomerAddress : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CustomerAddress+CoreDataProperties.h"
