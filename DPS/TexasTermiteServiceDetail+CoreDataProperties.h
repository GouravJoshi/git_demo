//
//  TexasTermiteServiceDetail+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/11/17.
//
//

#import "TexasTermiteServiceDetail+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TexasTermiteServiceDetail (CoreDataProperties)

+ (NSFetchRequest<TexasTermiteServiceDetail *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, retain) NSObject *texasTermiteServiceDetail;

@end

NS_ASSUME_NONNULL_END
