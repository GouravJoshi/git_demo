//
//  WoOtherDocuments+CoreDataProperties.h
//  
//
//  Created by Rakesh Jain on 18/09/17.
//
//

#import "WoOtherDocuments+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoOtherDocuments (CoreDataProperties)

+ (NSFetchRequest<WoOtherDocuments *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *isChecked;
@property (nullable, nonatomic, copy) NSString *isDefault;
@property (nullable, nonatomic, copy) NSString *otherDocSysName;
@property (nullable, nonatomic, copy) NSString *otherDocumentPath;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *woOtherDocumentId;
@property (nullable, nonatomic, copy) NSString *workorderId;//userName userName
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;

@end

NS_ASSUME_NONNULL_END
