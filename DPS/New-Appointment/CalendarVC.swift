//
//  CalendarVC.swift
//  DPS
//
//  Created by NavinPatidar on 9/9/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import KVKCalendar
import CoreData

protocol AddedBlockTimeProtocol : class {
    func refreshAppointmentList(strTodayAll: String)
}

class CalendarVC: UIViewController {
    
    //Mark: IBOutlet--------
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewTodaybutton: UIView!
    
    @IBOutlet weak var btnAppointment: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var viewTopButton: UIView!
    
    var aryData = NSMutableArray()
    var dictDataForCalender = NSMutableDictionary()
    var strIsGGQIntegration = ""
    var isBlockTimeAdded = false
    weak var handelDataRefreshOnAddingBlockTime: AddedBlockTimeProtocol?
    var strTodayAll = "Today"
    var strEmpLoginBranchSysName = ""
    var dictLoginData = NSDictionary()
    var aryApprovalCheckList = NSMutableArray()
    
    private var events = [Event]()
    private var selectDate: Date = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        // return formatter.date(from: "14.12.2018") ?? Date()
        return Date()
    }()
    
    private lazy var todayButton: UIButton = {
        if DeviceType.IS_IPAD {
            let button:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 75, height: 60))
            button.setTitleColor(UIColor.red, for: .normal)
            button.setTitle("Today", for: .normal)
            button.addTarget(self, action:#selector(self.today), for: .touchUpInside)
            return button
        }else{
            let button:UIButton = UIButton(frame: CGRect(x: 0, y: 10, width: 75, height: 30))
            button.setTitleColor(UIColor.red, for: .normal)
            button.setTitle("Today", for: .normal)
            button.addTarget(self, action:#selector(self.today), for: .touchUpInside)
            return button
        }
       
    }()
    
    private lazy var style: Style = {
        var style = Style()
        if UIDevice.current.userInterfaceIdiom == .phone {
            style.timeline.widthTime = 40
            style.timeline.currentLineHourWidth = 45
            style.timeline.offsetTimeX = 2
            style.timeline.offsetLineLeft = 2
        } else {
            style.timeline.widthEventViewer = 0.0
        }
        style.timeline.startFromFirstEvent = false
        style.followInSystemTheme = true
        style.timeline.offsetTimeY = 80
        style.timeline.offsetEvent = 3
        style.allDay.isPinned = true
        style.startWeekDay = .sunday
        style.timeHourSystem = .twelveHour
        style.event.isEnableMoveEvent = true
        return style
    }()
    
    private lazy var calendarView: CalendarView = {
        let calendar = CalendarView(frame: view.frame, date: selectDate, style: style)
        calendar.delegate = self
        calendar.dataSource = self
        return calendar
    }()
    
    private lazy var segmentedControl: UISegmentedControl = {
        // let array = CalendarType.AllCases
       
        let array = [CalendarType.day,CalendarType.week,CalendarType.month]
        
        let control = UISegmentedControl(items: array.map({ $0.rawValue.capitalized }))
        control.tintColor = .systemRed
        control.selectedSegmentIndex = 0
        control.addTarget(self, action: #selector(switchCalendar), for: .valueChanged)
        
        if DeviceType.IS_IPAD {
            let font = UIFont.systemFont(ofSize: 22)
            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        } else {
            let font = UIFont.systemFont(ofSize: 18)
            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        }
        return control
    }()
    
    private lazy var eventViewer: EventViewer = {
        let view = EventViewer(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: calendarView.frame.height))
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
            self.aryApprovalCheckList =  getApprovalCheckList().mutableCopy() as! NSMutableArray
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)

            setUPUI()
        }
    }
    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            setUPUI()
        } else {
            setUPUI()
        }
    }

    func setUPUI()  {
        let aryCalenderData = NSMutableArray()
        //let dictData = NSMutableDictionary()

        for (index, _) in aryData.enumerated() {
            
            let dictDataSalesDB = aryData[index] as! NSManagedObject
            
            if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {

                let blockTitle = "\(dictDataSalesDB.value(forKey: "titleBlockTime")!)"
                
                let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "fromDate")!)")
                var scheduleEndDate = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "toDate")!)")
                let strtitle =  "Block: \(blockTitle)"
                
                print("WorkOrder--------\(strtitle)")
                print("WorkOrder----start----\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("WorkOrder---End-----\(convertDateFormat(inputDate: scheduleEndDate!))")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                let datescheduleStartDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleStartDate!))")!
                let datescheduleEndDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleEndDate!))")!
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
                if(datescheduleStartDate == datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                else if(datescheduleStartDate > datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                print("WorkOrder---Update-start----\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("WorkOrder--Update-End-----\(convertDateFormat(inputDate: scheduleEndDate!))")
                
                let dictData = NSMutableDictionary()

                dictData.setValue(0, forKey: "all_day")
                dictData.setValue("\(index)", forKey: "id")
                dictData.setValue("#FFFFFF", forKey: "border_color")
                dictData.setValue("#94a9d7", forKey: "color")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleEndDate!))", forKey: "end")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleStartDate!))", forKey: "start")
                dictData.setValue("#000000", forKey: "text_color")
                dictData.setValue("Block: \(blockTitle)", forKey: "title")
                dictData.setValue([], forKey: "files")
                aryCalenderData.add(dictData)
                
                
                
            }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
                let taskName = "\(dictDataSalesDB.value(forKey: "taskName") ?? "")"
                
                let crmContactName = "\(dictDataSalesDB.value(forKey: "crmContactName") ?? "")"

                
                let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "dueDate")!)") //dueDate
                var scheduleEndDate = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "endDate")!)" == "" ? "\(dictDataSalesDB.value(forKey: "dueDate")!)" : "\(dictDataSalesDB.value(forKey: "endDate")!)" )
                let strtitle =  "\(taskName)"
                print("sales--------\(strtitle)")
                print("sales--Start------\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("sales--End------\(convertDateFormat(inputDate: scheduleEndDate!))")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                let datescheduleStartDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleStartDate!))")!
                let datescheduleEndDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleEndDate!))")!
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
                if(datescheduleStartDate == datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                else if(datescheduleStartDate > datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                print("sales--UpdateStart------\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("sales--UpdateEnd------\(convertDateFormat(inputDate: scheduleEndDate!))")
                let dictData = NSMutableDictionary()

                dictData.setValue(0, forKey: "all_day")
                dictData.setValue("\(index)", forKey: "id")
                dictData.setValue("#FFFFFF", forKey: "border_color")
                dictData.setValue("#94a9d7", forKey: "color")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleEndDate!))", forKey: "end")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleStartDate!))", forKey: "start")
                dictData.setValue("#000000", forKey: "text_color")
                dictData.setValue("\(crmContactName)\n\(strtitle)", forKey: "title")
                dictData.setValue([], forKey: "files")
                aryCalenderData.add(dictData)
                
            }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                
                var strName = ""
                
                if "\("\(dictDataSalesDB.value(forKey: "firstName") ?? "")")".count > 0
                {
                    strName = "\("\(dictDataSalesDB.value(forKey: "firstName") ?? "")")"
                }
                if "\("\(dictDataSalesDB.value(forKey: "middleName") ?? "")")".count > 0
                {
                    strName = "\(strName) \("\(dictDataSalesDB.value(forKey: "middleName") ?? "")")"
                }
                if "\("\(dictDataSalesDB.value(forKey: "lastName") ?? "")")".count > 0
                {
                    strName = "\(strName) \("\(dictDataSalesDB.value(forKey: "lastName") ?? "")")"
                }
                
                let accNumber = "\(dictDataSalesDB.value(forKey: "accountNo")!)"
                var accNoTxt = ""
                if accNumber.count > 0 {
                    accNoTxt = "Acc #: " + accNumber
                } else {
                    accNoTxt = "Third Party Acc #: " + "\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo")!)"
                }
                let workOrderNo = "\(dictDataSalesDB.value(forKey: "workOrderNo")!)"
                let workorderStatus = "\(dictDataSalesDB.value(forKey: "workorderStatus")!)"
                let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime")!)")
                var scheduleEndDate = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleEndDateTime")!)")
                let strtitle =  "wo#: \(workOrderNo) ,service - \(workorderStatus)"
                print("WorkOrder--------\(strtitle)")
                print("WorkOrder----start----\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("WorkOrder---End-----\(convertDateFormat(inputDate: scheduleEndDate!))")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                let datescheduleStartDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleStartDate!))")!
                let datescheduleEndDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleEndDate!))")!
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
                if(datescheduleStartDate == datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                else if(datescheduleStartDate > datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                print("WorkOrder---Update-start----\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("WorkOrder--Update-End-----\(convertDateFormat(inputDate: scheduleEndDate!))")
                
                //Nilind
                let arrTotalEstimationTime = "\(dictDataSalesDB.value(forKey: "totalEstimationTime") ?? "")".components(separatedBy: ":")
                var totalTime = 0
                
                if arrTotalEstimationTime.count > 0
                {
                    totalTime = totalTime + (((arrTotalEstimationTime[0] as NSString).integerValue) * (60*60))
                    totalTime = totalTime + (((arrTotalEstimationTime[1] as NSString).integerValue) * (60))
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(TimeInterval(totalTime)))
                }
                //End
                
                
                
                let dictData = NSMutableDictionary()

                dictData.setValue(0, forKey: "all_day")
                dictData.setValue("\(index)", forKey: "id")
                dictData.setValue("#FFFFFF", forKey: "border_color")
                dictData.setValue("#94a9d7", forKey: "color")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleEndDate!))", forKey: "end")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleStartDate!))", forKey: "start")
                dictData.setValue("#000000", forKey: "text_color")
                
                if strName.count > 0
                {
                    dictData.setValue("\(strName)\nWO #: \(workOrderNo),\(accNoTxt) ,service - \(workorderStatus)", forKey: "title")
                }
                else
                {
                    dictData.setValue("WO #: \(workOrderNo),\(accNoTxt) ,service - \(workorderStatus)", forKey: "title")
                }
                dictData.setValue([], forKey: "files")
                aryCalenderData.add(dictData)
                
                //Nilind
                
            } else{
                
                let leadNumber = "\(dictDataSalesDB.value(forKey: "leadNumber")!)"
                let accNumber = "\(dictDataSalesDB.value(forKey: "accountNo")!)"
                var accNoTxt = ""
                
                
                var strCustomerName = ""
                
                if "\("\(dictDataSalesDB.value(forKey: "firstName") ?? "")")".count > 0
                {
                    strCustomerName = "\("\(dictDataSalesDB.value(forKey: "firstName") ?? "")")"
                }
                if "\("\(dictDataSalesDB.value(forKey: "middleName") ?? "")")".count > 0
                {
                    strCustomerName = "\(strCustomerName) \("\(dictDataSalesDB.value(forKey: "middleName") ?? "")")"
                }
                if "\("\(dictDataSalesDB.value(forKey: "lastName") ?? "")")".count > 0
                {
                    strCustomerName = "\(strCustomerName) \("\(dictDataSalesDB.value(forKey: "lastName") ?? "")")"
                }
                
                if accNumber.count > 0 {
                    accNoTxt = "Acc #: " + accNumber
                } else {
                    accNoTxt = "Third Party Acc #: " + "\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo")!)"
                }

                let statusSysName = "\(dictDataSalesDB.value(forKey: "statusSysName")!)"
                let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDate")!)")
                var scheduleEndDate = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleEndDate")!)")
                let strtitle =  "opp # : \(leadNumber) ,Sales - \(statusSysName)"
                print("sales--------\(strtitle)")
                print("sales--Start------\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("sales--End------\(convertDateFormat(inputDate: scheduleEndDate!))")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                let datescheduleStartDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleStartDate!))")!
                let datescheduleEndDate = dateFormatter.date(from:"\(convertDateFormat(inputDate: scheduleEndDate!))")!
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
                if(datescheduleStartDate == datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                else if(datescheduleStartDate > datescheduleEndDate){
                    scheduleEndDate = dateFormatter.string(from: datescheduleStartDate.addingTimeInterval(50 * 60))
                }
                print("sales--UpdateStart------\(convertDateFormat(inputDate: scheduleStartDate!))")
                print("sales--UpdateEnd------\(convertDateFormat(inputDate: scheduleEndDate!))")
                // Test Saavan
                let dictData = NSMutableDictionary()

                dictData.setValue(0, forKey: "all_day")
                dictData.setValue("\(index)", forKey: "id")
                dictData.setValue("#FFFFFF", forKey: "border_color")
                dictData.setValue("#94a9d7", forKey: "color")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleEndDate!))", forKey: "end")
                dictData.setValue("\(convertDateFormat(inputDate: scheduleStartDate!))", forKey: "start")
                dictData.setValue("#000000", forKey: "text_color")
                
                if strCustomerName.count > 0
                {
                    dictData.setValue("\(strCustomerName)\nOpp #: \(leadNumber),\(accNoTxt) ,Sales - \(statusSysName)", forKey: "title")
                }
                else
                {
                    dictData.setValue("Opp #: \(leadNumber),\(accNoTxt) ,Sales - \(statusSysName)", forKey: "title")
                }
                //dictData.setValue("opp # : \(leadNumber),\(accNoTxt) ,Sales - \(statusSysName)", forKey: "title")
                
                dictData.setValue([], forKey: "files")
                
                
                if "\(dictDataSalesDB.value(forKey: "isInvisible") ?? "")".caseInsensitiveCompare("true") == .orderedSame ||  "\(dictDataSalesDB.value(forKey: "isInvisible") ?? "")" == "1"
                {
                    
                }
                else
                {
                    aryCalenderData.add(dictData)
                }
                
                //aryCalenderData.add(dictData)
            }
        }
        print(aryData.count)
        print(aryCalenderData.count)
        dictDataForCalender.setValue(aryCalenderData, forKey: "data")
        //viewTopButton.backgroundColor = UIColor.white
        //btnAppointment.layer.cornerRadius = btnAppointment.frame.size.height/2
        //btnAppointment.layer.borderWidth = 0
        //viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        //viewTopButton.layer.borderWidth = 0
        //btnCalendar.layer.cornerRadius = btnCalendar.frame.size.height/2
        //btnCalendar.layer.borderWidth = 0
        //viewTopButton.bringSubviewToFront(btnCalendar)
        
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        viewContain.addSubview(calendarView)
        if DeviceType.IS_IPAD {
            segmentedControl.frame = CGRect(x: 20, y: 0, width: self.view.frame.width - 100, height: 60)

        }else{
            segmentedControl.frame = CGRect(x: 20, y: 10, width: self.view.frame.width - 100, height: 30)

        }
      
        
        
        viewTop.addSubview(segmentedControl)
        viewTodaybutton.addSubview(todayButton)
        // navigationItem.titleView = segmentedControl
        //navigationItem.rightBarButtonItem = todayButton
        calendarView.addEventViewToDay(view: eventViewer)
        createEvents { (events) in
            self.events = events
            self.calendarView.reloadData()
        }
    }
    
    func convertDateFormat(inputDate: String) -> String {
        
        let inputDateee = changeStringDateToGivenFormat(strDate: inputDate, strRequiredFormat: "MM/dd/yyyy hh:mm a")
        
        if(inputDateee == "" || inputDateee == "<null>"){
            return  ""
        }
        
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let oldDate = olDateFormatter.date(from: inputDate)
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return convertDateFormatter.string(from: oldDate!)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var frame = viewContain.frame
        frame.origin.y = 0
        calendarView.reloadFrame(frame)
    }
    
    //MARK: -------------- Switch Branch ----------------
    
    func dateAfterDays(noOfDays : Int, strDate : String) -> Bool  {
        
        let todayStringDate = Global().strCurrentDateFormatted("MM/dd/yyyy hh:mm a", "EST")
        
        var today = changeStringDateToESTDate(strDate: "\(todayStringDate ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")

        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let todayDate = dateFormatter.date(from: todayStringDate!)
        today = todayDate!
        
        let date = dateFormatter.date(from: strDate)
        
        if let dateNew = date   //date new - work order date + no days  // today - current date
        {
            let modifiedDate = Calendar.current.date(byAdding: .day, value: noOfDays, to: dateNew)!
            
            print(modifiedDate)
            
            if today > modifiedDate
            {
                return true
            }
            else
            {
                return  false
            }
            
        }
        else
        {
            return   false
        }
    }
    
    func getPendingWorkOrders(arrayObj : NSArray) -> (arrayPendingWo : NSArray, strMessage : String) {
        
        let arrMutable = NSMutableArray()
        var arrFinal = NSArray()
        var strMessage = ""
        
        for item in arrayObj
        {
            let objMatched = item as! NSManagedObject
            
            if objMatched .isKind(of: WorkOrderDetailsService.self)
            {
                let strWoStageId = "\(objMatched.value(forKey: "wdoWorkflowStageId") ?? "")"
                //let strWoFlowStage = "\(objMatched.value(forKey: "wdoWorkflowStage") ?? "")"
                let wdoWorkflowStatus = "\(objMatched.value(forKey: "wdoWorkflowStatus") ?? "")".lowercased()
                let strWoId = "\(objMatched.value(forKey: "workOrderNo") ?? "")"
                if !strWoStageId.isEmpty && Double(strWoStageId) != 0 && (wdoWorkflowStatus.lowercased() != WdoWorkflowStatusEnum.Complete.rawValue.lowercased() && wdoWorkflowStatus.lowercased() != WdoWorkflowStatusEnum.Completed.rawValue.lowercased())
                {
                    let arrTemp = aryApprovalCheckList.filter { (dict) -> Bool in
                        
                        return "\((dict as! NSDictionary).value(forKey: "ApprovalCheckListId") ?? "")" == strWoStageId && ("\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")" == "1" || "\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")".caseInsensitiveCompare("true") == .orderedSame) && ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector")//(("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector") || ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Branch"))
                        
                    } as NSArray
                    
                    if arrTemp.count > 0
                    {
                        let dictObject = arrTemp.object(at: 0) as! NSDictionary
                        let noOfDays = Int("\(dictObject.value(forKey: "NoOfDays") ?? "")") ?? 0
                        
                        if dateAfterDays(noOfDays: noOfDays, strDate: changeStringDateToGivenFormatEST(strDate: "\(objMatched.value(forKey: "wdoWoStageDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a"))
                        {
                            arrMutable.add(strWoId) // strWOId = Workorder No
                        }
                    }
                }
                else if ((strWoStageId.isEmpty || Double(strWoStageId) == 0) && wdoWorkflowStatus == WdoWorkflowStatusEnum.Pending.rawValue.lowercased())
                 {
                     let arrTemp = aryApprovalCheckList.filter { (dict) -> Bool in
                         
                         return ("\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")" == "1" || "\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")".caseInsensitiveCompare("true") == .orderedSame) && ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector")//(("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector") || ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Branch"))
                         
                     } as NSArray
                     if arrTemp.count > 0
                     {
                         let dictObject = arrTemp.object(at: 0) as! NSDictionary
                         let noOfDays = Int("\(dictObject.value(forKey: "NoOfDays") ?? "")") ?? 0
                         
                         if dateAfterDays(noOfDays: noOfDays, strDate: changeStringDateToGivenFormatEST(strDate: "\(objMatched.value(forKey: "wdoWoStageDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a"))
                         {
                             arrMutable.add(strWoId) // strWOId = Workorder No
                         }
                     }
                 }
            }
        }
        
        arrFinal = arrMutable.mutableCopy() as! NSArray
        
        strMessage = arrFinal.componentsJoined(by: ",")
        
        if arrFinal.count == 1
        {
            strMessage = "You can not Process Other Work Order Beacuse Wo # \(strMessage) is Pending"
        }
        else
        {
            strMessage = "You can not Process Other Work Order Beacuse Wo # \(strMessage) are Pending"
        }
        
        return (arrFinal, strMessage)
    }
    
    func getApprovalCheckList() -> NSArray {
   
        let strBranchSysName = "\(Global().getEmployeeBranchSysName() ?? "")"
        
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") is NSArray){
                let aryTemp = (dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") as! NSArray).filter { (task) -> Bool in
                    
                    //return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true"))  && "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")
                    
                    return "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")

                }
                return aryTemp as NSArray
                
            }
            
        }
        return NSArray()
    }
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    private func showAlertForBranchChange(strAppoinntmentBranch : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "Appointment"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
   
    // MARK:
    //MARK:---------IBAction
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = (sender as AnyObject).bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            let AddBlockTime = (UIAlertAction(title: "Add Block Time", style: .default , handler:{ (UIAlertAction)in
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "AddBlockTime" : "AddBlockTime", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddBlockTimeVC") as? AddBlockTimeVC
                vc?.handelDataOnAddingBlockTime = self
                self.navigationController?.pushViewController(vc!, animated: false)
            }))
            
            AddBlockTime.setValue(#imageLiteral(resourceName: "addBlockIcon"), forKey: "image")
            AddBlockTime.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddBlockTime)
            
            let AddTask = (UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                
                print("User click Add Task button")
                let defs = UserDefaults.standard
                defs.setValue(nil, forKey: "TaskFilterSort")
                defs.synchronize()
                
                let objWebService = WebService()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
                
                objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
            }))
            
            AddTask.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
            AddTask.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddTask)
            
            
            
            let AddActivity = (UIAlertAction(title: "Add Activity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Activity button")
                
                let objWebService = WebService()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
                
                
                
                objByProductVC.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddActivity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
            AddActivity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddActivity)
            
            let AddLead = (UIAlertAction(title: "Add Lead/Opportunity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Add Lead button")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
                self.navigationController?.pushViewController(vc, animated: false)
            }))
            AddLead.setValue(#imageLiteral(resourceName: "Lead"), forKey: "image")
            AddLead.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddLead)
            
            
            let AddContact = (UIAlertAction(title: "Add Contact", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Contact  button")
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                
                
                
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddContact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
            AddContact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddContact)
            
            
            let AddCompany = (UIAlertAction(title: "Add Company", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Company  button")
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                
                
                self.navigationController?.pushViewController(controller, animated: false)
            }))
            AddCompany.setValue(#imageLiteral(resourceName: "CompanyAdd"), forKey: "image")
            AddCompany.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddCompany)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    @IBAction func actionOnList(_ sender: UIButton) {
        if isBlockTimeAdded {
            self.handelDataRefreshOnAddingBlockTime?.refreshAppointmentList(strTodayAll: strTodayAll)
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnDashBoard(_ sender: UIButton) {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    
    @objc func today(sender: UIBarButtonItem) {
        calendarView.scrollTo(Date())
    }
    
    @objc func switchCalendar(sender: UISegmentedControl) {
        let type = CalendarType.allCases[sender.selectedSegmentIndex]
        calendarView.set(type: type, date: selectDate)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        createEvents { (events) in
            self.events = events
            self.calendarView.reloadData()
        }
    }
    
    
    func createEvents(completion: ([Event]) -> Void) {
        //   let models = aryData as! NSArray
        saveToJsonFile()
        var data = Data()
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("Persons.json")
        // Read data from .json file and transform data into an array
        do {
            data = try Data(contentsOf: fileUrl, options: [])
        } catch {
            print(error)
        }
        let decoder = JSONDecoder()
        let result = try? decoder.decode(ItemData.self, from: data)
        
        let events = result!.data.compactMap({ (item) -> Event in
            let startDate = self.formatter(date: item.start)
            let endDate = self.formatter(date: item.end)
            let startTime = self.timeFormatter(date: startDate)
            let endTime = self.timeFormatter(date: endDate)
            
            var event = Event()
            event.ID = item.id
            event.start = startDate
            event.end = endDate
            event.color = EventColor(item.color)
            event.isAllDay = item.allDay
            event.isContainsFile = !item.files.isEmpty
            event.textForMonth = item.title
              event.text = "\(item.title)"
            //event.text = "Start-\(startTime) - End \(endTime)\n\(item.title)"
            return event
        })
        completion(events)
    }
    
    func timeFormatter(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = style.timeHourSystem.format
        return formatter.string(from: date)
    }
    
    func formatter(date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: date) ?? Date()
    }
    func saveToJsonFile() {
        
        
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Persons.json")
        do {
            let data = try JSONSerialization.data(withJSONObject: self.dictDataForCalender, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    // MARK: -  ------------------------- Redirection On did select functions -----------------------

     func goToSalesFlow(dictDataSalesLocal : NSManagedObject) {
         
         nsud.set("\(dictDataSalesLocal.value(forKey: "leadId")!)", forKey: "LeadId")
         nsud.set("\(dictDataSalesLocal.value(forKey: "accountNo")!)", forKey: "AccountNos")
         nsud.set("\(dictDataSalesLocal.value(forKey: "leadNumber")!)", forKey: "LeadNumber")
         nsud.set("\(dictDataSalesLocal.value(forKey: "statusSysName")!)", forKey: "leadStatusSales")
         nsud.set("\(dictDataSalesLocal.value(forKey: "stageSysName")!)", forKey: "stageSysNameSales")
         nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
         if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
             
             nsud.set(true, forKey: "YesSurveyService")
             
         } else {

             nsud.set(false, forKey: "YesSurveyService")

         }
         nsud.synchronize()
         
         let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
         vc!.dataGeneralInfo = dictDataSalesLocal
         self.navigationController?.pushViewController(vc!, animated: false)
         
        
//        if DeviceType.IS_IPAD {
//
//            if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Residential"
//            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
//                vc!.dataGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
//            }
//            else if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Commercial"
//            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfoiPad") as? SaleAutomationGeneralInfoiPad
//                vc!.matchesGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
//            }
//
//
//        } else {
//
//            if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Residential"
//            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
//                vc!.dataGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
//            }
//            else if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Commercial"
//            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfo") as? SaleAutomationGeneralInfo
//                vc!.matchesGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
//            }
//
//        }

         
     }
    
    func goToSalesFlowiPad(dictDataSalesLocal : NSManagedObject) {
        
        nsud.set("\(dictDataSalesLocal.value(forKey: "leadId")!)", forKey: "LeadId")
        nsud.set("\(dictDataSalesLocal.value(forKey: "accountNo")!)", forKey: "AccountNos")
        nsud.set("\(dictDataSalesLocal.value(forKey: "leadNumber")!)", forKey: "LeadNumber")
        nsud.set("\(dictDataSalesLocal.value(forKey: "statusSysName")!)", forKey: "leadStatusSales")
        nsud.set("\(dictDataSalesLocal.value(forKey: "stageSysName")!)", forKey: "stageSysNameSales")
        nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
        if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
            
            nsud.set(true, forKey: "YesSurveyService")
            
        } else {

            nsud.set(false, forKey: "YesSurveyService")

        }
        nsud.synchronize()
                
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
        vc!.dataGeneralInfo = dictDataSalesLocal
        self.navigationController?.pushViewController(vc!, animated: false)
        
        
//        if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Residential"
//        {
//            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
//            vc!.dataGeneralInfo = dictDataSalesLocal
//            self.navigationController?.pushViewController(vc!, animated: false)
//        }
//        else if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Commercial"
//        {
//            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfoiPad") as? SaleAutomationGeneralInfoiPad
//            vc!.matchesGeneralInfo = dictDataSalesLocal
//            self.navigationController?.pushViewController(vc!, animated: false)
//        }
        
    }
    
    
    func goToServiceFlow(dictDataSalesLocal : NSManagedObject) {
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType")!)"
            let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState")!)"
            let isNPMATermite = "\(dictDataSalesLocal.value(forKey: "isNPMATermite")!)"
            let strIsRegularPestFlow = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlow")!)"
            let strIsRegularPestFlowOnBranch = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlowOnBranch")!)"
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId")!)", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
            
            if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else if departmentType == "Termite" && serviceState == "California" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "GenralInfoVC") as? GenralInfoVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
              //  showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoVC") as? GeneralInfoVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
        
    }
    
    func goToServiceFlowiPad(dictDataSalesLocal : NSManagedObject) {
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName") ?? "")"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType") ?? "")"
            let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState") ?? "")"
            let isNPMATermite = "\(dictDataSalesLocal.value(forKey: "isNPMATermite") ?? "")"
            let strIsRegularPestFlow = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlow") ?? "")"
            let strIsRegularPestFlowOnBranch = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlowOnBranch") ?? "")"
            
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId") ?? "")", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId") ?? "")", forKey: "strDepartmentId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName") ?? "")", forKey: "departmentSysNameWO")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID") ?? "")", forKey: "SurveyID")
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID") ?? "")".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
            if departmentType == "Mechanical" {
                
                //showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
                let vc = UIStoryboard.init(name: "MechanicaliPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "MechanicalGeneralInfoViewController") as? MechanicalGeneralInfoViewController
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
                
                let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if departmentType == "Termite" && serviceState != "California" {
                
                let vc = UIStoryboard.init(name: "ServiceiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoAppointmentViewiPad") as? GeneralInfoAppointmentViewiPad
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if departmentType == "Termite" && serviceState == "California" {
                if checkRedirectSignApprovalOrNot(obj: dictDataSalesLocal, checkflowStageType: true, checkflowStatus: true) {
                    
                    if (isInternetAvailable()){

                        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalReportVC") as? WDO_PendingApprovalReportVC
                        
                        let wdoWorkflowStatus = "\(dictDataSalesLocal.value(forKey: "wdoWorkflowStatus") ?? "")"
                        if(wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased()){
                            vc!.reportview_From =  Enum_ReportviewFrom.InspectorSignComplete
                        }else{
                            vc!.reportview_From =  Enum_ReportviewFrom.InspectorSignApproval
                        }
                        vc!.objPendingApproval = dictDataSalesLocal
                        self.navigationController?.pushViewController(vc!, animated: false)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                    }
                }else{
                    let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
                    vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }else if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlowiPad" : "ServicePestNewFlowiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoNewPestiPad") as? GeneralInfoNewPestiPad
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
              //  showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            } else {
                
                let vc = UIStoryboard.init(name: "ServiceiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoAppointmentViewiPad") as? GeneralInfoAppointmentViewiPad
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)

        }
        
    }
    func checkRedirectSignApprovalOrNot(obj : NSManagedObject , checkflowStageType : Bool , checkflowStatus : Bool) -> Bool {
        //---------By navin Approval check
        let departmentType = "\(obj.value(forKey: "departmentType")!)"
        let serviceState = "\(obj.value(forKey: "serviceState")!)"
        if departmentType == "Termite" && serviceState == "California" {
            
            if (DeviceType.IS_IPAD && (departmentType == "Termite" && serviceState == "California") && self.aryApprovalCheckList.count != 0)  {
                
                //---For Show status
                let wdoWorkflowStageId = "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")"
                let wdoWorkflowStatus = "\(obj.value(forKey: "wdoWorkflowStatus") ?? "")"
               
                //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                if(wdoWorkflowStageId.count > 0){
                    let aryApprovalCheckListFilterObj = aryApprovalCheckList.filter { (task) -> Bool in
                        return ("\((task as! NSDictionary).value(forKey: "ApprovalCheckListId")!)".contains(wdoWorkflowStageId)  ) }
                    if aryApprovalCheckListFilterObj.count != 0 {
                        let objApprovalCheckList = aryApprovalCheckListFilterObj[0]as! NSDictionary
                        
                        let strWdoWorkflowStageType = "\(objApprovalCheckList.value(forKey: "WdoWorkflowStageType") ?? "")"
                        
                        if(checkflowStageType && checkflowStatus){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased()  && (wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased()){
                                return true
                                
                            }
                        }
                        
                        else if(checkflowStageType){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased() {
                                return true
                                
                            }
                        }
                        
                        
                    }
                }
            }
        }
        return false
    }
         
     func goToServiceFlowOld(dictDataSalesLocal : NSManagedObject) {
         
         let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType")!)"
         let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState")!)"
         
         if departmentType == "Termite" && serviceState == "California" {
             
             showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
             
         } else {
             
             nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId")!)", forKey: "LeadId")
             nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
             nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
             nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
             if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
                 
                 nsud.set(true, forKey: "YesSurveyService")
                 
             } else {

                 nsud.set(false, forKey: "YesSurveyService")

             }
             nsud.synchronize()
             
            if DeviceType.IS_IPAD {

                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPad" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            } else {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPad" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoVC") as? GeneralInfoVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
             
         }
         
     }
    
    func goToServiceFlowiPadOld(dictDataSalesLocal : NSManagedObject) {
        
        let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType")!)"
        let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState")!)"
        let isNPMATermite = "\(dictDataSalesLocal.value(forKey: "isNPMATermite")!)"
        let strIsRegularPestFlow = "\(dictDataSalesLocal.value(forKey: "isNPMATermite")!)"
        let strIsRegularPestFlowOnBranch = "\(dictDataSalesLocal.value(forKey: "isNPMATermite")!)"

        
        nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId")!)", forKey: "LeadId")
        nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
        nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
        nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
        if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
            
            nsud.set(true, forKey: "YesSurveyService")
            
        } else {

            nsud.set(false, forKey: "YesSurveyService")

        }
        nsud.synchronize()
        
        if departmentType == "Mechanical" {
            
            //showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
            
            let vc = UIStoryboard.init(name: "MechanicaliPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "MechanicalGeneralInfoViewController") as? MechanicalGeneralInfoViewController
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }else if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
            
            let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
            vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }else if departmentType == "Termite" && serviceState != "California" {
            
            let vc = UIStoryboard.init(name: "ServiceiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoAppointmentViewiPad") as? GeneralInfoAppointmentViewiPad
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }else if departmentType == "Termite" && serviceState == "California" {
            
            let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
            vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }else if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
            
        } else {
            
            let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
            vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
            self.navigationController?.pushViewController(vc!, animated: false)

            
        }
        
    }
    
}

extension CalendarVC: CalendarDelegate {
    func didChangeEvent(_ event: Event, start: Date?, end: Date?) {
        var eventTemp = event
        guard let startTemp = start, let endTemp = end else { return }
        
        let startTime = timeFormatter(date: startTemp)
        let endTime = timeFormatter(date: endTemp)
        eventTemp.start = startTemp
        eventTemp.end = endTemp
        eventTemp.text = "\(startTime) - \(endTime)\n new time"
        
        if let idx = events.firstIndex(where: { $0.compare(eventTemp) }) {
            events.remove(at: idx)
            events.append(eventTemp)
            calendarView.reloadData()
        }
    }
    
    func didSelectDate(_ date: Date?, type: CalendarType, frame: CGRect?) {
        selectDate = date ?? Date()
        calendarView.reloadData()
    }
    
    func didSelectEvent(_ event: Event, type: CalendarType, frame: CGRect?) {
        print(type, event)
        
        
        print(event.ID)
        
        //Nilind
        
        var chkForPending = false
        var arrPendingWO = NSArray()
        var strMessage = ""
        
        if DeviceType.IS_IPAD {
            
            if aryApprovalCheckList.count > 0
            {
                chkForPending = true
            }
            else
            {
                chkForPending = false
            }
            
            let pendindListObject = getPendingWorkOrders(arrayObj: aryData)
            arrPendingWO = pendindListObject.arrayPendingWo
            strMessage = pendindListObject.strMessage
            
        }
        
        //End
        
          
        let dictDataSalesDB = aryData[Int(event.ID)!] as! NSManagedObject
        
        nsud.set(false, forKey: "yesAudio")
        nsud.set("", forKey: "AudioNameService")
        nsud.synchronize()
        
        if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
            
            // Block Time
            
        }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
                        
            // Task List
            if chkForPending
            {
                if arrPendingWO.count > 0 && arrPendingWO.count > 0
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
                else
                {
                    self.view.endEditing(true)
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    
                    vc.taskId = "\(dictDataSalesDB.value(forKey: "leadTaskId")!)"
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                    
                }
            }
            else
            {
                self.view.endEditing(true)
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                       
                vc.taskId = "\(dictDataSalesDB.value(forKey: "leadTaskId")!)"
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
            
            // Service Flow
            
            nsud.set("\(dictDataSalesDB.value(forKey: "workorderId") ?? "")", forKey: "StrRefId")
            nsud.synchronize()
            
            if chkForPending && arrPendingWO.count > 0
            {
                let strWoNo = "\(dictDataSalesDB.value(forKey: "workOrderNo") ?? "")"
                
                if arrPendingWO.contains(strWoNo)
                {
                    if DeviceType.IS_IPAD {
                        
                        goToServiceFlowFinal(dictDataSalesDB: dictDataSalesDB)
                        
                    } else {
                        
                        goToServiceFlowFinal(dictDataSalesDB: dictDataSalesDB)
                        
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
            }
            else
            {
                if DeviceType.IS_IPAD {
                    
                    goToServiceFlowFinal(dictDataSalesDB: dictDataSalesDB)
                    
                } else {
                    
                    goToServiceFlowFinal(dictDataSalesDB: dictDataSalesDB)
                    
                }
            }
            

        }else{
            
            // Sales Flow
            
            nsud.set("\(dictDataSalesDB.value(forKey: "leadId") ?? "")", forKey: "StrRefId")
            nsud.synchronize()
            
            if chkForPending
            {
                if arrPendingWO.count > 0 && arrPendingWO.count > 0
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
                else
                {
                    if DeviceType.IS_IPAD {
                        
                        goToSalesFlowFinal(dictDataSalesDB: dictDataSalesDB)
                        
                    } else {
                        
                        goToSalesFlowFinal(dictDataSalesDB: dictDataSalesDB)
                        
                    }
                }
            }
            else
            {
                if DeviceType.IS_IPAD {
                    
                    goToSalesFlowFinal(dictDataSalesDB: dictDataSalesDB)
                    
                } else {
                    
                    goToSalesFlowFinal(dictDataSalesDB: dictDataSalesDB)
                    
                }
            }
            
            //goToSalesFlowFinal(dictDataSalesDB: dictDataSalesDB)
        }
        
        
        switch type {
        case .day:
            eventViewer.text = event.text
        default:
            break
        }
        
        
        
        
    }
    
    func goToSalesFlowFinal(dictDataSalesDB : NSManagedObject)  {
        
        let branchSysNameLocal = "\(dictDataSalesDB.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName
        {
            if DeviceType.IS_IPAD {
                
                self.goToSalesFlowiPad(dictDataSalesLocal: dictDataSalesDB)

            } else {
                
                self.goToSalesFlow(dictDataSalesLocal: dictDataSalesDB)

            }
        }
        else
        {
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
    }
    
    func goToServiceFlowFinal(dictDataSalesDB : NSManagedObject)  {
        
        let branchSysNameLocal = "\(dictDataSalesDB.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName
        {
            
            if DeviceType.IS_IPAD {
                
                goToServiceFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                
            } else {
                
                goToServiceFlow(dictDataSalesLocal: dictDataSalesDB)
                
            }
        }
        else
        {
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
    }
    
    func didSelectMore(_ date: Date, frame: CGRect?) {
        print(date)
    }
    
    func eventViewerFrame(_ frame: CGRect) {
        eventViewer.reloadFrame(frame: frame)
    }
    
    func didAddNewEvent(_ event: Event, _ date: Date?) {
        var newEvent = event
        
        guard let start = date, let end = Calendar.current.date(byAdding: .minute, value: 30, to: start) else { return }
        
        let startTime = timeFormatter(date: start)
        let endTime = timeFormatter(date: end)
        newEvent.start = start
        newEvent.end = end
        newEvent.ID = "\(events.count + 1)"
        newEvent.text = "\(startTime) - \(endTime)\n new event"
        events.append(newEvent)
        calendarView.reloadData()
    }
}

extension CalendarVC: CalendarDataSource {
    func eventsForCalendar() -> [Event] {
        return events
    }
    
    private var dates: [Date] {
        return Array(0...10).compactMap({ Calendar.current.date(byAdding: .day, value: $0, to: Date()) })
    }
    
    func willDisplayDate(_ date: Date?, events: [Event]) -> DateStyle? {
        guard dates.first(where: { $0.year == date?.year && $0.month == date?.month && $0.day == date?.day }) != nil else { return nil }
        
        return DateStyle(backgroundColor: .clear, textColor: .black, dotBackgroundColor: .red)
    }
    
    func willDisplayEventView(_ event: Event, frame: CGRect, date: Date?) -> EventViewGeneral? {
        guard event.ID == "" else { return nil }
        
        return CustomViewEvent(style: style, event: event, frame: frame)
    }
}
final class CustomViewEvent: EventViewGeneral {
    override init(style: Style, event: Event, frame: CGRect) {
        super.init(style: style, event: event, frame: frame)
        
        let imageView = UIImageView(image: UIImage(named: "ic_stub"))
        imageView.frame = CGRect(origin: CGPoint(x: 3, y: 1), size: CGSize(width: frame.width - 6, height: frame.height - 2))
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        backgroundColor = event.backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct ItemData: Decodable {
    let data: [Item]
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decode([Item].self, forKey: CodingKeys.data)
    }
}

struct Item: Decodable {
    let id: String, title: String, start: String, end: String
    let color: UIColor, colorText: UIColor
    let files: [String]
    let allDay: Bool
    
    enum CodingKeys: String, CodingKey {
        case id, title, start, end, color, files
        case colorText = "text_color"
        case allDay = "all_day"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: CodingKeys.id)
        title = try container.decode(String.self, forKey: CodingKeys.title)
        start = try container.decode(String.self, forKey: CodingKeys.start)
        end = try container.decode(String.self, forKey: CodingKeys.end)
        allDay = try container.decode(Int.self, forKey: CodingKeys.allDay) != 0
        files = try container.decode([String].self, forKey: CodingKeys.files)
        let strColor = try container.decode(String.self, forKey: CodingKeys.color)
        color = UIColor.hexStringToColor(hex: strColor)
        let strColorText = try container.decode(String.self, forKey: CodingKeys.colorText)
        colorText = UIColor.hexStringToColor(hex: strColorText)
    }
}
extension UIColor {
    static func hexStringToColor(hex: String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return .gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: 1.0)
    }
}

// MARK: - -----------------------------------AddBlockTimeProtocol -----------------------------------
extension CalendarVC : AddBlockTimeProtocol{
    
    func getDataOnAddBlockTime(arrAppointmentData: NSArray) {
        self.isBlockTimeAdded = true
        self.aryData = arrAppointmentData.mutableCopy()as! NSMutableArray
        setUPUI()
    }
}

