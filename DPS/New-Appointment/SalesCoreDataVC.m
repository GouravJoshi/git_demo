//
//  SalesCoreDataVC.m
//  DPS
//  Saavan Patidar
//  Created by Saavan Patidar on 24/08/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar 2020

#import "SalesCoreDataVC.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "AddLead+CoreDataProperties.h"
#import "AddLead.h"
#import "OutBox+CoreDataProperties.h"
#import "OutBox.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AllImportsViewController.h"
#import <MessageUI/MessageUI.h>
#import <AVKit/AVKit.h>
#import "Header.h"
#import "AllImportsViewController.h"
#import "AppointmentView.h"

#import "AppointmentAllView.h"
#import "UpcomingAppointmentTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "GeneralInfoAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceAutomation.h"
#import "ServiceAutomation+CoreDataProperties.h"
#import <MessageUI/MessageUI.h>
#import "SaleAutomationGeneralInfo.h"
#import "AppDelegate.h"
#import "WorkOrderDetailsService.h"
#import "WorkOrderDetailsService+CoreDataProperties.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "ServiceAutoCompanyDetails.h"
#import "ServiceAutoCompanyDetails+CoreDataProperties.h"
#import "WorkorderDetailChemicalListService.h"
#import "WorkorderDetailChemicalListService+CoreDataProperties.h"
#import "EmailDetailServiceAuto.h"
#import "EmailDetailServiceAuto+CoreDataProperties.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "SalesAutoModifyDate.h"
#import "SalesAutoModifyDate+CoreDataProperties.h"
#import "MBProgressHUD.h"
#import "SalesDynamicInspection+CoreDataProperties.h"
#import "SalesDynamicInspection.h"
#import "ServiceDynamicForm+CoreDataProperties.h"
#import "ServiceDynamicForm+CoreDataClass.h"
#import "InitialSetUp.h"
#import "WorkOrderEquipmentDetails+CoreDataProperties.h"
#import "EquipmentDynamicForm+CoreDataClass.h"
#import "EquipmentDynamicForm+CoreDataProperties.h"
#import "SendMailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "DPS-Swift.h"

@interface SalesCoreDataVC ()
{
    
    Global *global;
    NSString *strCompanyKeyy ,*strUserName , *strEmployeeNo , *strLeadIdGlobal, *strAudioNameGlobal , *strModifyDateToSendToServerAll , *strElectronicSign , *strWorkOrderIdGlobal , *serviceAddressImagePath,*strWorkOrderType;
    NSMutableDictionary *dictFinal;
    NSArray *arrAllObj;
    NSManagedObject *matches;
    NSMutableArray *arrOfAllImagesToSendToServer , *arrOfAllSignatureImagesToSendToServer , *arrCheckImage , *arrAllTargetImages , *arrOfAllDocumentsToSend , *arrOfAllCheckImageToSend, *arrOfAllProblemImageToSendToServer,*arrOfAllGraphXmlToSendToServer;
    NSMutableArray *arrOfSubWorkOrderDb,*arrOfSubWorkOrderIssuesDb,*arrOfSubWorkOrderIssuesRepairDb,*arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal;
    NSMutableDictionary *dictSubWorkOrderDetails,*dictSubWorkOrderIssuesDetails,*dictSubWorkOrderIssuesRepairDetails;
    
    NSManagedObject *objServiceWorkOrderGlobal;

}

@end

@implementation SalesCoreDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    global = [[Global alloc] init];

    // Do any additional setup after loading the view.
}

-(void)saveToCoreDataSalesInfo :(NSArray*)arrLeadExtSerDcs :(NSString*)strLeadIdToCompare :(NSString*)strType
{
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
            for (int i=0; i<arrLeadExtSerDcs.count; i++)
            {
                
                //**** For Lead Detail *****//
                NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
                
                NSString *strLeadIddd;
                if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
                {
                    strLeadIddd=@"";
                }
                else
                {
                    
                    strLeadIddd=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                }
                
                if ([strType isEqualToString:@"All"]) {
                    
                    strLeadIdToCompare = strLeadIddd;
                    
                }
#pragma mark- -- Lead Detail --

                //if ([strLeadIddd isEqualToString:strLeadIdToCompare]) {
                if ([strLeadIddd isEqualToString:strLeadIdToCompare]) {

                    if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
                    {
                        NSLog(@"NOTHING IN LEAD DETAIL");
                    }
                    else
                    {
                        
                        // Lead Detail Entity
                        entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
                        LeadDetail *objLeadDetail = [[LeadDetail alloc]initWithEntity:entityLeadDetail insertIntoManagedObjectContext:context];
                        
                        
                        NSLog(@"Saved In Database Lead");
                        
                        objLeadDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                        objLeadDetail.leadNumber=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadNumber"]];
                        objLeadDetail.companyId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyId"]];
                        objLeadDetail.companyMappingKey=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyMappingKey"]];
                        objLeadDetail.accountNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountNo"]];
                        objLeadDetail.leadName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadName"]];
                        objLeadDetail.firstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FirstName"]];
                        objLeadDetail.middleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.MiddleName"]];
                        
                        objLeadDetail.lastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LastName"]];
                        
                        objLeadDetail.companyName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyName"]];
                        objLeadDetail.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryEmail"]]];
                        objLeadDetail.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.SecondaryEmail"]]];
                        objLeadDetail.primaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryPhone"]];
                        objLeadDetail.secondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SecondaryPhone"]];
                        objLeadDetail.salesRepId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SalesRepId"]];
                        objLeadDetail.scheduleStartDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]];
                        objLeadDetail.scheduleEndDate=[NSString stringWithFormat:@"%@",[dictLeadDetail   valueForKeyPath:@"LeadDetail.ScheduleEndDate"]];
                        objLeadDetail.inspectionDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InspectionDate"]];
                        objLeadDetail.estimatedValue=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.EstimatedValue"]];
                        objLeadDetail.closeDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CloseDate"]];
                        objLeadDetail.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryServiceSysName"]];
                        objLeadDetail.stageSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.StageSysName"]];
                        objLeadDetail.statusSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.StatusSysName"]];
                        // objLeadDetail.serviceGroupSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceGroupSysName"]];   No Need
                        
                        objLeadDetail.prioritySysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrioritySysName"]];
                        objLeadDetail.sourceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SourceSysName"]];
                        objLeadDetail.tags=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Tags"]];
                        objLeadDetail.descriptionLeadDetail=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Description"]];
                        objLeadDetail.notes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Notes"]];
                        objLeadDetail.isInspectionCompleted=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInspectionCompleted"]];
                        objLeadDetail.inspectionCompletedTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InspectionCompletedTime"]];
                        objLeadDetail.branchSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BranchSysName"]];
                        objLeadDetail.divisionSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DivisionSysName"]];
                        objLeadDetail.departmentSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DepartmentSysName"]];
                        objLeadDetail.billingAddress1=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingAddress1"]];
                        objLeadDetail.billingAddress2=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingAddress2"]];
                        objLeadDetail.billingCountry=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCountry"]];
                        objLeadDetail.billingState=[NSString stringWithFormat:@"%@",    [dictLeadDetail valueForKeyPath:@"LeadDetail.BillingState"]];
                        objLeadDetail.billingCity=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCity"]];
                        objLeadDetail.billingZipcode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingZipcode"]];
                        objLeadDetail.servicesAddress1=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicesAddress1"]];
                        objLeadDetail.serviceAddress2=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddress2"]];
                        objLeadDetail.serviceCountry=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCountry"]];
                        objLeadDetail.serviceState=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceState"]];
                        objLeadDetail.serviceCity=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCity"]];
                        objLeadDetail.serviceZipcode=[NSString stringWithFormat:@"%@",[dictLeadDetail    valueForKeyPath:@"LeadDetail.ServiceZipcode"]];
                        objLeadDetail.isAgreementGenerated=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementGenerated"]];
                        objLeadDetail.collectedAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CollectedAmount"]];
                        
                        objLeadDetail.billedAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BilledAmount"]];
                        objLeadDetail.subTotalAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SubTotalAmount"]];
                        objLeadDetail.couponDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CouponDiscount"]];
                        objLeadDetail.latitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Latitude"]];
                        objLeadDetail.longitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Longitude"]];
                        objLeadDetail.customerName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CustomerName"]];
                        objLeadDetail.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CreatedBy"]];
                        objLeadDetail.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CreatedDate"]];
                        objLeadDetail.surveyID=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SurveyID"]];
                        
                        objLeadDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedBy"]];
                        objLeadDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                        
                        NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
                        [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
                        [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]]];
                        objLeadDetail.zdateScheduledStart=newTimeSchedule;
                        
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]]];
                        objLeadDetail.dateModified=newTime;
                        
                        
                        //Nilind new add 30 sept
                        objLeadDetail.tax=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Tax"]];
                        
                        objLeadDetail.leadGeneralTermsConditions=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadGeneralTermsConditions"]];
                        
                        //Nilind 7 Nov
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsProposalFromMobile"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isProposalFromMobile=@"true";
                        }
                        else
                        {
                            objLeadDetail.isProposalFromMobile=@"false";
                        }
                        
                        
                        //Nilind 29 Dec //isInitialSetupCreated
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isInitialSetupCreated=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isInitialSetupCreated=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@""])
                        {
                            objLeadDetail.isInitialSetupCreated=@"false";
                        }
                        else
                        {
                            objLeadDetail.isInitialSetupCreated=@"false";
                        }
                        
                        objLeadDetail.audioFilePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AudioFilePath"]];
                        objLeadDetail.deviceVersion=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DeviceVersion"]];
                        objLeadDetail.deviceName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DeviceName"]];
                        objLeadDetail.versionNumber=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.VersionNumber"]];
                        objLeadDetail.versionDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.VersionDate"]];
                        
                        
                        //Nilind 5 Jan
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.iAgreeTerms=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.iAgreeTerms=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@""])
                        {
                            objLeadDetail.iAgreeTerms=@"false";
                        }
                        else
                        {
                            objLeadDetail.iAgreeTerms=@"false";
                        }
                        //.........................
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"true"])
                        {
                            objLeadDetail.isCustomerNotPresent=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isCustomerNotPresent=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@""])
                        {
                            objLeadDetail.isCustomerNotPresent=@"false";
                        }
                        else
                        {
                            objLeadDetail.isCustomerNotPresent=@"false";
                        }
                        //.....................
                        
                        //.........................
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isBillingAddressSame=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isBillingAddressSame=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@""])
                        {
                            objLeadDetail.isBillingAddressSame=@"false";
                        }
                        else
                        {
                            objLeadDetail.isBillingAddressSame=@"false";
                        }
                        //.....................
                        
                        //Nilind 16 Nov
                        
                        //.........................
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isAgreementSigned=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isAgreementSigned=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@""])
                        {
                            objLeadDetail.isAgreementSigned=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"true"])
                        {
                            objLeadDetail.isAgreementSigned=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"false"])
                        {
                            objLeadDetail.isAgreementSigned=@"false";
                        }
                        else
                        {
                            objLeadDetail.isAgreementSigned=@"false";
                        }
                        //.....................
                        
                        objLeadDetail.userName=strUserName;
                        objLeadDetail.companyKey=strCompanyKeyy;
                        objLeadDetail.employeeNo_Mobile=strEmployeeNo;
                        
                        //........................
                        
                        objLeadDetail.zSync=@"blank";
                        
                        //Nilind 08 Feb
                        
                        objLeadDetail.area=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Area"]];
                        objLeadDetail.lotSizeSqFt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.lotSizeSqFt"]];
                        objLeadDetail.noOfBedroom=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoofBedroom"]];
                        objLeadDetail.noOfBathroom=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoofBathroom"]];
                        objLeadDetail.yearBuilt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.yearBuilt"]];
                        //End
                        
                        //.........................
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isEmployeePresetSignature=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isEmployeePresetSignature=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@""])
                        {
                            objLeadDetail.isEmployeePresetSignature=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"true"])
                        {
                            objLeadDetail.isEmployeePresetSignature=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"false"])
                        {
                            objLeadDetail.isEmployeePresetSignature=@"false";
                        }
                        else
                        {
                            objLeadDetail.isEmployeePresetSignature=@"false";
                        }
                        //.....................
                        
                        //Nilind 23 May
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@""])
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"true"])
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"false"])
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"false";
                        }
                        else
                        {
                            objLeadDetail.isServiceAddrTaxExempt=@"false";
                        }
                        objLeadDetail.serviceAddrTaxExemptionNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddrTaxExemptionNo"]];
                        objLeadDetail.serviceAddressSubType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddressSubType"]];
                        objLeadDetail.taxableAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxableAmount"]];
                        objLeadDetail.taxAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxAmount"]];
                        objLeadDetail.totalPrice=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TotalPrice"]];
                        
                        objLeadDetail.serviceGateCode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceGateCode"]];
                        objLeadDetail.serviceNotes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceNotes"]];
                        objLeadDetail.serviceDirection=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceDirection"]];
                        
                        objLeadDetail.billingMapcode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingMapcode"]];
                        objLeadDetail.serviceMapCode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceMapCode"]];
                        objLeadDetail.accountDescription=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountDescription"]];
                        
                        objLeadDetail.linearSqFt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.linearSqFt"]];
                        
                        objLeadDetail.surveyStatus=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SurveyStatus"]];
                        
                        //End
                        
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isCouponApplied=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isCouponApplied=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@""])
                        {
                            objLeadDetail.isCouponApplied=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"true"])
                        {
                            objLeadDetail.isCouponApplied=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"false"])
                        {
                            objLeadDetail.isCouponApplied=@"false";
                        }
                        else
                        {
                            objLeadDetail.isCouponApplied=@"false";
                        }
                        //Nilind 26 Oct
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isResendAgreementProposalMail=@"true";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@"0"])
                        {
                            objLeadDetail.isResendAgreementProposalMail=@"false";
                        }
                        else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@""])
                        {
                            objLeadDetail.isResendAgreementProposalMail=@"false";
                        }
                        else
                        {
                            objLeadDetail.isResendAgreementProposalMail=@"false";
                        }
                        //.........................
                        
                        objLeadDetail.cellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CellNo"]];
                        
                        objLeadDetail.billingFirstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingFirstName"]];
                        
                        
                        
                        objLeadDetail.billingMiddleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingMiddleName"]];
                        
                        objLeadDetail.billingLastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingLastName"]];
                        
                        objLeadDetail.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingPrimaryEmail"]]];
                        
                        objLeadDetail.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSecondaryEmail"]]];
                        
                        
                        
                        
                        
                        objLeadDetail.billingCellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCellNo"]];
                        
                        objLeadDetail.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingPrimaryPhone"]];
                        
                        objLeadDetail.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSecondaryPhone"]];
                        
                        
                        
                        objLeadDetail.serviceFirstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceFirstName"]];
                        
                        objLeadDetail.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceMiddleName"]];
                        
                        objLeadDetail.serviceLastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceLastName"]];
                        
                        objLeadDetail.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicePrimaryEmail"]]];
                        
                        objLeadDetail.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSecondaryEmail"]]];
                        
                        objLeadDetail.serviceCellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCellNo"]];
                        
                        objLeadDetail.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicePrimaryPhone"]];
                        
                        objLeadDetail.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSecondaryPhone"]];
                        //Nilind 08 Dec
                        
                        objLeadDetail.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTime"]];
                        objLeadDetail.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTimeLatitude"]];
                        objLeadDetail.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTimeLongitude"]];
                        objLeadDetail.strPreferredMonth=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.strPreferredMonth"]];
                        objLeadDetail.salesType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SalesType"]];
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SalesType"]] isEqualToString:@""])
                        {
                            objLeadDetail.salesType = @"inside";
                        }
                        
                        objLeadDetail.problemDescription=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProblemDescription"]];
                        objLeadDetail.taxableMaintAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxableMaintAmount"]];
                        objLeadDetail.taxMaintAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxMaintAmount"]];
                        //end
                        //akshay
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountElectronicAuthorizationFormSigned"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.accountElectronicAuthorizationFormSigned = @"true";
                        }
                        else
                        {
                            objLeadDetail.accountElectronicAuthorizationFormSigned = @"false";
                        }
                        objLeadDetail.billingCounty=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCounty"]];
                        objLeadDetail.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSchoolDistrict"]];
                        objLeadDetail.serviceCounty=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCounty"]];
                        objLeadDetail.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSchoolDistrict"]];

                        objLeadDetail.noOfStory=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoOfStory"]];
                        objLeadDetail.shrubArea=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.shrubArea"]];
                        objLeadDetail.turfArea=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.turfArea"]];
                        objLeadDetail.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ThirdPartyAccountNo"]];
                        //Commercial FLow
                        objLeadDetail.flowType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FlowType"]];
                        
                        objLeadDetail.initialServiceDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InitialServiceDate"]];
                        
                        objLeadDetail.recurringServiceMonth=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.RecurringServiceMonth"]];

                        objLeadDetail.taxSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxSysName"]];

                        objLeadDetail.totalMaintPrice=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.totalMaintPrice"]];
                        objLeadDetail.proposalNotes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProposalNotes"]];
                        objLeadDetail.driveTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DriveTime"]];
                        objLeadDetail.latestStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LatestStartTime"]];
                        objLeadDetail.earliestStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.EarliestStartTime"]];

                        objLeadDetail.fromDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]];
                        objLeadDetail.toDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleEndDate"]];
                        objLeadDetail.modifyDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                        
                        objLeadDetail.accountManagerId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerId"]];
                        objLeadDetail.accountManagerNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerNo"]];
                        objLeadDetail.accountManagerName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerName"]];
                        objLeadDetail.accountManagerEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerEmail"]]];
                        objLeadDetail.accountManagerPrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerPrimaryPhone"]];
                        
                        objLeadDetail.propertyType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PropertyType"]];
                        
                    objLeadDetail.serviceAddressPropertyTypeSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddressPropertyTypeSysName"]];

                        objLeadDetail.profileImage=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProfileImage"]];

                        objLeadDetail.isSelectionAllowedForCustomer=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsSelectionAllowedForCustomer"]];
                        
                        objLeadDetail.tipDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TipDiscount"]];

                        objLeadDetail.otherDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OtherDiscount"]];

                        objLeadDetail.isCommLeadUpdated=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCommLeadUpdated"]];
                        
                        //Nilind
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsMailSend"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isMailSend = @"true";
                        }
                        else
                        {
                            objLeadDetail.isMailSend = @"false";
                        }

                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInvisible"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isInvisible = @"true";
                        }
                        else
                        {
                            objLeadDetail.isInvisible = @"false";
                        }
                        
                        objLeadDetail.leadInspectionFee=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadInspectionFee"]];

                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsTipEligible"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isTipEligible = @"true";
                        }
                        else
                        {
                            objLeadDetail.isTipEligible = @"false";
                        }
                        
                        objLeadDetail.needTipService=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NeedTipService"]];

                         if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCommTaxUpdated"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isCommTaxUpdated = @"true";
                        }
                        else
                        {
                            objLeadDetail.isCommTaxUpdated = @"false";
                        }
                        
                        objLeadDetail.accountName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountName"]];
                        
                        objLeadDetail.buildingPermitAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BuildingPermitAmount"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsReopen"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isReopen = @"true";
                        }
                        else
                        {
                            objLeadDetail.isReopen = @"false";
                        }
                        objLeadDetail.isWdoLead = @"false";
                        objLeadDetail.subTotalMaintAmount = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SubTotalMaintAmount"]];
                        objLeadDetail.templateKey = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TemplateKey"]];
                        objLeadDetail.pdfPath = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PdfPath"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SmsReminders"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.smsReminders = @"true";
                        }
                        else
                        {
                            objLeadDetail.smsReminders = @"false";
                        }
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PhoneReminders"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.phoneReminders = @"true";
                        }
                        else
                        {
                            objLeadDetail.phoneReminders = @"false";
                        }
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.EmailReminders"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.emailReminders = @"true";
                        }
                        else
                        {
                            objLeadDetail.emailReminders = @"false";
                        }
                        
                        if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsPreview"]] isEqualToString:@"1"])
                        {
                            objLeadDetail.isPreview = @"true";
                        }
                        else
                        {
                            objLeadDetail.isPreview = @"false";
                        }
                
                        objLeadDetail.timeRangeId = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TimeRangeId"]];
                        objLeadDetail.scheduleTimeType = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleTimeType"]];
                        objLeadDetail.scheduleDate = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleDate"]];
                        objLeadDetail.scheduleTime = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleTime"]];
                        objLeadDetail.serviceAddressImagePath = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddressImagePath"]];
                        objLeadDetail.billToLocationId = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.billToLocationId"]];
                        objLeadDetail.creditCardType = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CreditCardType"]];
                        objLeadDetail.cardAccountNumber = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CardAccountNumber"]];

                    }
                    
                    
                    //------------------------------------------------------------------------
                    //------------------------------------------------------------------------
                    
                    //For Saving modify Date-------------
#pragma mark- -- SalesAutoModifyDate --

                    
                    entitySalesModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
                    SalesAutoModifyDate *objSalesModifyDate = [[SalesAutoModifyDate alloc]initWithEntity:entitySalesModifyDate insertIntoManagedObjectContext:context];
                    
                    objSalesModifyDate.companyKey=strCompanyKeyy;
                    objSalesModifyDate.userName=strUserName;
                    objSalesModifyDate.leadIdd=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                    [inputDateFormatter setTimeZone:inputTimeZone];
                    [inputDateFormatter setDateFormat:dateFormat];
                    
                    NSString *inputString = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                    NSDate *date = [inputDateFormatter dateFromString:inputString];
                    
                    NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                    [outputDateFormatter setTimeZone:outputTimeZone];
                    [outputDateFormatter setDateFormat:dateFormat];
                    NSString *outputString = [outputDateFormatter stringFromDate:date];
                    
                    objSalesModifyDate.modifyDate=outputString;//[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                    
                    //------------------------------------------------------------------------
                    //------------------------------------------------------------------------
                    
                    
                    //........................................................................
#pragma mark- -- PaymentInfo --

                    // For Payment Info
                    //PaymentInfo Entity
                    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
                    PaymentInfo *objentityPaymentInfo = [[PaymentInfo alloc]initWithEntity:entityPaymentInfo insertIntoManagedObjectContext:context];
                    NSLog(@"PaymentInfo>>>>%@",[dictLeadDetail valueForKey:@"PaymentInfo"]);
                    
                    if(![[dictLeadDetail valueForKey:@"PaymentInfo"] isKindOfClass:[NSDictionary class]])
                    {
                        NSLog(@"NOTHING IN PAYMENT INFO");
                    }
                    else
                    {
                        objentityPaymentInfo.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                        
                        
                        objentityPaymentInfo.leadPaymentDetailId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.LeadPaymentDetailId"]];
                        objentityPaymentInfo.paymentMode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.PaymentMode"]];
                        objentityPaymentInfo.amount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Amount"]];
                        objentityPaymentInfo.checkNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckNo"]];
                        objentityPaymentInfo.licenseNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.LicenseNo"]];
                        
                        objentityPaymentInfo.expirationDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ExpirationDate"]];
                        objentityPaymentInfo.specialInstructions=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.SpecialInstructions"]];
                        objentityPaymentInfo.agreement=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Agreement"]];
                        objentityPaymentInfo.proposal=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Proposal"]];
                        objentityPaymentInfo.customerSignature=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CustomerSignature"]];
                        
                        objentityPaymentInfo.salesSignature=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.SalesSignature"]];
                        objentityPaymentInfo.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CreatedBy"]];
                        objentityPaymentInfo.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CreatedDate"]];
                        objentityPaymentInfo.modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ModifiedBy"]];
                        objentityPaymentInfo.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ModifiedDate"]];
                        objentityPaymentInfo.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckFrontImagePath"]];
                        objentityPaymentInfo.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckBackImagePath"]];
                        
                        objentityPaymentInfo.inspection=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Inspection"]];

                        //Nilind 16 Nov
                        
                        objentityPaymentInfo.userName=strUserName;
                        objentityPaymentInfo.companyKey=strCompanyKeyy;
                        
                        //........................
                    }
                    //........................................................................
                    
                    // For LeadPreference
#pragma mark- -- LeadPreference --

                    //LeadPreference Entiy
                    entityLeadPreference=[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
                    
                    LeadPreference *objentityLeadPreference = [[LeadPreference alloc]initWithEntity:entityLeadPreference insertIntoManagedObjectContext:context];
                    if(![[dictLeadDetail valueForKey:@"LeadPreference"] isKindOfClass:[NSDictionary class]])
                    {
                        NSLog(@"NOTHING IN LeadPreference");
                    }
                    else
                    {
                        objentityLeadPreference.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                        
                        objentityLeadPreference.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.CreatedBy"]];
                        objentityLeadPreference.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.CreatedDate"]];
                        
                        objentityLeadPreference.leadPreferenceId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.LeadPreferenceId"]];
                        
                        objentityLeadPreference. modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.ModifiedBy"]];
                        objentityLeadPreference.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.ModifiedDate"]];
                        objentityLeadPreference.preferredDays=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredDays"]];
                        objentityLeadPreference.preferredEndTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredEndTime"]];
                        objentityLeadPreference.preferredStartDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredStartDate"]];
                        objentityLeadPreference.preferredStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredStartTime"]];
                        objentityLeadPreference.preferredTechnician=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredTechnician"]];
                        
                        //Nilind 16 Nov
                        
                        objentityLeadPreference.userName=strUserName;
                        objentityLeadPreference.companyKey=strCompanyKeyy;
                        
                        //........................
                        
                    }
                    
                    //........................................................................
                    
#pragma mark- -- DocumentsDetail --

                    // For DocumentsDetail
                    
                    NSArray *arrDocumentsDetail=[dictLeadDetail valueForKey:@"DocumentsDetail"];
                    
                    if([[dictLeadDetail valueForKey:@"DocumentsDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int z=0; z<arrDocumentsDetail.count; z++)
                        {
                            entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
                            
                            DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
                            dictDocumentsDetail=[arrDocumentsDetail objectAtIndex:z];
                            
                            //if(![[dictLeadDetail valueForKey:@"DocumentsDetail"] isKindOfClass:[NSDictionary class]])
                            // {
                            // NSLog(@"NOTHING IN DocumentsDetail");
                            // }
                            //else
                            //{
                            objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadId"]];
                            
                            objentityDocumentsDetail.accountNo=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"AccountNo"]];
                            
                            objentityDocumentsDetail.createdBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedBy"]];
                            
                            objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
                            
                            objentityDocumentsDetail.descriptionDocument=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Description"]];
                            
                            objentityDocumentsDetail. docType=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"DocType"]];
                            
                            objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"FileName"]];
                            
                            objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadDocumentId"]];
                            
                            objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
                            
                            objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
                            
                            objentityDocumentsDetail.scheduleStartDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ScheduleStartDate"]];
                            
                            objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
                            objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
                            objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objentityDocumentsDetail.docFormatType =  [NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"DocFormatType"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"IsAddendum"]] isEqualToString:@"1"])
                            {
                                objentityDocumentsDetail.isAddendum =  @"true";

                            }
                            else
                            {
                                objentityDocumentsDetail.isAddendum =  @"false";

                                
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"IsSync"]] isEqualToString:@"1"])

                            {

                            objentityDocumentsDetail.isSync =  @"true";
                                
                            }
                            else
                            {
                                objentityDocumentsDetail.isSync =  @"false";

                            }
                            objentityDocumentsDetail.isToUpload = @"false";
                            //Nilind 16 Nov
                            
                            if([[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"IsInternalUsage"]] isEqualToString:@"1"])

                            {

                                objentityDocumentsDetail.isInternalUsage =  @"true";

                            }
                            else
                            {
                                objentityDocumentsDetail.isInternalUsage =  @"false";

                            }
                            
                            objentityDocumentsDetail.userName=strUserName;
                            objentityDocumentsDetail.companyKey=strCompanyKeyy;
                            
                            objentityDocumentsDetail.inspectionFormBuilderMappingMasterId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"InspectionFormBuilderMappingMasterId"]];
                            objentityDocumentsDetail.templateKey=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"TemplateKey"]];
                            
                            //........................
                            NSError *error10;
                            [context save:&error10];
                            //}
                        }
                        
                    }
                    

                    //........................................................................
                    // For Email Detail /
                    // 26 Aug 2016
#pragma mark- -- EmailDetail --

                    
                    NSArray *arrEmailDetail=[dictLeadDetail valueForKey:@"EmailDetail"];//EmailDetail
                    
                    if([[dictLeadDetail valueForKey:@"EmailDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrEmailDetail.count; j++)
                        {
                            //Email Detail Entity
                            entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
                            
                            EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                            dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                            
                            objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                            objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                            objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                            //objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"True"])
                            {
                                objEmailDetail.isCustomerEmail=@"true";

                            }
                            else
                            {
                                objEmailDetail.isCustomerEmail=@"false";

                            }
                            
                            
                            
                            // objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                            objEmailDetail.leadMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"LeadMailId"]];
                            //objEmailDetail.isDefaultEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"True"])
                            {
                                objEmailDetail.isDefaultEmail=@"true";

                            }
                            else
                            {
                                objEmailDetail.isDefaultEmail=@"false";

                            }
                            
                            objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                            objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                            objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                            objEmailDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"])
                            {
                                objEmailDetail.isMailSent=@"true";
                            }
                            else
                            {
                                objEmailDetail.isMailSent=@"false";
                            }
                            //Nilind 16 Nov
                            
                            objEmailDetail.userName=strUserName;
                            objEmailDetail.companyKey=strCompanyKeyy;
                            
                            //........................
                            
                            //.................
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    //........................................................................
                    
                    // For Image Detail //
#pragma mark- -- ImagesDetail --

                    
                    
                    NSArray *arrImageDetail=[dictLeadDetail valueForKey:@"ImagesDetail"];
                    
                    if([[dictLeadDetail valueForKey:@"ImagesDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int k=0; k<arrImageDetail.count; k++)
                        {
                            // Image Detail Entity
                            entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
                            
                            ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictImageDetail=[[NSMutableDictionary alloc]init];
                            dictImageDetail=[arrImageDetail objectAtIndex:k];
                            
                            objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"CreatedBy"]];
                            objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"CreatedDate"]];
                            objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Description"]];
                            objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageId"]];
                            objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImagePath"]];
                            objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageType"]];
                            objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"ModifiedBy"]];
                            objImageDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"ModifiedDate"]];
                            objImageDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            //Nilind 16 Nov
                            
                            objImageDetail.userName=strUserName;
                            objImageDetail.companyKey=strCompanyKeyy;
                            
                            objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageCaption"]];
                            
                            objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Latitude"]];
                            objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Longitude"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsAddendum"]] isEqualToString:@"1"])
                            {
                                objImageDetail.isAddendum=@"true";
                            }
                            else
                            {
                                objImageDetail.isAddendum=@"false";
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsNewGraph"]] isEqualToString:@"1"])
                            {
                                objImageDetail.isNewGraph=@"true";
                            }
                            else
                            {
                                objImageDetail.isNewGraph=@"false";
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsImageSyncforMobile"]] isEqualToString:@"1"])
                            {
                                objImageDetail.isImageSyncforMobile=@"true";
                            }
                            else
                            {
                                objImageDetail.isImageSyncforMobile=@"false";
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsInternalUsage"]] isEqualToString:@"1"])
                            {
                                objImageDetail.isInternalUsage=@"true";
                            }
                            else
                            {
                                objImageDetail.isInternalUsage=@"false";
                            }
                            
                            objImageDetail.leadXmlPath=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadXmlPath"]];
                            
                            //........................
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    
#pragma mark- -- CRM Image Detail --
                    
                    NSArray *arrCRMImageDetail=[dictLeadDetail valueForKey:@"CRMImagesDetail"];//EmailDetail
                    if ([arrCRMImageDetail isKindOfClass:[NSArray class]])
                    {
                        WebService *objWebService = [[WebService alloc] init];
                        [objWebService saveCRMImageToDBWithStrLeadId:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]] strUserName: strUserName strCompanyKey:strCompanyKeyy arrCRMImageDetail:arrCRMImageDetail];
                    }
                    
                    //.......................................................................................

                    //........................................................................
#pragma mark- -- SoldServiceNonStandardDetail --

                    // For SoldServiceNonStandardDetail //
                    
                    NSArray *arrSoldServiceNonStandardDetail=[dictLeadDetail valueForKey:@"SoldServiceNonStandardDetail"];
                    
                    if([[dictLeadDetail valueForKey:@"SoldServiceNonStandardDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int l=0; l<arrSoldServiceNonStandardDetail.count; l++)
                        {
                            
                            //SoldServiceNonStandardDetail Entiy
                            entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
                            
                            SoldServiceNonStandardDetail *objentitySoldServiceNonStandardDetail = [[SoldServiceNonStandardDetail alloc]initWithEntity:entitySoldServiceNonStandardDetail insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictSoldServiceNonStandardDetail=[[NSMutableDictionary alloc]init];
                            dictSoldServiceNonStandardDetail=[arrSoldServiceNonStandardDetail objectAtIndex:l];
                            objentitySoldServiceNonStandardDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            objentitySoldServiceNonStandardDetail.createdBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedBy"]];
                            objentitySoldServiceNonStandardDetail.createdDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedDate"]];
                            objentitySoldServiceNonStandardDetail.departmentSysname=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DepartmentSysname"]];
                            objentitySoldServiceNonStandardDetail.discount=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Discount"]];
                            objentitySoldServiceNonStandardDetail.initialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InitialPrice"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]] isEqualToString:@"1"])
                            {
                                objentitySoldServiceNonStandardDetail.isSold=@"true";
                            }
                            else
                            {
                                objentitySoldServiceNonStandardDetail.isSold=@"false";
                            }
                            
                            //objentitySoldServiceNonStandardDetail.isSold=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]];
                            
                            
                            objentitySoldServiceNonStandardDetail.maintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MaintenancePrice"]];
                            objentitySoldServiceNonStandardDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedBy"]];
                            objentitySoldServiceNonStandardDetail. modifiedDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedDate"]];
                            objentitySoldServiceNonStandardDetail.modifiedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedInitialPrice"]];
                            objentitySoldServiceNonStandardDetail.modifiedMaintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedMaintenancePrice"]];
                            objentitySoldServiceNonStandardDetail.serviceDescription=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceDescription"]];
                            objentitySoldServiceNonStandardDetail.serviceFrequency=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceFrequency"]];
                            objentitySoldServiceNonStandardDetail.serviceName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceName"]];
                            objentitySoldServiceNonStandardDetail. soldServiceNonStandardId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"SoldServiceNonStandardId"]];
                            objentitySoldServiceNonStandardDetail.discountPercentage=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DiscountPercentage"]];
                            objentitySoldServiceNonStandardDetail.billingFrequencyPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencyPrice"]];
                            objentitySoldServiceNonStandardDetail.billingFrequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencySysName"]];
                            objentitySoldServiceNonStandardDetail.nonStdServiceTermsConditions=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"NonStdServiceTermsConditions"]];

                            objentitySoldServiceNonStandardDetail.frequencySysName = [NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FrequencySysName"]];

                            objentitySoldServiceNonStandardDetail.internalNotes=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InternalNotes"]];

                            //Nilind 16 Nov
                            
                            objentitySoldServiceNonStandardDetail.userName=strUserName;
                            objentitySoldServiceNonStandardDetail.companyKey=strCompanyKeyy;
                            
                            //........................
                            
                            objentitySoldServiceNonStandardDetail.wdoProblemIdentificationId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"WdoProblemIdentificationId"]];
                            objentitySoldServiceNonStandardDetail.mobileId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MobileId"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsBidOnRequest"]] isEqualToString:@"1"])
                            {
                                objentitySoldServiceNonStandardDetail.isBidOnRequest=@"true";
                            }
                            else
                            {
                                objentitySoldServiceNonStandardDetail.isBidOnRequest=@"false";
                            }
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsTaxable"]] isEqualToString:@"1"])
                            {
                                objentitySoldServiceNonStandardDetail.isTaxable=@"true";
                            }
                            else
                            {
                                objentitySoldServiceNonStandardDetail.isTaxable=@"false";
                            }
                            
//                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsLeadTest"]] isEqualToString:@"1"])
//                            {
//                                objentitySoldServiceNonStandardDetail.isLeadTest=@"true";
//                            }
//                            else
//                            {
//                                objentitySoldServiceNonStandardDetail.isLeadTest=@"false";
//                            }
                            objentitySoldServiceNonStandardDetail.isLeadTest=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsLeadTest"]];
                            
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsUnderWarranty"]] isEqualToString:@"1"])
                            {
                                objentitySoldServiceNonStandardDetail.isUnderWarranty=@"true";
                            }
                            else
                            {
                                objentitySoldServiceNonStandardDetail.isUnderWarranty=@"false";
                            }
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    //........................................................................
#pragma mark- -- SoldServiceStandardDetail --

                    
                    // For SoldServiceStandardDetail //
                    
                    NSArray *arrSoldServiceStandardDetail=[dictLeadDetail valueForKey:@"SoldServiceStandardDetail"];
                    
                    if([[dictLeadDetail valueForKey:@"SoldServiceStandardDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int m=0; m<arrSoldServiceStandardDetail.count; m++)
                        {
                            //SoldServiceStandardDetail Entiy
                            entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
                            
                            
                            SoldServiceStandardDetail *objentitySoldServiceStandardDetail = [[SoldServiceStandardDetail alloc]initWithEntity:entitySoldServiceStandardDetail insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictSoldServiceNonStandardDetail=[[NSMutableDictionary alloc]init];
                            dictSoldServiceNonStandardDetail=[arrSoldServiceStandardDetail objectAtIndex:m];
                            
                            objentitySoldServiceStandardDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objentitySoldServiceStandardDetail.createdBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedBy"]];
                            objentitySoldServiceStandardDetail.createdDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedDate"]];
                            objentitySoldServiceStandardDetail.discount=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Discount"]];
                            objentitySoldServiceStandardDetail.initialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InitialPrice"]];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]]isEqualToString:@"1"] )
                            {
                                objentitySoldServiceStandardDetail.isSold=@"true";
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isSold=@"false";
                            }
                            // objentitySoldServiceStandardDetail.isSold=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]];
                            
                            
                            objentitySoldServiceStandardDetail.maintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MaintenancePrice"]];
                            objentitySoldServiceStandardDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedBy"]];
                            objentitySoldServiceStandardDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedDate"]];
                            objentitySoldServiceStandardDetail.modifiedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedInitialPrice"]];
                            objentitySoldServiceStandardDetail.modifiedMaintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedMaintenancePrice"]];
                            
                            objentitySoldServiceStandardDetail.packageId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"PackageId"]];
                            
                            objentitySoldServiceStandardDetail.serviceFrequency=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceFrequency"]];
                            objentitySoldServiceStandardDetail.serviceId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceId"]];
                            objentitySoldServiceStandardDetail.serviceSysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceSysName"]];
                            objentitySoldServiceStandardDetail.soldServiceStandardId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"SoldServiceStandardId"]];
                            
                            //Nilind  30Sept
                            
                            objentitySoldServiceStandardDetail.serviceTermsConditions=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceTermsConditions"]];
                            
                            //.........................
                            
                            //Nilind 6 Oct
                            if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsTBD"]]isEqualToString:@"1"] )
                            {
                                objentitySoldServiceStandardDetail.isTBD=@"true";
                                
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isTBD=@"false";
                            }
                            //..........................
                            
                            //Nilind 16 Nov
                            
                            objentitySoldServiceStandardDetail.userName=strUserName;
                            objentitySoldServiceStandardDetail.companyKey=strCompanyKeyy;
                            
                            //........................
                            
                            //Nilind 22 May
                            
                            objentitySoldServiceStandardDetail.frequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FrequencySysName"]];
                            //End
                            
                            //Nilind 24 May
                            objentitySoldServiceStandardDetail.unit=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]];
                            objentitySoldServiceStandardDetail.finalUnitBasedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FinalUnitBasedInitialPrice"]];
                            objentitySoldServiceStandardDetail.finalUnitBasedMaintPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FinalUnitBasedMaintPrice"]];
                            
                            objentitySoldServiceStandardDetail.bundleId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]];
                            objentitySoldServiceStandardDetail.bundleDetailId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]];
                            objentitySoldServiceStandardDetail.billingFrequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencySysName"]];
                            objentitySoldServiceStandardDetail.discountPercentage=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DiscountPercentage"]];
                            
                            //End
                            
                            //Bundle Change
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]] isEqualToString:@""])
                                
                            {
                                
                                objentitySoldServiceStandardDetail.bundleId=@"0";
                                
                                
                            }
                            
                            else
                                
                            {
                                
                                objentitySoldServiceStandardDetail.bundleId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]];
                                
                                
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]] isEqualToString:@"0"])
                                
                            {
                                
                                //objentitySoldServiceStandardDetail.unit=@"0";
                                objentitySoldServiceStandardDetail.unit=@"1";

                                
                            }
                            
                            else
                                
                            {
                                
                                objentitySoldServiceStandardDetail.unit=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]];
                                
                            }
                            
                            
                            
                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]] isEqualToString:@""])
                                
                            {
                                
                                objentitySoldServiceStandardDetail.bundleDetailId=@"0";
                                
                                
                            }
                            
                            else
                                
                            {
                                
                                objentitySoldServiceStandardDetail.bundleDetailId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]];
                                
                                
                            }
                            
                            NSArray *arrTemp=[dictSoldServiceNonStandardDetail valueForKey:@"AdditionalParameterPriceDcs"];
                            
                            objentitySoldServiceStandardDetail.additionalParameterPriceDcs=arrTemp;
                            objentitySoldServiceStandardDetail.totalInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"TotalInitialPrice"]];
                            objentitySoldServiceStandardDetail.totalMaintPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"TotalMaintPrice"]];
                            objentitySoldServiceStandardDetail.billingFrequencyPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencyPrice"]];
                            objentitySoldServiceStandardDetail.servicePackageName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServicePackageName"]];
                            
                            objentitySoldServiceStandardDetail.serviceDescription=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceDescription"]];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsChangeServiceDesc"]]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsChangeServiceDesc"]] caseInsensitiveCompare:@"true"]==NSOrderedSame )
                            {
                                objentitySoldServiceStandardDetail.isChangeServiceDesc=@"true";
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isChangeServiceDesc=@"false";
                            }
                            
                            if([NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]].length==0 ||[[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]] isEqualToString:@"" ])
                            {
                                objentitySoldServiceStandardDetail.isResidentialTaxable=@"0";
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isResidentialTaxable=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]];
                            }
                            if([NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]].length==0 ||[[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]] isEqualToString:@"" ])
                            {
                                objentitySoldServiceStandardDetail.isCommercialTaxable=@"0";
                                
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isCommercialTaxable=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]];
                                
                            }

                            objentitySoldServiceStandardDetail.internalNotes=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InternalNotes"]];

                            if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsBidOnRequest"]] isEqualToString:@"1"])
                            {
                                objentitySoldServiceStandardDetail.isBidOnRequest=@"true";
                            }
                            else
                            {
                                objentitySoldServiceStandardDetail.isBidOnRequest=@"false";
                            }
                            
                            objentitySoldServiceStandardDetail.categorySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CategorySysName"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    
                    
                    //........Nilind 3 Oct................................................................
                    
                    // For CurrentService //
                    
#pragma mark- -- CurrentService --

                    
                    NSArray *arrCurrentService=[dictLeadDetail valueForKey:@"CurrentService"];
                    
                    if([[dictLeadDetail valueForKey:@"CurrentService"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int m=0; m<arrCurrentService.count; m++)
                            
                        {
                            
                            //SoldServiceStandardDetail Entiy
                            
                            entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
                            CurrentService *objentityCurrentService = [[CurrentService alloc]initWithEntity:entityCurrentService insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                            dictCurrentService=[arrCurrentService objectAtIndex:m];
                            objentityCurrentService.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            objentityCurrentService.serviceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceName"]];
                            objentityCurrentService.serviceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceSysName"]];
                            
                            objentityCurrentService.frequency=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Frequency"]];
                            
                            objentityCurrentService.frequencyId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"FrequencyId"]];
                            
                            objentityCurrentService.technicianName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"TechnicianName"]];
                            
                            objentityCurrentService.serviceDateTime=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceDateTime"]];
                            
                            objentityCurrentService.serviceTimeSlot=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceTimeSlot"]];
                            
                            objentityCurrentService.serviceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceId"]];
                            
                            objentityCurrentService.serviceKey=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceKey"]];
                            
                            
                            //Nilind 16 Nov
                            
                            objentityCurrentService.userName=strUserName;
                            objentityCurrentService.companyKey=strCompanyKeyy;
                            
                            //........................
                            
                            
                            NSError *error1;
                            
                            [context save:&error1];
                            
                        }
                        
                    }
                    

                    
                    //Nilind 16 Feb
                    //ServiceFollowUp
                    
#pragma mark- -- ServiceFollowUp --

                    NSArray *arrServiceFollowUp=[dictLeadDetail valueForKeyPath:@"LeadFollowup.serviceFollowUpDcs"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadFollowup.serviceFollowUpDcs"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int m=0; m<arrServiceFollowUp.count; m++)
                            
                        {
                            
                            //SoldServiceStandardDetail Entiy
                            
                            entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
                            ServiceFollowUpDcs *objentityServiceFollowUp = [[ServiceFollowUpDcs alloc]initWithEntity:entityServiceFollowUp insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                            dictCurrentService=[arrServiceFollowUp objectAtIndex:m];
                            
                            objentityServiceFollowUp.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objentityServiceFollowUp.departmentId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentId"]];
                            
                            objentityServiceFollowUp.departmentName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentName"]];
                            
                            objentityServiceFollowUp.departmentSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentSysName"]];
                            
                            objentityServiceFollowUp.followupDate=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"FollowupDate"]];
                            
                            objentityServiceFollowUp.id=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Id"]];
                            
                            objentityServiceFollowUp.notes=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Notes"]];
                            
                            objentityServiceFollowUp.serviceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceId"]];
                            
                            objentityServiceFollowUp.serviceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceName"]];
                            
                            objentityServiceFollowUp.serviceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceSysName"]];
                            NSError *error1;
                            
                            [context save:&error1];
                            
                        }
                        
                    }
                    

                    
                    //........................................................................
                    
                    
                    //End
                    
                    //Nilind 16 Feb
                    //ServiceFollowUp
#pragma mark- -- ProposalFollowUpDcs --

                    
                    NSArray *arrProposalFollowUp=[dictLeadDetail valueForKeyPath:@"LeadFollowup.proposalFollowUpDcs"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadFollowup.proposalFollowUpDcs"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int m=0; m<arrProposalFollowUp.count; m++)
                            
                        {
                            
                            //SoldServiceStandardDetail Entiy
                            
                            entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
                            ProposalFollowUpDcs *objentityProposalFollowUp = [[ProposalFollowUpDcs alloc]initWithEntity:entityProposalFollowUp insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                            dictCurrentService=[arrProposalFollowUp objectAtIndex:m];
                            
                            objentityProposalFollowUp.proposalLeadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objentityProposalFollowUp.proposalFollowupDate=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalFollowupDate"]];
                            
                            objentityProposalFollowUp.proposalId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalId"]];
                            
                            objentityProposalFollowUp.proposalNotes=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalNotes"]];
                            
                            objentityProposalFollowUp.proposalServiceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceId"]];
                            
                            objentityProposalFollowUp.proposalServiceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceName"]];
                            
                            objentityProposalFollowUp.proposalServiceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceSysName"]];
                            
                            NSError *error1;
                            
                            [context save:&error1];
                            
                        }
                        
                    }
                    

                    
                    //........................................................................
#pragma mark- -- LeadAgreementChecklistSetups --

                    //Agreement CheckList
                    
                    NSArray *arrAgreementCheckList=[dictLeadDetail valueForKeyPath:@"LeadAgreementChecklistSetups"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadAgreementChecklistSetups"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int m=0; m<arrAgreementCheckList.count; m++)
                        {
                            
                            entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
                            LeadAgreementChecklistSetups *objentityLeadAgreementChecklistSetups = [[LeadAgreementChecklistSetups alloc]initWithEntity:entityLeadAgreementChecklistSetups insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictAgreementCheckList=[[NSMutableDictionary alloc]init];
                            dictAgreementCheckList=[arrAgreementCheckList objectAtIndex:m];
                            
                            objentityLeadAgreementChecklistSetups.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objentityLeadAgreementChecklistSetups.leadAgreementChecklistSetupId=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"LeadAgreementChecklistSetupId"]];
                            
                            objentityLeadAgreementChecklistSetups.agreementChecklistId=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"AgreementChecklistId"]];
                            
                            objentityLeadAgreementChecklistSetups.agreementChecklistSysName=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"AgreementChecklistSysName"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"IsActive"]] isEqualToString:@"1"])
                            {
                                objentityLeadAgreementChecklistSetups.isActive=@"true";
                            }
                            else
                            {
                                objentityLeadAgreementChecklistSetups.isActive=@"false";
                            }
                            
                            objentityLeadAgreementChecklistSetups.userName=strUserName;
                            objentityLeadAgreementChecklistSetups.companyKey=strCompanyKeyy;
                            
                            //........................
                            NSError *error1;
                            [context save:&error1];
                            
                        }
                        
                    }
                    
                    
                    //........................................................................
                    //........................................................................
#pragma mark- -- ElectronicAuthorizationForm --

                    // Electronic Authorized Form
                    NSDictionary *dictAuthorizedForm;
                    if([[dictLeadDetail valueForKey:@"ElectronicAuthorizationForm"] isKindOfClass:[NSDictionary class]])
                    {
                        dictAuthorizedForm = [dictLeadDetail valueForKey:@"ElectronicAuthorizationForm"];
                    }
                    if(dictAuthorizedForm.count>0)
                    {
                        entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
                        
                        ElectronicAuthorizedForm *objMechanicalPOGeneric = [[ElectronicAuthorizedForm alloc] initWithEntity:entityElectronicAuthorizedForm insertIntoManagedObjectContext:context];
                        
                        objMechanicalPOGeneric.accountNo =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"AccountNo"]];
                        objMechanicalPOGeneric.leadNo = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"LeadNo"]];
                        objMechanicalPOGeneric.title = [dictAuthorizedForm valueForKey:@"Title"];
                        
                        objMechanicalPOGeneric.preNotes = [dictAuthorizedForm valueForKey:@"PreNotes"];
                        objMechanicalPOGeneric.postNotes = [dictAuthorizedForm valueForKey:@"PostNotes"];
                        objMechanicalPOGeneric.terms = [dictAuthorizedForm valueForKey:@"Terms"];
                        objMechanicalPOGeneric.termsSignUp =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"TermsSignUp"]];
                        
                        objMechanicalPOGeneric.frequency = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Frequency"]];
                        objMechanicalPOGeneric.monthlyDate =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"MonthlyDate"]];
                        objMechanicalPOGeneric.paymentMethod = [dictAuthorizedForm valueForKey:@"PaymentMethod"];
                        objMechanicalPOGeneric.bankName = [dictAuthorizedForm valueForKey:@"BankName"];            objMechanicalPOGeneric.bankAccountNo = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"BankAccountNo"]];
                        // [dictData valueForKey:@"BankAccountNo"];
                        objMechanicalPOGeneric.routingNo =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"RoutingNo"]];
                        objMechanicalPOGeneric.signaturePath = [dictAuthorizedForm valueForKey:@"SignaturePath"];            objMechanicalPOGeneric.firstName = [dictAuthorizedForm valueForKey:@"FirstName"];
                        objMechanicalPOGeneric.middleName = [dictAuthorizedForm valueForKey:@"MiddleName"];            objMechanicalPOGeneric.lastName = [dictAuthorizedForm valueForKey:@"LastName"];
                        objMechanicalPOGeneric.address1 = [dictAuthorizedForm valueForKey:@"Address1"];
                        objMechanicalPOGeneric.address2 = [dictAuthorizedForm valueForKey:@"Address2"];            objMechanicalPOGeneric.stateName = [dictAuthorizedForm valueForKey:@"StateName"];
                        objMechanicalPOGeneric.cityName = [dictAuthorizedForm valueForKey:@"CityName"];
                        objMechanicalPOGeneric.zipCode =[NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Zipcode"]];
                        objMechanicalPOGeneric.phone =[NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Phone"]];
                        objMechanicalPOGeneric.email = [global strReplacedEmail:[dictAuthorizedForm valueForKey:@"Email"]];
                        objMechanicalPOGeneric.date =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"Date"]];
                        objMechanicalPOGeneric.createdBy =[NSString stringWithFormat:@"%@" ,[dictAuthorizedForm valueForKey:@"CreatedBy"]];
                        objMechanicalPOGeneric.signatureName =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"SignatureName"]];
                        objMechanicalPOGeneric.stateId = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"StateId"]];
                        objMechanicalPOGeneric.leadId =[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                        
                        NSError *error2;
                        [context save:&error2];
                        
                        
                    }
                    // For LeadAppliedDiscounts /
                    // 26 Aug 2016
#pragma mark- -- LeadAppliedDiscounts --

                    NSArray *arrLeadAppliedDiscounts=[dictLeadDetail valueForKey:@"LeadAppliedDiscounts"];//EmailDetail
                    
                    if([[dictLeadDetail valueForKey:@"LeadAppliedDiscounts"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadAppliedDiscounts.count; j++)
                        {
                            //Email Detail Entity
                            entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
                            
                            LeadAppliedDiscounts *objLeadAppliedDiscounts = [[LeadAppliedDiscounts alloc]initWithEntity:entityLeadAppliedDiscounts insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictobjLeadAppliedDiscounts=[[NSMutableDictionary alloc]init];
                            dictobjLeadAppliedDiscounts=[arrLeadAppliedDiscounts objectAtIndex:j];
                            
                            
                            objLeadAppliedDiscounts.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            objLeadAppliedDiscounts.leadAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKeyPath:@"LeadAppliedDiscountId"]];
                            objLeadAppliedDiscounts.serviceSysName=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ServiceSysName"]];
                            objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountSysName"]];
                            objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountType"]];
                            objLeadAppliedDiscounts.discountCode=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountCode"]];
                            objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountAmount"]];
                            objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountDescription"]];
                            objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountPercent"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsActive"]] isEqualToString:@"1"])
                            {
                                objLeadAppliedDiscounts.isActive=@"true";
                            }
                            else
                            {
                                objLeadAppliedDiscounts.isActive=@"false";
                            }
                            if([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
                            {
                                objLeadAppliedDiscounts.isDiscountPercent=@"true";
                            }
                            else
                            {
                                objLeadAppliedDiscounts.isDiscountPercent=@"false";
                            }
                            //Nilind 16 Nov
                            objLeadAppliedDiscounts.soldServiceId=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"SoldServiceId"]];
                            objLeadAppliedDiscounts.serviceType=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ServiceType"]];
                            
                            objLeadAppliedDiscounts.userName=strUserName;
                            objLeadAppliedDiscounts.companyKey=strCompanyKeyy;
                            objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AppliedInitialDiscount"]];
                            objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AppliedMaintDiscount"]];
                            //........................
                            //objLeadAppliedDiscounts.applicableForInitial=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForInitial"]];
                            //objLeadAppliedDiscounts.applicableForMaintenance=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForMaintenance"]];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForInitial"]] isEqualToString:@"1"])
                            {
                                objLeadAppliedDiscounts.applicableForInitial=@"true";
                            }
                            else
                            {
                                objLeadAppliedDiscounts.applicableForInitial=@"false";
                                
                            }
                            
                            if ([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForMaintenance"]] isEqualToString:@"1"])
                            {
                                objLeadAppliedDiscounts.applicableForMaintenance=@"true";
                            }
                            else
                            {
                                objLeadAppliedDiscounts.applicableForMaintenance=@"false";
                                
                            }
                            
                            
                            objLeadAppliedDiscounts.isApplied=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsApplied"]];
                            objLeadAppliedDiscounts.accountNo=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AccountNo"]];

                            //.................
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    

                    //......................
                    
    #pragma mark- SAVE CLARK PEST DATA
    #pragma mark- -- LeadCommercialScopeExtDc --
                    
                    NSArray *arrLeadCommercialScopeExtDc=[dictLeadDetail valueForKey:@"LeadCommercialScopeExtDc"];//EmailDetail
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialScopeExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadCommercialScopeExtDc.count; j++)
                        {
                            //Email Detail Entity
                            entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
                            
                            LeadCommercialScopeExtDc *objLeadCommercial = [[LeadCommercialScopeExtDc alloc]initWithEntity:entityLeadCommercialScopeExtDc insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictobjLeadCommercialScopeExtDc=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercialScopeExtDc=[arrLeadCommercialScopeExtDc objectAtIndex:j];
                            
                            objLeadCommercial.companyKey=strCompanyKeyy;
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            objLeadCommercial.scopeSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"ScopeSysName"]];
                            
                            objLeadCommercial.scopeDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"ScopeDescription"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"IsChangedScopeDesc"]] isEqualToString:@"1"])
                            {
                                objLeadCommercial.isChangedScopeDesc=@"true";
                            }
                            else
                            {
                                objLeadCommercial.isChangedScopeDesc=@"false";
                            }
                            objLeadCommercial.title=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"Title"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    
                    
                    
                    //........................................................................
    #pragma mark- -- LeadCommercialTargetExtDc --
                    
                    NSArray *arrLeadCommercialTargetExtDc=[dictLeadDetail valueForKey:@"LeadCommercialTargetExtDc"];//EmailDetail
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialTargetExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                                        for (int j=0; j<arrLeadCommercialTargetExtDc.count; j++)
                                        {
                                            //Email Detail Entity
                                            entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
                                            
                                            LeadCommercialTargetExtDc *objLeadCommercial = [[LeadCommercialTargetExtDc alloc]initWithEntity:entityLeadCommercialTargetExtDc insertIntoManagedObjectContext:context];
                                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                                            dictobjLeadCommercial=[arrLeadCommercialTargetExtDc objectAtIndex:j];
                                            
                                            
                                            
                                            objLeadCommercial.companyKey=strCompanyKeyy;
                                            
                                            
                                            objLeadCommercial.userName=strUserName;
                                            
                                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                                            
                                            
                                            objLeadCommercial.targetSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TargetSysName"]];
                                            
                                            objLeadCommercial.targetDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKeyPath:@"TargetDescription"]];
                                            
                                            if([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"IsChangedTargetDesc"]] isEqualToString:@"1"])
                                            {
                                                objLeadCommercial.isChangedTargetDesc=@"true";
                                            }
                                            else
                                            {
                                                objLeadCommercial.isChangedTargetDesc=@"false";
                                            }
                                            objLeadCommercial.name=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"Name"]];
                                            
                                            objLeadCommercial.leadCommercialTargetId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommercialTargetId"]];
                                            
                                            objLeadCommercial.mobileTargetId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"MobileTargetId"]];
                                            objLeadCommercial.wdoProblemIdentificationId = [NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"WdoProblemIdentificationId"]];
                                            
                        #pragma mark- -- Target Image Detail --
                                            
                                            NSArray *arrTargetImageDetail=[dictobjLeadCommercial valueForKey:@"TargetImageDetailExtDc"];//EmailDetail
                                            
                                            WebService *objWebService = [[WebService alloc] init];
                                            [objWebService saveTargetImageToDBWithStrLeadId:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]] strUserName:strUserName strCompanyKey:strCompanyKeyy arrTargetImageDetail:arrTargetImageDetail];
                                            
                                            //.......................................................................................
                                            
                                            NSError *error1;
                                            [context save:&error1];
                                        }
                    }
                    
                    //........................................................................
                    
    #pragma mark- -- LeadCommercialInitialInfoExtDc --
                    
                    NSArray *arrLeadCommercialInitialInfoExtDc=[dictLeadDetail valueForKey:@"LeadCommercialInitialInfoExtDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialInitialInfoExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadCommercialInitialInfoExtDc.count; j++)
                        {
                            //Email Detail Entity
                            entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
                            
                            LeadCommercialInitialInfoExtDc *objLeadCommercial = [[LeadCommercialInitialInfoExtDc alloc]initWithEntity:entityLeadCommercialInitialInfoExtDc insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercial=[arrLeadCommercialInitialInfoExtDc objectAtIndex:j];
                            
                            
                            
                            objLeadCommercial.companyKey=strCompanyKeyy;
                            
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            
                            objLeadCommercial.initialDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"initialDescription"]];
                            
                            objLeadCommercial.leadCommInitialInfoId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommInitialInfoId"]];
                            
                            objLeadCommercial.initialPrice=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"InitialPrice"]];
                            
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    //.......................................................................................
    #pragma mark- -- LeadCommercialMaintInfoExtDc --
                    
                    NSArray *arrLeadCommercialMaintInfoExtDc=[dictLeadDetail valueForKey:@"LeadCommercialMaintInfoExtDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialMaintInfoExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadCommercialMaintInfoExtDc.count; j++)
                        {
                            //Email Detail Entity
                            entityLeadCommercialMaintInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
                            
                            LeadCommercialMaintInfoExtDc *objLeadCommercial = [[LeadCommercialMaintInfoExtDc alloc]initWithEntity:entityLeadCommercialMaintInfoExtDc insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercial=[arrLeadCommercialMaintInfoExtDc objectAtIndex:j];
                            
                            
                            
                            objLeadCommercial.companyKey=strCompanyKeyy;
                            
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            
                            objLeadCommercial.maintenanceDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"maintenanceDescription"]];
                            
                            objLeadCommercial.maintenancePrice=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"MaintenancePrice"]];
                            
                            objLeadCommercial.frequencySysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"FrequencySysName"]];
                            
                            objLeadCommercial.leadCommMaintInfoId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommMaintInfoId"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    //........................................................................
    #pragma mark- -- LeadCommercialDiscountExtDc --
                    
                    NSArray *arrLeadCommercialDiscountExtDc=[dictLeadDetail valueForKey:@"LeadCommercialDiscountExtDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialDiscountExtDc"] isKindOfClass:[NSArray class]])
                    {
                     
                        for (int j=0; j<arrLeadCommercialDiscountExtDc.count; j++)
                        {
                            entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
                            
                            LeadCommercialDiscountExtDc *objLeadCommercial = [[LeadCommercialDiscountExtDc alloc]initWithEntity:entityLeadCommercialDiscountExtDc insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercial=[arrLeadCommercialDiscountExtDc objectAtIndex:j];
                            
                            
                            
                            objLeadCommercial.companyKey = strCompanyKeyy;
                            
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            
                            objLeadCommercial.discountSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountSysName"]];
                            
                            objLeadCommercial.discountType=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountType"]];
                            
                            objLeadCommercial.discountDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountDescription"]];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
                            {
                                objLeadCommercial.isDiscountPercent=@"true";
                            }
                            else
                            {
                                objLeadCommercial.isDiscountPercent=@"false";
                                
                            }
                            objLeadCommercial.discountPercent=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountPercent"]];
                            objLeadCommercial.discountCode=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountCode"]];
                            
                            
                            objLeadCommercial.appliedInitialDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AppliedInitialDiscount"]];
                            
                            
                            objLeadCommercial.appliedMaintDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AppliedMaintDiscount"]];
                            
                            objLeadCommercial.accountNo=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AccountNo"]];
                            
                            
                            if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ApplicableForInitial"]] isEqualToString:@"1"])
                            {
                                objLeadCommercial.applicableForInitial=@"true";
                            }
                            else
                            {
                                objLeadCommercial.applicableForInitial=@"false";
                                
                            }
                            
                            if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ApplicableForMaintenance"]] isEqualToString:@"1"])
                            {
                                objLeadCommercial.applicableForMaintenance=@"true";
                            }
                            else
                            {
                                objLeadCommercial.applicableForMaintenance=@"false";
                                
                            }
                            
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    
                    //........................................................................
    #pragma mark- -- LeadCommercialTermsExtDc --
                    
                    NSArray *arrLeadCommercialTermsExtDc=[dictLeadDetail valueForKey:@"LeadCommercialTermsExtDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadCommercialTermsExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadCommercialTermsExtDc.count; j++)
                        {
                            entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
                            
                            LeadCommercialTermsExtDc *objLeadCommercial = [[LeadCommercialTermsExtDc alloc]initWithEntity:entityLeadCommercialTermsExtDc insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercial=[arrLeadCommercialTermsExtDc objectAtIndex:j];
                            
                            
                            
                            objLeadCommercial.companyKey=strCompanyKeyy;
                            
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            
                            objLeadCommercial.branchSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"BranchSysName"]];
                            
                            objLeadCommercial.termsnConditions=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TermsnConditions"]];
                            
                            objLeadCommercial.leadCommercialTermsId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommercialTermsId"]];
                            
                            objLeadCommercial.termsId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TermsId"]];
                            
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    

                    
                    //........................................................................
    #pragma mark- -- LeadCommercialDetailExtDc --
                    
                    entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialDetailExtDc *objLeadCommercialDetailExtDc = [[LeadCommercialDetailExtDc alloc]initWithEntity:entityLeadCommercialDetailExtDc insertIntoManagedObjectContext:context];
                    if(![[dictLeadDetail valueForKey:@"LeadCommercialDetailExtDc"] isKindOfClass:[NSDictionary class]])
                    {
                        NSLog(@"NOTHING IN LeadCommercialDetailExtDc");
                    }
                    else
                    {
                        objLeadCommercialDetailExtDc.companyKey=strCompanyKeyy;
                        
                        
                        objLeadCommercialDetailExtDc.userName=strUserName;
                        
                        objLeadCommercialDetailExtDc.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                        
                        
                        objLeadCommercialDetailExtDc.leadCommercialDetailId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.LeadCommercialDetailId"]];
                        
                        objLeadCommercialDetailExtDc.coverLetterSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.CoverLetterSysName"]];
                        
                        objLeadCommercialDetailExtDc.introSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IntroSysName"]];
                        
                        objLeadCommercialDetailExtDc.introContent=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IntroContent"]];
                        
                        objLeadCommercialDetailExtDc.termsOfServiceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.TermsOfServiceSysName"]];
                        
                        objLeadCommercialDetailExtDc.termsOfServiceContent=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.TermsOfServiceContent"]];
                        
                        objLeadCommercialDetailExtDc.isAgreementValidFor=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsAgreementValidFor"]];
                        
                        objLeadCommercialDetailExtDc.validFor=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.ValidFor"]];
                        
                        objLeadCommercialDetailExtDc.isInitialTaxApplicable=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsInitialTaxApplicable"]];
                        objLeadCommercialDetailExtDc.isMaintTaxApplicable=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsMaintTaxApplicable"]];
                        
                    }
                    
                    //End
                    
                    //........................................................................
    #pragma mark- -- LeadMarketingContentExtDc --
                    
                    NSArray *arrLeadMarketingContentExtDc=[dictLeadDetail valueForKey:@"LeadMarketingContentExtDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadMarketingContentExtDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int j=0; j<arrLeadMarketingContentExtDc.count; j++)
                        {
                            entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
                            
                            LeadMarketingContentExtDc *objLeadCommercial = [[LeadMarketingContentExtDc alloc]initWithEntity:entityLeadMarketingContentExtDc insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                            dictobjLeadCommercial=[arrLeadMarketingContentExtDc objectAtIndex:j];
                            
                            
                            
                            objLeadCommercial.companyKey=strCompanyKeyy;
                            
                            
                            objLeadCommercial.userName=strUserName;
                            
                            objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            
                            
                            objLeadCommercial.contentSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ContentSysName"]];
                            
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    

                    #pragma mark- -- LeadCotact Details --

                    // For LeadContactDetailExtSerDc
                    
                    NSArray *arrLeadContactDetail=[dictLeadDetail valueForKey:@"LeadContactDetailExtSerDc"];
                    
                    if([[dictLeadDetail valueForKey:@"LeadContactDetailExtSerDc"] isKindOfClass:[NSArray class]])
                    {
                        
                        for (int z=0; z<arrLeadContactDetail.count; z++)
                        {
                            entityContactNumberDetail=[NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
                            
                            LeadContactDetailExtSerDc *objLeadContactDetail = [[LeadContactDetailExtSerDc alloc]initWithEntity:entityContactNumberDetail insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictLeadContactDetail=[[NSMutableDictionary alloc]init];
                            dictLeadContactDetail=[arrLeadContactDetail objectAtIndex:z];
                          
                            
                            
                            objLeadContactDetail.leadContactId = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"LeadContactId"]];
                            objLeadContactDetail.contactNumber = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ContactNumber"]];
                            objLeadContactDetail.subject = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"Subject"]];
                            
                            objLeadContactDetail.createdBy = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"CreatedBy"]];

                            objLeadContactDetail.modifiedBy = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ModifiedBy"]];

                            objLeadContactDetail.createdDate = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"CreatedDate"]];
                            objLeadContactDetail.modifiedDate = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ModifiedDate"]];
                            
                            if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsMessageSent"]] isEqualToString:@"1"])
                            {
                                objLeadContactDetail.isMessageSent = @"true";

                            }
                            else
                            {
                                objLeadContactDetail.isMessageSent = @"false";

                            }
                            if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsCustomerNumber"]] isEqualToString:@"1"])
                            {
                                objLeadContactDetail.isCustomerNumber = @"true";
                                
                            }
                            else
                            {
                                objLeadContactDetail.isCustomerNumber = @"false";
                                
                            }
                            if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsDefaultNumber"]] isEqualToString:@"1"])
                            {
                                objLeadContactDetail.isDefaultNumber = @"true";
                                
                            }
                            else
                            {
                                objLeadContactDetail.isDefaultNumber = @"false";
                                
                            }
                            //Nilind 16 Nov
                             objLeadContactDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            objLeadContactDetail.userName=strUserName;
                            objLeadContactDetail.companyKey=strCompanyKeyy;
                            
                            //........................
                            NSError *error10;
                            [context save:&error10];
                            //}
                        }
                        //........................................................................
                        //........................................................................
                    }
  
                #pragma mark- -- Renewal Service Details --

                                           
                    NSArray *arrRenewalServiceDetail=[dictLeadDetail valueForKey:@"RenewalServiceExtDcs"];
                    
                    if([[dictLeadDetail valueForKey:@"RenewalServiceExtDcs"] isKindOfClass:[NSArray class]])
                    {
                       
                        for (int z=0; z<arrRenewalServiceDetail.count; z++)
                        {
                            entityRenewalServiceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
                            
                            RenewalServiceExtDcs *objRenewalServiceDetail = [[RenewalServiceExtDcs alloc]initWithEntity:entityRenewalServiceDetail insertIntoManagedObjectContext:context];
                            
                            NSMutableDictionary *dictRenewalServiceDetail=[[NSMutableDictionary alloc]init];
                            dictRenewalServiceDetail=[arrRenewalServiceDetail objectAtIndex:z];
                            
                            
                            objRenewalServiceDetail.renewalAmount = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalAmount"]];
                            objRenewalServiceDetail.renewalDescription = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalDescription"]];
                            objRenewalServiceDetail.renewalFrequencySysName = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalFrequencySysName"]];
                            objRenewalServiceDetail.renewalPercentage = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalPercentage"]];
                            objRenewalServiceDetail.renewalServiceId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalServiceId"]];
                            objRenewalServiceDetail.serviceId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"ServiceId"]];
                            objRenewalServiceDetail.soldServiceStandardId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"SoldServiceStandardId"]];
                            objRenewalServiceDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                            objRenewalServiceDetail.userName=strUserName;
                            objRenewalServiceDetail.companyKey=strCompanyKeyy;
                            
                            //........................
                            NSError *error10;
                            [context save:&error10];
                            
                        }
                        //........................................................................
                        //........................................................................
                    }
                    
                    
                    
                    #pragma mark- -- Save Tag Details Details --
                              
                    NSArray *arrTagDetail=[dictLeadDetail valueForKey:@"LeadTagExtDcs"];
                    if([arrTagDetail isKindOfClass:[NSArray class]] && arrTagDetail.count > 0)
                    {
                        WebService *objWebService = [[WebService alloc] init];
                        [objWebService saveTagDetailsWithArrayTags:arrTagDetail strLeadId:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]]];
                    }
                    
                    #pragma mark- -- UDF detail  --
                    
                    NSArray *arrUDFSavedData=[dictLeadDetail valueForKey:@"UDFSavedData"];
                    if([arrUDFSavedData isKindOfClass:[NSArray class]] && arrUDFSavedData.count > 0)
                    {
                        WebService *objWebService = [[WebService alloc] init];
                        
                        
                        [objWebService saveUserdefinedataDetailsWithArrayUDF:arrUDFSavedData strLeadId:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]]];
                        
                    }
                   
                    NSError *error;
                    [context save:&error];
                    
                    if ([strType isEqualToString:@"All"]) {

                        
                    }else{
                        
                        break;
                        
                    }
                    //break;
                    
                }
            }
    
}

-(void)saveWorkOrderObj : (NSDictionary*)dictDataWorkOrders{
    
    
    if(![dictDataWorkOrders isKindOfClass:[NSDictionary class]])
    {
        
        
    }
    else
    {
        
        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
        WorkOrderDetailsService *objWorkOrderDetailsService = [[WorkOrderDetailsService alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
        
        
        objWorkOrderDetailsService.workorderId =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
        
        objWorkOrderDetailsService.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
        
        objWorkOrderDetailsService.companyId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyId"]];
        
        objWorkOrderDetailsService.surveyID=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyID"]];
        
        objWorkOrderDetailsService.departmentId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentId"]];
        
        objWorkOrderDetailsService.accountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
        objWorkOrderDetailsService.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ThirdPartyAccountNo"]];

        
        objWorkOrderDetailsService.employeeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EmployeeNo"]];
        
        objWorkOrderDetailsService.firstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"FirstName"]];
        
        objWorkOrderDetailsService.middleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"MiddleName"]];
        
        objWorkOrderDetailsService.lastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastName"]];
        
        objWorkOrderDetailsService.companyName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyName"]];
        
        NSString *strEmailPrimaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryEmail"]];
        
        if (strEmailPrimaryServer.length==0) {
            
            objWorkOrderDetailsService.primaryEmail=@"";
            
        } else {
                                        
            objWorkOrderDetailsService.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"PrimaryEmail"]]];
            
        }
        
        
        NSString *strEmailSecondaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryEmail"]];
        
        if (strEmailSecondaryServer.length==0) {
            
            objWorkOrderDetailsService.secondaryEmail=@"";
            
        } else {
            
            objWorkOrderDetailsService.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"SecondaryEmail"]]];
            
        }
        
        
        objWorkOrderDetailsService.primaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryPhone"]];
        
        objWorkOrderDetailsService.secondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryPhone"]];
        
        objWorkOrderDetailsService.scheduleOnStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
        
        objWorkOrderDetailsService.scheduleOnEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
        
        objWorkOrderDetailsService.scheduleStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]];
        
        objWorkOrderDetailsService.scheduleEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]];
        
        objWorkOrderDetailsService.serviceDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDateTime"]];
        
        objWorkOrderDetailsService.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalEstimationTime"]];
        
        objWorkOrderDetailsService.branchId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchId"]];
        
        objWorkOrderDetailsService.workorderStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderStatus"]];
        
        objWorkOrderDetailsService.atributes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Attributes"]];
        
        objWorkOrderDetailsService.specialInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SpecialInstruction"]];
        
        objWorkOrderDetailsService.serviceInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceInstruction"]];
        
        objWorkOrderDetailsService.direction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Direction"]];
        
        objWorkOrderDetailsService.otherInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OtherInstruction"]];
        
        objWorkOrderDetailsService.categoryName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategoryName"]];
        
        objWorkOrderDetailsService.subCategory=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SubCategory"]];
        
        objWorkOrderDetailsService.services=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Services"]];
        
        objWorkOrderDetailsService.currentServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CurrentServices"]];
        
        objWorkOrderDetailsService.lastServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastServices"]];
        
        objWorkOrderDetailsService.technicianComment=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianComment"]];
        
        objWorkOrderDetailsService.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
        
        objWorkOrderDetailsService.timeIn=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeIn"]];
        
        objWorkOrderDetailsService.timeOut=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOut"]];
        
        objWorkOrderDetailsService.resetId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetId"]];
        
        objWorkOrderDetailsService.resetDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetDescription"]];
        
        objWorkOrderDetailsService.audioFilePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AudioFilePath"]];
        
        objWorkOrderDetailsService.invoiceAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"InvoiceAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoiceAmount"]];
        
        objWorkOrderDetailsService.productionAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"ProductionAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProductionAmount"]];
        
        objWorkOrderDetailsService.previousBalance=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"PreviousBalance"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PreviousBalance"]];
        
        objWorkOrderDetailsService.tax=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"Tax"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tax"]];
        
        objWorkOrderDetailsService.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerSignaturePath"]];
        
        objWorkOrderDetailsService.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianSignaturePath"]];
        
        objWorkOrderDetailsService.electronicSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ElectronicSignaturePath"]];
        
        objWorkOrderDetailsService.invoicePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoicePath"]];
        
        objWorkOrderDetailsService.billingAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress1"]];
        
        objWorkOrderDetailsService.billingAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress2"]];
        
        objWorkOrderDetailsService.billingCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCountry"]];
        
        objWorkOrderDetailsService.billingState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingState"]];
        
        objWorkOrderDetailsService.billingCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCity"]];
        
        objWorkOrderDetailsService.billingZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingZipcode"]];
        
        objWorkOrderDetailsService.servicesAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicesAddress1"]];
        
        objWorkOrderDetailsService.serviceAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddress2"]];
        
        objWorkOrderDetailsService.serviceCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCountry"]];
        
        objWorkOrderDetailsService.serviceState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceState"]];
        
        objWorkOrderDetailsService.serviceCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCity"]];
        
        objWorkOrderDetailsService.serviceZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceZipcode"]];
        
        objWorkOrderDetailsService.serviceAddressLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLatitude"]];
        
        objWorkOrderDetailsService.serviceAddressLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLongitude"]];
        
        objWorkOrderDetailsService.isPDFGenerated=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPDFGenerated"]];
        
        objWorkOrderDetailsService.isMailSent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsMailSent"]];
        
        objWorkOrderDetailsService.isElectronicSignatureAvailable=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsElectronicSignatureAvailable"]];
        
        objWorkOrderDetailsService.isFail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsFail"]];
        
        objWorkOrderDetailsService.operateMedium=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OperateMedium"]];
        
        objWorkOrderDetailsService.isActive=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsActive"]];
        
        objWorkOrderDetailsService.surveyStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyStatus"]];
        
        objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
        
        objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
        
        objWorkOrderDetailsService.modifiedFormatedDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
        
        objWorkOrderDetailsService.modifiedBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedBy"]];
        
        objWorkOrderDetailsService.userName=strUserName;
        objWorkOrderDetailsService.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTime"]];
        objWorkOrderDetailsService.routeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteNo"]];
        objWorkOrderDetailsService.routeName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteName"]];
        objWorkOrderDetailsService.targets=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Targets"]];
        
        objWorkOrderDetailsService.deviceVersion=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceVersion"]];
        objWorkOrderDetailsService.deviceName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceName"]];
        objWorkOrderDetailsService.versionNumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionNumber"]];
        objWorkOrderDetailsService.versionDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionDate"]];
        
        NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
        [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ScheduleStartDateTime"]]];
        objWorkOrderDetailsService.zdateScheduledStart=newTimeSchedule;
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ModifiedFormatedDate"]]];
        objWorkOrderDetailsService.dateModified=newTime;
        
        objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCustomerNotPresent"]];
        
        objWorkOrderDetailsService.totalallowCrew=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalallowCrew"]];
        
        objWorkOrderDetailsService.assignCrewIds=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AssignCrewIds"]];
        
        objWorkOrderDetailsService.routeCrewList=[dictDataWorkOrders valueForKey:@"RouteCrewList"];
        
        objWorkOrderDetailsService.zSync=@"blank";
        
        // objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",@"false"];
        
        //Nilind 27 Feb
        
        objWorkOrderDetailsService.arrivalDuration=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ArrivalDuration"]];
        objWorkOrderDetailsService.keyMap=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"KeyMap"]];
        objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
        objWorkOrderDetailsService.problemDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProblemDescription"]];
        objWorkOrderDetailsService.serviceDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDescription"]];
        //End
        
        objWorkOrderDetailsService.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryServiceSysName"]];
        objWorkOrderDetailsService.categorySysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategorySysName"]];
        
        NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
        NSString *dbCompanyKey=[defsLogin valueForKey:@"companyKey"];
        // NSString *dbUserName =[defsLogin valueForKey:@"username"];
        
        objWorkOrderDetailsService.dbCompanyKey=dbCompanyKey;
        objWorkOrderDetailsService.dbUserName=strEmployeeNo;
        
        objWorkOrderDetailsService.noChemical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"NoChemical"]];
        
        objWorkOrderDetailsService.serviceJobDescriptionId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescriptionId"]];
        
        objWorkOrderDetailsService.serviceJobDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescription"]];
        
        objWorkOrderDetailsService.isEmployeePresetSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsEmployeePresetSignature"]];
        
        objWorkOrderDetailsService.accountDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountDescription"]];
        
        //DepartmentSysName  isResendInvoiceMail
        objWorkOrderDetailsService.departmentSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
        
        objWorkOrderDetailsService.departmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
        
        objWorkOrderDetailsService.isResendInvoiceMail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsResendInvoiceMail"]];
        
        //
        
        objWorkOrderDetailsService.cellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CellNo"]];
        objWorkOrderDetailsService.billingFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingFirstName"]];
        objWorkOrderDetailsService.billingMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMiddleName"]];
        objWorkOrderDetailsService.billingLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingLastName"]];
        objWorkOrderDetailsService.billingCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCellNo"]];
        objWorkOrderDetailsService.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingPrimaryEmail"]]];
        objWorkOrderDetailsService.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingSecondaryEmail"]]];
        objWorkOrderDetailsService.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPrimaryPhone"]];
        objWorkOrderDetailsService.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSecondaryPhone"]];
        objWorkOrderDetailsService.billingMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMapCode"]];
        objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];

        
        objWorkOrderDetailsService.serviceFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceFirstName"]];
        objWorkOrderDetailsService.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMiddleName"]];
        objWorkOrderDetailsService.serviceLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceLastName"]];
        objWorkOrderDetailsService.serviceCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCellNo"]];
        objWorkOrderDetailsService.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServicePrimaryEmail"]]];
        objWorkOrderDetailsService.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServiceSecondaryEmail"]]];
        objWorkOrderDetailsService.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePrimaryPhone"]];
        objWorkOrderDetailsService.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSecondaryPhone"]];
        objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
        objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
        objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
        objWorkOrderDetailsService.serviceAddressImagePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressImagePath"]];
        objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];

        //Nilind 08 Dec
        
        objWorkOrderDetailsService.timeInLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLatitude"]];
        objWorkOrderDetailsService.timeInLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLongitude"]];
        objWorkOrderDetailsService.timeOutLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLatitude"]];
        objWorkOrderDetailsService.timeOutLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLongitude"]];
        objWorkOrderDetailsService.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLatitude"]];
        objWorkOrderDetailsService.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLongitude"]];
        objWorkOrderDetailsService.termiteStateType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TermiteStateType"]];
        //End
        
        objWorkOrderDetailsService.billingCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCounty"]];
        objWorkOrderDetailsService.serviceCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCounty"]];
        objWorkOrderDetailsService.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSchoolDistrict"]];
        objWorkOrderDetailsService.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSchoolDistrict"]];

        objWorkOrderDetailsService.earliestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTimeStr"]];
        objWorkOrderDetailsService.latestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTimeStr"]];
        objWorkOrderDetailsService.driveTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTimeStr"]];
        
        
        objWorkOrderDetailsService.fromDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
        objWorkOrderDetailsService.toDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
        objWorkOrderDetailsService.modifyDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
        
        NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
        
        if ([strIsRegularPestFlow isEqualToString:@"1"] || [strIsRegularPestFlow isEqualToString:@"True"] || [strIsRegularPestFlow isEqualToString:@"true"]) {
            
            objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"true"];
            
        } else {
            
            objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"false"];

        }
        
        NSString *strIsRegularPestFlowOnBranch = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];

        
        if ([strIsRegularPestFlowOnBranch isEqualToString:@"1"] || [strIsRegularPestFlowOnBranch isEqualToString:@"True"] || [strIsRegularPestFlowOnBranch isEqualToString:@"true"]) {
            
            objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"true"];

        } else {
            
            objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"false"];
            
        }
        

        NSString *strIsCollectPayment = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCollectPayment"]];
        
        if ([strIsCollectPayment isEqualToString:@"1"] || [strIsCollectPayment isEqualToString:@"True"] || [strIsCollectPayment isEqualToString:@"true"]) {
            
            objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"true"];
            
        } else {
            
            objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"false"];
            
        }
        
        
        NSString *strIsWhetherShow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsWhetherShow"]];
        
        if ([strIsWhetherShow isEqualToString:@"1"] || [strIsWhetherShow isEqualToString:@"True"] || [strIsWhetherShow isEqualToString:@"true"]) {
            
            objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"true"];
            
        } else {
            
            objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"false"];
            
        }
        
        /*
        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];
        */
        
        objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];
        
        objWorkOrderDetailsService.relatedOpportunityNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RelatedOpportunityNo"]];

        objWorkOrderDetailsService.taxPercent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TaxPercent"]];

        objWorkOrderDetailsService.serviceReportFormat=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceReportFormat"]];

        objWorkOrderDetailsService.isBatchReleased=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBatchReleased"]];

        objWorkOrderDetailsService.accountName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountName"]];

        objWorkOrderDetailsService.driveTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTime"]];

        objWorkOrderDetailsService.isNPMATermite=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsNPMATermite"]];
        
        objWorkOrderDetailsService.branchSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchSysNameNew"]];//BranchSysName

        objWorkOrderDetailsService.isMailSent = @"false";
        
        objWorkOrderDetailsService.isLocked=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LockTime"]];
        
        objWorkOrderDetailsService.earliestStartTime= [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTime"]];
        
        objWorkOrderDetailsService.latestStartTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTime"]];
        
        objWorkOrderDetailsService.isTaxExampt=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsTaxExempt"]];

        // WDO Pricing Approval New Changes
        objWorkOrderDetailsService.wdoWorkflowStageId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStageId"]];
        objWorkOrderDetailsService.wdoWorkflowStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStatus"]];
        objWorkOrderDetailsService.wdoWorkflowStage=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStage"]];
        objWorkOrderDetailsService.isInspectionSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsInspectionSignature"]];
        objWorkOrderDetailsService.inspectionReportSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InspectionReportSignaturePath"]];
        objWorkOrderDetailsService.wdoWorkOrderApprovalLatestDeclinednote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkOrderApprovalLatestDeclinednote"]];
        objWorkOrderDetailsService.wdoPricingApprovalLatestDeclinednote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoPricingApprovalLatestDeclinednote"]];
        objWorkOrderDetailsService.verbalApprovalBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VerbalApprovalBy"]];
        objWorkOrderDetailsService.isVerbalApproval=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsVerbalApproval"]];
        objWorkOrderDetailsService.priceChangeNote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeNote"]];
        objWorkOrderDetailsService.isPricingApprovalPending=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPricingApprovalPending"]];
        objWorkOrderDetailsService.isPricingApprovalMailSend=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPricingApprovalMailSend"]];
        objWorkOrderDetailsService.priceChangeReasonId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeReasonId"]];
        objWorkOrderDetailsService.isInspectorEditInfo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsInspectorEditInfo"]];
        objWorkOrderDetailsService.wdoWoStageDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWoStageDateTime"]];
        objWorkOrderDetailsService.priceChangeReasonOtherDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeReasonOtherDescription"]];
        
        objWorkOrderDetailsService.isBillingAddressAsSameServiceAddress = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBillingAddressAsSameServiceAddress"]];

        objWorkOrderDetailsService.tags=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tags"]];
        
        objWorkOrderDetailsService.isServiceManagerDeclined=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsServiceManagerDeclined"]];

        
        //Final Save
        NSError *error123;
        [context save:&error123];
        
    }
    
}

-(void)saveToCoreDataServiceWo :(NSArray*)arrWorkOrderExtSerDcs :(NSString*)strWoIdToCompare :(NSString*)strType
{
    global = [[Global alloc] init];
    
    NSString *strGlobalWorkOrderId;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
            for (int i=0; i<arrWorkOrderExtSerDcs.count; i++)
            {
                //============================================================================
                //============================================================================
    #pragma mark- ---------******** Work Order Detail *******----------------
                //============================================================================
                //============================================================================
                
                NSDictionary *dictWorkOrderDetail=arrWorkOrderExtSerDcs[i];
                
                
                NSDictionary *dictDataWorkOrders=[dictWorkOrderDetail valueForKey:@"WorkorderDetail"];
                
                NSString *strLeadIddd;
                if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
                {
                    strLeadIddd=@"";
                }
                else
                {
                    
                    strLeadIddd=[NSString stringWithFormat:@"%@",[dictWorkOrderDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
                    
                }
                
                if ([strType isEqualToString:@"All"]) {
                    
                    strWoIdToCompare = strLeadIddd;
                    
                }
                
                if ([strLeadIddd isEqualToString:strWoIdToCompare]) {
                    
                    // save service pest new flow data in DB
                    
                    WebService *objWebService = [[WebService alloc] init];
                    
                    [objWebService savingServicePestAllDataDBWithDictWorkOrderDetail:dictWorkOrderDetail strWoId:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]] strType:@"WithWoDetail" strToDeleteAreasDevices:@"No" ];
                    
                    // save WDO in DB
                    
                    //WebService *objWebService = [[WebService alloc] init];
                    
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                     
                        //[objWebService savingWDOAllDataDBWithDictWorkOrderDetail:dictWorkOrderDetail strWoId:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]] strType:@"WithWoDetail" strWdoLeadId:@""];
                        [objWebService savingWDOAllDataDBWithDictWorkOrderDetail:dictWorkOrderDetail strWoId:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]] strType:@"WithWoDetail" strWdoLeadId:@"" isFromServiceHistory: NO];

                        
                        NSString *strIsNPMA =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsNPMATermite"]];
                        
                        if ([strIsNPMA isEqualToString:@"1"] || [strIsNPMA isEqualToString:@"true"] || [strIsNPMA isEqualToString:@"True"]) {
                            
                            // save NPMA in DB

                            [objWebService savingNPMADataToCoreDBWithDictWorkOrderDetail:dictWorkOrderDetail strWoId:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]] strType:@""];
                            
                        }
                        
                    }
                    
                    if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
                    {
                        strGlobalWorkOrderId=@"";
                    }
                    else
                    {
                        
                        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
                        WorkOrderDetailsService *objWorkOrderDetailsService = [[WorkOrderDetailsService alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
                        
                        
                        objWorkOrderDetailsService.workorderId =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                        
                        strGlobalWorkOrderId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                        
                        objWorkOrderDetailsService.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
                        
                        objWorkOrderDetailsService.companyId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyId"]];
                        
                        objWorkOrderDetailsService.surveyID=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyID"]];
                        
                        objWorkOrderDetailsService.departmentId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentId"]];
                        
                        objWorkOrderDetailsService.accountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
                        objWorkOrderDetailsService.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ThirdPartyAccountNo"]];

                        
                        objWorkOrderDetailsService.employeeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EmployeeNo"]];
                        
                        objWorkOrderDetailsService.firstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"FirstName"]];
                        
                        objWorkOrderDetailsService.middleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"MiddleName"]];
                        
                        objWorkOrderDetailsService.lastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastName"]];
                        
                        objWorkOrderDetailsService.companyName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyName"]];
                        
                        NSString *strEmailPrimaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryEmail"]];
                        
                        if (strEmailPrimaryServer.length==0) {
                            
                            objWorkOrderDetailsService.primaryEmail=@"";
                            
                        } else {
                                                        
                            objWorkOrderDetailsService.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"PrimaryEmail"]]];
                            
                        }
                        
                        
                        NSString *strEmailSecondaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryEmail"]];
                        
                        if (strEmailSecondaryServer.length==0) {
                            
                            objWorkOrderDetailsService.secondaryEmail=@"";
                            
                        } else {
                            
                            objWorkOrderDetailsService.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"SecondaryEmail"]]];
                            
                        }
                        
                        
                        objWorkOrderDetailsService.primaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryPhone"]];
                        
                        objWorkOrderDetailsService.secondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryPhone"]];
                        
                        objWorkOrderDetailsService.scheduleOnStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
                        
                        objWorkOrderDetailsService.scheduleOnEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
                        
                        objWorkOrderDetailsService.scheduleStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]];
                        
                        objWorkOrderDetailsService.scheduleEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]];
                        
                        objWorkOrderDetailsService.serviceDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDateTime"]];
                        
                        objWorkOrderDetailsService.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalEstimationTime"]];
                        
                        objWorkOrderDetailsService.branchId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchId"]];
                        
                        objWorkOrderDetailsService.workorderStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderStatus"]];
                        
                        objWorkOrderDetailsService.atributes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Attributes"]];
                        
                        objWorkOrderDetailsService.specialInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SpecialInstruction"]];
                        
                        objWorkOrderDetailsService.serviceInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceInstruction"]];
                        
                        objWorkOrderDetailsService.direction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Direction"]];
                        
                        objWorkOrderDetailsService.otherInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OtherInstruction"]];
                        
                        objWorkOrderDetailsService.categoryName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategoryName"]];
                        
                        objWorkOrderDetailsService.subCategory=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SubCategory"]];
                        
                        objWorkOrderDetailsService.services=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Services"]];
                        
                        objWorkOrderDetailsService.currentServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CurrentServices"]];
                        
                        objWorkOrderDetailsService.lastServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastServices"]];
                        
                        objWorkOrderDetailsService.technicianComment=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianComment"]];
                        
                        objWorkOrderDetailsService.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
                        
                        objWorkOrderDetailsService.timeIn=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeIn"]];
                        
                        objWorkOrderDetailsService.timeOut=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOut"]];
                        
                        objWorkOrderDetailsService.resetId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetId"]];
                        
                        objWorkOrderDetailsService.resetDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetDescription"]];
                        
                        objWorkOrderDetailsService.audioFilePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AudioFilePath"]];
                        
                        objWorkOrderDetailsService.invoiceAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"InvoiceAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoiceAmount"]];
                        
                        objWorkOrderDetailsService.productionAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"ProductionAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProductionAmount"]];
                        
                        objWorkOrderDetailsService.previousBalance=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"PreviousBalance"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PreviousBalance"]];
                        
                        objWorkOrderDetailsService.tax=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"Tax"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tax"]];
                        
                        objWorkOrderDetailsService.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerSignaturePath"]];
                        
                        objWorkOrderDetailsService.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianSignaturePath"]];
                        
                        objWorkOrderDetailsService.electronicSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ElectronicSignaturePath"]];
                        
                        objWorkOrderDetailsService.invoicePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoicePath"]];
                        
                        objWorkOrderDetailsService.billingAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress1"]];
                        
                        objWorkOrderDetailsService.billingAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress2"]];
                        
                        objWorkOrderDetailsService.billingCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCountry"]];
                        
                        objWorkOrderDetailsService.billingState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingState"]];
                        
                        objWorkOrderDetailsService.billingCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCity"]];
                        
                        objWorkOrderDetailsService.billingZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingZipcode"]];
                        
                        objWorkOrderDetailsService.servicesAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicesAddress1"]];
                        
                        objWorkOrderDetailsService.serviceAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddress2"]];
                        
                        objWorkOrderDetailsService.serviceCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCountry"]];
                        
                        objWorkOrderDetailsService.serviceState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceState"]];
                        
                        objWorkOrderDetailsService.serviceCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCity"]];
                        
                        objWorkOrderDetailsService.serviceZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceZipcode"]];
                        
                        objWorkOrderDetailsService.serviceAddressLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLatitude"]];
                        
                        objWorkOrderDetailsService.serviceAddressLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLongitude"]];
                        
                        objWorkOrderDetailsService.isPDFGenerated=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPDFGenerated"]];
                        
                        objWorkOrderDetailsService.isMailSent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsMailSent"]];
                        
                        objWorkOrderDetailsService.isElectronicSignatureAvailable=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsElectronicSignatureAvailable"]];
                        
                        objWorkOrderDetailsService.isFail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsFail"]];
                        
                        objWorkOrderDetailsService.operateMedium=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OperateMedium"]];
                        
                        objWorkOrderDetailsService.isActive=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsActive"]];
                        
                        objWorkOrderDetailsService.surveyStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyStatus"]];
                        
                        objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                        
                        objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                        
                        objWorkOrderDetailsService.modifiedFormatedDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                        
                        objWorkOrderDetailsService.modifiedBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedBy"]];
                        
                        objWorkOrderDetailsService.userName=strUserName;
                        objWorkOrderDetailsService.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTime"]];
                        objWorkOrderDetailsService.routeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteNo"]];
                        objWorkOrderDetailsService.routeName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteName"]];
                        objWorkOrderDetailsService.targets=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Targets"]];
                        
                        objWorkOrderDetailsService.deviceVersion=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceVersion"]];
                        objWorkOrderDetailsService.deviceName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceName"]];
                        objWorkOrderDetailsService.versionNumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionNumber"]];
                        objWorkOrderDetailsService.versionDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionDate"]];
                        
                        NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
                        [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
                        [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ScheduleStartDateTime"]]];
                        objWorkOrderDetailsService.zdateScheduledStart=newTimeSchedule;
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ModifiedFormatedDate"]]];
                        objWorkOrderDetailsService.dateModified=newTime;
                        
                        objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCustomerNotPresent"]];
                        
                        objWorkOrderDetailsService.totalallowCrew=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalallowCrew"]];
                        
                        objWorkOrderDetailsService.assignCrewIds=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AssignCrewIds"]];
                        
                        objWorkOrderDetailsService.routeCrewList=[dictDataWorkOrders valueForKey:@"RouteCrewList"];
                        
                        objWorkOrderDetailsService.zSync=@"blank";
                        
                        // objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",@"false"];
                        
                        //Nilind 27 Feb
                        
                        objWorkOrderDetailsService.arrivalDuration=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ArrivalDuration"]];
                        objWorkOrderDetailsService.keyMap=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"KeyMap"]];
                        objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                        objWorkOrderDetailsService.problemDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProblemDescription"]];
                        objWorkOrderDetailsService.serviceDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDescription"]];
                        //End
                        
                        objWorkOrderDetailsService.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryServiceSysName"]];
                        objWorkOrderDetailsService.categorySysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategorySysName"]];
                        
                        NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
                        NSString *dbCompanyKey=[defsLogin valueForKey:@"companyKey"];
                        // NSString *dbUserName =[defsLogin valueForKey:@"username"];
                        
                        objWorkOrderDetailsService.dbCompanyKey=dbCompanyKey;
                        objWorkOrderDetailsService.dbUserName=strEmployeeNo;
                        
                        objWorkOrderDetailsService.noChemical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"NoChemical"]];
                        
                        objWorkOrderDetailsService.serviceJobDescriptionId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescriptionId"]];
                        
                        objWorkOrderDetailsService.serviceJobDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescription"]];
                        
                        objWorkOrderDetailsService.isEmployeePresetSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsEmployeePresetSignature"]];
                        
                        objWorkOrderDetailsService.accountDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountDescription"]];
                        
                        //DepartmentSysName  isResendInvoiceMail
                        objWorkOrderDetailsService.departmentSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                        
                        objWorkOrderDetailsService.departmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
                        
                        objWorkOrderDetailsService.isResendInvoiceMail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsResendInvoiceMail"]];
                        
                        //
                        
                        objWorkOrderDetailsService.cellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CellNo"]];
                        objWorkOrderDetailsService.billingFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingFirstName"]];
                        objWorkOrderDetailsService.billingMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMiddleName"]];
                        objWorkOrderDetailsService.billingLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingLastName"]];
                        objWorkOrderDetailsService.billingCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCellNo"]];
                        objWorkOrderDetailsService.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingPrimaryEmail"]]];
                        objWorkOrderDetailsService.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingSecondaryEmail"]]];
                        objWorkOrderDetailsService.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPrimaryPhone"]];
                        objWorkOrderDetailsService.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSecondaryPhone"]];
                        objWorkOrderDetailsService.billingMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMapCode"]];
                        objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];

                        
                        objWorkOrderDetailsService.serviceFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceFirstName"]];
                        objWorkOrderDetailsService.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMiddleName"]];
                        objWorkOrderDetailsService.serviceLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceLastName"]];
                        objWorkOrderDetailsService.serviceCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCellNo"]];
                        objWorkOrderDetailsService.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServicePrimaryEmail"]]];
                        objWorkOrderDetailsService.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServiceSecondaryEmail"]]];
                        objWorkOrderDetailsService.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePrimaryPhone"]];
                        objWorkOrderDetailsService.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSecondaryPhone"]];
                        objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
                        objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
                        objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                        objWorkOrderDetailsService.serviceAddressImagePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressImagePath"]];
                        objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];

                        //Nilind 08 Dec
                        
                        objWorkOrderDetailsService.timeInLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLatitude"]];
                        objWorkOrderDetailsService.timeInLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLongitude"]];
                        objWorkOrderDetailsService.timeOutLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLatitude"]];
                        objWorkOrderDetailsService.timeOutLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLongitude"]];
                        objWorkOrderDetailsService.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLatitude"]];
                        objWorkOrderDetailsService.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLongitude"]];
                        objWorkOrderDetailsService.termiteStateType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TermiteStateType"]];
                        //End
                        
                        objWorkOrderDetailsService.billingCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCounty"]];
                        objWorkOrderDetailsService.serviceCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCounty"]];
                        objWorkOrderDetailsService.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSchoolDistrict"]];
                        objWorkOrderDetailsService.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSchoolDistrict"]];

                        objWorkOrderDetailsService.earliestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTimeStr"]];
                        objWorkOrderDetailsService.latestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTimeStr"]];
                        objWorkOrderDetailsService.driveTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTimeStr"]];
                        
                        
                        objWorkOrderDetailsService.fromDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
                        objWorkOrderDetailsService.toDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
                        objWorkOrderDetailsService.modifyDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                        
                        NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
                        
                        if ([strIsRegularPestFlow isEqualToString:@"1"] || [strIsRegularPestFlow isEqualToString:@"True"] || [strIsRegularPestFlow isEqualToString:@"true"]) {
                            
                            objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"true"];
                            
                        } else {
                            
                            objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"false"];

                        }
                        
                        NSString *strIsRegularPestFlowOnBranch = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];

                        
                        if ([strIsRegularPestFlowOnBranch isEqualToString:@"1"] || [strIsRegularPestFlowOnBranch isEqualToString:@"True"] || [strIsRegularPestFlowOnBranch isEqualToString:@"true"]) {
                            
                            objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"true"];

                        } else {
                            
                            objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"false"];
                            
                        }
                        

                        NSString *strIsCollectPayment = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCollectPayment"]];
                        
                        if ([strIsCollectPayment isEqualToString:@"1"] || [strIsCollectPayment isEqualToString:@"True"] || [strIsCollectPayment isEqualToString:@"true"]) {
                            
                            objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"true"];
                            
                        } else {
                            
                            objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"false"];
                            
                        }
                        
                        
                        NSString *strIsWhetherShow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsWhetherShow"]];
                        
                        if ([strIsWhetherShow isEqualToString:@"1"] || [strIsWhetherShow isEqualToString:@"True"] || [strIsWhetherShow isEqualToString:@"true"]) {
                            
                            objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"true"];
                            
                        } else {
                            
                            objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"false"];
                            
                        }
                        
                        /*
                        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
                        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];
                        */
                        
                        objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];
                        
                        objWorkOrderDetailsService.relatedOpportunityNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RelatedOpportunityNo"]];

                        objWorkOrderDetailsService.taxPercent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TaxPercent"]];

                        objWorkOrderDetailsService.serviceReportFormat=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceReportFormat"]];

                        objWorkOrderDetailsService.isBatchReleased=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBatchReleased"]];

                        objWorkOrderDetailsService.accountName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountName"]];

                        objWorkOrderDetailsService.driveTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTime"]];

                        objWorkOrderDetailsService.isNPMATermite=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsNPMATermite"]];
                        
                        objWorkOrderDetailsService.branchSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchSysNameNew"]];//BranchSysName

                        objWorkOrderDetailsService.isMailSent = @"false";
                        
                        objWorkOrderDetailsService.isLocked=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LockTime"]];
                        
                        objWorkOrderDetailsService.earliestStartTime= [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTime"]];
                        
                        objWorkOrderDetailsService.latestStartTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTime"]];
                        
                        objWorkOrderDetailsService.isTaxExampt=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsTaxExempt"]];

                        // WDO Pricing Approval New Changes
                        objWorkOrderDetailsService.wdoWorkflowStageId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStageId"]];
                        objWorkOrderDetailsService.wdoWorkflowStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStatus"]];
                        objWorkOrderDetailsService.wdoWorkflowStage=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkflowStage"]];
                        objWorkOrderDetailsService.isInspectionSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsInspectionSignature"]];
                        objWorkOrderDetailsService.inspectionReportSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InspectionReportSignaturePath"]];
                        objWorkOrderDetailsService.wdoWorkOrderApprovalLatestDeclinednote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWorkOrderApprovalLatestDeclinednote"]];
                        objWorkOrderDetailsService.wdoPricingApprovalLatestDeclinednote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoPricingApprovalLatestDeclinednote"]];
                        objWorkOrderDetailsService.verbalApprovalBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VerbalApprovalBy"]];
                        objWorkOrderDetailsService.isVerbalApproval=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsVerbalApproval"]];
                        objWorkOrderDetailsService.priceChangeNote=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeNote"]];
                        objWorkOrderDetailsService.isPricingApprovalPending=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPricingApprovalPending"]];
                        objWorkOrderDetailsService.isPricingApprovalMailSend=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPricingApprovalMailSend"]];
                        objWorkOrderDetailsService.priceChangeReasonId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeReasonId"]];
                        objWorkOrderDetailsService.isInspectorEditInfo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsInspectorEditInfo"]];
                        objWorkOrderDetailsService.wdoWoStageDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WdoWoStageDateTime"]];
                        objWorkOrderDetailsService.priceChangeReasonOtherDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PriceChangeReasonOtherDescription"]];
                        
                        objWorkOrderDetailsService.isBillingAddressAsSameServiceAddress = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBillingAddressAsSameServiceAddress"]];

                        objWorkOrderDetailsService.tags=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tags"]];
                       
                        objWorkOrderDetailsService.isServiceManagerDeclined=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsServiceManagerDeclined"]];

                        objWorkOrderDetailsService.rateMasterId = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RateMasterId"]];
                        
                        
                        
                    }
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Payment Info Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
                    
                    PaymentInfoServiceAuto *objPaymentInfoServiceAuto = [[PaymentInfoServiceAuto alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
                    
                    //PaymentInfo
                    if(![[dictWorkOrderDetail valueForKey:@"PaymentInfo"] isKindOfClass:[NSDictionary class]])
                    {
                        
                    }
                    else
                    {
                        NSDictionary *dictOfPaymentInfo = [dictWorkOrderDetail valueForKey:@"PaymentInfo"];
                        objPaymentInfoServiceAuto.woPaymentId=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"WoPaymentId"]];
                        
                        objPaymentInfoServiceAuto.workorderId=strGlobalWorkOrderId;
                        
                        objPaymentInfoServiceAuto.paymentMode=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaymentMode"]];
                        
                        objPaymentInfoServiceAuto.paidAmount=[NSString stringWithFormat:@"%.02f", [[dictOfPaymentInfo valueForKey:@"PaidAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaidAmount"]];
                        
                        objPaymentInfoServiceAuto.checkNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckNo"]];
                        
                        objPaymentInfoServiceAuto.drivingLicenseNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"DrivingLicenseNo"]];
                        
                        objPaymentInfoServiceAuto.expirationDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ExpirationDate"]];
                        
                        objPaymentInfoServiceAuto.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckFrontImagePath"]];
                        
                        objPaymentInfoServiceAuto.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckBackImagePath"]];
                        
                        objPaymentInfoServiceAuto.createdDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedDate"]];
                        
                        objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedBy"]];
                        
                        objPaymentInfoServiceAuto.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedDate"]];
                        
                        objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedBy"]];
                        
                        objPaymentInfoServiceAuto.userName=strUserName;
                        
                    }
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Chemical List Save *******----------------
                    //============================================================================
                    //============================================================================
                    //WoProductDetail
                    if(![[dictWorkOrderDetail valueForKey:@"WoProductDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSDictionary *dictOfWoProductDetail = [dictWorkOrderDetail valueForKey:@"WoProductDetail"];
                        
                        NSDictionary *dictOfWoOtherProductDetail = [dictWorkOrderDetail valueForKey:@"WoOtherProductDetail"];
                        
                        entityChemicalListDetailServiceAuto=[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
                        
                        WorkorderDetailChemicalListService *objServiceAutoCompanyDetails = [[WorkorderDetailChemicalListService alloc]initWithEntity:entityChemicalListDetailServiceAuto insertIntoManagedObjectContext:context];
                        
                        objServiceAutoCompanyDetails.companyKey=strCompanyKeyy;
                        objServiceAutoCompanyDetails.userName=strUserName;
                        objServiceAutoCompanyDetails.chemicalList=dictOfWoProductDetail;
                        objServiceAutoCompanyDetails.workorderId=strGlobalWorkOrderId;
                        objServiceAutoCompanyDetails.otherChemicalList=dictOfWoOtherProductDetail;
                        
                    }
                    
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Email Id Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"EmailDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"EmailDetail"];//EmailDetail
                        for (int j=0; j<arrEmailDetail.count; j++)
                        {
                            //Email Detail Entity
                            entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
                            
                            EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                            dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                            
                            objEmailDetail.companyKey=strCompanyKeyy;
                            
                            objEmailDetail.userName=strUserName;
                            
                            objEmailDetail.workorderId=strGlobalWorkOrderId;
                            
                            objEmailDetail.woInvoiceMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WoInvoiceMailId"]];
                            
                            objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                            
                            objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                            
                            //  objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                            
                            //objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"True"])
                            {
                                objEmailDetail.isCustomerEmail=@"true";

                            }
                            else
                            {
                                objEmailDetail.isCustomerEmail=@"false";

                            }
                            
                            objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                            
                            objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                            
                            objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                            
                            objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"True"])
                            {
                                objEmailDetail.isMailSent=@"true";
                            }
                            else
                            {
                                objEmailDetail.isMailSent=@"false";
                            }
                            //IsDefaultEmail
                            if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"True"])
                            {
                                objEmailDetail.isDefaultEmail=@"true";
                            }
                            else
                            {
                                objEmailDetail.isDefaultEmail=@"false";
                            }
                            
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Image Detail Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"ImagesDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"ImagesDetail"];//EmailDetail
                        for (int j=0; j<arrImagesDetail.count; j++)
                        {
                            //Images Detail Entity
                            entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
                            
                            ImageDetailsServiceAuto *objImagesDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetailServiceAuto insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                            dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            //objImagesDetail.workorderId=strGlobalWorkOrderId;
                            objImagesDetail.workorderId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WorkorderId"]];

                            objImagesDetail.woImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageId"]];
                            
                            objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                            
                            objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                            
                            objImagesDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                            
                            objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                            
                            objImagesDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]];
                            
                            objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                            
                            objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                            
                            objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                            
                            objImagesDetail.latitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Latitude"]];
                            
                            objImagesDetail.longitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Longitude"]];
                            
                            if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"True"])
                            {
                                objImagesDetail.isProblemIdentifaction=@"true";
                            }
                            else
                            {
                                objImagesDetail.isProblemIdentifaction=@"false";
                            }
                            
                            objImagesDetail.graphXmlPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"GraphXmlPath"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Termite Image Detail Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"TexasTermiteImagesDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"TexasTermiteImagesDetail"];//EmailDetail
                        for (int j=0; j<arrImagesDetail.count; j++)
                        {
                            //Images Detail Entity
                            entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
                            
                            ImageDetailsTermite *objImagesDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                            dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            objImagesDetail.workorderId=strGlobalWorkOrderId;
                            
                            objImagesDetail.woTexasTermiteImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoTexasTermiteImageId"]];
                            
                            objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                            
                            objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                            
                            objImagesDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                            
                            objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                            
                            objImagesDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]];
                            
                            objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                            
                            objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                            
                            objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                            
                            objImagesDetail.latitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Latitude"]];
                            
                            objImagesDetail.longitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Longitude"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    
                    
                    // Wo Other Document Saving
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- --------- Wo Other Document  Detail Save ----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"WoOtherDocuments"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"WoOtherDocuments"];//WoOtherDocuments
                        for (int j=0; j<arrImagesDetail.count; j++)
                        {
                            //Images Detail Entity
                            entityWorkOrderDocuments=[NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
                            
                            WoOtherDocuments *objImagesDetail = [[WoOtherDocuments alloc]initWithEntity:entityWorkOrderDocuments insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                            dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            objImagesDetail.workorderId=strGlobalWorkOrderId;
                            
                            objImagesDetail.woOtherDocumentId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoOtherDocumentId"]];
                            
                            objImagesDetail.title=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Title"]];
                            
                            objImagesDetail.otherDocumentPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"OtherDocumentPath"]];
                            
                            
                            //IsDefaultEmail
                            if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsDefault"]] isEqualToString:@"1"])
                            {
                                objImagesDetail.isDefault=@"true";
                            }
                            else
                            {
                                objImagesDetail.isDefault=@"false";
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsChecked"]] isEqualToString:@"1"])
                            {
                                objImagesDetail.isChecked=@"true";
                            }
                            else
                            {
                                objImagesDetail.isChecked=@"false";
                            }
                            
                            if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsActive"]] isEqualToString:@"1"])
                            {
                                objImagesDetail.isActive=@"true";
                            }
                            else
                            {
                                objImagesDetail.isActive=@"false";
                            }
                            
                            objImagesDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"OtherDocSysName"]];
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                    }
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Equipment Detail Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Equipment Save  WoEquipmentDetail
                    if(![[dictWorkOrderDetail valueForKey:@"WoEquipmentDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        //                    NSDictionary *dictOfEquipmentList=[dictWorkOrderDetail valueForKey:@"WOEquipmentList"];
                        //                    if(![[dictOfEquipmentList valueForKey:@"WOEquipmentDcs"] isKindOfClass:[NSArray class]])
                        //                    {
                        //
                        //                    }
                        //                    else
                        //                    {
                        
                        NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"WoEquipmentDetail"];//EmailDetail
                        for (int j=0; j<arrImagesDetail.count; j++)
                        {
                            //Equipment Detail Entity
                            entityWOEquipmentServiceAuto=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
                            
                            WorkOrderEquipmentDetails *objImagesDetail = [[WorkOrderEquipmentDetails alloc]initWithEntity:entityWOEquipmentServiceAuto insertIntoManagedObjectContext:context];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            objImagesDetail.workorderId=strGlobalWorkOrderId;
                            
                            objImagesDetail.arrOfEquipmentList=arrImagesDetail;
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                        //}
                    }
                    
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** TexasTermiteServiceDetail Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Equipment Save  WoEquipmentDetail
                    if(![[dictWorkOrderDetail valueForKey:@"TexasTermiteServiceDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        
                        //Changes To Save Termite Static form for texas
                        
                        NSArray *arrTermiteTexas=[dictWorkOrderDetail valueForKey:@"TexasTermiteServiceDetail"];//EmailDetail
                        for (int j=0; j<arrTermiteTexas.count; j++)
                        {
                            //Equipment Detail Entity
                            entityTermiteTexas=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context];
                            
                            TexasTermiteServiceDetail *objImagesDetail = [[TexasTermiteServiceDetail alloc]initWithEntity:entityTermiteTexas insertIntoManagedObjectContext:context];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            objImagesDetail.workorderId=strGlobalWorkOrderId;
                            
                            NSDictionary *dictTermiteTexas;
                            
                            if (!(arrTermiteTexas.count==0)) {
                                
                                dictTermiteTexas=arrTermiteTexas[0];
                                
                            }else{
                                
                                dictTermiteTexas=nil;
                                
                            }
                            
                            objImagesDetail.texasTermiteServiceDetail=dictTermiteTexas;
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    //============================================================================
    #pragma mark- ---------******** FloridaTermiteServiceDetail Save *******----------------
                    //============================================================================
                    //
                    if(![[dictWorkOrderDetail valueForKey:@"FloridaTermiteServiceDetail"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        
                        //Changes To Save Termite Static form for texas
                        
                        NSArray *arrTermiteFlorida=[dictWorkOrderDetail valueForKey:@"FloridaTermiteServiceDetail"];//EmailDetail
                        for (int j=0; j<arrTermiteFlorida.count; j++)
                        {
                            //Equipment Detail Entity
                            entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
                            
                            FloridaTermiteServiceDetail *objImagesDetail = [[FloridaTermiteServiceDetail alloc]initWithEntity:entityTermiteFlorida insertIntoManagedObjectContext:context];
                            
                            objImagesDetail.companyKey=strCompanyKeyy;
                            
                            objImagesDetail.userName=strUserName;
                            
                            objImagesDetail.workorderId=strGlobalWorkOrderId;
                            
                            NSDictionary *dictTermiteTexas;
                            
                            if (!(arrTermiteFlorida.count==0)) {
                                
                                dictTermiteTexas=arrTermiteFlorida[0];
                                
                            }else{
                                
                                dictTermiteTexas=nil;
                                
                            }
                            
                            objImagesDetail.floridaTermiteServiceDetail=dictTermiteTexas;
                            
                            NSError *error1;
                            [context save:&error1];
                        }
                        
                    }
                    
                    
                    //ServiceAutoModifyDate
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Modify Date Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
                    
                    ServiceAutoModifyDate *objServiceAutoCompanyDetails = [[ServiceAutoModifyDate alloc]initWithEntity:entityModifyDateServiceAuto insertIntoManagedObjectContext:context];
                    
                    objServiceAutoCompanyDetails.companyKey=strCompanyKeyy;
                    objServiceAutoCompanyDetails.userName=strUserName;
                    objServiceAutoCompanyDetails.workorderId=strGlobalWorkOrderId;
                    
                    NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                    [inputDateFormatter setTimeZone:inputTimeZone];
                    [inputDateFormatter setDateFormat:dateFormat];
                    
                    NSString *inputString = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                    NSDate *date = [inputDateFormatter dateFromString:inputString];
                    
                    NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                    [outputDateFormatter setTimeZone:outputTimeZone];
                    [outputDateFormatter setDateFormat:dateFormat];
                    NSString *outputString = [outputDateFormatter stringFromDate:date];
                    
                    objServiceAutoCompanyDetails.modifyDate=outputString;//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];;
                    
                    //Final Save
                    NSError *error;
                    [context save:&error];
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Service Auto ServiceAddressPOCDetailDcs Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                        for (int j=0; j<arrEmailDetail.count; j++)
                        {
                            //AccountItemHistoryExtSerDcs Detail Entity
                            entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
                            
                            ServiceAddressPOCDetailDcs *objEmailDetail = [[ServiceAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalServiceAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                            dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                            
                            objEmailDetail.companyKey=strCompanyKeyy;
                            objEmailDetail.userName=strUserName;
                            objEmailDetail.workorderId=strGlobalWorkOrderId;
                            objEmailDetail.pocDetails=arrEmailDetail;
                            
                            NSError *error2222;
                            [context save:&error2222];
                        }
                    }
                    
                    
                    //============================================================================
                    //============================================================================
    #pragma mark- ---------******** Service Auto BillingAddressPOCDetailDcs Save *******----------------
                    //============================================================================
                    //============================================================================
                    
                    //Email Id Save
                    if(![[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                    {
                        
                    }
                    else
                    {
                        
                        NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                        for (int j=0; j<arrEmailDetail.count; j++)
                        {
                            //AccountItemHistoryExtSerDcs Detail Entity
                            entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
                            
                            BillingAddressPOCDetailDcs *objEmailDetail = [[BillingAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalBillingAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                            NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                            dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                            
                            objEmailDetail.companyKey=strCompanyKeyy;
                            objEmailDetail.userName=strUserName;
                            objEmailDetail.workorderId=strGlobalWorkOrderId;
                            objEmailDetail.pocDetails=arrEmailDetail;
                            
                            NSError *error22222;
                            [context save:&error22222];
                        }
                    }
                    
                    //Final Save
                    NSError *error123;
                    [context save:&error123];

                }
            }
    
}


-(void)saveToCoreDataPlumbingWo :(NSArray*)arrWorkOrderExtSerDcs :(NSString*)strWoIdToCompare :(NSString*)strType
{
    global = [[Global alloc] init];
    
    NSString *strGlobalWorkOrderId;
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
            
    for (int i=0; i<arrWorkOrderExtSerDcs.count; i++)
    {
        
        //============================================================================
        //============================================================================
#pragma mark- --------- Sub---Work-----Order----Save In Db ----------------
        //============================================================================
        //============================================================================
        
        NSDictionary *dictWorkOrderDetail=arrWorkOrderExtSerDcs[i];
        
        NSDictionary *dictDataWorkOrders=[dictWorkOrderDetail valueForKey:@"WorkorderDetail"];
        
        NSString *strDepartmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
        
        NSString *strLeadIddd,*strAccountNoMechanical;
        
        if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
        {
            strLeadIddd=@"";
        }
        else
        {
            
            strLeadIddd=[NSString stringWithFormat:@"%@",[dictWorkOrderDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
            
        }

        if ([strType isEqualToString:@"All"]) {
            
            strWoIdToCompare = strLeadIddd;
            
        }
        
        if ([strLeadIddd isEqualToString:strWoIdToCompare]) {
            
            if ([strDepartmentType isEqualToString:@"Mechanical"]) {
                
                
                if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
                {
                    strGlobalWorkOrderId=@"";
                }
                else
                {
                    
                    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
                    WorkOrderDetailsService *objWorkOrderDetailsService = [[WorkOrderDetailsService alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
                    
                    
                    objWorkOrderDetailsService.workorderId =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                    objWorkOrderDetailsService.departmentSysName =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                    
                    strGlobalWorkOrderId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                    
                    strAccountNoMechanical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
                    
                    objWorkOrderDetailsService.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
                    
                    objWorkOrderDetailsService.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ThirdPartyAccountNo"]];
                    
                    objWorkOrderDetailsService.companyKey=strCompanyKeyy;
                    
                    objWorkOrderDetailsService.companyId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyId"]];
                    
                    objWorkOrderDetailsService.surveyID=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyID"]];
                    
                    objWorkOrderDetailsService.departmentId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentId"]];
                    
                    objWorkOrderDetailsService.accountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
                    
                    objWorkOrderDetailsService.employeeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EmployeeNo"]];
                    
                    objWorkOrderDetailsService.firstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"FirstName"]];
                    
                    objWorkOrderDetailsService.middleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"MiddleName"]];
                    
                    objWorkOrderDetailsService.lastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastName"]];
                    
                    objWorkOrderDetailsService.companyName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyName"]];
                    
                    NSString *strEmailPrimaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryEmail"]];
                    
                    if (strEmailPrimaryServer.length==0) {
                        
                        objWorkOrderDetailsService.primaryEmail=@"";
                        
                    } else {
                        
                        objWorkOrderDetailsService.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"PrimaryEmail"]]];
                        
                    }
                    
                    
                    NSString *strEmailSecondaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryEmail"]];
                    
                    if (strEmailSecondaryServer.length==0) {
                        
                        objWorkOrderDetailsService.secondaryEmail=@"";
                        
                    } else {
                        
                        objWorkOrderDetailsService.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"SecondaryEmail"]]];
                        
                    }
                    
                    
                    objWorkOrderDetailsService.primaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryPhone"]];
                    
                    objWorkOrderDetailsService.secondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryPhone"]];
                    
                    objWorkOrderDetailsService.scheduleOnStartDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]]];
                    
                    objWorkOrderDetailsService.scheduleOnEndDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]]];
                    
                    objWorkOrderDetailsService.scheduleStartDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]]];
                    
                    objWorkOrderDetailsService.scheduleEndDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]]];
                    
                    objWorkOrderDetailsService.serviceDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDateTime"]]];
                    
                    objWorkOrderDetailsService.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalEstimationTime"]];
                    
                    objWorkOrderDetailsService.branchId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchId"]];
                    
                    objWorkOrderDetailsService.workorderStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderStatus"]];
                    
                    objWorkOrderDetailsService.atributes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Attributes"]];
                    
                    objWorkOrderDetailsService.specialInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SpecialInstruction"]];
                    
                    objWorkOrderDetailsService.serviceInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceInstruction"]];
                    
                    objWorkOrderDetailsService.direction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Direction"]];
                    
                    objWorkOrderDetailsService.otherInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OtherInstruction"]];
                    
                    objWorkOrderDetailsService.categoryName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategoryName"]];
                    
                    objWorkOrderDetailsService.subCategory=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SubCategory"]];
                    
                    objWorkOrderDetailsService.services=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Services"]];
                    
                    objWorkOrderDetailsService.currentServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CurrentServices"]];
                    
                    objWorkOrderDetailsService.lastServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastServices"]];
                    
                    objWorkOrderDetailsService.technicianComment=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianComment"]];
                    
                    objWorkOrderDetailsService.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
                    
                    objWorkOrderDetailsService.timeIn=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeIn"]];
                    
                    objWorkOrderDetailsService.timeOut=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOut"]];
                    
                    objWorkOrderDetailsService.resetId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetId"]];
                    
                    objWorkOrderDetailsService.resetDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetDescription"]];
                    
                    objWorkOrderDetailsService.audioFilePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AudioFilePath"]];
                    
                    objWorkOrderDetailsService.invoiceAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"InvoiceAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoiceAmount"]];
                    
                    objWorkOrderDetailsService.productionAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"ProductionAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProductionAmount"]];
                    
                    objWorkOrderDetailsService.previousBalance=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"PreviousBalance"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PreviousBalance"]];
                    
                    objWorkOrderDetailsService.tax=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"Tax"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tax"]];
                    
                    objWorkOrderDetailsService.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerSignaturePath"]];
                    
                    objWorkOrderDetailsService.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianSignaturePath"]];
                    
                    objWorkOrderDetailsService.electronicSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ElectronicSignaturePath"]];
                    
                    objWorkOrderDetailsService.invoicePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoicePath"]];
                    
                    objWorkOrderDetailsService.billingAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress1"]];
                    
                    objWorkOrderDetailsService.billingAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress2"]];
                    
                    objWorkOrderDetailsService.billingCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCountry"]];
                    
                    objWorkOrderDetailsService.billingState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingState"]];
                    
                    objWorkOrderDetailsService.billingCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCity"]];
                    
                    objWorkOrderDetailsService.billingZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingZipcode"]];
                    
                    objWorkOrderDetailsService.servicesAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicesAddress1"]];
                    
                    objWorkOrderDetailsService.serviceAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddress2"]];
                    
                    objWorkOrderDetailsService.serviceCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCountry"]];
                    
                    objWorkOrderDetailsService.serviceState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceState"]];
                    
                    objWorkOrderDetailsService.serviceCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCity"]];
                    
                    objWorkOrderDetailsService.serviceZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceZipcode"]];
                    
                    objWorkOrderDetailsService.serviceAddressLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLatitude"]];
                    
                    objWorkOrderDetailsService.serviceAddressLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLongitude"]];
                    
                    objWorkOrderDetailsService.isPDFGenerated=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPDFGenerated"]]];
                    
                    objWorkOrderDetailsService.isMailSent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsMailSent"]]];
                    
                    objWorkOrderDetailsService.isElectronicSignatureAvailable=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsElectronicSignatureAvailable"]];
                    
                    objWorkOrderDetailsService.isFail=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsFail"]]];
                    
                    objWorkOrderDetailsService.operateMedium=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OperateMedium"]];
                    
                    objWorkOrderDetailsService.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsActive"]]];
                    
                    objWorkOrderDetailsService.surveyStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyStatus"]];
                    
                    objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                    
                    objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                    
                    objWorkOrderDetailsService.modifiedFormatedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]]];
                    
                    objWorkOrderDetailsService.modifiedBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedBy"]];
                    
                    objWorkOrderDetailsService.userName=strUserName;
                    objWorkOrderDetailsService.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTime"]];
                    objWorkOrderDetailsService.routeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteNo"]];
                    objWorkOrderDetailsService.routeName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteName"]];
                    objWorkOrderDetailsService.targets=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Targets"]];
                    
                    objWorkOrderDetailsService.deviceVersion=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceVersion"]];
                    objWorkOrderDetailsService.deviceName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceName"]];
                    objWorkOrderDetailsService.versionNumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionNumber"]];
                    objWorkOrderDetailsService.versionDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionDate"]];
                    
                    NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
                    [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ScheduleStartDateTime"]]];
                    objWorkOrderDetailsService.zdateScheduledStart=newTimeSchedule;
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ModifiedFormatedDate"]]];
                    objWorkOrderDetailsService.dateModified=newTime;

                    objWorkOrderDetailsService.isCustomerNotPresent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCustomerNotPresent"]]];
                    
                    objWorkOrderDetailsService.totalallowCrew=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalallowCrew"]];
                    
                    objWorkOrderDetailsService.assignCrewIds=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AssignCrewIds"]];
                    
                    objWorkOrderDetailsService.routeCrewList=[dictDataWorkOrders valueForKey:@"RouteCrewList"];
                    
                    objWorkOrderDetailsService.zSync=@"blank";
                    
                    // objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",@"false"];
                    
                    //Nilind 27 Feb
                    
                    objWorkOrderDetailsService.arrivalDuration=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ArrivalDuration"]];
                    objWorkOrderDetailsService.keyMap=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"KeyMap"]];
                    objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                    objWorkOrderDetailsService.problemDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProblemDescription"]];
                    objWorkOrderDetailsService.serviceDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDescription"]];
                    //End
                    
                    objWorkOrderDetailsService.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryServiceSysName"]];
                    objWorkOrderDetailsService.categorySysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategorySysName"]];
                    
                    NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
                    NSString *dbCompanyKey=[defsLogin valueForKey:@"companyKey"];
                    // NSString *dbUserName =[defsLogin valueForKey:@"username"];
                    
                    objWorkOrderDetailsService.dbCompanyKey=dbCompanyKey;
                    objWorkOrderDetailsService.dbUserName=strEmployeeNo;
                    
                    objWorkOrderDetailsService.noChemical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"NoChemical"]];
                    
                    objWorkOrderDetailsService.serviceJobDescriptionId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescriptionId"]];
                    
                    objWorkOrderDetailsService.serviceJobDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescription"]];
                    
                    objWorkOrderDetailsService.isEmployeePresetSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsEmployeePresetSignature"]];
                    
                    objWorkOrderDetailsService.departmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
                    
                    objWorkOrderDetailsService.tags=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tags"]];
                    
                    objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
                    
                    objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
                    
                    objWorkOrderDetailsService.accountDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountDescription"]];
                    
                    objWorkOrderDetailsService.isClientApprovalReq=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsClientApprovalReq"]];
                    
                    //DepartmentSysName
                    objWorkOrderDetailsService.departmentSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                    
                    objWorkOrderDetailsService.isResendInvoiceMail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsResendInvoiceMail"]];

                    //Nilind 28 Dec
                    objWorkOrderDetailsService.cellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CellNo"]];
                    objWorkOrderDetailsService.billingFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingFirstName"]];
                    objWorkOrderDetailsService.billingMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMiddleName"]];
                    objWorkOrderDetailsService.billingLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingLastName"]];
                    objWorkOrderDetailsService.billingCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCellNo"]];
                    objWorkOrderDetailsService.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingPrimaryEmail"]]];
                    objWorkOrderDetailsService.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingSecondaryEmail"]]];
                    objWorkOrderDetailsService.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPrimaryPhone"]];
                    objWorkOrderDetailsService.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSecondaryPhone"]];
                    objWorkOrderDetailsService.billingMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMapCode"]];
                    objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];
                    
                    
                    objWorkOrderDetailsService.serviceFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceFirstName"]];
                    objWorkOrderDetailsService.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMiddleName"]];
                    objWorkOrderDetailsService.serviceLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceLastName"]];
                    objWorkOrderDetailsService.serviceCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCellNo"]];
                    objWorkOrderDetailsService.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServicePrimaryEmail"]]];
                    objWorkOrderDetailsService.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServiceSecondaryEmail"]]];
                    objWorkOrderDetailsService.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePrimaryPhone"]];
                    objWorkOrderDetailsService.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSecondaryPhone"]];
                    objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
                    objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
                    objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                    objWorkOrderDetailsService.serviceAddressImagePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressImagePath"]];
                    objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];
                    
                    //Nilind 08 Dec
                    
                    objWorkOrderDetailsService.timeInLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLatitude"]];
                    objWorkOrderDetailsService.timeInLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLongitude"]];
                    objWorkOrderDetailsService.timeOutLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLatitude"]];
                    objWorkOrderDetailsService.timeOutLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLongitude"]];
                    objWorkOrderDetailsService.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLatitude"]];
                    objWorkOrderDetailsService.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLongitude"]];
                    
                    //End
                    
                    objWorkOrderDetailsService.addressSubType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AddressSubType"]];
                    objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];
                    objWorkOrderDetailsService.customerPONumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerPONumber"]];
                    objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];
                    objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];
                    
                    objWorkOrderDetailsService.billingCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCounty"]];
                    objWorkOrderDetailsService.serviceCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCounty"]];
                    objWorkOrderDetailsService.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSchoolDistrict"]];
                    objWorkOrderDetailsService.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSchoolDistrict"]];
                    
                    objWorkOrderDetailsService.earliestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTimeStr"]];
                    objWorkOrderDetailsService.latestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTimeStr"]];
                    objWorkOrderDetailsService.driveTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTimeStr"]];
                    
                    
                    NSString *strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]];

                    objWorkOrderDetailsService.fromDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];
                    
                    strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]];

                    objWorkOrderDetailsService.toDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];
                    
                    strDateee=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];

                    objWorkOrderDetailsService.modifyDate=[NSString stringWithFormat:@"%@",[global strBlockDateTime:strDateee]];

                    NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
                    
                    if ([strIsRegularPestFlow isEqualToString:@"1"] || [strIsRegularPestFlow isEqualToString:@"True"] || [strIsRegularPestFlow isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    NSString *strIsRegularPestFlowOnBranch = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];
                    
                    
                    if ([strIsRegularPestFlowOnBranch isEqualToString:@"1"] || [strIsRegularPestFlowOnBranch isEqualToString:@"True"] || [strIsRegularPestFlowOnBranch isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    
                    objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];

                    NSString *strIsCollectPayment = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCollectPayment"]];
                    
                    if ([strIsCollectPayment isEqualToString:@"1"] || [strIsCollectPayment isEqualToString:@"True"] || [strIsCollectPayment isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    NSString *strIsWhetherShow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsWhetherShow"]];
                    
                    if ([strIsWhetherShow isEqualToString:@"1"] || [strIsWhetherShow isEqualToString:@"True"] || [strIsWhetherShow isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    objWorkOrderDetailsService.relatedOpportunityNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RelatedOpportunityNo"]];
                    
                    objWorkOrderDetailsService.taxPercent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TaxPercent"]];

                    objWorkOrderDetailsService.serviceReportFormat=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceReportFormat"]];

                    objWorkOrderDetailsService.isBatchReleased=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBatchReleased"]];

                    objWorkOrderDetailsService.accountName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountName"]];
                    objWorkOrderDetailsService.driveTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTime"]];
                    objWorkOrderDetailsService.isNPMATermite=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsNPMATermite"]];
                    objWorkOrderDetailsService.branchSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchSysNameNew"]];//BranchSysName

                    objWorkOrderDetailsService.isMailSent = @"false";

                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------********Mechanical Email Id Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"EmailDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"EmailDetail"];//EmailDetail
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //Email Detail Entity
                        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
                        
                        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        
                        objEmailDetail.userName=strUserName;
                        
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        
                        objEmailDetail.woInvoiceMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WoInvoiceMailId"]];
                        
                        objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                        
                        objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                        
                        //  objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                        
                        objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                        
                        objEmailDetail.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]]];
                        
                        objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                        
                        objEmailDetail.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]]];
                        
                        objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"])
                        {
                            objEmailDetail.isMailSent=@"true";
                        }
                        else
                        {
                            objEmailDetail.isMailSent=@"false";
                        }
                        //IsDefaultEmail
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"])
                        {
                            objEmailDetail.isDefaultEmail=@"true";
                        }
                        else
                        {
                            objEmailDetail.isDefaultEmail=@"false";
                        }
                        
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsInvoiceMailSent"]] isEqualToString:@"1"])
                        {
                            objEmailDetail.isInvoiceMailSent=@"true";
                        }
                        else
                        {
                            objEmailDetail.isInvoiceMailSent=@"false";
                        }
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Mechanical Image Detail Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"ImagesDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"ImagesDetail"];//EmailDetail
                    for (int j=0; j<arrImagesDetail.count; j++)
                    {
                        //Images Detail Entity
                        entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
                        
                        ImageDetailsServiceAuto *objImagesDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetailServiceAuto insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                        dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        objImagesDetail.woImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageId"]];
                        
                        objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                        
                        objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                        
                        objImagesDetail.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]]];
                        
                        objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                        
                        objImagesDetail.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]]];
                        
                        objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                        
                        objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                        
                        objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                        
                        objImagesDetail.latitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Latitude"]];
                        
                        objImagesDetail.longitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Longitude"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"True"])
                        {
                            objImagesDetail.isProblemIdentifaction=@"true";
                        }
                        else
                        {
                            objImagesDetail.isProblemIdentifaction=@"false";
                        }
                        
                        objImagesDetail.graphXmlPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"GraphXmlPath"]];
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ------------------ Sub Work Order Save --------------------------
                //============================================================================
                //============================================================================
                
                //                entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
                //
                //                MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
                
                //SubWorkOrder
                
                NSString *strSubWorkOrderIdTempToSave;
                
                if(![[dictWorkOrderDetail valueForKey:@"SubWorkOrder"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    NSArray *arrOfSubWorkOrderFromServer=[dictWorkOrderDetail valueForKey:@"SubWorkOrder"];
                    
                    for (int k=0; k<arrOfSubWorkOrderFromServer.count; k++) {
                        
                        entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
                        
                        MechanicalSubWorkOrder *objSubWorkOrder = [[MechanicalSubWorkOrder alloc]initWithEntity:entityMechanicalSubWorkOrder insertIntoManagedObjectContext:context];
                        
                        NSDictionary *dictOfSubWorkOrderInfo = arrOfSubWorkOrderFromServer[k];
                        
                        objSubWorkOrder.workorderId=strGlobalWorkOrderId;
                        objSubWorkOrder.departmentSysName =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                        objSubWorkOrder.accountNo=strAccountNoMechanical;
                        objSubWorkOrder.actualTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTime"]];
                        objSubWorkOrder.amtPaid=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AmtPaid"]];
                        //Comment becoz company key was getting null from web
                        //objSubWorkOrder.companyKey=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompanyKey"]];
                        objSubWorkOrder.companyKey=strCompanyKeyy;
                        objSubWorkOrder.completeSWO_CustSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_CustSignPath"]];
                        objSubWorkOrder.completeSWO_IsCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_IsCustomerNotPresent"]];
                        objSubWorkOrder.completeSWO_TechSignPath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CompleteSWO_TechSignPath"]];
                        objSubWorkOrder.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedBy"]];
                        objSubWorkOrder.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedDate"]]];
                        objSubWorkOrder.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CustomerSignaturePath"]];
                        objSubWorkOrder.diagnosticChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiagnosticChargeMasterId"]];
                        objSubWorkOrder.discountAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiscountAmt"]];
                        objSubWorkOrder.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"EmployeeNo"]];
                        objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
                        objSubWorkOrder.invoicePayType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoicePayType"]];
                        objSubWorkOrder.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsActive"]]];
                        objSubWorkOrder.isClientApprovalReq=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApprovalReq"]]];
                        objSubWorkOrder.isClientApproved=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsClientApproved"]];
                        objSubWorkOrder.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCompleted"]]];
                        objSubWorkOrder.isCustomerNotPresent=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCustomerNotPresent"]]];
                        objSubWorkOrder.isStdPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsStdPrice"]]];
                        objSubWorkOrder.jobDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"JobDesc"]];
                        objSubWorkOrder.laborPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborPercent"]];
                        objSubWorkOrder.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedBy"]];
                        objSubWorkOrder.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ModifiedDate"]]];
                        objSubWorkOrder.otherDiagnosticChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherDiagnosticChargesAmt"]];
                        objSubWorkOrder.otherTripChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"OtherTripChargesAmt"]];
                        objSubWorkOrder.partPercent=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPercent"]];
                        objSubWorkOrder.priceNotToExceed=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PriceNotToExceed"]];
                        //PriceNotToExceed
                        objSubWorkOrder.serviceDateTime=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceDateTime"]]];
                        objSubWorkOrder.serviceStartEndTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartEndTime"]];
                        objSubWorkOrder.serviceStartStartTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ServiceStartStartTime"]];
                        objSubWorkOrder.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                        
                        strSubWorkOrderIdTempToSave=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                        
                        objSubWorkOrder.subWorkOrderIssueId=@"";
                        objSubWorkOrder.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNo"]];
                        objSubWorkOrder.subWOStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOStatus"]];
                        objSubWorkOrder.subWOType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
                        objSubWorkOrder.taxAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TaxAmt"]];
                        objSubWorkOrder.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TechnicianSignaturePath"]];
                        objSubWorkOrder.totalApprovedAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalApprovedAmt"]];
                        objSubWorkOrder.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTime"]];
                        objSubWorkOrder.totalEstimationTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalEstimationTimeInt"]];
                        objSubWorkOrder.tripChargeMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargeMasterId"]];
                        objSubWorkOrder.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CreatedByDevice"]];
                        
                        objSubWorkOrder.pSPDiscount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPDiscount"]];
                        
                        objSubWorkOrder.pSPCharges=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPCharges"]];
                        
                        objSubWorkOrder.invoiceAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"InvoiceAmt"]];
                        
                        objSubWorkOrder.accountPSPId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AccountPSPId"]];
                        objSubWorkOrder.pSPMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PSPMasterId"]];
                        
                        
                        objSubWorkOrder.inspectionresults=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"Inspectionresults"]];
                        
                        
                        //Changes For New Sub Work Order
                        objSubWorkOrder.tripChargeName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargeName"]];
                        objSubWorkOrder.diagnosticChargeName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"DiagnosticChargeName"]];
                        objSubWorkOrder.isIncludeDetailOnInvoice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsIncludeDetailOnInvoice"]];
                        objSubWorkOrder.audioFilePath=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AudioFilePath"]];
                        objSubWorkOrder.propertyType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PropertyType"]];
                        objSubWorkOrder.addressType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AddressType"]];
                        objSubWorkOrder.isLaborTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsLaborTaxable"]];
                        objSubWorkOrder.isPartTaxable=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsPartTaxable"]];
                        objSubWorkOrder.partPriceType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"PartPriceType"]];
                        objSubWorkOrder.isVendorPayTax=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsVendorPayTax"]];
                        objSubWorkOrder.isChargeTaxOnFullAmount=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsChargeTaxOnFullAmount"]];
                        objSubWorkOrder.afterHrsDuration=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"AfterHrsDuration"]];
                        objSubWorkOrder.isHolidayHrs=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsHolidayHrs"]];
                        objSubWorkOrder.laborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborHrsPrice"]];
                        objSubWorkOrder.mileageChargesName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesName"]];
                        objSubWorkOrder.totalMileage=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TotalMileage"]];
                        objSubWorkOrder.mileageChargesAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MileageChargesAmt"]];
                        objSubWorkOrder.miscChargeAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"MiscChargeAmt"]];
                        objSubWorkOrder.actualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualTimeInt"]];
                        
                        objSubWorkOrder.isCreateWOforInCompleteIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsCreateWOforInCompleteIssue"]];
                        objSubWorkOrder.isSendDocWithOutSign=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"IsSendDocWithOutSign"]];
                        objSubWorkOrder.calculatedActualTimeInt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"CalculatedActualTimeInt"]];
                        objSubWorkOrder.actualLaborHrsPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ActualLaborHrsPrice"]];
                        objSubWorkOrder.clockStatus=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"ClockStatus"]];
                        
                        // Saving WorkorderNo in SubWorkOrder DB
                        objSubWorkOrder.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
                        objSubWorkOrder.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
                        objSubWorkOrder.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"LaborType"]];
                        objSubWorkOrder.tripChargesQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"TripChargesQty"]];

                        //============================================================================
                        //============================================================================
#pragma mark- ------------------ Sub Work Order Issues Save --------------------------
                        //============================================================================
                        //============================================================================
                        
                        //                        entityMechanicalSubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
                        //
                        //                        MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssues insertIntoManagedObjectContext:context];
                        
                        //SubWorkOrderIssues
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderIssueDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            NSArray *arrOfSubWorkOrderIssuesFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderIssueDcs"];
                            
                            for (int k1=0; k1<arrOfSubWorkOrderIssuesFromServer.count; k1++) {
                                
                                entityMechanicalSubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
                                
                                MechanicalSubWorkOrderIssueDcs *objSubWorkOrderIssues = [[MechanicalSubWorkOrderIssueDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssues insertIntoManagedObjectContext:context];
                                
                                NSDictionary *dictOfSubWorkOrderIssuesInfo = arrOfSubWorkOrderIssuesFromServer[k1];
                                
                                objSubWorkOrderIssues.workorderId=strGlobalWorkOrderId;
                                objSubWorkOrderIssues.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                objSubWorkOrderIssues.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                objSubWorkOrderIssues.serviceIssue=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ServiceIssue"]];
                                objSubWorkOrderIssues.priority=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Priority"]];
                                objSubWorkOrderIssues.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"IsActive"]]];
                                objSubWorkOrderIssues.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedBy"]];
                                objSubWorkOrderIssues.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedDate"]]];
                                objSubWorkOrderIssues.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedBy"]];
                                objSubWorkOrderIssues.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModifiedDate"]]];
                                objSubWorkOrderIssues.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"CreatedByDevice"]];
                                objSubWorkOrderIssues.equipmentCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCode"]];
                                objSubWorkOrderIssues.equipmentName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentName"]];
                                objSubWorkOrderIssues.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SerialNumber"]];
                                objSubWorkOrderIssues.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"ModelNumber"]];
                                objSubWorkOrderIssues.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"Manufacturer"]];
                                objSubWorkOrderIssues.equipmentCustomName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentCustomName"]];
                                objSubWorkOrderIssues.equipmentNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"EquipmentNo"]];
                                
                                NSError *error2;
                                [context save:&error2];
                                
                                NSString *strWOTYPE=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWOType"]];
                                
                                if ([strWOTYPE isEqualToString:@"TM"]) {
                                    
                                    
                                    //Yaha Parr Issues Repairs parts Save karna hai
                                    
                                    //============================================================================
                                    //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Parts Time And Material Save --------------------------
                                    //============================================================================
                                    //============================================================================
                                    
                                    //                                    entityMechanicalSubWorkOrderIssuesRepairsParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                    //
                                    //                                    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsParts insertIntoManagedObjectContext:context];
                                    
                                    //SubWorkOrderIssuesRepairsParts
                                    if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairPartDcs"];
                                        
                                        for (int k2=0; k2<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k2++) {
                                            
                                            entityMechanicalSubWorkOrderIssuesRepairsParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                            
                                            MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsParts insertIntoManagedObjectContext:context];
                                            
                                            NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k2];
                                            
                                            objSubWorkOrderIssuesRepairsParts.workorderId=strGlobalWorkOrderId;
                                            objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                            objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];;
                                            objSubWorkOrderIssuesRepairsParts.issueRepairId=@"";//[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                            objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                            objSubWorkOrderIssuesRepairsParts.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                            objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                            objSubWorkOrderIssuesRepairsParts.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                            objSubWorkOrderIssuesRepairsParts.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                            objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                                            objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                                            objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                                            objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                                            objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                                            objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                                            objSubWorkOrderIssuesRepairsParts.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                            objSubWorkOrderIssuesRepairsParts.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                            objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                                            objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                                            objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                                            objSubWorkOrderIssuesRepairsParts.installationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                                            objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                                            objSubWorkOrderIssuesRepairsParts.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]]];
                                            objSubWorkOrderIssuesRepairsParts.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                                            objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                                            objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                                            objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];
                                            
                                            // Vandor detail in parts
                                            objSubWorkOrderIssuesRepairsParts.vendorName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorName"]];
                                            objSubWorkOrderIssuesRepairsParts.vendorPartNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorPartNo"]];
                                            objSubWorkOrderIssuesRepairsParts.vendorQuoteNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorQuoteNo"]];
                                            
                                            objSubWorkOrderIssuesRepairsParts.partCategorySysName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCategorySysName"]];

                                            
                                            NSError *error5;
                                            [context save:&error5];
                                            
                                        }
                                        
                                    }
                                    
                                    
                                } else {
                                    
                                    //Yaha Par SubWork Order Repairs add honge
                                    
                                    //============================================================================
                                    //============================================================================
#pragma mark- @@@@@@@@------------------ Sub Work Order Issues Repairs Save --------------------------
                                    //============================================================================
                                    //============================================================================
                                    
                                    //                                    entityMechanicalSubWorkOrderIssuesRepairs=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
                                    //
                                    //                                    MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairs insertIntoManagedObjectContext:context];
                                    
                                    //SubWorkOrderIssuesRepairs
                                    if(![[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"] isKindOfClass:[NSArray class]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        NSArray *arrOfSubWorkOrderIssuesRepairsFromServer=[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWOIssueRepairDcs"];
                                        
                                        for (int k3=0; k3<arrOfSubWorkOrderIssuesRepairsFromServer.count; k3++) {
                                            
                                            entityMechanicalSubWorkOrderIssuesRepairs=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
                                            
                                            MechanicalSubWOIssueRepairDcs *objSubWorkOrderIssuesRepairs = [[MechanicalSubWOIssueRepairDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairs insertIntoManagedObjectContext:context];
                                            
                                            NSDictionary *dictOfSubWorkOrderIssuesRepairsInfo = arrOfSubWorkOrderIssuesRepairsFromServer[k3];
                                            
                                            objSubWorkOrderIssuesRepairs.workorderId=strGlobalWorkOrderId;
                                            objSubWorkOrderIssuesRepairs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                            objSubWorkOrderIssuesRepairs.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IssueRepairId"]];
                                            objSubWorkOrderIssuesRepairs.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                            objSubWorkOrderIssuesRepairs.repairMasterId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairMasterId"]];
                                            objSubWorkOrderIssuesRepairs.repairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairLaborId"]];
                                            objSubWorkOrderIssuesRepairs.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsActive"]]];
                                            objSubWorkOrderIssuesRepairs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedBy"]];
                                            objSubWorkOrderIssuesRepairs.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedDate"]]];
                                            objSubWorkOrderIssuesRepairs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedBy"]];
                                            objSubWorkOrderIssuesRepairs.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"ModifiedDate"]]];
                                            objSubWorkOrderIssuesRepairs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CreatedByDevice"]];
                                            objSubWorkOrderIssuesRepairs.repairName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairName"]];
                                            objSubWorkOrderIssuesRepairs.repairDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"RepairDesc"]];
                                            objSubWorkOrderIssuesRepairs.costAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CostAdjustment"]];
                                            objSubWorkOrderIssuesRepairs.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsWarranty"]]];
                                            objSubWorkOrderIssuesRepairs.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"IsCompleted"]]];
                                            objSubWorkOrderIssuesRepairs.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"CustomerFeedback"]]];
                                            
                                            objSubWorkOrderIssuesRepairs.partCostAdjustment=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"PartCostAdjustment"]];
                                            objSubWorkOrderIssuesRepairs.nonStdRepairAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdRepairAmt"]];
                                            objSubWorkOrderIssuesRepairs.nonStdPartAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdPartAmt"]];
                                            objSubWorkOrderIssuesRepairs.nonStdLaborAmt=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"NonStdLaborAmt"]];
                                            objSubWorkOrderIssuesRepairs.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"Qty"]];

                                            
                                            NSError *error4;
                                            [context save:&error4];
                                            
                                            
                                            //Yaha Parr Issues Repairs parts Save karna hai
                                            
                                            //============================================================================
                                            //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Parts Save --------------------------
                                            //============================================================================
                                            //============================================================================
                                            
                                            //                                            entityMechanicalSubWorkOrderIssuesRepairsParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                            //
                                            //                                            MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsParts insertIntoManagedObjectContext:context];
                                            
                                            //SubWorkOrderIssuesRepairsParts
                                            if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"] isKindOfClass:[NSArray class]])
                                            {
                                                
                                            }
                                            else
                                            {
                                                NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairPartDcs"];
                                                
                                                for (int k4=0; k4<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k4++) {
                                                    
                                                    entityMechanicalSubWorkOrderIssuesRepairsParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
                                                    
                                                    MechanicalSubWOIssueRepairPartDcs *objSubWorkOrderIssuesRepairsParts = [[MechanicalSubWOIssueRepairPartDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsParts insertIntoManagedObjectContext:context];
                                                    
                                                    NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k4];
                                                    
                                                    objSubWorkOrderIssuesRepairsParts.workorderId=strGlobalWorkOrderId;
                                                    objSubWorkOrderIssuesRepairsParts.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                                    objSubWorkOrderIssuesRepairsParts.issueRepairPartId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairPartId"]];
                                                    objSubWorkOrderIssuesRepairsParts.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                                    objSubWorkOrderIssuesRepairsParts.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                                    objSubWorkOrderIssuesRepairsParts.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                                    objSubWorkOrderIssuesRepairsParts.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                                    objSubWorkOrderIssuesRepairsParts.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                                    objSubWorkOrderIssuesRepairsParts.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                                    objSubWorkOrderIssuesRepairsParts.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                                    objSubWorkOrderIssuesRepairsParts.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                                    objSubWorkOrderIssuesRepairsParts.partType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartType"]];
                                                    objSubWorkOrderIssuesRepairsParts.partCode=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCode"]];
                                                    objSubWorkOrderIssuesRepairsParts.partName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartName"]];
                                                    objSubWorkOrderIssuesRepairsParts.partDesc=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartDesc"]];
                                                    objSubWorkOrderIssuesRepairsParts.qty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Qty"]];
                                                    objSubWorkOrderIssuesRepairsParts.unitPrice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"UnitPrice"]];
                                                    objSubWorkOrderIssuesRepairsParts.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                                    objSubWorkOrderIssuesRepairsParts.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                                    objSubWorkOrderIssuesRepairsParts.serialNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"SerialNumber"]];
                                                    objSubWorkOrderIssuesRepairsParts.modelNumber=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModelNumber"]];
                                                    objSubWorkOrderIssuesRepairsParts.manufacturer=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Manufacturer"]];
                                                    objSubWorkOrderIssuesRepairsParts.installationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"InstallationDate"]]];
                                                    objSubWorkOrderIssuesRepairsParts.multiplier=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"Multiplier"]];
                                                    objSubWorkOrderIssuesRepairsParts.isCompleted=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsCompleted"]]];
                                                    objSubWorkOrderIssuesRepairsParts.customerFeedback=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CustomerFeedback"]]];
                                                    objSubWorkOrderIssuesRepairsParts.isAddedAfterApproval=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsAddedAfterApproval"]]];
                                                    objSubWorkOrderIssuesRepairsParts.actualQty=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ActualQty"]];
                                                    objSubWorkOrderIssuesRepairsParts.isChangeStdPartPrice=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsChangeStdPartPrice"]]];
                                                    
                                                    
                                                    // Vandor detail in parts
                                                    objSubWorkOrderIssuesRepairsParts.vendorName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorName"]];
                                                    objSubWorkOrderIssuesRepairsParts.vendorPartNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorPartNo"]];
                                                    objSubWorkOrderIssuesRepairsParts.vendorQuoteNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"VendorQuoteNo"]];
                                                    
                                                    objSubWorkOrderIssuesRepairsParts.partCategorySysName=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"PartCategorySysName"]];

                                                    
                                                    NSError *error5;
                                                    [context save:&error5];
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            
                                            //Yaha Parr Issues Repairs parts Save karna hai
                                            
                                            
                                            //============================================================================
                                            //============================================================================
#pragma mark- @@@@@@------------------@@@@@@@@@@@@ Sub Work Order Issues Repairs Labours Save --------------------------
                                            //============================================================================
                                            //============================================================================
                                            
                                            //                                            entityMechanicalSubWorkOrderIssuesRepairsLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                                            //
                                            //                                            MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsLabour insertIntoManagedObjectContext:context];
                                            
                                            //SubWorkOrderIssues
                                            if(![[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"] isKindOfClass:[NSArray class]])
                                            {
                                                
                                            }
                                            else
                                            {
                                                NSArray *arrOfSubWorkOrderIssuesRepairsPartsFromServer=[dictOfSubWorkOrderIssuesRepairsInfo valueForKey:@"SubWOIssueRepairLaborDcs"];
                                                
                                                for (int k5=0; k5<arrOfSubWorkOrderIssuesRepairsPartsFromServer.count; k5++) {
                                                    
                                                    entityMechanicalSubWorkOrderIssuesRepairsLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
                                                    
                                                    MechannicalSubWOIssueRepairLaborDcs *objSubWorkOrderIssuesRepairsLabour = [[MechannicalSubWOIssueRepairLaborDcs alloc]initWithEntity:entityMechanicalSubWorkOrderIssuesRepairsLabour insertIntoManagedObjectContext:context];
                                                    
                                                    NSDictionary *dictOfSubWorkOrderIssuesRepairsPartsInfo = arrOfSubWorkOrderIssuesRepairsPartsFromServer[k5];
                                                    
                                                    objSubWorkOrderIssuesRepairsLabour.workorderId=strGlobalWorkOrderId;
                                                    objSubWorkOrderIssuesRepairsLabour.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                                    objSubWorkOrderIssuesRepairsLabour.subWorkOrderIssueId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesInfo valueForKey:@"SubWorkOrderIssueId"]];
                                                    objSubWorkOrderIssuesRepairsLabour.issueRepairLaborId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairLaborId"]];
                                                    objSubWorkOrderIssuesRepairsLabour.issueRepairId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IssueRepairId"]];
                                                    objSubWorkOrderIssuesRepairsLabour.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborType"]];
                                                    objSubWorkOrderIssuesRepairsLabour.laborCost=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCost"]];
                                                    
                                                    objSubWorkOrderIssuesRepairsLabour.laborHours=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborHours"]];
                                                    
                                                    objSubWorkOrderIssuesRepairsLabour.laborCostType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborCostType"]];
                                                    objSubWorkOrderIssuesRepairsLabour.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsActive"]]];
                                                    objSubWorkOrderIssuesRepairsLabour.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedBy"]];
                                                    objSubWorkOrderIssuesRepairsLabour.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedDate"]]];
                                                    objSubWorkOrderIssuesRepairsLabour.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedBy"]];
                                                    objSubWorkOrderIssuesRepairsLabour.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"ModifiedDate"]]];
                                                    objSubWorkOrderIssuesRepairsLabour.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"CreatedByDevice"]];
                                                    objSubWorkOrderIssuesRepairsLabour.laborDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"LaborDescription"]];
                                                    objSubWorkOrderIssuesRepairsLabour.isWarranty=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsWarranty"]]];
                                                    objSubWorkOrderIssuesRepairsLabour.isDefault=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderIssuesRepairsPartsInfo valueForKey:@"IsDefault"]]];
                                                    
                                                    NSError *error6;
                                                    [context save:&error6];
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                        }
                        
                        
                        //============================================================================
                        //============================================================================
#pragma mark- ------------------ Sub Work Order Helper Save --------------------------
                        //============================================================================
                        //============================================================================
                        
                        //                        entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
                        //
                        //                        MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
                        
                        //SubWorkOrderHelper
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOTechHelperDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            NSArray *arrOfSubWorkOrderHelperFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWOTechHelperDcs"];
                            
                            for (int k6=0; k6<arrOfSubWorkOrderHelperFromServer.count; k6++) {
                                
                                entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
                                
                                MechanicalSubWOTechHelperDcs *objSubWorkOrderHelper = [[MechanicalSubWOTechHelperDcs alloc]initWithEntity:entityMechanicalSubWorkOrderHelper insertIntoManagedObjectContext:context];
                                
                                NSDictionary *dictOfSubWorkOrderHelperInfo = arrOfSubWorkOrderHelperFromServer[k6];
                                
                                objSubWorkOrderHelper.workorderId=strGlobalWorkOrderId;
                                objSubWorkOrderHelper.subWorkOrderTechnicianId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"SubWorkOrderTechnicianId"]];
                                objSubWorkOrderHelper.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                objSubWorkOrderHelper.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"EmployeeNo"]];
                                objSubWorkOrderHelper.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"IsActive"]]];
                                objSubWorkOrderHelper.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedBy"]];
                                objSubWorkOrderHelper.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedDate"]]];
                                objSubWorkOrderHelper.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedBy"]];
                                objSubWorkOrderHelper.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"ModifiedDate"]]];
                                objSubWorkOrderHelper.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"CreatedByDevice"]];
                                objSubWorkOrderHelper.laborType=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderHelperInfo valueForKey:@"LaborType"]];

                                NSError *error2;
                                [context save:&error2];
                                
                            }
                            
                        }
                        
                        
#pragma mark- ---------******** Sub Work Order WorkOrderAppliedDiscountExtSerDcs Save *******----------------
                        // Akshay Start //
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"WorkOrderAppliedDiscountExtSerDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            NSArray *arrOfWorkOrderAppliedDiscount=[dictOfSubWorkOrderInfo valueForKey:@"WorkOrderAppliedDiscountExtSerDcs"];
                            
                            for (int k3=0; k3<arrOfWorkOrderAppliedDiscount.count; k3++) {
                                
                                entityWorkOrderAppliedDiscountExtSerDcs=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
                                
                                WorkOrderAppliedDiscountExtSerDcs *objWorkOrderAppliedDiscount = [[WorkOrderAppliedDiscountExtSerDcs alloc]initWithEntity:entityWorkOrderAppliedDiscountExtSerDcs insertIntoManagedObjectContext:context];
                                
                                NSDictionary *dictOfWorkOrderAppliedDiscount = arrOfWorkOrderAppliedDiscount[k3];
                                
                                objWorkOrderAppliedDiscount.companyKey = strCompanyKeyy;
                                objWorkOrderAppliedDiscount.userName = strUserName;
                                objWorkOrderAppliedDiscount.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"SubWorkOrderId"]];
                                
                                objWorkOrderAppliedDiscount.workOrderId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"WorkOrderId"]];
                                
                                objWorkOrderAppliedDiscount.workOrderAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"WorkOrderAppliedDiscountId"]];
                                
                                objWorkOrderAppliedDiscount.discountSysName=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountSysName"]];
                                
                                objWorkOrderAppliedDiscount.discountType=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountType"]];
                                
                                objWorkOrderAppliedDiscount.isActive=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"IsActive"]];
                                
                                objWorkOrderAppliedDiscount.createdBy=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedBy"]];
                                
                                objWorkOrderAppliedDiscount.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedDate"]]];
                                
                                objWorkOrderAppliedDiscount.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ModifiedBy"]];
                                
                                objWorkOrderAppliedDiscount.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ModifiedDate"]]];
                                
                                objWorkOrderAppliedDiscount.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"CreatedByDevice"]];
                                
                                objWorkOrderAppliedDiscount.discountCode=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountCode"]];
                                
                                objWorkOrderAppliedDiscount.discountAmount=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountAmount"]];
                                
                                objWorkOrderAppliedDiscount.discountDescription=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountDescription"]];
                                
                                objWorkOrderAppliedDiscount.discountName=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountName"]];
                                
                                objWorkOrderAppliedDiscount.discountPercent=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"DiscountPercent"]];
                                
                                objWorkOrderAppliedDiscount.applyOnPartPrice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ApplyOnPartPrice"]];
                                
                                objWorkOrderAppliedDiscount.applyOnLaborPrice=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"ApplyOnLaborPrice"]];
                                
                                objWorkOrderAppliedDiscount.appliedDiscountAmt=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"AppliedDiscountAmt"]];
                                
                                objWorkOrderAppliedDiscount.isDiscountPercent=[NSString stringWithFormat:@"%@",[dictOfWorkOrderAppliedDiscount valueForKey:@"IsDiscountPercent"]];
                                
                                
                                NSError *error4;
                                [context save:&error4];
                            }
                        }
                        // Akshay End //
                        
                        
                        //============================================================================
                        //============================================================================
#pragma mark- ------------------ Sub Work Order Notes Save --------------------------
                        //============================================================================
                        //============================================================================
                        
                        //                        entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
                        //
                        //                        MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
                        
                        //SubWorkOrderHelper
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNoteDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            NSArray *arrOfSubWorkOrderNotesFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderNoteDcs"];
                            
                            for (int k7=0; k7<arrOfSubWorkOrderNotesFromServer.count; k7++) {
                                
                                entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
                                
                                MechanicalSubWorkOrderNoteDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderNoteDcs alloc]initWithEntity:entityMechanicalSubWorkOrderNotes insertIntoManagedObjectContext:context];
                                
                                NSDictionary *dictOfSubWorkOrderNotesInfo = arrOfSubWorkOrderNotesFromServer[k7];
                                
                                objSubWorkOrderNotes.workorderId=strGlobalWorkOrderId;
                                objSubWorkOrderNotes.subWorkOrderNoteId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"SubWorkOrderNoteId"]];
                                objSubWorkOrderNotes.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                objSubWorkOrderNotes.note=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"Note"]];
                                objSubWorkOrderNotes.isActive=[global strBoolValueFromServer:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"IsActive"]]];
                                objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedBy"]];
                                objSubWorkOrderNotes.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedDate"]]];
                                objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"ModifiedBy"]];
                                objSubWorkOrderNotes.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"ModifiedDate"]]];
                                objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderNotesInfo valueForKey:@"CreatedByDevice"]];
                                
                                
                                NSError *error2;
                                [context save:&error2];
                                
                            }
                            
                        }
                        
                        NSError *error1;
                        [context save:&error1];
                        
                        
                        //============================================================================
                        //============================================================================
#pragma mark- ------------------ Sub Work Order Actual Hours Save --------------------------
                        //============================================================================
                        //============================================================================
                        
                        //                        entityMechanicalSubWorkOrderActualHours=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
                        //
                        //                        MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderActualHours = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHours insertIntoManagedObjectContext:context];
                        
                        //SubWorkOrderHelper
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderActualHoursDcs"] isKindOfClass:[NSArray class]])
                        {
                            
                        }
                        else
                        {
                            NSArray *arrOfSubWorkOrderActualHoursFromServer=[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderActualHoursDcs"];
                            
                            for (int k8=0; k8<arrOfSubWorkOrderActualHoursFromServer.count; k8++) {
                                
                                entityMechanicalSubWorkOrderActualHours=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
                                
                                MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderActualHours = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHours insertIntoManagedObjectContext:context];
                                
                                NSDictionary *dictOfSubWorkOrderActualHoursInfo = arrOfSubWorkOrderActualHoursFromServer[k8];
                                
                                objSubWorkOrderActualHours.workorderId=strGlobalWorkOrderId;
                                objSubWorkOrderActualHours.subWOActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourId"]];
                                objSubWorkOrderActualHours.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                                objSubWorkOrderActualHours.subWOActualHourNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWOActualHourNo"]];
                                objSubWorkOrderActualHours.isActive=[global strBoolValueFromServer: [NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"IsActive"]]];
                                objSubWorkOrderActualHours.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedBy"]];
                                objSubWorkOrderActualHours.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedDate"]]];
                                objSubWorkOrderActualHours.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedBy"]];
                                objSubWorkOrderActualHours.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ModifiedDate"]]];
                                objSubWorkOrderActualHours.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"CreatedByDevice"]];
                                objSubWorkOrderActualHours.companyKey=strCompanyKeyy;
                                objSubWorkOrderActualHours.timeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeOut"]]];
                                objSubWorkOrderActualHours.timeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"TimeIn"]]];
                                objSubWorkOrderActualHours.actHrsDescription=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"ActHrsDescription"]];
                                objSubWorkOrderActualHours.reason=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Reason"]];
                                objSubWorkOrderActualHours.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWorkOrderNo"]];
                                objSubWorkOrderActualHours.status=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Status"]];
                                
                                
                                objSubWorkOrderActualHours.mobileTimeIn=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeIn"]]];
                                objSubWorkOrderActualHours.mobileTimeOut=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"MobileTimeOut"]]];
                                objSubWorkOrderActualHours.latitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Latitude"]];
                                objSubWorkOrderActualHours.longitude=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderActualHoursInfo valueForKey:@"Longitude"]];
                                objSubWorkOrderActualHours.employeeNo=[global getEmployeeDeatils];
                                
                                NSError *error2;
                                [context save:&error2];
                                
                                
                                //============================================================================
                                //============================================================================
#pragma mark- --------------------- SubWoEmployeeWorkingTimeExtSerDcs  Save -------------------------
                                //============================================================================
                                //============================================================================
                                
                                //PaymentInfo
                                if(![[dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWoEmployeeWorkingTimeExtSerDcs"] isKindOfClass:[NSArray class]])
                                {
                                    
                                }
                                else
                                {
                                    
                                    NSArray *arrOfSubWoEmployeeWorkingTimeExtSerDcs= [dictOfSubWorkOrderActualHoursInfo valueForKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
                                    
                                    for (int k9=0; k9<arrOfSubWoEmployeeWorkingTimeExtSerDcs.count; k9++) {
                                        
                                        entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
                                        
                                        SubWoEmployeeWorkingTimeExtSerDcs *objSubWoEmployeeWorkingTimeExtSerDcs = [[SubWoEmployeeWorkingTimeExtSerDcs alloc]initWithEntity:entitySubWoEmployeeWorkingTimeExtSerDcs insertIntoManagedObjectContext:context];
                                        
                                        
                                        NSDictionary *dictOfSubWoEmployeeWorkingTimeExtSerDcs =arrOfSubWoEmployeeWorkingTimeExtSerDcs[k9];
                                        
                                        objSubWoEmployeeWorkingTimeExtSerDcs.subWOEmployeeWorkingTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWOEmployeeWorkingTimeId"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWorkOrderId"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=strGlobalWorkOrderId;
                                        objSubWoEmployeeWorkingTimeExtSerDcs.subWorkOrderActualHourId=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"SubWorkOrderActualHourId"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.employeeNo=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"EmployeeNo"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.employeeName=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"EmployeeName"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.workingDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"WorkingDate"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlotTitle=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"TimeSlotTitle"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"TimeSlot"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ActualDurationInMin"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ActualAmt"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"BillableDurationInMin"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"BillableAmt"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"WorkingType"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.isApprove=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsApprove"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsActive"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedDate"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedBy"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ModifiedDate"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"ModifiedBy"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"CreatedByDevice"]];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",strGlobalWorkOrderId];
                                        objSubWoEmployeeWorkingTimeExtSerDcs.isHelper=[NSString stringWithFormat:@"%@",[dictOfSubWoEmployeeWorkingTimeExtSerDcs valueForKey:@"IsHelper"]];

                                        NSError *errorSubWoEmployeeWorkingTimeExtSerDcs;
                                        [context save:&errorSubWoEmployeeWorkingTimeExtSerDcs];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                        
                        //Yaha Par Payment bhi
                        //============================================================================
                        //============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
                        //============================================================================
                        //============================================================================
                        
                        entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
                        
                        MechanicalSubWOPaymentDetailDcs *objPaymentInfoServiceAuto = [[MechanicalSubWOPaymentDetailDcs alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
                        
                        //PaymentInfo
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOPaymentDetailDcs"] isKindOfClass:[NSDictionary class]])
                        {
                            
                        }
                        else
                        {
                            NSDictionary *dictOfPaymentInfo = [dictOfSubWorkOrderInfo valueForKey:@"SubWOPaymentDetailDcs"];
                            objPaymentInfoServiceAuto.subWoPaymentId=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"SubWoPaymentId"]];
                            objPaymentInfoServiceAuto.recieptPath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"RecieptPath"]];
                            
                            objPaymentInfoServiceAuto.workorderId=strGlobalWorkOrderId;
                            objPaymentInfoServiceAuto.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWorkOrderInfo valueForKey:@"SubWorkOrderId"]];
                            
                            objPaymentInfoServiceAuto.paymentMode=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaymentMode"]];
                            
                            objPaymentInfoServiceAuto.paidAmount=[NSString stringWithFormat:@"%.02f", [[dictOfPaymentInfo valueForKey:@"PaidAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaidAmount"]];
                            
                            objPaymentInfoServiceAuto.checkNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckNo"]];
                            
                            objPaymentInfoServiceAuto.drivingLicenseNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"DrivingLicenseNo"]];
                            
                            objPaymentInfoServiceAuto.expirationDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ExpirationDate"]]];
                            
                            objPaymentInfoServiceAuto.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckFrontImagePath"]];
                            
                            objPaymentInfoServiceAuto.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckBackImagePath"]];
                            
                            objPaymentInfoServiceAuto.createdDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedDate"]]];
                            
                            objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedBy"]];
                            
                            objPaymentInfoServiceAuto.modifiedDate=[global ChangeDateToLocalDateMechanicalAllDates:[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedDate"]]];
                            
                            objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedBy"]];
                            
                            objPaymentInfoServiceAuto.userName=strUserName;
                            
                        }
                        
                        
                        //============================================================================
                        //============================================================================
#pragma mark- --------------------- SubWOCompleteTimeExtSerDcs  Save -------------------------
                        //============================================================================
                        //============================================================================
                        
                        entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
                        
                        SubWOCompleteTimeExtSerDcs *objSubWOCompleteTimeExtSerDcs = [[SubWOCompleteTimeExtSerDcs alloc]initWithEntity:entitySubWOCompleteTimeExtSerDcs insertIntoManagedObjectContext:context];
                        
                        //PaymentInfo
                        if(![[dictOfSubWorkOrderInfo valueForKey:@"SubWOCompleteTimeExtSerDcs"] isKindOfClass:[NSDictionary class]])
                        {
                            
                        }
                        else
                        {
                            NSDictionary *dictOfSubWOCompleteTimeExtSerDcs = [dictOfSubWorkOrderInfo valueForKey:@"SubWOCompleteTimeExtSerDcs"];
                            objSubWOCompleteTimeExtSerDcs.subWOCompleteTimeId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"SubWOCompleteTimeId"]];
                            objSubWOCompleteTimeExtSerDcs.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"SubWorkOrderId"]];
                            objSubWOCompleteTimeExtSerDcs.workorderId=strGlobalWorkOrderId;
                            objSubWOCompleteTimeExtSerDcs.actualDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ActualDurationInMin"]];
                            objSubWOCompleteTimeExtSerDcs.actualAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ActualAmt"]];
                            objSubWOCompleteTimeExtSerDcs.billableDurationInMin=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"BillableDurationInMin"]];
                            objSubWOCompleteTimeExtSerDcs.billableAmt=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"BillableAmt"]];
                            objSubWOCompleteTimeExtSerDcs.timeSlot=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"TimeSlot"]];
                            objSubWOCompleteTimeExtSerDcs.workingType=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"WorkingType"]];
                            objSubWOCompleteTimeExtSerDcs.isActive=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"IsActive"]];
                            objSubWOCompleteTimeExtSerDcs.createdDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedDate"]];
                            objSubWOCompleteTimeExtSerDcs.createdBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedBy"]];
                            objSubWOCompleteTimeExtSerDcs.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ModifiedDate"]];
                            objSubWOCompleteTimeExtSerDcs.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"ModifiedBy"]];
                            objSubWOCompleteTimeExtSerDcs.createdByDevice=[NSString stringWithFormat:@"%@",[dictOfSubWOCompleteTimeExtSerDcs valueForKey:@"CreatedByDevice"]];
                            objSubWOCompleteTimeExtSerDcs.workorderId=[NSString stringWithFormat:@"%@",strGlobalWorkOrderId];
                            
                        }
                        
                    }
                    
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Mechanical Equipment(AccountItemHistoryExtSerDcs) Id Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"AccountItemHistoryExtSerDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"AccountItemHistoryExtSerDcs"];//AccountItemHistoryExtSerDcs
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
                        
                        MechanicalWoEquipment *objEmailDetail = [[MechanicalWoEquipment alloc]initWithEntity:entityMechanicalEquipment insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        
                        objEmailDetail.userName=strUserName;
                        objEmailDetail.workorderId=strGlobalWorkOrderId;//strGlobalWorkOrderId
                        // objEmailDetail.subWorkOrderId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SubWorkOrderId"]];
                        objEmailDetail.subWorkOrderId=strSubWorkOrderIdTempToSave;
                        objEmailDetail.workOrderNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WorkOrderNo"]];
                        objEmailDetail.accountItemHistoryId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountItemHistoryId"]];
                        objEmailDetail.accountId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountId"]];
                        objEmailDetail.itemName=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemName"]];
                        objEmailDetail.itemcode=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Itemcode"]];
                        objEmailDetail.subWorkOrderNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SubWorkOrderNo"]];
                        objEmailDetail.serialNumber=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"SerialNumber"]];
                        objEmailDetail.modelNumber=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModelNumber"]];
                        objEmailDetail.manufacturer=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Manufacturer"]];
                        objEmailDetail.qty=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Qty"]];
                        objEmailDetail.installationDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"InstallationDate"]];
                        objEmailDetail.warrantyExpireDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WarrantyExpireDate"]];
                        objEmailDetail.accountNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"AccountNo"]];
                        objEmailDetail.serviceAddressId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ServiceAddressId"]];
                        objEmailDetail.barcodeUrl=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"BarcodeUrl"]];
                        objEmailDetail.installedArea=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"InstalledArea"]];
                        objEmailDetail.equipmentDesc=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EquipmentDesc"]];
                        objEmailDetail.createdByDevice=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedByDevice"]];
                        
                        objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                        
                        objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                        
                        objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                        
                        objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsActive"]] isEqualToString:@"True"])
                        {
                            objEmailDetail.isActive=@"true";
                        }
                        else
                        {
                            objEmailDetail.isActive=@"false";
                        }
                        
                        NSString *strItemCustomName=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemCustomName"]];
                        
                        if ([strItemCustomName isEqualToString:@"(null)"]) {
                            
                            strItemCustomName=@"";
                            
                        }
                        
                        objEmailDetail.itemCustomName=[NSString stringWithFormat:@"%@",strItemCustomName];
                        
                        NSString *stritemNo=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ItemNo"]];
                        
                        if ([stritemNo isEqualToString:@"(null)"]) {
                            
                            stritemNo=@"";
                            
                        }
                        objEmailDetail.itemNo=stritemNo;
                        
                        NSError *error222;
                        [context save:&error222];
                    }
                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Mechanical ServiceAddressPOCDetailDcs Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
                        
                        ServiceAddressPOCDetailDcs *objEmailDetail = [[ServiceAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalServiceAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        objEmailDetail.userName=strUserName;
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        objEmailDetail.pocDetails=arrEmailDetail;
                        
                        NSError *error2222;
                        [context save:&error2222];
                    }
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Mechanical BillingAddressPOCDetailDcs Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
                        
                        BillingAddressPOCDetailDcs *objEmailDetail = [[BillingAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalBillingAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        objEmailDetail.userName=strUserName;
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        objEmailDetail.pocDetails=arrEmailDetail;
                        
                        NSError *error22222;
                        [context save:&error22222];
                    }
                }
                
                
#pragma mark- ---------******** Mechanical AccountDiscountExtSerDcs Save *******----------------
                // Akshay Start//
                if(![[dictWorkOrderDetail valueForKey:@"AccountDiscountExtSerDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"AccountDiscountExtSerDcs"];
                    
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalAccountDiscountExtSerDcs=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
                        
                        AccountDiscountExtSerDcs *objAccountDiscount = [[AccountDiscountExtSerDcs alloc]initWithEntity:entityMechanicalAccountDiscountExtSerDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictAccDiscount=[[NSMutableDictionary alloc]init];
                        dictAccDiscount=[arrEmailDetail objectAtIndex:j];
                        
                        objAccountDiscount.companyKey=strCompanyKeyy;
                        objAccountDiscount.userName=strUserName;
                        objAccountDiscount.workorderId=strGlobalWorkOrderId;
                        objAccountDiscount.accountNo = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"AccountNo"]];
                        
                        objAccountDiscount.discountAmount = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountAmount"]];
                        
                        objAccountDiscount.discountSetupSysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountSetupSysName"]];
                        objAccountDiscount.discountSetupName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountSetupName"]];
                        objAccountDiscount.discountCode = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountCode"]];
                        
                        objAccountDiscount.isDiscountPercent = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsDiscountPercent"]];
                        objAccountDiscount.discountPercent = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountPercent"]];
                        objAccountDiscount.sysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"SysName"]];
                        
                        objAccountDiscount.name = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"Name"]];
                        
                        objAccountDiscount.isActive = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsActive"]];
                        
                        objAccountDiscount.isDelete = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"IsDeleted"]];
                        
                        objAccountDiscount.discountDescription = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DiscountDescription"]];
                        
                        objAccountDiscount.departmentSysName = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"DepartmentSysName"]];
                        objAccountDiscount.createdByDevice = [NSString stringWithFormat:@"%@",[dictAccDiscount valueForKey:@"CreatedByDevice"]];
                        
                        
                        
                        NSError *error22222;
                        [context save:&error22222];
                    }
                }
                // Akshay End
                
                
                //============================================================================
                //============================================================================
#pragma mark- --------- Mechanical WoOtherDocExtSerDcs Save ----------------
                //============================================================================
                //============================================================================
                
                //WoOtherDocExtSerDcs Save
                if(![[dictWorkOrderDetail valueForKey:@"WoOtherDocExtSerDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrWoOtherDocExtSerDcs=[dictWorkOrderDetail valueForKey:@"WoOtherDocExtSerDcs"];//WoOtherDocExtSerDcs
                    for (int j=0; j<arrWoOtherDocExtSerDcs.count; j++)
                    {
                        //WoOtherDocExtSerDcs Detail Entity
                        entityMechanicalWoOtherDocExtSerDcs=[NSEntityDescription entityForName:@"WoOtherDocExtSerDcs" inManagedObjectContext:context];
                        
                        WoOtherDocExtSerDcs *objWoOtherDocExtSerDcs = [[WoOtherDocExtSerDcs alloc]initWithEntity:entityMechanicalWoOtherDocExtSerDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictWoOtherDocExtSerDcs=[[NSMutableDictionary alloc]init];
                        dictWoOtherDocExtSerDcs=[arrWoOtherDocExtSerDcs objectAtIndex:j];
                        
                        objWoOtherDocExtSerDcs.companyKey=strCompanyKeyy;
                        objWoOtherDocExtSerDcs.userName=strUserName;
                        objWoOtherDocExtSerDcs.workOrderId=strGlobalWorkOrderId;
                        objWoOtherDocExtSerDcs.createdByDevice = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedByDevice"]];
                        objWoOtherDocExtSerDcs.wOOtherDocId = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocId"]];
                        objWoOtherDocExtSerDcs.workOrderNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WorkOrderNo"]];
                        objWoOtherDocExtSerDcs.leadNo = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"LeadNo"]];
                        objWoOtherDocExtSerDcs.wOOtherDocPath = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocPath"]];
                        objWoOtherDocExtSerDcs.wOOtherDocTitle = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WOOtherDocTitle"]];
                        objWoOtherDocExtSerDcs.woOtherDocDescription = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"WoOtherDocDescription"]];
                        objWoOtherDocExtSerDcs.createdDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedDate"]];
                        objWoOtherDocExtSerDcs.createdBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"CreatedBy"]];
                        objWoOtherDocExtSerDcs.modifiedDate = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedDate"]];
                        objWoOtherDocExtSerDcs.modifiedBy = [NSString stringWithFormat:@"%@",[dictWoOtherDocExtSerDcs valueForKey:@"ModifiedBy"]];
                        
                        NSError *errorObjWoOtherDocExtSerDcs;
                        [context save:&errorObjWoOtherDocExtSerDcs];
                    }
                }
                
                
                //Final Save
                NSError *error;
                [context save:&error];
                
                
            } else {
                
                //Kuch ni karna already save ho gya hai
                
            }
            
        }
    }
    
}

-(NSMutableArray*)checkIfToSendSalesLeadToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjModifyDate{
    
    NSManagedObject *matchesModifyDate;
    
    NSMutableArray *arrOfLeadsToFetchFromServer=[[NSMutableArray alloc]init];

    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"leadIdd"]];
        
    }
    
    for (int i=0; i<arrOfAppoint.count; i++)
    {
        
        NSDictionary *dictLeadDetail=[arrOfAppoint objectAtIndex:i];

        NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"LeadId"]];
        
        NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"ModifiedDate"]];
        
        [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
        
        if (!(arrAllObjModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjModifyDate.count; k++) {
                
                matchesModifyDate=arrAllObjModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                NSString *strModifyLeadModifyDate=[matchesModifyDate valueForKey:@"modifyDate"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                
                if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
                    NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                    NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                    
                    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                    [inputDateFormatter setTimeZone:inputTimeZone];
                    [inputDateFormatter setDateFormat:dateFormat];
                    
                    NSString *inputString = strModifyDateCompare;
                    NSDate *date = [inputDateFormatter dateFromString:inputString];
                    
                    NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                    [outputDateFormatter setTimeZone:outputTimeZone];
                    [outputDateFormatter setDateFormat:dateFormat];
                    NSString *outputString = [outputDateFormatter stringFromDate:date];
                    
                    ServerModifyDate = [dateFormatter dateFromString:outputString];
                    
                    NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                    
                    if(result == NSOrderedDescending)
                    {
                        NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                        //[arrOfLeadToSend addObject:strModifyLeadIdd];
                        
                        //change for is sync only on 27 july 2017
                        
                        BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            
                        }
                        
                    }
                    else if(result == NSOrderedAscending)
                    {
                        NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                        BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            [arrOfLeadsToFetchFromServer addObject:strModifyLeadIdd];
                            
                        }
                        [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                    }
                    else
                    {
                        NSLog(@"Equal date");//
                        BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            
                        }
                        
                    }
                }else{
                    
                    [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                    
                }
            }
            
            [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
            
        }
        
    }
    
    // Logic to fectch leads whicch are coming from server but not there in Local DB.
    //  arrOfWorkOrderIdComingFromServer   arrOfAlreadySavedLeadss
    
    NSMutableArray *arrTempTest1 = [[NSMutableArray alloc]init];
    [arrTempTest1 addObjectsFromArray:arrOfWorkOrderIdComingFromServer];
    //arrTempTest1 = arrOfWorkOrderIdComingFromServer;
    NSMutableArray *arrTempTest2 = arrOfAlreadySavedLeadss;
    [arrTempTest1 removeObjectsInArray:arrTempTest2];
    
    arrOfLeadsToFetchFromServer = [[arrOfLeadsToFetchFromServer arrayByAddingObjectsFromArray:arrTempTest1] mutableCopy];
    
    NSOrderedSet *arrTempTest3 = [NSOrderedSet orderedSetWithArray:arrOfLeadsToFetchFromServer];
    NSArray *arrTempTest4 = [arrTempTest3 array];
    
    arrOfLeadsToFetchFromServer = [[NSMutableArray alloc]init];
    [arrOfLeadsToFetchFromServer addObjectsFromArray:arrTempTest4];
    
    //Change if No Lead from web
    
    if (arrOfAppoint.count==0) {
        
        if (!(arrAllObjModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjModifyDate.count; k++) {
                
                matchesModifyDate=arrAllObjModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedLeadsSales:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    //Change To Delete void and tech Changed Wali
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
        
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendSalesLeadToServer"];
    
    NSOrderedSet *orderedId = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveInDbandDelete];
    arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSArray *aRRTemps1=[orderedId array];
    [arrOfLeadToSaveInDbandDelete addObjectsFromArray:aRRTemps1];
    
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveSalesLeadToLocalDbnDelete"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHow"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBSales"];
    [defs setObject:arrOfLeadsToFetchFromServer forKey:@"arrOfLeadsToFetchFromServer"];
    [defs synchronize];

    NSMutableArray *arrOfSalesLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    return arrOfSalesLeadToFetchDynamicData;
    
}


-(NSMutableArray*)checkIfToSendSalesLeadToServer : (NSDictionary*)dictForAppoint :(NSArray*)arrAllObjModifyDate{
    
    NSManagedObject *matchesModifyDate;
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"leadIdd"]];
        
    }
    
    NSArray *arrLeadExtSerDcs=[dictForAppoint valueForKey:@"LeadExtSerDcs"];
    for (int i=0; i<arrLeadExtSerDcs.count; i++)
    {
        NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
        if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
        {
            
        }
        else
        {
            NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
            
            NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
            
            [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
            
            if (!(arrAllObjModifyDate.count==0)) {
                
                for (int k=0; k<arrAllObjModifyDate.count; k++) {
                    
                    matchesModifyDate=arrAllObjModifyDate[k];
                    
                    NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                    NSString *strModifyLeadModifyDate=[matchesModifyDate valueForKey:@"modifyDate"];
                    
                    [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                    
                    if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
                        NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                        NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                        
                        NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                        NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                        NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                        [inputDateFormatter setTimeZone:inputTimeZone];
                        [inputDateFormatter setDateFormat:dateFormat];
                        
                        NSString *inputString = strModifyDateCompare;
                        NSDate *date = [inputDateFormatter dateFromString:inputString];
                        
                        NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                        [outputDateFormatter setTimeZone:outputTimeZone];
                        [outputDateFormatter setDateFormat:dateFormat];
                        NSString *outputString = [outputDateFormatter stringFromDate:date];
                        
                        ServerModifyDate = [dateFormatter dateFromString:outputString];
                        
                        NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                        
                        if(result == NSOrderedDescending)
                        {
                            NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                            //[arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                            //change for is sync only on 27 july 2017
                            
                            BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                        else if(result == NSOrderedAscending)
                        {
                            NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                            BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                        }
                        else
                        {
                            NSLog(@"Equal date");//
                            BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                    }else{
                        [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                    }
                }
                
                [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
            }
        }
    }
    
    
    //Change if No Lead from web
    
    if (arrLeadExtSerDcs.count==0) {
        
        if (!(arrAllObjModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjModifyDate.count; k++) {
                
                matchesModifyDate=arrAllObjModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedLeadsSales:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    //Change To Delete void and tech Changed Wali
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
        
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendSalesLeadToServer"];
    
    NSOrderedSet *orderedId = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveInDbandDelete];
    arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSArray *aRRTemps1=[orderedId array];
    [arrOfLeadToSaveInDbandDelete addObjectsFromArray:aRRTemps1];
    
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveSalesLeadToLocalDbnDelete"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHow"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBSales"];
    [defs synchronize];

    NSMutableArray *arrOfSalesLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    return arrOfSalesLeadToFetchDynamicData;
    
}

// 03 August Service Basic Api Implementation

-(NSMutableArray*)checkifToSendServiceAppointmentToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjServiceModifyDate{
 
    
    NSManagedObject *matchesServiceModifyDate;
    NSMutableArray *arrOfLeadsToFetchFromServer=[[NSMutableArray alloc]init];

    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjServiceModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"workorderId"]];
        
    }
    
    //NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];

    for (int i=0; i<arrOfAppoint.count; i++)
    {
        
        NSDictionary *dictLeadDetail=[arrOfAppoint objectAtIndex:i];

        NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"WorkorderId"]];
        
        [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
        
        if ([strLeadIdCompare isEqualToString:@"754"]) {
            
            NSLog(@"strLeadIdCompare======%@",strLeadIdCompare);
            
        }
        
        //NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"ModifiedDate"]];
        NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"ModifiedFormatedDate"]];

        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                NSString *strModifyLeadModifyDate=[matchesServiceModifyDate valueForKey:@"modifyDate"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                
                if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                    NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                    //                            NSDate *add90Min = [ServerModifyDate dateByAddingTimeInterval:(330*60)];
                    //                            ServerModifyDate = add90Min;
                    
                    NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                    [inputDateFormatter setTimeZone:inputTimeZone];
                    [inputDateFormatter setDateFormat:dateFormat];
                    
                    NSString *inputString = strModifyDateCompare;
                    NSDate *date = [inputDateFormatter dateFromString:inputString];
                    
                    NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                    [outputDateFormatter setTimeZone:outputTimeZone];
                    [outputDateFormatter setDateFormat:dateFormat];
                    NSString *outputString = [outputDateFormatter stringFromDate:date];
                    
                    ServerModifyDate = [inputDateFormatter dateFromString:outputString];
                    
                    NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                    
                    if(result == NSOrderedDescending)
                    {
                        NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                        //[arrOfLeadToSend addObject:strModifyLeadIdd];
                        
                        //change for is sync only on 27 july 2017
                        
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                        }
                        
                    }
                    else if(result == NSOrderedAscending)
                    {
                        NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            [arrOfLeadsToFetchFromServer addObject:strModifyLeadIdd];

                        }
                        
                        [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                    }
                    else
                    {
                        NSLog(@"Equal date");//
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                        }
                        
                    }
                }else{
                    [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                }
            }
            
            [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
        }
    }
    
    
    // Logic to fectch leads whicch are coming from server but not there in Local DB.
    //  arrOfWorkOrderIdComingFromServer   arrOfAlreadySavedLeadss
    
    NSMutableArray *arrTempTest1 = [[NSMutableArray alloc]init];
    [arrTempTest1 addObjectsFromArray:arrOfWorkOrderIdComingFromServer];
    //arrTempTest1 = arrOfWorkOrderIdComingFromServer;
    NSMutableArray *arrTempTest2 = arrOfAlreadySavedLeadss;
    [arrTempTest1 removeObjectsInArray:arrTempTest2];
    
    arrOfLeadsToFetchFromServer = [[arrOfLeadsToFetchFromServer arrayByAddingObjectsFromArray:arrTempTest1] mutableCopy];
    
    NSOrderedSet *arrTempTest3 = [NSOrderedSet orderedSetWithArray:arrOfLeadsToFetchFromServer];
    NSArray *arrTempTest4 = [arrTempTest3 array];
    
    arrOfLeadsToFetchFromServer = [[NSMutableArray alloc]init];
    [arrOfLeadsToFetchFromServer addObjectsFromArray:arrTempTest4];
    
    
    //Change if No workorder from web
    
    if (arrOfAppoint.count==0) {
        
        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    
    //Change To Delete void and tech Changed Wali
    
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendServiceLeadToServer"];
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveServiceLeadToLocalDbnDelete"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHowService"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBService"];
    [defs setObject:arrOfLeadsToFetchFromServer forKey:@"arrOfWorkOrdersToFetchFromServer"];
    [defs synchronize];

    
    NSMutableArray *arrOfServiceLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    
    return arrOfServiceLeadToFetchDynamicData;
        
}

-(NSMutableArray*)checkifToSendServiceAppointmentToServer : (NSDictionary*)dictForWorkOrderAppointment :(NSArray*)arrAllObjServiceModifyDate{
 
    
    NSManagedObject *matchesServiceModifyDate;

    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjServiceModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"workorderId"]];
        
    }
    
    NSArray *arrLeadExtSerDcs=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];
    for (int i=0; i<arrLeadExtSerDcs.count; i++)
    {
        NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
        if(![[dictLeadDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
        {
            
        }
        else
        {
            
            NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
            
            [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
            
            if ([strLeadIdCompare isEqualToString:@"754"]) {
                
                NSLog(@"strLeadIdCompare======%@",strLeadIdCompare);
                
            }
            
            NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.ModifiedFormatedDate"]];
            
            if (!(arrAllObjServiceModifyDate.count==0)) {
                
                for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                    
                    matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                    
                    NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                    NSString *strModifyLeadModifyDate=[matchesServiceModifyDate valueForKey:@"modifyDate"];
                    
                    [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                    
                    if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                        NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                        //                            NSDate *add90Min = [ServerModifyDate dateByAddingTimeInterval:(330*60)];
                        //                            ServerModifyDate = add90Min;
                        
                        NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                        NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                        NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                        [inputDateFormatter setTimeZone:inputTimeZone];
                        [inputDateFormatter setDateFormat:dateFormat];
                        
                        NSString *inputString = strModifyDateCompare;
                        NSDate *date = [inputDateFormatter dateFromString:inputString];
                        
                        NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                        [outputDateFormatter setTimeZone:outputTimeZone];
                        [outputDateFormatter setDateFormat:dateFormat];
                        NSString *outputString = [outputDateFormatter stringFromDate:date];
                        
                        ServerModifyDate = [dateFormatter dateFromString:outputString];
                        
                        NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                        
                        if(result == NSOrderedDescending)
                        {
                            NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                            //[arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                            //change for is sync only on 27 july 2017
                            
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                        else if(result == NSOrderedAscending)
                        {
                            NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                
                            }
                            
                            [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                        }
                        else
                        {
                            NSLog(@"Equal date");//
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                    }else{
                        [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                    }
                }
                
                [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
            }
        }
    }
    
    
    //Change if No workorder from web
    
    if (arrLeadExtSerDcs.count==0) {
        
        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    
    //Change To Delete void and tech Changed Wali
    
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendServiceLeadToServer"];
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveServiceLeadToLocalDbnDelete"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHowService"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBService"];
    [defs synchronize];
    
    NSMutableArray *arrOfServiceLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    
    return arrOfServiceLeadToFetchDynamicData;
        
}

-(BOOL)isSyncedWO :(NSString *)strWOid{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWOid];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrWO = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrWO.count==0)
    {
        
        return NO;
        
    }else
    {
        NSManagedObject *obj=arrWO[0];
        NSString *strzSync=[obj valueForKey:@"zSync"];
        //NSString *strStatusToCheck=[obj valueForKey:@"workorderStatus"];
        
        if ([strzSync isEqualToString:@"yes"]) {
            
//            if ([strStatusToCheck isEqualToString:@"Completed"] || [strStatusToCheck isEqualToString:@"Complete"]) {
//
//                return NO;
//
//            }else{
            
                return YES;
                
//            }
            
        }else{
            return NO;
        }
    }
    
}
-(BOOL)isSyncedLeadsSales :(NSString *)strWOid{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strWOid];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrWO = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrWO.count==0)
    {
        
        return NO;
        
    }else
    {
        NSManagedObject *obj=arrWO[0];
        NSString *strzSync=[obj valueForKey:@"zSync"];
        //NSString *strStatusToCheck=[obj valueForKey:@"statusSysName"];
        
        if ([strzSync isEqualToString:@"yes"]) {
            
//            if ([strStatusToCheck isEqualToString:@"Completed"] || [strStatusToCheck isEqualToString:@"Complete"]) {
//
//                return NO;
//
//            }else{
            
                return YES;
                
//            }
            
        }else{
            return NO;
        }
    }
    
}

-(NSMutableDictionary*)fetchSalesDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strLeadId{
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strLeadIdGlobal = strLeadId;
    strModifyDateToSendToServerAll = strModifyDateToSendToServer;
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrCheckImage=[[NSMutableArray alloc]init];
    arrAllTargetImages = [[NSMutableArray alloc]init];
    dictFinal=[[NSMutableDictionary alloc]init];

    [self fetchLeadDetail];
    [self fetchPaymentInfo];
    [self fetchImageDetail];
    [self fetchEmailDetail];
    [self fetchDocumentsDetail];
    [self fetchLeadPreference];
    [self fetchSoldServiceStandardDetail];
    [self fetchSoldServiceNonStandardDetail];
    [self fetchAgreementCheckListSales];
    [self fetchElectronicAuthorizedForm];
    [self fetchForAppliedDiscountFromCoreData];
    [self fetchLeadContactDetail];
    [self fetchRenewalServiceDetail];
    [self fetchTagDetail];
    //For  ClarkPest
    
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchLeadCommercialDetailExtDcFromCoreData];
    [self fetchMultiTermsFromCoreDataClarkPest];
    [self fetchForAppliedDiscountFromCoreDataClarkPest];
    [self fetchSalesMarketingContentFromCoreDataClarkPest];
    
    [self fetchUserDefinedData];
    [self fetchCRMImageDetail];
    //strAudioNameGlobal strElectronicSign
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:arrOfAllImagesToSendToServer forKey:@"arrOfAllImagesToSendToServer"];
    [dictTemp setValue:arrOfAllSignatureImagesToSendToServer forKey:@"arrOfAllSignatureImagesToSendToServer"];
    [dictTemp setValue:arrCheckImage forKey:@"arrCheckImage"];
    [dictTemp setValue:arrAllTargetImages forKey:@"arrAllTargetImages"];
    [dictTemp setValue:strAudioNameGlobal forKey:@"strAudioNameGlobal"];
    [dictTemp setValue:strElectronicSign forKey:@"strElectronicSign"];
    [dictTemp setValue:dictFinal forKey:@"dictFinal"];

    return dictTemp;

}

#pragma mark- -------- Fetching Sales Record ----------

-(void)fetchLeadDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    dictFinal = [[NSMutableDictionary alloc] init];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND employeeNo_Mobile=%@",strLeadIdGlobal,strEmployeeNo];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrLeadDetailKey);
            
            NSLog(@"all keys %@",arrLeadDetailValue);
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                //[arrLeadDetailValue addObject:str];
                if([[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"billingCountry"]||[[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"serviceCountry"])
                {
                    [arrLeadDetailValue addObject:[str stringByReplacingOccurrencesOfString:@" " withString:@""]];
                }
                else
                {
                    [arrLeadDetailValue addObject:str];
                }
                
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            //  [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            NSLog(@"%@",dictTemp);
            [dictFinal setObject:dictTemp forKey:@"LeadDetail"];
            
            
        }
    }
}
-(void)fetchPaymentInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrCheckImage=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSString *strCustomerImage=[matches valueForKey:@"customerSignature"];
            NSString *strSalesPersonImage=[matches valueForKey:@"salesSignature"];
            NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
            if (!(strCustomerImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerImage];//salesSignature
                
            }
            if (!(strSalesPersonImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strSalesPersonImage];//salesSignature
                
            }
            if (!(strCheckFrontImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckFrontImage];//salesSignature
                
            }
            if (!(strCheckBackImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckBackImage];//salesSignature
                
            }
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"PaymentInfo%@",dictPaymentInfo);
            [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
        }
    }
}
-(void)fetchImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSString *strImageName=[matches valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}
-(void)fetchCRMImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTagDetail=[NSEntityDescription entityForName:@"CRMImagesDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTagDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"CRMImagesDetail"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"CRMImagesDetail%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"CRMImagesDetail"];
    }
}
-(void)fetchEmailDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchLeadContactDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityContactNumberDetail= [NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
    [request setEntity:entityContactNumberDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"LeadContactDetailExtSerDc%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }
}
-(void)fetchDocumentsDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrDocumentsDetail addObject:dictPaymentInfo];
            NSLog(@"DocumentsDetail%@",arrDocumentsDetail);
        }
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
    }
}
-(void)fetchLeadPreference
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadPreference= [NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
    [request setEntity:entityLeadPreference];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadPreference"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadPreference%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadPreference"];
        }
        
        
    }
}
-(void)fetchSoldServiceStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrSoldServiceStandardDetail;
    arrSoldServiceStandardDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrPaymentInfoValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 if([str isEqual:nil] || str.length==0 )
                 {
                 str=@"";
                 }
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrSoldServiceStandardDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"SoldServiceStandardDetail%@",arrSoldServiceStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
    }
}
-(void)fetchSoldServiceNonStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrSoldServiceNonStandardDetail;
    arrSoldServiceNonStandardDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceNonStandardDetail;
            dictSoldServiceNonStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceNonStandardDetail);
            [arrSoldServiceNonStandardDetail addObject:dictSoldServiceNonStandardDetail];
            NSLog(@"SoldServiceNonStandardDetail%@",arrSoldServiceNonStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
        
    }
}
-(void)fetchRenewalServiceDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityRenewalServiceDetail= [NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    [request setEntity:entityRenewalServiceDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrRenewalServiceDetail;
    arrRenewalServiceDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);

            
            NSArray *arrRenewalServiceKey;
            NSMutableArray *arrRenewalServiceValue;
            
            arrRenewalServiceValue=[[NSMutableArray alloc]init];
            arrRenewalServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrRenewalServiceKey);
            
            NSLog(@"all keys %@",arrRenewalServiceValue);
            for (int i=0; i<arrRenewalServiceKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrRenewalServiceValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrRenewalServiceValue forKeys:arrRenewalServiceKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrRenewalServiceDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"RenewalServiceExtDcs%@",arrRenewalServiceDetail);
        }
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
    }
}
-(void)fetchTagDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTagDetail=[NSEntityDescription entityForName:@"LeadTagExtDcs" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTagDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadTagExtDcs%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadTagExtDcs"];
    }
}
-(void)fetchUserDefinedData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityTagDetail=[NSEntityDescription entityForName:@"UDFSavedData" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityTagDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"UDFSavedData"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                if([str isKindOfClass:[NSArray class]] || [str isKindOfClass:[NSDictionary class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"UDFSavedData%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        
        NSMutableArray *arrData = [[NSMutableArray alloc]init];
        for(int i=0; i<arrLeadCommercialScope.count;i++)
        {
            NSDictionary *dict = [arrLeadCommercialScope objectAtIndex:i];
            [arrData addObject:[dict valueForKey:@"userdefinedata"]];
        }
        [dictFinal setObject:arrData forKey:@"UDFSavedData"];

        //[dictFinal setObject:arrLeadCommercialScope forKey:@"UDFSavedData"];
    }
}

#pragma mark- ----------- Fetch Record For Clark Pest -------------

#pragma mark- ------ Fetch Scope --------

-(void)fetchScopeFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialScopeExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
    }
    
}
#pragma mark- ------- Fetch Target --------

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
             //Nilind  28 Nov 2019
             NSMutableArray *arrImage;
             for (int i=0; i<arrInfoKey.count; i++)
             {
             NSString *strLeadCommercialTargetId, *strMobileTargetId;
             strLeadCommercialTargetId = [matches valueForKey:@"leadCommercialTargetId"];
             strMobileTargetId = [matches valueForKey:@"mobileTargetId"];
             
             arrImage = [[NSMutableArray alloc]init];
             arrImage = [self fetchTargetImageDetail:strLeadCommercialTargetId MobileTargetId:strMobileTargetId];
             }
             NSMutableDictionary *dictPaymentInfo;
             dictPaymentInfo = [NSMutableDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
             [dictPaymentInfo setObject:arrImage forKey:@"targetImageDetailExtDc"];
             //End
            
//            NSDictionary *dictPaymentInfo;
//            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTargetExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
    }
    
    [self fetchAllTargetImages];
}

-(NSMutableArray *)fetchTargetImageDetail:(NSString *)strLeadCommercialTargetId MobileTargetId:(NSString *)strMobileTargetId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    
    if (strLeadCommercialTargetId.length > 0)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && leadCommercialTargetId=%@" ,strLeadIdGlobal, strLeadCommercialTargetId];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && mobileTargetId=%@",strLeadIdGlobal,strMobileTargetId];
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                // [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesTargetImage entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"TargetImageDetailImageDetail%@",arrImageDetail);
        }
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    return arrImageDetail;
}
-(void)fetchAllTargetImages
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrAllTargetImages = [[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                [arrAllTargetImages addObject:strImageName];
                
            }
        }
        
        
    }
    
}
#pragma mark- ------- Fetch LeadCommercialInitialInfoFromCoreData --------

-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    // arrAllObj=[[NSArray alloc]init];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialInitialInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
    }
    
    
    
}
#pragma mark- ------- Fetch LeadCommercialMaintInfoFromCoreData --------

-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialMaintInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
    }
    
    
}
#pragma mark- ------- Fetch MultiTermsFromCoreDataClarkPest --------

-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTermsExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
        
    }
}

#pragma mark- ------- Fetch LeadCommercialDiscountExtDc --------

-(void)fetchForAppliedDiscountFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialDiscountExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
    }
    
}
#pragma mark- ------- Fetch LeadCommercialDetailExtDc --------


-(void)fetchLeadCommercialDetailExtDcFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadCommercialDetailExtDc"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadCommercialDetailExtDc%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadCommercialDetailExtDc"];
        }
        
    }
    
}

#pragma mark- ------- Fetch Sales Marketing Content FromCoreDataClarkPest --------

-(void)fetchSalesMarketingContentFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadMarketingContentExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
}
-(void)fetchAgreementCheckListSales
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
    [request setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && isActive=%@",strLeadIdGlobal,@"true"];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND isActive = %@",strLeadId,@"true"];

    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            /*
             int indexToRemove=-1;
             int indexToReplaceModifyDate=-1;
             
             for (int k=0; k<arrPaymentInfoKey.count; k++) {
             
             NSString *strKeyLeadId=arrPaymentInfoKey[k];
             
             if ([strKeyLeadId isEqualToString:@"leadId"]) {
             
             indexToRemove=k;
             
             
             if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
             
             indexToReplaceModifyDate=k;
             
             }
             }
             
             NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
             
             [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
             
             [arrKeyTemp removeObjectAtIndex:indexToRemove];
             arrPaymentInfoKey=arrKeyTemp;
             [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
             
             [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
             */
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictAgreement;
            dictAgreement = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictAgreement);
            [arrEmailDetail addObject:dictAgreement];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }
}

-(void)fetchElectronicAuthorizedForm
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    [request setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"ElectronicAuthorizationForm"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSString  *strCustomerImage=[matches valueForKey:@"signaturePath"];
                if (!(strCustomerImage.length==0)) {
                    
                    strElectronicSign=strCustomerImage;
                    
                }
                
                NSArray *arrElectronicAuthorizedFormKey;
                NSMutableArray *arrElectronicAuthorizedFormValue;
                
                arrElectronicAuthorizedFormValue=[[NSMutableArray alloc]init];
                arrElectronicAuthorizedFormKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrElectronicAuthorizedFormKey);
                
                NSLog(@"all keys %@",arrElectronicAuthorizedFormValue);
                for (int i=0; i<arrElectronicAuthorizedFormKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrElectronicAuthorizedFormValue addObject:str];
                }
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrElectronicAuthorizedFormValue forKeys:arrElectronicAuthorizedFormKey];
                NSLog(@"ElectronicAuthorizationForm%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"ElectronicAuthorizationForm"];
            }
            
        }
    }
}

-(void)fetchForAppliedDiscountFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAppliedDiscounts= [NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    [request setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadAppliedDiscount;
    arrLeadAppliedDiscount=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadAppliedDiscounts%@",dictPaymentInfo);
            [arrLeadAppliedDiscount addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
    }
}

-(NSString*)strAudioName : (NSString*)strAudioName {
    
    NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
    if (equalRange.location != NSNotFound) {
        strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
    }
    
    return strAudioName;
}

-(NSMutableArray*)fetchOldLeadToDeleteFromMobileSalesFlow
{
    NSMutableArray *arrLeadIdToDelete = [[NSMutableArray alloc] init];

    @try {
        
        //============================================================================
        //============================================================================
        
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strConfigFromDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.FromDate"]];
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
        [request setEntity:entityLeadDetail];
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [request setSortDescriptors:sortDescriptors];
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray *arrOfSalesLeads = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        //    NSManagedObject *matchesNew;
        if (arrOfSalesLeads.count==0)
        {
            
            
            
        }else
        {
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSDate *date = [inputDateFormatter dateFromString:strConfigFromDateSalesAuto];
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *startFromDate = [[NSDate alloc] init];
            startFromDate = [outputDateFormatter dateFromString:outputString];
            
            NSDate *dateTwoDaysAgo = [date dateByAddingTimeInterval:-3*24*60*60];
            NSLog(@"2 days ago: %@", dateTwoDaysAgo);
            
            NSMutableArray *arrOfImagesNameToDelete=[[NSMutableArray alloc]init];
            NSMutableArray *arrOfDataBeingDeleted=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfSalesLeads.count; k++) {
                
                NSManagedObject *objCoreData =arrOfSalesLeads[k];
                
                NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [objCoreData valueForKey:@"scheduleStartDate"];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                NSDate *scheduleDate = [[NSDate alloc] init];
                scheduleDate = [outputDateFormatter dateFromString:outputString];
                
                NSComparisonResult result = [dateTwoDaysAgo compare:scheduleDate];
                
                if(result == NSOrderedDescending)
                {
                    NSLog(@"dateTwoDaysAgo is Greater");
                    
                    //ZSync No hai to hi delete karna hai
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"leadId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        
                        if (strAudioNameToDelete.length>0 && ![strAudioNameToDelete isEqualToString:@"(null)"]) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDeleteSalesAuto:[objCoreData valueForKey:@"leadId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailSalesAutoToDelete:[objCoreData valueForKey:@"leadId"]]];
                        
                        NSLog(@"images Name to be deleted for Leadssss===%@---%@",[objCoreData valueForKey:@"leadId"],arrOfImagesNameToDelete);
                        
                        [arrLeadIdToDelete addObject:[objCoreData valueForKey:@"leadId"]];
                        
                    }
                    
                }
                else if(result == NSOrderedAscending)
                {
                    NSLog(@"scheduleDate is greater");
                }
                else
                {
                    NSLog(@"Equal date");
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"leadId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        
                        if (strAudioNameToDelete.length>0 && ![strAudioNameToDelete isEqualToString:@"(null)"]) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDeleteSalesAuto:[objCoreData valueForKey:@"leadId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailSalesAutoToDelete:[objCoreData valueForKey:@"leadId"]]];
                        
                        
                        [arrLeadIdToDelete addObject:[objCoreData valueForKey:@"leadId"]];

                    }
                    
                }
                
            }
            
            for (int k=0; k<arrOfImagesNameToDelete.count; k++) {
                
                [self removeImage:arrOfImagesNameToDelete[k]];
                
            }

        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    return arrLeadIdToDelete;
    
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
        NSLog(@"Deleted filePath -:%@ ",filePath);
        
        //        UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        //        [removedSuccessFullyAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
-(NSMutableArray*)fetchPaymentInfoSignToDeleteSalesAuto :(NSString *)strLeadOrderIdDelete
{
    NSMutableArray *arrOfSignImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadOrderIdDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrPaymentInfo = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrPaymentInfo.count==0)
    {
        
        
    }else
    {
        
        for (int k=0; k<arrPaymentInfo.count; k++)
        {
            NSManagedObject *objCoreData =arrPaymentInfo[k];
            
            NSString *strCustomerImage=[objCoreData valueForKey:@"customerSignature"];
            NSString *strSalesPersonImage=[objCoreData valueForKey:@"salesSignature"];
            
            if (!(strCustomerImage.length==0)) {
                
                [arrOfSignImage addObject:strCustomerImage];//salesSignature
                
            }
            if (!(strSalesPersonImage.length==0)) {
                
                [arrOfSignImage addObject:strSalesPersonImage];//salesSignature
                
            }
            
        }
    }
    
    return arrOfSignImage;
}

-(NSMutableArray*)fetchPaymentInfoSignToDelete :(NSString *)strWorkOrderIdDelete
{
    NSMutableArray *arrOfSignImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrPaymentInfo = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrPaymentInfo.count==0)
    {
        
        
    }else
    {
        
        for (int k=0; k<arrPaymentInfo.count; k++)
        {
            NSManagedObject *objCoreData =arrPaymentInfo[k];
            
            NSString *strCheckFrontImage=[objCoreData valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[objCoreData valueForKey:@"checkBackImagePath"];
            
            if (!(strCheckFrontImage.length==0)) {
                
                [arrOfSignImage addObject:strCheckFrontImage];
                
            }
            if (!(strCheckBackImage.length==0)) {
                
                [arrOfSignImage addObject:strCheckBackImage];
                
            }
        }
    }
    
    return arrOfSignImage;
}

-(NSMutableArray*)fetchImageDetailSalesAutoToDelete :(NSString*)strLeaddIdToDelete
{
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeaddIdToDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrObjectImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrObjectImage.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrObjectImage.count; k++)
        {
            NSManagedObject *objCoredata=arrObjectImage[k];
            NSLog(@"LeadIsss IDDDD====%@",[objCoredata valueForKey:@"leadId"]);
            
            NSString *strImageName=[objCoredata valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrImageDetail addObject:strImageName];//salesSignature
                
            }
        }
    }
    
    return arrImageDetail;
    
}

-(NSMutableArray*)fetchOldWOToDeleteFromMobileServiceFlow
{
    NSMutableArray *arrWoIdToDelete = [[NSMutableArray alloc] init];

    @try {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        
        NSString *strConfigFromDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.FromDate"]];
        
        
        NSMutableArray *arrTempAllObj=[[NSMutableArray alloc]init];
        [arrTempAllObj addObjectsFromArray:arrAllObj];
        
        arrAllObj=nil;
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
        requestNewService = [[NSFetchRequest alloc] init];
        [requestNewService setEntity:entityWorkOderDetailServiceAuto];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNewService setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceAutomation setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
        NSArray *arrOfServiceWorkOrderId = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
        //    NSManagedObject *matchesNew;
        if (arrOfServiceWorkOrderId.count==0)
        {
            
            
        }else
        {
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSDate *date = [inputDateFormatter dateFromString:strConfigFromDateServiceAuto];
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *startFromDate = [[NSDate alloc] init];
            startFromDate = [outputDateFormatter dateFromString:outputString];
            
            NSDate *dateTwoDaysAgo = [date dateByAddingTimeInterval:-3*24*60*60];
            NSLog(@"2 days ago: %@", dateTwoDaysAgo);
            
            NSMutableArray *arrOfImagesNameToDelete=[[NSMutableArray alloc]init];
            NSMutableArray *arrOfDataBeingDeleted=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfServiceWorkOrderId.count; k++) {
                
                NSManagedObject *objCoreData =arrOfServiceWorkOrderId[k];
                
                NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [objCoreData valueForKey:@"scheduleStartDateTime"];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                NSDate *scheduleDate = [[NSDate alloc] init];
                scheduleDate = [outputDateFormatter dateFromString:outputString];
                
                NSComparisonResult result = [dateTwoDaysAgo compare:scheduleDate];
                
                if(result == NSOrderedDescending)
                {
                    NSLog(@"dateTwoDaysAgo is Greater");
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"workorderId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        NSString *strCustSignToDelete=[objCoreData valueForKey:@"customerSignaturePath"];
                        NSString *strTechSignToDelete=[objCoreData valueForKey:@"technicianSignaturePath"];
                        
                        if (strAudioNameToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        if (strCustSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strCustSignToDelete];
                            
                        }
                        if (strTechSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strTechSignToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailServiceAutoToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        NSLog(@"images Name to br deleted for WorkOrderId===%@---%@",[objCoreData valueForKey:@"workorderId"],arrOfImagesNameToDelete);
                        
                        [arrWoIdToDelete addObject:[objCoreData valueForKey:@"workorderId"]];

                    }
                }
                else if(result == NSOrderedAscending)
                {
                    NSLog(@"scheduleDate is greater");
                }
                else
                {
                    NSLog(@"Equal date");
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"workorderId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        NSString *strCustSignToDelete=[objCoreData valueForKey:@"customerSignaturePath"];
                        NSString *strTechSignToDelete=[objCoreData valueForKey:@"technicianSignaturePath"];
                        
                        if (strAudioNameToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        if (strCustSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strCustSignToDelete];
                            
                        }
                        if (strTechSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strTechSignToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailServiceAutoToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        
                        [arrWoIdToDelete addObject:[objCoreData valueForKey:@"workorderId"]];
                        
                    }
                }
                
            }
            
            for (int k=0; k<arrOfImagesNameToDelete.count; k++) {
                
                [self removeImage:arrOfImagesNameToDelete[k]];
                
            }
            NSLog(@"images Name to be deleted for Leadssss===---%@",arrOfImagesNameToDelete);

            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    return arrWoIdToDelete;
}

-(NSMutableArray*)fetchImageDetailServiceAutoToDelete :(NSString*)strWorkOrderIdToDelete
{
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdToDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrObjectImage = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrObjectImage.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrObjectImage.count; k++)
        {
            NSManagedObject *objCoredata=arrObjectImage[k];
            NSLog(@"WorkOrderID IDDDD====%@",[objCoredata valueForKey:@"workorderId"]);
            
            NSString *strImageName=[objCoredata valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrImageDetail addObject:strImageName];//salesSignature
                
            }
        }
    }
    
    return arrImageDetail;
    
}

#pragma mark- -- Fetch Service Details --

-(NSMutableDictionary*)fetchServiceDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strWoId :(NSString*)strWoNo{
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strWorkOrderIdGlobal = strWoId;
    strModifyDateToSendToServerAll = strModifyDateToSendToServer;
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllDocumentsToSend = [[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend = [[NSMutableArray alloc]init];
    arrOfAllProblemImageToSendToServer = [[NSMutableArray alloc]init];
    arrOfAllGraphXmlToSendToServer = [[NSMutableArray alloc]init];

    dictFinal=[[NSMutableDictionary alloc]init];
    [self fetchWorkOrderDetailServiceAuto];
    [self fetchWOProductDetail];
    [self fetchPaymentInfoServiceAuto];
    [self fetchTermiteFromDb];
    [self fetchTermiteFloridaFromDb];
    [self fetchImageDetailServiceAuto];
    [self fetchEquipmentDetailServiceAuto];
    [self fetchEmailDetailServiceAuto];
    [self fetchWorkOrderDocuments];
    [self fetchTermiteImageDetailServiceAuto];
    
    [self fetchServicePestDataAllFromDBObjC];
    
    GeneralInfoFinalJSONSyncingDBFuncs *objSyncgenenral = [[GeneralInfoFinalJSONSyncingDBFuncs alloc] init];
    NSDictionary *dictStartStop =  [objSyncgenenral fetchWOServiceStartStopDictWithStrWoId: strWoId];
    NSDictionary *dictServiceCat =  [objSyncgenenral fetchWOServiceCategoryGeneralInfoWithStrWoId: strWoId strWoNo:strWoNo];
    NSDictionary *dictDeviceDoc = [objSyncgenenral fetchServiceDeviceDocumentsDcWithStrWoId:strWoId];
    NSDictionary *dictAreaDoc = [objSyncgenenral fetchServiceAreaDocumentsDcWithStrWoId:strWoId];

    [dictFinal addEntriesFromDictionary:dictStartStop];
    [dictFinal addEntriesFromDictionary:dictServiceCat];
    [dictFinal addEntriesFromDictionary:dictDeviceDoc];
    [dictFinal addEntriesFromDictionary:dictAreaDoc];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
     
        // For WDO and NPMA
        
        [self fetchWDODataAllFromDBObjC];
        [self fetchProblemImageDetail];
        
        [self fetchNpmaTermiteDataAllFromDBObjC];
        
    }
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:arrOfAllDocumentsToSend forKey:@"arrOfAllDocumentsToSend"];
    [dictTemp setValue:arrOfAllImagesToSendToServer forKey:@"arrOfAllImagesToSendToServer"];
    [dictTemp setValue:arrOfAllSignatureImagesToSendToServer forKey:@"arrOfAllSignatureImagesToSendToServer"];
    [dictTemp setValue:arrOfAllCheckImageToSend forKey:@"arrOfAllCheckImageToSend"];
    [dictTemp setValue:strAudioNameGlobal forKey:@"strAudioNameGlobal"];
    [dictTemp setValue:serviceAddressImagePath forKey:@"serviceAddressImagePath"];
    [dictTemp setValue:serviceAddressImagePath forKey:@"serviceAddressImagePath"];
    [dictTemp setValue:arrOfAllProblemImageToSendToServer forKey:@"arrOfAllProblemImageToSendToServer"];
    [dictTemp setValue:arrOfAllGraphXmlToSendToServer forKey:@"arrOfAllGraphXmlToSendToServer"];
    [dictTemp setValue:dictFinal forKey:@"dictFinal"];
    
    return dictTemp;
    
}

#pragma mark- fetching for service Automation

-(void)fetchTermiteImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetailsTermite];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfTermiteImages;
    arrOfTermiteImages=[[NSMutableArray alloc]init];
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerImageDetail fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetailsTermite"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"TexasTermiteImagesDetail"];
        
    }
}
-(void)fetchWorkOrderDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"WorkorderDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            serviceAddressImagePath = [matches valueForKey:@"serviceAddressImagePath"];
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            
            
            NSString *strCustomerSign=[matches valueForKey:@"customerSignaturePath"];
            NSString *strTechSign=[matches valueForKey:@"technicianSignaturePath"];
            
            if (!(strCustomerSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
            }
            
            if (!(strTechSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
            }
            
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedFormatedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            // [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            
            //Date ka bhut bada change kara hai yaha  p bhai yad rakhna [global modifyDateService]
            //  [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:[global modifyDateService]];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            // [arrKeyTemp replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:@"ModifiedDate"];
            
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            NSLog(@"%@",dictTemp);
            // Commit.
            
            //IsNPMATermite
            
            NSString *strBoolKey = [NSString stringWithFormat:@"%@",[dictTemp valueForKey:@"isNPMATermite"]];
            
            BOOL isTrue = [strBoolKey boolValue];
            
            if (isTrue) {
                
                [dictTemp setValue:@"true" forKey:@"isNPMATermite"];
                
            } else {
                
                [dictTemp setValue:@"false" forKey:@"isNPMATermite"];
                
            }
            
            // Set Is Mail Sent True For Sending Mail.//Commit.
            //commit
            //commi

            [dictTemp setValue:@"false" forKey:@"isMailSent"];
            
            [dictFinal setObject:dictTemp forKey:@"WorkorderDetail"];
            
        }
    }
}
-(void)fetchWOProductDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWOProductServiceAuto= [NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
    [request setEntity:entityWOProductServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"WoProductDetail"];
        
    }else
    {
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        // matches=arrAllObj[k];
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        //            for (int k=0; k<arrAllObj.count; k++)
        //            {
        matches=arrAllObj[0];
        
        [dictFinal setObject:[matches valueForKey:@"chemicalList"] forKey:@"WoProductDetail"];
        
        [dictFinal setObject:[matches valueForKey:@"otherChemicalList"] forKey:@"WoOtherProductDetail"];
        
        //            }
        
        //        }
    }
}

-(void)fetchPaymentInfoServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
                
                NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
                NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
                
                if (!(strCheckFrontImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
                    
                }
                if (!(strCheckBackImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
                    
                }
                
                NSArray *arrPaymentInfoKey;
                NSMutableArray *arrPaymentInfoValue;
                
                arrPaymentInfoValue=[[NSMutableArray alloc]init];
                arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrPaymentInfoKey);
                
                NSLog(@"all keys %@",arrPaymentInfoValue);
                for (int i=0; i<arrPaymentInfoKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                    [arrPaymentInfoValue addObject:str];
                }
                
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                int indexToRemove=-1;
                int indexToReplaceModifyDate=-1;
                
                for (int k=0; k<arrPaymentInfoKey.count; k++) {
                    
                    NSString *strKeyLeadId=arrPaymentInfoKey[k];
                    
                    if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                        
                        indexToRemove=k;
                        
                    }
                    if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                        
                        indexToReplaceModifyDate=k;
                        
                    }
                }
                
                NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
                
                [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
                
                [arrKeyTemp removeObjectAtIndex:indexToRemove];
                arrPaymentInfoKey=arrKeyTemp;
                [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
                
                [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
                NSLog(@"PaymentInfo%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
            }
            
        }
    }
}


-(void)fetchTermiteFromDb{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        //        NSMutableArray *arrTexasTermiteServiceDetail;
        //
        //        arrTexasTermiteServiceDetail=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:@"" forKey:@"TexasTermiteServiceDetail"];
        
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"];
        
        
        NSString *strBuyersInitials_SignaturePath_10B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
        if (!(strBuyersInitials_SignaturePath_10B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_10B];
            
        }
        
        
        NSString *strGraphPath__TexasOffiicial_WoodDestroyingInsect=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
        if (!(strGraphPath__TexasOffiicial_WoodDestroyingInsect.length==0)){
            
            [arrOfAllImagesToSendToServer addObject:strGraphPath__TexasOffiicial_WoodDestroyingInsect];
            
            
        }
        
        
        NSString *strInspector_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
        if (!(strInspector_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strInspector_SignaturePath];
            
        }
        
        
        NSString *strCertifiedApplicator_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
        if (!(strCertifiedApplicator_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strCertifiedApplicator_SignaturePath];
            
        }
        
        
        NSString *strSignatureOfPurchaserOfProperty_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
        if (!(strSignatureOfPurchaserOfProperty_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureOfPurchaserOfProperty_SignaturePath];
            
        }
        
        
        NSString *strBuyersInitials_SignaturePath_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
        if (!(strBuyersInitials_SignaturePath_7B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_7B];
            
        }
        
        [dictFinal setObject:[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"] forKey:@"TexasTermiteServiceDetail"];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchTermiteFloridaFromDb
{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailFlorida;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    requestTermiteFlorida = [[NSFetchRequest alloc] init];
    [requestTermiteFlorida setEntity:entityTermiteFlorida];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestTermiteFlorida setPredicate:predicate];
    
    sortDescriptorTermiteFlorida = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsTermiteFlorida = [NSArray arrayWithObject:sortDescriptorTermiteFlorida];
    
    [requestTermiteFlorida setSortDescriptors:sortDescriptorsTermiteFlorida];
    
    fetchedResultsControllerWorkOrderDetailFlorida = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTermiteFlorida managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailFlorida setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailFlorida performFetch:&error];
    arrAllObjTermiteFlorida = [fetchedResultsControllerWorkOrderDetailFlorida fetchedObjects];
    if ([arrAllObjTermiteFlorida count] == 0)
    {
        
        [dictFinal setObject:@"" forKey:@"FloridaTermiteServiceDetail"];
        
    }
    else
    {
        
        matchesWorkOrderFlorida=arrAllObjTermiteFlorida[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"];
        
        NSString *strSignatureLicenseAgrent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfLicenseOrAgent"]];
        if (!(strSignatureLicenseAgrent.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureLicenseAgrent];
            
        }
        
        [dictFinal setObject:[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"] forKey:@"FloridaTermiteServiceDetail"];
        
    }
    
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            // Add Graph Xml in Array to send it to server
            
            NSString *strIsProblemId=[matches valueForKey:@"isProblemIdentifaction"];
            
            if ([strIsProblemId isEqualToString:@"true"] || [strIsProblemId isEqualToString:@"True"] || [strIsProblemId isEqualToString:@"1"]) {
                
                [arrOfAllGraphXmlToSendToServer addObject:[NSString stringWithFormat:@"%@",[matches valueForKey:@"graphXmlPath"]]];
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}


-(void)fetchWorkOrderDocuments{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOrderDocuments= [NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
    [request setEntity:entityWorkOrderDocuments];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptorWorkOrderDocuments = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptorsWorkOrderDocuments = [NSArray arrayWithObject:sortDescriptorWorkOrderDocuments];
    
    [request setSortDescriptors:sortDescriptorsWorkOrderDocuments];
    
    self.fetchedResultsWorkOrderDocuments = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsWorkOrderDocuments setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsWorkOrderDocuments performFetch:&error1];
    arrAllObjWorkOrderDocuments = [self.fetchedResultsWorkOrderDocuments fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObjWorkOrderDocuments.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }else
    {
        
        for (int k=0; k<arrAllObjWorkOrderDocuments.count; k++)
        {
            matchesWorkOrderDocuments=arrAllObjWorkOrderDocuments[k];
            NSLog(@"Lead IDDDD====%@",[matchesWorkOrderDocuments valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesWorkOrderDocuments entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"WoOtherDocuments%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }
}

-(void)fetchEmailDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchEquipmentDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"WoEquipmentDetail"];
        
    }else
    {
        
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        matches=arrAllObj[0];
        NSArray *arrToSend=[matches valueForKey:@"arrOfEquipmentList"];
        
        NSArray *objValue=[NSArray arrayWithObjects:
                           arrToSend,
                           arrToSend,nil];
        NSArray *objKey=[NSArray arrayWithObjects:
                         @"WOEquipmentDcs",
                         @"WOEquipmentDc",nil];
        
        NSMutableDictionary *dictTemp;
        dictTemp=[[NSMutableDictionary alloc]init];
        dictTemp=[NSMutableDictionary dictionaryWithObjects:objValue forKeys:objKey];
        
        //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
        //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
        NSLog(@"%@",dictTemp);
        [dictFinal setObject:arrToSend forKey:@"WoEquipmentDetail"];
        
        //        }
    }
}

#pragma mark- Service Pest New Flow Fetch Methods

-(void)fetchServicePestDataAllFromDBObjC{
    
    // Areas
    
    NSString *strServiceAddressIdTemp;
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];

    WebService *objWebService = [[WebService alloc] init];

    NSArray *arrTempData = [objWebService getDataFromCoreDataBaseArrayObjectiveCWithStrEntity:@"WorkOrderDetailsService" predicate:predicate];

    if (arrTempData.count>0) {
        
        NSManagedObject *objTemp = arrTempData[0];

        strServiceAddressIdTemp = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"serviceAddressId"]];
        
    }
    
    
    NSMutableArray *arrTemp = [self fetchServicePestNewFlowData:@"ServiceAreas" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceAreas"];
    
    // ServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDevices" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceDevices"];
    
    // ServiceProducts
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceProducts" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceProducts"];
    
    // ServicePests
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServicePests" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServicePests"];
    
    // ServiceConditions
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditions" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditions"];
    
    // ServiceComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceComments"];
    
    
    // ServiceConditionComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionComments"];
    
    
    // ServicePestDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"SAPestDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"SAPestDocuments"];
    
    
    // ServiceConditionDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionDocuments"];
    
    // ServiceDeviceInspection
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDeviceInspections" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceDeviceInspections"];
    
    // WOServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceDevices" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceDevices"];
    
    // WOServiceAreas
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceAreas" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceAreas"];
}


-(NSMutableArray*)fetchServicePestNewFlowData :(NSString*)strEntityName :(NSString*)strIsDocument :(NSString*)strServiceAddressIdForAreaDevice
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strWorkOrderIdGlobal];
    
    if (strServiceAddressIdForAreaDevice.length>0){
        
        predicate =[NSPredicate predicateWithFormat:@"serviceAddressId=%@",strServiceAddressIdForAreaDevice];
        
    }
    
    [request setPredicate:predicate];
    
    NSSortDescriptor * sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerServicePestNewFlowArea = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServicePestNewFlowArea setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServicePestNewFlowArea performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerServicePestNewFlowArea fetchedObjects];
    
    NSMutableArray *arrData;
    arrData=[[NSMutableArray alloc]init];
    
    if (ObjAllData.count==0)
    {
        
        //[dictFinal setObject:arrData forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[k];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            if ([strIsDocument isEqualToString:@"Yes"]) {
                
                NSString *isToUpload = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"isToUpload"]];
                
                if ([isToUpload isEqualToString:@"Yes"]) {
                    
                    [arrOfAllDocumentsToSend addObject:dictOfData];
                    
                }
                
            }
            
            NSArray *arrOfKey = [dictOfData allKeys];
            
            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"isInspectingFirstTime"] || [strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsDeletedArea"] || [strBoolKey isEqualToString:@"IsInspected"] || [strBoolKey isEqualToString:@"IsDeletedDevice"] || [strBoolKey isEqualToString:@"Activity"] || [strBoolKey isEqualToString:@"isDeletedDevice"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];

                    }else{
                        
                        if ([arrOfValue[k] isKindOfClass:[NSNull class]]) {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }else{
                            
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                            
                        }
                        
                    }

                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            
            //NSArray *arrOfValue = [dictOfData allValues];
            
            NSDictionary *dictData;
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
            [arrData addObject:dictData];
            
        }
        
    }
    
    return arrData;
    
}

-(NSArray*)fetchDataAppointment :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrOfTasks :(NSString*)isToday
{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    BOOL isTodayAppointments;
    
    if ([isToday isEqualToString:@"yes"]) {
        isTodayAppointments = true;
    } else {
        isTodayAppointments = false;
    }
    
    isTodayAppointments = true;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo_Mobile=%@",strEmployeeNo];

    [requestNew setPredicate:predicate];
    
    if (isTodayAppointments) {
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
        
    } else {
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
        
        if (isSortByScheduleDate) {
            
            BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
            
            if (isSortByScheduleDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
                
            }
            
        } else {
            
            BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
            
            if (isSortByModifiedDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
                
            }
            
        }
        
    }

    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        if (isTodayAppointments) {
            
            NSMutableArray *arrTodayDates=[[NSMutableArray alloc]init];
            
            NSMutableArray *arrComplete=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
                
                NSString *strStage=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
                BOOL chkProposal=NO;
                if ([strStatusComplete caseInsensitiveCompare:@"Open"] == NSOrderedSame && [strStage caseInsensitiveCompare:@"Proposed"] == NSOrderedSame)
                {
                    chkProposal=YES;
                }
                
                if ([strStatusComplete caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Completed"] == NSOrderedSame || chkProposal) {
                    
                    [arrComplete addObject:arrAllObj[k]];
                    
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:[matches valueForKey:@"scheduleStartDate"]];
                
                // NSComparisonResult result = [[NSDate date] compare:dateFromString];
                
                BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
                
                if (yesSameDay) {
                    
                    [arrTodayDates addObject:arrAllObj[k]];
                    
                    NSLog(@"Yes Today date=====%@",[matches valueForKey:@"scheduleStartDate"]);
                    
                }
            }
            if (!(arrComplete.count==0)) {
                
                [arrTodayDates removeObjectsInArray:arrComplete];
                
            }
            arrAllObj=arrTodayDates;
        }
        
        // Settings View Change
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isAllAppoitments = [defsApp boolForKey:@"AllAppointments"];
        
        if ((!isTodayAppointments) && (!isAllAppoitments)) {
            
            NSMutableArray *arrSettingStatus=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsAppp =[NSUserDefaults standardUserDefaults];
            
            NSArray *tempArr = [defsAppp objectForKey:@"AppointmentStatus"];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
                
                for (int k1=0; k1<tempArr.count; k1++) {
                    
                    NSString *statusLocal = tempArr[k1];
                    
                    if ([statusLocal caseInsensitiveCompare:strStatusComplete] ==NSOrderedSame) {
                        
                        [arrSettingStatus addObject:arrAllObj[k]];
                        
                        break;
                    }
                    
                }
                
            }
            
            if (arrSettingStatus.count>0) {
                
                arrAllObj=arrSettingStatus;
                
            }else{
                
                arrAllObj = nil;
                
            }
        }
    }
    
    [DejalBezelActivityView removeViewAnimated:NO];
        
    return [self ServiceAutomationFetch:isTodayAppointments :arrOfEmpBlockTime : arrOfTasks];
    
}


-(NSArray*)ServiceAutomationFetch :(BOOL)isTodayAppointments :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrOfTasks
{
    
    NSMutableArray *arrTempAllObj=[[NSMutableArray alloc]init];
    [arrTempAllObj addObjectsFromArray:arrAllObj];
    
    arrAllObj=nil;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"dbUserName=%@",strEmployeeNo];
    
    [requestNewService setPredicate:predicate];
    
    // fetching from settings
    
    if (isTodayAppointments) {
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
        
    } else {
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
        
        if (isSortByScheduleDate) {
            
            BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
            
            if (isSortByScheduleDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
                
            }
            
        } else {
            
            BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
            
            if (isSortByModifiedDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
                
            }
            
        }
        
    }
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        if (isTodayAppointments) {
            
            NSMutableArray *arrTodayDates=[[NSMutableArray alloc]init];
            
            NSMutableArray *arrComplete=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderStatus"]];
                if ([strStatusComplete caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                    
                    [arrComplete addObject:arrAllObj[k]];
                    
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:[matches valueForKey:@"scheduleStartDateTime"]];
                                
                BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
                
                if (yesSameDay) {
                    
                    [arrTodayDates addObject:arrAllObj[k]];
                    
                    NSLog(@"Yes Today date=====%@",[matches valueForKey:@"scheduleStartDateTime"]);
                    
                }
            }
            
            if (!(arrComplete.count==0)) {
                
                [arrTodayDates removeObjectsInArray:arrComplete];
                
            }
            
            arrAllObj=arrTodayDates;
        }
        
    }
        
    // Settings View Change
    
    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isAllAppoitments = [defsApp boolForKey:@"AllAppointments"];
    
    if ((!isTodayAppointments) && (!isAllAppoitments)) {
        
        NSMutableArray *arrSettingStatus=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defsAppp =[NSUserDefaults standardUserDefaults];
        
        NSArray *tempArr = [defsAppp objectForKey:@"AppointmentStatus"];
        
        for (int k=0; k<arrAllObj.count; k++) {
            
            matches=arrAllObj[k];
            
            NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderStatus"]];
            
            for (int k1=0; k1<tempArr.count; k1++) {
                
                NSString *statusLocal = tempArr[k1];
                
                if ([strStatusComplete caseInsensitiveCompare:@"Completed"] ==NSOrderedSame) {
                    
                    strStatusComplete=@"Complete";
                    
                }
                
                if ([statusLocal caseInsensitiveCompare:strStatusComplete] ==NSOrderedSame) {
                    
                    [arrSettingStatus addObject:arrAllObj[k]];
                    
                    break;
                }
                
            }
            
        }
        if (arrSettingStatus.count>0) {
            
            [arrTempAllObj addObjectsFromArray:arrSettingStatus];
            
        }
    }else{
        
        [arrTempAllObj addObjectsFromArray:arrAllObj];
        
    }
    
    
    arrAllObj=arrTempAllObj;
    
    [DejalBezelActivityView removeViewAnimated:YES];
    
    return [self filterAllAppointmentsIncludingBlockTime:isTodayAppointments :arrOfEmpBlockTime : arrOfTasks];
    
}

-(NSArray*)filterAllAppointmentsIncludingBlockTime :(BOOL)isTodayAppointments :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrOfTasks{
    
    NSMutableArray *arrTodayBlockDates = [[NSMutableArray alloc]init];
    
    NSMutableArray *arrTodayTaskss = [[NSMutableArray alloc]init];

    if (isTodayAppointments) {
        
        for (int P=0; P<arrOfEmpBlockTime.count; P++) {
            //2019-04-03T11:00:00
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate *dateFromString = [[NSDate alloc] init];
            NSDictionary *dictData = arrOfEmpBlockTime[P];
            dateFromString = [dateFormatter dateFromString:[dictData valueForKey:@"fromDate"]];
            
            BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
            
            if (yesSameDay) {
                
                [arrTodayBlockDates addObject:dictData];
                
            }
            
        }
        
        // Task List
        
        for (int P=0; P<arrOfTasks.count; P++) {
            //2019-04-03T11:00:00
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate *dateFromString = [[NSDate alloc] init];
            NSManagedObject *dictData = arrOfTasks[P];
            dateFromString = [dateFormatter dateFromString:[dictData valueForKey:@"fromDate"]];
            
            BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
            
            NSString *strStatusLocal = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"status"]];
            
            if ((yesSameDay) && ([strStatusLocal isEqualToString:@"Open"] || [strStatusLocal isEqualToString:@"open"])) {
                
                [arrTodayBlockDates addObject:dictData];
                
            }
            
        }
        
    }else{
        
        [arrTodayBlockDates addObjectsFromArray:arrOfEmpBlockTime];
        
    }
    
    NSMutableArray *arrTemp =[[NSMutableArray alloc]init];
    NSArray *arrTempFiltered =[[NSArray alloc]init];
    
    //[arrTemp addObjectsFromArray:arrAllObj];
    
    for (int k=0; k<arrTodayBlockDates.count; k++) {
        
        [arrTemp addObject:arrTodayBlockDates[k]];
        
    }
    for (int k=0; k<arrAllObj.count; k++) {
        
        [arrTemp addObject:arrAllObj[k]];
        
    }
    
    for (int k=0; k<arrTodayTaskss.count; k++) {
        
        [arrTemp addObject:arrTodayTaskss[k]];
        
    }
    
    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
    
    if (isSortByScheduleDate) {
        
        BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
        
        if (isSortByScheduleDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :NO];
            
        }
        
    } else {
        
        BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
        
        if (isSortByModifiedDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        }
        
    }
    
    arrAllObj = nil;
    
    arrAllObj = arrTempFiltered;
       
    return arrAllObj;
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(NSArray*)arrFilter :(NSArray*)arrToFilter :(NSString*)strKey :(BOOL)yesNO {
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: strKey ascending: yesNO];
    NSArray *sortedArray = [arrToFilter sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return sortedArray;
    
}

-(NSMutableDictionary*)dictdates {
    
    NSDate *fromDateMinimum,*toDateMaximum;

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSString *strConfigFromDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.FromDate"]];
    
    NSString *strConfigFromDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.FromDate"]];
    
    NSString *strConfigTooDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ToDate"]];
    
    NSString *strConfigTooDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ToDate"]];
    

    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    [dateFormat setTimeZone:outputTimeZone];
    NSDate *fromDateSalesAuto=[dateFormat dateFromString:strConfigFromDateSalesAuto];
    NSDate *fromDateServiceAuto=[dateFormat dateFromString:strConfigFromDateServiceAuto];
    NSDate *tooDateSalesAuto=[dateFormat dateFromString:strConfigTooDateSalesAuto];
    NSDate *tooDateServiceAuto=[dateFormat dateFromString:strConfigTooDateServiceAuto];
    
    BOOL isFromDateSalesGreater=[global isGreaterDate:fromDateSalesAuto :fromDateServiceAuto];
    BOOL isTooDateSalesGreater=[global isGreaterDate:tooDateSalesAuto :tooDateServiceAuto];
    
    //Temp Comment
    /*if (isFromDateSalesGreater) {
        fromDateMinimum= fromDateSalesAuto;
    } else {
        fromDateMinimum= fromDateServiceAuto;
    }
    
    if (isTooDateSalesGreater) {
        toDateMaximum=tooDateSalesAuto;
    } else {
        toDateMaximum=tooDateServiceAuto;
    }*/
    
    //Nilind 24 Sept 2021
    
    if ([fromDateSalesAuto compare:fromDateServiceAuto] == NSOrderedAscending) {
        fromDateMinimum= fromDateSalesAuto;
    }
    else if ([fromDateSalesAuto compare:fromDateServiceAuto] == NSOrderedDescending) {
        fromDateMinimum= fromDateServiceAuto;
    }
    else {
        fromDateMinimum= fromDateSalesAuto;
    }
    

    
    if ([tooDateSalesAuto compare:tooDateServiceAuto] == NSOrderedAscending) {
        toDateMaximum=tooDateServiceAuto;
    }
    if ([tooDateSalesAuto compare:tooDateServiceAuto] == NSOrderedDescending) {
        toDateMaximum=tooDateSalesAuto;
    }
    else {
        toDateMaximum=tooDateServiceAuto;
    }
    //End
    
    
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:fromDateMinimum forKey:@"fromDateMinimum"];
    [dictTemp setValue:toDateMaximum forKey:@"toDateMaximum"];
    
    //Nilind
    NSUserDefaults *defsDates = [NSUserDefaults standardUserDefaults];
    [defsDates setValue:fromDateServiceAuto forKey:@"fromDateServiceAuto"];
    [defsDates setValue:tooDateServiceAuto forKey:@"tooDateServiceAuto"];
    [defsDates synchronize];
    //End


    return dictTemp;
    
}

-(NSMutableArray*)fetchSubWorkOrderFromDB :(NSArray*)arrWorkOrderId{
    
    NSMutableArray *arrOfSubWorkOrderToFetchDynamicData = [[NSMutableArray alloc] init];
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    for (int i=0; i<arrWorkOrderId.count; i++) {
        
        entityWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context1];
        requestNewWorkOrder = [[NSFetchRequest alloc] init];
        [requestNewWorkOrder setEntity:entityWorkOrder];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",arrWorkOrderId[i]];
        
        [requestNewWorkOrder setPredicate:predicate];
        
        sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
        sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
        
        [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
        
        fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
        [fetchedResultsControllerWorkOrderDetails setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [fetchedResultsControllerWorkOrderDetails performFetch:&error];
        arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
        if ([arrAllObjWorkOrder count] == 0)
        {
            
        }
        else
        {
            
            for (int i=0; i<arrAllObjWorkOrder.count; i++) {
                
                matchesWorkOrder=arrAllObjWorkOrder[i];
                
                NSString *strSubWorkOrderId=[NSString stringWithFormat:@"%@",[matchesWorkOrder valueForKey:@"subWorkOrderId"]];
                
                [arrOfSubWorkOrderToFetchDynamicData addObject:strSubWorkOrderId];
                
            }

        }
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        } else {
        }
        
    }
    
    return arrOfSubWorkOrderToFetchDynamicData;
    
}

// 03 August Service Basic Api Implementation

-(NSMutableArray*)checkifToSendPlumbingAppointmentToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjServiceModifyDate{
 
    
    NSManagedObject *matchesServiceModifyDate;
    NSMutableArray *arrOfLeadsToFetchFromServer=[[NSMutableArray alloc]init];

    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjServiceModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"workorderId"]];
        
    }
    
    //NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
    for (int i=0; i<arrOfAppoint.count; i++)
    {
        
        NSDictionary *dictLeadDetail=[arrOfAppoint objectAtIndex:i];

        NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"WorkorderId"]];
        
        [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
        
        if ([strLeadIdCompare isEqualToString:@"754"]) {
            
            NSLog(@"strLeadIdCompare======%@",strLeadIdCompare);
            
        }
        
        //NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"ModifiedDate"]];
        NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKey:@"ModifiedFormatedDate"]];

        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                NSString *strModifyLeadModifyDate=[matchesServiceModifyDate valueForKey:@"modifiedFormatedDate"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                
                if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                    NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                    //                            NSDate *add90Min = [ServerModifyDate dateByAddingTimeInterval:(330*60)];
                    //                            ServerModifyDate = add90Min;
                    
                    NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                    [inputDateFormatter setTimeZone:inputTimeZone];
                    [inputDateFormatter setDateFormat:dateFormat];
                    
                    NSString *inputString = strModifyDateCompare;
                    NSDate *date = [inputDateFormatter dateFromString:inputString];
                    
                    NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                    [outputDateFormatter setTimeZone:outputTimeZone];
                    [outputDateFormatter setDateFormat:dateFormat];
                    NSString *outputString = [outputDateFormatter stringFromDate:date];
                    
                    ServerModifyDate = [inputDateFormatter dateFromString:outputString];
                    
                    NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                    
                    if(result == NSOrderedDescending)
                    {
                        NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                        //[arrOfLeadToSend addObject:strModifyLeadIdd];
                        
                        //change for is sync only on 27 july 2017
                        
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                        }
                        
                    }
                    else if(result == NSOrderedAscending)
                    {
                        NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            [arrOfLeadsToFetchFromServer addObject:strModifyLeadIdd];

                        }
                        
                        [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                    }
                    else
                    {
                        NSLog(@"Equal date");//
                        BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                        if (isSync) {
                            
                            [arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                        }else{
                            [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                        }
                        
                    }
                }else{
                    [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                }
            }
            
            [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
        }
    }
    
    
    // Logic to fectch leads whicch are coming from server but not there in Local DB.
    //  arrOfWorkOrderIdComingFromServer   arrOfAlreadySavedLeadss
    
    NSMutableArray *arrTempTest1 = [[NSMutableArray alloc]init];
    [arrTempTest1 addObjectsFromArray:arrOfWorkOrderIdComingFromServer];
    //arrTempTest1 = arrOfWorkOrderIdComingFromServer;
    NSMutableArray *arrTempTest2 = arrOfAlreadySavedLeadss;
    [arrTempTest1 removeObjectsInArray:arrTempTest2];
    
    arrOfLeadsToFetchFromServer = [[arrOfLeadsToFetchFromServer arrayByAddingObjectsFromArray:arrTempTest1] mutableCopy];
    
    NSOrderedSet *arrTempTest3 = [NSOrderedSet orderedSetWithArray:arrOfLeadsToFetchFromServer];
    NSArray *arrTempTest4 = [arrTempTest3 array];
    
    arrOfLeadsToFetchFromServer = [[NSMutableArray alloc]init];
    [arrOfLeadsToFetchFromServer addObjectsFromArray:arrTempTest4];
    
    //Change if No workorder from web
    
    if (arrOfAppoint.count==0) {
        
        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    
    //Change To Delete void and tech Changed Wali
    
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendServiceLeadToServerMechanical"];
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveServiceLeadToLocalDbnDeleteMechanical"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHowServiceMechanical"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBServiceMechanical"];
    [defs setObject:arrOfLeadsToFetchFromServer forKey:@"arrOfWorkOrdersToFetchFromServer"];
    [defs synchronize];

    
    NSMutableArray *arrOfServiceLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    return arrOfServiceLeadToFetchDynamicData;
        
}

-(NSMutableArray*)checkifToSendPlumbingAppointmentToServer : (NSDictionary*)dictForWorkOrderAppointment :(NSArray*)arrAllObjServiceModifyDate{
 
    
    NSManagedObject *matchesServiceModifyDate;

    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
        
        NSManagedObject *temp =arrAllObjServiceModifyDate[k];
        
        [arrOfAlreadySavedLeadss addObject:[temp valueForKey:@"workorderId"]];
        
    }
    
    NSArray *arrLeadExtSerDcs=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];
    for (int i=0; i<arrLeadExtSerDcs.count; i++)
    {
        NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
        if(![[dictLeadDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
        {
            
        }
        else
        {
            
            NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
            
            [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
            
            if ([strLeadIdCompare isEqualToString:@"754"]) {
                
                NSLog(@"strLeadIdCompare======%@",strLeadIdCompare);
                
            }
            
            NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.ModifiedFormatedDate"]];
            
            if (!(arrAllObjServiceModifyDate.count==0)) {
                
                for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                    
                    matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                    
                    NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                    NSString *strModifyLeadModifyDate=[matchesServiceModifyDate valueForKey:@"modifyDate"];
                    
                    [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                    
                    if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                        NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                        NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                        //                            NSDate *add90Min = [ServerModifyDate dateByAddingTimeInterval:(330*60)];
                        //                            ServerModifyDate = add90Min;
                        
                        NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                        NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                        NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                        [inputDateFormatter setTimeZone:inputTimeZone];
                        [inputDateFormatter setDateFormat:dateFormat];
                        
                        NSString *inputString = strModifyDateCompare;
                        NSDate *date = [inputDateFormatter dateFromString:inputString];
                        
                        NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                        [outputDateFormatter setTimeZone:outputTimeZone];
                        [outputDateFormatter setDateFormat:dateFormat];
                        NSString *outputString = [outputDateFormatter stringFromDate:date];
                        
                        ServerModifyDate = [dateFormatter dateFromString:outputString];
                        
                        NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                        
                        if(result == NSOrderedDescending)
                        {
                            NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                            //[arrOfLeadToSend addObject:strModifyLeadIdd];
                            
                            //change for is sync only on 27 july 2017
                            
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                        else if(result == NSOrderedAscending)
                        {
                            NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                            [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                        }
                        else
                        {
                            NSLog(@"Equal date");//
                            BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                            if (isSync) {
                                
                                [arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                            }else{
                                [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                            }
                            
                        }
                    }else{
                        [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                    }
                }
                
                [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
            }
        }
    }
    
    
    //Change if No workorder from web
    
    if (arrLeadExtSerDcs.count==0) {
        
        if (!(arrAllObjServiceModifyDate.count==0)) {
            
            for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                
                matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                
                NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"workorderId"];
                
                [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
            }
        }
    }
    
    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
    NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
    
    NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
    [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
    
    for (int kk=0; kk<arrOfWOModify.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWOModify[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWOModify[kk]];
            
        }
    }
    
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
    NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
    
    
    //Change To Delete void and tech Changed Wali
    
    
    NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
    for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
    {
        if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
        {
            [arrayListToSendWorkOrdernDelete addObject:speakerID];
        }
    }
    
    NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
    
    [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
    
    [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    
    arrOfWorkOrderIdinModifyDateunique=nil;
    
    arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
    
    for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
        
        BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
        if (isSync) {
            
            [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
            
        }
    }
    
    NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
    arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSArray *aRRTemps=[orderedSetNew array];
    [arrOfLeadToSend addObjectsFromArray:aRRTemps];
    
    NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
    [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadToSaveInDbandDelete=nil;
    arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
    
    
    NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
    [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
    [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
    arrOfLeadsToSaveeAnyHow=nil;
    arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:arrOfLeadToSend forKey:@"sendServiceLeadToServerMechanical"];
    [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveServiceLeadToLocalDbnDeleteMechanical"];
    [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHowServiceMechanical"];
    [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBServiceMechanical"];
    [defs synchronize];
    
    NSMutableArray *arrOfServiceLeadToFetchDynamicData = [[NSMutableArray alloc] init];
    
    if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
        
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
        
    }
    
    return arrOfServiceLeadToFetchDynamicData;
        
}

-(void)fetchSubWorkOrderFromDB{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictFinal setObject:arrEmailDetail forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderDetails=[[NSMutableDictionary alloc]init];
        
        for (int j=0; j<arrAllObjMechanicalSubWorkOrder.count; j++) {
            
            NSManagedObject *objTempSubWorkOrder=arrAllObjMechanicalSubWorkOrder[j];
            //subWorkOrderId
            
            //strWorkOrderType
            strWorkOrderType=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"subWOType"]];
            
            NSString *strStatus=[NSString stringWithFormat:@"%@",[objTempSubWorkOrder valueForKey:@"subWOStatus"]];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrder entity] attributesByName] allKeys];
            
            NSString *strCustomerSign;//=[objTempSubWorkOrder valueForKey:@"CustomerSignaturePath"];
            NSString *strTechSign;//=[objTempSubWorkOrder valueForKey:@"TechnicianSignaturePath"];
            
            BOOL isCompletedStatusMechanical=[global isCompletedSatusMechanical:strStatus];

            
            if (isCompletedStatusMechanical) {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"completeSWO_CustSignPath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"completeSWO_TechSignPath"];
                
            } else {
                
                strCustomerSign=[objTempSubWorkOrder valueForKey:@"customerSignaturePath"];
                strTechSign=[objTempSubWorkOrder valueForKey:@"technicianSignaturePath"];
                
            }
            
            //strAudioNameGlobal=[objTempSubWorkOrder valueForKey:@"audioFilePath"];
            
            if (!(strCustomerSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
            }
            
            if (!(strTechSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
            }
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrder valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            dictSubWorkOrderDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchPaymentInfoFromDB:[objTempSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            [self fetchSubWorkOrderIssuesFromDB:[objTempSubWorkOrder valueForKey:@"subWorkOrderId"]];
            
            [arrOfSubWorkOrderDb addObject:dictSubWorkOrderDetails];
        }
        
        [dictFinal setObject:arrOfSubWorkOrderDb forKey:@"SubWorkOrderExtSerDcs"];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)fetchPaymentInfoFromDB :(NSString*)strSubWorkOrderIdToFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"MechanicalSubWOPaymentDetailDcs" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subWorkOrderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"PaymentInfo"];
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue forKey:@"SubWOPaymentDetailDcs"];
        
    }else
    {
        
        NSMutableArray *arrPaymentInfoValue1;
        
        arrPaymentInfoValue1=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObj.count; k++) {
            
            matches=arrAllObj[k];
            
            NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
            
            if (!(strCheckFrontImage.length==0)) {
                
                [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
                
            }
            if (!(strCheckBackImage.length==0)) {
                
                [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [arrPaymentInfoValue1 addObject:dictPaymentInfo];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue1 forKey:@"SubWOPaymentDetailDcs"];

        
        /*
        matches=arrAllObj[0];
        
        NSString *strCheckFrontImage=[matches valueForKey:@"CheckFrontImagePath"];
        NSString *strCheckBackImage=[matches valueForKey:@"CheckBackImagePath"];
        
        if (!(strCheckFrontImage.length==0)) {
            
            [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
            
        }
        if (!(strCheckBackImage.length==0)) {
            
            [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
            
        }
        
        NSArray *arrPaymentInfoKey;
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
        
        for (int i=0; i<arrPaymentInfoKey.count; i++)
        {
            NSString *str;
            str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
            if([str isEqual:nil] || str.length==0 )
            {
                str=@"";
            }
            [arrPaymentInfoValue addObject:str];
        }
        
        NSDictionary *dictPaymentInfo;
        dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
        
        NSMutableArray *arrPaymentInfoValue1;
        
        arrPaymentInfoValue1=[[NSMutableArray alloc]init];
        
        [arrPaymentInfoValue1 addObject:dictPaymentInfo];
        
        [dictSubWorkOrderDetails setObject:arrPaymentInfoValue1 forKey:@"SubWOPaymentDetailDcs"];
        
         */
        
        
    }
}

-(void)fetchSubWorkOrderIssuesFromDB :(NSString*)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssues=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderIssueDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssues = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssues setEntity:entitySubWorkOrderIssues];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderIssues setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssues = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssues = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssues];
    
    [requestSubWorkOrderIssues setSortDescriptors:sortDescriptorsSubWorkOrderIssues];
    
    self.fetchedResultsControllerSubWorkOrderIssues = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssues managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssues setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssues performFetch:&error];
    arrAllObjSubWorkOrderIssues = [self.fetchedResultsControllerSubWorkOrderIssues fetchedObjects];
    if ([arrAllObjSubWorkOrderIssues count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderIssueDcs"];
        
        
    }
    else
    {
        
        arrOfSubWorkOrderIssuesDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssues.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssues=arrAllObjSubWorkOrderIssues[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssues entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssues valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            if ([strWorkOrderType isEqualToString:@"TM"]) {
                
                [dictSubWorkOrderIssuesDetails setObject: [dictSubWorkOrderIssuesDetails objectForKey: @"subWOIssueRepairPartDcs"] forKey: @"SubWOIssueRepairDcs"];
                
                [dictSubWorkOrderIssuesDetails removeObjectForKey: @"subWOIssueRepairPartDcs"];
                
                [self fetchSubWorkOrderIssuesPartsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            } else {
                
                [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
                
            }
            
            //Yaha Par Db Se Fetch Karna Issues k Repairs
            //  [self fetchSubWorkOrderIssuesRepairsFromDB:strSubWorkOrderIdToFetch :[objTempSubWorkOrderIssues valueForKey:@"subWorkOrderIssueId"]];
            
            [arrOfSubWorkOrderIssuesDb addObject:dictSubWorkOrderIssuesDetails];
        }
        
        // Yaha Par nootes helper hrs sab set honge
        [dictSubWorkOrderDetails setObject:arrOfSubWorkOrderIssuesDb forKey:@"SubWorkOrderIssueDcs"];
        
        [self fetchSubWorkOrderActualHrsFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWorkOrderHelperFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWorkOrderNotesFromDB :strSubWorkOrderIdToFetch];
        [self fetchSubWOCompleteTimeExtSerDc :strSubWorkOrderIdToFetch];

        // Akshay
        
        [self fetchWorkOrderAppliedDiscountExtSerDcs:strSubWorkOrderIdToFetch];

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepair=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepair = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepair setEntity:entitySubWorkOrderIssuesRepair];
    //subWorkOrderIssueId
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepair setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepair = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepair = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepair];
    
    [requestSubWorkOrderIssuesRepair setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepair];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepair = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepair managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepair performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepair = [self.fetchedResultsControllerSubWorkOrderIssuesRepair fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepair count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairDcs"];
        
    }
    else
    {
        arrOfSubWorkOrderIssuesRepairDb=[[NSMutableArray alloc]init];
        dictSubWorkOrderIssuesRepairDetails=[[NSMutableDictionary alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepair.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepair=arrAllObjSubWorkOrderIssuesRepair[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepair entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepair valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            dictSubWorkOrderIssuesRepairDetails = [NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [self fetchSubWorkOrderIssuesRepairPartsFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [self fetchSubWorkOrderIssuesRepairLabourFromDB:strSubWorkOrderIdToFetch :strSubWorkOrderIssueIdToFetch :[objTempSubWorkOrderIssuesRepair valueForKey:@"issueRepairId"]];
            
            [arrOfSubWorkOrderIssuesRepairDb addObject:dictSubWorkOrderIssuesRepairDetails];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfSubWorkOrderIssuesRepairDb forKey:@"SubWOIssueRepairDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWorkOrderIssuesRepairPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesPartsFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairParts=[NSEntityDescription entityForName:@"MechanicalSubWOIssueRepairPartDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairParts = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairParts setEntity:entitySubWorkOrderIssuesRepairParts];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch];
    
    [requestSubWorkOrderIssuesRepairParts setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairParts = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairParts = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairParts];
    
    [requestSubWorkOrderIssuesRepairParts setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairParts];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairParts = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairParts managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairParts = [self.fetchedResultsControllerSubWorkOrderIssuesRepairParts fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairParts count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairPartDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrOfPartsTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairParts.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairParts=arrAllObjSubWorkOrderIssuesRepairParts[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairParts entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairParts valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempParts = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfPartsTemp addObject:dictTempParts];
            
        }
        
        [dictSubWorkOrderIssuesDetails setObject:arrOfPartsTemp forKey:@"SubWOIssueRepairPartDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderIssuesRepairLabourFromDB :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderIssueIdToFetch :(NSString*)strIssueRepairIdTofetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderIssuesRepairLabour=[NSEntityDescription entityForName:@"MechannicalSubWOIssueRepairLaborDcs" inManagedObjectContext:context];
    requestSubWorkOrderIssuesRepairLabour = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderIssuesRepairLabour setEntity:entitySubWorkOrderIssuesRepairLabour];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderIssueId = %@ && issueRepairId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strSubWorkOrderIssueIdToFetch,strIssueRepairIdTofetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && issueRepairId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strIssueRepairIdTofetch];
    
    [requestSubWorkOrderIssuesRepairLabour setPredicate:predicate];
    
    sortDescriptorSubWorkOrderIssuesRepairLabour = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderIssuesRepairLabour = [NSArray arrayWithObject:sortDescriptorSubWorkOrderIssuesRepairLabour];
    
    [requestSubWorkOrderIssuesRepairLabour setSortDescriptors:sortDescriptorsSubWorkOrderIssuesRepairLabour];
    
    self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderIssuesRepairLabour managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour performFetch:&error];
    arrAllObjSubWorkOrderIssuesRepairLabour = [self.fetchedResultsControllerSubWorkOrderIssuesRepairLabour fetchedObjects];
    if ([arrAllObjSubWorkOrderIssuesRepairLabour count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderIssuesRepairDetails setObject:arrEmailDetail forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    else
    {
        NSMutableArray *arrOfLaborTemp=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderIssuesRepairLabour.count; k++) {
            
            NSManagedObject *objTempSubWorkOrderIssuesRepairLabor=arrAllObjSubWorkOrderIssuesRepairLabour[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempSubWorkOrderIssuesRepairLabor entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempSubWorkOrderIssuesRepairLabor valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempLabor = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfLaborTemp addObject:dictTempLabor];
            
        }
        
        [dictSubWorkOrderIssuesRepairDetails setObject:arrOfLaborTemp forKey:@"SubWOIssueRepairLaborDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderActualHrsFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderActualHrs setPredicate:predicate];
    
    sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
    
    [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
    
    self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
    arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
    if ([arrAllObjSubWorkOrderActualHrs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderActualHoursDcs"];
        
    }
    else
    {
        
        NSMutableArray *arrTempActualHrs=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
            
            NSManagedObject *objTempActualHrs=arrAllObjSubWorkOrderActualHrs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempActualHrs entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempActualHrs valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSMutableArray *tempKeyArray=[[NSMutableArray alloc]init];
            
            [tempKeyArray addObjectsFromArray:arrLeadDetailKey];
            
            [tempKeyArray addObject:@"EmployeeEmail"];
            
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            
            [arrLeadDetailValue addObject:[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeEmail"]]];
            
            //Change For Employee Time Sheet
            
            arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal=[[NSMutableArray alloc]init];

            [self fetchSubWoEmployeeWorkingTimeExtSerDcs:strSubWorkOrderIdToFetch :[NSString stringWithFormat:@"%@",[objTempActualHrs valueForKey:@"subWOActualHourId"]]];
            
            [tempKeyArray addObject:@"SubWoEmployeeWorkingTimeExtSerDcs"];
            
            [arrLeadDetailValue addObject:arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal];
            
            //Change For Employee Time Sheet
            
            arrLeadDetailKey=tempKeyArray;
            
            NSDictionary *dictTempActualHrs = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempActualHrs addObject:dictTempActualHrs];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempActualHrs forKey:@"SubWorkOrderActualHoursDcs"];
        
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
        
    }
}

-(void)fetchSubWorkOrderHelperFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderHelper=[NSEntityDescription entityForName:@"MechanicalSubWOTechHelperDcs" inManagedObjectContext:context];
    requestSubWorkOrderHelper = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderHelper setEntity:entityMechanicalSubWorkOrderHelper];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderHelper setPredicate:predicate];
    
    sortDescriptorSubWorkOrderHelper = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderHelper = [NSArray arrayWithObject:sortDescriptorSubWorkOrderHelper];
    
    [requestSubWorkOrderHelper setSortDescriptors:sortDescriptorsSubWorkOrderHelper];
    
    self.fetchedResultsControllerSubWorkOrderHelper = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderHelper managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderHelper setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderHelper performFetch:&error];
    arrAllObjSubWorkOrderHelper = [self.fetchedResultsControllerSubWorkOrderHelper fetchedObjects];
    if ([arrAllObjSubWorkOrderHelper count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWOTechHelperDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderHelper.count; k++) {
            
            NSManagedObject *objTempHelper=arrAllObjSubWorkOrderHelper[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempHelper entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempHelper = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempHelper addObject:dictTempHelper];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempHelper forKey:@"SubWOTechHelperDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderMechanicalEquipmentFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWorkOrderMechanicalEquipment=[NSEntityDescription entityForName:@"MechanicalWoEquipment" inManagedObjectContext:context];
    requestSubWorkOrderMechanicalEquipment = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderMechanicalEquipment setEntity:entitySubWorkOrderMechanicalEquipment];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestSubWorkOrderMechanicalEquipment setPredicate:predicate];
    
    sortDescriptorSubWorkOrderMechanicalEquipment = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderMechanicalEquipment = [NSArray arrayWithObject:sortDescriptorSubWorkOrderMechanicalEquipment];
    
    [requestSubWorkOrderMechanicalEquipment setSortDescriptors:sortDescriptorsSubWorkOrderMechanicalEquipment];
    
    self.fetchedResultsControllerSubWorkOrderMechanicalEquipment = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderMechanicalEquipment managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment performFetch:&error];
    arrAllObjSubWorkOrderMechanicalEquipment = [self.fetchedResultsControllerSubWorkOrderMechanicalEquipment fetchedObjects];
    if ([arrAllObjSubWorkOrderMechanicalEquipment count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictFinal setObject:arrEmailDetail forKey:@"AccountItemHistoryExtSerDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempHelper=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderMechanicalEquipment.count; k++) {
            
            NSManagedObject *objTempHelper=arrAllObjSubWorkOrderMechanicalEquipment[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempHelper entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempHelper valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                
                if ([str containsString:@"iOS"]) {
                    str=@"";
                }

                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempHelper = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempHelper addObject:dictTempHelper];
            
        }
        
        [dictFinal setObject:arrTempHelper forKey:@"AccountItemHistoryExtSerDcs"];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWorkOrderNotesFromDB :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrderNotes=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderNoteDcs" inManagedObjectContext:context];
    requestSubWorkOrderNotes = [[NSFetchRequest alloc] init];
    [requestSubWorkOrderNotes setEntity:entityMechanicalSubWorkOrderNotes];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch];
    
    [requestSubWorkOrderNotes setPredicate:predicate];
    
    sortDescriptorSubWorkOrderNotes = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWorkOrderNotes = [NSArray arrayWithObject:sortDescriptorSubWorkOrderNotes];
    
    [requestSubWorkOrderNotes setSortDescriptors:sortDescriptorsSubWorkOrderNotes];
    
    self.fetchedResultsControllerSubWorkOrderNotes = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderNotes managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWorkOrderNotes setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWorkOrderNotes performFetch:&error];
    arrAllObjSubWorkOrderNotes = [self.fetchedResultsControllerSubWorkOrderNotes fetchedObjects];
    if ([arrAllObjSubWorkOrderNotes count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWorkOrderNoteDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWorkOrderNotes.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWorkOrderNotes[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempNotes addObject:dictTempNotes];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"SubWorkOrderNoteDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


-(void)fetchSubWOCompleteTimeExtSerDc :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWOCompleteTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWOCompleteTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWOCompleteTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWOCompleteTimeExtSerDcs setEntity:entitySubWOCompleteTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch];
    
    [requestSubWOCompleteTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWOCompleteTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWOCompleteTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWOCompleteTimeExtSerDcs];
    
    [requestSubWOCompleteTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWOCompleteTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWOCompleteTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs performFetch:&error];
    arrAllObjSubWOCompleteTimeExtSerDcs = [self.fetchedResultsControllerSubWOCompleteTimeExtSerDcs fetchedObjects];
    if ([arrAllObjSubWOCompleteTimeExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"SubWOCompleteTimeExtSerDcs"];
        
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWOCompleteTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWOCompleteTimeExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrTempNotes addObject:dictTempNotes];
            
        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"SubWOCompleteTimeExtSerDcs"];
        
        NSLog(@"Complete TimeSheet being sent to server =====%@",arrTempNotes);

    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchSubWoEmployeeWorkingTimeExtSerDcs :(NSString *)strSubWorkOrderIdToFetch :(NSString *)strSubWorkOrderActualHoursId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySubWoEmployeeWorkingTimeExtSerDcs=[NSEntityDescription entityForName:@"SubWoEmployeeWorkingTimeExtSerDcs" inManagedObjectContext:context];
    requestSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchRequest alloc] init];
    [requestSubWoEmployeeWorkingTimeExtSerDcs setEntity:entitySubWoEmployeeWorkingTimeExtSerDcs];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@ && subWorkOrderActualHourId = %@",strWorkOrderIdGlobal,strSubWorkOrderIdToFetch,strSubWorkOrderActualHoursId];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setPredicate:predicate];
    
    sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs = [NSArray arrayWithObject:sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs];
    
    [requestSubWoEmployeeWorkingTimeExtSerDcs setSortDescriptors:sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs];
    
    self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWoEmployeeWorkingTimeExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs performFetch:&error];
    arrAllObjSubWoEmployeeWorkingTimeExtSerDcs = [self.fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs fetchedObjects];
    
    arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal=[[NSMutableArray alloc]init];
    
    if ([arrAllObjSubWoEmployeeWorkingTimeExtSerDcs count] == 0)
    {
        
//        NSMutableArray *arrEmailDetail;
//        arrEmailDetail=[[NSMutableArray alloc]init];
//       // [dictOfSubWoEmployeeWorkingTimeExtSerDcs setObject:arrEmailDetail forKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
//        [arrOfSubWoEmployeeWorkingTimeExtSerDcs addObject:arrEmailDetail];
        
    }
    else
    {
       // NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjSubWoEmployeeWorkingTimeExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjSubWoEmployeeWorkingTimeExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [arrOfSubWoEmployeeWorkingTimeExtSerDcsFinal addObject:dictTempNotes];
            
        }
        
        //[dictOfSubWoEmployeeWorkingTimeExtSerDcs setObject:arrTempNotes forKey:@"SubWoEmployeeWorkingTimeExtSerDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderAppliedDiscountExtSerDcs :(NSString *)strSubWorkOrderIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOrderAppliedDiscountExtSerDcs=[NSEntityDescription entityForName:@"WorkOrderAppliedDiscountExtSerDcs" inManagedObjectContext:context];
    requestWorkOrderAppliedDiscountExtSerDcs = [[NSFetchRequest alloc] init];
    [requestWorkOrderAppliedDiscountExtSerDcs setEntity:entityWorkOrderAppliedDiscountExtSerDcs];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && subWorkOrderId = %@",_strWorkOrderId,strSubWorkOrderIdToFetch];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"subWorkOrderId = %@",strSubWorkOrderIdToFetch];
    
    [requestWorkOrderAppliedDiscountExtSerDcs setPredicate:predicate];
    
    //    sortDescriptorWorkOrderAppliedDiscountExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorWorkOrderAppliedDiscountExtSerDcs = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:YES];
    sortDescriptorsWorkOrderAppliedDiscountExtSerDcs = [NSArray arrayWithObject:sortDescriptorWorkOrderAppliedDiscountExtSerDcs];
    
    [requestWorkOrderAppliedDiscountExtSerDcs setSortDescriptors:sortDescriptorsWorkOrderAppliedDiscountExtSerDcs];
    
    self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestWorkOrderAppliedDiscountExtSerDcs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs performFetch:&error];
    arrAllObjWorkOrderAppliedDiscountExtSerDcs = [self.fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs fetchedObjects];
    if ([arrAllObjWorkOrderAppliedDiscountExtSerDcs count] == 0)
    {
        
        NSMutableArray *arrEmailDetail;
        arrEmailDetail=[[NSMutableArray alloc]init];
        [dictSubWorkOrderDetails setObject:arrEmailDetail forKey:@"WorkOrderAppliedDiscountExtSerDcs"];
        
    }
    else
    {
        NSMutableArray *arrTempNotes=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrAllObjWorkOrderAppliedDiscountExtSerDcs.count; k++) {
            
            NSManagedObject *objTempNotes=arrAllObjWorkOrderAppliedDiscountExtSerDcs[k];
            
            NSArray *arrLeadDetailKey;
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[objTempNotes entity] attributesByName] allKeys];
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[objTempNotes valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            
            NSDictionary *dictTempNotes = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
//            NSMutableDictionary *dictAppliedDiscount = [[NSMutableDictionary alloc]initWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
//
//            if ([[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"1"] || [[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"True"] || [[dictAppliedDiscount valueForKey:@"isDiscountPercent"] isEqualToString:@"true"]) {
//
//                [dictAppliedDiscount setValue:@"True" forKey:@"isDiscountPercent"];
//
//            } else {
//
//                [dictAppliedDiscount setValue:@"False" forKey:@"isDiscountPercent"];
//
//            }
            
            [arrTempNotes addObject:dictTempNotes];
            //[arrTempNotes addObject:dictAppliedDiscount];

        }
        
        [dictSubWorkOrderDetails setObject:arrTempNotes forKey:@"WorkOrderAppliedDiscountExtSerDcs"];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchWorkOrderDetailFromDB
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"WorkorderDetail"];
        
    }else
    {
        matches=arrAllObj[0];
        objServiceWorkOrderGlobal=nil;
        objServiceWorkOrderGlobal=matches;
        
        NSArray *arrLeadDetailKey;
        NSMutableArray *arrLeadDetailValue;
        arrLeadDetailValue=[[NSMutableArray alloc]init];
        arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
        
        //            NSString *strCustomerSign=[matches valueForKey:@"CustomerSignaturePath"];
        //            NSString *strTechSign=[matches valueForKey:@"TechnicianSignaturePath"];
        //            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
        //
        //            if (!(strCustomerSign.length==0)) {
        //                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
        //            }
        //
        //            if (!(strTechSign.length==0)) {
        //                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
        //            }
        
        for (int i=0; i<arrLeadDetailKey.count; i++)
        {
            NSString *str;
            str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
            if ([str isKindOfClass:[NSDate class]]) {
                
                
            }else if ([str isKindOfClass:[NSArray class]]) {
                
                
            }
            else{
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
            }
            [arrLeadDetailValue addObject:str];
        }
        
        int indexToDeleteZDate = 0,indexToDeleteDateModified = 0;
        
        for (int k=0; k<arrLeadDetailKey.count; k++) {
            
            if ([arrLeadDetailKey[k] isEqualToString:@"dateModified"]) {
                
                indexToDeleteZDate=k;
                
            }
            if ([arrLeadDetailKey[k] isEqualToString:@"zdateScheduledStart"]) {
                
                indexToDeleteDateModified=k;
                
            }
            
        }
        
        NSMutableArray *tempArr=[[NSMutableArray alloc]init];
        [tempArr addObjectsFromArray:arrLeadDetailKey];
        
        [tempArr removeObjectAtIndex:indexToDeleteZDate];
        [tempArr removeObjectAtIndex:indexToDeleteDateModified-1];
        
        [arrLeadDetailValue removeObjectAtIndex:indexToDeleteZDate];
        [arrLeadDetailValue removeObjectAtIndex:indexToDeleteDateModified-1];
        
        arrLeadDetailKey=tempArr;
        
        NSDictionary *dictLeadDetail;
        dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
        [dictFinal setObject:dictLeadDetail forKey:@"WorkorderDetail"];
        
        [self fetchImageDetailFromDb];
        
        [self fetchEmailDetailDB];
        
        [self fetchSubWorkOrderFromDB];
        
        [self fetchSubWorkOrderMechanicalEquipmentFromDB :strWorkOrderIdGlobal];

        // Akshay
        [self fetchAccountDiscountExtSerDcs];
        
    }
    
}

-(void)fetchImageDetailFromDb
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            [arrImageDetail addObject:dictTemp];
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}

-(void)fetchEmailDetailDB
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}

-(void)fetchAccountDiscountExtSerDcs
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityAccountDiscount=[NSEntityDescription entityForName:@"AccountDiscountExtSerDcs" inManagedObjectContext:context];
    requestAccountDiscount = [[NSFetchRequest alloc] init];
    [requestAccountDiscount setEntity:entityAccountDiscount];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestAccountDiscount setPredicate:predicate];
    
    sortDescriptorAccDiscount = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsAccDiscount = [NSArray arrayWithObject:sortDescriptorAccDiscount];
    
    [requestAccountDiscount setSortDescriptors:sortDescriptorsAccDiscount];
    
    self.fetchedResultsControllerAccountDiscount = [[NSFetchedResultsController alloc] initWithFetchRequest:requestAccountDiscount managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerAccountDiscount setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerAccountDiscount performFetch:&error];
    NSArray* arrAccountDiscount = [self.fetchedResultsControllerAccountDiscount fetchedObjects];
    
    NSMutableArray *arrayAccountDiscont = [NSMutableArray new];
    
    if ([arrAccountDiscount count] == 0)
    {
        
    }
    else
    {
        for(NSManagedObject *accDiscount in arrAccountDiscount)
        {
            NSArray *keys = [[[accDiscount entity] attributesByName] allKeys];
            NSDictionary *dict = [accDiscount dictionaryWithValuesForKeys:keys];
            [arrayAccountDiscont addObject:dict];
            
        }
    }
    
    [dictFinal setObject:arrayAccountDiscont forKey:@"AccountDiscountExtSerDcs"];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
}

-(NSMutableDictionary*)fetchPlumbingDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strWoId{
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strWorkOrderIdGlobal = strWoId;
    strModifyDateToSendToServerAll = strModifyDateToSendToServer;
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllDocumentsToSend = [[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend = [[NSMutableArray alloc]init];
    
    dictFinal=[[NSMutableDictionary alloc]init];
    [self fetchWorkOrderDetailFromDB];
    [self fetchWOProductDetail];
    [self fetchPaymentInfoServiceAuto];
    [self fetchTermiteFromDb];
    [self fetchTermiteFloridaFromDb];
    [self fetchImageDetailServiceAuto];
    [self fetchEquipmentDetailServiceAuto];
    [self fetchEmailDetailServiceAuto];
    [self fetchWorkOrderDocuments];
    [self fetchTermiteImageDetailServiceAuto];
    
    [self fetchServicePestDataAllFromDBObjC];
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:arrOfAllDocumentsToSend forKey:@"arrOfAllDocumentsToSend"];
    [dictTemp setValue:arrOfAllImagesToSendToServer forKey:@"arrOfAllImagesToSendToServer"];
    [dictTemp setValue:arrOfAllSignatureImagesToSendToServer forKey:@"arrOfAllSignatureImagesToSendToServer"];
    [dictTemp setValue:arrOfAllCheckImageToSend forKey:@"arrOfAllCheckImageToSend"];
    [dictTemp setValue:strAudioNameGlobal forKey:@"strAudioNameGlobal"];
    [dictTemp setValue:serviceAddressImagePath forKey:@"serviceAddressImagePath"];
    [dictTemp setValue:dictFinal forKey:@"dictFinal"];
    
    return dictTemp;
    
}

-(NSMutableDictionary*)plumbingStatus : (NSString*)strId{
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    
    [dictTemp setValue:@"no" forKey:@"hidden"];
    [dictTemp setValue:@"yes" forKey:@"Enabled"];
    [dictTemp setValue:@"" forKey:@"ButtonStatus"];
    [dictTemp setValue:@"" forKey:@"SerivceStatus"];

    NSArray *arrOfSubWo = [self fetchSubWorkOrderFromDBForStatus:[NSString stringWithFormat:@"%@",strId]];
    
    if (arrOfSubWo.count>0) {
        
        NSManagedObject *objTempSubWoMain;
        
        for (int s=0; s<arrOfSubWo.count; s++) {
            
            NSManagedObject *objTempSubWo = arrOfSubWo[s];
            
            NSString *strSubWoStatusLocal = [NSString stringWithFormat:@"%@",[objTempSubWo valueForKey:@"subWOStatus"]];
            
            if ([strSubWoStatusLocal isEqualToString:@"Complete"] || [strSubWoStatusLocal isEqualToString:@"complete"] || [strSubWoStatusLocal isEqualToString:@"Completed"] || [strSubWoStatusLocal isEqualToString:@"completed"] || [strSubWoStatusLocal isEqualToString:@"CompletePending"] || [strSubWoStatusLocal isEqualToString:@"completePending"]) {
                
                strSubWoStatusLocal = @"Completed";
                
            }
            
            if (![strSubWoStatusLocal isEqualToString:@"Completed"]) {
                
                objTempSubWoMain = arrOfSubWo[s];
                
                break;
            }
            
        }
 
        NSString *strSubWoStatusLocal = [NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWOStatus"]];
        
        if (objTempSubWoMain==nil) {
            
            [dictTemp setValue:@"no" forKey:@"Enabled"];
            [dictTemp setValue:@"Completed" forKey:@"ButtonStatus"];
            [dictTemp setValue:@"Completed" forKey:@"SerivceStatus"];

        } else {
            
            if ([strSubWoStatusLocal isEqualToString:@"New"]) {
                
                [dictTemp setValue:@"Receive" forKey:@"ButtonStatus"];
                [dictTemp setValue:@"New" forKey:@"SerivceStatus"];

                
            } else if ([strSubWoStatusLocal isEqualToString:@"Dispatch"]){
                
                [dictTemp setValue:@"On My Way" forKey:@"ButtonStatus"];
                [dictTemp setValue:@"Received" forKey:@"SerivceStatus"];

            } else if ([strSubWoStatusLocal isEqualToString:@"OnRoute"]){

                [dictTemp setValue:@"Arrive" forKey:@"ButtonStatus"];
                [dictTemp setValue:@"On Route" forKey:@"SerivceStatus"];

            } else{
                
                [dictTemp setValue:@"no" forKey:@"Enabled"];
                [dictTemp setValue:strSubWoStatusLocal forKey:@"ButtonStatus"];
                [dictTemp setValue:strSubWoStatusLocal forKey:@"SerivceStatus"];
                
            }
            
            // Condition For Pause
            
            NSString *strStatusToCheckIfpause = strSubWoStatusLocal;
            NSString *strSubWoClockStatusLocal = [NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"clockStatus"]];

            if ( ([strStatusToCheckIfpause caseInsensitiveCompare:@"Completed"] == NSOrderedSame)
                || ([strStatusToCheckIfpause caseInsensitiveCompare:@"CompletePending"] == NSOrderedSame)
                || ([strStatusToCheckIfpause caseInsensitiveCompare:@"New"] == NSOrderedSame)
                || ([strStatusToCheckIfpause caseInsensitiveCompare:@"OnRoute"] == NSOrderedSame)
                || ([strStatusToCheckIfpause caseInsensitiveCompare:@"Dispatch"] == NSOrderedSame)
                || ([strStatusToCheckIfpause caseInsensitiveCompare:@"Not Started"] == NSOrderedSame)
                || ([strSubWoClockStatusLocal caseInsensitiveCompare:@"Start"] == NSOrderedSame)) {
                
                
            }else{
                
                [dictTemp setValue:@"Pause" forKey:@"SerivceStatus"];

            }
            
        }
        
    }else{
        
        [dictTemp setValue:@"yes" forKey:@"hidden"];
        [dictTemp setValue:@"no" forKey:@"Enabled"];
        [dictTemp setValue:@"" forKey:@"ButtonStatus"];
        [dictTemp setValue:@"" forKey:@"SerivceStatus"];
        
    }
        
    return dictTemp;
    
}

// CHanges for mechanical status changes on appointment view

-(NSArray*)fetchSubWorkOrderFromDBForStatus :(NSString*)strWoId{
    
    //subWOStatus
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND employeeNo = %@ AND (subWOStatus = %@ OR subWOStatus = %@)",strWoId,strEmployeeNo,@"Running",@"Inspection"];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND employeeNo = %@",strWoId,strEmployeeNo];

    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    NSArray *temArrSubWo = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([temArrSubWo count] == 0)
    {
        
        // No Sub Work Order Exist
        
    }
    else
    {
        // Logic to get latest by created date to show only one SubWork Order
        
        temArrSubWo = [global arrFilter:temArrSubWo :@"createdDate" :YES];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
    
    if (temArrSubWo.count==0) {
        
        temArrSubWo = [self fetchSubWorkOrderFromDBForStatusAll : strWoId];
        
    }
    
    return temArrSubWo;
    
}

-(NSArray*)fetchSubWorkOrderFromDBForStatusAll :(NSString*)strWoId{
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    strWorkOrderIdGlobal = strWoId;
    
    //subWOStatus
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND employeeNo = %@ AND (subWOStatus = %@ OR subWOStatus = %@ OR subWOStatus = %@ OR subWOStatus = %@)",strWoId,strEmployeeNo,@"New",@"Not Started",@"Dispatch",@"OnRoute"];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND employeeNo = %@",strWoId,strEmployeeNo];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    NSArray *temArrSubWo = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([temArrSubWo count] == 0)
    {
        
        // No Sub Work Order Exist
        
    }
    else
    {
        // Logic to get latest by created date to show only one SubWork Order
        
        temArrSubWo = [global arrFilter:temArrSubWo :@"createdDate" :YES];
        
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
    
    return temArrSubWo;
    
}

-(NSMutableDictionary*)fetchSubWoToUpdateStatus :(NSString*)strIdd :(NSManagedObject*)objmatches{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:@"" forKey:@"strMsg"];

    NSArray *arrOfSubWo = [self fetchSubWorkOrderFromDBForStatus:[NSString stringWithFormat:@"%@",strIdd]];
    
    if (arrOfSubWo.count>0) {
        
        NSManagedObject *objTempSubWoMain;
        
        for (int s=0; s<arrOfSubWo.count; s++) {
            
            NSManagedObject *objTempSubWo = arrOfSubWo[s];
            
            NSString *strSubWoStatusLocal = [NSString stringWithFormat:@"%@",[objTempSubWo valueForKey:@"subWOStatus"]];
            //CompletePending
            if ([strSubWoStatusLocal isEqualToString:@"Complete"] || [strSubWoStatusLocal isEqualToString:@"complete"] || [strSubWoStatusLocal isEqualToString:@"Completed"] || [strSubWoStatusLocal isEqualToString:@"completed"] || [strSubWoStatusLocal isEqualToString:@"CompletePending"] || [strSubWoStatusLocal isEqualToString:@"completePending"]) {
                
                strSubWoStatusLocal = @"Completed";
                
            }
            
            if (![strSubWoStatusLocal isEqualToString:@"Completed"]) {
                
                objTempSubWoMain = arrOfSubWo[s];
                
                break;
            }
            
        }
                
        NSString *strSubWoStatusLocal = [NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWOStatus"]];
        [dictTemp setValue:objTempSubWoMain forKey:@"objTempSubWoMain"];

        BOOL isEMP=[self IsDifferentEmp:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"employeeNo"]]];
        
        BOOL isStartedOtherSubWorkOrder=[global fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToSeeIfSubWorkorderIsAlreadyStarted:[global getEmployeeDeatils] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                
        if ([strSubWoStatusLocal isEqualToString:@"New"] || [strSubWoStatusLocal isEqualToString:@"Not Started"] || [strSubWoStatusLocal isEqualToString:@"NotStarted"]) {

            isStartedOtherSubWorkOrder=NO;
            
        }
        
        if (isEMP) {
            
            [dictTemp setValue:@"Work Order Assigned to other Technician you can not process it." forKey:@"strMsg"];
            
        } else if (isStartedOtherSubWorkOrder){
            
            [dictTemp setValue:@"isStartedOtherSubWorkOrder" forKey:@"strMsg"];

            //[self showAlertIfOtherWorkOrderStarted:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"workOrderNo"]] :[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]] : strSubWoStatusLocal : objTempSubWoMain];
            
        }else {
            
            [dictTemp setValue:@"updateStatus" forKey:@"strMsg"];

            if ([strSubWoStatusLocal isEqualToString:@"New"] || [strSubWoStatusLocal isEqualToString:@"Not Started"] || [strSubWoStatusLocal isEqualToString:@"NotStarted"]) {
                
                // change sub wo status in DB and send To Server
                
                BOOL isNetReachable=[global isNetReachable];
                                
                if (isNetReachable) {
                    
                    //[self metodUpdateSubWorkOrderStatus:@"New" :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                    
                } else {
                    
                    //[self addActualHrsIfResponseIsNil:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                    
                }
                
                //[self upDateSubWorkOrderStatusFromDB :@"Dispatch" :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                
                [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]]];
                
                
            } else if ([strSubWoStatusLocal isEqualToString:@"Dispatch"]){
                
                // change sub wo status in DB and send To Server
                
                BOOL isNetReachable=[global isNetReachable];
                
                if (isNetReachable) {
                    
                    //[self metodUpdateSubWorkOrderStatus:@"Dispatch" :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                    
                } else {
                    
                    //[self addActualHrsIfResponseIsNil:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                    
                }
                
                //[self upDateSubWorkOrderStatusFromDB:@"OnRoute" :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]]];
                
                [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]]];
                
            } else if ([strSubWoStatusLocal isEqualToString:@"OnRoute"]){
                
                // change sub wo status in DB and send To Server
                
                //[self fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]] :[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"subWorkOrderId"]] :objTempSubWoMain];
                
                [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:[NSString stringWithFormat:@"%@",[objTempSubWoMain valueForKey:@"workorderId"]]];
                
            } else{
                
                // Nothing to do
                [dictTemp setValue:@"" forKey:@"strMsg"];

            }
            
        }
        
    }
    
    return dictTemp;
}

-(BOOL)IsDifferentEmp :(NSString*)strEmpNoSub{
    
    BOOL yesDifferent;
    yesDifferent=YES;
    
    if ([strEmpNoSub isEqualToString:strEmployeeNo]) {
        
        yesDifferent=NO;
        
    } else {
        
        yesDifferent=YES;
        
    }
    
    return yesDifferent;
    
}

-(void)addActualHrsIfResponseIsNil :(NSString*)strWoId :(NSString*)strSubWoId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWoId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    objSubWorkOrderNotes.subWorkOrderId=strSubWoId;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKeyy;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    objSubWorkOrderNotes.status=@"OnRoute";
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
        
}


-(void)addStartActualHoursFromMobileToDB :(NSDictionary*)dict :(NSString*)strWoId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWoId;
    objSubWorkOrderNotes.subWOActualHourId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SubWOActualHourId"]];
    
    objSubWorkOrderNotes.subWorkOrderId=[NSString stringWithFormat:@"%@",[dict valueForKey:@"SubWorkOrderId"]];
    objSubWorkOrderNotes.timeIn=[NSString stringWithFormat:@"%@",[dict valueForKey:@"TimeIn"]];
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeIn=[NSString stringWithFormat:@"%@",[dict valueForKey:@"MobileTimeIn"]];
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKeyy;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",[dict valueForKey:@"IsActive"]];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedBy"]];
    objSubWorkOrderNotes.createdDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedDate"]];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ModifiedBy"]];
    objSubWorkOrderNotes.modifiedDate=[NSString stringWithFormat:@"%@",[dict valueForKey:@"ModifiedDate"]];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedByDevice"]];
    objSubWorkOrderNotes.status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Status"]];
    
    // Latitude changes
    //    CLLocationCoordinate2D coordinate = [global getLocation] ;
    //    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    //    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Latitude"]];;
    objSubWorkOrderNotes.longitude=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Longitude"]];;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    
    NSError *error2;
    [context save:&error2];
        
}

-(void)upDateSubWorkOrderStatusFromDB :(NSString*)strStatusSubWorkOrder :(NSString*)strWoId :(NSString*)strSubWoId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityMechanicalSubWorkOrder=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
    requestMechanicalSubWorkOrder = [[NSFetchRequest alloc] init];
    [requestMechanicalSubWorkOrder setEntity:entityMechanicalSubWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ && subWorkOrderId = %@",strWoId,strSubWoId];
    
    [requestMechanicalSubWorkOrder setPredicate:predicate];
    
    sortDescriptorMechanicalSubWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsMechanicalSubWorkOrder = [NSArray arrayWithObject:sortDescriptorMechanicalSubWorkOrder];
    
    [requestMechanicalSubWorkOrder setSortDescriptors:sortDescriptorsMechanicalSubWorkOrder];
    
    self.fetchedResultsControllerMechanicalSubWorkOrder = [[NSFetchedResultsController alloc] initWithFetchRequest:requestMechanicalSubWorkOrder managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerMechanicalSubWorkOrder setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerMechanicalSubWorkOrder performFetch:&error];
    arrAllObjMechanicalSubWorkOrder = [self.fetchedResultsControllerMechanicalSubWorkOrder fetchedObjects];
    if ([arrAllObjMechanicalSubWorkOrder count] == 0)
    {
        
        
        
    }
    else
    {
        
        matchesMechanicalSubWorkOrder=arrAllObjMechanicalSubWorkOrder[0];
        [matchesMechanicalSubWorkOrder setValue:strStatusSubWorkOrder forKey:@"subWOStatus"];
        [matchesMechanicalSubWorkOrder setValue:@"Start" forKey:@"clockStatus"];
        
        NSError *error1;
        [context save:&error1];
                
    }
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        
    }
}

-(void)addStartActualHoursFromMobileToDBForOnRoute :(NSString*)strWoId :(NSString*)strSubWoId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrderActualHoursDcs" inManagedObjectContext:context];
    
    MechanicalSubWorkOrderActualHoursDcs *objSubWorkOrderNotes = [[MechanicalSubWorkOrderActualHoursDcs alloc]initWithEntity:entityMechanicalSubWorkOrderActualHrs insertIntoManagedObjectContext:context];
    
    objSubWorkOrderNotes.workorderId=strWoId;
    
    NSString *strNos=[global getReferenceNumber];
    objSubWorkOrderNotes.subWOActualHourId=strNos;
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setValue:strNos forKey:@"subWOActualHourId"];
    [defs synchronize];
    
    objSubWorkOrderNotes.subWorkOrderId=strSubWoId;
    objSubWorkOrderNotes.timeIn=[global strCurrentDateFormattedForMechanical];
    objSubWorkOrderNotes.mobileTimeIn=objSubWorkOrderNotes.timeIn;
    objSubWorkOrderNotes.status=@"Inspection";
    objSubWorkOrderNotes.timeOut=@"";
    objSubWorkOrderNotes.mobileTimeOut=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKeyy;
    objSubWorkOrderNotes.reason=@"";
    objSubWorkOrderNotes.actHrsDescription=@"";
    objSubWorkOrderNotes.subWorkOrderNo=@"";
    objSubWorkOrderNotes.subWOActualHourNo=@"";
    objSubWorkOrderNotes.companyKey=strCompanyKeyy;
    objSubWorkOrderNotes.isActive=[NSString stringWithFormat:@"%@",@"true"];
    objSubWorkOrderNotes.createdBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.createdDate=[global strCurrentDate];
    objSubWorkOrderNotes.modifiedBy=[NSString stringWithFormat:@"%@",strUserName];
    objSubWorkOrderNotes.modifiedDate=[global strCurrentDate];
    objSubWorkOrderNotes.createdByDevice=[NSString stringWithFormat:@"%@",@"Mobile"];
    
    // Latitude changes
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    objSubWorkOrderNotes.latitude=latitude;
    objSubWorkOrderNotes.longitude=longitude;
    // Latitude changes end
    
    objSubWorkOrderNotes.employeeNo=[global getEmployeeDeatils];
    
    NSError *error2;
    [context save:&error2];
    
}

-(NSMutableDictionary*)showAlertIfOtherWorkOrderStarted :(NSString*)strWoNo :(NSString*)strWoId  :(NSString*)strSubWoId :(NSString*)strSubWoStatusLocal  :(NSManagedObject*)objTempSubWoMain{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc] init];
    [dictTemp setValue:@"" forKey:@"strMsgToShow"];

    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSString *strMsgToShow;
    
    NSUserDefaults *defsRunning =[NSUserDefaults standardUserDefaults];
    NSArray *arrOfRunningSubWorkOrder =[defsRunning objectForKey:@"RunningSubWorkOrder"];
    
    if (arrOfRunningSubWorkOrder.count>0) {
        
        
        NSString *strString = [arrOfRunningSubWorkOrder componentsJoinedByString:@","];
        
        strMsgToShow = [NSString stringWithFormat:@"Can't process this work-order because other work-orders (%@) is in process. Do you won't to stop other work-orders",strString];
        
        if ([arrOfRunningSubWorkOrder containsObject:strWoNo]) {
            
            NSMutableArray *arrTemp = [global fetchRunningSubWorkOrder:strEmployeeNo :strWoId :strSubWoId];
            
            if (arrTemp.count>0) {
                
                NSString *strIdsToShowInALert = [arrTemp componentsJoinedByString:@","];
                strMsgToShow = [NSString stringWithFormat:@"Can't process this sub-work-order because other sub-work-order (%@) is in process. Do you won't to stop other sub-work-orders",strIdsToShowInALert];
                
            }
            
        }
        
        BOOL isNetReachable=[global isNetReachable];
        
        if (!isNetReachable) {
            
            strMsgToShow = [strMsgToShow stringByReplacingOccurrencesOfString:@"Do you won't to stop other work-orders" withString:@""];
            strMsgToShow = [strMsgToShow stringByReplacingOccurrencesOfString:@"Do you won't to stop other sub-work-orders" withString:@""];
            
        }
        
    }
    
    [dictTemp setValue:strMsgToShow forKey:@"strMsgToShow"];
    [dictTemp setValue:arrOfRunningSubWorkOrder forKey:@"arrOfRunningSubWorkOrder"];

    return dictTemp;
    
}

-(NSMutableArray*)stopAllRunningSubWorkOrder :(NSArray*)arrOfWos{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSMutableArray *arrOfRunningSubWorkOrderId = [[NSMutableArray alloc]init];

    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];

    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    for (int k=0; k<arrOfWos.count; k++) {
        
        NSString *strWorkOrderIdFetched = [global fetchWorkOrderFromDataBaseForMechanicalToFetchWorkOrderId:arrOfWos[k]];
        
        BOOL isStarted;
        isStarted=NO;
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        NSEntityDescription *entityMechanicalSubWorkOrderActualHrs=[NSEntityDescription entityForName:@"MechanicalSubWorkOrder" inManagedObjectContext:context];
        NSFetchRequest *requestSubWorkOrderActualHrs = [[NSFetchRequest alloc] init];
        [requestSubWorkOrderActualHrs setEntity:entityMechanicalSubWorkOrderActualHrs];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo = %@ && workorderId = %@",strEmployeeNo,strWorkOrderIdFetched];
        
        [requestSubWorkOrderActualHrs setPredicate:predicate];
        
        NSSortDescriptor *sortDescriptorSubWorkOrderActualHrs = [[NSSortDescriptor alloc] initWithKey:@"employeeNo" ascending:NO];
        NSArray *sortDescriptorsSubWorkOrderActualHrs = [NSArray arrayWithObject:sortDescriptorSubWorkOrderActualHrs];
        
        [requestSubWorkOrderActualHrs setSortDescriptors:sortDescriptorsSubWorkOrderActualHrs];
        
        self.fetchedResultsControllerSubWorkOrderActualHrs = [[NSFetchedResultsController alloc] initWithFetchRequest:requestSubWorkOrderActualHrs managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSubWorkOrderActualHrs setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [self.fetchedResultsControllerSubWorkOrderActualHrs performFetch:&error];
        NSArray *arrAllObjSubWorkOrderActualHrs = [self.fetchedResultsControllerSubWorkOrderActualHrs fetchedObjects];
        if ([arrAllObjSubWorkOrderActualHrs count] == 0)
        {
            
            
        }
        else
        {
            
            
            NSString *strSubWorkOderIDRunning;
            
            strSubWorkOderIDRunning=@"";
            
            for (int k=0; k<arrAllObjSubWorkOrderActualHrs.count; k++) {
                
                NSManagedObject *matchesSubWorkOrderActualHrs=arrAllObjSubWorkOrderActualHrs[k];
                
                NSString *strSubWoStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWOStatus"]];//subWorkOrderId   employeeNo
                NSString *strSubWoClockStatus=[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"clockStatus"]];//subWorkOrderId   employeeNo
                                
                if ([strSubWoStatus isEqualToString:@"Completed"] || [strSubWoStatus isEqualToString:@"CompletePending"] || [strSubWoStatus isEqualToString:@"New"] || [strSubWoStatus isEqualToString:@"Not Started"] || [strSubWoStatus isEqualToString:@"Dispatch"]) {
                    
                    
                    
                }else{
                    
                    if ([strSubWoClockStatus caseInsensitiveCompare:@"Start"] == NSOrderedSame) {
                        
                        isStarted=YES;
                        
                        strSubWorkOderIDRunning =[NSString stringWithFormat:@"%@",[matchesSubWorkOrderActualHrs valueForKey:@"subWorkOrderId"]];
                        
                        NSString *strCombinedStringToShow =[NSString stringWithFormat:@"%@",strSubWorkOderIDRunning];
                        
                        if ([arrOfRunningSubWorkOrderId containsObject:strCombinedStringToShow]) {
                            
                            
                            
                        } else {
                            
                            [arrOfRunningSubWorkOrderId addObject:strCombinedStringToShow];
                            
                        }
                        
                        
                        //break;
                        
                    } else {
                        
                        
                    }
                }
            }
            
            if (arrOfRunningSubWorkOrderId.count>0) {
                
                // Fetched All Sub Work Order Which Are Started
                
            }
            
        }
        if (error) {
            
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
        } else {
            
            
            
        }
        
        [global fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate:strWorkOrderIdFetched];
        
    }
    
    return arrOfRunningSubWorkOrderId;
    
}

#pragma mark- WDO Flow Fetch Methods

-(void)fetchWDODataAllFromDBObjC{
    
    NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
    
    // WoWdoInspectionExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchWDOFlowData:@"WoWdoInspectionExtSerDc"];
    [dictFinal setObject:arrTemp forKey:@"WoWdoInspectionExtSerDc"];
    
    if (arrTemp.count==0) {
        
        [dictFinal setObject:@"" forKey:@"WoWdoInspectionExtSerDc"];
        
    } else {
        
        [dictFinal setObject:arrTemp[0] forKey:@"WoWdoInspectionExtSerDc"];
        
    }
    
    
    // WoWdoProblemIdentificationExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchWDOFlowData:@"WoWdoProblemIdentificationExtSerDcs"];
    [dictFinal setObject:arrTemp forKey:@"WoWdoProblemIdentificationExtSerDcs"];
    
    // WoWdoDisclaimerExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchWDOFlowData:@"WoWdoDisclaimerExtSerDcs"];
    [dictFinal setObject:arrTemp forKey:@"WoWdoDisclaimerExtSerDcs"];
    
    // WoWdoGeneralNotesExtSerDcs
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchWDOFlowData:@"WoWdoGeneralNotesExtSerDcs"];
    [dictFinal setObject:arrTemp forKey:@"WoWdoGeneralNotesExtSerDcs"];
    
    // WoWdoPricingMultiplierExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchWDOFlowData:@"WoWdoPricingMultiplierExtSerDc"];
    //[dictFinal setObject:arrTemp forKey:@"WoWdoPricingMultiplierExtSerDc"];
    
    if (arrTemp.count==0) {
        
        [dictFinal setObject:@"" forKey:@"WoWdoPricingMultiplierExtSerDc"];
        
    } else {
        
        [dictFinal setObject:arrTemp[0] forKey:@"WoWdoPricingMultiplierExtSerDc"];
        
    }
    
}


-(NSMutableArray*)fetchWDOFlowData :(NSString*)strEntityName
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    NSSortDescriptor * sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
    
    if ([strEntityName isEqualToString:@"WoWdoProblemIdentificationExtSerDcs"]) {
        
        sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"issuesCode" ascending:YES];
        
    }
    
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerWDO = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWDO setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerWDO performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerWDO fetchedObjects];
    
    NSMutableArray *arrData;
    arrData=[[NSMutableArray alloc]init];
    
    if (ObjAllData.count==0)
    {
        
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[k];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            NSArray *arrOfKey = [dictOfData allKeys];
            
            if ([arrOfKey containsObject:@"woWdoInspectionId"]) {
                
                if (k>0) {
                    
                    break;
                    
                }
                
            }
            
            
            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsChanged"] || [strBoolKey isEqualToString:@"IsBuildingPermit"] || [strBoolKey isEqualToString:@"IsSeparateReport"] || [strBoolKey isEqualToString:@"IsTipWarranty"] || [strBoolKey isEqualToString:@"IsYearUnknown"] || [strBoolKey isEqualToString:@"IsDiscount"] || [strBoolKey isEqualToString:@"IsAddToAgreement"] || [strBoolKey isEqualToString:@"IsCalculate"] || [strBoolKey isEqualToString:@"IsBuildingPermit"] || [strBoolKey isEqualToString:@"IsCalculate"] || [strBoolKey isEqualToString:@"IsBidOnRequest"] || [strBoolKey isEqualToString:@"IsResolved"] || [strBoolKey isEqualToString:@"IsOther"] || [strBoolKey isEqualToString:@"IsSlab"] || [strBoolKey isEqualToString:@"IsBasement"] || [strBoolKey isEqualToString:@"IsCrawl"] || [strBoolKey isEqualToString:@"IsPrimary"] || [strBoolKey isEqualToString:@"IsSecondary"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];
                        
                    }else{
                        
                        NSLog(@"----%@",strBoolKey);
                        NSLog(@"----%@",arrOfValue[k]);
                        
                        if ([arrOfValue[k] isKindOfClass:[NSNull class]]) {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }else{
                            
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                            
                        }
                    }
                    
                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            
            //NSArray *arrOfValue = [dictOfData allValues];
            
            if ([arrTempKey containsObject:@"ProblemIdentificationId"]) {
                
                [arrTempKey addObject:@"WoWdoProblemIdentificationPricingExtSerDc"];
                
                NSString *strProblemIdentificatioId = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"problemIdentificationId"]];
                
                NSString *strMobileProblemIdentificatioId = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"mobileId"]];

                if (strProblemIdentificatioId.length>0) {
                    
                    NSDictionary *dictData = [self fetchWoWdoProblemIdentificationPricingExtSerDc:@"WoWdoProblemIdentificationPricingExtSerDc":strProblemIdentificatioId];
                    
                    if ( (dictData.count==0)  || (dictData == nil) ){
                        
                        [arrTempValue addObject:@""];
                        
                    } else {
                        
                        [arrTempValue addObject:dictData];
                        
                    }
                    
                } else {
                    
                    NSDictionary *dictData = [self fetchWoWdoProblemIdentificationPricingExtSerDc:@"WoWdoProblemIdentificationPricingExtSerDc":strMobileProblemIdentificatioId];
                    
                    if ( (dictData.count==0)  || (dictData == nil) ){
                        
                        [arrTempValue addObject:@""];
                        
                    } else {
                        
                        [arrTempValue addObject:dictData];
                        
                    }
                    
                }
                
            }
            
            
            NSDictionary *dictData;
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
            [arrData addObject:dictData];
            
        }
        
    }
    
    return arrData;
    
}

-(NSDictionary*)fetchWoWdoProblemIdentificationPricingExtSerDc :(NSString*)strEntityName :(NSString*)strProblemIdentificationId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@ && problemIdentificationId=%@",strWorkOrderIdGlobal,strProblemIdentificationId];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strWorkOrderIdGlobal];

    [request setPredicate:predicate];
    
    NSSortDescriptor * sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerWDOPricing = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerWDOPricing setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerWDOPricing performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerWDOPricing fetchedObjects];

    NSDictionary *dictData;

    if (ObjAllData.count==0)
    {
        
        
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[0];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            NSArray *arrOfKey = [dictOfData allKeys];

            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsChanged"] || [strBoolKey isEqualToString:@"IsBuildingPermit"] || [strBoolKey isEqualToString:@"IsSeparateReport"] || [strBoolKey isEqualToString:@"IsTipWarranty"] || [strBoolKey isEqualToString:@"IsYearUnknown"] || [strBoolKey isEqualToString:@"IsDiscount"] || [strBoolKey isEqualToString:@"IsAddToAgreement"] || [strBoolKey isEqualToString:@"IsDefault"] || [strBoolKey isEqualToString:@"IsBuildingPermit"] || [strBoolKey isEqualToString:@"IsCalculate"] || [strBoolKey isEqualToString:@"IsBidOnRequest"] || [strBoolKey isEqualToString:@"IsResolved"] || [strBoolKey isEqualToString:@"IsOther"] || [strBoolKey isEqualToString:@"IsSlab"] || [strBoolKey isEqualToString:@"IsBasement"] || [strBoolKey isEqualToString:@"IsCrawl"] || [strBoolKey isEqualToString:@"IsPrimary"] || [strBoolKey isEqualToString:@"IsSecondary"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];
                        
                    }else{
                        
                        NSLog(@"----%@",strBoolKey);
                        NSLog(@"----%@",arrOfValue[k]);
                        
                        if ([arrOfValue[k] isKindOfClass:[NSNull class]]) {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }else{
                            
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            
            //NSArray *arrOfValue = [dictOfData allValues];
            
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
        }
        
    }
    
    return dictData;
    
}

//============================================================================
//============================================================================
#pragma mark- ------------------------Problem Image Detail------------------
//============================================================================
//============================================================================

-(void)fetchProblemImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityProblemImageDetail= [NSEntityDescription entityForName:@"WoWdoProblemIdentificationImagesExtSerDcs" inManagedObjectContext:context];
    [request setEntity:entityProblemImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerProblemImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerProblemImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerProblemImageDetail performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerProblemImageDetail fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"WoWdoProblemIdentificationImagesExtSerDcs"];
    }
    else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            
            if (!(strImageName.length==0)) {
                
                [arrOfAllProblemImageToSendToServer addObject:strImageName];
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [arrImageDetail addObject:dictTemp];
            
        }
        [dictFinal setObject:arrImageDetail forKey:@"WoWdoProblemIdentificationImagesExtSerDcs"];
        
    }
    
}

//===============================================================================================
#pragma mark- ================================= NPMA Termite =================================
//===============================================================================================


-(void)fetchNpmaTermiteDataAllFromDBObjC{
    
    arrOfAllDocumentsToSend = [[NSMutableArray alloc] init];
    NSMutableArray *arrTemp = [[NSMutableArray alloc] init];

    BOOL isTrue = false;
    
    NSDictionary *dictDataWorkOrders=[dictFinal valueForKey:@"WorkorderDetail"];

    if ([dictDataWorkOrders isKindOfClass:[NSDictionary class]]) {
        
        NSString *strBoolKey = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"isNPMATermite"]];
        
        isTrue = [strBoolKey boolValue];
        
    }
    
    if (isTrue) {

        // ServiceConditions
        arrTemp = [self fetchNpmaTermiteData:@"ServiceConditions" :@"No" : @"workOrderId"];
        [dictFinal setObject:arrTemp forKey:@"ServiceConditions"];

        // ServiceConditionComments
        arrTemp = [[NSMutableArray alloc] init];
        arrTemp = [self fetchNpmaTermiteData:@"ServiceConditionComments" :@"No" : @"workOrderId"];
        [dictFinal setObject:arrTemp forKey:@"ServiceConditionComments"];
        
        // ServiceConditionDocuments
        arrTemp = [[NSMutableArray alloc] init];
        arrTemp = [self fetchNpmaTermiteData:@"ServiceConditionDocuments" :@"Yes" : @"workOrderId"];
        [dictFinal setObject:arrTemp forKey:@"ServiceConditionDocuments"];

        // WoNPMAFinalizeReportDetailExtSerDc
        arrTemp = [[NSMutableArray alloc] init];
        arrTemp = [self fetchNpmaTermiteData:@"WoNPMAFinalizeReportDetailExtSerDc" :@"Yes" : @"workorderId"];
        [dictFinal setObject:arrTemp forKey:@"WoNPMAFinalizeReportDetailExtSerDc"];
        
        if (arrTemp.count==0) {
            
            [dictFinal setObject:@"" forKey:@"WoNPMAFinalizeReportDetailExtSerDc"];
            
        } else {
            
            [dictFinal setObject:arrTemp[0] forKey:@"WoNPMAFinalizeReportDetailExtSerDc"];
            
        }
        
    }

    
    // WoNPMAInspectionGeneralDescriptionExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAInspectionGeneralDescriptionExtSerDc" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAInspectionGeneralDescriptionExtSerDc"];
    
    if (arrTemp.count==0) {
        
        [dictFinal setObject:@"" forKey:@"WoNPMAInspectionGeneralDescriptionExtSerDc"];
        
    } else {
        
        [dictFinal setObject:arrTemp[0] forKey:@"WoNPMAInspectionGeneralDescriptionExtSerDc"];
        
    }
    
    // WoNPMAInspectionMoistureDetailExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAInspectionMoistureDetailExtSerDc" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAInspectionMoistureDetailExtSerDc"];
    
    if (arrTemp.count==0) {
        
        [dictFinal setObject:@"" forKey:@"WoNPMAInspectionMoistureDetailExtSerDc"];
        
    } else {
        
        [dictFinal setObject:arrTemp[0] forKey:@"WoNPMAInspectionMoistureDetailExtSerDc"];
        
    }
    
    // WoNPMAInspectionObstructionExtSerDcs
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAInspectionObstructionExtSerDcs" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAInspectionObstructionExtSerDcs"];
    
    
    // WoNPMAInspectionOtherDetailExtSerDc
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAInspectionOtherDetailExtSerDc" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAInspectionOtherDetailExtSerDc"];
    
    if (arrTemp.count==0) {
        
        [dictFinal setObject:@"" forKey:@"WoNPMAInspectionOtherDetailExtSerDc"];
        
    } else {
        
        [dictFinal setObject:arrTemp[0] forKey:@"WoNPMAInspectionOtherDetailExtSerDc"];
        
    }
    
    // WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs"];
    
    // WoNPMAProductDetailForSoilAndWoodExtSerDcs
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAProductDetailForSoilAndWoodExtSerDcs" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAProductDetailForSoilAndWoodExtSerDcs"];
    
    //WoNPMAServicePricingExtSerDcs
    
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAServicePricingExtSerDcs" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAServicePricingExtSerDcs"];
    
    //WoNPMAServiceTermsExtSerDc
    
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchNpmaTermiteData:@"WoNPMAServiceTermsExtSerDcs" :@"Yes" : @"workorderId"];
    [dictFinal setObject:arrTemp forKey:@"WoNPMAServiceTermsExtSerDcs"];
    
}


-(NSMutableArray*)fetchNpmaTermiteData :(NSString*)strEntityName :(NSString*)strIsDocument :(NSString*)strWorkOrderName
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate;
    NSSortDescriptor * sortDescriptorLocal;
    
    if ([strWorkOrderName isEqualToString:@"workOrderId"]) {
        
        predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strWorkOrderIdGlobal];
        [request setPredicate:predicate];
        sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
        
    } else {
        
        predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
        [request setPredicate:predicate];
        sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
        
    }
    
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerNpmaTermite = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerNpmaTermite setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerNpmaTermite performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerNpmaTermite fetchedObjects];
    
    NSMutableArray *arrData;
    arrData=[[NSMutableArray alloc]init];
    
    if (ObjAllData.count==0)
    {
        
        //[dictFinal setObject:arrData forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[k];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            if ([strIsDocument isEqualToString:@"Yes"]) {
                
                NSString *isToUpload = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"isToUpload"]];
                
                if ([isToUpload isEqualToString:@"Yes"]) {
                    
                    [arrOfAllDocumentsToSend addObject:dictOfData];
                    
                }
                
            }
            
            NSArray *arrOfKey = [dictOfData allKeys];
            
            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsBasement"] || [strBoolKey isEqualToString:@"IsOther"] || [strBoolKey isEqualToString:@"IsSlab"] || [strBoolKey isEqualToString:@"IsControlOfExistingInfestation"] || [strBoolKey isEqualToString:@"IsDamp"] || [strBoolKey isEqualToString:@"IsDry"] || [strBoolKey isEqualToString:@"IsDryerVentedOutside"] || [strBoolKey isEqualToString:@"IsHP"] || [strBoolKey isEqualToString:@"IsInside"] || [strBoolKey isEqualToString:@"IsInstallMoistureBarrier"] || [strBoolKey isEqualToString:@"IsOutside"] || [strBoolKey isEqualToString:@"IsPerimeter"] || [strBoolKey isEqualToString:@"IsPreventionOfInfestation"] || [strBoolKey isEqualToString:@"IsRemoveWoodDebris"] || [strBoolKey isEqualToString:@"IsVentsNecessary"] || [strBoolKey isEqualToString:@"IsWet"] || [strBoolKey isEqualToString:@"IsCorrectiveAction"] || [strBoolKey isEqualToString:@"IsDamage"] || [strBoolKey isEqualToString:@"IsEvidenceOfMoisture"] || [strBoolKey isEqualToString:@"IsEvidenceOfPresence"] || [strBoolKey isEqualToString:@"IsFungiObserved"] || [strBoolKey isEqualToString:@"IsNoVisibleEvidence"] || [strBoolKey isEqualToString:@"IsVisibleEvidence"] || [strBoolKey isEqualToString:@"IsDeadInsect"] || [strBoolKey isEqualToString:@"IsInspectorMakeCompleteInspection"] || [strBoolKey isEqualToString:@"IsLiveInsect"] || [strBoolKey isEqualToString:@"IsNoTreatmentRecommended"] || [strBoolKey isEqualToString:@"IsRecommendTreatment"] || [strBoolKey isEqualToString:@"IsVisibleDemage"] || [strBoolKey isEqualToString:@"IsVisibleEvidence"] || [strBoolKey isEqualToString:@"IsBaitStation"] || [strBoolKey isEqualToString:@"IsPrimary"] || [strBoolKey isEqualToString:@"IsSoil"] || [strBoolKey isEqualToString:@"IsTreatmentCompleted"] || [strBoolKey isEqualToString:@"IsRenewal"] || [strBoolKey isEqualToString:@"IsDiscount"] || [strBoolKey isEqualToString:@"IsResolved"] || [strBoolKey isEqualToString:@"isAddToAgreement"] || [strBoolKey isEqualToString:@"isBuildingPermit"] || [strBoolKey isEqualToString:@"isChanged"] || [strBoolKey isEqualToString:@"IsCrawl"] || [strBoolKey isEqualToString:@"isSlab"] || [strBoolKey isEqualToString:@"IsBrickVeneer"] || [strBoolKey isEqualToString:@"IsHollowBlack"] || [strBoolKey isEqualToString:@"IsStoneVeneer"] || [strBoolKey isEqualToString:@"IsPorches"] || [strBoolKey isEqualToString:@"IsGarege"] || [strBoolKey isEqualToString:@"IsChanged"] || [strBoolKey isEqualToString:@"IsAddToAgreement"] || [strBoolKey isEqualToString:@"IsBuildingPermit"] || [strBoolKey isEqualToString:@"IsSlab"] || [strBoolKey isEqualToString:@"IsPrimary "] || [strBoolKey isEqualToString:@"IsSecondary"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];
                        
                    }else{
                        
                        if ([arrOfValue[k] isKindOfClass:[NSNull class]]) {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }else{
                            
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                            
                        }
                        
                    }
                    
                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            NSDictionary *dictData;
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
            [arrData addObject:dictData];
            
        }
        
    }
    
    return arrData;
    
}

@end
