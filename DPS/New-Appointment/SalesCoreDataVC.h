//
//  SalesCoreDataVC.h
//  DPS
//
//  Created by Saavan Patidar on 24/08/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVKit/AVKit.h>
#import "Global.h"


NS_ASSUME_NONNULL_BEGIN

@interface SalesCoreDataVC : UIViewController<NSFetchedResultsControllerDelegate>{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityLeadDetail,
                        *entityImageDetail,
                        *entityEmailDetail,
                        *entityPaymentInfo,
                        *entityLeadPreference,
                        *entitySoldServiceNonStandardDetail,
                        *entitySoldServiceStandardDetail,
                        *entityDocumentsDetail,
                        *entitySalesModifyDate,
                        *entityServiceFollowUp,
                        *entityProposalFollowUp,
                        *entityLeadAgreementChecklistSetups,*entityElectronicAuthorizedForm,*entityLeadAppliedDiscounts,*entityContactNumberDetail,*entityRenewalServiceDetail,*entityTagDetail;
    
    NSEntityDescription *entityLeadCommercialScopeExtDc,*entityLeadCommercialTargetExtDc,*entityLeadCommercialMaintInfoExtDc,*entityLeadCommercialInitialInfoExtDc,*entityLeadCommercialDiscountExtDc,*entityLeadCommercialDetailExtDc,*entityLeadCommercialTermsExtDc,*entityLeadMarketingContentExtDc;

    NSEntityDescription *entityModifyDate,*entityCurrentService;

    NSEntityDescription *entityWorkOderDetailServiceAuto,
                        *entityImageDetailServiceAuto,
                        *entityEmailDetailServiceAuto,
                        *entityPaymentInfoServiceAuto,
                        *entityChemicalListDetailServiceAuto,
                        *entityModifyDateServiceAuto,
                        *entityCompanyDetailServiceAuto,
                        *entityWOProductServiceAuto,
                        *entityWOEquipmentServiceAuto,
    *entityImageDetailsTermite,
    *entityMechanicalServiceAddressPOCDetailDcs,
    *entityMechanicalBillingAddressPOCDetailDcs,*entityMechanicalSubWorkOrderNotes,*entityMechanicalSubWorkOrderActualHours,*entitySubWoEmployeeWorkingTimeExtSerDcs,*entityMechanicalSubWorkOrderIssuesRepairsParts,*entityMechanicalSubWorkOrderIssuesRepairs,*entityMechanicalSubWorkOrderIssuesRepairsLabour,*entityMechanicalSubWorkOrderHelper,*entityWorkOrderAppliedDiscountExtSerDcs,*entityMechanicalSubWorkOrder,*entityMechanicalSubWorkOrderIssues,*entitySubWOCompleteTimeExtSerDcs,*entityMechanicalEquipment,*entityMechanicalAccountDiscountExtSerDcs,*entityMechanicalWoOtherDocExtSerDcs,*entitySubWorkOrderIssues,*entitySubWorkOrderIssuesRepairParts,*entitySubWorkOrderMechanicalEquipment,*entitySubWorkOrderIssuesRepair,*entitySubWorkOrderIssuesRepairLabour,*entityMechanicalSubWorkOrderActualHrs,*entityAccountDiscount,*entityProblemImageDetail;
    
    NSEntityDescription *entityTermiteTexas;
    NSEntityDescription *entityWorkOrderDocuments;
    NSEntityDescription *entityTermiteFlorida;
    
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    
    NSFetchRequest *requestNew;
    NSFetchRequest *requestNewService;
    NSFetchRequest *requestImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;

    NSEntityDescription *entityWorkOrder;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    AppDelegate *appDelegate1;
    NSManagedObjectContext *context1;
    
    //Florida
    NSFetchRequest *requestTermiteFlorida;
    NSSortDescriptor *sortDescriptorTermiteFlorida;
    NSArray *sortDescriptorsTermiteFlorida;
    NSManagedObject *matchesTermiteFlorida,*matchesWorkOrderFlorida;
    NSArray *arrAllObjTermiteFlorida;

    NSFetchRequest *requestNewWorkOrderDocuments;
    NSSortDescriptor *sortDescriptorWorkOrderDocuments;
    NSArray *sortDescriptorsWorkOrderDocuments;
    NSManagedObject *matchesWorkOrderDocuments;
    NSArray *arrAllObjWorkOrderDocuments;
    
    NSFetchRequest *requestMechanicalSubWorkOrder;
    NSSortDescriptor *sortDescriptorMechanicalSubWorkOrder;
    NSArray *sortDescriptorsMechanicalSubWorkOrder;
    NSManagedObject *matchesMechanicalSubWorkOrder;
    NSArray *arrAllObjMechanicalSubWorkOrder;
    
    NSFetchRequest *requestSubWorkOrderIssues,*requestSubWorkOrderHelper,*requestSubWorkOrderNotes,*requestSubWorkOrderActualHrs,*requestSubWorkOrderIssuesRepair,*requestSubWorkOrderIssuesRepairParts,*requestSubWorkOrderIssuesRepairLabour,*requestSubWorkOrderMechanicalEquipment,*requestSubWOCompleteTimeExtSerDcs,*requestSubWoEmployeeWorkingTimeExtSerDcs,*requestAccountDiscount,*requestWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSSortDescriptor *sortDescriptorSubWorkOrderIssues,*sortDescriptorSubWorkOrderHelper,*sortDescriptorSubWorkOrderNotes,*sortDescriptorSubWorkOrderActualHrs,*sortDescriptorSubWorkOrderIssuesRepair,*sortDescriptorSubWorkOrderIssuesRepairParts,*sortDescriptorSubWorkOrderIssuesRepairLabour,*sortDescriptorSubWorkOrderMechanicalEquipment,*sortDescriptorSubWOCompleteTimeExtSerDcs,*sortDescriptorSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorAccDiscount,*sortDescriptorWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSArray *sortDescriptorsSubWorkOrderIssues,*sortDescriptorsSubWorkOrderHelper,*sortDescriptorsSubWorkOrderNotes,*sortDescriptorsSubWorkOrderActualHrs,*sortDescriptorsSubWorkOrderIssuesRepair,*sortDescriptorsSubWorkOrderIssuesRepairParts,*sortDescriptorsSubWorkOrderIssuesRepairLabour,*sortDescriptorsSubWorkOrderMechanicalEquipment,*sortDescriptorsSubWOCompleteTimeExtSerDcs,*sortDescriptorsSubWoEmployeeWorkingTimeExtSerDcs,*sortDescriptorsAccDiscount,*sortDescriptorsWorkOrderAppliedDiscountExtSerDcs;
    
    
    NSManagedObject *matchesSubWorkOrderIssues,*matchesSubWorkOrderHelper,*matchesSubWorkOrderNotes,*matchesSubWorkOrderActualHrs,*matchesSubWorkOrderIssuesRepair,*matchesSubWorkOrderIssuesRepairParts,*matchesSubWorkOrderIssuesRepairLabour,*matchesSubWorkOrderMechanicalEquipment,*matchesSubWOCompleteTimeExtSerDcs,*matchesSubWoEmployeeWorkingTimeExtSerDcs;
    
    
    NSArray *arrAllObjSubWorkOrderIssues,*arrAllObjSubWorkOrderHelper,*arrAllObjSubWorkOrderNotes,*arrAllObjSubWorkOrderActualHrs,*arrAllObjSubWorkOrderIssuesRepair,*arrAllObjSubWorkOrderIssuesRepairParts,*arrAllObjSubWorkOrderIssuesRepairLabour,*arrAllObjSubWorkOrderMechanicalEquipment,*arrAllObjSubWOCompleteTimeExtSerDcs,*arrAllObjSubWoEmployeeWorkingTimeExtSerDcs,*arrAllObjWorkOrderAppliedDiscountExtSerDcs;
    
}

-(void)saveToCoreDataSalesInfo :(NSArray*)arrLeadExtSerDcs :(NSString*)strLeadIdToCompare :(NSString*)strType;
-(void)saveToCoreDataServiceWo :(NSArray*)arrWorkOrderExtSerDcs :(NSString*)strWoIdToCompare :(NSString*)strType;
-(void)saveToCoreDataPlumbingWo :(NSArray*)arrWorkOrderExtSerDcs :(NSString*)strWoIdToCompare :(NSString*)strType;

-(BOOL)isSyncedWO :(NSString *)strWOid;
-(BOOL)isSyncedLeadsSales :(NSString *)strWOid;
-(NSMutableArray*)checkIfToSendSalesLeadToServer : (NSDictionary*)dictForAppoint :(NSArray*)arrAllObjModifyDate;
-(NSMutableArray*)checkifToSendServiceAppointmentToServer : (NSDictionary*)dictForWorkOrderAppointment :(NSArray*)arrAllObjServiceModifyDate;
-(NSMutableDictionary*)fetchSalesDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strLeadId;
-(NSString*)strAudioName : (NSString*)strAudioName;
-(NSMutableArray*)fetchOldLeadToDeleteFromMobileSalesFlow;
-(NSMutableArray*)fetchOldWOToDeleteFromMobileServiceFlow;
-(NSMutableDictionary*)fetchServiceDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strWoId :(NSString*)strWoNo;
-(NSMutableDictionary*)fetchPlumbingDataFromDB :(NSString*)strModifyDateToSendToServer :(NSString*)strWoId;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsWorkOrderDocuments;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServicePestNewFlowArea;
-(NSArray*)fetchDataAppointment :(NSArray*)arrOfEmpBlockTime :(NSArray*)arrOfTasks :(NSString*)isTodayAppointments;
-(NSMutableDictionary*)dictdates;
-(NSMutableArray*)fetchSubWorkOrderFromDB :(NSArray*)arrWorkOrderId;
-(NSMutableArray*)checkIfToSendSalesLeadToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjModifyDate;
-(NSMutableArray*)checkifToSendPlumbingAppointmentToServer : (NSDictionary*)dictForWorkOrderAppointment :(NSArray*)arrAllObjServiceModifyDate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalSubWorkOrder;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssues;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderHelper;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderMechanicalEquipment;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderNotes;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWOCompleteTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWoEmployeeWorkingTimeExtSerDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderActualHrs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepair;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairParts;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSubWorkOrderIssuesRepairLabour;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderAppliedDiscountExtSerDcs;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerAccountDiscount;
-(NSMutableDictionary*)plumbingStatus : (NSString*)strId;
-(NSMutableDictionary*)fetchSubWoToUpdateStatus :(NSString*)strIdd :(NSManagedObject*)objmatches;
-(void)addActualHrsIfResponseIsNil :(NSString*)strWoId :(NSString*)strSubWoId;
-(void)addStartActualHoursFromMobileToDB :(NSDictionary*)dict :(NSString*)strWoId;
-(void)upDateSubWorkOrderStatusFromDB :(NSString*)strStatusSubWorkOrder :(NSString*)strWoId :(NSString*)strSubWoId;
-(void)addStartActualHoursFromMobileToDBForOnRoute :(NSString*)strWoId :(NSString*)strSubWoId;
-(NSMutableDictionary*)showAlertIfOtherWorkOrderStarted :(NSString*)strWoNo :(NSString*)strWoId  :(NSString*)strSubWoId :(NSString*)strSubWoStatusLocal  :(NSManagedObject*)objTempSubWoMain;
-(NSMutableArray*)stopAllRunningSubWorkOrder :(NSArray*)arrOfWos;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDO;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWDOPricing;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerProblemImageDetail;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerNpmaTermite;

// 03 August Service Basic Api Implementation
-(NSMutableArray*)checkifToSendServiceAppointmentToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjServiceModifyDate;
-(NSMutableArray*)checkifToSendPlumbingAppointmentToServerNew : (NSArray*)arrOfAppoint :(NSArray*)arrAllObjServiceModifyDate;
-(void)saveWorkOrderObj : (NSDictionary*)dictDataWorkOrders;

@end

NS_ASSUME_NONNULL_END
