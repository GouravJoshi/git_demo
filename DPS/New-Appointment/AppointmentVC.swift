//
//  AppointmentVC.swift
//  DPS
//  Saavan Patidar 2020
//  Created by Saavan Patidar on 20/08/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  2021
//  2021 test
//  2021
//  Saavan Patidar 2021

import UIKit
import Alamofire
import CoreData

class CellAppointmentVC:UITableViewCell{
    
    @IBOutlet weak var lblAccName: UILabel!
    
}

class CellBlockTimeAppointmentVC:UITableViewCell{
    
    @IBOutlet weak var lblBlockTimeTitle: UILabel!
    @IBOutlet weak var lblBlockDate: UILabel!
    @IBOutlet weak var lblBlockTimeScheduleFrom: UILabel!
    @IBOutlet weak var lblBlockTimeScheduleTo: UILabel!
    @IBOutlet weak var lblBlockTimeDescriptions: UILabel!
    
    
}

class CellSalesLeadsAppointmentVC:UITableViewCell{
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblOppNo: UILabel!
    @IBOutlet weak var btnContactName: UIButton!
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var btnDriveTime: UIButton!
    @IBOutlet weak var lblTimeRange: UILabel!
    @IBOutlet weak var btnInitialSetup: UIButton!
    
    @IBOutlet weak var btnCellFooterMap: UIButton!
    @IBOutlet weak var btnCellFooterSMS: UIButton!
    @IBOutlet weak var btnCellFooterCall: UIButton!
    @IBOutlet weak var btnCellFooterEmail: UIButton!
    @IBOutlet weak var btnCellFooterPrint: UIButton!
    @IBOutlet weak var btnCellFooterResendMail: UIButton!
    @IBOutlet weak var btnResetStatus: UIButton!
    @IBOutlet weak var btnReSyncDataToServer: UIButton!
    
    @IBOutlet var btnFlowType: UIButton!
    
    @IBOutlet var imgFlowType: UIImageView!
    // Commit
}

class CellServiceAppointmentVC:UITableViewCell{
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblWoNo: UILabel!
    @IBOutlet weak var btnContactName: UIButton!
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var btnDriveTime: UIButton!
    @IBOutlet weak var lblTimeRange: UILabel!
    
    @IBOutlet weak var btnCellFooterMap: UIButton!
    @IBOutlet weak var btnCellFooterSMS: UIButton!
    @IBOutlet weak var btnCellFooterCall: UIButton!
    @IBOutlet weak var btnCellFooterEmail: UIButton!
    @IBOutlet weak var btnCellFooterPrint: UIButton!
    @IBOutlet weak var btnCellFooterResendMail: UIButton!
    @IBOutlet weak var btnIsLocked: UIButton!
    @IBOutlet weak var btnReSyncDataToServer: UIButton!
    @IBOutlet weak var cellBackCardView: CardView!
    @IBOutlet weak var cellBottomButtonsBackCardView: CardView!

}

class CellPlumbingAppointmentVC:UITableViewCell{
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblWoNo: UILabel!
    @IBOutlet weak var btnContactName: UIButton!
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var btnDriveTime: UIButton!
    @IBOutlet weak var lblTimeRange: UILabel!
    
    @IBOutlet weak var btnCellFooterMap: UIButton!
    @IBOutlet weak var btnCellFooterSMS: UIButton!
    @IBOutlet weak var btnCellFooterCall: UIButton!
    @IBOutlet weak var btnCellFooterEmail: UIButton!
    @IBOutlet weak var btnCellFooterPrint: UIButton!
    @IBOutlet weak var btnCellFooterResendMail: UIButton!
    @IBOutlet weak var btnCellFooterSubWoStatus: UIButton!
    
}

class AppointmentVC: UIViewController,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate {
    
    // MARK: -----------------------------------------OutLets-----------------------------------------
    
    @IBOutlet weak var searchBarAppointment: UISearchBar!
    @IBOutlet weak var tblViewAppointment: UITableView!
    @IBOutlet weak var viewTopButton: UIView!
    @IBOutlet weak var btnAppointment: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var lblTodayDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    @IBOutlet weak var headerView: TopView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    // MARK: -----------------------------------------Variables-----------------------------------------
    
    var arrayAppointment = NSArray() , arrayOfLeadsToFetchDynamicFormData = NSMutableArray() , arrayOfWoToFetchDynamicFormData = NSMutableArray() , arrOfAllImagesToSendToServer = NSMutableArray() , arrOfAllSignatureImagesToSendToServer = NSMutableArray() , arrOfAllCheckImageToSend = NSMutableArray() , arrCheckImage = NSMutableArray() , arrAllTargetImages = NSMutableArray(), arrayOfPlumbingWoToFetchDynamicFormData = NSMutableArray()
    
    var arrayAppointmentData = NSArray(),arrayAppointmentDataMain = NSArray()
    var dictFilter = NSMutableDictionary()
    
    var indexToSendImage = Int() , indexToSendImageSign = Int() , indexToSendCheckImages = Int() , indexToSendDocuments = Int() , indexDynamicFormToSend = Int() , indexDynamicFormToSendServiceAuto = Int()
    var refresher = UIRefreshControl()
    var dictLoginData = NSDictionary()
    var loader = UIAlertController()
    var isCallDelay = ""
    var loaderPlumbing = UIAlertController()
    var loaderPlumbingStopWO = UIAlertController()
    
    var strEmpNo = "" , strEmpName = "" , strSalesProcessCompanyId = "" , strCompanyKey = "" , strSalesProcessModuleCompanyId = "" , strServiceAutoUrlMain = "" , strServiceAutoCompanyId = "" , strSalesAutoUrlMain = "" , strEmployeeId = "" , strCoreServiceModuleUrlMain = "" , strUserName = "" , strEmployeeEProfileUrl = "" , strIsGGQIntegration = "" , strGGQCompanyKey = "" , strCoreCompanyId = "" , strWorkOrderNoLifeStyle = "" , strServiceAddressImageUrl = "", strEmpEmail = "", strEmpLoginBranchSysName = "", strEmpLoginBranchId = "", strSalesAutoCompanyId = "", strEmpId = ""
    
    let dispatchGroup = DispatchGroup()
    var dispatchGroupSales = DispatchGroup()
    var dispatchGroupServiceAuto = DispatchGroup()
    var dispatchGroupPlumbing = DispatchGroup()
    var selectedIndexToUpdateStatus = -1
    var dispatchGroupPlumbingStopWO = DispatchGroup()
    var strSubWoStatusForForcefullyStopWos = ""
    var objSubWosForForcefullyStopWos = NSManagedObject()
    var strSearchText = ""
    var strTodayAll = "Today"

    var dictOpportunityStageNameFromSysName = NSDictionary()
    var dictOpportunityStatusNameFromSysName = NSDictionary()
    var isEditSchedule = false
    
    var strConfigFromDateSalesAuto = ""
    var strConfigFromDateServiceAuto  = ""
    
    var aryApprovalCheckList = NSMutableArray()
    // MARK: --------------------------------Life Cycle-----------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            segment.addTarget(self, action: #selector(self.indexChangedAppointment(_:)), for: .valueChanged)
            segment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)], for: .normal)
            
            if isCallDelay == "yes"{
                
                sleep(3)
                
            }
            
            // Do any additional setup after loading the view.
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            isEditSchedule = (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsRescheduleSalesAppointment") ?? false) as! Bool

            tblViewAppointment.tableFooterView = UIView()
            tblViewAppointment.estimatedRowHeight = 50.0
            
            self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
            self.tblViewAppointment!.addSubview(refresher)
            
            setupViews()
            
            getLoginDataOnAppointment()
            //createBadgeView()
            
            allApiCalling()
            
            lblTodayDate.text = "Today, " + "\(Global().strGetCurrentDate(inFormat: "d MMM yyyy") ?? "")"
            
            UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")
            UserDefaults.standard.set(false, forKey: "isLeadOppAddedUpdated")
            
    //        if(DeviceType.IS_IPAD){
    //
    //            lbltaskCount.text = "0"
    //            lblScheduleCount.text = "0"
    //            lbltaskCount.layer.cornerRadius = 18.0
    //            lbltaskCount.backgroundColor = UIColor.red
    //            lblScheduleCount.layer.cornerRadius = 18.0
    //            lblScheduleCount.backgroundColor = UIColor.red
    //            lbltaskCount.layer.masksToBounds = true
    //            lblScheduleCount.layer.masksToBounds = true
    //
    //        }
            getOpportunityStatusNameFromSysName()
            
            
            self.aryApprovalCheckList =  getApprovalCheckList().mutableCopy() as! NSMutableArray
            
            //NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshListReport(notification:)), name: Notification.Name("RefreshAppointmentList"), object: nil)


        }
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
            self.setFooterMenuOption()
        })
        
    }
    
//    @objc func RefreshListReport(notification: Notification) {
//
//        RefreshloadData()
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("RefreshAppointmentList"), object: nil)
//
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            if nsud.bool(forKey: "isTaskAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isTaskAddedUpdated")
                    
                    self.allApiCalling()
                    
                }
                
            }
            
            if nsud.bool(forKey: "isLeadOppAddedUpdated") {
                
                if (isInternetAvailable()){
                    
                    UserDefaults.standard.set(false, forKey: "isLeadOppAddedUpdated")
                    
                    self.allApiCalling()
                    
                }
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            /*if nsud.bool(forKey: "backfromSalesGeneralInfo") == true
            {
                nsud.set(false, forKey: "backfromSalesGeneralInfo")
                nsud.synchronize()
                tblViewAppointment.reloadData()
            }*/
            tblViewAppointment.reloadData()
            
            //For Clark pest issue 09 Sept 2021
            
            if(nsud.value(forKey: "RefreshAppointmentList") != nil){
                if(nsud.value(forKey: "RefreshAppointmentList") as! Bool){
                    nsud.setValue(false, forKey: "RefreshAppointmentList")
                    self.RefreshloadData()
                }
            }
        }
      

    }
    
    
    // MARK: --------------------------------Initial Loading Functions-----------------------------------
    
    func setupViews()
    {
        
        viewTopButton.backgroundColor = UIColor.white
        
        btnAppointment.layer.cornerRadius = btnAppointment.frame.size.height/2
        btnAppointment.layer.borderWidth = 0
        
        viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        viewTopButton.layer.borderWidth = 0
        
        btnCalendar.layer.cornerRadius = btnCalendar.frame.size.height/2
        btnCalendar.layer.borderWidth = 0
        
        viewTopButton.bringSubviewToFront(btnCalendar)
        
        if let txfSearchField = searchBarAppointment.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
        }
        searchBarAppointment.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarAppointment.layer.borderWidth = 1
        searchBarAppointment.layer.opacity = 1.0
        //Test
    }
    
    func getLoginDataOnAppointment() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        strEmployeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName")!)"
        strEmpEmail = "\(dictLoginData.value(forKey: "EmployeeEmail")!)"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username")!)"
        strEmployeeEProfileUrl = "\(dictLoginData.value(forKey: "EmployeeEprofile")!)"
        strIsGGQIntegration = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsGGQIntegration")!)"
        strGGQCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GGQCompanyKey")!)"
        
        strSalesProcessCompanyId = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId")!)"
        strServiceAutoCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)"
        strSalesProcessModuleCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.CompanyId")!)"
        strCoreCompanyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId")!)"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        
        strSalesAutoUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")!)"
        strServiceAutoUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)"
        strCoreServiceModuleUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl")!)"
        strServiceAddressImageUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        strEmpLoginBranchId = "\(dictLoginData.value(forKey: "EmployeeBranchId")!)"
        strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        strSalesAutoCompanyId = "\(dictLoginData.value(forKeyPath: "Company.SalesAutoCompanyId") ?? "")"
        strEmpId = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
    }
    
    func createBadgeView() {
        
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                            
                            let badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                            
                        }
                        
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: --------------------------------------Obj C Functions---------------------------------------
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                //Call Api's
                
                self.lblTodayDate.text = "Today, " + "\(Global().strGetCurrentDate(inFormat: "d MMM yyyy") ?? "")"
                
                self.lblStatus.text = "Incomplete"
                
                self.segment.selectedSegmentIndex = 0
                
                self.strTodayAll = "Today"

                self.allApiCalling()
                
            })
            
        }else{
            
            // Offline get Data
            
        }
        
    }
    
    @objc func actionOnTaskCheckMark(sender: UIButton!)
    {
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            //let section = sender.tag / 1000
            let row = sender.tag// % 1000
            let dictData = arrayAppointment[row] as! NSManagedObject
            
            callAPIToUpdateTaskMarkAsDone(dictData: dictData,index: row)
            
        }
    }
    @objc func actionOnResetStatus(sender: UIButton!)
    {
        
        let alertController = UIAlertController(title: alertMessage, message: "Are you sure want to reopen opportunity", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        { [self]
            UIAlertAction in
            NSLog("OK Pressed")
            
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                let row = sender.tag
                
                let dictData = self.arrayAppointment[row] as! NSManagedObject
                
                if dictData .isKind(of: EmployeeBlockTime.self)
                {
                }
                else if dictData .isKind(of: TaskAppointmentVC.self)
                {
                }
                else if dictData .isKind(of: WorkOrderDetailsService.self)
                {
                    
                }
                else
                {
                    
                    let branchSysNameLocal = "\(dictData.value(forKey: "branchSysName")!)"
                    
                    if branchSysNameLocal == strEmpLoginBranchSysName {
                        
                        self.callAPIToResetOpportunityStatus(dictData: dictData,index: row)

                    }else{
                        
                        self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
                        
                    }
                    
                }
            }
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            NSLog("No Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func actionOnReSyncSalesDataToServer(sender: UIButton!)
    {
        
        let alertController = UIAlertController(title: alertMessage, message: "Are you sure want to sync opportunity", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        { [self]
            UIAlertAction in
            NSLog("OK Pressed")
            
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                let row = sender.tag
                
                let dictData = self.arrayAppointment[row] as! NSManagedObject
                
                if dictData .isKind(of: EmployeeBlockTime.self)
                {
                }
                else if dictData .isKind(of: TaskAppointmentVC.self)
                {
                }
                else if dictData .isKind(of: WorkOrderDetailsService.self)
                {
                    
                }
                else
                {
                    //  Update Modify date And Re Sync Appointment.
                    Global().updateSalesModifydate("\(dictData.value(forKey: "leadId")!)")
                    self.RefreshloadData()
                                        
                }
            }
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            NSLog("No Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func actionOnReSyncServiceDataToServer(sender: UIButton!)
    {
        
        let alertController = UIAlertController(title: alertMessage, message: "Are you sure want to sync work order", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        { [self]
            UIAlertAction in
            NSLog("OK Pressed")
            
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                let row = sender.tag
                
                let dictData = self.arrayAppointment[row] as! NSManagedObject
                
                if dictData .isKind(of: EmployeeBlockTime.self)
                {
                }
                else if dictData .isKind(of: TaskAppointmentVC.self)
                {
                }
                else if dictData .isKind(of: WorkOrderDetailsService.self)
                {
                    
                    //  Update Modify date And Re Sync Appointment.
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: "\(dictData.value(forKey: "workorderId")!)")
                    self.RefreshloadData()
                    
                }
                else
                {

                                        
                }
            }
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            NSLog("No Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: --------------------------- UIButton action ---------------------------
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        if nsud.bool(forKey: "fromCompanyVC") {
            
            nsud.set(false, forKey: "fromCompanyVC")
            self.navigationController?.popViewController(animated: false)
            
        }else{
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as? DashBoardNew_iPhoneVC
            self.navigationController?.pushViewController(vc!, animated: false)
            
        }
        
    }
    
    @IBAction func actionOnContact(_ sender: UIButton)
    {
        
        self.view.endEditing(true)
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Appointment_iPAD" : "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "AppoinmentFilterVC") as? AppoinmentFilterVC
        vc!.dictFilter = self.dictFilter
        vc!.delegate = self
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton)
    {
        
        self.view.endEditing(true)
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        //testController.strFromVC = strFromVC
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            
            
            let Near = (UIAlertAction(title: "Near By", style: .default , handler:{ (UIAlertAction)in
                print("User click Near By button")
                let global = Global()
                let isnet = global.isNetReachable()
                if isnet {
                    
                    let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
                    
                    //VSP
                    /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone*/
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                } else {
                    global.alertMethod(Alert, ErrorInternetMsg)
                }
            }))
            Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
            Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(Near)
            
            let Signed = (UIAlertAction(title: "Signed View Agreements", style: .default , handler:{ (UIAlertAction)in
                print("User click Signed View Agreements")
                if(DeviceType.IS_IPAD){
                    let mainStoryboard = UIStoryboard(
                        name: "CRMiPad",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                    self.navigationController?.present(objByProductVC!, animated: true)
                }else{
                    let mainStoryboard = UIStoryboard(
                        name: "CRM",
                        bundle: nil)
                    let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                    self.navigationController?.present(objByProductVC!, animated: true)
                }
                
            }))
            
            Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
            Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(Signed)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = (sender as AnyObject).bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            let AddBlockTime = (UIAlertAction(title: "Add Block Time", style: .default , handler:{ (UIAlertAction)in
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "AddBlockTime" : "AddBlockTime", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddBlockTimeVC") as? AddBlockTimeVC
                vc?.handelDataOnAddingBlockTime = self
                self.navigationController?.pushViewController(vc!, animated: false)
            }))
            
            AddBlockTime.setValue(#imageLiteral(resourceName: "addBlockIcon"), forKey: "image")
            AddBlockTime.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddBlockTime)
            
            let AddTask = (UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                
                print("User click Add Task button")
                let defs = UserDefaults.standard
                defs.setValue(nil, forKey: "TaskFilterSort")
                defs.synchronize()
                
                let objWebService = WebService()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
                
                objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
            }))
            
            AddTask.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
            AddTask.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddTask)
            
            
            
            let AddActivity = (UIAlertAction(title: "Add Activity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Activity button")
                
                let objWebService = WebService()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
                
                
                
                objByProductVC.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddActivity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
            AddActivity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddActivity)
            
            let AddLead = (UIAlertAction(title: "Add Lead/Opportunity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Add Lead button")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
                self.navigationController?.pushViewController(vc, animated: false)
            }))
            AddLead.setValue(#imageLiteral(resourceName: "Lead"), forKey: "image")
            AddLead.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddLead)
            
            
            let AddContact = (UIAlertAction(title: "Add Contact", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Contact  button")
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                
                
                
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddContact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
            AddContact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddContact)
            
            
            let AddCompany = (UIAlertAction(title: "Add Company", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Company  button")
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                
                
                self.navigationController?.pushViewController(controller, animated: false)
            }))
            AddCompany.setValue(#imageLiteral(resourceName: "CompanyAdd"), forKey: "image")
            AddCompany.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddCompany)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    @IBAction func actionOnHome(_ sender: Any)
    {
        self.view.endEditing(true)
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnCalendar(_ sender: Any)
    {
        self.view.endEditing(true)
        let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Appointment_iPAD" : "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        objByProductVC.aryData = self.arrayAppointmentDataMain.mutableCopy()as! NSMutableArray
        self.navigationController?.pushViewController(objByProductVC, animated: false)
    }
    // MARK: --------------------------- Api's Calling ---------------------------
    
    func allApiCalling()  {
        
        if(isInternetAvailable()){
            
            // Start Loader
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            
            // Check if Sales Auto Is Enabled
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.IsActive") as! Bool) == true {
                
                //check if data present in DB or not.
                
                let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesAutoModifyDate, predicate: NSPredicate(format: "userName == %@", strUserName))
                
                if arrayOfLeadsLocal.count == 0 {
                    
                    // No data in local DB to compare so call get API and Save All Data into DB.
                    
                    getSalesAutoAppointmentsV1(strLeadId: "")
                    
                }else{
                    
                    // get basic data to compare date and send
                    
                    getSalesAutoAppointmentsWithBasicInfo()
                    
                }
                                
            }
                // Check if Service Auto enabled
                if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") as! Bool) == true {
                    
                    //check if data present in DB or not.
                    
                    let arrayOfWorkOrderLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceWorkOrderDetailsService, predicate: NSPredicate(format: "userName == %@", strUserName))
                    
                    if arrayOfWorkOrderLocal.count == 0 {
                        
                        // No data in local DB to compare so call get API and Save All Data into DB.
                        
                        getServiceAutoAppointmentsV1(arrOfWorkOrderIdLocal: NSArray())
                        
                    }else{
                        
                        // get basic data to compare date and send
                        
                        getServiceAutoAppointmentsWithBasicInfo()
                        
                    }
                    
                }
                
     
            
   
            if(DeviceType.IS_IPAD){
                
                // Check if Service Auto enabled
                if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") as! Bool) == true {
                    
                    //check if data present in DB or not.
                    
                    let arrayOfWorkOrderLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceWorkOrderDetailsService, predicate: NSPredicate(format: "userName == %@", strUserName))
                    
                    if arrayOfWorkOrderLocal.count == 0 {
                        
                        // No data in local DB to compare so call get API and Save All Data into DB.
                        
                        getPlumbingAppointmentsV1(arrOfWorkOrderIdLocal: NSArray())
                        
                    }else{
                        
                        // get basic data to compare date and send
                        
                        getPlumbingAppointmentsWithBasicInfo()
                        
                    }
                    
                }
                
            }
            
            // Call Api To Get Employee Block Time
            
            getEmployeeBlockTime()
            
            // Call Api To get Task List
            
            callAPIToGetTaskList()
            
            Global().getUnsignedDocumentsWoNos(strServiceAutoUrlMain, strCompanyKey)
            
            // Code for Favourite Items
            
            if DeviceType.IS_IPAD
            {
                var chkForSync = Bool()
                chkForSync = false
                
                if nsud.bool(forKey: "isSyncFavourite") == true
                {
                    
                    self.callAPIMarkAsFavouriteNew(strType: "Finding")
                    
                }
                else
                {
                    
                    //self.getFavouriteCodes()
                    chkForSync = true
                    
                }
                if nsud.bool(forKey: "isSyncFavouriteNote") == true
                {
                    
                    self.callAPIMarkAsFavouriteNew(strType: "Note")
                    
                }
                else
                {
                    
                    //self.getFavouriteNotes()
                    chkForSync = true
                    
                }
                
                if chkForSync == true
                {
                    self.getFavouriteCodeNotesNew() //For Finding and Notes
                }
                else
                {
                    //self.callAPIMarkAsFavouriteNew(strType: "") //For Finding and Notes

                }
                
            }
            
            self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                // All data downloaded
                self.loader.dismiss(animated: false) {
                    DispatchQueue.main.async {
                        // Reload Table
                        
                        self.refresher.endRefreshing()
                        
                        print("Merged All Data called")
                        
                        self.mergeAllData()
                        
                    }
                }
            })
            
        }else{
            
            //Global().alertMethod(Alert, ErrorInternetMsg)
            
            self.mergeAllData()
            
        }
        
    }
    
    func mergeAllData() {
        
        self.arrayAppointment = NSArray()
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "employeeNo_Mobile == %@", strEmpNo))
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceWorkOrderDetailsService, predicate: NSPredicate(format: "dbUserName == %@", strEmpNo))
        
        let arrayOfBlockTime = getDataFromCoreDataBaseArray(strEntity: Entity_EmployeeBlockTimeAppointmentVC, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        let arrOfTasksList = getDataFromCoreDataBaseArray(strEntity: Entity_CrmTaskAppointmentVC, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        self.arrayAppointment = arrayOfLeadsLocal + arrayOfWoLocal
        
        let arrMergedBlockTimeAndTask = arrayOfBlockTime + arrOfTasksList
        
        self.arrayAppointment = Global().filterData(afterMergingAppointments: arrMergedBlockTimeAndTask as? [Any] , self.arrayAppointment as? [Any] )! as NSArray
        
        self.arrayAppointmentData = NSArray()
        self.arrayAppointmentDataMain = NSArray()
        self.arrayAppointmentData = self.arrayAppointment
        self.arrayAppointmentDataMain  = self.arrayAppointment
        
        self.arrayAppointmentData = NSArray()
        self.arrayAppointment = NSArray()
        self.arrayAppointment = SalesCoreDataVC().fetchDataAppointment(arrayOfBlockTime as! [Any], arrOfTasksList as! [Any], "yes") as NSArray
        self.arrayAppointmentData = self.arrayAppointment
        self.tblViewAppointment.reloadData()
        
        self.filterOnClickHeader(strType: strTodayAll)
        
    }
    
    func todayData() {
        
        //workorderStatus
        
        self.arrayAppointment = NSArray()
        
        let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
        let currentdate = Date(timeIntervalSinceReferenceDate: timeInterval)
        
        var andPredicateLead : [NSPredicate] = []
        
        let subPredicateLead = NSPredicate(format: "employeeNo_Mobile == %@", strEmpNo)
        let subPredicateLead1 = NSPredicate(format: "statusSysName != %@", "Complete")
        let subPredicateLead2 = NSPredicate(format: "statusSysName != %@", "Won")
        
        andPredicateLead.append(subPredicateLead)
        andPredicateLead.append(subPredicateLead1)
        andPredicateLead.append(subPredicateLead2)
        
        let andPredicateLeadMain = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateLead)
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesLeadDetail, predicate: andPredicateLeadMain)
        
        var andPredicateWO : [NSPredicate] = []
        
        let subPredicateWO = NSPredicate(format: "dbUserName == %@", strEmpNo)
        let subPredicateWO1 = NSPredicate(format: "workorderStatus == %@", "InComplete")
        let subPredicateWO2 = NSPredicate(format: "workorderStatus == %@", "Incomplete")
        
        andPredicateWO.append(subPredicateWO)
        andPredicateWO.append(subPredicateWO1)
        andPredicateWO.append(subPredicateWO2)
        
        let andPredicateWOMain = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateWO)
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceWorkOrderDetailsService, predicate: andPredicateWOMain)
        
        let arrayOfBlockTime = getDataFromCoreDataBaseArray(strEntity: Entity_EmployeeBlockTimeAppointmentVC, predicate: NSPredicate(format: "userName == %@ AND fromDateFormatted = %@", strUserName , currentdate as CVarArg))
        
        let arrOfTasksList = getDataFromCoreDataBaseArray(strEntity: Entity_CrmTaskAppointmentVC, predicate: NSPredicate(format: "userName == %@ AND status = %@ AND dueDateFormatted = %@", strUserName , "Open" , currentdate as CVarArg))
        
        self.arrayAppointment = arrayOfLeadsLocal + arrayOfWoLocal
        
        let arrMergedBlockTimeAndTask = arrayOfBlockTime + arrOfTasksList
        
        self.arrayAppointment = Global().filterData(afterMergingAppointments: arrMergedBlockTimeAndTask as? [Any] , self.arrayAppointment as? [Any] )! as NSArray
        
        self.arrayAppointmentData = NSArray()
        self.arrayAppointmentData = self.arrayAppointment
        self.tblViewAppointment.reloadData()
        
    }
    
    func callAPIMarkAsFavourite()
    {

        let start1 = CFAbsoluteTimeGetCurrent()

        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)"
        
        WebService.postRequestWithHeaders(dictJson: getPostData() , url: strURL, responseStringComing: "TimeLine") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to AddUpdateFindingFavoriteDetail API Data From SQL Server.")
                
                let dictResponse = response.value(forKey: "data") as! NSDictionary
                let arrData = dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") as! NSArray
                let arrTemp = NSMutableArray()
                if arrData.count > 0
                {
                    for item in arrData
                    {
                        let dict = item as! NSDictionary
                        arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                    }
                }
                var arrOfFavouriteCodes = NSArray()
                arrOfFavouriteCodes = arrTemp
                nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                nsud.set(false, forKey: "isSyncFavourite")
                nsud.synchronize()
                
            }
            
        }
        
        
    }
    
    func getFavouriteCodes()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()

        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/GetWdoFindingFavoriteDetail?CompanyKey=\(strCompanyKey)&EmployeeNo=\(strEmpNumber)"
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "Description") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(Status){
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to GetWdoFindingFavoriteDetail API Data From SQL Server.")
                
                let arrData = Response["data"] as! NSArray
                
                if arrData.isKind(of: NSArray.self)
                {
                    if arrData.count > 0
                    {
                        //arrOfFavouriteCodes = arrData
                        var arrOfFavouriteCodes = NSArray()
                        //Temp
                        
                        let arrTemp = NSMutableArray()
                        for item in arrData
                        {
                            let dict = item as! NSDictionary
                            arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                        }
                        
                        arrOfFavouriteCodes = arrTemp
                        nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                        nsud.set(false, forKey: "isSyncFavourite")
                        nsud.synchronize()
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    func getPostDataNew(strType : String) -> NSDictionary
    {
        
        var dict = [String:Any]()
        
        let arr = NSMutableArray()
    
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        if strType == "Note"
        {
            var arrOfFavouriteNotes = NSArray()
            
            if nsud.value(forKey: "favouriteNote") is NSArray
            {
                let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteNote") as! NSArray
                arrOfFavouriteNotes = arrFvouriteCodesTemp
            }
            else
            {
                arrOfFavouriteNotes = NSArray()
            }
            
            for item in arrOfFavouriteNotes
            {
                let strId = item as! String
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("WoWdoNoteFavoriteDetailId")
                arrOfKeys.add("NoteId")
                arrOfKeys.add("EmployeeId")
                arrOfKeys.add("CompanyId")
                arrOfKeys.add("CreatedDate")
                arrOfKeys.add("CreatedBy")
                arrOfKeys.add("ModifiedDate")
                arrOfKeys.add("ModifiedBy")
                
                arrOfValues.add("")
                arrOfValues.add("\(strId)")
                arrOfValues.add("\(strEmpID)")
                arrOfValues.add("\(strCompanyId)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                
                let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                arr.add(dict_ToSend)
                
            }
            
            dict = [
                    "WoWdoNoteFavoriteDetailExtSerDcs":arr,
            ]

        }
        else
        {
            var arrFvouriteCodesTemp = NSArray()
            
            if nsud.value(forKey: "favouriteCode") is NSArray
            {
                //arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
                
                arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
                
                let arrUniqueDocuments = arrFvouriteCodesTemp.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
            
                arrFvouriteCodesTemp = arrUniqueDocuments//arrFvouriteCodesTemp
            }
            
            
            for item in arrFvouriteCodesTemp
            {
                let strId = item as! String
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("WoWdoFindingFavoriteDetailId")
                arrOfKeys.add("CodeMasterId")
                arrOfKeys.add("EmployeeId")
                arrOfKeys.add("CompanyId")
                arrOfKeys.add("CreatedDate")
                arrOfKeys.add("CreatedBy")
                arrOfKeys.add("ModifiedDate")
                arrOfKeys.add("ModifiedBy")
                
                arrOfValues.add("")
                arrOfValues.add("\(strId)")
                arrOfValues.add("\(strEmpID)")
                arrOfValues.add("\(strCompanyId)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                arrOfValues.add(Global().getCurrentDate())
                arrOfValues.add("\(strUserName)")
                
                let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                arr.add(dict_ToSend)
                
            }
            
            dict = [
                "WoWdoFindingFavoriteDetailExtSerDcs":arr
            ]
            
        }
        
        return dict as NSDictionary
    }
    
    func callAPIMarkAsFavouriteNew(strType : String)
    {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateNoteAndFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)&Type=\("\(strType)")"
        
        WebService.postRequestWithHeaders(dictJson: getPostDataNew(strType: strType) , url: strURL, responseStringComing: "TimeLine") { (response, status) in
            
            if(status == true)
            {
                
                let dictResponse = response.value(forKey: "data") as! NSDictionary
                
                if strType == "Note" //ForNotes
                {
                    let arrData = dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") as! NSArray
                    let arrTemp = NSMutableArray()
                    if arrData.count > 0
                    {
                        for item in arrData
                        {
                            let dict = item as! NSDictionary
                            arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                        }
                    }
                    var arrOfFavouriteCodes = NSArray()
                    arrOfFavouriteCodes = arrTemp
                    nsud.set(arrOfFavouriteCodes, forKey: "favouriteNote")
                    nsud.set(false, forKey: "isSyncFavouriteNote")
                    nsud.synchronize()
                }
                else if strType == "Finding"  //For Finding Code
                {
                    let arrData = dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") as! NSArray
                    let arrTemp = NSMutableArray()
                    if arrData.count > 0
                    {
                        for item in arrData
                        {
                            let dict = item as! NSDictionary
                            arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                        }
                    }
                    var arrOfFavouriteCodes = NSArray()
                    arrOfFavouriteCodes = arrTemp
                    nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                    nsud.set(false, forKey: "isSyncFavourite")
                    nsud.synchronize()
                }
                else
                {
                    //For Notes
                    let arrDataNotes = dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") as! NSArray
                    let arrTempNotes = NSMutableArray()
                    if arrDataNotes.count > 0
                    {
                        for item in arrDataNotes
                        {
                            let dict = item as! NSDictionary
                            arrTempNotes.add("\(dict.value(forKey: "NoteId") ?? "")")
                        }
                    }
                    var arrOfFavouriteNotes = NSArray()
                    arrOfFavouriteNotes = arrTempNotes
                    nsud.set(arrOfFavouriteNotes, forKey: "favouriteNote")
                    nsud.set(false, forKey: "isSyncFavouriteNote")
                    nsud.synchronize()
                    
                    
                    //ForFinding
                    
                    let arrDataFinding = dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") as! NSArray
                    let arrTempFinding = NSMutableArray()
                    if arrDataFinding.count > 0
                    {
                        for item in arrDataFinding
                        {
                            let dict = item as! NSDictionary
                            arrTempFinding.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                        }
                    }
                    var arrOfFavouriteFinding = NSArray()
                    arrOfFavouriteFinding = arrTempFinding
                    nsud.set(arrOfFavouriteFinding, forKey: "favouriteCode")
                    nsud.set(false, forKey: "isSyncFavourite")
                    nsud.synchronize()
                }
                
            }
            
        }
    }
    
    func getPostData() -> NSDictionary
    {
        let arr = NSMutableArray()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        var arrFvouriteCodesTemp = NSArray()
        
        if nsud.value(forKey: "favouriteCode") is NSArray
        {
            //arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
            
            arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
            
            let arrUniqueDocuments = arrFvouriteCodesTemp.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
            arrFvouriteCodesTemp = arrUniqueDocuments//arrFvouriteCodesTemp
        }
        
        
        for item in arrFvouriteCodesTemp
        {
            let strId = item as! String
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("WoWdoFindingFavoriteDetailId")
            arrOfKeys.add("CodeMasterId")
            arrOfKeys.add("EmployeeId")
            arrOfKeys.add("CompanyId")
            arrOfKeys.add("CreatedDate")
            arrOfKeys.add("CreatedBy")
            arrOfKeys.add("ModifiedDate")
            arrOfKeys.add("ModifiedBy")
            
            arrOfValues.add("")
            arrOfValues.add("\(strId)")
            arrOfValues.add("\(strEmpID)")
            arrOfValues.add("\(strCompanyId)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            
            let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
            arr.add(dict_ToSend)
            
        }
        
        var dict = [String:Any]()
        
        dict = [
            "WoWdoFindingFavoriteDetailExtSerDcs":arr
        ]
        
        return dict as NSDictionary
    }
    
    func getPostDataNotes() -> NSDictionary
    {
        let arr = NSMutableArray()
        var arrOfFavouriteNotes = NSArray()

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"

        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        if nsud.value(forKey: "favouriteNote") is NSArray
        {
            let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteNote") as! NSArray
            arrOfFavouriteNotes = arrFvouriteCodesTemp
        }
        else
        {
            arrOfFavouriteNotes = NSArray()
        }
        
        for item in arrOfFavouriteNotes
        {
            let strId = item as! String
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("WoWdoNoteFavoriteDetailId")
            arrOfKeys.add("NoteId")
            arrOfKeys.add("EmployeeId")
            arrOfKeys.add("CompanyId")
            arrOfKeys.add("CreatedDate")
            arrOfKeys.add("CreatedBy")
            arrOfKeys.add("ModifiedDate")
            arrOfKeys.add("ModifiedBy")
            
            arrOfValues.add("")
            arrOfValues.add("\(strId)")
            arrOfValues.add("\(strEmpID)")
            arrOfValues.add("\(strCompanyId)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            
            let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
            arr.add(dict_ToSend)
            
        }

        var dict = [String:Any]()
        

        dict = [
                "WoWdoNoteFavoriteDetailExtSerDcs":arr,
        ]
        
        return dict as NSDictionary
    }
    
    func callAPIMarkAsFavouriteNote()
    {

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
                
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateNoteAndFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)&Type=\("Note")"
        
        WebService.postRequestWithHeaders(dictJson: getPostDataNotes() , url: strURL, responseStringComing: "TimeLine") { (response, status) in
            
            if(status == true)
            {
                
                let dictResponse = response.value(forKey: "data") as! NSDictionary
                let arrData = dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") as! NSArray
                let arrTemp = NSMutableArray()
                if arrData.count > 0
                {
                    for item in arrData
                    {
                        let dict = item as! NSDictionary
                        arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                    }
                }
                var arrOfFavouriteCodes = NSArray()
                arrOfFavouriteCodes = arrTemp
                nsud.set(arrOfFavouriteCodes, forKey: "favouriteNote")
                nsud.set(false, forKey: "isSyncFavouriteNote")
                nsud.synchronize()
                
            }

        }
        
    }
    func getFavouriteNotes()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()

        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/GetWdoNoteAndFindingFavoriteDetail?CompanyKey=\(strCompanyKey)&EmployeeNo=\(strEmpNumber)"

        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "Description") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(Status){
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to GetWdoFindingFavoriteDetail API Data From SQL Server.")
                                
                let dictResponse = Response["data"] as! NSDictionary
                
                let arrData = dictResponse["WoWdoNoteFavoriteDetailExtSerDcs"] as! NSArray
                
                if arrData.isKind(of: NSArray.self)
                {
                    if arrData.count > 0
                    {
                        //arrOfFavouriteCodes = arrData
                        var arrOfFavouriteNotes = NSArray()
                        //Temp
                        
                        let arrTemp = NSMutableArray()
                        for item in arrData
                        {
                            let dict = item as! NSDictionary
                            arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                        }
                        arrOfFavouriteNotes = arrTemp
                        nsud.set(arrOfFavouriteNotes, forKey: "favouriteNote")
                        nsud.set(false, forKey: "isSyncFavouriteNote")
                        nsud.synchronize()

                        //ENd
                    }
                    else
                    {
                        
                    }
                }
                
            }
            
        }
        
    }
    func getFavouriteCodeNotesNew()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/GetWdoNoteAndFindingFavoriteDetail?CompanyKey=\(strCompanyKey)&EmployeeNo=\(strEmpNumber)"
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "Description") { (Response, Status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(Status){
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to GetWdoFindingFavoriteDetail API Data From SQL Server.")
                
                let dictResponse = Response["data"] as! NSDictionary
                
                //For Notes
                
                if dictResponse["WoWdoNoteFavoriteDetailExtSerDcs"] is NSArray {
                    
                    let arrDataNotes = dictResponse["WoWdoNoteFavoriteDetailExtSerDcs"] as! NSArray
                    
                    if arrDataNotes.isKind(of: NSArray.self)
                    {
                        if arrDataNotes.count > 0
                        {
                            //arrOfFavouriteCodes = arrData
                            var arrOfFavouriteNotes = NSArray()
                            //Temp
                            
                            let arrTemp = NSMutableArray()
                            for item in arrDataNotes
                            {
                                let dict = item as! NSDictionary
                                arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                            }
                            arrOfFavouriteNotes = arrTemp
                            nsud.set(arrOfFavouriteNotes, forKey: "favouriteNote")
                            nsud.set(false, forKey: "isSyncFavouriteNote")
                            nsud.synchronize()
                            
                            //ENd
                        }
                        else
                        {
                            
                        }
                    }
                    
                }
                
                //For Codes
                
                if dictResponse["WoWdoFindingFavoriteDetailExtSerDcs"] is NSArray {
                    
                    let arrDataCodes = dictResponse["WoWdoFindingFavoriteDetailExtSerDcs"] as! NSArray
                    
                    if arrDataCodes.isKind(of: NSArray.self)
                    {
                        if arrDataCodes.count > 0
                        {
                            //arrOfFavouriteCodes = arrData
                            var arrOfFavouriteCodes = NSArray()
                            //Temp
                            
                            let arrTemp = NSMutableArray()
                            for item in arrDataCodes
                            {
                                let dict = item as! NSDictionary
                                arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                            }
                            arrOfFavouriteCodes = arrTemp
                            nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                            nsud.set(false, forKey: "isSyncFavourite")
                            nsud.synchronize()
                            
                            //ENd
                        }
                        else
                        {
                            
                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    // MARK: ---------------------------Footer Functions ---------------------------
    
    func GotoDashboardViewController() {
        
        self.view.endEditing(true)
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func GotoAccountViewController() {
        
        self.view.endEditing(true)
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func GotoSalesViewController() {
        
        self.view.endEditing(true)
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func setFooterMenuOption() {
        
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("Schedule", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))
        
        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
            self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
            //self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
            self.GotoAccountViewController()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            print("call home")
            self.GotoSalesViewController()
        }
        
    }
    
    func setTopMenuOption() {
        
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        
        nsud.setValue("List", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
        headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        
        self.headerView.addSubview(headerViewtop)
        
        headerViewtop.onClickFilterButton = {() -> Void in
            self.onClickFilter()
        }
        headerViewtop.onClickAddButtonWithSender = {(_ sender: UIButton) -> Void in
            self.onClickAdd(sender)
        }
        
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.arrayAppointment.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "List":
                break
            case "Calendar":
                self.goToCalender()
                break
            default:
                break
            }
            
        }
        
    }
    
    // MARK: ---------------------------Functions ---------------------------
    
    @objc func indexChangedAppointment(_ sender: UISegmentedControl) {
        
        ReloadViewOnScrollAccordingConditionAppointment(tag: sender.selectedSegmentIndex)
        
    }
    
    
    func goToCalender() {
        self.view.endEditing(true)
        let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Appointment_iPAD" : "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        objByProductVC.aryData = self.arrayAppointmentDataMain.mutableCopy()as! NSMutableArray
        objByProductVC.handelDataRefreshOnAddingBlockTime = self
        objByProductVC.strTodayAll = strTodayAll
        self.navigationController?.pushViewController(objByProductVC, animated: false)
    }
    func ReloadViewOnScrollAccordingConditionAppointment(tag:Int)
    {
        
        self.view.endEditing(true)
        searchBarAppointment.text = ""
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
        })
        if(tag == 0){ // Today
            
            self.filterOnClickHeader(strType: "Today")
            strTodayAll = "Today"
            
        }else if(tag == 1){ // All
            
            self.filterOnClickHeader(strType: "All")
            strTodayAll = "All"

        }
    }
    
    func filterOnClickHeader(strType : String) {
        
        let dict = NSMutableDictionary()
        dict.setValue("", forKey: "FName")
        dict.setValue("", forKey: "LName")
        dict.setValue("", forKey: "ACNumber")
        dict.setValue("", forKey: "WONumber")
        dict.setValue("", forKey: "OppNumber")
        dict.setValue(false, forKey: "FromFilterAppointment")

        
        if strType == "Today" {
            
            dict.setValue("InComplete", forKey: "Status")
            dict.setValue("\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")", forKey: "Todatestr")
            dict.setValue("\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")", forKey: "FromeDatestr")
            
            lblTodayDate.text = "Today, " + "\(Global().strGetCurrentDate(inFormat: "d MMM yyyy") ?? "")"
            lblStatus.text = "InComplete"
            
        }else{
            
            let dictDataDates = SalesCoreDataVC().dictdates() as NSDictionary
            
            let minDate = dictDataDates.value(forKey: "fromDateMinimum") as! Date
            let maxDate = dictDataDates.value(forKey: "toDateMaximum") as! Date
            
            let modifiedDateMin = Calendar.current.date(byAdding: .day, value: 1, to: minDate)!
            let modifiedDateMax = Calendar.current.date(byAdding: .day, value: 1, to: maxDate)!
            
            let min = Global().getOnlyDateNeww(modifiedDateMin)
            let max = Global().getOnlyDateNeww(modifiedDateMax)
            
            dict.setValue("All", forKey: "Status")
            let dateeee = "\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")"
            dict.setValue("\(max ?? dateeee)", forKey: "Todatestr")
            dict.setValue("\(min ?? dateeee)", forKey: "FromeDatestr")
            
            lblTodayDate.text = "\(dict.value(forKey: "FromeDatestr") ?? "") - " + "\(dict.value(forKey: "Todatestr") ?? "")"
            lblStatus.text = "All"
            
        }
        
        self.FilterData(dict: dict)
        
        
    }
    
    func onClickFilter() {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Appointment_iPAD" : "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "AppoinmentFilterVC") as? AppoinmentFilterVC
        vc!.dictFilter = self.dictFilter
        vc!.delegate = self
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    func onClickAdd(_ sender: Any)  {
        
        self.view.endEditing(true)
        
        let global = Global()
        let isnet = global.isNetReachable()
        if isnet {
            
            let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as? UIView
                popoverController.sourceRect = (sender as AnyObject).bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            }
            
            let AddBlockTime = (UIAlertAction(title: "Add Block Time", style: .default , handler:{ (UIAlertAction)in
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "AddBlockTime" : "AddBlockTime", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddBlockTimeVC") as? AddBlockTimeVC
                vc?.handelDataOnAddingBlockTime = self
                self.navigationController?.pushViewController(vc!, animated: false)
            }))
            
            AddBlockTime.setValue(#imageLiteral(resourceName: "addBlockIcon"), forKey: "image")
            AddBlockTime.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddBlockTime)
            
            let AddTask = (UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
                
                print("User click Add Task button")
                let defs = UserDefaults.standard
                defs.setValue(nil, forKey: "TaskFilterSort")
                defs.synchronize()
                
                let objWebService = WebService()
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
                
                objByProductVC.dictTaskDetailsData = objWebService.setDefaultTaskDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
            }))
            
            AddTask.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
            AddTask.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddTask)
            
            
            
            let AddActivity = (UIAlertAction(title: "Add Activity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Activity button")
                
                let objWebService = WebService()
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddActivity_iPhoneVC") as! AddActivity_iPhoneVC
                
                
                
                objByProductVC.dictActivityDetailsData = objWebService.setDefaultActivityDataFromDashBoard()
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddActivity.setValue(#imageLiteral(resourceName: "Add Activity"), forKey: "image")
            AddActivity.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddActivity)
            
            let AddLead = (UIAlertAction(title: "Add Lead/Opportunity", style: .default, handler:{ (UIAlertAction)in
                
                print("User click Add Add Lead button")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddLeadProspectVC") as! AddLeadProspectVC
                self.navigationController?.pushViewController(vc, animated: false)
            }))
            AddLead.setValue(#imageLiteral(resourceName: "Lead"), forKey: "image")
            AddLead.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddLead)
            
            
            let AddContact = (UIAlertAction(title: "Add Contact", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Contact  button")
                
                let objByProductVC = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                
                
                
                self.navigationController?.pushViewController(objByProductVC, animated: false)
                
            }))
            
            AddContact.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
            AddContact.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddContact)
            
            
            let AddCompany = (UIAlertAction(title: "Add Company", style: .default, handler:{ (UIAlertAction)in
                print("User click Add Company  button")
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                
                
                self.navigationController?.pushViewController(controller, animated: false)
            }))
            AddCompany.setValue(#imageLiteral(resourceName: "CompanyAdd"), forKey: "image")
            AddCompany.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            alert.addAction(AddCompany)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Add Cancel  button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        } else {
            global.alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    func getEmployeeBlockTime()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strCoreServiceModuleUrlMain + URLGetEmployeeBlockTime_SP + strCompanyKey + URLGetEmployeeBlockTimeEmployeeNo + strEmpNo
        
        print("Employee Block Time API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Employee Block Time API Data From SQL Server.")
                
                print("Employee Block Time API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        // Save To DB
                        
                        // delete before saving dataa
                        
                        deleteAllRecordsFromDB(strEntity: Entity_EmployeeBlockTimeAppointmentVC, predicate: NSPredicate(format: "companyKey == %@ && userName == %@ && strEmpId == %@", Global().getCompanyKey(),Global().getUserName(),Global().getEmployeeId()))
                        
                        WebService.savingEmployeeBlockTimeToDB(arrEmployeeBlockTime: dictData.value(forKey: "data") as! NSArray)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func updateOnMyWayStatusLifeStyle() {
        
        //  Saavan Patidar pulled code
        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyId",
                "WorkOrderId",
                "WorkOrderNo",
                "Status",
                "EmployeeId",
                "OnMyWayLatitude",
                "OnMyWayLongitude"]
        
        values = [strCoreCompanyId,
                  "0",
                  strWorkOrderNoLifeStyle,
                  "true",
                  strEmployeeId,
                  "",
                  "",]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let headerss: HTTPHeaders = ["Content-Type":"application/json",
                                     "BranchId":Global().strEmpBranchID(),
                                     "BranchSysName":Global().strEmpBranchSysName(),
                                     "IsCorporateUser":Global().strIsCorporateUser(),
                                     "ClientTimeZone":Global().strClientTimeZone(),
                                     "Accept":"application/json",
                                     "VisitorIP":Global().getIPAddress(),
                                     "Browser":"IOS",
                                     "BranchSysName":Global().strEmpBranchSysName(),
                                     "BranchSysName":Global().strEmpBranchSysName()]
        
        let strUrl = strServiceAutoUrlMain + URLUpdateOnMyWayLifeStyle
        
        AF.request(strUrl, method: .post, parameters: dictToSend as? Parameters, encoding: URLEncoding.default, headers: headerss).responseJSON { response in
            
            switch response.result {
            case .success:
                
                if let value = response.value {
                    
                    print(value)
                    
                }
            case .failure(let error):
                
                DispatchQueue.main.async() {
                    
                    print(error)
                    
                }
                
            }
        }
        
    }
    
    fileprivate func callAPIToGetTaskList()
    {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let dictData = Global().dateStartEndSales()!
        
        let fromDateMinimum = "\(dictData.value(forKey: "fromDateMinimum") ?? "")"
        let toDateMaximum = "\(dictData.value(forKey: "toDateMaximum") ?? "")"
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        var dicJson = [String : Any]()
        let aryPriority = NSArray()
        let aryTaskTypeId = NSArray()
        
        
        dicJson = ["RefId":strEmployeeId,
                   "RefType":"Employee",
                   "IsDefault":"true",
                   "SkipDefaultDate":"false",
                   "Status":"",
                   "FromDate":fromDateMinimum,
                   "ToDate":toDateMaximum,
                   "EmployeeId":"",
                   "TakeRecords":"",
                   "TaskTypeId":aryTaskTypeId,
                   "PriorityIds":aryPriority]
        
        
        let strURL = strServiceAddressImageUrl + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3"
        
        WebService.postRequestWithHeaders(dictJson: dicJson as NSDictionary, url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            if(status == true)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Task List API Data From SQL Server.")
                
                print("Task List API Response")
                
                if let dictData = response.value(forKey: "data"){
                    if((dictData as! NSDictionary).count > 0){
                        
                        if ((dictData as! NSDictionary).value(forKey: "Tasks")) is NSArray
                        {
                            if(((dictData as! NSDictionary).value(forKey: "Tasks") as! NSArray).count > 0){
                                
                                let dicDataTemp = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                                
                                let dictDataTaskTemp = dicDataTemp.value(forKey: "data") as! NSDictionary
                                
                                deleteAllRecordsFromDB(strEntity: Entity_CrmTaskAppointmentVC, predicate: NSPredicate(format: "companyKey == %@ && userName == %@ && strEmpId == %@", Global().getCompanyKey(),Global().getUserName(),Global().getEmployeeId()))
                                
                                WebService.savingCrmTaskToDB(arrTask: dictDataTaskTemp.value(forKey: "Tasks") as! NSArray)
                                
                            }
                            
                        }

                    }
                }
            }
        }
    }
    
    fileprivate func callAPIToUpdateTaskMarkAsDone(dictData:NSManagedObject, index:Int)
    {
        var taskStatus = ""
        
        if("\(dictData.value(forKey: "status")!)" == "Open")
        {
            taskStatus = "Done"
        }
        else
        {
            taskStatus = "Open"
        }
        let strURL = strServiceAddressImageUrl + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "leadTaskId")!)&Status=\(taskStatus)"
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    if("\(response.value(forKey: "data")!)" == "true")
                    {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("status")
                        arrOfValues.add(taskStatus)
                        
                        let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_CrmTaskAppointmentVC, predicate: NSPredicate(format: "companyKey=%@ && userName=%@ && leadTaskId = %@", Global().getCompanyKey() , self.strUserName , "\(dictData.value(forKey: "leadTaskId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            // Saved in DB
                            
                        }
                        
                        if("\(dictData.value(forKey: "status")!)" == "Done")
                        {
                            
                            //UserDefaults.standard.set(true, forKey: "isTaskAddedUpdated")
                            
                            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                            
                            controller.dictLeadTaskData = NSDictionary()
                            controller.leadTaskId = "\((dictData.value(forKey: "leadTaskId")!))"
                            controller.dictOfAssociations = NSMutableDictionary()
                            self.navigationController?.pushViewController(controller, animated: false)
                            
                        }
                        
                        self.mergeAllData()
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
        }
    }
    fileprivate func callAPIToResetOpportunityStatus(dictData:NSManagedObject, index:Int)
    {

        let strURL = strSalesAutoUrlMain + "/api/mobiletosaleauto/ReopenOpportunity?CompanyKey=\(strCompanyKey)&leadnumber=\(dictData.value(forKey: "leadNumber") ?? "")"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            self.loader.dismiss(animated: false)
            {
                if(status == true)
                {
                    if("\(response.value(forKey: "data")!)" == "true")
                    {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("statusSysName")
                        arrOfKeys.add("stageSysName")
                        arrOfKeys.add("isCustomerNotPresent")
                        arrOfKeys.add("isReopen")
                        arrOfKeys.add("isResendAgreementProposalMail")

                        

                        arrOfValues.add("Complete")
                        arrOfValues.add("CompletePending")
                        arrOfValues.add("true")
                        arrOfValues.add("true")
                        arrOfValues.add("false")



                        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "companyKey=%@ && userName=%@ && leadId = %@", Global().getCompanyKey() , self.strUserName , "\(dictData.value(forKey: "leadId")!)"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            
                        }
                        
                        
                        let arrOfKeysPayment = NSMutableArray()
                        let arrOfValuesPayment = NSMutableArray()
                        
                        arrOfKeysPayment.add("customerSignature")
                        arrOfValuesPayment.add("")
                        
                        let isSuccessPayment =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "companyKey=%@ && userName=%@ && leadId = %@", Global().getCompanyKey() , self.strUserName , "\(dictData.value(forKey: "leadId")!)"), arrayOfKey: arrOfKeysPayment, arrayOfValue: arrOfValuesPayment)
                        
                        if isSuccessPayment {
                            
                            
                        }
                        
                        self.tblViewAppointment.reloadData()
                        
                        
                        if isSuccessPayment {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Status updated successfully", viewcontrol: self)

                        }
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
        }
    }
    //Mark:
    //Mark:------------------------Filter data------------------------
    func FilterData(dict: NSDictionary) {
        let aryWorkOrderDetailsService = NSMutableArray()
        let arySalesService = NSMutableArray()
        let aryTaskss = NSMutableArray()
        let aryBlockTime = NSMutableArray()
        
        for (index, _) in self.arrayAppointmentDataMain.enumerated() {
            let dictDataSalesDB = self.arrayAppointmentDataMain[index] as! NSManagedObject
            
            if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
                aryBlockTime.add(dictDataSalesDB)
                
            }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
                aryTaskss.add(dictDataSalesDB)
                
            }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                aryWorkOrderDetailsService.add(dictDataSalesDB)
            }else{
                arySalesService.add(dictDataSalesDB)
            }
        }
        let strFname = "\(dict.value(forKey: "FName")!)"
        let strLName = "\(dict.value(forKey: "LName")!)"
        let strACNumber = "\(dict.value(forKey: "ACNumber")!)"
        let strWONumber = "\(dict.value(forKey: "WONumber")!)"
        let strOppNumber = "\(dict.value(forKey: "OppNumber")!)"
        let strStatus = "\(dict.value(forKey: "Status")!)"
        let strTodate = "\(dict.value(forKey: "Todatestr")!)"
        let strFromeDate = "\(dict.value(forKey: "FromeDatestr")!)"
        let isFromFilter = dict.value(forKey: "FromFilterAppointment") as! Bool
        var firstDate = Date() ,SecondDate = Date()
        
        if(strTodate != "" && strFromeDate != ""){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let Date1 = formatter.date(from: strFromeDate)
            formatter.dateFormat = "MM/dd/yyyy"
            firstDate = formatter.date(from: "\(formatter.string(from: Date1!))")!
            
            
            formatter.dateFormat = "MM/dd/yyyy"
            let Date2 = formatter.date(from: strTodate)
            formatter.dateFormat = "MM/dd/yyyy"
            SecondDate = formatter.date(from: "\(formatter.string(from: Date2!))")!
            
        }
        
        
        
        var aryWOTemp = aryWorkOrderDetailsService.filter { (activity) -> Bool in
            
            var statusWoLocal = strStatus
            
            if statusWoLocal == "Complete" {
                
                statusWoLocal = "Completed"
                
            }
            
            if strStatus == "All" {
                
                return "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(strFname.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(strLName.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(strACNumber.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "workOrderNo")!)".lowercased().contains(strWONumber.lowercased())
                
                
            }else{
                
                return "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(strFname.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(strLName.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(strACNumber.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "workOrderNo")!)".lowercased().contains(strWONumber.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "workorderStatus")!)".lowercased().contains(statusWoLocal.lowercased())
                
                
            }
            
        }
        
        var arySales = arySalesService.filter { (activity) -> Bool in
            
            var statusLeadLocal = strStatus
            
            if statusLeadLocal == "InComplete" {
                
                statusLeadLocal = "Open"
                
            }
            
            if strStatus == "All" {
                
                return "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(strFname.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(strLName.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(strACNumber.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "leadNumber")!)".lowercased().contains(strOppNumber.lowercased())
                
            }else{
                
                return "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(strFname.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(strLName.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(strACNumber.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "leadNumber")!)".lowercased().contains(strOppNumber.lowercased())  || "\((activity as! NSManagedObject).value(forKey: "statusSysName")!)".lowercased().contains(statusLeadLocal.lowercased())
                
            }
            
        }
        
        var aryTaskTemp = aryTaskss.filter { (activity) -> Bool in
            
            var statusTaskLocal = strStatus
            
            if statusTaskLocal == "Complete" {
                
                statusTaskLocal = "Done"
                
            }
            if statusTaskLocal == "InComplete" {
                
                statusTaskLocal = "Open"
                
            }
            
            if strStatus == "All" {
                
                return "\((activity as! NSManagedObject).value(forKey: "taskName")!)".lowercased().contains(strFname.lowercased())
                
            }else{
                
                return "\((activity as! NSManagedObject).value(forKey: "taskName")!)".lowercased().contains(strFname.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "status")!)".lowercased().contains(statusTaskLocal.lowercased())
                
            }
            
        }
        
        if(strFname == "" && strLName == "" && strACNumber == ""  && strWONumber == ""  && strOppNumber == ""  && (strStatus == "" || strStatus == "All")){
            
            aryWOTemp = (aryWorkOrderDetailsService as NSArray) as! [NSArray.Element]
            arySales  = (arySalesService as NSArray) as! [NSArray.Element]
            aryTaskTemp  = (aryTaskss as NSArray) as! [NSArray.Element]
            
        }
        
        let aryEmpBlockTimeTemp  = (aryBlockTime as NSArray) as! [NSArray.Element]
        
        let aryWorkOrderDetailsFilterDate = NSMutableArray()
        let arySalesServiceFilterDate = NSMutableArray()
        let aryTaskFilterDate = NSMutableArray()
        let aryEmpBlockTimeFilterDate = NSMutableArray()
        
        let arrayTemp = ((aryWOTemp as NSArray) + (arySales as NSArray)) + ((aryTaskTemp as NSArray) + (aryEmpBlockTimeTemp as NSArray))
        
        for (index, _) in arrayTemp.enumerated() {
            let dictDataSalesDB = arrayTemp[index] as! NSManagedObject
            
            if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
                if(strTodate != "" && strFromeDate != ""){
                    
                    let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "fromDate")!)")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                    let Date1 = formatter.date(from: scheduleStartDate!)
                    formatter.dateFormat = "MM/dd/yyyy"
                    let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                    if((mainDate! >= firstDate) && (mainDate! <= SecondDate)){
                        aryEmpBlockTimeFilterDate.add(dictDataSalesDB)
                    }
                }else{
                    aryEmpBlockTimeFilterDate.add(dictDataSalesDB)
                }
            }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
                if(strTodate != "" && strFromeDate != ""){
                    
                    let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "dueDate")!)")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                    let Date1 = formatter.date(from: scheduleStartDate!)
                    formatter.dateFormat = "MM/dd/yyyy"
                    let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                    if((mainDate! >= firstDate) && (mainDate! <= SecondDate)){
                        aryTaskFilterDate.add(dictDataSalesDB)
                    }
                }else{
                    aryTaskFilterDate.add(dictDataSalesDB)
                }
            }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                if(strTodate != "" && strFromeDate != ""){
                
                    if !isFromFilter
                    {
                        //Nilind If not from filter
                        
                        if self.segment.selectedSegmentIndex == 0 //Today
                        {
                            
                            let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime")!)")
                            let formatter = DateFormatter()
                            formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                            let Date1 = formatter.date(from: scheduleStartDate!)
                            formatter.dateFormat = "MM/dd/yyyy"
                            let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                            
                            if((mainDate! >= firstDate) && (mainDate! <= SecondDate)){
                                aryWorkOrderDetailsFilterDate.add(dictDataSalesDB)
                            }
                        }
                        else
                        {
                            let formatterNew = DateFormatter()
                            formatterNew.dateFormat = "MM/dd/yyyy"
                            let strFromDateService = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.FromDate") ?? "")"
                            let strToDateService = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ToDate") ?? "")"
                            var firstDateService = Date() ,SecondDateService = Date()
                            firstDateService = formatterNew.date(from: changeStringDateToGivenFormat(strDate: strFromDateService, strRequiredFormat: "MM/dd/yyyy")) ?? Date()
                            SecondDateService = formatterNew.date(from: changeStringDateToGivenFormat(strDate: strToDateService, strRequiredFormat: "MM/dd/yyyy")) ?? Date()

                            let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime")!)")
                            let formatter = DateFormatter()
                            formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                            let Date1 = formatter.date(from: scheduleStartDate!)
                            formatter.dateFormat = "MM/dd/yyyy"
                            let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                            
                            if((mainDate! >= firstDateService) && (mainDate! <= SecondDateService)){
                                aryWorkOrderDetailsFilterDate.add(dictDataSalesDB)
                            }
                        }
                    
                       
                    }
                    else //Today
                    {
                        
                        let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime")!)")
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                        let Date1 = formatter.date(from: scheduleStartDate!)
                        formatter.dateFormat = "MM/dd/yyyy"
                        let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                        
                        if((mainDate! >= firstDate) && (mainDate! <= SecondDate)){
                            aryWorkOrderDetailsFilterDate.add(dictDataSalesDB)
                        }
                    }
      
                }
                else
                {
                    aryWorkOrderDetailsFilterDate.add(dictDataSalesDB)
                }
            }else{
                if(strTodate != "" && strFromeDate != ""){
                    
                    let scheduleStartDate  = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDate")!)")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy hh:mm a"
                    let Date1 = formatter.date(from: scheduleStartDate!)
                    formatter.dateFormat = "MM/dd/yyyy"
                    let mainDate = formatter.date(from: "\(formatter.string(from: Date1!))")
                    if((mainDate! >= firstDate) && (mainDate! <= SecondDate)){
                        arySalesServiceFilterDate.add(dictDataSalesDB)
                    }
                    
                    
                }else{
                    arySalesServiceFilterDate.add(dictDataSalesDB)
                }
            }
        }
        
        
        self.arrayAppointment = NSArray()
        self.arrayAppointment = aryWorkOrderDetailsFilterDate as NSArray + arySalesServiceFilterDate as NSArray + aryTaskFilterDate as NSArray + aryEmpBlockTimeFilterDate as NSArray//aryFilter
        self.arrayAppointmentData = NSArray()
        self.arrayAppointmentData =  aryWorkOrderDetailsFilterDate as NSArray + arySalesServiceFilterDate as NSArray + aryTaskFilterDate as NSArray + aryEmpBlockTimeFilterDate as NSArray//aryFilter
        self.tblViewAppointment.reloadData()
        
        //Ruchika 21 Oct
        
        getArrOfWorkorderIdForIsRegularPestFlow()
        
    }
    
    func reDirectOnSettingsView() {
        
        let alertController = UIAlertController(title: alertInfo, message: alertNoCustomMessageAvailable, preferredStyle: .alert)
        // Create the actions
        
        let okAction = UIAlertAction(title: "Go to settings", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            UserDefaults.standard.set(true, forKey: "AppointmentFlowNew")
            
            if DeviceType.IS_IPAD {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SettingsViewiPad") as? SettingsViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }else{
                
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SettingsView") as? SettingsView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
            
            
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func manageUserDefaultsForWDO()  {
        
        nsud.setValue(NSArray(), forKey: "sendServiceLeadToServer")
        nsud.setValue(NSArray(), forKey: "saveServiceLeadToLocalDbnDelete")
        nsud.setValue(NSArray(), forKey: "saveTolocalDbAnyHowService")
        nsud.setValue(NSArray(), forKey: "DeleteExtraWorkOrderFromDBService")
        nsud.setValue(NSArray(), forKey: "arrOfWorkOrdersToFetchFromServer")
        nsud.synchronize()
    }
    
    // MARK: --------------------------- Sales Auto Functions ---------------------------
    
    func getSalesAutoAppointmentsV1(strLeadId : String)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strSalesAutoUrlMain + UrlSalesInfoGeneral_SPV1 + strCompanyKey + "&EmployeeNo=" + strEmpNo + "&LeadIds=" + strLeadId
        
        print("Sales Auto Get Leads V1 API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesGetLead_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Auto V1 Appointments API Data From SQL Server.")
                
                print("Sales Auto V1 Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        let dictSalesLeadResponseLocal = (dictData.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        
                        
                        //For UDF Save
                        if(dictSalesLeadResponseLocal.value(forKey: "LeadExtSerDcs") is NSArray){
                            let temp = response.value(forKey: "data") as! NSDictionary
                            
                       //     let LeadExtSerDcs = (temp.value(forKey: "LeadExtSerDcs") as! NSArray).object(at: 0)as! NSDictionary
                            let LeadExtSerDcs = (temp.value(forKey: "LeadExtSerDcs") as! NSArray)
                         
                            for i in 0..<LeadExtSerDcs.count{
                                let temp = response.value(forKey: "data") as! NSDictionary
                                let LeadExtSerDcs = (temp.value(forKey: "LeadExtSerDcs") as! NSArray).object(at: i)as! NSDictionary
                                let UDFSavedData = LeadExtSerDcs.value(forKey: "UDFSavedData")as! NSArray
                                
                                //Replcae UDFSavedData
                                let dictLeadExtSerDcs = ((dictSalesLeadResponseLocal.value(forKey: "LeadExtSerDcs") as! NSArray).object(at: i)as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                dictLeadExtSerDcs.setValue(UDFSavedData, forKey: "UDFSavedData")
                                
                                let arylist = ((dictSalesLeadResponseLocal.value(forKey: "LeadExtSerDcs") as! NSArray).mutableCopy() as! NSMutableArray)
                                arylist.replaceObject(at: i, with: dictLeadExtSerDcs)
                                
                                dictSalesLeadResponseLocal.setValue(arylist, forKey: "LeadExtSerDcs")
                            }
                           
                            
                        }
                        //---End
                        
                        
                        
                        if dictSalesLeadResponseLocal.value(forKey: "CompanyDetail") is NSDictionary {
                            
                            if dictSalesLeadResponseLocal.count > 0 {
                                
                                let companyDetail = dictSalesLeadResponseLocal.value(forKey: "CompanyDetail") as! NSDictionary
                                
                                nsud.set(companyDetail, forKey: "companyDetail")
                                nsud.synchronize()
                                
                            }
                            
                            self.saveLeadsDB(dictOfSalesLeadData: dictSalesLeadResponseLocal)
                            
                        }
                    }
                }
            }
        }
    }
    
    func getSalesAutoAppointmentsWithBasicInfo()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strSalesAutoUrlMain + UrlSalesInfoGeneral_BasicInfo + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Sales Auto Get Leads Basic Info API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesGetLeadBasicInfo_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Auto Basic Info Appointments API Data From SQL Server.")
                
                print("Sales Auto Get Basic Info Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                                                
                        let arrayLeadResponseLocal = response.value(forKey: "data") as! NSArray
                        
                        if arrayLeadResponseLocal.count > 0 {
                            
                            self.fetchSalesLeadFromDBToCheck(arrayOfLeads: arrayLeadResponseLocal)
                            
                        }
                    }
                }
            }else{
                
                // delete extra WO if exists from Db
                
                let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesAutoModifyDate, predicate: NSPredicate(format: "userName == %@", self.strUserName))

                // Check which to send and which to update directly
                
                let arrTemp = NSArray()
                
                self.arrayOfLeadsToFetchDynamicFormData = SalesCoreDataVC().checkIfToSendSalesLead(toServerNew: arrTemp as! [Any], arrayOfLeadsLocal as! [Any])
                
                // Delete Leads Data Before Saving
                
                self.deletSaleLeadFromDB()
                
            }
        }
    }
    
    func getSalesAutoAppointments()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strSalesAutoUrlMain + UrlSalesInfoGeneral_SP + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Sales Auto Get Leads API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesGetLead_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Auto Appointments API Data From SQL Server.")
                
                print("Sales Auto Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictSalesLeadResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        if dictSalesLeadResponseLocal.value(forKey: "CompanyDetail") is NSDictionary {
                            
                            if dictSalesLeadResponseLocal.count > 0 {
                                
                                let companyDetail = dictSalesLeadResponseLocal.value(forKey: "CompanyDetail") as! NSDictionary
                                
                                nsud.set(companyDetail, forKey: "companyDetail")
                                nsud.synchronize()
                                
                            }
                            
                            self.fetchSalesLeadFromDB(dictOfSalesLeadData: dictSalesLeadResponseLocal)
                        }
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    func saveLeadsDB(dictOfSalesLeadData : NSDictionary) {
        
        // No data to compare direct save to DB and fetch Dynamic Form Data
                
        let arrayOfLeadsIdToFetchDynamicForm = NSMutableArray()
        
        if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
            
            let arrLeadComingFromServer = dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") as! NSArray
            
            if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
                
                for k in 0 ..< arrLeadComingFromServer.count {
                    
                    if arrLeadComingFromServer[k] is NSDictionary {
                        
                        let dictSalesLeadLocal = arrLeadComingFromServer[k] as! NSDictionary
                        
                        if dictSalesLeadLocal.count > 0 {
                            
                            if dictSalesLeadLocal.value(forKey: "LeadDetail") is NSDictionary {
                                
                                let leadDetailLocal = dictSalesLeadLocal.value(forKey: "LeadDetail") as! NSDictionary
                                
                                arrayOfLeadsIdToFetchDynamicForm.add("\(leadDetailLocal.value(forKey: "LeadId") ?? "")")
                                
                                deleteSalesLeadById(leadIdLocal: "\(leadDetailLocal.value(forKey: "LeadId") ?? "")" as NSString)
                                
                            }
                        }
                    }
                }
            }
        }
        
        saveSaleLeadToDB(dictOfSalesLeadData: dictOfSalesLeadData)
        
        if arrayOfLeadsIdToFetchDynamicForm.count > 0 {
            
            self.getSalesAutoDynamicForm(arrayOfLeadsIdToFetchDynamicForm: arrayOfLeadsIdToFetchDynamicForm)
            
        }
        
    }
    
    func fetchSalesLeadFromDBToCheck(arrayOfLeads : NSArray) {
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesAutoModifyDate, predicate: NSPredicate(format: "userName == %@", strUserName))

        // Check which to send and which to update directly
        
        arrayOfLeadsToFetchDynamicFormData = SalesCoreDataVC().checkIfToSendSalesLead(toServerNew: arrayOfLeads as! [Any], arrayOfLeadsLocal as! [Any])
        
        
        // Delete Leads Data Before Saving
        
        deletSaleLeadFromDB()
        
        let arrOfLeadsToFetchFromServer = nsud.value(forKey: "arrOfLeadsToFetchFromServer") as! NSArray
        
        let strLeadsToFetchFromServer = arrOfLeadsToFetchFromServer.componentsJoined(by: ",")
                
        if arrOfLeadsToFetchFromServer.count > 0 {
            
            self.getSalesAutoAppointmentsV1(strLeadId: strLeadsToFetchFromServer)
            
        }
        
        // send to server if
        
        let arrofLeadIdsToSendToServer = nsud.value(forKey: "sendSalesLeadToServer") as! NSArray
        
        if arrofLeadIdsToSendToServer.count > 0 {
            
            indexDynamicFormToSend = 0
            // Sending lead and DYnamic form to server
            self.sendSalesDataToServer(indexx: 0)
            
        }
        
        // Download Dynamic Form From Server
        
        if arrOfLeadsToFetchFromServer.count > 0 {
            
            self.getSalesAutoDynamicForm(arrayOfLeadsIdToFetchDynamicForm: arrOfLeadsToFetchFromServer)
            
        }
        
    }
    
    func fetchSalesLeadFromDB(dictOfSalesLeadData : NSDictionary) {
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesAutoModifyDate, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        if arrayOfLeadsLocal.count == 0 {
            
            // No data to compare direct save to DB and fetch Dynamic Form Data
            
            saveSaleLeadToDB(dictOfSalesLeadData: dictOfSalesLeadData)
            
            let arrayOfLeadsIdToFetchDynamicForm = NSMutableArray()
            
            if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
                
                let arrLeadComingFromServer = dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") as! NSArray
                
                if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
                    
                    for k in 0 ..< arrLeadComingFromServer.count {
                        
                        if arrLeadComingFromServer[k] is NSDictionary {
                            
                            let dictSalesLeadLocal = arrLeadComingFromServer[k] as! NSDictionary
                            
                            if dictSalesLeadLocal.count > 0 {
                                
                                if dictSalesLeadLocal.value(forKey: "LeadDetail") is NSDictionary {
                                    
                                    let leadDetailLocal = dictSalesLeadLocal.value(forKey: "LeadDetail") as! NSDictionary
                                    
                                    arrayOfLeadsIdToFetchDynamicForm.add("\(leadDetailLocal.value(forKey: "LeadId") ?? "")")
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    if arrayOfLeadsIdToFetchDynamicForm.count > 0 {
                        
                        self.getSalesAutoDynamicForm(arrayOfLeadsIdToFetchDynamicForm: arrayOfLeadsIdToFetchDynamicForm)

                    }
                    
                }
                
            }
            
        } else {
            
            // Check which to send and which to update directly
            
            arrayOfLeadsToFetchDynamicFormData = SalesCoreDataVC().checkIfToSendSalesLead(toServer: dictOfSalesLeadData as! [AnyHashable : Any], arrayOfLeadsLocal as! [Any])
            
            
            // Delete Leads Data Before Saving
            
            deletSaleLeadFromDB()
            
            let arrofLeadIdsLocal = nsud.value(forKey: "saveSalesLeadToLocalDbnDelete") as! NSArray
            let arrofLeadIdsLocal1 = nsud.value(forKey: "saveTolocalDbAnyHow") as! NSArray
            
            let arrofLeadIdsAll = arrofLeadIdsLocal + arrofLeadIdsLocal1 as! NSMutableArray
            
            for k1 in 0 ..< arrofLeadIdsAll.count {
                
                if dictOfSalesLeadData.count > 0 {
                    
                    if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
                        
                        let arrayOfLeadDataLocal = dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") as! NSArray
                        
                        if arrayOfLeadDataLocal.count > 0 {
                            
                            // delete before saving
                            
                            self.deleteSalesLeadById(leadIdLocal: "\(arrofLeadIdsAll[k1])" as NSString)
                            
                            SalesCoreDataVC().save(toCoreDataSalesInfo: arrayOfLeadDataLocal as! [Any], "\(arrofLeadIdsAll[k1])", "Single")
                            
                        }
                    }
                }
            }
            
            // send to server if
            
            let arrofLeadIdsToSendToServer = nsud.value(forKey: "sendSalesLeadToServer") as! NSArray
            
            if arrofLeadIdsToSendToServer.count > 0 {
                
                indexDynamicFormToSend = 0
                // Sending lead and DYnamic form to server
                self.sendSalesDataToServer(indexx: 0)
                
            }
            
            // Download Dynamic Form From Server
            
            if arrofLeadIdsAll.count > 0 {
                
                self.getSalesAutoDynamicForm(arrayOfLeadsIdToFetchDynamicForm: arrofLeadIdsAll)

            }
            
        }
    }
    
    func sendSalesDataToServer(indexx : Int) {
        
        self.dispatchGroupSales = DispatchGroup()
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let arrofLeadIdsToSendToServer = nsud.value(forKey: "sendSalesLeadToServer") as! NSArray
        
        if arrofLeadIdsToSendToServer.count <= indexx {
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            // All Sent To server
            
        }else{
            
            // Sending lead and DYnamic form to server Committed
            
            let arrayOfSalesDynamicInspection = getDataFromCoreDataBaseArray(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId = %@ AND userName = %@","\(arrofLeadIdsToSendToServer[indexx])", strUserName))
            
            if arrayOfSalesDynamicInspection.count == 0 {
                
                // Dynamic form not exist directly send Sales Lead to server
                
                self.sendingSalesLeadToServer(strLeadIdToSend: "\(arrofLeadIdsToSendToServer[indexx])", indexx: indexx)
                
            }else{
                
                // First Send Dynamic Form And then will send Sales lead to server
                
                let objSalesInspectionData = arrayOfSalesDynamicInspection[0] as! NSManagedObject
                
                if objSalesInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                    
                    let arrOfSalesDynamicForm = objSalesInspectionData.value(forKey: "arrFinalInspection") as! NSArray
                    
                    self.sendingSalesDynamicDataToServer(arrData: arrOfSalesDynamicForm, strLeadIdToSend: arrofLeadIdsToSendToServer[indexx] as! String)
                    
                }else{
                    
                    // Dynamic form not exist directly send Sales Lead to server
                    
                    self.sendingSalesLeadToServer(strLeadIdToSend: "\(arrofLeadIdsToSendToServer[indexx])", indexx: indexx)
                    
                }
                
                
                
            }
            
        }
        
    }
    
    // Change Task
    
    func sendingSalesLeadToServer(strLeadIdToSend : String , indexx : Int) {
        //Nilind
        //Time Range Sync
                
        var matchesGeneralInfo = NSManagedObject()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", strUserName, strLeadIdToSend))//LeadDetail
        
        if arryOfData.count > 0
        {
            matchesGeneralInfo = arryOfData.object(at: 0) as! NSManagedObject
            
            matchesGeneralInfo = getLeadDetail(strLeadId: strLeadIdToSend as String, strUserName: strUserName)
            
            if isEditSchedule && "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")".count > 0
            {
                syncTimeRange(dictToSend: getTimeRangeData(strLeadId: strLeadIdToSend, matchesGeneralInfo: matchesGeneralInfo), strLeadIdToSend: strLeadIdToSend)
            }
            
//            if  matchesGeneralInfo.entity.attributesByName.contains(where: { (key: String, value: NSAttributeDescription) in key == "earliestStartTime"}) == true && matchesGeneralInfo.entity.attributesByName.contains(where: { (key: String, value: NSAttributeDescription) in key == "latestStartTime"}) == true && matchesGeneralInfo.entity.attributesByName.contains(where: { (key: String, value: NSAttributeDescription) in key == "scheduleTime"}) == true && matchesGeneralInfo.entity.attributesByName.contains(where: { (key: String, value: NSAttributeDescription) in key == "scheduleTimeType"}) == true
//            {
//
//            }

        }
        
        arrOfAllImagesToSendToServer = NSMutableArray()
        arrOfAllSignatureImagesToSendToServer = NSMutableArray()
        arrCheckImage = NSMutableArray()
        arrAllTargetImages = NSMutableArray()
        
        var strModifyDateToSendToServerAll = ""
        
        let arrayOfLeadsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesAutoModifyDate, predicate: NSPredicate(format: "leadIdd = %@ AND userName = %@",strLeadIdToSend, strUserName))
        
        if arrayOfLeadsLocal.count > 0 {
            
            let objTemp = arrayOfLeadsLocal[0] as! NSManagedObject
            strModifyDateToSendToServerAll = "\(objTemp.value(forKey: "modifyDate") ?? "")"
            
        }
        
        let dictDataTemp = SalesCoreDataVC().fetchSalesData(fromDB: strModifyDateToSendToServerAll, strLeadIdToSend)
        
        arrOfAllImagesToSendToServer = dictDataTemp.value(forKey: "arrOfAllImagesToSendToServer") as! NSMutableArray
        
        for k1 in 0 ..< arrOfAllImagesToSendToServer.count {
            
            let strURL = strSalesAutoUrlMain + UrlSalesImageUpload
            
            self.uploadImageSales(strImageName: "\(arrOfAllImagesToSendToServer[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
        }
        
        arrOfAllSignatureImagesToSendToServer = dictDataTemp.value(forKey: "arrOfAllSignatureImagesToSendToServer") as! NSMutableArray
        
        for k1 in 0 ..< arrOfAllSignatureImagesToSendToServer.count {
            
            let strURL = strSalesAutoUrlMain + UrlSalesSignatureImageUpload
            
            self.uploadImageSales(strImageName: "\(arrOfAllSignatureImagesToSendToServer[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
            
        }
        
        arrCheckImage = dictDataTemp.value(forKey: "arrCheckImage") as! NSMutableArray
        
        for k1 in 0 ..< arrCheckImage.count {
            
            let strURL = strSalesAutoUrlMain + "/api/File/UploadCheckImagesAsync"
            
            self.uploadImageSales(strImageName: "\(arrCheckImage[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
            
        }
        
        arrAllTargetImages = dictDataTemp.value(forKey: "arrAllTargetImages") as! NSMutableArray
        
        for k1 in 0 ..< arrAllTargetImages.count {
            
            let strURL = strSalesAutoUrlMain + UrlUploadTargetImage
            
            self.uploadImageSales(strImageName: "\(arrAllTargetImages[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
            
        }
        
        let strElectricSign = "\(dictDataTemp.value(forKey: "strElectronicSign") ?? "")"
        
        if strElectricSign.count > 0 {
            
            let strURL = strSalesAutoUrlMain + "api/File/UploadSignatureAsync"
            
            self.uploadImageSales(strImageName: strElectricSign, strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
            
        }
        
        let stAudioSales = "\(dictDataTemp.value(forKey: "strAudioNameGlobal") ?? "")"
        
        if stAudioSales.count > 0 {
            
            let strURL = strSalesAutoUrlMain + UrlAudioUploadAsync
            
            let strName = SalesCoreDataVC().strAudioName(stAudioSales)
            
            self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
            
        }
        
        // Upload Documents
        
        let arrayOfLeadsDocuments = getDataFromCoreDataBaseArray(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId=%@ && docType=%@ && isToUpload=%@",strLeadIdToSend, "Other","true"))
        
        if arrayOfLeadsDocuments.count > 0 {
            
            for k1 in 0 ..< arrayOfLeadsDocuments.count {
                
                let objTemp = arrayOfLeadsDocuments[k1] as! NSManagedObject
                var strfileName = "\(objTemp.value(forKey: "fileName") ?? "")"
                
                strfileName = Global().strDocName(fromPath: strfileName)
                
                let strleadDocumentId = "\(objTemp.value(forKey: "leadDocumentId") ?? "")"
                
                let strURL = strSalesAutoUrlMain + "/api/File/UploadOtherDocsAsync"
                
                self.uploadDocumentsSales(strLeadIdToSend: strLeadIdToSend, strDocName: strfileName, strDocId: strleadDocumentId, strUrll: strURL)
                
            }
            
        }
        
        // Upload Graph XML
        
        let arrOfAllGraphXmlToSendToServer = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId=%@ && userName=%@ && isNewGraph=%@",strLeadIdToSend, strUserName,"true"))
        
        for item in arrOfAllGraphXmlToSendToServer {
            
            let matchImageDetail = item as! NSManagedObject
            
            let strXMLPath = "\(matchImageDetail.value(forKey: "leadXmlPath") ?? "")"
            
            if strXMLPath.count > 0
            {
                let strURL = strSalesAutoUrlMain + UrlUploadXMLSales
                let strName = SalesCoreDataVC().strAudioName(strXMLPath)
                //self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
                self.uploadAudioSales(strAudioName: "\\Documents\\GraphXML\\\(strName)", strUrll: strURL)
            }
        }
        
        //CRM Image
        
        let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isImageSyncforMobile == %@", strLeadIdToSend , strUserName,"false"))
        let aryCRMImagesList = NSMutableArray()
        
        for item in arrImageCRM
        {
            let match = item as! NSManagedObject
            aryCRMImagesList.add("\(match.value(forKey: "path") ?? "")")

        }
        
        if aryCRMImagesList.count != 0 {
            
            
            for k1 in 0 ..< aryCRMImagesList.count {
                
                let strURL = "\(URL.Gallery.uploadCRMImagesSingle)"
                
                self.uploadImageSales(strImageName: "\(aryCRMImagesList[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
            }

        }
        
        
        
        let dictFinal = dictDataTemp.value(forKey: "dictFinal") as! NSMutableDictionary
        
        self.dispatchGroupSales.notify(queue: DispatchQueue.main, execute: {
            DispatchQueue.main.async {
                // Reload Table
                
                self.sendingSalesLeadDataToServer(dictToSend: dictFinal, strLeadIdToSend: strLeadIdToSend)
                
            }
        })
        
    }
        
    func syncTimeRange(dictToSend : NSDictionary , strLeadIdToSend : String)  {
        
        self.dispatchGroupSales.enter()
        var jsonString = String()
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonString)
        }
        
        let strUrl = "\(URL.TimeRange.updateTimeRange)"
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "bool"){ (responce, status) in
            if(status)
            {
                debugPrint(responce)
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                }
                else
                {
                }
                //self.dispatchGroupSales.leave()
                let count = self.dispatchGroupSales.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupSales.leave()
                }
            }
            else
            {
                //self.dispatchGroupSales.leave()
                
                let count = self.dispatchGroupSales.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupSales.leave()
                }
            }
        }
    }
    
    func uploadImageSales(strImageName : String , strUrll : String , strType : String , dictDataTemp : NSDictionary , strIddd : String) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            self.dispatchGroupSales.enter()
            
            let imagee = getImagefromDirectory(strname: strImageName)
            
            let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                
                //self.dispatchGroupSales.leave()
                //defer { self.dispatchGroupSales.leave() }
                
                let count = self.dispatchGroupSales.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupSales.leave()
                }
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        if strType == "serviceAddressDocNameSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let mobileServiceAddressDocId = "\(dictDataTemp.value(forKey: "mobileServiceAddressDocId") ?? "")"
                            
                            if mobileServiceAddressDocId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",strIddd,mobileServiceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            } else {
                                
                                let serviceAddressDocId = "\(dictDataTemp.value(forKey: "serviceAddressDocId") ?? "")"
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",strIddd,serviceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else if strType == "documentPathSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let sAConditionDocumentId = "\(dictDataTemp.value(forKey: "sAConditionDocumentId") ?? "")"
                            
                            if sAConditionDocumentId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",strIddd,sAConditionDocumentId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else {
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func uploadAudioSales(strAudioName : String , strUrll : String) {
        
        if fileAvailableAtPath(strname: strAudioName) {
            
            self.dispatchGroupSales.enter()
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strAudioName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strAudioName, url: strUrll) { (responce, status) in
                
                  //self.dispatchGroupSales.leave()
                //defer { self.dispatchGroupSales.leave() }
                let count = self.dispatchGroupSales.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupSales.leave()
                }
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                    }
                }
            }
        }
    }
    
    func uploadDocumentsSales(strLeadIdToSend : String , strDocName : String ,strDocId : String , strUrll : String) {
        
        if fileAvailableAtPath(strname: strDocName) {
            
            self.dispatchGroupSales.enter()
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strDocName)
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strDocName, url: strUrll) { (responce, status) in
                
                self.dispatchGroupSales.leave()
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("isToUpload")
                        arrOfValues.add("false")
                        
                        let isSuccess =  getDataFromDbToUpdate(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId=%@ && docType=%@ && leadDocumentId = %@", strLeadIdToSend , "Other" , strDocId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        if isSuccess {
                            
                            // Saved in DB
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    func sendingSalesDynamicDataToServer(arrData : NSArray , strLeadIdToSend : String) {
        
        self.dispatchGroupSales.enter()
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["InspectionFormsData"]
        
        if arrData.count > 0
        {
            let arrTemp = NSMutableArray()
            for item in arrData
            {
                let obj = item as! NSDictionary
                arrTemp.add(obj)
            }
            values = arrTemp as NSArray
            
            //let dictData = arrData[0] as! NSDictionary
            //values = [arrData] //dictData.value(forKey: "arrFinalInspection") as! NSArray
        }
        else
        {
            values = []
        }
        
        //values = [arrData]
        
        let dictToSend = NSMutableDictionary()//NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        dictToSend.setValue(values, forKey: "InspectionFormsData")
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = strSalesAutoUrlMain + UrlSalesInspectionDynamicFormSubmission
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "string"){ (responce, status) in
            
            //self.sendingSalesLeadToServer(strLeadIdToSend: strLeadIdToSend, indexx: self.indexDynamicFormToSend)
            
            
            self.dispatchGroupSales.leave()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                
                self.sendingSalesLeadToServer(strLeadIdToSend: strLeadIdToSend, indexx: self.indexDynamicFormToSend)
                
            }
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    // Handle Success Response Here
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isSentToServer")
                    arrOfValues.add("Yes")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId = %@ AND userName = %@",strLeadIdToSend, self.strUserName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func sendingSalesLeadDataToServer(dictToSend : NSDictionary , strLeadIdToSend : String) {
        
        //self.dispatchGroup.enter(); print("Dispatch Group Main Entered");
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("Json Sales \(jsonString)")
            
        }
        
        let strUrl = strSalesAutoUrlMain + UrlSalesFinalUpdate
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
            
            // Sales Data synced
            self.indexDynamicFormToSend = self.indexDynamicFormToSend + 1
            
            self.sendSalesDataToServer(indexx: self.indexDynamicFormToSend)
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    let value = responce.value(forKey: "data")
                    var strBillToId = "", strCreditCardType = "", strCardAccountNo = ""
                    
                    debugPrint(responce)
                    
                    // Saving Target In DB Also
                    
                    if value is NSDictionary {
                        
                        let dictTemp = value as! NSDictionary
                        
                        if dictTemp.value(forKey: "leadCommercialTargetExtDcs") is NSArray {
                            
                            let leadCommercialTargetExtDcs = dictTemp.value(forKey: "leadCommercialTargetExtDcs") as! NSArray
                            
                            WebService().savingTargets(arrTarget: leadCommercialTargetExtDcs, strLeadId: strLeadIdToSend)
                            
                        }
                        
                        strBillToId = "\(dictTemp.value(forKey: "billToLocationId") ?? "")"
                        
                       
                        strCreditCardType = "\(dictTemp.value(forKey: "CreditCardType") ?? "")"
                        strCardAccountNo = "\(dictTemp.value(forKey: "CardAccountNumber") ?? "")"
                    }
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("zSync")
                    arrOfValues.add("no")
                    
                    arrOfKeys.add("isMailSend")
                    arrOfValues.add("false")
                    
                    arrOfKeys.add("billToLocationId")
                    arrOfValues.add(strBillToId)
                    
                    if strBillToId.count > 0 {
                        // remove from nsud billtoid info which was set locally
                        nsud.setValue("", forKey: strBillToId)
                    }
                
                    arrOfKeys.add("creditCardType")
                    arrOfValues.add(strCreditCardType)
                    
                    arrOfKeys.add("cardAccountNumber")
                    arrOfValues.add(strCardAccountNo)
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId=%@",strLeadIdToSend), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func deletSaleLeadFromDB() {
        
        //let arrofLeadIdsToDelete = nsud.value(forKey: "saveSalesLeadToLocalDbnDelete") as! NSArray
        
        let arrofLeadIdsToDeleteExtra = nsud.value(forKey: "DeleteExtraWorkOrderFromDBSales") as! NSArray
        
        let arrOldLeadToDelete = SalesCoreDataVC().fetchOldLeadToDeleteFromMobileSalesFlow() as! NSArray
        
        let mixedArrayToDelete = arrOldLeadToDelete + arrofLeadIdsToDeleteExtra
        
        for k1 in 0 ..< mixedArrayToDelete.count {
            
            let leadIdLocal = "\(mixedArrayToDelete[k1])"
            
            //deleteAllRecordsFromDB(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId = %@ AND employeeNo_Mobile = %@", leadIdLocal,strEmpNo))
            deleteAllRecordsFromDB(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadPreference", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SalesAutoModifyDate", predicate: NSPredicate(format: "leadIdd = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "CurrentService", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ElectronicAuthorizedForm", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialInitialInfoExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))

            deleteAllRecordsFromDB(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, predicate: NSPredicate(format: "leadId == %@", leadIdLocal))

        }
        
    }
    
    func deleteSalesLeadById(leadIdLocal : NSString) {
        
        //deleteAllRecordsFromDB(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId = %@ AND employeeNo_Mobile = %@", leadIdLocal,strEmpNo))
        deleteAllRecordsFromDB(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "EmailDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadPreference", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "DocumentsDetail", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SalesAutoModifyDate", predicate: NSPredicate(format: "leadIdd = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "CurrentService", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ElectronicAuthorizedForm", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialInitialInfoExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadContactDetailExtSerDc", predicate: NSPredicate(format: "leadId = %@", leadIdLocal))
                
        deleteAllRecordsFromDB(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))

        deleteAllRecordsFromDB(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "LeadTagExtDcs", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@", leadIdLocal))
    }
    
    func saveSaleLeadToDB(dictOfSalesLeadData : NSDictionary) {
        
        if dictOfSalesLeadData.count > 0 {
            
            if dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") is NSArray {
                
                let arrayOfLeadDataLocal = dictOfSalesLeadData.value(forKey: "LeadExtSerDcs") as! NSArray
                
                if arrayOfLeadDataLocal.count > 0 {
                    
                    // delete before saving
                    
                    SalesCoreDataVC().save(toCoreDataSalesInfo: arrayOfLeadDataLocal as! [Any], "", "All")
                    
                }
                
            }
            
        }
        
    }
    
    func getSalesAutoDynamicForm(arrayOfLeadsIdToFetchDynamicForm : NSArray)  {
                
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")

        
        let strLeadsIdToFetchDynamicForm = arrayOfLeadsIdToFetchDynamicForm.componentsJoined(by: ",")
        
        let strURL = strSalesAutoUrlMain + UrlSalesDynamicFormAllLeads + strCompanyKey + UrlSalesDynamicFormAllLeadsLeadId + strLeadsIdToFetchDynamicForm
        
        print("Sales Auto Dynamic Form Get API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Sales Auto Dynamic Form Data From SQL Server.")
                
                print("Sales Auto Dynamic Form Get API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrayOfSalesDynamicFormLocal = dictData.value(forKey: "data") as! NSArray
                        
                        if arrayOfSalesDynamicFormLocal.count > 0 {
                            
                            self.checkSalesDynamicFormResponseAndSaveInDB(arrayOfSalesDynamicFormLocal: arrayOfSalesDynamicFormLocal)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func checkSalesDynamicFormResponseAndSaveInDB(arrayOfSalesDynamicFormLocal : NSArray) {
        
        for k in 0 ..< arrayOfSalesDynamicFormLocal.count {
            
            if arrayOfSalesDynamicFormLocal[k] is NSDictionary {
                
                let arrayOfDynamicFormLocal = NSMutableArray()
                
                let dictSalesDynamicFormLocal = arrayOfSalesDynamicFormLocal[k] as! NSDictionary
                
                if dictSalesDynamicFormLocal.count > 0 {
                    
                    let leadIdLocal = "\(dictSalesDynamicFormLocal.value(forKey: "LeadId") ?? "")"
                    
                    for k1 in 0 ..< arrayOfSalesDynamicFormLocal.count {
                        
                        let dictSalesDynamicFormLocalNew = arrayOfSalesDynamicFormLocal[k1] as! NSDictionary
                        
                        if dictSalesDynamicFormLocalNew.count > 0 {
                            
                            let leadIdLocalNew = "\(dictSalesDynamicFormLocalNew.value(forKey: "LeadId") ?? "")"
                            
                            if leadIdLocal == leadIdLocalNew {
                                
                                arrayOfDynamicFormLocal.add(dictSalesDynamicFormLocalNew)
                                
                            }
                            
                        }
                        
                    }
                    
                    // Save all combined Dynamic Form Data
                    
                    if arrayOfDynamicFormLocal.count > 0 {
                        
                        self.saveSalesLeadDynamicFormToDB(arrayOfDynamicFormLocal: arrayOfDynamicFormLocal , leadIdLocal: leadIdLocal)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveSalesLeadDynamicFormToDB(arrayOfDynamicFormLocal : NSArray , leadIdLocal : String) {
        
        deleteAllRecordsFromDB(strEntity: Entity_SalesDynamicInspection, predicate: NSPredicate(format: "leadId = %@ AND userName = %@", leadIdLocal , strUserName))
        
        WebService.savingSalesAutoDynamicFormDataToDB(arrayOfLeadsDynamicFormLocal: arrayOfDynamicFormLocal, leadIdLocal: leadIdLocal)
        
        /*let arrayOfLeadsDynamicFormLocal = getDataFromCoreDataBaseArray(strEntity: Entity_SalesDynamicInspection, predicate: NSPredicate(format: "leadId = %@ AND userName = %@", leadIdLocal , strUserName))
         
         if arrayOfLeadsDynamicFormLocal.count > 0 {
         
         // Save in DB
         
         WebService.savingSalesAutoDynamicFormDataToDB(arrayOfLeadsDynamicFormLocal: arrayOfLeadsDynamicFormLocal, leadIdLocal: leadIdLocal)
         
         }else{
         
         // Update in DB
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()
         
         arrOfKeys.add("arrFinalInspection")
         arrOfKeys.add("isSentToServer")
         
         arrOfValues.add(arrayOfDynamicFormLocal)
         arrOfValues.add("Yes")
         
         
         let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_SalesDynamicInspection, predicate: NSPredicate(format: "leadId = %@ AND userName = %@", leadIdLocal , strUserName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         
         if isSuccess {
         
         // Saved in DB
         
         } else {
         
         // Delete and save again
         
         deleteAllRecordsFromDB(strEntity: Entity_SalesDynamicInspection, predicate: NSPredicate(format: "leadId = %@ AND userName = %@", leadIdLocal , strUserName))
         
         //
         WebService.savingSalesAutoDynamicFormDataToDB(arrayOfLeadsDynamicFormLocal: arrayOfLeadsDynamicFormLocal, leadIdLocal: leadIdLocal)
         
         }
         
         }*/
        
    }
    
    func getSalesNo(dictDataSalesDB : NSManagedObject) -> String {
        
        var strNoLocal = "\(dictDataSalesDB.value(forKey: "serviceCellNo")!)"
        
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "servicePrimaryPhone")!)"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "serviceSecondaryPhone")!)"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "cellNo")!)"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "primaryPhone")!)"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "secondaryPhone")!)"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = ""
            
        }
        
        return strNoLocal
        
    }
    
    
    func goToSendMailSales(strIsCustomerPresent : String) {
        
        //Appointment
        
        if DeviceType.IS_IPAD {
            
//            let mainStoryboard = UIStoryboard(
//                name: "SalesMainiPad",
//                bundle: nil)
//            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SendMailViewControlleriPad") as? SendMailViewControlleriPad
//            objByProductVC?.strFromWhere = "Appointment"
//            objByProductVC?.isCustomerPresent = strIsCustomerPresent as NSString
//            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            
            let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
            vc.strFrom = "Appointment"
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            
//            let mainStoryboard = UIStoryboard(
//                name: "SalesMain",
//                bundle: nil)
//            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SendMailViewController") as? SendMailViewController
//            objByProductVC?.strFromWhere = "Appointment"
//            objByProductVC?.isCustomerPresent = strIsCustomerPresent as NSString
//            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
            vc.strFrom = "Appointment"
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

        
    }
    
    func goToSendMailServicAuto(strIsCustomerPresent : String) {
        
        //Appointment
        if DeviceType.IS_IPAD {
            
            let mainStoryboard = UIStoryboard(
                name: "ServiceiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSendMailViewControlleriPad") as? ServiceSendMailViewControlleriPad
            objByProductVC?.fromWhere = "Appointment"
            objByProductVC?.isCustomerPresent = strIsCustomerPresent as NSString
            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            
        } else {
            
            let mainStoryboard = UIStoryboard(
                name: "Service_iPhone",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSendMailViewController") as? ServiceSendMailViewController
            objByProductVC?.fromWhere = "Appointment"
            objByProductVC?.isCustomerPresent = strIsCustomerPresent as NSString
            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            
        }
        
    }
    
    func goToSendMailPlumbing(strIsCustomerPresent : String) {
        
        //Appointment
        
        if DeviceType.IS_IPAD {

            let mainStoryboard = UIStoryboard(
                name: "MechanicaliPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SendMailMechanicaliPadViewController") as? SendMailMechanicaliPadViewController
            objByProductVC?.strFromWhere = "ResendMailFromAppointment"
            objByProductVC?.isCustomerPresent = strIsCustomerPresent as NSString
            self.navigationController?.pushViewController(objByProductVC!, animated: false)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: InfoComingSoon, viewcontrol: self)
            
        }
        
    }
    
    @objc func showInitialSetup(dictSalesDB: NSManagedObject) -> Bool
    {
        var show = Bool()
        
        let strInitialSetupStatus = "\(dictSalesDB.value(forKey: "isInitialSetupCreated") ?? "")"
        let strLeadStatus = "\(dictSalesDB.value(forKey: "statusSysName") ?? "")"
        let strLeadIdSales = "\(dictSalesDB.value(forKey: "leadId") ?? "")"
        let strIsSendProposalStatus = "\(dictSalesDB.value(forKey: "isProposalFromMobile") ?? "")"
        let strStageSysName = "\(dictSalesDB.value(forKey: "stageSysName") ?? "")"
        
        let strIsServiceActive = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.IsActive") ?? "")"
        
        let arrSold = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        
        if !isInternetAvailable()
        {
            show = false
        }
        else if(strIsSendProposalStatus.caseInsensitiveCompare("true") == .orderedSame)
        {
            show = false
        }
        else if(strStageSysName.caseInsensitiveCompare("lost") == .orderedSame)
        {
            show = false
        }
        else if(strIsServiceActive.caseInsensitiveCompare("false") == .orderedSame || strIsServiceActive == "0")
        {
            show = false
        }
        else if(strLeadStatus.caseInsensitiveCompare("open") == .orderedSame)
        {
            show = false
        }
        else if(strIsSendProposalStatus.caseInsensitiveCompare("true") == .orderedSame)
        {
            show = false
        }
        else if(strInitialSetupStatus.caseInsensitiveCompare("false") == .orderedSame && strLeadStatus.caseInsensitiveCompare("complete") == .orderedSame && arrSold.count > 0)
        {
            show = true
        }
        else
        {
            show = false
        }
        
        return show
        
    }
    @objc func showGenerateWorkorderSetup(dictSalesDB: NSManagedObject) -> Bool
    {
        var show = Bool()
        
        let arrOneTime = NSMutableArray()
        let arrNonOneTime = NSMutableArray()
        
        let strLeadIdSales = "\(dictSalesDB.value(forKey: "leadId") ?? "")"
        
        let arrSoldStandard = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        for obj in arrSoldStandard
        {
            let data = obj as! NSManagedObject
            
            if("\(data.value(forKey: "serviceFrequency") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame || "\(data.value(forKey: "serviceFrequency") ?? "")" == "One Time")
            {
                arrOneTime.add("OneTime")
            }
            else
            {
                arrNonOneTime.add("NonOneTime")
                
            }
        }
        
        let arrSoldNonStandard = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadIdSales, "true"))
        
        for obj in arrSoldNonStandard
        {
            let data = obj as! NSManagedObject
            
            if("\(data.value(forKey: "serviceFrequency") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame || "\(data.value(forKey: "serviceFrequency") ?? "")" == "One Time")
            {
                arrOneTime.add("OneTime")
            }
            else
            {
                arrNonOneTime.add("NonOneTime")
                
            }
        }
        
        if arrNonOneTime.count > 0
        {
            show = false
        }
        else
        {
            if (arrOneTime.count > 0)
            {
                show = true
            }
            else
            {
                show = false
            }
        }
        
        return show
    }
    
    func getTimeRangeData(strLeadId : String, matchesGeneralInfo: NSManagedObject) -> NSDictionary {
        
        //var matchesGeneralInfo = NSManagedObject()
        //matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
        let strEarliestStartTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "earliestStartTime") ?? "")", strRequiredFormat: "HH:mm:ss")
        let strLatestStartTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "latestStartTime") ?? "")", strRequiredFormat: "HH:mm:ss")
        let strServiceTime = changeStringDateToGivenFormat(strDate: "\(matchesGeneralInfo.value(forKey: "scheduleTime") ?? "")", strRequiredFormat: "HH:mm:ss")
        
        let arrKeys = ["LeadId",
                       "LeadNumber",
                       "CompanyId",
                       "CompanyKey",
                       "AccountNo",
                       "EmployeeId",
                       "Result",
                       "RangeofTimeId",
                       "ServiceDate",
                       "ServiceTime",
                       "EarliestTimeRange",
                       "LatestTimeRange",
                       "ScheduleTimeType",
                       "ScheduleStartDate",
                       "ScheduleEndDate",
                       "TotalEstimationTime"]
        
        let arrValues = [strLeadId,
                         "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")",
                         strSalesAutoCompanyId,
                         strCompanyKey,
                         "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")",
                         strEmpId,
                         "",
                         "\(matchesGeneralInfo.value(forKey: "timeRangeId") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleDate") ?? "")",
                         strServiceTime,
                         strEarliestStartTime,
                         strLatestStartTime,
                         "\(matchesGeneralInfo.value(forKey: "scheduleTimeType") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleStartDate") ?? "")",
                         "\(matchesGeneralInfo.value(forKey: "scheduleEndDate") ?? "")",
                         ""]
    
        let dictObject = NSDictionary.init(objects: arrValues as [Any], forKeys: arrKeys as [NSCopying])
        
        return dictObject
    }
    
    func getOpportunityStatusNameFromSysName()
    {
        if nsud.value(forKey: "TotalLeadCountResponse") is NSDictionary
        {
            let dictTemp = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary

            if dictTemp.value(forKey: "LeadStageMasters") is NSArray && dictTemp.value(forKey: "LeadStatusMasters") is NSArray
            {
                let arrStageMaster = dictTemp.value(forKey: "LeadStageMasters") as! NSArray
                let arrStatusMaster = dictTemp.value(forKey: "LeadStatusMasters") as! NSArray
                print(arrStageMaster,arrStatusMaster)
                
                let arrStageName = NSMutableArray()
                let arrStageId = NSMutableArray()
                let arrStageSysName = NSMutableArray()
                let arrStatusName = NSMutableArray()
                let arrStatusId = NSMutableArray()
                let arrStatusSysName = NSMutableArray()
                
                if arrStageMaster.count > 0
                {
                    for item in arrStageMaster
                    {
                        let dict = item as! NSDictionary
                        arrStageName.add("\(dict.value(forKey: "Name")!)")
                        arrStageId.add("\(dict.value(forKey: "LeadStageId")!)")
                        arrStageSysName.add("\(dict.value(forKey: "SysName")!)")
                    }
                }
                
                if arrStatusMaster.count > 0
                {
                    for item in arrStatusMaster
                    {
                        let dict = item as! NSDictionary
                        arrStatusName.add("\(dict.value(forKey: "StatusName")!)")
                        arrStatusId.add("\(dict.value(forKey: "StatusId")!)")
                        arrStatusSysName.add("\(dict.value(forKey: "SysName")!)")
                    }
                }
            
                dictOpportunityStageNameFromSysName = NSDictionary(objects:arrStageName as! [Any], forKeys:arrStageSysName as! [NSCopying]) as Dictionary as NSDictionary
                
                dictOpportunityStatusNameFromSysName = NSDictionary(objects:arrStatusName as! [Any], forKeys:arrStatusSysName as! [NSCopying]) as Dictionary as NSDictionary
                
            }
            
        }
       
    }
    
    // MARK: --------------------------- Sales Auto Cell Button Actions ---------------------------
    
    @objc func actionOnSalesAutoContactNameClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            /*let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
             
             let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as? ContactDetailsVC_CRMContactNew_iPhone
             controller?.strCRMContactIdGlobal = "\(dictDataSalesDB.value(forKey: "CompanyId")!)"
             controller?.dictContactDetailFromList = NSDictionary()
             self.navigationController?.pushViewController(controller!, animated: false)*/
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    @objc func actionOnBtnFlowTypeClick(sender: UIButton!)
    {
        let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Residential", style: .default , handler:{ (UIAlertAction)in
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("flowType")
            arrOfValues.add("Residential")

            let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",self.strUserName, "\(dictDataSalesDB.value(forKey: "leadId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                self.tblViewAppointment.reloadData()
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Commercial", style: .default , handler:{ (UIAlertAction)in
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()

            arrOfKeys.add("flowType")
            arrOfValues.add("Commercial")

            let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@",self.strUserName, "\(dictDataSalesDB.value(forKey: "leadId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                self.tblViewAppointment.reloadData()
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
//        if DeviceType.IS_IPAD
//        {
//            alert.popoverPresentationController?.sourceView = sender
//        }
        alert.popoverPresentationController?.sourceView = sender
        self.present(alert, animated: true, completion: {
        })
    }
    
    
    
    @objc func actionOnSalesAutoCompanyNameClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            /*let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
             
             let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
             controller.strCRMCompanyIdGlobal = "\(dictDataSalesDB.value(forKey: "companyId")!)"
             self.navigationController?.pushViewController(controller, animated: false)*/
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    @objc func actionOnSalesAutoAddressClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            let strAddress = Global().strCombinedAddressService(for: dictDataSalesDB)
            
            if strAddress!.count > 0
            {
                
                let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
                // Create the actions
                
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, strAddress)
                    
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    @objc func actionOnSalesAutoEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            self.sendEmail(strEmail: "\(dictDataSalesDB.value(forKey: "primaryEmail") ?? "")")
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnSalesAutoSMSClick(sender: UIButton!)
    {
        
        // Check if cutom message is selected
        
        if(nsud.value(forKey: "CustomMessage") != nil){
            
            let strCustomMessage = "\(nsud.value(forKey: "CustomMessage")!)"
            
            if strCustomMessage.count > 0 {
                
                var strMessageFinal = NSMutableString()
                
                let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
                
                strMessageFinal = strCustomMessage.replacingOccurrences(of: "##CustomerName##", with: "\(dictDataSalesDB.value(forKey: "customerName")!)") as! NSMutableString
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##TechName##", with: "\(strEmpName)") as! NSMutableString
                
                let strAddressString = "\(Global().strCombinedAddressService(for: dictDataSalesDB) ?? "")"
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##Address##", with: strAddressString) as! NSMutableString
                
                if strEmployeeEProfileUrl.count == 0 {
                    strEmployeeEProfileUrl = "http://pestream.com/"
                }
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##EprofileUrl##", with: "\(strEmployeeEProfileUrl)") as! NSMutableString
                
                let strServiceAddressString = "\(Global().strCombinedAddressService(for: dictDataSalesDB) ?? "")"
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##ServiceAddress##", with: strServiceAddressString) as! NSMutableString
                
                self.displayMessageInterface(strNo: "\(dictDataSalesDB.value(forKey: "primaryPhone")!)", strMessage: strMessageFinal as String)
                
            }else{
                
                self.reDirectOnSettingsView()
                
            }
            
        }else{
            
            self.reDirectOnSettingsView()
            
        }
        
    }
    
    @objc func actionOnSalesAutoCallClick(sender: UIButton!)
    {
        
        let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
        
        let strNoLocal = self.getSalesNoNew(dictDataSalesDB: dictDataSalesDB)
        
        Global().calling(strNoLocal)
        
    }
    func getSalesNoNew(dictDataSalesDB : NSManagedObject) -> String {
        

        var strNoLocal = "\(dictDataSalesDB.value(forKey: "cellNo") ?? "")"
        
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "primaryPhone") ?? "")"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "secondaryPhone") ?? "")"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "serviceCellNo") ?? "")"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "servicePrimaryPhone") ?? "")"
            
        }
        if strNoLocal.count == 0 {
            
            strNoLocal = "\(dictDataSalesDB.value(forKey: "serviceSecondaryPhone") ?? "")"
            
        }

        if strNoLocal.count == 0 {
            
            strNoLocal = ""
            
        }
        
        return strNoLocal
        
    }
    
    
    
    
    @objc func actionOnSalesAutoFooterEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            // Start Loader
            let loaderTempLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loaderTempLocal, animated: false, completion: nil)
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            let surveyIdLocal = "\(dictDataSalesDB.value(forKey: "surveyID")!)"
            
            let start1 = CFAbsoluteTimeGetCurrent()
            
            let strURL = UrlSalesEmail + "remindersms" + "&CompanyKey=" + strGGQCompanyKey + "&aId=" + surveyIdLocal
            
            print("Sales Email API called --- %@",strURL)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesEmail_AppointmentVC") { (response, status) in
                
                loaderTempLocal.dismiss(animated: false) {
                    
                    DispatchQueue.main.async {
                        
                        if(status)
                        {
                            
                            let diff1 = CFAbsoluteTimeGetCurrent() - start1
                            print("Took \(diff1) seconds to get Sales Email API Data From SQL Server.")
                            
                            print("Sales Email API Response")
                            
                            let arrKeys = response.allKeys as NSArray
                            
                            if arrKeys.contains("data") {
                                
                                if response.value(forKey: "data") is NSString {
                                    
                                    // sent email
                                    
                                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: SuccessMailSend, viewcontrol: self)
                                    
                                }
                            }
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                    }
                }
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnSalesAutoPrintClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "PrintiPhoneViewController") as! PrintiPhoneViewController
            controller.strLeadId = "\(dictDataSalesDB.value(forKey: "leadNumber")!)"
            controller.strAgreementName = ""
            controller.strProposalName = ""
            controller.strInspectionName = ""
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnSalesAutoResendEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject

            let branchSysNameLocal = "\(dictDataSalesDB.value(forKey: "branchSysName")!)"
            
            if branchSysNameLocal == branchSysNameLocal {
                                
                nsud.set(nil, forKey: "salesDynamicForm")
                nsud.set("\(dictDataSalesDB.value(forKey: "leadId")!)", forKey: "LeadId")
                nsud.set("\(dictDataSalesDB.value(forKey: "accountNo")!)", forKey: "AccountNos")
                nsud.set("\(dictDataSalesDB.value(forKey: "leadNumber")!)", forKey: "LeadNumber")
                nsud.set("\(dictDataSalesDB.value(forKey: "statusSysName")!)", forKey: "leadStatusSales")
                nsud.set("\(dictDataSalesDB.value(forKey: "surveyID")!)", forKey: "SurveyID")
                
                if strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID")!)".count > 0 {
                    
                    nsud.set(true, forKey: "YesSurveyService")
                    
                } else {
                    
                    nsud.set(false, forKey: "YesSurveyService")
                    
                }
                
                nsud.synchronize()
                
                if "\(dictDataSalesDB.value(forKey: "isCustomerNotPresent")!)" == "1"  || "\(dictDataSalesDB.value(forKey: "isCustomerNotPresent")!)" == "true" || "\(dictDataSalesDB.value(forKey: "isCustomerNotPresent")!)" == "True"{
                    
                    self.goToSendMailSales(strIsCustomerPresent: "yes")
                    
                } else {
                    
                    self.goToSendMailSales(strIsCustomerPresent: "no")
                    
                }
                
            }else{
                
                self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)

            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnCreateInitialSetupClick(sender: UIButton!)
    {
        let strTitle = (sender.titleLabel?.text ?? "")
        print("\(strTitle)")
        
        let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
        
        let branchSysNameLocal = "\(dictDataSalesDB.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            let strLeadId = "\(dictDataSalesDB.value(forKey: "leadId") ?? "")"
            let strLeadNumber = "\(dictDataSalesDB.value(forKey: "leadNumber") ?? "")"
            let strAccountNo = "\(dictDataSalesDB.value(forKey: "accountNo") ?? "")"
            
            nsud.setValue(strLeadId, forKey: "ForInitialSetUpLeadId")
            nsud.setValue(strLeadId, forKey: "LeadId")
            nsud.setValue(strAccountNo, forKey: "AccountNos")
            nsud.setValue(strLeadNumber, forKey: "LeadNumber")
            nsud.synchronize()
            
            
            if strTitle == "Generate Workorder"
            {
                
                if DeviceType.IS_IPAD {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GenerateWorkOrderiPad") as? GenerateWorkOrderiPad
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                } else {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "GenerateWorkOrder") as? GenerateWorkOrder
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                }

            }
            else
            {
                
                if DeviceType.IS_IPAD {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitialSetUpiPad") as? InitialSetUpiPad
                    vc?.strFromAppointment = strLeadId
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                } else {
                    
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitialSetUp") as? InitialSetUp
                    vc?.strFromAppointment = strLeadId
                    self.navigationController?.pushViewController(vc!, animated: false)
                    
                }
                

            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }

    }
    
    // MARK: --------------------------- Service Auto Cell Button Actions ---------------------------
    
    @objc func actionOnServiceAutoContactNameClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            /*let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
             
             let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as? ContactDetailsVC_CRMContactNew_iPhone
             controller?.strCRMContactIdGlobal = "\(dictDataSalesDB.value(forKey: "CompanyId")!)"
             controller?.dictContactDetailFromList = NSDictionary()
             self.navigationController?.pushViewController(controller!, animated: false)*/
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnServiceAutoCompanyNameClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            /*let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
             
             let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
             controller.strCRMCompanyIdGlobal = "\(dictDataSalesDB.value(forKey: "companyId")!)"
             self.navigationController?.pushViewController(controller, animated: false)*/
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
        }
        
    }
    
    @objc func actionOnServiceAutoAddressClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            let strAddress = Global().strCombinedAddressService(for: dictDataSalesDB)
            
            if strAddress!.count > 0
            {
                
                let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
                // Create the actions
                
                let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
                {
                    UIAlertAction in
                    Global().redirect(onAppleMap: self, strAddress)
                    
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    @objc func actionOnServiceAutoEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            strWorkOrderNoLifeStyle = "\(dictDataSalesDB.value(forKey: "workOrderNo")!)"
            
            self.sendEmail(strEmail: "\(dictDataSalesDB.value(forKey: "primaryEmail")!)")
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnServiceAutoSMSClick(sender: UIButton!)
    {
        
        // Check if cutom message is selected
        
        if(nsud.value(forKey: "CustomMessage") != nil){
            
            let strCustomMessage = "\(nsud.value(forKey: "CustomMessage")!)"
            
            if strCustomMessage.count > 0 {
                
                var strMessageFinal = NSMutableString()
                
                let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
                
                strWorkOrderNoLifeStyle = "\(dictDataSalesDB.value(forKey: "workOrderNo")!)"
                
                let strFullNameString = "\(Global().strFullName(for: dictDataSalesDB) ?? "")"
                
                strMessageFinal = strCustomMessage.replacingOccurrences(of: "##CustomerName##", with: strFullNameString) as! NSMutableString
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##TechName##", with: "\(strEmpName)") as! NSMutableString
                
                let strAddressString = "\(Global().strCombinedAddressService(for: dictDataSalesDB) ?? "")"
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##Address##", with: strAddressString) as! NSMutableString
                
                if strEmployeeEProfileUrl.count == 0 {
                    strEmployeeEProfileUrl = "http://pestream.com/"
                }
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##EprofileUrl##", with: "\(strEmployeeEProfileUrl)") as! NSMutableString
                
                strMessageFinal = strMessageFinal.replacingOccurrences(of: "##ServiceAddress##", with: Global().strCombinedAddressService(for: dictDataSalesDB)) as! NSMutableString
                
                self.displayMessageInterface(strNo: "\(dictDataSalesDB.value(forKey: "primaryPhone")!)", strMessage: strMessageFinal as String)
                
            }else{
                
                self.reDirectOnSettingsView()
                
            }
            
        }else{
            
            self.reDirectOnSettingsView()
            
        }
        
    }
    
    @objc func actionOnServiceAutoCallClick(sender: UIButton!)
    {
        
        let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
        
        let strNoLocal = self.getSalesNo(dictDataSalesDB: dictDataSalesDB)
        
        Global().calling(strNoLocal)
        
    }
    
    @objc func actionOnServiceAutoFooterEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            // Start Loader
            let loaderTempLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loaderTempLocal, animated: false, completion: nil)
            
            let dictDataSalesDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            let surveyIdLocal = "\(dictDataSalesDB.value(forKey: "surveyID")!)"
            
            let start1 = CFAbsoluteTimeGetCurrent()
            
            let strURL = UrlSalesEmail + "remindersms" + "&CompanyKey=" + strGGQCompanyKey + "&aId=" + surveyIdLocal
            
            print("Sales Email API called --- %@",strURL)
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesEmail_AppointmentVC") { (response, status) in
                
                loaderTempLocal.dismiss(animated: false) {
                    
                    DispatchQueue.main.async {
                        
                        if(status)
                        {
                            
                            let diff1 = CFAbsoluteTimeGetCurrent() - start1
                            print("Took \(diff1) seconds to get Sales Email API Data From SQL Server.")
                            
                            print("Sales Email API Response")
                            
                            let arrKeys = response.allKeys as NSArray
                            
                            if arrKeys.contains("data") {
                                
                                if response.value(forKey: "data") is NSString {
                                    
                                    // sent email
                                    
                                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: SuccessMailSend, viewcontrol: self)
                                    
                                }
                            }
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                    }
                }
            }
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnServiceAutoPrintClick(sender: UIButton!)
    {
        
        /*if isInternetAvailable() {
         
         
         
         }else{
         
         Global().alertMethod(Alert, ErrorInternetMsg)
         
         }*/
        
    }
    
    @objc func actionOnServiceAutoResendEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataServiceDB = arrayAppointment[sender.tag] as! NSManagedObject
            
            let branchSysNameLocal = "\(dictDataServiceDB.value(forKey: "branchSysName")!)"
            
            if branchSysNameLocal == strEmpLoginBranchSysName {
                
                nsud.set("\(dictDataServiceDB.value(forKey: "workorderId")!)", forKey: "LeadId")
                nsud.set("\(dictDataServiceDB.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
                nsud.set("\(dictDataServiceDB.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
                nsud.set("\(dictDataServiceDB.value(forKey: "surveyID")!)", forKey: "SurveyID")
                
                if strIsGGQIntegration == "1" && "\(dictDataServiceDB.value(forKey: "surveyID")!)".count > 0 {
                    
                    nsud.set(true, forKey: "YesSurveyService")
                    
                } else {
                    
                    nsud.set(false, forKey: "YesSurveyService")
                    
                }
                
                nsud.synchronize()
                
                if "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "1"  || "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "true" || "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "True"{
                    
                    self.goToSendMailServicAuto(strIsCustomerPresent: "yes")
                    
                } else {
                    
                    self.goToSendMailServicAuto(strIsCustomerPresent: "no")
                    
                }
                
            }else{
                
                self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
                
            }
          
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    // MARK: --------------------------- Plumbing Cell Button Actions ---------------------------
    
    @objc func actionOnPlumbingResendEmailClick(sender: UIButton!)
    {
        
        if isInternetAvailable() {
            
            let dictDataServiceDB = arrayAppointment[sender.tag] as! NSManagedObject

            let branchSysNameLocal = "\(dictDataServiceDB.value(forKey: "branchSysName")!)"
            
            if branchSysNameLocal == strEmpLoginBranchSysName {
                
                nsud.set("\(dictDataServiceDB.value(forKey: "workorderId")!)", forKey: "LeadId")
                nsud.set("\(dictDataServiceDB.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
                nsud.set("\(dictDataServiceDB.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
                nsud.set("\(dictDataServiceDB.value(forKey: "surveyID")!)", forKey: "SurveyID")
                
                if strIsGGQIntegration == "1" && "\(dictDataServiceDB.value(forKey: "surveyID")!)".count > 0 {
                    
                    nsud.set(true, forKey: "YesSurveyService")
                    
                } else {
                    
                    nsud.set(false, forKey: "YesSurveyService")
                    
                }
                
                nsud.synchronize()
                
                if "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "1"  || "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "true" || "\(dictDataServiceDB.value(forKey: "isCustomerNotPresent")!)" == "True"{
                    
                    self.goToSendMailPlumbing(strIsCustomerPresent: "yes")
                    
                } else {
                    
                    self.goToSendMailPlumbing(strIsCustomerPresent: "no")
                    
                }
                
            }else{
                
                self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
                
            }
            
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
    }
    
    @objc func actionOnPlumbingSubWoStatusChange(sender: UIButton!)
    {
        
        let dictDataServiceDB = arrayAppointment[sender.tag] as! NSManagedObject

        let branchSysNameLocal = "\(dictDataServiceDB.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            selectedIndexToUpdateStatus = sender.tag
            
            let dictDataTemp = SalesCoreDataVC().fetchSubWo(toUpdateStatus: "\(dictDataServiceDB.value(forKey: "workorderId")!)", dictDataServiceDB)
            
            let strMsg = "\(dictDataTemp.value(forKey: "strMsg")!)"
            
            if strMsg == "" {
                
                // Nothing to doa
                
            }else if strMsg == "isStartedOtherSubWorkOrder" {
                
                let objTempp = dictDataTemp.value(forKey: "objTempSubWoMain") as! NSManagedObject
                
                let dictDataTempWO = SalesCoreDataVC().showAlertIfOtherWorkOrderStarted("\(dictDataServiceDB.value(forKey: "workOrderNo")!)", "\(dictDataServiceDB.value(forKey: "workorderId")!)", "\(objTempp.value(forKey: "subWorkOrderId")!)", "\(objTempp.value(forKey: "subWOStatus")!)", objTempp)
                
                let strMsgs = "\(dictDataTempWO.value(forKey: "strMsgToShow")!)"
                
                strSubWoStatusForForcefullyStopWos = "\(objTempp.value(forKey: "subWOStatus")!)"
                objSubWosForForcefullyStopWos = objTempp
                
                let alert = UIAlertController(title: alertMessage, message: strMsgs, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Yes-Stop", style: .destructive, handler: { (nil) in
                    
                    
                    // Call API Here
                    
                    let arrOfRunningSubWorkOrderId = SalesCoreDataVC().stopAllRunningSubWorkOrder((dictDataTempWO.value(forKey: "arrOfRunningSubWorkOrder") as! NSArray) as! [Any])
                    
                    if arrOfRunningSubWorkOrderId.count > 0 {
                        
                        self.dispatchGroupPlumbingStopWO = DispatchGroup()
                        
                        
                        for k1 in 0 ..< arrOfRunningSubWorkOrderId.count {
                            
                            var strWorkOrderIddd = ""
                            
                            let arrayOfWoTemp = getDataFromCoreDataBaseArray(strEntity: "MechanicalSubWorkOrder", predicate: NSPredicate(format: "subWorkOrderId == %@", "\(arrOfRunningSubWorkOrderId[k1])"))
                            
                            if arrayOfWoTemp.count > 0  {
                                
                                let tempObj = arrayOfWoTemp[0] as! NSManagedObject
                                
                                strWorkOrderIddd = "\(tempObj.value(forKey: "workorderId")!)"
                                
                            }
                            
                            
                            
                            let strActualHourId = Global().fetchSubWorkOrderActualHrsFromDataBase(forActualHourId: strWorkOrderIddd, "\(arrOfRunningSubWorkOrderId[k1])")
                            
                            let arrayOfWoActualHoursDcs = getDataFromCoreDataBaseArray(strEntity: "MechanicalSubWorkOrderActualHoursDcs", predicate: NSPredicate(format: "workorderId = %@ && subWorkOrderId = %@ && subWOActualHourId = %@", strWorkOrderIddd, "\(arrOfRunningSubWorkOrderId[k1])", "\(strActualHourId ?? "")"))
                            
                            if arrayOfWoActualHoursDcs.count > 0 {
                                
                                let matchesSubWorkOrderActualHrs = arrayOfWoActualHoursDcs[0] as! NSManagedObject
                                
                                let timeOutt = Global().strCurrentDateFormattedForMechanical()
                                
                                matchesSubWorkOrderActualHrs.setValue(timeOutt, forKey: "timeOut")
                                matchesSubWorkOrderActualHrs.setValue(timeOutt, forKey: "mobileTimeOut")
                                
                                matchesSubWorkOrderActualHrs.setValue("Due to start other WorkOrder", forKey: "reason")
                                matchesSubWorkOrderActualHrs.setValue("Due to start other WorkOrder", forKey: "actHrsDescription")
                                
                                let context = getContext()
                                do {
                                    try context.save()
                                } catch _ as NSError  {
                                    
                                } catch {
                                    
                                }
                                
                                if isInternetAvailable() {
                                    
                                    // Start Loader
                                    self.loaderPlumbingStopWO = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                    self.present(self.loaderPlumbingStopWO, animated: false, completion: nil)
                                    
                                    self.dispatchGroupPlumbingStopWO.enter();
                                    
                                    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "MechanicalActualHrsSyncGeneralInfoForceFully"),
                                                                           object: nil,
                                                                           queue: nil,
                                                                           using:self.catchNotificationMechanicalStopWO)
                                    
                                    SyncMechanicalViewController().syncMechanicalActualHours(strWorkOrderIddd, "\(arrOfRunningSubWorkOrderId[k1])", "\(strActualHourId ?? "")", "MechanicalActualHrsSyncGeneralInfoForceFully")
                                    
                                }
                                
                            }
                            
                            Global().fetchMechanicalSubWorkOrder(toStopJob: strWorkOrderIddd, "\(arrOfRunningSubWorkOrderId[k1])")
                            
                            
                        }
                        
                        
                        self.dispatchGroupPlumbingStopWO.notify(queue: DispatchQueue.main, execute: {
                            // All data downloaded
                            self.loaderPlumbingStopWO.dismiss(animated: false) {
                                DispatchQueue.main.async {
                                    // Reload Table
                                    
                                    self.checkAfterStopingWos()
                                    
                                }
                            }
                        })
                        
                    }
                    
                    
                }))
                alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                    
                    
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
                
            } else if strMsg == "updateStatus" {
                
                let objTempSubWoMain = dictDataTemp.value(forKey: "objTempSubWoMain") as! NSManagedObject
                
                let strSubWoStatusLocal = "\(objTempSubWoMain.value(forKey: "subWOStatus")!)"
                
                if strSubWoStatusLocal == "New" || strSubWoStatusLocal == "Not Started" || strSubWoStatusLocal == "NotStarted" {
                    
                    if Global().isNetReachable() {
                        
                        self.metodUpdateSubWorkOrderStatus(strStatusToSent: "New", strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                        
                    }else{
                        
                        SalesCoreDataVC().addActualHrsIfResponseIsNil("\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                        
                    }
                    
                    SalesCoreDataVC().upDateSubWorkOrderStatus(fromDB: "Dispatch", "\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                    
                    //fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate
                    Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
                    
                }else if strSubWoStatusLocal == "Dispatch" {
                    
                    if Global().isNetReachable() {
                        
                        self.metodUpdateSubWorkOrderStatus(strStatusToSent: "Dispatch", strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                        
                    }else{
                        
                        SalesCoreDataVC().addActualHrsIfResponseIsNil("\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                        
                    }
                    
                    SalesCoreDataVC().upDateSubWorkOrderStatus(fromDB: "OnRoute", "\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                    
                    //fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate
                    Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
                    
                }else if strSubWoStatusLocal == "OnRoute" {
                    
                    self.fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute(strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)", objSubWoTemp: objTempSubWoMain)
                    
                    Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
                    
                }else {
                    
                    
                }
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMsg, viewcontrol: self)
                
            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }

    }
    // MARK: --------------------------- Service Auto Functions ---------------------------
    
    // 03 August Service Basic Api Implementation

    func getServiceAutoAppointmentsV1(arrOfWorkOrderIdLocal : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
                
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersServiceAutomation_SPV1

        print("Service Auto Get Work order API called --- %@",strURL)

        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyKey",
                "EmployeeNo",
                "WorkOrderIds"]
        
        values = [strCompanyKey,
                  strEmpNo,
                  arrOfWorkOrderIdLocal]
        
        let dictReqObjToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        
        WebService.postDataWithHeadersToServer(dictJson: dictReqObjToSend, url: strURL, strType: "AppointmentPostBasicInfoGet") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Auto Appointments API Data From SQL Server.")
                
                print("Service Auto Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictServiceWoResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        self.saveWorkOrdeDb(dictOfServiceWoData: dictServiceWoResponseLocal)
                        
                    }
                }
            }
        }
    }
    
    func getServiceAutoAppointmentsWithBasicInfo()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersServiceAutomation_BasicInfo + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Service Auto Get Leads Basic Info API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetBasicInfo_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Auto Basic Info Appointments API Data From SQL Server.")
                
                print("Service Auto Get Basic Info Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                                                
                        let arrayWorkOrderResponseLocal = response.value(forKey: "data") as! NSArray
                        
                        if arrayWorkOrderResponseLocal.count > 0 {
                            
                            self.fetchServiceWorkOrderFromDBToCheck(arrayOfWorkOrders: arrayWorkOrderResponseLocal)
                            
                        }
                    }
                }
            }else{
                
                // delete extra WO if exists from Db
                
                let arrayOfWorkOrdersLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceAutoModifyDate, predicate: NSPredicate(format: "userName == %@", self.strUserName))

                // Check which to send and which to update directly
                
                let arrTemp = NSArray()
                self.manageUserDefaultsForWDO()
                self.arrayOfWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendServiceAppointment(toServerNew: arrTemp as! [Any], arrayOfWorkOrdersLocal as! [Any])
                                
                // Delete Work Order Data Before Saving
                
                self.deletServiceWoFromDB()
                
            }
        }
    }
    
    func getServiceAutoAppointments()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersServiceAutomation_SP + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Service Auto Get Work order API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "ServiceGetWO_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Auto Appointments API Data From SQL Server.")
                
                print("Service Auto Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictServiceWoResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        self.fetchServiceWoFromDB(dictOfServiceWoData: dictServiceWoResponseLocal)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    // 03 August Service Basic Api Implementation

    func fetchServiceWorkOrderFromDBToCheck(arrayOfWorkOrders : NSArray) {
        
        let arrayOfWorkOrdersLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceAutoModifyDate, predicate: NSPredicate(format: "userName == %@", strUserName))

        // Check which to send and which to update directly
        manageUserDefaultsForWDO()
        arrayOfWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendServiceAppointment(toServerNew: arrayOfWorkOrders as! [Any], arrayOfWorkOrdersLocal as! [Any])
                                                                                                
        
        // Delete Work Orders Data Before Saving
        
        deletServiceWoFromDB()
        
        let arrOfWorkOrdersToFetchFromServer = nsud.value(forKey: "arrOfWorkOrdersToFetchFromServer") as! NSArray
                        
        if arrOfWorkOrdersToFetchFromServer.count > 0 {
            
            self.getServiceAutoAppointmentsV1(arrOfWorkOrderIdLocal: arrOfWorkOrdersToFetchFromServer)
            
        }
        
        // send to server if
        
        if(nsud.value(forKey: "sendServiceLeadToServer") != nil){
            
            let arrofWorkOrdersToSendToServer = nsud.value(forKey: "sendServiceLeadToServer") as! NSArray
            
            if arrofWorkOrdersToSendToServer.count > 0 {
                indexDynamicFormToSendServiceAuto = 0
                // Sending Work Order and DYnamic form to server
                self.sendServiceDataToServer(indexx: 0)
            }
            
        }
        
        // Download Dynamic Form From Server
        
        if arrOfWorkOrdersToFetchFromServer.count > 0 {
            
            self.getServiceAutoDynamicForm(arrayOfWOIdToFetchDynamicForm: arrOfWorkOrdersToFetchFromServer)
            
            //  Fetch Service Equipments Dynammic Form
            
            let isEquipmentEnabled = nsud.bool(forKey: "isEquipmentEnabled")
            
            if isEquipmentEnabled {
                
                self.getServiceAutoEquipmentDynamicForm(arrayOfWOIdToFetchDynamicForm: arrOfWorkOrdersToFetchFromServer)
                
            }
            
            // fetch Device dynamic form
            
            self.getServiceAutoDeviceDynamicForm(arrayOfWOIdToFetchDynamicForm: arrOfWorkOrdersToFetchFromServer)
            
        }
        
    }
    
    func saveWorkOrdeDb(dictOfServiceWoData : NSDictionary) {
        
        let arrayOfWoIdToFetchDynamicForm = NSMutableArray()
        
        if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
            
            let arrWoComingFromServer = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                for k in 0 ..< arrWoComingFromServer.count {
                    
                    if arrWoComingFromServer[k] is NSDictionary {
                        
                        let dictServiceLocal = arrWoComingFromServer[k] as! NSDictionary
                        
                        if dictServiceLocal.count > 0 {
                            
                            if dictServiceLocal.value(forKey: "WorkorderDetail") is NSDictionary {
                                
                                let woDetailLocal = dictServiceLocal.value(forKey: "WorkorderDetail") as! NSDictionary
                                
                                arrayOfWoIdToFetchDynamicForm.add("\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")
                                
                                deleteWoById(leadIdLocal: "\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")
                                
                            }
                        }
                    }
                }
            }
        }
        
        saveServiceToDB(dictOfServiceWoData: dictOfServiceWoData)
        
        if arrayOfWoIdToFetchDynamicForm.count > 0 {
            
            // fetch Service Dynmaic forms
            self.getServiceAutoDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
            
            //  Fetch Service Equipments Dynammic Form
            
            let isEquipmentEnabled = nsud.bool(forKey: "isEquipmentEnabled")
            
            if isEquipmentEnabled {
                
                self.getServiceAutoEquipmentDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
                
            }
            // fetch Device dynamic form
            
            self.getServiceAutoDeviceDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
        }
    }
    
    func fetchServiceWoFromDB(dictOfServiceWoData : NSDictionary) {
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceAutoModifyDate, predicate: NSPredicate(format: "userName == %@", strUserName))
        
        if arrayOfWoLocal.count == 0 {
            
            // No data to compare direct save to DB and fetch Dynamic Form Data
            
            saveServiceToDB(dictOfServiceWoData: dictOfServiceWoData)
            
            let arrayOfWoIdToFetchDynamicForm = NSMutableArray()
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                let arrWoComingFromServer = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                
                if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                    
                    
                    for k in 0 ..< arrWoComingFromServer.count {
                        
                        if arrWoComingFromServer[k] is NSDictionary {
                            
                            let dictServiceLocal = arrWoComingFromServer[k] as! NSDictionary
                            
                            if dictServiceLocal.count > 0 {
                                
                                if dictServiceLocal.value(forKey: "WorkorderDetail") is NSDictionary {
                                    
                                    let woDetailLocal = dictServiceLocal.value(forKey: "WorkorderDetail") as! NSDictionary
                                    
                                    arrayOfWoIdToFetchDynamicForm.add("\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    if arrayOfWoIdToFetchDynamicForm.count > 0 {
                        
                        // fetch Service Dynmaic forms
                        self.getServiceAutoDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
                        
                        //  Fetch Service Equipments Dynammic Form
                        
                        let isEquipmentEnabled = nsud.bool(forKey: "isEquipmentEnabled")
                        
                        if isEquipmentEnabled {
                            
                            self.getServiceAutoEquipmentDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
                            
                        }
                        
                        // fetch Device dynamic form
                        
                        self.getServiceAutoDeviceDynamicForm(arrayOfWOIdToFetchDynamicForm: arrayOfWoIdToFetchDynamicForm)
                        
                    }
                    
                }
                
            }
            
            
        } else {
            
            manageUserDefaultsForWDO()
            arrayOfWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendServiceAppointment(toServer: dictOfServiceWoData as! [AnyHashable : Any], arrayOfWoLocal as! [Any])
            
            // Delete WO Data Before Saving
            
            deletServiceWoFromDB()
            
            let arrofWoIdsLocal = nsud.value(forKey: "saveServiceLeadToLocalDbnDelete") as! NSArray
            let arrofWoIdsLocal1 = nsud.value(forKey: "saveTolocalDbAnyHowService") as! NSArray
            
            let arrofWoIdsAll = arrofWoIdsLocal + arrofWoIdsLocal1 as! NSMutableArray
            
            for k1 in 0 ..< arrofWoIdsAll.count {
                
                if dictOfServiceWoData.count > 0 {
                    
                    if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                        
                        let arrayOfWoDataLocal = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                        
                        if arrayOfWoDataLocal.count > 0 {
                            
                            // Wo delete before saving
                            
                            self.deleteWoById(leadIdLocal: "\(arrofWoIdsAll[k1])")
                            
                            SalesCoreDataVC().save(toCoreDataServiceWo: arrayOfWoDataLocal as! [Any], "\(arrofWoIdsAll[k1])", "Single")
                            
                        }
                        
                    }
                    
                }
            }
            
            // send to server if
            
            let arrofWoIdsToSendToServer = nsud.value(forKey: "sendServiceLeadToServer") as! NSArray
            
            if arrofWoIdsToSendToServer.count > 0 {
                indexDynamicFormToSendServiceAuto = 0
                // Sending lead and DYnamic form to server
                self.sendServiceDataToServer(indexx: 0)
            }
            
            // Download Dynamic Form From Server
            
            if arrofWoIdsAll.count > 0 {
                
                // fetch Service Dynmaic forms
                self.getServiceAutoDynamicForm(arrayOfWOIdToFetchDynamicForm: arrofWoIdsAll)
                
                //  Fetch Service Equipments Dynammic Form
                
                let isEquipmentEnabled = nsud.bool(forKey: "isEquipmentEnabled")
                
                if isEquipmentEnabled {
                    
                    self.getServiceAutoEquipmentDynamicForm(arrayOfWOIdToFetchDynamicForm: arrofWoIdsAll)
                    
                }
                
                // fetch Device dynamic form
                
                self.getServiceAutoDeviceDynamicForm(arrayOfWOIdToFetchDynamicForm: arrofWoIdsAll)
                
            }
            
        }
    }
    
    
    func sendServiceDataToServer(indexx : Int) {
        
        self.dispatchGroupServiceAuto = DispatchGroup()
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let arrofWoIdsToSendToServer = nsud.value(forKey: "sendServiceLeadToServer") as! NSArray
        // 2 > 0 2 > 1 2 > 2
        if arrofWoIdsToSendToServer.count <= indexx {
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            // All Sent To server
            
        }else{
            
            
            // check if servcie dynamci available then upload it
            
            let arrayOfServiceDynamicInspection = getDataFromCoreDataBaseArray(strEntity: "ServiceDynamicForm", predicate: NSPredicate(format: "workOrderId = %@","\(arrofWoIdsToSendToServer[indexx])"))
            
            if arrayOfServiceDynamicInspection.count > 0 {
                
                var dictTemp = NSDictionary()
                var arrTemp = NSArray()
                
                let objServiceInspectionData = arrayOfServiceDynamicInspection[0] as! NSManagedObject
                
                if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                    
                    arrTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSArray
                    
                    if arrTemp.count > 0 {
                        
                        if arrTemp[0] is NSDictionary {
                            
                            dictTemp = arrTemp[0] as! NSDictionary
                            
                        }
                        
                    }
                    
                } else if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSDictionary {
                    
                    dictTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSDictionary
                    
                } else {
                    
                }
                
                if dictTemp.count > 0 {
                    
                    // Uploading API Here
                    
                    self.sendingServiceAutoDynamicDataToServer(dictToSend: dictTemp, strWoIdToSend: "\(arrofWoIdsToSendToServer[indexx])")
                    
                }
                
            }
            
            // Fetch data to upload images audio n documents n all
            
            arrOfAllImagesToSendToServer = NSMutableArray()
            arrOfAllSignatureImagesToSendToServer = NSMutableArray()
            arrOfAllCheckImageToSend = NSMutableArray()
            
            var strModifyDateToSendToServerAll = ""
            
            let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId = %@","\(arrofWoIdsToSendToServer[indexx])"))
            
            if arrayOfWoLocal.count > 0 {
                
                let objTemp = arrayOfWoLocal[0] as! NSManagedObject
                strModifyDateToSendToServerAll = "\(objTemp.value(forKey: "modifyDate") ?? "")"
                
            }
            
            var objWorkorderDetail = NSManagedObject()

            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", "\(arrofWoIdsToSendToServer[indexx])"))
            
            var strworkorderNo = ""

            if arryOfData.count > 0 {
                
                objWorkorderDetail = arryOfData[0] as! NSManagedObject
                
                let tempStrWoNo = objWorkorderDetail.value(forKey: "workOrderNo") as? String ?? ""
                
                if tempStrWoNo == ""{
                    strworkorderNo = ""
                }
                else{
                    strworkorderNo = tempStrWoNo
                }
                
            }

            let dictDataTemp = SalesCoreDataVC().fetchServiceData(fromDB: strModifyDateToSendToServerAll, "\(arrofWoIdsToSendToServer[indexx])", strworkorderNo)
            
            // Uploading Documnets To Server
            
            let arrOfAllDocumentsToSend = dictDataTemp.value(forKey: "arrOfAllDocumentsToSend") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllDocumentsToSend.count {
                
                let objTemp = arrOfAllDocumentsToSend[k1] as! NSDictionary
                
                let arrTemp = objTemp.allKeys as NSArray
                
                if arrTemp.contains("serviceAddressDocName") {
                    
                    var strServiceAddressDocPath = "\(objTemp.value(forKey: "serviceAddressDocPath") ?? "")"
                    
                    strServiceAddressDocPath = Global().strDocName(fromPath: strServiceAddressDocPath)
                    
                    let strURL = strServiceAutoUrlMain + UrlServicePestDocUploadAsync
                    
                    self.uploadImageService(strImageName: strServiceAddressDocPath, strUrll: strURL, strType: "serviceAddressDocNameSAPestDocuments" , dictDataTemp: objTemp, strIddd: "\(arrofWoIdsToSendToServer[indexx])")
                    
                }else{
                    
                    var strServiceAddressDocPath = "\(objTemp.value(forKey: "documentPath") ?? "")"
                    
                    strServiceAddressDocPath = Global().strDocName(fromPath: strServiceAddressDocPath)
                    
                    let strURL = strServiceAutoUrlMain + UrlServicePestConditionDocUploadAsync
                    
                    self.uploadImageService(strImageName: strServiceAddressDocPath, strUrll: strURL, strType: "documentPathSAPestDocuments" , dictDataTemp: objTemp , strIddd: "\(arrofWoIdsToSendToServer[indexx])")
                    
                }
                
            }
            
            // Images
            
            let arrOfAllImagesToSendToServerServcie = dictDataTemp.value(forKey: "arrOfAllImagesToSendToServer") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllImagesToSendToServerServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImageService(strImageName: "\(arrOfAllImagesToSendToServerServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            let arrOfStringImages = GeneralInfoFinalJSONSyncingDBFuncs().fetchImagesToBeSync(strWoId: "\(arrofWoIdsToSendToServer[indexx])")
            
            let arrOfImages = self.getUIImageFromString(arrOfImagesString : arrOfStringImages)
            
            ServiceGalleryService().uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: self.strServiceAutoUrlMain, arrOfImages, arrOfStringImages) { (object, error) in
                if let object = object {
                    print(object)
                } else {
                    print(error ?? "")
                }
                
            }

            GeneralInfoFinalJSONSyncingDBFuncs().fetchDocumentToBeSync(strWoId: "\(arrofWoIdsToSendToServer[indexx])" , strServiceAutoUrlMain:  "\(self.dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") ?? "")" , strWoNo: strworkorderNo)
            
            // signature Images
            
            let arrOfAllSignatureImagesToSendToServerServcie = dictDataTemp.value(forKey: "arrOfAllSignatureImagesToSendToServer") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllSignatureImagesToSendToServerServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImageService(strImageName: "\(arrOfAllSignatureImagesToSendToServerServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            // check Images
            
            let arrOfAllCheckImageToSendServcie = dictDataTemp.value(forKey: "arrOfAllCheckImageToSend") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllCheckImageToSendServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImageService(strImageName: "\(arrOfAllCheckImageToSendServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            // AUdio Upload
            
            let arrofAudios = GeneralInfoFinalJSONSyncingDBFuncs().fetchAudioToBeSync(strWoId: "\(arrofWoIdsToSendToServer[indexx])")
            
            for i in 0..<arrofAudios.count{
                let strAudio = arrofAudios[i]
                if strAudio != ""{
                    self.uploadAudioSales(strAudioName: strAudio, strUrll: strServiceAutoUrlMain + UrlAudioUploadAsync)
                }
            }
            
            //CComment by Ruchika oon 18 Novv
//            let stAudioSales = "\(dictDataTemp.value(forKey: "strAudioNameGlobal") ?? "")"
//
//            if stAudioSales.count > 0 {
//
//                let strURL = strServiceAutoUrlMain + UrlAudioUploadAsync
//
//                let strName = SalesCoreDataVC().strAudioName(stAudioSales)
//
//                self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
//
//            }
            
            // Upload Service Address Image
            
            // Commit.
            
            let serviceAddressImagePath = "\(dictDataTemp.value(forKey: "serviceAddressImagePath") ?? "")"
            
            if serviceAddressImagePath.count > 0 {
                
                let strURL = strServiceAddressImageUrl + "api/File/UploadCustomerAddressImageAsync"
                
                self.uploadImageService(strImageName: serviceAddressImagePath, strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            let dictFinal = dictDataTemp.value(forKey: "dictFinal") as! NSMutableDictionary
            
            // Upload Texas termite Image Details
            
            if dictFinal.value(forKey: "TexasTermiteImagesDetail") is NSArray {
                
                let arrTexasTermiteImagesDetail = dictFinal.value(forKey: "TexasTermiteImagesDetail") as! NSArray
                
                for k1 in 0 ..< arrTexasTermiteImagesDetail.count {
                    
                    let dictObjTemp = arrTexasTermiteImagesDetail[k1] as! NSDictionary
                    
                    let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                    
                    self.uploadImageService(strImageName: "\(dictObjTemp.value(forKey: "woImagePath") ?? "")", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                    
                }
                
            }
            
            if DeviceType.IS_IPAD {
                
                // Upload Problem Images
                
                let arrOfAllProblemImageToSendToServer = dictDataTemp.value(forKey: "arrOfAllProblemImageToSendToServer") as! NSMutableArray
                
                for k1 in 0 ..< arrOfAllProblemImageToSendToServer.count {
                    
                    let strURL = strServiceAutoUrlMain + UrlWdoProblemImageUploadAsync
                    
                    self.uploadImageService(strImageName: "\(arrOfAllProblemImageToSendToServer[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                    
                }
                
                // Upload Graph XML
                
                let arrOfAllGraphXmlToSendToServer = dictDataTemp.value(forKey: "arrOfAllGraphXmlToSendToServer") as! NSMutableArray
                
                for k1 in 0 ..< arrOfAllGraphXmlToSendToServer.count {
                    
                    let strURL = strServiceAutoUrlMain + UrlWdoGraphXmlUploadAsync
                    
                    self.uploadAudioSales(strAudioName: "\(arrOfAllGraphXmlToSendToServer[k1])", strUrll: strURL)
                    
                }
                
            }
            
            self.dispatchGroupServiceAuto.notify(queue: DispatchQueue.main, execute: {
                DispatchQueue.main.async {
                    // Reload Table
                    
                    // Send FInal Service json To server
                    
                    self.sendingServiceWoDataToServer(dictToSend: dictFinal, strWoIdToSend: "\(arrofWoIdsToSendToServer[indexx])")
                    
                }
                
            })
            
        }
        
    }
    
    //Add by Ruchika
    func getUIImageFromString(arrOfImagesString: [String]) -> [UIImage]{
        var arrOfUIImages = [UIImage]()
        
        for i in 0..<arrOfImagesString.count{
            print(arrOfImagesString[i])
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arrOfImagesString[i])
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arrOfImagesString[i])
            print("\(arrOfImagesString[i]) image exist \(isImageExists)")

            if isImageExists == true{
                arrOfUIImages.append(image!)
            }
            
        }
        
        return arrOfUIImages
    }
    
    func sendingServiceWoDataToServer(dictToSend : NSDictionary , strWoIdToSend : String) {
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)//commit
            
        }
        
        let strUrl = strServiceAutoUrlMain + UrlUpdateWorkorderDetail
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            // Service Data synced
            self.indexDynamicFormToSendServiceAuto = self.indexDynamicFormToSendServiceAuto + 1
            
            self.sendServiceDataToServer(indexx: self.indexDynamicFormToSendServiceAuto)
            
            self.sendingServiceWoAllDynamicFormDataToServer(strWoIdToSend: strWoIdToSend)
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    let value = responce.value(forKey: "data")
                    
                    debugPrint(responce)
                    
                    // Saving Target In DB Also
                    
                    if value is NSDictionary {
                        
                        var dictTemp = value as! NSDictionary
                        
                        dictTemp = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTemp as! [AnyHashable : Any])) as NSDictionary
                        
                        if dictTemp.value(forKey: "RegularflowResults") is NSDictionary {
                            
                            let RegularflowResults = dictTemp.value(forKey: "RegularflowResults") as! NSDictionary
                            
                            WebService().savingServicePestAllDataDB(dictWorkOrderDetail: RegularflowResults, strWoId: strWoIdToSend, strType: "WithoutWoDetail", strToDeleteAreasDevices: "Yes")
                            
                        }
                        //
                        
                        if DeviceType.IS_IPAD {
                            
                            if dictTemp.value(forKey: "WONPMAResult") is NSDictionary {
                                
                                let WONPMAResult = dictTemp.value(forKey: "WONPMAResult") as! NSDictionary
                                
                                WebService().savingNPMADataToCoreDB(dictWorkOrderDetail: WONPMAResult, strWoId: "\(dictTemp.value(forKey: "WorkOrderId") ?? strWoIdToSend)", strType: "")
                                
                                /*// Updating IsMailSent to False for Completed WO it's a Jugad for not sending Mails again
                                 
                                 let statussResponse = "\(dictTemp.value(forKey: "WorkorderStatus") ?? "")"
                                 
                                 if statussResponse == "Complete" || statussResponse == "Completed" || statussResponse == "complete" || statussResponse == "completed" {
                                 
                                 Global().updateWdoNpmaIsMailSent("\(dictTemp.value(forKey: "WorkOrderId") ?? strWoIdToSend)")
                                 
                                 }*/
                                
                                
                            }
                            
                            if dictTemp.value(forKey: "WOWDOResult") is NSDictionary {
                                
                                let WOWDOResult = dictTemp.value(forKey: "WOWDOResult") as! NSDictionary
                                
                                //WebService().savingWDOAllDataDB(dictWorkOrderDetail: WOWDOResult, strWoId: "\(dictTemp.value(forKey: "WorkOrderId") ?? strWoIdToSend)", strType: "WithoutWoDetail", strWdoLeadId: "")
                                WebService().savingWDOAllDataDB(dictWorkOrderDetail: WOWDOResult, strWoId: "\(dictTemp.value(forKey: "WorkOrderId") ?? strWoIdToSend)", strType: "WithoutWoDetail", strWdoLeadId: "", isFromServiceHistory: false)

                                
                                
                                
                                
                                /*// Updating IsMailSent to False for Completed WO it's a Jugad for not sending Mails again
                                 
                                 let statussResponse = "\(dictTemp.value(forKey: "WorkorderStatus") ?? "")"
                                 
                                 if statussResponse == "Complete" || statussResponse == "Completed" || statussResponse == "complete" || statussResponse == "completed" {
                                 
                                 Global().updateWdoNpmaIsMailSent("\(dictTemp.value(forKey: "WorkOrderId") ?? strWoIdToSend)")
                                 
                                 }*/
                                
                            }
                            
                        }
                        
                    }
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("zSync")
                    arrOfValues.add("no")
                    
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId=%@",strWoIdToSend), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func sendingServiceWoAllDynamicFormDataToServer(strWoIdToSend : String) {
        
        // check if servcie Equipemnets dynamci available then upload it
        
        let arrayOfServiceEquipmentsDynamicInspection = getDataFromCoreDataBaseArray(strEntity: "EquipmentDynamicForm", predicate: NSPredicate(format: "workorderId = %@","\(strWoIdToSend)"))
        
        if arrayOfServiceEquipmentsDynamicInspection.count > 0 {
            
            let arrTempMain = NSMutableArray()
            
            for k1 in 0 ..< arrayOfServiceEquipmentsDynamicInspection.count {
                
                var dictTemp = NSDictionary()
                var arrTemp = NSArray()
                
                let objServiceInspectionData = arrayOfServiceEquipmentsDynamicInspection[k1] as! NSManagedObject
                
                if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                    
                    arrTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSArray
                    
                    if arrTemp.count > 0 {
                        
                        if arrTemp[0] is NSDictionary {
                            
                            dictTemp = arrTemp[0] as! NSDictionary
                            
                        }
                        
                    }
                    
                } else if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSDictionary {
                    
                    dictTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSDictionary
                    
                } else {
                    
                }
                
                if dictTemp.count > 0 {
                    
                    arrTempMain.add(dictTemp)
                    
                }
                
            }
            
            if arrTempMain.count > 0 {
                
                self.sendingServiceAutoEquipmentsDynamicDataToServer(arrData: arrTempMain, strWoIdToSend: "\(strWoIdToSend)")
                
            }
            
        }
        
        // check if servcie Device dynamci available then upload it
        
        let arrayOfServiceDeviceDynamicInspection = getDataFromCoreDataBaseArray(strEntity: "DeviceInspectionDynamicForm", predicate: NSPredicate(format: "workOrderId = %@","\(strWoIdToSend)"))
        
        if arrayOfServiceDeviceDynamicInspection.count > 0 {
            
            let arrTempMain = NSMutableArray()
            
            for k1 in 0 ..< arrayOfServiceDeviceDynamicInspection.count {
                
                var dictTemp = NSDictionary()
                var arrTemp = NSArray()
                
                let objServiceInspectionData = arrayOfServiceDeviceDynamicInspection[k1] as! NSManagedObject
                
                if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                    
                    arrTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSArray
                    
                    if arrTemp.count > 0 {
                        
                        if arrTemp[0] is NSDictionary {
                            
                            dictTemp = arrTemp[0] as! NSDictionary
                            
                        }
                        
                    }
                    
                } else if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSDictionary {
                    
                    dictTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSDictionary
                    
                } else {
                    
                }
                
                if dictTemp.count > 0 {
                    
                    arrTempMain.add(dictTemp)
                    
                }
                
            }
            
            if arrTempMain.count > 0 {
                
                self.sendingServiceAutoDeviceDynamicDataToServer(arrData: arrTempMain, strWoIdToSend: "\(strWoIdToSend)")
                
            }
            
            
        }
        
    }
    
    func sendingServiceAutoDynamicDataToServer(dictToSend : NSDictionary , strWoIdToSend : String) {
        
        self.dispatchGroupServiceAuto.enter()
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = strServiceAutoUrlMain + UrlServiceDynamicFormSubmission
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "string"){ (responce, status) in
            
            self.dispatchGroupServiceAuto.leave()
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    debugPrint(responce)
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isSentToServer")
                    arrOfValues.add("Yes")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceDynamicForm", predicate: NSPredicate(format: "workOrderId = %@",strWoIdToSend), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func sendingServiceAutoEquipmentsDynamicDataToServer(arrData : NSArray , strWoIdToSend : String) {
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["EquipmentInspectionFormsData"]
        
        values = [arrData]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = strServiceAutoUrlMain + UrlEquipmentDynamicFormSubmission
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "string"){ (responce, status) in
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    debugPrint(responce)
                    
                }
                
            }
            
        }
        
    }
    
    func sendingServiceAutoDeviceDynamicDataToServer(arrData : NSArray , strWoIdToSend : String) {
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        /*var keys = NSArray()
         var values = NSArray()
         keys = ["DeviceTypeFormInformationJsonExtSerDc"]
         
         values = [arrData]
         
         let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])*/
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(arrData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: arrData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strdatafirst = "\(jsonString)"
        
        let requestData = strdatafirst.data(using: .utf8)
        
        let strUrl = strServiceAutoUrlMain + UrlDeviceDynamicFormSubmission
        
        Global().getServerResponse(forSalesDynamicJson: strUrl, requestData, withCallback: { success, response, error in
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            if success {
                
                
                
            }
            
            
        })
        
    }
    
    //  Logic to Delete Wdo Graph Draw Version History for the WO which are not present in DB
    
    func deleteWdoGraphDrawHistoryRecordsFromDB() {
        
        let context = getContext()

        let arrofWoIdsToDeleteExtra = nsud.value(forKey: "DeleteExtraWorkOrderFromDBService") as! NSArray
        
        for k1 in 0 ..< arrofWoIdsToDeleteExtra.count {
            
            let workOrderIdLocal = "\(arrofWoIdsToDeleteExtra[k1])"
         
            // Create Fetch Request
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WdoGraphDrawHistory")

            // Add Sort Descriptor
            let sortDescriptor = NSSortDescriptor(key: "localCreatedDate", ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]
            let predicateL = NSPredicate(format: "workorderId == %@", workOrderIdLocal)
            fetchRequest.predicate = predicateL

            do {
                
                let records = try context.fetch(fetchRequest) as! [NSManagedObject]

                for k in 0 ..< records.count {
                    
                    let recordObjL = records[k]
                    
                    // Delete data from Document Directory
                    
                    let xmlImageName = "\(recordObjL.value(forKey: "woImagePath") ?? "")"
                    
                    Global().removeImage(fromDocumentDirectory: xmlImageName)
                    
                    let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")

                    Global().removeImage(fromDocumentDirectory: strImageNameSales)
                    
                    context.delete(recordObjL)

                }

            } catch {
                print(error)
            }
        }
    }
    
    func deletServiceWoFromDB() {
        
        // delete wdo graph draw history
        
        self.deleteWdoGraphDrawHistoryRecordsFromDB()
        
        //let arrofLeadIdsToDelete = nsud.value(forKey: "saveServiceLeadToLocalDbnDelete") as! NSArray
        
        let arrofLeadIdsToDeleteExtra = nsud.value(forKey: "DeleteExtraWorkOrderFromDBService") as! NSArray
        
        let arrOldLeadToDelete = SalesCoreDataVC().fetchOldWOToDeleteFromMobileServiceFlow() as NSArray
        
        let mixedArrayToDelete = arrofLeadIdsToDeleteExtra + arrOldLeadToDelete
        
        for k1 in 0 ..< mixedArrayToDelete.count {
            
            let leadIdLocal = "\(mixedArrayToDelete[k1])"
            
            deleteAllRecordsFromDB(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WorkOrderEquipmentDetails", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ImageDetailsTermite", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WorkorderDetailChemicalListService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "TexasTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "FloridaTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            // Db's Created by Ruchika for New Pest Flow
            
//            deleteAllRecordsFromDB(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//            deleteAllRecordsFromDB(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//            deleteAllRecordsFromDB(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//            deleteAllRecordsFromDB(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
            
        }
        
    }
    
    func deleteWoById(leadIdLocal : String) {
        
        deleteAllRecordsFromDB(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WorkOrderEquipmentDetails", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ImageDetailsTermite", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WorkorderDetailChemicalListService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "TexasTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "FloridaTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        // Db's Created by Ruchika for New Pest Flow
        
//        deleteAllRecordsFromDB(strEntity: "GeneralInfoWorkorderTimeSheet", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//        deleteAllRecordsFromDB(strEntity: "ServiceAreaDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//        deleteAllRecordsFromDB(strEntity: "ServiceDeviceDocuments", predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
//
//        deleteAllRecordsFromDB(strEntity: "ServiceCategoryGeneralInfo", predicate: NSPredicate(format: "workorderId == %@ && companyKey == %@", leadIdLocal,Global().getCompanyKey()))
        
        
    }
    
    func saveServiceToDB(dictOfServiceWoData : NSDictionary) {
        
        if dictOfServiceWoData.count > 0 {
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                let arrayOfWoDataLocal = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                
                if arrayOfWoDataLocal.count > 0 {
                    
                    SalesCoreDataVC().save(toCoreDataServiceWo: arrayOfWoDataLocal as! [Any], "", "All")
                    
                }
                
            }
            
        }
        
    }
    
    func getServiceAutoDynamicForm(arrayOfWOIdToFetchDynamicForm : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strWOIdToFetchDynamicForm = arrayOfWOIdToFetchDynamicForm.componentsJoined(by: ",")
        
        let strURL = strServiceAutoUrlMain + UrlServiceDynamicFormAllWorkOrders + strCompanyKey + UrlServiceDynamicFormAllWorkOrdersWorkOrderID + strWOIdToFetchDynamicForm
        
        print("Service Auto Dynamic Form Get API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Auto Dynamic Form Data From SQL Server.")
                
                print("Service Auto Dynamic Form Get API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrayOfServiceDynamicFormLocal = dictData.value(forKey: "data") as! NSArray
                        
                        if arrayOfServiceDynamicFormLocal.count > 0 {
                            
                            for k in 0 ..< arrayOfServiceDynamicFormLocal.count {
                                
                                if arrayOfServiceDynamicFormLocal[k] is NSDictionary {
                                    
                                    let dictDataLocal = arrayOfServiceDynamicFormLocal[k] as! NSDictionary
                                    
                                    self.saveServiceAutoDynamicFormToDB(dictOfDynamicFormLocal: dictDataLocal, woIdLocal: "\(dictDataLocal.value(forKey: "WorkorderId") ?? "")")
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveServiceAutoDynamicFormToDB(dictOfDynamicFormLocal : NSDictionary , woIdLocal : String) {
        
        
        // Delete and save again
        
        deleteAllRecordsFromDB(strEntity: Entity_ServiceDynamicForm, predicate: NSPredicate(format: "workOrderId = %@", woIdLocal))
        
        // save
        WebService.savingServiceAutoDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal)
        
        /*let arrayOfLeadsDynamicFormLocal = getDataFromCoreDataBaseArray(strEntity: Entity_ServiceDynamicForm, predicate: NSPredicate(format: "workOrderId = %@", woIdLocal))
         
         if arrayOfLeadsDynamicFormLocal.count > 0 {
         
         // Save in DB
         
         WebService.savingServiceAutoDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal)
         
         }else{
         
         // Update in DB
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()
         
         arrOfKeys.add("arrFinalInspection")
         arrOfKeys.add("isSentToServer")
         
         arrOfValues.add(dictOfDynamicFormLocal)
         arrOfValues.add("Yes")
         
         
         let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_ServiceDynamicForm, predicate: NSPredicate(format: "workOrderId = %@", woIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         
         if isSuccess {
         
         // Saved in DB
         
         } else {
         
         // Delete and save again
         
         deleteAllRecordsFromDB(strEntity: Entity_ServiceDynamicForm, predicate: NSPredicate(format: "workOrderId = %@", woIdLocal))
         
         // save
         WebService.savingServiceAutoDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal)
         
         }
         
         }*/
        
    }
    
    func getServiceAutoEquipmentDynamicForm(arrayOfWOIdToFetchDynamicForm : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strWOIdToFetchDynamicForm = arrayOfWOIdToFetchDynamicForm.componentsJoined(by: ",")
        
        let strURL = strServiceAutoUrlMain + UrlEquipmentsDynamicFormAllWorkOrders + strCompanyKey + UrlServiceDynamicFormAllWorkOrdersWorkOrderID + strWOIdToFetchDynamicForm
        
        print("Service Equipments Auto Dynamic Form Get API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Equipments Auto Dynamic Form Data From SQL Server.")
                
                print("Service Auto Equipments Dynamic Form Get API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrayOfServiceEquipmentDynamicFormLocal = dictData.value(forKey: "data") as! NSArray
                        
                        if arrayOfServiceEquipmentDynamicFormLocal.count > 0 {
                            
                            for k in 0 ..< arrayOfServiceEquipmentDynamicFormLocal.count {
                                
                                if arrayOfServiceEquipmentDynamicFormLocal[k] is NSDictionary {
                                    
                                    let dictDataLocal = arrayOfServiceEquipmentDynamicFormLocal[k] as! NSDictionary
                                    
                                    var strType = "woEquipId"
                                    var idLocal = "\(dictDataLocal.value(forKey: "WOEquipmentId") ?? "")"
                                    
                                    if idLocal.count == 0 {
                                        
                                        idLocal = "\(dictDataLocal.value(forKey: "MobileUniqueId") ?? "")"
                                        strType = "mobileEquipId"
                                        
                                    }
                                    
                                    self.saveServiceAutoEquipmentDynamicFormToDB(dictOfDynamicFormLocal: dictDataLocal, woIdLocal: "\(dictDataLocal.value(forKey: "WorkorderId") ?? "")", strEquipmentId: idLocal, strEquipmentIdType: strType)
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func getServiceAutoDeviceDynamicForm(arrayOfWOIdToFetchDynamicForm : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strWOIdToFetchDynamicForm = arrayOfWOIdToFetchDynamicForm.componentsJoined(by: ",")
        
        let strURL = strServiceAutoUrlMain + UrlDeviceDynamicFormAllWorkOrders + strCompanyKey + UrlServiceDynamicFormAllWorkOrdersWorkOrderID + strWOIdToFetchDynamicForm
        
        print("Service Device Dynamic Form Get API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Device Dynamic Form Data From SQL Server.")
                
                print("Service Auto Device Dynamic Form Get API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        
                        if dictData.value(forKey: "data") is NSArray {
                            
                            let arrayOfServiceDeviceDynamicFormLocal = dictData.value(forKey: "data") as! NSArray
                            
                            if arrayOfServiceDeviceDynamicFormLocal.count > 0 {
                                
                                WebService().savingDeviceDynamicAllDataDB(arrOfDeviceDynamicFormData: arrayOfServiceDeviceDynamicFormLocal)
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveServiceAutoEquipmentDynamicFormToDB(dictOfDynamicFormLocal : NSDictionary , woIdLocal : String , strEquipmentId : String , strEquipmentIdType : String) {
        
        // Delete and save again
        
        deleteAllRecordsFromDB(strEntity: Entity_EquipmentDynamicForm, predicate: NSPredicate(format: "workorderId = %@ AND %@ = %@", woIdLocal , strEquipmentId , strEquipmentIdType))
        
        // save
        WebService.savingServiceAutoEquipmentDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal, strEquipmentId: strEquipmentId, strEquipmentIdType: strEquipmentIdType)
        
        /*let arrayOfLeadsDynamicFormLocal = getDataFromCoreDataBaseArray(strEntity: Entity_EquipmentDynamicForm, predicate: NSPredicate(format: "workorderId = %@ AND %@ = %@", woIdLocal , strEquipmentId , strEquipmentIdType))
         
         if arrayOfLeadsDynamicFormLocal.count > 0 {
         
         // Save in DB
         
         WebService.savingServiceAutoEquipmentDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal, strEquipmentId: strEquipmentId, strEquipmentIdType: strEquipmentIdType)
         
         }else{
         
         // Update in DB
         
         let arrOfKeys = NSMutableArray()
         let arrOfValues = NSMutableArray()
         
         arrOfKeys.add("arrFinalInspection")
         arrOfKeys.add("isSentToServer")
         
         arrOfValues.add(dictOfDynamicFormLocal)
         arrOfValues.add("Yes")
         
         
         let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_EquipmentDynamicForm, predicate: NSPredicate(format: "workorderId = %@ AND %@ = %@", woIdLocal , strEquipmentId , strEquipmentIdType), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         
         if isSuccess {
         
         // Saved in DB
         
         } else {
         
         // Delete and save again
         
         deleteAllRecordsFromDB(strEntity: Entity_EquipmentDynamicForm, predicate: NSPredicate(format: "workorderId = %@ AND %@ = %@", woIdLocal , strEquipmentId , strEquipmentIdType))
         
         // save
         WebService.savingServiceAutoEquipmentDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, woIdLocal: woIdLocal, strEquipmentId: strEquipmentId, strEquipmentIdType: strEquipmentIdType)
         
         }
         
         }*/
        
    }
    
    func checkAfterStopingWos() {
        
        let objTempSubWoMain = objSubWosForForcefullyStopWos
        
        let strSubWoStatusLocal = strSubWoStatusForForcefullyStopWos
        
        if strSubWoStatusLocal == "New" || strSubWoStatusLocal == "Not Started" || strSubWoStatusLocal == "NotStarted" {
            
            if Global().isNetReachable() {
                
                self.metodUpdateSubWorkOrderStatus(strStatusToSent: "New", strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                
            }else{
                
                SalesCoreDataVC().addActualHrsIfResponseIsNil("\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                
            }
            
            SalesCoreDataVC().upDateSubWorkOrderStatus(fromDB: "Dispatch", "\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
            
            //fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate
            Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
            
        }else if strSubWoStatusLocal == "Dispatch" {
            
            if Global().isNetReachable() {
                
                self.metodUpdateSubWorkOrderStatus(strStatusToSent: "Dispatch", strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                
            }else{
                
                SalesCoreDataVC().addActualHrsIfResponseIsNil("\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
                
            }
            
            SalesCoreDataVC().upDateSubWorkOrderStatus(fromDB: "OnRoute", "\(objTempSubWoMain.value(forKey: "workorderId")!)", "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)")
            
            //fetchWorkOrderFromDataBaseForMechanicalToUpdateModifydate
            Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
            
        }else if strSubWoStatusLocal == "OnRoute" {
            
            self.fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute(strWoId: "\(objTempSubWoMain.value(forKey: "workorderId")!)", strSubWoId: "\(objTempSubWoMain.value(forKey: "subWorkOrderId")!)", objSubWoTemp: objTempSubWoMain)
            
            Global().fetchWorkOrderFromDataBaseForMechanical(toUpdateModifydate: "\(objTempSubWoMain.value(forKey: "workorderId")!)")
            
        }else {
            
            
        }
        
    }
    
    
    
    // MARK: -------------------- New Pest Flow Ruchika Funciton ----------------------------------
    
    func getArrOfWorkorderIdForIsRegularPestFlow(){

        var arrOfWorkOrderIds = [String]()

        if arrayAppointment.count > 0 {

            for i in 0..<arrayAppointment.count{

                let dictDataSalesDB = arrayAppointment[i] as! NSManagedObject

                if dictDataSalesDB.isKind(of: WorkOrderDetailsService.self) {

                    let strIsRegularPestFlow = "\(dictDataSalesDB.value(forKey: "isRegularPestFlow")!)"

                    let strIsRegularPestFlowOnBranch = "\(dictDataSalesDB.value(forKey: "isRegularPestFlowOnBranch")!)"

                    if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {

                        let strWoNo = "\(dictDataSalesDB.value(forKey: "workOrderNo") ?? "")"

                        arrOfWorkOrderIds.append(strWoNo)

                    }

                }

            }

        }

        if arrOfWorkOrderIds.count > 0 {

            DispatchQueue.global(qos: .background).async {

                self.callWebServiceToGetNonPrefferableDataAndTime(arrOfWorkorderId: arrOfWorkOrderIds)

            }

        }

    }
    
    // MARK: -------------------- New Pest Flow Ruchika Funciton End ----------------------------------
    
    
    func uploadImageService(strImageName : String , strUrll : String , strType : String , dictDataTemp : NSDictionary , strIddd : String) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            self.dispatchGroupServiceAuto.enter()
            
            //let imagee = getImagefromDirectory(strname: strImageName)
            
            //let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            let dataToSend = GetDataFromDocumentDirectory(strFileName: strImageName)

            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                
                self.dispatchGroupServiceAuto.leave()
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        if strType == "serviceAddressDocNameSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let mobileServiceAddressDocId = "\(dictDataTemp.value(forKey: "mobileServiceAddressDocId") ?? "")"
                            
                            if mobileServiceAddressDocId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",strIddd,mobileServiceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            } else {
                                
                                let serviceAddressDocId = "\(dictDataTemp.value(forKey: "serviceAddressDocId") ?? "")"
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",strIddd,serviceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else if strType == "documentPathSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let sAConditionDocumentId = "\(dictDataTemp.value(forKey: "sAConditionDocumentId") ?? "")"
                            
                            if sAConditionDocumentId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",strIddd,sAConditionDocumentId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else {
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    // MARK: --------------------------- Plumbing Functions ---------------------------
    
    // 03 August Service Basic Api Implementation

    func getPlumbingAppointmentsV1(arrOfWorkOrderIdLocal : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersMechanical_SPV1

        print("Plumbing Get Work order API called --- %@",strURL)

        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyKey",
                "EmployeeNo",
                "WorkOrderIds"]
        
        values = [strCompanyKey,
                  strEmpNo,
                  arrOfWorkOrderIdLocal]
        
        let dictReqObjToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
        
        WebService.postDataWithHeadersToServer(dictJson: dictReqObjToSend, url: strURL, strType: "AppointmentPostBasicInfoGet") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Plumbing Auto Appointments API Data From SQL Server.")
                
                print("Plumbing Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictServiceWoResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        self.savePlumbingWorkOrdeDb(dictOfServiceWoData: dictServiceWoResponseLocal)
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    func getPlumbingAppointmentsWithBasicInfo()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersMechanical_SPBasicInfo + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Plumbing Get Leads Basic Info API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "PlumbingGetBasicInfo_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Plumbing Basic Info Appointments API Data From SQL Server.")
                
                print("Plumbing Get Basic Info Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                                                
                        let arrayWorkOrderResponseLocal = response.value(forKey: "data") as! NSArray
                        
                        if arrayWorkOrderResponseLocal.count > 0 {
                            
                            self.fetchPlumbingWorkOrderFromDBToCheck(arrayOfWorkOrders: arrayWorkOrderResponseLocal)
                            
                        }
                    }
                }
            }else{
                
                // delete extra WO if exists from Db
                
                let arrayOfWorkOrdersLocal = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "userName == %@", self.strUserName))

                // Check which to send and which to update directly
                
                let arrTemp = NSArray()
                
                self.arrayOfPlumbingWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendPlumbingAppointment(toServerNew: arrTemp as! [Any], arrayOfWorkOrdersLocal as! [Any])
                                
                // Delete Work Order Data Before Saving
                
                self.deletPlumbingWoFromDB()
                
            }
        }
    }
    
    func getPlumbingAppointments()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strURL = strServiceAutoUrlMain + UrlGetTotalWorkOrdersMechanical_SP + strCompanyKey + "&EmployeeNo=" + strEmpNo
        
        print("Plumbing Get Work order API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "PlumbingGetWO_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Plumbing Auto Appointments API Data From SQL Server.")
                
                print("Plumbing Get Appointments API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSDictionary {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let dictServiceWoResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                        
                        self.fetchPlumbingWoFromDB(dictOfServiceWoData: dictServiceWoResponseLocal)
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    // 03 August Service Basic Api Implementation

    func fetchPlumbingWorkOrderFromDBToCheck(arrayOfWorkOrders : NSArray) {
        
        let arrayOfWorkOrdersLocal = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "userName == %@ && departmentType == %@", strUserName, "Mechanical"))

        // Check which to send and which to update directly
        
        arrayOfPlumbingWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendPlumbingAppointment(toServerNew: arrayOfWorkOrders as! [Any], arrayOfWorkOrdersLocal as! [Any])
                                                                                                
        
        // Delete Work Orders Data Before Saving
        
        deletPlumbingWoFromDB()
        
        let arrOfWorkOrdersToFetchFromServer = nsud.value(forKey: "arrOfWorkOrdersToFetchFromServer") as! NSArray
                        
        if arrOfWorkOrdersToFetchFromServer.count > 0 {
            
            self.getPlumbingAppointmentsV1(arrOfWorkOrderIdLocal: arrOfWorkOrdersToFetchFromServer)
            
        }
        
        // send to server if
        
        let arrofWorkOrdersToSendToServer = nsud.value(forKey: "sendServiceLeadToServerMechanical") as! NSArray
        
        if arrofWorkOrdersToSendToServer.count > 0 {
            
            indexDynamicFormToSendServiceAuto = 0
            // Sending Work Order and DYnamic form to server
            self.sendPlumbingDataToServer(indexx: 0)
            
        }
        
        // Download Dynamic Form From Server
        
        if arrOfWorkOrdersToFetchFromServer.count > 0 {
            
            let arrTemp = SalesCoreDataVC().fetchSubWorkOrder(fromDB: arrOfWorkOrdersToFetchFromServer as! [Any])
            
            if arrTemp.count > 0 {
                
                // fetch Service Dynmaic forms
                self.getPlumbingDynamicForm(arrayOfSubWOIdToFetchDynamicForm: arrTemp)
                
            }
            
        }
        
    }
    
    func savePlumbingWorkOrdeDb(dictOfServiceWoData : NSDictionary) {
        
        let arrayOfWoIdToFetchDynamicForm = NSMutableArray()
        
        if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
            
            let arrWoComingFromServer = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                for k in 0 ..< arrWoComingFromServer.count {
                    
                    if arrWoComingFromServer[k] is NSDictionary {
                        
                        let dictServiceLocal = arrWoComingFromServer[k] as! NSDictionary
                        
                        if dictServiceLocal.count > 0 {
                            
                            if dictServiceLocal.value(forKey: "WorkorderDetail") is NSDictionary {
                                
                                let woDetailLocal = dictServiceLocal.value(forKey: "WorkorderDetail") as! NSDictionary
                                
                                if "\(woDetailLocal.value(forKey: "DepartmentType") ?? "")" == "Mechanical" {
                                    
                                    arrayOfWoIdToFetchDynamicForm.add("\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")
                                    
                                    deletePlumbingWoById(leadIdLocal: "\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")

                                }
                            }
                        }
                    }
                }
            }
        }
        
        savePlumbingToDB(dictOfServiceWoData: dictOfServiceWoData)

        if arrayOfWoIdToFetchDynamicForm.count > 0 {
            
            let arrTemp = SalesCoreDataVC().fetchSubWorkOrder(fromDB: arrayOfWoIdToFetchDynamicForm as! [Any])
            
            if arrTemp.count > 0 {
                
                // fetch Service Dynmaic forms
                self.getPlumbingDynamicForm(arrayOfSubWOIdToFetchDynamicForm: arrTemp)
                
            }
            
        }
    }
    
    func fetchPlumbingWoFromDB(dictOfServiceWoData : NSDictionary) {
        
        let arrayOfWoLocal = getDataFromCoreDataBaseArray(strEntity: Entity_PlumbingWorkOrderDetailsService, predicate: NSPredicate(format: "userName == %@ && departmentType == %@", strUserName, "Mechanical"))
        
        if arrayOfWoLocal.count == 0 {
            
            // No data to compare direct save to DB and fetch Dynamic Form Data
            
            savePlumbingToDB(dictOfServiceWoData: dictOfServiceWoData)
            
            let arrayOfWoIdToFetchDynamicForm = NSMutableArray()
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                let arrWoComingFromServer = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                
                if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                    
                    
                    for k in 0 ..< arrWoComingFromServer.count {
                        
                        if arrWoComingFromServer[k] is NSDictionary {
                            
                            let dictServiceLocal = arrWoComingFromServer[k] as! NSDictionary
                            
                            if dictServiceLocal.count > 0 {
                                
                                if dictServiceLocal.value(forKey: "WorkorderDetail") is NSDictionary {
                                    
                                    let woDetailLocal = dictServiceLocal.value(forKey: "WorkorderDetail") as! NSDictionary
                                    
                                    if "\(woDetailLocal.value(forKey: "DepartmentType") ?? "")" == "Mechanical" {
                                        
                                        arrayOfWoIdToFetchDynamicForm.add("\(woDetailLocal.value(forKey: "WorkorderId") ?? "")")
                                        
                                    }
                                }
                            }
                        }
                    }
                    
                    if arrayOfWoIdToFetchDynamicForm.count > 0 {
                        
                        let arrTemp = SalesCoreDataVC().fetchSubWorkOrder(fromDB: arrayOfWoIdToFetchDynamicForm as! [Any])
                        
                        if arrTemp.count > 0 {
                            
                            // fetch Service Dynmaic forms
                            self.getPlumbingDynamicForm(arrayOfSubWOIdToFetchDynamicForm: arrTemp)
                            
                        }
                        
                    }
                    
                }
            }
            
            
        } else {
            
            arrayOfPlumbingWoToFetchDynamicFormData = SalesCoreDataVC().checkifToSendPlumbingAppointment(toServer: dictOfServiceWoData as! [AnyHashable : Any], arrayOfWoLocal as! [Any])
            
            // Delete WO Data Before Saving
            
            deletPlumbingWoFromDB()
            
            let arrofWoIdsLocal = nsud.value(forKey: "saveServiceLeadToLocalDbnDeleteMechanical") as! NSArray
            let arrofWoIdsLocal1 = nsud.value(forKey: "saveTolocalDbAnyHowServiceMechanical") as! NSArray
            
            let arrofWoIdsAll = arrofWoIdsLocal + arrofWoIdsLocal1 as! NSMutableArray
            
            for k1 in 0 ..< arrofWoIdsAll.count {
                
                if dictOfServiceWoData.count > 0 {
                    
                    if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                        
                        let arrayOfWoDataLocal = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                        
                        if arrayOfWoDataLocal.count > 0 {
                            
                            // Wo delete before saving
                            
                            self.deletePlumbingWoById(leadIdLocal: "\(arrofWoIdsAll[k1])")
                            
                            SalesCoreDataVC().save(toCoreDataPlumbingWo: arrayOfWoDataLocal as! [Any], "\(arrofWoIdsAll[k1])", "Single")
                            
                        }
                        
                    }
                    
                }
            }
            
            // send to server if
            
            let arrofWoIdsToSendToServer = nsud.value(forKey: "sendServiceLeadToServerMechanical") as! NSArray
            
            if arrofWoIdsToSendToServer.count > 0 {
                // Sending lead and DYnamic form to server
                self.sendPlumbingDataToServer(indexx: 0)
            }
            
            // Download Dynamic Form From Server
            
            if arrofWoIdsAll.count > 0 {
                
                // fetch Service Dynmaic forms
                
                if arrofWoIdsAll.count > 0 {
                    
                    let arrTemp = SalesCoreDataVC().fetchSubWorkOrder(fromDB: arrofWoIdsAll as! [Any])
                    
                    if arrTemp.count > 0 {
                        
                        // fetch Service Dynmaic forms
                        self.getPlumbingDynamicForm(arrayOfSubWOIdToFetchDynamicForm: arrTemp)
                        
                    }
                }
            }
        }
    }
    
    func savePlumbingToDB(dictOfServiceWoData : NSDictionary) {
        
        if dictOfServiceWoData.count > 0 {
            
            if dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") is NSArray {
                
                let arrayOfWoDataLocal = dictOfServiceWoData.value(forKey: "WorkOrderExtSerDcs") as! NSArray
                
                if arrayOfWoDataLocal.count > 0 {
                    
                    SalesCoreDataVC().save(toCoreDataPlumbingWo: arrayOfWoDataLocal as! [Any], "", "All")
                    
                }
                
            }
            
        }
        
    }
    
    func getPlumbingDynamicForm(arrayOfSubWOIdToFetchDynamicForm : NSArray)  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let strSubWOIdToFetchDynamicForm = arrayOfSubWOIdToFetchDynamicForm.componentsJoined(by: ",")
        
        let strURL = strServiceAutoUrlMain + UrlGetDynamicFormMechanical + strCompanyKey + UrlMechanicalDynamicFormAllSubWorkOrderId + strSubWOIdToFetchDynamicForm
        
        print("Plumbing Dynamic Form Get API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "BlockTime_AppointmentVC") { (response, status) in
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
                
            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Service Auto Dynamic Form Data From SQL Server.")
                
                print("Plumbing Dynamic Form Get API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSArray {
                        
                        let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                        
                        let arrayOfServiceDynamicFormLocal = dictData.value(forKey: "data") as! NSArray
                        
                        if arrayOfServiceDynamicFormLocal.count > 0 {
                            
                            for k in 0 ..< arrayOfServiceDynamicFormLocal.count {
                                
                                if arrayOfServiceDynamicFormLocal[k] is NSDictionary {
                                    
                                    let dictDataLocal = arrayOfServiceDynamicFormLocal[k] as! NSDictionary
                                    
                                    self.savePlumbingDynamicFormToDB(dictOfDynamicFormLocal: dictDataLocal, subWoIdLocal: "\(dictDataLocal.value(forKey: "SubWorkOrderId") ?? "")")
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func savePlumbingDynamicFormToDB(dictOfDynamicFormLocal : NSDictionary , subWoIdLocal : String) {
        
        // Delete and save again
        
        deleteAllRecordsFromDB(strEntity: Entity_PlumbingDynamicForm, predicate: NSPredicate(format: "subWorkOrderId = %@", subWoIdLocal))
        
        // save
        WebService.savingPlumbingDynamicFormDataToDB(dictDynamicFormLocal: dictOfDynamicFormLocal, subWoIdLocal: subWoIdLocal)
        
    }
    
    func deletPlumbingWoFromDB() {
        
        //let arrofLeadIdsToDelete = nsud.value(forKey: "saveServiceLeadToLocalDbnDeleteMechanical") as! NSArray
        
        let arrofLeadIdsToDeleteExtra = nsud.value(forKey: "DeleteExtraWorkOrderFromDBServiceMechanical") as! NSArray
        
        let arrOldLeadToDelete = SalesCoreDataVC().fetchOldWOToDeleteFromMobileServiceFlow() as NSArray
        
        let mixedArrayToDelete = arrofLeadIdsToDeleteExtra + arrOldLeadToDelete
        
        for k1 in 0 ..< mixedArrayToDelete.count {
            
            let leadIdLocal = "\(mixedArrayToDelete[k1])"
            
            deleteAllRecordsFromDB(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WorkOrderEquipmentDetails", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ImageDetailsTermite", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WorkorderDetailChemicalListService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "TexasTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "FloridaTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrder", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWOIssueRepairDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWOIssueRepairPartDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderActualHoursDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderIssueDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderNoteDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWOTechHelperDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechannicalSubWOIssueRepairLaborDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalWoEquipment", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "ServiceAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "BillingAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SubWoEmployeeWorkingTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SubWOCompleteTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WoOtherDocExtSerDcs", predicate: NSPredicate(format: "workOrderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "SubWOCompleteTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "AccountDiscountExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "WorkOrderAppliedDiscountExtSerDcs", predicate: NSPredicate(format: "workOrderId = %@", leadIdLocal))
            
            deleteAllRecordsFromDB(strEntity: "MechanicalSubWOPaymentDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
            
        }
        
    }
    
    func deletePlumbingWoById(leadIdLocal : String) {
        
        deleteAllRecordsFromDB(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WorkOrderEquipmentDetails", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ImageDetailsTermite", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WorkorderDetailChemicalListService", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "TexasTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "FloridaTermiteServiceDetail", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrder", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWOIssueRepairDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWOIssueRepairPartDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderActualHoursDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderIssueDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWorkOrderNoteDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWOTechHelperDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechannicalSubWOIssueRepairLaborDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalWoEquipment", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "ServiceAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "BillingAddressPOCDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SubWoEmployeeWorkingTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SubWOCompleteTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WoOtherDocExtSerDcs", predicate: NSPredicate(format: "workOrderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "SubWOCompleteTimeExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "AccountDiscountExtSerDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "WorkOrderAppliedDiscountExtSerDcs", predicate: NSPredicate(format: "workOrderId = %@", leadIdLocal))
        
        deleteAllRecordsFromDB(strEntity: "MechanicalSubWOPaymentDetailDcs", predicate: NSPredicate(format: "workorderId = %@", leadIdLocal))
    }
    
    func sendPlumbingDataToServer(indexx : Int) {
        
        self.dispatchGroupPlumbing = DispatchGroup()
        self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        
        let arrofWoIdsToSendToServer = nsud.value(forKey: "sendServiceLeadToServerMechanical") as! NSArray
        
        if arrofWoIdsToSendToServer.count <= indexx {
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            // All Sent To server
            
        }else{
            
            
            // check if servcie dynamci available then upload it
            
            let arrTemp = SalesCoreDataVC().fetchSubWorkOrder(fromDB: arrofWoIdsToSendToServer as! [Any])
            
            if arrTemp.count > 0 {
                
                for k1 in 0 ..< arrTemp.count {
                    
                    let arrayOfServiceDynamicInspection = getDataFromCoreDataBaseArray(strEntity: "MechanicalDynamicForm", predicate: NSPredicate(format: "subWorkOrderId = %@","\(arrTemp[k1])"))
                    
                    if arrayOfServiceDynamicInspection.count > 0 {
                        
                        var dictTemp = NSDictionary()
                        var arrTemp = NSArray()
                        
                        let objServiceInspectionData = arrayOfServiceDynamicInspection[0] as! NSManagedObject
                        
                        if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                            
                            arrTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSArray
                            
                            if arrTemp.count > 0 {
                                
                                if arrTemp[0] is NSDictionary {
                                    
                                    dictTemp = arrTemp[0] as! NSDictionary
                                    
                                }
                                
                            }
                            
                        } else if objServiceInspectionData.value(forKey: "arrFinalInspection") is NSDictionary {
                            
                            dictTemp = objServiceInspectionData.value(forKey: "arrFinalInspection") as! NSDictionary
                            
                        } else {
                            
                        }
                        
                        if dictTemp.count > 0 {
                            
                            // Uploading API Here
                            
                            self.sendingPlumbingDynamicDataToServer(dictToSend: dictTemp, strSubWoIdToSend: "\(arrTemp[k1])")
                            
                        }
                        
                    }
                }
            }
            
            
            //Ftech Other
            
            // Fetch data to upload images audio n documents n all
            
            let dictDataTemp = SalesCoreDataVC().fetchPlumbingData(fromDB: Global().modifyDateService(), "\(arrofWoIdsToSendToServer[indexx])")
            
            // Images
            
            let arrOfAllImagesToSendToServerServcie = dictDataTemp.value(forKey: "arrOfAllImagesToSendToServer") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllImagesToSendToServerServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImagePlumbing(strImageName: "\(arrOfAllImagesToSendToServerServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            // signature Images
            
            let arrOfAllSignatureImagesToSendToServerServcie = dictDataTemp.value(forKey: "arrOfAllSignatureImagesToSendToServer") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllSignatureImagesToSendToServerServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImagePlumbing(strImageName: "\(arrOfAllSignatureImagesToSendToServerServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            // check Images
            
            let arrOfAllCheckImageToSendServcie = dictDataTemp.value(forKey: "arrOfAllCheckImageToSend") as! NSMutableArray
            
            for k1 in 0 ..< arrOfAllCheckImageToSendServcie.count {
                
                let strURL = strServiceAutoUrlMain + UrlSalesImageUpload
                
                self.uploadImagePlumbing(strImageName: "\(arrOfAllCheckImageToSendServcie[k1])", strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            // AUdio Upload
            
            let stAudioSales = "\(dictDataTemp.value(forKey: "strAudioNameGlobal") ?? "")"
            
            if stAudioSales.count > 0 {
                
                let strURL = strServiceAutoUrlMain + UrlAudioUploadAsync
                
                let strName = SalesCoreDataVC().strAudioName(stAudioSales)
                
                self.uploadAudioSales(strAudioName: strName, strUrll: strURL)
                
            }
            
            // Upload Service Address Image
            
            let serviceAddressImagePath = "\(dictDataTemp.value(forKey: "serviceAddressImagePath") ?? "")"
            
            if serviceAddressImagePath.count > 0 {
                
                let strURL = strServiceAddressImageUrl + "api/File/UploadCustomerAddressImageAsync"
                
                self.uploadImagePlumbing(strImageName: serviceAddressImagePath, strUrll: strURL, strType: "", dictDataTemp: NSDictionary(), strIddd: "")
                
            }
            
            let dictFinal = dictDataTemp.value(forKey: "dictFinal") as! NSMutableDictionary
            
            self.dispatchGroupPlumbing.notify(queue: DispatchQueue.main, execute: {
                DispatchQueue.main.async {
                    // Reload Table
                    
                    // Send FInal Service json To server
                    
                    self.sendingPlumbingWoDataToServer(dictToSend: dictFinal, strWoIdToSend: "\(arrofWoIdsToSendToServer[indexx])")
                    
                }
            })
            
        }
    }
    
    
    func sendingPlumbingDynamicDataToServer(dictToSend : NSDictionary , strSubWoIdToSend : String) {
        
        self.dispatchGroupPlumbing.enter()
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = strServiceAutoUrlMain + UrlMechanicalDynamicFormSubmission
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "string"){ (responce, status) in
            
            self.dispatchGroupPlumbing.leave()
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    debugPrint(responce)
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isSentToServer")
                    arrOfValues.add("Yes")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "MechanicalDynamicForm", predicate: NSPredicate(format: "subWorkOrderId = %@",strSubWoIdToSend), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func sendingPlumbingWoDataToServer(dictToSend : NSDictionary , strWoIdToSend : String) {
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strUrl = strServiceAutoUrlMain + UrlUpdateMechanicalWorkOrder + "?EmployeeEmail=" + strEmpEmail
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
            
            self.dispatchGroup.leave(); print("Dispatch Group Main Left")
            
            if(status)
            {
                
                debugPrint(responce)
                
                if responce.value(forKey: "Success") as! NSString == "true"
                {
                    
                    //let value = responce.value(forKey: "data")
                    
                    debugPrint(responce)
                    
                    // Updating in DB that dyanmic form is synced
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("zSync")
                    arrOfValues.add("no")
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId=%@",strWoIdToSend), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func metodUpdateSubWorkOrderStatus(strStatusToSent : String , strWoId : String , strSubWoId : String)  {
        
        // Start Loader
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: false, completion: nil)
        
        var strStatusToSend = ""
        
        if strStatusToSent == "New" {
            
            strStatusToSend = "Dispatch"
            
        } else if strStatusToSent == "Dispatch" {
            
            strStatusToSend = "OnRoute"
            
        }
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let coordinate = Global().getLocation()
        
        let strURL = strServiceAutoUrlMain + UrlMechanicalSubWorkOrderStatusUpdate + strCompanyKey + "&subWorkOrderId=" + strSubWoId + "&userName=" + strUserName + "&status=" + strStatusToSend + "&mobileTimeIn=" + Global().strCurrentDateFormattedForMechanical() + "&Latitude=" + "\(coordinate.latitude)" + "&Longitude=" + "\(coordinate.longitude)"
        
        print("metodUpdateSubWorkOrderStatus API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "metodUpdateSubWorkOrderStatus_AppointmentVC") { (response, status) in
            
            loaderLocal.dismiss(animated: false) {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Service Auto Appointments API Data From SQL Server.")
                    
                    print("metodUpdateSubWorkOrderStatus API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if strStatusToSent == "New" {
                            
                            //New Matlb Dispatch Ka Response
                            
                        }else if strStatusToSent == "Dispatch" {
                            //Dispatch Matlb OnRoute Ka Response
                            
                            if response.value(forKey: "data") is NSDictionary {
                                
                                // addStartActualHoursFromMobileToDB
                                
                                SalesCoreDataVC().addStartActualHoursFromMobile(toDB: response.value(forKey: "data") as! [AnyHashable : Any], strWoId)
                                
                            }else{
                                
                                // Response nil hai to addActualHrsIfResponseIsNil
                                
                                SalesCoreDataVC().addActualHrsIfResponseIsNil(strWoId, strSubWoId)
                                
                            }
                        }
                        
                        self.tblViewAppointment.reloadData()
                        let ipath = IndexPath(row: self.selectedIndexToUpdateStatus, section: 0)
                        self.tblViewAppointment.scrollToRow(at: ipath, at: .top, animated: false)
                        
                    }
                }
                
            }
            
        }
    }
    
    func fetchSubWorkOrderActualHrsFromDataBaseForMechanicalToUpdateTimeOutForOnRoute(strWoId : String , strSubWoId : String , objSubWoTemp : NSManagedObject) {
        
        let arrTemp = getDataFromCoreDataBaseArray(strEntity: "MechanicalSubWorkOrderActualHoursDcs", predicate: (NSPredicate(format: "workorderId == %@ AND subWorkOrderId == %@", strWoId, strSubWoId)))
        
        if(arrTemp.count != 0){
            
            for k1 in 0 ..< arrTemp.count {
                
                let objTemp = arrTemp[k1] as! NSManagedObject
                
                let strStatus = "\(objTemp.value(forKey: "status")!)"
                
                if strStatus == "OnRoute" {
                    
                    let timeOutt = Global().strCurrentDateFormattedForMechanical()
                    
                    objTemp.setValue(timeOutt, forKey: "timeOut")
                    objTemp.setValue(ReasonOnRoute, forKey: "reason")
                    
                    objSubWoTemp.setValue("Inspection", forKey: "subWOStatus")
                    objSubWoTemp.setValue("Start", forKey: "clockStatus")
                    
                    let context = getContext()
                    do {
                        try context.save()
                    } catch _ as NSError  {
                        
                    } catch {
                        
                    }
                    
                    nsud.setValue("Inspection", forKey: "WoStatus")
                    nsud.synchronize()
                    
                    self.metodUpdateSubWorkOrderStatusOnRoute(strTimeOut: timeOutt!, strWoId: strWoId, strSubWoId: strSubWoId)
                    
                    break
                    
                }
                
            }
            
        }
        
    }
    
    
    func metodUpdateSubWorkOrderStatusOnRoute(strTimeOut : String , strWoId : String , strSubWoId : String)  {
        
        // Start Loader
        let loaderLocal = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderLocal, animated: false, completion: nil)
        
        let strStatusToSend = "OnRoute"
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let coordinate = Global().getLocation()
        
        let strURL = strServiceAutoUrlMain + UrlMechanicalSubWorkOrderStatusUpdateInspection + strCompanyKey + "&subWorkOrderId=" + strSubWoId + "&userName=" + strUserName + "&status=" + strStatusToSend + "&mobileTimeOut=" + Global().strCurrentDateFormattedForMechanical() + "&Latitude=" + "\(coordinate.latitude)" + "&Longitude=" + "\(coordinate.longitude)"
        
        print("metodUpdateSubWorkOrderStatusOnRoute API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "metodUpdateSubWorkOrderStatus_AppointmentVC") { (response, status) in
            
            loaderLocal.dismiss(animated: false) {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Service Auto Appointments API Data From SQL Server.")
                    
                    print("metodUpdateSubWorkOrderStatusOnRoute API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        SalesCoreDataVC().addStartActualHoursFromMobileToDBFor(onRoute: strWoId, strSubWoId)
                        
                        if (isInternetAvailable()){
                            
                            // Start Loader
                            self.loaderPlumbing = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(self.loaderPlumbing, animated: false, completion: nil)
                            
                            // SyncData
                            
                            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "MechanicalActualHrsSyncInspectionFR2"),
                                                                   object: nil,
                                                                   queue: nil,
                                                                   using:self.catchNotificationMechanicalActualHrsSync)
                            
                            SyncMechanicalViewController().syncMechanicalActualHours(strWoId, strSubWoId, "\(nsud.value(forKey: "subWOActualHourId") ?? "")", "MechanicalActualHrsSyncInspectionFR2")
                            
                        }else{
                            
                            self.tblViewAppointment.reloadData()
                            let ipath = IndexPath(row: self.selectedIndexToUpdateStatus, section: 0)
                            self.tblViewAppointment.scrollToRow(at: ipath, at: .top, animated: false)
                            
                            // goto mechanical view on clcik arrivedid
                            
                            let dictDataSalesDB = self.arrayAppointment[self.selectedIndexToUpdateStatus] as! NSManagedObject
                            
                            if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                                
                                // Service Flow
                                
                                if DeviceType.IS_IPAD {
                                    
                                    self.goToServiceFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    
                }
                
            }
            
        }
    }
    
    func catchNotificationMechanicalActualHrsSync(notification:Notification) {
        
        self.loaderPlumbing.dismiss(animated: false) {
            
            self.tblViewAppointment.reloadData()
            let ipath = IndexPath(row: self.selectedIndexToUpdateStatus, section: 0)
            self.tblViewAppointment.scrollToRow(at: ipath, at: .top, animated: false)
            
            // goto mechanical view on clcik arrivedid
            
            let dictDataSalesDB = self.arrayAppointment[self.selectedIndexToUpdateStatus] as! NSManagedObject
            
            if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                
                // Service Flow
                
                if DeviceType.IS_IPAD {
                    
                    self.goToServiceFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                    
                }
                
            }
            
        }
        
    }
    
    func catchNotificationMechanicalStopWO(notification:Notification) {
        
        self.dispatchGroupPlumbingStopWO.leave()
        
    }
    
    func uploadImagePlumbing(strImageName : String , strUrll : String , strType : String , dictDataTemp : NSDictionary , strIddd : String) {
        
        if fileAvailableAtPath(strname: strImageName) {
            
            self.dispatchGroupPlumbing.enter()
            
            let imagee = getImagefromDirectory(strname: strImageName)
            
            let dataToSend = imagee.jpegData(compressionQuality: 1.0)!
            
            WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: dataToSend, strDataName : strImageName, url: strUrll) { (responce, status) in
                
                self.dispatchGroupPlumbing.leave()
                
                if(status)
                {
                    debugPrint(responce)
                    
                    if(responce.value(forKey: "data")is NSDictionary)
                    {
                        // Handle Success Response Here
                        
                        if strType == "serviceAddressDocNameSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let mobileServiceAddressDocId = "\(dictDataTemp.value(forKey: "mobileServiceAddressDocId") ?? "")"
                            
                            if mobileServiceAddressDocId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",strIddd,mobileServiceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            } else {
                                
                                let serviceAddressDocId = "\(dictDataTemp.value(forKey: "serviceAddressDocId") ?? "")"
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "SAPestDocuments", predicate: NSPredicate(format: "workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",strIddd,serviceAddressDocId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else if strType == "documentPathSAPestDocuments" {
                            
                            // Updating in DB that Doc is synced
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("isToUpload")
                            arrOfValues.add("no")
                            
                            let sAConditionDocumentId = "\(dictDataTemp.value(forKey: "sAConditionDocumentId") ?? "")"
                            
                            if sAConditionDocumentId.count > 0 {
                                
                                let isSuccess = getDataFromDbToUpdate(strEntity: "ServiceConditionDocuments", predicate: NSPredicate(format: "workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",strIddd,sAConditionDocumentId, "\(dictDataTemp.value(forKey: "companyKey") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                
                                if isSuccess {
                                    
                                    
                                    
                                }
                                
                            }
                            
                        } else {
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    // MARK: -  ------------------------------ Message Composer Delegate ------------------------------
    
    func displayMessageInterface(strNo: String , strMessage: String)
    {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["\(strNo)"]
        composeVC.body = strMessage
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        //dismiss(animated: false, completion: nil)
        
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                
                self.updateOnMyWayStatusLifeStyle()
                
            }
            
        }
        
    }
    
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //controller.dismiss(animated: true)
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                
                self.updateOnMyWayStatusLifeStyle()
                
            }
            
        }
    }
    
    // MARK: -  ------------------------- Redirection On did select functions -----------------------
    
    func goToSalesFlow(dictDataSalesLocal : NSManagedObject) {
        
        guard nsud.value(forKey: "MasterSalesAutomation") is NSDictionary else {
            
            return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSyncSalesMaster, viewcontrol: self)
        }
        
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "leadId")!)", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "accountNo")!)", forKey: "AccountNos")
            nsud.set("\(dictDataSalesLocal.value(forKey: "leadNumber")!)", forKey: "LeadNumber")
            nsud.set("\(dictDataSalesLocal.value(forKey: "statusSysName")!)", forKey: "leadStatusSales")
            nsud.set("\(dictDataSalesLocal.value(forKey: "stageSysName")!)", forKey: "stageSysNameSales")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
             //Old Sales Flow
            /*let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfo") as? SaleAutomationGeneralInfo
            vc!.matchesGeneralInfo = dictDataSalesLocal
            self.navigationController?.pushViewController(vc!, animated: false)*/
            
            //Nilind - New Sales Flow
            
           if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Residential"
            {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
                vc!.dataGeneralInfo = dictDataSalesLocal
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            else if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Commercial"
            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMain" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfo") as? SaleAutomationGeneralInfo
//                vc!.matchesGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
                vc!.dataGeneralInfo = dictDataSalesLocal
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            
            
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
        
    }
    
    
    
    func goToSalesFlowiPad(dictDataSalesLocal : NSManagedObject) {
        
        guard nsud.value(forKey: "MasterSalesAutomation") is NSDictionary else {
            
            return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSyncSalesMaster, viewcontrol: self)
        }
        
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "leadId")!)", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "accountNo")!)", forKey: "AccountNos")
            nsud.set("\(dictDataSalesLocal.value(forKey: "leadNumber")!)", forKey: "LeadNumber")
            nsud.set("\(dictDataSalesLocal.value(forKey: "statusSysName")!)", forKey: "leadStatusSales")
            nsud.set("\(dictDataSalesLocal.value(forKey: "stageSysName")!)", forKey: "stageSysNameSales")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
            //Old Sales Flow
           /* let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfoiPad") as? SaleAutomationGeneralInfoiPad
            vc!.matchesGeneralInfo = dictDataSalesLocal
            self.navigationController?.pushViewController(vc!, animated: false)*/
            
            
            //Nilind
            
            
            
            if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Residential"
            {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
                vc!.dataGeneralInfo = dictDataSalesLocal
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            else if "\(dictDataSalesLocal.value(forKey: "flowType") ?? "")" == "Commercial"
            {
//                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "SalesMainiPad" : "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SaleAutomationGeneralInfoiPad") as? SaleAutomationGeneralInfoiPad
//                vc!.matchesGeneralInfo = dictDataSalesLocal
//                self.navigationController?.pushViewController(vc!, animated: false)
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_GeneralInfoVC") as? SalesNew_GeneralInfoVC
                vc!.dataGeneralInfo = dictDataSalesLocal
                self.navigationController?.pushViewController(vc!, animated: false)
            }
            

        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
        
    }
    
    func goToServiceFlow(dictDataSalesLocal : NSManagedObject) {
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName")!)"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType")!)"
            let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState")!)"
            let isNPMATermite = "\(dictDataSalesLocal.value(forKey: "isNPMATermite")!)"
            let strIsRegularPestFlow = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlow")!)"
            let strIsRegularPestFlowOnBranch = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlowOnBranch")!)"
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId")!)", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId")!)", forKey: "strDepartmentId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName")!)", forKey: "departmentSysNameWO")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID")!)", forKey: "SurveyID")
            
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID")!)".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
            
            if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else if departmentType == "Termite" && serviceState == "California" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlow" : "ServicePestNewFlow", bundle: Bundle.main).instantiateViewController(withIdentifier: "GenralInfoVC") as? GenralInfoVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
              //  showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            }else {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestiPhone" : "PestiPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoVC") as? GeneralInfoVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)
            
        }
        
    }
    
    func goToServiceFlowiPad(dictDataSalesLocal : NSManagedObject) {
        
        let branchSysNameLocal = "\(dictDataSalesLocal.value(forKey: "branchSysName") ?? "")"
        
        if branchSysNameLocal == strEmpLoginBranchSysName {
            
            let departmentType = "\(dictDataSalesLocal.value(forKey: "departmentType") ?? "")"
            let serviceState = "\(dictDataSalesLocal.value(forKey: "serviceState") ?? "")"
            let isNPMATermite = "\(dictDataSalesLocal.value(forKey: "isNPMATermite") ?? "")"
            let strIsRegularPestFlow = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlow") ?? "")"
            let strIsRegularPestFlowOnBranch = "\(dictDataSalesLocal.value(forKey: "isRegularPestFlowOnBranch") ?? "")"
            
            
            nsud.set("\(dictDataSalesLocal.value(forKey: "workorderId") ?? "")", forKey: "LeadId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentId") ?? "")", forKey: "strDepartmentId")
            nsud.set("\(dictDataSalesLocal.value(forKey: "departmentSysName") ?? "")", forKey: "departmentSysNameWO")
            nsud.set("\(dictDataSalesLocal.value(forKey: "surveyID") ?? "")", forKey: "SurveyID")
            if strIsGGQIntegration == "1" && "\(dictDataSalesLocal.value(forKey: "surveyID") ?? "")".count > 0 {
                
                nsud.set(true, forKey: "YesSurveyService")
                
            } else {
                
                nsud.set(false, forKey: "YesSurveyService")
                
            }
            nsud.synchronize()
            
            if departmentType == "Mechanical" {
                
                //showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
                let vc = UIStoryboard.init(name: "MechanicaliPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "MechanicalGeneralInfoViewController") as? MechanicalGeneralInfoViewController
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if isNPMATermite == "true" || isNPMATermite == "True" || isNPMATermite == "1" {
                
                let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if departmentType == "Termite" && serviceState != "California" {
                
                let vc = UIStoryboard.init(name: "ServiceiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoAppointmentViewiPad") as? GeneralInfoAppointmentViewiPad
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }else if departmentType == "Termite" && serviceState == "California" {
                if checkRedirectSignApprovalOrNot(obj: dictDataSalesLocal, checkflowStageType: true, checkflowStatus: true) {
                    if (isInternetAvailable()){
                        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalReportVC") as? WDO_PendingApprovalReportVC
                        let wdoWorkflowStatus = "\(dictDataSalesLocal.value(forKey: "wdoWorkflowStatus") ?? "")"
                        if(wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased()){
                            vc!.reportview_From =  Enum_ReportviewFrom.InspectorSignComplete
                        }else{
                            vc!.reportview_From =  Enum_ReportviewFrom.InspectorSignApproval
                        }
                        vc!.objPendingApproval = dictDataSalesLocal
                        self.navigationController?.pushViewController(vc!, animated: false)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                    }
                }else{
                    let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
                    vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }else if (strIsRegularPestFlow == "True" || strIsRegularPestFlow == "true" || strIsRegularPestFlow == "1" || strIsRegularPestFlowOnBranch == "True" || strIsRegularPestFlowOnBranch == "true" || strIsRegularPestFlowOnBranch == "1") {
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ServicePestNewFlowiPad" : "ServicePestNewFlowiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoNewPestiPad") as? GeneralInfoNewPestiPad
                vc!.strWoId = "\(dictDataSalesLocal.value(forKey: "workorderId")!)" as NSString
                self.navigationController?.pushViewController(vc!, animated: false)
              //  showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoComingSoon, viewcontrol: self)
                
            } else {
                
                let vc = UIStoryboard.init(name: "ServiceiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoAppointmentViewiPad") as? GeneralInfoAppointmentViewiPad
                self.navigationController?.pushViewController(vc!, animated: false)
                
            }
            
        }else{
            
            self.showAlertForBranchChange(strAppoinntmentBranch: branchSysNameLocal)

        }
        
    }
    
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    private func showAlertForBranchChange(strAppoinntmentBranch : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "Appointment"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    func callWebServiceToGetNonPrefferableDataAndTime(arrOfWorkorderId: [String])
    {
         let strURL =  String(format:  URL.ServiceNewPestFlow.getTheNonPreferabledateAndTime, strCompanyKey)

         let param = arrOfWorkorderId
        
         WebService.postArrayRequestWithHeadersNewRuchika(dictJson: param, url: strURL, responseStringComing: "TimeLine") { (response, status) in

             if(status)
             {
                 if response.value(forKey: "data") != nil
                 {
                     DispatchQueue.main.async {
                         
                         for i in 0..<arrOfWorkorderId.count{

                             let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkorderNonPrefrableDateAndTime", predicate: NSPredicate(format: "workOrderId == %@", arrOfWorkorderId[i]))
                             
                             if arryOfData.count > 0
                             {
                                 for j in 0..<arryOfData.count{

                                     let objData = arryOfData[j] as! NSManagedObject

                                     deleteDataFromDB(obj: objData)
                                 }
                             }
                         }
                         guard let arrOfNonPreferedDateAndTime = response.value(forKey: "data") as? [[String:Any]] else {
                             return
                         }

                         print(arrOfNonPreferedDateAndTime.count)

                         if arrOfNonPreferedDateAndTime.count > 0 {

                             for i in 0..<arrOfNonPreferedDateAndTime.count{

                                 let strNonPreferredTime = arrOfNonPreferedDateAndTime[i]["NonPreferredTime"] as? String ?? ""

                                 let strWorkOrderNo = arrOfNonPreferedDateAndTime[i]["WorkOrderNo"] as? String ?? ""

                                 guard let arrNonPreferredDays = arrOfNonPreferedDateAndTime[i]["NonPreferredDaysList"] as? [[String: Any]] else {

                                     return
                                 }

                                 var strMonValue = "" ;

                                 var strTueValue = "" ;

                                 var strWedValue = "" ;

                                 var strThurValue = "" ;

                                 var strFriValue = "" ;

                                 var strSatValue = "" ;

                                 var strSunValue = "" ;

                                 if arrNonPreferredDays.count > 0{

                                     for i in 0..<arrNonPreferredDays.count{

                                         if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Sunday".lowercased(){

                                             strSunValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Monday".lowercased(){

                                             strMonValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Tuesday".lowercased(){

                                             strTueValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Wednesday".lowercased(){

                                             strWedValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Thursday".lowercased(){

                                             strThurValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Friday".lowercased(){

                                             strFriValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                         else if (arrNonPreferredDays[i]["Text"] as! String).lowercased() == "Saturday".lowercased(){

                                             strSatValue = String(describing: arrNonPreferredDays[i]["Selected"] as? NSNumber ?? 0)

                                         }

                                     }

                                     self.saveNonPreferableDateAndTimeForWorkorder(strMonValue: strMonValue, strTueValue: strTueValue, strWedValue: strWedValue, strThurValue: strThurValue, strFriValue: strFriValue, strSatValue: strSatValue, strSunValue: strSunValue, strWorkOrderNo: strWorkOrderNo, strNonPreferredTime: strNonPreferredTime)

                                 }

                             }

                         }

                     }

                 }

             }

             else{

                 print()
             }

         }
     }
    
    func saveNonPreferableDateAndTimeForWorkorder(strMonValue :String, strTueValue:String, strWedValue:String, strThurValue:String, strFriValue :String, strSatValue :String,strSunValue :String, strWorkOrderNo: String,strNonPreferredTime: String) {
        //TechnicianComment
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isMondayPreferable")
        arrOfKeys.add("isTuesdayPreferable")
        arrOfKeys.add("isWednesdayPreferable")
        arrOfKeys.add("isThurdayPreferable")
        arrOfKeys.add("isFridayPreferable")
        arrOfKeys.add("isSaturdayPreferable")
        arrOfKeys.add("isSundayPreferable")
        arrOfKeys.add("nonPreferredTime")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("companyKey")

        arrOfValues.add(strMonValue)
        arrOfValues.add(strTueValue)
        arrOfValues.add(strWedValue)
        arrOfValues.add(strThurValue)
        arrOfValues.add(strFriValue)
        arrOfValues.add(strSatValue)
        arrOfValues.add(strSunValue)
        arrOfValues.add(strNonPreferredTime)
        arrOfValues.add(strUserName)
        arrOfValues.add(strWorkOrderNo)
        arrOfValues.add(strCompanyKey)

        saveDataInDB(strEntity: "WorkorderNonPrefrableDateAndTime", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func checkRedirectSignApprovalOrNot(obj : NSManagedObject , checkflowStageType : Bool , checkflowStatus : Bool) -> Bool {
        //---------By navin Approval check
        let departmentType = "\(obj.value(forKey: "departmentType")!)"
        let serviceState = "\(obj.value(forKey: "serviceState")!)"
        if departmentType == "Termite" && serviceState == "California" {
            
            if (DeviceType.IS_IPAD && (departmentType == "Termite" && serviceState == "California") && self.aryApprovalCheckList.count != 0)  {
                
                //---For Show status
                let wdoWorkflowStageId = "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(obj.value(forKey: "wdoWorkflowStageId") ?? "")"
                let wdoWorkflowStatus = "\(obj.value(forKey: "wdoWorkflowStatus") ?? "")"
               
                //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                if(wdoWorkflowStageId.count > 0){
                    let aryApprovalCheckListFilterObj = aryApprovalCheckList.filter { (task) -> Bool in
                        return ("\((task as! NSDictionary).value(forKey: "ApprovalCheckListId")!)".contains(wdoWorkflowStageId)  ) }
                    if aryApprovalCheckListFilterObj.count != 0 {
                        let objApprovalCheckList = aryApprovalCheckListFilterObj[0]as! NSDictionary
                        
                        let strWdoWorkflowStageType = "\(objApprovalCheckList.value(forKey: "WdoWorkflowStageType") ?? "")"
                        
                        if(checkflowStageType && checkflowStatus){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased()  && (wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased()){
                                return true
                                
                            }
                        }
                        
                        else if(checkflowStageType){
                            if strWdoWorkflowStageType.lowercased() == WdoWorkflowStageTypeEnum.Inspector.rawValue.lowercased() {
                                return true
                                
                            }
                        }
                        
                        
                    }
                }
            }
        }
        return false
    }
    
    
    // MARK: --------------------------- Delete Local Data After Sync ---------------------------
    
    func addDaysInDate(noOfDays : Int, strDate : String) -> Date  {
        
        var dateComponent = DateComponents()
        dateComponent.day = noOfDays
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: strDate, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy"))
        print(futureDate!)
        
        return futureDate ?? Date()
    }
    
    
    
    @objc func salesAutomationFetchAfterSyncingData()  {
        
        let arrLeadDelete = NSMutableArray()
        
        
        if dictLoginData.count == 0
        {
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        }
        
        strConfigFromDateSalesAuto = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.FromDate") ?? "")"
        strConfigFromDateServiceAuto = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.FromDate") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let arrOfLeads = getDataFromCoreDataBaseArray(strEntity: "LeadDetail", predicate: NSPredicate(format: "userName == %@", strUserName))//LeadDetail
        
        if arrOfLeads.count == 0
        {
            serviceAutomationFetchAfterSyncingData()
        }
        else
        {
            var dateFromConfiguration = changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: strConfigFromDateSalesAuto, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
            let strDateFromConfiguraiton = changeStringDateToStringDateInAnyTimeZoneFormat(strDate: strConfigFromDateSalesAuto, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
            dateFromConfiguration = addDaysInDate(noOfDays: -2, strDate: strDateFromConfiguraiton)
           print(dateFromConfiguration)
            
            for item in arrOfLeads
            {
                let matches =  item as! NSManagedObject
                
                var strLeadScheduleDate = "\(matches.value(forKey: "scheduleStartDate") ?? "")"
                strLeadScheduleDate = changeStringDateToStringDateInAnyTimeZoneFormat(strDate: strLeadScheduleDate, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
                let dateScheduleLead = changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: strLeadScheduleDate, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
                
                if dateFromConfiguration >= dateScheduleLead
                {

                    if "\(matches.value(forKey: "zSync") ?? "")" == "no" || "\(matches.value(forKey: "zSync") ?? "")" == "blank" || "\(matches.value(forKey: "zSync") ?? "")" == "(null)" || matches.value(forKey: "zSync") == nil
                    {
                        print("Sales workorder configuration date is greater than leadschedule date")
                        arrLeadDelete.add("\(matches.value(forKey: "leadId") ?? "") \(matches.value(forKey: "scheduleStartDate") ?? "")")
                        deleteSalesLeadById(leadIdLocal: "\(matches.value(forKey: "leadId") ?? "")" as NSString)
                    }
                   
                }
                else
                {
                    print("Sales workorder leadschedule date is greater than configuration date")
                }
            }
            
            print("Deleted Leads\(arrLeadDelete)")
            
            serviceAutomationFetchAfterSyncingData()
            
        }
        
    }
    
    @objc func serviceAutomationFetchAfterSyncingData()  {
        
        
        let arrLeadDelete = NSMutableArray()
        
        strConfigFromDateSalesAuto = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.FromDate") ?? "")"
        strConfigFromDateServiceAuto = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.FromDate") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let arrOfWoData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "userName == %@", strUserName))//LeadDetail
        
        if arrOfWoData.count == 0
        {
        }
        else
        {
            var dateFromConfiguration = changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: strConfigFromDateServiceAuto, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
            let strDateFromConfiguraiton = changeStringDateToStringDateInAnyTimeZoneFormat(strDate: strConfigFromDateServiceAuto, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
            dateFromConfiguration = addDaysInDate(noOfDays: -2, strDate: strDateFromConfiguraiton)
            print(dateFromConfiguration)
            
            for item in arrOfWoData
            {
                let matches =  item as! NSManagedObject
                
                var strLeadScheduleDate = "\(matches.value(forKey: "scheduleStartDateTime") ?? "")"
                strLeadScheduleDate = changeStringDateToStringDateInAnyTimeZoneFormat(strDate: strLeadScheduleDate, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
                let dateScheduleLead = changeStringDateToDateOnlyInAnyTimeZomeFormat(strDate: strLeadScheduleDate, strTimeZone: "UTC", strRequiredFormat: "MM/dd/yyyy")
                
                if dateFromConfiguration >= dateScheduleLead
                {
                    if "\(matches.value(forKey: "zSync") ?? "")" == "no" || "\(matches.value(forKey: "zSync") ?? "")" == "blank" || "\(matches.value(forKey: "zSync") ?? "")" == "(null)" || matches.value(forKey: "zSync") == nil
                    {
                        print("Service workorder Configuration date is greater than schedule date")
                        
                        arrLeadDelete.add("\(matches.value(forKey: "workorderId") ?? "") \(matches.value(forKey: "scheduleStartDateTime") ?? "")")
                        deleteWoById(leadIdLocal: "\(matches.value(forKey: "workorderId") ?? "")")
                        deletePlumbingWoById(leadIdLocal: "\(matches.value(forKey: "workorderId") ?? "")")
                    }
                    
                }
                else
                {
                    print("Service workorder schedule date is greater than configuration date")
                    
                }
                
            }
            
            print("Deleted Workorders \(arrLeadDelete)")
        }
        
    }

    
}


// MARK: --------------------------- Extensions ---------------------------


extension AppointmentVC:UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UItableView's delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayAppointment.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dictDataSalesDB = arrayAppointment[indexPath.row] as! NSManagedObject
        
        if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
            
            // MARK: -
            // MARK: ----------Cell For Row For Employee Block Time---------
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellBlockTimeAppointmentVC") as! CellBlockTimeAppointmentVC

            cell.lblBlockTimeTitle.text = "\(dictDataSalesDB.value(forKey: "titleBlockTime") ?? "")"
            cell.lblBlockDate.text = changeStringDateToGivenFormat(strDate: "\(dictDataSalesDB.value(forKey: "fromDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            cell.lblBlockTimeScheduleTo.text = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "toDate") ?? "")")
            cell.lblBlockTimeScheduleFrom.text = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "fromDate") ?? "")")
            if "\(dictDataSalesDB.value(forKey: "descriptionBlockTime") ?? "")".count == 0 || "\(dictDataSalesDB.value(forKey: "descriptionBlockTime") ?? "")" == ""
            {
                cell.lblBlockTimeDescriptions.text = "   "
                
            }
            else
            {
                cell.lblBlockTimeDescriptions.text = "\(dictDataSalesDB.value(forKey: "descriptionBlockTime") ?? "  ")"

            }
            
            return cell
            
        }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
            
            // MARK: -
            // MARK: ----------Cell For Row For Task---------
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew") as! LeadVCCell
            
            if let taskName = dictDataSalesDB.value(forKey: "taskName"){
                if("\(taskName)".count > 0 && "\(taskName)" != "<null>"){
                    cell.lblTaskName.text = "\(taskName)"
                }
                else{
                    cell.lblTaskName.text = ""
                }
            }
            else{
                cell.lblTaskName.text = ""
            }
            
            cell.lblCompanyName.text = ""
            var strDueDate = "", str_company_Name = "" , str_contact_Name = ""
            
            if let str_strDueDate = dictDataSalesDB.value(forKey: "dueDate"){
                if("\(str_strDueDate)".count > 0 && "\(str_strDueDate)" != "<null>" && "\(str_strDueDate)" != " "){
                    var date = ""
                    var time = ""
                    date = Global().convertDate("\(str_strDueDate)")
                    time = Global().convertTime("\(str_strDueDate)")
                    strDueDate = "\(date) \(time)"
                    cell.lblCompanyName.text = strDueDate
                }
            }
            
            if let contact_Name = dictDataSalesDB.value(forKey: "crmContactName"){
                if("\(contact_Name)".count > 0 && "\(contact_Name)" != "<null>" && "\(contact_Name)" != " "){
                    str_contact_Name = "\(contact_Name)"
                    cell.lblCompanyName.text = cell.lblCompanyName.text! + "  \u{2022}  " + str_contact_Name
                }
            }
            
            if let company_Name = dictDataSalesDB.value(forKey: "crmCompanyName"){
                if("\(company_Name)".count > 0 && "\(company_Name)" != "<null>" && "\(company_Name)" != " "){
                    str_company_Name = "\(company_Name)"
                    cell.lblCompanyName.text = cell.lblCompanyName.text! + "  \u{2022}  " + str_company_Name
                }
            }
            
            if cell.lblCompanyName.text!.suffix(5) == "  \u{2022}  " {
                cell.lblCompanyName.text = String((cell.lblCompanyName.text?.dropLast(5))!)
            }
            
            if let taskTypeId = dictDataSalesDB.value(forKey: "taskTypeId"){
                
                print(dictDataSalesDB)
                
                if("\(taskTypeId)".count > 0 && "\(taskTypeId)" != "<null>"){
                    
                    print("taskTypeId Print")
                    print(taskTypeId)
                    let strImageName = getIconNameViaTaskTypeId(strId: "\(dictDataSalesDB.value(forKey: "taskTypeId") ?? "")")
                    print("strImageName Print")
                    print(strImageName)
                    cell.imgviewgTaskType.image = UIImage(named: strImageName)
                    cell.imgviewgTaskType.isHidden = false
                    
                }
                else{
                    
                    cell.imgviewgTaskType.isHidden = false
                    
                }
            }
            
            cell.btnTaskCheckMark.tag = indexPath.row//(indexPath.section * 1000) + indexPath.row
            cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
            
            var dateDue = Global().changeDate(toLocalDateMechanical: "\(dictDataSalesDB.value(forKey: "dueDate") ?? "")")
            //let dateDue = dictData.value(forKey: "DueDate") as! Date
            
            let timeInterval = floor(Date().timeIntervalSinceReferenceDate / 86400) * 86400
            let currentdate = Date(timeIntervalSinceReferenceDate: timeInterval)
            
            if dateDue == nil {
                
                dateDue = currentdate
                
            }
            
            if("\(dictDataSalesDB.value(forKey: "status")!)" == "Done" || "\(dictDataSalesDB.value(forKey: "status")!)" == "done")
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                cell.viewContainer.backgroundColor = hexStringToUIColor(hex: "C2EAB6")
                // cell.viewVertical.backgroundColor = hexStringToUIColor(hex: "499536")
                // green color
            }
            
            else if(dateDue!.compare(currentdate) == .orderedAscending && ("\(dictDataSalesDB.value(forKey: "status")!)" == "Open" || "\(dictDataSalesDB.value(forKey: "status")!)" == "open"))
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                cell.viewContainer.backgroundColor = hexStringToUIColor(hex: "FADBDC")
                //cell.viewVertical.backgroundColor = hexStringToUIColor(hex: "EE5E60")
            }
            
            else if(dateDue!.compare(currentdate) == .orderedDescending || dateDue!.compare(currentdate) == .orderedSame && ("\(dictDataSalesDB.value(forKey: "status")!)" != "Done" || "\(dictDataSalesDB.value(forKey: "status")!)" != "done"))
            {
                cell.viewContainer.backgroundColor = UIColor.white
                //  cell.viewVertical.backgroundColor = UIColor.white
            }
            else
            {
                cell.viewContainer.backgroundColor = UIColor.white
                //  cell.viewVertical.backgroundColor = UIColor.white
            }
            
            if("\(dictDataSalesDB.value(forKey: "status")!)" == "Open" || "\(dictDataSalesDB.value(forKey: "status")!)" == "open")
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
            }
            else
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
            }
            cell.selectionStyle = .none
            
            return cell
            
        }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
            
            // MARK: -
            // MARK: ----------Cell For Row For Plumbing---------
            
            let departmentType = "\(dictDataSalesDB.value(forKey: "departmentType")!)"
            
            if departmentType == "Mechanical" {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellPlumbingAppointmentVC") as! CellPlumbingAppointmentVC
                
                cell.lblAccountName.text = "Account: \(dictDataSalesDB.value(forKey: "accountName") ?? "")"//"Account Name: \(dictDataSalesDB.value(forKey: "accountName")!)"
                
                cell.lblWoNo.text = "WO #: \(dictDataSalesDB.value(forKey: "workOrderNo")!)"
                
                var str_company_Name = "" , str_contact_Name = ""
                
                var strAccounNo = ""

                if "\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? "")".count > 0
                {
                    strAccounNo = " (\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? ""))"
                }
                else
                {
                    strAccounNo = " (\(dictDataSalesDB.value(forKey: "accountNo") ?? ""))"
                }
                
                if "\(dictDataSalesDB.value(forKey: "accountName")!)".count == 0 {
                    
                    if let contact_Name = Global().strFullName(fromCoreDB: dictDataSalesDB){
                        if("\(contact_Name)".count > 0 && "\(contact_Name)" != "<null>" && "\(contact_Name)" != " "){
                            str_contact_Name = "\(contact_Name)"
                            cell.lblAccountName.text = cell.lblAccountName.text! + str_contact_Name
                        }
                    }
                    
                    if let company_Name = dictDataSalesDB.value(forKey: "companyName"){
                        if("\(company_Name)".count > 0 && "\(company_Name)" != "<null>" && "\(company_Name)" != " "){
                            str_company_Name = "\(company_Name)"
                            cell.lblAccountName.text = cell.lblAccountName.text! + "  \u{2022}  " + str_company_Name
                        }
                    }
                    
                }
                cell.lblAccountName.text = cell.lblAccountName.text! + strAccounNo
                cell.lblStatus.text = "Service - \(dictDataSalesDB.value(forKey: "workorderStatus")!)"
                
                cell.btnContactName.tag = indexPath.row
                cell.btnContactName.setTitle(Global().strFullName(fromCoreDB: dictDataSalesDB), for: .normal)
                cell.btnContactName.addTarget(self, action: #selector(actionOnServiceAutoContactNameClick), for: .touchUpInside)
                if Global().strFullName(fromCoreDB: dictDataSalesDB).count > 0 {
                    
                    cell.btnContactName.isEnabled = true
                    
                } else {
                    
                    cell.btnContactName.isEnabled = false
                    
                }
                
                
                cell.btnCompanyName.tag = indexPath.row
                cell.btnCompanyName.setTitle("\(dictDataSalesDB.value(forKey: "companyName")!)", for: .normal)
                cell.btnCompanyName.addTarget(self, action: #selector(actionOnServiceAutoCompanyNameClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "companyName")!)".count > 0 {
                    
                    cell.btnCompanyName.isEnabled = true
                    
                } else {
                    
                    cell.btnCompanyName.isEnabled = false
                    
                }
                
                cell.btnAddress.tag = indexPath.row
                cell.btnAddress.setTitle(Global().strCombinedAddressService(for: dictDataSalesDB), for: .normal)
                cell.btnAddress.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
                if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                    
                    cell.btnAddress.isEnabled = true
                    
                } else {
                    
                    cell.btnAddress.isEnabled = false
                    
                }
                
                cell.btnEmail.tag = indexPath.row
                cell.btnEmail.setTitle("\(dictDataSalesDB.value(forKey: "primaryEmail")!)", for: .normal)
                cell.btnEmail.addTarget(self, action: #selector(actionOnServiceAutoEmailClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "primaryEmail")!)".count > 0 {
                    
                    cell.btnEmail.isEnabled = true
                    
                } else {
                    
                    cell.btnEmail.isEnabled = false
                    
                }
                
                cell.lblScheduleDate.text = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime")!)")
                
                cell.btnDriveTime.setTitle("\(dictDataSalesDB.value(forKey: "driveTimeStr")!)", for: .normal)
                
                if "\(dictDataSalesDB.value(forKey: "driveTimeStr")!)".count > 0 {
                    
                    cell.btnDriveTime.isHidden = false
                    
                } else {
                    
                    cell.btnDriveTime.isHidden = true
                    
                }
                
                if "\(dictDataSalesDB.value(forKey: "earliestStartTimeStr")!)".count > 0 {
                    
                    cell.lblTimeRange.text = "Time Range: " + "\(dictDataSalesDB.value(forKey: "earliestStartTimeStr")!)" + " - " + "\(dictDataSalesDB.value(forKey: "latestStartTimeStr")!)"
                    
                } else {
                    
                    cell.lblTimeRange.text = ""
                    
                }
                
                //---For Services
                let strService = "\(dictDataSalesDB.value(forKey: "services") ?? "")"
                if(strService != ""){
                    
                    if DeviceType.IS_IPAD {
                        if cell.lblTimeRange.text == "" {
                            cell.lblTimeRange.text = "Services: \(strService)"
                        }else{
                            cell.lblTimeRange.text =  cell.lblTimeRange.text! + ", Services: \(strService)"
                        }
                     
                    }else{
                        if cell.lblTimeRange.text == "" {
                            cell.lblTimeRange.text = "Services: \(strService)"
                        }else{
                            cell.lblTimeRange.text =  cell.lblTimeRange.text! + "\nServices: \(strService)"

                        }
                        
                      
                    }
                }

                cell.btnCellFooterMap.tag = indexPath.row
                cell.btnCellFooterMap.setTitle("", for: .normal)
                cell.btnCellFooterMap.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
                if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                    
                    cell.btnCellFooterMap.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterMap.isEnabled = false
                    
                }
                
                cell.btnCellFooterSMS.tag = indexPath.row
                cell.btnCellFooterSMS.setTitle("", for: .normal)
                cell.btnCellFooterSMS.addTarget(self, action: #selector(actionOnServiceAutoSMSClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "primaryPhone")!)".count > 0 && strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID")!)".count > 0 {
                    
                    cell.btnCellFooterSMS.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterSMS.isEnabled = false
                    
                }
                
                let strNoLocal = self.getSalesNo(dictDataSalesDB: dictDataSalesDB)
                
                cell.btnCellFooterCall.tag = indexPath.row
                cell.btnCellFooterCall.setTitle("", for: .normal)
                cell.btnCellFooterCall.addTarget(self, action: #selector(actionOnServiceAutoCallClick), for: .touchUpInside)
                if strNoLocal.count > 0 {
                    
                    cell.btnCellFooterCall.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterCall.isEnabled = false
                    
                }
                
                cell.btnCellFooterEmail.tag = indexPath.row
                cell.btnCellFooterEmail.setTitle("", for: .normal)
                cell.btnCellFooterEmail.addTarget(self, action: #selector(actionOnServiceAutoFooterEmailClick), for: .touchUpInside)
                if strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID")!)".count > 0 {
                    
                    cell.btnCellFooterEmail.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterEmail.isEnabled = false
                    
                }
                
                cell.btnCellFooterPrint.tag = indexPath.row
                cell.btnCellFooterPrint.setTitle("", for: .normal)
                cell.btnCellFooterPrint.addTarget(self, action: #selector(actionOnServiceAutoPrintClick), for: .touchUpInside)
                
                cell.btnCellFooterResendMail.tag = indexPath.row
                cell.btnCellFooterResendMail.setTitle("", for: .normal)
                cell.btnCellFooterResendMail.addTarget(self, action: #selector(actionOnPlumbingResendEmailClick), for: .touchUpInside)
                
                var isResendEmailLocal = true
                
                if "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "1" || "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "true" || "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "True"{
                    
                    isResendEmailLocal = true
                    
                    if "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "Complete" || "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "complete" || "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "Completed" || "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "completed" || "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "CompletePending" || "\(dictDataSalesDB.value(forKey: "workorderStatus")!)" == "completePending"{
                        
                        isResendEmailLocal = true
                        
                    } else {
                        
                        isResendEmailLocal = false
                        
                    }
                    
                    
                } else {
                    
                    isResendEmailLocal = false
                    
                }
                
                if isResendEmailLocal {
                    
                    cell.btnCellFooterResendMail.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterResendMail.isEnabled = false
                    
                }
                
                cell.btnCellFooterSubWoStatus.isHidden = true
                
                let dictDataTemp = SalesCoreDataVC().plumbingStatus("\(dictDataSalesDB.value(forKey: "workorderId")!)")
                
                if "\(dictDataTemp.value(forKey: "hidden") ?? "")" == "no" {
                    
                    cell.btnCellFooterSubWoStatus.isHidden = false
                    
                }else{
                    
                    cell.btnCellFooterSubWoStatus.isHidden = true
                    
                }
                
                if "\(dictDataTemp.value(forKey: "Enabled") ?? "")" == "yes" {
                    
                    cell.btnCellFooterSubWoStatus.isEnabled = true
                    
                }else{
                    
                    cell.btnCellFooterSubWoStatus.isEnabled = false
                    
                }
                
                cell.btnCellFooterSubWoStatus.setTitle("\(dictDataTemp.value(forKey: "ButtonStatus") ?? "")", for: .normal)
                
                cell.lblStatus.text = "Service - \(dictDataTemp.value(forKey: "SerivceStatus") ?? "")"
                
                cell.btnCellFooterSubWoStatus.tag = indexPath.row
                cell.btnCellFooterSubWoStatus.addTarget(self, action: #selector(actionOnPlumbingSubWoStatusChange), for: .touchUpInside)
                
                return cell
            } else {
                
                // MARK: -
                // MARK: ----------Cell For Row For Service---------
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellServiceAppointmentVC") as! CellServiceAppointmentVC
                cell.contentView.backgroundColor = UIColor.white
                
                if (DeviceType.IS_IPAD){
                    
                    cell.cellBackCardView.backgroundColor = UIColor.white //
                    cell.cellBottomButtonsBackCardView.backgroundColor = hexStringToUIColor(hex: "F2F2F7")
                    
                }

                cell.lblAccountName.textColor  = hexStringToUIColor(hex: appThemeColor)

                cell.lblAccountName.text = "Account: \(dictDataSalesDB.value(forKey: "accountName") ?? "")"//"Account Name: \(dictDataSalesDB.value(forKey: "accountName")!)"
                
                cell.lblWoNo.text = "WO #: \(dictDataSalesDB.value(forKey: "workOrderNo")!)"
                
                var str_company_Name = "" , str_contact_Name = ""
                
                var strAccounNo = ""

                if "\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? "")".count > 0
                {
                    strAccounNo = " (\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? ""))"
                }
                else
                {
                    strAccounNo = " (\(dictDataSalesDB.value(forKey: "accountNo") ?? ""))"
                }
                
                if "\(dictDataSalesDB.value(forKey: "accountName")!)".count == 0 {
                    
                    if let contact_Name = Global().strFullName(fromCoreDB: dictDataSalesDB){
                        if("\(contact_Name)".count > 0 && "\(contact_Name)" != "<null>" && "\(contact_Name)" != " "){
                            str_contact_Name = "\(contact_Name)"
                            cell.lblAccountName.text = cell.lblAccountName.text! + str_contact_Name
                        }
                    }
                    
                    if let company_Name = dictDataSalesDB.value(forKey: "companyName"){
                        if("\(company_Name)".count > 0 && "\(company_Name)" != "<null>" && "\(company_Name)" != " "){
                            str_company_Name = "\(company_Name)"
                            cell.lblAccountName.text = cell.lblAccountName.text! + "  \u{2022}  " + str_company_Name
                        }
                    }
                    
                }
                cell.lblAccountName.text = cell.lblAccountName.text! + strAccounNo
                
                cell.lblStatus.text = "Service - \(dictDataSalesDB.value(forKey: "workorderStatus")!)"
                
                cell.btnContactName.tag = indexPath.row
                cell.btnContactName.setTitle(Global().strFullName(fromCoreDB: dictDataSalesDB), for: .normal)
                cell.btnContactName.addTarget(self, action: #selector(actionOnServiceAutoContactNameClick), for: .touchUpInside)
                if Global().strFullName(fromCoreDB: dictDataSalesDB).count > 0 {
                    
                    cell.btnContactName.isEnabled = true
                    
                } else {
                    
                    cell.btnContactName.isEnabled = false
                    
                }
                
                
                cell.btnCompanyName.tag = indexPath.row
                cell.btnCompanyName.setTitle("\(dictDataSalesDB.value(forKey: "companyName") ?? "")", for: .normal)
                cell.btnCompanyName.addTarget(self, action: #selector(actionOnServiceAutoCompanyNameClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "companyName") ?? "")".count > 0 {
                    
                    cell.btnCompanyName.isEnabled = true
                    
                } else {
                    
                    cell.btnCompanyName.isEnabled = false
                    
                }
                
                cell.btnAddress.tag = indexPath.row
                cell.btnAddress.setTitle(Global().strCombinedAddressService(for: dictDataSalesDB), for: .normal)
                cell.btnAddress.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
                if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                    
                    cell.btnAddress.isEnabled = true
                    
                } else {
                    
                    cell.btnAddress.isEnabled = false
                    
                }
                
                cell.btnEmail.tag = indexPath.row
                cell.btnEmail.setTitle("\(dictDataSalesDB.value(forKey: "primaryEmail") ?? "")", for: .normal)
                cell.btnEmail.addTarget(self, action: #selector(actionOnServiceAutoEmailClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "primaryEmail") ?? "")".count > 0 {
                    
                    cell.btnEmail.isEnabled = true
                    
                } else {
                    
                    cell.btnEmail.isEnabled = false
                    
                }
                
                cell.lblScheduleDate.text = Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dictDataSalesDB.value(forKey: "scheduleStartDateTime") ?? "")")

                cell.btnDriveTime.setTitle("\(dictDataSalesDB.value(forKey: "driveTimeStr") ?? "")", for: .normal)
                
                if "\(dictDataSalesDB.value(forKey: "driveTimeStr") ?? "")".count > 0 {
                    
                    cell.btnDriveTime.isHidden = false
                    
                } else {
                    
                    cell.btnDriveTime.isHidden = true
                    
                }
                
                if "\(dictDataSalesDB.value(forKey: "earliestStartTimeStr") ?? "")".count > 0 {
                    //cell.lblTimeRange.text = "Time Range: " + "\(dictDataSalesDB.value(forKey: "earliestStartTimeStr") ?? "")" + " - " + "\(dictDataSalesDB.value(forKey: "latestStartTimeStr") ?? "")"
                    
                    let strEarliestStartTime = changeStringDateToGivenFormat(strDate:  "\(dictDataSalesDB.value(forKey: "earliestStartTime") ?? "")", strRequiredFormat: "hh:mm a")

                    let strLatestStartTime = changeStringDateToGivenFormat(strDate:  "\(dictDataSalesDB.value(forKey: "latestStartTime") ?? "")" , strRequiredFormat: "hh:mm a")

                    cell.lblTimeRange.text = "Time Range: " + strEarliestStartTime + " - " + strLatestStartTime
                    
                } else {
                    
                 //   cell.lblTimeRange.text = "Time Range:"
                    cell.lblTimeRange.text = ""
                }
                
                //---For Services
                let strService = "\(dictDataSalesDB.value(forKey: "services") ?? "")"
                if(strService != ""){
                    
                    if DeviceType.IS_IPAD {
                        if cell.lblTimeRange.text == "" {
                            cell.lblTimeRange.text = "Services: \(strService)"
                        }else{
                            cell.lblTimeRange.text =  cell.lblTimeRange.text! + ", Services: \(strService)"
                        }
                     
                    }else{
                        if cell.lblTimeRange.text == "" {
                            cell.lblTimeRange.text = "Services: \(strService)"
                        }else{
                            cell.lblTimeRange.text =  cell.lblTimeRange.text! + "\nServices: \(strService)"

                        }
                        
                      
                    }
                }
                
                cell.btnCellFooterMap.tag = indexPath.row
                cell.btnCellFooterMap.setTitle("", for: .normal)
                cell.btnCellFooterMap.addTarget(self, action: #selector(actionOnServiceAutoAddressClick), for: .touchUpInside)
                if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                    
                    cell.btnCellFooterMap.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterMap.isEnabled = false
                    
                }
                
        //Temp Comment 17 Sept        //callWebServiceToGetNonPrefferableDataAndTime(strWorkorderId: "\(dictDataSalesDB.value(forKey: "workOrderNo")!)")
                
                if "\(dictDataSalesDB.value(forKey: "isLocked") ?? "")" == "1" {
                    
                    cell.btnIsLocked.isHidden = false
                    cell.btnIsLocked.setImage(UIImage(named: "lock"), for: .normal)
                    
                } else {
                    
                    cell.btnIsLocked.isHidden = true
                    
                }
                
                cell.btnCellFooterSMS.tag = indexPath.row
                cell.btnCellFooterSMS.setTitle("", for: .normal)
                cell.btnCellFooterSMS.addTarget(self, action: #selector(actionOnServiceAutoSMSClick), for: .touchUpInside)
                if "\(dictDataSalesDB.value(forKey: "primaryPhone") ?? "")".count > 0 && strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID") ?? "")".count > 0 {
                    
                    cell.btnCellFooterSMS.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterSMS.isEnabled = false
                    
                }
                
                let strNoLocal = self.getSalesNo(dictDataSalesDB: dictDataSalesDB)
                
                cell.btnCellFooterCall.tag = indexPath.row
                cell.btnCellFooterCall.setTitle("", for: .normal)
                cell.btnCellFooterCall.addTarget(self, action: #selector(actionOnServiceAutoCallClick), for: .touchUpInside)
                if strNoLocal.count > 0 {
                    
                    cell.btnCellFooterCall.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterCall.isEnabled = false
                    
                }
                
                cell.btnCellFooterEmail.tag = indexPath.row
                cell.btnCellFooterEmail.setTitle("", for: .normal)
                cell.btnCellFooterEmail.addTarget(self, action: #selector(actionOnServiceAutoFooterEmailClick), for: .touchUpInside)
                if strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID") ?? "")".count > 0 {
                    
                    cell.btnCellFooterEmail.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterEmail.isEnabled = false
                    
                }
                
                cell.btnCellFooterPrint.tag = indexPath.row
                cell.btnCellFooterPrint.setTitle("", for: .normal)
                cell.btnCellFooterPrint.addTarget(self, action: #selector(actionOnServiceAutoPrintClick), for: .touchUpInside)
                
                cell.btnCellFooterResendMail.tag = indexPath.row
                cell.btnCellFooterResendMail.setTitle("", for: .normal)
                cell.btnCellFooterResendMail.addTarget(self, action: #selector(actionOnServiceAutoResendEmailClick), for: .touchUpInside)
                
                var isResendEmailLocal = true
                
                if "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "1" || "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "true" || "\(dictDataSalesDB.value(forKey: "isResendInvoiceMail") ?? "")" == "True"{
                    
                    isResendEmailLocal = true
                    
                    if "\(dictDataSalesDB.value(forKey: "workorderStatus") ?? "")" == "Incomplete" || "\(dictDataSalesDB.value(forKey: "workorderStatus") ?? "")" == "InComplete"{
                        
                        isResendEmailLocal = false
                        
                    } else {
                        
                        isResendEmailLocal = true
                        
                    }
                    
                    
                } else {
                    
                    isResendEmailLocal = false
                    
                }
                
                if isResendEmailLocal {
                    
                    cell.btnCellFooterResendMail.isEnabled = true
                    
                } else {
                    
                    cell.btnCellFooterResendMail.isEnabled = false
                    
                }
                
                if DeviceType.IS_IPAD {

                    let departmentType = "\(dictDataSalesDB.value(forKey: "departmentType")!)"
                    let serviceState = "\(dictDataSalesDB.value(forKey: "serviceState")!)"
                    
                    if departmentType == "Termite" && serviceState == "California" {
                        
                        // ForResync Data zSync
                        if "\(dictDataSalesDB.value(forKey: "zSync") ?? "")" == "yes"
                        {
                            cell.btnReSyncDataToServer.isHidden = false
                        }
                        else
                        {
                            cell.btnReSyncDataToServer.isHidden = true
                        }
                        cell.btnReSyncDataToServer.tag = indexPath.row
                        cell.btnReSyncDataToServer.addTarget(self, action: #selector(actionOnReSyncServiceDataToServer), for: .touchUpInside)
                        
                        buttonRound(sender:  cell.btnReSyncDataToServer)
                        
                    }else{
                        
                        cell.btnReSyncDataToServer.isHidden = true
                        
                    }
                    
                }
                
                
                //---------By navin Approval check
                let departmentType = "\(dictDataSalesDB.value(forKey: "departmentType")!)"
                let serviceState = "\(dictDataSalesDB.value(forKey: "serviceState")!)"
                if departmentType == "Termite" && serviceState == "California" {
                    
                    if (DeviceType.IS_IPAD && (departmentType == "Termite" && serviceState == "California") && self.aryApprovalCheckList.count != 0)  {
                        
                        //---For Show status
                        let wdoWorkflowStageId =  "\(dictDataSalesDB.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(dictDataSalesDB.value(forKey: "wdoWorkflowStageId") ?? "")"
                        let wdoWorkflowStatus = "\(dictDataSalesDB.value(forKey: "wdoWorkflowStatus") ?? "")"
                        if(wdoWorkflowStageId.count == 0 && wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Pending.rawValue.lowercased()){
                            cell.lblStatus.text = cell.lblStatus.text! + " (\(dictDataSalesDB.value(forKey: "wdoWorkflowStatus")!) - Inspector Correction)"
                            cell.contentView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                            cell.cellBackCardView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                            cell.cellBottomButtonsBackCardView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                            cell.lblAccountName.textColor  = hexStringToUIColor(hex: "BA0100")
                            
                        }
                        //Redirect krna hai Vo Webview View Jaha Sign Edit krega…
                      let ISStage =  ((wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased()) || (wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased()))

                        if checkRedirectSignApprovalOrNot(obj: dictDataSalesDB, checkflowStageType: true, checkflowStatus: true) &&   ISStage == false{
                            
                            cell.contentView.backgroundColor = hexStringToUIColor(hex: "DCF9F8")
                            cell.cellBackCardView.backgroundColor = hexStringToUIColor(hex: "DCF9F8")
                            cell.cellBottomButtonsBackCardView.backgroundColor = hexStringToUIColor(hex: "DCF9F8")
                            
                            
                            cell.lblStatus.text = cell.lblStatus.text! + " (\(dictDataSalesDB.value(forKey: "wdoWorkflowStatus")!) - \(dictDataSalesDB.value(forKey: "wdoWorkflowStage")!))"
                            
                            //Servicxe manager decline then show in red and other green
                            let isServiceManagerDeclined =  "\(dictDataSalesDB.value(forKey: "isServiceManagerDeclined") ?? "0")"

                            if isServiceManagerDeclined.lowercased() == "1" ||  isServiceManagerDeclined.lowercased() == "true"{
                                cell.contentView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                                cell.cellBackCardView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                                cell.cellBottomButtonsBackCardView.backgroundColor = hexStringToUIColor(hex: "F8DEE0")
                                cell.lblAccountName.textColor  = hexStringToUIColor(hex: "BA0100")
                                
                            }

                        }else if (wdoWorkflowStageId.count > 0 && wdoWorkflowStatus.count > 0) {
                            
                            if (wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Complete.rawValue.lowercased() || wdoWorkflowStatus.lowercased() == WdoWorkflowStatusEnum.Completed.rawValue.lowercased() ) {
                                
                                cell.lblStatus.text = cell.lblStatus.text! + "(Approved)"
                                
                            }else{
                                
                                cell.lblStatus.text = cell.lblStatus.text! + " (\(dictDataSalesDB.value(forKey: "wdoWorkflowStatus")!) - \(dictDataSalesDB.value(forKey: "wdoWorkflowStage")!))"

                            }
                        }
                    }
                }
                return cell
            }
        }else{
            
            // MARK: -
            // MARK: ----------Cell For Row For Sales Auto---------
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellSalesLeadsAppointmentVC") as! CellSalesLeadsAppointmentVC
            cell.lblAccountName.text = "Account: \(dictDataSalesDB.value(forKey: "accountName") ?? "")"//"Account Name: \(dictDataSalesDB.value(forKey: "accountName")!)"
            cell.lblOppNo.text = "Opp #: \(dictDataSalesDB.value(forKey: "leadNumber")!)"
            
            var str_company_Name = "" , str_contact_Name = ""
            
            var strAccounNo = ""

            if "\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? "")".count > 0
            {
                strAccounNo = " (\(dictDataSalesDB.value(forKey: "thirdPartyAccountNo") ?? ""))"
            }
            else
            {
                strAccounNo = " (\(dictDataSalesDB.value(forKey: "accountNo") ?? ""))"
            }
            
            if "\(dictDataSalesDB.value(forKey: "accountName")!)".count == 0 {
                
                if let contact_Name = Global().strFullName(fromCoreDB: dictDataSalesDB){
                    if("\(contact_Name)".count > 0 && "\(contact_Name)" != "<null>" && "\(contact_Name)" != " "){
                        str_contact_Name = "\(contact_Name)"
                        cell.lblAccountName.text = cell.lblAccountName.text! +  str_contact_Name
                    }
                }
                
                if let company_Name = dictDataSalesDB.value(forKey: "companyName"){
                    if("\(company_Name)".count > 0 && "\(company_Name)" != "<null>" && "\(company_Name)" != " "){
                        str_company_Name = "\(company_Name)"
                        cell.lblAccountName.text = cell.lblAccountName.text! + "  \u{2022}  " + str_company_Name
                    }
                }
                
            }
            cell.lblAccountName.text = cell.lblAccountName.text! + strAccounNo
            //cell.lblStatus.text = "Sales - \(dictDataSalesDB.value(forKey: "statusSysName")!) - \(dictDataSalesDB.value(forKey: "stageSysName")!)"
            
            let statusSysName = "\(dictDataSalesDB.value(forKey: "statusSysName") ?? "")"
            let stageSysName = "\(dictDataSalesDB.value(forKey: "stageSysName") ?? "")"
            
            cell.lblStatus.text = "Sales - \(dictOpportunityStatusNameFromSysName.value(forKey: statusSysName) ?? "") - \(dictOpportunityStageNameFromSysName.value(forKey: stageSysName) ?? "")"
            
            cell.btnContactName.tag = indexPath.row
            cell.btnContactName.setTitle(Global().strFullName(fromCoreDB: dictDataSalesDB), for: .normal)
            cell.btnContactName.addTarget(self, action: #selector(actionOnSalesAutoContactNameClick), for: .touchUpInside)
            cell.btnFlowType.addTarget(self, action: #selector(actionOnBtnFlowTypeClick), for: .touchUpInside)
            cell.btnFlowType.setTitle("\(dictDataSalesDB.value(forKey: "flowType") ?? "")", for: .normal)
            cell.btnFlowType.tag = indexPath.row
            if Global().strFullName(fromCoreDB: dictDataSalesDB).count > 0 {
                
                cell.btnContactName.isEnabled = true
                
            } else {
                
                cell.btnContactName.isEnabled = false
                
            }
            
            
            cell.btnCompanyName.tag = indexPath.row
            cell.btnCompanyName.setTitle("\(dictDataSalesDB.value(forKey: "companyName")!)", for: .normal)
            cell.btnCompanyName.addTarget(self, action: #selector(actionOnSalesAutoCompanyNameClick), for: .touchUpInside)
            if "\(dictDataSalesDB.value(forKey: "companyId")!)".count > 0 {
                
                cell.btnCompanyName.isEnabled = true
                
            } else {
                
                cell.btnCompanyName.isEnabled = false
                
            }
            
            cell.btnAddress.tag = indexPath.row
            cell.btnAddress.setTitle(Global().strCombinedAddressService(for: dictDataSalesDB), for: .normal)
            cell.btnAddress.addTarget(self, action: #selector(actionOnSalesAutoAddressClick), for: .touchUpInside)
            if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                
                cell.btnAddress.isEnabled = true
                
            } else {
                
                cell.btnAddress.isEnabled = false
                
            }
            
            cell.btnEmail.tag = indexPath.row
            cell.btnEmail.setTitle("\(dictDataSalesDB.value(forKey: "primaryEmail")!)", for: .normal)
            cell.btnEmail.addTarget(self, action: #selector(actionOnSalesAutoEmailClick), for: .touchUpInside)
            if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                
                cell.btnEmail.isEnabled = true
                
            } else {
                
                cell.btnEmail.isEnabled = false
                
            }
            
            cell.lblScheduleDate.text = Global().changeDate(toLocalDateOtherSaavan: "\(dictDataSalesDB.value(forKey: "scheduleStartDate")!)")
            
            cell.btnDriveTime.setTitle("\(dictDataSalesDB.value(forKey: "driveTime")!)", for: .normal)
            
            if "\(dictDataSalesDB.value(forKey: "driveTime")!)".count > 0 {
                
                cell.btnDriveTime.isHidden = false
                
            } else {
                
                cell.btnDriveTime.isHidden = true
                
            }
            
            if "\(dictDataSalesDB.value(forKey: "latestStartTime")!)".count > 0 {
                
                //cell.lblTimeRange.text = "Time Range: " + Global().changeDate(toLocalDateAkshay: "\(dictDataSalesDB.value(forKey: "latestStartTime")!)") + "-" + Global().changeDate(toLocalDateAkshay: "\(dictDataSalesDB.value(forKey: "earliestStartTime")!)")
                
                //cell.lblTimeRange.text = "Time Range: " + Global().changeDate(toLocalTimeAkshay: "\(dictDataSalesDB.value(forKey: "earliestStartTime")!)", type: "time") + " - " + Global().changeDate(toLocalTimeAkshay: "\(dictDataSalesDB.value(forKey: "latestStartTime")!)", type: "time")
                
                cell.lblTimeRange.text = "Time Range: " + changeStringDateToGivenFormat(strDate: "\(dictDataSalesDB.value(forKey: "earliestStartTime") ?? "")", strRequiredFormat: "hh:mm a") + "-" +  changeStringDateToGivenFormat(strDate: "\(dictDataSalesDB.value(forKey: "latestStartTime") ?? "")", strRequiredFormat: "hh:mm a")   //Global().changeDate(toLocalTimeAkshay: "\(dictDataSalesDB.value(forKey: "earliestStartTime")!)", type: "time") + " - " + Global().changeDate(toLocalTimeAkshay: "\(dictDataSalesDB.value(forKey: "latestStartTime")!)", type: "time")

                
                
            } else {
                
                cell.lblTimeRange.text = "Time Range:"
                
            }
            
            cell.btnCellFooterMap.tag = indexPath.row
            cell.btnCellFooterMap.setTitle("", for: .normal)
            cell.btnCellFooterMap.addTarget(self, action: #selector(actionOnSalesAutoAddressClick), for: .touchUpInside)
            if Global().strCombinedAddressService(for: dictDataSalesDB).count > 0 {
                
                cell.btnCellFooterMap.isEnabled = true
                
            } else {
                
                cell.btnCellFooterMap.isEnabled = false
                
            }
            
            cell.btnCellFooterSMS.tag = indexPath.row
            cell.btnCellFooterSMS.setTitle("", for: .normal)
            cell.btnCellFooterSMS.addTarget(self, action: #selector(actionOnSalesAutoSMSClick), for: .touchUpInside)
            if "\(dictDataSalesDB.value(forKey: "primaryPhone")!)".count > 0 && strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID")!)".count > 0 {
                
                cell.btnCellFooterSMS.isEnabled = true
                
            } else {
                
                cell.btnCellFooterSMS.isEnabled = false
                
            }
            
            let strNoLocal = self.getSalesNo(dictDataSalesDB: dictDataSalesDB)
            
            cell.btnCellFooterCall.tag = indexPath.row
            cell.btnCellFooterCall.setTitle("", for: .normal)
            cell.btnCellFooterCall.addTarget(self, action: #selector(actionOnSalesAutoCallClick), for: .touchUpInside)
            if strNoLocal.count > 0 {
                
                cell.btnCellFooterCall.isEnabled = true
                
            } else {
                
                cell.btnCellFooterCall.isEnabled = false
                
            }
            
            cell.btnCellFooterEmail.tag = indexPath.row
            cell.btnCellFooterEmail.setTitle("", for: .normal)
            cell.btnCellFooterEmail.addTarget(self, action: #selector(actionOnSalesAutoFooterEmailClick), for: .touchUpInside)
            if strIsGGQIntegration == "1" && "\(dictDataSalesDB.value(forKey: "surveyID")!)".count > 0 {
                
                cell.btnCellFooterEmail.isEnabled = true
                
            } else {
                
                cell.btnCellFooterEmail.isEnabled = false
                
            }
            
            cell.btnCellFooterPrint.tag = indexPath.row
            cell.btnCellFooterPrint.setTitle("", for: .normal)
            cell.btnCellFooterPrint.addTarget(self, action: #selector(actionOnSalesAutoPrintClick), for: .touchUpInside)
            
            cell.btnCellFooterResendMail.tag = indexPath.row
            cell.btnCellFooterResendMail.setTitle("", for: .normal)
            cell.btnCellFooterResendMail.addTarget(self, action: #selector(actionOnSalesAutoResendEmailClick), for: .touchUpInside)
            
            var isResendEmailLocal = true
            
            if "\(dictDataSalesDB.value(forKey: "isResendAgreementProposalMail") ?? "")" == "1" || "\(dictDataSalesDB.value(forKey: "isResendAgreementProposalMail") ?? "")" == "true" || "\(dictDataSalesDB.value(forKey: "isResendAgreementProposalMail") ?? "")" == "True"{
                
                isResendEmailLocal = true
                
                if "\(dictDataSalesDB.value(forKey: "statusSysName")!)" == "Incomplete" || "\(dictDataSalesDB.value(forKey: "statusSysName")!)" == "InComplete"{
                    
                    isResendEmailLocal = false
                    
                } else {
                    
                    isResendEmailLocal = true
                    
                }
                
                
            } else {
                
                isResendEmailLocal = false
                
            }
            
            if "\(dictDataSalesDB.value(forKey: "stageSysName")!)" == "lost" || "\(dictDataSalesDB.value(forKey: "stageSysName")!)" == "Lost"{
                
                //isResendEmailLocal = false
                
            } else {
                
                //isResendEmailLocal = true
                
            }
            
            if isResendEmailLocal {
                
                cell.btnCellFooterResendMail.isEnabled = true
                
            } else {
                
                cell.btnCellFooterResendMail.isEnabled = false
                
            }
            
            // For Initial Setup
            
            cell.btnInitialSetup.tag = indexPath.row
            
            //cell.btnInitialSetup.isHidden = true
            
            cell.btnInitialSetup.addTarget(self, action: #selector(actionOnCreateInitialSetupClick), for: .touchUpInside)
            
            if (showGenerateWorkorderSetup(dictSalesDB: dictDataSalesDB) == true)
            {
                let myNormalAttributedTitle = NSAttributedString(string: "Generate Workorder",
                                                                 attributes: [NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "007AFF")])
                
                cell.btnInitialSetup.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            }
            else
            {
                let myNormalAttributedTitle = NSAttributedString(string: "Create Initial Setup",
                                                                 attributes: [NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "007AFF")])
                
                cell.btnInitialSetup.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            }
            
            if (showInitialSetup(dictSalesDB: dictDataSalesDB) == true)
            {
                cell.btnInitialSetup.isHidden = false
            }
            else
            {
                cell.btnInitialSetup.isHidden = true
            }
            
            let strLeadStatus = "\(dictDataSalesDB.value(forKey: "statusSysName") ?? "")"
            let strStageSysName = "\(dictDataSalesDB.value(forKey: "stageSysName") ?? "")"
            
            if strLeadStatus.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Won") == .orderedSame
            {
                cell.btnResetStatus.isHidden = false
                cell.btnFlowType.isHidden = true
                cell.imgFlowType.isHidden = true


            }
            else
            {
                cell.btnResetStatus.isHidden = true
                cell.btnFlowType.isHidden = false
                cell.imgFlowType.isHidden = false

                
                if strLeadStatus.caseInsensitiveCompare("Complete") == .orderedSame && strStageSysName.caseInsensitiveCompare("Lost") == .orderedSame
                {
                    //cell.btnFlowType.isHidden = true
                    //cell.imgFlowType.isHidden = true
                }
                
            }
            cell.btnResetStatus.tag = indexPath.row
            cell.btnResetStatus.addTarget(self, action: #selector(actionOnResetStatus), for: .touchUpInside)
            
            buttonRound(sender:  cell.btnResetStatus)
            
            if DeviceType.IS_IPAD {
                
                // ForResync Data zSync
                if "\(dictDataSalesDB.value(forKey: "zSync") ?? "")" == "yes"
                {
                    cell.btnReSyncDataToServer.isHidden = false
                }
                else
                {
                    cell.btnReSyncDataToServer.isHidden = true
                }
                cell.btnReSyncDataToServer.tag = indexPath.row
                cell.btnReSyncDataToServer.addTarget(self, action: #selector(actionOnReSyncSalesDataToServer), for: .touchUpInside)
                
                buttonRound(sender:  cell.btnReSyncDataToServer)
                cell.btnReSyncDataToServer.isHidden = true
                
            }
                        
            if "\(dictDataSalesDB.value(forKey: "flowType") ?? "")".caseInsensitiveCompare("Commercial") == .orderedSame
            {
                cell.btnInitialSetup.isHidden = true
            }
            //if ("\(dictDataSalesDB.valueFOr)".caseInsensitiveCompare("Complete") == .orderedSame )
            
            
            // Hide for Now
            
            //cell.btnFlowType.isHidden = true
            //cell.imgFlowType.isHidden = true

            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dictDataSalesDB = arrayAppointment[indexPath.row] as! NSManagedObject
        
        if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
            
            // Block Time
            return UITableView.automaticDimension//tableView.rowHeight
            
        }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
            
            // Task List
            
            return tableView.rowHeight
            
        }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
            
            return tableView.rowHeight
            
        }else{
            
            if "\(dictDataSalesDB.value(forKey: "isInvisible")!)" == "1" || "\(dictDataSalesDB.value(forKey: "isInvisible")!)" == "true" || "\(dictDataSalesDB.value(forKey: "isInvisible")!)" == "True"{
                
                return 0
                
            }else{
                
                return tableView.rowHeight
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        
        var chkForPending = false
        var arrPendingWO = NSArray()
        var strMessage = ""
        
        if DeviceType.IS_IPAD {
            
            if aryApprovalCheckList.count > 0
            {
                chkForPending = true
            }
            else
            {
                chkForPending = false
            }
            
            let pendindListObject = getPendingWorkOrders(arrayObj: arrayAppointmentDataMain)
            arrPendingWO = pendindListObject.arrayPendingWo
            strMessage = pendindListObject.strMessage
            
        }
        
        let dictDataSalesDB = arrayAppointment[indexPath.row] as! NSManagedObject
        
        nsud.set(false, forKey: "yesAudio")
        nsud.set("", forKey: "AudioNameService")
        nsud.synchronize()
        
        if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
            
            // Block Time
            
        }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
            
            // Task List
            if chkForPending
            {
                if arrPendingWO.count > 0 && arrPendingWO.count > 0
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
                else
                {
                    self.view.endEditing(true)
                    tableView.deselectRow(at: indexPath, animated: false)
                    let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                    
                    vc.taskId = "\(dictDataSalesDB.value(forKey: "leadTaskId")!)" //LeadTaskId
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else
            {
                self.view.endEditing(true)
                tableView.deselectRow(at: indexPath, animated: false)
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
                
                vc.taskId = "\(dictDataSalesDB.value(forKey: "leadTaskId")!)" //LeadTaskId
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
            
            // Service Flow
            
            // Save Id For Notes History View To
            
            nsud.set("\(dictDataSalesDB.value(forKey: "workorderId") ?? "")", forKey: "StrRefId")
            nsud.synchronize()
            
            if chkForPending && arrPendingWO.count > 0
            {
                let strWoNo = "\(dictDataSalesDB.value(forKey: "workOrderNo") ?? "")"
                
                if arrPendingWO.contains(strWoNo)
                {
                    if DeviceType.IS_IPAD {
                        
                        goToServiceFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                        
                    } else {
                        
                        goToServiceFlow(dictDataSalesLocal: dictDataSalesDB)
                        
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
            }
            else
            {
                if DeviceType.IS_IPAD {
                    
                    goToServiceFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                    
                } else {
                    
                    goToServiceFlow(dictDataSalesLocal: dictDataSalesDB)
                    
                }
            }
            
        }else{
            
            // Sales Flow
            
            // Save Id For Notes History View To
            
            nsud.set("\(dictDataSalesDB.value(forKey: "leadId") ?? "")", forKey: "StrRefId")
            nsud.synchronize()
            
            if chkForPending
            {
                if arrPendingWO.count > 0 && arrPendingWO.count > 0
                {
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: strMessage, viewcontrol: self)
                }
                else
                {
                    if DeviceType.IS_IPAD {
                        
                        self.goToSalesFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                        
                    } else {
                        
                        self.goToSalesFlow(dictDataSalesLocal: dictDataSalesDB)
                        
                    }
                }
            }
            else
            {
                if DeviceType.IS_IPAD {
                    
                    self.goToSalesFlowiPad(dictDataSalesLocal: dictDataSalesDB)
                    
                } else {
                    
                    self.goToSalesFlow(dictDataSalesLocal: dictDataSalesDB)
                    
                }
            }
            
            
        }
        
    }
    
}


extension  AppointmentVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.text = ""
        //const_ViewTop_H.constant = 60
        //const_SearchBar_H.constant = 0
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
    }
    
    func searchAutocomplete(searchText: String) -> Void{
        
        if searchText.isEmpty {
            
            self.arrayAppointment = NSArray()
            self.arrayAppointment = self.arrayAppointmentData
            self.tblViewAppointment.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBarAppointment.resignFirstResponder()
            }
            
        }
        
        else
        {
            
            let aryWorkOrderDetailsService = NSMutableArray()
            let arySalesService = NSMutableArray()
            let aryTaskss = NSMutableArray()
            
            for (index, _) in self.arrayAppointmentData.enumerated() {
                let dictDataSalesDB = self.arrayAppointmentData[index] as! NSManagedObject
                
                if dictDataSalesDB .isKind(of: EmployeeBlockTime.self) {
                    
                }else if dictDataSalesDB .isKind(of: TaskAppointmentVC.self) {
                    aryTaskss.add(dictDataSalesDB)
                    
                }else if dictDataSalesDB .isKind(of: WorkOrderDetailsService.self) {
                    aryWorkOrderDetailsService.add(dictDataSalesDB)
                    
                }else{
                    arySalesService.add(dictDataSalesDB)
                    
                }
            }
            
            let aryWOTemp = aryWorkOrderDetailsService.filter { (activity) -> Bool in
                return "\((activity as! NSManagedObject).value(forKey: "workorderId")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "workOrderNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "thirdPartyAccountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "servicesAddress1")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceAddress2")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCity")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceState")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCountry")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceZipcode")!)".lowercased().contains(searchText.lowercased()) || Global().strCombinedAddressService(for: activity as? NSManagedObject).lowercased().contains(searchText.lowercased())
                        }
            
            let arySales = arySalesService.filter { (activity) -> Bool in
                          return "\((activity as! NSManagedObject).value(forKey: "leadNumber")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "leadName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "servicesAddress1")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceAddress2")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCity")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceState")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCountry")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceZipcode")!)".lowercased().contains(searchText.lowercased()) || Global().strCombinedAddressService(for: activity as? NSManagedObject).lowercased().contains(searchText.lowercased())}
            
            let aryTaskTemp = aryTaskss.filter { (activity) -> Bool in
                return "\((activity as! NSManagedObject).value(forKey: "taskName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "crmCompanyName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "crmContactName")!)".lowercased().contains(searchText.lowercased())}
            
            let aryFilter = NSMutableArray()
            for item in aryWOTemp {
                aryFilter.add(item)
            }
            for item in arySales {
                aryFilter.add(item)
            }
            for item in aryTaskTemp {
                aryFilter.add(item)
            }
            
            self.arrayAppointment = NSArray()
            self.arrayAppointment = aryFilter
            
            self.tblViewAppointment.reloadData()
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchAutocomplete(searchText: searchText)
        
    }
}
// MARK: -
// MARK: ----------Filter Delegate---------
extension AppointmentVC : FilterAppoinmentNewProtocol{
    func getDataFromFilterAppoinmentNew(dict: NSDictionary, tag: Int) {
        
        self.segment.selectedSegmentIndex = 1
        
        
        print(dict)
        self.dictFilter = NSMutableDictionary()
        self.dictFilter = dict.mutableCopy() as! NSMutableDictionary
        
        if self.dictFilter.count > 0 {
            
            if "\(self.dictFilter.value(forKey: "FromeDatestr") ?? "")".count > 0 {
                
                lblTodayDate.text = "\(self.dictFilter.value(forKey: "FromeDatestr") ?? "") - " + "\(self.dictFilter.value(forKey: "Todatestr") ?? "")"
                
            } else {
                
                lblTodayDate.text = ""
                
            }
            
            if "\(self.dictFilter.value(forKey: "Status") ?? "")".count > 0 {
                
                lblStatus.text = "\(self.dictFilter.value(forKey: "Status") ?? "")"
                
            } else {
                
                lblStatus.text = ""
                
            }
            
        }else{
            
            lblTodayDate.text = "Today, " + "\(Global().strGetCurrentDate(inFormat: "d MMM yyyy") ?? "")"
            lblStatus.text = "InComplete"
            
        }
        
        searchBarAppointment.text = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            if(dict.count == 0){
                self.arrayAppointmentData = NSArray()
                self.arrayAppointment = NSArray()
                self.arrayAppointment = self.arrayAppointmentDataMain
                self.arrayAppointmentData  = self.arrayAppointmentDataMain
                self.tblViewAppointment.reloadData()
            }else{
                self.FilterData(dict: dict)
            }
            
        }
        
    }
    
}


/*
 
 
 let aryWOTemp = aryWorkOrderDetailsService.filter { (activity) -> Bool in
                 return "\((activity as! NSManagedObject).value(forKey: "workorderId")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "workOrderNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "thirdPartyAccountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "servicesAddress1")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceAddress2")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCity")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceState")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCountry")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceZipcode")!)".lowercased().contains(searchText.lowercased())
             }
 
 let arySales = arySalesService.filter { (activity) -> Bool in
               return "\((activity as! NSManagedObject).value(forKey: "leadNumber")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "leadName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "firstName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "lastName")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "accountNo")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "servicesAddress1")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceAddress2")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCity")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceState")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceCountry")!)".lowercased().contains(searchText.lowercased()) || "\((activity as! NSManagedObject).value(forKey: "serviceZipcode")!)".lowercased().contains(searchText.lowercased())}
 
 
 */


// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension AppointmentVC : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           self.setTopMenuOption()
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}




// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension AppointmentVC {
    
    func getApprovalCheckList() -> NSArray {
   
        let strBranchSysName = "\(Global().getEmployeeBranchSysName() ?? "")"
        
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") is NSArray){
                let aryTemp = (dictMaster.value(forKey: "ApprovalCheckListMasterExtSerDcs") as! NSArray).filter { (task) -> Bool in
                    
                    //return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true"))  && "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")
                    
                    return "\((task as! NSDictionary).value(forKey: "BranchSysName")!)".contains("\(strBranchSysName)")

                }
                return aryTemp as NSArray
                
            }
            
        }
        return NSArray()
    }
    
    // MARK: --------------------------- Nilind ---------------------------
    
    func dateAfterDays(noOfDays : Int, strDate : String) -> Bool  {
        
        let todayStringDate = Global().strCurrentDateFormatted("MM/dd/yyyy hh:mm a", "EST")
        
        var today = changeStringDateToESTDate(strDate: "\(todayStringDate ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a")
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")

        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let todayDate = dateFormatter.date(from: todayStringDate!)
        today = todayDate!
        
        let date = dateFormatter.date(from: strDate)
        
        if let dateNew = date   //date new - work order date + no days  // today - current date
        {
            let modifiedDate = Calendar.current.date(byAdding: .day, value: noOfDays, to: dateNew)!
            
            print(modifiedDate)
            
            if today > modifiedDate
            {
                return true
            }
            else
            {
                return false
            }
            
        }
        else
        {
            return false
        }
    }
    
    
    func getPendingWorkOrders(arrayObj : NSArray) -> (arrayPendingWo : NSArray, strMessage : String) {
        
        let arrMutable = NSMutableArray()
        var arrFinal = NSArray()
        var strMessage = ""
        
        for item in arrayObj
        {
            let objMatched = item as! NSManagedObject
            
            if objMatched .isKind(of: WorkOrderDetailsService.self)
            {
                let strWoStageId = "\(objMatched.value(forKey: "wdoWorkflowStageId") ?? "")"
                //let strWoFlowStage = "\(objMatched.value(forKey: "wdoWorkflowStage") ?? "")"
                let wdoWorkflowStatus = "\(objMatched.value(forKey: "wdoWorkflowStatus") ?? "")".lowercased()
                let strWoId = "\(objMatched.value(forKey: "workOrderNo") ?? "")"
                
                if !strWoStageId.isEmpty && Double(strWoStageId) != 0 && (wdoWorkflowStatus.lowercased() != WdoWorkflowStatusEnum.Complete.rawValue.lowercased() && wdoWorkflowStatus.lowercased() != WdoWorkflowStatusEnum.Completed.rawValue.lowercased())
                {
                    let arrTemp = aryApprovalCheckList.filter { (dict) -> Bool in
                        
                        return "\((dict as! NSDictionary).value(forKey: "ApprovalCheckListId") ?? "")" == strWoStageId && ("\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")" == "1" || "\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")".caseInsensitiveCompare("true") == .orderedSame) && ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector")//(("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector") || ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Branch"))
                        
                    } as NSArray
                    
                    if arrTemp.count > 0
                    {
                        let dictObject = arrTemp.object(at: 0) as! NSDictionary
                        let noOfDays = Int("\(dictObject.value(forKey: "NoOfDays") ?? "")") ?? 0
                        
                        if dateAfterDays(noOfDays: noOfDays, strDate: changeStringDateToGivenFormatEST(strDate: "\(objMatched.value(forKey: "wdoWoStageDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a"))
                        {
                            arrMutable.add(strWoId) // strWOId = Workorder No
                        }
                    }
                }
                else if ((strWoStageId.isEmpty || Double(strWoStageId) == 0) && wdoWorkflowStatus == WdoWorkflowStatusEnum.Pending.rawValue.lowercased())
                 {
                     let arrTemp = aryApprovalCheckList.filter { (dict) -> Bool in
                         
                         return ("\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")" == "1" || "\((dict as! NSDictionary).value(forKey: "IsScreenLock") ?? "")".caseInsensitiveCompare("true") == .orderedSame) && ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector")//(("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Inspector") || ("\((dict as! NSDictionary).value(forKey: "WdoWorkflowStageType") ?? "")" == "Branch"))
                         
                     } as NSArray
                     if arrTemp.count > 0
                     {
                         let dictObject = arrTemp.object(at: 0) as! NSDictionary
                         let noOfDays = Int("\(dictObject.value(forKey: "NoOfDays") ?? "")") ?? 0
                         
                         if dateAfterDays(noOfDays: noOfDays, strDate: changeStringDateToGivenFormatEST(strDate: "\(objMatched.value(forKey: "wdoWoStageDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a"))
                         {
                             arrMutable.add(strWoId) // strWOId = Workorder No
                         }
                     }
                 }
            }
        }
        
        arrFinal = arrMutable.mutableCopy() as! NSArray
        
        strMessage = arrFinal.componentsJoined(by: ",")
        
        if arrFinal.count == 1
        {
            strMessage = "You can not Process Other Work Order Beacuse Wo # \(strMessage) is Pending"
        }
        else
        {
            strMessage = "You can not Process Other Work Order Beacuse Wo # \(strMessage) are Pending"
        }
        
        return (arrFinal, strMessage)
    }
    
}



// MARK: - --------Enum WdoWorkflow---------


enum WdoWorkflowInitialStageTypeEnum : String
{
    case InspectorCorrection = "Inspector Correction"
}
enum WdoWorkflowStageTypeEnum : String
{
    
    case Corporate = "Corporate"
    case Inspector = "Inspector"
    case ServiceManager = "Service Manager"
    case Branch = "Branch"
    
}

enum WdoWorkflowStatusEnum : String
{
    case Pending = "Pending"
    case Complete = "Complete"
    case Completed = "Completed"
    
}

enum WdoWorkflowActionEnum : String
{
    
    case Approved = "Approved"
    case Declined = "Declined"
    case Signed = "Signed"
    case Modified = "Modified"
    case Completed = "Completed"
    
}
// MARK: - -----------------------------------AddBlockTimeProtocol -----------------------------------
extension AppointmentVC : AddBlockTimeProtocol{
    
    func getDataOnAddBlockTime(arrAppointmentData: NSArray) {
        self.mergeAllData()
    }
}

extension AppointmentVC : AddedBlockTimeProtocol{
    func refreshAppointmentList(strTodayAll: String) {
        self.strTodayAll = strTodayAll
        self.mergeAllData()
    }
}
