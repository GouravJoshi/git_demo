//
//  DummyVC.swift
//  DPS
//
//  Created by Saavan Patidar on 13/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class DummyVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sleep(1)

        self.dismiss(animated: false, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
