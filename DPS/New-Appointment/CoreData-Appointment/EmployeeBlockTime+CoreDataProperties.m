//
//  EmployeeBlockTime+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import "EmployeeBlockTime+CoreDataProperties.h"

@implementation EmployeeBlockTime (CoreDataProperties)

+ (NSFetchRequest<EmployeeBlockTime *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"EmployeeBlockTime"];
}

@dynamic companyKey;
@dynamic strEmpId;
@dynamic employeeBlockTimeAssignmentId;
@dynamic employeeBlockTimeId;
@dynamic blockType;
@dynamic fromDate;
@dynamic toDate;
@dynamic titleBlockTime;
@dynamic descriptionBlockTime;
@dynamic isActive;
@dynamic employeeName;
@dynamic employeeNo;
@dynamic modifyDate;
@dynamic userName;
@dynamic fromDateFormatted;

@end
