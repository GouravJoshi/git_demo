//
//  EmployeeBlockTime+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import "EmployeeBlockTime+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface EmployeeBlockTime (CoreDataProperties)

+ (NSFetchRequest<EmployeeBlockTime *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *strEmpId;
@property (nullable, nonatomic, copy) NSString *employeeBlockTimeAssignmentId;
@property (nullable, nonatomic, copy) NSString *employeeBlockTimeId;
@property (nullable, nonatomic, copy) NSString *blockType;
@property (nullable, nonatomic, copy) NSString *fromDate;
@property (nullable, nonatomic, copy) NSString *toDate;
@property (nullable, nonatomic, copy) NSString *titleBlockTime;
@property (nullable, nonatomic, copy) NSString *descriptionBlockTime;
@property (nullable, nonatomic, copy) NSString *isActive;
@property (nullable, nonatomic, copy) NSString *employeeName;
@property (nullable, nonatomic, copy) NSString *employeeNo;
@property (nullable, nonatomic, copy) NSString *modifyDate;
@property (nullable, nonatomic, copy) NSString *userName; //fromDateFormatted
@property (nullable, nonatomic, copy) NSDate *fromDateFormatted; //fromDateFormatted

@end

NS_ASSUME_NONNULL_END
