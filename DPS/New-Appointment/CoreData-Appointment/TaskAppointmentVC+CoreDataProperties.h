//
//  TaskAppointmentVC+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import "TaskAppointmentVC+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TaskAppointmentVC (CoreDataProperties)

+ (NSFetchRequest<TaskAppointmentVC *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *strEmpId;
@property (nullable, nonatomic, copy) NSString *leadTaskId;
@property (nullable, nonatomic, copy) NSString *followUpFromTaskId;
@property (nullable, nonatomic, copy) NSString *taskName;
@property (nullable, nonatomic, copy) NSString *dueDate;
@property (nullable, nonatomic, copy) NSString *reminderDate;
@property (nullable, nonatomic, copy) NSString *endDate;
@property (nullable, nonatomic, copy) NSString *clientCreatedDate;
@property (nullable, nonatomic, copy) NSString *clientModifiedDate;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *assignedTo;
@property (nullable, nonatomic, copy) NSString *priority;
@property (nullable, nonatomic, copy) NSString *taskTypeId;
@property (nullable, nonatomic, copy) NSString *assignedByStr;
@property (nullable, nonatomic, copy) NSString *assignedToStr;
@property (nullable, nonatomic, copy) NSString *priorityStr;
@property (nullable, nonatomic, copy) NSString *crmContactName;
@property (nullable, nonatomic, copy) NSString *crmCompanyName;
@property (nullable, nonatomic, copy) NSString *fromDate;
@property (nullable, nonatomic, copy) NSString *modifyDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *companyKey;//dueDateFormatted
@property (nullable, nonatomic, copy) NSDate *dueDateFormatted;//dueDateFormatted

@end

NS_ASSUME_NONNULL_END
