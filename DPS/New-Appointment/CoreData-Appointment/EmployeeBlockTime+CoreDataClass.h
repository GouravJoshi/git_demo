//
//  EmployeeBlockTime+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeBlockTime : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "EmployeeBlockTime+CoreDataProperties.h"
