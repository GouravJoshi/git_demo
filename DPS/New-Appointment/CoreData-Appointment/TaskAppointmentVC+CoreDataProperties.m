//
//  TaskAppointmentVC+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import "TaskAppointmentVC+CoreDataProperties.h"

@implementation TaskAppointmentVC (CoreDataProperties)

+ (NSFetchRequest<TaskAppointmentVC *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"TaskAppointmentVC"];
}

@dynamic strEmpId;
@dynamic leadTaskId;
@dynamic followUpFromTaskId;
@dynamic taskName;
@dynamic dueDate;
@dynamic reminderDate;
@dynamic endDate;
@dynamic clientCreatedDate;
@dynamic clientModifiedDate;
@dynamic modifiedDate;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic createdBy;
@dynamic status;
@dynamic assignedTo;
@dynamic priority;
@dynamic taskTypeId;
@dynamic assignedByStr;
@dynamic assignedToStr;
@dynamic priorityStr;
@dynamic crmContactName;
@dynamic crmCompanyName;
@dynamic fromDate;
@dynamic modifyDate;
@dynamic userName;
@dynamic companyKey;
@dynamic dueDateFormatted;

@end
