//
//  TaskAppointmentVC+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 22/09/20.
//
//  Test

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaskAppointmentVC : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TaskAppointmentVC+CoreDataProperties.h"
