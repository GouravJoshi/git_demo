//
//  AppoinmentFilterVC.swift
//  DPS
//
//  Created by NavinPatidar on 9/15/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar
//  2020 Saavan Patidar
//  peSTream 2020
//  2021
// 2021


import UIKit
protocol FilterAppoinmentNewProtocol : class
{
    func getDataFromFilterAppoinmentNew(dict : NSDictionary ,tag : Int)
}
class AppoinmentFilterVC: UIViewController {
   weak var delegate: FilterAppoinmentNewProtocol?
   var strtag = Int()
    var dictFilter = NSMutableDictionary()
    //MARK:
    //MARK: ---------IBOutlet
    @IBOutlet weak var tv_Filter: UITableView!
    @IBOutlet weak var txtfld_FName: ACFloatingTextField!
    @IBOutlet weak var txtfld_LName: ACFloatingTextField!
    @IBOutlet weak var txtfld_ACnumber: ACFloatingTextField!
    @IBOutlet weak var txtfld_WOnumber: ACFloatingTextField!
    @IBOutlet weak var txtfld_OPPnumber: ACFloatingTextField!
    
    @IBOutlet weak var txtfld_status: ACFloatingTextField!
    @IBOutlet weak var txtfld_Todate: ACFloatingTextField!
    @IBOutlet weak var txtfld_FromDate: ACFloatingTextField!
           //MARK:
            //MARK: ---------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
  tv_Filter.estimatedRowHeight = 70.0
        
        
        if(dictFilter.count != 0){
            
            
            txtfld_FName.text = "\(dictFilter.value(forKey: "FName")!)"
            txtfld_LName.text = "\(dictFilter.value(forKey: "LName")!)"
            txtfld_ACnumber.text = "\(dictFilter.value(forKey: "ACNumber")!)"
            txtfld_WOnumber.text = "\(dictFilter.value(forKey: "WONumber")!)"
            txtfld_OPPnumber.text = "\(dictFilter.value(forKey: "OppNumber")!)"
            txtfld_status.text = "\(dictFilter.value(forKey: "Status")!)"
            txtfld_Todate.text = "\(dictFilter.value(forKey: "Todatestr")!)"
            txtfld_FromDate.text = "\(dictFilter.value(forKey: "FromeDatestr")!)"
          
                                                   
        }else{
            
            txtfld_status.text = "InComplete"
            txtfld_Todate.text = "\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")"
            txtfld_FromDate.text = "\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")"
            
        }
        
    }
    //MARK:
          //MARK: ----------IBAction
       
       @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
           self.navigationController?.popViewController(animated: false)
       }
       @IBAction func action_Clear(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: alertMessage, message: alertFilterClearNew, preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                
                //self.clearData()
                
                let dict = NSMutableDictionary()
                dict.setValue("", forKey: "FName")
                dict.setValue("", forKey: "LName")
                dict.setValue("", forKey: "ACNumber")
                dict.setValue("", forKey: "WONumber")
                dict.setValue("", forKey: "OppNumber")
                dict.setValue("InComplete", forKey: "Status")
                dict.setValue("\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")", forKey: "Todatestr")
                dict.setValue("\(Global().strGetCurrentDate(inFormat: "MM/dd/yyyy") ?? "")", forKey: "FromeDatestr")
                dict.setValue(false, forKey: "FromFilterAppointment")
                   
                   
                self.delegate?.getDataFromFilterAppoinmentNew(dict: dict, tag: self.strtag)
                          
                self.navigationController?.popViewController(animated: false)
                
               }))
               alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
               self.present(alert, animated: true, completion: nil)
        
    }
    
    func clearData() {
        
        txtfld_FName.text = ""
        txtfld_LName.text = ""
        txtfld_ACnumber.text = ""
        txtfld_WOnumber.text = ""
        txtfld_OPPnumber.text = ""
        txtfld_status.text = ""
        txtfld_Todate.text = ""
        txtfld_FromDate.text = ""
        
    }
    
    @IBAction func action_Save(_ sender: Any) {
        self.view.endEditing(true)
        
        if(txtfld_FName.text == "" && txtfld_LName.text == "" && txtfld_ACnumber.text == "" && txtfld_WOnumber.text == "" && txtfld_OPPnumber.text == "" && txtfld_status.text == "" && txtfld_Todate.text == ""  && txtfld_FromDate.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one option.", viewcontrol: self)
        }
     
        else if (txtfld_Todate.text != "" && txtfld_FromDate.text != "" ){
        
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let firstDate = formatter.date(from: "\(txtfld_FromDate.text!)")
            let secondDate = formatter.date(from: "\(txtfld_Todate.text!)")
            if firstDate?.compare(secondDate!) == .orderedAscending  || firstDate?.compare(secondDate!) == .orderedSame{
               
                saveFilterData()
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To date should be greater then from date or equal.", viewcontrol: self)
            }
        }
        else if (txtfld_Todate.text == "" && txtfld_FromDate.text == "" ){
            saveFilterData()
        }
        else{
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select From Date & To Date.", viewcontrol: self)
        }
    }
    func saveFilterData()  {
        let dict = NSMutableDictionary()
        dict.setValue(txtfld_FName.text!, forKey: "FName")
        dict.setValue(txtfld_LName.text!, forKey: "LName")
        dict.setValue(txtfld_ACnumber.text!, forKey: "ACNumber")
        dict.setValue(txtfld_WOnumber.text!, forKey: "WONumber")
        dict.setValue(txtfld_OPPnumber.text!, forKey: "OppNumber")
        dict.setValue(txtfld_status.text!, forKey: "Status")
        dict.setValue(txtfld_Todate.text!, forKey: "Todatestr")
        dict.setValue(txtfld_FromDate.text!, forKey: "FromeDatestr")
        dict.setValue(true, forKey: "FromFilterAppointment")
        delegate?.getDataFromFilterAppoinmentNew(dict: dict, tag: self.strtag)
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_FromDate(_ sender: UIButton) {
        sender.tag = 1
        gotoDatePickerView(sender: sender, strType: "Date", minTime: false , maxTime: false)
      }
     @IBAction func action_Todate(_ sender: UIButton) {
         sender.tag = 2

         gotoDatePickerView(sender: sender, strType: "Date", minTime: false , maxTime: false)
      }
    @IBAction func action_OnStatus(_ sender: UIButton) {
        let aryStatus = NSMutableArray()
        
        let defsLead = UserDefaults.standard
        
        let arrTemp = defsLead.value(forKey: "LeadStatusMasters") as! NSArray
        
        let arrOfLeadStatusMasters = arrTemp.mutableCopy() as! NSMutableArray
        
        var dict = NSMutableDictionary()
        dict.setValue("All", forKey: "Name")
        aryStatus.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("InComplete", forKey: "Name")
        aryStatus.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Reset", forKey: "Name")
        aryStatus.add(dict)
        for k in 0..<(arrOfLeadStatusMasters.count) {
            let dictData = arrOfLeadStatusMasters[k] as? NSDictionary
            if let value = dictData?["StatusName"] {
                dict = NSMutableDictionary()
                dict.setValue("\(value)", forKey: "Name")
                aryStatus.add(dict)
            }
        }
        
   
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = 59
        vc.arySelectedItems = NSMutableArray()
        if aryStatus.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: aryStatus)
            self.present(vc, animated: false, completion: {})
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    

    
    //MARK:
    //MARK: ---------Open Picker For Date Select
    
    func gotoDatePickerView(sender: UIButton, strType:String , minTime : Bool , maxTime : Bool )  {
        
        let dictDataDates = SalesCoreDataVC().dictdates() as NSDictionary
        
        let minDate = dictDataDates.value(forKey: "fromDateMinimum") as! Date
        let maxDate = dictDataDates.value(forKey: "toDateMaximum") as! Date

        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = minTime
        vc.chkForMaxDate = maxTime
        vc.maxDate = maxDate
        vc.minDate = minDate
        vc.chkCustomDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        // vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
}
// MARK: -
// MARK: ----------DatePickerProtocol-------
extension AppoinmentFilterVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        if(tag == 1)
        {
            txtfld_FromDate.text = strDate
        }
        if(tag == 2)
        {
            txtfld_Todate.text = strDate
        }
       
    }
}
// MARK: -
// MARK: - Selection CustomTableView
extension AppoinmentFilterVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(dictData.count != 0){
            if(tag == 59){
                txtfld_status.text = "\(dictData.value(forKey: "Name")!)"
            }
        }
    }
}
// MARK: -
// MARK: -  UITextFieldDelegate
extension AppoinmentFilterVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if range.location == 0 && string == " " {
                return false
            }
            return true
    }
}
