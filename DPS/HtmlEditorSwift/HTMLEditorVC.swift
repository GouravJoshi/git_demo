//
//  HTMLEditorVC.swift
//  DPS
//
//  Created by Rakesh Jain on 31/10/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan Patidar
//  saavan Patidar

import UIKit
import RichEditorView

protocol HTMLEditorDelegate: class
{
    func didReceiveHTMLEditorText(strText : String)
    func didReceiveRenewalHTMLEditorText(strText : String, managedObject: NSManagedObject)
}
protocol HTMLEditorForCommercialDelegate: class
{
    func didReceiveHTMLEditorText(strText : String, strFrom: String)
}


class HTMLEditorVC: UIViewController {

    
    @IBOutlet weak var editorView: RichEditorView!
    var delegate: HTMLEditorDelegate?
    var matchesObject = NSManagedObject()
    var delegateCommerical: HTMLEditorForCommercialDelegate?

    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    
   @objc var strHtml = ""
   @objc var strfrom = String()
   @objc var strLeadId = String()
   @objc var strServiceId = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Type some text..."
        editorView.html = strHtml
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        var options = toolbar.options
        options.append(item)
        toolbar.options = options
    }
    
    @IBAction func action_back(_ sender: UIButton) {
        
        if strfrom == "sales"
        {
            nsud.set(strHtml, forKey: "htmlContents")
            strfrom = ""
            nsud.synchronize()
        }
        
        dismiss(animated: false, completion: nil)

    }
    
    @IBAction func action_Save(_ sender: UIButton)
    {
        
        var strHtmlEditorContent = editorView.contentHTML

        if strHtmlEditorContent == "<br>" {
            
            strHtmlEditorContent = ""
            
        }
        
        if strfrom == "SalesResidentialNonStan"
        {
            updateNonStandardServiceDescription(strDescription:strHtmlEditorContent)
        }
        else if strfrom == "SalesCommercialNonStan"
        {
            updateNonStandardServiceDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "SalesCommercialTarget"
        {
            updateNonStandardServiceDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "SalesCommercialScope"
        {
            updateNonStandardServiceDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "SalesCommercialService"
        {
            updateNonStandardServiceDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "SalesServiceDecStandard"
        {
            delegate?.didReceiveHTMLEditorText(strText: strHtmlEditorContent)
        }
        else if strfrom == "ForRenewal"
        {
            delegate?.didReceiveRenewalHTMLEditorText(strText: strHtmlEditorContent, managedObject: matchesObject)
        }
        else if strfrom == "EditServiceDescription"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)
        }
        else if strfrom == "EditInternalNotes"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)
        }
        else if strfrom == "EditTermsAndCondition"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)
        }
        else if strfrom == "EditTargetFromSelectService"{
           updateCommericalTargetDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "EditScopeFromSelectService"{
          updateCommericalScopeDescription(strDescription: strHtmlEditorContent)
        }
        else if strfrom == "EitherTargetOrScope"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)

        }
        else if strfrom == "EditIntroductionLetterDecriptionConfig"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)
        }
        
        else if strfrom == "EditConfigureTermsOfService"{
            delegateCommerical?.didReceiveHTMLEditorText(strText: strHtmlEditorContent, strFrom: strfrom)
        }
        
        else
        {
            
            nsud.set(true, forKey: "EditedHtmlContents")
            nsud.set(strHtmlEditorContent, forKey: "htmlContents")
            nsud.synchronize()
            
        }
        strfrom = ""
        dismiss(animated: false, completion: nil)

    }
    
    func updateCommericalTargetDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("targetDescription")
        arrOfValues.add(strDescription)
        
        arrOfKeys.add("isChangedTargetDesc")
        arrOfValues.add("true")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@ && targetSysName == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateCommericalScopeDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("scopeDescription")
        arrOfValues.add(strDescription)
        
        arrOfKeys.add("isChangedScopeDesc")
        arrOfValues.add("true")
        
        
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@ && scopeSysName == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    
    
    func updateNonStandardServiceDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("serviceDescription")
        arrOfValues.add(strDescription)
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", strLeadId, strServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateStandardServiceDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add(strDescription)
        arrOfValues.add("")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", "", ""), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateTargetDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add(strDescription)
        arrOfValues.add("")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", "", ""), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    func updateScopeDescription(strDescription : String)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add(strDescription)
        arrOfValues.add("")
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", "", ""), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
        }
        
    }
    

}
extension HTMLEditorVC: RichEditorDelegate {
    
    /* func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
     if content.isEmpty {
     htmlTextView.text = "HTML Preview"
     } else {
     htmlTextView.text = content
     }
     }*/
    
}
extension HTMLEditorVC: RichEditorToolbarDelegate
{
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if toolbar.editor?.hasRangeSelection == true {
            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
        }
    }
}

