//
//  AppDelegate.m
//  DPS
//
//  Created by Rakesh Jain on 17/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "AppDelegate.h"
#import "Global.h"
#import "Header.h"
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UserNotifications/UserNotifications.h>
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
#import "AllImportsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CoreLocation.h>
#import "AllImportsViewController.h"
#import "DPS-Swift.h"
#import <CallKit/CallKit.h>
#import <FIRCrashlytics.h>
@import GoogleMaps;
@import GooglePlaces;

@interface AppDelegate () <CLLocationManagerDelegate,CXCallObserverDelegate,FIRMessagingDelegate>
{
    
    Global *global;
    NSString *strServiceUrlMain;
    CLLocationManager *locationManager;
    AppDelegate *app;
    NSString *strLattitude,*strLongitude,*strSpeed;
    NSTimer *timer;
    UIApplicationShortcutItem *shortCutItems;

}
@end

@implementation AppDelegate


- (void)saveOrientation :(UIInterfaceOrientationMask )Orientation{
        orientationLock = Orientation;
   
}



-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    
    if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        statusBar.backgroundColor = [UIColor themeColor];
        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        
    } else {
        
        // Fallback on earlier versions
        //[[UIApplication sharedApplication] setStatusBarHidden:NO];
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor themeColor];//set whatever color you like
            
        }
            
    }
    
    return orientationLock;
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
          orientationLock = UIInterfaceOrientationMaskAll;

    }else{
        orientationLock = UIInterfaceOrientationMaskPortrait;

    }
      
    global = [[Global alloc] init];

    // check if app update available
    
    [global checkIfAppUpdateAvilable];
    
    CXCallObserver *callObserver = [[CXCallObserver alloc] init];
    [callObserver setDelegate:self queue:nil];
    self.callObserver = callObserver;
    
    //Nilind
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
        [locationManager setAllowsBackgroundLocationUpdates:YES];
    }
    locationManager.desiredAccuracy = 45;
    locationManager.distanceFilter = 100;
    [locationManager startUpdatingLocation];
    timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                             target:self
                                           selector:@selector(startTrackingBg)
                                           userInfo:nil
                                            repeats:YES];
    
    //End
    
    //Google Map Setup
    [GMSServices provideAPIKey:@"AIzaSyA7mLJumXM32bv7twuY5n6gDVpzdLIshuk"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyA7mLJumXM32bv7twuY5n6gDVpzdLIshuk"];

    
    NSUserDefaults *defs111=[NSUserDefaults standardUserDefaults];
    [defs111 setBool:YES forKey:@"AppTerminated"];
    [defs111 synchronize];
    
    //[Fabric with:@[[Crashlytics class]]];
    
    [FTIndicator setIndicatorStyleToDefaultStyle];
    
    /*if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Permission has been Granted.
                    
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
        
        // Permission has been Granted.
        
    }
    

    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Permission has been Granted.
                    
                });
            } else {
                // Permission has been denied.
            }
        }];
    } else {
        
        // Permission has been Granted.
        
    }*/
    
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        NSLog(@"%zd", [group numberOfAssets]);
    } failureBlock:^(NSError *error) {
        if (error.code == ALAssetsLibraryAccessUserDeniedError) {
            NSLog(@"user denied access, code: %zd", error.code);
        } else {
            NSLog(@"Other error code: %zd", error.code);
        }
    }];
    
    
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSString *strError;
    @try {
        strError=[dictLoginData valueForKey:@"Error"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    if (strError.length==0) {
        
        strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
        
    }else{
        
    }
    
    //============================================================================
    //============================================================================
    

    //============================================================================
    //============================================================================
    BOOL sept292020 = [[NSUserDefaults standardUserDefaults] boolForKey: @"sept292020"];
    if (!sept292020) {
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setValue:@"Old" forKey:@"AppointmentFlow"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"sept292020"];
    }
    
    BOOL downloaded = [[NSUserDefaults standardUserDefaults] boolForKey: @"firstInstall_4Dec2018"];
    if (!downloaded) {
        double uniqueId=0;
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setDouble:uniqueId forKey:@"uniqueIdTask"];
        [defs setBool:YES forKey:@"firstAudio"];
        [defs setBool:YES forKey:@"firstCamera"];
        [defs setBool:YES forKey:@"firstGallery"];
        [defs setBool:YES forKey:@"imageCaptionSetting"];
        [defs setBool:NO forKey:@"graphDebug"];
        [defs setBool:YES forKey:@"imageCaptionSettingSales"];
        [defs setBool:YES forKey:@"imageCaptionSetting"];
        [defs setBool:YES forKey:@"imageCaptionSettingSales"];
        [defs setBool:YES forKey:@"AllAppointments"];
        [defs setBool:YES forKey:@"SortByScheduleDate"];
        [defs setBool:YES forKey:@"SortByScheduleDateAscending"];
        [defs setBool:YES forKey:@"SortByModifiedDateAscending"];
        NSArray *tempArr = [[NSArray alloc]init];
        [defs setObject:tempArr forKey:@"AppointmentStatus"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"firstInstall_4Dec2018"];
    }
    
    BOOL downloaded1 = [[NSUserDefaults standardUserDefaults] boolForKey: @"firstInstall_13June2019"];
    if (!downloaded1) {
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setObject:[global strCurrentDateFormatted:@"MM/dd/yyyy hh:mm:ss a" :@"EST"] forKey:@"LastDateSignedAgreements"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"firstInstall_13June2019"];
        
    }
    
    BOOL downloaded13Apr2021 = [[NSUserDefaults standardUserDefaults] boolForKey: @"firstInstall_13Apr2021"];
    if (!downloaded13Apr2021) {
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"isEnableAutoPropertyInformation"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"firstInstall_13Apr2021"];
        
    }
    
    BOOL downloaded7May2021 = [[NSUserDefaults standardUserDefaults] boolForKey: @"firstInstall_7May2021"];
    if (!downloaded7May2021) {
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        [defs setBool:NO forKey:@"isLoadSalesDynamicFormInBackground"];
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey: @"firstInstall_7May2021"];
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"fromAppDelegate"];
    NSString *strIndex=[NSString stringWithFormat:@"%@",@"1"];
    [defs setObject:strIndex forKey:@"index"];
    [defs synchronize];
    
    
    //============================================================================
    //============================================================================
    //Local Notification
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if (!error) {
                                      NSLog(@"request succeeded!");
                                  }
                              }];
    }else  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        
    }
    
    /*
     
     NSError *error;
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
     NSString *documentsDirectory = [paths objectAtIndex:0];
     
     NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/LogFiles"];
     if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
     [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
     
     NSString *fileName =[NSString stringWithFormat:@"%@.log",@"peSTream"];
     NSString *logFilePath = [dataPath stringByAppendingPathComponent:fileName];
     freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
     
     */
    
    
    //Firebase Changes For Notiifcation
    
    // Load a named file.
    
    /*NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GoogleService-Info-FireStore" ofType:@"plist"];
    
    FIROptions *optionss = [[FIROptions alloc] initWithContentsOfFile:filePath];
    
    [FIRApp configureWithOptions:optionss];*/
    
    [FIRApp configure];
    [self connectToFCM];
                            
    //WebService *objWebServiceNew = [[WebService alloc] init];
    //[objWebServiceNew configureGooglePlistForFireStore];
    
    [FIRCrashlytics crashlytics];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    /*NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", fireBaseToken);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
    [userDefaults synchronize];*/
    
     // New Messaging for fireBase
     
    [FIRMessaging messaging].delegate = self;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:FIRMessagingRegistrationTokenRefreshedNotification object:nil];

    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];
    
    
    // determine whether we've launched from a shortcut item or not
    shortCutItems = [launchOptions valueForKey:UIApplicationLaunchOptionsShortcutItemKey];
    
    if (shortCutItems) {
        
        
        
    } else {
        
        [self isIPHONEorIPAD];
        
    }
    
    //[self isIPHONEorIPAD];
    
    // [self goToTermiteFlow];
    
    /*
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        
        statusBar.backgroundColor = [UIColor themeColor];//set whatever color you like
    }
    */
    
    /*
    // Check For App Version On app Store

    NSDate *lastAlertDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"lastAlertDate"];
    
    if(lastAlertDate==nil)
    {
        [self checkAppVersion];
    }
    else
    {
        if(![[NSCalendar currentCalendar] isDateInToday:lastAlertDate])
        {
            
            [self checkAppVersion];
            
            NSDate *today= [NSDate date];
            [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"lastAlertDate"];
            
        }
    }
    */
    
    /*if (@available(iOS 13.0, *)) {
        
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        statusBar.backgroundColor = [UIColor themeColor];
        [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        
    } else {
        
        // Fallback on earlier versions
        //[[UIApplication sharedApplication] setStatusBarHidden:NO];
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor themeColor];//set whatever color you like
            
        }
            
    }*/
    
    return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler{
    
    shortCutItems = shortcutItem;
    
}




-(void)goToTermiteFlow{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Service_iPhone"
                                                             bundle: nil];
    TermiteInspectionTexasViewController
    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"TermiteInspectionTexasViewController"];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:objByProductVC];
    [self.window makeKeyAndVisible];
    
}
-(void)isIPHONEorIPAD{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainiPad" bundle:nil];
        LoginViewControlleriPad *verificationBadgesViewController1 = (LoginViewControlleriPad *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewControlleriPad"];
        
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
        self.window.rootViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.window makeKeyAndVisible];
        
        
    }else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *verificationBadgesViewController1 = (LoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:verificationBadgesViewController1];
        self.window.rootViewController.modalPresentationStyle = UIModalPresentationFullScreen;

        [self.window makeKeyAndVisible];
        
    }
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}


// With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:deviceToken forKey:@"FirebaseToken"];
    [userDefaults synchronize];
    
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    
        [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];
    
}

/*-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    if ([pushNotificationType isEqualToString:@"Testing"]) {
        
        [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
        
        NSLog(@"Testing Token===%@",deviceToken);
        
    } else {
        
        [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
        
        NSLog(@"Production Token===%@",deviceToken);
        
    }
}
*/
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        NSLog(@"didRegisterUser");
        [application registerForRemoteNotifications];
    }
}

-(void)connectToFCM
{
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                         NSError * _Nullable error) {
       if (error != nil) {
         NSLog(@"Error fetching remote instance ID: %@", error);
       } else {
         NSLog(@"Remote instance ID token: %@", result.token);
         NSString* fireBaseToken =
           [NSString stringWithFormat:@"%@", result.token];
           NSLog(@"InstanceID token: %@", fireBaseToken);
           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
           [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
           [userDefaults synchronize];
       }
     }];

}
/*- (void)tokenRefreshNotification:(NSNotification *)notification {
    
    NSString *fireBaseToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", fireBaseToken);
    if(fireBaseToken != nil)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:fireBaseToken forKey:@"FirebaseToken"];
        [userDefaults synchronize];
        [self connectToFCM];
    }
    
}*/
- (void)applicationWillResignActive:(UIApplication *)application
{
    
    [locationManager stopUpdatingLocation];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    locationManager.pausesLocationUpdatesAutomatically = NO;
    locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [locationManager startUpdatingLocation];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppEnterdInBackgroundSaveData" object:self];

    //[locationManager stopUpdatingLocation];
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    /*__block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
     bgTask = UIBackgroundTaskInvalid;
     }];*/
    __block UIBackgroundTaskIdentifier backgroundTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
        NSLog(@"Background Time:%f",[[UIApplication sharedApplication] backgroundTimeRemaining]);
        
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
        
        backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    }];
    
    /*timer = [NSTimer scheduledTimerWithTimeInterval:20.0
     target:self
     selector:@selector(startTrackingBg)
     userInfo:nil
     repeats:YES];*/
    NSLog(@"App is running in background");
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"AppTerminated"];
    [defs synchronize];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:NO forKey:@"AppTerminated"];
    [defs synchronize];
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [self connectToFCM];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:@"No" forKey:@"YesFromShortcut"];
    [userDefaults synchronize];
    
    if (shortCutItems) {
                
        NSLog(@"We've launched from shortcut item: %@", shortCutItems.localizedTitle);
        
        if ([shortCutItems.type isEqualToString:@"com.infocrats.Pestream.AddTask"]) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:@"AddTask" forKey:@"YesFromShortcut"];
            [userDefaults synchronize];
            
        }
        
        if ([shortCutItems.type isEqualToString:@"com.infocrats.Pestream.AddActivity"]) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:@"AddActivity" forKey:@"YesFromShortcut"];
            [userDefaults synchronize];
            
        }
        
        // or have we launched Deep Link Level 2?
        if ([shortCutItems.type isEqualToString:@"com.infocrats.Pestream.AddLead"]) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:@"AddLead" forKey:@"YesFromShortcut"];
            [userDefaults synchronize];
            
        }
        
        // or have we launched Deep Link Level 2?
        if ([shortCutItems.type isEqualToString:@"com.infocrats.Pestream.AddContact"]) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:@"AddContact" forKey:@"YesFromShortcut"];
            [userDefaults synchronize];
            
        }
        
        shortCutItems = nil;
        
        [self isIPHONEorIPAD];
        
    } else {
        NSLog(@"We've launched properly.");

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:@"No" forKey:@"YesFromShortcut"];
        [userDefaults synchronize];
                
    }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppTerminatedSaveData" object:self];

    NSLog(@"App Terminated");
    [timer invalidate];
    [locationManager stopUpdatingLocation];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setBool:YES forKey:@"AppTerminated"];
    [defs synchronize];
    
    [self saveContext];
    
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "InfocratsWebSolution.DPS" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DPS" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DPS.sqlite"];
    /*
     NSError *error = nil;
     NSString *failureReason = @"There was an error creating or loading the application's saved data.";
     if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
     // Report any error we got.
     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
     dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
     dict[NSLocalizedFailureReasonErrorKey] = failureReason;
     dict[NSUnderlyingErrorKey] = error;
     error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
     // Replace this with code to handle the error appropriately.
     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
     abort();
     }
     */
    
    // For model versioning
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options: @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
//============================================================================
//============================================================================
#pragma mark- Upload Final Json METHOD
//============================================================================
//============================================================================
-(void)sendLeadToServer{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityOutBox=[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityOutBox];
    
    //    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    //    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    //
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    //    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"datenTime" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerOutBox = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerOutBox setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerOutBox performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerOutBox fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        for (int r=0; r<arrAllObj.count; r++) {
            matches =[arrAllObj objectAtIndex:r];
            
            NSString *alreadyAvailableLeadService=[matches valueForKey:@"finalJson"];
            
            NSData* data = [alreadyAvailableLeadService dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *e;
            NSDictionary *dictOffinalJson = [NSJSONSerialization JSONObjectWithData:data options:nil error:&e];
            
            //============================================================================
            //============================================================================
            NSArray *arrOfleadServiceExtDcsArray = [dictOffinalJson valueForKey:@"LeadServices"];
            
            
            for (int j=0; j<arrOfleadServiceExtDcsArray.count; j++) {
                NSDictionary *dictDataMedia=[arrOfleadServiceExtDcsArray objectAtIndex:j];
                NSArray *arrOfMediaTosend=[dictDataMedia valueForKey:@"LeadMediaExtDcs"];
                for (int k=0; k<arrOfMediaTosend.count; k++) {
                    NSDictionary *dictDataMediaToSend=[arrOfMediaTosend objectAtIndex:k];
                    NSString *strType=[dictDataMediaToSend valueForKey:@"MediaTypeSysName"];
                    NSString *strMediaName=[dictDataMediaToSend valueForKey:@"Name"];
                    
                    NSDictionary *dictData=[matches valueForKey:@"dictData"];
                    
                    NSString *strUrlSaved=[dictData valueForKey:@"strServiceUrlMain"];
                    
                    if ([strType isEqualToString:@"Image"]) {
                        [self uploadImage:strMediaName :strUrlSaved];
                    } else {
                        [self uploadAudio:strMediaName :strUrlSaved];
                    }
                }
                //MediaTypeSysName  MediaSourceSysName
            }
            
            //============================================================================
            //============================================================================
            
            [self sendingLeadToServer:matches :[NSString stringWithFormat:@"%d",r]];
        }
    }
}

-(void)sendingLeadToServer :(NSManagedObject*)managedObj :(NSString *)strIndex{
    
    NSString *strFinalJson=[managedObj valueForKey:@"finalJson"];
    
    NSString *strdateTime=[managedObj valueForKey:@"datenTime"];
    
    NSDictionary *dictData=[managedObj valueForKey:@"dictData"];
    
    NSString *strUrlSaved=[dictData valueForKey:@"strServiceUrlMain"];
    
    NSData *requestData = [strFinalJson dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strUrlSaved,UrlSendLeadToServerNew];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        strUrl = [NSString stringWithFormat:@"%@%@",strUrlSaved,UrlSendLeadToServerNew];
        
    }else{
        
        strUrl = [NSString stringWithFormat:@"%@%@",strUrlSaved,UrlSendLeadToServerNew];
        
    }
    
    NSDictionary *dictTemp;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSendLeadToServerOffline:strUrl :requestData :dictTemp :strdateTime :dictData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     // NSString *str=[NSString stringWithFormat:@"%@",response];
                     // UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Response" message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     // [alert show];
                     // [self deletefromOutbox:0];
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
}
//============================================================================
//============================================================================
#pragma mark- FetchFromCoreDataOutBox METHODS
//============================================================================
//============================================================================

-(void)FetchFromCoreDataOutBox{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityOutBox=[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context];
    
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityOutBox];
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //[request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"datenTime" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerOutBox = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerOutBox setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerOutBox performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerOutBox fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        matches = arrAllObj[0];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}


//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName :(NSString*)strUrll{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
    UIImage *imagee = [UIImage imageWithContentsOfFile:path];
    
    if (imagee == nil){
        
    }else{
            
    NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strUrll,UrlImageUpload];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Image Sent");
    }
    NSLog(@"Image Sent");
    
    }
    
}


//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName :(NSString*)strUrll
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
    NSData *audioData;
    
    audioData = [NSData dataWithContentsOfFile:path];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",strUrll,UrlImageUpload];
    
    // setting up the request object now
    NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
    [request1 setURL:[NSURL URLWithString:urlString]];
    [request1 setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:audioData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request1 setHTTPBody:body];
    
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if ([returnString isEqualToString:@"OK"])
    {
        NSLog(@"Audio Sent");
    }
}

//============================================================================
//============================================================================
#pragma mark- Delete METHOD
//============================================================================
//============================================================================

-(void)deletefromOutbox :(int)IndexToDelete{
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"OutBox" inManagedObjectContext:context]];
    //    NSSortDescriptor *sortDescriptor123;
    //    NSArray *sortDescriptors123;
    //    sortDescriptor123 = [[NSSortDescriptor alloc] initWithKey:@"supplierCompanyName" ascending:NO];
    //    sortDescriptors123 = [NSArray arrayWithObject:sortDescriptor123];
    //    [allData setSortDescriptors:sortDescriptors123];
    //    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    if (Data.count==0) {
        
    }
    else {
        NSManagedObject * data = [Data objectAtIndex:IndexToDelete];
        [context deleteObject:data];
        NSError *saveError = nil;
        [context save:&saveError];
    }
    
    // [self FetchFromCoreDataOutBox];
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    NSString *strNotification = notification.userInfo[@"Notification"];
    if ([strNotification isEqualToString:@"Image"]) {
        
        [global AlertMethod:@"Info." :@"Image Downloading Completed.\n You can check in Photos"];
        
    } else if ([strNotification isEqualToString:@"Files"])
    {
        
        [global AlertMethod:@"Info." :@"File Downloading Completed.\n You can check in Files Tab"];
        
    }
    else if ([strNotification isEqualToString:@"Audio"])
    {
        
        [global AlertMethod:@"Info." :@"Audio Downloading Completed.\n You can check in Photos"];
        
    }
    else if ([strNotification isEqualToString:@"Audio12"])
    {
        
        [global AlertMethod:@"Info." :@"Download Completed.\n You can check in Photos"];
        
    }
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        
    }
    else{
        
    }
    application.applicationIconBadgeNumber=0;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo

{
    
    NSLog(@"%@", userInfo);
    
    NSString *strMessage = [userInfo valueForKey:@"message"];
    
    NSString *strTitle = [userInfo valueForKey:@"title"];
    
    NSLog(@"Message---%@", strMessage);
    
    NSLog(@"Title---%@", strTitle);
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        
        /*NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
                    
        NSString *strLeadId=[defsLead valueForKey:@"LeadId"];
                    
        [global updateSalesModifydate:strLeadId];*/
        
        if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"])
        {
            
        }else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View"])
        {
            
            [self goToSignedAgreement];
            
        }
        
    }
    
}
- (void)goToSignedAgreement
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    BOOL isAlreadyLoggedIn=[defs boolForKey:@"YesLoggedIn"];
    
    if (isAlreadyLoggedIn) {
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CRM" bundle:nil];
            SignedAgreementiPhoneVC *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"SignedAgreementiPhoneVC"];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:loginViewController animated:YES completion:NULL];
            
        }else{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CRMiPad" bundle:nil];
            SignedAgreementiPadVC *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"SignedAgreementiPadVC"];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:loginViewController animated:YES completion:NULL];
            
        }
        
    }else{
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"SignedAgreementNotification"];
        [defs synchronize];
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:Info message:@"Please login to view Signed Agreements." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];
        
    }
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Push Type For Task Notification==== PestreamTasksCountSummary
    
    NSString *strPushType=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"PushType"]];
    
    NSDictionary *dictapsData=[userInfo valueForKey:@"aps"];
    NSDictionary *dictAlertData=[dictapsData valueForKey:@"alert"];
    NSString *strBody=[NSString stringWithFormat:@"%@",[dictAlertData valueForKey:@"body"]];
    NSString *strTitle=[NSString stringWithFormat:@"%@",[dictAlertData valueForKey:@"title"]];
    
    BOOL isActiveStateApp=[global runningInForeground];
    BOOL isBackroundStateApp=[global runningInBackground];
    BOOL isInActiveStateApp=[global runningInInActiveState];
    
    if ([strPushType isEqualToString:@"PestreamTasksCountSummary"]) {
        
        if (isActiveStateApp) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:NO forKey:@"TaskNotifications"];
            [defs synchronize];
            
        } else if (isBackroundStateApp){
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setBool:NO forKey:@"TaskNotifications"];
            [defs synchronize];
            
        } else if (isInActiveStateApp){
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            
            BOOL wasAppTerminated=[defs boolForKey:@"AppTerminated"];
            
            if (wasAppTerminated) {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:YES forKey:@"TaskNotifications"];
                [defs setBool:NO forKey:@"AppTerminated"];
                [defs synchronize];
                
                //                UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:@"Inactive" message:strBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                [alertt show];
                
            } else {
                
                NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                [defs setBool:NO forKey:@"TaskNotifications"];
                [defs synchronize];
                
            }
            
        }
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];
        
    } else if ([strPushType isEqualToString:@"SignedAgreement"]) {
    
        
        // Update Status complete WOn so that we can show Reopen button
        
        NSString *strSalesAutoLeadId=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"SalesAutoLeadId"]];
        
        global = [[Global alloc] init];

        [global updateLeadStatus:strSalesAutoLeadId];
        
        // For Signed Agreements
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
        alertt.tag=1;
        [alertt show];
        
        
        /*
        if (isActiveStateApp) {
            
            UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            alertt.tag=1;
            [alertt show];
            
        } else if (isBackroundStateApp){
            
            UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            alertt.tag=1;
            [alertt show];
            
        } else if (isInActiveStateApp){
            
            UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            alertt.tag=1;
            [alertt show];
            
        }
        */
        
    }
    else {
        
        UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:strTitle message:strBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertt show];
        
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}


-(void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    
    
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center  willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    NSString *strMsg=[NSString stringWithFormat:@"%@",notification.request.content.userInfo];
    
    UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:@"Notification" message:strMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    // [alertt show];
    
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    NSString *strMsg=[NSString stringWithFormat:@"%@",response.notification.request.content.userInfo];
    
    UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:@"Notification" message:strMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[alertt show];
    
}



//Nilind 19 Dec



-(void)startTrackingBg
{
    
    //[locationManager startUpdatingLocation];
    [self calculateDistance];
    
}

-(CLLocationCoordinate2D) getLocation
{
    // CLLocationManager *locationManager;
    //locationManager = [[CLLocationManager alloc]init] ;
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

-(void)calculateDistance
{
    CLLocationCoordinate2D coordinate = [self getLocation];
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy HH:mm:ss.SSS";
    
    double latitude = coordinate.latitude;
    double longitude = coordinate.longitude;
    
    strLattitude=nil;
    strLongitude=nil;
    
    strLattitude=[[NSString alloc]init];
    strLongitude=[[NSString alloc]init];
    
    
    NSLog(@"%f-------%f-----%@----%@----",latitude,longitude,strLattitude,strLongitude);
    
    strLattitude=[NSString stringWithFormat:@"%f",latitude];
    strLongitude=[NSString stringWithFormat:@"%f",longitude];
    
    NSLog(@"%f-------%f-----%@----%@----",latitude,longitude,strLattitude,strLongitude);
    
    
    [locationManager stopUpdatingLocation];
    
    
    NSString *strTime=[dateFormatter stringFromDate:now];
    
    NSDictionary *userLocation=@{@"lat":[NSString stringWithFormat:@"%f",latitude],@"long":[NSString stringWithFormat:@"%f",longitude],@"time":strTime};
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"userOldLocation"] isKindOfClass:[NSDictionary class]])
    {
        
        NSString *oldLat,*oldLong,*oldTime;
        NSDictionary *dictOldData=[[NSUserDefaults standardUserDefaults] valueForKey:@"userOldLocation"];
        oldLat=[dictOldData valueForKey:@"lat"];
        oldLong=[dictOldData valueForKey:@"long"];
        oldTime=[dictOldData valueForKey:@"time"];
        
        
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[oldLat doubleValue] longitude:[oldLong doubleValue]];
        
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude: latitude longitude:longitude];
        
        NSTimeInterval timeElapsed;
        CLLocationDistance distance;
        double speed;
        distance = [location1 distanceFromLocation:location2];
        NSLog(@"Distance Travelled >>> %f",distance);
        
        
        NSString *duration = [self calculateDuration:oldTime secondDate:strTime];
        timeElapsed=[duration doubleValue];
        speed =(distance/timeElapsed);
        NSLog(@"Speed In m/sec>>>>  %f  km/hr>>>%f",speed,speed*3.6);
        
       // [global AlertMethod:[NSString stringWithFormat:@"Lat %@ Long %@",strLattitude,strLongitude] :[NSString stringWithFormat:@"Speed >>%f  Dsitance >>%f",speed,distance]];
        strSpeed=[NSString stringWithFormat:@"%.2f",speed];
        
        if (distance>50)
        {
            [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:@"userOldLocation"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
            if(strEmployeeNumber.length ==0 || [strEmployeeNumber isEqualToString:@"(null)"])
            {
                
                
                
            }
            else
            {
                
                [self trackLocation];
                
            }
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:@"userOldLocation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    /*
     NSDate *now = [NSDate date];
     
     static NSDateFormatter *dateFormatter;
     if (!dateFormatter) {
     dateFormatter = [[NSDateFormatter alloc] init];
     dateFormatter.dateFormat = @"h:mm:ss a";  // very simple format  "8:47:22 AM"
     }
     _lblTime.text = [dateFormatter stringFromDate:now];
     */
    
    
}

/*- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
 {
 
 NSLog(@"called after 10 sec");
 NSLog(@"Location: %f, %f",newLocation.coordinate.longitude, newLocation.coordinate.latitude);
 strLattitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
 strLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
 
 NSString *strOldLat,*strOldLong;
 strOldLat=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
 strOldLong=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
 
 
 NSTimeInterval timeElapsed;
 CLLocationDistance distance;
 double speed;
 
 CLLocation *locA = [[CLLocation alloc] initWithLatitude:oldLocation.coordinate.latitude  longitude:oldLocation.coordinate.longitude];
 
 CLLocation *locB = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
 distance = [locA distanceFromLocation:locB];
 NSLog(@"Distance Travelled >>> %f",distance);
 timeElapsed = [newLocation.timestamp timeIntervalSinceDate:oldLocation.timestamp];
 speed =distance/timeElapsed;
 NSLog(@"Speed >>>> %f",speed);
 strSpeed=[NSString stringWithFormat:@"%.2f",speed];
 
 
 
 UIAlertView *alertt=[[UIAlertView alloc]initWithTitle:@"Alert" message:[NSString stringWithFormat:@"Distance = %f Speed = %f",distance,speed] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
 //[alertt show];
 [locationManager stopUpdatingLocation];
 if (distance>0)
 {
 NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
 NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
 NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
 if(strEmployeeNumber.length ==0 || [strEmployeeNumber isEqualToString:@"(null)"])
 {
 }
 else
 {
 [global AlertMethod:@"Lat Long" :[NSString stringWithFormat:@"New Lat Long %@ %@  Old Lat Old Long %@ %@",strLattitude,strLongitude,strOldLat,strOldLong]];
 [self trackLocation];
 }
 }
 }
 */
-(void)trackLocation
{
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strEmployeeNumber =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    NSString *strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    
    NSString* strDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", strDeviceId);
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
       // [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        NSString *strUrl;
        
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/EmployeeCurrentLocation/AddEmployeeCurrentLocation"];
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"EmployeeNo",
             @"DeviceId",
             @"Latitude",
             @"Longitude",
             @"Speed",
             @"IP",
             @"CreatedDate",
             @"CompanyId",
             nil];
        value=[NSArray arrayWithObjects:
               strEmployeeNumber,
               strDeviceId,
               strLattitude,
               strLongitude,
               strSpeed,
               [global getIPAddress],
               [global strCurrentDate],
               strCoreCompanyId,
               nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSLog(@"Json Location----------%@",strdatafirst);
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        @try {
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,NSData * data,NSError * error)
             {
                 NSData* jsonData = [NSData dataWithData:data];
                 
                 NSString* myString;
                 myString = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
                 
                 /* NSDictionary*  ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                  */
                 //[global AlertMethod:Alert :[NSString stringWithFormat:@"Input = %@ Response = %@",dict_ToSend,myString]];
                 NSLog(@"Input = %@  Location Tracking Response  = = = =  = %@",dict_ToSend,myString);
                 [DejalActivityView removeView];
             }];
        }
        @catch (NSException *exception) {
            //[global AlertMethod:Alert :Sorry];
        }
        @finally {
        }
    }
}

- (NSString *)calculateDuration:(NSString *)oldTime secondDate:(NSString *)currentTime
{
    
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy HH:mm:ss.SSS";
    
    
    NSDate *date1 = [dateFormatter dateFromString:oldTime];
    NSDate *date2 = [dateFormatter dateFromString:currentTime];
    
    
    
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    
    int hh = secondsBetween / (60*60);
    double rem = fmod(secondsBetween, (60*60));
    int mm = rem / 60;
    rem = fmod(rem, 60);
    int ss = rem;
    
    //NSString *str = [NSString stringWithFormat:@"%02d:%02d:%02d",hh,mm,ss];
    
    NSString *str = [NSString stringWithFormat:@"%f",secondsBetween];
    
    return str;
}

-(void)checkAppVersion
{
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
    NSString *strBundleIdentifier=@"com.infocrats.Pestream";
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", strBundleIdentifier]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
     
                                       queue:[NSOperationQueue mainQueue]
     
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if (!error) {
                                   
                                   NSError* parseError;
                                   
                                   NSDictionary *appMetadataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseError];
                                   
                                   NSArray *resultsArray = (appMetadataDictionary)?[appMetadataDictionary objectForKey:@"results"]:nil;
                                   
                                   NSDictionary *resultsDic = [resultsArray firstObject];
                                   
                                   if (resultsDic) {
                                       
                                       // compare version with your apps local version
                                       
                                       NSString *iTunesVersion = [resultsDic objectForKey:@"version"];
                                       
                                       NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)@"CFBundleShortVersionString"];
                                       
                                       if (iTunesVersion && [appVersion compare:iTunesVersion] != NSOrderedDescending&&[appVersion compare:iTunesVersion] != NSOrderedSame) {
                                           
                                           UIAlertController * alert=   [UIAlertController
                                                                         
                                                                         alertControllerWithTitle:@"Update"
                                                                         
                                                                         message:@"A new version of this app is available."
                                                                         
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                           
                                           
                                           UIAlertAction* updateBtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Update"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
                                                                           //                                                                                                                                                      NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/in/app/apple-store/id%@?mt=8",strAppStoreId];
                                                                           
                                                                           NSString *iTunesLink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/in/app/id1174328672?mt=8"];
                                                                           
                                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                                       }];
                                           
                                           UIAlertAction *Cancelbtn = [UIAlertAction
                                                                       
                                                                       actionWithTitle:@"Cancel"
                                                                       
                                                                       style:UIAlertActionStyleDefault
                                                                       
                                                                       handler:^(UIAlertAction * action)
                                                                       
                                                                       {
                                                                           
                                                                           [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
                                                                           
                                                                       }];
                                           
                                           [alert addAction:Cancelbtn];
                                           
                                           [alert addAction:updateBtn];
                                           
                                           [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
                                       }
                                   }
                               } else {
                                   // error occurred with http(s) request
                                   NSLog(@"error occurred communicating with iTunes");
                               }
                           }];
    
}

#pragma mark- ---------------Call Delegates Methods---------------

-(void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    
    if (call.hasConnected) {
        
        NSLog(@"********** voice call connected **********/n");
                
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CallEnded" object:self];

    } else if(call.hasEnded) {
        
        NSLog(@"********** voice call disconnected **********/n");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CallEnded" object:self];

    } else if(call.onHold) {
        
        NSLog(@"********** voice call onhold **********/n");
        
    } else if(call.outgoing) {
        
        NSLog(@"********** voice call outgoing **********/n");
        
    }else {
        
        NSLog(@"********** voice call Delegate **********/n");
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"CallEnded" object:self];

    }
    
    /*
     if call.hasEnded == true {
         print("Disconnected")
         let nc = NotificationCenter.default
         nc.post(name: Notification.Name("CallDisconnected"), object: nil)
         
     }
     if call.isOutgoing == true && call.hasConnected == false {
         print("Dialing")
     }
     if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
         print("Incoming")
     }

     if call.hasConnected == true && call.hasEnded == false {
         print("Connected")
     }
     */
    
}

@end
