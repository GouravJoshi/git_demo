//
//  LoginViewController.h
//  DPS
//
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
- (IBAction)loginAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtCompanyKey;
-(IBAction)hideKeyBoard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)forgotPass:(id)sender;

@end
