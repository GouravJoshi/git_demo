//  PestPacPaymentVC.swift
//  Created by INFOCRATS on 09/03/21.
//  Developed by Saavan Patidar iOS Team Lead & Senior iOS Developer.
//  Copyright © 2021 Saavan. All rights reserved.


import UIKit
import Alamofire
import WebKit

class PestPacPaymentVC: UIViewController {

    @IBOutlet weak var webVieww: WKWebView!
    
    let dispatchGroupPayment = DispatchGroup()
    var loaderPayment = UIAlertController()
    var hostedPaymentUrl = ""
    var dictLoginData = NSDictionary()
    var leadId = ""
    var strSalesAutoUrlMain = ""
    var dictPaymentDetails = NSMutableDictionary()
    var labelAlert = UILabel(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.width-25, width: UIScreen.main.bounds.size.width, height: 25))
    var logMsg = ""


    override func viewDidLoad() {
                
        super.viewDidLoad()
        
        // Find out Device Info etc.
        
        logMsg = "PestPacPaymentVC : \n\n Device Info \(Global().dictOfDeviceInfo() ?? NSMutableDictionary())"
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Setting Default Value
        
        leadId = "\(dictPaymentDetails.value(forKey: "leadId") ?? "")"

        self.webVieww.navigationDelegate = self
        
        // Configuring WKEWebview
        
        let contentController = self.webVieww.configuration.userContentController
        contentController.add(self, name: "onload")

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strSalesAutoUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")!)"

        self.webVieww.isHidden = true
        
        // Do any additional setup after loading the view.
        
        if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPestPacIntegration") as! Bool) == true {
            
            if (dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.IsPaymentIntegration") as! Bool) == true {
                
                self.perform(#selector(loadData), with: nil, afterDelay: 0.1)
                
                //labelAlert.center = CGPoint(x: 160, y: 285)
                if DeviceType.IS_IPAD {
                    labelAlert = UILabel(frame: CGRect(x: 0, y: 130, width: UIScreen.main.bounds.size.width, height: 50))
                    labelAlert.font = UIFont.systemFont(ofSize: 23)
                }else{
                    labelAlert = UILabel(frame: CGRect(x: 0, y: 120, width: UIScreen.main.bounds.size.width, height: 50))
                    labelAlert.font = UIFont.systemFont(ofSize: 17)
                }
                labelAlert.backgroundColor = .lightText
                labelAlert.numberOfLines = 3
                labelAlert.textAlignment = .center
                labelAlert.text = "Please wait data is loading or Press back arrow to cancel transaction"
                self.view.addSubview(labelAlert)
                
            }else{
                
                // Payment Integration is Off
                self.alertOnNoPestPacIntegration()
                //self.perform(#selector(loadData), with: nil, afterDelay: 0.1)

            }
            
        }else{
            
            // Payment Integration is Off
            self.alertOnNoPestPacIntegration()
            //self.perform(#selector(loadData), with: nil, afterDelay: 0.1)

        }
        
                
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    func logAppErrorAPI(strErrorMsg : String , typeee : String) {
        
        // Start Loader
        let loaderTempLocal = loader_Show(controller: self, strMessage: "Something went wrong sending logs to server", title: "Please wait...", style: .alert)
        self.present(loaderTempLocal, animated: false, completion: nil)
        
        let dictToSend = NSMutableDictionary()
        dictToSend.setValue(strErrorMsg, forKey: "ErrorMsg")
        dictToSend.setValue(typeee, forKey: "IsError")
        
        if(isInternetAvailable()){
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl")!)" + "/api/MobileAppToCore/LogAppError"
            
            WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strURL, strType: "bool"){ (responce, status) in
                
                print(responce)
                
                loaderTempLocal.dismiss(animated: false) {

                    if(status)
                    {
                        
                        if responce.value(forKey: "Success") as! NSString == "true"
                        {
                            
                            let alert = UIAlertController(title: Info, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                                
                                //self.navigationController?.popViewController(animated: false)
                                self.dismiss(animated: false, completion: nil)
                                
                            }))

                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
                                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
    @objc func loadData() {
        
        if isInternetAvailable() {
            
            self.pestPacAccessTokenAPI()
            
            self.dispatchGroupPayment.notify(queue: DispatchQueue.main, execute: {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.labelAlert.removeFromSuperview()

                //self.loaderPayment.dismiss(animated: false) {
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false

                    if self.hostedPaymentUrl.count > 0 {
                                                
                        // Payment Link Opened Load WebView Here
                        self.webVieww.isHidden = false

                        /*let url = self.hostedPaymentUrl

                        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        
                        self.webVieww.load(URLRequest(url: URL(string: strUrlwithString!)!))*/
                                                
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                //}
                
            })
            
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
    }
    
    
    @IBAction func action_Bacck(_ sender: Any) {

        let alert = UIAlertController(title: alertMessage, message: "Are you sure you want to cancel Transaction ?, if your transaction is in progress it is suggested not to close the window.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: { (nil) in
            
            //self.navigationController?.popViewController(animated: false)
            self.dismiss(animated: false, completion: nil)
            
        }))
        alert.addAction(UIAlertAction (title: "Dismiss", style: .cancel, handler: { (nil) in
            
            
            
        }))
        
        self.present(alert, animated: true, completion: nil)

    }
    
    func alertOnNoPestPacIntegration() {
        
        let alert = UIAlertController(title: alertMessage, message: "Payment not allowed", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "ok", style: .default, handler: { (nil) in

            //self.navigationController?.popViewController(animated: false)
            self.dismiss(animated: false, completion: nil)

        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func pestPacAccessTokenAPI()  {
        
        self.hostedPaymentUrl = ""
            
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.dispatchGroupPayment.enter()

        let dictData = NSMutableDictionary()
        
        dictData.setValue("\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.UserName") ?? "")", forKey: "username")
        dictData.setValue("\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.Password") ?? "")", forKey: "password")
        dictData.setValue("\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.PestPacCompanyKey") ?? "")", forKey: "companyKey")
        dictData.setValue("\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.OAuthClientID") ?? "")", forKey: "client_id")
        dictData.setValue("\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.OAuthClientSecret") ?? "")", forKey: "client_secret")
        dictData.setValue(PestPacGrant_Type, forKey: "grant_type")
        
        self.logMsg = self.logMsg + "\n\n URL For Token : \(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.ApiTokenBaseUri") ?? "No URL")" + "\n\n Access Token Dict Data : \(dictData)"
        
            /*dictData.setValue(PestPacUserName, forKey: "username")
            dictData.setValue(PestPacPassword, forKey: "password")
            dictData.setValue(PestPacCompanyKey, forKey: "companyKey")
            dictData.setValue(PestPacClient_Id, forKey: "client_id")
            dictData.setValue(PestPacClient_Secret, forKey: "client_secret")
            dictData.setValue(PestPacGrant_Type, forKey: "grant_type")*/
        
            WebService.callAPIBYPOST(parameter: dictData, url: "\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.ApiTokenBaseUri") ?? "")") { (responce, status) in
                print(responce)
                
                if(status){
                    //access_token
                    
                    self.logMsg = self.logMsg + "\n\n Response On Token API : \(responce)"

                    if(responce.value(forKey: "data") is NSDictionary){
                        
                        let responceData = responce.value(forKey: "data") as! NSDictionary
                        
                        let allkeys = responceData.allKeys as NSArray
                        
                        if allkeys.contains("access_token") {
                            
                            self.pestPacHostedPaymentAPI(strAcccesToken: "\(responceData.value(forKey: "access_token") ?? "")")
                            
                        } else {
                            
                            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                        let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                        if let first = count.first, let int = Int(first), int > 0  {
                            self.dispatchGroupPayment.leave()
                        }

                    }else{
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                        if let first = count.first, let int = Int(first), int > 0  {
                            self.dispatchGroupPayment.leave()
                        }
                    }
                    
                }else{
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                    if let first = count.first, let int = Int(first), int > 0  {
                        self.dispatchGroupPayment.leave()
                    }

                }
                
            }
            
        }
    
    
    func pestPacHostedPaymentAPI(strAcccesToken : String)  {
        
        self.dispatchGroupPayment.enter()

        let authorization = "Bearer " + strAcccesToken
        
        // "\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.ApiKey") ?? "")"
        
        let headers: HTTPHeaders = [
            "apikey": "\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.ApiKey") ?? "")",
            "tenant-id": "\(dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.PestPacCompanyKey") ?? "")",
            "Authorization": authorization
        ]
        
        /*let headers: HTTPHeaders = [
            "apikey": PestPacApikey,
            "tenant-id": PestPacCompanyKey,
            "Authorization": authorization
        ]*/
        
        let billToId = "\(dictPaymentDetails.value(forKey: "billToId") ?? "")"
        let amount = "\(dictPaymentDetails.value(forKey: "amount") ?? "")"
        let nameOnCard = "\(dictPaymentDetails.value(forKey: "nameOnCard") ?? "")"
        let billingAddress = "\(dictPaymentDetails.value(forKey: "billingAddress") ?? "")"
        let billingPostalCode = "\(dictPaymentDetails.value(forKey: "billingPostalCode") ?? "")"
        
        let url = UrlPestPacHostedPaymentSaveCard + "billToId=\(billToId)" + "&amount=\(amount)" + "&nameOnCard=\(nameOnCard)" + "&billingAddress=\(billingAddress)" + "&billingPostalCode=\(billingPostalCode)"
        
        self.logMsg = self.logMsg + "\n\n Headers For Payment URL : \(headers)" + "\n\n URL For Payment URL : \(url)"

        
        WebService.callAPIBYGetPayment(headers:headers,parameter: NSDictionary(), url: url) { (responce, status) in
                print(responce)
                if(status){
                    
                    self.logMsg = self.logMsg + "\n\n Response On Payment Url Get API : \(responce)"

                    //access_token
                    if(responce.value(forKey: "data") is NSString){
                        
                        self.hostedPaymentUrl = "\(responce.value(forKey: "data") ?? "")"
                        
                        if self.hostedPaymentUrl.count > 0 {
                            
                            if self.hostedPaymentUrl.contains("error") {
                                
                                self.logAppErrorAPI(strErrorMsg: self.logMsg, typeee: "true")
                                
                            }else if self.hostedPaymentUrl.contains("Token validation returned unsuccessfully ") {
                                
                                self.logAppErrorAPI(strErrorMsg: self.logMsg, typeee: "true")
                                
                            }else if self.hostedPaymentUrl.contains("Token validation returned unsuccessfully") {
                                
                                self.logAppErrorAPI(strErrorMsg: self.logMsg, typeee: "true")
                                
                            }else{
                                
                                self.dispatchGroupPayment.enter()

                                // Payment Link Opened Load WebView Here
                                self.webVieww.isHidden = false

                                let url = self.hostedPaymentUrl

                                let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                
                                self.webVieww.load(URLRequest(url: URL(string: strUrlwithString!)!))
                                
                            }
                            
                        }else{
                            
                        }
                        
                    }else{
                        
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                    let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                    if let first = count.first, let int = Int(first), int > 0  {
                        self.dispatchGroupPayment.leave()
                    }
                    
                }else{
                    
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                    let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                    if let first = count.first, let int = Int(first), int > 0  {
                        self.dispatchGroupPayment.leave()
                    }
                    
                }
                
            }
            
        }
    
}

extension PestPacPaymentVC: WKNavigationDelegate {
 
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    

    }
 
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { (html: Any?, error: Error?) in print(html!)
            
            let strHtml = html as! String
            
            if strHtml.contains("handleResult(e)"){
                
                let count = self.dispatchGroupPayment.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupPayment.leave()
                }

                //  this is first time loading when open iFrame Url
                
            }else if strHtml.contains("JSON.stringify"){
                
                // Response on doing payment or cancellinng payment
                
                var strBreakedHtml = strHtml.slice(from: "JSON.stringify", to: "\"></body")
                
                strBreakedHtml!.remove(at: strBreakedHtml!.startIndex)
                                
                while strBreakedHtml!.contains(")") {
                    
                    strBreakedHtml = strBreakedHtml?.strRemove(strToRemove: strBreakedHtml!)
                    
                }
                
                strBreakedHtml = strBreakedHtml!.trimmingCharacters(in: .whitespacesAndNewlines)
                
                let data: Data = strBreakedHtml!.data(using: .utf8)!
                
                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
                    
                    print(attributedString)
                    
                    let jsonString = attributedString.string
                    
                    print("Json String Transaction Details \(jsonString)")

                    let dictTransactionData = self.convertToDictionary(text: jsonString)! as NSDictionary
                    
                    print("Dict of Transaction Details \(dictTransactionData)")
                    
                    // check response and save in DB
                    
                    // Store wali ka response hai y wala
                    
                    self.saveCreditCardDetailsToDB(dictTransaction: dictTransactionData)
                    
                    // charge wali ka Response hai y wala
                    
                    //self.saveTransactionDetailsToDB(dictTransaction: dictTransactionData)

                }
                
            }else{
                
                let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                    
                    //self.navigationController?.popViewController(animated: false)
                    self.dismiss(animated: false, completion: nil)

                }))
                
                self.present(alert, animated: true, completion: nil)

            }
            
        })
        
    }
    
    func saveTransactionDetailsToDB(dictTransaction : NSDictionary) {
        
        if dictTransaction.count > 0 {
            
            // checck if success response
            
            if dictTransaction.value(forKey: "TransactionResponse") is NSDictionary {
                
                let dict = dictTransaction.value(forKey: "TransactionResponse") as! NSDictionary

                let transactionResult = "\(dict.value(forKey: "TransactionResult") ?? "")"
                
                if transactionResult.caseInsensitiveCompare("Approved") == .orderedSame {
                    
                    // Success Save in DB and upload to server
                    
                    self.savePaymentDB(dictData: dictTransaction)
                    
                }else if transactionResult.caseInsensitiveCompare("NotApproved") == .orderedSame {
                    
                    // Success Save in DB and upload to server
                                        
                    let alert = UIAlertController(title: alertMessage, message: "\(dict.value(forKey: "Message") ?? "")", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                        
                        //self.navigationController?.popViewController(animated: false)
                        self.dismiss(animated: false, completion: nil)

                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        
    }
    
    func saveCreditCardDetailsToDB(dictTransaction : NSDictionary) {
        
        if dictTransaction.count > 0 {
            
            // checck if success response
            
            if dictTransaction.value(forKey: "CreditCard") is NSDictionary {
                
                let dictCreditCard = dictTransaction.value(forKey: "CreditCard") as! NSDictionary

                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("leadId")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("leadPaymentCardInfoId")

                arrOfKeys.add("cardID")
                arrOfKeys.add("billToID")
                arrOfKeys.add("branchID")
                arrOfKeys.add("branch")
                arrOfKeys.add("accountType")
                arrOfKeys.add("isPrimary")
                
                
                arrOfKeys.add("nameOnCard")
                arrOfKeys.add("creditCardType")
                arrOfKeys.add("accountNumber")
                arrOfKeys.add("cardToken")
                arrOfKeys.add("expirationDate")
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(leadId)
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("0")

                arrOfValues.add("\(dictTransaction.value(forKey: "CardID") ?? "")")
                arrOfValues.add("\(dictTransaction.value(forKey: "BillToID") ?? "")")
                arrOfValues.add("\(dictTransaction.value(forKey: "BranchID") ?? "")")
                arrOfValues.add("\(dictTransaction.value(forKey: "Branch") ?? "")")
                arrOfValues.add("\(dictTransaction.value(forKey: "AccountType") ?? "")")
                arrOfValues.add((dictTransaction.value(forKey: "IsPrimary") as! Bool))
                
                arrOfValues.add("\(dictCreditCard.value(forKey: "NameOnCard") ?? "")")
                arrOfValues.add("\(dictCreditCard.value(forKey: "CreditCardType") ?? "")")
                arrOfValues.add("\(dictCreditCard.value(forKey: "AccountNumber") ?? "")")
                arrOfValues.add("\(dictCreditCard.value(forKey: "CardToken") ?? "")")
                arrOfValues.add("\(dictCreditCard.value(forKey: "ExpirationDate") ?? "")")

                
                let dictToSend = NSMutableDictionary()

                dictToSend.setValue(Global().getCompanyKey(), forKey: "CompanyKey")
                dictToSend.setValue(leadId, forKey: "LeadId")
                let dictTemp = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
                let arrTemp = NSMutableArray()
                arrTemp.add(dictTemp)
                dictToSend.setValue(arrTemp, forKey: "DTDLeadPaymentCardInfoDc")

                
                nsud.setValue(dictTemp, forKey: "\(dictTransaction.value(forKey: "BillToID") ?? "")")
                
                var jsonString = String()
                
                if(JSONSerialization.isValidJSONObject(dictToSend) == true)
                {
                    
                    let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                    
                    print(jsonString)
                    
                }
                
                saveDataInDB(strEntity: Entity_D2DPestPacCreditCardDC, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                self.syncCreditCardToServer(dictToSend: dictToSend)
                
                
            }
            
        }else{
                                            
            let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                
                //self.navigationController?.popViewController(animated: false)
                self.dismiss(animated: false, completion: nil)
                
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func syncCreditCardToServer(dictToSend : NSDictionary) {
        
        // Start Loader
        let loaderTempLocal = loader_Show(controller: self, strMessage: "Please do not close the window while transaction is in process.", title: "Please wait...", style: .alert)
        self.present(loaderTempLocal, animated: false, completion: nil)
        
        let strUrl = strSalesAutoUrlMain + PestPacAddCreditCardToServer
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
                                
            loaderTempLocal.dismiss(animated: false) {

                if(status)
                {
                    
                    debugPrint(responce)
                    
                    if responce.value(forKey: "Success") as! NSString == "true"
                    {
                        
                        // Handle Success Response Here
                        if responce.value(forKey: "data") is NSDictionary {

                            let dictData = responce.value(forKey: "data") as! NSDictionary
                            
                            if dictData.value(forKey: "DTDLeadPaymentCardInfoDc") is NSArray {
                                
                                let arrDTDLeadPaymentTransaction = dictData.value(forKey: "DTDLeadPaymentCardInfoDc") as! NSArray
                                
                                if arrDTDLeadPaymentTransaction.count > 0 {
                                    
                                    for i in 0 ..< arrDTDLeadPaymentTransaction.count
                                    {
                                        
                                        let dictDTDLeadPaymentTransaction = arrDTDLeadPaymentTransaction[i] as! NSDictionary

                                        let strLeadPaymentId = "\(dictDTDLeadPaymentTransaction.value(forKey: "LeadPaymentCardInfoId") ?? "")"
                                        
                                        if strLeadPaymentId.count == 0 || strLeadPaymentId == "0" {
                                            
                                            // Fail Show Alert
                                            
                                            self.errorMsg()
                                            
                                        }else{
                                            
                                            // Delete data after syncing to server
                                            
                                            self.deleteCreditCardDB(dictData: dictDTDLeadPaymentTransaction)
                                            
                                            // Success Show Alert
                                            
                                            let alert = UIAlertController(title: alertInfo, message: "Card added successfully", preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                                                
                                                //self.navigationController?.popViewController(animated: false)
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddedCreditCard_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])

                                                self.dismiss(animated: false, completion: nil)

                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    }
                                } else {
                                    
                                    self.errorMsg()
                                }
                            }else{
                                
                                self.errorMsg()
                            }
                        }else{
                            
                            self.errorMsg()
                        }
                    }else{
                        
                        self.errorMsg()
                    }
                }
            }
        }
    }
    
    func savePaymentDB(dictData : NSDictionary) {
        
        let dictTransactionResponse = dictData.value(forKey: "TransactionResponse") as! NSDictionary
        let dictPayment = dictData.value(forKey: "Payment") as! NSDictionary

        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
                
        arrOfKeys.add("adjustmentReason")
        arrOfKeys.add("amount")
        arrOfKeys.add("approvalNumber")
        arrOfKeys.add("balance")
        arrOfKeys.add("batchNumber")
        arrOfKeys.add("billToID")
        arrOfKeys.add("branch")
        arrOfKeys.add("branchID")
        arrOfKeys.add("cardType")
        arrOfKeys.add("committedBy")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("gLCode")
        arrOfKeys.add("idempotencyId")
        arrOfKeys.add("lastFour")
        arrOfKeys.add("leadPaymentId")
        arrOfKeys.add("message")
        arrOfKeys.add("methodOfPayment")
        arrOfKeys.add("paymentDate")
        arrOfKeys.add("paymentID")
        arrOfKeys.add("paymentType")
        arrOfKeys.add("processorTransactionId")
        arrOfKeys.add("reference")
        arrOfKeys.add("relatedPaymentID")
        arrOfKeys.add("reversal")
        arrOfKeys.add("service")
        arrOfKeys.add("serviceDescription")
        arrOfKeys.add("transactionDate")
        arrOfKeys.add("transactionResult")
        
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(leadId)
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        
        arrOfValues.add("\(dictPayment.value(forKey: "AdjustmentReason") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "Amount") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "ApprovalNumber") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "Balance") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "BatchNumber") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "BillToID") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "Branch") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "BranchID") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "CardType") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "Committed") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "ExpirationDate") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "GLCode") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "IdempotencyId") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "LastFour") ?? "")")
        arrOfValues.add("0")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "Message") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "MethodOfPayment") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "PaymentDate") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "PaymentID") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "PaymentType") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "ProcessorTransactionId") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "Reference") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "RelatedPaymentID") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "Reversal") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "Service") ?? "")")
        arrOfValues.add("\(dictPayment.value(forKey: "ServiceDescription") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "TransactionDate") ?? "")")
        arrOfValues.add("\(dictTransactionResponse.value(forKey: "TransactionResult") ?? "")")
        
        let dictToSend = NSMutableDictionary()

        dictToSend.setValue(Global().getCompanyKey(), forKey: "CompanyKey")
        dictToSend.setValue(leadId, forKey: "LeadId")
        let dictTemp = NSDictionary.init(objects: arrOfValues as! [Any], forKeys: arrOfKeys as! [NSCopying])
        let arrTemp = NSMutableArray()
        arrTemp.add(dictTemp)
        dictToSend.setValue(arrTemp, forKey: "DTDLeadPaymentTransaction")

        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        saveDataInDB(strEntity: Entity_D2DPestPacPaymentDC, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        self.syncPaymentToServer(dictToSend: dictToSend)

        // Now upload following data to server
        
        // and on succcessfully uploading delete data from DB
        
    }
    
    func syncPaymentToServer(dictToSend : NSDictionary) {
        
        // Start Loader
        let loaderTempLocal = loader_Show(controller: self, strMessage: "Please do not close the window while transaction is in process.", title: "Please wait...", style: .alert)
        self.present(loaderTempLocal, animated: false, completion: nil)
        
        let strUrl = strSalesAutoUrlMain + PestPacUpdatePaymentToServer
        
        WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strUrl, strType: "dict"){ (responce, status) in
                                
            loaderTempLocal.dismiss(animated: false) {

                if(status)
                {
                    
                    debugPrint(responce)
                    
                    if responce.value(forKey: "Success") as! NSString == "true"
                    {
                        
                        // Handle Success Response Here
                        if responce.value(forKey: "data") is NSDictionary {

                            let dictData = responce.value(forKey: "data") as! NSDictionary
                            
                            if dictData.value(forKey: "DTDLeadPaymentTransaction") is NSArray {
                                
                                let arrDTDLeadPaymentTransaction = dictData.value(forKey: "DTDLeadPaymentTransaction") as! NSArray
                                
                                if arrDTDLeadPaymentTransaction.count > 0 {
                                    
                                    for i in 0 ..< arrDTDLeadPaymentTransaction.count
                                    {
                                        
                                        let dictDTDLeadPaymentTransaction = arrDTDLeadPaymentTransaction[i] as! NSDictionary

                                        let strLeadPaymentId = "\(dictDTDLeadPaymentTransaction.value(forKey: "LeadPaymentId") ?? "")"
                                        
                                        if strLeadPaymentId.count == 0 || strLeadPaymentId == "0" {
                                            
                                            // Fail Show Alert
                                            
                                            self.errorMsg()
                                            
                                        }else{
                                            
                                            // Delete data after syncing to server
                                            
                                            self.deletePaymentDB(dictData: dictDTDLeadPaymentTransaction)
                                            
                                            // Success Show Alert
                                            
                                            let alert = UIAlertController(title: alertInfo, message: "Payment Success", preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
                                                
                                                //self.navigationController?.popViewController(animated: false)
                                                self.dismiss(animated: false, completion: nil)

                                            }))
                                            
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        }
                                        
                                    }
                                    
                                } else {
                                    
                                    self.errorMsg()
                                    
                                }
                                

                            }else{
                                
                                self.errorMsg()

                            }
                        }else{
                            
                            self.errorMsg()

                        }
                    }else{
                        
                        self.errorMsg()

                    }
                }
            }
        }
    }
    
    func errorMsg() {
        
        // Fail Show Alert
        
        let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "ok", style: .default, handler: { (nil) in
            
            //self.navigationController?.popViewController(animated: false)
            self.dismiss(animated: false, completion: nil)

        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deletePaymentDB(dictData: NSDictionary) {
        
        deleteAllRecordsFromDB(strEntity: Entity_D2DPestPacPaymentDC, predicate: NSPredicate(format: "leadId == %@ && companyKey == %@ && paymentID == %@", leadId,Global().getCompanyKey(), "\(dictData.value(forKey: "PaymentID") ?? "")"))
        
    }
    
    func deleteCreditCardDB(dictData: NSDictionary) {
        
        deleteAllRecordsFromDB(strEntity: Entity_D2DPestPacCreditCardDC, predicate: NSPredicate(format: "leadId == %@ && companyKey == %@ && cardToken == %@", leadId,Global().getCompanyKey(), "\(dictData.value(forKey: "CardToken") ?? "")"))
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
 
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            
        let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in
            
            //self.navigationController?.popViewController(animated: false)
            self.dismiss(animated: false, completion: nil)

        }))
        
        self.present(alert, animated: true, completion: nil)

    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {

        
    }

}

extension PestPacPaymentVC: WKScriptMessageHandler{
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let dict = message.body as? [String : AnyObject] else {
            return
        }

        print(dict)
    }
}

extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    
    func strRemove(strToRemove: String) -> String? {
        
        //Reverse the string
        let reversedStr = String(strToRemove.reversed())
        //Find the first (last) occurance of @
        let endIndex = reversedStr.range(of: ")")!.upperBound
        //Get the string up to and after the @ symbol
        let newStr = reversedStr.substring(from: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)

        //Store the new string over the original
        return String(newStr.reversed())
        
    }
    
}
