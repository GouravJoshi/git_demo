//
//  WdoGraphDrawHistoryVC_iPad.swift
//  DPS
//
//  Created by INFOCRATS on 14/07/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import CoreData
import SafariServices

import Alamofire
import SwiftyXMLParser

@objc protocol RefreshGraph : class
{
    func refresh()
    func refreshWebGraph(dict : NSDictionary)

}


class CellWdoGraphDrawHistory:UITableViewCell{
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
   

    override func awakeFromNib() {
        
        super.awakeFromNib()
        imgView.layer.borderWidth = 1.0
        imgView.layer.masksToBounds = false
        imgView.layer.borderColor = UIColor.white.cgColor
        imgView.layer.cornerRadius = imgView.frame.size.width / 2
        imgView.clipsToBounds = true
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

class WdoGraphDrawHistoryVC_iPad: UIViewController {

    // MARK: - ------------------Outlets------------------

    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblTitleHeader: UILabel!
    @IBOutlet var btnLoad: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!

    var loader = UIAlertController()

    // MARK: - ------------------Global Variables------------------
    @objc weak var delegate: RefreshGraph?

    var arrayOfList = NSMutableArray () , aryWebGraphList = NSArray()
    @objc  var strHeaderTitle = NSString ()
    @objc var strWoStatus = NSString ()
    var selectedRow = -1
    var strGlobalWoId = ""
    var strWoLeadId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitleHeader.text = "\(strHeaderTitle)"
        // Do any additional setup after loading the view.
        
        tblView.estimatedRowHeight = 50.0
        tblView.tableFooterView = UIView()
        
        //if strWoStatus == "Completed"
        if ((strWoStatus == "Completed") || (strWoStatus == "Complete"))
        {
            
            btnLoad.isEnabled = false
            
        } else {
            
            btnLoad.isEnabled = true
            
        }
        
        // delete record
        self.deleteWdoGraphDrawHistoryRecordsFromDB()
        segment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16)], for: .normal)

        segment.addTarget(self, action: #selector(actionViewSegment), for: .valueChanged)
    }


    override func viewWillAppear(_ animated: Bool) {
        
        if(segment.selectedSegmentIndex == 0){
            loadDataFromDB()
        }else{
         
        }
    }
    @objc func actionViewSegment(sender : UISegmentedControl) {
        self.view.endEditing(true)
        if(sender.selectedSegmentIndex == 0){
            selectedRow = -1
            loadDataFromDB()
        }else{
            if(isInternetAvailable()){
                selectedRow = -1
               loadDataFromWeb()
            }else{
                segment.selectedSegmentIndex = 0
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }

    }
    
    // MARK: - -------------------------Functions------------------------

    func deleteWdoGraphDrawHistoryRecordsFromDB() {
        
        // Delete reccords after getting count from setting
        
        let recordCount = "\(nsud.value(forKey: "WdoGraphDrawHistoryRecordsCount") ?? "")"
        
        var recordCountInt = 10
        if recordCount.count > 0 {
            recordCountInt = Int(recordCount)!
        }
                
        let context = getContext()
        
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WdoGraphDrawHistory")

        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "localCreatedDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let predicateL = NSPredicate(format: "workorderId == %@", strGlobalWoId)
        fetchRequest.predicate = predicateL

        do {
            let records = try context.fetch(fetchRequest) as! [NSManagedObject]
            for k in 0 ..< records.count {
                if k < recordCountInt {
                    
                }else{
                    let recordObjL = records[k]
                    // Delete data from Document Directory
                    let xmlImageName = "\(recordObjL.value(forKey: "woImagePath") ?? "")"
                    Global().removeImage(fromDocumentDirectory: xmlImageName)
                    let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                    Global().removeImage(fromDocumentDirectory: strImageNameSales)
                    context.delete(recordObjL)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func loadDataFromDB() {
        
        let sortDescriptor = NSSortDescriptor(key: "localCreatedDate", ascending: false)
        
        //arrayOfList = getDataFromCoreDataBaseArray(strEntity: "WdoGraphDrawHistory", predicate: NSPredicate(format: "workorderId == %@", strGlobalWoId))
        self.arrayOfList = NSMutableArray()
        let arrayOfListtemp = getDataFromCoreDataBaseArraySorted(strEntity: "WdoGraphDrawHistory", predicate: NSPredicate(format: "workorderId == %@", strGlobalWoId), sort: sortDescriptor)
        
        for item in arrayOfListtemp {
            let objTemp = item as! NSManagedObject
            let graphXML = objTemp.value(forKey: "graphXML") as! String
            // parse xml document
            let xml = try! XML.parse(graphXML)
            let strcount =  xml.mxGraphModel.root.mxCell.all?.count
            if(strcount! > 3){
                let aaygraphXML = self.arrayOfList.filter { (task) -> Bool in
                    return ("\((task as! NSManagedObject).value(forKey: "graphXML")!)".contains(graphXML)) }
                if aaygraphXML.count == 0 {
                    self.arrayOfList.add(item)
                }
               
            }
            print("------------------" , graphXML)
            
        }
        
        
        if arrayOfList.count > 0 {
            
            tblView.isHidden = false
            tblView.reloadData()
            
        } else {
            
            tblView.isHidden = true

        }
        
    }
    func loadDataFromWeb() {
        arrayOfList = NSMutableArray()
        tblView.reloadData()
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
     
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWoWDOGraphVersionListByMobile?CompanyKey=\(strCompanyKey)&WorkorderId=\(strGlobalWoId)"
      
        let dict = NSMutableDictionary()
        
        WebService.callAPIBYGETWithoutKey(parameter: dict, url: strURL) { (response, status) in
            self.loader.dismiss(animated: false) { [self] in
                print(response)
                if(status){
                            if(response.value(forKey: "data") is NSArray){
                                let arytemp = (response.value(forKey: "data") as! NSArray)
                                arrayOfList =  NSMutableArray(array: arytemp.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                                tblView.reloadData()
                            }
                        }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    
    func restoreWebGraph(dict : NSDictionary)  {
        
        
        if(dict.count != 0){
   
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "\(dict.value(forKey: "GraphXmlPath") ?? "")".replacingOccurrences(of: "\\", with: "//")
                var loader = UIAlertController()
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                AF.request(strURL, method: .get).responseString { responce in
                    print(responce)
                    loader.dismiss(animated: false) { [self] in
                        
                        if responce.value?.count != 0{
                            
                            let xml = try! XML.parse(responce.value!)
                            let strcount =  xml.mxGraphModel.root.mxCell.all?.count
                            if(strcount! > 3){
                                self.saveGraphXmlToImageDetail(dictData: dict, graphXmll: responce.value!)
                              
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Graph is blank.", viewcontrol: self)

                            }
                        
                        }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }

          
                }

        }
    }
    
    
    func deleteMainGraphImageBeforeSaving() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlImageName = "\(dictData.value(forKey: "woImagePath") ?? "")"

            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"true"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"True"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"1"))
            
            //Global().removeImage(fromDocumentDirectory: xmlImageName)
            
            let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            //Global().removeImage(fromDocumentDirectory: strImageNameSales)
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strImageNameSales))

            // jugad for deleting sales image if synced from web
            
            let strNameTemp = "UploadImages\\" + strImageNameSales
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strNameTemp))

        }
        
    }
    
    func  saveGraphData(dictOfData : NSManagedObject) {
        
        let xmlFileName = "\\Documents\\GraphXMLFiles\\XML"  + "MainGraph" + "\(strGlobalWoId)\(getUniqueValueForId())" + ".xml"

        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")

        arrOfKeys.add("woImageId")
        arrOfKeys.add("woImagePath")
        arrOfKeys.add("woImageType")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("graphXmlPath")
        arrOfKeys.add("isProblemIdentifaction")

        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strGlobalWoId)
        arrOfValues.add("\(dictOfData.value(forKey: "createdBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "createdDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "modifiedBy") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "modifiedDate") ?? "")")
        
        arrOfValues.add("\(dictOfData.value(forKey: "woImageId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "woImagePath") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "woImageType") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "imageCaption") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "imageDescription") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "latitude") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "longitude") ?? "")")
        arrOfValues.add(xmlFileName)

        let strIsProblemIdentifaction = "\(dictOfData.value(forKey: "isProblemIdentifaction") ?? "")"

        if strIsProblemIdentifaction == "1" ||  strIsProblemIdentifaction == "true" || strIsProblemIdentifaction == "True"{
     
            arrOfValues.add("true")
     
        }else{
     
            arrOfValues.add("false")
     
        }

        saveDataInDB(strEntity: Entity_WdoImages, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        // Save Same Data in Sales Auto Lead
                
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let strImageNameSales = "\(dictOfData.value(forKey: "woImagePath") ?? "")".replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
        
        let arrOfKeysSales = NSMutableArray()
        let arrOfValuesSales = NSMutableArray()
        
        arrOfKeysSales.add("createdBy")
        arrOfKeysSales.add("createdDate")
        arrOfKeysSales.add("companyKey")
        arrOfKeysSales.add("userName")
        arrOfKeysSales.add("leadImagePath")
        arrOfKeysSales.add("leadImageType")
        arrOfKeysSales.add("modifiedBy")
        arrOfKeysSales.add("leadImageId")
        arrOfKeysSales.add("modifiedDate")
        arrOfKeysSales.add("leadId")
        arrOfKeysSales.add("leadImageCaption")
        arrOfKeysSales.add("descriptionImageDetail")
        arrOfKeysSales.add("latitude")
        arrOfKeysSales.add("longitude")
        
        arrOfValuesSales.add("") //createdBy
        arrOfValuesSales.add("") //createdDate
        arrOfValuesSales.add(strCompanyKey)
        arrOfValuesSales.add(strUserName)
        arrOfValuesSales.add(strImageNameSales)
        arrOfValuesSales.add("Graph")
        arrOfValuesSales.add(strEmployeeid)
        arrOfValuesSales.add("\(dictOfData.value(forKey: "woImageId") ?? "")") // woImageId
        arrOfValuesSales.add("\(dictOfData.value(forKey: "modifiedDate") ?? "")")
        arrOfValuesSales.add(strWoLeadId)
        arrOfValuesSales.add("\(dictOfData.value(forKey: "imageCaption") ?? "")") // imageCaption
        arrOfValuesSales.add("\(dictOfData.value(forKey: "imageDescription") ?? "")") // imageDescription
        arrOfValuesSales.add("\(dictOfData.value(forKey: "latitude") ?? "")") // latitude
        arrOfValuesSales.add("\(dictOfData.value(forKey: "longitude") ?? "")") //  longitude
        
        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
        
        
        // Saving Graph XML To Document Directory
        
        let str = "\(dictOfData.value(forKey: "graphXML") ?? "")"
        
        //"\\Documents\\GraphXMLFiles\\XML"  + "Graph" + "\(strWoId)\(getUniqueValueForId())" + ".xml"
        
        let filename = getDocumentsDirectory().appendingPathComponent(xmlFileName)
        
        do {
            try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
        
        
        //Saving Image to Documents Directory.
        
        let strImagePath = "\(dictOfData.value(forKey: "graphXmlPath") ?? "")"
        
        let imageL: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
        
        if imageL != nil  {
            
            saveImageDocumentDirectory(strFileName: "\(dictOfData.value(forKey: "woImagePath") ?? "")", image: imageL!)
            
            // Save Image To Sales Auto Also
                    
            saveImageDocumentDirectory(strFileName: strImageNameSales, image: imageL!)
            
        }
        
    }
    
    
    func saveGraphXmlToImageDetail(dictData : NSDictionary , graphXmll : String) {
      
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strImageName = "\\Documents\\UploadImages\\Img"  + "Graph" + "\(strGlobalWoId)\(getUniqueValueForId())" + ".jpeg"
        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
        
        let str = graphXmll
        
        let xmlFileName = "\\Documents\\GraphXMLFiles\\XML"  + "Graph" + "\(strGlobalWoId)\(getUniqueValueForId())" + ".xml"

        let filename = getDocumentsDirectory().appendingPathComponent(xmlFileName)
        
        do {
            try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
           
       
  
        
       // saveGraphXmlToImageDetail(graphXmlPath: xmlFileName ,graphXmll: str)
        
        deleteMainGraphImageBeforeSaving()


        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("woImagePath")
        arrOfKeys.add("woImageType")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("woImageId")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("isProblemIdentifaction")
        arrOfKeys.add("graphXmlPath")

        
       
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let woImageId = "Mobile" + "\(strGlobalWoId)" + getUniqueValueForId()
        let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
        
        
        arrOfValues.add("") //createdBy
        arrOfValues.add("") //createdDate
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strImageName)
        arrOfValues.add("Graph")
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(woImageId) // woImageId
        arrOfValues.add(modifiedDate ?? "")
        arrOfValues.add(strGlobalWoId)
        arrOfValues.add("") // imageCaption
        arrOfValues.add("") // imageDescription
        arrOfValues.add(latitude) // latitude
        arrOfValues.add(longitude) //  longitude
        arrOfValues.add("true") //  isProblemIdentifaction
        arrOfValues.add(xmlFileName) //  isProblemIdentifaction

        saveDataInDB(strEntity: "ImageDetailsServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

        

        // Save Main Graph to service auto Also
        
        
        let arrOfKeysSales = NSMutableArray()
        let arrOfValuesSales = NSMutableArray()
        
        arrOfKeysSales.add("createdBy")
        arrOfKeysSales.add("createdDate")
        arrOfKeysSales.add("companyKey")
        arrOfKeysSales.add("userName")
        arrOfKeysSales.add("leadImagePath")
        arrOfKeysSales.add("leadImageType")
        arrOfKeysSales.add("modifiedBy")
        arrOfKeysSales.add("leadImageId")
        arrOfKeysSales.add("modifiedDate")
        arrOfKeysSales.add("leadId")
        arrOfKeysSales.add("leadImageCaption")
        arrOfKeysSales.add("descriptionImageDetail")
        arrOfKeysSales.add("latitude")
        arrOfKeysSales.add("longitude")
        
        arrOfValuesSales.add("") //createdBy
        arrOfValuesSales.add("") //createdDate
        arrOfValuesSales.add(strCompanyKey)
        arrOfValuesSales.add(strUserName)
        arrOfValuesSales.add(strImageNameSales)
        arrOfValuesSales.add("Graph")
        arrOfValuesSales.add(strEmployeeid)
        arrOfValuesSales.add(woImageId) // woImageId
        arrOfValuesSales.add(modifiedDate ?? "")
        arrOfValuesSales.add(strWoLeadId)
        arrOfValuesSales.add("") // imageCaption
        arrOfValuesSales.add("") // imageDescription
        arrOfValuesSales.add(latitude) // latitude
        arrOfValuesSales.add(longitude) //  longitude
        
        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
        
        // Create URL
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "\(dictData.value(forKey: "WoImagePath")!)".replacingOccurrences(of: "\\", with: "//")
        
            let url = URL(string: strURL)!

        if let data = try? Data(contentsOf: url) {
               // Create Image and Update Image View
              // imageView.image = UIImage(data: data)
            print(UIImage(data: data))
            
            saveImageDocumentDirectory(strFileName: strImageName, image: UIImage(data: data)!)
            
            // Save Image To Sales Auto Also
     
            saveImageDocumentDirectory(strFileName: strImageNameSales, image: UIImage(data: data)!)
            
            //WdoGraphDrawHistory
            let dictDataL = NSMutableDictionary()
            dictDataL.setObject(arrOfKeys, forKey: arrOfValues)
            ProblemIdentification_WDOVC().saveWdoGraphDrawHistory(graphXmll: graphXmll, decodedimage: UIImage(data: data)!, dictDataL: dictDataL)
            
            self.dismiss(animated: false) { [self] in
                delegate?.refresh()
            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Graph is not restored", viewcontrol: self)
        }

        
    }
    
    
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
        
    }
    
    // MARK: - -------------------------Actions------------------------

    @objc func actionOnCheckBox(sender: UIButton!)
    {
        
        if(sender.currentImage == UIImage(named: "NPMA_UnCheck")){
            
            selectedRow = sender.tag
            
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            
        }else{

            selectedRow = -1
            
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
        }
        
        self.tblView.reloadData()

    }
    
    @IBAction func action_LoadData(_ sender: UIButton) {
        
        self.view.endEditing(true)
        // For local restore
        if segment.selectedSegmentIndex == 0 {
            if selectedRow >= 0 {
                
                let alert = UIAlertController(title: Alert, message: "Are you sure to Restore Graph", preferredStyle: UIAlertController.Style.alert)
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                    
                    
                    
                }))
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction (title: "Yes - Restore Graph", style: .default, handler: { (nil) in
                    
                    // Delete Before Saving
                    self.deleteMainGraphImageBeforeSaving()
                    
                    // Save selected Graph.
                    
                    self.saveGraphData(dictOfData: self.arrayOfList[self.selectedRow] as! NSManagedObject)
                    
                    nsud.setValue(true, forKey: "yesRestoredLocalGraph")
                    nsud.synchronize()
                    
                    self.dismiss(animated: false) {
                        self.delegate?.refresh()
                    }
                    // After Saving Dismiss View
                      
                }))
                self.present(alert, animated: true, completion: nil)

                
            }

        }
        
        
        // For Web Graph restore

        else{
            if selectedRow >= 0 {
                
                let alert = UIAlertController(title: Alert, message: "Are you sure to Restore Graph", preferredStyle: UIAlertController.Style.alert)
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                    
                    
                    
                }))
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction (title: "Yes - Restore Graph", style: .default, handler: { [self] (nil) in
                    
                     
                    restoreWebGraph(dict: self.arrayOfList[self.selectedRow] as! NSDictionary)

                  
                    
                }))
                self.present(alert, animated: true, completion: nil)

                
            }

        }

    }
    
    @IBAction func action_Back(_ sender: Any) {
        
        self.view.endEditing(true)
//        if self.segment.selectedSegmentIndex == 0  {
//            //self.delegate?.refresh()
//
//        }else{
//            //self.delegate?.refreshWebGraph(dict: self.arrayOfList[self.selectedRow] as! NSDictionary)
//           // self.delegate?.refresh()
//
//        }
        self.dismiss(animated: false, completion: nil)

    }
    
}


// MARK: - -------------------------UITableViewDelegate------------------------

extension WdoGraphDrawHistoryVC_iPad : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return arrayOfList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if segment.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellWdoGraphDrawHistory", for: indexPath as IndexPath) as! CellWdoGraphDrawHistory
            tblView.separatorColor = UIColor.theme()

            let objTemp = arrayOfList[indexPath.row] as! NSManagedObject
            
            //cell.lblName.text = "\(objTemp.value(forKey: "graphImageName") ?? "")"
            
            cell.lblName.text = "Graph " + "\(arrayOfList.count-indexPath.row)" + "\n\n" + "Created Date: \(objTemp.value(forKey: "createdDate") ?? "")"

            let strImagePath = objTemp.value(forKey: "graphXmlPath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgView.image = image
                
            }
            
            if(indexPath.row == selectedRow){
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_Check.png"), for: .normal)
            }else{
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck.png"), for: .normal)
            }
            
            cell.btnCheckBox.tag = indexPath.row
            cell.btnCheckBox.addTarget(self, action: #selector(actionOnCheckBox), for: .touchUpInside)
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellWdoGraphDrawHistory", for: indexPath as IndexPath) as! CellWdoGraphDrawHistory
            tblView.separatorColor = UIColor.theme()
            let objTemp = arrayOfList[indexPath.row] as! NSDictionary
            cell.lblName.text = "Graph " + "\(arrayOfList.count-indexPath.row)" + "\n\n" + "Created Date: \(Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(objTemp.value(forKey: "CreatedDate")!)") ?? "")"
    //    https://psrsstaging.pestream.com////Documents//UploadImages////Graph834633_202111250718297258.jpg
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "\(objTemp.value(forKey: "WoImagePath")!)".replacingOccurrences(of: "\\", with: "//")
            
            cell.imgView.setImageWith(URL(string: strURL), placeholderImage: UIImage(named: "NoImage.jpg"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                //print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            
            if(indexPath.row == selectedRow){
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_Check.png"), for: .normal)
            }else{
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck.png"), for: .normal)
            }
            
            cell.btnCheckBox.tag = indexPath.row
            cell.btnCheckBox.addTarget(self, action: #selector(actionOnCheckBox), for: .touchUpInside)
            
            return cell

        }
        
       
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {

        return tableView.rowHeight
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if segment.selectedSegmentIndex == 0 {
            let image: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image)
            
            let objTemp = arrayOfList[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "graphXmlPath")
            
            let imageL: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if imageL != nil  {
                imageView = UIImageView(image: imageL)
            }

            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
        }else{
            
            let objTemp = arrayOfList[indexPath.row] as! NSDictionary
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "\(objTemp.value(forKey: "WoImagePath")!)".replacingOccurrences(of: "\\", with: "//")
            
            let safariVC = SFSafariViewController(url: NSURL(string: strURL)! as URL)
                       self.present(safariVC, animated: true, completion: nil)
                       safariVC.delegate = self
            
        }
        
     
        
    }
    
    func reloadWithAnimation() {
        
        tblView.reloadData()
        let tableViewHeight = tblView.bounds.size.height
        let cells = tblView.visibleCells
        
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.5, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
        
}


// MARK: - -------------------------SFSafariViewControllerDelegate------------------------

extension WdoGraphDrawHistoryVC_iPad : SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
              controller.dismiss(animated: true, completion: nil)
          }
}
