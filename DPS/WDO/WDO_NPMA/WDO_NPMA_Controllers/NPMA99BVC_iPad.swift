//
//  Form99BVC.swift
//  DPS
//
//  Created by Rakesh Jain on 05/05/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class NPMA99BVC_iPad: UIViewController {

    // Outlets
    
    // Section 1. General Information
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyAddress: ACFloatingTextField!
    @IBOutlet weak var txtfldCity: ACFloatingTextField!
    @IBOutlet weak var txtfldState: ACFloatingTextField!
    @IBOutlet weak var txtfldZip: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyBusinessLicNo: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyPhoneNo: ACFloatingTextField!
    @IBOutlet weak var txtfldFHACaseNo: ACFloatingTextField!
    
   // Section 2. Builder Information
    @IBOutlet weak var txtfldCompanyName_BuilderInformation: ACFloatingTextField!
    @IBOutlet weak var txtfldPhoneNo_BuilderInformation: ACFloatingTextField!
    
     // Section 3. Property Information
    @IBOutlet weak var txtfldLocationOfStructure: ACFloatingTextField!
    
    // Section 4. Service Information
    @IBOutlet weak var txtfldDateOfService: ACFloatingTextField!
    @IBOutlet weak var txtfldDescription: ACFloatingTextField!
    @IBOutlet weak var btnSlab: UIButton!
    @IBOutlet weak var btnBasement: UIButton!
    @IBOutlet weak var btnCrawl: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var btnSoilAppliedLiquidTermiticide: UIButton!
    @IBOutlet weak var txtfldBrandNameTermiticide_SoilApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldEPARegNo_SoilApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldApproxDilution_SoilApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldApproxTotalGallon_SoilApplied: ACFloatingTextField!
    @IBOutlet weak var btnYes_TreatmentCompletedOnExterior: UIButton!
    @IBOutlet weak var btnNo_TreatmentCompletedOnExteior: UIButton!
    @IBOutlet weak var btnWoodAppliedLiquidTermiticide: UIButton!
    @IBOutlet weak var txtfldBrandNameTermiticide_WoodApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldEPARegNo_WoodApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldApproxDilution_WoodApplied: ACFloatingTextField!
    @IBOutlet weak var txtfldApproxTotalGallon_WoodApplied: ACFloatingTextField!
    @IBOutlet weak var btnBaitSystemInstalled: UIButton!
    @IBOutlet weak var txtfldNameOfSystem_BaitSystemInstalled: ACFloatingTextField!
    @IBOutlet weak var txtfldEPARegNo_BaitSystemInstalled: ACFloatingTextField!
    @IBOutlet weak var btnPhysicalBarrierSystemInstalled: UIButton!
    @IBOutlet weak var txtfldNameOfSystem_PhysicalBarrierSystemInstalled: ACFloatingTextField!
    @IBOutlet weak var txtfldAttachInstallationInformation: ACFloatingTextField!
    @IBOutlet weak var btnYesServiceAgreementAvailable: UIButton!
    @IBOutlet weak var btnNoServiceAgreementAvailable: UIButton!
    @IBOutlet weak var txtfldAttachmentList: ACFloatingTextField!
    @IBOutlet weak var txtviewComment: UITextView!
    @IBOutlet weak var txtfldNameOfApplicator: ACFloatingTextField!
    @IBOutlet weak var txtfldCertificationNo: ACFloatingTextField!
    @IBOutlet weak var imgviewSignature: UIImageView!
    @IBOutlet weak var btnDeleteSignature: UIButton!
    @IBOutlet weak var txtfldDate: ACFloatingTextField!
    @IBOutlet weak var viewContainerSignBtns: UIView!
    @IBOutlet weak var txtfldNoOfStationsInstalled: ACFloatingTextField!
    
    
    // MARK: view lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

       initialSetUp()
        
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnSlab(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnBasement(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnCrawl(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }

    @IBAction func actionOnOther(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnSoilAppliedLiquidTermiticide(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnYes_TreatmentCompletedOnExterior(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            btnNo_TreatmentCompletedOnExteior.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnNo_TreatmentCompletedOnExterior(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            btnYes_TreatmentCompletedOnExterior.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnWoodAppliedLiquidTermiticide(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnBaitSystemInstalled(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnPhysicalBarrierSystemInstalled(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnYes_ServiceAgreementAvailable(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
         btnNoServiceAgreementAvailable.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnNo_ServiceAgreementAvailable(_ sender: UIButton) {
        
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        btnYesServiceAgreementAvailable.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
    @IBAction func actionOnSignature(_ sender: UIButton) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
               let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
               vc!.strMessage = "99 B"//"strEmpName
               vc!.delegate = self
               vc?.modalPresentationStyle = .fullScreen
               self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func actionOnDeleteSignature(_ sender: UIButton) {
               imgviewSignature.image = nil
               btnDeleteSignature.isHidden = true
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        
    }
    // MARK: Functions
    fileprivate func initialSetUp(){
        
        txtviewComment.layer.cornerRadius = 5.0
        txtviewComment.layer.borderWidth = 1.0
        txtviewComment.layer.borderColor = UIColor.theme()?.cgColor
        
        viewContainerSignBtns.layer.borderWidth = 1.0
        viewContainerSignBtns.layer.borderColor = UIColor.theme()?.cgColor
      
        btnDeleteSignature.isHidden = true
    }
    
    func gotoDatePickerView(sender: AnyObject, strType:String)  {
           
           let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
           
           let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
           vc.strTag = sender.tag
           vc.chkForMinDate = true
           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
           vc.modalTransitionStyle = .coverVertical
           //vc.handleOnSelectionOfDate = self
           vc.handleDateSelectionForDatePickerProtocol = self
           
           vc.strType = strType
           self.present(vc, animated: true, completion: {})
           
       }
       
}
// MARK: ------------- Signature Delegate --------------

extension NPMA99BVC_iPad : SignatureViewDelegate{
    
    func imageFromSignatureView(image: UIImage)
    {
       
            imgviewSignature.image = image
            btnDeleteSignature.isHidden = false
       
    }
}

extension NPMA99BVC_iPad:UITextFieldDelegate,UITextViewDelegate{
    
    // MARK:UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == txtfldDateOfService){
            
             gotoDatePickerView(sender: txtfldDateOfService, strType: "Date")
            return false
        }
        if(textField == txtfldDate){
            
             gotoDatePickerView(sender: txtfldDate, strType: "Date")
            return false
        }
        if(textField == txtfldAttachmentList){
            // here open file picker to attach docs
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfldCompanyName)
        {
            if(string == " " && txtfldCompanyName.text?.count == 0)
            {
                return false
            }
        }
        if(textField == txtfldCompanyAddress )
        {
            if(string == " " && txtfldCompanyAddress.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldCity)
        {
            if(string == " " && txtfldCity.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldState)
        {
            if(string == " " && txtfldState.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldCompanyBusinessLicNo)
        {
            if(string == " " && txtfldCompanyBusinessLicNo.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldFHACaseNo)
        {
            if(string == " " && txtfldFHACaseNo.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldCompanyName_BuilderInformation)
        {
            if(string == " " && txtfldCompanyName_BuilderInformation.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldLocationOfStructure)
        {
            if(string == " " && txtfldLocationOfStructure.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldDescription)
        {
            if(string == " " && txtfldDescription.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldBrandNameTermiticide_SoilApplied)
        {
            if(string == " " && txtfldBrandNameTermiticide_SoilApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldEPARegNo_SoilApplied)
        {
            if(string == " " && txtfldEPARegNo_SoilApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldApproxDilution_SoilApplied)
        {
            if(string == " " && txtfldApproxDilution_SoilApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldApproxTotalGallon_SoilApplied)
        {
            if(string == " " && txtfldApproxTotalGallon_SoilApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldBrandNameTermiticide_WoodApplied)
        {
            if(string == " " && txtfldBrandNameTermiticide_WoodApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldEPARegNo_WoodApplied)
        {
            if(string == " " && txtfldEPARegNo_WoodApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldApproxDilution_WoodApplied)
        {
            if(string == " " && txtfldApproxDilution_WoodApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldApproxTotalGallon_WoodApplied)
        {
            if(string == " " && txtfldApproxTotalGallon_WoodApplied.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldNameOfSystem_BaitSystemInstalled)
        {
            if(string == " " && txtfldNameOfSystem_BaitSystemInstalled.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldEPARegNo_BaitSystemInstalled)
        {
            if(string == " " && txtfldEPARegNo_BaitSystemInstalled.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldNoOfStationsInstalled)
        {
            if(string == " " && txtfldNoOfStationsInstalled.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldNameOfSystem_PhysicalBarrierSystemInstalled)
        {
            if(string == " " && txtfldNameOfSystem_PhysicalBarrierSystemInstalled.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldAttachInstallationInformation)
        {
            if(string == " " && txtfldAttachInstallationInformation.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldNameOfApplicator)
        {
            if(string == " " && txtfldNameOfApplicator.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldCertificationNo)
        {
            if(string == " " && txtfldCertificationNo.text?.count == 0)
            {
                return false
            }
            return true
        }
        return true
    }
    
    // MARK:UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        return true
    }
    
}

extension NPMA99BVC_iPad: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        if(tag == 1){
            
            txtfldDateOfService.text = strDate
        }
        if(tag == 2){
            
            txtfldDate.text = strDate
        }
        
    }
}
