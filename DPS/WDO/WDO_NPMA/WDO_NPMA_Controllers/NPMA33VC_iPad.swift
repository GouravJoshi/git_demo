//
//  NPMA33VC_iPad.swift
//  DPS
//
//  Created by Rakesh Jain on 23/04/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class NPMA33VC_iPad: UIViewController {
    
    // Outlets
    
    // Section1 outlets
    @IBOutlet weak var txtfldInspectionCompany: ACFloatingTextField!
    @IBOutlet weak var txtfldAddress: ACFloatingTextField!
    @IBOutlet weak var txtfldPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyBusinessLicNo: ACFloatingTextField!
    @IBOutlet weak var txtfldDateOfInspection: ACFloatingTextField!
    @IBOutlet weak var txtfldAddOfPropertyInspected: ACFloatingTextField!
    @IBOutlet weak var txtfldInspectorNameSignLicNo: ACFloatingTextField!
    @IBOutlet weak var txtfldStructureInspected: ACFloatingTextField!
    
    // Section2 outlets
    @IBOutlet weak var btnNoVisibleEvidenceSection2: UIButton!
    @IBOutlet weak var btnVisibleEvidenceSection2: UIButton!
    @IBOutlet weak var btnLiveInsectsSection2: UIButton!
    @IBOutlet weak var txtviewDescriptionLiveInsectsSection2: UITextView!
    @IBOutlet weak var btnDeadInsectsSection2: UIButton!
    @IBOutlet weak var txtviewDescriptionDeadInsectsSection2: UITextView!
    @IBOutlet weak var btnVisibleDamageSection2: UIButton!
    @IBOutlet weak var txtviewDescriptionVisibleDamageSection2: UITextView!
    
    // Section3 outlets
    @IBOutlet weak var btnNoActionSection3: UIButton!
    @IBOutlet weak var txtviewRecommend1Section3: UITextView!
    @IBOutlet weak var btnRecommendActionSection3: UIButton!
    @IBOutlet weak var txtviewRecommend2Section3: UITextView!
    
    // Section4 outlets
    
    @IBOutlet weak var btnBasement: UIButton!
    @IBOutlet weak var txtfldBasement: ACFloatingTextField!
    @IBOutlet weak var btnCrawlspace: UIButton!
    @IBOutlet weak var txtfldCrawlSpace: ACFloatingTextField!
    @IBOutlet weak var btnMainLevel: UIButton!
    @IBOutlet weak var txtfldMainLevel: ACFloatingTextField!
    @IBOutlet weak var btnAttic: UIButton!
    @IBOutlet weak var txtfldAttic: ACFloatingTextField!
    @IBOutlet weak var btnGarage: UIButton!
    @IBOutlet weak var txtfldGarage: ACFloatingTextField!
    @IBOutlet weak var btnExterior: UIButton!
    @IBOutlet weak var txtfldExterior: ACFloatingTextField!
    @IBOutlet weak var btnPorch: UIButton!
    @IBOutlet weak var txtfldPorch: ACFloatingTextField!
    @IBOutlet weak var btnAddition: UIButton!
    @IBOutlet weak var txtfldAddition: ACFloatingTextField!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var txtfldOther: ACFloatingTextField!
    
    // Section5 outlets
    
    @IBOutlet weak var txtviewCommentsSection5: UITextView!
    @IBOutlet weak var txtfldAttachmentsSection5: ACFloatingTextField!
    @IBOutlet weak var imgviewSignOfSeller: UIImageView!
    @IBOutlet weak var viewContainerSignBtns: UIView!
    @IBOutlet weak var btnDeleteSellerSign: UIButton!
    
    @IBOutlet weak var imgviewSignOfBuyer: UIImageView!
    @IBOutlet weak var btnDeleteBuyerSign: UIButton!
    
    // variable
    var signatureType = ""
    
    // MARK: view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnNoVisibleEvidenceSection2(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnVisibleEvidenceSection2(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnLiveInsectsSection2(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnDeadInsectsSection2(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnVisibleDamageSection2(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnNoActionSection3(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnRecommendActionSection3(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnBasement(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnCrawlspace(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnMainLevel(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnAttic(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnGarage(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnExterior(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnPorch(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnAddition(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnOther(_ sender: UIButton) {
       
        if(sender.currentImage!.pngData() == UIImage(named: "uncheck.png")!.pngData()){
            
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    @IBAction func actionOnSellerSignature(_ sender: UIButton) {
        
       // yesEditedSomething = true
        signatureType = "seller"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = "NPMA33"//"strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnDeleteSellerSignature(_ sender: UIButton) {
     
        imgviewSignOfSeller.image = nil
        btnDeleteSellerSign.isHidden = true
       
    }
    
    @IBAction func actionOnBuyerSignature(_ sender: UIButton) {
        
        // yesEditedSomething = true
        signatureType = "buyer"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = "NPMA33"//"strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func actionOnDeleteBuyerSignature(_ sender: UIButton) {
        imgviewSignOfBuyer.image = nil
        btnDeleteBuyerSign.isHidden = true
       
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        
    }
    
    
    // MARK: Functions
    fileprivate func initialSetUp(){
        
        txtviewDescriptionLiveInsectsSection2.layer.cornerRadius = 5.0
        txtviewDescriptionLiveInsectsSection2.layer.borderWidth = 1.0
        txtviewDescriptionLiveInsectsSection2.layer.borderColor = UIColor.theme()?.cgColor
        
        txtviewDescriptionDeadInsectsSection2.layer.cornerRadius = 5.0
        txtviewDescriptionDeadInsectsSection2.layer.borderWidth = 1.0
        txtviewDescriptionDeadInsectsSection2.layer.borderColor = UIColor.theme()?.cgColor
        
        txtviewDescriptionVisibleDamageSection2.layer.cornerRadius = 5.0
        txtviewDescriptionVisibleDamageSection2.layer.borderWidth = 1.0
        txtviewDescriptionVisibleDamageSection2.layer.borderColor = UIColor.theme()?.cgColor
        
        
        txtviewRecommend1Section3.layer.cornerRadius = 5.0
        txtviewRecommend1Section3.layer.borderWidth = 1.0
        txtviewRecommend1Section3.layer.borderColor = UIColor.theme()?.cgColor
        
        txtviewRecommend2Section3.layer.cornerRadius = 5.0
        txtviewRecommend2Section3.layer.borderWidth = 1.0
        txtviewRecommend2Section3.layer.borderColor = UIColor.theme()?.cgColor
        
        txtviewCommentsSection5.layer.cornerRadius = 5.0
        txtviewCommentsSection5.layer.borderWidth = 1.0
        txtviewCommentsSection5.layer.borderColor = UIColor.theme()?.cgColor
        
        viewContainerSignBtns.layer.borderWidth = 1.0
        viewContainerSignBtns.layer.borderColor = UIColor.theme()?.cgColor
        
        btnDeleteSellerSign.isHidden = true
        btnDeleteBuyerSign.isHidden = true
        
    }
    func gotoDatePickerView(sender: AnyObject, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
}

extension NPMA33VC_iPad:UITextFieldDelegate,UITextViewDelegate{
    
    // MARK:UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == txtfldDateOfInspection){
            
             gotoDatePickerView(sender: txtfldDateOfInspection, strType: "Date")
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtfldInspectionCompany)
        {
            if(string == " " && txtfldInspectionCompany.text?.count == 0)
            {
                return false
            }
        }
        if(textField == txtfldAddress )
        {
            if(string == " " && txtfldAddress.text?.count == 0)
            {
                return false
            }
            return true
        }
        
        if(textField == txtfldCompanyBusinessLicNo)
        {
            if(string == " " && txtfldCompanyBusinessLicNo.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldAddOfPropertyInspected)
        {
            if(string == " " && txtfldAddOfPropertyInspected.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldInspectorNameSignLicNo)
        {
            if(string == " " && txtfldInspectorNameSignLicNo.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldStructureInspected)
        {
            if(string == " " && txtfldStructureInspected.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldBasement)
        {
            if(string == " " && txtfldBasement.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldCrawlSpace)
        {
            if(string == " " && txtfldCrawlSpace.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldMainLevel)
        {
            if(string == " " && txtfldMainLevel.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldAttic)
        {
            if(string == " " && txtfldAttic.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldGarage)
        {
            if(string == " " && txtfldGarage.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldExterior)
        {
            if(string == " " && txtfldExterior.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldPorch)
        {
            if(string == " " && txtfldPorch.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldAddition)
        {
            if(string == " " && txtfldAddition.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldOther)
        {
            if(string == " " && txtfldOther.text?.count == 0)
            {
                return false
            }
            return true
        }
        if(textField == txtfldAttachmentsSection5)
        {
            if(string == " " && txtfldAttachmentsSection5.text?.count == 0)
            {
                return false
            }
            return true
        }
        return true
    }
    
    // MARK:UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        return true
    }
    
}
extension NPMA33VC_iPad: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        txtfldDateOfInspection.text = strDate
    }
}
// MARK: ------------- Signature Delegate --------------

extension NPMA33VC_iPad : SignatureViewDelegate{
    
    func imageFromSignatureView(image: UIImage)
    {
        if signatureType == "seller"
        {
            imgviewSignOfSeller.image = image
            btnDeleteSellerSign.isHidden = false
        }
        else if signatureType == "buyer"
        {
            imgviewSignOfBuyer.image = image
            btnDeleteBuyerSign.isHidden = false
        }
    }
}
