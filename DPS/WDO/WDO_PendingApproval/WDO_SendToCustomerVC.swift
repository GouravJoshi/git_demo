//
//  WDO_SendToCustomerVC.swift
//  DPS
//
//  Created by Navin Patidar on 28/07/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit
class WDO_SendToCustomerVC: UIViewController {
    // MARK: -
    // MARK: ------------IBOutlet--------------
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_Subject: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lbl_FromEmail: UILabel!
    // MARK: -
    // MARK: ------------Variable--------------
    var objWO = NSManagedObject()
    var strPricingErrorMessage = "" , strReportErrorMessage = ""
    // MARK: -
    // MARK: ------------Variable--------------
    var aryEmailList = NSMutableArray()
    var aryDocumentList = NSMutableArray()
    var dictLoginData = NSDictionary()
    // MARK: -
    // MARK: ------------View Life Cycle--------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(nsud.value(forKey: "LoginDetails") != nil){
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            UIInitialization()
            setEmailSubject()
            fetchEmailList()
            fetchDocumentList()
        }else{

            let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                self.navigationController?.popViewController(animated: false)

            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: -
    // MARK: ------------Get Email Add Email AND Update Email -------------
    func setEmailSubject()  {
        //Show From Email
        if("\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType") ?? "")" == "CompanyEmail"){
            lbl_FromEmail.text = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail") ?? "")"
        }else{
            lbl_FromEmail.text = "\(dictLoginData.value(forKeyPath: "EmployeeEmail") ?? "")"
        }
        if(lbl_FromEmail.text?.count == 0){
            lbl_FromEmail.text = "\(dictLoginData.value(forKeyPath: "Company.Email") ?? "")"
        }
        //For Subject
        Call_GetEmailSubject()
    }

    func fetchEmailList()  {
        print(objWO)
        // Fetch Email
        let arrayEmailDetail = getDataFromLocal(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && userName == %@", "\(objWO.value(forKey: "workorderId") ?? "")" , "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"))
        self.aryEmailList = NSMutableArray()
        self.aryEmailList = arrayEmailDetail.mutableCopy() as! NSMutableArray
        print(self.aryEmailList)
        self.tv_List.reloadData()
    }
    func updateMailStatus(index : Int) {
        let dict = (aryEmailList.object(at: index)as! NSManagedObject)
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("isMailSent")
        
        let  isMailSent = "\(dict.value(forKey: "isMailSent")!)".lowercased()
        if(isMailSent == "true" || isMailSent == "1"){
            arrOfValues.add("false")
        }else{
            arrOfValues.add("true")
        }
    
        let isSuccess = getDataFromDbToUpdate(strEntity: "EmailDetailServiceAuto", predicate: NSPredicate(format: "userName == %@ && emailId == %@ && workorderId == %@",Global().getUserName(), "\(dict.value(forKey: "emailId") ?? "")", "\(objWO.value(forKey: "workorderId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if isSuccess {
            fetchEmailList()
        }
    }
    
    func SaveEmailInDataBase(strEmail : String , strSubject : String , strWOID : String)  {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("emailId")
        arrOfValues.add(strEmail)
        
        arrOfKeys.add("isCustomerEmail")
        arrOfValues.add("true")

        arrOfKeys.add("isMailSent")
        arrOfValues.add("true")

        arrOfKeys.add("isDefaultEmail")
        arrOfValues.add("false")

        arrOfKeys.add("subject")
        arrOfValues.add(strSubject)

        arrOfKeys.add("workorderId")
        arrOfValues.add(strWOID)
        
        arrOfKeys.add("woInvoiceMailId")
        arrOfValues.add("")
       
        saveDataInDB(strEntity: "EmailDetailServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        txt_Email.text = ""
        fetchEmailList()
    }
    // MARK: -
    // MARK: ------------Get Document and update Document-------------
    func fetchDocumentList() {
        print(objWO)
        // Fetch Email
        let arrayDocumentDetail = getDataFromLocal(strEntity: "WoOtherDocuments", predicate: NSPredicate(format: "workorderId == %@ && userName == %@", "\(objWO.value(forKey: "workorderId") ?? "")" , "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"))
       
        self.aryDocumentList = NSMutableArray()
        for item in arrayDocumentDetail {
            if item is NSManagedObject {
                let dict = (item as! NSManagedObject)
               
              
                let  isDefault = "\(dict.value(forKey: "isDefault")!)".lowercased()
                if(isDefault == "true" || isDefault == "1"){
                    dict.setValue("true", forKey: "isChecked")
                }else{
                    dict.setValue("false", forKey: "isChecked")
                }
                self.aryDocumentList.add(dict)
            }
        }
     //   self.aryDocumentList = arrayDocumentDetail.mutableCopy() as! NSMutableArray
        print(self.aryDocumentList)
        self.tv_List.reloadData()
    }
    func updateDocumentStatus(index : Int) {
        let dict = (aryDocumentList.object(at: index)as! NSManagedObject)
    
        let  isChecked = "\(dict.value(forKey: "isChecked")!)".lowercased()
        if(isChecked == "true" || isChecked == "1"){
            dict.setValue("false", forKey: "isChecked")
        }else{
            dict.setValue("true", forKey: "isChecked")
        }
        aryDocumentList.replaceObject(at: index, with: dict)
        tv_List.reloadData()
        
        
    }
    
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        lbl_Title.text = "Send To Customer"
        txt_Email.delegate = self
        txt_Email.layer.cornerRadius = 3.0
        txt_Email.layer.borderColor = hexStringToUIColor(hex: "DBDBDB").cgColor
        txt_Email.layer.borderWidth = 1.0
      
        txt_Subject.delegate = self
        txt_Subject.layer.cornerRadius = 3.0
        txt_Subject.layer.borderColor = hexStringToUIColor(hex: "DBDBDB").cgColor
        txt_Subject.layer.borderWidth = 1.0
        
        
      
    }
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionAddEmail(_ sender: UIButton) {
        let aryTemp = self.aryEmailList.filter { (task) -> Bool in
            return ("\((task as! NSManagedObject).value(forKey: "emailId") ?? "")"  == txt_Email.text ?? "")  }
        
        if(txt_Email.text?.trimmed == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email is required.", viewcontrol: self)
        }else if(aryTemp.count != 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email Id already exist", viewcontrol: self)
        }
        
        else if !(validateEmail(email: txt_Email.text!.trimmed)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email is invalid.", viewcontrol: self)
        }else{
            SaveEmailInDataBase(strEmail: txt_Email.text!, strSubject: txt_Subject.text!, strWOID: "\(objWO.value(forKey: "workorderId") ?? "")")
        }
    }
    
    @IBAction func actionSendEmail(_ sender: UIButton) {
        let aryTemp = self.aryEmailList.filter { (task) -> Bool in
            return ("\((task as! NSManagedObject).value(forKey: "isMailSent") ?? "")"  == "1" || "\((task as! NSManagedObject).value(forKey: "isMailSent") ?? "")".lowercased()  == "true")  }
        if(aryTemp.count != 0){
            Call_SendtoCustomerAPI()
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select email address.", viewcontrol: self)
        }
    }
    
    
    // MARK: -
    // MARK: ------------Calling API------------
    
    func getEmailSubject(){
        if isInternetAvailable() {
            
        }
    }
    
}
// MARK: -
// MARK: ------------UITableViewDelegate--------------

extension WDO_SendToCustomerVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return section == 0 ? aryEmailList.count : aryDocumentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_SendToCustomerCell" : "WDO_SendToCustomerCell", for: indexPath as IndexPath) as! WDO_SendToCustomerCell
        //-----Email list
        if indexPath.section == 0 {
            let dict = (aryEmailList.object(at: indexPath.row)as! NSManagedObject)
            cell.btnCheckBox.isUserInteractionEnabled = false
            cell.lblTitle.text = "\(dict.value(forKey: "emailId")!)"
            let  isMailSent = "\(dict.value(forKey: "isMailSent")!)".lowercased()
            if(isMailSent == "true" || isMailSent == "1"){
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
               // cell.accessoryType = .checkmark
            }else{
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

               // cell.accessoryType = .none
            }
        }
        //----Document List

        else{
            let dict = (aryDocumentList.object(at: indexPath.row)as! NSManagedObject)
            cell.lblTitle.text = "\(dict.value(forKey: "title") ?? "")"
            let  isChecked = "\(dict.value(forKey: "isChecked") ?? "")".lowercased()
            let  isDefault = "\(dict.value(forKey: "isDefault") ?? "")".lowercased()
            cell.btnCheckBox.isUserInteractionEnabled = false

            if(isChecked == "true" || isChecked == "1")  {
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
              //  cell.accessoryType = .checkmark
            }else{
                cell.btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
               // cell.accessoryType = .none
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //-----Email List
        if indexPath.section == 0 {
            updateMailStatus(index: indexPath.row)
   
        }
        //-----DocumentList
        else{
            updateDocumentStatus(index: indexPath.row)
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return (aryEmailList.count != 0  ? "" : "No Email")
        default:
            return  (aryDocumentList.count != 0  ? "" : "No Document")

        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        switch section {
        case 0:
            return aryEmailList.count != 0  ? 0 : 100
        default:
            return aryDocumentList.count != 0  ? 0 : 100

        }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = .white
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.textAlignment = .center
       header.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Send Email to:"
        default:
            return "Select Documents:"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

         //For Header Background Color
        // view.tintColor = .groupTableViewBackground
        view.tintColor = hexStringToUIColor(hex: "DBDBDB")
        // For Header Text Color
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = hexStringToUIColor(hex: appThemeColor)
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
    }

}
// MARK: -
// MARK: ----------WDO_PendingApprovalListCell---------
class WDO_SendToCustomerCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
 
    @IBOutlet weak var btnCheckBox: UIButton!

    @IBOutlet var lblApprovalStatus: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
//MARK:------UITextFieldDelegate-----------
//MARK:
extension WDO_SendToCustomerVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
   
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 85)
    }
    
}
// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_SendToCustomerVC {
    func Call_GetEmailSubject() {

        if (isInternetAvailable()){
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "api/MobileToServiceAuto/GetEmailSubjectForMobile?CompanyKey=\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")&WorkOrderId=\(objWO.value(forKey: "workorderId") ?? "")"
            WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (response, status) in
                    print(response)
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            if (response.value(forKey: "data") as! NSDictionary).value(forKey: "Status") != nil {
                                let Result = "\((response.value(forKey: "data") as! NSDictionary).value(forKey: "Status") ?? "")"
                                if(Result.lowercased() == "1" || Result.lowercased() == "true"){
                                    txt_Subject.text = "\((response.value(forKey: "data") as! NSDictionary).value(forKey: "Subject") ?? "")"
                                }
                            }
                           
                        }
                    }else{
                    }
            }
        }
        else{

        }
    }
    
    func Call_SendtoCustomerAPI() {

        if (isInternetAvailable()){
            let woID = "\(objWO.value(forKey: "workorderId") ?? "")"
            
            let dictToSend = NSMutableDictionary()
            dictToSend.setValue(woID, forKey: "WorkorderId")
            dictToSend.setValue("\(txt_Subject.text ?? "")", forKey: "Subject")
            let aryOtherDocumentsExtDcs = NSMutableArray()
            for item in aryDocumentList {
                if (item is NSManagedObject) {
                    let isChecked = "\((item as! NSManagedObject).value(forKey: "isChecked")  ?? "")"
                    if(isChecked.lowercased() == "true" || isChecked == "1"){
                        let OtherDocumentId = "\((item as! NSManagedObject).value(forKey: "woOtherDocumentId")  ?? "")"
                        let OtherDocName = "\((item as! NSManagedObject).value(forKey: "otherDocumentPath")  ?? "")"
                        let OtherDocSysName = "\((item as! NSManagedObject).value(forKey: "otherDocSysName")  ?? "")"
                        let dict = NSMutableDictionary()
                        dict.setValue(OtherDocumentId, forKey: "OtherDocumentId")
                        dict.setValue(OtherDocName, forKey: "OtherDocName")
                        dict.setValue(OtherDocSysName, forKey: "OtherDocSysName")
                        aryOtherDocumentsExtDcs.add(dict)
                    }
                
                }
            }
            
            
            let aryEmailListExtDcs = NSMutableArray()
            for item in aryEmailList {
                if (item is NSManagedObject) {
                    let emailAddress = "\((item as! NSManagedObject).value(forKey: "emailId")  ?? "")"
                    let isChecked = "\((item as! NSManagedObject).value(forKey: "isMailSent")  ?? "")"
                    let dict = NSMutableDictionary()
                    dict.setValue(emailAddress, forKey: "EmailAddress")
                    dict.setValue(isChecked, forKey: "IsChecked")
                    aryEmailListExtDcs.add(dict)
                }
            }
            dictToSend.setValue(aryOtherDocumentsExtDcs, forKey: "OtherDocumentsExtDcs")
            dictToSend.setValue(aryEmailListExtDcs, forKey: "EmailListExtDcs")

            var jsonString = String()
            if(JSONSerialization.isValidJSONObject(dictToSend) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                print("Final Json : \(jsonString)")
            }
            
            
            if(aryEmailListExtDcs.count != 0){
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/Email/SendToCustomerEmailFromMobile"
                let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                WebService.postDataWithHeadersToServerDevice(dictJson: dictToSend, url: strURL, strType: "dict") { (response, status) in
                   loader.dismiss(animated: false) { [self] in
                    

                        print(response)
                        if(status){
                            
                            if(response.value(forKey: "data") is NSDictionary){
                                if (response.value(forKey: "data") as! NSDictionary).value(forKey: "Status") != nil {
                                    let Result = "\((response.value(forKey: "data") as! NSDictionary).value(forKey: "Status") ?? "")"
                                    if(Result.lowercased() == "1" || Result.lowercased() == "true"){
                                        let alert = UIAlertController(title: alertMessage, message: "Email has been successfully sent.", preferredStyle: UIAlertController.Style.alert)
                                        
                                        // add the actions (buttons)
                                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                            goToAppointment()                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                      
                                    }else{
                                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\((response.value(forKey: "data") as! NSDictionary).value(forKey: "Error") ?? "")", viewcontrol: self)
                                    }
                                }else{
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                                }
                               
                                
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                            }
                            
                    }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select or add email address.", viewcontrol: self)
            }
            
        
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    func goToAppointment() {
        var isdash = false
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AppointmentVC {
                isdash = true
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        
        if(!isdash){
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Appointment_iPAD" : "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "AppointmentVC") as! AppointmentVC
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
}
