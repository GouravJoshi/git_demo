//
//  WDO_PendingApprovalListVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class WDO_PendingApprovalListVC: UIViewController {
    
    // MARK: -
    // MARK: ------------IBOutlet--------------
   
    @IBOutlet weak var tv_List: UITableView!
    
    // MARK: -
    // MARK: ------------Variable--------------
    var aryReportList = NSMutableArray()
    var aryPricingList = NSMutableArray()

    var loader = UIAlertController()
    var refresher = UIRefreshControl()
    var strPricingErrorMessage = "" , strReportErrorMessage = ""
    var dispatchGroup = DispatchGroup()
    // MARK: -
    // MARK: ------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.refresher.addTarget(self, action: #selector(refreshloadData), for: .valueChanged)
        self.tv_List!.addSubview(refresher)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshListPricing(notification:)), name: Notification.Name("RefreshPricingApprovalList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshListReport(notification:)), name: Notification.Name("RefreshReportApprovalList"), object: nil)

        //UIInitialization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        UIInitialization()
        
    }
    
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        tv_List.tableFooterView = UIView()
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            Call_GetPendingPricingApprovalList()
            Call_GetPendingReportApprovalList()
            self.dispatchGroup.notify(queue: DispatchQueue.main, execute: { [self] in
                loader.dismiss(animated: false, completion: nil)
                self.refresher.endRefreshing()
            })
     
   
        }
    @objc func RefreshListPricing(notification: Notification) {
      
        aryPricingList = NSMutableArray()
        strPricingErrorMessage = ""
        tv_List.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            Call_GetPendingPricingApprovalList()
            NotificationCenter.default.removeObserver(self, name: Notification.Name("RefreshPricingApprovalList"), object: nil)
            
        }
    
    }
    @objc func RefreshListReport(notification: Notification) {
        aryReportList = NSMutableArray()
        strReportErrorMessage = ""
        tv_List.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in

        Call_GetPendingReportApprovalList()
        NotificationCenter.default.removeObserver(self, name: Notification.Name("RefreshReportApprovalList"), object: nil)
        }
    }
    
    
    func reloadTableView(){
        self.tv_List.dataSource = self
        self.tv_List.delegate = self
        self.tv_List.reloadData()
    }
    
    // MARK: ------------Refresh Data-------------

    
    @objc func refreshloadData() {
        
        UIInitialization()
        
    }
    
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_PendingApprovalListVC {
    func Call_GetPendingPricingApprovalList() {

        if (isInternetAvailable()){
            dispatchGroup.enter()
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkordersPendingPricingApproval?companyId=\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)&employeeId=\(Global().getEmployeeId() ?? "")"
  
          //  let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkordersPendingPricingApproval?companyId=3&employeeId=32171"
          
            WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (response, status) in
                    print(response)
                dispatchGroup.leave()
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"

                            if(dictData.value(forKey: "IsSuccess") as! Bool){
                                self.aryPricingList = NSMutableArray()
                                self.aryPricingList = (dictData.value(forKey: "Response") as! NSArray).mutableCopy() as! NSMutableArray
                                strPricingErrorMessage = ""
                                self.reloadTableView()
                            }else{
                                self.aryPricingList = NSMutableArray()
                                strPricingErrorMessage = ErrorMessage
                                self.reloadTableView()
                            }
                         
                        }
                    }else{
                        strPricingErrorMessage = alertSomeError
                        self.reloadTableView()
                    }
            }
        }
        else{
            strPricingErrorMessage = alertInternet
            self.reloadTableView()

        }
    }
    func Call_GetPendingReportApprovalList() {

        if (isInternetAvailable()){
            dispatchGroup.enter()
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkordersPendingReportApproval?companyId=\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)&employeeId=\(Global().getEmployeeId() ?? "")"

          // let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkordersPendingReportApproval?companyId=3&employeeId=32171"
          
            WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (response, status) in
                    print(response)
                dispatchGroup.leave()
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"

                            if(dictData.value(forKey: "IsSuccess") as! Bool){
                                self.aryReportList = NSMutableArray()
                                self.aryReportList = (dictData.value(forKey: "Response") as! NSArray).mutableCopy() as! NSMutableArray
                                strReportErrorMessage = ""
                                self.reloadTableView()
                            }else{
                                self.aryReportList = NSMutableArray()
                                self.strReportErrorMessage = ErrorMessage
                                self.reloadTableView()
                            }
                        }
                    }else{
                        self.strReportErrorMessage = alertSomeError
                        self.reloadTableView()
                    }
            }
        }
        else{
            strReportErrorMessage = alertInternet
            self.reloadTableView()

        }
    }
}

// MARK: -
// MARK: ------------UITableViewDelegate--------------

extension WDO_PendingApprovalListVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? aryReportList.count : aryPricingList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_PendingApprovalListCell" : "WDO_PendingApprovalListCell", for: indexPath as IndexPath) as! WDO_PendingApprovalListCell
        //-----ReportList
        if indexPath.section == 0 {
            let dict = aryReportList.object(at: indexPath.row) as! NSDictionary
            cell.lblAccountNumber.text = "\(dict.value(forKey: "AccountNo") ?? "")" + ", Wo # " + "\(dict.value(forKey: "WorkOrderNo") ?? ""), \(dict.value(forKey: "ServicePOCName") ?? "")"
            cell.lblAddress.text = "\(dict.value(forKey: "ServiceAddress") ?? "")"
            cell.lblInspectedBy.text = "Inspected by: \(dict.value(forKey: "InspectedBy") ?? "")"
            cell.lblApprovalStatus.text = ""
        }
        //-----PricingList

        else{
            let dict = aryPricingList.object(at: indexPath.row) as! NSDictionary
            cell.lblAccountNumber.text = "\(dict.value(forKey: "AccountNo") ?? "")" + ", Wo # " + "\(dict.value(forKey: "WorkOrderNo") ?? ""), \(dict.value(forKey: "ServicePOCName") ?? "")"
            cell.lblAddress.text = "\(dict.value(forKey: "ServiceAddress") ?? "")"
            cell.lblInspectedBy.text = "Inspected by: \(dict.value(forKey: "InspectedBy") ?? "")"
            
            
            cell.lblApprovalStatus.text = "\(dict.value(forKey: "PricingApprovalStatus") ?? "")" == "<null>"  ? "" : "\(dict.value(forKey: "PricingApprovalStatus") ?? "")"
            
            if "\(dict.value(forKey: "PricingApprovalStatus") ?? "")" == "Decline" || "\(dict.value(forKey: "PricingApprovalStatus") ?? "")" == "Declined"
            {
                cell.lblApprovalStatus.textColor = UIColor.red

            }else{
                cell.lblApprovalStatus.textColor = UIColor.black

            }
        }
       

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //-----ReportList
        if indexPath.section == 0 {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalReportVC") as? WDO_PendingApprovalReportVC
            vc!.reportview_From =  Enum_ReportviewFrom.ReportApproval
            vc!.dictPendingApproval = removeNullFromDict(dict: (aryReportList.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            self.navigationController?.pushViewController(vc!, animated: false)
        }
        //-----PricingList
        else{
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalDetailVC") as? WDO_PendingApprovalDetailVC
            vc!.dictPendingApproval = removeNullFromDict(dict: (aryPricingList.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return strReportErrorMessage == "" ? (aryReportList.count != 0  ? "" : "No reports for approval") :  strReportErrorMessage
        default:
            return strPricingErrorMessage == "" ? (aryPricingList.count != 0  ? "" : "No reports for approval") :  strPricingErrorMessage

        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        switch section {
        case 0:
            return aryReportList.count != 0  ? 0 : 100
        default:
            return aryPricingList.count != 0  ? 0 : 100

        }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = .white
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.textAlignment = .center
       header.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Report Approval"
        default:
            return "Pricing Approval"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

         //For Header Background Color
        // view.tintColor = .groupTableViewBackground
        view.tintColor = hexStringToUIColor(hex: "DBDBDB")
        // For Header Text Color
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = hexStringToUIColor(hex: appThemeColor)
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
    }

}
// MARK: -
// MARK: ----------WDO_PendingApprovalListCell---------
class WDO_PendingApprovalListCell: UITableViewCell {
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblInspectedBy: UILabel!

    @IBOutlet var lblApprovalStatus: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
