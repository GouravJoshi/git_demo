//
//  WDO_PendingApprovalReportVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import WebKit

enum Enum_ReportviewFrom {
    case PricingApproval
    case ReportApproval
    case InspectorSignApproval
    case InspectorSignComplete
}



class WDO_PendingApprovalReportVC: UIViewController {
  
    // MARK: -
    // MARK: ------------IBOutlet--------------
    @IBOutlet weak var wkwebview: WKWebView!
  
    @IBOutlet weak var viewBottomButton: UIView!
    @IBOutlet weak var height_viewBottomButton: NSLayoutConstraint!
    @IBOutlet weak var btnApprove: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var lblTitle: UILabel!

    // MARK: -
    // MARK: ------------Variable--------------
    var strTitle = "" , strPDFLink = "" , strSignPath = ""
    var dictPendingApproval = NSMutableDictionary()
    var objPendingApproval = NSManagedObject()
    var reportview_From = Enum_ReportviewFrom.PricingApproval
    var loader = UIAlertController()
    var dispatchGroupSignUpload = DispatchGroup()
    var isSignTaken = false
    var dictDataResponseOnSIgnUpload = NSMutableDictionary()
    
    // MARK: -
    // MARK: ------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        UIInitialization()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if isSignTaken {
           
            self.dispatchGroupSignUpload.enter()
            
        }
        
        if strPDFLink.count != 0 {
            
            LoadWebViewWithUrl(strURl: strPDFLink, webview: wkwebview)
            
        }
        
    }
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        if(nsud.value(forKey: "LoginDetails") is NSDictionary){
            
            height_viewBottomButton.constant = 0.0

            wkwebview.isHidden = true
            wkwebview.navigationDelegate = self
            wkwebview.uiDelegate = self
            btnApprove.layer.cornerRadius = 6.0
            btnDecline.layer.cornerRadius = 6.0
            switch reportview_From {
            case Enum_ReportviewFrom.PricingApproval:
                lblTitle.text = "Report"
                LoadWebViewWithUrl(strURl: "\(dictPendingApproval.value(forKey: "ReportUrl") ?? "")", webview: wkwebview)
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
            case Enum_ReportviewFrom.ReportApproval:
                btnApprove.setTitle("Approve", for: .normal)
                btnApprove.backgroundColor = hexStringToUIColor(hex: appThemeColor)
                
                btnDecline.setTitle("Decline", for: .normal)
                btnDecline.backgroundColor = hexStringToUIColor(hex: "BA0100")
                
                lblTitle.text = "Wo #\(dictPendingApproval.value(forKey: "WorkOrderNo") ?? "")"
                LoadWebViewWithUrl(strURl: "\(dictPendingApproval.value(forKey: "ReportUrl") ?? "")", webview: wkwebview)
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
           
            case Enum_ReportviewFrom.InspectorSignApproval:
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                btnApprove.setTitle("Sign", for: .normal)
                btnApprove.backgroundColor = hexStringToUIColor(hex: appThemeColor)
                
                btnDecline.setTitle("Edit", for: .normal)
                btnDecline.backgroundColor = hexStringToUIColor(hex: "D0B527")
                lblTitle.text = "Wo #\(objPendingApproval.value(forKey: "workOrderNo") ?? "")"
                
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "api/GenerateInvoice/GenerateWdoInspectionReport?WorkorderId=" + "\(objPendingApproval.value(forKey: "workorderId") ?? "")"
                
                LoadWebViewWithUrl(strURl: strURL, webview: wkwebview)
                
                height_viewBottomButton.constant = 65.0
            case Enum_ReportviewFrom.InspectorSignComplete:
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                btnApprove.setTitle("Send to Customer", for: .normal)
                btnApprove.backgroundColor = hexStringToUIColor(hex: appThemeColor)
                
                btnDecline.setTitle("View", for: .normal)
                btnDecline.backgroundColor = hexStringToUIColor(hex: "D0B527")
                lblTitle.text = "Wo #\(objPendingApproval.value(forKey: "workOrderNo") ?? "")"
                
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                
                let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "api/GenerateInvoice/GenerateWdoInspectionReport?WorkorderId=" + "\(objPendingApproval.value(forKey: "workorderId") ?? "")"
                
                LoadWebViewWithUrl(strURl: strURL, webview: wkwebview)
                
                height_viewBottomButton.constant = 65.0
                
               //TO show send to customer buttion according to company Configration''
                
              
            
                let IsStateReportEmailAppEnable = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsStateReportEmailAppEnable") ?? "")"
                if IsStateReportEmailAppEnable.lowercased() == "true" || IsStateReportEmailAppEnable == "1" {
                    btnApprove.isHidden = false
                }else{
                   btnApprove.isHidden = true
                }
                
            }
          
        }
       
    }
    
    func LoadWebViewWithUrl(strURl : String , webview : WKWebView) {
        webview.navigationDelegate = self
        webview.uiDelegate = self
        print(strURl)
        let strPdf = strURl.replacingOccurrences(of: "\\", with: "//")
        print(strPdf)

        let request = URLRequest(url: URL(string: strPdf)!)
        webview.load(request)
      
    }
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionApprove(_ sender: UIButton) {
        switch reportview_From {
        case Enum_ReportviewFrom.PricingApproval:
          
            print("Pricing se aaya Approval")
        case Enum_ReportviewFrom.ReportApproval:
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to approve.", preferredStyle: .alert)
            alert.view.tintColor = UIColor.darkGray
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
               
            }))
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [self] action in
                self.Call_ReportApprovalAPI()
            }))
           
            self.present(alert, animated: true, completion: nil)
            
           
        case .InspectorSignApproval:
            
            if "\(objPendingApproval.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "1" || "\(objPendingApproval.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "true" {
               
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "You can't move forward, you have pending pricing approval.", viewcontrol: self)
                
            }else{
                
                if (isInternetAvailable()){

                    let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                    let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
                    // vc!.strMessage = strEmpName
                    vc!.delegate = self
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: true, completion: nil)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

                }
            }
        case .InspectorSignComplete:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_SendToCustomerVC") as? WDO_SendToCustomerVC
            vc!.objWO = objPendingApproval
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    @IBAction func actionDecline(_ sender: UIButton) {
        switch reportview_From {
        case Enum_ReportviewFrom.PricingApproval:
          
            print("InspectorSignComplete se aaya Approval")
        case Enum_ReportviewFrom.ReportApproval:
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApproval_DeclineReasonVC") as? WDO_PendingApproval_DeclineReasonVC
            vc!.dictPendingApproval = removeNullFromDict(dict: (dictPendingApproval))
            vc!.declineReason_From = Enum_DeclineReasonFrom.ReportDetail
            self.navigationController?.pushViewController(vc!, animated: false)
        case .InspectorSignApproval:
            
            // Setting in NsuerDefaults
            
            let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
            vc!.strWoId = "\(objPendingApproval.value(forKey: "workorderId")!)" as NSString
            vc!.strFromWhere = "clickedOnEditFromPendingApproval"
            self.navigationController?.pushViewController(vc!, animated: false)
            //go to edit View
            
            break
            
        case .InspectorSignComplete:
            print("InspectorSignComplete se aaya Approval")
         
            let vc = UIStoryboard.init(name: "PestiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "GeneralInfoiPadVC") as? GeneralInfoiPadVC
            vc!.strWoId = "\(objPendingApproval.value(forKey: "workorderId")!)" as NSString
            self.navigationController?.pushViewController(vc!, animated: false)
            //go to Only View
        }
    }
}

// MARK: -
// MARK: ------------WKNavigationDelegate-------------

extension WDO_PendingApprovalReportVC : WKNavigationDelegate , WKUIDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        debugPrint("didCommit")
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        debugPrint("didCommit")
    }

    
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("didFinish")
        if strPDFLink.count == 0 {
            webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                       completionHandler: { [self] (html: Any?, error: Error?) in
                                        print((html as! String).html2String)
                                        strPDFLink = (String((html as! String).html2String.dropLast().dropFirst()))
                                        LoadWebViewWithUrl(strURl:String(strPDFLink), webview: self.wkwebview)
                                        
                                       })
        }else{
            switch reportview_From {
            case Enum_ReportviewFrom.PricingApproval:
                height_viewBottomButton.constant = 0.0
            case Enum_ReportviewFrom.ReportApproval:
                height_viewBottomButton.constant = 65.0
            case .InspectorSignApproval:
                height_viewBottomButton.constant = 65.0

            case .InspectorSignComplete:
                height_viewBottomButton.constant = 65.0
            }
            wkwebview.isHidden = false

            if isSignTaken {
               
                let count = self.dispatchGroupSignUpload.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
                if let first = count.first, let int = Int(first), int > 0  {
                    self.dispatchGroupSignUpload.leave()
                }

            }
            else{
             
                loader.dismiss(animated: false, completion: nil)

            }
        }

       
    }

     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("didFail")
        if isSignTaken {
           
            self.dispatchGroupSignUpload.leave()
            
        }else{
         
            loader.dismiss(animated: false, completion: nil)

        }
        
     }
}



// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_PendingApprovalReportVC {
    
    func UplaodSignImageOnServer(imageSign : UIImage) {

        self.dispatchGroupSignUpload = DispatchGroup()

        self.dispatchGroupSignUpload.enter()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
               //call any function
            self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            
        }

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let jpegData = imageSign.jpegData(compressionQuality: 1.0)

        let strUrll = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/File/UploadWdoInspectionReportSignatureAsync"
        WebService.uploadImageServer(JsonData: "",JsonDataKey: "", data: jpegData!, strDataName : self.strSignPath, url: strUrll) { [self] (responce, status) in
            
            self.dispatchGroupSignUpload.leave()
            
            if(status)
            {
                debugPrint(responce)
                if(responce.value(forKey: "data")is NSArray)
                {
                    let aryResponce = responce.value(forKey: "data") as! NSArray
                    if(aryResponce.object(at: 0) is NSDictionary){
                        
                        let dict = (aryResponce.object(at: 0) as! NSDictionary)
                        if(dict.value(forKey: "Name") != nil){
                            
                            let relativePathWithImageName = "\(dict.value(forKey: "RelativePath") ?? "")" + "\(dict.value(forKey: "Name") ?? "")"
                            
                            Call_ReportApproval_SignAPI(strRelativePath: relativePathWithImageName)
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                           // Call_ReportApproval_SignAPI()
                        }
                    }
                }}
            }
    }
    
    func Call_ReportApproval_SignAPI(strRelativePath : String) {
        
        if (isInternetAvailable()){
         
            self.dispatchGroupSignUpload.enter()

            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/WdoWorkflowApproveDeclineSignedModified"
          
            let dict = NSMutableDictionary()
            dict.setValue("\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)", forKey: "CompanyId")
            dict.setValue("\(objPendingApproval.value(forKey: "workorderId") ?? "")", forKey: "WorkOrderId")
            dict.setValue("\(objPendingApproval.value(forKey: "wdoWorkflowStageId") ?? "")", forKey: "WdoWorkflowStageId")
            //dict.setValue("", forKey: "WdoWorkflowStageId")
            dict.setValue("Signed", forKey: "ActionPerformed")
            dict.setValue("", forKey: "Comments")
            dict.setValue("\(strRelativePath)", forKey: "InspectionReportSignaturePath") // before strRelativePath it was strSignPath
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "CreatedBy")
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "ModifiedBy")

            WebService.postDataWithHeadersToServerDevice(dictJson: dict, url: strURL, strType: "dict") { (response, status) in
                //self.loader.dismiss(animated: false) { [self] in
                
                self.dispatchGroupSignUpload.leave()

                    print(response)
                    if(status){
                        
                        if(response.value(forKey: "data") is NSDictionary){
                            
                            self.dictDataResponseOnSIgnUpload = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            
                        }
                        
                }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                //}
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
        
        // Notify Block Yaha Likha Hai
        
        self.dispatchGroupSignUpload.notify(queue: DispatchQueue.main, execute: {
            
            self.loader.dismiss(animated: false) {
                
                // Successfully Uploaded Give Message and Redirect To Back View
                self.isSignTaken = false

                if self.dictDataResponseOnSIgnUpload.count > 0 {
                    
                    let ErrorMessage = "\(self.dictDataResponseOnSIgnUpload.value(forKey: "ErrorMessage") ?? "")"

                    if(self.dictDataResponseOnSIgnUpload.value(forKey: "IsSuccess") as! Bool){

                        let alertCOntroller = UIAlertController(title: Info, message: "Signed successfully.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            //NotificationCenter.default.post(name: Notification.Name("RefreshAppointmentList"), object: nil, userInfo: nil)
                            nsud.setValue(true, forKey: "RefreshAppointmentList")

                            self.navigationController?.popViewController(animated: false)
                            
                        })
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: self)

                    }
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        })
    }
    
    func Call_ReportApprovalAPI() {
        if (isInternetAvailable()){
            self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
         
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/WdoWorkflowApproveDeclineSignedModified"
          
            let dict = NSMutableDictionary()
            
            dict.setValue("\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")", forKey: "WorkOrderId")
            dict.setValue("\(dictPendingApproval.value(forKey: "WdoWorkflowStageId") ?? "")", forKey: "WdoWorkflowStageId")
            dict.setValue("Approved", forKey: "ActionPerformed")
            dict.setValue("", forKey: "Comments")
            dict.setValue("", forKey: "InspectionReportSignaturePath")
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "CreatedBy")
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "ModifiedBy")
            dict.setValue("\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)", forKey: "CompanyId")
            
             
            WebService.postDataWithHeadersToServerDevice(dictJson: dict, url: strURL, strType: "dict") { (response, status) in
                self.loader.dismiss(animated: false) { [self] in
                    print(response)
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"
                            
                            if(dictData.value(forKey: "IsSuccess") as! Bool){
                                self.navigationController?.popViewController(animated: false)
                                
                                NotificationCenter.default.post(name: Notification.Name("RefreshReportApprovalList"), object: nil, userInfo: nil)
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: self)
                            }
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}



// MARK: ------------- Signature Delegate --------------

extension WDO_PendingApprovalReportVC : SignatureViewDelegate{
    func imageFromSignatureView(image: UIImage)
    {
        
        if (isInternetAvailable()){
            
            isSignTaken = true
            self.strSignPath = "\\Documents\\UploadImages\\WDOPendingApprovalSign_" + "\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")" + "_" + Global().getReferenceNumber() + ".jpg"
            self.UplaodSignImageOnServer(imageSign: image)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
}


extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
