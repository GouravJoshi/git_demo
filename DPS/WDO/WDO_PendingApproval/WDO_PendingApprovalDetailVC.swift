//
//  WDO_PendingApprovalDetailVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class WDO_PendingApprovalDetailVC: UIViewController {
    // MARK: -
    // MARK: ------------IBOutlet--------------
   
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var btn_Decline: UIButton!
    @IBOutlet weak var btn_Approve: UIButton!

    // MARK: -
    // MARK: ------------Variable--------------
    var aryChargeList = NSMutableArray()
    var dictPendingApproval = NSMutableDictionary()
    var loader = UIAlertController()

    // MARK: -
    // MARK: ------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        UIInitialization()
        
        // Check if To SYnc This Work Order To Server

        checkIfWorkOrderIsPendingToSyncToServer()
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "startLoaderWdoPendingFromSideMenuServiceAuto"), object: nil)

    }
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        btn_Decline.layer.cornerRadius = 4.0
        btn_Approve.layer.cornerRadius = 4.0
        btn_Decline.backgroundColor = hexStringToUIColor(hex: "BA0100")

        
        var dict = NSMutableDictionary()
        dict.setValue("Subtotal", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "SubTotal") ?? "")", forKey: "value")
        aryChargeList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Other Discount", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "Discount") ?? "")", forKey: "value")
        aryChargeList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Tip Discount", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "TIPDiscount") ?? "")", forKey: "value")
        aryChargeList.add(dict)
        
        
        dict = NSMutableDictionary()
        dict.setValue("Lead Inspection Fee", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "LeadInspectionFee") ?? "")", forKey: "value")
        if Double("\(dictPendingApproval.value(forKey: "LeadInspectionFee") ?? "")") != 0
        {
            aryChargeList.add(dict)
        }
        
        dict = NSMutableDictionary()
        dict.setValue("Building Permit", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "BuildingPermit") ?? "")", forKey: "value")
        aryChargeList.add(dict)
      
        dict = NSMutableDictionary()
        dict.setValue("Billing Amount", forKey: "title")
        dict.setValue("\(dictPendingApproval.value(forKey: "Total") ?? "")", forKey: "value")
        aryChargeList.add(dict)
        
        // If declined then not show decline button.
        
        if "\(dictPendingApproval.value(forKey: "PricingApprovalStatus") ?? "")" == "Decline" || "\(dictPendingApproval.value(forKey: "PricingApprovalStatus") ?? "")" == "Declined"
        {
            btn_Decline.isEnabled = false
            btn_Decline.setTitle("Declined", for: .normal)

        }else{
            btn_Decline.isEnabled = true
            btn_Decline.setTitle("Decline", for: .normal)

        }
    }
    
    
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionApprove(_ sender: UIButton) {
        let alert = UIAlertController(title: alertMessage, message: "Are you sure want to approve.", preferredStyle: .alert)
        alert.view.tintColor = UIColor.darkGray
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
           
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [self] action in
            Call_ApprovalAPI()
        }))
       
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    @IBAction func actionDecline(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApproval_DeclineReasonVC") as? WDO_PendingApproval_DeclineReasonVC
        vc!.dictPendingApproval = removeNullFromDict(dict: (dictPendingApproval))
        vc!.declineReason_From = Enum_DeclineReasonFrom.PricingDetail
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}


// MARK: -
// MARK: ------------UITableViewDelegate--------------

extension WDO_PendingApprovalDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return aryList.count
        switch section {
        case 2:
            return aryChargeList.count
        default:
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
          
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_PendingApprovalAccountDetailCell" : "WDO_PendingApprovalAccountDetailCell", for: indexPath as IndexPath) as! WDO_PendingApprovalDetailCell
            cell.btnReportView.addTarget(self, action: #selector(didTapReportView), for: .touchUpInside)
            //cell.lblAccountNumber.text = "\(dictPendingApproval.value(forKey: "AccountNo") ?? "")"
            cell.lblAccountNumber.text = "\(dictPendingApproval.value(forKey: "AccountNo") ?? "")" + ", Wo # " + "\(dictPendingApproval.value(forKey: "WorkOrderNo") ?? ""), \(dictPendingApproval.value(forKey: "ServicePOCName") ?? "")"
            cell.lblAddress.text = "\(dictPendingApproval.value(forKey: "ServiceAddress") ?? "")"
            cell.lblInspectedBy.text = "Inspected by: \(dictPendingApproval.value(forKey: "InspectedBy") ?? "")"

            cell.btnReportView.addTarget(self, action: #selector(didTapReportView), for: .touchUpInside)

            
            return cell
            
        case 1:
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_PendingApprovalRateDetailCell" : "WDO_PendingApprovalRateDetailCell", for: indexPath as IndexPath) as! WDO_PendingApprovalDetailCell
            
            let MaxHourlyRate = Double("\(dictPendingApproval.value(forKey: "MaxHourlyRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxHourlyRate") ?? "0")")
            let MinHourlyRate = Double("\(dictPendingApproval.value(forKey: "MinHourlyRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinHourlyRate") ?? "0")")
            let HourlyRate =  Double("\(dictPendingApproval.value(forKey: "HourlyRate") ?? "0")")
           
            let MaxMaterialMultiplier = Double("\(dictPendingApproval.value(forKey: "MaxMaterialMultiplier") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxMaterialMultiplier") ?? "0")")
            let MinMaterialMultiplier = Double("\(dictPendingApproval.value(forKey: "MinMaterialMultiplier") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinMaterialMultiplier") ?? "0")")
            let MaterialMultiplier = Double("\(dictPendingApproval.value(forKey: "MaterialMultiplier") ?? "0")")
           
            let MaxSubFeeMultiplier = Double("\(dictPendingApproval.value(forKey: "MaxSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxSubFeeMultiplier") ?? "0")")
            let MinSubFeeMultiplier = Double("\(dictPendingApproval.value(forKey: "MinSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinSubFeeMultiplier") ?? "0")")
            let SubFeeMultiplier = Double("\(dictPendingApproval.value(forKey: "SubFeeMultiplier") ?? "0")")
           
            let MaxSoilTreatmentRate = Double("\(dictPendingApproval.value(forKey: "MaxSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxSoilTreatmentRate") ?? "0")")
            let MinSoilTreatmentRate = Double("\(dictPendingApproval.value(forKey: "MinSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinSoilTreatmentRate") ?? "0")")
            let SoilTreatmentRate = Double("\(dictPendingApproval.value(forKey: "SoilTreatmentRate") ?? "0")")
          
            let MaxDrilledConcreteRate = Double("\(dictPendingApproval.value(forKey: "MaxDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxDrilledConcreteRate") ?? "0")")
            let MinDrilledConcreteRate = Double("\(dictPendingApproval.value(forKey: "MinDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinDrilledConcreteRate") ?? "0")")
            let DrilledConcreteRate = Double("\(dictPendingApproval.value(forKey: "DrilledConcreteRate") ?? "0")")
           
            let MaxCubicRate = Double("\(dictPendingApproval.value(forKey: "MaxCubicRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MaxCubicRate") ?? "0")")
            let MinCubicRate = Double("\(dictPendingApproval.value(forKey: "MinCubicRate") ?? "0")" == "" ? "0" : "\(dictPendingApproval.value(forKey: "MinCubicRate") ?? "0")")
            let CubicRate = Double("\(dictPendingApproval.value(forKey: "CubicRate") ?? "0")")
            

            if (MinHourlyRate != 0 && MaxHourlyRate != 0) && !(MinHourlyRate!...MaxHourlyRate! ~= HourlyRate!)
            {
                cell.lblHourlyRate.textColor = hexStringToUIColor(hex: "BA0100")
            }else{
                cell.lblHourlyRate.textColor = hexStringToUIColor(hex: appThemeColor)
            }
   
            cell.lblHourlyRate.text = "Hourly rate ($): \(String(format: "%.2f", HourlyRate!))"
            cell.lblHourly_AcceptRange.text = "Acceptable Range ($): \(String(format: "%.2f", MinHourlyRate!)) - \(String(format: "%.2f", MaxHourlyRate!))"

            
            
            if (MinMaterialMultiplier != 0 && MaxMaterialMultiplier != 0) && !(MinMaterialMultiplier!...MaxMaterialMultiplier! ~= MaterialMultiplier!)
            {
                (cell.lblMaterialRate.textColor = hexStringToUIColor(hex: "BA0100"))
            }else{
                (cell.lblMaterialRate.textColor = hexStringToUIColor(hex: appThemeColor))
            }
            cell.lblMaterialRate.text = "Material Multiplier: \(String(format: "%.2f", MaterialMultiplier!))"
            cell.lblMaterial_AcceptRange.text = "Acceptable Range: \(String(format: "%.2f", MinMaterialMultiplier!)) - \(String(format: "%.2f", MaxMaterialMultiplier!))"
            
         
            if (MinSubFeeMultiplier != 0 && MaxSubFeeMultiplier != 0) && !(MinSubFeeMultiplier!...MaxSubFeeMultiplier! ~= SubFeeMultiplier!)
            {
                (cell.lblSubFeeMultiplierRate.textColor = hexStringToUIColor(hex: "BA0100"))
            }else{
                (cell.lblSubFeeMultiplierRate.textColor = hexStringToUIColor(hex: appThemeColor))
            }
            cell.lblSubFeeMultiplierRate.text = "Sub fee Multiplier: \(String(format: "%.2f", SubFeeMultiplier!))"
            cell.lblSubFeeMultiplier_AcceptRange.text = "Acceptable Range: \(String(format: "%.2f", MinSubFeeMultiplier!)) - \(String(format: "%.2f", MaxSubFeeMultiplier!))"
            
            
            if (MinSoilTreatmentRate != 0 && MaxSoilTreatmentRate != 0) && !(MinSoilTreatmentRate!...MaxSoilTreatmentRate! ~= SoilTreatmentRate!)
            {
                (cell.lblSoilTreatMentRate.textColor = hexStringToUIColor(hex: "BA0100"))
            }else{
                (cell.lblSoilTreatMentRate.textColor = hexStringToUIColor(hex: appThemeColor))
            }
            
            cell.lblSoilTreatMentRate.text = "Linear Ft. Soil Treatment Rate ($): \(String(format: "%.2f", SoilTreatmentRate!))"
            cell.lblSoilTreatMent_AcceptRange.text = "Acceptable Range ($): \(String(format: "%.2f", MinSoilTreatmentRate!)) - \(String(format: "%.2f", MaxSoilTreatmentRate!))"
           
            if (MinDrilledConcreteRate != 0 && MaxDrilledConcreteRate != 0) && !(MinDrilledConcreteRate!...MaxDrilledConcreteRate! ~= DrilledConcreteRate!)
            {
                (cell.lblDrillConcreatRate.textColor = hexStringToUIColor(hex: "BA0100"))
            }else{
                (cell.lblDrillConcreatRate.textColor = hexStringToUIColor(hex: appThemeColor))
            }
            
            cell.lblDrillConcreatRate.text = "Linear Ft. Drill Concreat Rate ($): \(String(format: "%.2f", DrilledConcreteRate!))"
            cell.lblDrillConcreat_AcceptRange.text = "Acceptable Range ($): \(String(format: "%.2f", MinDrilledConcreteRate!)) - \(String(format: "%.2f", MaxDrilledConcreteRate!))"
            
            
            if (MinCubicRate != 0 && MaxCubicRate != 0) && !(MinCubicRate!...MaxCubicRate! ~= CubicRate!)
            {
                (cell.lblCubitRate.textColor = hexStringToUIColor(hex: "BA0100"))
            }else{
                (cell.lblCubitRate.textColor = hexStringToUIColor(hex: appThemeColor))
            }
            
            cell.lblCubitRate.text = "Cubit Ft. Rate ($): \(String(format: "%.2f", CubicRate!))"
            cell.lblCubitRate_AcceptRange.text = "Acceptable Range ($): \(String(format: "%.2f", MinCubicRate!)) - \(String(format: "%.2f", MaxCubicRate!))"
            
            cell.lblChangeReason.text = "\(dictPendingApproval.value(forKey: "ChangeReason") ?? "")"
            cell.lblInspectorNotes.text = "\(dictPendingApproval.value(forKey: "PriceChangeNote") ?? "")"

   
            return cell
 
        default:
           
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_PendingApprovalChargeDetailCell" : "WDO_PendingApprovalChargeDetailCell", for: indexPath as IndexPath) as! WDO_PendingApprovalDetailCell
         
            let dict = aryChargeList.object(at: indexPath.row) as! NSDictionary

            cell.lblChargeTitle.text = "\(dict.value(forKey: "title") ?? "")"
            cell.lblChargeValue.text = convertDoubleToCurrency(amount: Double("\(dict.value(forKey: "value") ?? "0")")!)

            
            if aryChargeList.count-1 == indexPath.row {
                cell.lblChargeTitle.font = UIFont.boldSystemFont(ofSize: 18)
                cell.lblChargeValue.font = UIFont.boldSystemFont(ofSize: 18)
                cell.lblChargeTitle.textColor = UIColor.black
                cell.lblChargeValue.textColor = UIColor.black
            }else{
                cell.lblChargeTitle.font = UIFont.systemFont(ofSize: 18)
                cell.lblChargeValue.font = UIFont.systemFont(ofSize: 18)
                cell.lblChargeTitle.textColor = UIColor.darkGray
                cell.lblChargeValue.textColor = UIColor.darkGray
            }
    //        let dict = aryList.object(at: indexPath.row) as! NSDictionary
    //        cell.lblAccountNumber.text = "\(dict.value(forKey: "") ?? "")"
    //        cell.lblAddress.text = "\(dict.value(forKey: "") ?? "")"
    //        cell.lblInspectedBy.text = "Inspected by: \(dict.value(forKey: "") ?? "")"
            return cell
            
        }
        

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let lbl = UILabel.init(frame: CGRect(x: 25, y: 0, width: tableView.frame.width - 50, height: 48))
        let lblGrayLine = UILabel.init(frame: CGRect(x: 25, y: 49, width: tableView.frame.width - 50, height: 1))
        lblGrayLine.backgroundColor = UIColor.lightGray
        
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        
        switch section {
        case 1:
            lbl.text = "Rates"
        case 2:
            lbl.text = "Charges"
        default:
            lbl.text = ""

        }
        view.clipsToBounds = true
        view.addSubview(lbl)
        view.addSubview(lblGrayLine)

        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 50
        case 2:
            return 50
        default:
            return 0

        }
        
    }
  
    @objc func didTapReportView() {
        
        if (isInternetAvailable()){
            
            if("\(dictPendingApproval.value(forKey: "ReportUrl") ?? "")" != ""){
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDO_PendingApproval" : "WDO_PendingApproval", bundle: Bundle.main).instantiateViewController(withIdentifier: "WDO_PendingApprovalReportVC") as? WDO_PendingApprovalReportVC
                vc!.reportview_From =  Enum_ReportviewFrom.PricingApproval
                vc!.dictPendingApproval = dictPendingApproval
                self.navigationController?.pushViewController(vc!, animated: false)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Report Link is not available!", viewcontrol: self)
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
}

// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_PendingApprovalDetailVC {
    
    func checkIfWorkOrderIsPendingToSyncToServer() {
        
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", "\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")"))
        
        if arryOfWorkOrderData.count > 0
        {
            
            let objWorkorderDetail = arryOfWorkOrderData[0] as! NSManagedObject
            
            // ForResync Data zSync
            if "\(objWorkorderDetail.value(forKey: "zSync") ?? "")" == "yes"
            {

                let alert = UIAlertController(title: alertMessage, message: "This work order needs to be synced before proceeding.", preferredStyle: .alert)
                alert.view.tintColor = UIColor.darkGray
                
                alert.addAction(UIAlertAction(title: "Sync", style: .default, handler: { [self] action in
                                        
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(self.loader, animated: false, completion: nil)
                    
                    self.perform(#selector(callServiceAutoSyncAPI), with: nil, afterDelay: 0.1)
                    
                }))
               
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    @objc func callServiceAutoSyncAPI() {
        
        if (isInternetAvailable()){
            
            // Sync To Server
            
            NotificationCenter.default.addObserver(self, selector: #selector(stopLoaderServiceDataIsSyncedSideMenu(_:)), name: NSNotification.Name("startLoaderWdoPendingFromSideMenuServiceAuto"), object: nil)

            let objServiceSendMailViewControlleriPad = ServiceSendMailViewControlleriPad()
            objServiceSendMailViewControlleriPad.callApiToSyncDataForWdoPendingApprovalAgreement(toServer: "WDOPendingApprovalFromSideMenu", "\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")")
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

            self.navigationController?.popViewController(animated: false)
            
        }
        
    }
    
    @objc private func stopLoaderServiceDataIsSyncedSideMenu(_ notification: Notification) {
        
        self.loader.dismiss(animated: false) {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_DataSynced, viewcontrol: self)
            
        }
        
    }
    
    func Call_ApprovalAPI() {
        if (isInternetAvailable()){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
         
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/ManageWoPricingApproval"
          
            let dict = NSMutableDictionary()
            dict.setValue("0", forKey: "PricingApprovalHistoryId")
            dict.setValue("\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")", forKey: "WorkOrderId")
            dict.setValue("\(Global().getEmployeeId()!)", forKey: "RequestedBy")
            dict.setValue("Approved", forKey: "RequestType")
            dict.setValue("", forKey: "RequestedTo")
            dict.setValue("", forKey: "Note")
            dict.setValue("false", forKey: "IsVerbalApproval")
            dict.setValue("true", forKey: "IsActive")
            dict.setValue("", forKey: "PriceChangeReasonId")
            dict.setValue("\(Global().getCompanyKey()!)", forKey: "CompanyKey")
            WebService.callAPIBYPOST(parameter: dict, url: strURL) { (response, status) in
                self.loader.dismiss(animated: false) { [self] in
                    print(response)
                    if(status){
                            if(response.value(forKey: "data") is NSDictionary){
                                let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"

                                if(dictData.value(forKey: "IsSuccess") as! Bool){
                                    self.navigationController?.popViewController(animated: false)

                                    NotificationCenter.default.post(name: Notification.Name("RefreshPricingApprovalList"), object: nil, userInfo: nil)
                                }else{
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: self)
                                }
                            }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}

// MARK: -
// MARK: ----------WDO_PendingApprovalDetailCell---------
class WDO_PendingApprovalDetailCell: UITableViewCell {
    //---For Ac Detail
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblInspectedBy: UILabel!
    @IBOutlet weak var btnReportView: UIButton!

    //---For Rate Detail
    @IBOutlet weak var lblHourlyRate: UILabel!
    @IBOutlet weak var lblHourly_AcceptRange: UILabel!
    
    @IBOutlet weak var lblMaterialRate: UILabel!
    @IBOutlet weak var lblMaterial_AcceptRange: UILabel!
    
    @IBOutlet weak var lblSubFeeMultiplierRate: UILabel!
    @IBOutlet weak var lblSubFeeMultiplier_AcceptRange: UILabel!
    
    @IBOutlet weak var lblSoilTreatMentRate: UILabel!
    @IBOutlet weak var lblSoilTreatMent_AcceptRange: UILabel!
    
    @IBOutlet weak var lblDrillConcreatRate: UILabel!
    @IBOutlet weak var lblDrillConcreat_AcceptRange: UILabel!
    
    @IBOutlet weak var lblCubitRate: UILabel!
    @IBOutlet weak var lblCubitRate_AcceptRange: UILabel!
   
    @IBOutlet weak var lblChangeReason: UILabel!
    @IBOutlet weak var lblInspectorNotes: UILabel!

    //---For Charges
    @IBOutlet weak var lblChargeTitle: UILabel!
    @IBOutlet weak var lblChargeValue: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
