//
//  WDO_PendingApproval_DeclineReasonVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

enum Enum_DeclineReasonFrom {
    case PricingDetail
    case ReportDetail
        
}

class WDO_PendingApproval_DeclineReasonVC: UIViewController {
    // MARK: -
    // MARK: ------------IBOutlet--------------
   
    @IBOutlet weak var txtReason: UITextView!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var btn_Close: UIButton!

    // MARK: -
    // MARK: ------------Variable--------------
    var loader = UIAlertController()
    var dictPendingApproval = NSMutableDictionary()
    var declineReason_From = Enum_DeclineReasonFrom.PricingDetail

    // MARK: -
    // MARK: ------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        UIInitialization()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        btn_Submit.layer.cornerRadius = 4.0
        btn_Close.layer.cornerRadius = 4.0
        txtReason.layer.borderWidth = 1.0
        txtReason.layer.borderColor = UIColor.lightGray.cgColor
        txtReason.delegate = self
    }
    
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        
        switch declineReason_From {
        case Enum_DeclineReasonFrom.PricingDetail:
            Call_Pricing_DeclineReasonAPI()
        case Enum_DeclineReasonFrom.ReportDetail:
            Call_Report_DeclineReasonAPI()
        }
        
        /*if(txtReason.text.trimmed == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Decline Reason is required!", viewcontrol: self)
        }else{
            switch declineReason_From {
            case Enum_DeclineReasonFrom.PricingDetail:
                Call_Pricing_DeclineReasonAPI()
            case Enum_DeclineReasonFrom.ReportDetail:
                Call_Report_DeclineReasonAPI()
            }
        }*/
        
    }
    @IBAction func actionClose(_ sender: UIButton) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: false)
    }
}
// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_PendingApproval_DeclineReasonVC {
    func Call_Pricing_DeclineReasonAPI() {
        if (isInternetAvailable()){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
         
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
          
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/ManageWoPricingApproval"
          
            let dict = NSMutableDictionary()
            dict.setValue("0", forKey: "PricingApprovalHistoryId")
            dict.setValue("\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")", forKey: "WorkOrderId")
            dict.setValue("\(Global().getEmployeeId()!)", forKey: "RequestedBy")
            dict.setValue("Declined", forKey: "RequestType")
            dict.setValue("", forKey: "RequestedTo")
            dict.setValue("\(txtReason.text!)", forKey: "Note")
            dict.setValue("false", forKey: "IsVerbalApproval")
            dict.setValue("true", forKey: "IsActive")
            dict.setValue("", forKey: "PriceChangeReasonId")
            dict.setValue("\(Global().getCompanyKey()!)", forKey: "CompanyKey")
          
            WebService.callAPIBYPOST(parameter: dict, url: strURL) { (response, status) in
                self.loader.dismiss(animated: false) { [self] in
                    print(response)
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"

                            if(dictData.value(forKey: "IsSuccess") as! Bool){
                                for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: WDO_PendingApprovalListVC.self) {
                                          
                                            NotificationCenter.default.post(name: Notification.Name("RefreshPricingApprovalList"), object: nil, userInfo: nil)
                                            _ =  self.navigationController!.popToViewController(controller, animated: true)

                                            break
                                        }
                                    }

                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: self)

                            }
                                
                         
                        }
                    
                    
                 
                }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
    
    func Call_Report_DeclineReasonAPI() {
        if (isInternetAvailable()){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(self.loader, animated: false, completion: nil)
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/WdoWorkflowApproveDeclineSignedModified"
          
            let dict = NSMutableDictionary()
            dict.setValue("\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId")!)", forKey: "CompanyId")
            dict.setValue("\(dictPendingApproval.value(forKey: "WorkOrderId") ?? "")", forKey: "WorkOrderId")
            dict.setValue("\(dictPendingApproval.value(forKey: "WdoWorkflowStageId") ?? "")", forKey: "WdoWorkflowStageId")
            dict.setValue("Declined", forKey: "ActionPerformed")
            dict.setValue("\(txtReason.text!)", forKey: "Comments")
            dict.setValue("", forKey: "InspectionReportSignaturePath")
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "CreatedBy")
            dict.setValue("\(Global().getEmployeeId() ?? "")", forKey: "ModifiedBy")

            
            
           WebService.postDataWithHeadersToServerDevice(dictJson: dict, url: strURL, strType: "dict") { (response, status) in
                self.loader.dismiss(animated: false) { [self] in
                    print(response)
                    if(status){
                        if(response.value(forKey: "data") is NSDictionary){
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"

                            if(dictData.value(forKey: "IsSuccess") as! Bool){
                                for controller in self.navigationController!.viewControllers as Array {
                                        if controller.isKind(of: WDO_PendingApprovalListVC.self) {
                                          
                                            NotificationCenter.default.post(name: Notification.Name("RefreshReportApprovalList"), object: nil, userInfo: nil)
                                            _ =  self.navigationController!.popToViewController(controller, animated: true)

                                            break
                                        }
                                    }

                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: self)

                            }
                                
                         
                        }
                    
                    
                 
                }else{
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
}


// MARK: -
// MARK: -----------UITextViewDelegate------------
extension WDO_PendingApproval_DeclineReasonVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return txtViewValidation(textField: textView, string: text, returnOnly: "", limitValue: 499)
    }
}
