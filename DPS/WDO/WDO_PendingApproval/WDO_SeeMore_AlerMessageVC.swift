//
//  WDO_SeeMore_AlerMessageVC.swift
//  DPS
//
//  Created by NavinPatidar on 7/26/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class WDO_SeeMore_AlerMessageVC: UIViewController {
    // MARK: -
    // MARK: ------------IBOutlet--------------
   
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var lbl_Title: UILabel!

    // MARK: -
    // MARK: ------------Variable--------------
    var ary_approvalCommentsList = NSArray() , ary_priceCommentsList = NSArray()
  

    var loader = UIAlertController()
    var strApprovalCommentsErrorMessage = "" , strPriceCommentsErrorMessage = ""
    var dispatchGroup = DispatchGroup()
    var workOrderId = ""
    var refresher = UIRefreshControl()

    // MARK: -
    // MARK: ------------View Life Cycle--------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tv_List.tableFooterView = UIView()
        tv_List.estimatedRowHeight = 75.0
        
        self.refresher.addTarget(self, action: #selector(refreshloadData), for: .valueChanged)
        self.tv_List!.addSubview(refresher)
        
        // Do any additional setup after loading the view.
        UIInitialization()
        
    }
    // MARK: -
    // MARK: ------------UIInitialization--------------
    
    func UIInitialization()  {
        
        tv_List.tableFooterView = UIView()
        
        if (isInternetAvailable()){
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            Call_GetMessageAlertList()
                
            self.dispatchGroup.notify(queue: DispatchQueue.main, execute: { [self] in

                loader.dismiss(animated: false, completion: nil)
                self.refresher.endRefreshing()

            })
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)

        }
    }
    
    // MARK: ------------Refresh Data-------------

    
    @objc func refreshloadData() {
        
        UIInitialization()
        
    }
    
    func reloadTableView(){
        self.tv_List.dataSource = self
        self.tv_List.delegate = self
        self.tv_List.reloadData()
    }
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
}
// MARK: -
// MARK: ------------API's Calling--------------

extension WDO_SeeMore_AlerMessageVC {
    func Call_GetMessageAlertList() {

        if (isInternetAvailable()){
            
            dispatchGroup.enter()
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                        
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWdoWorkflowSeeMoreDetailByWorkorderId?WorkorderId=" + "\(workOrderId)"
          
            
            WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "WDO_See_More") { [self] (response, status) in
                    
                print(response)
                
                ary_priceCommentsList = NSArray()
                ary_approvalCommentsList = NSArray()

                dispatchGroup.leave()
                
                    if(status){
                        
                        if(response.value(forKey: "data") is NSDictionary){
                            
                            let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary

                            if dictData.count > 0 {
                                
                                let arrKeys = dictData.allKeys as NSArray
                                
                                if arrKeys.contains("WorkOrderPricingApprovalHistoryExtMinDcs") && dictData.value(forKey: "WorkOrderPricingApprovalHistoryExtMinDcs") is NSArray {
                                    
                                    ary_priceCommentsList = dictData.value(forKey: "WorkOrderPricingApprovalHistoryExtMinDcs") as! NSArray
                                    self.reloadTableView()

                                } else{
                                    
                                    strPriceCommentsErrorMessage = alertDataNotFound
                                    self.reloadTableView()
                                    
                                }
                                
                                if arrKeys.contains("WoWdoWorkflowStageHistoryExtMinDcs") && dictData.value(forKey: "WoWdoWorkflowStageHistoryExtMinDcs") is NSArray{
                                    
                                    ary_approvalCommentsList = dictData.value(forKey: "WoWdoWorkflowStageHistoryExtMinDcs") as! NSArray
                                    self.reloadTableView()

                                }else{
                                    
                                    strApprovalCommentsErrorMessage = alertDataNotFound
                                    self.reloadTableView()

                                }
                            }else{
                                
                                strPriceCommentsErrorMessage = alertDataNotFound
                                strApprovalCommentsErrorMessage = alertDataNotFound
                                self.reloadTableView()
                            
                            }
                         
                        }else{
                            
                            strPriceCommentsErrorMessage = alertDataNotFound
                            strApprovalCommentsErrorMessage = alertDataNotFound
                            self.reloadTableView()
                        
                        }
                        
                    }else{
                        
                        strPriceCommentsErrorMessage = alertDataNotFound
                        strApprovalCommentsErrorMessage = alertDataNotFound
                        self.reloadTableView()
                    
                    }
            }
        }
        else{
            
            
            strPriceCommentsErrorMessage = alertInternet
            strApprovalCommentsErrorMessage = alertInternet
            
            self.reloadTableView()

        }
    }
}

// MARK: -
// MARK: ------------UITableViewDelegate--------------

extension WDO_SeeMore_AlerMessageVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? ary_approvalCommentsList.count : ary_priceCommentsList.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "WDO_MessageListCell" : "WDO_MessageListCell", for: indexPath as IndexPath) as! WDO_MessageListCell
        if indexPath.section == 0 {
            
            let dict = ary_approvalCommentsList.object(at: indexPath.row) as! NSDictionary
            cell.lblTitle.text = "\(dict.value(forKey: "CreatedByName") ?? "")"
            cell.lblMessage.text = "\(dict.value(forKey: "Note") ?? "")" == "<null>"  ? "" : "\(dict.value(forKey: "Note") ?? "")"
            cell.lblDate.text = "On " + Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dict.value(forKey: "CreatedDate")!)")
        }

        else{
            let dict = ary_priceCommentsList.object(at: indexPath.row) as! NSDictionary

            cell.lblTitle.text = "\(dict.value(forKey: "RequestedByName") ?? "")"
            cell.lblMessage.text = "\(dict.value(forKey: "Note") ?? "")" == "<null>"  ? "" : "\(dict.value(forKey: "Note") ?? "")"
            cell.lblDate.text = "On " + Global().changeDate(toLocalDateOtherServiceAppointmentsUpdated: "\(dict.value(forKey: "CreatedDate")!)")
            
        }
       

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return strApprovalCommentsErrorMessage == "" ? (ary_approvalCommentsList.count != 0  ? "" : alertDataNotFound) :  strApprovalCommentsErrorMessage
        default:
            return strPriceCommentsErrorMessage == "" ? (ary_priceCommentsList.count != 0  ? "" : alertDataNotFound) :  strPriceCommentsErrorMessage

        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        switch section {
        case 0:
            return ary_approvalCommentsList.count != 0  ? 0 : 100
        default:
            return ary_priceCommentsList.count != 0  ? 0 : 100

        }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = .white
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.textAlignment = .center
       header.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Approval Comments"
        default:
            return "Price Comments"
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

         //For Header Background Color
         view.tintColor = .groupTableViewBackground

        // For Header Text Color
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = hexStringToUIColor(hex: appThemeColor)
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
    }

}
// MARK: -
// MARK: ----------WDO_MessageListCell---------
class WDO_MessageListCell: UITableViewCell {
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
