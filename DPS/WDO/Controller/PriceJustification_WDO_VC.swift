//
//  PriceJustification_WDO_VC.swift
//  DPS
//
//  Created by Saavan Patidar on 29/07/21.
//  Copyright © 2021 Saavan. All rights reserved.
//  Saavan Patidar 2021 

import UIKit


class PriceJustification_WDO_VC: UIViewController {
    // MARK: -
    // MARK: ------------IBOutlet--------------
   
    @IBOutlet weak var txtReason: UITextView!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var btn_Close: UIButton!

    // MARK: -
    // MARK: ------------Variable--------------
    var loader = UIAlertController()
    var dictPendingApproval = NSMutableDictionary()
    var declineReason_From = Enum_DeclineReasonFrom.PricingDetail
    // managed object
    var objWorkorderDetail = NSManagedObject()
    
    var dispatchGroupWdoPending = DispatchGroup()

    // MARK: -
    // MARK: ------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtReason.delegate = self
        UIInitialization()
        self.dispatchGroupWdoPending = DispatchGroup()
        txtReason.text = "\(objWorkorderDetail.value(forKey: "priceChangeNote") ?? "")"
        txtReason.text = ""

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.view.endEditing(true)
    }
    // MARK: -
    // MARK: ------------UIInitialization--------------
    func UIInitialization()  {
        btn_Submit.layer.cornerRadius = 4.0
        btn_Close.layer.cornerRadius = 4.0
        txtReason.layer.borderWidth = 1.0
        txtReason.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    // MARK: -
    // MARK: ------------IBAction-------------
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionSubmit(_ sender: UIButton)
    {
        
        if isInternetAvailable() {
            
            if (txtReason.text.trimmed.count > 500 ) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "You can not enter more then 500 characters", viewcontrol: self)

                
            } else {
                
                updateJustification()
                
            }
            
        }else{
            
            Global().alertMethod(Alert, ErrorInternetMsg)
            
        }
        
        /*if(txtReason.text.trimmed == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Price/Discount justification is required!", viewcontrol: self)
        }
        else
        {
            
            if isInternetAvailable() {
                
                updateJustification()
                
            }else{
                
                Global().alertMethod(Alert, ErrorInternetMsg)
                
            }
        }*/
        
    }
    @IBAction func actionClose(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: ------------Nilind--------------
    
    func updateTempData(strLeadNoL : String)
    {
        // Update Opp Flow Type to Commercial for WDO Opp always.
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("stageSysName")
        arrOfKeys.add("statusSysName")
        
        arrOfValues.add("Scheduled")
        arrOfValues.add("Open")
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strLeadNoL), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
            // Updated SuccessFully
            
        }
        
    }
    
    func updateJustification() {
                
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("priceChangeNote")
        arrOfKeys.add("isPricingApprovalPending")
        arrOfKeys.add("isPricingApprovalMailSend")

        arrOfValues.add(txtReason.text ?? "")
        arrOfValues.add("true")
        arrOfValues.add("true") // Make this False once Data is Synced to Server.


        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifyDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")

        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@","\(objWorkorderDetail.value(forKey: "workorderId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if(isSuccess)
        {
            print("pricing multiplier updated successfully")
        }
        

        //  Comment This code it is Temporary
        let departmentType = "\(objWorkorderDetail.value(forKey: "departmentType")!)"
        let serviceState = "\(objWorkorderDetail.value(forKey: "serviceState")!)"
        
        if departmentType == "Termite" && serviceState == "California" {
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
                        
            self.updateTempData(strLeadNoL: strWoLeadNo)
            
        }
        
        
        //  Start Syncing Data To Server First Will Sync Sales Data and Then Service Auto Data For Approval
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        
        self.perform(#selector(callSaleServiceApi), with: nil, afterDelay: 0.1)

    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "startLoaderWdoPendingSalesAuto"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "startLoaderWdoPendingServiceAuto"), object: nil)

    }
    
    @objc private func callSaleServiceApi() {
        
        self.dispatchGroupWdoPending.enter()

        NotificationCenter.default.addObserver(self, selector: #selector(stopLoaderSalesDataIsSynced(_:)), name: NSNotification.Name("startLoaderWdoPendingSalesAuto"), object: nil)
        
        let objGlobalSyncViewController = GlobalSyncViewController()
        objGlobalSyncViewController.syncCall("WDOPendingApproval")
        
        self.dispatchGroupWdoPending.enter()

        NotificationCenter.default.addObserver(self, selector: #selector(stopLoaderServiceDataIsSynced(_:)), name: NSNotification.Name("startLoaderWdoPendingServiceAuto"), object: nil)

        let objServiceSendMailViewControlleriPad = ServiceSendMailViewControlleriPad()
        objServiceSendMailViewControlleriPad.callApiToSyncDataForWdoPendingApprovalAgreement(toServer: "WDOPendingApproval", "\(objWorkorderDetail.value(forKey: "workorderId") ?? "")")
        
        self.dispatchGroupWdoPending.notify(queue: DispatchQueue.main, execute: {
            
            self.loader.dismiss(animated: false) {
                
                self.updateWorkOrderDetailsOnResponse()
                
                let alertCOntroller = UIAlertController(title: Info, message: "Request for approval sent successfully.", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    
                    self.navigationController?.popViewController(animated: false)
                    
                    /*if(DeviceType.IS_IPAD){
                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                        
                        if strAppointmentFlow == "New" {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "Appointment_iPAD",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        } else {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "MainiPad",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        }
                    }else{
                        
                        let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
                        
                        if strAppointmentFlow == "New" {
                            
                            let mainStoryboard = UIStoryboard(
                                name: "Appointment",
                                bundle: nil)
                            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                            objByProductVC?.isCallDelay = "yes"
                            self.navigationController?.pushViewController(objByProductVC!, animated: false)
                            
                        } else {
                            
                        let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
                        self.navigationController?.pushViewController(controller, animated: false)
                            
                        }
                        
                    }*/
                    
                    
                    
                })
                
                let alertActionNo = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in

                    
                    
                })
                //alertCOntroller.addAction(alertActionNo)
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
        })
        
    }
    
    @objc private func stopLoaderSalesDataIsSynced(_ notification: Notification) {
        
        self.dispatchGroupWdoPending.leave()
        
    }
    
    @objc private func stopLoaderServiceDataIsSynced(_ notification: Notification) {
        
        self.dispatchGroupWdoPending.leave()
        
    }
    
    func updateWorkOrderDetailsOnResponse() {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        //arrOfKeys.add("isPricingApprovalPending")
        arrOfKeys.add("isPricingApprovalMailSend")

        //arrOfValues.add("false")
        arrOfValues.add("false") // Make this False once Data is Synced to Server.

        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@","\(objWorkorderDetail.value(forKey: "workorderId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if(isSuccess)
        {
            print("pricing multiplier updated successfully")
        }
        
    }
    
}
// MARK: -
// MARK: ------------API's Calling--------------

extension PriceJustification_WDO_VC {
    
    
    
}

extension PriceJustification_WDO_VC : UITextViewDelegate{
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //if range.location == 0 && text == " " {
            //return false
        //}
        
        return txtViewValidation(textField: textView, string: text, returnOnly: "", limitValue: 499)
        
        //return true
        
    }
    
}
