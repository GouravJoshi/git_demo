//
//  ServiceHistory_Filter_iPadVC.swift
//  DPS
//
//  Created by NavinPatidar on 5/21/20.
//  Copyright © 2020 Saavan. All rights reserved.


import UIKit



protocol ServiceHistory_Filter_iPadProtocol : class
{
    
    func getDataServiceHistory_Filter_iPadProtocol(dict : NSDictionary ,tag : Int)
    
}

class ServiceHistory_Filter_iPadVC: UIViewController {

    
    // MARK:
      // MARK: ----------IBOutlet
      var aryTblList = NSMutableArray()
      weak var delegate: ServiceHistory_Filter_iPadProtocol?

      @IBOutlet private weak var tvList: UITableView! {
          didSet {
              tvList.tableFooterView = UIView()
              tvList.estimatedRowHeight = 50
          }
      }
      
      var tag = 0 , selectionIndex = 99
    
    // MARK:
    // MARK: ----------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        aryTblList = [["id" : "1" , "name" : "All" ],["id" : "2" , "name" : "Termite work order"]]
        
    }
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        //self.navigationController?.popViewController(animated: false)
        dismiss(animated: false)

    }
    
    @IBAction func actionOnApply(_ sender: UIButton) {
        self.view.endEditing(true)
        delegate?.getDataServiceHistory_Filter_iPadProtocol(dict: aryTblList.object(at: selectionIndex)as! NSDictionary, tag: self.tag)
        //self.navigationController?.popViewController(animated: false)
        dismiss(animated: false)

    }
    
    
    

}
// MARK: -
// MARK: -----UITableViewDelegate

extension ServiceHistory_Filter_iPadVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return aryTblList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "ServiceHistoryFilterCell", for: indexPath as IndexPath) as! ServiceHistoryFilterCell
        let dictData = aryTblList.object(at: indexPath.row)as! NSDictionary
        cell.lbl_Title.text = "\(dictData.value(forKey: "name")!)"
        selectionIndex == indexPath.row ? cell.accessoryType = .checkmark : (cell.accessoryType = .none)
          return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectionIndex = indexPath.row
        self.tvList.reloadData()
        
    }

}


// MARK: -
// MARK: -ServiceHistoryCell
class ServiceHistoryFilterCell: UITableViewCell {
    @IBOutlet weak var lbl_Title: UILabel!
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
   
}
