//
//  ProblemIdentification_WDOVC.swift
//  DPS  peSTream 2020
//  Created by Rakesh Jain on 12/09/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan Patidar 2021fsdgdfgfdgdfgdfgdfgdg


import UIKit
import Alamofire
import SwiftyXMLParser
import CoreData

// MARK: UITableView Cell

class ProblemIdentificationCell:UITableViewCell
{
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblIssueCode: UILabel!
    @IBOutlet weak var lblRecommendationCode: UILabel!
    @IBOutlet weak var lblSubsectionCode: UILabel!
    @IBOutlet weak var lblInfastation: UILabel!
    @IBOutlet weak var lblLeadTest: UILabel!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var lblFinding: UILabel!
    
    @IBOutlet weak var txtviewRecommendation: UITextView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblFindingFilling: UILabel!
    @IBOutlet weak var lblRecommendationFilling: UILabel!
    @IBOutlet weak var lblInfastationTitle: UILabel!
    @IBOutlet weak var lblLeadTestTitle: UILabel!
    @IBOutlet weak var const_LeadTest_Leading: NSLayoutConstraint!
    @IBOutlet weak var lblViewBelowInfestation: UIView!
    @IBOutlet weak var const_FindingFilling_VerticalUp: NSLayoutConstraint!
    @IBOutlet weak var lblBuildingPermit: UILabel!
    @IBOutlet weak var txtviewFinding: UITextView!
    @IBOutlet weak var viewProblemImages: UIView!
    @IBOutlet weak var const_ViewProblemImages_H: NSLayoutConstraint!
    
}

class ProblemIdentificationCellNew:UITableViewCell
{
    @IBOutlet weak var lblFinding: TopAlignedLabel!
    @IBOutlet weak var lblFindingCode: TopAlignedLabel!
    @IBOutlet weak var lblFindingFIlling: TopAlignedLabel!
    @IBOutlet weak var lblRecomndationCode: TopAlignedLabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!

    @IBOutlet weak var lblFinding_S: UILabel!
    @IBOutlet weak var lblFindingCode_S: UILabel!
    @IBOutlet weak var lblFindingFIlling_S: UILabel!
    @IBOutlet weak var lblRecomndationCode_S: UILabel!
    
    @IBOutlet weak var lblUnderline: UILabel!

}
class DisclaimerCell:UITableViewCell
{
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWhere: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    @IBOutlet weak var btnDelete: UIButton!
    
}

class GeneralNotesCell:UITableViewCell
{
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblFillin: UILabel!
    
    @IBOutlet weak var txtViewGeneralNoteDesc: UITextView!
    
}

class ProblemIdentification_WDOVC: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var btnShowHideWDOInspection: UIButton!
    @IBOutlet weak var btnEditWdoInspection: UIButton!
    
    
    // Report Type
    @IBOutlet weak var btnCompleteReport: UIButton!
    @IBOutlet weak var btnLimitedResport: UIButton!
    @IBOutlet weak var btnSupplementalReport: UIButton!
    @IBOutlet weak var btnReinspectionReport: UIButton!
    //////////////
    
    
    @IBOutlet weak var txtSubareaAccess: ACFloatingTextField!
    @IBOutlet weak var txtNumberOfStories: ACFloatingTextField!
    @IBOutlet weak var txtExternalMaterial: ACFloatingTextField!
    @IBOutlet weak var txtRoofingMaterial: ACFloatingTextField!
    
    @IBOutlet weak var btnYearUnknown: UIButton!
    @IBOutlet weak var btnYes_BuildingPermit: UIButton!
    @IBOutlet weak var btnNo_BuildingPermit: UIButton!
    
    
    @IBOutlet weak var txtCPCInspTagPosted: ACFloatingTextField!
    @IBOutlet weak var txtPastPcoTagPosted: ACFloatingTextField!
    @IBOutlet weak var txtNameOfPrevPco: ACFloatingTextField!
    @IBOutlet weak var txtDateOfPrevPco: ACFloatingTextField!
    
    
    @IBOutlet weak var txtviewStructureDescription: UITextView!
    @IBOutlet weak var txtviewComment: UITextView!
    @IBOutlet weak var txtviewGenralDescription: UITextView!
    
    
//    @IBOutlet weak var btnSubterranianTermites: UIButton!
//    @IBOutlet weak var btnDrywoodTermited: UIButton!
//    @IBOutlet weak var btnFungus: UIButton!
//    @IBOutlet weak var btnOtherFindings: UIButton!
//    @IBOutlet weak var btnFurtherInspection: UIButton!
    
    
    // Others
    @IBOutlet weak var btnOccupied: UIButton!
    @IBOutlet weak var btnVacant: UIButton!
    @IBOutlet weak var btnUnfurnished: UIButton!
    @IBOutlet weak var btnFurnished: UIButton!
    @IBOutlet weak var btnSlab: UIButton!
    @IBOutlet weak var btnRaised: UIButton!
    @IBOutlet weak var btnRaisedSlab: UIButton!
    
    // Separate Report
    @IBOutlet weak var btnYes_SeparateReport: UIButton!
    @IBOutlet weak var btnNo_SeparateReport: UIButton!
    
    // TIP Warranty
    //@IBOutlet weak var btnYes_TipWarranty: UIButton!
    //@IBOutlet weak var btnNoTipWarranty: UIButton!
    
    // TIP Warranty Type
    //@IBOutlet weak var btnTipWarrantyType: UIButton!
    //@IBOutlet weak var txtMonthlyAmount: ACFloatingTextField!
    
    // Problem Identification
    @IBOutlet weak var btnShowHideProblemIdentification: UIButton!
    @IBOutlet weak var btnAddProblemIdentification: UIButton!
    @IBOutlet weak var tblviewProblemIdentification: UITableView!
    
    // Disclaimer
    @IBOutlet weak var tblviewDisclaimer: UITableView!
    
    @IBOutlet weak var btnShowHideDisclaimer: UIButton!
    
    @IBOutlet weak var btnAddDisclaimer: UIButton!
    
    
    @IBOutlet weak var btnProposal: UIButton!
    @IBOutlet weak var btnFinalizeReport: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // constant outlets
    
    @IBOutlet weak var hghtConstViewContainerWDOInspection: NSLayoutConstraint!
    @IBOutlet weak var hghtConstTblViewProblemIdentification: NSLayoutConstraint!
    @IBOutlet weak var hghtConstTblViewDisclaimer: NSLayoutConstraint!
    @IBOutlet weak var hghtConstGraph: NSLayoutConstraint!
    
    //@IBOutlet weak var hghtConstViewContainerTIPWarrantyType: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblOrderBY: UILabel!
    
    @IBOutlet weak var lblOrderByAddress: UILabel!
    
    @IBOutlet weak var lblPropertyOwnerName: UILabel!
    
    @IBOutlet weak var lblPropertyOwnerAddress: UILabel!
    
    @IBOutlet weak var lblReportSentName: UILabel!
    
    @IBOutlet weak var lblReportSentAddress: UILabel!
    
    @IBOutlet weak var txtYearOfStructure: ACFloatingTextField!
    
    //@IBOutlet weak var hghtConstBtnAddProblem: NSLayoutConstraint!
    
    @IBOutlet weak var segmentControls: UISegmentedControl!
    
    @IBOutlet weak var hghtConstWdoInspectionViewHeader: NSLayoutConstraint!
    
    @IBOutlet weak var viewWdoInspectionHeader: CardView!
    
    @IBOutlet weak var hghtConstProblemIdentificationHeader: NSLayoutConstraint!
    
    @IBOutlet weak var viewProblemIdentificationHeader: CardView!
    
    @IBOutlet weak var hghtConstDisclaimerHeader: NSLayoutConstraint!
    
    @IBOutlet weak var viewDisclaimerHeader: CardView!
    
    @IBOutlet weak var hghtConstAddProblemHeader: NSLayoutConstraint!
    
    @IBOutlet weak var constDisclaimerHeaderUpVerticalSpace: NSLayoutConstraint!
    
    @IBOutlet weak var constProblemIdentificationHeaderUpVerticalSpace: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var btnShowHideGraphWebView: UIButton!
    
    @IBOutlet weak var hghtConstGraphWebViewHide: NSLayoutConstraint!
    
    @IBOutlet weak var viewTermiteIssueCodes: UIView!
    
    @IBOutlet weak var hghtConstTermiteIssueCodeView: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubAreaAccess: UIButton!
    
    @IBOutlet weak var btnNoOfStories: UIButton!
    
    @IBOutlet weak var btnExternalMaterial: UIButton!
    
    @IBOutlet weak var btnRoofingMaterial: UIButton!
    
    @IBOutlet weak var btnCpcTagPosted: UIButton!
    
    @IBOutlet weak var btnPastPcoTagPosted: UIButton!
    
    @IBOutlet weak var btnStructureDescription: UIButton!
    
    @IBOutlet var viewProblemId: UIView!
    
    @IBOutlet weak var hghtConstTblViewProblemId: NSLayoutConstraint!
    
    @IBOutlet weak var btnHideProblemIdList: UIButton!
    
    @IBOutlet weak var btnFollowUpInspection: UIButton!
    @IBOutlet weak var btn_Garage: UIButton!

    @IBOutlet var viewGeneralNotes: UIView!
    
    @IBOutlet weak var tblViewGeneralNotes: UITableView!
    
    @IBOutlet weak var btnAddGeneralNotes: UIButton!
    
    // TIP Warranty Type
    @IBOutlet weak var btnTipWarrantyType: UIButton!
    @IBOutlet weak var txtMonthlyAmount: ACFloatingTextField!
    
    @IBOutlet weak var txtFldOther: ACFloatingTextField!
    
//Graph Legend

    @IBOutlet weak var viewGraphLegend: UIView!
    @IBOutlet weak var heightGraphLegend: NSLayoutConstraint!
    @IBOutlet weak var heightScrollGraphLegend: NSLayoutConstraint!

    @IBOutlet weak var view_Footer: UIView!

    

    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var arrOfDiclaimer = NSMutableArray()
    var dictOfTipWarranty = NSDictionary()
    var arrOfProblemIdentification = NSArray()
    var strReportTypeName = String()
    var strReportTypeId = String()
    var strReportTypeSysName = String()
    var isBuildingPermit = true
    var isYearUnknown = true
    var isTipWarranty = true
    var isSeparateReport = true
    var strTipMasterName = String()
    var strTipMasterId = String()
    var strOccupiedVacant = String()
    var strFurnishedUFurnished = String()
    var strSlabRaised = String()
    var arrOfCheckBox = NSMutableArray()
    var strWoLeadId = String ()
    var dictOfSubAreaAccess = NSDictionary()
    var dictOfNumberOfStories = NSDictionary()
    var dictOfExternalMaterials = NSDictionary()
    var dictOfRoofingMaterials = NSDictionary()
    var dictOfCpcTagPosted = NSDictionary()
    var dictOfPastPcoTagPosted = NSDictionary()
    var dictOfStructereDesc = NSDictionary()
    var dictOfFollowUpInspection = NSDictionary()
    var dictOfGarage = NSDictionary()
    var dictLoginData = NSDictionary()
    
    var arrOfGeneralNotes = NSArray()
    var graphXmlGlobalIfExist = String()
    var graphXmlGlobalIfExistNew = String()

    @IBOutlet weak var graphDrawingWebView: WKWebView!
    var graphXmlParseMsg = "Unable to update graph"
    var IsTIPWarrantyRequired =  "true"
    var isGeneralDescriptionPrepare = false
    // MARK: - -----------------------------------Life Cycle -----------------------------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        txtExternalMaterial.delegate = self
        txtRoofingMaterial.delegate = self
        let defsLogindDetail = UserDefaults.standard
        
        if  defsLogindDetail.value(forKey: "LoginDetails") != nil && defsLogindDetail.value(forKey: "LoginDetails") is NSDictionary
        {
            dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        }
        if dictLoginData.count != 0 {
            IsTIPWarrantyRequired = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsTIPWarrantyRequired") ?? "true")"
        }
        
        txtFldOther.isHidden = true
        tblViewGeneralNotes.tableFooterView = UIView()
        tblViewGeneralNotes.estimatedRowHeight = 600.0
        tblViewGeneralNotes.rowHeight = UITableView.automaticDimension
        
        //hghtConstBtnAddProblem.constant = 0.0
        
        lblOrderNumber.text = nsud.value(forKey: "lblName") as? String
        
        // Do any additional setup after loading the view.
        setUpInitialConstanOfViews()
        configureUI()
        
        btnTipWarrantyType.setTitle(strSelectString, for: .normal)
        btnFollowUpInspection.setTitle(strSelectString, for: .normal)

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            //strWoLeadId = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                strWoLeadId = "\(match.value(forKey: "leadId")!)"
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        txtDateOfPrevPco.addTarget(self, action: #selector(selectDatePrevPCO), for: .allEvents)
        //txtYearOfStructure.addTarget(self, action: #selector(selectYearStructure), for: .allEvents)
        
        
        setValuesWdoInspection()
        
        setInitalSegement()
        
        //btnShowHideGraphWebView.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
        hghtConstGraphWebViewHide.constant = 0

        createDynamicIssueCode()
        
        
        // check if to add default disclaimer in WO
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
        }else{
            
            // add default disclaimer from Master to DB.
            
            addDefaultDisclaimer()
            
        }
        
        nsud.set(false, forKey: "yesupdatedWdoInspection")
        nsud.synchronize()
        
        // Saavan Patidar Changes to save data if App entered background or Terminated
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(onAppTerminatedSaveData), name: Notification.Name("AppTerminatedSaveData"), object: nil)
        nc.addObserver(self, selector: #selector(onAppTerminatedSaveData), name: Notification.Name("AppEnterdInBackgroundSaveData"), object: nil)

        // delete wdo graph draw vversion history
        
        self.deleteWdoGraphDrawHistoryRecordsFromDB()
        
    }
    
    //
    @objc func onAppTerminatedSaveData(_ notification:Notification) {
        // App DisAppeared
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            // Not To Edit
        }else{
            if isBackGround {
                saveGraphAsXML()
           }
        }
    }


    /*
     override func viewDidAppear(_ animated: Bool) {
     super.viewDidAppear(animated)
     
     let bundle = Bundle.main
     
     let filename = "index"
     let fileExtension = "html"
     let directory = "graphing"
     
     let indexUrl = bundle.url(forResource: filename, withExtension: fileExtension, subdirectory: directory)
     let indexPath = bundle.path(forResource: filename, ofType: fileExtension, inDirectory: directory)
     
     let wwwUrl = bundle.url(forResource: directory, withExtension: nil)
     let wwwPath = bundle.path(forResource: directory, ofType: nil)
     
     graphDrawingWebView.loadFileURL(indexUrl!, allowingReadAccessTo: wwwUrl!)
     
     //graphDrawingWebView.loadFileUrl(indexUrl!, allowingReadAccessTo: wwwUrl!)
     
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {

        reloadTermiteIssueCode()

        if (nsud.value(forKey: "yesupdatedWdoInspection") != nil) {
            
            let yesupdatedWdoInspection = nsud.bool(forKey: "yesupdatedWdoInspection")
            
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "yesupdatedWdoInspection")
                nsud.synchronize()
                
                fetchDataFrmDB()
                setValuesWdoInspection()
                
            }
            
        }
        
        fetchDataFrmDB()

        /*
        // If From Add Problem Identification to show Problem  list view open Default
        if (nsud.value(forKey: "addedProblem") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "addedProblem")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "addedProblem")
                nsud.synchronize()
                
                if segmentControls.selectedSegmentIndex == 1 {
                    
                    setProblemViewOnEdit()
                    
                }
                
            }
            
        }
        */
        disableControls()
        
        if segmentControls.selectedSegmentIndex == 1 {
            
            showProblemIdentification()
            
        }
        
        // Conditon  To Reload Graph when coming from Restore View.
        if (nsud.value(forKey: "yesRestoredLocalGraph") != nil) {
            
            let yesupdatedWdoInspection = nsud.bool(forKey: "yesRestoredLocalGraph")
            
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "yesRestoredLocalGraph")
                nsud.synchronize()
                
                if segmentControls.selectedSegmentIndex == 1 {
                    
                    self.view.isUserInteractionEnabled = false

                    // Graph Diagram
                    hghtConstWdoInspectionViewHeader.constant = 0
                    hghtConstViewContainerWDOInspection.constant = 0
                    viewWdoInspectionHeader.isHidden = true
                    
                    hghtConstDisclaimerHeader.constant = 0
                    viewDisclaimerHeader.isHidden = true
                    hghtConstTblViewDisclaimer.constant = 0.0
                    
                    viewProblemIdentificationHeader.isHidden = true
                    hghtConstProblemIdentificationHeader.constant = 0
                    showProblemIdentification()
                    hghtConstGraphWebViewHide.constant = 0

                    constProblemIdentificationHeaderUpVerticalSpace.constant = -20-CGFloat(210*self.arrOfDiclaimer.count)
                    
                    scrollView.isScrollEnabled = false
                    self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
                    graphDrawingWebView.isHidden = false
                    scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                    viewGeneralNotes.removeFromSuperview()
                    viewProblemId.removeFromSuperview()

                    saveGraphAsXML()
                    
                    saveWdoInspectionInDB(yesCheck: false)
                    
                    fetchNotes()
                    
                    fetchDisclaimer()
                    
                    self.view.isUserInteractionEnabled = true
                    
                }
                
            }
            
        }
        
        // Saavan Paatidar
        /*if nsud.bool(forKey: "RefreshGraph_ProblemAdd") {
            
           UserDefaults.standard.set(false, forKey: "RefreshGraph_ProblemAdd")

            if segmentControls.selectedSegmentIndex == 1 {
                
                showProblemIdentification()
                
            }
            
        }*/
        showProblemIdentification()
        showGraphLegend()
        isBackGround = true
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        isBackGround = false
    }
    
    // MARK: ---------------------------------------showGraphLegend------------------------------------------

    func showGraphLegend() {
        //For graph Legend
       
        if segmentControls.selectedSegmentIndex == 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { [self] in
                
                
                LegendGraphView().CreatFormLegend(viewLegend: viewGraphLegend, controllor: self) { viewContain  in
                    
                    let heightScroll = Int(viewContain.tag) + 50
                  
                    if getGraphLegendCategory().count != 0 {
                        self.heightGraphLegend.constant = 150.0
                      
                    }else{
                        self.heightGraphLegend.constant = 0.0
                    }
                    self.heightScrollGraphLegend.constant = CGFloat(heightScroll)
                    
                   
                }

            })
        }else{
            self.heightGraphLegend.constant = 0.0
        }
       
    }
    // MARK: - -----------------------------------Button Actions-----------------------------------
    
    func back()  {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func action_SaveGraph(_ sender: Any) {
        graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { (results, error) in
//            print(results ?? "results")
//            print("Results")
            self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
        })
    }
        
    func loadSavedXMLGraph() {
        do {
            var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
            if arrayOfImagesXML.count == 0 {
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
                
            }
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            }
            //loadGraphCategory()
            if arrayOfImagesXML.count > 0 {
                
                let dictData = arrayOfImagesXML[0] as! NSManagedObject
                
                let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
                //let xmlFileName = "Demo.xml"
                
                let path = (getDirectoryPath() as NSString).appendingPathComponent(xmlFileName)
                
                let xmlContents = try String(contentsOfFile: path)
                
                graphXmlGlobalIfExist = xmlContents
                
               // print(xmlContents)
                
                let xmlContentEdited = "'\(xmlContents.trimmingCharacters(in: .whitespacesAndNewlines))'"

               // print(xmlContentEdited)
                
                let fileName = "'\(xmlFileName)'"
                
             //   print("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")
                
                graphDrawingWebView.evaluateJavaScript("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")  { (result, error) in
                    guard error == nil else {
                        print("there was an error")
                        print(error!.localizedDescription)
                        print(error!)
                        return
                    }
                    
                }
                
            }
            
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func loadSavedXMLGraphToCheckIfToUpdatedModifyDate() {
        
        do {
            
            var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
            
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
                
            }
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
                
            }
                        
            if arrayOfImagesXML.count > 0 {
                
                let dictData = arrayOfImagesXML[0] as! NSManagedObject
                
                let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
                
                let path = (getDirectoryPath() as NSString).appendingPathComponent(xmlFileName)
                
                let xmlContents = try String(contentsOfFile: path)
                
                graphXmlGlobalIfExistNew = xmlContents
                
                if graphXmlGlobalIfExistNew == graphXmlGlobalIfExist {
                    
                }else{
                    
                    //Update Modify Date In Sales DB
                    Global().updateSalesModifydate(strWoLeadId)

                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }
                                                
            }
            
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func saveGraphToDocumentDirectoryAsXML(stringXML : String) {
        // parse xml document
        let xml = try? XML.parse(stringXML)
        let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
       // if(strcount ?? 0 > 3){
        let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
        
        if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
            let str = stringXML
            
            let xmlFileName = "\\Documents\\GraphXMLFiles\\XML"  + "Graph" + "\(strWoId)\(getUniqueValueForId())" + ".xml"
            
            saveGraphXmlToImageDetail(graphXmlPath: xmlFileName ,graphXmll: str)

            let filename = getDocumentsDirectory().appendingPathComponent(xmlFileName)
            
            do {
                try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
            } catch {
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
        }
     
                
    }
    
    
  
    
    func deleteMainGraphImageBeforeSaving() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlImageName = "\(dictData.value(forKey: "woImagePath") ?? "")"

            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"true"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"True"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"1"))
            
            //Global().removeImage(fromDocumentDirectory: xmlImageName)
            
            let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            //Global().removeImage(fromDocumentDirectory: strImageNameSales)
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strImageNameSales))

            // jugad for deleting sales image if synced from web
            
            let strNameTemp = "UploadImages\\" + strImageNameSales
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strNameTemp))

        }
        
    }
    
    func saveGraphXmlToImageDetail(graphXmlPath : String , graphXmll : String) {
        
        deleteMainGraphImageBeforeSaving()

        let strImageName = "\\Documents\\UploadImages\\Img"  + "Graph" + "\(strWoId)\(getUniqueValueForId())" + ".jpeg"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("woImagePath")
        arrOfKeys.add("woImageType")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("woImageId")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("isProblemIdentifaction")
        arrOfKeys.add("graphXmlPath")

        
        let defsLogindDetail = UserDefaults.standard
        
        if  defsLogindDetail.value(forKey: "LoginDetails") != nil && defsLogindDetail.value(forKey: "LoginDetails") is NSDictionary
        {
            dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        }
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let woImageId = "Mobile" + "\(strWoId)" + getUniqueValueForId()
        let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
        
        
        arrOfValues.add("") //createdBy
        arrOfValues.add("") //createdDate
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strImageName)
        arrOfValues.add("Graph")
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(woImageId) // woImageId
        arrOfValues.add(modifiedDate ?? "")
        arrOfValues.add(strWoId)
        arrOfValues.add("") // imageCaption
        arrOfValues.add("") // imageDescription
        arrOfValues.add(latitude) // latitude
        arrOfValues.add(longitude) //  longitude
        arrOfValues.add("true") //  isProblemIdentifaction
        arrOfValues.add(graphXmlPath) //  isProblemIdentifaction

        // Fetch Image From WebView Graph
        // "window.graphEditor.exportImage()"
        //"window.graphEditor.imageWithoutGrid()"
        
        /*graphDrawingWebView.evaluateJavaScript("window.graphEditor.exportImage()", completionHandler: { (results, error) in
            
             print(results ?? "results")
             print("Results")
            
            let dictDataL = NSMutableDictionary()
            dictDataL.setObject(arrOfKeys, forKey: arrOfValues)
            
            self.getImageFromXMl(strImageName: strImageName ,graphXmll: graphXmll, dictDataL: dictDataL)

            if results != nil {
                
                
            }
            
        })*/
        
        
        // Changed the above code after discussing with Parm Sir.
        
        let dictDataL = NSMutableDictionary()
        dictDataL.setObject(arrOfKeys, forKey: arrOfValues)
        
        self.getImageFromXMl(strImageName: strImageName ,graphXmll: graphXmll, dictDataL: dictDataL)

        
        saveDataInDB(strEntity: "ImageDetailsServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        // Save Main Graph to service auto Also
        
        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
        
        let arrOfKeysSales = NSMutableArray()
        let arrOfValuesSales = NSMutableArray()
        
        arrOfKeysSales.add("createdBy")
        arrOfKeysSales.add("createdDate")
        arrOfKeysSales.add("companyKey")
        arrOfKeysSales.add("userName")
        arrOfKeysSales.add("leadImagePath")
        arrOfKeysSales.add("leadImageType")
        arrOfKeysSales.add("modifiedBy")
        arrOfKeysSales.add("leadImageId")
        arrOfKeysSales.add("modifiedDate")
        arrOfKeysSales.add("leadId")
        arrOfKeysSales.add("leadImageCaption")
        arrOfKeysSales.add("descriptionImageDetail")
        arrOfKeysSales.add("latitude")
        arrOfKeysSales.add("longitude")
        
        arrOfValuesSales.add("") //createdBy
        arrOfValuesSales.add("") //createdDate
        arrOfValuesSales.add(strCompanyKey)
        arrOfValuesSales.add(strUserName)
        arrOfValuesSales.add(strImageNameSales)
        arrOfValuesSales.add("Graph")
        arrOfValuesSales.add(strEmployeeid)
        arrOfValuesSales.add(woImageId) // woImageId
        arrOfValuesSales.add(modifiedDate ?? "")
        arrOfValuesSales.add(strWoLeadId)
        arrOfValuesSales.add("") // imageCaption
        arrOfValuesSales.add("") // imageDescription
        arrOfValuesSales.add(latitude) // latitude
        arrOfValuesSales.add(longitude) //  longitude
        
        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
        
        // WdoGraphDrawHistory
    }
    
    func getImageFromXMl(strImageName : String , graphXmll : String , dictDataL : NSMutableDictionary) {
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

        // If Ture then Image Without Grid Wali milegi and If False to With Grid Wali Milegi
        
        self.graphDrawingWebView.evaluateJavaScript("window.graphEditor.convertToImage(\(true))", completionHandler: { (results, error) in
            
            //print(results ?? "results")
            //print("Results")
            
            if results != nil {
                
                var strBase64Img = String()
                
                strBase64Img = results as! String
                
                if strBase64Img == "Not able to save image." {
                    
                    //
                    
                }else{
                    
                    strBase64Img = strBase64Img.replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
                    
                    if strBase64Img == "/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAAaADAAQAAAABAAAAAQAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgAAQABAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAgICAgICAwICAwUDAwMFBgUFBQUGCAYGBgYGCAoICAgICAgKCgoKCgoKCgwMDAwMDA4ODg4ODw8PDw8PDw8PD//bAEMBAgICBAQEBwQEBxALCQsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEP/dAAQAAf/aAAwDAQACEQMRAD8A+wKKKK/rQ/mM/9k=" {
                        
                        // No Image Added for Graph so delete Image Detail Obj from DB
                        
                        self.deleteMainGraphImageBeforeSaving()
                        
                    }else{
                        
                        let dataDecoded:NSData = NSData(base64Encoded: strBase64Img, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                        
                        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
                        
                        //let imageResized = Global().resizeImageGloballl(decodedimage)
                        
                        saveImageDocumentDirectory(strFileName: strImageName, image: decodedimage)
                        
                        // Save Image To Sales Auto Also
                        
                        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                        
                        saveImageDocumentDirectory(strFileName: strImageNameSales, image: decodedimage)
                        
                        //WdoGraphDrawHistory
                        
                        self.saveWdoGraphDrawHistory(graphXmll: graphXmll, decodedimage: decodedimage, dictDataL: dictDataL)
                        
                    }
                    
                }
                
            }
            
        })
        
    }
    
    func saveWdoGraphDrawHistory(graphXmll : String , decodedimage : UIImage , dictDataL : NSMutableDictionary) {
        
        
        //---Check Graph is blank or alredy exist
        
        let xml = try? XML.parse(graphXmll)
        let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
        //if(strcount ?? 0 > 3){
        let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
        
        if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
            let sortDescriptor = NSSortDescriptor(key: "localCreatedDate", ascending: false)
            //arrayOfList = getDataFromCoreDataBaseArray(strEntity: "WdoGraphDrawHistory", predicate: NSPredicate(format: "workorderId == %@", strGlobalWoId))
            let arrayOfListtemp = getDataFromCoreDataBaseArraySorted(strEntity: "WdoGraphDrawHistory", predicate: NSPredicate(format: "workorderId == %@", strWoId), sort: sortDescriptor)
                    let aaygraphXML = arrayOfListtemp.filter { (task) -> Bool in
                        return ("\((task as! NSManagedObject).value(forKey: "graphXML")!)".contains(graphXmll)) }
                    if aaygraphXML.count == 0 {

                        
                        let strImageName = "\\Documents\\UploadImages\\Img"  + "MainGraph" + "\(strWoId)\(getUniqueValueForId())" + ".jpeg"
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("graphXmlPath")
                        arrOfKeys.add("graphImageName")
                        arrOfKeys.add("graphXML")
                        arrOfKeys.add("workorderId")
                        
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("woImagePath")
                        arrOfKeys.add("woImageType")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("woImageId")
                        arrOfKeys.add("modifiedDate")
                        arrOfKeys.add("workorderId")
                        arrOfKeys.add("imageCaption")
                        arrOfKeys.add("imageDescription")
                        arrOfKeys.add("latitude")
                        arrOfKeys.add("longitude")
                        arrOfKeys.add("isProblemIdentifaction")
                        arrOfKeys.add("localCreatedDate")

                        
                        let defsLogindDetail = UserDefaults.standard
                        
                        if  defsLogindDetail.value(forKey: "LoginDetails") != nil && defsLogindDetail.value(forKey: "LoginDetails") is NSDictionary
                        {
                            dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                        }
                        var strCompanyKey = String()
                        var strUserName = String()
                        
                        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                            
                            strCompanyKey = "\(value)"
                        }
                        
                        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                            
                            strUserName = "\(value)"
                            
                        }
                        let strLocalCreatedDate = Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss.SSS", "EST")

                        arrOfValues.add(strCompanyKey)
                        arrOfValues.add(strUserName)
                        arrOfValues.add(strImageName)
                        arrOfValues.add(strImageName)
                        arrOfValues.add(graphXmll)
                        arrOfValues.add(strWoId)
                        
                        arrOfValues.add("") //createdBy
                        arrOfValues.add(strLocalCreatedDate) //createdDate
                        arrOfValues.add(strImageName)
                        arrOfValues.add("Graph")
                        arrOfValues.add("\(dictDataL.value(forKey: "modifiedBy") ?? "")")
                        arrOfValues.add("\(dictDataL.value(forKey: "woImageId") ?? "")") // woImageId
                        arrOfValues.add("\(dictDataL.value(forKey: "modifiedDate") ?? "")")
                        arrOfValues.add(strWoId)
                        arrOfValues.add("") // imageCaption
                        arrOfValues.add("") // imageDescription
                        arrOfValues.add("\(dictDataL.value(forKey: "latitude") ?? "")") // latitude
                        arrOfValues.add("\(dictDataL.value(forKey: "longitude") ?? "")") //  longitude
                        arrOfValues.add("true") //  isProblemIdentifaction
                        
                        let dateLocalCreated = Global().getDueDate(strLocalCreatedDate)
                        
                        arrOfValues.add(dateLocalCreated!)

                        saveDataInDB(strEntity: "WdoGraphDrawHistory", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        saveImageDocumentDirectory(strFileName: strImageName, image: decodedimage)
                        
                    }else{
                      //  showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Graph xml Same", viewcontrol: self)
                    }
                   
                
        }
        
        
  
        
        
      
        
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
        
    }
    
    @IBAction func action_SelectTipWarranty(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            ary = addSelectInArray(strName: "TIPWarrantyType", array: ary)
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    
    @IBAction func action_back(_ sender: UIButton) {
    
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            // Not To Edit
            
        }else{
            
            saveGraphAsXML()
            
        }
        
        back()
        
    }
    
    @IBAction func action_showHideWDOInspection(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "close_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            self.hghtConstViewContainerWDOInspection.constant = 0.0
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            self.hghtConstViewContainerWDOInspection.constant = 1800

        }
        
    }
    
    @IBAction func action_EditWDOInspection(_ sender: UIButton) {
        
        // goto edit wdo inspection vc
        goToEditInspectionView()
        
    }
    
    @IBAction func action_completeReport(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "radio_uncheck_ipad"))
        {
            sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            
            btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            strReportTypeName = enumReportCompleteName
            strReportTypeId = "1"
            strReportTypeName = enumReportComplete
            strReportTypeSysName = enumReportCompleteName
            
            checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "No")
            
        }
        
    }
    
    @IBAction func action_limitedReport(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "radio_uncheck_ipad"))
        {
            sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            
            btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            strReportTypeName = enumReportLimitedName
            strReportTypeId = "2"
            strReportTypeName = enumReportLimited
            strReportTypeSysName = enumReportLimitedName
            
            checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "No")

        }
    }
    
    @IBAction func action_supplementalReport(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strReportTypeName = enumReportSupplementalName
        strReportTypeId = "3"
        strReportTypeName = enumReportSupplemental
        strReportTypeSysName = enumReportSupplementalName
        
        checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "No")

    }
    
    @IBAction func action_reinspectionReport(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strReportTypeName = enumReportReinspectionName
        strReportTypeId = "4"
        strReportTypeName = enumReportReinspection
        strReportTypeSysName = enumReportReinspectionName
        
        checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "No")

    }
    
    @IBAction func action_yearUnknown(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            isYearUnknown = false
            
            txtYearOfStructure.isHidden = false
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            isYearUnknown = true
            
            txtYearOfStructure.isHidden = true
            
        }
    }
    
    @IBAction func action_yesBuildingPermit(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnNo_BuildingPermit.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        isBuildingPermit = true
        
    }
    
    @IBAction func action_NoBuildingPermit(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnYes_BuildingPermit.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isBuildingPermit = false
        
    }
    
    @IBAction func action_subterranianTermites(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            
            arrOfCheckBox.remove(enumSUBTERRANEANTERMITES)
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
            arrOfCheckBox.add(enumSUBTERRANEANTERMITES)
            
        }
    }
    
    @IBAction func action_drywoodTermited(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            
            arrOfCheckBox.remove(enumDRYWOODTERMITES)
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
            arrOfCheckBox.add(enumDRYWOODTERMITES)
            
        }
    }
    
    @IBAction func action_fungus(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            
            arrOfCheckBox.remove(enumFUNGUSDRYROT)
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
            arrOfCheckBox.add(enumFUNGUSDRYROT)
            
        }
    }
    
    @IBAction func action_otherFindings(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            
            arrOfCheckBox.remove(enumOTHERFINDINGS)
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
            arrOfCheckBox.add(enumOTHERFINDINGS)
            
        }
        
    }
    
    @IBAction func action_FurtherInspection(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            
            arrOfCheckBox.remove(enumFURTHERINSPECTION)
            
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
            arrOfCheckBox.add(enumFURTHERINSPECTION)
            
        }
        
    }
    
    @IBAction func action_occupied(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "radio_uncheck_ipad"))
        {
            
            sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            
            btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            strOccupiedVacant = enumOccuppied
            generateGeneralDesc()

            //            btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            
        }
        
    }
    
    @IBAction func action_vacant(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "radio_uncheck_ipad"))
        {
            
            sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            
            btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            strOccupiedVacant = enumVacant
            generateGeneralDesc()

            //            btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            //            btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
        }
    }
    
    @IBAction func action_unfurnished(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        //        btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strFurnishedUFurnished = enumUnfurnished
        generateGeneralDesc()

        //        btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
    }
    
    @IBAction func action_furnished(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        //        btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strFurnishedUFurnished = enumFurnished
        generateGeneralDesc()

        //        btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
    }
    
    @IBAction func action_slab(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        //        btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strSlabRaised = enumSlab
        
        btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        generateGeneralDesc()

    }
    
    @IBAction func action_raised(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        //        btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strSlabRaised = enumRaised
        generateGeneralDesc()

    }
    
    @IBAction func action_raisedSlab(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
        //        btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        //        btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        strSlabRaised = enumRaisedSlab
        generateGeneralDesc()

        
    }
    
    @IBAction func action_yesSeparateReport(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnNo_SeparateReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isSeparateReport = true
        
    }
    
    @IBAction func action_noSeparateReport(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnYes_SeparateReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isSeparateReport = false
        
    }
    
    @IBAction func action_yesTIPWarranty(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        //btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isTipWarranty = true
        
        //hghtConstViewContainerTIPWarrantyType.constant = 100
        //hghtConstViewContainerWDOInspection.constant = 2740
        
        if dictOfTipWarranty.count > 0 {
            
            let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
            
            if isMonthlyAmount {
                
                //txtMonthlyAmount.isHidden = false
                
            } else {
                
                //txtMonthlyAmount.isHidden = true
                
            }
            
        } else {
            
            //txtMonthlyAmount.isHidden = true
            
        }
        
        
    }
    
    @IBAction func action_noTIPWarranty(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        //btnYes_TipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isTipWarranty = false
        //hghtConstViewContainerTIPWarrantyType.constant = 0.0
        strTipMasterName = ""
        strTipMasterId = ""
        
    }
    
    @IBAction func action_showHideProblemIdentification(_ sender: UIButton) {
        
        
        if(sender.currentImage == UIImage(named: "close_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            //hghtConstTblViewProblemIdentification.constant = 0.0
            hghtConstGraph.constant = 0.0
            //hghtConstBtnAddProblem.constant = 0.0
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            
            
            UIView.animate(withDuration: 1.0) {
                
                //self.hghtConstTblViewProblemIdentification.constant = CGFloat(60*self.arrOfProblemIdentification.count)// here calculate tableview height
                //self.hghtConstTblViewProblemIdentification.constant = self.hghtConstTblViewProblemIdentification.constant
                self.hghtConstGraph.constant = 576
                self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
                self.graphDrawingWebView.isHidden = false
                //self.hghtConstBtnAddProblem.constant = 60.0

                self.showProblemIdentification()
            }
            
        }
        
    }
    
    
    @IBAction func action_addProblemIdentification(_ sender: UIButton) {
        
        goToProblemIdentificationAddView(strToUpdate: "No", strProblemIdentificationIdLocal: "", strMobileProblemIdentificationIdLocal: "")
        
    }
    
    @IBAction func action_showHideDisclaimer(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "close_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            hghtConstTblViewDisclaimer.constant = 0.0
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            
            self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)

            /*
            UIView.animate(withDuration: 1.0) {
                self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count) // here calculate tableview height
                self.view.layoutIfNeeded()
            }*/
            
        }
        
    }
    
    @IBAction func action_addDisclaimer(_ sender: UIButton) {
        
        // goto add disclaimer vc
        
        goToDisclaimerAddView(strToUpdate: "No", strDisclaimerId: "")
        
    }
    
    @IBAction func action_proposal(_ sender: UIButton) {
        
        self.view.endEditing(true)

        
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            goToConfigureProposalView(strForProposal: "Yes")
            
        }else{
            if( validationForAddWdoInspection(yesCheck: true)){
                saveWdoInspectionInDB(yesCheck: true)
                saveGraphAsXML()
                validationproposal()
            }
           
        }
        
    }
    

    
    @IBAction func action_finalizeReport(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            goToFinalizeReportView()
            
        }else{
            
            if( validationForAddWdoInspection(yesCheck: true)){
                saveWdoInspectionInDB(yesCheck: true)
                saveGraphAsXML()
                UpdateRateMasterID_InWO()
                validationFinlizeReport()
            }
           
    
        }
        
    }
    
    func validationproposal()  {
        if (CheckPrimaryCodeAvailableOrNot()){ //Primary Add  hai / Pura blank Hai
            
            var loader = UIAlertController()
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { (results, error) in
//                    print(results ?? "results")
//                    print("Results")
                DispatchQueue.main.async {
                    loader.dismiss(animated: false) { [self] in
                        if results != nil {
                            let xml = try? XML.parse(results as! String)
                            let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
                           // if(strcount ?? 0 > 3){
                            let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
                            
                            if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
                                goToConfigureProposalView(strForProposal: "Yes")

                            }else if(strcount == nil){
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: graphXmlParseMsg, viewcontrol: self)
                            }
                            else{
                                self.showGraphAlert(str: "Proposal", isPricing: "")
                            }
                            
                        }else{
                            self.showGraphAlert(str: "Proposal", isPricing: "")
                        }
                    }
                }
                }
            )
                
        
                

        }else{
            
            showAlertWithoutAnyAction(strtitle: "Warning", strMessage: "You Must Add A Primary Recommendation Before Proceeding.", viewcontrol: self)
        }
    }
        
    func validationFinlizeReport()  {
        
        if (CheckPrimaryCodeAvailableOrNot()){ //Primary Add  hai / Pura blank Hai
            
            // If IsPricingPending Approval true so can not move Forward
            // Show Alert
            //showAlertWithoutAnyAction(strtitle: Alert, strMessage: "You can't move forward, you have pending pricing approval.", viewcontrol: self)
            
            var loader = UIAlertController()
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            if ("\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "1" || "\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "true") {
                
                let strMessage = checkIfPricingExceed()
                
                if strMessage.count == 0 {
                    
                    graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { [self] (results, error) in
//                        print(results ?? "results")
//                        print("Results")
                        DispatchQueue.main.async {
                            loader.dismiss(animated: false) {
                                if results != nil {
                                    let xml = try? XML.parse(results as! String)
                                    let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
                                    //if(strcount ?? 0 > 3){
                                    let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
                                    
                                    if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
                                        updatePricingApproval(strTrueFalse: "false")
                                        goToFinalizeReportView()
                                        
                                    }else if(strcount == nil){
                                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: graphXmlParseMsg, viewcontrol: self)
                                    }
                                    
                                    else{
                                        showGraphAlert(str: "", isPricing: "true")
                                    }
                                    
                                }else{
                                    showGraphAlert(str: "", isPricing: "true")
                                }
                            }
                          
                            
                        }
                     
                    })
                    
                }else{
                    DispatchQueue.main.async {
                        loader.dismiss(animated: false) {
                            // Show Alert
                            let alert = UIAlertController(title: "Warning", message: strMessage, preferredStyle: .alert)
                            alert.view.tintColor = UIColor.darkGray
                            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                                
                            }))
                            
                            alert.addAction(UIAlertAction(title: "Request Approval", style: .default, handler: { [self] action in
                                
                                // Set True Is Pending Approval For Price
                                
                                let arrOfKeys = NSMutableArray()
                                let arrOfValues = NSMutableArray()
                                
                                arrOfKeys.add("isPricingApprovalPending")
                                arrOfValues.add("true")
                                
                                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                if isSuccess
                                {
                                    //Update Modify Date In Work Order DB
                                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                                    
                                }
                                
                                self.goForPriceJustificaiton()
                                
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }
                    
                
                    
                }
            }else{
                
                
                graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { [self] (results, error) in
//                    print(results ?? "results")
//                    print("Results")
                    DispatchQueue.main.async {
                        loader.dismiss(animated: false) {
                            if results != nil {
                                let xml = try? XML.parse(results as! String)
                                let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
                                //if(strcount ?? 0 > 3){
                                let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
                                
                                if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
                                    goToFinalizeReportView()
                                }
                                else if(strcount == nil){
                                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: graphXmlParseMsg, viewcontrol: self)
                                }
                                else{
                                    showGraphAlert(str: "", isPricing: "")
                                }
                            }else{
                                showGraphAlert(str: "", isPricing: "")
                            }
                        }
                    }
                })
            }
        }else{
            showAlertWithoutAnyAction(strtitle: "Warning", strMessage: "You Must Add A Primary Recommendation Before Proceeding.", viewcontrol: self)
        }
    }
    
    func showGraphAlert(str : String , isPricing : String) {
        let alert = UIAlertController(title: alertMessage, message: "Graph is blank. Do you want to restore it?", preferredStyle: .alert)
        
        let Close = UIAlertAction(title: "Close", style: .default, handler: {  action in
           

        })
        let continue_action = UIAlertAction(title: "Continue", style: .default, handler: { [self] action in
           
            if(str == "Proposal"){
                goToConfigureProposalView(strForProposal: "Yes")
            }else{
                if(isPricing == "true"){
                    updatePricingApproval(strTrueFalse: "false")
                    goToFinalizeReportView()
                }
                else{
                    goToFinalizeReportView()

                }
            }
            
         

        })
        let restore = UIAlertAction(title: "Restore", style: .default, handler: { action in
            self.goToWdoGraphDrawHistory()
            
        })
        alert.addAction(restore)
        alert.addAction(Close)
        alert.addAction(continue_action)
        alert.view.tintColor = UIColor.black
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    @IBAction func action_SegementControl(_ sender: Any) {
       
        showGraphLegend()
        switch segmentControls.selectedSegmentIndex
        {
        case 0:
            // General Descriptions
            scrollView.isScrollEnabled = true
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

            hghtConstProblemIdentificationHeader.constant = 0
            viewProblemIdentificationHeader.isHidden = true
            hghtConstGraph.constant = 0.0
            hghtConstGraphWebViewHide.constant = 0

            constProblemIdentificationHeaderUpVerticalSpace.constant = 20

            createDynamicIssueCode()
            
            viewProblemId.removeFromSuperview()
            viewGeneralNotes.removeFromSuperview()

            constantForSegement()

            saveGraphAsXML()
            
            saveWdoInspectionInDB(yesCheck: false)
            
            fetchNotes()

            fetchDisclaimer()
            
            graphDrawingWebView.isHidden = true
            
        case 1:
            
            self.view.isUserInteractionEnabled = false

            // Graph Diagram
            hghtConstWdoInspectionViewHeader.constant = 0
            hghtConstViewContainerWDOInspection.constant = 0
            viewWdoInspectionHeader.isHidden = true
            
            hghtConstDisclaimerHeader.constant = 0
            viewDisclaimerHeader.isHidden = true
            hghtConstTblViewDisclaimer.constant = 0.0
            
            viewProblemIdentificationHeader.isHidden = true
            hghtConstProblemIdentificationHeader.constant = 0
            showProblemIdentification()
            hghtConstGraphWebViewHide.constant = 0

            constProblemIdentificationHeaderUpVerticalSpace.constant = -20-CGFloat(210*self.arrOfDiclaimer.count)
            
            scrollView.isScrollEnabled = false
            self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
            
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            viewGeneralNotes.removeFromSuperview()
            viewProblemId.removeFromSuperview()

            saveGraphAsXML()
            
            saveWdoInspectionInDB(yesCheck: false)
            
            fetchNotes()
            
            fetchDisclaimer()

            self.view.isUserInteractionEnabled = true
            
            graphDrawingWebView.isHidden = false
            showGraphLegend()
        case 2:
            // FINDINGS
            hghtConstWdoInspectionViewHeader.constant = 0
            hghtConstViewContainerWDOInspection.constant = 0
            viewWdoInspectionHeader.isHidden = true
            
            hghtConstProblemIdentificationHeader.constant = 0
            viewProblemIdentificationHeader.isHidden = true
            hghtConstGraph.constant = 0.0
            hghtConstGraphWebViewHide.constant = 0
            
            hghtConstDisclaimerHeader.constant = 0
            viewDisclaimerHeader.isHidden = true
            hghtConstTblViewDisclaimer.constant = 0.0
            
            constProblemIdentificationHeaderUpVerticalSpace.constant = -20
            
            scrollView.isScrollEnabled = true
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            viewProblemId.removeFromSuperview()
            viewGeneralNotes.removeFromSuperview()

            saveGraphAsXML()
            
            saveWdoInspectionInDB(yesCheck: false)
            
            let framee = CGRect(x: 0, y: segmentControls.frame.maxY, width: UIScreen.main.bounds.size.width, height: self.view_Footer.frame.origin.y - (segmentControls.frame.maxY))

            viewProblemId.frame = framee
            
           // hghtConstTblViewProblemId.constant = framee.height-80
            
            self.view.addSubview(viewProblemId)
            
            fetchDisclaimer()
            
            graphDrawingWebView.isHidden = true
            
        case 3:
            // Notes
            hghtConstWdoInspectionViewHeader.constant = 0
            hghtConstViewContainerWDOInspection.constant = 0
            viewWdoInspectionHeader.isHidden = true
            
            hghtConstProblemIdentificationHeader.constant = 0
            viewProblemIdentificationHeader.isHidden = true
            hghtConstGraph.constant = 0.0
            hghtConstGraphWebViewHide.constant = 0
            
            hghtConstDisclaimerHeader.constant = 0
            viewDisclaimerHeader.isHidden = true
            hghtConstTblViewDisclaimer.constant = 0.0
            
            constProblemIdentificationHeaderUpVerticalSpace.constant = -20
            
            scrollView.isScrollEnabled = true
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            viewProblemId.removeFromSuperview()
            
            saveGraphAsXML()
            
            saveWdoInspectionInDB(yesCheck: false)
            
            let framee = CGRect(x: 0, y: segmentControls.frame.maxY, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-250)
            
            viewGeneralNotes.frame = framee
            
            self.view.addSubview(viewGeneralNotes)
            
            fetchNotes()
            
            fetchDisclaimer()
            
            graphDrawingWebView.isHidden = true
            
        default:
            break
        }
    }
    
    
    @IBAction func action_submit(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            goToConfigureProposalView(strForProposal: "No")
            
        }else{
            
            saveWdoInspectionInDB(yesCheck: true)
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: InfoDataSaved, viewcontrol: self)
            
        }
        
    }
    
    
    @IBAction func action_HideGraphWebView(_ sender: UIButton) {
        
        /*
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
            scrollView.isScrollEnabled = false
            loadWebGraphView()
            hghtConstGraphWebViewHide.constant = 60
         
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            
            scrollView.isScrollEnabled = true
            self.hghtConstGraph.constant = 0
            hghtConstGraphWebViewHide.constant = 60
            saveGraphAsXML()

        }
        */
        
        saveGraphAsXML()
        
        let framee = CGRect(x: 0, y: segmentControls.frame.maxY, width: UIScreen.main.bounds.size.width, height: self.view_Footer.frame.origin.y - (segmentControls.frame.maxY))

        viewProblemId.frame = framee
        
       // hghtConstTblViewProblemId.constant = framee.height-50
        
        self.view.addSubview(viewProblemId)
        
    }
    
    
    @IBAction func action_CloseProblemIdList(_ sender: UIButton) {
        
        viewProblemId.removeFromSuperview()
        
    }
    @IBAction func action_Garage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        var arrayData = NSMutableArray()
        
        if  nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
            let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as!  NSDictionary
            if((dictMaster.value(forKey: "Garages") is NSArray)){
                arrayData = (dictMaster.value(forKey: "Garages") as! NSArray).mutableCopy() as! NSMutableArray
                if arrayData.count > 0 {
                    arrayData = addSelectInArray(strName: "Text", array: arrayData)
                }
            }
            
           
            let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
            let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            vc.strTitle = "Select"
            vc.strTag = 68
            if arrayData.count != 0{
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.handelDataSelectionTable = self
                vc.aryTBL = arrayData
                self.present(vc, animated: false, completion: {})
            }else{
                Global().displayAlertController(Alert, NoDataAvailableee, self)
            }
        }
        
    }
    @IBAction func action_FollowUpInspection(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "FollowUpInspectionExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            ary = addSelectInArray(strName: "Title", array: ary)
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    
    @IBAction func action_AddGeneralNotes(_ sender: UIButton) {
        
        goToGeneralAddView(strToUpdate: "No", strGeneralNoteId: "")
        
    }
    
    //:-------------------------------------------------------------------------------------------------------------------------
    // MARK: ----------------------------------------Wdo Inspection Button Actions------------------------------------------
    //:-------------------------------------------------------------------------------------------------------------------------
    
    @IBAction func action_SubAreaAccess(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "SubAreaAccesss", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_NoOfStories(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "NumberofStories", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_ExternalMaterials(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "ExteriorMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_RoofingMaterial(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RoofMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_CpCTagPosted(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CpcTagLocateds", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_PastPcoTagPosted(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "OtherPcoTags", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    @IBAction func action_StructureDescription(_ sender: UIButton) {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "StructuredDescriptions", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        if array.count > 0 {
            
            array = addSelectInArrayNew(strName: "Text", array: array , strNameNew: "Value")
            
        }
        
        self.goToPopUpView(ary: array, tagBtn: sender.tag)
        
    }
    
    //:-------------------------------------------------------------------------------------------------------------------------
    // MARK: ----------------------------------------Footer Button Actions------------------------------------------
    //:-------------------------------------------------------------------------------------------------------------------------
    
    @IBAction func actionOnServiceDoc(_ sender: Any)
    {
        saveWdoInspectionInDB(yesCheck: false)

        goToServiceDocument()
    }
    
    @IBAction func actionOnClock(_ sender: Any)
    {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            saveWdoInspectionInDB(yesCheck: false)

            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        //deleteAllRecordsFromDB(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@", self.strWoId))
        
        saveWdoInspectionInDB(yesCheck: false)

        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            
            let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction(title: "Restore Graph", style: .default, handler: { [self] action in
                
                self.view.endEditing(true)
                
                saveWdoInspectionInDB(yesCheck: false)

                //self.goToGlobalmage(strType: "Graph")
                self.goToWdoGraphDrawHistory()
                
            }))
            alert.addAction(UIAlertAction(title: "Service History", style: .default, handler: { action in
            
                self.view.endEditing(true)
                
                self.saveWdoInspectionInDB(yesCheck: false)

                self.goToServiceHistory()
                
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
                
            }))
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender
               // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = [.up]
            }
            self.present(alert, animated: true)
            
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Restore Graph", style: .default , handler:{ (UIAlertAction)in
                
                self.view.endEditing(true)
                
                self.saveWdoInspectionInDB(yesCheck: false)

                //self.goToGlobalmage(strType: "Graph")
                self.goToWdoGraphDrawHistory()

            }))
            
            
            alert.addAction(UIAlertAction(title: "Service History", style: .default , handler:{ (UIAlertAction)in

                self.view.endEditing(true)
                
                self.saveWdoInspectionInDB(yesCheck: false)

                self.goToServiceHistory()
                
            }))
            
            
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            
            alert.popoverPresentationController?.sourceView = sender
            
            self.present(alert, animated: true, completion: {
            })
        }
               
      }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        saveWdoInspectionInDB(yesCheck: false)

        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        saveWdoInspectionInDB(yesCheck: false)

        self.goToGlobalmage(strType: "Graph")
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        saveWdoInspectionInDB(yesCheck: false)

        self.goToNotesHistory()
        
    }
    
    //:---------------------------------------------------------------------------------------------------------
    // MARK: ----------------------------------------Functions------------------------------------------
    //:---------------------------------------------------------------------------------------------------------
    func generateGeneralDesc() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { [self] in
            if !isGeneralDescriptionPrepare {
                var strDes = ""
                if ("\(btnNoOfStories.titleLabel?.text ?? "")" != strSelectString) && "\(btnNoOfStories.titleLabel?.text ?? "")".lowercased() != "other"{
                        strDes = strDes + "\(btnNoOfStories.titleLabel?.text ?? "") Story, "
                }else{
                    if txtNumberOfStories.isHidden == false && txtNumberOfStories.text != "" {
                        strDes = strDes + "\(txtNumberOfStories.text ?? "") Story, "
                    }
                }
                
                if "\(txtviewStructureDescription.text ?? "")" != "" && "\(txtviewStructureDescription.text ?? "")".lowercased() != "other"{

                    strDes =  "\(strDes)" + "\(txtviewStructureDescription.text ?? ""), "
                }
                else{
                    if txtFldOther.isHidden == false && txtFldOther.text != "" {
                        strDes =  "\(strDes)" + "\(txtFldOther.text ?? ""), "
                    }
                }
                if "\(txtYearOfStructure.text ?? "")" != ""{
                    strDes =  "\(strDes)" + "year built: \(txtYearOfStructure.text ?? ""), "
                }
                if "\(strOccupiedVacant)" != ""{
                    if "\(strOccupiedVacant)" == "1" {
                        strDes =  "\(strDes)" + "Occupied, "
                    }else if ("\(strOccupiedVacant)" == "2"){
                        strDes =  "\(strDes)" + "Vacant, "
                    }
                }
                
                if "\(strFurnishedUFurnished)" != ""{
                    if "\(strFurnishedUFurnished)" == "1" {
                        strDes =  "\(strDes)" + "Unfurnished, "
                    }else if ("\(strFurnishedUFurnished)" == "2"){
                        strDes =  "\(strDes)" + "Furnished, "
                    }
                }
                
                if "\(strSlabRaised)" != ""{
                    if "\(strSlabRaised)" == "1" {
                        strDes =  "\(strDes)" + "Slab, "
                    }else if ("\(strSlabRaised)" == "2"){
                        strDes =  "\(strDes)" + "Raised, "
                    }else if ("\(strSlabRaised)" == "3"){
                        strDes =  "\(strDes)" + "Raised/Slab, "
                    }
                }
                if "\(btn_Garage.titleLabel?.text ?? "")" != strSelectString{
                    strDes =  "\(strDes)" + "foundation with an \(btn_Garage.titleLabel?.text ?? ""), "
                }
                
                var strstaticstring = ""
                if "\(btnExternalMaterial.titleLabel?.text ?? "")" != strSelectString  && "\(btnExternalMaterial.titleLabel?.text ?? "")".lowercased() != "other" {
                    strstaticstring = "and a "
                        strDes =  "\(strDes)" + "\(btnExternalMaterial.titleLabel?.text ?? "") siding"
                        }
                else{
                    if txtExternalMaterial.isHidden == false && txtExternalMaterial.text != "" {
                        strstaticstring = "and a "

                        strDes =  "\(strDes)" + "\(txtExternalMaterial.text ?? "") siding"

                    }
                }
                
                if "\(btnRoofingMaterial.titleLabel?.text ?? "")" != strSelectString && "\(btnRoofingMaterial.titleLabel?.text ?? "")".lowercased() != "other" {
                        strDes =  "\(strDes) \(strstaticstring)" + "\(btnRoofingMaterial.titleLabel?.text ?? "") roof"

                }else{
                    if txtRoofingMaterial.isHidden == false && txtRoofingMaterial.text != "" {
                        strDes =  "\(strDes) \(strstaticstring)" + "\(txtRoofingMaterial.text ?? "") roof"

                    }
                }
                var strdeccheck = strDes.trimmed.suffix(1)
                if strdeccheck == "," {
                    strDes = String(strDes.trimmed.dropLast())

                }
                strdeccheck = strDes.trimmed.suffix(2)
                if strdeccheck == ",." {
                    strDes = String(strDes.trimmed.dropLast())
                    strDes = String(strDes.trimmed.dropLast())
                }
                txtviewGenralDescription.text = strDes  + "."
            }
        })

    }
    
    
    func updatePricingApproval(strTrueFalse : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isPricingApprovalPending")
        arrOfValues.add(strTrueFalse)
        
        arrOfKeys.add("isPricingApprovalMailSend")
        arrOfValues.add("false")
        
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifyDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if isSuccess
        {
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
        }
        
    }
    
    func goForPriceJustificaiton()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDOiPad" : "WDOiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "PriceJustification_WDO_VC") as? PriceJustification_WDO_VC
        vc?.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    func GetCodeMasterRecommendation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "CodeMasterRecommendation") != nil){
                return (dictMaster.value(forKey: "CodeMasterRecommendation") as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        return NSMutableArray()
    }
    
    
    func CheckPrimaryCodeAvailableOrNot() -> Bool {
        let aryCodeMasterRecommendation = GetCodeMasterRecommendation()
        let aryPro_Identification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)))
        if aryPro_Identification.count == 0 {
            return true
        }
        
        
        let aryCodeMasterExtSerDcs = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        let aryProblem = NSMutableArray()
        for item in aryPro_Identification {
            let dict = (item as! NSManagedObject)
            let recommendationCodeId = "\(dict.value(forKey: "recommendationCodeId") ?? "")"

            for item in aryCodeMasterExtSerDcs {
                let codeMasterId = "\((item as! NSDictionary).value(forKey: "CodeMasterId") ?? "")"
                if(recommendationCodeId == codeMasterId){
                    
                    if (item as! NSDictionary).value(forKey: "IsPrimaryNotSelected") is Bool {
                        dict.setValue((item as! NSDictionary).value(forKey: "IsPrimaryNotSelected") as! Bool, forKey: "isPrimaryNotSelected")
                        dict.setValue((item as! NSDictionary).value(forKey: "IsSecondary") as! Bool, forKey: "isSecondary")
                        dict.setValue((item as! NSDictionary).value(forKey: "IsPrimary") as! Bool, forKey: "isPrimary")

                        aryProblem.add(dict)
                    } else {
                        dict.setValue(false, forKey: "isPrimaryNotSelected")
                        aryProblem.add(dict)
                    }
                    
               
                    
                 
                }
            }
            
            
        }
        
        
        let arySecondaryProblemIdentification = aryProblem.filter { (task) -> Bool in
            return ("\((task as! NSManagedObject).value(forKey: "isSecondary")!)" == "1"  || "\((task as! NSManagedObject).value(forKey: "isSecondary")!)".lowercased() == "true") && ("\((task as! NSManagedObject).value(forKey: "isPrimaryNotSelected")!)" == "0"  || "\((task as! NSManagedObject).value(forKey: "isPrimaryNotSelected")!)".lowercased() == "false")}
        
        
      //  isPrimaryNotSelected == true ata hai  primary ka check htana hai
        
            for item in arySecondaryProblemIdentification {
                let subsectionCodeId = "\((item as! NSManagedObject).value(forKey: "recommendationCodeId")!)"
                let aryPrimaryCode = aryCodeMasterRecommendation.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "CodeMasterId")!)".contains(subsectionCodeId))
                }
              
                var tagTemp = 0
                for itemCode in aryPrimaryCode {
                    let primaryCode = "\((itemCode as! NSDictionary).value(forKey: "PrimaryCodeMasterId")!)"
                    for itemproblem in aryProblem {
                        let recommendationCodeId = "\((itemproblem as! NSManagedObject).value(forKey: "recommendationCodeId")!)"
                        if(recommendationCodeId.lowercased() == primaryCode.lowercased()){
                            tagTemp = 1
                        }
                    }
                  
                }
                
                
                if(tagTemp == 1){
                  return true
                }else{
                    return false

                }
        }
        return true
    }
    
    func CheckPrimaryCodeAvailableOrNotOld() -> Bool {
        let aryCodeMasterRecommendation = GetCodeMasterRecommendation()
        let aryPro_Identification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)))
        
        if aryPro_Identification.count == 0 {
            return true
        }
        
        let arySecondaryProblemIdentification = aryPro_Identification.filter { (task) -> Bool in
            return ("\((task as! NSManagedObject).value(forKey: "isSecondary")!)" == "1"  || "\((task as! NSManagedObject).value(forKey: "isSecondary")!)".lowercased() == "true")}
            for item in arySecondaryProblemIdentification {
                let subsectionCodeId = "\((item as! NSManagedObject).value(forKey: "subsectionCodeId")!)"
                let aryPrimaryCode = aryCodeMasterRecommendation.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "CodeMasterId")!)".contains(subsectionCodeId))
                }
                for itemCode in aryPrimaryCode {
                    let primaryCode = "\((itemCode as! NSDictionary).value(forKey: "PrimaryCodeMasterId")!)"
                    let aryPrimaryCode_Temp = aryPro_Identification.filter { (task) -> Bool in
                        return "\((task as! NSManagedObject).value(forKey: "subsectionCodeId")!)".contains(primaryCode) }
                    if(aryPrimaryCode_Temp.count == 0){
                        return false
                    }
                }
        }
        return true
    }
    
    func deleteWdoGraphDrawHistoryRecordsFromDB() {
        
        // Delete reccords after getting count from setting
        
        let recordCount = "\(nsud.value(forKey: "WdoGraphDrawHistoryRecordsCount") ?? "")"
        
        var recordCountInt = 10
        
        if recordCount.count > 0 {
            
            recordCountInt = Int(recordCount)!
            
        }
                
        let context = getContext()
        
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WdoGraphDrawHistory")

        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "localCreatedDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let predicateL = NSPredicate(format: "workorderId == %@", strWoId)
        fetchRequest.predicate = predicateL

        do {
            
            let records = try context.fetch(fetchRequest) as! [NSManagedObject]

            for k in 0 ..< records.count {
                
                if k < recordCountInt {
                    
                }else{
                    
                    let recordObjL = records[k]
                    
                    // Delete data from Document Directory
                    
                    let xmlImageName = "\(recordObjL.value(forKey: "woImagePath") ?? "")"
                    
                    Global().removeImage(fromDocumentDirectory: xmlImageName)
                    
                    let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")

                    Global().removeImage(fromDocumentDirectory: strImageNameSales)
                    
                    context.delete(recordObjL)

                }
                
            }

        } catch {
            print(error)
        }

        
    }
    
    func addDefaultDisclaimer() {
        
        // delete before adding
        
        //deleteAllRecordsFromDB(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && isDefault == %@", self.strWoId, "1"))
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        if ary.count != 0{
            
            for item in ary {
                
                let dictData = item as! NSDictionary
                
                let isDeafultt = (dictData.value(forKey: "IsDefault") as! Bool)
                
                if isDeafultt {
                    
                    // Check if Already present in DB
                    
                    let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && disclaimerMasterId == %@", strWoId , "\(dictData.value(forKey: "DisclaimerMasterId")!)"))
                    
                    if arrOfDiclaimer.count > 0 {
                        
                        
                        
                    }else{
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workOrderId")
                        arrOfKeys.add("isActive")
                        
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        arrOfValues.add(true)
                        
                        
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifiedDate")
                        
                        arrOfKeys.add("disclaimerMasterId")
                        arrOfKeys.add("disclaimerDescription")
                        arrOfKeys.add("disclaimerName")
                        arrOfKeys.add("isChanged")
                        arrOfKeys.add("woWdoDisclaimerId")
                        arrOfKeys.add("fillIn")
                        arrOfKeys.add("isDefault")
                        arrOfKeys.add("isChanged")

                        arrOfValues.add(Global().getEmployeeId())
                        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                        arrOfValues.add(Global().getEmployeeId())
                        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                        
                        arrOfValues.add("\(dictData.value(forKey: "DisclaimerMasterId")!)")
                        arrOfValues.add("\(dictData.value(forKey: "Disclaimer")!)")
                        
                        arrOfValues.add("\(dictData.value(forKey: "Title")!)")
                        arrOfValues.add(false)
                        
                        arrOfValues.add(Global().getReferenceNumber())
                        arrOfValues.add("")
                        arrOfValues.add(true)
                        arrOfValues.add(false)

                        // Saving Disclaimer In DB
                        saveDataInDB(strEntity: Entity_Disclaimer, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                    }

                }
                
            }
            
        }
        
    }
    
    func goToPopUpView(ary : NSMutableArray , tagBtn : Int) {
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tagBtn
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    func setProblemViewOnEdit() {
        
        hghtConstWdoInspectionViewHeader.constant = 0
        hghtConstViewContainerWDOInspection.constant = 0
        viewWdoInspectionHeader.isHidden = true
        
        viewProblemIdentificationHeader.isHidden = true
        hghtConstProblemIdentificationHeader.constant = 0
        //showProblemIdentification()
        //hghtConstAddProblemHeader.constant = 62.0
        hghtConstGraphWebViewHide.constant = 60
        
        hghtConstDisclaimerHeader.constant = 0
        viewDisclaimerHeader.isHidden = true
        hghtConstTblViewDisclaimer.constant = 0.0
        
        constProblemIdentificationHeaderUpVerticalSpace.constant = -20
        
        scrollView.isScrollEnabled = false
        self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
        
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        
        //btnShowHideGraphWebView.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
        
        scrollView.isScrollEnabled = true
        self.hghtConstGraph.constant = 0
        self.graphDrawingWebView.isHidden = true
        hghtConstGraphWebViewHide.constant = 60
        
        self.btnShowHideProblemIdentification.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
        
    }
    
    func saveGraphAsXML() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {

            // No Need to Edit
            
        }else{
          
            graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { [self] (results, error) in
//                print(results ?? "results")
//                print("Results")
//
                if results != nil {
                    
                 //   let
                    
                    // parse xml document
                    let xml = try? XML.parse(results as! String)
                    let strcount =  xml?.mxGraphModel.root.mxCell.all?.count
                    let strBackGroundImage = xml?.mxGraphModel.attributes["backgroundImage"] ?? ""
                    
                    if(strcount ??  0 > 3 || strBackGroundImage.count > 3){
                        
                        Global().updateSalesModifydate(self.strWoLeadId as String)
                        
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                        
                        self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
                        
                      
                    }else{
                        if(strcount == nil){
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: graphXmlParseMsg, viewcontrol: self)
                        }
                     //
                    }
                   
                }
                //self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
                
            })
            
        }
    }
 
    
 
     
    func loadWebGraphView() {
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

        self.graphDrawingWebView.scrollView.bounces = false
        self.graphDrawingWebView.allowsBackForwardNavigationGestures = false
        
        //let xmlFileName = (self.strWoId as String) + ".xml"
        var isXmlFileExists = false
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
                
            isXmlFileExists = checkIfImageExistAtPath(strFileName: xmlFileName)

        }
                
        let bundle = Bundle.main
        
        let filename = "index"
        let fileExtension = "html"
        let directory = "graphing"
        
        let isGraphDebugEnabled = nsud.bool(forKey: "graphDebug")
        
        if isGraphDebugEnabled {
            
            // general catermite
            
            let url = URL(string: "https://graphing.z30.web.core.windows.net/index.html?selectedCategory=catermite")!
            
            //https://graphing.z30.web.core.windows.net/index.html
            //
            
            self.graphDrawingWebView.load(URLRequest(url: url))
            
        } else {
            
            let indexUrl = bundle.url(forResource: filename, withExtension: fileExtension, subdirectory: directory)
            
            let fullUrl = URL(string: "?selectedCategory=catermite", relativeTo: indexUrl)
            let request = URLRequest(url: fullUrl!)
            self.graphDrawingWebView.load(request)
            
        }
        
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

        //self.graphDrawingWebView.scrollView.isScrollEnabled = false
        
        self.view.layoutIfNeeded()
        
        if isXmlFileExists {
            
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
            
            self.loadSavedXMLGraph()
            
        }else{
            
            //RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)

            //loadGraphCategory()
            
        }
        
    }
    

    
    
    func setInitalSegement() {
        
        hghtConstWdoInspectionViewHeader.constant = 60
        hghtConstViewContainerWDOInspection.constant = 1800
        viewWdoInspectionHeader.isHidden = false
        
        hghtConstProblemIdentificationHeader.constant = 0
        viewProblemIdentificationHeader.isHidden = true
        hghtConstGraph.constant = 0.0
        
        segmentControls.selectedSegmentIndex = 0
        
        let font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 16)
        segmentControls.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        
        btnShowHideWDOInspection.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
        btnShowHideDisclaimer.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
        btnShowHideProblemIdentification.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
        
        
        hghtConstDisclaimerHeader.constant = 60
        viewDisclaimerHeader.isHidden = false
        hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)
        btnShowHideDisclaimer.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
                
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: hghtConstTblViewDisclaimer.constant + hghtConstDisclaimerHeader.constant + hghtConstWdoInspectionViewHeader.constant + hghtConstViewContainerWDOInspection.constant + 150)
        
        
        
        //Nilind
        
        graphDrawingWebView.isHidden = true
    }
    
    func showProblemIdentification() {
        
            //self.hghtConstGraph.constant = 576
            //self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
        
            loadWebGraphView()
        
    }
    
    func disableControls() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            
            btnEditWdoInspection.isEnabled = false
            
            btnCompleteReport.isEnabled = false
            btnLimitedResport.isEnabled = false
            btnSupplementalReport.isEnabled = false
            btnReinspectionReport.isEnabled = false
            
            txtSubareaAccess.isEnabled = false
            txtNumberOfStories.isEnabled = false
            txtExternalMaterial.isEnabled = false
            txtRoofingMaterial.isEnabled = false
            txtYearOfStructure.isEnabled = false
            
            btnYearUnknown.isEnabled = false
            btnNo_BuildingPermit.isEnabled = false
            btnYes_BuildingPermit.isEnabled = false
            
            txtCPCInspTagPosted.isEnabled = false
            txtPastPcoTagPosted.isEnabled = false
            txtNameOfPrevPco.isEnabled = false
            txtDateOfPrevPco.isEnabled = false
            
            txtviewStructureDescription.isEditable = false
            txtviewGenralDescription.isEditable = false

            txtviewComment.isEditable = false
            
//            btnSubterranianTermites.isEnabled = false
//            btnDrywoodTermited.isEnabled = false
//            btnFungus.isEnabled = false
//            btnOtherFindings.isEnabled = false
//            btnFurtherInspection.isEnabled = false
            
            btnOccupied.isEnabled = false
            btnVacant.isEnabled = false
            btnUnfurnished.isEnabled = false
            btnFurnished.isEnabled = false
            btnSlab.isEnabled = false
            btnRaised.isEnabled = false
            btnRaisedSlab.isEnabled = false
            
            btnNo_SeparateReport.isEnabled = false
            btnYes_SeparateReport.isEnabled = false
            
            //btnYes_TipWarranty.isEnabled = false
            //btnNoTipWarranty.isEnabled = false
            btnTipWarrantyType.isEnabled = false
            btnFollowUpInspection.isEnabled = false
            btn_Garage.isEnabled = false
            txtMonthlyAmount.isEnabled = false
            
            btnAddProblemIdentification.isEnabled = false
            btnAddDisclaimer.isEnabled = false
            
            btnRoofingMaterial.isEnabled = false
            btnStructureDescription.isEnabled = false
            btnSubAreaAccess.isEnabled = false
            btnNoOfStories.isEnabled = false
            btnExternalMaterial.isEnabled = false
            btnCpcTagPosted.isEnabled = false
            btnPastPcoTagPosted.isEnabled = false
            btnAddGeneralNotes.isEnabled = false
            
            graphDrawingWebView.isUserInteractionEnabled =  false
            
            

        }else{
            
        }
        
    }

    
    @objc func selectDatePrevPCO(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        
        if (txtDateOfPrevPco.text?.count)! > 0 {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let fromDate = dateFormatter.date(from:(txtDateOfPrevPco.text)!)!
            
            gotoDatePickerView(sender: textField, strType: "Date" , dateToSet: fromDate)
            
        }else{
            
            gotoDatePickerView(sender: textField, strType: "Date" , dateToSet: Date())
            
            
        }
        
    }
    
    @objc func selectYearStructure(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        //gotoDatePickerView(sender: textField, strType: "Date")
        
        
    }
    
    func gotoDatePickerView(sender: UITextField, strType:String, dateToSet:Date )  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
    
    func goToProblemIdentificationAddView(strToUpdate : String , strProblemIdentificationIdLocal : String , strMobileProblemIdentificationIdLocal : String)  {
        
        saveGraphAsXML()
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddProblemIdentification_WDOVC") as? AddProblemIdentification_WDOVC
        testController?.strWoId = strWoId
        testController?.strToUpdate = strToUpdate
        testController?.strProblemIdentificationIdToUpdate = strProblemIdentificationIdLocal
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strMobileProblemIdentificationIdToUpdate = strMobileProblemIdentificationIdLocal
        
        if isSeparateReport {
            
            testController?.showInfestation = true
            
        }else{
            
            testController?.showInfestation = false
            
        }
        
        if txtYearOfStructure.text!.count > 0 {
            
            let yearStructure = Int(txtYearOfStructure.text!)
            
            if yearStructure! <= 1977 {
                
                testController?.showLeadTest = true
                
            }else{
                
                testController?.showLeadTest = false
                
            }
            
        }else{
            
            testController?.showLeadTest = false
            
        }
        
        if isYearUnknown {
            
            testController?.showLeadTest = false
            
        }
        
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToDisclaimerAddView(strToUpdate : String , strDisclaimerId : String)  {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddDisclaimerVC") as? AddDisclaimerVC
        testController?.strWoId = strWoId as String
        testController?.strToUpdate = strToUpdate
        testController?.strDisclaimerIdToUpdate = strDisclaimerId
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToGeneralAddView(strToUpdate : String , strGeneralNoteId : String)  {
        
        saveGraphAsXML()
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "AddGeneralNotesVC") as? AddGeneralNotesVC
        testController?.strWoId = strWoId as String
        testController?.strToUpdate = strToUpdate
        testController?.strGeneralNotesIdToUpdate = strGeneralNoteId
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToEditInspectionView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "Edit_WPOInspectionVC") as? Edit_WPOInspectionVC
        testController?.strWoId = strWoId
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToConfigureProposalView(strForProposal : String)  {
        
        if strWoLeadId.count > 0 && strWoLeadId != "0" {
            
            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "ConfigureProposal_WDOVC") as? ConfigureProposal_WDOVC
            testController?.strWoId = strWoId
            testController?.strLeadId = strWoLeadId
            testController?.strForProposal = strForProposal
            testController?.objWorkorderDetail = objWorkorderDetail
            self.navigationController?.pushViewController(testController!, animated: false)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Opportunity not available", viewcontrol: self)
            
        }
        
    }
    
    func graphValidationCheck_AndGotoFinalizeReport() {
      
    }
    
    
    func goToFinalizeReportView()  {
            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "FinalizeReport_WDOVC") as? FinalizeReport_WDOVC
        testController?.strWoId = strWoId
        testController?.strWdoLeadId = strWoLeadId as NSString
        self.navigationController?.pushViewController(testController!, animated: false)
        
        
    }
    
    func goToAgreementView()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "Agreement_WDOVC") as? Agreement_WDOVC
        testController?.strWoId = strWoId
        testController?.strLeadId = strWoLeadId
        self.navigationController?.pushViewController(testController!, animated: false)
        
    }
    
    func setUpInitialConstanOfViews()
    {
        
        hghtConstViewContainerWDOInspection.constant = 0.0
        //hghtConstTblViewProblemIdentification.constant = 0.0
        hghtConstTblViewDisclaimer.constant = 0.0
        hghtConstGraph.constant = 0.0
        
    }
    
    func configureUI()
    {
        
        makeCornerRadius(value: 2.0, view: txtviewStructureDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewGenralDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewComment, borderWidth: 1.0, borderColor: UIColor.lightGray)
        //makeCornerRadius(value: 2.0, view: btnProposal, borderWidth: 0, borderColor: UIColor.lightGray)
        //makeCornerRadius(value: 2.0, view: btnFinalizeReport, borderWidth: 0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnTipWarrantyType, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnFollowUpInspection, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btn_Garage, borderWidth: 1.0, borderColor: UIColor.lightGray)

        
        //buttonRound(sender: btnSubmit)
        buttonRound(sender: btnFinalizeReport)
        buttonRound(sender: btnProposal)
        
        setColorBorderForView(item: btnSubAreaAccess)
        setColorBorderForView(item: btnNoOfStories)
        setColorBorderForView(item: btnExternalMaterial)
        setColorBorderForView(item: btnRoofingMaterial)
        setColorBorderForView(item: btnCpcTagPosted)
        setColorBorderForView(item: btnPastPcoTagPosted)

        txtviewGenralDescription.delegate = self
    }
    
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func fetchNotes() {
        
        // fetch General Notes
        
        let sort1 = NSSortDescriptor(key: "title", ascending: true)
        
        arrOfGeneralNotes = getDataFromCoreDataBaseArraySorted(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort1)
        tblViewGeneralNotes.reloadData()
        
    }
    
    func fetchDisclaimer() {
        
        let tempArrayDiscalimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId))
        
        arrOfDiclaimer = NSMutableArray()

        arrOfDiclaimer = (tempArrayDiscalimer ).mutableCopy() as! NSMutableArray

        if arrOfDiclaimer.count > 0 {
            
            self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)

            tblviewDisclaimer.reloadData()
            
        }
        
        if self.arrOfDiclaimer.count < 1 {
            
            self.btnShowHideDisclaimer.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            self.hghtConstTblViewDisclaimer.constant = 0.0
            
        }
        
    }
    
    func fetchDataFrmDB() {
        
        let tempArrayDiscalimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId))
        
        arrOfDiclaimer = NSMutableArray()
        
        arrOfDiclaimer = (tempArrayDiscalimer ).mutableCopy() as! NSMutableArray
        
            
            self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)

            tblviewDisclaimer.reloadData()
            
        
        
        let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
        
        arrOfProblemIdentification = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort)
                
        if arrOfProblemIdentification.count > 0 {
            
            //tblviewProblemIdentification.reloadData()
            
            let arrTemp = NSMutableArray()
            
            arrTemp.addObjects(from: arrOfProblemIdentification as! [Any])
            arrTemp.add("")
            //arrTemp.add("")

            arrOfProblemIdentification = arrTemp
            tblviewProblemIdentification.reloadData()
        }else{
            tblviewProblemIdentification.reloadData()
        }
      
        
        if self.arrOfProblemIdentification.count < 1 {
            
            self.btnShowHideProblemIdentification.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            //self.hghtConstTblViewProblemIdentification.constant = 0.0
            //self.hghtConstGraph.constant = 0.0
            
        }
        if self.arrOfDiclaimer.count < 1 {
            
            self.btnShowHideDisclaimer.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            self.hghtConstTblViewDisclaimer.constant = 0.0
            
        }
        
        constantForSegement()
        
        if segmentControls.selectedSegmentIndex == 1 {
            
            hghtConstGraph.constant = 576
            self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
            graphDrawingWebView.isHidden = false
            
        }
        
        // fetch General Notes
        
        let sort1 = NSSortDescriptor(key: "title", ascending: true)
        
        arrOfGeneralNotes = getDataFromCoreDataBaseArraySorted(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort1)
        
        tblViewGeneralNotes.reloadData()
        
    }
    
    
    func saveWdoInspectionInDB(yesCheck : Bool) {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {

            // no Need to Edit
            
        }else{
            
            if validationForAddWdoInspection(yesCheck: yesCheck) {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("isActive")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strWoId)
                arrOfValues.add(true)
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                
                arrOfKeys.add("comments")
                arrOfKeys.add("cpcInspTagPosted")
                arrOfKeys.add("dateOfPrevPco")
                arrOfKeys.add("externalMaterial")
                arrOfKeys.add("furnishedUnfurnished")
                arrOfKeys.add("generalDescription")
                arrOfKeys.add("isBuildingPermit")
                arrOfKeys.add("isSeparateReport")
                arrOfKeys.add("isYearUnknown")
                arrOfKeys.add("monthlyAmount")
                arrOfKeys.add("nameOfPrevPco")
                arrOfKeys.add("numberOfStories")
                arrOfKeys.add("occupiedVacant")
                arrOfKeys.add("reportTypeId")
                arrOfKeys.add("reportTypeName")
                arrOfKeys.add("reportSysName")
                arrOfKeys.add("roofingMaterial")
                arrOfKeys.add("slabRaised")
                arrOfKeys.add("structureDescription")
                arrOfKeys.add("subareaAccess")
                arrOfKeys.add("tIPMasterId")
                arrOfKeys.add("followUpInspectionId")
                arrOfKeys.add("garage")

                arrOfKeys.add("tIPMasterName")
                arrOfKeys.add("yearOfStructure")
                arrOfKeys.add("isTipWarranty")
                arrOfKeys.add("pastPcoTagPosted") //checkBox
                arrOfKeys.add("wdoTermiteIssueCodeSysNames")
                arrOfKeys.add("reportTypeSysName")
                

                arrOfValues.add(txtviewComment.text!)
                //arrOfValues.add(txtCPCInspTagPosted.text!)
                if dictOfCpcTagPosted.count > 0 {
                    
                    arrOfValues.add("\(dictOfCpcTagPosted.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(txtDateOfPrevPco.text!)
                //arrOfValues.add(txtExternalMaterial.text!)
                if dictOfExternalMaterials.count > 0 {
                    
                    arrOfValues.add("\(dictOfExternalMaterials.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(strFurnishedUFurnished)
                arrOfValues.add(txtviewGenralDescription.text!)
                arrOfValues.add(isBuildingPermit)
                arrOfValues.add(isSeparateReport)
                arrOfValues.add(isYearUnknown)
                arrOfValues.add(txtMonthlyAmount.text!)
                arrOfValues.add(txtNameOfPrevPco.text!)
                //arrOfValues.add(txtNumberOfStories.text!)
                if dictOfNumberOfStories.count > 0 {
                    
                    arrOfValues.add("\(dictOfNumberOfStories.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(strOccupiedVacant)
                arrOfValues.add(strReportTypeId)
                arrOfValues.add(strReportTypeName)
                arrOfValues.add(strReportTypeSysName)
                //arrOfValues.add(txtRoofingMaterial.text!)
                if dictOfRoofingMaterials.count > 0 {
                    
                    arrOfValues.add("\(dictOfRoofingMaterials.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(strSlabRaised)
                //arrOfValues.add(txtviewStructureDescription.text!)
                if dictOfStructereDesc.count > 0 {
                    
                    arrOfValues.add("\(dictOfStructereDesc.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add(txtviewStructureDescription.text!)
                    
                }
                
                //arrOfValues.add(txtSubareaAccess.text!)
                if dictOfSubAreaAccess.count > 0 {
                    
                    arrOfValues.add("\(dictOfSubAreaAccess.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(strTipMasterId)
                
                if dictOfFollowUpInspection.count > 0 {
                    arrOfValues.add("\(dictOfFollowUpInspection.value(forKey: "FollowUpId")!)")
                }else {
                    arrOfValues.add("")
                }
                
                if dictOfGarage.count > 0 {
                    arrOfValues.add("\(dictOfGarage.value(forKey: "Value")!)")
                }else {
                    arrOfValues.add("")
                }
                
                arrOfValues.add(strTipMasterName)
                
                if isYearUnknown {
                    
                    arrOfValues.add("")
                    
                }else{
                    
                    arrOfValues.add(txtYearOfStructure.text!)
                    
                }
                
                arrOfValues.add(isTipWarranty)
                //arrOfValues.add(txtPastPcoTagPosted.text!)
                if dictOfPastPcoTagPosted.count > 0 {
                    
                    arrOfValues.add("\(dictOfPastPcoTagPosted.value(forKey: "Value")!)")
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                
                if arrOfCheckBox.count > 0 {
                    
                    let temp = arrOfCheckBox.componentsJoined(by: ",")
                    arrOfValues.add(temp)
                    
                }else {
                    
                    arrOfValues.add("")
                    
                }
                
                arrOfValues.add(strReportTypeSysName)
                
                // Setting values order by property owner and report sent to
                
                arrOfKeys.add("orderedBy")
                arrOfKeys.add("orderedByAddress")
                arrOfKeys.add("propertyOwner")
                arrOfKeys.add("propertyOwnerAddress")
                arrOfKeys.add("reportSentTo")
                arrOfKeys.add("reportSentToAddress")
                
                arrOfValues.add(lblOrderBY.text!)
                arrOfValues.add(lblOrderByAddress.text!)
                arrOfValues.add(lblPropertyOwnerName.text!)
                arrOfValues.add(lblPropertyOwnerAddress.text!)
                arrOfValues.add(lblReportSentName.text!)
                arrOfValues.add(lblReportSentAddress.text!)
                
                arrOfKeys.add("othersCpcTagLocated")
                arrOfKeys.add("othersExteriorMaterial")
                arrOfKeys.add("othersNumberofStories")
                arrOfKeys.add("othersOtherPcoTag")
                arrOfKeys.add("othersRoofMaterials")
                arrOfKeys.add("othersSubAreaAccess")
                
                arrOfValues.add(txtCPCInspTagPosted.text!)
                arrOfValues.add(txtExternalMaterial.text!)
                arrOfValues.add(txtNumberOfStories.text!)
                arrOfValues.add(txtPastPcoTagPosted.text!)
                arrOfValues.add(txtRoofingMaterial.text!)
                arrOfValues.add(txtSubareaAccess.text!)
                
                
                arrOfKeys.add("othersStructureDescription")
                arrOfValues.add(txtFldOther.text ?? "")
                
                
                arrOfKeys.add("tIPTHPSType")
                if dictOfTipWarranty.count != 0 {
                    arrOfValues.add(dictOfTipWarranty.value(forKey: "TIPTHPSType") ?? "")
                }else{
                    arrOfValues.add("")
                }

                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }
                
                
                // Check and Save General Notes Default
                
                /*
                if isBuildingPermit {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "BuildingPermit", strIdToCheck: "")
                    
                }*/
                
                
                if isTipWarranty {
                    
                    if strTipMasterId.count > 0 {
                        
                        checkWoWdoGeneralNoteMapping(strMappingType: "TIPMaster", strIdToCheck: strTipMasterId)
                        
                    }
                    /*if dictOfFollowUpInspection.count > 0 {
                        
                        checkWoWdoGeneralNoteMapping(strMappingType: "FollowUpMaster", strIdToCheck: "\(dictOfFollowUpInspection.value(forKey: "FollowUpId")!)")
                        
                    }*/
                    
                }
                
                if isYearUnknown {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "YearUnknown", strIdToCheck: "")
                    
                }
                
                if (txtYearOfStructure.text?.count)! > 0 {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "YearOfStructure", strIdToCheck: "")
                    
                }
                
                if dictOfFollowUpInspection.count > 0 {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "FollowUpMaster", strIdToCheck: "\(dictOfFollowUpInspection.value(forKey: "FollowUpId")!)")
                    
                }
                
                // For Report Type
                
                if strReportTypeSysName.count > 0 {
                    
                    checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "Yes")
                    
                }
            }
        }
    }
    
    func constantForSegement() {
        
        if segmentControls.selectedSegmentIndex == 0 {
            
            scrollView.isScrollEnabled = true

            hghtConstDisclaimerHeader.constant = 60
            viewDisclaimerHeader.isHidden = false
            hghtConstWdoInspectionViewHeader.constant = 60
            viewWdoInspectionHeader.isHidden = false
            
            if(btnShowHideDisclaimer.currentImage == UIImage(named: "close_arrow_ipad"))
            {//the show content
                
                hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)
                
            }
            else
            {// hide content
                
                self.hghtConstTblViewDisclaimer.constant = 0.0
                
            }
            
            if(btnShowHideWDOInspection.currentImage == UIImage(named: "close_arrow_ipad"))
            {//the show content
                
                hghtConstViewContainerWDOInspection.constant = 1800
                
            }
            else
            {// hide content
                
                hghtConstViewContainerWDOInspection.constant = 0.0
                
            }
            
            scrollView.contentSize = CGSize(width: self.view.frame.width, height: hghtConstTblViewDisclaimer.constant + hghtConstDisclaimerHeader.constant + hghtConstWdoInspectionViewHeader.constant + hghtConstViewContainerWDOInspection.constant + 150)
            
        }
        
    }
    
    // MARK:-
    // MARK:-Validation
    
    func validationForAddWdoInspection(yesCheck : Bool) -> Bool {
        
        if lblOrderBY.text!.count == 0 {
            
            let alert = UIAlertController(title: Alert, message: "Please enter ordered by name", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
                
                
                
            }))
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: "Enter", style: .default, handler: { (nil) in
                
                // goto edit wdo inspection vc
                self.goToEditInspectionView()
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            //showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please enter ordered by name", viewcontrol: self)
            return false

        }
        else  if((isTipWarranty && (IsTIPWarrantyRequired.lowercased() == "1" || IsTIPWarrantyRequired.lowercased() == "true")) && (yesCheck)){
            
            if(btnTipWarrantyType.title(for: .normal) == strSelectString){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectTipWarrantyType, viewcontrol: self)
                
                return false
                
            }
            
            /*
            if(btnFollowUpInspection.title(for: .normal) == strSelectString){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectFollowUpInspection, viewcontrol: self)
                
                return false
                
            }
            */
            
            // check to show monthly amount textfireld IsMonthlyPrice
            
            if dictOfTipWarranty.count > 0{
                
                let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                
                if isMonthlyAmount {
                    
                    if((txtMonthlyAmount.text?.count)! < 1){
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterMonthlyIncome, viewcontrol: self)
                        
                        return false
                        
                    }
                    
                    let amountt = (txtMonthlyAmount.text! as NSString).doubleValue
                    
                    if amountt < 1 {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterMonthlyIncomeZero, viewcontrol: self)
                        
                        return false
                        
                    }
                    
                }
                
            }
            
        }
        else if((txtYearOfStructure.text?.count)! > 1){
            
            if !(txtYearOfStructure.text?.count == 4){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_YearOfStructure, viewcontrol: self)
                
                return false
                
            }
            return true
        }
        
        return true
    }
    
    func setDefaultValuesInspection() {
        
        // setting default values on Inspection
        
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            
            
        }else{
            
            var strServiceContactName = String()
            strServiceContactName = "\(objWorkorderDetail.value(forKey: "serviceFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceLastName") ?? "")"
            
            if (lblOrderBY.text?.count)! < 1 {
                
                lblOrderBY.text = strServiceContactName
                
            }
            
            if (lblPropertyOwnerName.text?.count)! < 1 {
                
                lblPropertyOwnerName.text = strServiceContactName
                
            }
            
            if (lblReportSentName.text?.count)! < 1 {
                
                lblReportSentName.text = strServiceContactName
                
            }
            
            if (lblOrderByAddress.text?.count)! < 1 {
                
                lblOrderByAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
            if (lblPropertyOwnerAddress.text?.count)! < 1 {
                
                lblPropertyOwnerAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
            if (lblReportSentAddress.text?.count)! < 1 {
                
                lblReportSentAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
        }
        
    }
    
    func reloadTermiteIssueCode() {
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            
            let checkBoxes = "\(objData.value(forKey: "wdoTermiteIssueCodeSysNames") ?? "")"
            
            var tempArray = NSArray()
            
            if checkBoxes.count > 1 {
                
                tempArray = checkBoxes.components(separatedBy: ",") as NSArray
                
            }
            
            arrOfCheckBox = NSMutableArray()
            
            if tempArray.count > 0 {
                
                arrOfCheckBox.addObjects(from: tempArray as! [Any])
                
            }
            
        }
        
    }
    
    func setValuesWdoInspection() {
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            
            //strWoLeadId = "\(objData.value(forKey: "wDOLeadId") ?? "")"
            
            lblOrderBY.text = "\(objData.value(forKey: "orderedBy") ?? "")"
            lblOrderByAddress.text = "\(objData.value(forKey: "orderedByAddress") ?? "")"
            lblPropertyOwnerName.text = "\(objData.value(forKey: "propertyOwner") ?? "")"
            lblPropertyOwnerAddress.text = "\(objData.value(forKey: "propertyOwnerAddress") ?? "")"
            lblReportSentName.text = "\(objData.value(forKey: "reportSentTo") ?? "")"
            lblReportSentAddress.text = "\(objData.value(forKey: "reportSentToAddress") ?? "")"
            
            setDefaultValuesInspection()
            
            strReportTypeName = "\(objData.value(forKey: "reportTypeName") ?? "")"
            strReportTypeId = "\(objData.value(forKey: "reportTypeId") ?? "")"
            strReportTypeSysName = "\(objData.value(forKey: "reportTypeSysName") ?? "")"
            
            if strReportTypeSysName == enumReportCompleteName {
                
                btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnCompleteReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                strReportTypeName = enumReportComplete
                
            }else if strReportTypeSysName == enumReportLimitedName {
                
                btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnLimitedResport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                strReportTypeName = enumReportLimited
                
            }else if strReportTypeSysName == enumReportSupplementalName {
                
                btnSupplementalReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                strReportTypeName = enumReportSupplemental
                
            }else if strReportTypeSysName == enumReportReinspectionName { // Reinspection
                
                btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnCompleteReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnReinspectionReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                
                strReportTypeName = enumReportReinspection
                
            }else {
                
                btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnCompleteReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                strReportTypeId = "1"
                strReportTypeName = enumReportComplete
                strReportTypeSysName = enumReportCompleteName
                
                if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {

                }else{
                    
                    checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(strReportTypeSysName)" , strToSave: "No")

                }
                
            }
            
            isBuildingPermit = objData.value(forKey: "isBuildingPermit") as! Bool
            
            if isBuildingPermit {
                
                btnYes_BuildingPermit.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnNo_BuildingPermit.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }else{
                
                btnNo_BuildingPermit.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnYes_BuildingPermit.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            
            isYearUnknown = objData.value(forKey: "isYearUnknown") as! Bool
            
            if isYearUnknown {
                
                btnYearUnknown.setImage(UIImage(named: "check_ipad"), for: .normal)
                
                txtYearOfStructure.isHidden = true
                
            }else{
                
                btnYearUnknown.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                
                txtYearOfStructure.isHidden = false
                
            }
            
            isTipWarranty = objData.value(forKey: "isTipWarranty") as! Bool
            
            isTipWarranty = true
            
            if isTipWarranty {
                
                //btnYes_TipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                //btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                //hghtConstViewContainerTIPWarrantyType.constant = 100
                
            }else{
                
                //btnNoTipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                //btnYes_TipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                //hghtConstViewContainerTIPWarrantyType.constant = 0.0
                
            }
            
            isSeparateReport = objData.value(forKey: "isSeparateReport") as! Bool
            
            
            if isSeparateReport {
                
                btnYes_SeparateReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnNo_SeparateReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }else{
                
                btnNo_SeparateReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnYes_SeparateReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            
            txtSubareaAccess.text = "\(objData.value(forKey: "subareaAccess") ?? "")"
            txtNumberOfStories.text = "\(objData.value(forKey: "numberOfStories") ?? "")"
            txtExternalMaterial.text = "\(objData.value(forKey: "externalMaterial") ?? "")"
            txtRoofingMaterial.text = "\(objData.value(forKey: "roofingMaterial") ?? "")"
            
            txtYearOfStructure.text = "\(objData.value(forKey: "yearOfStructure") ?? "")"
            
            txtCPCInspTagPosted.text = "\(objData.value(forKey: "cpcInspTagPosted") ?? "")"
            txtPastPcoTagPosted.text = "\(objData.value(forKey: "pastPcoTagPosted") ?? "")"
            
            btnSubAreaAccess.setTitle("\(objData.value(forKey: "subareaAccess") ?? "")", for: .normal)
            btnNoOfStories.setTitle("\(objData.value(forKey: "numberOfStories") ?? "")", for: .normal)
            btnExternalMaterial.setTitle("\(objData.value(forKey: "externalMaterial") ?? "")", for: .normal)
            btnRoofingMaterial.setTitle("\(objData.value(forKey: "roofingMaterial") ?? "")", for: .normal)
            btnCpcTagPosted.setTitle("\(objData.value(forKey: "cpcInspTagPosted") ?? "")", for: .normal)
            btnPastPcoTagPosted.setTitle("\(objData.value(forKey: "pastPcoTagPosted") ?? "")", for: .normal)
            
            if "\(objData.value(forKey: "subareaAccess") ?? "")" == "" {
                btnSubAreaAccess.setTitle(strSelectString, for: .normal)
            }
            
            if "\(objData.value(forKey: "numberOfStories") ?? "")" == "" {
                btnNoOfStories.setTitle(strSelectString, for: .normal)
            }
            
            if "\(objData.value(forKey: "externalMaterial") ?? "")" == "" {
                btnExternalMaterial.setTitle(strSelectString, for: .normal)
            }
            
            if "\(objData.value(forKey: "roofingMaterial") ?? "")" == "" {
                btnRoofingMaterial.setTitle(strSelectString, for: .normal)
            }
            
            if "\(objData.value(forKey: "cpcInspTagPosted") ?? "")" == "" {
                btnCpcTagPosted.setTitle(strSelectString, for: .normal)
            }
            
            if "\(objData.value(forKey: "pastPcoTagPosted") ?? "")" == "" {
                btnPastPcoTagPosted.setTitle(strSelectString, for: .normal)
            }

            // New Changes for Others Filed
            
            txtSubareaAccess.text = "\(objData.value(forKey: "othersSubAreaAccess") ?? "")"
            txtNumberOfStories.text = "\(objData.value(forKey: "othersNumberofStories") ?? "")"
            txtExternalMaterial.text = "\(objData.value(forKey: "othersExteriorMaterial") ?? "")"
            txtRoofingMaterial.text = "\(objData.value(forKey: "othersRoofMaterials") ?? "")"
            txtCPCInspTagPosted.text = "\(objData.value(forKey: "othersCpcTagLocated") ?? "")"
            txtPastPcoTagPosted.text = "\(objData.value(forKey: "othersOtherPcoTag") ?? "")"
            
            if "\(objData.value(forKey: "subareaAccess") ?? "")" == strOtherString {
                txtSubareaAccess.isHidden = false
            }else{
                txtSubareaAccess.isHidden = true
            }
            
            if "\(objData.value(forKey: "numberOfStories") ?? "")" == "0" {
                txtNumberOfStories.isHidden = false
            }else{
                txtNumberOfStories.isHidden = true
            }
            
            if "\(objData.value(forKey: "externalMaterial") ?? "")" == strOtherString {
                txtExternalMaterial.isHidden = false
            }else{
                txtExternalMaterial.isHidden = true
            }
            
            if "\(objData.value(forKey: "roofingMaterial") ?? "")" == strOtherString {
                txtRoofingMaterial.isHidden = false
            }else{
                txtRoofingMaterial.isHidden = true
            }
            
            if "\(objData.value(forKey: "cpcInspTagPosted") ?? "")" == strOtherString {
                txtCPCInspTagPosted.isHidden = false
            }else{
                txtCPCInspTagPosted.isHidden = true
            }
            
            if "\(objData.value(forKey: "pastPcoTagPosted") ?? "")" == strOtherString {
                txtPastPcoTagPosted.isHidden = false
            }else{
                txtPastPcoTagPosted.isHidden = true
            }
            
            txtNameOfPrevPco.text = "\(objData.value(forKey: "nameOfPrevPco") ?? "")"
            txtDateOfPrevPco.text = "\(objData.value(forKey: "dateOfPrevPco") ?? "")"
            txtDateOfPrevPco.text = changeStringDateToGivenFormat(strDate: "\(objData.value(forKey: "dateOfPrevPco") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            
            txtviewStructureDescription.text = "\(objData.value(forKey: "structureDescription") ?? "")"
            
            txtFldOther.text = "\(objData.value(forKey: "othersStructureDescription") ?? "")"
            
            if "\(objData.value(forKey: "structureDescription")!)".caseInsensitiveCompare("others") == .orderedSame
            {
                txtFldOther.isHidden = false
            }
            else
            {
                txtFldOther.isHidden = true
            }
            
            txtviewGenralDescription.text = "\(objData.value(forKey: "generalDescription") ?? "")"
            if "\(objData.value(forKey: "generalDescription") ?? "")" != "" {
                isGeneralDescriptionPrepare = true
            }
            txtviewComment.text = "\(objData.value(forKey: "comments") ?? "")"
            txtMonthlyAmount.text = "\(objData.value(forKey: "monthlyAmount") ?? "")"
            
            // Sub Area Access
            
            var array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "SubAreaAccesss", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "subareaAccess") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfSubAreaAccess = dictData
                    //txtSubareaAccess.text = "\(dictOfSubAreaAccess.value(forKey: "Value")!)"
                    btnSubAreaAccess.setTitle("\(dictOfSubAreaAccess.value(forKey: "Value")!)", for: .normal)
                    btnSubAreaAccess.setTitle("\(dictOfSubAreaAccess.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            // Number Of Stories
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "NumberofStories", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "numberOfStories") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfNumberOfStories = dictData
                    //txtNumberOfStories.text = "\(dictOfNumberOfStories.value(forKey: "Value")!)"
                    btnNoOfStories.setTitle("\(dictOfNumberOfStories.value(forKey: "Value")!)", for: .normal)
                    btnNoOfStories.setTitle("\(dictOfNumberOfStories.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            // External Materials
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "ExteriorMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "externalMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfExternalMaterials = dictData
                    //txtExternalMaterial.text = "\(dictOfExternalMaterials.value(forKey: "Value")!)"
                    btnExternalMaterial.setTitle("\(dictOfExternalMaterials.value(forKey: "Value")!)", for: .normal)
                    btnExternalMaterial.setTitle("\(dictOfExternalMaterials.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            
            // Roofing Materials
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RoofMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "roofingMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfRoofingMaterials = dictData
                    //txtRoofingMaterial.text = "\(dictOfRoofingMaterials.value(forKey: "Value")!)"
                    btnRoofingMaterial.setTitle("\(dictOfRoofingMaterials.value(forKey: "Value")!)", for: .normal)
                    btnRoofingMaterial.setTitle("\(dictOfRoofingMaterials.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            
            // CPC Tag Posted
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CpcTagLocateds", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "cpcInspTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfCpcTagPosted = dictData
                    //txtCPCInspTagPosted.text = "\(dictOfCpcTagPosted.value(forKey: "Value")!)"
                    btnCpcTagPosted.setTitle("\(dictOfCpcTagPosted.value(forKey: "Value")!)", for: .normal)
                    btnCpcTagPosted.setTitle("\(dictOfCpcTagPosted.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            // Past Pco Tag Posted
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "OtherPcoTags", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "pastPcoTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfPastPcoTagPosted = dictData
                    //txtPastPcoTagPosted.text = "\(dictOfPastPcoTagPosted.value(forKey: "Value")!)"
                    btnPastPcoTagPosted.setTitle("\(dictOfPastPcoTagPosted.value(forKey: "Value")!)", for: .normal)
                    btnPastPcoTagPosted.setTitle("\(dictOfPastPcoTagPosted.value(forKey: "Text")!)", for: .normal)

                    break
                    
                }
                
            }
            
            
            // Structure Descriptions
            
            array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "StructuredDescriptions", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if "\(objData.value(forKey: "structureDescription") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                    
                    dictOfStructereDesc = dictData
                    txtviewStructureDescription.text = "\(dictOfStructereDesc.value(forKey: "Value")!)"
                    txtviewStructureDescription.text = "\(dictOfStructereDesc.value(forKey: "Text")!)"

                    break
                    
                }
                
            }
            
            // tIPMasterName  tIPMasterId
            strTipMasterName = "\(objData.value(forKey: "tIPMasterName") ?? "")"
            strTipMasterId = "\(objData.value(forKey: "tIPMasterId") ?? "")"
            
            if strTipMasterName.count > 0 {
                
                btnTipWarrantyType.setTitle(strTipMasterName, for: .normal)
                
                var ary = NSMutableArray()
                
                ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
                
                if ary.count > 0 {
                    
                    for k in 0 ..< ary.count {
                        
                        let dictData = ary[k] as! NSDictionary
                        
                        if strTipMasterId == "\(dictData.value(forKey: "TIPMasterId") ?? "")" {
                            
                            dictOfTipWarranty = dictData
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                if dictOfTipWarranty.count > 0 {
                    
                    let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                    
                    if !isMonthlyAmount {
                        
                        txtMonthlyAmount.isHidden = true
                        
                    }else{
                        
                        txtMonthlyAmount.isHidden = false

                    }
                    
                }
                
                
            }
            else{
                btnTipWarrantyType.setTitle(strSelectString, for: .normal)
                txtMonthlyAmount.isHidden = true
                txtMonthlyAmount.text = ""
            }
            // For Follow up Inspection
            let strFollowUpInspectionId = "\(objData.value(forKey: "followUpInspectionId") ?? "")"
            if strFollowUpInspectionId.count > 0 {
                var ary = NSMutableArray()
                ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "FollowUpInspectionExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
                if ary.count > 0 {
                    for k in 0 ..< ary.count {
                        let dictData = ary[k] as! NSDictionary
                        if strFollowUpInspectionId == "\(dictData.value(forKey: "FollowUpId") ?? "")" {
                            dictOfFollowUpInspection = dictData
                            btnFollowUpInspection.setTitle("\(dictOfFollowUpInspection.value(forKey: "Title")!)", for: .normal)
                            break
                        }
                    }
                }
            }else{
                btnFollowUpInspection.setTitle(strSelectString, for: .normal)
            }
           
            //----Garage---
            let strGarageID = "\(objData.value(forKey: "garage") ?? "")"
            if strGarageID.count > 0 {
                if  nsud.value(forKey: "MasterServiceAutomation") is NSDictionary
                {
                    let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as!  NSDictionary
                    
                    if dictMaster.value(forKey: "Garages") is NSArray
                    {
                        let arrayData = (dictMaster.value(forKey: "Garages") as! NSArray).mutableCopy() as! NSMutableArray
                       
                        let aryTemp = arrayData.filter { (task) -> Bool in
                            return "\((task as! NSDictionary).value(forKey: "Value")!)".lowercased() == strGarageID.lowercased()
                            
                        }
                        if(aryTemp.count != 0)
                        {
                            let dict = aryTemp[0] as! NSDictionary
                            self.dictOfGarage = NSMutableDictionary()
                            self.dictOfGarage = dict.mutableCopy() as! NSMutableDictionary
                            self.btn_Garage.setTitle("\(dictOfGarage.value(forKey: "Text")!)", for: .normal)

                        }
                        else
                        {
                            self.dictOfGarage = NSMutableDictionary()
                            self.btn_Garage.setTitle(strSelectString, for: .normal)
                        }
                    }
                    else
                    {
                        self.dictOfGarage = NSMutableDictionary()
                        self.btn_Garage.setTitle(strSelectString, for: .normal)
                    }
                    
                }
                else
                {
                    self.dictOfGarage = NSMutableDictionary()
                    self.btn_Garage.setTitle(strSelectString, for: .normal)
                }
            }else{
                self.dictOfGarage = NSMutableDictionary()

                btn_Garage.setTitle(strSelectString, for: .normal)
            }
       
            
            strSlabRaised = "\(objData.value(forKey: "slabRaised") ?? "")"
            
            // radio_check_ipad
            // radio_uncheck_ipad
            
            if strSlabRaised == "1" {
                
                btnSlab.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            else if strSlabRaised == "2"{
                
                btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnRaised.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            else if strSlabRaised == "3"{
                
                btnSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnRaisedSlab.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                
            }
            else{
                
                strSlabRaised = "1"
                btnSlab.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            
            strOccupiedVacant = "\(objData.value(forKey: "occupiedVacant") ?? "")"
            
            if strOccupiedVacant == "1" {
                
                btnOccupied.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            else if strOccupiedVacant == "2"{
                
                btnOccupied.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnVacant.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                
            }
            else{
                
                strOccupiedVacant = "1"
                btnOccupied.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            
            strFurnishedUFurnished = "\(objData.value(forKey: "furnishedUnfurnished") ?? "")"
            
            if strFurnishedUFurnished == "1" {
                
                btnUnfurnished.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            else if strFurnishedUFurnished == "2"{
                
                btnUnfurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                btnFurnished.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                
            }
            else{
                
                strFurnishedUFurnished = "1"
                btnUnfurnished.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
            }
            
            let checkBoxes = "\(objData.value(forKey: "wdoTermiteIssueCodeSysNames") ?? "")"
            
            var tempArray = NSArray()
            
            if checkBoxes.count > 1 {
                
                tempArray = checkBoxes.components(separatedBy: ",") as NSArray
                
            }
            
            if tempArray.count > 0 {
                
                arrOfCheckBox.addObjects(from: tempArray as! [Any])
                
                /*
                if arrOfCheckBox.contains(enumSUBTERRANEANTERMITES) {
                    
                    btnSubterranianTermites.setImage(UIImage(named: "check_ipad"), for: .normal)
                    
                }else{
                    
                    btnSubterranianTermites.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                    
                }
                
                if arrOfCheckBox.contains(enumDRYWOODTERMITES) {
                    
                    btnDrywoodTermited.setImage(UIImage(named: "check_ipad"), for: .normal)
                    
                }else{
                    
                    btnDrywoodTermited.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                    
                }
                
                if arrOfCheckBox.contains(enumFUNGUSDRYROT) {
                    
                    btnFungus.setImage(UIImage(named: "check_ipad"), for: .normal)
                    
                }else{
                    
                    btnFungus.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                    
                }
                
                if arrOfCheckBox.contains(enumOTHERFINDINGS) {
                    
                    btnOtherFindings.setImage(UIImage(named: "check_ipad"), for: .normal)
                    
                }else{
                    
                    btnOtherFindings.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                    
                }
                
                if arrOfCheckBox.contains(enumFURTHERINSPECTION) {
                    
                    btnFurtherInspection.setImage(UIImage(named: "check_ipad"), for: .normal)
                    
                }else{
                    
                    btnFurtherInspection.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                    
                }
                 */
                
            }else{
                
                /*
                btnSubterranianTermites.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                btnDrywoodTermited.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                btnFungus.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                btnOtherFindings.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                btnFurtherInspection.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                */
            }
            
            
            
            
        }
        else
        {
            txtFldOther.text = ""
            lblOrderBY.text = ""
            lblOrderByAddress.text = ""
            lblPropertyOwnerName.text = ""
            lblPropertyOwnerAddress.text = ""
            lblReportSentName.text = ""
            lblReportSentAddress.text = ""
            strReportTypeName = ""
            strReportTypeId = ""
            
            btnSupplementalReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnCompleteReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnLimitedResport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnReinspectionReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            strReportTypeId = "1"
            strReportTypeName = enumReportCompleteName
            strReportTypeName = enumReportComplete
            strReportTypeSysName = enumReportCompleteName
            
            btnYes_BuildingPermit.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnNo_BuildingPermit.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            isBuildingPermit = true
            
            btnYearUnknown.setImage(UIImage(named: "check_ipad"), for: .normal)
            isYearUnknown = true
            
            //btnYes_TipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            //btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            isTipWarranty = true
            //hghtConstViewContainerTIPWarrantyType.constant = 100
            
            btnYes_SeparateReport.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnNo_SeparateReport.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            isSeparateReport = true
            
            txtSubareaAccess.text = ""
            txtNumberOfStories.text = ""
            txtExternalMaterial.text = ""
            txtRoofingMaterial.text = ""
            txtYearOfStructure.text = ""
            
            txtCPCInspTagPosted.text = ""
            txtPastPcoTagPosted.text = ""
            txtNameOfPrevPco.text = ""
            txtDateOfPrevPco.text = ""
            txtviewStructureDescription.text = ""
            txtviewGenralDescription.text = ""
            txtviewComment.text = ""
            txtMonthlyAmount.text = ""
            
            // tIPMasterName  tIPMasterId
            strTipMasterName = ""
            strTipMasterId = ""
            
            btnOccupied.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnVacant.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            strOccupiedVacant = "1"
            
            btnUnfurnished.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnFurnished.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            strFurnishedUFurnished = "1"
            
            strSlabRaised = "1"
            btnSlab.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnRaised.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            btnRaisedSlab.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            
            /*
            btnSubterranianTermites.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            btnDrywoodTermited.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            btnFungus.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            btnOtherFindings.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            btnFurtherInspection.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            */
            
            arrOfCheckBox = NSMutableArray()
            
        }
        
    }
    
    @objc func editDisclaimer(sender: UIButton) {
        
        saveWdoInspectionInDB(yesCheck: false)

        fetchDisclaimer()

        if self.arrOfDiclaimer[sender.tag] is NSDictionary {
            
            let objArea = self.arrOfDiclaimer[sender.tag] as! NSDictionary
            let strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
            goToDisclaimerAddView(strToUpdate: "Yes", strDisclaimerId: strDisclaimerIdLocal)
            
        }else{
            
            let objArea = self.arrOfDiclaimer[sender.tag] as! NSManagedObject
            let strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
            goToDisclaimerAddView(strToUpdate: "Yes", strDisclaimerId: strDisclaimerIdLocal)
            
        }
        
        /*let objArea = self.arrOfDiclaimer[sender.tag] as! NSManagedObject
        let strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
        goToDisclaimerAddView(strToUpdate: "Yes", strDisclaimerId: strDisclaimerIdLocal)*/
        
    }
    
    @objc func deleteDisclaimer(sender: UIButton) {
        
        let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            
            //let objArea = self.arrOfDiclaimer[sender.tag] as! NSManagedObject
            var strDisclaimerIdLocal = ""
            
            if self.arrOfDiclaimer[sender.tag] is NSDictionary {
                
                let objArea = self.arrOfDiclaimer[sender.tag] as! NSDictionary
                strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
                
            }else{
                
                let objArea = self.arrOfDiclaimer[sender.tag] as! NSManagedObject
                strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
                
            }
            
            //deleteAllRecordsFromDB(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && disclaimerId == %@", self.strWoId, strDisclaimerIdLocal))
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && woWdoDisclaimerId == %@", self.strWoId, strDisclaimerIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            self.fetchDataFrmDB()
            
            self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)
            
            if self.arrOfDiclaimer.count < 1 {
                
                self.btnShowHideDisclaimer.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
                self.hghtConstTblViewDisclaimer.constant = 0.0
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
            
        })
        
    }
    
    @objc func editProblemIdentification(sender: UIButton) {
        
        let objArea = self.arrOfProblemIdentification[sender.tag] as! NSManagedObject
        let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
        let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
        
        goToProblemIdentificationAddView(strToUpdate: "Yes", strProblemIdentificationIdLocal: strProblemIdentificationIdLocal,strMobileProblemIdentificationIdLocal: strMobileProblemIdentificationIdLocal)
        
    }
    
    @objc func deleteProblemIdentification(sender: UIButton) {
                
        let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            
            let objArea = self.arrOfProblemIdentification[sender.tag] as! NSManagedObject
            let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
            let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            var isSuccess = false
            
            if strProblemIdentificationIdLocal.count > 0 {
                
                isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }else{
                
                isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", self.strWoId, strMobileProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            //let isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            //let isSuccessPricing = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            var isSuccessPricing = false
            
            if strProblemIdentificationIdLocal.count > 0 {
                
                isSuccessPricing = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }else{
                
                isSuccessPricing = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strMobileProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            if isSuccessPricing {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
            
            // Delete Target and Target Images
            
            //if strProblemIdentificationIdLocal.count > 0 {
                
                let arrayOfTarget1 = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strProblemIdentificationIdLocal))

                for k in 0 ..< arrayOfTarget1.count {
                    
                    let dictData = arrayOfTarget1[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strProblemIdentificationIdLocal))

            //}else{
                
                let arrayOfTarget = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strMobileProblemIdentificationIdLocal))

                for k in 0 ..< arrayOfTarget.count {
                    
                    let dictData = arrayOfTarget[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strMobileProblemIdentificationIdLocal))

            //}

            self.fetchDataFrmDB()

            self.setProblemViewOnEdit()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
            
        })
        
    }
    
    @objc func deleteGeneralNotes(sender: UIButton) {
        
        let alert = UIAlertController(title: AlertDelete, message: AlertDeleteDataMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            
            let objArea = self.arrOfGeneralNotes[sender.tag] as! NSManagedObject
            let strwdoGeneralNoteId = "\(objArea.value(forKey: "wdoGeneralNoteId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && wdoGeneralNoteId == %@", self.strWoId, strwdoGeneralNoteId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            self.fetchDataFrmDB()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
            
        })
        
    }
    
    @objc func editGeneralNotes(sender: UIButton) {
        
        let objArea = self.arrOfGeneralNotes[sender.tag] as! NSManagedObject
        let strWdoGeneralNoteId = "\(objArea.value(forKey: "wdoGeneralNoteId") ?? "")"
        goToGeneralAddView(strToUpdate: "Yes", strGeneralNoteId: strWdoGeneralNoteId)

    }
    
    
    func checkIfPricingExceed() -> String  {
        
        var strHourlyRate = "",  strMaterialMultiplier = "", strSubFeeMultiplier = "",  strLinearFtSoilTreatmentRate = "", strLinearFtDrilledConcreteRate = "", strCubicFtRate = "", strDiscount = "", subTotalAmount = ""

        
        let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
        
        if(arrayLeadDetail.count > 0)
        {
            
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            
            subTotalAmount = "\(match.value(forKey: "subTotalAmount")!)"
            
        }
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            
            strDiscount = "\(objData.value(forKey: "otherDiscount") ?? "")"

        }
        
        var message = ""

        let arrayWdoPricingMultipliers = getDataFromLocal(strEntity: Entity_WdoPricingMultipliers, predicate: NSPredicate(format: "workOrderId == %@",strWoId))
        
        let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "no", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)
        
        
        if arrayWdoPricingMultipliers.count > 0
        {
            let matchObject = arrayWdoPricingMultipliers.object(at: 0) as! NSManagedObject
            
            strHourlyRate = "\(matchObject.value(forKey: "hourlyRate") ?? "")"
            strMaterialMultiplier = "\(matchObject.value(forKey: "materialMultiplier") ?? "")"
            strSubFeeMultiplier = "\(matchObject.value(forKey: "subFeeMultiplier") ?? "")"
            strLinearFtSoilTreatmentRate = "\(matchObject.value(forKey: "linearFtSoilTreatmentRate") ?? "")"
            strLinearFtDrilledConcreteRate = "\(matchObject.value(forKey: "linearFtDrilledConcreteRate") ?? "")"
            strCubicFtRate = "\(matchObject.value(forKey: "cubicFtRate") ?? "")"
            
            if(aryRateMasterExtSerDcs.count != 0)
            {
                
                let obj = aryRateMasterExtSerDcs.firstObject as! NSDictionary
                let MaxHourlyRate = Double("\(obj.value(forKey: "MaxHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxHourlyRate") ?? "0")")
                let MinHourlyRate = Double("\(obj.value(forKey: "MinHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinHourlyRate") ?? "0")")
                let HourlyRate =  Double("\(strHourlyRate )")
                
                if MinHourlyRate != 0 && MaxHourlyRate != 0
                {
                    if !(MinHourlyRate!...MaxHourlyRate! ~= HourlyRate!) {
                        message = "! Hourly Rate ($) is exceeding \(MinHourlyRate!) and \(MaxHourlyRate!)\n"
                    }
                }
                
                let MaxMaterialMultiplier = Double("\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")" == "" ?  "0" : "\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")")
                let MinMaterialMultiplier = Double("\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")")
                let MaterialMultiplier = Double("\(strMaterialMultiplier )")
                
                if MinMaterialMultiplier != 0 && MaxMaterialMultiplier != 0
                {
                    if !(MinMaterialMultiplier!...MaxMaterialMultiplier! ~= MaterialMultiplier!) {
                        message =  message + "! MaterialMultiplier is exceeding \(MinMaterialMultiplier!) and \(MaxMaterialMultiplier!)\n"
                    }
                }
                
                let MaxSubFeeMultiplier = Double("\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")")
                let MinSubFeeMultiplier = Double("\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")")
                let SubFeeMultiplier = Double("\(strSubFeeMultiplier )")
                

                if MinSubFeeMultiplier != 0 && MaxSubFeeMultiplier != 0
                {
                    if !(MinSubFeeMultiplier!...MaxSubFeeMultiplier! ~= SubFeeMultiplier!) {
                        message =  message + "! Sub Fee Multiplier is exceeding \(MinSubFeeMultiplier!) and \(MaxSubFeeMultiplier!)\n"
                    }
                }
                
                let MaxSoilTreatmentRate = Double("\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")")
                let MinSoilTreatmentRate = Double("\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")")
                let SoilTreatmentRate = Double("\(strLinearFtSoilTreatmentRate )")
                
                if MinSoilTreatmentRate != 0 && MaxSoilTreatmentRate != 0
                {
                    if !(MinSoilTreatmentRate!...MaxSoilTreatmentRate! ~= SoilTreatmentRate!) {
                        message =  message + "! Linear Ft. Soil Treatment  Rate ($) is exceeding \(MinSoilTreatmentRate!) and \(MaxSoilTreatmentRate!)\n"
                    }
                }
                
                let MaxDrilledConcreteRate = Double("\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")")
                let MinDrilledConcreteRate = Double("\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")")
                let DrilledConcreteRate = Double("\(strLinearFtDrilledConcreteRate )")
                
                if MinDrilledConcreteRate != 0 && MaxDrilledConcreteRate != 0
                {
                    if !(MinDrilledConcreteRate!...MaxDrilledConcreteRate! ~= DrilledConcreteRate!) {
                        message =  message + "! Linear Ft. Drilled Concrete  Rate ($) is exceeding \(MinDrilledConcreteRate!) and \(MaxDrilledConcreteRate!)\n"
                    }
                }
                
                
                let MaxCubicRate = Double("\(obj.value(forKey: "MaxCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxCubicRate") ?? "0")")
                let MinCubicRate = Double("\(obj.value(forKey: "MinCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinCubicRate") ?? "0")")
                let CubicRate = Double("\(strCubicFtRate)")
                
                if MinCubicRate != 0 && MaxCubicRate != 0
                {
                    if !(MinCubicRate!...MaxCubicRate! ~= CubicRate!) {
                        message =  message + "! Cubic ft. Rate ($) is exceeding \(MinCubicRate!) and \(MaxCubicRate!)\n"
                    }
                }
                
                //CalculateDiscount
                let subTotalAmount = Double("\(subTotalAmount)") ?? 0
                let MaxDiscount = (Double("\(obj.value(forKey: "MaxAllowed") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
                let MinDiscount = Double("0") ?? 0
                var discount = Double("\(strDiscount)") ?? 0
                discount = discount < 0 ? 0 : discount
                
                if MaxDiscount != 0
                {
                    if !(MinDiscount...MaxDiscount ~= discount) {
                        message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount.rounded())\n"
                    }
                }
                
                if message.count > 0 {
                    
                    message = message + "You will need a supervisor's approval and signature before the customer can sign the proposal."
                }
            }
        }
        return message
    }
    func showFirstSegmentDefault() {
        
        self.view.isUserInteractionEnabled = false

        // Graph Diagram
        hghtConstWdoInspectionViewHeader.constant = 0
        hghtConstViewContainerWDOInspection.constant = 0
        viewWdoInspectionHeader.isHidden = true
        
        hghtConstDisclaimerHeader.constant = 0
        viewDisclaimerHeader.isHidden = true
        hghtConstTblViewDisclaimer.constant = 0.0
        
        viewProblemIdentificationHeader.isHidden = true
        hghtConstProblemIdentificationHeader.constant = 0
        showProblemIdentification()
        hghtConstGraphWebViewHide.constant = 0

        constProblemIdentificationHeaderUpVerticalSpace.constant = -20-CGFloat(210*self.arrOfDiclaimer.count)
        
        scrollView.isScrollEnabled = false
        self.hghtConstGraph.constant = UIScreen.main.bounds.height - 80
        graphDrawingWebView.isHidden = false
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        viewGeneralNotes.removeFromSuperview()
        viewProblemId.removeFromSuperview()

        saveGraphAsXML()
        
        saveWdoInspectionInDB(yesCheck: false)
        
        fetchNotes()
        
        fetchDisclaimer()

        self.view.isUserInteractionEnabled = true
    }
   
    //Navin Save rateMasterId in DB
    func UpdateRateMasterID_InWO() {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if(arryOfData.count != 0){
            let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "no", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
            
            let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)
            if aryRateMasterExtSerDcs.count > 0
            {
                let matchObject = aryRateMasterExtSerDcs.object(at: 0) as! NSDictionary
                let RateMasterId = "\(matchObject.value(forKey: "RateMasterId") ?? "")"
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
             
                arrOfKeys.add("rateMasterId")
                arrOfValues.add(RateMasterId)
            
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess
                {
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                }
            }
            
            
        
        }
    }
    //Navin End
    
    //:---------------------------------------------------------------------------------------------------------
    // MARK: ----------------------------------------Dynamic Issue Codes------------------------------------------
    //:---------------------------------------------------------------------------------------------------------
    
    @objc func actionOnCheckButton(sender: UIButton) {
        
        //sender.tag
        if(sender.currentImage == UIImage(named: "uncheck.png"))
        {
            
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
            arrOfCheckBox.add(sender.titleLabel?.text! ?? "")

        }
        else
        {
            
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
            arrOfCheckBox.remove(sender.titleLabel?.text! ?? "")

        }
        
    }
    
    func createDynamicIssueCode() {
        
        reloadTermiteIssueCode()
        
        viewTermiteIssueCodes.backgroundColor = UIColor.white
        
        for view in self.viewTermiteIssueCodes.subviews {
            
            view.removeFromSuperview()
            
        }
        
        var yAxix = 0.0
        
        let tempArray = NSMutableArray()
        //tempArray.add("asdf")
        
        let arrOfIssuesCodeMasters = NSMutableArray()
        let arrOfIssuesCodeMastersCodeSysName = NSMutableArray()
        let arrOfIssuesCodeMastersCodeTitle = NSMutableArray()

        var array = NSMutableArray()

        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TermiteIssueCodeExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if arrOfCheckBox.contains("\(dictData.value(forKey: "CodeSysName") ?? "")") || "\(dictData.value(forKey: "IsActive") ?? "")" == "1" || "\(dictData.value(forKey: "IsActive") ?? "")" == "true" || "\(dictData.value(forKey: "IsActive") ?? "")" == "True"{
                    
                    arrOfIssuesCodeMasters.add(dictData)
                    arrOfIssuesCodeMastersCodeSysName.add("\(dictData.value(forKey: "CodeSysName") ?? "")")
                    arrOfIssuesCodeMastersCodeTitle.add("\(dictData.value(forKey: "CodeTitle") ?? "")")

                }
                
            }
            
        }

        for (index, item) in tempArray.enumerated() {
         
            let strOption = arrOfIssuesCodeMastersCodeTitle.componentsJoined(by: ",")
            let strAnswer = arrOfCheckBox.componentsJoined(by: ",")
            
            
            //Option
            let aryOption = strOption.split(separator: ",")
            let aryAnswer = strAnswer.split(separator: ",")
            
            for (indextemp, item) in aryOption.enumerated(){
                
                let btnCheckBox = UIButton()
                btnCheckBox.setTitleColor(UIColor.clear, for: .normal)
                btnCheckBox.frame = CGRect(x: 30, y: Int(yAxix), width: 40, height: 40)
                btnCheckBox.setTitle(((item as AnyObject)as! String), for: .normal)
                btnCheckBox.setTitle(arrOfIssuesCodeMastersCodeSysName[indextemp] as? String, for: .normal)
                btnCheckBox.setImage(UIImage(named: "uncheck.png"), for: .normal)
                
                if(strAnswer == ""){
                    
                    btnCheckBox.setImage(UIImage(named: "uncheck.png"), for: .normal)
                    
                }else{
                    
                    for item in aryAnswer{
                        if ("\((btnCheckBox.titleLabel?.text!)!)" ==  (item as AnyObject) as! String){
                            btnCheckBox.setImage(UIImage(named: "checked.png"), for: .normal)
                        }
                    }
                    
                }
                btnCheckBox.tag = index
                btnCheckBox.addTarget(self, action: #selector(actionOnCheckButton), for: .touchUpInside)
                
                if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                    btnCheckBox.isUserInteractionEnabled = false
                }else{
                    btnCheckBox.isUserInteractionEnabled = true
                }
                
                let lblOption = UILabel()
                lblOption.text = ((item as AnyObject)as! String)
                lblOption.font = UIFont.systemFont(ofSize: 20)
                lblOption.numberOfLines = 0
                lblOption.frame = CGRect(x: 80, y: Int(yAxix), width: Int(UIScreen.main.bounds.width) - 100, height:  Int(WebService().estimatedHeightOfLabel(text: lblOption.text!, view: self.viewTermiteIssueCodes)))
                
                self.viewTermiteIssueCodes.addSubview(btnCheckBox)
                self.viewTermiteIssueCodes.addSubview(lblOption)
                
                let hghtTemp = lblOption.frame.height < 40 ? 40 : lblOption.frame.height
                
                yAxix = Double(CGFloat(yAxix) +  hghtTemp  + 10)
                
            }
            
        }
        
        self.hghtConstTermiteIssueCodeView.constant = CGFloat(yAxix)
        self.hghtConstViewContainerWDOInspection.constant = CGFloat(1800 + yAxix)
        
    }
    
    
    // MARK: - -----------------------------------Footer Functions -----------------------------------
    
    func goToServiceDocument() {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToServiceHistory()  {
        
        saveGraphAsXML()
        
        //UserDefaults.standard.set(true, forKey: "RefreshGraph_ProblemAdd")

        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToWdoGraphDrawHistory()  {
        
        saveGraphAsXML()

        let storyboardIpad = UIStoryboard.init(name: "WdoGraphDrawHistory_iPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "WdoGraphDrawHistoryVC_iPad") as? WdoGraphDrawHistoryVC_iPad
        testController?.strHeaderTitle = "Graph Draw History"
        testController?.strGlobalWoId = "\(strWoId)"
        testController?.strWoLeadId = "\(strWoLeadId)"
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        testController?.delegate = self
        self.present(testController!, animated: false, completion: nil)
        
    }

    // MARK: - -----------------------------------General Notes Based on TIP and Follow Up Inspection-----------------------------------

    func checkWoWdoGeneralNoteMapping(strMappingType : String , strIdToCheck : String) {
        
        //WoWdoGeneralNoteMappingExtSerDcs FollowUpMaster TIPMaster BuildingPermit
        // MasterId MappingType IsActive GeneralNoteId
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoGeneralNoteMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                if strMappingType == "BuildingPermit" || strMappingType == "YearUnknown"{
                    
                    if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                        
                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            
                            saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                            
                        }
                        
                    }
                    
                }else if strMappingType == "YearOfStructure"{//
                    
                    if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                        
                        //YearRange 1950-2018

                        let yearRange = "\(dictData.value(forKey: "YearRange") ?? "")"
                        
                        let arrRange = yearRange.components(separatedBy: "-")
                        
                        if arrRange.count == 2 {
                            
                            var range1 = Int(arrRange[0])
                            range1 = range1! - 1 + 1
                            var range2 = Int(arrRange[1])
                            range2 = range2! + 1
                            let yearOfStructureInt = Int(txtYearOfStructure.text!)
                            
                            let successful = (range1!..<range2!).contains(yearOfStructureInt!)
                            
                            if successful {
                                
                                let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                                
                                let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp))
                                
                                if arrOfGeneralNoteTemp.count == 0 {
                                    
                                    // General Note Id Does not exist so insert data of General Notes in DC
                                    
                                    saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }else if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                    
                    if strIdToCheck == "\(dictData.value(forKey: "MasterId") ?? "")" {

                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"

                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            
                            saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveGeneralNotesDB(strGeneralNotedId : String , strMappingType : String) {
        
        var array = NSMutableArray()
        var dictDataOfGeneralNotesTemp = NSDictionary()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if strGeneralNotedId == "\(dictData.value(forKey: "ServiceJobDescriptionId") ?? "")" {
                    
                    dictDataOfGeneralNotesTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfGeneralNotesTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("generalNoteMasterId")
            arrOfKeys.add("generalNoteDescription")
            arrOfKeys.add("title")
            arrOfKeys.add("wdoGeneralNoteId")
            
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescriptionId")!)")
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescription")!)")
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "Title")!)")
            
            arrOfValues.add(Global().getReferenceNumber())
            
            arrOfKeys.add("type")
            arrOfValues.add(strMappingType)
            
            arrOfKeys.add("isChanged")
            arrOfValues.add(false)
            
            arrOfKeys.add("fillIn")
            arrOfValues.add("")

            
            // Saving Disclaimer In DB
            saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }
    
    // MARK: - -----------------------------------Discalimer Based on Report Type-----------------------------------
    
    func checkWoWdoDiscalimerMapping(strMappingType : String , strIdToCheck : String , strToSave : String) {
        
        if strToSave == "No" {
            
            fetchDisclaimer()
            
        }
        
        //WoWdoGeneralNoteMappingExtSerDcs FollowUpMaster TIPMaster BuildingPermit
        // MasterId MappingType IsActive GeneralNoteId
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoDisclaimerMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                //if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                    
                    if strIdToCheck == "\(dictData.value(forKey: "ReportTypeSysName") ?? "")" {
                        
                        let disclaimerIdTemp = "\(dictData.value(forKey: "DisclaimerId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  disclaimerMasterId == %@", strWoId , disclaimerIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // Discalimer Id Does not exist so insert data of Discalimer in DB
                            
                            saveDiscalimerDB(strDisclaimerId: disclaimerIdTemp, strMappingType: strMappingType, strToSave: strToSave)
                            
                        }
                        
                    }
                    
                //}
                
            }
            
        }
        
    }
    
    
    func saveDiscalimerDB(strDisclaimerId : String , strMappingType : String , strToSave : String) {
        
        var dictDataOfDiscalimerTemp = NSDictionary()
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        // Check if Deafult Disclaimer then remove it from list
        if ary.count != 0{
            
            for item in ary {
                
                let dictData = item as! NSDictionary
                
                if strDisclaimerId == "\(dictData.value(forKey: "DisclaimerMasterId") ?? "")" {
                    
                    dictDataOfDiscalimerTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfDiscalimerTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            arrOfKeys.add("isDefault")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            arrOfValues.add(false)

            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("disclaimerMasterId")
            arrOfKeys.add("disclaimerDescription")
            arrOfKeys.add("disclaimerName")
            arrOfKeys.add("isChanged")
            arrOfKeys.add("woWdoDisclaimerId")
            arrOfKeys.add("fillIn")
            arrOfKeys.add("isChanged")

            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "DisclaimerMasterId")!)")
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "Disclaimer")!)")
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "Title")!)")
            arrOfValues.add(false)
            arrOfValues.add(Global().getReferenceNumber())
            arrOfValues.add("")
            arrOfValues.add(false)
            
            if strToSave == "No" {
                
                let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
                
                arrOfDiclaimer.add(dict_ToSend)
                
                
                self.hghtConstTblViewDisclaimer.constant = CGFloat(210*self.arrOfDiclaimer.count)

                //
                tblviewDisclaimer.reloadData()
                
            }else{
                
                // Saving Disclaimer In DB
                saveDataInDB(strEntity: Entity_Disclaimer, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }
    
    func updateIsEditedMonthlyAmount() {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isEditedMonthlyAmount")
        arrOfValues.add("true")

        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
        }
        
    }
    
}


// MARK: - -----------------------------------Table View Delegates -----------------------------------

extension ProblemIdentification_WDOVC:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView.tag == 100)// Problem Identification tableview
        {
            return arrOfProblemIdentification.count
        }
        else if(tableView.tag == 102)// General Notes tableview
        {
            return arrOfGeneralNotes.count
        }
        else// Disclaimer tableview
        {
            return arrOfDiclaimer.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(tableView.tag == 100 && tableView == tblviewProblemIdentification)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProblemIdentificationCellNew") as! ProblemIdentificationCellNew

            if arrOfProblemIdentification[indexPath.row] is String {
                                
                cell.lblFinding.text = ""
                cell.lblFindingCode.text = ""
                cell.lblFindingFIlling.text = ""
                cell.lblRecomndationCode.text = ""

                cell.lblFinding_S.text = ""
                cell.lblFindingCode_S.text = ""
                cell.lblFindingFIlling_S.text = ""
                cell.lblRecomndationCode_S.text = ""
                cell.lblUnderline.backgroundColor = .clear

                cell.selectionStyle = .none
                cell.btnEdit.isHidden = true
                cell.btnDelete.isHidden = true
                cell.btnDelete.tag = indexPath.row
                cell.btnDelete.addTarget(self, action: #selector(deleteProblemIdentification), for: .touchUpInside)
                
                cell.btnEdit.tag = indexPath.row
                cell.btnEdit.addTarget(self, action: #selector(editProblemIdentification), for: .touchUpInside)
                
                return cell
                
            } else {
              
                let objArea = arrOfProblemIdentification[indexPath.row] as! NSManagedObject
                cell.btnEdit.isHidden = false
                cell.btnDelete.isHidden = false
               
                cell.lblFinding.text = "\(objArea.value(forKey: "issuesCode") ?? "N/A")" == "" ? "N/A" : "\(objArea.value(forKey: "issuesCode") ?? "N/A")"
                cell.lblFindingCode.text = "\(objArea.value(forKey: "subsectionCode") ?? "N/A")" == "" ? "N/A" : "\(objArea.value(forKey: "subsectionCode") ?? "N/A")"
                cell.lblFindingFIlling.text = "\(objArea.value(forKey: "findingFillIn") ?? "N/A")"
                cell.lblRecomndationCode.text =  "\(objArea.value(forKey: "recommendationCode") ?? "N/A")" == "" ? "N/A" : "\(objArea.value(forKey: "recommendationCode") ?? "N/A")"

                cell.lblFinding.sizeToFit()
                cell.lblFindingCode.sizeToFit()
                cell.lblFindingFIlling.sizeToFit()
                cell.lblRecomndationCode.sizeToFit()
                cell.lblFinding.numberOfLines = 3
                cell.lblFindingCode.numberOfLines = 3
                cell.lblFindingFIlling.numberOfLines = 3
                cell.lblRecomndationCode.numberOfLines = 3
//                cell.lblFinding.lineBreakMode  = .byWordWrapping
//                cell.lblFindingCode.lineBreakMode  = .byWordWrapping
//                cell.lblFindingFIlling.lineBreakMode  = .byWordWrapping
//                cell.lblRecomndationCode.lineBreakMode  = .byWordWrapping
//                cell.lblRecomndationCode.layoutIfNeeded()
//                cell.lblFindingFIlling.layoutIfNeeded()
//                cell.lblFindingCode.layoutIfNeeded()
//                cell.lblFinding.layoutIfNeeded()

                
                
                cell.lblFinding_S.text = "Finding:"
                cell.lblFindingCode_S.text = "Finding Code:"
                cell.lblFindingFIlling_S.text = "Finding Filling:"
                cell.lblRecomndationCode_S.text = "Recommendation Code:"
                if("\(objArea.value(forKey: "findingFillIn") ?? "N/A")" == ""){
                    cell.lblFindingFIlling_S.text = ""
                }
                cell.lblUnderline.backgroundColor = .lightGray

                cell.lblFinding_S.textColor = .black

                cell.lblFindingCode_S.textColor = .black
                cell.lblFindingFIlling_S.textColor = .black
                cell.lblRecomndationCode_S.textColor = .black

                cell.btnDelete.tag = indexPath.row
                cell.btnDelete.addTarget(self, action: #selector(deleteProblemIdentification), for: .touchUpInside)
                
                cell.btnEdit.tag = indexPath.row
                cell.btnEdit.addTarget(self, action: #selector(editProblemIdentification), for: .touchUpInside)
                
                cell.selectionStyle = .none
                
                if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                    cell.btnEdit.isEnabled = false
                    cell.btnDelete.isEnabled = false
                }else{
                    cell.btnEdit.isEnabled = true
                    cell.btnDelete.isEnabled = true
                }
                
                return cell
                
            }
            
        }else if(tableView.tag == 102 && tableView == tblViewGeneralNotes)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralNotesCell") as! GeneralNotesCell
            
            let objArea = arrOfGeneralNotes[indexPath.row] as! NSManagedObject
            
            cell.lblTitle.text = "Title: \(objArea.value(forKey: "title") ?? "N/A")"
            cell.txtViewGeneralNoteDesc.text = "Description: \(objArea.value(forKey: "generalNoteDescription") ?? "N/A")"
            
            let strReplacedString = replaceTags(strFillingLocal: "\(objArea.value(forKey: "fillIn") ?? "")", strFindingLocal: "\(objArea.value(forKey: "generalNoteDescription")!)")
            
            cell.txtViewGeneralNoteDesc.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
            
            
            cell.lblFillin.text = "Fillin: \(objArea.value(forKey: "fillIn") ?? "N/A")"

            if cell.lblFillin.text == "Fillin: <null>" {
                
                cell.lblFillin.text = "Fillin: "
                
            }
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(deleteGeneralNotes), for: .touchUpInside)
            
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(editGeneralNotes), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                cell.btnEdit.isEnabled = false
                cell.btnDelete.isEnabled = false
            }else{
                cell.btnEdit.isEnabled = true
                cell.btnDelete.isEnabled = true
            }
            
            return cell
            
        }else if(tableView == tblviewDisclaimer){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisclaimerCell") as! DisclaimerCell
            
            var isDefault = false
            
            if arrOfDiclaimer[indexPath.row] is NSDictionary {
                
                let objArea = arrOfDiclaimer[indexPath.row] as! NSDictionary
                cell.lblTitle.text = "\(objArea.value(forKey: "disclaimerName") ?? "N/A")"
                cell.lblDescription.text = "\(objArea.value(forKey: "disclaimerDescription") ?? "N/A")"
                let strReplacedString = replaceTags(strFillingLocal: "\(objArea.value(forKey: "fillIn")!)", strFindingLocal: "\(objArea.value(forKey: "disclaimerDescription")!)")
                cell.lblDescription.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
                
                cell.lblWhere.text = "\(objArea.value(forKey: "fillIn") ?? "N/A")"
                isDefault = (objArea.value(forKey: "isDefault") as! Bool)
                
            }else {
                
                let objArea = arrOfDiclaimer[indexPath.row] as! NSManagedObject
                cell.lblTitle.text = "\(objArea.value(forKey: "disclaimerName") ?? "N/A")"
                cell.lblDescription.text = "\(objArea.value(forKey: "disclaimerDescription") ?? "N/A")"
                let strReplacedString = replaceTags(strFillingLocal: "\(objArea.value(forKey: "fillIn")!)", strFindingLocal: "\(objArea.value(forKey: "disclaimerDescription")!)")
                cell.lblDescription.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
                cell.lblWhere.text = "\(objArea.value(forKey: "fillIn") ?? "N/A")"
                isDefault = (objArea.value(forKey: "isDefault") as! Bool)
            }
            
            /*let objArea = arrOfDiclaimer[indexPath.row] as! NSManagedObject
            cell.lblTitle.text = "\(objArea.value(forKey: "disclaimerName") ?? "N/A")"
            cell.lblDescription.text = "\(objArea.value(forKey: "disclaimerDescription") ?? "N/A")"
            let strReplacedString = replaceTags(strFillingLocal: "\(objArea.value(forKey: "fillIn")!)", strFindingLocal: "\(objArea.value(forKey: "disclaimerDescription")!)")
            cell.lblDescription.attributedText = htmlAttributedString(strHtmlString: strReplacedString)
            cell.lblWhere.text = "\(objArea.value(forKey: "fillIn") ?? "N/A")"*/
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(editDisclaimer), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(deleteDisclaimer), for: .touchUpInside)
            
            //makeCornerRadius(value: 2.0, view: cell.lblDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
            
            cell.selectionStyle = .none
            
            if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                cell.btnEdit.isEnabled = false
                cell.btnDelete.isEnabled = false
            }else{
                cell.btnEdit.isEnabled = true
                cell.btnDelete.isEnabled = true
                
                if isDefault{
                    
                    //cell.btnEdit.isEnabled = false
                    cell.btnDelete.isEnabled = false
                    
                }
                
            }
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            if(tableView.tag == 100)
            {
                // Problem
                
                let objArea = self.arrOfProblemIdentification[indexPath.row] as! NSManagedObject
                let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
                let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
                
                goToProblemIdentificationAddView(strToUpdate: "Yes", strProblemIdentificationIdLocal: strProblemIdentificationIdLocal,strMobileProblemIdentificationIdLocal: strMobileProblemIdentificationIdLocal)
                
            }
            else if(tableView.tag == 101)
            {
                //Disclaimer
                
                let objArea = self.arrOfDiclaimer[indexPath.row] as! NSManagedObject
                let strDisclaimerIdLocal = "\(objArea.value(forKey: "woWdoDisclaimerId") ?? "")"
                //goToDisclaimerAddView(strToUpdate: "Yes", strDisclaimerId: strDisclaimerIdLocal)
                
            }
            else if(tableView.tag == 102)
            {
                // General Notes
                
                let objArea = self.arrOfGeneralNotes[indexPath.row] as! NSManagedObject
                let strWdoGeneralNoteId = "\(objArea.value(forKey: "wdoGeneralNoteId") ?? "")"
                goToGeneralAddView(strToUpdate: "Yes", strGeneralNoteId: strWdoGeneralNoteId)
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView.tag == 100)
        {
            return tableView.rowHeight
        }
        else if(tableView.tag == 102)
        {
            return tableView.rowHeight
        }
        return 208.0
    }
    
    
}

// MARK:-
// MARK:-------------------------Selection_PopUpDelegate--------------------------

extension ProblemIdentification_WDOVC : CustomTableView
{
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if tag == 45 {
            
            
            if dictData["TIPWarrantyType"] as? String  == strSelectString {
                
                dictOfTipWarranty = NSDictionary()
                btnTipWarrantyType.setTitle("\(strSelectString)", for: .normal)
                strTipMasterId = ""
                strTipMasterName = ""
                
                // check to show monthly amount textfireld IsMonthlyPrice
                txtMonthlyAmount.isHidden = true
                txtMonthlyAmount.text = ""
                
            }else{
                
                dictOfTipWarranty = dictData
                btnTipWarrantyType.setTitle("\(dictOfTipWarranty.value(forKey: "TIPWarrantyType")!)", for: .normal)
                strTipMasterId = "\(dictOfTipWarranty.value(forKey: "TIPMasterId")!)"
                strTipMasterName = "\(dictOfTipWarranty.value(forKey: "TIPWarrantyType")!)"
                
                // check to show monthly amount textfireld IsMonthlyPrice
                
                let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                
                if isMonthlyAmount {
                    
                    txtMonthlyAmount.isHidden = false
                    
                } else {
                    
                    txtMonthlyAmount.isHidden = true
                    
                }
                
            }
            
        } else if tag == 49{
            
            // Sub Area Access
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtSubareaAccess.text = ""
                dictOfSubAreaAccess = NSDictionary()
                txtSubareaAccess.isHidden = true
                btnSubAreaAccess.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtSubareaAccess.text = "\(dictData.value(forKey: "Value")!)"
                dictOfSubAreaAccess = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == strOtherString {
                    txtSubareaAccess.isHidden = false
                    
                }else{
                    
                    txtSubareaAccess.isHidden = true
                    //txtSubareaAccess.text = "\(dictData.value(forKey: "Value")!)"

                }
                //btnSubAreaAccess.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
                btnSubAreaAccess.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)

            }
            
        } else if tag == 50{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtNumberOfStories.text = ""
                dictOfNumberOfStories = NSDictionary()
                txtNumberOfStories.isHidden = true
                btnNoOfStories.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtNumberOfStories.text = "\(dictData.value(forKey: "Value")!)"
                dictOfNumberOfStories = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == "0" {
                    txtNumberOfStories.isHidden = false
                }else{
                    txtNumberOfStories.isHidden = true
                    //txtNumberOfStories.text = "\(dictData.value(forKey: "Value")!)"

                }
                
                
            }
            //btnNoOfStories.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
            btnNoOfStories.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)

        } else if tag == 51{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtExternalMaterial.text = ""
                dictOfExternalMaterials = NSDictionary()
                txtExternalMaterial.isHidden = true
                btnExternalMaterial.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtExternalMaterial.text = "\(dictData.value(forKey: "Value")!)"
                dictOfExternalMaterials = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == strOtherString {
                    txtExternalMaterial.isHidden = false
                }else{
                    txtExternalMaterial.isHidden = true
                    //txtExternalMaterial.text = "\(dictData.value(forKey: "Value")!)"
                }
            }
            //btnExternalMaterial.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
            btnExternalMaterial.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)
        } else if tag == 52{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtRoofingMaterial.text = ""
                dictOfRoofingMaterials = NSDictionary()
                txtRoofingMaterial.isHidden = true
                btnRoofingMaterial.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtRoofingMaterial.text = "\(dictData.value(forKey: "Value")!)"
                dictOfRoofingMaterials = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == strOtherString {
                    txtRoofingMaterial.isHidden = false
                }else{
                    txtRoofingMaterial.isHidden = true
                    //txtRoofingMaterial.text = "\(dictData.value(forKey: "Value")!)"
                }
            }
            //btnRoofingMaterial.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
            btnRoofingMaterial.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)


        } else if tag == 53{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtCPCInspTagPosted.text = ""
                dictOfCpcTagPosted = NSDictionary()
                txtCPCInspTagPosted.isHidden = true
                btnCpcTagPosted.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtCPCInspTagPosted.text = "\(dictData.value(forKey: "Value")!)"
                dictOfCpcTagPosted = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == strOtherString {
                    txtCPCInspTagPosted.isHidden = false
                }else{
                    txtCPCInspTagPosted.isHidden = true
                    //txtCPCInspTagPosted.text = "\(dictData.value(forKey: "Value")!)"
                }
                
            }
            //btnCpcTagPosted.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
            btnCpcTagPosted.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)

        } else if tag == 54{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtPastPcoTagPosted.text = ""
                dictOfPastPcoTagPosted = NSDictionary()
                txtPastPcoTagPosted.isHidden = true
                btnPastPcoTagPosted.setTitle(strSelectString, for: .normal)

            }else{
                
                //txtPastPcoTagPosted.text = "\(dictData.value(forKey: "Value")!)"
                dictOfPastPcoTagPosted = dictData
                
                if "\(dictData.value(forKey: "Value") ?? "")" == strOtherString {
                    txtPastPcoTagPosted.isHidden = false
                }else{
                    txtPastPcoTagPosted.isHidden = true
                    //txtPastPcoTagPosted.text = "\(dictData.value(forKey: "Value")!)"
                }
                
            }
            //btnPastPcoTagPosted.setTitle("\(dictData.value(forKey: "Value") ?? "")", for: .normal)
            btnPastPcoTagPosted.setTitle("\(dictData.value(forKey: "Text") ?? "")", for: .normal)

        } else if tag == 55{
            
            if dictData["Value"] as? String  == strSelectString {
                
                txtviewStructureDescription.text = ""
                dictOfStructereDesc = NSDictionary()
                txtFldOther.isHidden = true
                
            }else{
                
                //txtviewStructureDescription.text = "\(dictData.value(forKey: "Value")!)"
                txtviewStructureDescription.text = "\(dictData.value(forKey: "Text")!)"
                dictOfStructereDesc = dictData
                
                if "\(dictData.value(forKey: "Text")!)".caseInsensitiveCompare("other") == .orderedSame
                {
                    txtFldOther.isHidden = false
                }
                else
                {
                    txtFldOther.isHidden = true
                }
                
            }

        } else if tag == 57{
            
            if dictData["Title"] as? String  == strSelectString {

                dictOfFollowUpInspection = NSDictionary()
                btnFollowUpInspection.setTitle("\(strSelectString)", for: .normal)
                
            }else{
                
                dictOfFollowUpInspection = dictData
                btnFollowUpInspection.setTitle("\(dictData.value(forKey: "Title")!)", for: .normal)
                
            }
            
        }
        else if tag == 68{ // For Garage
            if dictData.count == 0 {
                dictOfGarage = NSDictionary()
                btn_Garage.setTitle("\(strSelectString)", for: .normal)
            }else{
                dictOfGarage = dictData
                btn_Garage.setTitle("\(dictData.value(forKey: "Text")!)", for: .normal)
                
            }

       }
        generateGeneralDesc()
    }
    
}

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------


extension ProblemIdentification_WDOVC: DatePickerProtocol
{
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        
        if tag == 1 {
            
            txtDateOfPrevPco.text = strDate
            //strFromDate = strDate
            
        } else if tag == 2 {
            
            txtYearOfStructure.text = strDate
            //strToDate = strDate
            
        }
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension ProblemIdentification_WDOVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtYearOfStructure  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 3)
            
        }
        
        if textField == txtMonthlyAmount  {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
            
            self.updateIsEditedMonthlyAmount()

            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        
        if textField == txtNumberOfStories  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        generateGeneralDesc()
        if textField == txtYearOfStructure  {
            
            tblviewProblemIdentification.reloadData()
           
        }
        
    }
    
}

// MARK: - -----------------------------------UITextViewDelegate -----------------------------------
// MARK: -

extension ProblemIdentification_WDOVC : UITextViewDelegate{
   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        isGeneralDescriptionPrepare = true
        return true
    }
}
// MARK: - -----------------------------------UIWebViewDelegate -----------------------------------
// MARK: -

extension ProblemIdentification_WDOVC : UIWebViewDelegate{

    
    
}




//MARK:  RefreshGraph
extension ProblemIdentification_WDOVC : RefreshGraph {
    func refresh(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            showProblemIdentification()
        }
        
    }
    
    func refreshWebGraph(dict: NSDictionary) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            showProblemIdentification()
            saveGraphAsXML()

        }
        
    }
}


