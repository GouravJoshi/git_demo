//  Building Permit Fee
//  Agreement_WDOVC.swift
//  peSTream 2020 Changes
//  Created by Rakesh Jain on 16/09/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  2021 change by navin patidar kkdjjhfkd

import UIKit


class GraphImagesCell:UITableViewCell
{
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
}

class TargetCell:UITableViewCell
{
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
}


class PricingCell:UITableViewCell
{
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnAddToAgreement: UIButton!
    @IBOutlet weak var btnDiscount: UIButton!
    @IBOutlet weak var lblIssueCode: UILabel!
    @IBOutlet weak var lblRecommendationCode: UILabel!
    @IBOutlet weak var lblSubsectionCode: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblTitleDiscount: UILabel!
    
    @IBOutlet weak var lblTitleTotal: UILabel!
    @IBOutlet weak var btnBidOnRequest: UIButton!
    @IBOutlet weak var lblBidOnRequest: UILabel!

    
}

class Agreement_WDOVC: UIViewController {
    
    // outlet
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    
    // Letters
    @IBOutlet weak var webviewCoverLetter: UIWebView!
    @IBOutlet weak var webviewIntroduction: UIWebView!
    @IBOutlet weak var webviewSalesMarketingContent: UIWebView!
    
    
    // Personal Info
    @IBOutlet weak var lblPerson: UILabel!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblSalesRep: UILabel!
    @IBOutlet weak var lblServiceAddress: UILabel!
    
    @IBOutlet weak var btnServiceAddress: UIButton!
    @IBOutlet weak var lblPhonePersonalInfo: UILabel!
    
    @IBOutlet weak var btnPhonePersonalnfo: UIButton!
    
    @IBOutlet weak var btnAddCard: UIButton!
    @IBOutlet weak var lblCardType: UILabel!
    
    // Inspection Details
    
    @IBOutlet weak var lblOrderedBy: UILabel!
    
    @IBOutlet weak var lblAddressOrderBy: UILabel!
    @IBOutlet weak var lblPropertyOwner: UILabel!
    @IBOutlet weak var lblAddressPropertyOwner: UILabel!
    @IBOutlet weak var lblReportSentTo: UILabel!
    @IBOutlet weak var lblAddressReportSentTo: UILabel!
    @IBOutlet weak var lblReportType: UILabel!
    @IBOutlet weak var lblSubareaAccess: UILabel!
    @IBOutlet weak var lblNoOfStories: UILabel!
    @IBOutlet weak var lblExternalMaterial: UILabel!
    @IBOutlet weak var lblRoofingMaterial: UILabel!
    @IBOutlet weak var lblYearOfStructure: UILabel!
    @IBOutlet weak var lblYearUnknown: UILabel!
    @IBOutlet weak var lblBuildingPermit: UILabel!
    
    @IBOutlet weak var lblEmailAddressInspectionDetails: UILabel!
    @IBOutlet weak var lblCPCInspTagPosted: UILabel!
    @IBOutlet weak var lblPastPcoTagPosted: UILabel!
    
    @IBOutlet weak var lblNameOfPrevPCO: UILabel!
    
    @IBOutlet weak var lblDateOfPrevPCO: UILabel!
    
    @IBOutlet weak var lblGeneralDescription: UILabel!
    @IBOutlet weak var lblStructureDescription: UILabel!
    
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblTermites: UILabel!
    
    @IBOutlet weak var lblOther: UILabel!
    
    @IBOutlet weak var lblSeparateReport: UILabel!
    @IBOutlet weak var lblTipWarranty: UILabel!
    @IBOutlet weak var lblTipWarrantyType: UILabel!
    @IBOutlet weak var lblMonthlyAmount: UILabel!
    
    // Service Months
    @IBOutlet weak var viewContainerServiceMonths: CardView!
    @IBOutlet weak var lblServiceMonths: UILabel!
    
    // Graph & Images
    
    @IBOutlet weak var tblviewGraphAndImages: UITableView!
    
    // Pricing
    
    @IBOutlet weak var tblviewPricing: UITableView!
    @IBOutlet weak var lblTotalPricing: UILabel!
    
    // Others
    
    @IBOutlet weak var txtviewAdditionalNotes: UITextView!
    
    // Terms Of Services
    @IBOutlet weak var viewContainerTermsOfService: CardView!
    @IBOutlet weak var txtviewTermsOfServices: UITextView!
    
    // Price Information
    
    @IBOutlet weak var lblSubtotalPriceInformation: UILabel!
    @IBOutlet weak var lblOtherPriceInformation: UILabel!
    @IBOutlet weak var lblTipDiscountPriceInformation: UILabel!
    @IBOutlet weak var lblTaxPriceInformation: UILabel!
    @IBOutlet weak var lblTotalPriceInformation: UILabel!
    @IBOutlet weak var lblBillingAmountPriceInformation: UILabel!
    @IBOutlet weak var btnIsTaxable: UIButton!
    @IBOutlet weak var txtTotalTIPDiscount: ACFloatingTextField!
    @IBOutlet weak var txtBuildingPermitFee: ACFloatingTextField!
    @IBOutlet weak var lblBuildingPermitFeeText: UILabel!

    @IBOutlet var txtOtherDiscount: ACFloatingTextField!
    // Payment Mode
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var btnAutochargeCustomer: UIButton!
    @IBOutlet weak var btnCollectAtTimeOfScheduling: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var txtAmountPaymentMode: ACFloatingTextField!
    @IBOutlet weak var txtCheckNoPaymentMode: ACFloatingTextField!
    @IBOutlet weak var txtDrivingLicenceNoPaymentMode: ACFloatingTextField!
    @IBOutlet weak var btnSelectExpirationdate: UIButton!
    
    
    // Signature
    
    @IBOutlet weak var btnCustomerNotPresent: UIButton!
    @IBOutlet weak var lblDate_Signature: UILabel!
    @IBOutlet weak var imgviewCustomerSign: UIImageView!
    @IBOutlet weak var imgviewTechnicianSign: UIImageView!
    @IBOutlet weak var viewCustomerSign: UIView!
    @IBOutlet weak var viewTechnicianSign: UIView!
    @IBOutlet weak var btnCustomerSign: UIButton!
    @IBOutlet weak var btnTechnicianSign: UIButton!
    
    // Terms & Conditions
    
    @IBOutlet weak var txtviewTermsAndConditions: UITextView!
    @IBOutlet weak var btnTermsAndConditionsCheckMark: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var viewContainerElectronicFormBtn: UIView!
    
    
    // NSLayoutConstraint
    
    @IBOutlet weak var hghtContWebViewCoverLetter: NSLayoutConstraint!
    @IBOutlet weak var hghtConstWebViewIntroLetter: NSLayoutConstraint!
    @IBOutlet weak var hghtConstWebViewMarketingContent: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerCheckDetails: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerServiceMonths: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerTermsOfService: NSLayoutConstraint!
    @IBOutlet weak var hghtConstTblViewGraphAndImage: NSLayoutConstraint!
    
    @IBOutlet weak var hghtConstViewContainerPrice: NSLayoutConstraint!
    
    @IBOutlet weak var hghtConstTargetHeader: NSLayoutConstraint!
    
    
    @IBOutlet weak var const_ElectronicFormView: NSLayoutConstraint!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnAllowCustomerToMakeSelection: UIButton!
    
    @IBOutlet weak var lblAllowCustomerToMakeSelection: UILabel!
    
    @IBOutlet weak var const_HghtMainGraphImage: NSLayoutConstraint!
    
    @IBOutlet weak var mainGraphImageView: UIImageView!
    
    @IBOutlet weak var constHghtTargetTblView: NSLayoutConstraint!
    
    @IBOutlet weak var tblViewTarget: UITableView!
    
    @IBOutlet weak var viewTargetHeader: UIView!
    
    @IBOutlet weak var btnMainGraph: UIButton!
    
    @IBOutlet weak var lblLeadInspectionFee: UILabel!
    
    @IBOutlet weak var HghtConstLeadInspectionFee: NSLayoutConstraint!
    
    @IBOutlet weak var btn_YesApplyTIP: UIButton!
    
    @IBOutlet weak var btn_NoApplyTIP: UIButton!
    
    @IBOutlet weak var HghtConstBuldingPermitFee: NSLayoutConstraint!

    @IBOutlet weak var HghtConstBuldingPermitView: NSLayoutConstraint!
    @IBOutlet weak var HghtConstLeadInspectionFeeView: NSLayoutConstraint!
    @IBOutlet weak var buldingPermitFeeLineView: UIView!
    @IBOutlet weak var leadInspectionFeeLineView: UIView!
    
    @IBOutlet var viewVerbalApproval: UIView!
    
    @IBOutlet var btnCheckBoxVerbalApproval: UIButton!
    
    @IBOutlet var viewVerbalApprovalFrom: UIStackView!
    @IBOutlet var btnVerbalApprovalFrom: UIButton!
    
    //Nilind
    @IBOutlet var viewBuildingPermitViewNew: UIView!
    
    @IBOutlet var const_ConverLetter_Top: NSLayoutConstraint!
    
    @IBOutlet var const_IntroductionLetter_Top: NSLayoutConstraint!
    @IBOutlet var const_SalesMarketingContent_Top: NSLayoutConstraint!
    @IBOutlet var const_ViewPersonalInfo_Top: NSLayoutConstraint!
    @IBOutlet weak var lblGarage: UILabel!
    @IBOutlet weak var stk_TipWarrenty: UIStackView!
    @IBOutlet weak var lbl_ApplyTIP_Yes: UILabel!
    @IBOutlet weak var lbl_ApplyTIP_No: UILabel!
    @IBOutlet weak var lbl_TIP_Title_InspectionDetail: UILabel!
    @IBOutlet weak var lbl_TIP_Title_PriceDetail: UILabel!
    @IBOutlet weak var lbl_ApplyTIP_Title: UILabel!


    // variables
    //array
    private  var arrayCoverLetter = NSMutableArray()
    private  var arrayIntroduction = NSMutableArray()
    private  var arrayMarketingContent = NSMutableArray()
    private  var arrayTermsOfService = NSMutableArray()
    private  var arrayTermsAndConditions = NSMutableArray()
    private  var arrayTermsAndConditionsMultiSelected = NSMutableArray()
    private  var arrayOfImages = NSArray()
    private  var arrayOfTargets = NSArray()
    private  var arrayProblemIdentification = NSMutableArray()
    private  var arrayProblemIdentificationPricing = NSMutableArray()
    fileprivate var arrayRecommendationMaster = NSMutableArray()
    
    var arySelectedEmployee = NSMutableArray()
    var aryEmployeeList = NSMutableArray()
    // string
    @objc var strWoId = NSString ()
    private  var strTermsConditions = ""
    private var strGlobalPaymentMode = ""
    fileprivate var strCheckFrontImage = ""
    fileprivate var strCheckBackImage = ""
    fileprivate var signatureType = ""
    fileprivate var strCustomerSign = ""
    fileprivate var strTechnicianSign = ""
    fileprivate var strEmpName = ""
    fileprivate var strEmpID = ""
    fileprivate var strUserName = ""
    fileprivate var strCompanyKey = ""
    fileprivate var strLeadStatusGlobal = ""
    fileprivate var strStageSysName = ""
    fileprivate var strExpirationDate = ""
    fileprivate var strIsPresetWO = ""
    fileprivate var strIsFormFilled = ""
    fileprivate var strAccountNo = ""
    fileprivate var strAudioName = ""
    fileprivate var strAudioNameServiceAuto = ""
    fileprivate var strCompanyLogoPath = ""
    fileprivate var strBranchLogoImagePath = ""
    fileprivate var strCustomerCompanyProfileImage = ""
    fileprivate var strLeadNumber = ""
    fileprivate var strAccountManagerName = ""
    fileprivate var strAccountManagerEmail = ""
    fileprivate var strAccountManagerPrimaryPhone = ""
    fileprivate var strCellNo = ""
    fileprivate var strCompanyAddress = ""
    fileprivate var strBillingPocName = ""
    fileprivate var strApplyTIP = "" //strApplyTIP
    fileprivate var strOtherDiscountFromLeadDetail = "" //strApplyTIP

    
    var strLeadId = ""
    var strForProposal = ""
    var strInspectorId = ""
    var strCustomerName = ""
    
    // bool
    fileprivate var chkFrontImage = false
    fileprivate var chkCustomerNotPresent = false
    fileprivate var isPreferredMonths = false
    fileprivate var isTermsOfService = false
    fileprivate var isPreSetSignGlobal = false;
    fileprivate var yesEditedSomething = false
    fileprivate var isApplyTIPShow = false
    fileprivate var isVerbalApproval = false

    // dictionary
    var dictLoginData = NSDictionary()
    var dictRateMaster = NSDictionary()

    
    // class variable
    fileprivate let global = Global()
    
    // managed object
    var objWorkorderDetail = NSManagedObject()
    var matchesGeneralInfo = NSManagedObject()
    var isGoToPriceJustification = false

    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnVerbalApprovalFrom.setTitle("----Select----", for: .normal)
        HghtConstBuldingPermitFee.constant = 0.0
        //HghtConstBuldingPermitView.constant = 0.0
        lblBuildingPermitFeeText.isHidden = true
        buldingPermitFeeLineView.isHidden = true
        viewBuildingPermitViewNew.isHidden = true
        stk_TipWarrenty.isHidden = true
       
        
        const_HghtMainGraphImage.constant = 0
        
        // Do any additional setup after loading the view.
        
        configureUI()
        
        // Master
        getLetterTemplateAndTermsOfService()
        
        getRecommendationMaster()
        
        
        lblOrderNumber.text = nsud.value(forKey: "lblName") as? String
        
        //        strLeadStatusGlobal = "\(nsud.value(forKey: "leadStatusSales") ?? "")"
        //        strStageSysName = "\(nsud.value(forKey: "stageSysName") ?? "")"
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            strLeadStatusGlobal = "Complete";
        }
        let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
        let fullAccess = opprtunityAccess.status
        let limitedAccess = opprtunityAccess.opportunityLimitedAccess

        if fullAccess && limitedAccess{
            disableUserInteration()
        }
        else if(!fullAccess && limitedAccess){
            disableUserInteration()
            accessWhenOpportunityCompletwPending()
        }else{
            
        }
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        lblSalesRep.text = strEmpName
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        isPreferredMonths = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPreferredMonth") as! Bool
        
        isTermsOfService = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsTermsOfService") as! Bool
        
        strCompanyLogoPath = "\(dictLoginData.value(forKeyPath: "Company.LogoImagePath") ?? "")"
        
        strBranchLogoImagePath = "\(dictLoginData.value(forKeyPath: "EmployeeBranchLogoImagePath") ?? "")"
        
        
        let strCompanyAdd1 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine1") ?? "")"
        
        let strCompanyAdd2 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine2") ?? "")"
        
        let strCompanyCountry = "\(dictLoginData.value(forKeyPath: "Company.CountryName") ?? "")"
        
        let strCompnayCity = "\(dictLoginData.value(forKeyPath: "Company.CityName") ?? "")"
        
        let strCompanyState = "\(dictLoginData.value(forKeyPath: "Company.StateName") ?? "")"
        
        let strCompnayZipCode = "\(dictLoginData.value(forKeyPath: "Company.ZipCode") ?? "")"
        
        
        if(strCompanyAdd1.count > 0)
        {
            strCompanyAddress = strCompanyAdd1
        }
        if(strCompanyAdd2.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyAdd2 : strCompanyAdd2
        }
        if(strCompanyCountry.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyCountry : strCompanyCountry
        }
        
        if(strCompnayCity.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayCity : strCompnayCity
        }
        
        if(strCompanyState.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyState : strCompanyState
        }
        
        if(strCompnayZipCode.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayZipCode : strCompnayZipCode
        }
        
        if(isPreferredMonths == false)
        {
            hghtConstViewContainerServiceMonths.constant = 0.0
            viewContainerServiceMonths.isHidden = true
        }
        else
        {
            hghtConstViewContainerServiceMonths.constant = 140
            viewContainerServiceMonths.isHidden = false
        }
        
        if(isTermsOfService == false)
        {
            hghtConstViewContainerTermsOfService.constant = 0.0
            viewContainerTermsOfService.isHidden = true
        }
        else
        {
            hghtConstViewContainerTermsOfService.constant = 240
            viewContainerTermsOfService.isHidden = false
        }
        
        // local db
        fetchLeadDetailFromLocalDB()
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            strLeadStatusGlobal = "Complete";
            
        }
        let opprtunityAccess1 = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
        let fullAccess1 = opprtunityAccess1.status
        let limitedAccess1 = opprtunityAccess1.opportunityLimitedAccess

        if fullAccess1 && limitedAccess1{
            disableUserInteration()
        }
        else if(!fullAccess1 && limitedAccess1){
            disableUserInteration()
            accessWhenOpportunityCompletwPending()
        }else{
            
        }
        
        
        fetchInspectionDetail()
        fetchWoWdoProblemIdentificationExtSerDcs()
        fetchMultiTermsAndConditionsFromLocalDB()
        fetchPaymentInfoFromLocalDB()
        fetchLetterTemplateFromLocalDB()
        fetchMarketingContentFromLocalDB()
       
        //Nilind
        showPriceInformation()
        //showPriceInformationInitial()
        
        if("\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")".count > 0)
        {
            strAudioNameServiceAuto = "\(objWorkorderDetail.value(forKey: "audioFilePath")!)"
        }
        
        fetchTargets()
        functionForVerbalApproval()
        
        
        let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

        
        if(aryRateMasterExtSerDcs.count != 0)
        {
            dictRateMaster = aryRateMasterExtSerDcs.firstObject as! NSDictionary
        }
        
        if const_ConverLetter_Top.constant == 0 && const_IntroductionLetter_Top.constant == 0 && const_SalesMarketingContent_Top.constant == 0
        {
            const_ViewPersonalInfo_Top.constant = 0
        }
    }
    
    func functionForVerbalApproval()  {
        
        aryEmployeeList = getEmployeeList()
        
        btnCheckBoxVerbalApproval.setImage(UIImage(named: "uncheck_ipad"), for: .normal)

        if isInternetAvailable()
        {
            viewVerbalApproval.isHidden = true //verbal approval show nhi krna he
        }
        else
        {
            
            if ("\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "1" || "\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "true") {
                
                let message = checkForRequestPricingApprovalNew()
                
                if message.count > 0 {
                    
                    viewVerbalApproval.isHidden = false //verbal approval show krna he
                    
                } else {
                    
                    viewVerbalApproval.isHidden = true //verbal approval show nhi krna he
                    
                }
                
                
            }else{
                
                viewVerbalApproval.isHidden = true //verbal approval show nhi krna he
                
            }
            
        }
        
        if "\(objWorkorderDetail.value(forKey: "isVerbalApproval") ?? "")" == "true"
        {
            btnCheckBoxVerbalApproval.setImage(UIImage(named: "check_ipad"), for: .normal)
            viewVerbalApprovalFrom.isHidden = false
            isVerbalApproval = true
        }
        else
        {
            btnCheckBoxVerbalApproval.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            viewVerbalApprovalFrom.isHidden = true
            isVerbalApproval = false
            
        }

        let aryTemp = aryEmployeeList.filter({ (dict) -> Bool in
            
            return "\((dict as! NSDictionary).value(forKey: "EmployeeId") ?? "")" == "\(objWorkorderDetail.value(forKey: "verbalApprovalBy") ?? "")"
        }) as NSArray
        
        if aryTemp.count > 0
        {
            
            arySelectedEmployee = aryTemp.mutableCopy() as! NSMutableArray
            btnVerbalApprovalFrom.setTitle("\((arySelectedEmployee.object(at: 0) as! NSDictionary).value(forKey: "FullName") ?? "")", for: .normal)
        }
        else
        {
            btnVerbalApprovalFrom.setTitle("----Select----", for: .normal)
        }
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if (nsud.value(forKey: "loadeDataRefreshAgreementView") != nil) {
            let yesupdatedWdoInspection = nsud.bool(forKey: "loadeDataRefreshAgreementView")
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "loadeDataRefreshAgreementView")
                nsud.synchronize()
                
                // Reload Data if Loaded Data
                fetchInspectionDetail()
                fetchWoWdoProblemIdentificationExtSerDcs()
                
                //Nilind
                showPriceInformation()
                //showPriceInformationInitial()

            }
        }
        
        //For Audio
        if (nsud.value(forKey: "yesAudio") != nil)
        {
            yesEditedSomething = true
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio
            {
                
                strAudioName = nsud.value(forKey: "AudioNameWdoSales") as! String
                strAudioNameServiceAuto = nsud.value(forKey: "AudioNameService") as! String

            }
            
        }
        
        // Check if from send mail and agreement synced and move to finalize report  YesWdoAgreementSynced
        
        //nsud.set(true, forKey: "synAgreementWdo")
        //nsud.synchronize()
        
        if (nsud.value(forKey: "isFromBackSendMailWdo") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "isFromBackSendMailWdo")
            
            if isScannedCode {
                
                fetchLeadDetailFromLocalDB()
                
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    
                    strLeadStatusGlobal = "Complete";
                }
                let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
                let fullAccess = opprtunityAccess.status
                let limitedAccess = opprtunityAccess.opportunityLimitedAccess

                if fullAccess && limitedAccess{
                    disableUserInteration()
                }
                else if(!fullAccess && limitedAccess){
                    disableUserInteration()
                    accessWhenOpportunityCompletwPending()
                }else{
                    
                }
                
                nsud.set(false, forKey: "isFromBackSendMailWdo")
                nsud.synchronize()
                
            }else{
                
                methodOnViewAppearAfterSynData()
                
            }
            
        }else{
            
            methodOnViewAppearAfterSynData()
            
        }
        
        showHideElectronicView()
        

        //Fwd: Agreements access for inspectors.

        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            strLeadStatusGlobal = "Complete";
          
        }
        let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
        let fullAccess = opprtunityAccess.status
        let limitedAccess = opprtunityAccess.opportunityLimitedAccess
        if fullAccess && limitedAccess{
            disableUserInteration()
        }
        else if(!fullAccess && limitedAccess){
            disableUserInteration()
            accessWhenOpportunityCompletwPending()
        }else{
            
        }
        
        
//        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
//        {
//            disableUserInteraction()
//        }
        
    }
    
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func action_Cash(_ sender: UIButton) {
        
        yesEditedSomething = true
        cashPaymentMode()
    }
    
    @IBAction func action_Check(_ sender: UIButton) {
        
        yesEditedSomething = true
        checkPaymentMode()
    }
    
    @IBAction func action_CreditCard(_ sender: UIButton) {
        
        yesEditedSomething = true
        creditCardPaymentMode()
    }
    
    @IBAction func action_AutochargeCustomer(_ sender: UIButton) {
        
        yesEditedSomething = true
        autoChargeCustomerPaymentMode()
    }
    
    
    @IBAction func action_collectAtTimeOfScheduling(_ sender: UIButton) {
        
        yesEditedSomething = true
        collectAtTimeOfSchedulingPaymentMode()
    }
    
    @IBAction func action_Invoice(_ sender: UIButton) {
        
        yesEditedSomething = true
        invoicePaymentMode()
    }
    
    
    @IBAction func action_selectExpirationDate(_ sender: UIButton) {
        
        yesEditedSomething = true
        self.view.endEditing(true)
        self.gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    @IBAction func action_checkFrontImage(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        self.view.endEditing(true)
        
        chkFrontImage = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func check_BackImage(_ sender: UIButton) {
        
        
        yesEditedSomething = true
        
        self.view.endEditing(true)
        
        chkFrontImage = false
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func action_CustomerNotPresent(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            viewCustomerSign.isHidden = false
            btnCustomerSign.isHidden = false
            btnTermsAndConditionsCheckMark.isHidden = false
            btnTermsAndCondition.isHidden = false
            chkCustomerNotPresent = false
            btnAllowCustomerToMakeSelection.isHidden = true
            lblAllowCustomerToMakeSelection.isHidden = true
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            viewCustomerSign.isHidden = true
            btnCustomerSign.isHidden = true
            btnTermsAndConditionsCheckMark.isHidden = true
            btnTermsAndCondition.isHidden = true
            chkCustomerNotPresent = true
            btnAllowCustomerToMakeSelection.isHidden = false
            lblAllowCustomerToMakeSelection.isHidden = false
        }
        
        showHideElectronicView()
        
    }
    
    
    @IBAction func action_customerSignature(_ sender: UIButton) {
        self.view.endEditing(true)
        
        yesEditedSomething = true
        signatureType = "customer"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    @IBAction func action_technicianSignature(_ sender: UIButton) {
        
        yesEditedSomething = true
        self.view.endEditing(true)
        
        signatureType = "technician"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func action_termsAndConditionsCheckMark(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_IAgreeTermsAndConditions(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(arrayTermsAndConditionsMultiSelected.count > 0)
        {
            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "TermsAndConditions_WDOVC") as? TermsAndConditions_WDOVC
            
            testController?.arrayTermsAndCondaitions = arrayTermsAndConditionsMultiSelected
            
            present(testController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func action_electronicAuthorizationForm(_ sender: UIButton) {
        
        
        yesEditedSomething = true
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            
            let storyboardIpad = UIStoryboard.init(name: "ElectronicAuthorization_iPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPad") as! ElectronicAuthorization_iPad
            
            // testController.strLeadId = strLeadId
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
            {
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
            else
            {
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    
    @IBAction func action_ContinueToFinalizeReport(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        updateWoDetail()
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            goToFinalizereport()

            
        }
        else
        {
            // check if is Added to agreement
            
            let isAddedToAgreement = checkIsAddToAgreement()
            
            if isAddedToAgreement {
                
                let isDiscountGreater = checkIfDiscountGreater()
                
                if isDiscountGreater {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Discount amount should be less then Subtotal amount", viewcontrol: self)
                    
                }
                else{
                    
                    finalSaveWithVerbalApprovalCheck()
                    
                }
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please select atleast one service to the agreement", viewcontrol: self)
                
            }
        }
        
    }
    
    @IBAction func action_image(_ sender: UIButton) {
        
        yesEditedSomething = true
        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: UIButton) {
        
        goToServiceHistory()
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: UIButton) {
        goToCustomerSalesDocuments()
    }
    
    @IBAction func action_graph(_ sender: UIButton) {
        
        yesEditedSomething = true
        self.goToGlobalmage(strType: "Graph")
    }
    
    @IBAction func action_NotesHistory(_ sender: UIButton) {
        goToNotesHistory()
    }
    
    @IBAction func action_SaveContinue(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        updateWoDetail()
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            goToFinalizereport()

         
           }
        else
        {
            // check if is Added to agreement
            
            let isAddedToAgreement = checkIsAddToAgreement()
            
            if isAddedToAgreement {
                
                let isDiscountGreater = checkIfDiscountGreater()
                
                if isDiscountGreater {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Discount amount should be less then Subtotal amount", viewcontrol: self)
                    
                }
                else{
                    
                    // check if Apply TIP selcted
                    
                    if isApplyTIPShow {
                        
                        if strApplyTIP.count == 0 {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please select Apply TIP/THPS Charge Yes or No", viewcontrol: self)
                            
                        }
                        else
                        {
                            finalSaveWithVerbalApprovalCheck()
                        }
                        
                    }else{
                        
                        finalSaveWithVerbalApprovalCheck()
                        
                    }
                    
                }

            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please select atleast one service to the agreement", viewcontrol: self)

            }
        }
        
    }
    
    @objc func action_AddToAgreement(_ sender: UIButton){
        
        yesEditedSomething = true
        
        let obj = arrayProblemIdentification.object(at: sender.tag) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            obj.setValue(false, forKey: "isAddToAgreement")
        }
        else
        {
            obj.setValue(true, forKey: "isAddToAgreement")
        }
        
        arrayProblemIdentification.replaceObject(at: sender.tag, with: obj)
        
        showPriceInformation()
        
        tblviewPricing.reloadData()
        
    }
    
    
    @IBAction func action_Audio(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
        }else{
            
            alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                
                let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                testController?.strFromWhere = flowTypeWdoSalesService
                        testController?.modalPresentationStyle = .fullScreen
                self.present(testController!, animated: false, completion: nil)
                
            }))
            
        }
        
        
        alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
            
            var audioName = nsud.value(forKey: "AudioNameWdoSales")
            
            audioName = self.strAudioName
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
            testController!.strAudioName = audioName as! String
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    
    @IBAction func action_BillingAddress(_ sender: UIButton) {
        
        let strAddress = "\(btnBillingAddress.titleLabel?.text ?? "")" as String
        
        if (strAddress.count>0)
        {
            global.redirect(onAppleMap: self, strAddress)
            
        }
    }
    
    @IBAction func action_Email(_ sender: UIButton) {
        
        let strEmail = "\(btnEmail.titleLabel?.text ?? "")" as String
        
        if (strEmail.count>0)
        {
            global.emailComposer(strEmail, "", "", self)
            
        }
    }
    
    @IBAction func actionServiceAddress(_ sender: UIButton) {
        
        let strAddress = "\(btnServiceAddress.titleLabel?.text ?? "")" as String
        
        if (strAddress.count>0)
        {
            global.redirect(onAppleMap: self, strAddress)
            
        }
    }
    
    @IBAction func action_PhonePersonalInfo(_ sender: UIButton) {
        
        let strNo = "\(btnPhonePersonalnfo.titleLabel?.text ?? "")" as String
        
        if (strNo.count>0)
        {
            global.calling(strNo)
            
        }
    }
    
    
    @IBAction func action_isTaxable(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
        }
        else
        {
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
        
    }
    
    @IBAction func action_AllowCustomerToMakeSelection(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            sender.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        else
        {
            sender.setImage(UIImage(named: "checked.png"), for: .normal)
        }
        
    }
    
    @IBAction func action_MainGraphPreview(_ sender: Any) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = mainGraphImageView.image!
                
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    @IBAction func action_YesApplyTIP(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btn_NoApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtTotalTIPDiscount.isEnabled = false
        strApplyTIP = "true"
        
    }
    
    @IBAction func action_NoApplyTIP(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btn_YesApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtTotalTIPDiscount.isEnabled = true
        strApplyTIP = "false"

    }
    
    @IBAction func actionOnChkBoxVerbalApproval(_ sender: Any) {
        
        if btnCheckBoxVerbalApproval.currentImage == UIImage(named: "check_ipad")
        {
            btnCheckBoxVerbalApproval.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            viewVerbalApprovalFrom.isHidden = true
            isVerbalApproval = false

        }
        else
        {
            btnCheckBoxVerbalApproval.setImage(UIImage(named: "check_ipad"), for: .normal)
            viewVerbalApprovalFrom.isHidden = false
            isVerbalApproval = true
        }
    }
    
    @IBAction func actionOnBtnVerbalApprovalFrom(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if aryEmployeeList.count == 0
        {
            
        }
        else
        {
            openTableViewPopUp(tag: 1105, ary: aryEmployeeList , aryselectedItem: arySelectedEmployee)

        }
    }
    
    @IBAction func action_RefreshWorkOrder(_ sender: Any) {
        refreshWorkOrderStatus(view: self, strWOID: strWoId as String)
    }
    
    @IBAction func action_AddCard(_ sender: Any) {
        
        print("Add Card")
        self.goToPestPac_PaymentIntegration()

    }
    
    fileprivate func getEmployeeList() -> NSMutableArray {
        var aryEmployeeList = NSMutableArray()
        if let aryTemp = nsud.value(forKeyPath: "EmployeeList"){
            if(aryTemp is NSArray){
                if((aryTemp as! NSArray).count > 0){
                   
                    let resultPredicate = NSPredicate(format: "IsActive == true")
                    let result = (aryTemp as! NSArray).filtered(using: resultPredicate)
                    aryEmployeeList = (result as NSArray).mutableCopy() as! NSMutableArray
                }
            }
        }
        
        return aryEmployeeList
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    // MARK: --------------------------------------------------
    // MARK: syncBasicInfoToServiceAuto before agreement sync
    func syncBasicInfoToServiceAuto() {
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/ManageWoWdoInspectionBasicDetail"
      
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0
        {
            let objInspectionDetail = arrOfWdoInspection[0] as! NSManagedObject
        
            let woWdoInspectionId = "\(objInspectionDetail.value(forKey: "woWdoInspectionId") ?? "")"
            if(woWdoInspectionId != "" && woWdoInspectionId == "0"){
                let workOrderId = strWoId
                let tIPMasterId = "\(objInspectionDetail.value(forKey: "tIPMasterId") ?? "")"
                let monthlyAmount = "\(objInspectionDetail.value(forKey: "monthlyAmount") ?? "")"
                let tIPTHPSType = "\(objInspectionDetail.value(forKey: "tIPTHPSType") ?? "")"
                var isSeparateReport = "\(objInspectionDetail.value(forKey: "isSeparateReport") ?? "")"
                if isSeparateReport.lowercased() == "true" || isSeparateReport == "1"{
                    isSeparateReport = "true"
                }else{
                    isSeparateReport = "false"

                }
                var isYearUnknown = "\(objInspectionDetail.value(forKey: "isYearUnknown") ?? "")"
                if isYearUnknown.lowercased() == "true" || isYearUnknown == "1"{
                    isYearUnknown = "true"
                }else{
                    isYearUnknown = "false"

                }
                var isBuildingPermit = "\(objInspectionDetail.value(forKey: "isBuildingPermit") ?? "")"
                if isBuildingPermit.lowercased() == "true" || isBuildingPermit == "1"{
                    isBuildingPermit = "true"
                }else{
                    isBuildingPermit = "false"

                }
                
                let ReportTypeId = "\(objInspectionDetail.value(forKey: "reportTypeId") ?? "")"
                var isTipWarranty = "\(objInspectionDetail.value(forKey: "isTipWarranty") ?? "")"
                if isTipWarranty.lowercased() == "true" || isTipWarranty == "1"{
                    isTipWarranty = "true"
                }else{
                    isTipWarranty = "false"

                }
                
                
                let dictSend = NSMutableDictionary()
            dictSend.setValue("\(woWdoInspectionId)", forKey: "WoWdoInspectionId")
            dictSend.setValue("\(workOrderId)", forKey: "WorkOrderId")
            dictSend.setValue("\(tIPMasterId)", forKey: "TIPMasterId")
            dictSend.setValue("\(monthlyAmount)", forKey: "MonthlyAmount")
            dictSend.setValue("\(tIPTHPSType)", forKey: "TIPTHPSType")
            dictSend.setValue("\(isSeparateReport)", forKey: "IsSeparateReport")
            dictSend.setValue("\(isYearUnknown)", forKey: "IsYearUnknown")
            dictSend.setValue("\(isBuildingPermit)", forKey: "IsBuildingPermit")
            dictSend.setValue("\(ReportTypeId)", forKey: "ReportTypeId")
            dictSend.setValue("\(isTipWarranty)", forKey: "IsTipWarranty")
                dictSend.setValue("\(global.getUserName() ?? "")", forKey: "ModifiedBy")
            dictSend.setValue("", forKey: "CreatedDate")
                dictSend.setValue("\(global.getCurrentDate() ?? "")", forKey: "ModifieDate")
            dictSend.setValue("", forKey: "CreatedBy")
                
                if (isInternetAvailable()){
                    let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    
                       // self.present(loader, animated: false, completion: nil)
                    
                    
                    WebService.callAPIBYPOST(parameter: dictSend, url: strURL) { (response, status) in
                      //  loader.dismiss(animated: false) { [self] in
                            print(response)
                            if(status){
                                
                            }else{
                             //   showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertSomeError, viewcontrol: self)
                            }
                    //    }
                          
                      
                    }
                }
            }
          
        }


    }
    
    
    
    
    // MARK: functions
    
    func checkIfDiscountGreater() -> Bool {
        
        var isDiscountGreater = false
        
        //if( (((lblOtherPriceInformation.text! as NSString).floatValue + (txtTotalTIPDiscount.text! as NSString).floatValue) > (lblSubtotalPriceInformation.text! as NSString).floatValue) )
        if( ((((txtOtherDiscount.text ?? "") as NSString).floatValue + (txtTotalTIPDiscount.text! as NSString).floatValue) > (lblSubtotalPriceInformation.text! as NSString).floatValue) )

        {
           
            isDiscountGreater = true
            
        }
        
        return isDiscountGreater
    }
    
    func checkIsAddToAgreement() -> Bool {
        
        var isAddToAgreement = false

        if arrayProblemIdentification.count > 0 {
            
            for k1 in 0 ..< arrayProblemIdentification.count {
                
                let dictData = arrayProblemIdentification[k1] as! NSDictionary
                
                if let isAgreement = dictData.value(forKey: "isAddToAgreement")
                {
                    if(dictData.value(forKey: "isAddToAgreement") as! Bool == true)
                    {

                        print(isAgreement)
                        isAddToAgreement = true
                        break
                    }
                }
            }
            
        }
        
        return isAddToAgreement
        
    }
    
    /* fileprivate func goToCreditCardScreen()
     {
     
     if isInternetAvailable() == true
     {
     let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
     let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
     
     var strValue = String()
     if chkCustomerNotPresent == true
     {
     strValue = "no"
     }
     else
     {
     strValue = "yes"
     }
     objCreditCardView?.isCustomerPresent = strValue as NSString
     objCreditCardView?.strAmount = txtAmountPaymentMode.text
     objCreditCardView?.strGlobalWorkOrderId = strWoId as String
     objCreditCardView?.strTypeOfService = "service"
     objCreditCardView?.workOrderDetailNew = objWorkorderDetail
     self.navigationController?.pushViewController(objCreditCardView!, animated: false)
     
     }
     else
     {
     showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
     }
     
     }*/
    
    func showHideElectronicView() {
        
        var isHideView = false
        
        let shouldShowElectronicFormLink = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElectronicAuthorizationForm") as! Bool
        
        if(shouldShowElectronicFormLink == true)
        {
            if(strIsFormFilled == "true")
            {
                if(methodToCheckIfElectronicFormExistsForLeadId(strleadid: strLeadId) == true)
                {
                    viewContainerElectronicFormBtn.isHidden = false
                    isHideView = false
                    
                }
                else
                {
                    viewContainerElectronicFormBtn.isHidden = true
                    isHideView = true
                }
            }
            else
            {
                viewContainerElectronicFormBtn.isHidden = false
                isHideView = false
            }
        }
        else
        {
            viewContainerElectronicFormBtn.isHidden = true
            isHideView = true
        }
        
        var isHideTermsView = false
        
        if(btnCustomerNotPresent.currentImage == UIImage(named: "check_ipad"))
        {
            
            isHideTermsView = true
            
        }
        else
        {
            
           isHideTermsView = false
            
        }
        
        if isHideView && isHideTermsView {
            
            const_ElectronicFormView.constant = 0
            
        }else{
            
            const_ElectronicFormView.constant = 200
            
        }
        
        
    }
    
    func methodOnViewAppearAfterSynData() {
        
        if nsud.bool(forKey: "offlineWdoSaved") == true {
            
            let isScannedCode = nsud.bool(forKey: "offlineWdoSaved")
            
            if isScannedCode {
                
                fetchLeadDetailFromLocalDB()
                
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    strLeadStatusGlobal = "Complete";
                }
                let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
                let fullAccess = opprtunityAccess.status
                let limitedAccess = opprtunityAccess.opportunityLimitedAccess

                if fullAccess && limitedAccess{
                    disableUserInteration()
                }
                else if(!fullAccess && limitedAccess){
                    disableUserInteration()
                    accessWhenOpportunityCompletwPending()
                }else{
                    
                }
                

                nsud.set(false, forKey: "offlineWdoSaved")
                nsud.synchronize()
                
                goToFinalizereport()
                
            }else{
                
                viewAppearMethod()
                
            }
            
            print("Data synced and move to next view")
            
        }else if (nsud.value(forKey: "YesWdoAgreementSynced") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "YesWdoAgreementSynced")
            
            if isScannedCode {
                
                fetchLeadDetailFromLocalDB()
                
                if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
                {
                    strLeadStatusGlobal = "Complete";
                    
                }
                let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
                let fullAccess = opprtunityAccess.status
                let limitedAccess = opprtunityAccess.opportunityLimitedAccess

                if fullAccess && limitedAccess{
                    disableUserInteration()
                }
                else if(!fullAccess && limitedAccess){
                    disableUserInteration()
                    accessWhenOpportunityCompletwPending()
                }else{
                    
                }
                
                
                
                nsud.set(false, forKey: "YesWdoAgreementSynced")
                nsud.set(false, forKey: "synAgreementWdo")
                nsud.synchronize()
                
                goToFinalizereport()
                
            }else{
                
                viewAppearMethod()
                
            }
            
            print("Data synced and move to next view")
            
        }else{
            
            viewAppearMethod()
            
        }
        
    }
    
    func viewAppearMethod() {
        
        fetchBeforeImageFromLocalDB()
        
        if strTechnicianSign.count>0
        {
            downloadTechnicianSign(strImageName: strTechnicianSign)
        }
        
        if strCustomerSign.count>0
        {
            downloadCustomerSign(strImageName: strCustomerSign)
        }
        
        //For Preset
        var strImageName = String()
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignSales")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        
        if ((isPreSetSign) && (strSignUrl.count>0) && !( (strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")))
        {
            
            strImageName = strSignUrl
            strTechnicianSign = strSignUrl
            
            let nsUrl = URL(string: strSignUrl)
            
            imgviewTechnicianSign.load(url: nsUrl! as URL , strImageName: strImageName)
            
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgviewTechnicianSign.image!)
            
            btnTechnicianSign.isEnabled = false
            isPreSetSignGlobal = true
            
        }
        else
        {
            isPreSetSignGlobal = false
            btnTechnicianSign.isEnabled = true
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won") && !is_WOEditMode(woObj: self.objWorkorderDetail)
            {
                btnTechnicianSign.isEnabled = false
            }
            if strTechnicianSign.count>0
            {
                if strIsPresetWO == "true" || strIsPresetWO == "1"
                {
                    downloadTechnicianPreset(strImageName: strTechnicianSign, stringUrl: strSignUrl)
                }
                else
                {
                    downloadTechnicianSign(strImageName: strTechnicianSign)
                }
            }
        }
        
        let shouldShowElectronicFormLink = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElectronicAuthorizationForm") as! Bool
        
        if(shouldShowElectronicFormLink == true)
        {
            if(strIsFormFilled == "true")
            {
                if(methodToCheckIfElectronicFormExistsForLeadId(strleadid: strLeadId) == true)
                {
                    viewContainerElectronicFormBtn.isHidden = false
                }
                else
                {
                    viewContainerElectronicFormBtn.isHidden = true
                }
            }
            else
            {
                viewContainerElectronicFormBtn.isHidden = false
            }
        }
        else
        {
            viewContainerElectronicFormBtn.isHidden = true
        }
        
    }
    
    fileprivate func getRecommendationMaster()
    {
        
        
        arrayRecommendationMaster = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
    }
    fileprivate func showPriceInformationInitial()
    {
        if(arrayProblemIdentificationPricing.count > 0)
        {
            var subTotal = 0.00
            var otherDiscount = 0.00
            var taxPercent =  0.0
            var tipDiscount =  0.00
            var taxAmt = 0.00
            var leadInspectionFee =  0.00
            var buildingPermitFee =  0.00

            for item in arrayProblemIdentificationPricing
            {
                
                for itemPI in arrayProblemIdentification
                {
                    if(("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false)
                    {
                        if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                        {
                            subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                        }
                        
                        if ("\((item as! NSMutableDictionary).value(forKey: "discount") ?? "")".count > 0)
                        {
                            otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
                        }
                        //otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
                        
                    }else if(("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "mobileId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false)
                    {
                        if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                        {
                            subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                        }
                        
                        if ("\((item as! NSMutableDictionary).value(forKey: "discount") ?? "")".count > 0)
                        {
                            otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
                        }
                        //otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)

                        
                    }
                    
                }
                
                
                /*
                 if((objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue > 0.0)
                 {
                 let tax = (objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue
                 
                 taxVal = ((subTotal - otherDiscount) * tax)/100.0
                 
                 }
                 */
                
            }
            
            lblSubtotalPriceInformation.text = String(format: "%0.2f", subTotal)
            lblOtherPriceInformation.text = String(format: "%0.2f", otherDiscount)
            txtOtherDiscount.text = String(format: "%0.2f", otherDiscount)

            // taxPercent = (objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue
            
            
            
            // fetch WDO inspection
            let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
            
            if arrOfWdoInspection.count > 0 {
                
                let objData = arrOfWdoInspection[0] as! NSManagedObject
                
                if((objData.value(forKey: "monthlyAmount") as! NSString).floatValue > 0)
                {
                    tipDiscount = Double((objData.value(forKey: "monthlyAmount") as! NSString).floatValue)
                }
                
                // Re-Calculating TIP Discount
                tipDiscount = Double((txtTotalTIPDiscount.text! as NSString).floatValue)

            }
            
            lblTipDiscountPriceInformation.text = String(format: "%.2f", tipDiscount)
            
            leadInspectionFee = (lblLeadInspectionFee.text! as NSString).doubleValue
            
            buildingPermitFee = (txtBuildingPermitFee.text! as NSString).doubleValue

            if( ((subTotal + leadInspectionFee + buildingPermitFee - otherDiscount) - tipDiscount) < 0)
            {
                //lblTotalPriceInformation.text = "0.00"
                lblBillingAmountPriceInformation.text = "0.00"
            }
            else
            {
                lblBillingAmountPriceInformation.text = String(format: "%.2f", ((subTotal + leadInspectionFee + buildingPermitFee - otherDiscount) - tipDiscount))
                //  lblTotalPriceInformation.text = String(format: "%.2f", ((subTotal - otherDiscount) - tipDiscount*12))
            }
            
            // tax calculation
            
            
            /* let totalPrice = (lblTotalPriceInformation.text! as NSString).doubleValue
             
             if(totalPrice > 0)
             {
             taxAmt = (totalPrice*taxPercent)/100.0
             }
             
             if(taxAmt < 0)
             {
             taxAmt = 0.00
             }
             
             lblTaxPriceInformation.text = String(format: "%0.2f", taxAmt)
             
             lblBillingAmountPriceInformation.text = String(format: "%0.2f", totalPrice+taxAmt)*/
            
        }
    }
    fileprivate func showPriceInformation()
    {
        if(arrayProblemIdentificationPricing.count > 0)
        {
            var subTotal = 0.00
            var otherDiscount = 0.00
            otherDiscount = ("\(txtOtherDiscount.text ?? "")" as NSString).doubleValue
            var taxPercent =  0.0
            var tipDiscount =  0.00
            var taxAmt = 0.00
            var leadInspectionFee =  0.00
            var buildingPermitFee =  0.00

            for item in arrayProblemIdentificationPricing
            {
                
                for itemPI in arrayProblemIdentification
                {
                    if((("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false) && (("\((itemPI as! NSMutableDictionary).value(forKey: "isCoveredUnderWarranty") ?? "")".lowercased() == "false") || ("\((itemPI as! NSMutableDictionary).value(forKey: "isCoveredUnderWarranty") ?? "")" == "")))
                    {
                        if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                        {
                            subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                        }
                        
//                        if ("\((item as! NSMutableDictionary).value(forKey: "discount") ?? "")".count > 0)
//                        {
//                            otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
//                        }
                        otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
                        
                    }else if((("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "mobileId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false) && (("\((itemPI as! NSMutableDictionary).value(forKey: "isCoveredUnderWarranty") ?? "")".lowercased() == "false") || ("\((itemPI as! NSMutableDictionary).value(forKey: "isCoveredUnderWarranty") ?? "")" == "")))
                    {
                        if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                        {
                            subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                        }
                        
//                        if ("\((item as! NSMutableDictionary).value(forKey: "discount") ?? "")".count > 0)
//                        {
//                            otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)
//                        }
                        otherDiscount = otherDiscount + Double(((item as! NSMutableDictionary).value(forKey: "discount") as! NSString).floatValue)

                        
                    }
                    
                }
                
                
                /*
                 if((objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue > 0.0)
                 {
                 let tax = (objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue
                 
                 taxVal = ((subTotal - otherDiscount) * tax)/100.0
                 
                 }
                 */
                
            }
            
            lblSubtotalPriceInformation.text = String(format: "%0.2f", subTotal)
            lblOtherPriceInformation.text = String(format: "%0.2f", otherDiscount)
            txtOtherDiscount.text = String(format: "%0.2f", otherDiscount)

            // taxPercent = (objWorkorderDetail .value(forKey: "tax") as! NSString).doubleValue
            
            
            
            // fetch WDO inspection
            let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
            
            if arrOfWdoInspection.count > 0 {
                
                let objData = arrOfWdoInspection[0] as! NSManagedObject
                
                if((objData.value(forKey: "monthlyAmount") as! NSString).floatValue > 0)
                {
                    tipDiscount = Double((objData.value(forKey: "monthlyAmount") as! NSString).floatValue)
                }
                
                // Re-Calculating TIP Discount
                tipDiscount = Double((txtTotalTIPDiscount.text! as NSString).floatValue)

            }
            
            lblTipDiscountPriceInformation.text = String(format: "%.2f", tipDiscount)
            
            leadInspectionFee = (lblLeadInspectionFee.text! as NSString).doubleValue
            
            buildingPermitFee = (txtBuildingPermitFee.text! as NSString).doubleValue

            if( ((subTotal + leadInspectionFee + buildingPermitFee - otherDiscount) - tipDiscount) < 0)
            {
                //lblTotalPriceInformation.text = "0.00"
                lblBillingAmountPriceInformation.text = "0.00"
            }
            else
            {
                lblBillingAmountPriceInformation.text = String(format: "%.2f", ((subTotal + leadInspectionFee + buildingPermitFee - otherDiscount) - tipDiscount))
                //  lblTotalPriceInformation.text = String(format: "%.2f", ((subTotal - otherDiscount) - tipDiscount*12))
            }
            
            // tax calculation
            
            
            /* let totalPrice = (lblTotalPriceInformation.text! as NSString).doubleValue
             
             if(totalPrice > 0)
             {
             taxAmt = (totalPrice*taxPercent)/100.0
             }
             
             if(taxAmt < 0)
             {
             taxAmt = 0.00
             }
             
             lblTaxPriceInformation.text = String(format: "%0.2f", taxAmt)
             
             lblBillingAmountPriceInformation.text = String(format: "%0.2f", totalPrice+taxAmt)*/
            
        }
    }
    
    fileprivate func disableUserInteration()
    {
        
        btnCash.isUserInteractionEnabled = false
        btnCheck.isUserInteractionEnabled = false
        btnCreditCard.isUserInteractionEnabled = false
        btnAutochargeCustomer.isUserInteractionEnabled = false
        btnCollectAtTimeOfScheduling.isUserInteractionEnabled = false
        btnInvoice.isUserInteractionEnabled = false
        txtAmountPaymentMode.isUserInteractionEnabled = false
        txtCheckNoPaymentMode.isUserInteractionEnabled = false
        txtDrivingLicenceNoPaymentMode.isUserInteractionEnabled = false
        btnSelectExpirationdate.isUserInteractionEnabled = false
        btnCustomerNotPresent.isUserInteractionEnabled = false
        btnCustomerSign.isUserInteractionEnabled = false
        btnTechnicianSign.isUserInteractionEnabled = false
        txtviewTermsAndConditions.isUserInteractionEnabled = false
        btnTermsAndConditionsCheckMark.isUserInteractionEnabled = false
        btnTermsAndCondition.isUserInteractionEnabled = false
        btnAllowCustomerToMakeSelection.isUserInteractionEnabled = false
        txtTotalTIPDiscount.isUserInteractionEnabled = false
        txtBuildingPermitFee.isUserInteractionEnabled = false
        btn_YesApplyTIP.isUserInteractionEnabled = false
        btn_NoApplyTIP.isUserInteractionEnabled = false
        btnIsTaxable.isUserInteractionEnabled = false
        txtOtherDiscount.isUserInteractionEnabled = false
        viewVerbalApproval.isUserInteractionEnabled = false
    }
    
    fileprivate func accessWhenOpportunityCompletwPending()  {
        btnCash.isUserInteractionEnabled = true
        btnCheck.isUserInteractionEnabled = true
        btnCreditCard.isUserInteractionEnabled = true
        btnAutochargeCustomer.isUserInteractionEnabled = true
        btnCollectAtTimeOfScheduling.isUserInteractionEnabled = true
        btnInvoice.isUserInteractionEnabled = true
        txtAmountPaymentMode.isUserInteractionEnabled = true
        txtCheckNoPaymentMode.isUserInteractionEnabled = true
        txtDrivingLicenceNoPaymentMode.isUserInteractionEnabled = true
        btnSelectExpirationdate.isUserInteractionEnabled = true
        btnCustomerNotPresent.isUserInteractionEnabled = true
        btnCustomerSign.isUserInteractionEnabled = true
        btnTechnicianSign.isUserInteractionEnabled = true
        btnAllowCustomerToMakeSelection.isUserInteractionEnabled = true
        btnIsTaxable.isUserInteractionEnabled = true
        txtOtherDiscount.isUserInteractionEnabled = true

    }
    
    
    func goToSendMail() {
        if !isGoToPriceJustification {
            self.syncBasicInfoToServiceAuto()
            
            let storyboardIpad = UIStoryboard.init(name: "SalesMainiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "SendMailViewControlleriPad") as! SendMailViewControlleriPad
            if (chkCustomerNotPresent) {
                testController.isCustomerPresent = "no"
            } else {
                testController.isCustomerPresent = "yes"
            }
            testController.isCustomerPresent = strWoId
            testController.strFromWdo = "yes"
            testController.strHeaderValue = "WDO SERVICE PROPOSAL"
            self.navigationController?.pushViewController(testController, animated: false)
            
        }
    }
    
    func goToCreditCardView() {
        
        /*
         objCreditCard.strGlobalLeadId=strLeadId;
         objCreditCard.strAmount=_txtPaidAmountPriceInforamtion.text;
         objCreditCard.strTypeOfService=@"Lead";
         objCreditCard.strDeviceType=@"iPhone";
         */
        
        let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
        let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
        
        var strValue = String()
        if chkCustomerNotPresent == true
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objCreditCardView?.isCustomerPresent = strValue as NSString
        objCreditCardView?.strAmount = txtAmountPaymentMode.text!
        objCreditCardView?.strGlobalLeadId = strLeadId as String
        objCreditCardView?.strTypeOfService = "Lead"
        //objCreditCardView?.workOrderDetailNew = objWorkorderDetail
        objCreditCardView?.strFromWhichView = "WdoFlow"
        objCreditCardView?.strDeviceType = "iPad"
        self.navigationController?.pushViewController(objCreditCardView!, animated: false)
        
    }
    
    func goToFinalizereport() {
        
        if !isGoToPriceJustification {
            
            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "FinalizeReport_WDOVC") as! FinalizeReport_WDOVC
            testController.strWoId = strWoId
            testController.strWdoLeadId = strLeadId as NSString
            self.navigationController?.pushViewController(testController, animated: false)
            
        }
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = strAccountNo
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    fileprivate func configureUI()
    {
        hghtConstViewContainerCheckDetails.constant = 0.0
        txtAmountPaymentMode.isHidden = false
        
        makeCornerRadius(value: 2.0, view: txtviewTermsOfServices, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewAdditionalNotes, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: viewCustomerSign, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: viewTechnicianSign, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        buttonRound(sender: btnSave)
        
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    // payment mode related methods
    
    fileprivate func cashPaymentMode()
    {
        btnCash.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnInvoice.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        
        hghtConstViewContainerCheckDetails.constant = 0.0
        txtAmountPaymentMode.isHidden = false
        txtAmountPaymentMode.superview?.isHidden = false
        txtCheckNoPaymentMode.text = ""
        txtDrivingLicenceNoPaymentMode.text = ""
        
        strGlobalPaymentMode = "Cash"
        
        showAddCardButton()
        
    }
    
    fileprivate func checkPaymentMode()
    {
        
        btnCheck.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnInvoice.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        hghtConstViewContainerCheckDetails.constant = 270.0
        txtAmountPaymentMode.isHidden = false
        txtAmountPaymentMode.superview?.isHidden = false
        strGlobalPaymentMode = "Check"
        
        showAddCardButton()
    }
    
    fileprivate func creditCardPaymentMode()
    {
        btnCreditCard.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnInvoice.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        hghtConstViewContainerCheckDetails.constant = 0.0
        txtAmountPaymentMode.isHidden = false
        txtAmountPaymentMode.superview?.isHidden = false
        txtCheckNoPaymentMode.text = ""
        txtDrivingLicenceNoPaymentMode.text = ""
        
        strGlobalPaymentMode = "CreditCard"
        
        showAddCardButton()
    }
    
    fileprivate func autoChargeCustomerPaymentMode()
    {
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnInvoice.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        hghtConstViewContainerCheckDetails.constant = 0.0
        
        txtAmountPaymentMode.isHidden = true
        txtAmountPaymentMode.superview?.isHidden = true
        txtAmountPaymentMode.text = ""
        txtCheckNoPaymentMode.text = ""
        txtDrivingLicenceNoPaymentMode.text = ""
        strGlobalPaymentMode = "AutoChargeCustomer"
        
        showAddCardButton()
    }
    fileprivate func collectAtTimeOfSchedulingPaymentMode()
    {
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnInvoice.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        hghtConstViewContainerCheckDetails.constant = 0.0
        txtAmountPaymentMode.isHidden = false
        txtAmountPaymentMode.superview?.isHidden = false
       // txtAmountPaymentMode.text = ""
        txtCheckNoPaymentMode.text = ""
        txtDrivingLicenceNoPaymentMode.text = ""
        strGlobalPaymentMode = "CollectattimeofScheduling"// ye abhi nh pata ki yaha per kya jaega
        
      
        
        showAddCardButton()
        
    }
    fileprivate func invoicePaymentMode()
    {
        btnInvoice.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutochargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCollectAtTimeOfScheduling.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        hghtConstViewContainerCheckDetails.constant = 0.0
        txtAmountPaymentMode.isHidden = true
        txtAmountPaymentMode.superview?.isHidden = true
        txtAmountPaymentMode.text = ""
        txtCheckNoPaymentMode.text = ""
        txtDrivingLicenceNoPaymentMode.text = ""
        
        strGlobalPaymentMode = "Invoice"// ye abhi nh pata ki yaha per kya jaega
        
        showAddCardButton()
    }
    
    // MARK: Image Download
    func downloadCustomerSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgviewCustomerSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")" //SalesProcessModule
                      
            if strImageName.contains("Documents"){
                
                strUrl = strUrl + strImageName

            }else{
                strUrl = strUrl + "//Documents/" + strImageName

            }
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            
            let nsUrl = URL(string: strUrl)
            imgviewCustomerSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func downloadTechnicianSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgviewTechnicianSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")" // SalesProcessModule
            
            if strImageName.contains("Documents"){
                
                strUrl = strUrl + strImageName

            }else{
                strUrl = strUrl + "//Documents/" + strImageName

            }
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            let nsUrl = URL(string: strUrl)
            imgviewTechnicianSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    func downloadTechnicianPreset(strImageName: String, stringUrl: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgviewTechnicianSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")" //HrmsServiceModule
            
            //strUrl = strUrl + "//Documents/" + strImageName
            strUrl = stringUrl
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            imgviewTechnicianSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    /* func updateWorkOrderDetail()
     {
     if strCustomerSign.count > 0
     {
     saveImageDocumentDirectory(strFileName: strCustomerSign, image: imgviewCustomerSign.image!)
     }
     if strTechnicianSign.count > 0
     {
     saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgviewTechnicianSign.image!)
     }
     
     // Update Payment Info
     
     if chkCustomerNotPresent == true
     {
     strCustomerSign = ""
     }
     
     
     var strWorkOrderStatusFinal = String()
     strWorkOrderStatusFinal = "Completed"
     
     
     let dateFormatter = DateFormatter()
     dateFormatter.timeZone = NSTimeZone.local
     dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
     
     let date = Date()
     var strTimeOut = String()
     strTimeOut = dateFormatter.string(from: date)
     
     var coordinate = CLLocationCoordinate2D()
     coordinate = Global().getLocation()
     let strTimeOutLat = "\(coordinate.latitude)"
     let strTimeOutLong = "\(coordinate.longitude)"
     
     
     var strResendStatus = String()
     strResendStatus = "0"
     if strWorkOrderStatus == "Incomplete"
     {
     strResendStatus = "0"
     }
     
     
     var isElementIntegraiton = Bool()
     isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
     if isElementIntegraiton == false
     {
     if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
     {
     strWorkOrderStatusFinal = "InComplete"
     }
     }
     
     var strCustomerNotPresent = String()
     
     if chkCustomerNotPresent == false
     {
     strCustomerNotPresent = "false"
     }
     else
     {
     strCustomerNotPresent = "true"
     }
     
     var strPresetStatus = String()
     
     if isPreSetSignGlobal == true
     {
     strPresetStatus = "true"
     }
     else
     {
     strPresetStatus = "false"
     }
     
     var arrOfKeys = NSMutableArray()
     var arrOfValues = NSMutableArray()
     
     
     
     
     arrOfKeys = ["technicianSignaturePath",
     "customerSignaturePath",
     "audioFilePath",
     "technicianComment",
     "officeNotes",
     "timeOut",
     "timeOutLatitude",
     "timeOutLongitude",
     "isResendInvoiceMail",
     "workorderStatus",
     "isCustomerNotPresent",
     "IsRegularPestFlow",
     "isEmployeePresetSignature"
     ]
     //strWorkOrderStatusFinal = "InCompleted"
     arrOfValues = [strTechnicianSign,
     strCustomerSign,
     strAudioName,
     txtViewTechnicianComment.text,
     txtViewOfficeNotes.text,
     strTimeOut,
     strTimeOutLat,
     strTimeOutLong,
     strResendStatus,
     strWorkOrderStatusFinal,
     strCustomerNotPresent,
     "true",
     strPresetStatus
     
     
     ]
     
     
     let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
     
     
     if isSuccess
     {
     if yesEditedSomething
     {
     updateModifyDate()
     
     //Update Modify Date In Work Order DB
     updateServicePestModifyDate(strWoId: self.strWoId as String)
     
     }
     
     
     
     if isElementIntegraiton == true
     {
     if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
     {
     print("Go to credit card ciew")
     goToCreditCardScreen()
     }
     else
     {
     print("Go to send mail")
     goToSendEmailScreen()
     }
     }
     else
     {
     print("Go to send mail")
     goToSendEmailScreen()
     }
     
     } else {
     
     showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
     
     }
     }*/
    
    
    // MARK: Core Data
    
    func updateWDoInspectionDetails() {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("totalTIPDiscount")
        arrOfValues.add(txtTotalTIPDiscount.text!)
        
        arrOfKeys.add("isApplyTipDiscount")
        arrOfValues.add(strApplyTIP)
        
        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }
    
    func updateWoDetail() {
        
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
        
            
            
        }else{
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("audioFilePath")
            
            if(strAudioNameServiceAuto.count > 0)
            {
                arrOfValues.add(strAudioNameServiceAuto)
            }
            else
            {
                arrOfValues.add("")
            }
            
            //Nilind
            //Aproval Code
            
            if isVerbalApproval && !viewVerbalApproval.isHidden
            {
                arrOfKeys.add("isVerbalApproval")
                arrOfValues.add("true")
            }
            else
            {
                arrOfKeys.add("isVerbalApproval")
                arrOfValues.add("false")
            }

            if arySelectedEmployee.count > 0 && !viewVerbalApproval.isHidden
            {
                arrOfKeys.add("verbalApprovalBy")
                arrOfValues.add("\((arySelectedEmployee.object(at: 0) as! NSDictionary).value(forKey: "EmployeeId") ?? "")")
            }
            else
            {
                arrOfKeys.add("verbalApprovalBy")
                arrOfValues.add("")
            }
            
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifyDate")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")

            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().modifyDateService())
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().modifyDateService())
            
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            
            if isSuccess
            {
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }
        
    }
    fileprivate func updateInspectionDB()
    {
        
            if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
                
                
            }
            else
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("otherDiscount")
                arrOfKeys.add("buildingPermitFee")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
            
                arrOfValues.add(txtOtherDiscount.text ?? "0.00")
                arrOfValues.add(txtBuildingPermitFee.text ?? "0.00")
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().modifyDateService())
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().modifyDateService())
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoInspectionExtSerDc", predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                
                if isSuccess
                {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }
            }
    }
    fileprivate func updateLeadDetails()
    {
                
        if strCustomerSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strCustomerSign, image: imgviewCustomerSign.image!)
        }
        if strTechnicianSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgviewTechnicianSign.image!)
        }
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        
        if(btnCustomerNotPresent.currentImage == UIImage(named: "check_ipad"))
        {
            
            arrOfKeys.add("isCustomerNotPresent")
            arrOfKeys.add("isAgreementSigned")
            arrOfKeys.add("isAgreementGenerated")
            
            arrOfValues.add("true")//isCustomerNotPresent
            arrOfValues.add("false")//isAgreementSigned
            arrOfValues.add("false")//isAgreementGenerated
            
            if(btnAllowCustomerToMakeSelection.currentImage == UIImage(named: "checked.png"))
            {
                
                arrOfKeys.add("isSelectionAllowedForCustomer")
                arrOfValues.add("true")
                
            }else{
                
                arrOfKeys.add("isSelectionAllowedForCustomer")
                arrOfValues.add("false")
                
            }
            
        }
        else
        {
            
            arrOfKeys.add("isCustomerNotPresent")
            arrOfKeys.add("isAgreementSigned")
            arrOfKeys.add("isAgreementGenerated")
            
            arrOfValues.add("false")//isCustomerNotPresent
            arrOfValues.add("true")//isAgreementSigned
            arrOfValues.add("true")//isAgreementGenerated
            
            arrOfKeys.add("isSelectionAllowedForCustomer")
            arrOfValues.add("false")
            
        }
        
        if(isPreSetSignGlobal)
        {
            
            arrOfKeys.add("isEmployeePresetSignature")
            arrOfValues.add("true")
            
        }
        else
        {
            
            arrOfKeys.add("isEmployeePresetSignature")
            arrOfValues.add("false")
            
        }
        
        arrOfKeys.add("iAgreeTerms")
        arrOfKeys.add("subTotalAmount")
        arrOfKeys.add("totalPrice")
        arrOfKeys.add("collectedAmount")
        arrOfKeys.add("tipDiscount")
        arrOfKeys.add("otherDiscount")
        //arrOfKeys.add("billedAmount")
        arrOfKeys.add("buildingPermitAmount")
        
        arrOfValues.add("true")
        arrOfValues.add(lblSubtotalPriceInformation.text!)
        arrOfValues.add(lblBillingAmountPriceInformation.text!)
        arrOfValues.add(txtAmountPaymentMode.text!)
        arrOfValues.add(txtTotalTIPDiscount.text!)
        arrOfValues.add(txtOtherDiscount.text ?? "0.00") //lblOtherPriceInformation.text!
        //arrOfValues.add(lblBillingAmountPriceInformation.text!)
        arrOfValues.add(txtBuildingPermitFee.text!)

        
        arrOfKeys.add("audioFilePath")
        
        if(strAudioName.count > 0)
        {
            arrOfValues.add(strAudioName)
        }
        else
        {
            arrOfValues.add("")
        }
        
        if(yesEditedSomething)
        {
            
            arrOfKeys.add("zSync")
            arrOfValues.add("yes")
            
        }
        
        var leadStatusFinal = ""
        var isElementIntegraiton = Bool()
        isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
        
        //isElementIntegraiton = true
        // Saavan Patidar
        
        if strForProposal == "Yes" {
            
            if(btnCustomerNotPresent.currentImage == UIImage(named: "check_ipad"))
            {
                
                leadStatusFinal = "Complete"
                
                // customer not present
                arrOfKeys.add("stageSysName")
                
                arrOfValues.add("CompletePending")
                
                nsud.set("Complete", forKey: "leadStatusSales")
                nsud.set("CompletePending", forKey: "stageSysNameSales")
                nsud.synchronize()
                
            }else{
                
                leadStatusFinal = "Complete"
                
                // customer present
                arrOfKeys.add("stageSysName")
                
                arrOfValues.add("Won")
                
                nsud.set("Complete", forKey: "leadStatusSales")
                nsud.set("Won", forKey: "stageSysNameSales")
                nsud.synchronize()
                
            }
            
        }
        
        if isElementIntegraiton == true
        {
            
            if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
            {
                leadStatusFinal = "Open"
            }
        }
        
        arrOfKeys.add("statusSysName")
        arrOfValues.add(leadStatusFinal)
        arrOfKeys.add("isCommLeadUpdated")
        arrOfValues.add("true")
        
        //if ((isApplyTIPShow) && (btnCustomerNotPresent.currentImage == UIImage(named: "uncheck_ipad")) && strApplyTIP.lowercased() == "true")
        if ((isApplyTIPShow) && strApplyTIP.lowercased() == "true")
        {
            
            arrOfKeys.add("needTipService")
            arrOfValues.add("true")
            //Biiling amount same as before
            arrOfKeys.add("billedAmount")
            arrOfValues.add(lblBillingAmountPriceInformation.text!)
            
        }else{
            
            arrOfKeys.add("needTipService")
            arrOfValues.add("false")
            
            //Change in Billed Amountadding billed amount and TIP Doscount amount
            let tipDiscountTemp =  Double((txtTotalTIPDiscount.text! as NSString).floatValue)
            let billedAmount =  Double((lblBillingAmountPriceInformation.text! as NSString).floatValue)
            
            let billedAMountLocalTemp = String(format: "%.2f", ((billedAmount + tipDiscountTemp)))
            
            arrOfKeys.add("billedAmount")
            arrOfValues.add("\(billedAMountLocalTemp)")
            
        }
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if(isSuccess == true)
        {
            if(yesEditedSomething)
            {
                global.updateSalesModifydate(strLeadId)
            }
            
            if strForProposal == "Yes" {
                
                if(arrayProblemIdentification.count == 0)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CreateBid, viewcontrol: self)
                }
                else
                {
                    
                    if isElementIntegraiton == true
                    {
                        if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
                        {
                            
                            print("Go to credit card ciew")
                            goToCreditCardView()
                            
                        }
                        else
                        {
                            print("Go to send mail")
                            goToSendMail()
                            
                        }
                    }else{
                        
                        goToSendMail()
                        
                    }
                    
                }
            }else{
                
                if(yesEditedSomething)
                {
                    
                    // sett true to sync sales data after wdo data
                    nsud.set(true, forKey: "synAgreementWdo")
                    nsud.synchronize()
                    
                }
                
                goToFinalizereport()
                
            }
            
            //            let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
            //            let testController = storyboardIpad.instantiateViewController(withIdentifier: "FinalizeReport_WDOVC") as! FinalizeReport_WDOVC
            //            testController.strWoId = strWoId
            //            testController.strWdoLeadId = strLeadId as NSString
            //            self.navigationController?.pushViewController(testController, animated: false)
            
        }
        else
        {
            print("updates failed")
        }
        
        updateWDoInspectionDetails()
        
    }
    
    fileprivate func updatePaymentInfo()
    {
        // Update Payment Info
        
        
        let arrayAllObject = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if (arrayAllObject.count==0)
        {
            savePaymentInfo()
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("paymentMode")
            arrOfKeys.add("amount")
            arrOfKeys.add("checkNo")
            arrOfKeys.add("licenseNo")
            arrOfKeys.add("expirationDate")
            arrOfKeys.add("checkFrontImagePath")
            arrOfKeys.add("checkBackImagePath")
            arrOfKeys.add("customerSignature")
            arrOfKeys.add("salesSignature")
            arrOfKeys.add("modifiedDate")
            //arrOfKeys.add("SpecialInstructions")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strGlobalPaymentMode)
            arrOfValues.add(txtAmountPaymentMode.text ?? "")
            arrOfValues.add(txtCheckNoPaymentMode.text ?? "")
            arrOfValues.add(txtDrivingLicenceNoPaymentMode.text ?? "")
            arrOfValues.add(strExpirationDate)
            arrOfValues.add(strCheckFrontImage)
            arrOfValues.add(strCheckBackImage)
            
            if (chkCustomerNotPresent == true)
            {
                arrOfValues.add("")
            }
            else
            {
                arrOfValues.add(strCustomerSign)
            }
            
            arrOfValues.add(strTechnicianSign)
            
            var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as! NSString
            
            let isPreSetSign = nsud.value(forKey: "isPreSetSignSales") as! Bool
            
            if(strSignUrl.length > 0 && isPreSetSign == true)
            {
                strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/") as NSString
                
                var result: NSString!
                
                let equalRange = strSignUrl.range(of: "Documents", options: .backwards)
                
                if(equalRange.location != NSNotFound)
                {
                    result = strSignUrl.substring(from: equalRange.location + equalRange.length) as NSString
                }
                else
                {
                    result=strSignUrl;
                }
                
                arrOfValues.removeLastObject()
                arrOfValues.add(result)
            }
            
            arrOfValues.add(global.modifyDate())
            //arrOfValues.add("")
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    fileprivate func updateProblemIdentification()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        for item in arrayProblemIdentification
        {
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("isAddToAgreement")
            
            
            // values
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strEmpID)
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            if (item as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true {
                
                arrOfValues.add(true)

            }else{
                
                arrOfValues.add(false)
                
            }

            var isSuccess = false
            
            var idLocal = "\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)"
            
            if idLocal.count > 0 {
                
                isSuccess =  getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId, idLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }else{
                
                idLocal = "\((item as! NSMutableDictionary).value(forKey: "mobileId")!)"
                
                isSuccess =  getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId, idLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            
            //let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if(isSuccess)
            {
                print("problem identification updated successfully")
                
                
                let arrOfKeys3 = NSMutableArray()
                let arrOfValues3 = NSMutableArray()
                
                // keys
                
                arrOfKeys3.add("userName")
                arrOfKeys3.add("companyKey")
                arrOfKeys3.add("leadId")
                arrOfKeys3.add("createdBy")
                arrOfKeys3.add("createdDate")
                arrOfKeys3.add("modifiedBy")
                arrOfKeys3.add("modifiedDate")
                
                arrOfKeys3.add("isSold")
                
                
                
                // values
                
                arrOfValues3.add(strUserName)
                arrOfValues3.add(strCompanyKey)
                arrOfValues3.add(strLeadId)
                arrOfValues3.add(strEmpID)
                arrOfValues3.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfValues3.add(strEmpID)
                arrOfValues3.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                
                if((item as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true)
                {
                    arrOfValues3.add("true")
                }
                else
                {
                    arrOfValues3.add("false")
                }
                
                // to check for mobile id
                
                var idLocal = "\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)"
                
                if idLocal.count > 0 {
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "wdoProblemIdentificationId == %@", idLocal) , arrayOfKey: arrOfKeys3, arrayOfValue: arrOfValues3)
                    
                    if(isSuccess)
                    {
                        print("non standard service updated successfully")
                    }
                    
                }else{
                    
                    idLocal = "\((item as! NSMutableDictionary).value(forKey: "mobileId")!)"
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "mobileId == %@", idLocal) , arrayOfKey: arrOfKeys3, arrayOfValue: arrOfValues3)
                    
                    if(isSuccess)
                    {
                        print("non standard service updated successfully")
                    }
                    
                }

                
            }
        }
    }
    
    fileprivate func updateLeadCommercialDetail()
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        
        
        arrOfKeys.add("isInitialTaxApplicable")
        
        
        // values
        
        if(btnIsTaxable.currentImage == UIImage(named: "check_ipad"))
        {
            arrOfValues.add("true")
        }
        else
        {
            arrOfValues.add("false")
        }
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            print("LeadCommercialDetailExtDc updated successfully")
        }
        
    }
    
    fileprivate func savePaymentInfo()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadPaymentDetailId")
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("amount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("licenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("specialInstructions")
        arrOfKeys.add("agreement")
        arrOfKeys.add("proposal")
        arrOfKeys.add("customerSignature")
        arrOfKeys.add("salesSignature")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strLeadId)
        arrOfValues.add("") // leadPaymentDetailId
        arrOfValues.add(strGlobalPaymentMode)
        arrOfValues.add(txtAmountPaymentMode.text ?? "")
        arrOfValues.add(txtCheckNoPaymentMode.text ?? "")
        arrOfValues.add(txtDrivingLicenceNoPaymentMode.text ?? "")
        arrOfValues.add(strExpirationDate)
        arrOfValues.add("") // special instruction
        arrOfValues.add("") // agreement
        arrOfValues.add("") // proposal
        arrOfValues.add(strCustomerSign)
        arrOfValues.add(strTechnicianSign)
        
        arrOfValues.add("") // createdBy
        arrOfValues.add("") // createdDate
        arrOfValues.add("") // modifiedBy
        arrOfValues.add(global.modifyDate())
        
        arrOfValues.add(strCheckFrontImage)
        arrOfValues.add(strCheckBackImage)
        
        saveDataInDB(strEntity: "PaymentInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    fileprivate func checkImage() -> Bool
    {
        let chkImage = nsud.bool(forKey: "isCompulsoryAfterImageService")
        
        if chkImage == true
        {
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strLeadId, "Before"))
            
            if arryOfData.count > 0
            {
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            return false
        }
        
    }
    
    func updatePricingApprovalTrue(strTrueFalse : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("isPricingApprovalPending")
        arrOfValues.add(strTrueFalse)
        
        arrOfKeys.add("isPricingApprovalMailSend")
        arrOfValues.add("false")
        
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifyDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().modifyDateService())
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if isSuccess
        {
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
        }
        
    }
    
    fileprivate func finalSaveWithVerbalApprovalCheck()
    {
        if !isInternetAvailable()
        {
            saveOffline()
        }
        else
        {
            saveOnline()
        }
    }
    
    fileprivate func finalSaveWithoutValidaiton()
    {
        updateWoDetail()
        
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard"
        {
            
            if(strGlobalPaymentMode == "Check" && txtCheckNoPaymentMode.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check #", viewcontrol: self)
                return
            }
                
            else
            {
                updateProblemIdentification()
                updateLeadCommercialDetail()
                updatePaymentInfo()
                updateLeadDetails()
                updateInspectionDB()
            }
        }
        else if strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "CollectattimeofScheduling" || strGlobalPaymentMode == "Invoice"
        {
            updateProblemIdentification()
            updateLeadCommercialDetail()
            updatePaymentInfo()
            updateLeadDetails()
            updateInspectionDB()
        }
        else
        {
            updateProblemIdentification()
            updateLeadCommercialDetail()
            updatePaymentInfo()
            updateLeadDetails()
            updateInspectionDB()
        }
    }
    
    
    fileprivate func finalSave()
    {
        
        updateWoDetail()
        
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard"
        {
            if(txtAmountPaymentMode.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
                return
                
            }
            else if( (txtAmountPaymentMode.text! as NSString).floatValue == 0.0)
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
                return
            }
            
            if(strGlobalPaymentMode == "Check" && txtCheckNoPaymentMode.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check #", viewcontrol: self)
                return
            }
                
            else
            {
                if(btnCustomerNotPresent.currentImage == UIImage(named: "check_ipad"))
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    }
                        
                        //                    else if checkImage() == true
                        //                    {
                        //                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                        //                    }
                    else
                    {
                        updateProblemIdentification()
                        updateLeadCommercialDetail()
                        updatePaymentInfo()
                        updateLeadDetails()
                        updateInspectionDB()
                    }
                }
                else
                {
                    if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    }
                    else if strCustomerSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                        
                    }
                    else if(btnTermsAndConditionsCheckMark.currentImage == UIImage(named: "uncheck_ipad"))
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Kindly accept the Terms & Conditions", viewcontrol: self)
                    }
                        /* else if checkImage() == true
                         {
                         showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                         }*/
                    else
                    {
                        updateProblemIdentification()
                        updateLeadCommercialDetail()
                        updatePaymentInfo()
                        updateLeadDetails()
                        updateInspectionDB()
                    }
                }
            }
        }
        else if strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "CollectattimeofScheduling" || strGlobalPaymentMode == "Invoice"
        {
            if chkCustomerNotPresent == true
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                    //                else if checkImage() == true
                    //                {
                    //                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    //                }
                else
                {
                    updateProblemIdentification()
                    updateLeadCommercialDetail()
                    updatePaymentInfo()
                    updateLeadDetails()
                    updateInspectionDB()
                }
            }
            else
            {
                if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if strCustomerSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                    
                }
                else if(btnTermsAndCondition.currentImage == UIImage(named: "uncheck_ipad"))
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Kindly accept the Terms & Conditions", viewcontrol: self)
                }
                    /* else if checkImage() == true
                     {
                     showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                     }*/
                else
                {
                    updateProblemIdentification()
                    updateLeadCommercialDetail()
                    updatePaymentInfo()
                    updateLeadDetails()
                    updateInspectionDB()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
        }
    }
    
    
    
    // MARK: getLetterTemplateAndTermsOfService
    fileprivate func getLetterTemplateAndTermsOfService()
    {
     
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
//        guard dictMaster.count > 0 else {
//            return
//        }
        
        
        guard dictMaster.value(forKey: "LetterTemplateMaster") is NSArray else {
            return
        }
        guard dictMaster.value(forKey: "SalesMarketingContentMaster") is NSArray  else {
            return
        }
        guard dictMaster.value(forKey: "TermsOfServiceMaster") is NSArray  else {
            return
        }
        
        
        let arrayLetterTemplateMaster = (dictMaster.value(forKey: "LetterTemplateMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        let arraySalesMarketingContentMaster = (dictMaster.value(forKey: "SalesMarketingContentMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        arrayTermsOfService = (dictMaster.value(forKey: "TermsOfServiceMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        
        // cover letter and Introduction
        for item in arrayLetterTemplateMaster
        {
            if("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "CoverLetter")
            {
                arrayCoverLetter.add(item as! NSDictionary)
            }
            else if ("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "Introduction")
            {
                arrayIntroduction.add(item as! NSDictionary)
            }
        }
        
        // Sales Marketing Content
        
        for item in arraySalesMarketingContentMaster
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
                else
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
            }
        }
        
        // Terms & Conditions
        
        let arrayMultipleGeneralTermsConditions = (dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray).mutableCopy() as! NSMutableArray
        
        for item in arrayMultipleGeneralTermsConditions
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayTermsAndConditions.add(item as! NSDictionary)
                }
            }
            
            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true && (item as! NSDictionary).value(forKey: "IsDefault") as! Bool == true)
            {
                // arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
            }
        }
        
    }
    
    // MARK: Fetch from local DB
    
    func fetchLetterTemplateFromLocalDB()
    {
        let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(arrayLetterTemplate.count > 0)
        {
            let match = arrayLetterTemplate.firstObject as! NSManagedObject
            
            // cover letter
            if("\(match.value(forKey: "coverLetterSysName") ?? "")".count > 0)
            {
                const_ConverLetter_Top.constant = 20
                for item in arrayCoverLetter
                {
                    var strCoverLetterContent = ""
                    
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "coverLetterSysName")!)")
                    {
                        strCoverLetterContent = "\((item as! NSDictionary).value(forKey: "TemplateContent")!)"
                        
                        
                        var strText = NSMutableString()
                        strText = NSMutableString(string: "\((item as! NSDictionary).value(forKey: "TemplateContent")!)")
                        
                        
                        var strMutableText = NSMutableString()
                        strMutableText = strText
                        
                        
                        strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                        
                        
                        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
                        
                        for i in 0 ..< 4
                        {
                            print(i)
                            
                            // [AdditionalNotes]
                            //[PreferredMonths]
                            //[AgreementCheckList]
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AdditionalNotes]", with: txtviewAdditionalNotes.text))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PreferredMonths]", with: lblServiceMonths.text ?? ""))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AgreementCheckList]", with: "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath as String))
                            
                            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage as String))
                            
                            
                            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate() as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyName]", with: lblPerson.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddress]", with: lblServiceAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddressName]", with: lblServiceAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddress]", with: lblBillingAddress.text ?? "" as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddressName]", with: lblBillingAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountNo]", with: strAccountNo as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber as String))
                            
                            
                            if(dictInspector.count > 0)
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String))
                                
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String))
                            }
                            else
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "" as String))
                                
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "" as String))
                            }
                            
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Technician]", with: strEmpName as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellPhone]", with: lblPhonePersonalInfo.text ?? "" as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellNumber]", with: strCellNo as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Licenseno]", with: "" as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PrimaryPhone]", with: lblPhonePersonalInfo.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName as String))
                            
                            
                            
                        }
                        
                        //                        NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(3), target: self, selector: "functionHere", userInfo: nil, repeats: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                            
                            self.webviewCoverLetter.loadHTMLString(strMutableText as String, baseURL: Bundle.main.bundleURL)
                        }
                        
                        
                        
                        
                    }
                }
            }
            else
            {
                const_ConverLetter_Top.constant = 0

            }
            
            // Introduction letter
            
            if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
            {
                const_IntroductionLetter_Top.constant = 20

                for itemNew in arrayIntroduction
                {
                    if("\((itemNew as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                    {
                        
                        //let strCoverLetterContent = "\((item as! NSDictionary).value(forKey: "introContent")!)"
                        
                        
                        var strText = NSMutableString()
                        strText = NSMutableString(string: "\((itemNew as! NSDictionary).value(forKey: "TemplateContent")!)")
                        
                        // Change By Saavan
                        strText = NSMutableString(string: "\(match.value(forKey: "introContent")!)")
                        
                        var strMutableText = NSMutableString()
                        strMutableText = strText
                        
                        
                        strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                        
                        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
                        
                        for i in 0 ..< 4
                        {
                            print(i)
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AdditionalNotes]", with: txtviewAdditionalNotes.text))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PreferredMonths]", with: lblServiceMonths.text ?? ""))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AgreementCheckList]", with: "" as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath as String))
                            
                            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage as String))
                            
                            
                            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate() as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyName]", with: lblPerson.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddress]", with: lblServiceAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddressName]", with: lblServiceAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddress]", with: lblBillingAddress.text ?? "" as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddressName]", with: lblBillingAddress.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountNo]", with: strAccountNo as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber as String))
                            
                            if(dictInspector.count > 0)
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String))
                                
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String))
                            }
                            else
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "" as String))
                                
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "" as String))
                            }
                            
                            
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Technician]", with: strEmpName as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName as String) )
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellPhone]", with: lblPhonePersonalInfo.text ?? "" as String))
                            
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellNumber]", with: strCellNo as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Licenseno]", with: "" as String))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PrimaryPhone]", with: lblPhonePersonalInfo.text ?? "" as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress as String))
                            
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName as String))
                            
                            
                            
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                            
                            self.webviewIntroduction.loadHTMLString(strMutableText as String, baseURL: Bundle.main.bundleURL)
                            
                        }
                        
                        
                    }
                }
            }
            else
            {
                const_IntroductionLetter_Top.constant = 0

            }
            
            // Terms Of Service
            if("\(match.value(forKey: "termsOfServiceSysName") ?? "")".count > 0)
            {
                
                for item in arrayTermsOfService
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "termsOfServiceSysName")!)")
                    {
                        
                        txtviewTermsOfServices.attributedText = getAttributedHtmlStringUnicode(strText: "\((item as! NSDictionary).value(forKey: "Description")!)")
                        
                        
                    }
                }
            }
            else
            {
                
            }
            
            // isAgreementvalidfor(Other)
            
            if("\(match.value(forKey: "isAgreementValidFor")!)" == "true")
            {
                
            }
                
            else
            {
                
            }
            if("\(match.value(forKey: "isInitialTaxApplicable")!)" == "true")
            {
                btnIsTaxable.setImage(UIImage(named: "check_ipad"), for: .normal)
            }
            else
            {
                btnIsTaxable.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            }
            
        }
    }
    
    
    fileprivate func fetchMarketingContentFromLocalDB()
    {
        let arrayMatches = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(arrayMatches.count > 0)
        {
            const_SalesMarketingContent_Top.constant = 20
            var strMarketingContent = ""
            
            for match in arrayMatches
            {
                for item in arrayMarketingContent
                {
                    if("\((match as! NSManagedObject).value(forKey: "contentSysName")!)" == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                    {
                        strMarketingContent.append("\((item as! NSDictionary).value(forKey: "Description")!)")
                    }
                }
            }
            if(strMarketingContent.count > 0)
            {
                webviewSalesMarketingContent.loadHTMLString(strMarketingContent, baseURL: Bundle.main.bundleURL)
                
            }
        }
        else
        {
            const_SalesMarketingContent_Top.constant = 0

        }
    }
    
    fileprivate func fetchMultiTermsAndConditionsFromLocalDB()
    {
        let arrayMultiTermsConditions = getDataFromLocal(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        arrayTermsAndConditionsMultiSelected.removeAllObjects()
        
        if(arrayMultiTermsConditions.count > 0)
        {
            for match in arrayMultiTermsConditions
            {
                for item in arrayTermsAndConditions
                {
                    if("\((match as! NSManagedObject).value(forKey: "leadCommercialTermsId")!)" == "\((item as! NSDictionary).value(forKey: "Id")!)")
                    {
                        arrayTermsAndConditionsMultiSelected.add(item as! NSDictionary)
                    }
                }
            }
        }
    }
    
    fileprivate func fetchLeadDetailFromLocalDB()
    {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(arrayLeadDetail.count > 0)
        {
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            matchesGeneralInfo = match
            var strMonths = "\(match.value(forKey: "strPreferredMonth")!)"
            strMonths = strMonths.trimmingCharacters(in: .whitespacesAndNewlines)
            
            var arrMonth = strMonths.components(separatedBy: ",") as NSArray
            let arrMutable = NSMutableArray()
            if arrMonth.count > 0
            {
                for item in arrMonth
                {
                    let str = item as! String
                    
                    if str == "January" || str == "February" || str == "March" || str == "April" || str == "June" || str == "July" || str == "August" || str == "September" || str == "October" || str == "November" || str == "December"
                    {
                        arrMutable.add(str)
                    }
                }
            }
            arrMonth = arrMutable.mutableCopy() as! NSArray
            if arrMonth.count > 0
            {
                strMonths = arrMonth.componentsJoined(by: ", ")
            }
            else
            {
                strMonths = ""
            }
            
            lblServiceMonths.text = strMonths
            
            if strMonths.count <= 0 {
                
                hghtConstViewContainerServiceMonths.constant = 0.0
                
            }
            
            // showing person name
            var strName = "\(match.value(forKey: "firstName") ?? "")"
            
            if "\(match.value(forKey: "middleName") ?? "")".count > 0
            {
                strName = strName + " " + "\(match.value(forKey: "middleName") ?? "")"
            }
            
            strName = strName + " " + "\(match.value(forKey: "lastName") ?? "")"
            lblPerson.text = strName
            
            // showing billing address
            
            lblBillingAddress.text = global.strCombinedAddressService(for: objWorkorderDetail)
            
            btnBillingAddress.setTitle(lblBillingAddress.text, for: .normal)
            
            // showing email address
            lblEmail.text = "\(match.value(forKey: "primaryEmail") ?? "")"
            
            btnEmail.setTitle(lblEmail.text, for: .normal)
            
            
            // showing service address
            
            lblServiceAddress.text =  global.strCombinedAddressService(for: objWorkorderDetail)
            
            btnServiceAddress.setTitle(lblBillingAddress.text, for: .normal)
            
            
            lblPhonePersonalInfo.text = "\(match.value(forKey: "primaryPhone") ?? "")"
            
            btnPhonePersonalnfo.setTitle(lblPhonePersonalInfo.text, for: .normal)
            
            strIsPresetWO = "\(match.value(forKey: "isEmployeePresetSignature") ?? "")"
            
            if("\(match.value(forKey: "isEmployeePresetSignature") ?? "")" == "true")
            {
                //btnCustomerNotPresent.setImage(UIImage(named: "check_ipad"), for: .normal)
                viewCustomerSign.isHidden = true
                btnCustomerSign.isHidden = true
                btnTermsAndConditionsCheckMark.isHidden = true
                btnTermsAndCondition.isHidden = true
            }
            else
            {
                //btnCustomerNotPresent.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                viewCustomerSign.isHidden = false
                btnCustomerSign.isHidden = false
                btnTermsAndConditionsCheckMark.isHidden = false
                btnTermsAndCondition.isHidden = false
            }
            
            // isCustomerNotPresent
            
            if("\(match.value(forKey: "isCustomerNotPresent") ?? "")" == "true")
            {
                
                btnCustomerNotPresent.setImage(UIImage(named: "check_ipad"), for: .normal)
                chkCustomerNotPresent = true
                viewCustomerSign.isHidden = true
                btnCustomerSign.isHidden = true
                btnTermsAndConditionsCheckMark.isHidden = true
                btnTermsAndCondition.isHidden = true
                btnAllowCustomerToMakeSelection.isHidden = false
                lblAllowCustomerToMakeSelection.isHidden = false
                
            }
            else
            {
                
                btnCustomerNotPresent.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                chkCustomerNotPresent = false
                viewCustomerSign.isHidden = false
                btnCustomerSign.isHidden = false
                btnTermsAndConditionsCheckMark.isHidden = false
                btnTermsAndCondition.isHidden = false
                btnAllowCustomerToMakeSelection.isHidden = true
                lblAllowCustomerToMakeSelection.isHidden = true
                
            }
            
            
            if("\(match.value(forKey: "isEmployeePresetSignature") ?? "")" == "true")
            {
                strIsFormFilled = "true"
            }
            else
            {
                strIsFormFilled = "false"
            }
            
            strAccountNo = "\(match.value(forKey: "accountNo") ?? "")"
            
            
            if("\(match.value(forKey: "audioFilePath") ?? "")".count > 0)
            {
                strAudioName = "\(match.value(forKey: "audioFilePath")!)"
            }
            
            strLeadStatusGlobal = "\(match.value(forKey: "statusSysName") ?? "")"
            
            strStageSysName = "\(match.value(forKey: "stageSysName") ?? "")"
            
            // sales rep
            
            lblSalesRep.text = strEmpName
            
            
            strCustomerCompanyProfileImage = "\(match.value(forKey: "profileImage") ?? "")"
            
            strLeadNumber = "\(match.value(forKey: "leadNumber") ?? "")"
            
            strAccountManagerName = "\(match.value(forKey: "accountManagerName") ?? "")"
            
            strAccountManagerEmail = "\(match.value(forKey: "accountManagerEmail") ?? "")"
            
            strAccountManagerPrimaryPhone = "\(match.value(forKey: "accountManagerPrimaryPhone") ?? "")"
            
            strCustomerName = "\(match.value(forKey: "customerName") ?? "")"
            
            strCellNo = "\(match.value(forKey: "cellNo") ?? "")"
            
            let strBillingPocFirstname = "\(match.value(forKey: "billingFirstName") ?? "")"
            
            let strBillingPocMiddleName = "\(match.value(forKey: "billingMiddleName") ?? "")"
            
            let strBillingPocLastName = "\(match.value(forKey: "billingLastName") ?? "")"
            
            
            if(strBillingPocFirstname.count>0)
            {
                strBillingPocName = strBillingPocFirstname
            }
            
            if(strBillingPocMiddleName.count>0)
            {
                strBillingPocName = strBillingPocName.count > 0 ? strBillingPocName + "," + strBillingPocMiddleName:strBillingPocMiddleName
            }
            
            if(strBillingPocLastName.count>0)
            {
                strBillingPocName = strBillingPocName.count > 0 ? strBillingPocName + "," + strBillingPocLastName:strBillingPocLastName
            }
            
            
            strCustomerCompanyProfileImage = "\(match.value(forKey: "profileImage") ?? "")"
            
            strInspectorId = "\(match.value(forKey: "salesRepId") ?? "")"
            
            // Condition For Is Allow Customer To Make Selection
            let isAllowCustomerToMakeSelection = "\(match.value(forKey: "isSelectionAllowedForCustomer") ?? "")"
            
            if isAllowCustomerToMakeSelection == "true" || isAllowCustomerToMakeSelection == "True" || isAllowCustomerToMakeSelection == "1" {
                
                btnAllowCustomerToMakeSelection.setImage(UIImage(named: "checked.png"), for: .normal)
                
            }else{
                
                btnAllowCustomerToMakeSelection.setImage(UIImage(named: "uncheck.png"), for: .normal)
                
            }

            // Lead Inspetion Fee
            
            let strLeadInspectionFee = "\(match.value(forKey: "leadInspectionFee") ?? "")"
            
            let doubleLeadInspectionFee = (strLeadInspectionFee as NSString).doubleValue
            
            if doubleLeadInspectionFee > 0 {
                
                lblLeadInspectionFee.text = String(format: "%.2f", doubleLeadInspectionFee)
                
                HghtConstLeadInspectionFee.constant = 50.0
                HghtConstLeadInspectionFeeView.constant = 10.0
                leadInspectionFeeLineView.isHidden = false
                
            }else {
                
                lblLeadInspectionFee.text = "0.00"
                
                HghtConstLeadInspectionFee.constant = 0.0
                HghtConstLeadInspectionFeeView.constant = 0.0
                leadInspectionFeeLineView.isHidden = true

            }
            
            txtBuildingPermitFee.text = "\(match.value(forKey: "buildingPermitAmount") ?? "")"
            
            
            strOtherDiscountFromLeadDetail = "\(match.value(forKey: "otherDiscount") ?? "")"
            
            let doubleOtherDiscountFromLeadDetail = (strOtherDiscountFromLeadDetail as NSString).doubleValue

            if doubleOtherDiscountFromLeadDetail > 0
            {
                txtOtherDiscount.text = String(format: "%.2f", doubleOtherDiscountFromLeadDetail)
            }
            else
            {
                txtOtherDiscount.text = String(format: "%.2f", 0)
            }
            
        }
    }
    
    fileprivate func fetchTargets()
    {
        
        let arrOfTargetImagesCount = NSMutableArray()
        
        let sort = NSSortDescriptor(key: "name", ascending: true)
        
        arrayOfTargets = getDataFromCoreDataBaseArraySorted(strEntity: Entity_LeadCommercialTargetExtDc, predicate: (NSPredicate(format: "leadId == %@", strLeadId)), sort: sort)
        
        if arrayOfTargets.count > 0 {
            
            for k1 in 0 ..< arrayOfTargets.count {
                
                let dictData = arrayOfTargets[k1] as! NSManagedObject
                
                let leadCommercialTargetIdLocal = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                
                let mobileTargetIdLocal = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                
                if leadCommercialTargetIdLocal.count > 0 {
                    
                    let aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", strLeadId , leadCommercialTargetIdLocal))
                    
                    for k2 in 0 ..< aryTemp.count {
                        
                        arrOfTargetImagesCount.add("1")
                        
                    }
                    
                }else if mobileTargetIdLocal.count > 0 {
                    
                    let aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", strLeadId , mobileTargetIdLocal))
                    
                    for k2 in 0 ..< aryTemp.count {
                        
                        arrOfTargetImagesCount.add("1")
                        
                    }
                    
                }

                // leadCommercialTargetId   mobileTargetId
                
            }
            
            viewTargetHeader.isHidden = false
            hghtConstTargetHeader.constant = 60.0

        }else{
            
            hghtConstTargetHeader.constant = 0.0
            viewTargetHeader.isHidden = true
            
        }
        var heightTblTarget = 0.0
        
        for k1 in 0 ..< arrayOfTargets.count {
            
            let objArea = arrayOfTargets[k1] as! NSManagedObject
            
            let targetDescription = "\(objArea.value(forKey: "targetDescription") ?? "")"
            
            let attributedString =  attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: targetDescription), strMinFont: fontMin)
            
            
            let rect = attributedString.boundingRect(with: CGSize(width: self.tblViewTarget.frame.width-150, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
            print("attributedString Height = ",rect.height)
            
            heightTblTarget =  heightTblTarget + CGFloat(rect.height) + 50
            
            print(heightTblTarget)
            
        }
        
        constHghtTargetTblView.constant = CGFloat(heightTblTarget + 60) + CGFloat(arrOfTargetImagesCount.count*260)
        tblViewTarget.reloadData()
        
//        constHghtTargetTblView.constant = CGFloat(arrayOfTargets.count*120 + arrOfTargetImagesCount.count*250)
        
    }
    
    fileprivate func fetchBeforeImageFromLocalDB()
    {
        
        btnMainGraph.isHidden = true
        
        arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        //let arrOfProblemImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@", strWoId))

        let arrTempGraphImages = NSMutableArray()
        let arrTempBeforeImages = NSMutableArray()
        let arrTempAfterImages = NSMutableArray()

        if arrayOfImages.count > 0 {
            
            for k1 in 0 ..< arrayOfImages.count {
                
                let dictData = arrayOfImages[k1] as! NSManagedObject
                
                if "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "1" || "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "true" || "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "True"{

                    //const_HghtMainGraphImage.constant = 550
                    
                    let strImagePath = dictData.value(forKey: "woImagePath") as! String
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                    
                    let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                    
                    if isImageExists!  {
                        
                        mainGraphImageView.image = image
                        
                        btnMainGraph.isHidden = false
                        
                    }else {
                        
                        let defsLogindDetail = UserDefaults.standard
                        
                        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                        
                        var strURL = String()
                        
                        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                            
                            strURL = "\(value)"
                            
                        }
                        
                        strURL = strURL + "\(strImagePath)"
                        
                        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                        
                        let image: UIImage = UIImage(named: "NoImage.jpg")!
                        
                        mainGraphImageView.image = image
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            let url = URL(string:strURL)
                            let data = try? Data(contentsOf: url!)
                            
                            if data != nil && (data?.count)! > 0 {
                                
                                let image: UIImage = UIImage(data: data!)!
                                
                                DispatchQueue.main.async {
                                    
                                    saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                    
                                    self.mainGraphImageView.image = image
                                    
                                    self.btnMainGraph.isHidden = false

                                }}
                            
                        }
                        
                    }
                    
                    const_HghtMainGraphImage.constant = (image?.size.height)!
                    
                    btnMainGraph.frame = CGRect(x: btnMainGraph.frame.origin.x, y: btnMainGraph.frame.origin.y, width: btnMainGraph.frame.width, height: (image?.size.height)!)
                    
                    //arrTempGraphImages.add(dictData)
                    
                }else if "\(dictData.value(forKey: "woImageType") ?? "")" == "Graph" {
                    
                    arrTempGraphImages.add(dictData)
                    
                }else if "\(dictData.value(forKey: "woImageType") ?? "")" == "Before" {
                    
                    arrTempBeforeImages.add(dictData)
                    
                }else{
                    
                    arrTempAfterImages.add(dictData)

                }
                
            }
            
        }
        
        
        let arrTempAllImages = NSMutableArray()
        
        //arrTempAllImages.addObjects(from: arrOfProblemImages as! [Any])

        arrTempAllImages.addObjects(from: arrTempGraphImages as! [Any])
        
        //arrTempAllImages.addObjects(from: arrOfProblemImages as! [Any])

        arrTempAllImages.addObjects(from: arrTempBeforeImages as! [Any])

        arrTempAllImages.addObjects(from: arrTempAfterImages as! [Any])

        arrayOfImages = arrTempAllImages
        
        if(arrayOfImages.count > 0)
        {
            tblviewGraphAndImages.isHidden = false
            
            hghtConstTblViewGraphAndImage.constant = CGFloat(arrayOfImages.count*250)
            
            tblviewGraphAndImages.reloadData()
        }
        else
        {
            tblviewGraphAndImages.isHidden = true
            hghtConstTblViewGraphAndImage.constant = 0.0
            
        }
    }
    fileprivate func fetchPaymentInfoFromLocalDB()
    {
        let ary = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(ary.count > 0)
        {
            let match = ary.firstObject as! NSManagedObject
            
            strCustomerSign = "\(match.value(forKey: "customerSignature") ?? "")"
            strTechnicianSign = "\(match.value(forKey: "salesSignature") ?? "")"
            strGlobalPaymentMode = "\(match.value(forKey: "paymentMode") ?? "")"
            
            strCheckFrontImage = "\(match.value(forKey: "checkFrontImagePath") ?? "")"
            
            strCheckBackImage = "\(match.value(forKey: "checkBackImagePath") ?? "")"
            
            if(strGlobalPaymentMode == "Cash")
            {
                cashPaymentMode()
            }
            else if(strGlobalPaymentMode == "Check")
            {
                checkPaymentMode()
            }
            else if(strGlobalPaymentMode == "CreditCard")
            {
                creditCardPaymentMode()
            }
            else if(strGlobalPaymentMode == "AutoChargeCustomer")
            {
                autoChargeCustomerPaymentMode()
            }
            else if(strGlobalPaymentMode == "CollectattimeofScheduling")
            {
                collectAtTimeOfSchedulingPaymentMode()
            }
            else if(strGlobalPaymentMode == "Invoice")
            {
                invoicePaymentMode()
            }
            
            if("\(match.value(forKey: "amount") ?? "")" .count > 0)
            {
                txtAmountPaymentMode.text = "\(match.value(forKey: "amount")!)"
            }
            
            if("\(match.value(forKey: "checkNo") ?? "")" .count > 0)
            {
                txtCheckNoPaymentMode.text = "\(match.value(forKey: "checkNo")!)"
            }
            
            if("\(match.value(forKey: "licenseNo") ?? "")" .count > 0)
            {
                txtDrivingLicenceNoPaymentMode.text = "\(match.value(forKey: "licenseNo")!)"
            }
            
            if("\(match.value(forKey: "expirationDate") ?? "")" .count > 0)
            {
                btnSelectExpirationdate.setTitle("\(match.value(forKey: "expirationDate")!)", for: .normal)
                
                strExpirationDate = "\(match.value(forKey: "expirationDate")!)"
            }
            
            if("\(match.value(forKey: "specialInstructions") ?? "")" .count > 0)
            {
                txtviewAdditionalNotes.text = "\(match.value(forKey: "specialInstructions")!)"
                
            }
            
        }
        
        showAddCardButton()
    }
    
    fileprivate func fetchInspectionDetail()
    {
        var objInspectionDetail = NSManagedObject()
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0
        {
            objInspectionDetail = arrOfWdoInspection[0] as! NSManagedObject
            
            lblOrderedBy.text  = "\(objInspectionDetail.value(forKey: "orderedBy") ?? "")"
            
            lblAddressOrderBy.text = "\(objInspectionDetail.value(forKey: "orderedByAddress") ?? " ")"
            
            if (lblAddressOrderBy.text == "")
            {
                lblAddressOrderBy.text = "    "
            }
            
            
            
            lblPropertyOwner.text  = "\(objInspectionDetail.value(forKey: "propertyOwner") ?? "")"
            lblAddressPropertyOwner.text  = "\(objInspectionDetail.value(forKey: "propertyOwnerAddress") ?? " ")"
            
            if (lblAddressPropertyOwner.text == "")
            {
                lblAddressPropertyOwner.text = "    "
            }
            
            
            lblReportSentTo.text  = "\(objInspectionDetail.value(forKey: "reportSentTo") ?? "")"
            lblAddressReportSentTo.text  = "\(objInspectionDetail.value(forKey: "reportSentToAddress") ?? " ")"
            
            if (lblAddressReportSentTo.text == "")
            {
                lblAddressReportSentTo.text = "    "
            }
            
            
            lblReportType.text  = "\(objInspectionDetail.value(forKey: "reportTypeName") ?? "")"
            
            let strGarageID = "\(objInspectionDetail.value(forKey: "garage") ?? "")"
            
            if strGarageID.count > 0 {
                
                if  nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                    
                    let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as!  NSDictionary
                    
                    let arrayData = (dictMaster.value(forKey: "Garages") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let aryTemp = arrayData.filter { (task) -> Bool in
                        
                        return "\((task as! NSDictionary).value(forKey: "Value")!)".lowercased() == strGarageID.lowercased()
                        
                    }
                    
                    if(aryTemp.count != 0){
                        
                        let dict = aryTemp[0] as! NSDictionary
                        
                        self.lblGarage.text = "\(dict.value(forKey: "Text")!)"
                        
                    }
                    
                }
                
            }
            
            
            lblSubareaAccess.text  = "\(objInspectionDetail.value(forKey: "subareaAccess") ?? "")"
            
            lblNoOfStories.text  = "\(objInspectionDetail.value(forKey: "numberOfStories") ?? "")"
            
            lblExternalMaterial.text  = "\(objInspectionDetail.value(forKey: "externalMaterial") ?? "")"
            
            lblRoofingMaterial.text  = "\(objInspectionDetail.value(forKey: "roofingMaterial") ?? "")"
            
            lblYearOfStructure.text  = "\(objInspectionDetail.value(forKey: "yearOfStructure") ?? "")"
            
            
            let isYearKnown = "\(objInspectionDetail.value(forKey: "isYearUnknown") ?? "")"
            
            if isYearKnown == "1"{
                
                lblYearUnknown.text  = "Yes"
                lblYearOfStructure.text  = "UnKnown"

            }else{
                
                lblYearUnknown.text  = "No"
                
            }
            
            if objInspectionDetail.value(forKey: "isBuildingPermit") as! Bool == true
            {
                lblBuildingPermit.text  = "Yes"
                /*HghtConstBuldingPermitFee.constant = 50.0
                HghtConstBuldingPermitView.constant = 10.0
                buldingPermitFeeLineView.isHidden = false*/

            }
            else
            {
                lblBuildingPermit.text  = "No"
                /*HghtConstBuldingPermitFee.constant = 0.0
                HghtConstBuldingPermitView.constant = 0.0
                buldingPermitFeeLineView.isHidden = true*/

            }
            
            
            lblEmailAddressInspectionDetails.text  = ""//"\(objInspectionDetail.value(forKey: "") ?? "")"
            
            
            lblCPCInspTagPosted.text  = "\(objInspectionDetail.value(forKey: "cpcInspTagPosted") ?? "")"
            
            lblPastPcoTagPosted.text  = "\(objInspectionDetail.value(forKey: "pastPcoTagPosted") ?? "")"
            
            lblNameOfPrevPCO.text  = "\(objInspectionDetail.value(forKey: "nameOfPrevPco") ?? "")"
            
            lblDateOfPrevPCO.text  = "\(objInspectionDetail.value(forKey: "dateOfPrevPco") ?? "")"
            
           
            if "\(objInspectionDetail.value(forKey: "subareaAccess") ?? "")" == strOtherString {
                lblSubareaAccess.text = "\(objInspectionDetail.value(forKey: "othersSubAreaAccess") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "numberOfStories") ?? "")" == "0" {
                lblNoOfStories.text = "\(objInspectionDetail.value(forKey: "othersNumberofStories") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "externalMaterial") ?? "")" == strOtherString {
                lblExternalMaterial.text = "\(objInspectionDetail.value(forKey: "othersExteriorMaterial") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "roofingMaterial") ?? "")" == strOtherString {
                lblRoofingMaterial.text = "\(objInspectionDetail.value(forKey: "othersRoofMaterials") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "cpcInspTagPosted") ?? "")" == strOtherString {
                lblCPCInspTagPosted.text = "\(objInspectionDetail.value(forKey: "othersCpcTagLocated") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "pastPcoTagPosted") ?? "")" == strOtherString {
                lblPastPcoTagPosted.text = "\(objInspectionDetail.value(forKey: "othersOtherPcoTag") ?? "")"
            }

            
            if objInspectionDetail.value(forKey: "isSeparateReport") as! Bool == true
            {
                lblSeparateReport.text  = "Yes"
            }
            else
            {
                lblSeparateReport.text  = "No"
            }
            
            lblGeneralDescription.text = "\(objInspectionDetail.value(forKey: "generalDescription") ?? " ")"
            
            
            if (lblGeneralDescription.text == "")
            {
                lblGeneralDescription.text = "    "
            }
            
            lblStructureDescription.text = "\(objInspectionDetail.value(forKey: "structureDescription") ?? "  ")"
            
            if (lblStructureDescription.text == "")
            {
                lblStructureDescription.text = "    "
            }
            
            lblComments.text = "\(objInspectionDetail.value(forKey: "comments") ?? "  ")"
            
            if (lblComments.text == "")
            {
                lblComments.text = "    "
            }
            
            if objInspectionDetail.value(forKey: "isTipWarranty") as! Bool == true
            {
                lblTipWarranty.text  = "Yes"
                
                let strTipMasterName = "\(objInspectionDetail.value(forKey: "tIPMasterName") ?? "")"
                let strTipMasterId = "\(objInspectionDetail.value(forKey: "tIPMasterId") ?? "")"

                lblTipWarrantyType.text  = strTipMasterName
                
                lblMonthlyAmount.text  = "\(objInspectionDetail.value(forKey: "monthlyAmount") ?? "")"
                
                var dictOfTipWarranty = NSDictionary()
                
                var ary = NSMutableArray()
                
                ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
                
                if ary.count > 0 {
                    
                    for k in 0 ..< ary.count {
                        
                        let dictData = ary[k] as! NSDictionary
                        
                        if strTipMasterId == "\(dictData.value(forKey: "TIPMasterId") ?? "")" {
                            
                            dictOfTipWarranty = dictData
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                if dictOfTipWarranty.count > 0 {
                    
                    let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                    
                    if !isMonthlyAmount {
                        
                        lblMonthlyAmount.text  = ""
                        
                    }else{
                        
                        isApplyTIPShow = true
                        stk_TipWarrenty.isHidden = false
                       
                    }
                  
                }
            }
            else
            {
                lblTipWarranty.text  = "No"
                lblTipWarrantyType.text  = ""
                lblMonthlyAmount.text  = ""
            }
            
            
            //SHow TIP/THPS
            let strTipType = "\(objInspectionDetail.value(forKey: "tIPTHPSType") ?? "")"
            if strTipType.lowercased() == WDO_TIP_Type.THPS.lowercased() {
                lbl_ApplyTIP_Yes.text = "THPS warranty will be provided for this property for $\(lblMonthlyAmount.text ?? "0")/month once the necessary work on this service agreement is completed."
                lbl_ApplyTIP_No.text = "Customer declines the THPS warranty."
               lbl_TIP_Title_InspectionDetail.text = "THPS Warranty type:"
                lbl_TIP_Title_PriceDetail.text = "THPS Discount($):"
                txtTotalTIPDiscount.placeholder = "Enter THPS Discount"
                lbl_ApplyTIP_Title.text = "Apply THPS Charge"
            }else{
                lbl_ApplyTIP_Yes.text = "TIP warranty will be provided for this property for $\(lblMonthlyAmount.text ?? "0")/month once the necessary work on this service agreement is completed."
                lbl_ApplyTIP_No.text = "Customer declines the TIP warranty."
               lbl_TIP_Title_InspectionDetail.text = "TIP Warranty type:"
                lbl_TIP_Title_PriceDetail.text = "TIP Discount($):"
                txtTotalTIPDiscount.placeholder = "Enter TIP Discount"
                lbl_ApplyTIP_Title.text = "Apply TIP Charge"


            }
            
            // check is apply TIP
            
            strApplyTIP = "\(objInspectionDetail.value(forKey: "isApplyTipDiscount") ?? "")"
            if strApplyTIP.lowercased() == "true" {
                
                btn_YesApplyTIP.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btn_NoApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }else if strApplyTIP.lowercased() == "false" {
                
                btn_NoApplyTIP.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
                btn_YesApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }else {
                
                btn_NoApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                btn_YesApplyTIP.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
                
            }
            
            let checkBoxes = "\(objInspectionDetail.value(forKey: "wdoTermiteIssueCodeSysNames") ?? "")"
            
            var tempArray = NSArray()
            let tempTermiteIssuesCodeName = NSMutableArray()
            
            if checkBoxes.count > 1 {
                
                tempArray = checkBoxes.components(separatedBy: ",") as NSArray
                
            }
            
            if tempArray.count > 0 {
                
                if tempArray.contains(enumSUBTERRANEANTERMITES) {
                    
                    tempTermiteIssuesCodeName.add("SUBTERRANEAN TERMITES")
                    
                }
                if tempArray.contains(enumDRYWOODTERMITES) {
                    
                    tempTermiteIssuesCodeName.add("DRYWOOD TERMITES")
                    
                }
                if tempArray.contains(enumFUNGUSDRYROT) {
                    
                    tempTermiteIssuesCodeName.add("FUNGUS/DRYROT")
                    
                }
                if tempArray.contains(enumOTHERFINDINGS) {
                    
                    tempTermiteIssuesCodeName.add("OTHER FINDINGS")
                    
                }
                
                lblTermites.text = tempTermiteIssuesCodeName.componentsJoined(by: ", ")
                
            }else{
                
                lblTermites.text = "     "
                
            }
            
            
            var strSlabRaised = "\(objInspectionDetail.value(forKey: "slabRaised") ?? "")"
            
            if strSlabRaised == "1" {
                
                strSlabRaised = "Slab"
                
            }
            else if strSlabRaised == "2"{
                
                strSlabRaised = "Raised"
                
            }
            else if strSlabRaised == "3"{
                
                strSlabRaised = "Slab/Raised"
                
            }else {
                
                strSlabRaised = ""
                
            }
            
            var strOccupiedVacant = "\(objInspectionDetail.value(forKey: "occupiedVacant") ?? "")"
            
            if strOccupiedVacant == "1" {
                
                strOccupiedVacant = "Occupied"
                
            }
            else if strOccupiedVacant == "2"{
                
                strOccupiedVacant = "Vacant"
                
            }else{
                
                strOccupiedVacant = ""
                
            }
            
            var strFurnishedUFurnished = "\(objInspectionDetail.value(forKey: "furnishedUnfurnished") ?? "")"
            
            if strFurnishedUFurnished == "2" {
                
                strFurnishedUFurnished = "Furnished"
                
            }
            else if strFurnishedUFurnished == "1"{
                
                strFurnishedUFurnished = "Unfurnished"
                
            }
            else{
                
                strFurnishedUFurnished = "N/A'"
                
            }
            
            lblOther.text = "\(strOccupiedVacant), " + "\(strFurnishedUFurnished), " + "\(strSlabRaised)"
            
            showNameFromSysNameForInspectionFields(objData: objInspectionDetail)

            // Total TIP Discount
            
            txtTotalTIPDiscount.text = "\(objInspectionDetail.value(forKey: "totalTIPDiscount") ?? "")"
            
        }else{
            
            txtTotalTIPDiscount.text = "0.00"
            
        }
    }
    
    func showNameFromSysNameForInspectionFields(objData : NSManagedObject) {
        
        // Sub Area Access
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "SubAreaAccesss", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "subareaAccess") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblSubareaAccess.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Number Of Stories
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "NumberofStories", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "numberOfStories") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblNoOfStories.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // External Materials
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "ExteriorMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "externalMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblExternalMaterial.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        
        // Roofing Materials
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RoofMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "roofingMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblRoofingMaterial.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        
        // CPC Tag Posted
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CpcTagLocateds", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "cpcInspTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblCPCInspTagPosted.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Past Pco Tag Posted
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "OtherPcoTags", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "pastPcoTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblPastPcoTagPosted.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Structure Descriptions
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "StructuredDescriptions", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "structureDescription") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblStructureDescription.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
        }
    }
    
    fileprivate func fetchWoWdoProblemIdentificationExtSerDcs()
    {
        
        var isBuildingPermitLocal = false
        
        arrayProblemIdentification.removeAllObjects()
        arrayProblemIdentificationPricing = NSMutableArray()
        
        //let arrayPI = getDataFromLocal(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@",strWoId)).mutableCopy() as! NSArray
        
        let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
        let arrayPI = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort)
        
        if(arrayPI.count > 0)
        {
            
            arrayProblemIdentification.removeAllObjects()
            arrayProblemIdentificationPricing = NSMutableArray()
            
            //            arrayProblemIdentification = arrayPI.mutableCopy() as! NSMutableArray
            for obj in arrayPI
            {
                arrayProblemIdentification.add(getMutableDictionaryFromNSManagedObject(obj: obj as! NSManagedObject))
            }
            
            for item in arrayProblemIdentification
            {
                
                if (item as AnyObject).value(forKey: "isBuildingPermit") as! Bool == true
                {
                    HghtConstBuldingPermitFee.constant = 50.0
                    //HghtConstBuldingPermitView.constant = 30.0
                    lblBuildingPermitFeeText.isHidden = false
                    viewBuildingPermitViewNew.isHidden = false
                    buldingPermitFeeLineView.isHidden = false
                    isBuildingPermitLocal = true
                }
                
                let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)")).mutableCopy() as! NSArray
                
                if(arrayPIPricing.count > 0)
                {
                    arrayProblemIdentificationPricing.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                }else{
                    
                    // to check for mobile id
                    
                    let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "mobileId")!)")).mutableCopy() as! NSArray
                    
                    if(arrayPIPricing.count > 0)
                    {
                        arrayProblemIdentificationPricing.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                    }
                    
                }
            }
            
            var heightProblemCell = 0
            
            for k in 0 ..< arrayProblemIdentificationPricing.count {
                
                let dictData = arrayProblemIdentificationPricing[k] as! NSDictionary
                
                let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
                
                if isBidOnRequestLocal {
                    
                    heightProblemCell = heightProblemCell + 264
                    
                } else {
                    
                    heightProblemCell = heightProblemCell + 350
                    
                }
                
            }
            
            hghtConstViewContainerPrice.constant = CGFloat(104 + heightProblemCell)
            
            var totalPrice = 0.0
            for item in arrayProblemIdentificationPricing
            {
                if("\((item as! NSMutableDictionary).value(forKey: "total") ?? "")".count > 0)
                {
                    totalPrice = totalPrice + Double(((item as! NSMutableDictionary).value(forKey: "total") as! NSString).floatValue)
                }
            }
            
            lblTotalPricing.text = "Total($):" +  String(format: "%.2f", totalPrice)
            
            tblviewPricing.reloadData()
        }
        else
        {
            hghtConstViewContainerPrice.constant = 0.0
        }
        
        if !isBuildingPermitLocal {
            
            txtBuildingPermitFee.text = "0.00";
            
        }
        
    }
    
    fileprivate func methodToCheckIfElectronicFormExistsForLeadId(strleadid:String)-> Bool
    {
        let ary = getDataFromCoreDataBaseArray(strEntity: "ElectronicAuthorizedForm", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(ary.count > 0)
        {
            return true
        }
        
        return false
        
    }
    
    fileprivate func getUpdatedHtmlString(strMutableText:NSMutableString, stringToReplace:NSString, strWithReplace:NSString)-> NSMutableString
    {
        let range = strMutableText.range(of: stringToReplace as String)
        
        if(range.location == NSNotFound)
        {
            
        }
        else
        {
            do
            {
                
                strMutableText.replaceCharacters(in: range, with: strWithReplace as String)
            }
            
        }
        
        return strMutableText
    }
    
    /*-(NSMutableString*)getUpdateHtmlString : (NSMutableString*) strMutableText stringToReplace: (NSString *)stringToReplace stringWithReplace:(NSString*)strWithReplace
     {
     NSRange range = [strMutableText rangeOfString:stringToReplace];
     
     if(range.location == NSNotFound)
     {
     
     }
     else
     {
     @try
     {
     [strMutableText replaceCharactersInRange:range withString:strWithReplace];
     }
     @catch (NSException *exception)
     {
     
     }
     @finally
     {
     
     }
     // [strMutableText replaceCharactersInRange:range withString:strWithReplace];
     }
     
     return strMutableText;
     }*/
    
    
    // MARK: ------------- Pestpac Integration -------------------
    
    func showAddCardButton()  {
        
        if strGlobalPaymentMode != ""
        {
            if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
            {
                btnAddCard.isHidden = false
                lblCardType.text = self.showCreditCardDetail()
                lblCardType.isHidden = false
            }
            else
            {
                btnAddCard.isHidden = true
                lblCardType.isHidden = true
            }
        }
        else
        {
            btnAddCard.isHidden = true
            lblCardType.isHidden = true
        }
    }

    func showCreditCardDetail() -> String {
        // if billToLocationId.count > 0 && billToLocationId != 0 && paymentMode = "CreditCard"
        // show add card button else hide
        
        let billToId = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"//"1693164"
        var strTypeNo = "\(matchesGeneralInfo.value(forKey: "creditCardType") ?? "")"//"Discover" //Nedd to add by back end - creditCardType
        let creditCardNumber = "\(matchesGeneralInfo.value(forKey: "cardAccountNumber") ?? "")"//"*************7890" //Nedd to add by back end - creditCardNo
        if creditCardNumber.count >= 4  {
            let last4 = creditCardNumber.suffix(4)
            strTypeNo = strTypeNo + "  \(last4)"
        }
        // For Local Check.
        if(nsud.value(forKey: billToId) != nil){
            
            if nsud.value(forKey: billToId) is NSDictionary {
                
                let dictCreditCardInfo = nsud.value(forKey: billToId) as! NSDictionary
                let accountNumber = String("\(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")".suffix(4))
                strTypeNo = "\(dictCreditCardInfo.value(forKey: "creditCardType") ?? "") " + accountNumber
            }
        }
        
        print(strTypeNo) // Show this value with Add Cart button If strtypeno.count > 0 than only show card value lable
        return strTypeNo
    }
    
    func goToPestPac_PaymentIntegration() {
        
        self.view.endEditing(true)
        
        if isInternetAvailable() {
            
            let firstNameL = "\(matchesGeneralInfo.value(forKey: "billingFirstName") ?? "")"
            let lastNameL = "\(matchesGeneralInfo.value(forKey: "billingLastName") ?? "")"
            let billingAddress1L = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
            let billingZipcodeL = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
            let billToLocationIdL = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"
            let webLeadIdL = strLeadId
            let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

            let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
            
            let ammountL = Double(txtAmountPaymentMode.text ?? "") ?? 0.0
                        
            if contactNameToCheck.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
                
            }
            else if contactName.count > 30 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
                
            }
            else if billingAddress1L.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
                
            }else if billingZipcodeL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
                
            }else if billToLocationIdL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
                
            }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
                
            }
//            else if ammountL <= 0 {
//
//                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
//
//            }
            else
            {
                
                let dictPaymentDetails = NSMutableDictionary()

                dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
                dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
                dictPaymentDetails.setValue("\(ammountL)", forKey: "amount")
                dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
                dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
                dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
                vc?.dictPaymentDetails = dictPaymentDetails
                self.navigationController?.present(vc!, animated: false, completion: nil)

            }
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
        /*if isInternetAvailable() {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc!.delegate = self
            vc!.strViewComeFrom = "Dashboard"
            self.navigationController?.present(vc!, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }*/
        
    }
    
    
    
}

extension Agreement_WDOVC : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if(webView == webviewCoverLetter)
        {
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            webView.frame.size.width = self.view.frame.width-20
            webView.scrollView.isScrollEnabled=true;
            hghtContWebViewCoverLetter.constant = webView.scrollView.contentSize.height + 50
            webView.scalesPageToFit = true
            webviewCoverLetter.autoresizesSubviews = true
        }
        if(webView == webviewIntroduction)
        {
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            webView.frame.size.width = self.view.frame.width-20
            webView.scrollView.isScrollEnabled=true;
            hghtConstWebViewIntroLetter.constant = webView.scrollView.contentSize.height + 50
            webView.scalesPageToFit = true
        }
        if(webView == webviewSalesMarketingContent)
        {
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            webView.frame.size.width = self.view.frame.width-20
            webView.scrollView.isScrollEnabled=true;
            hghtConstWebViewMarketingContent.constant = webView.scrollView.contentSize.height + 50
            webView.scalesPageToFit = true
        }
    }
}

// MARK: UITableView Delegate and DataSource

extension Agreement_WDOVC:UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(tableView.tag == 102)
        {
            return arrayOfTargets.count
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(tableView.tag == 102)
        {
            let dictData = arrayOfTargets[section] as! NSManagedObject
            let targetDescription = "\(dictData.value(forKey: "targetDescription") ?? "")"

            let attributedString =  attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: targetDescription), strMinFont: fontMin)
            let rect = attributedString.boundingRect(with: CGSize(width: tableView.frame.width-150, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
            print("attributedString Height = ",rect.height)
            
            return CGFloat(rect.height) + 50
        } else {
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(tableView.tag == 102)
        {
            let dictData = arrayOfTargets[section] as! NSManagedObject
            
            let name = "Name: \(dictData.value(forKey: "name") ?? "")"
            
            let targetDescription = "\(dictData.value(forKey: "targetDescription") ?? "")"
 
            let attributedString = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: targetDescription), strMinFont: fontMin)
            let rect = attributedString.boundingRect(with: CGSize(width: tableView.frame.width-150, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
            print("attributedString Height = ",rect.height)
            
            let vw = UIView(frame: CGRect(x: 20, y: 0, width: tableView.frame.width-40, height: CGFloat(rect.height) + 50))
            vw.backgroundColor = UIColor.lightTextColorTimeSheet()
            vw.backgroundColor = UIColor.clear

            //let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.width-20, height: 50))
            //let lblDesc = UILabel(frame: CGRect(x: 20, y: 50, width: tableView.frame.width-20, height: 50))
            
            let lbl = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.width-40, height: 30))
            let lblDesTitle = UILabel(frame: CGRect(x: 20, y: lbl.frame.maxY, width: 135, height: 30))

            let lblDesc = UILabel(frame: CGRect(x: lblDesTitle.frame.maxX, y: lbl.frame.maxY, width: tableView.frame.width-(lblDesTitle.frame.maxX + 20), height: CGFloat(rect.height) ))

           
            lbl.text = name
            lbl.font = UIFont.systemFont(ofSize: 20)
            lbl.textColor = UIColor.black
            vw.addSubview(lbl)
            
            lblDesTitle.text = "Description:"
            lblDesTitle.font = UIFont.systemFont(ofSize: 20)
            vw.addSubview(lblDesTitle)

    
            lblDesc.textColor = UIColor.black
            lblDesc.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: targetDescription), strMinFont: fontMin)
            
            //lblDesc.text = "\(targetDescription)"//getAttributedHtmlStringUnicode(strText: targetDescription)
            lblDesc.numberOfLines = 0
            vw.addSubview(lblDesc)
            
            return vw
            
        }else{
            
            
            let vw = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            vw.backgroundColor = UIColor.clear
            return vw

        }
        
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if(tableView.tag == 102)
        {
            
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
            footerView.backgroundColor = UIColor.lightTextColorTimeSheet()
            return footerView
            
        }else{
            
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            footerView.backgroundColor = UIColor.clear
            return footerView
            
        }

    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if(tableView.tag == 102)
        {
            
            return 1

        }
        else{
            
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView.tag == 100)
        {
            
            return arrayOfImages.count
            
        }
        else if(tableView.tag == 101)
        {
            
            return arrayProblemIdentification.count
            
        }else{
            
            let dictData = arrayOfTargets[section] as! NSManagedObject
            
            let leadCommercialTargetIdLocal = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
            
            let mobileTargetIdLocal = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
            
            if leadCommercialTargetIdLocal.count > 0 {
                
                let aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", strLeadId , leadCommercialTargetIdLocal))
                
                return aryTemp.count

            }else if mobileTargetIdLocal.count > 0 {
                
                let aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", strLeadId , mobileTargetIdLocal))
                
                return aryTemp.count

            } else {
                
                return 0
                
            }

        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(tableView.tag == 100)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GraphImagesCell") as! GraphImagesCell
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            cell.lblCaption.text = (objTemp.value(forKey: "imageCaption") as! String).count>0 ? "" + (objTemp.value(forKey: "imageCaption") as! String) : ""
            cell.lblDescription.text = (objTemp.value(forKey: "imageDescription") as! String).count>0 ? "" + (objTemp.value(forKey: "imageDescription") as! String) : ""
            
            let strImagePath = objTemp.value(forKey: "woImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgview.image = image
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                strURL = strURL + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                let strUrlwithString = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgview.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strUrlwithString ?? "")
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgview.image = image
                            
                        }}
                    
                }
                
            }
            
            cell.selectionStyle = .none
            
            return cell
            
        }
        else if(tableView.tag == 101)
        {
            
            // pricing cell
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PricingCell") as! PricingCell
            
            //makeCornerRadius(value: 2.0, view: cell.viewContainer, borderWidth: 1.0, borderColor: UIColor.lightGray)
            
            //makeCornerRadius(value: 2.0, view: cell.txtviewDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
            
            let dataPI = arrayProblemIdentification.object(at: indexPath.row) as! NSMutableDictionary
            
            let dataPIPricing = arrayProblemIdentificationPricing.object(at: indexPath.row) as! NSMutableDictionary
            
            cell.lblIssueCode.text = "\(dataPI.value(forKey: "issuesCode") ?? "")"
            
            // cell.lblRecommendationCode.text = "\(dataPI.value(forKey: "recommendationCode") ?? "")"
            // cell.lblSubsectionCode.text = "\(dataPI.value(forKey: "subsectionCode") ?? "")"
            //cell.txtviewDescription.text = "\(dataPI.value(forKey: "recommendation") ?? "")"
            
            let strFillingLocal = "\(dataPI.value(forKey: "recommendationFillIn") ?? "")"
            
            let strRecommendationLocal = "\(dataPI.value(forKey: "proposalDescription") ?? "")"
            
            var strReplacedString = replaceTags(strFillingLocal: strFillingLocal, strFindingLocal: strRecommendationLocal)
            
            cell.txtviewDescription.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
            
     
            cell.lblPrice.text = "0.00"
            cell.lblDiscount.text = "0.00"
            cell.lblTotal.text = "0.00"
            
            if("\(dataPIPricing.value(forKey: "subTotal") ?? "")".count > 0)
            {
                let val = (dataPIPricing.value(forKey: "subTotal") as! NSString).doubleValue
                
                cell.lblPrice.text = String(format: "%.2f", val)
            }
            
            if("\(dataPIPricing.value(forKey: "discount") ?? "")".count > 0)
            {
                let val = (dataPIPricing.value(forKey: "discount") as! NSString).doubleValue
                
                cell.lblDiscount.text = String(format: "%.2f", val)
            }
            
            if("\(dataPIPricing.value(forKey: "total") ?? "")".count > 0)
            {
                let val = (dataPIPricing.value(forKey: "total") as! NSString).doubleValue
                
                cell.lblTotal.text = String(format: "%.2f", val)
            }
            
            cell.btnAddToAgreement.tag = indexPath.row
            cell.btnAddToAgreement.isUserInteractionEnabled = true
          
            let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
            let fullAccess = opprtunityAccess.status
            let limitedAccess = opprtunityAccess.opportunityLimitedAccess

           
            
            if fullAccess && limitedAccess{
                cell.btnAddToAgreement.isUserInteractionEnabled = false
            }
            else if(!fullAccess && limitedAccess){
                cell.btnAddToAgreement.isUserInteractionEnabled = true

            }else{
                
            }
            
            
            
            
            cell.btnAddToAgreement.addTarget(self, action: #selector(Agreement_WDOVC.action_AddToAgreement(_:)), for: .touchUpInside)
            
            if let isAgreement = dataPI.value(forKey: "isAddToAgreement")
            {
                print(isAgreement)
                
                if(dataPI.value(forKey: "isAddToAgreement") as! Bool == true)
                {
                    cell.btnAddToAgreement.setImage(UIImage(named: "checked.png"), for: .normal)
                }
                else
                {
                    cell.btnAddToAgreement.setImage(UIImage(named: "uncheck.png"), for: .normal)
                }
            }
            
            if let isBidOnRequest = dataPIPricing.value(forKey: "isBidOnRequest")
            {
                print(isBidOnRequest)

                if(dataPIPricing.value(forKey: "isBidOnRequest") as! Bool == true)
                {
                    cell.btnBidOnRequest.setImage(UIImage(named: "checked.png"), for: .normal)
                    
                    cell.btnBidOnRequest.isHidden = false
                    cell.lblBidOnRequest.isHidden = false
                    
                    let isNoBidGiven = "\(dataPIPricing.value(forKey: "isNoBidGiven") ?? "")"
                    if(isBidOnRequest as! Bool && (isNoBidGiven.lowercased() == "true" || isNoBidGiven == "1")){
                        strReplacedString =  strReplacedString.replacingOccurrences(of: "NO BID GIVEN", with: "")
                            let noBidGivenboldText  = " NO BID GIVEN"
                            let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
                            let boldString = NSMutableAttributedString(string: noBidGivenboldText, attributes:attrs)
                            let atributed = getAttributedHtmlStringUnicode(strText: strReplacedString).mutableCopy() as! NSMutableAttributedString
                            atributed.append(boldString)
                            cell.txtviewDescription.attributedText  = attributedTextFontUpdate(str: atributed, strMinFont: fontMin)
                        }
                    
                }
                else
                {
                    cell.btnBidOnRequest.setImage(UIImage(named: "uncheck.png"), for: .normal)
                    
                    cell.btnBidOnRequest.isHidden = true
                    cell.lblBidOnRequest.isHidden = true
                    
                }
            }
            
            //makeCornerRadius(value: 2.0, view:  cell.txtviewDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
            
            cell.selectionStyle = .none
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TargetCell") as! TargetCell

            let dictData = arrayOfTargets[indexPath.section] as! NSManagedObject
            
            let leadCommercialTargetIdLocal = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
            
            let mobileTargetIdLocal = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
            
            var aryTemp = NSArray()
            
            if leadCommercialTargetIdLocal.count > 0 {
                
                aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", strLeadId , leadCommercialTargetIdLocal))
                
            }else if mobileTargetIdLocal.count > 0 {
                
                aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", strLeadId , mobileTargetIdLocal))
                
            }
            
            let objTemp = aryTemp[indexPath.row] as! NSManagedObject
            
            cell.lblCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "" + (objTemp.value(forKey: "leadImageCaption") as! String) : ""
            cell.lblDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : ""
            
            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                cell.imgview.image = image
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                //strURL = strURL + "\(strImagePath)"
                strURL = strURL + "Documents/Targetimages/" + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.imgview.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.imgview.image = image
                            
                        }}
                    
                }
                
            }
            
            cell.selectionStyle = .none
            
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(tableView.tag == 100)
        {
            
            let image: UIImage = UIImage(named: "NoImage.jpg")!
            
            var imageView = UIImageView(image: image)
            
            let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "woImagePath") as! String
            
            let imageLocal: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                //imageView.image = imageLocal
                imageView = UIImageView(image: imageLocal)
                
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }else if(tableView.tag == 102)
        {
            
            let image: UIImage = UIImage(named: "NoImage.jpg")!
            
            var imageView = UIImageView(image: image)
            
            let dictData = arrayOfTargets[indexPath.section] as! NSManagedObject
            
            let leadCommercialTargetIdLocal = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
            
            let mobileTargetIdLocal = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
            
            var aryTemp = NSArray()
            
            if leadCommercialTargetIdLocal.count > 0 {
                
                aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", strLeadId , leadCommercialTargetIdLocal))
                
            }else if mobileTargetIdLocal.count > 0 {
                
                aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", strLeadId , mobileTargetIdLocal))
                
            }
            
            let objTemp = aryTemp[indexPath.row] as! NSManagedObject
            
            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
            
            let imageLocal: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                //imageView.image = imageLocal
                imageView = UIImageView(image: imageLocal)
                
            }
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView.tag == 100)
        {
            return 250.0
            
        }else if(tableView.tag == 101)
        {
            
            let dictData = arrayProblemIdentificationPricing[indexPath.row] as! NSDictionary
            
            let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
            
            if isBidOnRequestLocal {
                
                return 264.0
                
            } else {
                
                return 350.0
                
            }
            
        }else{
            return 250.0
            
        }
    }
    
    // MARK: --------------------- Nilind  --------------
    
    func saveOffline() {
        
        self.isGoToPriceJustification = false

        let message = checkForRequestPricingApprovalNew()
        
        if viewVerbalApproval.isHidden == false && isVerbalApproval == true // Verbal Approval Checked
        {
            if arySelectedEmployee.count == 0
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please select an Employee for Verbal Approval", viewcontrol: self)
            }
            else
            {
                
                // Verbal Approval is Done so Set Pricing Approval Pending False
                updatePricingApprovalTrue(strTrueFalse: "false")
                
                finalSave()
                
            }
        }
        else // Verbal Approval Not Checked
        {
            
            if message.count > 0 {
                
                let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
                alert.view.tintColor = UIColor.darkGray
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Request Approval", style: .default, handler: { [self] action in
                    
                    //self.isGoToPriceJustification = true
                    
                    self.finalSaveWithoutValidaiton()
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("statusSysName")
                    arrOfValues.add("Open")
                    arrOfKeys.add("stageSysName")
                    arrOfValues.add("Scheduled")
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    if isSuccess
                    {
                        
                    }
                    self.alertOnOffline()
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                updatePricingApprovalTrue(strTrueFalse: "false")
                
                finalSave()
                
            }
        }
    }
    
    func alertOnOffline() {
        
        let alert = UIAlertController(title: alertInfo, message: "You are offline, your request is saved", preferredStyle: .alert)
        alert.view.tintColor = UIColor.darkGray
//        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
//
//        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [self] action in
            
            self.GotoScheduleViewController()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func GotoScheduleViewController() {
        self.view.endEditing(true)
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }
    }
    
    func saveOnline()  {
        
        self.isGoToPriceJustification = false

        let message = checkForRequestPricingApprovalNew()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if arryOfData.count > 0 {
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
        }
        
        if ("\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "1" || "\(objWorkorderDetail.value(forKey: "isPricingApprovalPending") ?? "")".lowercased() == "true") {
            
            if message.count > 0 {
                
                let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
                alert.view.tintColor = UIColor.darkGray
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Request Approval", style: .default, handler: { [self] action in
                    
                    self.isGoToPriceJustification = true
                    
                    updatePricingApprovalTrue(strTrueFalse: "true")
                    
                    self.finalSaveWithoutValidaiton()
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("statusSysName")
                    arrOfValues.add("Open")
                    arrOfKeys.add("stageSysName")
                    arrOfValues.add("Scheduled")
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    if isSuccess
                    {
                        
                    }
                    self.goForPriceJustificaiton()
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                self.finalSave()
                
            }
        }
        else
        {
            self.finalSave()
        }
        
    }
    
    func checkForRequestPricingApprovalNew() -> String  {
        
        var message = ""
        
        let arrayWdoPricingMultipliers = getDataFromLocal(strEntity: Entity_WdoPricingMultipliers, predicate: NSPredicate(format: "workOrderId == %@",strWoId))
        
        let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

        
        var strHourlyRate = "",  strMaterialMultiplier = "", strSubFeeMultiplier = "",  strLinearFtSoilTreatmentRate = "", strLinearFtDrilledConcreteRate = "", strCubicFtRate = "", strDiscount = ""
        
        strDiscount = txtOtherDiscount.text ?? ""
        
        if arrayWdoPricingMultipliers.count > 0
        {
            let matchObject = arrayWdoPricingMultipliers.object(at: 0) as! NSManagedObject
            
            strHourlyRate = "\(matchObject.value(forKey: "hourlyRate") ?? "")"
            strMaterialMultiplier = "\(matchObject.value(forKey: "materialMultiplier") ?? "")"
            strSubFeeMultiplier = "\(matchObject.value(forKey: "subFeeMultiplier") ?? "")"
            strLinearFtSoilTreatmentRate = "\(matchObject.value(forKey: "linearFtSoilTreatmentRate") ?? "")"
            strLinearFtDrilledConcreteRate = "\(matchObject.value(forKey: "linearFtDrilledConcreteRate") ?? "")"
            strCubicFtRate = "\(matchObject.value(forKey: "cubicFtRate") ?? "")"
            
            if(aryRateMasterExtSerDcs.count != 0)
            {
                
                let obj = aryRateMasterExtSerDcs.firstObject as! NSDictionary
                dictRateMaster = obj
                let MaxHourlyRate = Double("\(obj.value(forKey: "MaxHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxHourlyRate") ?? "0")")
                let MinHourlyRate = Double("\(obj.value(forKey: "MinHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinHourlyRate") ?? "0")")
                let HourlyRate =  Double("\(strHourlyRate )")
                
                if MinHourlyRate != 0 && MaxHourlyRate != 0
                {
                    if !(MinHourlyRate!...MaxHourlyRate! ~= HourlyRate!) {
                        message = "! Hourly Rate ($) is exceeding \(MinHourlyRate!) and \(MaxHourlyRate!)\n"
                    }
                }
                
                let MaxMaterialMultiplier = Double("\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")")
                let MinMaterialMultiplier = Double("\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")")
                let MaterialMultiplier = Double("\(strMaterialMultiplier )")
                
                if MinMaterialMultiplier != 0 && MaxMaterialMultiplier != 0
                {
                    if !(MinMaterialMultiplier!...MaxMaterialMultiplier! ~= MaterialMultiplier!) {
                        message =  message + "! MaterialMultiplier is exceeding \(MinMaterialMultiplier!) and \(MaxMaterialMultiplier!)\n"
                    }
                }
                
                let MaxSubFeeMultiplier = Double("\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")")
                let MinSubFeeMultiplier = Double("\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")")
                let SubFeeMultiplier = Double("\(strSubFeeMultiplier )")
                

                if MinSubFeeMultiplier != 0 && MaxSubFeeMultiplier != 0
                {
                    if !(MinSubFeeMultiplier!...MaxSubFeeMultiplier! ~= SubFeeMultiplier!) {
                        message =  message + "! Sub Fee Multiplier is exceeding \(MinSubFeeMultiplier!) and \(MaxSubFeeMultiplier!)\n"
                    }
                }
                
                let MaxSoilTreatmentRate = Double("\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")")
                let MinSoilTreatmentRate = Double("\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")")
                let SoilTreatmentRate = Double("\(strLinearFtSoilTreatmentRate )")
                
                if MinSoilTreatmentRate != 0 && MaxSoilTreatmentRate != 0
                {
                    if !(MinSoilTreatmentRate!...MaxSoilTreatmentRate! ~= SoilTreatmentRate!) {
                        message =  message + "! Linear Ft. Soil Treatment  Rate ($) is exceeding \(MinSoilTreatmentRate!) and \(MaxSoilTreatmentRate!)\n"
                    }
                }
                
                let MaxDrilledConcreteRate = Double("\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")")
                let MinDrilledConcreteRate = Double("\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")")
                let DrilledConcreteRate = Double("\(strLinearFtDrilledConcreteRate )")
                
                if MinDrilledConcreteRate != 0 && MaxDrilledConcreteRate != 0
                {
                    if !(MinDrilledConcreteRate!...MaxDrilledConcreteRate! ~= DrilledConcreteRate!) {
                        message =  message + "! Linear Ft. Drilled Concrete  Rate ($) is exceeding \(MinDrilledConcreteRate!) and \(MaxDrilledConcreteRate!)\n"
                    }
                }
                
                
                let MaxCubicRate = Double("\(obj.value(forKey: "MaxCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxCubicRate") ?? "0")")
                let MinCubicRate = Double("\(obj.value(forKey: "MinCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinCubicRate") ?? "0")")
                let CubicRate = Double("\(strCubicFtRate)")
                
                if MinCubicRate != 0 && MaxCubicRate != 0
                {
                    if !(MinCubicRate!...MaxCubicRate! ~= CubicRate!) {
                        message =  message + "! Cubic ft. Rate ($) is exceeding \(MinCubicRate!) and \(MaxCubicRate!)\n"
                    }
                }
                
                //CalculateDiscount
                let subTotalAmount = Double("\(lblSubtotalPriceInformation.text ?? "")") ?? 0
                let MaxDiscount = (Double("\(obj.value(forKey: "MaxAllowed") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
                let MinDiscount = Double("0") ?? 0
                var discount = Double("\(strDiscount)") ?? 0
                discount = discount < 0 ? 0 : discount
                
                if MaxDiscount != 0
                {
                    if !(MinDiscount...MaxDiscount ~= discount) {
                        message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount.rounded())\n"
                    }
                }
                
                if message.count > 0 {
                    
                    message = message + "You will need a supervisor's approval and signature before the customer can sign the proposal."
                }
            }
        }
        return message
    }
    
    func checkForDiscountRange(discount : Double) -> Bool {
        
        if isInternetAvailable()
        {
            return true
        }
        else
        {
            var message = ""
            let subTotalAmount = Double("\(lblSubtotalPriceInformation.text ?? "")") ?? 0
            let MaxDiscount = (Double("\(dictRateMaster.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
                        //let MaxDiscount = 20.0 * (subTotalAmount/100)

            let MinDiscount = Double("0") ?? 0
            if !(MinDiscount...MaxDiscount ~= discount) {
                message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount.rounded())\n"
                return false
            }
            else
            {
                return true
            }
        }
        
    }
    
    func checkForDiscountRangeOnline(discount : Double)  {
        
        var message = ""
        let subTotalAmount = Double("\(lblSubtotalPriceInformation.text ?? "")") ?? 0
        let MaxDiscount = (Double("\(dictRateMaster.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
        
        let MinDiscount = Double("0") ?? 0
        if !(MinDiscount...MaxDiscount ~= discount) {
            message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount.rounded())\n"
            updatePricingApprovalTrue(strTrueFalse: "true")
        }else{
            
        }
        
    }
    
    func goForPriceJustificaiton()  {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "WDOiPad" : "WDOiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "PriceJustification_WDO_VC") as? PriceJustification_WDO_VC
        vc?.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
}

// MARK: --------------------- Text Field Delegate --------------

extension Agreement_WDOVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        yesEditedSomething = true
        
        if ( textField == txtDrivingLicenceNoPaymentMode || textField == txtCheckNoPaymentMode  )
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 60)
            
        }
        else if ( textField == txtOtherDiscount  )
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            if string == ""
            {
                var otherDiscount = (((txtOtherDiscount.text ?? "") as NSString).replacingCharacters(in: range, with: "") as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                var tipDiscount = ("\(txtTotalTIPDiscount.text ?? "")" as NSString).doubleValue
                let buildingPermitFee = ("\(txtBuildingPermitFee.text ?? "")" as NSString).doubleValue
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue
                var leadInspectionFee = ("\(lblLeadInspectionFee.text ?? "")" as NSString).doubleValue

                
                if leadInspectionFee < 0
                {
                    leadInspectionFee = 0
                }
                
                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount + leadInspectionFee
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)
                
                if self.checkForDiscountRange(discount: otherDiscount)
                {
                    self.viewVerbalApproval.isHidden = true
                }
                else
                {
                    self.viewVerbalApproval.isHidden = false
                }
                
                self.checkForDiscountRangeOnline(discount: otherDiscount)
              
            }
            else
            {
                var strDiscount = "\(txtOtherDiscount.text ?? "")"
                strDiscount = strDiscount + string
                var otherDiscount = (strDiscount as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                var tipDiscount = ("\(txtTotalTIPDiscount.text ?? "")" as NSString).doubleValue
                let buildingPermitFee = ("\(txtBuildingPermitFee.text ?? "")" as NSString).doubleValue
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue
                var leadInspectionFee = ("\(lblLeadInspectionFee.text ?? "")" as NSString).doubleValue

                
                if leadInspectionFee < 0
                {
                    leadInspectionFee = 0
                }
                
                
                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount + leadInspectionFee
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)
                
                if self.checkForDiscountRange(discount: otherDiscount)
                {
                    self.viewVerbalApproval.isHidden = true
                }
                else
                {
                    self.viewVerbalApproval.isHidden = false
                }
                
                self.checkForDiscountRangeOnline(discount: otherDiscount)

            }
            
            return chk
        }
            
        else if ( textField == txtAmountPaymentMode )
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            //            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
        }
        else if ( textField == txtTotalTIPDiscount  )
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            if string == ""
            {
                var otherDiscount =  ("\(txtOtherDiscount.text ?? "")" as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                var tipDiscount = (((txtTotalTIPDiscount.text ?? "") as NSString).replacingCharacters(in: range, with: "") as NSString).doubleValue
                let buildingPermitFee = ("\(txtBuildingPermitFee.text ?? "")" as NSString).doubleValue
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue

                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)

              
            }
            else
            {
               
                var otherDiscount = ("\(txtOtherDiscount.text ?? "")" as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                
                var strTipDiscount = "\(txtTotalTIPDiscount.text ?? "")"
                strTipDiscount = strTipDiscount + string
                var tipDiscount = ("\(strTipDiscount)" as NSString).doubleValue
                
                let buildingPermitFee = ("\(txtBuildingPermitFee.text ?? "")" as NSString).doubleValue
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue
                
                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)

            }
            
            
            return chk
            
        }
        else if ( textField == txtBuildingPermitFee  )
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            
            if string == ""
            {
                var otherDiscount =  ("\(txtOtherDiscount.text ?? "")" as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                var tipDiscount = ("\(txtTotalTIPDiscount.text ?? "")" as NSString).doubleValue
                let buildingPermitFee = (((txtBuildingPermitFee.text ?? "") as NSString).replacingCharacters(in: range, with: "") as NSString).doubleValue
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue

                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)

              
            }
            else
            {
                var otherDiscount = ("\(txtOtherDiscount.text ?? "")" as NSString).doubleValue
                let subtotalAmount = ("\(lblSubtotalPriceInformation.text ?? "")" as NSString).doubleValue
                var tipDiscount = (("\(txtTotalTIPDiscount.text ?? "")" as NSString) as NSString).doubleValue
                
                var strBuildingPermitFee = "\(txtBuildingPermitFee.text ?? "")"
                strBuildingPermitFee = strBuildingPermitFee + string
                let buildingPermitFee = ("\(strBuildingPermitFee)" as NSString).doubleValue
            
                var billingAmount = ("\(lblBillingAmountPriceInformation.text ?? "")" as NSString).doubleValue
                
                if tipDiscount < 0
                {
                    tipDiscount = 0
                }
                if otherDiscount < 0
                {
                    otherDiscount = 0
                }
                
                billingAmount = subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount
                
                lblBillingAmountPriceInformation.text =  String(format: "%.2f", billingAmount<0 ? 0 : billingAmount)

            }
            
            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
    
    func calculateBillingAmount(subtotalAmount: Double, otherDiscount: Double, tipDiscount: Double, buildingPermitFee: Double) -> Double {
        
        
        return subtotalAmount + buildingPermitFee - otherDiscount - tipDiscount
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtAmountPaymentMode)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = (textField.text! as NSString).doubleValue
                
                textField.text = String(format: "%.2f", amt)
            }
            else
            {
                textField.text = "0.00"
            }
            
        }
        if(textField == txtTotalTIPDiscount)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = (textField.text! as NSString).doubleValue
                
                textField.text = String(format: "%.2f", amt)
            }
            else
            {
                textField.text = "0.00"
            }
            
            // Recaluclate the Total amount
            
            self.showPriceInformation()
            
        }
        if(textField == txtBuildingPermitFee)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = (textField.text! as NSString).doubleValue
                
                textField.text = String(format: "%.2f", amt)
            }
            else
            {
                textField.text = "0.00"
            }
            
            // Recaluclate the Total amount
            
            self.showPriceInformation()
            
        }
    }
}
// MARK: --------------------- DatePicker  Delegate --------------

extension Agreement_WDOVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        yesEditedSomething = true
        strExpirationDate = strDate
        btnSelectExpirationdate.setTitle(strDate, for: .normal)
    }
    
    
}

// MARK: --------------------- ImageView Delegate --------------


extension Agreement_WDOVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        yesEditedSomething = true
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            if chkFrontImage
            {
                strCheckFrontImage = "Img"  + "CheckFrontImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckFrontImage, image: pickedImage)
            }
            else
            {
                strCheckBackImage = "Img"  + "CheckBackImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckBackImage, image: pickedImage)
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: ------------- Signature Delegate --------------

extension Agreement_WDOVC : SignatureViewDelegate{
    func imageFromSignatureView(image: UIImage)
    {
        if signatureType == "customer"
        {
            imgviewCustomerSign.image = image
            strCustomerSign = "Cust_" + "\(strLeadId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
        else if signatureType == "technician"
        {
            imgviewTechnicianSign.image = image
            strTechnicianSign = "Tech_" + "\(strLeadId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
    }
}
// MARK: - ----------------- Selection CustomTableView -----------------
// MARK: -

extension Agreement_WDOVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 1105)
        {
            if(dictData.count == 0){
                btnVerbalApprovalFrom.setTitle("----Select----", for: .normal)
                self.arySelectedEmployee = NSMutableArray()
            }else{
                btnVerbalApprovalFrom.setTitle("\(dictData.value(forKey: "FullName")!)", for: .normal)
                self.arySelectedEmployee = NSMutableArray()
                self.arySelectedEmployee.add(dictData)
            }
        }
        
    }
}
// MARK: --
// MARK: -------------RefreshStatus-------------
extension Agreement_WDOVC {
   
    func refreshWorkOrderStatus(view : UIViewController , strWOID : String){
        var loader = UIAlertController()
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        view.present(loader, animated: false, completion: nil)
        if (isInternetAvailable()){
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkorderApprovalStatusByWorkorderId?workorderId=\(strWOID)"
            
            WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self]  (response, status) in
                print(response)
                loader.dismiss(animated: false, completion: nil)
                if(status){
                    if(response.value(forKey: "data") is NSDictionary){
                        let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"
                        if(dictData.value(forKey: "IsSuccess") as! Bool){
                            workOrder_DataBaseUpdate(dict: ((dictData.value(forKey: "Response") as! NSDictionary).mutableCopy() as! NSMutableDictionary), strWOID: strWOID)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: view)
                        }
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: view)
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: view)
        }
    }
    
    func workOrder_DataBaseUpdate(dict : NSMutableDictionary , strWOID : String) {
       
        if(dict.count != 0){
            let dictWo = removeNullFromDict(dict: dict)
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOID))
            if(arryOfData.count != 0){

                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                arrOfKeys = ["isPricingApprovalPending",
                             "wdoWorkflowStage","wdoWorkflowStatus","priceChangeReasonId" , "priceChangeNote" , "isVerbalApproval" , "verbalApprovalBy"]
                /*arrOfValues = ["\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeNote") ?? "")",
                               "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")",
                               "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")"
                ]*/
                
                arrOfValues = ["\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")" == "<null>"  ? "false" : "\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeNote") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "PriceChangeNote") ?? "")",
                               "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")" == "<null>"  ? "false" : "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")",
                               "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")"
                ]
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess
                {

                }
                
                // "\(dict.value(forKey: "Note") ?? "")" == "<null>"  ? "" : "\(dict.value(forKey: "Note") ?? "")"
                
                // AGain Fetch WorkOrder Details
                
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
                
                if arryOfData.count > 0 {
                    
                    objWorkorderDetail = arryOfData[0] as! NSManagedObject
                    
                }
                
            }
        }
    }
}



