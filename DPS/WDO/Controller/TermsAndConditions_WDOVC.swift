//
//  TermsAndConditions_WDOVC.swift
//  DPS
//  Test 01
//  Created by Rakesh Jain on 25/09/19.
//  Copyright © 2019 Saavan. All rights reserved.


import UIKit

class TermsAndConditions_WDOVC: UIViewController {
    
    // Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    
    // variable
    var arrayTermsAndCondaitions = NSMutableArray()
   
    private var yPosition:CGFloat = 20
    private var height:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        for item in arrayTermsAndCondaitions
      {
        
        let lblTitle = UILabel(frame: CGRect(x: 30, y: yPosition, width: self.view.frame.size.width-60, height: 30))
        
        lblTitle.text = "\((item as! NSDictionary).value(forKey: "TermsTitle")!)"
        lblTitle.font = UIFont.boldSystemFont(ofSize: 22.0)
        
        var description = "\((item as! NSDictionary).value(forKey: "TermsConditions")!)"
        
        //converHTML(html: "\((item as! NSDictionary).value(forKey: "TermsConditions")!)")
        
        description = description.trimmingCharacters(in: .whitespacesAndNewlines)
        let data = Data(description.utf8)
      
        var attStr:NSAttributedString!
        if let attributedString = try? NSAttributedString(data: data,  options: [.documentType: NSAttributedString.DocumentType.html],  documentAttributes: nil)
        {
            attStr = attributedString
        }
        
        let lblDescription = UILabel(frame: CGRect(x: 30, y: lblTitle.frame.maxY+20, width: self.view.frame.size.width-60, height: heightForString(attStr, width: self.view.frame.size.width-60)))
        
        
        lblDescription.font = UIFont(name: "Helvetica Neue", size: 22.0)
        lblDescription.numberOfLines = 0
        lblDescription.lineBreakMode = .byWordWrapping
      //  lblDescription.backgroundColor = UIColor.red
        lblDescription.attributedText = attStr
       
        
        let viewSeparator = UIView(frame: CGRect(x: 0, y: lblDescription.frame.maxY+20, width: self.view.frame.size.width, height: 1))
        
        viewSeparator.backgroundColor = UIColor.lightGray
        
        yPosition = viewSeparator.frame.maxY + 20
        
        scrollView.addSubview(lblTitle)
        scrollView.addSubview(lblDescription)
        scrollView.addSubview(viewSeparator)
        
        height = height + lblTitle.frame.size.height + lblDescription.frame.size.height + viewSeparator.frame.size.height
        
        print("LabelTitle:-\(lblTitle)")
        print("LabelDescription:-\(lblDescription)")
        print("ViewSeparator:-\(viewSeparator)")
        
        }
        height = height + 60
      
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: height)
        
    }
    
    // MARK: UIButton action    
    
    @IBAction func action_back(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func heightForString(_ str : NSAttributedString, width : CGFloat) -> CGFloat {
        let ts = NSTextStorage(attributedString: str)
        
        let size = CGSize(width:width, height:CGFloat.greatestFiniteMagnitude)
        
        let tc = NSTextContainer(size: size)
        tc.lineFragmentPadding = 0.0
        
        let lm = NSLayoutManager()
        lm.addTextContainer(tc)
        
        ts.addLayoutManager(lm)
        lm.glyphRange(forBoundingRect: CGRect(origin: .zero, size: size), in: tc)
        
        let rect = lm.usedRect(for: tc)
        
        return rect.integral.size.height
    }
    
}
