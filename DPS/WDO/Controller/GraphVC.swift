//
//  GraphVC.swift
//  DPS
//  Saavan Patidar 2020
//  Created by Saavan Patidar on 09/03/20.
//  Copyright © 2020 Saavan. All rights reserved.


import UIKit
import WebKit

class GraphVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //let url = "https://payments.workwave.com//HostedTransactions/frames?hostedTransactionId=RPDaQx9dbkN7YK06n0AMz3oVZmWJ8ApyBjM2"

        let url = "https://payments.workwave.com//HostedTransactions/frames?hostedTransactionId=g7aWYopvxDbZVNnqOkjWrqdRA9jmre02k8P4"

        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        self.webView.load(URLRequest(url: URL(string: strUrlwithString!)!))

        
    }
    
    @IBAction func action_back(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        self.navigationController?.dismiss(animated: false, completion: nil)
        
    }
    

}
