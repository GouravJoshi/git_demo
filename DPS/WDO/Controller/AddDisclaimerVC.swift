//  Saavan Patidar
//  AddDisclaimerVC.swift
//  Created by Navin Patidar on 9/19/19.
//  Copyright © 2019 Saavan. All rights reserved. 2020
//  Saavan patidar
//  2021

import UIKit

class AddDisclaimerVC: UIViewController {
   
    // MARK:
    // MARK: IBOutlet
    
    @IBOutlet weak var txt_Filling: ACFloatingTextField!
    @IBOutlet weak var txtV_Description: UITextView!
    @IBOutlet weak var btnSelectedDisclaimer: UIButton!
    @IBOutlet weak var btnEditDisclaimer: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnMoreSelectDisclaimer: UIButton!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = String()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var strToUpdate = String()
    var dictDataOfDisclaimerSelected = NSDictionary ()
    var isDescChanged = false
    var strDisclaimerIdToUpdate = String()
    var strDisclaimerMasterId = String()
    var isEditedDisclaimer = false

    
    // MARK:
    // MARK: Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
         txtV_Description.layer.borderWidth = 1.0
         txtV_Description.layer.borderColor = UIColor.lightGray.cgColor
         txtV_Description.layer.cornerRadius = 10.0
        
        btnSelectedDisclaimer.setTitle(strSelectString, for: .normal)
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        print(strWoId)
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strToUpdate == "Yes" {
            
            setValuesOnUpdate()
            btnSelectedDisclaimer.isEnabled = false
            
        }else{
            
            btnSelectedDisclaimer.isEnabled = true
            
        }
        
        disableControls()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // If From Editing Html Contents
        if (nsud.value(forKey: "EditedHtmlContents") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "EditedHtmlContents")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "EditedHtmlContents")
                nsud.synchronize()
                
                txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(nsud.value(forKey: "htmlContents")!)")//htmlAttributedString(strHtmlString: "\(nsud.value(forKey: "htmlContents")!)")
                
                isEditedDisclaimer = true
                
                isDescChanged = true
                
            }
            
        }
        
    }
    
    // MARK:
    // MARK: IBAction's
    
    @IBAction func action_OnDropDownButton(_ sender: UIButton) {
    
        self.view.endEditing(true)

        var ary = NSMutableArray()
        let aryDisclaimer = NSMutableArray()

        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        // Check if Deafult Disclaimer then remove it from list
        if ary.count != 0{
            
            for item in ary {
                
                let dictData = item as! NSDictionary
                
                let isDeafultt = (dictData.value(forKey: "IsDefault") as! Bool)
                
                if !isDeafultt {
                    
                    aryDisclaimer.add(dictData)
                    
                }
                
            }
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if aryDisclaimer.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryDisclaimer
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    @IBAction func action_OnBackButton(_ sender: UIButton) {
        
        self.view.endEditing(true)

        back()

    }
    
    @IBAction func action_EditDisclaimer(_ sender: Any) {
        
        //if txtV_Description.text.count > 0 {
            
            if isEditedDisclaimer {
                
                let description = "\(nsud.value(forKey: "htmlContents")!)"
                htmlEditorView(htmlString: description)

            }else{
                
                let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && woWdoDisclaimerId == %@", strWoId , strDisclaimerIdToUpdate))
                
                if arrOfDiclaimer.count > 0 {
                    
                    let objData = arrOfDiclaimer[0] as! NSManagedObject
                    
                    
                    htmlEditorView(htmlString: "\(objData.value(forKey: "disclaimerDescription")!)")

                    
                }else{
                    
                    if dictDataOfDisclaimerSelected.count > 0 {
                        
                        htmlEditorView(htmlString: "\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")
                        
                    }

                }

            }
            
        //}
        
    }
    
    
    @IBAction func action_OnSaveButton(_ sender: UIButton) {
        
        self.view.endEditing(true)

        if(validationForAddDisclaimer()){
         
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            arrOfKeys.add("isDefault")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            arrOfValues.add(false)

            if strToUpdate == "Yes" {
                
                arrOfKeys.add("disclaimerDescription")
                arrOfKeys.add("fillIn")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("isChanged")

                //arrOfValues.add(txtV_Description.text!)
                if isEditedDisclaimer {
                    
                    let description = "\(nsud.value(forKey: "htmlContents")!)"
                    arrOfValues.add("\(description)")
                    
                }else{
                    
                    let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && woWdoDisclaimerId == %@", strWoId , strDisclaimerIdToUpdate))
                    
                    if arrOfDiclaimer.count > 0 {
                        
                        let objData = arrOfDiclaimer[0] as! NSManagedObject
                        
                        arrOfValues.add("\(objData.value(forKey: "disclaimerDescription")!)")

                    }else{
                        
                        arrOfValues.add("\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")
                        
                    }
                    
                }

                
                arrOfValues.add(txt_Filling.text!)
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                if isDescChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && woWdoDisclaimerId == %@", strWoId, strDisclaimerIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    back()
                    
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                
            }else{
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate") // ModifiedDate

                arrOfKeys.add("disclaimerMasterId")
                arrOfKeys.add("disclaimerDescription")
                arrOfKeys.add("disclaimerName")
                arrOfKeys.add("isChanged")
                arrOfKeys.add("woWdoDisclaimerId")
                arrOfKeys.add("fillIn")

                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfValues.add("\(dictDataOfDisclaimerSelected.value(forKey: "DisclaimerMasterId")!)")
                //arrOfValues.add(txtV_Description.text!)
                if isEditedDisclaimer {
                    
                    let description = "\(nsud.value(forKey: "htmlContents")!)"
                    arrOfValues.add("\(description)")
                    
                }else{
                    
                    arrOfValues.add("\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")
                    
                }
                arrOfValues.add("\(dictDataOfDisclaimerSelected.value(forKey: "Title")!)")

                if isDescChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                arrOfValues.add(Global().getReferenceNumber())
                arrOfValues.add(txt_Filling.text!)

                // Saving Disclaimer In DB
                saveDataInDB(strEntity: Entity_Disclaimer, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                back()

            }
            
        }

        
    }
    
    @IBAction func action_MoreSelectDisclaimer(_ sender: Any) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        let aryDisclaimer = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        // Check if Deafult Disclaimer then remove it from list
        if ary.count != 0{
            
            for item in ary {
                
                let dictData = item as! NSDictionary
                
                let isDeafultt = (dictData.value(forKey: "IsDefault") as! Bool)
                
                if !isDeafultt {
                    
                    aryDisclaimer.add(dictData)
                    
                }
                
            }
            
        }
        
        
        if aryDisclaimer.count > 0 {
            
            self.goToSelection(strTags: 110 , strTitleToShow: "Disclaimer" , array: aryDisclaimer)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    
    // MARK: - ----------------------------------- Functions  -----------------------------------
    // MARK: -
    
    func goToSelection(strTags : Int , strTitleToShow : String , array : NSMutableArray)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitleToShow
        vc.strTag = strTags
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = array
        //vc.arySelectedTargets = arrayOfCorrectiveActionsSelected
        present(vc, animated: false, completion: {})
        
    }
    
    func disableControls() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            txtV_Description.isUserInteractionEnabled = false
            btnSelectedDisclaimer.isUserInteractionEnabled = false
            btnEditDisclaimer.isUserInteractionEnabled = false
            txt_Filling.isUserInteractionEnabled = false
            btnSave.isUserInteractionEnabled = false

        }
        
    }
    
    func htmlEditorView(htmlString : String) {
        
        let storyboard = UIStoryboard(name: "WDOiPad", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        
        controller.strHtml = htmlString
        
        self.present(controller, animated: false, completion: nil)
        
    }
    
    func back() {
        
        dismiss(animated: false)
        
    }
    
    func setValuesOnUpdate() {
        
        let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ && woWdoDisclaimerId == %@", strWoId , strDisclaimerIdToUpdate))
        
        if arrOfDiclaimer.count > 0 {
            
            let objData = arrOfDiclaimer[0] as! NSManagedObject
            
            btnSelectedDisclaimer.setTitle("\(objData.value(forKey: "disclaimerName") ?? "")", for: .normal) // woWdoDisclaimerId
            strDisclaimerMasterId = "\(objData.value(forKey: "disclaimerMasterId") ?? "")" //  disclaimerMasterId
            
            txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(objData.value(forKey: "disclaimerDescription") ?? "")")//htmlAttributedString(strHtmlString: "\(objData.value(forKey: "disclaimerDescription") ?? "")")
            
            txt_Filling.text = "\(objData.value(forKey: "fillIn") ?? "")"//fillIn
            isDescChanged = objData.value(forKey: "isChanged") as! Bool
            
            var ary = NSMutableArray()
            
            ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
            
            if ary.count > 0 {
                
                for k1 in 0 ..< ary.count {
                    
                    let dictData = ary[k1] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "DisclaimerMasterId") ?? "")" == "\(objData.value(forKey: "disclaimerMasterId") ?? "")"{
                        
                        dictDataOfDisclaimerSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
            // check if isDeafult disable editing
            
            let isDefaultt = objData.value(forKey: "isDefault") as! Bool
            
            if isDefaultt {
                
                btnEditDisclaimer.isUserInteractionEnabled = false
                btnMoreSelectDisclaimer.isUserInteractionEnabled = false
                btnEditDisclaimer.isEnabled = false
                btnMoreSelectDisclaimer.isEnabled = false
                
            }
            
        }else{
            
            back()
            
        }
        
    }
    
    // MARK:-
    // MARK:-Validation
    
    func validationForAddDisclaimer() -> Bool {
        
        if dictDataOfDisclaimerSelected.count > 0 {
            
            var isHash = false
            
            if (txt_Filling.text?.count == 0){
                
             isHash = checkIfStringContains(strString: txtV_Description.text!, strChar: "##", countt: 2)
                
            }
            
            var  arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  disclaimerMasterId == %@", strWoId , "\(dictDataOfDisclaimerSelected.value(forKey: "DisclaimerMasterId")!)"))
            
            if strToUpdate == "Yes" {
                
                arrOfDiclaimer = NSArray()
                
            }
            
            if arrOfDiclaimer.count > 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_DisclaimerExist, viewcontrol: self)
                
                return false
                
            }
            else if(btnSelectedDisclaimer.title(for: .normal) == strSelectString){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectDisclaimer, viewcontrol: self)
                
                return false
                
            }
            else if isHash {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterDisclaimerFillin, viewcontrol: self)
                
                return false
                
            }
            else if (txtV_Description.text?.count == 0){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterDisclaimerDesc, viewcontrol: self)
                
                return false
                
            }
            
            return true
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectDisclaimer, viewcontrol: self)
            
            return false
            
        }
        
    }
    

    
}
// MARK:-
// MARK:-Selection_PopUpDelegate

extension AddDisclaimerVC : CustomTableView
{
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        dictDataOfDisclaimerSelected = dictData
        btnSelectedDisclaimer.setTitle("\(dictDataOfDisclaimerSelected.value(forKey: "Title")!)", for: .normal)

        txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")//htmlAttributedString(strHtmlString: "\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")
        
    }

}

// MARK: - -----------------------------------Selection Delegate -----------------------------------

extension AddDisclaimerVC : PopUpDelegate
{
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 110){
            
            dictDataOfDisclaimerSelected = dictData
            btnSelectedDisclaimer.setTitle("\(dictDataOfDisclaimerSelected.value(forKey: "Title")!)", for: .normal)
            
            txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")//htmlAttributedString(strHtmlString: "\(dictDataOfDisclaimerSelected.value(forKey: "Disclaimer")!)")
            
        }
        
    }
}


// MARK: - -----------------------------------UITextViewDelegate -----------------------------------
// MARK: -

extension AddDisclaimerVC : UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        
        isDescChanged = true
        
    }
    
}
