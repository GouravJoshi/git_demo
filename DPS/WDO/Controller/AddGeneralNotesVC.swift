//  Saavan Changes 2020
//  AddGeneralNotesVC.swift
//  DPS Saavan Patidar
//  peSTream 2020
//  Created by Saavan Patidar on 10/12/19.
//  Copyright © 2019 Saavan. All rights reserved.


import UIKit

class AddGeneralNotesVC: UIViewController {
    
    // MARK:
    // MARK: IBOutlet
    
    @IBOutlet weak var txtV_Description: UITextView!
    @IBOutlet weak var btnSelectedGeneralNotes: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnSelectGeneralNotesSearch: UIButton!
    
    @IBOutlet weak var txtFillin: ACFloatingTextField!
    
    @IBOutlet weak var btnEditGeneralNotesDesc: UIButton!
    
    @IBOutlet weak var btnFavouriteNotes: UIButton!
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = String()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var strToUpdate = String()
    var dictDataOfGeneralNotesSelected = NSDictionary ()
    var isDescChanged = false
    var strGeneralNotesIdToUpdate = String()
    var strGeneralNotesMasterId = String()
    var isEditedGeneralNotes = false
    var arrOfFavouriteNotes = NSArray()

    
    // MARK:
    // MARK: Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        txtV_Description.layer.borderWidth = 1.0
        txtV_Description.layer.borderColor = UIColor.lightGray.cgColor
        txtV_Description.layer.cornerRadius = 10.0
        
        btnSelectedGeneralNotes.setTitle(strSelectString, for: .normal)
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        print(strWoId)
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        if strToUpdate == "Yes" {
            
            setValuesOnUpdate()
            btnSelectedGeneralNotes.isEnabled = false
            btnSelectGeneralNotesSearch.isEnabled = false

        }else{
            
            btnSelectedGeneralNotes.isEnabled = true
            btnSelectGeneralNotesSearch.isEnabled = true

        }
        
        disableControls()
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddToFavouriteNotes_Notification"),
        object: nil,
        queue: nil,
        using:catchNotification1)
        
        
        if (isInternetAvailable())
        {
            if nsud.bool(forKey: "isSyncFavouriteNote") == true
            {
                callAPIMarkAsFavouriteNotes()
            }
            else
            {
                getFinalNotes()
            }
        }
        else
        {
            getFinalNotes()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        // If From Editing Html Contents
        if (nsud.value(forKey: "EditedHtmlContents") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "EditedHtmlContents")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "EditedHtmlContents")
                nsud.synchronize()
                
                //htmlContents
                txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(nsud.value(forKey: "htmlContents")!)")
                
                isEditedGeneralNotes = true
                
                isDescChanged = true
                
            }
            
        }
        
    }
 
    
    
    // MARK:
    // MARK: IBAction's
    
    @IBAction func action_SearchGeneralNotes(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            self.goToSelection(strTags: 109 , strTitleToShow: "General Notes" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
    }
    
    
    @IBAction func action_OnDropDownButton(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if array.count != 0{
            
            array = addSelectInArray(strName: "Title", array: array)
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = array
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    @IBAction func action_OnBackButton(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        back()
        
    }
    
    @IBAction func action_EditGeneralNotes(_ sender: Any) {
        
        //if txtV_Description.text.count > 0 {
            
            if isEditedGeneralNotes {
                
                let description = "\(nsud.value(forKey: "htmlContents")!)"
                htmlEditorView(htmlString: description)
                
            }else{
                
                let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && wdoGeneralNoteId == %@", strWoId , strGeneralNotesIdToUpdate))
                
                if arrOfDiclaimer.count > 0 {
                    
                    let objData = arrOfDiclaimer[0] as! NSManagedObject
                    
                    
                    htmlEditorView(htmlString: "\(objData.value(forKey: "generalNoteDescription")!)")
                    
                    
                }else{
                    
                    if dictDataOfGeneralNotesSelected.count > 0 {
                        
                        htmlEditorView(htmlString: "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)")
                        
                    }
                    
                }
                
            }
            
        //}
        
    }
    
    
    @IBAction func action_OnSaveButton(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(validationForAddGeneralNotes()){
            
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            arrOfKeys.add("type")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            arrOfValues.add("")

            if strToUpdate == "Yes" {
                
                arrOfKeys.add("generalNoteDescription")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("fillIn")
                arrOfKeys.add("isChanged")

                //arrOfValues.add(txtV_Description.text!)
                
                
                if isEditedGeneralNotes {
                    
                    let description = "\(nsud.value(forKey: "htmlContents")!)"
                    arrOfValues.add("\(description)")
                    
                }else{
                    
                    let arrOfGeneralNotes = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && wdoGeneralNoteId == %@", strWoId , strGeneralNotesIdToUpdate))
                    
                    if arrOfGeneralNotes.count > 0 {
                        
                        let objData = arrOfGeneralNotes[0] as! NSManagedObject
                        
                        arrOfValues.add("\(objData.value(forKey: "generalNoteDescription")!)")
                        
                    }else{
                        
                        arrOfValues.add("\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)")
                        
                    }
                    
                }
                
                
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(txtFillin.text!)

                
                if isDescChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                
                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && wdoGeneralNoteId == %@", strWoId, strGeneralNotesIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    back()
                    
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                
            }else{
                
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate") // ModifiedDate
                
                arrOfKeys.add("generalNoteMasterId")
                arrOfKeys.add("generalNoteDescription")
                arrOfKeys.add("title")
                arrOfKeys.add("wdoGeneralNoteId")
                arrOfKeys.add("fillIn")
                arrOfKeys.add("isChanged")

                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                arrOfValues.add("\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescriptionId")!)")
                //arrOfValues.add(txtV_Description.text!)
                
                
                if isEditedGeneralNotes {
                    
                    let description = "\(nsud.value(forKey: "htmlContents")!)"
                    arrOfValues.add("\(description)")
                    
                }else{
                    
                    arrOfValues.add("\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)")
                    
                }
                
                
                arrOfValues.add("\(dictDataOfGeneralNotesSelected.value(forKey: "Title")!)")

                arrOfValues.add(Global().getReferenceNumber())
                arrOfValues.add(txtFillin.text!)
                
                if isDescChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                // Saving Disclaimer In DB
                saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                back()
                
            }
            
        }
        
        
    }
    
    @IBAction func actionOnFavouriteNotes(_ sender: Any)
    {
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        let arrMutable = NSMutableArray()
        
        for item in array
        {
            let dict = item as! NSDictionary
            
            for itemSecond in arrOfFavouriteNotes
            {

                let str = itemSecond as! String
                
                if "\(dict.value(forKey: "ServiceJobDescriptionId") ?? "")" == str
                {
                    arrMutable.add(dict)
                }

            }
            
        }
        
        array = NSMutableArray()
        array = arrMutable
        

        if array.count > 0 {
            
            self.goToSelection(strTags: 109 , strTitleToShow: "General Notes" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
    }
    
    
    // MARK: - ----------------------------------- Functions  -----------------------------------
    // MARK: -
    
    func disableControls() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            txtV_Description.isUserInteractionEnabled = false
            btnSelectedGeneralNotes.isUserInteractionEnabled = false
            btnSave.isUserInteractionEnabled = false
            btnSelectGeneralNotesSearch.isUserInteractionEnabled = false
            txtFillin.isUserInteractionEnabled = false

        }
        
    }
    
    func htmlEditorView(htmlString : String) {
        
        let storyboard = UIStoryboard(name: "WDOiPad", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        
        controller.strHtml = htmlString
        
        self.present(controller, animated: false, completion: nil)
        
    }
    
    func back() {
        
        dismiss(animated: false)
        
    }
    
    func setValuesOnUpdate() {
        
        let arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ && wdoGeneralNoteId == %@", strWoId , strGeneralNotesIdToUpdate))
        
        if arrOfDiclaimer.count > 0 {
            
            let objData = arrOfDiclaimer[0] as! NSManagedObject
            
            btnSelectedGeneralNotes.setTitle("\(objData.value(forKey: "title") ?? "")", for: .normal) // woWdoDisclaimerId
            strGeneralNotesMasterId = "\(objData.value(forKey: "generalNoteMasterId") ?? "")" //  disclaimerMasterId
            
            txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(objData.value(forKey: "generalNoteDescription") ?? "")")
            
            txtFillin.text = "\(objData.value(forKey: "fillIn") ?? "")"

            if txtFillin.text == "<null>" {
                
                txtFillin.text = ""
                
            }
            
            isDescChanged = objData.value(forKey: "isChanged") as! Bool
            
            var ary = NSMutableArray()
            
            ary = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")

            if ary.count > 0 {
                
                for k1 in 0 ..< ary.count {
                    
                    let dictData = ary[k1] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ServiceJobDescriptionId") ?? "")" == "\(objData.value(forKey: "generalNoteMasterId") ?? "")"{
                        
                        dictDataOfGeneralNotesSelected = dictData
                        
                        break
                        
                    }
                    
                }
                
            }
            
        }else{
            
            back()
            
        }
        
    }
    
    func goToSelection(strTags : Int , strTitleToShow : String , array : NSMutableArray)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitleToShow
        vc.strTag = strTags
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = array
        
        if strTags == 109
        {
            let arrCode = NSMutableArray()
            for item in arrOfFavouriteNotes
            {
                let str = item as! String
                arrCode.add(str)
            }
            vc.arrMarkAsFaourite = arrCode
        }
        //vc.arySelectedTargets = arrayOfCorrectiveActionsSelected
        present(vc, animated: false, completion: {})
        
    }
    
    // MARK:- API CODE
    
    func catchNotification1(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getFinalNotes()
            }

        }
        
    }
    
    func getFinalNotes()
    {
        if(nsud.value(forKey: "favouriteNote") != nil){
            if nsud.value(forKey: "favouriteNote") is NSArray
            {
                let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteNote") as! NSArray
                arrOfFavouriteNotes = arrFvouriteCodesTemp
            }
            else
            {
                arrOfFavouriteNotes = NSArray()
            }

        }else{
            arrOfFavouriteNotes = NSArray()

        }
        
      
    }
    func getPostNotes() -> NSDictionary
    {
        let arr = NSMutableArray()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"

        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        
        if(nsud.value(forKey: "favouriteNote") != nil){
            if nsud.value(forKey: "favouriteNote") is NSArray
            {
                let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteNote") as! NSArray
                arrOfFavouriteNotes = arrFvouriteCodesTemp
            }
            else
            {
                arrOfFavouriteNotes = NSArray()
            }
            
        }else{
            arrOfFavouriteNotes = NSArray()

        }
        
      
        for item in arrOfFavouriteNotes
        {
            let strId = item as! String
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("WoWdoNoteFavoriteDetailId")
            arrOfKeys.add("NoteId")
            arrOfKeys.add("EmployeeId")
            arrOfKeys.add("CompanyId")
            arrOfKeys.add("CreatedDate")
            arrOfKeys.add("CreatedBy")
            arrOfKeys.add("ModifiedDate")
            arrOfKeys.add("ModifiedBy")
            
            arrOfValues.add("")
            arrOfValues.add("\(strId)")
            arrOfValues.add("\(strEmpID)")
            arrOfValues.add("\(strCompanyId)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            
            let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
            arr.add(dict_ToSend)
            
        }

        var dict = [String:Any]()
        

        dict = [
                "WoWdoNoteFavoriteDetailExtSerDcs":arr,
        ]
        
        return dict as NSDictionary
    }
    
    func callAPIMarkAsFavouriteNotes()
    {

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateNoteAndFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)&Type=\("Note")"

        WebService.postRequestWithHeaders(dictJson: getPostNotes() , url: strURL, responseStringComing: "TimeLine") { [self] (response, status) in
            
            if(status == true)
            {
                let arrTemp = NSMutableArray()
                var arrData = NSArray()

               
                if(response.value(forKey: "data") is NSDictionary){
                    let dictResponse = response.value(forKey: "data") as! NSDictionary
                    if(dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") is NSArray){
                         arrData = dictResponse.value(forKey: "WoWdoNoteFavoriteDetailExtSerDcs") as! NSArray

                    }
                }
                
                
                if arrData.count > 0
                {
                    for item in arrData
                    {
                        let dict = item as! NSDictionary
                        arrTemp.add("\(dict.value(forKey: "NoteId") ?? "")")
                    }
                }
                self.arrOfFavouriteNotes = arrTemp
                var arrOfFavouriteCodes = NSArray()
                arrOfFavouriteCodes = arrTemp
                nsud.set(arrOfFavouriteCodes, forKey: "favouriteNote")
                nsud.set(false, forKey: "isSyncFavouriteNote")
                nsud.synchronize()
                
                self.getFinalNotes()

            }

        }
        
    }

    
    // MARK:-
    // MARK:-Validation
    
    func validationForAddGeneralNotes() -> Bool {
        
        if dictDataOfGeneralNotesSelected.count > 0 {
            
            var isHash = false
            
            if (txtFillin.text?.count == 0) {
                
                isHash = checkIfStringContains(strString: txtV_Description.text!, strChar: "##", countt: 2)

            }
            
            var  arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescriptionId")!)"))
            
            if strToUpdate == "Yes" {
                
                arrOfDiclaimer = NSArray()
                
            }
            
            if arrOfDiclaimer.count > 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_GeneralNotesExist, viewcontrol: self)
                
                return false
                
            }
            else if(btnSelectedGeneralNotes.title(for: .normal) == strSelectString){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectGeneralNotes, viewcontrol: self)
                
                return false
                
            }
            else if isHash {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterDisclaimerFillin, viewcontrol: self)
                
                return false
                
            }
            else if (txtV_Description.text?.count == 0){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterDisclaimerDesc, viewcontrol: self)
                
                return false
                
            }
            
            return true
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectGeneralNotes, viewcontrol: self)
            
            return false
            
        }
        
    }
    
}


// MARK: - -----------------------------------Search Selection Delegate -----------------------------------

extension AddGeneralNotesVC : PopUpDelegate
{
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 109){
            
            // General Notes
            
            dictDataOfGeneralNotesSelected = dictData
            btnSelectedGeneralNotes.setTitle("\(dictDataOfGeneralNotesSelected.value(forKey: "Title")!)", for: .normal)
            //txtV_Description.text = "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)"
            txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)")

        }
        
    }
}

// MARK:-
// MARK:-Selection_PopUpDelegate

extension AddGeneralNotesVC : CustomTableView
{
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if dictData["Title"] as? String  == strSelectString {
            
            dictDataOfGeneralNotesSelected = NSDictionary ()
            
            txtV_Description.text = ""
            
            btnSelectedGeneralNotes.setTitle(strSelectString, for: .normal)
            
        }else{
            
            dictDataOfGeneralNotesSelected = dictData
            btnSelectedGeneralNotes.setTitle("\(dictDataOfGeneralNotesSelected.value(forKey: "Title")!)", for: .normal)
            //txtV_Description.text = "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)"
            txtV_Description.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictDataOfGeneralNotesSelected.value(forKey: "ServiceJobDescription")!)")

        }
        
    }
    
}


// MARK: - -----------------------------------UITextViewDelegate -----------------------------------
// MARK: -

extension AddGeneralNotesVC : UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        
        isDescChanged = true
        
    }
    
}
