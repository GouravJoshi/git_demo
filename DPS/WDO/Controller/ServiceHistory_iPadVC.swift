//
//  ServiceHistoryVC.swift
//  DPS
//  peSTream 2020 Changes
//  Created by NavinPatidar on 5/21/20.
//  Copyright © 2020 Saavan. All rights reserved.



import UIKit
import SafariServices

class ServiceHistory_iPadVC: UIViewController {
    // MARK:
    // MARK: ----------IBOutlet
    
    
    @IBOutlet private weak var tvList: UITableView! {
        didSet {
            tvList.tableFooterView = UIView()
            tvList.estimatedRowHeight = 50
        }
    }
    @IBOutlet weak var btn_LoadData: UIButton!
    
    var strAcctNo = ""
    var aryTblList = NSMutableArray(),aryServiceHistoryData =  NSMutableArray()
    var dictLoginData = NSDictionary()
    var strGlobalWoId = ""
    var objWorkorderDetail = NSManagedObject()
    var strYearOfStructure = ""
    var strWoLeadId = ""
    var refresher = UIRefreshControl()
    var strWoType = ""
    var strWoStatus = ""
    var strWoBranchId = ""

    var indexTag = "" , str_Inspection = Bool() , str_Graph = Bool() , str_OtherDetails = Bool()
    
    // MARK:
    // MARK: ----------Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        strAcctNo = nsud.value(forKey: "AccNoService") as! String
        
        strWoStatus = "InComplete"
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strGlobalWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
                        
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            let strDepartmentType = "\(objWorkorderDetail.value(forKey: "departmentType") ?? "")"
            let strServiceStateLocal = "\(objWorkorderDetail.value(forKey: "serviceState") ?? "")"
            strWoBranchId = "\(objWorkorderDetail.value(forKey: "branchId") ?? "")"
            //Termite Termite
            let strIsNPMATermite = "\(objWorkorderDetail.value(forKey: "isNPMATermite") ?? "")"

            if strDepartmentType == "Termite" && strServiceStateLocal == "California" {
                
                strWoType = "WDO"
                
            }
            
            if strIsNPMATermite == "true" || strIsNPMATermite == "True" || strIsNPMATermite == "1"{
                
                strWoType = "NpmaTermite"
                
            }
            
            // isNPMATermite
            
            if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                
                // Batch Released do not copy
                
                strWoStatus = "Complete"

            }else{
                
                
                
            }
 
                
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                strWoLeadId = "\(match.value(forKey: "leadId")!)"
                
            }
            
        }
        
        btn_LoadData.layer.cornerRadius = 12.0
        self.dictLoginData =  nsud.value(forKey: "LoginDetails")as! NSDictionary
        
        if (isInternetAvailable()){
            
            //FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

                self.getService_HistoryList()

            })

        }else{
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvList!.addSubview(refresher)
        
    }
    
    // MARK:
    // MARK: ----------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        //self.navigationController?.popViewController(animated: false)
        dismiss(animated: false)

    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let controller = UIStoryboard.init(name: "WDOiPad", bundle: nil).instantiateViewController(withIdentifier: "ServiceHistory_Filter_iPadVC") as! ServiceHistory_Filter_iPadVC
        controller.tag = 1
        controller.delegate = self
        //self.navigationController?.pushViewController(controller, animated: false)
        self.present(controller, animated: false, completion: nil)

    }
    @IBAction func actionOnLoadData(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(str_Inspection == false && str_Graph == false && str_OtherDetails == false){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select order to load data", viewcontrol: self)
            
        }else{
            
            
            let alert = UIAlertController(title: alertInfo, message: "This will replace the existing data if exist. Are you sure to load and replace.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Load Data", style: .default , handler:{ (UIAlertAction)in
                
                self.Call_LoadDataAPI()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
                
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
                    
        }
        
    }
    
}


// MARK: -
// MARK: -----UITableViewDelegate

extension ServiceHistory_iPadVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTblList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "ServiceHistoryCell", for: indexPath as IndexPath) as! ServiceHistoryCell
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        cell.lbl_Name.text = Global().strFullName(dictData as? [AnyHashable : Any])
        cell.lbl_Order.text = "\(dictData.value(forKey: "WorkOrderNo")!)"
        cell.lbl_Address.text = Global().strCombinedAddressService(dictData as? [AnyHashable : Any])

        cell.lbl_Service.text = "\(dictData.value(forKey: "Service")!)"
        cell.lbl_Route.text = "\(dictData.value(forKey: "RouteNo")!)" == "" ? "N/A" : "\(dictData.value(forKey: "RouteNo")!)"
        
        cell.lbl_Empolyee.text = "\(dictData.value(forKey: "EmployeeName")!) (\(dictData.value(forKey: "EmployeeNo")!))"
        
        cell.lbl_ScheduleStartDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "ScheduleOnStartDateTime")!)", strRequiredFormat: "MM/dd/yyyy HH:mm:ss")
        
        cell.btn_PDF.setTitle("Service Report", for: .normal)
        
        let strDepartmentType = "\(dictData.value(forKey: "DepartmentType")!)"
        
        let strServiceStateLocal = "\(dictData.value(forKey: "ServiceState")!)"
        
        let strIsNPMATermite = "\(dictData.value(forKey: "IsNPMATermite") ?? "")"
        
        cell.btn_height.constant = 0.0
        
        var isNPMATermiteWO = false
        
        if (strIsNPMATermite == "true" || strIsNPMATermite == "True" || strIsNPMATermite == "1") {
            isNPMATermiteWO = true
        }

        if (strDepartmentType == "Termite" && strServiceStateLocal == "California" && strWoType == "WDO" && strWoStatus == "InComplete" && !isNPMATermiteWO)
        {
            cell.btn_height.constant = 40.0
        }else if ((strWoType == "NpmaTermite") && (strIsNPMATermite == "true" || strIsNPMATermite == "True" || strIsNPMATermite == "1"))
        {
            cell.btn_height.constant = 40.0
        }else{
            cell.btn_height.constant = 0.0
        }
        
        if(indexTag == "\(indexPath.row)"){
            self.str_Graph == true ? cell.btn_Graph.setImage( UIImage(named: "check_ipad"), for: .normal) : cell.btn_Graph.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
            self.str_Inspection == true ? cell.btn_IbspectionDetails.setImage( UIImage(named: "check_ipad"), for: .normal) : cell.btn_IbspectionDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
            self.str_OtherDetails == true ? cell.btn_OtherDetails.setImage( UIImage(named: "check_ipad"), for: .normal) : cell.btn_OtherDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)

            
        }else{
            cell.btn_Graph.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
            cell.btn_IbspectionDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)
            cell.btn_OtherDetails.setImage( UIImage(named: "uncheck_ipad"), for: .normal)

        }
        
        
        cell.btn_Graph.tag = indexPath.row
        cell.btn_Graph.addTarget(self, action: #selector(self.actionWithGraph(sender:)), for: .touchUpInside)
        
        
        cell.btn_PDF.tag = indexPath.row
        cell.btn_PDF.addTarget(self, action: #selector(self.actionWithPDF(sender:)), for: .touchUpInside)
        
        cell.btn_IbspectionDetails.tag = indexPath.row
        cell.btn_IbspectionDetails.addTarget(self, action: #selector(self.actionWithIbspectionDetails(sender:)), for: .touchUpInside)
        
        cell.btn_OtherDetails.tag = indexPath.row
        cell.btn_OtherDetails.addTarget(self, action: #selector(self.actionWithOtherDetails), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    @objc func actionWithPDF(sender : UIButton){
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        
        let strPath = "\(dictData.value(forKey: "InvoicePath")!)"
        if(strPath == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }else{
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + strPath
            let strUrlwithString = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let url = URL(string: strUrlwithString)
            let safariVC = SFSafariViewController(url: url!)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        
        
    }
    @objc func actionWithIbspectionDetails(sender : UIButton){
        
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
        
        let branchIdLocal = "\(dictData.value(forKey: "BranchId")!)"
        
        if branchIdLocal == strWoBranchId {
            
            if(self.indexTag != "\(sender.tag)"){
                self.str_Inspection = true
                self.str_Graph = false
                self.str_OtherDetails = false

            }else{
                self.str_Inspection == true ? self.str_Inspection = false : (self.str_Inspection = true)
            }
            self.indexTag = "\(sender.tag)"
            self.tvList.reloadData()
            
        } else {
            
            Global().alertMethod(Alert, "This operation cannot be completed because the work order belongs to a different branch.")
            
        }
        
    }
    
    @objc func actionWithOtherDetails(sender : UIButton){
        
        let dictData =   removeNullFromDict(dict: (aryTblList.object(at: sender.tag)as! NSDictionary).mutableCopy()as! NSMutableDictionary)

        let branchIdLocal = "\(dictData.value(forKey: "BranchId")!)"
        
        if branchIdLocal == strWoBranchId {
            
            if(self.indexTag != "\(sender.tag)"){
                self.str_OtherDetails = true
                self.str_Graph = false
                self.str_Inspection = false

            }else{
                self.str_OtherDetails == true ? self.str_OtherDetails = false : (self.str_OtherDetails = true)
            }
            self.indexTag = "\(sender.tag)"
            self.tvList.reloadData()
            
        } else {
            
            Global().alertMethod(Alert, "This operation cannot be completed because the work order belongs to a different branch.")
            
        }
        
    }
    
    @objc func actionWithGraph(sender : UIButton){
        
        if(self.indexTag != "\(sender.tag)"){
            self.str_Graph = true
            self.str_Inspection = false
            self.str_OtherDetails = false

        }else{
            self.str_Graph == true ? self.str_Graph = false : (self.str_Graph = true)
        }
        self.indexTag = "\(sender.tag)"
        self.tvList.reloadData()
        
    }
}


// MARK: -
// MARK: -ServiceHistoryCell
class ServiceHistoryCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Order: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Service: UILabel!
    
    @IBOutlet weak var lbl_Route: UILabel!
    @IBOutlet weak var lbl_Empolyee: UILabel!
    @IBOutlet weak var lbl_ScheduleStartDate: UILabel!
    @IBOutlet weak var btn_PDF: UIButton!
    @IBOutlet weak var btn_Graph: UIButton!
    @IBOutlet weak var btn_IbspectionDetails: UIButton!
    @IBOutlet weak var btn_height: NSLayoutConstraint!
    @IBOutlet weak var btn_OtherDetails: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    
    
}

// MARK: -
// MARK: - API Calling
extension ServiceHistory_iPadVC{
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.getService_HistoryList()

            })

        }
        
    }
    func getService_HistoryList () {
        if(isInternetAvailable()){
                        
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + UrlMechanicalServiceHistory + "\(self.dictLoginData.value(forKeyPath: "Company.CompanyKey")!)&AccountNo=" + strAcctNo
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                
                if(Status)
                {
                    if(Response["data"] is NSArray){
                        self.aryServiceHistoryData = NSMutableArray()
                        self.aryTblList = NSMutableArray()

                        self.aryServiceHistoryData = ((Response["data"] as! NSArray).mutableCopy()as! NSMutableArray)
                        self.aryTblList = ((Response["data"] as! NSArray).mutableCopy()as! NSMutableArray)
                        
                        /*var arrData = Response.value(forKey: "data") as! NSArray
                        arrData = arrData.reversed() as NSArray
                        self.aryServiceHistoryData = arrData.mutableCopy() as! NSMutableArray
                        self.aryTblList =  self.aryServiceHistoryData*/
                        
                        self.tvList.reloadData()
                    }
                    
                    DispatchQueue.main.async {
                        FTIndicator.dismissProgress()
                        self.refresher.endRefreshing()
                    }
                    
                }
                else
                {
                   
                    DispatchQueue.main.async {
                        FTIndicator.dismissProgress()
                        self.refresher.endRefreshing()
                    }
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }else{
            
            DispatchQueue.main.async {
                FTIndicator.dismissProgress()
                self.refresher.endRefreshing()
            }
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
    }
    
    func downloadGraphXML(strWoId : String) {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
            
            var strURL = String()
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            
            strURL = strURL + "\(xmlFileName)"
            
            strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
            
            DispatchQueue.global(qos: .background).async {
                
                let url = URL(string:strURL)
                let data = try? Data(contentsOf: url!)
                
                if data != nil && (data?.count)! > 0 {
                    
                    DispatchQueue.main.async {
                        
                        savepdfDocumentDirectory(strFileName: xmlFileName, dataa: data!)
                        
                    }}
                
            }
        }
        
        
    }
    func saveInspectionData(dictData : NSDictionary , strWoId : String) {
        
            if (dictData.value(forKey: Entity_WdoInspection)) is NSDictionary {

                        var dictOfData = dictData.value(forKey: Entity_WdoInspection) as! NSDictionary
                
                        dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary

                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workOrderId")
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifiedDate")
        
                        arrOfKeys.add("comments")
                        arrOfKeys.add("cpcInspTagPosted")
                        arrOfKeys.add("dateOfPrevPco")
                        arrOfKeys.add("externalMaterial")
                        arrOfKeys.add("furnishedUnfurnished")
                        arrOfKeys.add("generalDescription")
                        arrOfKeys.add("isBuildingPermit")
                        arrOfKeys.add("isSeparateReport")
                        arrOfKeys.add("isYearUnknown")
                        arrOfKeys.add("monthlyAmount")
                        arrOfKeys.add("nameOfPrevPco")
                        arrOfKeys.add("numberOfStories")
                        arrOfKeys.add("occupiedVacant")
                        arrOfKeys.add("reportTypeId")
                        arrOfKeys.add("reportTypeName")
                        arrOfKeys.add("roofingMaterial")
                        arrOfKeys.add("slabRaised")
                        arrOfKeys.add("structureDescription")
                        arrOfKeys.add("subareaAccess")
                        arrOfKeys.add("tIPMasterId")
                        arrOfKeys.add("followUpInspectionId")
                        arrOfKeys.add("tIPMasterName")
                        arrOfKeys.add("yearOfStructure")
                        arrOfKeys.add("isTipWarranty")
                        arrOfKeys.add("pastPcoTagPosted")
                        
                        arrOfKeys.add("isActive")
                        arrOfKeys.add("orderedBy")
                        arrOfKeys.add("orderedByAddress")
                        arrOfKeys.add("propertyOwner")
                        arrOfKeys.add("propertyOwnerAddress")
                        arrOfKeys.add("reportSentTo")
                        arrOfKeys.add("reportSentToAddress")
                        arrOfKeys.add("woWdoInspectionId")
                        arrOfKeys.add("wdoTermiteIssueCodeSysNames")
                        arrOfKeys.add("reportSysName")
                        arrOfKeys.add("wDOLeadId") // reportTypeSysName
                        arrOfKeys.add("reportTypeSysName")
                        arrOfKeys.add("totalTIPDiscount")
                        arrOfKeys.add("leadInspectionFee")
                        arrOfKeys.add("othersSubAreaAccess")
                        arrOfKeys.add("othersRoofMaterials")
                        arrOfKeys.add("othersExteriorMaterial")
                        arrOfKeys.add("othersNumberofStories")
                        arrOfKeys.add("othersCpcTagLocated")
                        arrOfKeys.add("othersOtherPcoTag")
                        arrOfKeys.add("isApplyTipDiscount")//isApplyTipDiscount
                
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                      
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "Comments") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CpcInspTagPosted") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "DateOfPrevPco") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ExternalMaterial") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "FurnishedUnfurnished") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "GeneralDescription") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsBuildingPermit") as! Bool)
                        arrOfValues.add(dictOfData.value(forKey: "IsSeparateReport") as! Bool)
                        arrOfValues.add(dictOfData.value(forKey: "IsYearUnknown") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "MonthlyAmount") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "NameOfPrevPco") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "NumberOfStories") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OccupiedVacant") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "RoofingMaterial") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "SlabRaised") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "StructureDescription") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "SubareaAccess") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "TIPMasterId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "FollowUpInspectionId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "TIPMasterName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "YearOfStructure") ?? "")")
                        arrOfValues.add(dictOfData.value(forKey: "IsTipWarranty") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "PastPcoTagPosted") ?? "")")

                        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
                        arrOfValues.add("\(dictOfData.value(forKey: "OrderedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OrderedByAddress") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "PropertyOwner") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "PropertyOwnerAddress") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportSentTo") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportSentToAddress") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "WoWdoInspectionId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "WdoTermiteIssueCodeSysNames") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "WDOLeadId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "TotalTIPDiscount") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "LeadInspectionFee") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersSubAreaAccess") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersRoofMaterials") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersExteriorMaterial") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersNumberofStories") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersCpcTagLocated") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "OthersOtherPcoTag") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "IsApplyTipDiscount") ?? "")")

                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strGlobalWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strGlobalWoId as String)
                    
                }
                
                // Update Lead Inspection Fee in Lead Object
                
                self.updateLeadInspectionFee(leadInspectionFee: "\(dictOfData.value(forKey: "LeadInspectionFee") ?? "")")
                
                // Check and Save Default Master
                
                strYearOfStructure = "\(dictOfData.value(forKey: "YearOfStructure") ?? "")"
                
                if dictOfData.value(forKey: "IsTipWarranty") as! Bool == true {
                    
                    if "\(dictOfData.value(forKey: "TIPMasterId") ?? "")".count > 0 {
                        
                        checkWoWdoGeneralNoteMapping(strMappingType: "TIPMaster", strIdToCheck: "\(dictOfData.value(forKey: "TIPMasterId") ?? "")")
                        
                    }
                    
                }
                
                if dictOfData.value(forKey: "IsYearUnknown") as! Bool == true {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "YearUnknown", strIdToCheck: "")
                    
                }
                
                if "\(dictOfData.value(forKey: "YearOfStructure") ?? "")".count > 0 {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "YearOfStructure", strIdToCheck: "")
                    
                }
                
                if "\(dictOfData.value(forKey: "FollowUpInspectionId") ?? "")".count > 0 {
                    
                    checkWoWdoGeneralNoteMapping(strMappingType: "FollowUpMaster", strIdToCheck: "\(dictOfData.value(forKey: "FollowUpInspectionId") ?? "")")
                    
                }
                
                // For Report Type
                
                if "\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")".count > 0 {
                    
                    checkWoWdoDiscalimerMapping(strMappingType: "ReportType", strIdToCheck: "\(dictOfData.value(forKey: "ReportTypeSysName") ?? "")" , strToSave: "Yes")
                    
                }
                
            }
        
    }
    
    fileprivate func updateLeadInspectionFee(leadInspectionFee : String)
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        arrOfKeys.add("leadInspectionFee")

        // values
        arrOfValues.add(leadInspectionFee)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strWoLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    // MARK: - -----------------------------------General Notes Based on TIP and Follow Up Inspection-----------------------------------

    func checkWoWdoGeneralNoteMapping(strMappingType : String , strIdToCheck : String) {

        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoGeneralNoteMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                if strMappingType == "BuildingPermit" || strMappingType == "YearUnknown"{
                    
                    if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                        
                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strGlobalWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            
                            saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                            
                        }
                        
                    }
                    
                }else if strMappingType == "YearOfStructure"{//
                    
                    if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                        
                        //YearRange 1950-2018

                        let yearRange = "\(dictData.value(forKey: "YearRange") ?? "")"
                        
                        let arrRange = yearRange.components(separatedBy: "-")
                        
                        if arrRange.count == 2 {
                            
                            var range1 = Int(arrRange[0])
                            range1 = range1! - 1 + 1
                            var range2 = Int(arrRange[1])
                            range2 = range2! + 1
                            let yearOfStructureInt = Int(strYearOfStructure)
                            
                            let successful = (range1!..<range2!).contains(yearOfStructureInt!)
                            
                            if successful {
                                
                                let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                                
                                let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strGlobalWoId , generalNoteIdTemp))
                                
                                if arrOfGeneralNoteTemp.count == 0 {
                                    
                                    // General Note Id Does not exist so insert data of General Notes in DC
                                    
                                    saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }else if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                    
                    if strIdToCheck == "\(dictData.value(forKey: "MasterId") ?? "")" {

                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"

                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strGlobalWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            
                            saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveGeneralNotesDB(strGeneralNotedId : String , strMappingType : String) {
        
        var array = NSMutableArray()
        var dictDataOfGeneralNotesTemp = NSDictionary()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if strGeneralNotedId == "\(dictData.value(forKey: "ServiceJobDescriptionId") ?? "")" {
                    
                    dictDataOfGeneralNotesTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfGeneralNotesTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strGlobalWoId)
            arrOfValues.add(true)
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("generalNoteMasterId")
            arrOfKeys.add("generalNoteDescription")
            arrOfKeys.add("title")
            arrOfKeys.add("wdoGeneralNoteId")
            
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescriptionId")!)")
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescription")!)")
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "Title")!)")
            
            arrOfValues.add(Global().getReferenceNumber())
            
            arrOfKeys.add("type")
            arrOfValues.add(strMappingType)
            
            arrOfKeys.add("isChanged")
            arrOfValues.add(false)
            
            arrOfKeys.add("fillIn")
            arrOfValues.add("")

            
            // Saving Disclaimer In DB
            saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strGlobalWoId as String)
            
        }
        
    }
    
    // MARK: - -----------------------------------Discalimer Based on Report Type-----------------------------------
    
    func checkWoWdoDiscalimerMapping(strMappingType : String , strIdToCheck : String , strToSave : String) {
        
        
        //WoWdoGeneralNoteMappingExtSerDcs FollowUpMaster TIPMaster BuildingPermit
        // MasterId MappingType IsActive GeneralNoteId
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoDisclaimerMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                //if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                    
                    if strIdToCheck == "\(dictData.value(forKey: "ReportTypeSysName") ?? "")" {
                        
                        let disclaimerIdTemp = "\(dictData.value(forKey: "DisclaimerId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  disclaimerMasterId == %@", strGlobalWoId , disclaimerIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // Discalimer Id Does not exist so insert data of Discalimer in DB
                            
                            saveDiscalimerDB(strDisclaimerId: disclaimerIdTemp, strMappingType: strMappingType, strToSave: strToSave)
                            
                        }
                        
                    }
                    
                //}
                
            }
            
        }
        
    }
    
    
    func saveDiscalimerDB(strDisclaimerId : String , strMappingType : String , strToSave : String) {
        
        var dictDataOfDiscalimerTemp = NSDictionary()
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "DisclaimerMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        // Check if Deafult Disclaimer then remove it from list
        if ary.count != 0{
            
            for item in ary {
                
                let dictData = item as! NSDictionary
                
                if strDisclaimerId == "\(dictData.value(forKey: "DisclaimerMasterId") ?? "")" {
                    
                    dictDataOfDiscalimerTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfDiscalimerTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            arrOfKeys.add("isDefault")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strGlobalWoId)
            arrOfValues.add(true)
            arrOfValues.add(false)

            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("disclaimerMasterId")
            arrOfKeys.add("disclaimerDescription")
            arrOfKeys.add("disclaimerName")
            arrOfKeys.add("isChanged")
            arrOfKeys.add("woWdoDisclaimerId")
            arrOfKeys.add("fillIn")
            arrOfKeys.add("isChanged")

            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "DisclaimerMasterId")!)")
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "Disclaimer")!)")
            arrOfValues.add("\(dictDataOfDiscalimerTemp.value(forKey: "Title")!)")
            arrOfValues.add(false)
            arrOfValues.add(Global().getReferenceNumber())
            arrOfValues.add("")
            arrOfValues.add(false)
            
            if strToSave == "No" {
                
                
            }else{
                
                // Saving Disclaimer In DB
                saveDataInDB(strEntity: Entity_Disclaimer, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strGlobalWoId as String)
            
        }
        
    }
    
    func deleteMainGraphImageBeforeSaving() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strGlobalWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlImageName = "\(dictData.value(forKey: "woImagePath") ?? "")"

            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"true"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"True"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strGlobalWoId,"1"))
            
            Global().removeImage(fromDocumentDirectory: xmlImageName)
            
            let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            Global().removeImage(fromDocumentDirectory: strImageNameSales)
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strImageNameSales))

            // jugad for deleting sales image if synced from web
            
            let strNameTemp = "UploadImages\\" + strImageNameSales
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strNameTemp))

        }
        
    }
    
    func saveGraphData(dictData : NSDictionary , strWoId : String) {
        
            if (dictData.value(forKey: "WoImagesDetailExtSerDc")) is NSDictionary {

                // Delete Before Saving
                self.deleteMainGraphImageBeforeSaving()
                
                        var dictOfData = dictData.value(forKey: "WoImagesDetailExtSerDc") as! NSDictionary
                        
                        dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary

                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                
                        arrOfKeys.add("companyKey")
                        arrOfKeys.add("userName")
                        arrOfKeys.add("workorderId")
                        arrOfKeys.add("createdBy")
                        arrOfKeys.add("createdDate")
                        arrOfKeys.add("modifiedBy")
                        arrOfKeys.add("modifiedDate")
        
                        arrOfKeys.add("woImageId")
                        arrOfKeys.add("woImagePath")
                        arrOfKeys.add("woImageType")
                        arrOfKeys.add("imageCaption")
                        arrOfKeys.add("imageDescription")
                        arrOfKeys.add("latitude")
                        arrOfKeys.add("longitude")
                        arrOfKeys.add("graphXmlPath")
                        arrOfKeys.add("isProblemIdentifaction")
                
                        arrOfValues.add(Global().getCompanyKey())
                        arrOfValues.add(Global().getUserName())
                        arrOfValues.add(strWoId)
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifyBy") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                        
                        arrOfValues.add("\(dictOfData.value(forKey: "WoImageId") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "WoImagePath") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "WoImageType") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "ImageCaption") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "imageDescription") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Latitude") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "Longitude") ?? "")")
                        arrOfValues.add("\(dictOfData.value(forKey: "GraphXmlPath") ?? "")")

                        let strIsProblemIdentifaction = "\(dictOfData.value(forKey: "IsProblemIdentifaction") ?? "")"
                
                        if strIsProblemIdentifaction == "1" ||  strIsProblemIdentifaction == "true" ||         strIsProblemIdentifaction == "True"{
                     
                            arrOfValues.add("true")
                     
                        }else{
                     
                            arrOfValues.add("false")
                     
                        }

                        saveDataInDB(strEntity: Entity_WdoImages, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                // Save Same Data in Sales Auto Lead
                
                // Save Main Graph to service auto Also
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strCompanyKey = String()
                var strUserName = String()
                var strEmployeeid = String() // EmployeeId
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
                    
                    strCompanyKey = "\(value)"
                }
                
                if let value = dictLoginData.value(forKeyPath: "Company.Username") {
                    
                    strUserName = "\(value)"
                }
                
                if let value = dictLoginData.value(forKey: "EmployeeId") {
                    
                    strEmployeeid = "\(value)"
                    
                }
                
                let strImageNameSales = "\(dictOfData.value(forKey: "WoImagePath") ?? "")".replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                
                let arrOfKeysSales = NSMutableArray()
                let arrOfValuesSales = NSMutableArray()
                
                arrOfKeysSales.add("createdBy")
                arrOfKeysSales.add("createdDate")
                arrOfKeysSales.add("companyKey")
                arrOfKeysSales.add("userName")
                arrOfKeysSales.add("leadImagePath")
                arrOfKeysSales.add("leadImageType")
                arrOfKeysSales.add("modifiedBy")
                arrOfKeysSales.add("leadImageId")
                arrOfKeysSales.add("modifiedDate")
                arrOfKeysSales.add("leadId")
                arrOfKeysSales.add("leadImageCaption")
                arrOfKeysSales.add("descriptionImageDetail")
                arrOfKeysSales.add("latitude")
                arrOfKeysSales.add("longitude")
                
                arrOfValuesSales.add("") //createdBy
                arrOfValuesSales.add("") //createdDate
                arrOfValuesSales.add(strCompanyKey)
                arrOfValuesSales.add(strUserName)
                arrOfValuesSales.add(strImageNameSales)
                arrOfValuesSales.add("Graph")
                arrOfValuesSales.add(strEmployeeid)
                arrOfValuesSales.add("\(dictOfData.value(forKey: "WoImageId") ?? "")") // woImageId
                arrOfValuesSales.add("\(dictOfData.value(forKey: "ModifiedDate") ?? "")")
                arrOfValuesSales.add(strWoLeadId)
                arrOfValuesSales.add("\(dictOfData.value(forKey: "ImageCaption") ?? "")") // imageCaption
                arrOfValuesSales.add("\(dictOfData.value(forKey: "imageDescription") ?? "")") // imageDescription
                arrOfValuesSales.add("\(dictOfData.value(forKey: "Latitude") ?? "")") // latitude
                arrOfValuesSales.add("\(dictOfData.value(forKey: "Longitude") ?? "")") //  longitude
                
                saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
                
            }
        
        // download graph XML
        
        self.downloadGraphXML(strWoId: strGlobalWoId)
        
    }
    
    func Call_LoadDataAPI () {
        if(isInternetAvailable()){
            FTIndicator.showProgress(withMessage: "Please wait...", userInteractionEnable: false)
            
            var type = ""
            if(str_OtherDetails == true){
                type = "detail"
            }else if(str_Inspection == true && str_Graph == true){
                type = "both"
            }else if(str_Inspection == true){
                type = "inspection"
            }else if(str_Graph == true){
                type = "graph"
            }
            let dictData =   removeNullFromDict(dict: (aryTblList.object(at:Int(indexTag)!)as! NSDictionary).mutableCopy()as! NSMutableDictionary)
            let strWorkOrderID = "\(dictData.value(forKey: "WorkorderId")!)"
            
            
            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + UrlServiceHistory_LoadDataV2 + "\(type)&WorkOrderId=" + strWorkOrderID
            
            if (strWoType == "NpmaTermite")
            {
                
                strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + UrlNpmaTermiteServiceHistory_LoadDataV2 + "\(type)&WorkOrderId=" + strWorkOrderID
                
            }
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
                FTIndicator.dismissProgress()
                print(Response)
                
                if(Status)
                {
                    
                    // Handle response
                    
                    if Response.value(forKey: "data") is NSDictionary {
                        
                        if self.strWoType == "NpmaTermite" {// NPMA Termite flow
                            
                            nsud.set(true, forKey: "yesupdatedNPMAInspection")
                            nsud.set(true, forKey: "loadeDataRefreshConfigureProposalViewNPMA")
                            nsud.set(true, forKey: "loadeDataRefreshAgreementViewNPMA")
                            nsud.set(true, forKey: "loadeDataRefreshFinalizeViewNPMA")
                            nsud.synchronize()
                            
                            var strMessage = "Saved Successfully"
                            
                            var dictData = Response.value(forKey: "data") as! NSDictionary
                            
                            if(self.str_OtherDetails == true){
                                
                                if dictData.value(forKey: "WorkOrderExtSerDc") is NSDictionary {
                                    
                                    // data exist replace Full Work Order Here
                                    
                                   let dictOfDataWdoWo = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary

                                    
                                    //WebService().savingWDOAllDataDB(dictWorkOrderDetail: dictOfDataWdoWo.value(forKey: "WorkOrderExtSerDc") as! NSDictionary, strWoId: self.strGlobalWoId, strType: "WdoLoadData", strWdoLeadId: self.strWoLeadId)
                                    WebService().savingWDOAllDataDB(dictWorkOrderDetail: dictOfDataWdoWo.value(forKey: "WorkOrderExtSerDc") as! NSDictionary, strWoId: self.strGlobalWoId, strType: "WdoLoadData", strWdoLeadId: self.strWoLeadId, isFromServiceHistory: true)

                                    
                                    // NPMA Data
                                    WebService().savingNPMADataToCoreDB(dictWorkOrderDetail: dictOfDataWdoWo.value(forKey: "WorkOrderExtSerDc") as! NSDictionary, strWoId: self.strGlobalWoId , strType: "NPMALoadData")

                                    // Also Save Images Detail Data if exist
                                    
                                    let dictDataInspection = dictData.value(forKey: "WorkOrderExtSerDc") as! NSDictionary

                                    let arrOfImagesData = dictDataInspection.value(forKey: "ImagesDetail") as! NSArray
                                    
                                    for k in 0 ..< arrOfImagesData.count {
                                        
                                        if arrOfImagesData[k] is NSDictionary {
                                                    
                                            var dictOfData = arrOfImagesData[k] as! NSDictionary
                                            
                                            if (dictOfData.value(forKey: "IsProblemIdentifaction") as! Bool) == true {
                                                
                                                //var dictOfData = dictData.value(forKey: "ImagesDetail") as! NSDictionary
                                                
                                                dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary
                                                
                                                let graphXMLPath = "\(dictOfData.value(forKey: "GraphXmlPath") ?? "")"
                                                
                                                if graphXMLPath.count > 0 {
                                                    
                                                    self.saveGraphData(dictData: dictData, strWoId: self.strGlobalWoId)
                                                    
                                                }
                                                
                                                break
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    let alert = UIAlertController(title: alertInfo, message: "Data saved successfully", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.dismiss(animated: false)
                                        
                                    }))
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                }else{
                                    
                                    let alert = UIAlertController(title: alertInfo, message: NoDataAvailableee, preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.dismiss(animated: false)
                                        
                                    }))
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                }
                                
                            }else{
                                
                                dictData =  Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary

                                self.saveNpmaTermiteInspectionData(dictWorkOrderDetail: dictData, strWoId: self.strGlobalWoId)
                                
                                if dictData.value(forKey: "WoImagesDetailExtSerDc") is NSDictionary {
                                    
                                    // GraphXmlPath
                                    
                                    var dictOfData = dictData.value(forKey: "WoImagesDetailExtSerDc") as! NSDictionary
                                    
                                    dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary
                                    
                                    let graphXMLPath = "\(dictOfData.value(forKey: "GraphXmlPath") ?? "")"
                                    
                                    if graphXMLPath.count > 0 {
                                        
                                        self.saveGraphData(dictData: dictData, strWoId: self.strGlobalWoId)
                                        
                                    }else{
                                        
                                        if(self.str_Inspection == true && self.str_Graph == true){
                                            
                                            strMessage = "No Graph available for this work order. Inspection saved successfully"
                                            
                                        }else if(self.str_Graph == true){
                                            
                                            strMessage = "No Graph available for this work order."
                                            
                                        }
                                        
                                    }
                                    
                                }else{
                                    
                                    if(self.str_Inspection == true && self.str_Graph == true){
                                        
                                        strMessage = "No Graph available for this work order. Inspection saved successfully"
                                        
                                    }else if(self.str_Graph == true){
                                        
                                        strMessage = alertNoGraphAvailable
                                        
                                    }
                                    
                                }
                                
                                //Update Modify Date In Work Order DB
                                updateServicePestModifyDate(strWoId: self.strGlobalWoId as String)
                                
                                
                                let alert = UIAlertController(title: alertInfo, message: strMessage, preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                    
                                    if strMessage == alertNoGraphAvailable {
                                        
                                        
                                        
                                    }else{
                                        
                                        self.dismiss(animated: false)

                                    }
                                    
                                }))
                                
                                alert.popoverPresentationController?.sourceView = self.view
                                
                                self.present(alert, animated: true, completion: {
                                    
                                })
                                
                            }
                            
                        } else { // WDO FLOW
                            
                            nsud.set(true, forKey: "yesupdatedWdoInspection")
                            nsud.set(true, forKey: "loadeDataRefreshConfigureProposalView")
                            nsud.set(true, forKey: "loadeDataRefreshAgreementView")
                            nsud.set(true, forKey: "loadeDataRefreshFinalizeView")
                            nsud.synchronize()
                            
                            var strMessage = "Saved Successfully"
                            
                            let dictData = Response.value(forKey: "data") as! NSDictionary
                            
                            if(self.str_OtherDetails == true){
                                
                                if dictData.value(forKey: "WorkOrderExtSerDc") is NSDictionary {
                                    
                                    // data exist replace Full Work Order Here
                                    
                                   let dictOfDataWdoWo = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictData as! [AnyHashable : Any]))! as NSDictionary

                                  //  WebService().savingWDOAllDataDB(dictWorkOrderDetail: dictOfDataWdoWo.value(forKey: "WorkOrderExtSerDc") as! NSDictionary, strWoId: self.strGlobalWoId, strType: "WdoLoadData", strWdoLeadId: self.strWoLeadId)
                                    WebService().savingWDOAllDataDB(dictWorkOrderDetail: dictOfDataWdoWo.value(forKey: "WorkOrderExtSerDc") as! NSDictionary, strWoId: self.strGlobalWoId, strType: "WdoLoadData", strWdoLeadId: self.strWoLeadId, isFromServiceHistory: true)

                                    
                                    // Also Save insepction Data if exist
                                    
                                    let dictDataInspection = dictData.value(forKey: "WorkOrderExtSerDc") as! NSDictionary
                                    
                                    if dictDataInspection.value(forKey: "WoWdoInspectionExtSerDc") is NSDictionary {

                                        self.saveInspectionData(dictData: dictDataInspection , strWoId: self.strGlobalWoId)
                                        
                                        let arrOfImagesData = dictDataInspection.value(forKey: "ImagesDetail") as! NSArray
                                        
                                        for k in 0 ..< arrOfImagesData.count {
                                            
                                            if arrOfImagesData[k] is NSDictionary {
                                                        
                                                var dictOfData = arrOfImagesData[k] as! NSDictionary
                                                
                                                if (dictOfData.value(forKey: "IsProblemIdentifaction") as! Bool) == true {
                                                    
                                                    //var dictOfData = dictData.value(forKey: "ImagesDetail") as! NSDictionary
                                                    
                                                    dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary
                                                    
                                                    let graphXMLPath = "\(dictOfData.value(forKey: "GraphXmlPath") ?? "")"
                                                    
                                                    if graphXMLPath.count > 0 {
                                                        
                                                        self.saveGraphData(dictData: dictData, strWoId: self.strGlobalWoId)
                                                        
                                                    }
                                                    
                                                    break
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    let alert = UIAlertController(title: alertInfo, message: "Data saved successfully", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.dismiss(animated: false)
                                        
                                    }))
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                }else{
                                    
                                    let alert = UIAlertController(title: alertInfo, message: NoDataAvailableee, preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                        
                                        self.dismiss(animated: false)
                                        
                                    }))
                                    alert.popoverPresentationController?.sourceView = self.view
                                    self.present(alert, animated: true, completion: {
                                    })
                                    
                                }
                                
                            }else{
                                
                                if dictData.value(forKey: "WoWdoInspectionExtSerDc") is NSDictionary {
                                    
                                    self.saveInspectionData(dictData: dictData, strWoId: self.strGlobalWoId)
                                    
                                }
                                
                                if dictData.value(forKey: "WoImagesDetailExtSerDc") is NSDictionary {
                                    
                                    // GraphXmlPath
                                    
                                    var dictOfData = dictData.value(forKey: "WoImagesDetailExtSerDc") as! NSDictionary
                                    
                                    dictOfData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictOfData as! [AnyHashable : Any]))! as NSDictionary
                                    
                                    let graphXMLPath = "\(dictOfData.value(forKey: "GraphXmlPath") ?? "")"
                                    
                                    if graphXMLPath.count > 0 {
                                        
                                        self.saveGraphData(dictData: dictData, strWoId: self.strGlobalWoId)
                                        
                                    }else{
                                        
                                        if(self.str_Inspection == true && self.str_Graph == true){
                                            
                                            strMessage = "No Graph available for this work order. Inspection saved successfully"
                                            
                                        }else if(self.str_Graph == true){
                                            
                                            strMessage = "No Graph available for this work order."
                                            
                                        }
                                        
                                    }
                                    
                                }else{
                                    
                                    if(self.str_Inspection == true && self.str_Graph == true){
                                        
                                        strMessage = "No Graph available for this work order. Inspection saved successfully"
                                        
                                    }else if(self.str_Graph == true){
                                        
                                        strMessage = alertNoGraphAvailable
                                        
                                    }
                                    
                                }
                                
                                //Update Modify Date In Work Order DB
                                updateServicePestModifyDate(strWoId: self.strGlobalWoId as String)
                                
                                
                                let alert = UIAlertController(title: alertInfo, message: strMessage, preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                                    
                                    if strMessage == alertNoGraphAvailable {
                                        
                                        
                                        
                                    }else{
                                        
                                        self.dismiss(animated: false)

                                    }
                                    
                                }))
                                
                                alert.popoverPresentationController?.sourceView = self.view
                                
                                self.present(alert, animated: true, completion: {
                                    
                                })
                                
                            }
                            
                        }
                        

                  
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    func saveNpmaTermiteInspectionData(dictWorkOrderDetail : NSDictionary , strWoId : String) {

        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionGeneralDescriptionExtSerDc")) is NSDictionary {
            
            WebService().savingWoNPMAInspectionGeneralDescriptionExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionGeneralDescriptionExtSerDc") as! NSDictionary, strWoId: strWoId, strType: "NPMALoadData")
            
        }
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionOtherDetailExtSerDc")) is NSDictionary {
            
            WebService().savingWoNPMAInspectionOtherDetailExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionOtherDetailExtSerDc") as! NSDictionary, strWoId: strWoId , strType: "NPMALoadData")
            
        }
        
        // WoNPMAInspectionMoistureDetailExtSerDc
        
        if (dictWorkOrderDetail.value(forKey: "WoNPMAInspectionMoistureDetailExtSerDc")) is NSDictionary {
            
            WebService().savingWoNPMAInspectionMoistureDetailExtSerDcToCoreDB(dictData: dictWorkOrderDetail.value(forKey: "WoNPMAInspectionMoistureDetailExtSerDc") as! NSDictionary, strWoId: strWoId , strType: "NPMALoadData")
            
        }
        
    }
    
}
// MARK: -
// MARK: - ServiceHistory_Filter_iPadProtocol
extension ServiceHistory_iPadVC: ServiceHistory_Filter_iPadProtocol
{
    func getDataServiceHistory_Filter_iPadProtocol(dict: NSDictionary, tag: Int) {
        if(tag == 1){
            let id =  "\(dict.value(forKey: "id")!)"
            if(id == "1"){ // For All
                aryTblList = NSMutableArray()
                aryTblList = aryServiceHistoryData
                self.tvList.reloadData()
            }
            else if(id == "2"){ // Only Termite
               aryTblList = NSMutableArray()
                for item in aryServiceHistoryData {
                    if "\((item as AnyObject).value(forKey: "DepartmentType")!)" == "Termite"{
                        aryTblList.add(item)
                    }
                }
                self.tvList.reloadData()
            }
        }
    }
}
// MARK: -
// MARK: -----UITableViewDelegate

extension ServiceHistory_iPadVC: SFSafariViewControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        //dismiss(animated: true)
    }
}
