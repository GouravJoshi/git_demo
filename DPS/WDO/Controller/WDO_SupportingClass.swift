//
//  WDO_SupportingClass.swift
//  DPS
//
//  Created by Navin Patidar on 07/11/22.
//  Copyright © 2022 Saavan. All rights reserved.
//ASGASfvdgfasghdfhgas

import Foundation
import CoreData

//MARK: WDO_TIP_Type----------------------------------


enum WDO_TIP_Type{
    static let TIP = "TIP"
    static let THPS = "THPS"
}

//MARK: getRateMasterByID---------------------------------

func getRateMasterByID(aryMaster : NSMutableArray , id : String , objWO : NSManagedObject) -> NSMutableArray {
   
    var aryMasterData = NSArray()
    
    //Rate master ID check
    if id != "" && id != "<null>" {
        aryMasterData = aryMaster.filter { (task) -> Bool in
            return "\((task as! NSDictionary).value(forKey: "RateMasterId") ??  "")" == "\(id)" } as NSArray
    }
    //Rate master ID blank and WO status completed
    else{
        
        if getWDO_WorkOrderStatus(woObj: objWO) {
            if("\(objWO.value(forKey: "scheduleStartDateTime") ?? "")" != ""){
                let scheduleStartDate1  = changeStringDateToGivenFormat(strDate: "\(objWO.value(forKey: "scheduleStartDateTime")!)", strRequiredFormat: "MM/dd/yyyy")
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let date = dateFormatter.date(from: scheduleStartDate1)
                
                checkDate_isBetweenRateMaster(dateWO: date!, aryMaster: aryMaster) { aryselected, status in
                    if status{
                        aryMasterData = NSMutableArray()
                        aryMasterData = aryselected
                    }
                   
                }
            }
        }
    }
    
    //IF nothing found then check IsEffective
    if  aryMasterData.count == 0 {
    if aryMaster.count != 0 {
        let dict = (aryMaster.firstObject)as! NSDictionary
        if dict.value(forKey: "IsEffective") != nil {
            aryMasterData = aryMaster.filter { (task) -> Bool in
                return ("\((task as! NSDictionary).value(forKey: "IsEffective") ??  "")".lowercased() == "true" || "\((task as! NSDictionary).value(forKey: "IsEffective") ??  "")".lowercased() == "1") && ("\((task as! NSDictionary).value(forKey: "IsActive") ??  "")".lowercased() == "true" || "\((task as! NSDictionary).value(forKey: "IsActive") ??  "")" == "1")  } as NSArray
        }
        
    }
    }
    
    if  aryMasterData.count == 0 {
    if aryMaster.count != 0 {
    aryMasterData = aryMaster.filter { (task) -> Bool in
        return "\((task as! NSDictionary).value(forKey: "IsActive") ??  "")".lowercased() == "true" || "\((task as! NSDictionary).value(forKey: "IsActive") ??  "")" == "1" } as NSArray
    }
 }
    return aryMasterData.mutableCopy()as! NSMutableArray
}


func checkDate_isBetweenRateMaster( dateWO:Date, aryMaster : NSMutableArray,OnResultBlock: @escaping (_ arydata: NSMutableArray , _ statusMaster : Bool   ) -> Void) {
    for (_ , item) in aryMaster.enumerated() {
        if (item as AnyObject).value(forKey: "EffectiveStartDate") != nil {
            let startDate = "\((item as AnyObject).value(forKey: "EffectiveStartDate") ?? "")"
            let endDate = "\((item as AnyObject).value(forKey: "EffectiveEndDate") ?? "")"
            
            if(startDate != "" && endDate != ""){
                let startDate1  = changeStringDateToGivenFormat(strDate: startDate, strRequiredFormat: "MM/dd/yyyy")
                let endDate1  = changeStringDateToGivenFormat(strDate: endDate, strRequiredFormat: "MM/dd/yyyy")

                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let startDate2 = dateFormatter.date(from: startDate1)!
                let endDate2 = dateFormatter.date(from: endDate1)!

                if dateWO.isBetween(startDate2, and: endDate2) {
                    let ary = NSMutableArray()
                    ary.add(item)
                    OnResultBlock(ary,true)
                    return
                }
            }
        }
    }
 
    OnResultBlock(NSMutableArray(),false)
    return
 

}




func getWDO_WorkOrderStatus(woObj: NSManagedObject) -> Bool {
    
    if woObj.value(forKey: "isBatchReleased") as? String == "true" || woObj.value(forKey: "isBatchReleased") as? String == "True" || woObj.value(forKey: "isBatchReleased") as? String == "1"{
        
        return true
        
    }else{
        
    //---For Show status
    //    let wdoWorkflowStageId = "\(woObj.value(forKey: "wdoWorkflowStageId") ?? "")" == "0" ? "" : "\(woObj.value(forKey: "wdoWorkflowStageId") ?? "")"
        //let wdoWorkflowStatus = "\(woObj.value(forKey: "wdoWorkflowStatus") ?? "")"
        let wdoWorkOrderStatus = "\(woObj.value(forKey: "workorderStatus") ?? "")"
        if((wdoWorkOrderStatus.lowercased() == "completed" || wdoWorkOrderStatus.lowercased() == "complete")){
            return true
        }else{
            return false
        }
    }
}

func is_WOEditMode(woObj : NSManagedObject) -> Bool {
    if WebService().checkIfStageTypeIsIsnpector(obj: woObj, checkflowStageType: true, checkflowStatus: false) {
        return true // edit rkhna hai
    }else{
        return false
    }
}



func checkOpportunityEditOrNot(status : String , stage : String , woObj : NSManagedObject) -> (status: Bool, opportunityLimitedAccess: Bool) {
  //  let wostatus = is_WOEditMode(woObj: woObj)
    let wostatus1 = WebService().wdoWorkOrderStatus(woObj: woObj)
    
  // wostatus1 == true ==  not edit WO

    if(status.lowercased() == "complete" && stage.lowercased() == "won"){
        return (true , true)
    }else{
        
        if wostatus1 {
            
            return (false , true)
        }else{
            return (false , false)

        }
    }
}


extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}
final class TopAlignedLabel: UILabel {
  override func drawText(in rect: CGRect) {
    super.drawText(in: .init(
      origin: .zero,
      size: textRect(
        forBounds: rect,
        limitedToNumberOfLines: numberOfLines
      ).size
    ))
  }
}
