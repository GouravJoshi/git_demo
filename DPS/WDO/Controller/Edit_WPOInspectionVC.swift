//
//  Edit_WPOInspectionVC.swift
//  DPS
//  peSTream
//  Created by Navin Patidar on 9/19/19.
//  Copyright © 2019 Saavan. All rights reserved.


import UIKit

class Edit_WPOInspectionVC: UIViewController {

    // MARK:
    // MARK: IBOutlet

    @IBOutlet weak var txt_OrderbyName: ACFloatingTextField!
    @IBOutlet weak var txt_OrderbyAddress: ACFloatingTextField!
    @IBOutlet weak var txt_PropertyOwnerName: ACFloatingTextField!
    @IBOutlet weak var txt_PropertyOwnerAddress: ACFloatingTextField!
    @IBOutlet weak var txt_ReportSentToName: ACFloatingTextField!
    @IBOutlet weak var txt_ReportSentToAddress: ACFloatingTextField!
    
    
    // MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = NSString ()
    var objWorkorderDetail = NSManagedObject()
    
    // MARK:
    // MARK: Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            //setDefaultValuesInspection()
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
       setValuesToUpdate()
        
       setDefaultValuesInspection()

    }
    
    // MARK:
    // MARK: IBAction's
    @IBAction func action_OnBackButton(_ sender: UIButton) {
        
        self.view.endEditing(true)

        back()
        
    }
    
    
    @IBAction func action_OnSaveButton(_ sender: UIButton) {
        
        self.view.endEditing(true)

        if txt_OrderbyName.text!.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please enter ordered by name", viewcontrol: self)
            
        }else {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            
            arrOfKeys.add("orderedBy")
            arrOfKeys.add("orderedByAddress")
            arrOfKeys.add("propertyOwner")
            arrOfKeys.add("propertyOwnerAddress")
            arrOfKeys.add("reportSentTo")
            arrOfKeys.add("reportSentToAddress")
            
            arrOfValues.add(txt_OrderbyName.text!)
            arrOfValues.add(txt_OrderbyAddress.text!)
            arrOfValues.add(txt_PropertyOwnerName.text!)
            arrOfValues.add(txt_PropertyOwnerAddress.text!)
            arrOfValues.add(txt_ReportSentToName.text!)
            arrOfValues.add(txt_ReportSentToAddress.text!)
            
            // Temporary hai hatana hai
            //        arrOfKeys.add("isBuildingPermit")
            //        arrOfKeys.add("isSeparateReport")
            //        arrOfKeys.add("isYearUnknown")
            //        arrOfKeys.add("isTipWarranty")
            //        arrOfKeys.add("dateOfPrevPco")
            //        arrOfValues.add(true)
            //        arrOfValues.add(true)
            //        arrOfValues.add(true)
            //        arrOfValues.add(true)
            //        arrOfValues.add("15/05/2019")
            
            
            saveDataInDB(strEntity: Entity_WdoInspection, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                nsud.set(true, forKey: "yesupdatedWdoInspection")
                nsud.synchronize()
                
                back()
                //  asdnfmnasd
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }

    }
    
    // MARK: - -----------------------------------Functions -----------------------------------

    func back() {
        
        dismiss(animated: false)
        
    }
    
    func setValuesToUpdate() {
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            
            txt_OrderbyName.text = "\(objData.value(forKey: "orderedBy") ?? "")"
            txt_OrderbyAddress.text = "\(objData.value(forKey: "orderedByAddress") ?? "")"
            txt_PropertyOwnerName.text = "\(objData.value(forKey: "propertyOwner") ?? "")"
            txt_PropertyOwnerAddress.text = "\(objData.value(forKey: "propertyOwnerAddress") ?? "")"
            txt_ReportSentToName.text = "\(objData.value(forKey: "reportSentTo") ?? "")"
            txt_ReportSentToAddress.text = "\(objData.value(forKey: "reportSentToAddress") ?? "")"
            
        }else{
            
            back()
            
        }
        
    }
    
    func setDefaultValuesInspection() {
        
        // setting default values on Inspection
        
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            
            
        }else{
            
            var strServiceContactName = String()
            strServiceContactName = "\(objWorkorderDetail.value(forKey: "serviceFirstName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceMiddleName") ?? "")" + " " + "\(objWorkorderDetail.value(forKey: "serviceLastName") ?? "")"
            
            if (txt_OrderbyName.text?.count)! < 1 {
                
                txt_OrderbyName.text = strServiceContactName
                
            }
            
            if (txt_PropertyOwnerName.text?.count)! < 1 {
                
                txt_PropertyOwnerName.text = strServiceContactName
                
            }
            
            if (txt_ReportSentToName.text?.count)! < 1 {
                
                txt_ReportSentToName.text = strServiceContactName
                
            }
            
            if (txt_OrderbyAddress.text?.count)! < 1 {
                
                txt_OrderbyAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
            if (txt_PropertyOwnerAddress.text?.count)! < 1 {
                
                txt_PropertyOwnerAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
            if (txt_ReportSentToAddress.text?.count)! < 1 {
                
                txt_ReportSentToAddress.text = Global().strCombinedAddressService(for: objWorkorderDetail)
                
            }
            
        }
        
    }
    

}
