//
//  FinalizeReport_WDOVC.swift
//  DPS 2020 September
//  Created by Rakesh Jain on 20/09/19.
//  Copyright © 2019 Saavan. All rights reserved.
//  Saavan Commit.


import UIKit

class PricingCellFinalizeReport:UITableViewCell
{
    
    @IBOutlet weak var btnAddToAgreement: UIButton!
    @IBOutlet weak var btnDiscount: UIButton!
    @IBOutlet weak var lblIssueCode: UILabel!
    @IBOutlet weak var lblRecommendationCode: UILabel!
    @IBOutlet weak var lblSubsectionCode: UILabel!
    @IBOutlet weak var lblRecommendation: UITextView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblSubTotalName: UILabel!
    @IBOutlet weak var lblDiscountName: UILabel!
    @IBOutlet weak var lblTotalName: UILabel!
  //by navin
    @IBOutlet weak var viewBidOnrequest: UIView!
    @IBOutlet weak var stkViewSubTotal: UIStackView!
    @IBOutlet weak var stkViewTotal: UIStackView!
    @IBOutlet weak var stkViewDiscount: UIStackView!

}

class GeneralNotesFinalizeReportCell:UITableViewCell
{
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFillin: UILabel!
    @IBOutlet weak var txtViewGeneralNoteDesc: UITextView!

}

class FinalizeReport_WDOVC: UIViewController {
    
    // MARK:     ------------------------  Outlets   ------------------------
    
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    
    // MARK:     ---------  Outlet  Personal Info   ---------
    
    @IBOutlet weak var viewPersonalInfo: UIView!
    @IBOutlet weak var lblPerson: UILabel!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblSalesRepName: UILabel!
    @IBOutlet weak var lblServiceAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var btnEmailPersonalInfo: UIButton!
    
    @IBOutlet weak var btnPhonePersonalInfo: UIButton!
    @IBOutlet weak var btnServiceAddress: UIButton!
    
    @IBOutlet weak var btnAudio: UIButton!
    
    
    // MARK:     ---------  Outlet  Inspection Details   ---------
    
    @IBOutlet weak var lblOrderedBy: UILabel!
    @IBOutlet weak var lblAddressOrderBy: UILabel!
    
    @IBOutlet weak var lblPropertyOwner: UILabel!
    @IBOutlet weak var lblAddressPropertyOwner: UILabel!
    
    @IBOutlet weak var lblReportSentTo: UILabel!
    @IBOutlet weak var lblAddressReportSentTo: UILabel!
    
    
    @IBOutlet weak var lblGeneralDescription: UILabel!
    @IBOutlet weak var lblStructureDescription: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    
    @IBOutlet weak var lblReportType: UILabel!
    @IBOutlet weak var lblSubareaAccess: UILabel!
    @IBOutlet weak var lblNoOfStories: UILabel!
    @IBOutlet weak var lblExternalMaterial: UILabel!
    @IBOutlet weak var lblRoofingMaterial: UILabel!
    @IBOutlet weak var lblYearOfStructure: UILabel!
    @IBOutlet weak var lblYearUnknown: UILabel!
    @IBOutlet weak var lblBuildingPermit: UILabel!
    @IBOutlet weak var lblEmailAddress_InspectionDetails: UILabel!
    @IBOutlet weak var lblCPCIspeTagPosted: UILabel!
    @IBOutlet weak var lblPastPcoTagPosted: UILabel!
    @IBOutlet weak var lblNameOfPrevPco: UILabel!
    @IBOutlet weak var lblDateOfPrevPco: UILabel!
    
    @IBOutlet weak var lblSeperateReport: UILabel!
    @IBOutlet weak var lblTipWarranty: UILabel!
    @IBOutlet weak var lblTipWarrantyType: UILabel!
    @IBOutlet weak var lblMonthlyAmount: UILabel!
    @IBOutlet weak var lblTermiteIssueCode: UILabel!
    
    @IBOutlet weak var lblOthers: UILabel!
    
    
    // MARK:     --------- Outlet Problem Identification Details   ---------
    
    @IBOutlet weak var tblProblemIdentification: UITableView!
    @IBOutlet weak var const_TblProblemIdentification_H: NSLayoutConstraint!
    
    // MARK:     --------- Outlet Pricing ---------
    
    @IBOutlet weak var tblPricing: UITableView!
    
    @IBOutlet weak var const_TblPricing_H: NSLayoutConstraint!
    @IBOutlet weak var lblTotalPricing: UILabel!
    
    // MARK:     --------- Outlet Payment Mode  ---------
    
    @IBOutlet weak var viewPaymentMode: UIView!
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCreditCard: UIButton!
    @IBOutlet weak var btnAutoChargeCustomer: UIButton!
    @IBOutlet weak var btnPaymentPending: UIButton!
    @IBOutlet weak var btnNoCharge: UIButton!
    @IBOutlet weak var btnBill: UIButton!
    
    
    
    
    @IBOutlet weak var const_ViewCheck_H: NSLayoutConstraint!
    @IBOutlet weak var txtAmount: ACFloatingTextField!
    @IBOutlet weak var txtCheckNo: ACFloatingTextField!
    @IBOutlet weak var txtDrivingLicenseNo: ACFloatingTextField!
    @IBOutlet weak var btnExpirationDate: UIButton!
    @IBOutlet weak var btnCheckFrontImage: UIButton!
    @IBOutlet weak var btnCheckBackImage: UIButton!
    
    // MARK:     ---------Outlet Payment Detail  ---------
    
    @IBOutlet weak var lblChargesOfCurrentServices: UILabel!
    @IBOutlet weak var lblTaxValue: UILabel!
    @IBOutlet weak var lblTotalDueValue: UILabel!
    @IBOutlet weak var txtFldAmount: ACFloatingTextField!
    
    // MARK:     ---------Outlet Other Detail  ---------
    
    @IBOutlet weak var viewOtherDetail: CardView!
    @IBOutlet weak var lblTimeIn: UILabel!
    @IBOutlet weak var lblTimeOut: UILabel!
    @IBOutlet weak var txtViewTechnicianComment: UITextView!
    
    @IBOutlet weak var txtViewOfficeNotes: UITextView!
    
    // MARK:     ---------Outlet Signature  ---------
    
    @IBOutlet weak var viewSignature: CardView!
    @IBOutlet weak var imgViewCustomerSignature: UIImageView!
    @IBOutlet weak var imgViewTechnicianSignature: UIImageView!
    
    @IBOutlet weak var btnCustomerNotPresent: UIButton!
    
    @IBOutlet weak var btnCustomerSignature: UIButton!
    @IBOutlet weak var btnTechnicianSignature: UIButton!
    
    @IBOutlet weak var height_viewSignature: NSLayoutConstraint!
    
    // MARK:     ---------Outlet Terms Condition  ---------
    
    @IBOutlet weak var txtViewTermsCondition: UITextView!
    @IBOutlet weak var constViewTermsCondition: NSLayoutConstraint!
    
    @IBOutlet weak var const_viewServiceTerms_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_HghtMainGraphImage: NSLayoutConstraint!

    @IBOutlet weak var mainGraphImageView: UIImageView!

    // MARK:     ---------Outlet General Notes  ---------
    @IBOutlet weak var hghtConstTblViewGeneralNotes: NSLayoutConstraint!
    
    @IBOutlet weak var tblViewGeneralNotes: UITableView!
    
    @IBOutlet weak var hghtConstTitleGeneralNotes: NSLayoutConstraint!
    
    @IBOutlet weak var btnMainGraph: UIButton!

    @IBOutlet weak var tblViewDiscalimer: UITableView!
    
    @IBOutlet weak var hghtConstTblViewDisclaimer: NSLayoutConstraint!
    
    @IBOutlet weak var hghtConstTitleDiclaimer: NSLayoutConstraint!
    
    @IBOutlet weak var lblGarage: UILabel!
    
    @IBOutlet weak var lblTIP_Title: UILabel!

    // MARK:     ------------------------  Variable   ------------------------
    
    
    
    // MARK: - ---------  Global Variables  ---------

    @objc var strWdoLeadId = NSString ()
    @objc var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    @objc var objWdoLeadDetail = NSManagedObject()

    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    
    
    // MARK:     ---------  NSManagedObject   ---------
    
    
    var paymentInfoDetail = NSManagedObject()
    
    
    // MARK:     ---------  Dictionary   ---------
    
    
    var dictLoginData = NSDictionary()
    var dictDepartmentNameFromSysName = NSDictionary()
    
    
    // MARK:     ---------  Array   ---------
    
    var arrData = NSMutableArray()
    var arrOfProblemIdentification = NSArray()
    var arrOfPricingPolicy = NSArray()
    var arrOfPricing = NSMutableArray()
    var arrOfGeneralNotes = NSArray()
    var arrOfDiclaimer = NSArray()
    
    // MARK:     ---------  String  ---------
    
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strGlobalPaymentMode = String()
    var strPaymentModes = String()
    var strPaidAmount = String()
    var strCustomerSign = String()
    var strTechnicianSign = String()
    var strCheckFrontImage = String()
    var strCheckBackImage = String()
    var strAudioName = String()
    var signatureType = String()
    var strAudioNameSales = String()

    
    
    // MARK:     ---------  Bool   ---------
    
    var chkCustomerNotPresent = Bool()
    var chkFrontImage = Bool()
    var isPreSetSignGlobal = Bool()
    var yesEditedSomething = Bool()
    var isAudioTaken = Bool()

    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        const_HghtMainGraphImage.constant = 0
        
        strGlobalPaymentMode = ""
        strPaymentModes = ""
        strPaidAmount = ""
        strCheckFrontImage = ""
        strCheckBackImage = ""
        strCustomerSign = ""
        strTechnicianSign = ""
        strAudioName = ""
        signatureType = ""
        
        chkFrontImage = false
        chkCustomerNotPresent = false
        isPreSetSignGlobal = false
        yesEditedSomething = false
        
        lblHeaderTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"
        
        btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportType == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        setBorderAtLoad()
        fetchWdoLeadDeatils()
        fetchWorkOrderDetailFromCoreData()
        fetchPaymentInfoDetailFromCoreData()
        fetchInspectionDetail()
        fetchProblemIdentificaiton()
        getTermsCondition()
        fetchBeforeImageFromLocalDB()
        fetchGeneralNotesFromLocalDB()
        fetchDisclaimerFromLocalDB()
        
        // Do any additional setup after loading the view.

        tblViewGeneralNotes.tableFooterView = UIView()

    }
    
    override func viewDidLayoutSubviews() {

        if hghtConstTblViewGeneralNotes.constant < tblViewGeneralNotes.contentSize.height {
            
            hghtConstTblViewGeneralNotes.constant = CGFloat(tblViewGeneralNotes.contentSize.height) + 10
            tblViewGeneralNotes.reloadData()

        }
        
        if hghtConstTblViewDisclaimer.constant < tblViewDiscalimer.contentSize.height {
            
            hghtConstTblViewDisclaimer.constant = CGFloat(tblViewDiscalimer.contentSize.height)
            tblViewDiscalimer.reloadData()

        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        if (nsud.value(forKey: "loadeDataRefreshFinalizeView") != nil) {
            let yesupdatedWdoInspection = nsud.bool(forKey: "loadeDataRefreshFinalizeView")
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "loadeDataRefreshFinalizeView")
                nsud.synchronize()
                
                // Reload Data if Loaded Data
                /*fetchInspectionDetail()
                fetchProblemIdentificaiton()
                fetchBeforeImageFromLocalDB()
                fetchGeneralNotesFromLocalDB()
                fetchDisclaimerFromLocalDB()*/
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    
                    self.fetchInspectionDetail()
                    self.fetchProblemIdentificaiton()
                    self.fetchBeforeImageFromLocalDB()
                    self.fetchGeneralNotesFromLocalDB()
                    self.fetchDisclaimerFromLocalDB()

                })
                
            }
            
        }
        
        //callOnLoad()

        if strTechnicianSign.count>0
        {
            downloadTechnicianSign(strImageName: strTechnicianSign)
        }
        
        if strCustomerSign.count>0
        {
            downloadCustomerSign(strImageName: strCustomerSign)
        }
        
        
        //For Preset
        var strImageName = String()
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)))
        {

            strImageName = Global().strTechSign(strSignUrl)
            strTechnicianSign = Global().strTechSign(strSignUrl)
            
            let nsUrl = URL(string: strSignUrl)
            
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
            
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgViewTechnicianSignature.image!)
            
            btnTechnicianSignature.isEnabled = false
            isPreSetSignGlobal = true
            
        }
        else
        {
            isPreSetSignGlobal = false
            btnTechnicianSignature.isEnabled = true
            
            if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
            {
                btnTechnicianSignature.isEnabled = false
            }
            if strTechnicianSign.count>0
            {
                let strIsPresetWO = objWorkorderDetail.value(forKey: "isEmployeePresetSignature") as! String
                
                if strIsPresetWO == "true" || strIsPresetWO == "1"
                {
                    downloadTechnicianPreset(strImageName: strTechnicianSign, stringUrl: strSignUrl)
                }
                else
                {
                    downloadTechnicianSign(strImageName: strTechnicianSign)
                }
                
                
            }
        }
        
        //For Audio
        if (nsud.value(forKey: "yesAudio") != nil)
        {
            yesEditedSomething = true
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio
            {
                
                strAudioName = nsud.value(forKey: "AudioNameService") as! String
                strAudioNameSales = nsud.value(forKey: "AudioNameWdoSales") as! String
                isAudioTaken = true
            }
            
        }
        
        methodForDisable()
        
    }

    func callOnLoad() {
        
        arrData = NSMutableArray()
        arrOfPricing = NSMutableArray()
        
        const_HghtMainGraphImage.constant = 0
        
        strGlobalPaymentMode = ""
        strPaymentModes = ""
        strPaidAmount = ""
        strCheckFrontImage = ""
        strCheckBackImage = ""
        strCustomerSign = ""
        strTechnicianSign = ""
        strAudioName = ""
        signatureType = ""
        
        chkFrontImage = false
        chkCustomerNotPresent = false
        isPreSetSignGlobal = false
        yesEditedSomething = false
        
        lblHeaderTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"
        
        btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportType == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
        setBorderAtLoad()
        fetchWdoLeadDeatils()
        fetchWorkOrderDetailFromCoreData()
        fetchPaymentInfoDetailFromCoreData()
        fetchInspectionDetail()
        fetchProblemIdentificaiton()
        getTermsCondition()
        fetchBeforeImageFromLocalDB()
        fetchGeneralNotesFromLocalDB()
        fetchDisclaimerFromLocalDB()
        
        // Do any additional setup after loading the view.

        tblViewGeneralNotes.tableFooterView = UIView()

    }
    
    // MARK:     ------------------------  Actions   ------------------------
    
    @IBAction func actionOnBack(_ sender: Any)
    {
        // Save Office Notes and Comments On Back in DB
        
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {
            
        }else{
            
            saveDataOnBack()
            
        }
        
        goBack()
    }
    
    @IBAction func actionOnAudio(_ sender: Any)
    {
        yesEditedSomething = true
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {
            
        }else{
         
            alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                
                let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                testController?.strFromWhere = flowTypeWdoSalesService
                        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
                
            }))
            
        }
        
        
        alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
            
            var audioName = nsud.value(forKey: "AudioNameService")
            
            audioName = self.strAudioName
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
            testController!.strAudioName = audioName as! String
                    testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    // MARK:  ------  Actions Personal Info  ------
    
    @IBAction func actionOnBillingAddress(_ sender: Any)
    {
        let strAddress = "\(btnBillingAddress.titleLabel?.text ?? "")" as String
        
        if (strAddress.count>0)
        {
            global.redirect(onAppleMap: self, strAddress)
            
        }
    }
    
    @IBAction func actionOnEmailPersonalInfo(_ sender: Any)
    {
        let strEmail = "\(btnEmailPersonalInfo.titleLabel?.text ?? "")" as String
        
        if (strEmail.count>0)
        {
            global.emailComposer(strEmail, "", "", self)
            
        }
    }
    
    @IBAction func actionOnServiceAddress(_ sender: Any)
    {
        let strAddress = "\(btnServiceAddress.titleLabel?.text ?? "")" as String
        
        if (strAddress.count>0)
        {
            global.redirect(onAppleMap: self, strAddress)
            
        }
    }
    
    @IBAction func actionOnPhonePersonalInfo(_ sender: Any)
    {
        let strNo = "\(btnPhonePersonalInfo.titleLabel?.text ?? "")" as String
        
        if (strNo.count>0)
        {
            global.calling(strNo)
            
        }
    }
    // MARK:  ------  Actions Payment Mode  ------
    
    @IBAction func actionOnCash(_ sender: Any)
    {
        yesEditedSomething = true
        cashPaymentMode()
    }
    
    @IBAction func actionOnCheck(_ sender: Any)
    {
        yesEditedSomething = true
        checkPaymentMode()
    }
    
    @IBAction func actionOnCreditCard(_ sender: Any)
    {
        yesEditedSomething = true
        creditCardPaymentMode()
    }
    
    @IBAction func actionOnAutoChargeCustomer(_ sender: Any)
    {
        yesEditedSomething = true
        autoChargeCustomerPaymentMode()
    }
    
    @IBAction func actionOnPaymentPending(_ sender: Any)
    {
        yesEditedSomething = true
        paymentPendingPaymentMode()
    }
    
    @IBAction func actionOnNoCharge(_ sender: Any)
    {
        yesEditedSomething = true
        noChargePaymentMode()
    }
    
    @IBAction func actionOnBill(_ sender: Any)
    {
        yesEditedSomething = true
        noBillPaymentMode()
    }
    
    // MARK:  ------  Actions Signature View  ------
    
    @IBAction func actionOnCustomerSignature(_ sender: Any)
    {
        yesEditedSomething = true
        signatureType = "customer"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
        
    }
    
    @IBAction func actionOnTechnicianSignature(_ sender: Any)
    {
        yesEditedSomething = true
        signatureType = "technician"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func actionOnCustomerNotPresent(_ sender: Any)
    {
        yesEditedSomething = true
        
        if (btnCustomerNotPresent.backgroundImage(for: .normal) == UIImage(named: "check_box_1.png"))
        {
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            imgViewCustomerSignature.isHidden = true
            btnCustomerSignature.isHidden = true
            chkCustomerNotPresent = true
        }
        else
        {
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            
            imgViewCustomerSignature.isHidden = false
            btnCustomerSignature.isHidden = false
            
            chkCustomerNotPresent = false
            
        }
        
    }
    
    @IBAction func actionOnExpirationDate(_ sender: Any)
    {
        
        yesEditedSomething = true
        
        self.gotoDatePickerView(sender: sender as! UIButton, strType: "Date")
        
    }
    
    @IBAction func actionOnCheckFrontImage(_ sender: Any)
    {
        
        chkFrontImage = true
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    @IBAction func actionOnCheckBackImage(_ sender: Any)
    {
        
        chkFrontImage = false
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            
            
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .camera
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
            var isCameraPermissionAvailable = Bool()
            
            isCameraPermissionAvailable = self.global.isCameraPermissionAvailable()
            
            if isCameraPermissionAvailable == true
            {
                let imagePickController = UIImagePickerController()
                imagePickController.delegate = self //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePickController.allowsEditing = false
                imagePickController.sourceType = .photoLibrary
                self.present(imagePickController, animated: true, completion: nil)
                
                
            }
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
            
        })
    }
    
    // MARK:  ------  Actions Footer View  ------
    
    @IBAction func action_Image(_ sender: Any)
    {
        
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.goToNotesHistory()
        
    }
    
    
    @IBAction func actionOnSave(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {
            /*
            let mainStoryboard = UIStoryboard(name: "ServiceiPad", bundle: nil)
            let objVC = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSendMailViewControlleriPad") as? ServiceSendMailViewControlleriPad
            
            var strFromWheree = NSString()
            strFromWheree = "PestInvoice"
            
            objVC?.fromWhere = strFromWheree as String
            self.navigationController?.pushViewController(objVC!, animated: false)
            */
            
            goToSendEmailScreen()
            
        }
        else
        {
            
            finalSave(strPaymentMode: strGlobalPaymentMode)
            
        }
    }
    func finalSave(strPaymentMode: String)
    {
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard"
        {
            
            let valTextAmount = Float(txtAmount.text!)
            
            if txtAmount.text == "" || txtAmount.text == "0" || txtAmount.text == "00.00" ||  (Double(txtAmount.text!) == nil || valTextAmount == 0  )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
            }
                
            else
            {
                if chkCustomerNotPresent == true
                {
                    /*if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                        
                    else*/
                        if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtCheckNo.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                         
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                    }
                    
                }
                else
                {
                    /*if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                    else if strCustomerSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                        
                    }
                    else*/
                        if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtCheckNo.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                        
                    }
                }
            }
            
        }
        else if strGlobalPaymentMode == "Bill" || strGlobalPaymentMode == "NoCharge" || strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "PaymentPending"
        {
            if chkCustomerNotPresent == true
            {
                /*if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else*/
                    if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
            else
            {
                /*if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if strCustomerSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                    
                }
                else*/
                    if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
        }
        
        
    }
    
    // MARK:     ------------------------ Functions   ------------------------
    
    // MARK:     ---------  Core Data Functions   ---------
    
    func fetchWdoLeadDeatils() {
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWdoLeadId))
        
        if(arrayLeadDetail.count > 0)
        {
            objWdoLeadDetail = arrayLeadDetail.firstObject as! NSManagedObject
  
            if("\(objWdoLeadDetail.value(forKey: "audioFilePath") ?? "")".count > 0)
            {
                strAudioNameSales = "\(objWdoLeadDetail.value(forKey: "audioFilePath")!)"
                isAudioTaken = false
            }
        
        }
        
    }
    
    func fetchWorkOrderDetailFromCoreData()
    {
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderData.count > 0
        {
            
            objWorkorderDetail = arryOfWorkOrderData[0] as! NSManagedObject
            assignValues()
            
            //------Hide SIgn View in the case of termite
            
            let departmentType = "\(objWorkorderDetail.value(forKey: "departmentType")!)"
            let serviceState = "\(objWorkorderDetail.value(forKey: "serviceState")!)"
            if departmentType == "Termite" && serviceState == "California" {
                if (DeviceType.IS_IPAD){
                    height_viewSignature.constant = 0.0
                    viewSignature.isHidden = true
                }
            }else{
                height_viewSignature.constant = 425.0
                viewSignature.isHidden = false
            }
            
        }
        else
        {
            
            
        }
    }
    func fetchPaymentInfoDetailFromCoreData()
    {
        
        let arryOfWorkOrderPaymentData = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderPaymentData.count>0
        {
            let objPaymentInfoData = arryOfWorkOrderPaymentData[0] as! NSManagedObject
            paymentInfoDetail = objPaymentInfoData
            strPaymentModes = paymentInfoDetail.value(forKey: "paymentMode")  as! String
            strPaidAmount = paymentInfoDetail.value(forKey: "paidAmount") as! String
            
            txtAmount.text = strPaidAmount
            
        }
        else
        {
            savePaymentInfoInCoreData()
            fetchPaymentInfoDetailFromCoreData()
        }
        
        
        if strPaymentModes == "Cash"
        {
            cashPaymentMode()
        }
        else if strPaymentModes == "Check"
        {
            checkPaymentMode()
        }
        else if strPaymentModes == "CreditCard"
        {
            creditCardPaymentMode()
        }
        else if strPaymentModes == "AutoChargeCustomer"
        {
            autoChargeCustomerPaymentMode()
        }
        else if strPaymentModes == "NoCharge"
        {
            noChargePaymentMode()
        }
        else if strPaymentModes == "NoBill"
        {
            noBillPaymentMode()
        }
        else if strPaymentModes == "PaymentPending"
        {
            paymentPendingPaymentMode()
        }
        else
        {
            cashPaymentMode()
        }
        
        txtCheckNo.text =  "\(paymentInfoDetail.value(forKey: "checkNo") ?? "")"
        txtDrivingLicenseNo.text = "\(paymentInfoDetail.value(forKey: "drivingLicenseNo") ?? "")"
        
        let strExpirationDate = "\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")"
        
        if strExpirationDate.count == 0 || strExpirationDate == ""
        {
            
        }
        else
        {
            btnExpirationDate.setTitle("\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")", for: .normal)
            
        }
    }
    func fetchInspectionDetail()
    {
        
        var objInspectionDetail = NSManagedObject()
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0
        {
            
            objInspectionDetail = arrOfWdoInspection[0] as! NSManagedObject
            
            lblOrderedBy.text  = "\(objInspectionDetail.value(forKey: "orderedBy") ?? "")"
            lblAddressOrderBy.text = "\(objInspectionDetail.value(forKey: "orderedByAddress") ?? " ")"
            
            
            lblPropertyOwner.text  = "\(objInspectionDetail.value(forKey: "propertyOwner") ?? "")"
            lblAddressPropertyOwner.text  = "\(objInspectionDetail.value(forKey: "propertyOwnerAddress") ?? " ")"
            
            
            lblReportSentTo.text  = "\(objInspectionDetail.value(forKey: "reportSentTo") ?? "")"
            lblAddressReportSentTo.text  = "\(objInspectionDetail.value(forKey: "reportSentToAddress") ?? " ")"
            
            
            lblReportType.text  = "\(objInspectionDetail.value(forKey: "reportTypeName") ?? "")"
            
            lblGarage.text  = ""

            let strGarageID = "\(objInspectionDetail.value(forKey: "garage") ?? "")"
            
            if strGarageID.count > 0 {
                
                if  nsud.value(forKey: "MasterServiceAutomation") is NSDictionary {
                    
                    let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as!  NSDictionary
                    if((dictMaster.value(forKey: "Garages") is NSArray)){
                        let arrayData = (dictMaster.value(forKey: "Garages") as! NSArray).mutableCopy() as! NSMutableArray
                        
                        let aryTemp = arrayData.filter { (task) -> Bool in
                            
                            return "\((task as! NSDictionary).value(forKey: "Value")!)".lowercased() == strGarageID.lowercased()
                            
                        }
                        
                        if(aryTemp.count != 0){
                            
                            let dict = aryTemp[0] as! NSDictionary
                            
                            self.lblGarage.text = "\(dict.value(forKey: "Text")!)"
                            
                        }
                    }
               
                    
                }
                
            }
            
            
            lblSubareaAccess.text  = "\(objInspectionDetail.value(forKey: "subareaAccess") ?? "")"
            
            lblNoOfStories.text  = "\(objInspectionDetail.value(forKey: "numberOfStories") ?? "")"
            
            lblExternalMaterial.text  = "\(objInspectionDetail.value(forKey: "externalMaterial") ?? "")"
            
            lblRoofingMaterial.text  = "\(objInspectionDetail.value(forKey: "roofingMaterial") ?? "")"
            
            lblYearOfStructure.text  = "\(objInspectionDetail.value(forKey: "yearOfStructure") ?? "")"
            
            let isYearKnown = "\(objInspectionDetail.value(forKey: "isYearUnknown") ?? "")"
            
            if isYearKnown == "1"{
                
                lblYearUnknown.text  = "Yes"
                lblYearOfStructure.text  = "UnKnown"

            }else{
                
                lblYearUnknown.text  = "No"
                
            }
            
            if objInspectionDetail.value(forKey: "isBuildingPermit") as! Bool == true
            {
                lblBuildingPermit.text  = "Yes"
            }
            else
            {
                lblBuildingPermit.text  = "No"
            }
            
            
            lblEmailAddress_InspectionDetails.text  = ""//"\(objInspectionDetail.value(forKey: "") ?? "")"
            
            
            lblCPCIspeTagPosted.text  = "\(objInspectionDetail.value(forKey: "cpcInspTagPosted") ?? "")"
            
            lblPastPcoTagPosted.text  = "\(objInspectionDetail.value(forKey: "pastPcoTagPosted") ?? "")"
            
            lblNameOfPrevPco.text  = "\(objInspectionDetail.value(forKey: "nameOfPrevPco") ?? "")"
            
            lblDateOfPrevPco.text  = "\(objInspectionDetail.value(forKey: "dateOfPrevPco") ?? "")"
            
            if "\(objInspectionDetail.value(forKey: "subareaAccess") ?? "")" == strOtherString {
                lblSubareaAccess.text = "\(objInspectionDetail.value(forKey: "othersSubAreaAccess") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "numberOfStories") ?? "")" == "0" {
                lblNoOfStories.text = "\(objInspectionDetail.value(forKey: "othersNumberofStories") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "externalMaterial") ?? "")" == strOtherString {
                lblExternalMaterial.text = "\(objInspectionDetail.value(forKey: "othersExteriorMaterial") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "roofingMaterial") ?? "")" == strOtherString {
                lblRoofingMaterial.text = "\(objInspectionDetail.value(forKey: "othersRoofMaterials") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "cpcInspTagPosted") ?? "")" == strOtherString {
                lblCPCIspeTagPosted.text = "\(objInspectionDetail.value(forKey: "othersCpcTagLocated") ?? "")"
            }
            
            if "\(objInspectionDetail.value(forKey: "pastPcoTagPosted") ?? "")" == strOtherString {
                lblPastPcoTagPosted.text = "\(objInspectionDetail.value(forKey: "othersOtherPcoTag") ?? "")"
            }
            
            if objInspectionDetail.value(forKey: "isSeparateReport") as! Bool == true
            {
                lblSeperateReport.text  = "Yes"
            }
            else
            {
                lblSeperateReport.text  = "No"
            }
            
            lblGeneralDescription.text = "\(objInspectionDetail.value(forKey: "generalDescription") ?? " ")"
            lblStructureDescription.text = "\(objInspectionDetail.value(forKey: "structureDescription") ?? " ")"
            lblComments.text = "\(objInspectionDetail.value(forKey: "comments") ?? " ")"
            
            
            if objInspectionDetail.value(forKey: "isTipWarranty") as! Bool == true
            {
                lblTipWarranty.text  = "Yes"
                
                let strTipMasterName = "\(objInspectionDetail.value(forKey: "tIPMasterName") ?? "")"
                let strTipMasterId = "\(objInspectionDetail.value(forKey: "tIPMasterId") ?? "")"

                lblTipWarrantyType.text  = strTipMasterName
                
                lblMonthlyAmount.text  = "\(objInspectionDetail.value(forKey: "monthlyAmount") ?? "")"
                
                var dictOfTipWarranty = NSDictionary()
                
                var ary = NSMutableArray()
                
                ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
                
                if ary.count > 0 {
                    
                    for k in 0 ..< ary.count {
                        
                        let dictData = ary[k] as! NSDictionary
                        
                        if strTipMasterId == "\(dictData.value(forKey: "TIPMasterId") ?? "")" {
                            
                            dictOfTipWarranty = dictData
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                if dictOfTipWarranty.count > 0 {

                    let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                    
                    if !isMonthlyAmount {
                        
                        lblMonthlyAmount.text  = ""
                        
                    }
                    
                    
                }
                
            }
            else
            {
                
                lblTipWarranty.text  = "No"
                
                lblTipWarrantyType.text  = ""
                
                lblMonthlyAmount.text  = ""
                
            }
            
            
            //---Show TIP / THPS
            if("\(objInspectionDetail.value(forKey: "tIPTHPSType") ?? "")".lowercased() == WDO_TIP_Type.THPS.lowercased()){
                lblTIP_Title.text = "THPS Warranty Type:"
            }else{
                lblTIP_Title.text = "TIP Warranty Type:"
            }
           
            
//            let strTipMasterName = "\(objInspectionDetail.value(forKey: "tIPMasterName") ?? "")"
//
//            lblTipWarrantyType.text  = strTipMasterName
//
//            lblMonthlyAmount.text  = "\(objInspectionDetail.value(forKey: "monthlyAmount") ?? "")"
            
            let checkBoxes = "\(objInspectionDetail.value(forKey: "wdoTermiteIssueCodeSysNames") ?? "")"
            
            var tempArray = NSArray()
            let tempTermiteIssuesCodeName = NSMutableArray()

            if checkBoxes.count > 1 {
                
                tempArray = checkBoxes.components(separatedBy: ",") as NSArray
                
            }
            
            if tempArray.count > 0 {
                
                if tempArray.contains(enumSUBTERRANEANTERMITES) {
                    
                    tempTermiteIssuesCodeName.add("SUBTERRANEAN TERMITES")
                    
                }
                if tempArray.contains(enumDRYWOODTERMITES) {
                    
                    tempTermiteIssuesCodeName.add("DRYWOOD TERMITES")

                }
                if tempArray.contains(enumFUNGUSDRYROT) {
                    
                    tempTermiteIssuesCodeName.add("FUNGUS/DRYROT")

                }
                if tempArray.contains(enumOTHERFINDINGS) {
                    
                    tempTermiteIssuesCodeName.add("OTHER FINDINGS")

                }
                
                lblTermiteIssueCode.text = tempTermiteIssuesCodeName.componentsJoined(by: ", ")
                
            }else{
                
                lblTermiteIssueCode.text = "     "
                
            }
            
            
            var strSlabRaised = "\(objInspectionDetail.value(forKey: "slabRaised") ?? "")"

            if strSlabRaised == "1" {
                
                strSlabRaised = "Slab"
                
            }
            else if strSlabRaised == "2"{
                
                strSlabRaised = "Raised"
                
            }
            else if strSlabRaised == "3"{
                
                strSlabRaised = "Slab/Raised"
                
            }else {
                
                strSlabRaised = ""

            }
            
            var strOccupiedVacant = "\(objInspectionDetail.value(forKey: "occupiedVacant") ?? "")"
            
            if strOccupiedVacant == "1" {
                
                strOccupiedVacant = "Occupied"
                
            }
            else if strOccupiedVacant == "2"{
                
                strOccupiedVacant = "Vacant"
                
            }else{
                
                strOccupiedVacant = ""

            }
            
            var strFurnishedUFurnished = "\(objInspectionDetail.value(forKey: "furnishedUnfurnished") ?? "")"
            
            if strFurnishedUFurnished == "2" {
                
                strFurnishedUFurnished = "Furnished"
                
            }
            else if strFurnishedUFurnished == "1"{
                
                strFurnishedUFurnished = "Unfurnished"
                
            }
            else{
                
                strFurnishedUFurnished = ""
                
            }
            
            lblOthers.text = "\(strOccupiedVacant), " + "\(strFurnishedUFurnished), " + "\(strSlabRaised)"
            
            showNameFromSysNameForInspectionFields(objData: objInspectionDetail)
            
            if lblAddressOrderBy.text == ""
            {
                lblAddressOrderBy.text = "     "
            }
            if lblAddressPropertyOwner.text == ""
            {
                lblAddressPropertyOwner.text = "     "
            }
            if lblAddressReportSentTo.text == ""
            {
                lblAddressReportSentTo.text = "     "
            }
            if lblGeneralDescription.text == ""
            {
                lblGeneralDescription.text = "     "
            }
            if lblStructureDescription.text == ""
            {
                lblStructureDescription.text = "     "
            }
            if lblComments.text == ""
            {
                lblComments.text = "     "
            }
            if lblTermiteIssueCode.text == ""
            {
                lblTermiteIssueCode.text = "     "
            }
            if lblOthers.text == ""
            {
                lblOthers.text = "     "
            }
            
        }
        else
        {
            
            lblOrderedBy.text  = " "
            
            lblAddressOrderBy.text = " "
            
            lblTermiteIssueCode.text = "     "
            
            lblOthers.text = "     "
            
            lblPropertyOwner.text  = " "
            
            lblAddressPropertyOwner.text  = " "
            
            lblReportSentTo.text  = " "
            
            lblAddressReportSentTo.text  = " "

            lblReportType.text  = " "

            lblSubareaAccess.text  = " "

            lblNoOfStories.text  = " "

            lblExternalMaterial.text  = " "

            lblRoofingMaterial.text  = " "
            
            lblYearOfStructure.text  = " "
 
            lblYearUnknown.text  = " "

            lblBuildingPermit.text  = " "

            lblBuildingPermit.text  = " "
            
            lblEmailAddress_InspectionDetails.text  = " "

            lblCPCIspeTagPosted.text  = " "

            lblPastPcoTagPosted.text  = " "

            lblNameOfPrevPco.text  = " "

            lblDateOfPrevPco.text  = " "

            lblSeperateReport.text  = " "

            lblSeperateReport.text  = " "

            lblGeneralDescription.text = " "

            lblStructureDescription.text = " "

            lblComments.text = " "

            lblTipWarranty.text  = " "

            lblTipWarranty.text  = " "

            lblTipWarranty.text  = " "

            lblMonthlyAmount.text  = " "
            
        }
        
    }
    
    func showNameFromSysNameForInspectionFields(objData : NSManagedObject) {
        
        // Sub Area Access
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "SubAreaAccesss", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "subareaAccess") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblSubareaAccess.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Number Of Stories
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "NumberofStories", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "numberOfStories") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblNoOfStories.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // External Materials
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "ExteriorMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "externalMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblExternalMaterial.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        
        // Roofing Materials
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RoofMaterials", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "roofingMaterial") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblRoofingMaterial.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        
        // CPC Tag Posted
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CpcTagLocateds", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "cpcInspTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblCPCIspeTagPosted.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Past Pco Tag Posted
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "OtherPcoTags", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "pastPcoTagPosted") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblPastPcoTagPosted.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
            
        }
        
        // Structure Descriptions
        
        array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "StructuredDescriptions", strBranchId: "", strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "")
        
        for k in 0 ..< array.count {
            
            let dictData = array[k] as! NSDictionary
            
            if "\(objData.value(forKey: "structureDescription") ?? "")" == "\(dictData.value(forKey: "Value") ?? "")" {
                
                lblStructureDescription.text = "\(dictData.value(forKey: "Text")!)"

                break
                
            }
        }
    }
    
    func fetchProblemIdentificaiton()
    {

        arrOfPricing = NSMutableArray()

        let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
        let arrayPI = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort)
        
        //let arrayPI = getDataFromLocal(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES",strWoId))
        
        let arrImagesLocal = NSMutableArray()
        
        if(arrayPI.count > 0)
        {
            
            arrOfProblemIdentification = arrayPI.mutableCopy() as! NSMutableArray
            
            for item in arrOfProblemIdentification
            {
                let arrayPIPricing = getDataFromLocal(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "problemIdentificationId == %@ &&  isActive == YES","\((item as! NSManagedObject).value(forKey: "problemIdentificationId")!)"))
                
                if(arrayPIPricing.count > 0)
                {
                    arrOfPricing.add(arrayPIPricing.firstObject as! NSManagedObject)
                    
//                    let objProblemIdentificationPricing = arrayPIPricing.firstObject as! NSManagedObject
//                    let strTotal = "\(objProblemIdentificationPricing.value(forKey: "total") ?? " ")"
                    
                }else{
                    
                    
                    let arrayPIPricing = getDataFromLocal(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "problemIdentificationId == %@ &&  isActive == YES","\((item as! NSManagedObject).value(forKey: "mobileId")!)"))
                    
                    if(arrayPIPricing.count > 0)
                    {
                        arrOfPricing.add(arrayPIPricing.firstObject as! NSManagedObject)
                        
                    }
                    
                }
                
                let objProblemIdentification = item as! NSManagedObject

                let strProblemIdTemp = "\(objProblemIdentification.value(forKey: "problemIdentificationId") ?? " ")"
                let strMobileProblemIdTemp = "\(objProblemIdentification.value(forKey: "mobileId") ?? " ")"
                
                var arrOfProblemImagesTemp = NSArray()
                
                if strProblemIdTemp.count > 0 {
                    
                    arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdTemp))
                    
                }else{
                    
                    arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdTemp))
                    
                }
                
                if arrOfProblemImagesTemp.count > 0 {
                    
                    for k1 in 0 ..< arrOfProblemImagesTemp.count {
                        
                        arrImagesLocal.add("33")
                        
                    }
                    
                }
                
            }
        
        }
            
         if arrOfProblemIdentification.count > 0 {
            
             tblProblemIdentification.reloadData()
            
         }
         const_TblProblemIdentification_H.constant = 702
         const_TblProblemIdentification_H.constant =  const_TblProblemIdentification_H.constant * CGFloat(arrOfProblemIdentification.count) + 20
        
        if arrImagesLocal.count > 0 {
            
            let hghtLocal = CGFloat(arrImagesLocal.count) * 210
            
            const_TblProblemIdentification_H.constant =  const_TblProblemIdentification_H.constant + hghtLocal + 10
            
        }

        if arrOfPricing.count > 0 {
            
            tblPricing.reloadData()
            
        }
        
        const_TblPricing_H.constant = 300
        const_TblPricing_H.constant =  const_TblPricing_H.constant * CGFloat(arrOfPricing.count)
        
        lblTotalPricing.text = "Total Price: $ \(Global().getTotalPrice(arrOfPricing) ?? "0.00")"
        
    }
    
    fileprivate func fetchGeneralNotesFromLocalDB()
    {
        
        // fetch General Notes
        
        let sort1 = NSSortDescriptor(key: "title", ascending: true)
        
        arrOfGeneralNotes = getDataFromCoreDataBaseArraySorted(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort1)
        
        var heightTblGeneralNotes = 0.0
        
        if arrOfGeneralNotes.count > 0 {
            
            hghtConstTitleGeneralNotes.constant = 50.0
            heightTblGeneralNotes = Double(arrOfGeneralNotes.count) * 93.0
            for k1 in 0 ..< arrOfGeneralNotes.count {
                
                let objArea = arrOfGeneralNotes[k1] as! NSManagedObject
                
                let strLocalDesc = "\(objArea.value(forKey: "generalNoteDescription") ?? "")"
                let attributedString =  attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strLocalDesc), strMinFont: fontMin)
                heightTblGeneralNotes =  heightTblGeneralNotes + CGFloat(attributedString.heightWithConstrainedWidth(width: self.tblViewDiscalimer.frame.width-100))
                print(heightTblGeneralNotes)
     
            }
            
            hghtConstTblViewGeneralNotes.constant = CGFloat(heightTblGeneralNotes) + 100

            tblViewGeneralNotes.reloadData()
            
        }else{
            
            hghtConstTitleGeneralNotes.constant = 0.0
            hghtConstTblViewGeneralNotes.constant = 0.0

        }
        
    }
    
    fileprivate func fetchDisclaimerFromLocalDB()
    {
        
        // fetch General Notes
        
        arrOfDiclaimer = getDataFromCoreDataBaseArray(strEntity: Entity_Disclaimer, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId))
        
        var heightTblDisclaimer = 0.0
        
        if arrOfDiclaimer.count > 0 {
            hghtConstTitleDiclaimer.constant = 50.0
            heightTblDisclaimer = Double(arrOfDiclaimer.count) * 104.0
            for k1 in 0 ..< arrOfDiclaimer.count {
                
                let objArea = arrOfDiclaimer[k1] as! NSManagedObject
                
                let strLocalDesc = "\(objArea.value(forKey: "disclaimerDescription") ?? "")"
                let attributedString =  attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strLocalDesc), strMinFont: fontMin)
                heightTblDisclaimer =  heightTblDisclaimer + CGFloat(attributedString.heightWithConstrainedWidth(width: self.tblViewDiscalimer.frame.width-143))
                print(heightTblDisclaimer)
                
            }
            
            hghtConstTblViewDisclaimer.constant = CGFloat(heightTblDisclaimer)
            
            tblViewDiscalimer.reloadData()
            
        }else{
            
            hghtConstTitleDiclaimer.constant = 0.0
            hghtConstTblViewDisclaimer.constant = 0.0
            
        }
        
    }
    
    fileprivate func fetchBeforeImageFromLocalDB()
    {
        
        self.btnMainGraph.isHidden = true

        var arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        let arrTempGraphImages = NSMutableArray()
        let arrTempBeforeImages = NSMutableArray()
        let arrTempAfterImages = NSMutableArray()
        
        if arrayOfImages.count > 0 {
            
            for k1 in 0 ..< arrayOfImages.count {
                
                let dictData = arrayOfImages[k1] as! NSManagedObject
                
                if "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "1" || "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "true" || "\(dictData.value(forKey: "isProblemIdentifaction") ?? "")" == "True"{
                    
                    //const_HghtMainGraphImage.constant = 550
                    
                    let strImagePath = dictData.value(forKey: "woImagePath") as! String
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                    
                    let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                    
                    if isImageExists!  {
                        
                        mainGraphImageView.image = image
                        
                        self.btnMainGraph.isHidden = false

                    }else {
                        
                        let defsLogindDetail = UserDefaults.standard
                        
                        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                        
                        var strURL = String()
                        
                        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                            
                            strURL = "\(value)"
                            
                        }
                        
                        strURL = strURL + "\(strImagePath)"
                        
                        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                        
                        let image: UIImage = UIImage(named: "NoImage.jpg")!
                        
                        mainGraphImageView.image = image
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            let url = URL(string:strURL)
                            let data = try? Data(contentsOf: url!)
                            
                            if data != nil && (data?.count)! > 0 {
                                
                                let image: UIImage = UIImage(data: data!)!
                                
                                DispatchQueue.main.async {
                                    
                                    saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                    
                                    self.mainGraphImageView.image = image

                                    self.btnMainGraph.isHidden = false

                                }}
                            
                        }
                        
                    }
                    
                    const_HghtMainGraphImage.constant = (image?.size.height)!
                    
                    btnMainGraph.frame = CGRect(x: btnMainGraph.frame.origin.x, y: btnMainGraph.frame.origin.y, width: btnMainGraph.frame.width, height: (image?.size.height)!)

                    //arrTempGraphImages.add(dictData)
                    
                }else if "\(dictData.value(forKey: "woImageType") ?? "")" == "Graph" {
                    
                    arrTempGraphImages.add(dictData)
                    
                }else if "\(dictData.value(forKey: "woImageType") ?? "")" == "Before" {
                    
                    arrTempBeforeImages.add(dictData)
                    
                }else{
                    
                    arrTempAfterImages.add(dictData)
                    
                }
                
            }
            
        }
        
        
        let arrTempAllImages = NSMutableArray()
        
        //arrTempAllImages.addObjects(from: arrOfProblemImages as! [Any])
        
        arrTempAllImages.addObjects(from: arrTempGraphImages as! [Any])
        
        //arrTempAllImages.addObjects(from: arrOfProblemImages as! [Any])
        
        arrTempAllImages.addObjects(from: arrTempBeforeImages as! [Any])
        
        arrTempAllImages.addObjects(from: arrTempAfterImages as! [Any])
        
        arrayOfImages = arrTempAllImages
        
        if(arrayOfImages.count > 0)
        {
//            tblviewGraphAndImages.isHidden = false
//
//            hghtConstTblViewGraphAndImage.constant = CGFloat(arrayOfImages.count*250)
//
//            tblviewGraphAndImages.reloadData()
        }
        else
        {
//            tblviewGraphAndImages.isHidden = true
//            hghtConstTblViewGraphAndImage.constant = 0.0
            
        }
    }
    
    func fetchPricingPolicy()
    {
        
        arrOfPricingPolicy = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId))
        
        if arrOfPricingPolicy.count > 0 {
            
            tblPricing.reloadData()
            
        }
        const_TblPricing_H.constant = 480
        const_TblPricing_H.constant =  const_TblPricing_H.constant * CGFloat(arrOfPricingPolicy.count)
        
        
    }
    
    func savePaymentInfoInCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["CheckBackImagePath",
                     "CheckFrontImagePath",
                     "CheckNo",
                     "CreatedBy",
                     "CreatedDate",
                     "DrivingLicenseNo",
                     "ExpirationDate",
                     "ModifiedBy",
                     "ModifiedDate",
                     "PaidAmount",
                     "PaymentMode",
                     "WoPaymentId"]
        
        arrOfValues = ["",
                       "",
                       "",
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "" ,
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "Cash",
                       ""]
        
        saveDataInDB(strEntity: "PaymentInfoServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    func updatePaymentInfo()
    {
        // Update Payment Info
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("paidAmount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("drivingLicenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strGlobalPaymentMode)
        arrOfValues.add(txtAmount.text ?? "")
        arrOfValues.add(txtCheckNo.text ?? "")
        arrOfValues.add(txtDrivingLicenseNo.text ?? "")
        arrOfValues.add(btnExpirationDate.titleLabel?.text ?? "")
        arrOfValues.add(strCheckFrontImage)
        arrOfValues.add(strCheckBackImage)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    
    func updateLeadDetail() {
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWdoLeadId))
        
        if(arrayLeadDetail.count > 0){
            
            let strLeadStatusGlobal = "\(objWdoLeadDetail.value(forKey: "statusSysName") ?? "")"
            
            let strStageSysName = "\(objWdoLeadDetail.value(forKey: "stageSysName") ?? "")"
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
            {
                
                
            }else {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("audioFilePath")
                
                if(strAudioNameSales.count > 0)
                {
                    arrOfValues.add(strAudioNameSales)
                }
                else
                {
                    arrOfValues.add("")
                }
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strWdoLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                
                if isSuccess
                {
                    //Update Modify Date In Work Order DB
                   // global.updateSalesModifydate(strWdoLeadId as String)
                    //When audio taken then update value lead detail value
                     if strAudioNameSales.count > 0 && isAudioTaken == true {
                                             //Update Modify Date In Work Order DB
                                             global.updateSalesModifydate(strWdoLeadId as String)
                                             // sett true to sync sales data after wdo data
                                             nsud.set(true, forKey: "synAgreementWdo")
                                             nsud.synchronize()
                                         }
                    
                }
                
            }
            
        }
        
    }
    func updateWorkOrderDetail()
    {
        
        updateLeadDetail()
        
        if strCustomerSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strCustomerSign, image: imgViewCustomerSignature.image!)
        }
        if strTechnicianSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: imgViewTechnicianSignature.image!)
        }
        
        // Update Payment Info
        
        if chkCustomerNotPresent == true
        {
            strCustomerSign = ""
        }
        
        
        var strWorkOrderStatusFinal = String()
        strWorkOrderStatusFinal = "Completed"
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var strTimeOut = String()
        strTimeOut = dateFormatter.string(from: date)
        
        var coordinate = CLLocationCoordinate2D()
        coordinate = Global().getLocation()
        let strTimeOutLat = "\(coordinate.latitude)"
        let strTimeOutLong = "\(coordinate.longitude)"
        
        
        var strResendStatus = String()
        strResendStatus = "0"
        if strWorkOrderStatus == "Incomplete"
        {
            strResendStatus = "0"
        }
        
        
        var isElementIntegraiton = Bool()
        isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
        
        if isElementIntegraiton == true
        {
            
            if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
            {
                strWorkOrderStatusFinal = "InComplete"
            }
            
        }
        
        var strCustomerNotPresent = String()
        
        if chkCustomerNotPresent == false
        {
            strCustomerNotPresent = "false"
        }
        else
        {
            strCustomerNotPresent = "true"
        }
        
        var strPresetStatus = String()
        
        if isPreSetSignGlobal == true
        {
            strPresetStatus = "true"
        }
        else
        {
            strPresetStatus = "false"
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        let amount = Double("\(txtFldAmount.text ?? "0.0")")!
        let taxPercentLocal = Double("\((objWorkorderDetail.value(forKey: "taxPercent") ?? ""))")!
        
        let taxPercentAmount = (amount * taxPercentLocal) / 100
        
        arrOfKeys = ["technicianSignaturePath",
                     "customerSignaturePath",
                     "invoiceAmount",
                     "tax",
                     "audioFilePath",
                     "technicianComment",
                     "officeNotes",
                     "timeOut",
                     "timeOutLatitude",
                     "timeOutLongitude",
                     "isResendInvoiceMail",
                     "workorderStatus",
                     "isCustomerNotPresent",
                     "IsRegularPestFlow",
                     "isEmployeePresetSignature"
        ]
        //strWorkOrderStatusFinal = "InCompleted"
        arrOfValues = [strTechnicianSign,
                       strCustomerSign,
                       (txtFldAmount.text?.count)! > 0 ? txtFldAmount.text! : "0.0",
                       "\(taxPercentAmount)",
                       strAudioName,
                       txtViewTechnicianComment.text,
                       txtViewOfficeNotes.text,
                       strTimeOut,
                       strTimeOutLat,
                       strTimeOutLong,
                       strResendStatus,
                       strWorkOrderStatusFinal,
                       strCustomerNotPresent,
                       "true",
                       strPresetStatus
            
            
        ]
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            if yesEditedSomething
            {
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            if isElementIntegraiton == true
            {
                if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
                {
                    print("Go to credit card ciew")
                    goToCreditCardScreen()
                }
                else
                {
                    print("Go to send mail")
                    goToSendEmailScreen()
                }
            }
            else
            {
                print("Go to send mail")
                goToSendEmailScreen()
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
        
    }
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            
        }
        
    }
    
    
    // MARK:    ---------   Local Functions   ---------
    
    func saveDataOnBack()  {
    
    var arrOfKeys = NSMutableArray()
    var arrOfValues = NSMutableArray()
    
    arrOfKeys = ["technicianComment",
    "officeNotes"
    ]
    //strWorkOrderStatusFinal = "InCompleted"
    arrOfValues = [txtViewTechnicianComment.text,
    txtViewOfficeNotes.text
    ]
    
    let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    
    
    if isSuccess
    {
    
    updateModifyDate()
    
    //Update Modify Date In Work Order DB
    updateServicePestModifyDate(strWoId: self.strWoId as String)
    
    }
    
    }
    
    func setColorBorderForView(item: AnyObject) -> Void
    {
        /*item.layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor
        item.layer.borderWidth = 1.0
        item.layer.cornerRadius = 5.0*/
        
        if(item is UITextView){

            (item as! UITextView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextView).layer.borderWidth = 1.0

            (item as! UITextView).layer.cornerRadius = 5.0

        }else if(item is UITextField){

            (item as! UITextField).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UITextField).layer.borderWidth = 1.0

            (item as! UITextField).layer.cornerRadius = 5.0

        }

        else if(item is UIView){

            (item as! UIView).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

            (item as! UIView).layer.borderWidth = 1.0

            (item as! UIView).layer.cornerRadius = 5.0

        }

        else if(item is UIButton){

                (item as! UIButton).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UIButton).layer.borderWidth = 1.0

                (item as! UIButton).layer.cornerRadius = 5.0

            }

           else if(item is UILabel){

                (item as! UILabel).layer.borderColor = (UIColor(red: 154.0 / 255, green: 154.0 / 255, blue: 154.0 / 255, alpha: 1)).cgColor

                (item as! UILabel).layer.borderWidth = 1.0

                (item as! UILabel).layer.cornerRadius = 5.0

            }
        
    }
    
    func getTermsCondition()
    {
        var strTremsnConditions = String()
        strTremsnConditions = ""
        let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        
        if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
        {
            let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
            
            for item in arrOfServiceJobDescriptionTemplate
            {
                let dict = item as! NSDictionary
                
                if ( "\((dict.value(forKey: "DepartmentId"))!)" == "\((objWorkorderDetail.value(forKey: "departmentId"))!)" )
                {
                    strTremsnConditions = "\(dict.value(forKey: "SR_TermsAndConditions") ?? "")"
                    break
                }
                
            }
        }

        txtViewTermsCondition.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strTremsnConditions), strMinFont: fontMin)
        
        
        
        textViewDidChange(textView: txtViewTermsCondition)
    }
    
    func downloadCustomerSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewCustomerSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            
            let nsUrl = URL(string: strUrl)
            imgViewCustomerSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func downloadTechnicianSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewTechnicianSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            let nsUrl = URL(string: strUrl)
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    func downloadTechnicianPreset(strImageName: String, stringUrl: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            imgViewTechnicianSignature.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //strUrl = strUrl + "//Documents/" + strImageName
            strUrl = stringUrl
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            imgViewTechnicianSignature.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    func goToSendEmailScreen()
    {
        
        //let strTimeInFromDb = "\(objWorkorderDetail.value(forKey: "timeIn"))"
        
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewControlleriPad") as? ServiceSendMailViewControlleriPad
        
        var strValue = String()
        var strFromWheree = NSString()
        strFromWheree = "WDO"
        
        if chkCustomerNotPresent == true
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objSendMail?.isCustomerPresent = strValue as NSString
        objSendMail?.fromWhere = strFromWheree  as NSString as String
        objSendMail?.strWoIdd = strWoId as String
        objSendMail?.strHeaderValue = "WDO SERVICE REPORT"
        //self.navigationController?.pushViewController(objSendMail!, animated: false)
        self.navigationController?.pushViewController(objSendMail!, animated: false)
        
    }
    func goToCreditCardScreen()
    {
        
        if isInternetAvailable() == true
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
            let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
            
            var strValue = String()
            if chkCustomerNotPresent == true
            {
                strValue = "no"
            }
            else
            {
                strValue = "yes"
            }
            objCreditCardView?.isCustomerPresent = strValue as NSString
            objCreditCardView?.strAmount = txtAmount.text
            objCreditCardView?.strGlobalWorkOrderId = strWoId as String
            objCreditCardView?.strTypeOfService = "service"
            objCreditCardView?.workOrderDetailNew = objWorkorderDetail
            objCreditCardView?.strFromWhichView = "WdoFlow"
            objCreditCardView?.fromWhere = "WDO"
            self.navigationController?.pushViewController(objCreditCardView!, animated: false)
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    func methodForDisable()
    {
        txtViewTermsCondition.isEditable = false
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
        {
            btnCustomerSignature.isEnabled = false
            btnTechnicianSignature.isEnabled = false
            txtViewTechnicianComment.isEditable = false
            txtViewOfficeNotes.isEditable = false
            txtAmount.isEnabled = false
            txtCheckNo.isEnabled = false
            txtDrivingLicenseNo.isEnabled = false
            btnExpirationDate.isEnabled = false
            btnCash.isEnabled = false
            btnNoCharge.isEnabled = false
            btnAutoChargeCustomer.isEnabled = false
            btnBill.isEnabled = false
            btnCreditCard.isEnabled = false
            btnCheck.isEnabled = false
            btnPaymentPending.isEnabled = false
            btnCheckFrontImage.isEnabled = false
            btnCheckBackImage.isEnabled = false
            btnCustomerNotPresent.isEnabled = false
            txtFldAmount.isEnabled = false
            
        }
        else
        {
            btnCustomerSignature.isEnabled = true
            btnTechnicianSignature.isEnabled = true
            txtViewTechnicianComment.isEditable = true
            txtViewOfficeNotes.isEditable = true
            txtAmount.isEnabled = true
            txtCheckNo.isEnabled = true
            txtDrivingLicenseNo.isEnabled = true
            btnExpirationDate.isEnabled = true
            btnCash.isEnabled = true
            btnNoCharge.isEnabled = true
            btnAutoChargeCustomer.isEnabled = true
            btnBill.isEnabled = true
            btnCreditCard.isEnabled = true
            btnCheck.isEnabled = true
            btnPaymentPending.isEnabled = true
            btnCheckFrontImage.isEnabled = true
            btnCheckBackImage.isEnabled = true
            btnCustomerNotPresent.isEnabled = true
            txtFldAmount.isEnabled = true
        }
        
    }
    func setBorderAtLoad()
    {
        buttonRound(sender: btnCustomerSignature)
        buttonRound(sender: btnTechnicianSignature)
        buttonRound(sender: btnSave)
        
        setColorBorderForView(item: txtViewOfficeNotes)
        setColorBorderForView(item: txtViewTermsCondition)
        setColorBorderForView(item: txtViewTechnicianComment)
        
        //setColorBorderForView(item: tblProblemIdentification)
        
        /*setBorderColor(item: txtViewOfficeNotes)
         setBorderColor(item: txtViewTermsCondition)
         setBorderColor(item: txtViewTechnicianComment)*/
        
    }
    
    
    func goBack()
    {
        ///dismiss(animated: false, completion: nil)
        //dismiss(animated: false)
        navigationController?.popViewController(animated: false)
        
    }
    
    func assignValues()
    {
        
        lblSalesRepName.text = strEmpName
        
        strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        txtViewOfficeNotes.text = "\(objWorkorderDetail.value(forKey: "officeNotes") ?? "")"
        txtViewTechnicianComment.text = "\(objWorkorderDetail.value(forKey: "technicianComment") ?? "")"
        //lblCustomerName.text = global.strFullName(for: objWorkorderDetail)

        let strCustomerNotPresent = "\(objWorkorderDetail.value(forKey: "isCustomerNotPresent") ?? "" )"
        if strCustomerNotPresent == "true" || strCustomerNotPresent == "1"
        {
            chkCustomerNotPresent = true
            
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_2.png"), for: .normal)
            imgViewCustomerSignature.isHidden = true
            btnCustomerSignature.isHidden = true
            
            //txtViewServiceJobDescription.isEditable = false
        }
        else
        {
            chkCustomerNotPresent = false
            btnCustomerNotPresent.setBackgroundImage(UIImage (named: "check_box_1.png"), for: .normal)
            imgViewCustomerSignature.isHidden = false
            btnCustomerSignature.isHidden = false
            //txtViewServiceJobDescription.isEditable = true
            
        }
        
        
        strCustomerSign = "\(objWorkorderDetail.value(forKey: "customerSignaturePath") ?? "")"
        strTechnicianSign = "\(objWorkorderDetail.value(forKey: "technicianSignaturePath") ?? "")"
        
        strAudioName = "\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")"
        
        var strName = "\(objWorkorderDetail.value(forKey: "firstName") ?? "")"
        
        if "\(objWorkorderDetail.value(forKey: "middleName") ?? "")".count > 0
        {
            strName = strName + " " + "\(objWorkorderDetail.value(forKey: "middleName") ?? "")"
        }
        
        strName = strName + " " + "\(objWorkorderDetail.value(forKey: "lastName") ?? "")"
        
        
        lblPerson.text = strName
        
        var strBillingAddress = String()
        strBillingAddress = global.strCombinedAddressBilling(for: objWorkorderDetail)
        lblBillingAddress.text = strBillingAddress
        btnBillingAddress.setTitle(lblBillingAddress.text, for: .normal)
        
        lblEmail.text = "\(objWorkorderDetail.value(forKey: "primaryEmail") ?? "")"
        btnEmailPersonalInfo.setTitle(lblEmail.text, for: .normal)
        
        
        var strServiceAddress = String()
        
        strServiceAddress = global.strCombinedAddressService(for: objWorkorderDetail)
        lblServiceAddress.text = strServiceAddress
        btnServiceAddress.setTitle(lblServiceAddress.text, for: .normal)
        
        lblPhone.text = "\(objWorkorderDetail.value(forKey: "primaryPhone") ?? "")"
        btnPhonePersonalInfo.setTitle(lblPhone.text, for: .normal)
        
        
        
        
        
        
        let strTimeIn = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a") as String
        if strTimeIn.count > 0
        {
            lblTimeIn.text = "Time In: " + strTimeIn
            
        }
        else
        {
            lblTimeIn.text = "Time In: " + "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
            
        }
        
        let strTimeOut = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "timeOut") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a") as String
        
        if strTimeOut.count > 0
        {
            lblTimeOut.text = "Time Out: " + strTimeOut
            
        }
        else
        {
            lblTimeOut.text = "Time Out: " + "\(objWorkorderDetail.value(forKey: "timeOut") ?? "")"
        }
        
        var total = Double()
        total = Double("\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "previousBalance") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "tax") ?? ""))")!
        
        lblChargesOfCurrentServices.text = "Charges for current services: " + "$" +  "\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))"
        
        lblTaxValue.text = "Tax: " + "$" + "\((objWorkorderDetail.value(forKey: "tax") ?? ""))"
      
        if "\((objWorkorderDetail.value(forKey: "tax") ?? ""))" == "" || "\((objWorkorderDetail.value(forKey: "tax") ?? ""))" == "0.00" {
            lblTaxValue.text = ""
        }
        lblTotalDueValue.text = "Total Due: " + "$" + "\(total)"
        
        txtFldAmount.text = ("\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))")
        
        calculateTotalDue()
        
    }
    
    func calculateTotalDue() {
        
        var total = Double()
        
        if txtFldAmount.text?.count == 0 {
            
            txtFldAmount.text = "0.0"
            
        }
        
        let amount = Double("\(txtFldAmount.text ?? "0.0")")!
        let taxPercentLocal = Double("\((objWorkorderDetail.value(forKey: "taxPercent") ?? ""))")!

        let taxPercentAmount = (amount * taxPercentLocal) / 100
        
        total = Double("\(txtFldAmount.text ?? "0.0")")! + Double("\((objWorkorderDetail.value(forKey: "previousBalance") ?? ""))")! + Double("\(taxPercentAmount)")!
        
        lblChargesOfCurrentServices.text = "Charges for current services: " + "$" +  "\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))"
        
        //let strPercentage =
        
        let strTaxPercent = String(format: "%.02f", taxPercentAmount)
        
        lblTaxValue.text = "Tax: " + "$" + "\((strTaxPercent))"
        if "\(strTaxPercent)" == "" || "\(strTaxPercent)" == "0.00" {
            lblTaxValue.text = ""
        }
       
        
        let strTotalLocal = String(format: "%.02f", total)

        lblTotalDueValue.text = "Total Due: " + "$" + "\(strTotalLocal)"
        
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWdoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func cashPaymentMode()
    {
        btnCash.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        
        txtAmount.isHidden = false
        
        strGlobalPaymentMode = "Cash"
    }
    func checkPaymentMode()
    {
        btnCheck.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 270
        
        txtAmount.isHidden = false
        
        strGlobalPaymentMode = "Check"
        
    }
    func creditCardPaymentMode()
    {
        btnCreditCard.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        
        txtAmount.isHidden = false
        strGlobalPaymentMode = "CreditCard"
    }
    func noBillPaymentMode()
    {
        btnBill.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        txtAmount.isHidden = true
        strGlobalPaymentMode = "Bill"
    }
    func autoChargeCustomerPaymentMode()
    {
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        
        txtAmount.isHidden = true
        strGlobalPaymentMode = "AutoChargeCustomer"
    }
    func paymentPendingPaymentMode()
    {
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnNoCharge.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        
        txtAmount.isHidden = true
        strGlobalPaymentMode = "PaymentPending"
    }
    func noChargePaymentMode()
    {
        btnNoCharge.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnAutoChargeCustomer.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCash.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCheck.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnCreditCard.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnPaymentPending.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        btnBill.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        
        const_ViewCheck_H.constant = 0
        txtAmount.isHidden = true
        strGlobalPaymentMode = "NoCharge"
    }
    func checkImage() -> Bool
    {
        
        let chkImage = nsud.bool(forKey: "isCompulsoryAfterImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "After"))
            
            if arryOfData.count > 0
            {
                return false
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    func getPricingDesc(strRecommendationCodeId : String) -> String {
        
        var strPricingDesc = ""
        
        let arrayRecommendationMaster = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        for item in arrayRecommendationMaster
        {
            if("\((item as! NSDictionary).value(forKey: "CodeMasterId") ?? "")" == strRecommendationCodeId)
            {
                
                strPricingDesc = "\((item as! NSDictionary).value(forKey: "PricingDescription") ?? "")"
                
                break
            }
        }
        
        return strPricingDesc
        
    }
    
    @objc func action_ProblemImgView(_ sender: UIButton){

        let indexx = Int(sender.title(for: .normal)!)
        
        let objProblemIdentification = arrOfProblemIdentification[indexx!] as! NSManagedObject

        // Creating Dynamic Problem Images View after txtviewRecommendation
        
        let strProblemIdTemp = "\(objProblemIdentification.value(forKey: "problemIdentificationId") ?? " ")"
        let strMobileProblemIdTemp = "\(objProblemIdentification.value(forKey: "mobileId") ?? " ")"
        
        var arrOfProblemImagesTemp = NSArray()
        
        if strProblemIdTemp.count > 0 {
            
            arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdTemp))
            
        }else{
            
            arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdTemp))
            
        }
        
        
        if arrOfProblemImagesTemp.count > 0 {
            
            let objTemp = arrOfProblemImagesTemp[sender.tag] as! NSManagedObject
            
            let image: UIImage = UIImage(named: "NoImage.jpg")!
            var imageView = UIImageView(image: image)
            
            let strImagePath = objTemp.value(forKey: "woImagePath")
            
            let image1: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
            
            if image1 != nil  {
                
                imageView = UIImageView(image: image1)
                
            }else {
                
                
            }
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
                    testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }
                
        
    }
    
    @IBAction func action_MainGraphPreview(_ sender: Any) {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
        testController!.img = mainGraphImageView.image!
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    @IBAction func action_RefreshWorkOrder(_ sender: Any) {
        refreshWorkOrderStatus(view: self, strWOID: strWoId as String)
    }
    
}
// MARK: -------------------------- Extensions ---------------------------


// MARK: ------------- Signature Delegate --------------

extension FinalizeReport_WDOVC : SignatureViewDelegate{
    func imageFromSignatureView(image: UIImage)
    {
        if signatureType == "customer"
        {
            
            self.imgViewCustomerSignature.image = image
            strCustomerSign = "\\Documents\\UploadImages\\TechnicianSign_" + "\(strWoId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
        else if signatureType == "technician"
        {
            
            self.imgViewTechnicianSignature.image = image
            strTechnicianSign = "\\Documents\\UploadImages\\TechnicianSign_" + "\(strWoId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
        
    }
}
// MARK: --------------------- DatePicker  Delegate --------------

extension FinalizeReport_WDOVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        yesEditedSomething = true
        btnExpirationDate.setTitle(strDate, for: .normal)
        //strFromTimeNonBillable = strDate
        
        
        
    }
    
    
}

// MARK: --------------------- ImageView Delegate --------------


extension FinalizeReport_WDOVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            if chkFrontImage
            {
                strCheckFrontImage = "\\Documents\\UploadImages\\Img"  + "CheckFrontImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckFrontImage, image: pickedImage)
            }
            else
            {
                strCheckBackImage = "\\Documents\\UploadImages\\Img"  + "CheckBackImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckBackImage, image: pickedImage)
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: --------------------- TableView Delegate --------------



extension FinalizeReport_WDOVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tblProblemIdentification)
        {
            return arrOfProblemIdentification.count
        }
        if(tableView == tblPricing)
        {
            return arrOfPricing.count
        }
        else if(tableView == tblViewGeneralNotes)
        {
            return arrOfGeneralNotes.count
        }
        else if(tableView == tblViewDiscalimer)
        {
            return arrOfDiclaimer.count
        }
        else
        {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if(tableView == tblProblemIdentification)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProblemIdentificationCell") as! ProblemIdentificationCell
            
            for view in cell.viewProblemImages.subviews {
                view.removeFromSuperview()
            }
            
            let objProblemIdentification = arrOfProblemIdentification[indexPath.row] as! NSManagedObject
            
            if objProblemIdentification.value(forKey: "isBuildingPermit") as! Bool == true
            {
                let mainString = "Building Permit: Yes"
                let attributedWithTextColor: NSAttributedString = mainString.attributedStringWithColor(["Yes"], color: cell.lblLeadTest.textColor)

                cell.lblBuildingPermit.text = ""
                cell.lblBuildingPermit.attributedText = attributedWithTextColor
        
            }
            else
            {
                cell.lblBuildingPermit.text = ""
                let mainString = "Building Permit: No"
                let attributedWithTextColor: NSAttributedString = mainString.attributedStringWithColor(["No"], color: cell.lblLeadTest.textColor)

                cell.lblBuildingPermit.attributedText = attributedWithTextColor
              
                
            }
            
            cell.lblIssueCode.text = "\(objProblemIdentification.value(forKey: "issuesCode") ?? " ")"
            
            cell.lblRecommendationCode.text =  "\(objProblemIdentification.value(forKey: "recommendationCode") ?? " ")"
            
            cell.lblSubsectionCode.text =  "\(objProblemIdentification.value(forKey: "subsectionCode") ?? " ")"
            
            cell.lblInfastation.text =  "\(objProblemIdentification.value(forKey: "infestation") ?? " ")"
            
            cell.lblInfastationTitle.text = "Infestation:"
            cell.const_LeadTest_Leading.constant = 20
            
            let strInfestation = "\(objProblemIdentification.value(forKey: "infestation") ?? "")"
            var isInfestation = true
            
            if strInfestation == "1" {
                
                cell.lblInfastation.text = "Section I"
                
            }else if strInfestation == "2" {
                
                cell.lblInfastation.text = "Section II"
                //cell.const_LeadTest_Leading.constant = -3

            }else if strInfestation == "3" {
                
                cell.lblInfastation.text = "Further Inspection"
                //cell.const_LeadTest_Leading.constant = -1
                
            }else if strInfestation == "4" {
                
                cell.lblInfastation.text = "Non-Sectioned Finding"
                //cell.const_LeadTest_Leading.constant = -1
                
            }else {
                
                cell.lblInfastation.text = ""
                cell.lblInfastationTitle.text = ""
                cell.const_LeadTest_Leading.constant = -7
                isInfestation = false
                
            }
            
            let isLeadTestLocal = "\(objProblemIdentification.value(forKey: "isLeadTest") ?? "")"
            
            if isLeadTestLocal == "Yes" {
                
                cell.lblLeadTest.text = "Yes"
                
            }else if isLeadTestLocal == "No"{
                
                cell.lblLeadTest.text = "No"
                
            }else if isLeadTestLocal == "FurtherInspection"{
                
                //FurtherInspection
                cell.lblLeadTest.text = "Further Inspection"

            }else{
                
                cell.lblLeadTest.text = " "
                
            }
            
            cell.lblLeadTestTitle.text = "Lead Test:"
            
            cell.lblViewBelowInfestation.isHidden = false
            cell.const_FindingFilling_VerticalUp.constant = 10
            
            if ("\(lblYearOfStructure.text!)" == "Unknown") {
                
                
                
            }else if (lblYearOfStructure.text!.count > 0){
                
                let yearStructure = Int(lblYearOfStructure.text!)
                
                if yearStructure == nil {
                    
                }else if yearStructure! <= 1977 {
                    
                    
                }else{
                    
                    cell.lblLeadTestTitle.text = ""
                    cell.lblLeadTest.text = ""
                    
                    if !isInfestation {
                        
                        cell.lblViewBelowInfestation.isHidden = true
                        cell.const_FindingFilling_VerticalUp.constant = -50
                        
                        cell.lblViewBelowInfestation.isHidden = false
                        cell.const_FindingFilling_VerticalUp.constant = 10
                        
                    }else{
                        
                        cell.lblViewBelowInfestation.isHidden = false
                        cell.const_FindingFilling_VerticalUp.constant = 10
                        
                    }
                    
                }
                
            }else{
                
                cell.lblLeadTestTitle.text = ""
                cell.lblLeadTest.text = ""
                
                if !isInfestation {
                    
                    cell.lblViewBelowInfestation.isHidden = true
                    cell.const_FindingFilling_VerticalUp.constant = -50
                    
                    cell.lblViewBelowInfestation.isHidden = false
                    cell.const_FindingFilling_VerticalUp.constant = 10
                    
                }else{
                    
                    cell.lblViewBelowInfestation.isHidden = false
                    cell.const_FindingFilling_VerticalUp.constant = 10
                    
                }
                
            }

            //cell.lblFinding.attributedText = getAttributedHtmlStringUnicode(strText: "\(objProblemIdentification.value(forKey: "finding") ?? " ")")
            
            cell.txtviewRecommendation.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: "\(objProblemIdentification.value(forKey: "recommendation") ?? " ")"), strMinFont: fontMin)
            
            
            
            //setColorBorderForView(item: cell.txtviewRecommendation)
            
            cell.lblRecommendationFilling.text = "\(objProblemIdentification.value(forKey: "recommendationFillIn") ?? "")"
            cell.lblFindingFilling.text = "\(objProblemIdentification.value(forKey: "findingFillIn") ?? "")"
            
            //
            
            let strReplacedStringFinding = replaceTags(strFillingLocal: "\(cell.lblFindingFilling.text!)", strFindingLocal: "\(objProblemIdentification.value(forKey: "finding") ?? " ")")

            cell.txtviewFinding.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedStringFinding), strMinFont: fontMin)
            
            
            
            let strReplacedStringRecommendation = replaceTags(strFillingLocal: "\(cell.lblRecommendationFilling.text!)", strFindingLocal: "\(objProblemIdentification.value(forKey: "recommendation") ?? " ")")

            cell.txtviewRecommendation.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedStringRecommendation), strMinFont: fontMin)
            
            

            var strshow = ""
            if("\(objProblemIdentification.value(forKey: "isLeadTest") ?? "")".lowercased() == "true" || "\(objProblemIdentification.value(forKey: "isLeadTest") ?? "")" == "1"  || "\(objProblemIdentification.value(forKey: "isLeadTest") ?? "")".lowercased() == "yes"){
                strshow = " LEAD TEST REQUIRED"
            }
            if("\(objProblemIdentification.value(forKey: "isBuildingPermit") ?? "")".lowercased() == "true" || "\(objProblemIdentification.value(forKey: "isBuildingPermit") ?? "")" == "1"){
                strshow = strshow + " BUILDING PERMIT REQUIRED"
            }
            if(strshow != ""){
               
                let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
                let boldString = NSMutableAttributedString(string: strshow, attributes:attrs)
                let atributed = getAttributedHtmlStringUnicode(strText: " " + strReplacedStringRecommendation).mutableCopy() as! NSMutableAttributedString
                atributed.append(boldString)
                cell.txtviewRecommendation.attributedText  = attributedTextFontUpdate(str: atributed, strMinFont: fontMin)
                
                
                
                
                
            }
            // Creating Dynamic Problem Images View after txtviewRecommendation
            
            let strProblemIdTemp = "\(objProblemIdentification.value(forKey: "problemIdentificationId") ?? " ")"
            let strMobileProblemIdTemp = "\(objProblemIdentification.value(forKey: "mobileId") ?? " ")"

            var arrOfProblemImagesTemp = NSArray()
            
            if strProblemIdTemp.count > 0 {
                
                arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdTemp))
                
            }else{
                
                arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdTemp))
                
            }
            
            var yAxis = CGFloat(696.0)//cell.txtviewRecommendation.frame.maxY + 8
            var imgViewLocal = UIButton()
            var lblCaptionLocal = UILabel()
            var lblDescriptionLocal = UITextView()
            
            cell.viewProblemImages.backgroundColor = .clear
            //cell.viewProblemImages.frame = CGRect(x: 0, y: yAxis, width: tblProblemIdentification.frame.size.width, height: tblProblemIdentification.rowHeight-yAxis)
            
            cell.const_ViewProblemImages_H.constant = CGFloat(arrOfProblemImagesTemp.count * 210) + yAxis

            yAxis = 0.0
            
            if arrOfProblemImagesTemp.count > 0 {
                
                imgViewLocal = UIButton()
                lblCaptionLocal = UILabel()
                lblDescriptionLocal = UITextView()
                
                for k1 in 0 ..< arrOfProblemImagesTemp.count {
                    
                    imgViewLocal = UIButton()
                    lblCaptionLocal = UILabel()
                    lblDescriptionLocal = UITextView()
                    
                    let objTemp = arrOfProblemImagesTemp[k1] as! NSManagedObject

                    imgViewLocal = UIButton(frame: CGRect(x: 20, y: yAxis, width: 200, height: 200))
                    
                    lblCaptionLocal = UILabel(frame: CGRect(x: 228, y: yAxis, width: tableView.frame.width-248, height: 50))
                    
                    lblCaptionLocal.font = UIFont.boldSystemFont(ofSize: 20)

                    lblDescriptionLocal = UITextView(frame: CGRect(x: 228, y: yAxis+58, width: tableView.frame.width-248, height: 140))

                    lblDescriptionLocal.font = UIFont.systemFont(ofSize: 18)
                    lblCaptionLocal.text = ""
                    lblDescriptionLocal.text = ""
                        
                    lblCaptionLocal.text = (objTemp.value(forKey: "imageCaption") as! String).count>0 ? "" + (objTemp.value(forKey: "imageCaption") as! String) : "Caption :"
                    lblDescriptionLocal.text = (objTemp.value(forKey: "imageDescription") as! String).count>0 ? "" + (objTemp.value(forKey: "imageDescription") as! String) : "Description :"
                    
                    let strImagePath = objTemp.value(forKey: "woImagePath") as! String
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                    
                    let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                    
                    let imageTemp: UIImage = UIImage(named: "NoImage.jpg")!
                    imgViewLocal.setImage(imageTemp, for: .normal)
                    
                    if isImageExists!  {
                        
                        //imgViewLocal.image = image
                        imgViewLocal.setImage(image, for: .normal)
                        
                    }else {
                        
                        
                        /*let defsLogindDetail = UserDefaults.standard
                        
                        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                        
                        var strURL = String()
                        
                        if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                            
                            strURL = "\(value)"
                            
                        }
                        
                        strURL = strURL + "\(strImagePath)"
                        
                        strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                        
                        strURL = replaceBackSlasheFromUrl(strUrl: strURL)
                        
                        let image: UIImage = UIImage(named: "NoImage.jpg")!
                        
                        //imgViewLocal.image = image
                        imgViewLocal.setImage(image, for: .normal)
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            let url = URL(string:strURL)
                            let data = try? Data(contentsOf: url!)
                            
                            if data != nil && (data?.count)! > 0 {
                                
                                let image: UIImage = UIImage(data: data!)!
                                
                                DispatchQueue.main.async {
                                    
                                    saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                    
                                    //imgViewLocal.image = image
                                    imgViewLocal.setImage(image, for: .normal)
                                    
                                }}
                            
                        }
                         */
                        
                    }
                    
                    imgViewLocal.tag = k1
                    
                    let strIndex = "\(indexPath.row)"
                    
                    imgViewLocal.setTitle(strIndex, for: .normal)
                    
                    imgViewLocal.setTitleColor(UIColor.clear, for: .normal)
                    
                    imgViewLocal.addTarget(self, action: #selector(FinalizeReport_WDOVC.action_ProblemImgView(_:)), for: .touchUpInside)
                    
                    cell.viewProblemImages.addSubview(imgViewLocal)
                    cell.viewProblemImages.addSubview(lblCaptionLocal)
                    cell.viewProblemImages.addSubview(lblDescriptionLocal)
                    lblCaptionLocal.isUserInteractionEnabled = false
                    lblDescriptionLocal.isUserInteractionEnabled = false

                    yAxis = yAxis + 210

                }
                
                arrOfProblemImagesTemp = NSArray()
                
            }else{
                
                let imgViewLocal = UIButton(frame: CGRect(x: 20, y: yAxis, width: 200, height: 200))
                
                imgViewLocal.tag = 0
                
                let strIndex = "\(indexPath.row)"
                
                imgViewLocal.setTitle(strIndex, for: .normal)
                
                imgViewLocal.setTitleColor(UIColor.clear, for: .normal)
                
                imgViewLocal.addTarget(self, action: #selector(FinalizeReport_WDOVC.action_ProblemImgView(_:)), for: .touchUpInside)
                
                cell.viewProblemImages.addSubview(imgViewLocal)
                
            }
            
            arrOfProblemImagesTemp = NSArray()
            yAxis = CGFloat(0.0)

            return cell
            
        }
        else if(tableView == tblViewGeneralNotes)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralNotesFinalizeReportCell") as! GeneralNotesFinalizeReportCell
            
            let objArea = arrOfGeneralNotes[indexPath.row] as! NSManagedObject
            
            cell.lblTitle.text = "Title: \(objArea.value(forKey: "title") ?? "")"
            //cell.lblDescription.text = "Description: \(objArea.value(forKey: "generalNoteDescription") ?? "")"
            cell.lblFillin.text = "Fillin: \(objArea.value(forKey: "fillIn") ?? "")"
            if cell.lblFillin.text == "Fillin: <null>" {
                
                cell.lblFillin.text = "Fillin: "
                
            }
            let strReplacedString = replaceTags(strFillingLocal: "Fillin: \(objArea.value(forKey: "fillIn") ?? "")", strFindingLocal: "\(objArea.value(forKey: "generalNoteDescription")!)")
            
            cell.txtViewGeneralNoteDesc.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
            
            
            
            return cell
            
        }else if(tableView == tblViewDiscalimer){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisclaimerCell") as! DisclaimerCell
            
            let objArea = arrOfDiclaimer[indexPath.row] as! NSManagedObject
            
            cell.lblTitle.text = "\(objArea.value(forKey: "disclaimerName") ?? "")"
            cell.lblDescription.text = "\(objArea.value(forKey: "disclaimerDescription") ?? "")"
            
            let strReplacedString = replaceTags(strFillingLocal: "\(objArea.value(forKey: "fillIn")!)", strFindingLocal: "\(objArea.value(forKey: "disclaimerDescription")!)")
            
            cell.lblDescription.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
            
            
            
            cell.lblWhere.text = "\(objArea.value(forKey: "fillIn") ?? "")"
            
            cell.selectionStyle = .none
            return cell
            
        }
        else //if(tableView == tblProblemIdentification)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PricingCellFinalizeReport") as! PricingCellFinalizeReport
            cell.viewBidOnrequest.isHidden = true
            cell.stkViewSubTotal.isHidden = true
            cell.stkViewDiscount.isHidden = true
            cell.stkViewTotal.isHidden = true
            cell.lblTotalName.textAlignment = .left
            cell.lblTotal.textAlignment = .left
            
            let objProblemIdentification = arrOfProblemIdentification[indexPath.row] as! NSManagedObject
            
            cell.lblIssueCode.text = "\(objProblemIdentification.value(forKey: "issuesCode") ?? " ")"
            
            //cell.lblRecommendationCode.text =  "\(objProblemIdentification.value(forKey: "recommendationCode") ?? " ")"
            
            //cell.lblSubsectionCode.text =  "\(objProblemIdentification.value(forKey: "subsectionCode") ?? " ")"
            
            let strRecommnedationCodeIdLocal = "\(objProblemIdentification.value(forKey: "recommendationCodeId") ?? " ")"//recommendationCodeId
            
    
            //cell.lblRecommendation.attributedText = getAttributedHtmlStringUnicode(strText: getPricingDesc(strRecommendationCodeId: strRecommnedationCodeIdLocal))
            
            //Nilind
            let strpricingdes = "\(objProblemIdentification.value(forKey: "proposalDescription") ?? " ")"
            var strReplacedStringRecommendation = replaceTags(strFillingLocal: "\(objProblemIdentification.value(forKey: "recommendationFillIn") ?? " ")", strFindingLocal: strpricingdes == "" ? getPricingDesc(strRecommendationCodeId: strRecommnedationCodeIdLocal) : strpricingdes)

            cell.lblRecommendation.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedStringRecommendation), strMinFont: fontMin)
            
            
            
            
            
            //setColorBorderForView(item: cell.lblRecommendation)
            
            // Pricing Table Se data show krna hai
            
            let objProblemIdentificationPricing = arrOfPricing[indexPath.row] as! NSManagedObject
            
            // isDiscount
            
            let isDiscountt = objProblemIdentificationPricing.value(forKey: "isDiscount") as! Bool
            
            if isDiscountt {
                cell.lblSubTotalName.text = "SubTotal"
//                cell.lblSubTotalName.isHidden = false
//                cell.lblSubTotal.isHidden = false
//                cell.lblDiscountName.isHidden = false
//                cell.lblDiscount.isHidden = false
//                cell.lblTotalName.isHidden = false
//                cell.lblTotal.isHidden = false
                
                cell.stkViewSubTotal.isHidden = false
                cell.stkViewDiscount.isHidden = false
                cell.stkViewTotal.isHidden = false
                
                
                
                cell.lblSubTotal.text = "$ \(objProblemIdentificationPricing.value(forKey: "subTotal") ?? " ")"
                cell.lblDiscount.text = "$ \(objProblemIdentificationPricing.value(forKey: "discount") ?? " ")"
                cell.lblTotal.text = "$ \(objProblemIdentificationPricing.value(forKey: "total") ?? " ")"
                
                if let isBidOnRequest = objProblemIdentificationPricing.value(forKey: "isBidOnRequest")
                {
                    print(isBidOnRequest)
                    if(objProblemIdentificationPricing.value(forKey: "isBidOnRequest") as! Bool == true)
                    {
                        
                        cell.lblTotalName.text = "Total($)"
                        
//                        cell.lblDiscountName.isHidden = true
//                        cell.lblDiscount.isHidden = true
//                        cell.lblSubTotalName.isHidden = true
//                        cell.lblSubTotal.isHidden = true
//                        cell.lblTotalName.isHidden = true
//                        cell.lblTotal.isHidden = true
                        cell.stkViewSubTotal.isHidden = true
                        cell.stkViewDiscount.isHidden = true
                        cell.stkViewTotal.isHidden = true
                        
                        cell.viewBidOnrequest.isHidden = false

//
//                        let lblHeader = UILabel(frame: CGRect(x: tableView.frame.width-160, y: 195, width: 140, height: 50))
//                        lblHeader.backgroundColor = UIColor.clear
//                        lblHeader.text = "Bid On Request"
//                        lblHeader.font = UIFont.systemFont(ofSize: 20)
//
//                        let btnBidOnRequestLocal = UIButton(frame: CGRect(x: lblHeader.frame.origin.x-58, y: 195, width: 50, height: 50))
//                        btnBidOnRequestLocal.setImage(UIImage(named: "checked.png"), for: .normal)
//
//                        cell.addSubview(lblHeader)
//                        cell.addSubview(btnBidOnRequestLocal)
//
                        
                        let isNoBidGiven = "\(objProblemIdentificationPricing.value(forKey: "isNoBidGiven") ?? "")"
                        if(isBidOnRequest as! Bool && (isNoBidGiven.lowercased() == "true" || isNoBidGiven == "1")){
                            strReplacedStringRecommendation =  strReplacedStringRecommendation.replacingOccurrences(of: "NO BID GIVEN", with: "")

                                let noBidGivenboldText  = " NO BID GIVEN"
                                let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
                                let boldString = NSMutableAttributedString(string: noBidGivenboldText, attributes:attrs)
                                let atributed = getAttributedHtmlStringUnicode(strText: strReplacedStringRecommendation).mutableCopy() as! NSMutableAttributedString
                                atributed.append(boldString)
                                cell.lblRecommendation.attributedText  = attributedTextFontUpdate(str: atributed, strMinFont: fontMin)
                            
                            
                            }
                        
                        
                    }

                }
                
            }else{
                
                cell.lblSubTotalName.text = "Total"
//                cell.lblSubTotalName.isHidden = true
//                cell.lblSubTotal.isHidden = true
//                cell.lblDiscountName.isHidden = true
//                cell.lblDiscount.isHidden = true
//
//                cell.lblTotalName.isHidden = false
//                cell.lblTotal.isHidden = false
                
                cell.stkViewSubTotal.isHidden = true
                cell.stkViewDiscount.isHidden = true
                cell.stkViewTotal.isHidden = false
                cell.lblTotalName.textAlignment = .right
                cell.lblTotal.textAlignment = .right
                cell.lblSubTotal.text = "$ \(objProblemIdentificationPricing.value(forKey: "subTotal") ?? " ")"
                cell.lblDiscount.text = "$ \(objProblemIdentificationPricing.value(forKey: "discount") ?? " ")"
                cell.lblTotal.text = "$ \(objProblemIdentificationPricing.value(forKey: "total") ?? " ")"
                
                if let isBidOnRequest = objProblemIdentificationPricing.value(forKey: "isBidOnRequest")
                {
                    print(isBidOnRequest)
                    
                    if(objProblemIdentificationPricing.value(forKey: "isBidOnRequest") as! Bool == true)
                    {
                        
                        cell.lblTotalName.text = "Total($)"
//                        cell.lblDiscountName.isHidden = true
//                        cell.lblDiscount.isHidden = true
//                        cell.lblSubTotalName.isHidden = true
//                        cell.lblSubTotal.isHidden = true
//                        cell.lblTotalName.isHidden = true
//                        cell.lblTotal.isHidden = true
                        cell.stkViewSubTotal.isHidden = true
                        cell.stkViewDiscount.isHidden = true
                        cell.stkViewTotal.isHidden = true
                        
                        cell.viewBidOnrequest.isHidden = false
//                        let lblHeader = UILabel(frame: CGRect(x: tableView.frame.width-160, y: 195, width: 140, height: 50))
//                        lblHeader.backgroundColor = UIColor.clear
//                        lblHeader.text = "Bid On Request"
//                        lblHeader.font = UIFont.systemFont(ofSize: 20)
//                        let btnBidOnRequestLocal = UIButton(frame: CGRect(x: lblHeader.frame.origin.x-58, y: 195, width: 50, height: 50))
//                        btnBidOnRequestLocal.setImage(UIImage(named: "checked.png"), for: .normal)
//
//                        cell.addSubview(lblHeader)
//                        cell.addSubview(btnBidOnRequestLocal)

                        let isNoBidGiven = "\(objProblemIdentificationPricing.value(forKey: "isNoBidGiven") ?? "")"
                        if(isBidOnRequest as! Bool && (isNoBidGiven.lowercased() == "true" || isNoBidGiven == "1")){
                                let noBidGivenboldText  = " NO BID GIVEN"
                                strReplacedStringRecommendation =  strReplacedStringRecommendation.replacingOccurrences(of: "NO BID GIVEN", with: "")

                                let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
                                let boldString = NSMutableAttributedString(string: noBidGivenboldText, attributes:attrs)
                                let atributed = getAttributedHtmlStringUnicode(strText: strReplacedStringRecommendation).mutableCopy() as! NSMutableAttributedString
                                atributed.append(boldString)
                                cell.lblRecommendation.attributedText  = attributedTextFontUpdate(str: atributed, strMinFont: fontMin)
                            
                            
                            }
                        
                    }
                }
                
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == tblProblemIdentification)
        {
            
            let objProblemIdentification = arrOfProblemIdentification[indexPath.row] as! NSManagedObject

            // Creating Dynamic Problem Images View after txtviewRecommendation
            
            let strProblemIdTemp = "\(objProblemIdentification.value(forKey: "problemIdentificationId") ?? " ")"
            let strMobileProblemIdTemp = "\(objProblemIdentification.value(forKey: "mobileId") ?? " ")"
            
            var arrOfProblemImagesTemp = NSArray()
            
            if strProblemIdTemp.count > 0 {
                
                arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdTemp))
                
            }else{
                
                arrOfProblemImagesTemp = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdTemp))
                
            }
            
            if arrOfProblemImagesTemp.count > 0 {
                
                let hghtLocal = CGFloat(arrOfProblemImagesTemp.count) * 210
                arrOfProblemImagesTemp = NSArray()

                return 702.0 + hghtLocal + 10
                
            }else{
                
                arrOfProblemImagesTemp = NSArray()

                return 645.0 + 50.0
                
            }
            

        }
        else if(tableView == tblViewGeneralNotes)
        {
            return tableView.rowHeight
        }
        else if(tableView == tblViewDiscalimer)
        {
            return UITableView.automaticDimension
        }
        return 300.0
    }
    
}

// MARK: --------------------- Text View Delegate --------------

extension FinalizeReport_WDOVC : UITextViewDelegate
{
    
    private func textViewDidChange(textView: UITextView)
    {
        const_viewServiceTerms_H.constant = 50
        
        if textView == txtViewTermsCondition
        {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            //textView.frame = newFrame;
            const_viewServiceTerms_H.constant = newFrame.size.height
            
            /* if  const_viewServiceTerms_H.constant>400
             {
             const_viewServiceTerms_H.constant = 400
             }*/
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        yesEditedSomething = true
        return true
    }
    
}
// MARK: --------------------- Text Field Delegate --------------

extension FinalizeReport_WDOVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        yesEditedSomething = true
        
        if ( textField == txtDrivingLicenseNo || textField == txtCheckNo  )
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        }
            
        else if ( textField == txtAmount  )
        {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else if ( textField == txtFldAmount  )
        {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)

            return chk
            
        }
        else
        {
            
            return true
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if ( textField == txtFldAmount  )
        {
            
          
            self.calculateTotalDue()
            
        }
        
    }
    
}

// MARK: --
// MARK: -------------RefreshStatus-------------
extension FinalizeReport_WDOVC {
   
    func refreshWorkOrderStatus(view : UIViewController , strWOID : String){
        var loader = UIAlertController()
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        view.present(loader, animated: false, completion: nil)
        if (isInternetAvailable()){
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" +  "/api/MobileToServiceAuto/GetWDOWorkorderApprovalStatusByWorkorderId?workorderId=\(strWOID)"
            
            WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self]  (response, status) in
                print(response)
                loader.dismiss(animated: false, completion: nil)
                if(status){
                    if(response.value(forKey: "data") is NSDictionary){
                        let dictData = (response.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        let ErrorMessage = "\(dictData.value(forKey: "ErrorMessage") ?? "")"
                        
                        if(dictData.value(forKey: "IsSuccess") as! Bool){
                            workOrder_DataBaseUpdate(dict: ((dictData.value(forKey: "Response") as! NSDictionary).mutableCopy() as! NSMutableDictionary), strWOID: strWOID)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorMessage, viewcontrol: view)
                        }
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: view)
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: view)
        }
    }
    
    func workOrder_DataBaseUpdate(dict : NSMutableDictionary , strWOID : String) {
       
        if(dict.count != 0){
            let dictWo = removeNullFromDict(dict: dict)
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOID))
            if(arryOfData.count != 0){

                var arrOfKeys = NSMutableArray()
                var arrOfValues = NSMutableArray()
                arrOfKeys = ["isPricingApprovalPending",
                             "wdoWorkflowStage","wdoWorkflowStatus","priceChangeReasonId" , "priceChangeNote" , "isVerbalApproval" , "verbalApprovalBy"]
                /*arrOfValues = ["\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeNote") ?? "")",
                               "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")",
                               "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")"
                ]*/
                
                arrOfValues = ["\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")" == "<null>"  ? "false" : "\(dictWo.value(forKey: "IsPricingApprovalPending") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "WdoWorkflowStage") ?? "")",
                               "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "WdoWorkflowStatus") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "PriceChangeReasonId") ?? "")",
                               "\(dictWo.value(forKey: "PriceChangeNote") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "PriceChangeNote") ?? "")",
                               "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")" == "<null>"  ? "false" : "\(dictWo.value(forKey: "IsVerbalApproval") ?? "")",
                               "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")" == "<null>"  ? "" : "\(dictWo.value(forKey: "VerbalApprovalBy") ?? "")"
                ]
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWOID), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess
                {
                    
                }
                // AGain Fetch WorkOrder Details
                
                let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
                
                if arryOfData.count > 0 {
                    
                    objWorkorderDetail = arryOfData[0] as! NSManagedObject
                    
                }
                
            }
        }
    }
}
extension String {
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }

        guard let characterSpacing = characterSpacing else {return attributedString}

        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

        return attributedString
    }
}
