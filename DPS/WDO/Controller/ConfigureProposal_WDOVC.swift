//
//  ConfigureProposal_WDOVC.swift
//
//  Saavan Changes 2020
//  Created by Rakesh Jain on 13/09/19.
//  Copyright © 2099 Saavan. All rights reserved.
//  2021

import UIKit
import CoreData
class ConfigureProposalCell:UITableViewCell
{
    // Create Bid
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblIssueCode: UILabel!
    @IBOutlet weak var lblRecommendationCode: UILabel!
    @IBOutlet weak var lblSubsectoinCode: UILabel!
    
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtHrs: ACFloatingTextField!
    @IBOutlet weak var txtMaterials: ACFloatingTextField!
    @IBOutlet weak var txtSubfee: ACFloatingTextField!
    @IBOutlet weak var txtLinearFtSoilTreatment: ACFloatingTextField!
    @IBOutlet weak var txtFtDrilledConcrete: ACFloatingTextField!
    @IBOutlet weak var txtCubicFt: ACFloatingTextField!
    

    @IBOutlet weak var lblTitleSubtotal: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var btnDiscount: UIButton!
    @IBOutlet weak var txtDiscount: ACFloatingTextField!
    @IBOutlet weak var lblTitleTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btnAddToAgreement: UIButton!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var txtSubTotal: ACFloatingTextField!
    @IBOutlet weak var txtTotal: ACFloatingTextField!
    @IBOutlet weak var btnEditProposalDesc: UIButton!
    @IBOutlet weak var btnBidOnRequest: UIButton!

    @IBOutlet weak var btnNoBidGiven: UIButton!
    @IBOutlet weak var viewNoBidGiven: UIView!

}

class ConfigureProposal_WDOVC: UIViewController {
    
    // Create Bid
    
    @IBOutlet weak var tblviewConfigureProposal: UITableView!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var txtHourlyRateCreateBid: ACFloatingTextField!
    @IBOutlet weak var txtMaterialMultiplierCreateBid: ACFloatingTextField!
    @IBOutlet weak var txtSubFeeMultiplierCreateBid: ACFloatingTextField!
    @IBOutlet weak var txtLinearFtSoilTreatmentCreateBid: ACFloatingTextField!
    @IBOutlet weak var txtLinearFtDrilledConcreteCreateBid: ACFloatingTextField!
    @IBOutlet weak var txtCubicFtRateCreateBid: ACFloatingTextField!
   
    @IBOutlet weak var txtPriceModificationReason: ACFloatingTextField!

    
    @IBOutlet weak var txt_other_PriceModificationReason: ACFloatingTextField!
    @IBOutlet weak var width_txt_other_PriceModificationReason: NSLayoutConstraint!

    
    @IBOutlet weak var lbl_HourlyRateCreateBid: UILabel!
    @IBOutlet weak var lbl_MaterialMultiplierCreateBid: UILabel!
    @IBOutlet weak var lbl_SubFeeMultiplierCreateBid: UILabel!
    @IBOutlet weak var lbl_LinearFtSoilTreatmentCreateBid: UILabel!
    @IBOutlet weak var lbl_LinearFtDrilledConcreteCreateBid: UILabel!
    @IBOutlet weak var lbl_CubicFtRateCreateBid: UILabel!
    @IBOutlet weak var btnEditRate: UIButton!

    @IBOutlet weak var lbl_HourlyRateCreateBid_static: UILabel!
    @IBOutlet weak var lbl_MaterialMultiplierCreateBid_static: UILabel!
    @IBOutlet weak var lbl_SubFeeMultiplierCreateBid_static: UILabel!
    @IBOutlet weak var lbl_LinearFtSoilTreatmentCreateBid_static: UILabel!
    @IBOutlet weak var lbl_LinearFtDrilledConcreteCreateBid_static: UILabel!
    @IBOutlet weak var lbl_CubicFtRateCreateBid_static: UILabel!
    
    @IBOutlet weak var heightViewCreateBid_EditMode: NSLayoutConstraint!
    @IBOutlet weak var heightViewCreateBid_ViewMode: NSLayoutConstraint!

    // Cover Letter
    
    @IBOutlet weak var btnCoverLetter: UIButton!
    
    // Introduction
    
    @IBOutlet weak var btnIntroduction: UIButton!
    @IBOutlet weak var txtviewIntroduction: UITextView!
    
    // Sales Marketing Content
    
    @IBOutlet weak var btnSalesMarketingContent: UIButton!
    
    // Service Months
    
    @IBOutlet weak var btnShowHideServiceMonths: UIButton!
    @IBOutlet weak var btnJanuary: UIButton!
    @IBOutlet weak var btnFebruary: UIButton!
    @IBOutlet weak var btnMarch: UIButton!
    @IBOutlet weak var btnApril: UIButton!
    @IBOutlet weak var btnMay: UIButton!
    @IBOutlet weak var btnJune: UIButton!
    @IBOutlet weak var btnJuly: UIButton!
    @IBOutlet weak var btnAugust: UIButton!
    @IBOutlet weak var btnSeptember: UIButton!
    @IBOutlet weak var btnOctober: UIButton!
    @IBOutlet weak var btnNovember: UIButton!
    @IBOutlet weak var btnDecember: UIButton!
    
    // Other
    
    @IBOutlet weak var btnShowHideOther: UIButton!
    @IBOutlet weak var txtviewAdditionalNotes: UITextView!
    @IBOutlet weak var btnAgreementValidFor: UIButton!
    @IBOutlet weak var txtValidFor: ACFloatingTextField!
    
    
    // Terms Of Service
    
    @IBOutlet weak var btnTermsOfService: UIButton!
    @IBOutlet weak var txtviewTermsOfService: UITextView!
    
    
    // Terms and Conditions
    
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    
    
    // Constant outlets
    @IBOutlet weak var hghtConstViewContainerCreateBid: NSLayoutConstraint!
    @IBOutlet weak var hghtConstTblView: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerCoverLetter: NSLayoutConstraint!
    @IBOutlet weak var hghtconstViewContainerIntroduction: NSLayoutConstraint!
    @IBOutlet weak var hghtconstViewContainerSalesMarketing: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerServiceMonths: NSLayoutConstraint!
    @IBOutlet weak var hghtConstViewContainerOther: NSLayoutConstraint!
    @IBOutlet weak var hghtconstViewContainerTermsOfService: NSLayoutConstraint!
    @IBOutlet weak var hghtconstViewContainerTermsCondition: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewContainerCreateBid: CardView!
    @IBOutlet weak var viewConainerCoverSheet: CardView!
    @IBOutlet weak var viewContainerIntroduction: CardView!
    @IBOutlet weak var viewContainerMarketingContent: CardView!
    @IBOutlet weak var viewContainerServiceMonths: CardView!
    @IBOutlet weak var viewConainerOther: CardView!
    @IBOutlet weak var viewContainerTermsOfService: CardView!
    @IBOutlet weak var viewContainerTermsAndConditions: CardView!
    
    @IBOutlet weak var btnEditIntroductionLetter: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    // TIP Warranty
    @IBOutlet weak var btnYes_TipWarranty: UIButton!
    @IBOutlet weak var btnNoTipWarranty: UIButton!
    // TIP Warranty Type
    @IBOutlet weak var btnTipWarrantyType: UIButton!
    @IBOutlet weak var txtMonthlyAmount: ACFloatingTextField!
    @IBOutlet weak var hghtConstTipYesNoView: NSLayoutConstraint!
    @IBOutlet weak var hghtConstTipWarrantySelectionView: NSLayoutConstraint!
    @IBOutlet weak var hghtConstLeadInspectionFee: NSLayoutConstraint!
    @IBOutlet weak var txtLeadInspectionFee: ACFloatingTextField!

    // variables
    var isTipWarranty = true
    var dictOfTipWarranty = NSDictionary()
    var strTipMasterName = String()
    var strTipMasterId = String()
    var isEditedTIPWarranty = false
    
    // array
    fileprivate var arrayCoverLetter = NSMutableArray()
    fileprivate var arrayIntroduction = NSMutableArray()
    fileprivate var arrayMarketingContent = NSMutableArray()
    fileprivate var arrayMarketingContentMultipleSelected = NSMutableArray()
    fileprivate var arrayTermsOfService = NSMutableArray()
    fileprivate var arrayTermsAndConditions = NSMutableArray()
    fileprivate var arrayTermsAndConditionsMultipleSelected = NSMutableArray()
    fileprivate var arrayServiceMonths = NSMutableArray()
    fileprivate var arrayProblemIdentification = NSMutableArray()
    fileprivate var arrayProblemIdentificationPricing = NSMutableArray()
    fileprivate var arrayRecommendationMaster = NSMutableArray()
    fileprivate var arrayMultiplier = NSArray()
    
    // dictionary
    fileprivate var dictCoverLetter = NSDictionary()
    fileprivate var dictIntroduction = NSDictionary()
    fileprivate var dictMarketingContent = NSDictionary()
    fileprivate var dictTermsOfService = NSDictionary()
    fileprivate var dictTermsAndConditions = NSDictionary()
    fileprivate var dictLoginData = NSDictionary()
    // string
    var strLeadId = ""
    var strForProposal = ""
    @objc var strWoId = NSString ()
    fileprivate var strLeadStatusGlobal = ""
    fileprivate var strStageSysName = ""
    fileprivate var strAccountNo = ""
    fileprivate var strCompanyKey = ""
    fileprivate var strUserName = ""
    fileprivate var strEmpID = ""
    fileprivate var strEmpName = ""
    
    // managed object
    var objWorkorderDetail = NSManagedObject()
    
    // bool
    fileprivate var yesEditedSomething = false
    
    fileprivate var isIntroductionLetterEdited = false
    fileprivate var isUpdateApprovalParameter = false
    var indexOfPropsalDesc = 0
    fileprivate var isUpdatedPrice = false
    fileprivate var isUpdateCreateBid = false

    // class variable
    let global = Global()
    var IsTIPWarrantyRequired =  "true"

    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defsLogindDetail = UserDefaults.standard
        if  defsLogindDetail.value(forKey: "LoginDetails") != nil && defsLogindDetail.value(forKey: "LoginDetails") is NSDictionary
        {
            dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        }
        if dictLoginData.count != 0 {
            IsTIPWarrantyRequired = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsTIPWarrantyRequired") ?? "true")"
        }
      
        btnTipWarrantyType.setTitle(strSelectString, for: .normal)
        btnEditRate.isHidden = false

        configureUI()
        checkIfLeadInspectionFee()
        setValuesWdoInspection()
        getLetterTemplateAndTermsOfService()
        getRecommendationMaster()
        
        fetchLetterTemplateFromLocalDB()
        fetchMarketingContentFromLocalDB()
        fetchLeadDetailFromLocalDB()
        fetchMultiTermsAndConditionsFromLocalDB()
        fetchWoWdoPricingMultiplierExtSerDcs()
        fetchWoWdoProblemIdentificationExtSerDcs()
        fetchPaymentInfoFromLocalDB()
        
       
        
        lblOrderNumber.text = nsud.value(forKey: "lblName") as? String
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            strLeadStatusGlobal = "Complete";
            
        }
        print(strLeadId)
        
        let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
        let fullAccess = opprtunityAccess.status
        let limitedAccess = opprtunityAccess.opportunityLimitedAccess
         //True means not edit
        if fullAccess && limitedAccess{
            disableUserInteration()
        }
        else if(!fullAccess && limitedAccess){
            disableUserInteration()
            accessWhenOpportunityCompletwPending()
        }else{
            
        }
        
        
        //---For Edit and View Only Creat Bid
        heightViewCreateBid_ViewMode.constant = 185.0
            heightViewCreateBid_EditMode.constant = 0.0
        setUpInitialConstanOfViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if (nsud.value(forKey: "loadeDataRefreshConfigureProposalView") != nil) {
            let yesupdatedWdoInspection = nsud.bool(forKey: "loadeDataRefreshConfigureProposalView")
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "loadeDataRefreshConfigureProposalView")
                nsud.synchronize()
                
                // Reload Data if Loaded Data
                checkIfLeadInspectionFee()
                setValuesWdoInspection()
                fetchWoWdoPricingMultiplierExtSerDcs()
                fetchWoWdoProblemIdentificationExtSerDcs()
                setUpInitialConstanOfViews()

            }
        }
        
        fetchLeadDetailFromLocalDB()
       
        
        // Fwd: Agreements access for inspectors.

        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            strLeadStatusGlobal = "Complete";
            
        }
        
        
        
        let opprtunityAccess = checkOpportunityEditOrNot(status: strLeadStatusGlobal, stage: strStageSysName, woObj: self.objWorkorderDetail)
        let fullAccess = opprtunityAccess.status
        let limitedAccess = opprtunityAccess.opportunityLimitedAccess

        if fullAccess && limitedAccess{
            disableUserInteration()
        }
       else if(!fullAccess && limitedAccess){
            disableUserInteration()
            accessWhenOpportunityCompletwPending()
        }else{
            
        }
        
        
        
//        if(WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)){
//            disableUserInteration()
//        }

        
        // If From Editing Html Contents
        if (nsud.value(forKey: "EditedHtmlContents") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "EditedHtmlContents")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "EditedHtmlContents")
                nsud.synchronize()
                
                let strCodeType = "\(nsud.value(forKey: "EditorTypeOnConfigurePropsal")!)"
                
                if strCodeType == "Intro" {
                    
                    // Introduction Html Code Edited
                    
                    nsud.set("\(nsud.value(forKey: "htmlContents")!)", forKey: "htmlContentsIntroduction")
                    nsud.synchronize()
                    
                    txtviewIntroduction.attributedText = getAttributedHtmlStringUnicode(strText: "\(nsud.value(forKey: "htmlContents")!)")//htmlAttributedString(strHtmlString: "\(nsud.value(forKey: "htmlContents")!)")
                    
                    isIntroductionLetterEdited = true
                    
                }else{
                    
                    // Proposal Descriptions Html Code Edited
                    
                    yesEditedSomething = true
                    
                    let obj = arrayProblemIdentification.object(at: indexOfPropsalDesc) as! NSMutableDictionary
                    
                    obj.setValue("\(nsud.value(forKey: "htmlContents")!)", forKey: "proposalDescription")

                    arrayProblemIdentification.replaceObject(at: indexOfPropsalDesc, with: obj)
                    
                    tblviewConfigureProposal.reloadData()
                    
                }
                
            }
            
        }
        
    }
    
    
    // MARK: UIButton action
    @IBAction func action_showHideCreateBid(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            
            viewContainerCreateBid.isHidden = false
            
            var heightProblemCell = 0
            
            for k in 0 ..< arrayProblemIdentificationPricing.count {
                
                let dictData = arrayProblemIdentificationPricing[k] as! NSDictionary
                
                let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
                
                if isBidOnRequestLocal {
                    
                    heightProblemCell = heightProblemCell + 285
                    
                } else {
                    
                    heightProblemCell = heightProblemCell + 550
                    
                }
                
            }
            
            hghtConstTblView.constant = CGFloat((heightProblemCell)+40)
            
            hghtConstViewContainerCreateBid.constant = (heightViewCreateBid_EditMode.constant + heightViewCreateBid_ViewMode.constant) + hghtConstTblView.constant
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerCreateBid.isHidden = true
            hghtConstTblView.constant = 0.0
            hghtConstViewContainerCreateBid.constant = 0.0
            
        }
    }
    
    @IBAction func action_showHideCoverLetter(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewConainerCoverSheet.isHidden = false
            hghtConstViewContainerCoverLetter.constant = 85
            
        }
        else
        {// hide content
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewConainerCoverSheet.isHidden = true
            hghtConstViewContainerCoverLetter.constant = 0.0
        }
    }
    
    @IBAction func action_showHideIntroduction(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewContainerIntroduction.isHidden = false
            hghtconstViewContainerIntroduction.constant = 250
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerIntroduction.isHidden = true
            hghtconstViewContainerIntroduction.constant = 0.0
        }
    }
    
    @IBAction func action_showHideSalesMarketingContent(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewContainerMarketingContent.isHidden = false
            hghtconstViewContainerSalesMarketing.constant = 85
            
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerMarketingContent.isHidden = true
            hghtconstViewContainerSalesMarketing.constant = 0.0
            
        }
    }
    
    @IBAction func action_showHideServiceMonths(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewContainerServiceMonths.isHidden = false
            hghtConstViewContainerServiceMonths.constant = 285
            
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerServiceMonths.isHidden = true
            hghtConstViewContainerServiceMonths.constant = 0.0
            
        }
    }
    
    @IBAction func action_showHideOther(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewConainerOther.isHidden = false
            hghtConstViewContainerOther.constant = 300
            
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewConainerOther.isHidden = true
            hghtConstViewContainerOther.constant = 0.0
            
        }
    }
    
    @IBAction func action_showHideTermsOfService(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewContainerTermsOfService.isHidden = false
            hghtconstViewContainerTermsOfService.constant = 250
            
            
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerTermsOfService.isHidden = true
            hghtconstViewContainerTermsOfService.constant = 0.0
            
        }
    }
    
    @IBAction func action_showHideTermsAndCondition(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "open_arrow_ipad"))
        {//the show content
            
            sender.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewContainerTermsAndConditions.isHidden = false
            hghtconstViewContainerTermsCondition.constant = 85
        }
        else
        {// hide content
            
            sender.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewContainerTermsAndConditions.isHidden = true
            hghtconstViewContainerTermsCondition.constant = 0.0
        }
    }
    
    @IBAction func action_January(_ sender: UIButton) {
        
        yesEditedSomething = true
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("January")
        }
        else
        {
            
            arrayServiceMonths.add("January")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_February(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("February")
        }
        else
        {
            
            arrayServiceMonths.add("February")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_March(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("March")
        }
        else
        {
            
            arrayServiceMonths.add("March")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
            
        }
    }
    
    @IBAction func action_April(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("April")
        }
        else
        {
            
            arrayServiceMonths.add("April")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_May(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("May")
        }
        else
        {
            
            arrayServiceMonths.add("May")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_June(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("June")
        }
        else
        {
            
            arrayServiceMonths.add("June")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_July(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("July")
        }
        else
        {
            
            arrayServiceMonths.add("July")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_August(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("August")
        }
        else
        {
            
            arrayServiceMonths.add("August")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_September(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("September")
        }
        else
        {
            
            arrayServiceMonths.add("September")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_October(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("October")
        }
        else
        {
            
            arrayServiceMonths.add("October")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_November(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("November")
        }
        else
        {
            
            arrayServiceMonths.add("November")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_December(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            arrayServiceMonths.remove("December")
        }
        else
        {
            
            arrayServiceMonths.add("December")
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_AgreementValidFor(_ sender: UIButton) {
        
        yesEditedSomething = true
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            sender.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            txtValidFor.isHidden = true
        }
        else
        {
            txtValidFor.isHidden = false
            sender.setImage(UIImage(named: "check_ipad"), for: .normal)
        }
    }
    
    @IBAction func action_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    @IBAction func action_CoverLetter(_ sender: UIButton) {
        
        openTableViewPopUp(tag: 101, ary: arrayCoverLetter, aryselectedItem: NSMutableArray())
    }
    
    @IBAction func action_Introduction(_ sender: UIButton) {
        
        openTableViewPopUp(tag: 102, ary: arrayIntroduction, aryselectedItem: NSMutableArray())
    }
    
    @IBAction func action_MarketingContent(_ sender: UIButton) {
        
        openTableViewPopUp(tag: 103, ary: arrayMarketingContent, aryselectedItem: arrayMarketingContentMultipleSelected)
    }
    
    @IBAction func action_TermsOfService(_ sender: UIButton) {
        
        openTableViewPopUp(tag: 104, ary: arrayTermsOfService, aryselectedItem: NSMutableArray())
        
    }
    
    @IBAction func action_TermsConditions(_ sender: UIButton) {
        
        openTableViewPopUp(tag: 105, ary: arrayTermsAndConditions, aryselectedItem: arrayTermsAndConditionsMultipleSelected)
    }
    
    @IBAction func action_image(_ sender: UIButton) {
        
        self.goToGlobalmage(strType: "Before")
    }
    
    @IBAction func action_ServiceHistory(_ sender: UIButton) {
        goToServiceHistory()
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: UIButton) {
        goToCustomerSalesDocuments()
    }
    
    @IBAction func action_Graph(_ sender: UIButton) {
        
        self.goToGlobalmage(strType: "Graph")
    }
    
    @IBAction func action_NotesHistory(_ sender: UIButton) {
        
        goToNotesHistory()
    }
    
    @IBAction func action_SaveAndContinue(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            // Lead ispection fee update sales and service when assign issu by Testing team   JV-17460
           // checkIfLeadInspectionFee()
            goToAgreementAfterAlert()
        }
        else
        {
          
            if(getPriceChangeAlertMessage(isCalculateDiscount: true).count != 0 && txtPriceModificationReason.tag == 0){
                
              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "The proposed price rate is out of range. Please select the reason in the rate section", viewcontrol: self)
                
            }else if(isUpdateCreateBid){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "You have edited rates, Please save before procedding.", viewcontrol: self)
                  
            }else{
                
                if validationForAddWdoInspectionOnConfigure() {
                    
                    saveWdoInspection()
                    UpdateRateMasterID_InWO()
                    
                    if(arrayProblemIdentificationPricing.count > 0)
                    {
                    
                        saveLetterTemplateCoverLetterIntroLetterAndTermsOfService(dictCoverLetter: dictCoverLetter, dictIntroLetter: dictIntroduction, dictTermsOfService: dictTermsOfService)
                        
                        saveMarketingContent(arraySeletecMarketingContent: arrayMarketingContentMultipleSelected)
                        
                        saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
                        saveProblemIdentificationMultiplierAndProblemIdentificationAndProblemIdentificationPricing()
                        
                        updateLeadDetails()
                        
                        updatePaymentInfo()
                        
                        gotoAgreementView()
                        
                        // check Condition if User Edit Amount and If Discount is Out Of Max Allowed Range
                        
                        self.checkIfDiscountIsOutOfRange()
                        
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CreateBid, viewcontrol: self)
                    }
                    
                }
            }
        }
    }
    
    @objc func action_Discount(_ sender: UIButton){
        
        yesEditedSomething = true
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblviewConfigureProposal)
        
        let indexPath = self.tblviewConfigureProposal.indexPathForRow(at: buttonPosition)
        
        let obj = arrayProblemIdentificationPricing.object(at: indexPath!.row) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            obj.setValue(false, forKey: "isDiscount")
            obj.setValue("0.00", forKey: "discount")
        }
        else
        {
            obj.setValue(true, forKey: "isDiscount") //isDiscount
        }
        
        arrayProblemIdentificationPricing.replaceObject(at: indexPath!.row, with: obj)
        
        tblviewConfigureProposal.reloadData()
        
        
    }
    
    @objc func action_Calculate(_ sender: UIButton){
        
        yesEditedSomething = true
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblviewConfigureProposal)
        
        let indexPath = self.tblviewConfigureProposal.indexPathForRow(at: buttonPosition)
        
        let obj = arrayProblemIdentificationPricing.object(at: indexPath!.row) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            obj.setValue(false, forKey: "isCalculate")
            //obj.setValue("0.00", forKey: "discount")
        }
        else
        {
            obj.setValue(true, forKey: "isCalculate") //isDiscount
        }
        
        arrayProblemIdentificationPricing.replaceObject(at: indexPath!.row, with: obj)
        
        tblviewConfigureProposal.reloadData()
        
    }
    
    @objc func action_EditPropsalDesc(_ sender: UIButton){
        
        indexOfPropsalDesc = sender.tag
        
        // pricingDescription
        yesEditedSomething = true
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblviewConfigureProposal)
        
        let indexPath = self.tblviewConfigureProposal.indexPathForRow(at: buttonPosition)
        
        let obj = arrayProblemIdentification.object(at: indexPath!.row) as! NSMutableDictionary
        
        let str = "\(obj.value(forKey: "proposalDescription") ?? "")"
        
        //if str.count > 0 {
            
            nsud.set("Proposal", forKey: "EditorTypeOnConfigurePropsal")
            nsud.synchronize()
            
            htmlEditorView(htmlString: str)

        //}
        
    }
    
    @objc func action_AddToAgreement(_ sender: UIButton){
        
        let obj = arrayProblemIdentification.object(at: sender.tag) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "check_ipad"))
        {
            obj.setValue(false, forKey: "isAddToAgreement")
        }
        else
        {
            obj.setValue(true, forKey: "isAddToAgreement")
        }
        
        arrayProblemIdentification.replaceObject(at: sender.tag, with: obj)
        
        tblviewConfigureProposal.reloadData()
        
    }
    
    @objc func action_BidOnRequest(_ sender: UIButton){
        
        let obj = arrayProblemIdentificationPricing.object(at: sender.tag) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            obj.setValue(false, forKey: "isBidOnRequest")
            obj.setValue("false", forKey: "isNoBidGiven")

        }
        else
        {
            obj.setValue(true, forKey: "isBidOnRequest")
            obj.setValue("false", forKey: "isNoBidGiven")

        }
        
        arrayProblemIdentificationPricing.replaceObject(at: sender.tag, with: obj)
        
        var heightProblemCell = 0
        
        for k in 0 ..< arrayProblemIdentificationPricing.count {
            
            let dictData = arrayProblemIdentificationPricing[k] as! NSDictionary
            
            let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
            
            if isBidOnRequestLocal {
                
                heightProblemCell = heightProblemCell + 285
                
            } else {
                
                heightProblemCell = heightProblemCell + 550
                
            }
            
        }
        
        hghtConstTblView.constant = CGFloat((heightProblemCell)+40)
        
        hghtConstViewContainerCreateBid.constant = (heightViewCreateBid_EditMode.constant + heightViewCreateBid_ViewMode.constant) + hghtConstTblView.constant
     
        DispatchQueue.main.async { [self] in
            let indexPath = IndexPath(item: sender.tag, section: 0)
            tblviewConfigureProposal.reloadRows(at: [indexPath], with: .none)
           // tblviewConfigureProposal.reloadData()
                     
        }
       
        
    }
    
    @objc func action_NoBidGiven(_ sender: UIButton){
        let obj = arrayProblemIdentificationPricing.object(at: sender.tag) as! NSMutableDictionary
        
        if(sender.currentImage == UIImage(named: "checked.png"))
        {
            obj.setValue("false", forKey: "isNoBidGiven")

        }
        else
        {
            obj.setValue("true", forKey: "isNoBidGiven")

        }
        
        arrayProblemIdentificationPricing.replaceObject(at: sender.tag, with: obj)
        DispatchQueue.main.async { [self] in
            let indexPath = IndexPath(item: sender.tag, section: 0)
            tblviewConfigureProposal.reloadRows(at: [indexPath], with: .none)
           // tblviewConfigureProposal.reloadData()
                     
        }
      
    }
 
    
    @IBAction func action_EditIntroductionLetter(_ sender: UIButton) {
        
        if txtviewIntroduction.text.count > 0 {
            
            nsud.set("Intro", forKey: "EditorTypeOnConfigurePropsal")
            nsud.synchronize()
            
            if isIntroductionLetterEdited {
                
                let description = "\(nsud.value(forKey: "htmlContentsIntroduction")!)"
                htmlEditorView(htmlString: description)
                
            }else{
                
                let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
                
                if(arrayLetterTemplate.count > 0)
                {
                    
                    let match = arrayLetterTemplate.firstObject as! NSManagedObject
                    // Introduction letter
                    
                    if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
                    {
                        for itemNew in arrayIntroduction
                        {
                            if("\((itemNew as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                            {
                                htmlEditorView(htmlString: "\(match.value(forKey: "introContent")!)")
                            }
                            
                        }
                        
                    }
                    
                }else{
                    
                    htmlEditorView(htmlString: "\(dictIntroduction.value(forKey: "TemplateContent")!)")

                }

            }

        }
        
    }
    
    @IBAction func action_SelectTipWarranty(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            ary = addSelectInArray(strName: "TIPWarrantyType", array: ary)
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    @IBAction func action_yesTIPWarranty(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isTipWarranty = true
        
        hghtConstTipYesNoView.constant = 209 + hghtConstLeadInspectionFee.constant
        hghtConstTipWarrantySelectionView.constant = 108
        
        if dictOfTipWarranty.count > 0 {
            
            let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
            
            if isMonthlyAmount {
                
                txtMonthlyAmount.isHidden = false
                
            } else {
                
                txtMonthlyAmount.isHidden = true
                
            }
            
        } else {
            
            txtMonthlyAmount.isHidden = true
            
        }
        
        
    }
    
    @IBAction func action_noTIPWarranty(_ sender: UIButton) {
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        btnYes_TipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
        
        isTipWarranty = false
        hghtConstTipYesNoView.constant = 101 + hghtConstLeadInspectionFee.constant
        hghtConstTipWarrantySelectionView.constant = 0
        strTipMasterName = ""
        strTipMasterId = ""
        
        dictOfTipWarranty = NSDictionary()
        btnTipWarrantyType.setTitle("\(strSelectString)", for: .normal)
        txtMonthlyAmount.isHidden = true
        txtMonthlyAmount.text = ""
        
    }
    
    
    @IBAction func action_Edit_CreatBid(_ sender: UIButton) {
        
        heightViewCreateBid_ViewMode.constant = 0.0
        heightViewCreateBid_EditMode.constant = 265.0
        setUpInitialConstanOfViews()
    }
    @IBAction func action_Save_CreatBid(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(txtPriceModificationReason.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Price modification reason is required.", viewcontrol: self)
        }else{

            let messageAlert = getPriceChangeAlertMessage(isCalculateDiscount: false)
            if messageAlert.count > 0 {
                let alert = UIAlertController(title: "Warning", message: messageAlert, preferredStyle: .alert)
                alert.view.tintColor = UIColor.darkGray
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    
                }))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                    if isUpdateApprovalParameter
                    {
                        self.updateWoDetail()
                    }
                    isUpdateCreateBid = false
                    self.savePricingDetails()
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                isUpdateCreateBid = false
                self.savePricingDetails()
            }
        }
    }
    
    @IBAction func action_Cancel_CreatBid(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.fetchWoWdoPricingMultiplierExtSerDcs()
        heightViewCreateBid_ViewMode.constant = 185.0
        heightViewCreateBid_EditMode.constant = 0.0
        setUpInitialConstanOfViews()
        
        
        
    }
    @IBAction func action_SelectModificationReason(_ sender: UIButton) {
        if(getPriceChangeReasonsMaster().count != 0){
            var aryReason = getPriceChangeReasonsMaster()
            aryReason =  addSelectInArray(strName: "Reason", array: aryReason)
            openTableViewPopUp(tag: 106, ary: aryReason , aryselectedItem: NSMutableArray())
        }else{
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Reason is not Available!", viewcontrol: self)
        }
    }
    @IBAction func action_RefreshWorkOrder(_ sender: Any) {
        FinalizeReport_WDOVC().refreshWorkOrderStatus(view: self, strWOID: strWoId as String)
    }
    
        // MARK: --------------------------------Validation-------------------------------------
    
    func validationForAddWdoInspectionOnConfigure() -> Bool {
        
        if(isTipWarranty && (IsTIPWarrantyRequired.lowercased() == "1" || IsTIPWarrantyRequired.lowercased() == "true")){
            
            if(btnTipWarrantyType.title(for: .normal) == strSelectString){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectTipWarrantyType, viewcontrol: self)
                
                return false
                
            }
            
            // check to show monthly amount textfireld IsMonthlyPrice
            
            if dictOfTipWarranty.count > 0{
                
                let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                
                if isMonthlyAmount {
                    
                    if((txtMonthlyAmount.text?.count)! < 1){
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterMonthlyIncome, viewcontrol: self)
                        
                        return false
                        
                    }
                    
                    let amountt = (txtMonthlyAmount.text! as NSString).doubleValue
                    
                    if amountt < 1 {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterMonthlyIncomeZero, viewcontrol: self)
                        
                        return false
                        
                    }
                    
                }
                
            }
            
        }
        
        return true
    }
    
    // MARK: --------------------------------functions-------------------------------------
    
    func checkIfDiscountIsOutOfRange() {
        
        if isUpdatedPrice {
            
            // Something is edited So Check for Discount out Of Range
            
            var strDiscount = "", subTotalAmount = ""
            
            let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
            
            if arrOfWdoInspection.count > 0 {
                
                let objData = arrOfWdoInspection[0] as! NSManagedObject
                
                strDiscount = "\(objData.value(forKey: "otherDiscount") ?? "")"
                
            }
            
            // To Fetch SubTotal At Run Time
            
            let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
            let arrayPI = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort)
            
            if(arrayPI.count > 0)
            {
                
                let arrayProblemIdentificationPricingLocal = NSMutableArray()
                
                let arrayProblemIdentificationLocal = NSMutableArray()
                
                //            arrayProblemIdentification = arrayPI.mutableCopy() as! NSMutableArray
                for obj in arrayPI
                {
                    arrayProblemIdentificationLocal.add(getMutableDictionaryFromNSManagedObject(obj: obj as! NSManagedObject))
                }
                
                for item in arrayProblemIdentificationLocal
                {
                    
                    let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)")).mutableCopy() as! NSArray
                    
                    if(arrayPIPricing.count > 0)
                    {
                        arrayProblemIdentificationPricingLocal.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                    }else{
                        
                        // to check for mobile id
                        
                        let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "mobileId")!)")).mutableCopy() as! NSArray
                        
                        if(arrayPIPricing.count > 0)
                        {
                            arrayProblemIdentificationPricingLocal.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                        }
                        
                    }
                    
                }
                
                
                if(arrayProblemIdentificationPricingLocal.count > 0)
                {
                    var subTotal = 0.00
                    
                    for item in arrayProblemIdentificationPricingLocal
                    {
                        
                        for itemPI in arrayProblemIdentificationLocal
                        {
                            if(("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false)
                            {
                                if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                                {
                                    subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                                }
                            }else if(("\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" == "\((itemPI as! NSMutableDictionary).value(forKey: "mobileId")!)") && (itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true && (item as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == false)
                            {
                                if ("\((item as! NSMutableDictionary).value(forKey: "subTotal") ?? "")".count > 0)
                                {
                                    subTotal = subTotal + Double(((item as! NSMutableDictionary).value(forKey: "subTotal") as! NSString).floatValue)
                                }
                            }
                        }
                    }
                    //
                    subTotalAmount = String(format: "%0.2f", subTotal)
                }
            }
            
            var message = ""
            let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
            
            let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

            
            if(aryRateMasterExtSerDcs.count != 0)
            {
                
                let obj = aryRateMasterExtSerDcs.firstObject as! NSDictionary
                
                //CalculateDiscount
                let subTotalAmount = Double("\(subTotalAmount)") ?? 0
                let MaxDiscount = (Double("\(obj.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
                let MinDiscount = Double("0") ?? 0
                var discount = Double("\(strDiscount)") ?? 0
                discount = discount < 0 ? 0 : discount
                
                if MaxDiscount != 0
                {
                    if !(MinDiscount...MaxDiscount ~= discount) {
                        message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount)\n"
                    }
                }
                
                if message.count > 0 {
                    
                    message = message + "You will need a supervisor's approval and signature before the customer can sign the proposal."
                    
                    // Update IsPricingApprovalPending true
                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isPricingApprovalPending")
                    arrOfValues.add("true")
                    
                    arrOfKeys.add("isPricingApprovalMailSend")
                    arrOfValues.add("false")
                    
                    arrOfKeys.add("modifiedBy")
                    arrOfKeys.add("modifyDate")
                    arrOfKeys.add("createdBy")
                    arrOfKeys.add("createdDate")
                    
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().modifyDateService())
                    arrOfValues.add(Global().getEmployeeId())
                    arrOfValues.add(Global().modifyDateService())
                    
                    let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    if isSuccess
                    {
                        //Update Modify Date In Work Order DB
                        updateServicePestModifyDate(strWoId: self.strWoId as String)
                    }
                }
            }
        }
    }
    
    func getPriceChangeAlertMessage(isCalculateDiscount : Bool) -> String {
        
        var strDiscount = "", subTotalAmount = ""
        
        let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
        
        if(arrayLeadDetail.count > 0)
        {
            
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            
            subTotalAmount = "\(match.value(forKey: "subTotalAmount")!)"
            
        }
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            
            strDiscount = "\(objData.value(forKey: "otherDiscount") ?? "")"

        }
        
        var message = ""
        let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

        
        if(aryRateMasterExtSerDcs.count != 0)
        {
          
            let obj = aryRateMasterExtSerDcs.firstObject as! NSDictionary
            let MaxHourlyRate = Double("\(obj.value(forKey: "MaxHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxHourlyRate") ?? "0")" )
            let MinHourlyRate = Double("\(obj.value(forKey: "MinHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinHourlyRate") ?? "0")")
            let HourlyRate =  Double("\(txtHourlyRateCreateBid.text ?? "0")")
            
            if MinHourlyRate != 0 && MaxHourlyRate != 0
            {
                if !(MinHourlyRate!...MaxHourlyRate! ~= HourlyRate!) {
                    message = "! Hourly Rate ($) is exceeding \(MinHourlyRate!) and \(MaxHourlyRate!)\n"
                }
            }
           
            let MaxMaterialMultiplier = Double("\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")")
            let MinMaterialMultiplier = Double("\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")")
            let MaterialMultiplier = Double("\(txtMaterialMultiplierCreateBid.text ?? "0")")
           
            if MinMaterialMultiplier != 0 && MaxMaterialMultiplier != 0
            {
                if !(MinMaterialMultiplier!...MaxMaterialMultiplier! ~= MaterialMultiplier!) {
                    message =  message + "! MaterialMultiplier is exceeding \(MinMaterialMultiplier!) and \(MaxMaterialMultiplier!)\n"
                }
            }
            
            let MaxSubFeeMultiplier = Double("\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")")
            let MinSubFeeMultiplier = Double("\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")")
            let SubFeeMultiplier = Double("\(txtSubFeeMultiplierCreateBid.text ?? "0")")

            if MinSubFeeMultiplier != 0 && MaxSubFeeMultiplier != 0
            {
                if !(MinSubFeeMultiplier!...MaxSubFeeMultiplier! ~= SubFeeMultiplier!) {
                    message =  message + "! Sub Fee Multiplier is exceeding \(MinSubFeeMultiplier!) and \(MaxSubFeeMultiplier!)\n"
                }
            }
            
            let MaxSoilTreatmentRate = Double("\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")")
            let MinSoilTreatmentRate = Double("\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")")
            let SoilTreatmentRate = Double("\(txtLinearFtSoilTreatmentCreateBid.text ?? "0")")
            
            if MinSoilTreatmentRate != 0 && MaxSoilTreatmentRate != 0
            {
                if !(MinSoilTreatmentRate!...MaxSoilTreatmentRate! ~= SoilTreatmentRate!) {
                    message =  message + "! Linear Ft. Soil Treatment  Rate ($) is exceeding \(MinSoilTreatmentRate!) and \(MaxSoilTreatmentRate!)\n"
                }
            }
            
            let MaxDrilledConcreteRate = Double("\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")")
            let MinDrilledConcreteRate = Double("\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")")
            let DrilledConcreteRate = Double("\(txtLinearFtDrilledConcreteCreateBid.text ?? "0")")
            
            if MinDrilledConcreteRate != 0 && MaxDrilledConcreteRate != 0
            {
                if !(MinDrilledConcreteRate!...MaxDrilledConcreteRate! ~= DrilledConcreteRate!) {
                    message =  message + "! Linear Ft. Drilled Concrete  Rate ($) is exceeding \(MinDrilledConcreteRate!) and \(MaxDrilledConcreteRate!)\n"
                }
            }
            
            
            let MaxCubicRate = Double("\(obj.value(forKey: "MaxCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxCubicRate") ?? "0")")
            let MinCubicRate = Double("\(obj.value(forKey: "MinCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinCubicRate") ?? "0")")
            let CubicRate = Double("\(txtCubicFtRateCreateBid.text ?? "0")")

            if MinCubicRate != 0 && MaxCubicRate != 0
            {
                if !(MinCubicRate!...MaxCubicRate! ~= CubicRate!) {
                    message =  message + "! Cubic ft. Rate ($) is exceeding \(MinCubicRate!) and \(MaxCubicRate!)\n"
                }
                
            }
            
            if isCalculateDiscount {
                
                //CalculateDiscount
                let subTotalAmount = Double("\(subTotalAmount)") ?? 0
                let MaxDiscount = (Double("\(obj.value(forKey: "MaxAllowed") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxAllowed") ?? "0")") ?? 0) * ((subTotalAmount/100))
                let MinDiscount = Double("0") ?? 0
                var discount = Double("\(strDiscount)") ?? 0
                discount = discount < 0 ? 0 : discount
                
                if MaxDiscount != 0
                {
                    if !(MinDiscount...MaxDiscount ~= discount) {
                        message =  message + "! Discount ($) is exceeding \(MinDiscount) and \(MaxDiscount.rounded())\n"
                    }
                }
                
            }
          
            if message.count > 0 {
                
                message = message + "You will need a supervisor's approval and signature before the customer can sign the proposal."
                
            }
            
        }
        return message
    }
    
    func savePricingDetails()  {
        
        heightViewCreateBid_ViewMode.constant = 185.0
        heightViewCreateBid_EditMode.constant = 0.0
        UpdatePriceChangeReason()

        saveProblemIdentificationMultiplierAndProblemIdentificationAndProblemIdentificationPricing()
        fetchWoWdoPricingMultiplierExtSerDcs()
        setUpInitialConstanOfViews()
    }
    
    func saveWdoInspection() {
        
        nsud.set(true, forKey: "yesupdatedWdoInspection")
        nsud.synchronize()
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()

        arrOfKeys.add("tIPMasterId")
        arrOfKeys.add("tIPMasterName")
        arrOfKeys.add("isTipWarranty")
        arrOfKeys.add("monthlyAmount")

        arrOfValues.add(strTipMasterId)
        arrOfValues.add(strTipMasterName)
        arrOfValues.add(isTipWarranty)
        
        if (isTipWarranty ) {
            
            let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
            
            if isMonthlyAmount {
                
                arrOfValues.add(txtMonthlyAmount.text!)
                
            } else {
                
                arrOfValues.add("")

            }
            
        } else {
            
            arrOfValues.add("")
            
        }
    
        // Calcualte TOTAL TIP Discount
        //let totalTIPDiscountLocal = String(format: "%.2f", Double((txtMonthlyAmount.text! as NSString).floatValue) * 12)

        //arrOfKeys.add("totalTIPDiscount")
        arrOfKeys.add("leadInspectionFee")

        //arrOfValues.add(totalTIPDiscountLocal)
        arrOfValues.add(txtLeadInspectionFee.text!)
        
        arrOfKeys.add("tIPTHPSType")
        if dictOfTipWarranty.count != 0 {
            arrOfValues.add(dictOfTipWarranty.value(forKey: "TIPTHPSType") ?? "")
        }else{
            arrOfValues.add("")
        }

        
        
        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
        if (isTipWarranty ) {
            
            if strTipMasterId.count > 0 {
                
                checkWoWdoGeneralNoteMapping(strMappingType: "TIPMaster", strIdToCheck: strTipMasterId)
                
            }
            
        }
        
    }
    
    func saveTIPDiscount(arrOfValues : NSMutableArray) {
        
        isEditedTIPWarranty = false
        
        let arrOfKeys = NSMutableArray()
        arrOfKeys.add("totalTIPDiscount")
        
        if (isTipWarranty ) {
            let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
            if isMonthlyAmount {
                
            } else {
                
                arrOfValues.removeAllObjects()
                arrOfValues.add("")
                
            }
        } else {
            
            arrOfValues.removeAllObjects()
            arrOfValues.add("")
            
        }
        
        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }

    // MARK: - -----------------------------------General Notes Based on TIP -----------------------------------
    
    func checkWoWdoGeneralNoteMapping(strMappingType : String , strIdToCheck : String) {
        
        //WoWdoGeneralNoteMappingExtSerDcs FollowUpMaster TIPMaster BuildingPermit
        // MasterId MappingType IsActive GeneralNoteId
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoGeneralNoteMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                    
                    if strIdToCheck == "\(dictData.value(forKey: "MasterId") ?? "")" {
                        
                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            
                            saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strMappingType: strMappingType)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    func saveGeneralNotesDB(strGeneralNotedId : String , strMappingType : String) {
        
        var array = NSMutableArray()
        var dictDataOfGeneralNotesTemp = NSDictionary()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if strGeneralNotedId == "\(dictData.value(forKey: "ServiceJobDescriptionId") ?? "")" {
                    
                    dictDataOfGeneralNotesTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfGeneralNotesTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("generalNoteMasterId")
            arrOfKeys.add("generalNoteDescription")
            arrOfKeys.add("title")
            arrOfKeys.add("wdoGeneralNoteId")
            
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescriptionId")!)")
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescription")!)")
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "Title")!)")
            
            arrOfValues.add(Global().getReferenceNumber())
            
            
            arrOfKeys.add("type")
            arrOfValues.add(strMappingType)
            
            arrOfKeys.add("isChanged")
            arrOfValues.add(false)
            
            // Saving Disclaimer In DB
            saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }
    //Change LeadInspectionFee logic
    func checkIfLeadInspectionFee() {
        
        let arrOfProblemIdentifications = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
     
        let aryTemp = arrOfProblemIdentifications.filter { (task) -> Bool in
            
            return "\((task as! NSManagedObject).value(forKey: "isLeadTest") ??  "")".lowercased().contains("yes".lowercased()) || "\((task as! NSManagedObject).value(forKey: "isLeadTest") ??  "")".lowercased().contains("furtherInspection".lowercased()) }
    
        if aryTemp.count > 0 {
//            let objData = arrOfProblemIdentifications[0] as! NSManagedObject
//            let isTipWarranty = objData.value(forKey: "isLeadTest")
            hghtConstLeadInspectionFee.constant = 50
            
        }else{
            
            hghtConstLeadInspectionFee.constant = 0
            txtLeadInspectionFee.text = ""
            // Update lead inspection fee in db
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            arrOfKeys.add("leadInspectionFee")
            arrOfValues.add(txtLeadInspectionFee.text!)
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
            let arrOfKey = NSMutableArray()
            let arrOfValue = NSMutableArray()
            
            // keys
            arrOfKey.add("leadInspectionFee")

            // values
            arrOfValue.add(txtLeadInspectionFee.text!)

            if(yesEditedSomething)
            {
                arrOfKeys.add("zSync")
                arrOfValues.add("yes")
            }
    
            
            let isSuccessLeadDetail =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if(isSuccessLeadDetail == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
            
        }
        
    }
    
    func setValuesWdoInspection() {
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject

            isTipWarranty = objData.value(forKey: "isTipWarranty") as! Bool
            
            isTipWarranty = true

            if (isTipWarranty ) {
                
                btnYes_TipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                hghtConstTipYesNoView.constant = 209 + hghtConstLeadInspectionFee.constant
                hghtConstTipWarrantySelectionView.constant = 108
            }else{
                
                btnNoTipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                btnYes_TipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
                
                hghtConstTipYesNoView.constant = 101 + hghtConstLeadInspectionFee.constant
                hghtConstTipWarrantySelectionView.constant = 108
            }

            txtMonthlyAmount.text = "\(objData.value(forKey: "monthlyAmount") ?? "")"
            
            // tIPMasterName  tIPMasterId
            strTipMasterName = "\(objData.value(forKey: "tIPMasterName") ?? "")"
            strTipMasterId = "\(objData.value(forKey: "tIPMasterId") ?? "")"
            
            if strTipMasterName.count > 0 {
                
                btnTipWarrantyType.setTitle(strTipMasterName, for: .normal)
                
                var ary = NSMutableArray()
                
                ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TIPMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
                
                if ary.count > 0 {
                    
                    for k in 0 ..< ary.count {
                        
                        let dictData = ary[k] as! NSDictionary
                        
                        if strTipMasterId == "\(dictData.value(forKey: "TIPMasterId") ?? "")" {
                            
                            dictOfTipWarranty = dictData
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                if dictOfTipWarranty.count > 0 {
                    
                    let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                    
                    if !isMonthlyAmount {
                        
                        txtMonthlyAmount.isHidden = true
                        
                    }
                    
                }
                
                
            }
            else{
                
                btnTipWarrantyType.setTitle(strSelectString, for: .normal)
                txtMonthlyAmount.isHidden = true
                txtMonthlyAmount.text = ""
                
            }
            
            
        }else{

            btnYes_TipWarranty.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
            btnNoTipWarranty.setImage(UIImage(named: "radio_uncheck_ipad"), for: .normal)
            isTipWarranty = true
            hghtConstTipYesNoView.constant = 101 + hghtConstLeadInspectionFee.constant
            hghtConstTipWarrantySelectionView.constant = 0
            
            txtMonthlyAmount.text = ""
            
            // tIPMasterName  tIPMasterId
            strTipMasterName = ""
            strTipMasterId = ""
            
        }
        
    }
    
    func htmlEditorView(htmlString : String) {
        
        let storyboard = UIStoryboard(name: "WDOiPad", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        
        controller.strHtml = htmlString
        
        self.present(controller, animated: false, completion: nil)

    }
    
    fileprivate func accessWhenOpportunityCompletwPending()  {
        txtviewAdditionalNotes.isUserInteractionEnabled = true
        btnAgreementValidFor.isUserInteractionEnabled = true
        txtValidFor.isUserInteractionEnabled = true
        btnTermsOfService.isUserInteractionEnabled = true
    }
    
    fileprivate func disableUserInteration()
    {
        
        tblviewConfigureProposal.isUserInteractionEnabled = false
        lblOrderNumber.isUserInteractionEnabled = false
        txtHourlyRateCreateBid.isUserInteractionEnabled = false
        txtMaterialMultiplierCreateBid.isUserInteractionEnabled = false
        txtLinearFtSoilTreatmentCreateBid.isUserInteractionEnabled = false
        txtLinearFtDrilledConcreteCreateBid.isUserInteractionEnabled = false
        txtCubicFtRateCreateBid.isUserInteractionEnabled = false
        btnCoverLetter.isUserInteractionEnabled = false
        btnIntroduction.isUserInteractionEnabled = false
        btnSalesMarketingContent.isUserInteractionEnabled = false
        btnJanuary.isUserInteractionEnabled = false
        btnFebruary.isUserInteractionEnabled = false
        btnMarch.isUserInteractionEnabled = false
        btnApril.isUserInteractionEnabled = false
        btnMay.isUserInteractionEnabled = false
        btnJune.isUserInteractionEnabled = false
        btnJuly.isUserInteractionEnabled = false
        btnAugust.isUserInteractionEnabled = false
        btnSeptember.isUserInteractionEnabled = false
        btnOctober.isUserInteractionEnabled = false
        btnNovember.isUserInteractionEnabled = false
        btnDecember.isUserInteractionEnabled = false
        txtviewAdditionalNotes.isUserInteractionEnabled = false
        btnAgreementValidFor.isUserInteractionEnabled = false
        txtValidFor.isUserInteractionEnabled = false
        btnTermsOfService.isUserInteractionEnabled = false
        btnTermsAndConditions.isUserInteractionEnabled = false
        txtviewAdditionalNotes.isUserInteractionEnabled = false
        btnEditIntroductionLetter.isUserInteractionEnabled = false
        btnEditRate.isHidden = true
        btnTipWarrantyType.isUserInteractionEnabled = false
        txtLeadInspectionFee.isUserInteractionEnabled = false
        txtMonthlyAmount.isUserInteractionEnabled = false
        
        
        
    }
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = strAccountNo
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    // save
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    fileprivate func goToAgreementAfterAlert()
    {
        
        if(yesEditedSomething)
        {
            global.updateSalesModifydate(strLeadId)
        }
        
        if(yesEditedSomething)
        {
            // sett true to sync sales data after wdo data
            nsud.set(true, forKey: "synAgreementWdo")
            nsud.synchronize()
            
        }
        //  Commit Changes.
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "Agreement_WDOVC") as! Agreement_WDOVC
        testController.strLeadId = strLeadId
        testController.strWoId = strWoId
        testController.strForProposal = strForProposal
        testController.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    func updateIsEditedMonthlyAmount(str : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("isEditedMonthlyAmount")
        arrOfValues.add(str)

        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
        }
        
    }
    
    fileprivate func gotoAgreementView()
    {
        
        let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
        
        if arrOfWdoInspection.count > 0 {
            
            let objData = arrOfWdoInspection[0] as! NSManagedObject
            let tipDiscount = Double((objData.value(forKey: "totalTIPDiscount") as! NSString).floatValue)
            
            let isEditedMonthlyAmountisEditedMonthlyAmount = "\(objData.value(forKey: "isEditedMonthlyAmount") ?? "")"
            
            //if tipDiscount <= 0 {
            if isEditedMonthlyAmountisEditedMonthlyAmount == "true" {
                
                let arrOfValues = NSMutableArray()
                
                // Calcualte TOTAL TIP Discount
                let totalTIPDiscountLocal = String(format: "%.2f", Double((self.txtMonthlyAmount.text! as NSString).floatValue) * 12)
                
                arrOfValues.add(totalTIPDiscountLocal)
                
                self.saveTIPDiscount(arrOfValues: arrOfValues)
                
                isEditedTIPWarranty = false
                
            }else{
                
                let arrOfValues = NSMutableArray()
                
                let totalTIPDiscountLocal = String(format: "%.2f", tipDiscount)
                arrOfValues.add(totalTIPDiscountLocal)

                self.saveTIPDiscount(arrOfValues: arrOfValues)

            }

            self.updateIsEditedMonthlyAmount(str: "false")

        }
        
        // Check If Edited TIP Warranty then ask if to update total TIP Discount or not.
        
        if isEditedTIPWarranty {
            
            let alert = UIAlertController(title: Alert, message: "Do you won't to update Total TIP Discount", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES-Update", style: .default , handler:{ (UIAlertAction)in
                
                let arrOfValues = NSMutableArray()
                
                // Calcualte TOTAL TIP Discount
                let totalTIPDiscountLocal = String(format: "%.2f", Double((self.txtMonthlyAmount.text! as NSString).floatValue) * 12)
                
                arrOfValues.add(totalTIPDiscountLocal)
                
                self.saveTIPDiscount(arrOfValues: arrOfValues)
                
                self.goToAgreementAfterAlert()
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                
                self.goToAgreementAfterAlert()
                
            }))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
            
        } else {
            
            goToAgreementAfterAlert()
            
        }
        
        /*
        if(yesEditedSomething)
        {
            global.updateSalesModifydate(strLeadId)
        }
        
        if(yesEditedSomething)
        {
            // sett true to sync sales data after wdo data
            nsud.set(true, forKey: "synAgreementWdo")
            nsud.synchronize()
        }
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "Agreement_WDOVC") as! Agreement_WDOVC
        testController.strLeadId = strLeadId
        testController.strWoId = strWoId
        testController.strForProposal = strForProposal
        testController.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)*/
        
    }
    fileprivate func setUpInitialConstanOfViews()
    {
        // hghtConstViewContainerCreateBid.constant = 0.0
        
        viewContainerCreateBid.isHidden = false
        
        var heightProblemCell = 0
        
        for k in 0 ..< arrayProblemIdentificationPricing.count {
            
            let dictData = arrayProblemIdentificationPricing[k] as! NSDictionary
            
            let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
            
            if isBidOnRequestLocal {
                
                heightProblemCell = heightProblemCell + 285
                
            } else {
                
                heightProblemCell = heightProblemCell + 550
                
            }
            
        }
        
        hghtConstTblView.constant = CGFloat((heightProblemCell)+40)
        
        hghtConstViewContainerCreateBid.constant = (heightViewCreateBid_EditMode.constant + heightViewCreateBid_ViewMode.constant) + hghtConstTblView.constant
        
        
        
        hghtConstViewContainerCoverLetter.constant = 0.0
        hghtconstViewContainerIntroduction.constant = 0.0
        hghtconstViewContainerSalesMarketing.constant = 0.0
        hghtConstViewContainerServiceMonths.constant = 0.0
        hghtConstViewContainerOther.constant = 0.0
        hghtconstViewContainerTermsOfService.constant = 0.0
        hghtconstViewContainerTermsCondition.constant = 0.0
        
        
        //viewContainerCreateBid.isHidden = true
        viewConainerCoverSheet.isHidden = true
        viewContainerIntroduction.isHidden = true
        viewContainerMarketingContent.isHidden = true
        viewContainerServiceMonths.isHidden = true
        viewConainerOther.isHidden = true
        viewContainerTermsOfService.isHidden = true
        viewContainerTermsAndConditions.isHidden = true
        
    }
    
    fileprivate func configureUI()
    {
        
        makeCornerRadius(value: 2.0, view: btnCoverLetter, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewIntroduction, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnIntroduction, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnSalesMarketingContent, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewTermsOfService, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnTermsOfService, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        makeCornerRadius(value: 2.0, view: btnTermsAndConditions, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewAdditionalNotes, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnTipWarrantyType, borderWidth: 1.0, borderColor: UIColor.lightGray)

        buttonRound(sender: btnSave)
        
        txtValidFor.isHidden = true
        
    }
    
    fileprivate func getLetterTemplateAndTermsOfService()
    {
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMaster.value(forKey: "LetterTemplateMaster") is NSArray){
            let arrayLetterTemplateMaster = (dictMaster.value(forKey: "LetterTemplateMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            let arraySalesMarketingContentMaster = (dictMaster.value(forKey: "SalesMarketingContentMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            arrayTermsOfService = (dictMaster.value(forKey: "TermsOfServiceMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            
            // cover letter and Introduction
            for item in arrayLetterTemplateMaster
            {
                if("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "CoverLetter")
                {
                    arrayCoverLetter.add(item as! NSDictionary)
                }
                else if ("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "Introduction")
                {
                    arrayIntroduction.add(item as! NSDictionary)
                }
            }
            // Sales Marketing Content
            
            for item in arraySalesMarketingContentMaster
            {
                if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
                {
                    if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                    {
                        arrayMarketingContent.add(item as! NSDictionary)
                    }
                    else
                    {
                        arrayMarketingContent.add(item as! NSDictionary)
                    }
                }
            }
        }
     
    
        
        // Terms & Conditions
        
        
        if(dictMaster.value(forKey: "MultipleGeneralTermsConditions") is NSArray){
            let arrayMultipleGeneralTermsConditions = (dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrayMultipleGeneralTermsConditions
            {
                if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
                {
                    if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                    {
                        arrayTermsAndConditions.add(item as! NSDictionary)
                    }
                }
                
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true && (item as! NSDictionary).value(forKey: "IsDefault") as! Bool == true)
                {
                    arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                    
                    btnTermsAndConditions.setTitle("\((item as! NSDictionary).value(forKey: "TermsTitle")!)", for: .normal)
                    
                }
            }
        }
  
        
    }
    
    fileprivate func getRecommendationMaster()
    {
        
        
        arrayRecommendationMaster = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
    }
    
    // MARK: GetPriceChangeReasonsMaster
    func getPriceChangeReasonsMaster() -> NSMutableArray
    {
        let dictMaster = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        if((dictMaster.count) != 0){
            if((dictMaster.value(forKey: "PriceChangeReasons") is NSArray)){
                let arrayPriceChangeReasonMasterExtDc = (dictMaster.value(forKey: "PriceChangeReasons") as! NSArray).mutableCopy() as! NSMutableArray
                let aryTemp = arrayPriceChangeReasonMasterExtDc.filter { (task) -> Bool in
                    
                    return "\((task as! NSDictionary).value(forKey: "IsService")!)".lowercased().contains("true")  || "\((task as! NSDictionary).value(forKey: "IsService")!)".lowercased().contains("1") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("1")}
                return (aryTemp as NSArray).mutableCopy() as! NSMutableArray
            }
        }
        return NSMutableArray()
    }
    
    // MARK: Save to local DB
    fileprivate func saveLetterTemplateCoverLetterIntroLetterAndTermsOfService(dictCoverLetter:NSDictionary, dictIntroLetter:NSDictionary,dictTermsOfService:NSDictionary)
    {
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadCommercialDetailId")
        arrOfKeys.add("coverLetterSysName")
        arrOfKeys.add("introSysName")
        arrOfKeys.add("introContent")
        arrOfKeys.add("termsOfServiceSysName")
        arrOfKeys.add("termsOfServiceContent")
        arrOfKeys.add("isAgreementValidFor")
        arrOfKeys.add("validFor")
        arrOfKeys.add("isInitialTaxApplicable")
        arrOfKeys.add("isMaintTaxApplicable")
        
        // values
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strLeadId)
        arrOfValues.add("")
        
        // cover letter
        if(dictCoverLetter.count > 0)
        {
            arrOfValues.add("\(dictCoverLetter.value(forKey: "SysName")!)")
        }
        else
        {
            arrOfValues.add("")
        }
        
        // Intro Letter
        if(dictIntroLetter.count > 0)
        {
            
            arrOfValues.add("\(dictIntroLetter.value(forKey: "SysName")!)")
            //arrOfValues.add("\(dictIntroLetter.value(forKey: "TemplateContent")!)")
            
            if isIntroductionLetterEdited {
                
                let description = "\(nsud.value(forKey: "htmlContentsIntroduction")!)"
                arrOfValues.add("\(description)")
                
            }else{
                
                arrOfValues.add("\(dictIntroLetter.value(forKey: "TemplateContent")!)")
                
            }

        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        // Terms Of Service
        
        if(dictTermsOfService.count > 0)
        {
            arrOfValues.add("\(dictTermsOfService.value(forKey: "SysName")!)")
            arrOfValues.add("\(converHTML(html: dictTermsOfService.value(forKey: "Description")! as! String))")
        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        // isAgreementValidFor
        
        if(btnAgreementValidFor.currentImage == UIImage(named: "check_ipad"))
        {
            if txtValidFor.text == ""
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter valid for", viewcontrol: self)
                
                return
                
            }
            else
            {
                arrOfValues.add("true")
                arrOfValues.add("\(txtValidFor.text!)")
            }
            
            
        }
        else
        {
            arrOfValues.add("false")
            arrOfValues.add("")
        }
        
        // isInitialTaxApplicable and isMaintTaxApplicable
        
        arrOfValues.add("")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "LeadCommercialDetailExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    fileprivate func saveMarketingContent(arraySeletecMarketingContent:NSMutableArray)
    {
        if(arraySeletecMarketingContent.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
            
            for item in arraySeletecMarketingContent
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                // keys
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("leadId")
                arrOfKeys.add("contentSysName")
                
                
                // values
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strLeadId)
                arrOfValues.add("\((item as! NSDictionary).value(forKey: "SysName")!)")
                
                saveDataInDB(strEntity: "LeadMarketingContentExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }
    }
    
    fileprivate func saveTermsAndConditions(arraySeletecTermsAndConditions:NSMutableArray)
    {
        
        /* NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
         NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
         NSString *strEmpBranchID =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeBranchSysName"]];*/
        
        let dictLogInDetail = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeBranchSysName = "\(dictLogInDetail.value(forKey: "EmployeeBranchSysName")!)"
        
        
        if(arraySeletecTermsAndConditions.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        }
        
        for item in arraySeletecTermsAndConditions
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadCommercialTermsId")
            arrOfKeys.add("branchSysName")
            arrOfKeys.add("termsnConditions")
            arrOfKeys.add("termsId")
            
            // values
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strLeadId)
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            arrOfValues.add("\(employeeBranchSysName)")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "TermsConditions")!)")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            
            saveDataInDB(strEntity: "LeadCommercialTermsExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
    }
    
    fileprivate func saveProblemIdentificationMultiplierAndProblemIdentificationAndProblemIdentificationPricing()
    {
        
        deleteAllRecordsFromDB(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        // save problem identification multiplier
        if(arrayMultiplier.count > 0) // update multiplier
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("hourlyRate")
            arrOfKeys.add("materialMultiplier")
            arrOfKeys.add("subFeeMultiplier")
            arrOfKeys.add("linearFtSoilTreatmentRate")
            arrOfKeys.add("linearFtDrilledConcreteRate")
            arrOfKeys.add("cubicFtRate")
            arrOfKeys.add("isActive")
            //arrOfKeys.add("priceChangeReasonId")

            arrOfValues.add(strWoId)
            arrOfValues.add(strEmpID)
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add("\(txtHourlyRateCreateBid.text ?? "")")
            arrOfValues.add("\(txtMaterialMultiplierCreateBid.text ?? "")")
            arrOfValues.add("\(txtSubFeeMultiplierCreateBid.text ?? "")")
            arrOfValues.add("\(txtLinearFtSoilTreatmentCreateBid.text ?? "")")
            arrOfValues.add("\(txtLinearFtDrilledConcreteCreateBid.text ?? "")")
            arrOfValues.add("\(txtCubicFtRateCreateBid.text ?? "")")
            arrOfValues.add(true)
            //arrOfValues.add(txtPriceModificationReason.tag == 0 ? "" : "\(txtPriceModificationReason.tag)")

            let isSuccess =  getDataFromDbToUpdate(strEntity: Entity_WdoPricingMultipliers, predicate: NSPredicate(format: "workOrderId == %@",strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if(isSuccess)
            {
                print("pricing multiplier updated successfully")
            }
        }
        else
        { // save multiplier
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            arrOfKeys.add("userName")
            arrOfKeys.add("woWdoPricingMultiplierId")
            arrOfKeys.add("hourlyRate")
            arrOfKeys.add("materialMultiplier")
            arrOfKeys.add("subFeeMultiplier")
            arrOfKeys.add("linearFtSoilTreatmentRate")
            arrOfKeys.add("linearFtDrilledConcreteRate")
            arrOfKeys.add("cubicFtRate")
            //arrOfKeys.add("priceChangeReasonId")

            
            // values
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strEmpID)
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(strEmpID)
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(strWoId)
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            arrOfValues.add(strUserName)
            arrOfValues.add("")
            
            arrOfValues.add("\(txtHourlyRateCreateBid.text ?? "")")
            arrOfValues.add("\(txtMaterialMultiplierCreateBid.text ?? "")")
            arrOfValues.add("\(txtSubFeeMultiplierCreateBid.text ?? "")")
            arrOfValues.add("\(txtLinearFtSoilTreatmentCreateBid.text ?? "")")
            arrOfValues.add("\(txtLinearFtDrilledConcreteCreateBid.text ?? "")")
            arrOfValues.add("\(txtCubicFtRateCreateBid.text ?? "")")
            //arrOfValues.add(txtPriceModificationReason.tag == 0 ? "" : "\(txtPriceModificationReason.tag)")

            
            saveDataInDB(strEntity: Entity_WdoPricingMultipliers, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
        for itemPI in arrayProblemIdentification
        {
            let item = itemPI as! NSMutableDictionary
            
            let arrOfKeys1 = NSMutableArray()
            let arrOfValues1 = NSMutableArray()
            
            
            arrOfKeys1.add("workOrderId")
            arrOfKeys1.add("recommendation")
            arrOfKeys1.add("modifiedBy")
            arrOfKeys1.add("modifiedDate")
            arrOfKeys1.add("isAddToAgreement")
            //arrOfKeys1.add("isCalculate")
            arrOfKeys1.add("proposalDescription")

            arrOfValues1.add(strWoId)
            arrOfValues1.add("\(item.value(forKey: "recommendation") ?? "")")
            arrOfValues1.add(strEmpID)
            arrOfValues1.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            //arrOfValues1.add(item.value(forKey: "isAddToAgreement") as! Bool)
            
            if (item.value(forKey: "isAddToAgreement") as! Bool) == true {
                
                arrOfValues1.add(true)
                
            }else{
                
                arrOfValues1.add(false)
                
            }
            
            //arrOfValues1.add(item.value(forKey: "isCalculate") as! Bool)
            
            let strFillingLocal = "\(item.value(forKey: "recommendationFillIn") ?? "")"
            
            let strRecommendationLocal = "\(item.value(forKey: "proposalDescription") ?? "")"
            
            let strReplacedString = replaceTags(strFillingLocal: strFillingLocal, strFindingLocal: strRecommendationLocal)
            
            //arrOfValues1.add(strReplacedString)

            arrOfValues1.add("\(item.value(forKey: "proposalDescription") ?? "")")
            
            var idLocal = "\(item.value(forKey: "problemIdentificationId")!)"
            
            var isSuccess = false
            
            if idLocal.count > 0 {
                
                isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoProblemIdentificationExtSerDcs", predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@",strWoId,"\(item.value(forKey: "problemIdentificationId")!)"), arrayOfKey: arrOfKeys1, arrayOfValue: arrOfValues1)

                
            }else{
                
                idLocal = "\(item.value(forKey: "mobileId")!)"
                
                isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoProblemIdentificationExtSerDcs", predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@",strWoId,"\(item.value(forKey: "mobileId")!)"), arrayOfKey: arrOfKeys1, arrayOfValue: arrOfValues1)
                
            }
            
            
            if(isSuccess)
            {
                for itemPIPricing in arrayProblemIdentificationPricing
                {
                    if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)" ==  idLocal)
                    {

                        
                        let arrOfKeys2 = NSMutableArray()
                        let arrOfValues2 = NSMutableArray()
                        
                        arrOfKeys2.add("modifiedBy")
                        arrOfKeys2.add("modifiedDate")
                        arrOfKeys2.add("hours")
                        arrOfKeys2.add("materials")
                        arrOfKeys2.add("subFee")
                        arrOfKeys2.add("linearFtSoilTreatment")
                        arrOfKeys2.add("linearFtDrilledConcrete")
                        arrOfKeys2.add("cubicFt")
                        arrOfKeys2.add("subTotal")
                        arrOfKeys2.add("isDiscount")
                        arrOfKeys2.add("discount")
                        arrOfKeys2.add("total")
                        arrOfKeys2.add("problemIdentificationId")
                        arrOfKeys2.add("isCalculate")
                        arrOfKeys2.add("isBidOnRequest")
                        arrOfKeys2.add("isNoBidGiven")

                        
                        arrOfValues2.add(strEmpID)
                        arrOfValues2.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                        
                        if (itemPIPricing as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool {
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add("0")
                            
                            arrOfValues2.add((itemPIPricing as! NSMutableDictionary).value(forKey: "isDiscount") as! Bool)
                            
                            arrOfValues2.add("0")
                            
                            // for total key
                            if ((itemPIPricing as! NSMutableDictionary).value(forKey: "isDiscount") as! Bool ==  true)
                            {
                                arrOfValues2.add("0")
                            }
                            else
                            {
                                arrOfValues2.add("0")
                            }
                            
                        }else {
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "hours") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "materials") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "subFee") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtSoilTreatment") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtDrilledConcrete") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "cubicFt") ?? "")")
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "subTotal") ?? "")")
                            
                            arrOfValues2.add((itemPIPricing as! NSMutableDictionary).value(forKey: "isDiscount") as! Bool)
                            
                            arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "discount") ?? "")")
                            
                            // for total key
                            if ((itemPIPricing as! NSMutableDictionary).value(forKey: "isDiscount") as! Bool ==  true)
                            {
                                arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "total") ?? "")")
                            }
                            else
                            {
                                arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "subTotal") ?? "")")
                            }
                            
                        }
                        
                        arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)")
                        arrOfValues2.add((itemPIPricing as! NSMutableDictionary).value(forKey: "isCalculate") as! Bool)
                        arrOfValues2.add((itemPIPricing as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool)
                        arrOfValues2.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "isNoBidGiven")!)")

                        
                        let idLocal = "\(item.value(forKey: "problemIdentificationId")!)"
                        
                        var isSuccess = false
                        
                        // Jugad for change in cubic Rate
                        
                        arrOfKeys2.add("isChangedCubicRate")
                        arrOfValues2.add(false)
                        
                        arrOfKeys2.add("workOrderId")
                        arrOfValues2.add(strWoId)
                        
                        arrOfKeys2.add("companyKey")
                        arrOfValues2.add(strCompanyKey)
                        
                        
                        if idLocal.count > 0 {
                            
                            isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\(item.value(forKey: "problemIdentificationId")!)"), arrayOfKey: arrOfKeys2, arrayOfValue: arrOfValues2)
                            
                        }else{
                            
                            isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\(item.value(forKey: "mobileId")!)"), arrayOfKey: arrOfKeys2, arrayOfValue: arrOfValues2)
                            
                        }
                        
                        //let isSuccess =  getDataFromDbToUpdate(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\(item.value(forKey: "problemIdentificationId")!)"), arrayOfKey: arrOfKeys2, arrayOfValue: arrOfValues2)
                        
                        
                        if(isSuccess)
                        {
                            
                            let arrOfKeys3 = NSMutableArray()
                            let arrOfValues3 = NSMutableArray()
                            
                            // keys
                            
                            
                            arrOfKeys3.add("wdoProblemIdentificationId")
                            arrOfKeys3.add("mobileId")
                            arrOfKeys3.add("userName")
                            arrOfKeys3.add("companyKey")
                            arrOfKeys3.add("leadId")
                            arrOfKeys3.add("createdBy")
                            arrOfKeys3.add("createdDate")
                            arrOfKeys3.add("modifiedBy")
                            arrOfKeys3.add("modifiedDate")
                            arrOfKeys3.add("serviceName")
                            arrOfKeys3.add("serviceDescription")
                            arrOfKeys3.add("discount")
                            arrOfKeys3.add("isSold")
                            arrOfKeys3.add("frequencySysName")
                            arrOfKeys3.add("serviceFrequency")
                            arrOfKeys3.add("departmentSysname")
                            arrOfKeys3.add("soldServiceNonStandardId")
                            arrOfKeys3.add("initialPrice")
                            
                            arrOfKeys3.add("billingFrequencyPrice")
                            arrOfKeys3.add("billingFrequencySysName")
                            arrOfKeys3.add("discountPercentage")
                            arrOfKeys3.add("maintenancePrice")
                            arrOfKeys3.add("modifiedInitialPrice")
                            arrOfKeys3.add("modifiedMaintenancePrice")
                            arrOfKeys3.add("nonStdServiceTermsConditions")
                            
                            arrOfKeys3.add("internalNotes")
                            arrOfKeys3.add("isBidOnRequest")

                            
                            // values
                            
                            arrOfValues3.add("\(item.value(forKey: "problemIdentificationId")!)")
                            arrOfValues3.add("\(item.value(forKey: "mobileId")!)")
                            arrOfValues3.add(strUserName)
                            arrOfValues3.add(strCompanyKey)
                            arrOfValues3.add(strLeadId)
                            arrOfValues3.add(strEmpID)
                            arrOfValues3.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                            
                            arrOfValues3.add(strEmpID)
                            arrOfValues3.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                            
                            
                            arrOfValues3.add("\((itemPI as! NSMutableDictionary).value(forKey: "issuesCode") ?? "")")
                            
                            //let serviceDescription = "\((itemPI as AnyObject).value(forKey: "proposalDescription") ?? "")"
                            
                            // Replace tags
                            
                            let strFillingLocal = "\((itemPI as AnyObject).value(forKey: "recommendationFillIn") ?? "")"
                            
                            var strRecommendationLocal = "\((itemPI as AnyObject).value(forKey: "proposalDescription") ?? "")"
                            strRecommendationLocal = strRecommendationLocal.replacingOccurrences(of: "<b> NO BID GIVEN</b>", with: "")
                            
                            if(((itemPIPricing as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == true) && ("\((itemPIPricing as! NSMutableDictionary).value(forKey: "isNoBidGiven")!)".lowercased() == "true" || "\((itemPIPricing as! NSMutableDictionary).value(forKey: "isNoBidGiven")!)".lowercased() == "1")){
                                        let noBidGivenboldText  = "<b> NO BID GIVEN</b>"
                               
                                strRecommendationLocal = "\(strRecommendationLocal + noBidGivenboldText)"
                                     
                            }
                            
                            let strReplacedString = replaceTags(strFillingLocal: strFillingLocal, strFindingLocal: strRecommendationLocal)
               
                            arrOfValues3.add(strReplacedString)

                            
                            
                            
                            //arrOfValues3.add(serviceDescription)
                            
                            arrOfValues3.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "discount") ?? "")")
                            
                            
                            if((itemPI as! NSMutableDictionary).value(forKey: "isAddToAgreement") as! Bool == true)
                            {
                                arrOfValues3.add("true")
                            }
                            else
                            {
                                arrOfValues3.add("false")
                            }
                            
                            
                            arrOfValues3.add("OneTime")
                            arrOfValues3.add("One Time")
                            
                            
                            arrOfValues3.add("\(objWorkorderDetail.value(forKey: "departmentSysName") ?? "")")
                            
                            arrOfValues3.add("\(global.getReferenceNumber()!)")
                            
                            arrOfValues3.add("\((itemPIPricing as! NSMutableDictionary).value(forKey: "subTotal") ?? "")")
                            
                            
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            arrOfValues3.add("")
                            
                            var strInternalNotes = ""
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "hours") ?? "")".count > 0)
                            {
                                if(((itemPIPricing as! NSMutableDictionary).value(forKey: "hours") as! NSString).floatValue > 0)
                                {
                                    strInternalNotes = "Hours(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "hours") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtHourlyRateCreateBid.text!) ?? "")))"
                                }
                            }
                            
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "materials") ?? "")".count > 0)
                            {
                                if(strInternalNotes.count > 0)
                                {
                                    strInternalNotes = strInternalNotes + "," + "Materials(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "materials") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtMaterialMultiplierCreateBid.text!) ?? "")))"
                                }
                                else
                                {
                                    strInternalNotes = "Materials(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "materials") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtMaterialMultiplierCreateBid.text!) ?? "")))"
                                }
                            }
                            
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "subFee") ?? "")".count > 0)
                            {
                                if(strInternalNotes.count > 0)
                                {
                                    strInternalNotes = strInternalNotes + "," + "Sub Fee(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "subFee") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtSubFeeMultiplierCreateBid.text!) ?? "")))"
                                }
                                else
                                {
                                    strInternalNotes = "Sub Fee(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "subFee") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtSubFeeMultiplierCreateBid.text!) ?? "")))"
                                }
                                
                            }
                            
                            
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtSoilTreatment") ?? "")".count > 0)
                            {
                                if(strInternalNotes.count > 0)
                                {
                                    strInternalNotes = strInternalNotes + "," + "Linear Ft.Soil Treatment(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtSoilTreatment") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtLinearFtSoilTreatmentCreateBid.text!) ?? "")))"
                                }
                                else
                                {
                                    strInternalNotes = "Linear Ft.Soil Treatment(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtSoilTreatment") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtLinearFtSoilTreatmentCreateBid.text!) ?? "")))"
                                }
                            }
                            
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtDrilledConcrete") ?? "")".count > 0)
                            {
                                if(strInternalNotes.count > 0)
                                {
                                    strInternalNotes = strInternalNotes + "," + "Linear Ft.Soil Treatment(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtDrilledConcrete") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtLinearFtDrilledConcreteCreateBid.text!) ?? "")))"
                                    
                                }
                                else
                                {
                                    strInternalNotes = "Linear Ft.Drilled Concrete(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "linearFtDrilledConcrete") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtLinearFtDrilledConcreteCreateBid.text!) ?? "")))"
                                }
                            }
                            
                            if("\((itemPIPricing as! NSMutableDictionary).value(forKey: "cubicFt") ?? "")".count > 0)
                            {
                                if(strInternalNotes.count > 0)
                                {
                                    
                                    //strInternalNotes = strInternalNotes + "," + "Cubic Ft.(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "cubicFt") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtCubicFtRateCreateBid.text!) ?? "")))"
                                    
                                }
                                else
                                {
                                    //strInternalNotes = "Cubic Ft.(\(String(format: "%0.2f", ((itemPIPricing as! NSMutableDictionary).value(forKey: "cubicFt") as! NSString).floatValue)) * \(String(format: "%0.2f", Float(txtCubicFtRateCreateBid.text!) ?? "")))"
                                }
                            }
                            
                            arrOfValues3.add(strInternalNotes)
                            
                            if((itemPIPricing as! NSMutableDictionary).value(forKey: "isBidOnRequest") as! Bool == true)
                            {
                                arrOfValues3.add("true")
                            }
                            else
                            {
                                arrOfValues3.add("false")
                            }
                            
                            
                            arrOfKeys3.add("isLeadTest")
                            arrOfValues3.add("\(item.value(forKey: "isLeadTest") ?? "")")

                            arrOfKeys3.add("isUnderWarranty")
                            if("\(item.value(forKey: "isCoveredUnderWarranty") ?? "")".lowercased() == "true" || "\(item.value(forKey: "isCoveredUnderWarranty") ?? "")".lowercased() == "1")
                            {
                                arrOfValues3.add("true")
                            }
                            else
                            {
                                arrOfValues3.add("false")
                            }
                            
                  
                            
                            saveDataInDB(strEntity: "SoldServiceNonStandardDetail", arrayOfKey: arrOfKeys3, arrayOfValue: arrOfValues3)
                            print("pricing updated successfully")
                        }
                    }
                }
            }
        }
    }
    
    
    
    fileprivate func updateLeadDetails()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        arrOfKeys.add("strPreferredMonth")
        arrOfKeys.add("leadInspectionFee")

        // values
        arrOfValues.add(arrayServiceMonths.componentsJoined(by: ","))
        arrOfValues.add(txtLeadInspectionFee.text!)

        if(yesEditedSomething)
        {
            
            arrOfKeys.add("zSync")
            arrOfValues.add("yes")
            
        }
        
        if (isTipWarranty ) {
            
            let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
            
            if isMonthlyAmount {
                
                arrOfKeys.add("isTipEligible")
                arrOfValues.add("true")
                
            } else {
                
                arrOfKeys.add("isTipEligible")
                arrOfValues.add("false")
                
            }
            
        }
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    fileprivate func updatePaymentInfo()
    {
        // Update Payment Info
                
        let arrayAllObject = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if (arrayAllObject.count==0)
        {
            savePaymentInfo()
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("specialInstructions")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strEmpID)
            arrOfValues.add(global.modifyDate())
            arrOfValues.add(txtviewAdditionalNotes.text)
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    fileprivate func savePaymentInfo()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadPaymentDetailId")
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("amount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("licenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("specialInstructions")
        arrOfKeys.add("agreement")
        arrOfKeys.add("proposal")
        arrOfKeys.add("customerSignature")
        arrOfKeys.add("salesSignature")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strLeadId)
        arrOfValues.add("") // leadPaymentDetailId
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add(txtviewAdditionalNotes.text) // special instruction
        arrOfValues.add("") // agreement
        arrOfValues.add("") // proposal
        arrOfValues.add("")
        arrOfValues.add("")
        
        arrOfValues.add(strEmpID) // createdBy
        arrOfValues.add(global.modifyDate()) // createdDate
        arrOfValues.add(strEmpID) // modifiedBy
        arrOfValues.add(global.modifyDate())
        
        arrOfValues.add("")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "PaymentInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    //Navin Start
    func UpdatePriceChangeReason() {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if(arryOfData.count != 0){
           
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
         
            arrOfKeys.add("priceChangeReasonId")
            arrOfValues.add(txtPriceModificationReason.tag == 0 ? "" : "\(txtPriceModificationReason.tag)")
            
            if "\(txtPriceModificationReason.text ?? "")".lowercased() == strOtherStringlowerCase {
                arrOfKeys.add("priceChangeReasonOtherDescription")
                arrOfValues.add("\(txt_other_PriceModificationReason.text!)")
            }
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess
            {
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
        }
    }
    //Navin Start
    func UpdateRateMasterID_InWO() {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if(arryOfData.count != 0){
            let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "no", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
            
            let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)
            if aryRateMasterExtSerDcs.count > 0
            {
                let matchObject = aryRateMasterExtSerDcs.object(at: 0) as! NSDictionary
                let RateMasterId = "\(matchObject.value(forKey: "RateMasterId") ?? "")"
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
             
                arrOfKeys.add("rateMasterId")
                arrOfValues.add(RateMasterId)
            
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess
                {
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                }
            }
            
            
        
        }
    }
    //Navin End
    
    
    func updateWoDetail() {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if isUpdateApprovalParameter
        {
            
            arrOfKeys.add("isPricingApprovalPending")
            arrOfValues.add("true")
            
            arrOfKeys.add("isPricingApprovalMailSend")
            arrOfValues.add("false")
            
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifyDate")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().modifyDateService())
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().modifyDateService())
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess
            {
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
        }
    }
    

    // MARK: Fetch from local DB
    
    func fetchLetterTemplateFromLocalDB()
    {
        let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayLetterTemplate.count > 0)
        {
            let match = arrayLetterTemplate.firstObject as! NSManagedObject
            
            // cover letter
            if("\(match.value(forKey: "coverLetterSysName") ?? "")".count > 0)
            {
                for item in arrayCoverLetter
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "coverLetterSysName")!)")
                    {
                        dictCoverLetter = item as! NSDictionary
                        btnCoverLetter.setTitle("\(dictCoverLetter.value(forKey: "TemplateName") ?? "")", for: .normal)
                        break
                    }
                }
            }
            else
            {
                btnCoverLetter.setTitle("---Select---", for: .normal)
            }
            
            // Introduction letter
            
            if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
            {
                for item in arrayIntroduction
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                    {
                        dictIntroduction = item as! NSDictionary
                        
                        btnIntroduction.setTitle("\(dictIntroduction.value(forKey: "TemplateName")!)", for: .normal)
                        
                        //let description = "\(dictIntroduction.value(forKey: "TemplateContent")!)".trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        // Change By Saavan
                        
                        txtviewIntroduction.attributedText = getAttributedHtmlStringUnicode(strText: "\(match.value(forKey: "introContent")!)")//htmlAttributedString(strHtmlString: "\(match.value(forKey: "introContent")!)")
                        
                    }
                }
            }
            else
            {
                btnIntroduction.setTitle("---Select---", for: .normal)
            }
            
            // Terms Of Service
            if("\(match.value(forKey: "termsOfServiceSysName") ?? "")".count > 0)
            {
                for item in arrayTermsOfService
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "termsOfServiceSysName")!)")
                    {
                        dictTermsOfService = item as! NSDictionary
                        
                        btnTermsOfService.setTitle("\(dictTermsOfService.value(forKey: "Title")!)", for: .normal)
                        
                        txtviewTermsOfService.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictTermsOfService.value(forKey: "Description")!)")//htmlAttributedString(strHtmlString: "\(dictTermsOfService.value(forKey: "Description")!)")
                        
                    }
                }
            }
            else
            {
                btnTermsOfService.setTitle("---Select---", for: .normal)
            }
            
            // isAgreementvalidfor(Other)
            
            if("\(match.value(forKey: "isAgreementValidFor") ?? "")" == "true" || "\(match.value(forKey: "isAgreementValidFor") ?? "")" == "1" || "\(match.value(forKey: "isAgreementValidFor") ?? "")" == "True")
            {
                txtValidFor.isHidden = false
                btnAgreementValidFor.setImage(UIImage(named: "check_ipad"), for: .normal)
                txtValidFor.text = "\(match.value(forKey: "validFor") ?? "")"
            }
                
            else
            {
                txtValidFor.isHidden = true
                btnAgreementValidFor.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
            }
        }
    }
    
    
    fileprivate func fetchMarketingContentFromLocalDB()
    {
        let arrayMatches = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayMatches.count > 0)
        {
            for match in arrayMatches
            {
                for item in arrayMarketingContent
                {
                    if("\((match as! NSManagedObject).value(forKey: "contentSysName")!)" == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                    {
                        arrayMarketingContentMultipleSelected.add(item as! NSDictionary)
                        break
                    }
                }
            }
            if(arrayMarketingContentMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayMarketingContentMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "Title")!)" + ","
                }
                termsTitle.removeLast()
                btnSalesMarketingContent.setTitle(termsTitle, for: .normal)
            }
        }
    }
    
    fileprivate func fetchMultiTermsAndConditionsFromLocalDB()
    {
        let arrayMultiTermsConditions = getDataFromLocal(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayMultiTermsConditions.count > 0)
        {
            for match in arrayMultiTermsConditions
            {
                for item in arrayTermsAndConditions
                {
                    if("\((match as! NSManagedObject).value(forKey: "termsId")!)" == "\((item as! NSDictionary).value(forKey: "Id")!)")
                    {
                        if(arrayTermsAndConditionsMultipleSelected.contains(item as! NSDictionary) == false)
                        {
                            arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                            
                            break
                        }
                                                
                    }
                }
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayTermsAndConditionsMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
                }
                termsTitle.removeLast()
                btnTermsAndConditions.setTitle(termsTitle, for: .normal)
            }
        }
    }
    
    fileprivate func fetchLeadDetailFromLocalDB()
    {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayLeadDetail.count > 0)
        {
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            
            let strMonths = "\(match.value(forKey: "strPreferredMonth") ?? "")"
            
            if(strMonths.count > 0)
            {
                if(strMonths.contains(","))
                {
                    arrayServiceMonths = (strMonths.components(separatedBy: ",") as NSArray).mutableCopy() as! NSMutableArray
                }
                else
                {
                    arrayServiceMonths.add(strMonths)
                }
            }
            
            for item in arrayServiceMonths
            {
                if("\(item)".trimmingCharacters(in: .whitespaces) == "January")
                {
                    btnJanuary.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "February")
                {
                    btnFebruary.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "March")
                {
                    btnMarch.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "April")
                {
                    btnApril.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "May")
                {
                    btnMay.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "June")
                {
                    btnJune.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "July")
                {
                    btnJuly.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "August")
                {
                    btnAugust.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "September")
                {
                    btnSeptember.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "October")
                {
                    btnOctober.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "November")
                {
                    btnNovember.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "December")
                {
                    btnDecember.setImage(UIImage(named: "check_ipad"), for: .normal)
                }
            }
            
            strLeadStatusGlobal = "\(match.value(forKey: "statusSysName") ?? "")"
            
            strStageSysName = "\(match.value(forKey: "stageSysName") ?? "")"
            
            strAccountNo = "\(match.value(forKey: "accountNo") ?? "")"
            
            // Lead Inspetion Fee
            
            let strLeadInspectionFee = "\(match.value(forKey: "leadInspectionFee") ?? "")"
            
            let doubleLeadInspectionFee = (strLeadInspectionFee as NSString).doubleValue
            
            if doubleLeadInspectionFee > 0 {
                
                txtLeadInspectionFee.text = String(format: "%.2f", doubleLeadInspectionFee)
                
            }else {
                
                txtLeadInspectionFee.text = "0.00"
                
            }

        }
    }
    
    fileprivate func fetchWoWdoPricingMultiplierExtSerDcs()
    {
        
        // By Navin for Set Price Change Reason Value
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        if(arryOfData.count != 0){
            let wo_Object = arryOfData[0] as! NSManagedObject
            let priceModificationReasonID = "\(wo_Object.value(forKey: "priceChangeReasonId") ?? "")"
            let aryTemp = getPriceChangeReasonsMaster().filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "PriceChangeReasonId")!)" == priceModificationReasonID }
            if(aryTemp.count != 0){
                let dict  = aryTemp[0]as! NSDictionary
                txtPriceModificationReason.text = "\(dict.value(forKey: "Title") ?? "")"
                txtPriceModificationReason.tag = Int("\(dict.value(forKey: "PriceChangeReasonId") ?? "0")")!
                if "\(dict.value(forKey: "Title") ?? "")".lowercased() == strOtherStringlowerCase {
                    txt_other_PriceModificationReason.text = "\(wo_Object.value(forKey: "priceChangeReasonOtherDescription") ?? "")"
                    width_txt_other_PriceModificationReason.constant = self.view.frame.width/2
                }else{
                    width_txt_other_PriceModificationReason.constant = 0

                }
            }else{
                txtPriceModificationReason.text = ""
                txtPriceModificationReason.tag = 0
                width_txt_other_PriceModificationReason.constant = 0
            }
            
        }
     //End
        
        
        arrayMultiplier = getDataFromLocal(strEntity: Entity_WdoPricingMultipliers, predicate: NSPredicate(format: "workOrderId == %@",strWoId)).mutableCopy() as! NSArray
        
        if(arrayMultiplier.count > 0)
        {
            let obj = getMutableDictionaryFromNSManagedObject(obj: arrayMultiplier.firstObject as! NSManagedObject)
            
            txtHourlyRateCreateBid.text = String(format: "%.2f", (obj.value(forKey: "hourlyRate") as! NSString).floatValue)
            
            lbl_HourlyRateCreateBid.text = String(format: "%.2f", (obj.value(forKey: "hourlyRate") as! NSString).floatValue)
            
            txtMaterialMultiplierCreateBid.text = String(format: "%.2f", (obj.value(forKey: "materialMultiplier") as! NSString).floatValue)
            lbl_MaterialMultiplierCreateBid.text = String(format: "%.2f", (obj.value(forKey: "materialMultiplier") as! NSString).floatValue)

            
            txtSubFeeMultiplierCreateBid.text = String(format: "%.2f", (obj.value(forKey: "subFeeMultiplier") as! NSString).floatValue)
            lbl_SubFeeMultiplierCreateBid.text = String(format: "%.2f", (obj.value(forKey: "subFeeMultiplier") as! NSString).floatValue)

            
            txtLinearFtSoilTreatmentCreateBid.text = String(format: "%.2f", (obj.value(forKey: "linearFtSoilTreatmentRate") as! NSString).floatValue)
            lbl_LinearFtSoilTreatmentCreateBid.text = String(format: "%.2f", (obj.value(forKey: "linearFtSoilTreatmentRate") as! NSString).floatValue)

            
            txtLinearFtDrilledConcreteCreateBid.text = String(format: "%.2f", (obj.value(forKey: "linearFtDrilledConcreteRate") as! NSString).floatValue)
            lbl_LinearFtDrilledConcreteCreateBid.text = String(format: "%.2f", (obj.value(forKey: "linearFtDrilledConcreteRate") as! NSString).floatValue)

            
            txtCubicFtRateCreateBid.text = String(format: "%.2f", (obj.value(forKey: "cubicFtRate") as! NSString).floatValue)
            lbl_CubicFtRateCreateBid.text = String(format: "%.2f", (obj.value(forKey: "cubicFtRate") as! NSString).floatValue)

        
            
        }
        else
        {// from master
            
            var aryRateMasterExtSerDcs_temp = NSMutableArray()
            
            aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
          
            let ary = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

       
            if(ary.count > 0)
            {
                let obj = ary.firstObject as! NSDictionary
                
                if("\(obj.value(forKey: "HourlyRate") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "HourlyRate")!)" as NSString).floatValue
                    
                    txtHourlyRateCreateBid.text =  String(format: "%.2f", val)
                    lbl_HourlyRateCreateBid.text =  String(format: "%.2f", val)

                    
                }
                else
                {
                    txtHourlyRateCreateBid.text = "0.00"
                    lbl_HourlyRateCreateBid.text = "0.00"

                }
                if("\(obj.value(forKey: "MaterialMultiplier") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "MaterialMultiplier")!)" as NSString).floatValue
                    
                    txtMaterialMultiplierCreateBid.text =  String(format: "%.2f", val)
                    lbl_MaterialMultiplierCreateBid.text =  String(format: "%.2f", val)

                }
                else
                {
                    txtMaterialMultiplierCreateBid.text = "0.00"
                    lbl_MaterialMultiplierCreateBid.text = "0.00"

                }
                
                if("\(obj.value(forKey: "SubFeeMultiplier") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "SubFeeMultiplier")!)" as NSString).floatValue
                    
                    txtSubFeeMultiplierCreateBid.text =  String(format: "%.2f", val)
                    lbl_SubFeeMultiplierCreateBid.text =  String(format: "%.2f", val)

                }
                else
                {
                    txtSubFeeMultiplierCreateBid.text = "0.00"
                    lbl_SubFeeMultiplierCreateBid.text = "0.00"

                }
                if("\(obj.value(forKey: "SoilTreatmentRate") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "SoilTreatmentRate")!)" as NSString).floatValue
                    
                    txtLinearFtSoilTreatmentCreateBid.text =  String(format: "%.2f", val)
                    lbl_LinearFtSoilTreatmentCreateBid.text =  String(format: "%.2f", val)

                }
                else
                {
                    txtLinearFtSoilTreatmentCreateBid.text = "0.00"
                    lbl_LinearFtSoilTreatmentCreateBid.text = "0.00"

                }
                if("\(obj.value(forKey: "DrilledConcreteRate") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "DrilledConcreteRate")!)" as NSString).floatValue
                    
                    txtLinearFtDrilledConcreteCreateBid.text =  String(format: "%.2f", val)
                    lbl_LinearFtDrilledConcreteCreateBid.text =  String(format: "%.2f", val)

                }
                    
                else
                {
                    txtLinearFtDrilledConcreteCreateBid.text = "0.00"
                    lbl_LinearFtDrilledConcreteCreateBid.text = "0.00"

                }
                if("\(obj.value(forKey: "CubicRate") ?? "")".count > 0)
                {
                    let val = ("\(obj.value(forKey: "CubicRate")!)" as NSString).floatValue
                    
                    txtCubicFtRateCreateBid.text =  String(format: "%.2f", val)
                    lbl_CubicFtRateCreateBid.text =  String(format: "%.2f", val)

                }
                else
                {
                    txtCubicFtRateCreateBid.text = "0.00"
                    lbl_CubicFtRateCreateBid.text = "0.00"

                }
             
            }
        }
        
        
        //Check in range or not and show red color
        
        let aryRateMasterExtSerDcs_temp = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "RateMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        let aryRateMasterExtSerDcs = getRateMasterByID(aryMaster: aryRateMasterExtSerDcs_temp, id: "\(objWorkorderDetail.value(forKey: "rateMasterId") ?? "")", objWO: self.objWorkorderDetail)

        
        if(aryRateMasterExtSerDcs.count != 0)
        {
            let obj = aryRateMasterExtSerDcs.firstObject as! NSDictionary
            let MaxHourlyRate = Double("\(obj.value(forKey: "MaxHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxHourlyRate") ?? "0")" )
            let MinHourlyRate = Double("\(obj.value(forKey: "MinHourlyRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinHourlyRate") ?? "0")")
            let HourlyRate =  Double("\(lbl_HourlyRateCreateBid.text ?? "0")")
            
            if (MinHourlyRate != 0 && MaxHourlyRate != 0) && !(MinHourlyRate!...MaxHourlyRate! ~= HourlyRate!)
            {
                lbl_HourlyRateCreateBid.textColor = UIColor.red
                lbl_HourlyRateCreateBid_static.textColor = UIColor.red
                
            }else{
                lbl_HourlyRateCreateBid.textColor = UIColor.darkGray
                lbl_HourlyRateCreateBid_static.textColor = UIColor.black
            }
          
            
            
             let MaxMaterialMultiplier = Double("\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxMaterialMultiplier") ?? "0")")
             let MinMaterialMultiplier = Double("\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinMaterialMultiplier") ?? "0")")
             let MaterialMultiplier = Double("\(lbl_MaterialMultiplierCreateBid.text ?? "0")")
            
             if (MinMaterialMultiplier != 0 && MaxMaterialMultiplier != 0) && !(MinMaterialMultiplier!...MaxMaterialMultiplier! ~= MaterialMultiplier!)
             {
                lbl_MaterialMultiplierCreateBid.textColor = UIColor.red
                lbl_MaterialMultiplierCreateBid_static.textColor = UIColor.red
             }else{
                lbl_MaterialMultiplierCreateBid.textColor = UIColor.darkGray
                lbl_MaterialMultiplierCreateBid_static.textColor = UIColor.black
             }
             

            let MaxSubFeeMultiplier = Double("\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSubFeeMultiplier") ?? "0")")
            let MinSubFeeMultiplier = Double("\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSubFeeMultiplier") ?? "0")")
            let SubFeeMultiplier = Double("\(lbl_SubFeeMultiplierCreateBid.text ?? "0")")

            if (MinSubFeeMultiplier != 0 && MaxSubFeeMultiplier != 0) && !(MinSubFeeMultiplier!...MaxSubFeeMultiplier! ~= SubFeeMultiplier!)
            {
                lbl_SubFeeMultiplierCreateBid.textColor = UIColor.red
                lbl_SubFeeMultiplierCreateBid_static.textColor = UIColor.red
            }else{
                lbl_SubFeeMultiplierCreateBid.textColor = UIColor.darkGray
                lbl_SubFeeMultiplierCreateBid_static.textColor = UIColor.black
            }
    
           
            let MaxSoilTreatmentRate = Double("\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxSoilTreatmentRate") ?? "0")")
            let MinSoilTreatmentRate = Double("\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinSoilTreatmentRate") ?? "0")")
            let SoilTreatmentRate = Double("\(lbl_LinearFtSoilTreatmentCreateBid.text ?? "0")")
            
            if (MinSoilTreatmentRate != 0 && MaxSoilTreatmentRate != 0) && !(MinSoilTreatmentRate!...MaxSoilTreatmentRate! ~= SoilTreatmentRate!)
            {
                lbl_LinearFtSoilTreatmentCreateBid.textColor = UIColor.red
                lbl_LinearFtSoilTreatmentCreateBid_static.textColor = UIColor.red
            }else{
                lbl_LinearFtSoilTreatmentCreateBid.textColor = UIColor.darkGray
                lbl_LinearFtSoilTreatmentCreateBid_static.textColor = UIColor.black
            }
            
            let MaxDrilledConcreteRate = Double("\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxDrilledConcreteRate") ?? "0")")
            let MinDrilledConcreteRate = Double("\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinDrilledConcreteRate") ?? "0")")
            let DrilledConcreteRate = Double("\(lbl_LinearFtDrilledConcreteCreateBid.text ?? "0")")
            if (MinDrilledConcreteRate != 0 && MaxDrilledConcreteRate != 0) && !(MinDrilledConcreteRate!...MaxDrilledConcreteRate! ~= DrilledConcreteRate!)
            {
                lbl_LinearFtDrilledConcreteCreateBid.textColor = UIColor.red
                lbl_LinearFtDrilledConcreteCreateBid_static.textColor = UIColor.red
            }else{
                lbl_LinearFtDrilledConcreteCreateBid.textColor = UIColor.darkGray
                lbl_LinearFtDrilledConcreteCreateBid_static.textColor = UIColor.black
            }
           
            
            let MaxCubicRate = Double("\(obj.value(forKey: "MaxCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MaxCubicRate") ?? "0")")
            let MinCubicRate = Double("\(obj.value(forKey: "MinCubicRate") ?? "0")" == "" ? "0" : "\(obj.value(forKey: "MinCubicRate") ?? "0")")
            let CubicRate = Double("\(txtCubicFtRateCreateBid.text ?? "0")")

            if (MinCubicRate != 0 && MaxCubicRate != 0) && !(MinCubicRate!...MaxCubicRate! ~= CubicRate!)
            {
                lbl_CubicFtRateCreateBid.textColor = UIColor.red
                lbl_CubicFtRateCreateBid_static.textColor = UIColor.red
            }else{
                lbl_CubicFtRateCreateBid.textColor = UIColor.darkGray
                lbl_CubicFtRateCreateBid_static.textColor = UIColor.black
            }
       
        }
        
    }
    
    fileprivate func fetchWoWdoProblemIdentificationExtSerDcs()
    {
        
        arrayProblemIdentification.removeAllObjects()
        arrayProblemIdentificationPricing = NSMutableArray()
        
        let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
        let arrayPI = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isActive == YES", strWoId)), sort: sort)
        
        //let arrayPI = getDataFromLocal(strEntity: "WoWdoProblemIdentificationExtSerDcs", predicate: NSPredicate(format: "workOrderId == %@",strWoId)).mutableCopy() as! NSArray
        
        if(arrayPI.count > 0)
        {
            arrayProblemIdentification.removeAllObjects()
            arrayProblemIdentificationPricing = NSMutableArray()
            
            //arrayProblemIdentification = arrayPI.mutableCopy() as! NSMutableArray
            
            for obj in arrayPI
            {
                arrayProblemIdentification.add(getMutableDictionaryFromNSManagedObject(obj: obj as! NSManagedObject))
            }
            
            
            
            for item in arrayProblemIdentification
            {
                let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "problemIdentificationId")!)")).mutableCopy() as! NSArray
                
                
                if(arrayPIPricing.count > 0)
                {
                    arrayProblemIdentificationPricing.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                }else{
                    
                    let arrayPIPricing = getDataFromLocal(strEntity: "WoWdoProblemIdentificationPricingExtSerDc", predicate: NSPredicate(format: "problemIdentificationId == %@","\((item as! NSMutableDictionary).value(forKey: "mobileId")!)")).mutableCopy() as! NSArray
                    
                    if(arrayPIPricing.count > 0)
                    {
                        arrayProblemIdentificationPricing.add(getMutableDictionaryFromNSManagedObject(obj: arrayPIPricing.firstObject as! NSManagedObject))
                    }
                    
                }
            }
            DispatchQueue.main.async { [self] in
              
               tblviewConfigureProposal.reloadData()
                         
            }
           
        }
    }
    fileprivate func fetchPaymentInfoFromLocalDB()
    {
        let ary = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(ary.count > 0)
        {
            let match = ary.firstObject as! NSManagedObject
            
            if("\(match.value(forKey: "specialInstructions") ?? "")" .count > 0)
            {
                txtviewAdditionalNotes.text = "\(match.value(forKey: "specialInstructions") ?? "")"
                
            }
        }
    }
    
    
    
    
}

extension ConfigureProposal_WDOVC:UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard range.location == 0 else {
            return true
        }
        
        let newString = (textView.text as NSString).replacingCharacters(in: range, with: text) as NSString
        
        return newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        yesEditedSomething = true
        
        let obj = arrayProblemIdentification.object(at: textView.tag) as! NSMutableDictionary
        
        obj.setValue(textView.text, forKey: "recommendation")
        
        arrayProblemIdentification.replaceObject(at: textView.tag, with: obj)
        
        tblviewConfigureProposal.reloadData()
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -


extension ConfigureProposal_WDOVC:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMonthlyAmount  {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
            
            self.updateIsEditedMonthlyAmount(str: "true")

            isEditedTIPWarranty = true
           
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else if textField == txtLeadInspectionFee  {
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 10)
            let chk = decimalValidation(textField: textField, range: range, string: string)
            return chk
        }
        else if(textField == txtValidFor)
        {
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters) == nil
        }
        else if textField == txtHourlyRateCreateBid || textField == txtMaterialMultiplierCreateBid || textField == txtSubFeeMultiplierCreateBid || textField == txtLinearFtSoilTreatmentCreateBid || textField == txtLinearFtDrilledConcreteCreateBid || textField == txtCubicFtRateCreateBid  || textField == txtPriceModificationReason
        {
            isUpdateApprovalParameter = true
            isUpdateCreateBid = true
            
        }else if(textField == txtHourlyRateCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if(textField == txtMaterialMultiplierCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if(textField == txtSubFeeMultiplierCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if(textField == txtLinearFtSoilTreatmentCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if(textField == txtLinearFtDrilledConcreteCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if(textField == txtCubicFtRateCreateBid)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if (textField == txt_other_PriceModificationReason)
        {
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 500)
        }
        else {
            
            var cell: ConfigureProposalCell!
            if(textField.superview?.frame.size.height == 60)// discount textfield
            {
                cell = (textField.superview!.superview?.superview?.superview?.superview as! ConfigureProposalCell)
            }
            else
            {
                cell = (textField.superview!.superview?.superview?.superview?.superview as! ConfigureProposalCell)
            }

            if textField == cell.txtHrs || textField == cell.txtMaterials || textField == cell.txtSubfee || textField == cell.txtLinearFtSoilTreatment || textField == cell.txtFtDrilledConcrete || textField == cell.txtCubicFt || textField == cell.txtTotal || textField == cell.txtSubTotal
            {
                
                isUpdatedPrice = true

            }
            
        }
        let chk = decimalValidation(textField: textField, range: range, string: string)
        
        if string == "" && textField.text!.count > 12
        {
            return true
        }
        
        return chk
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        yesEditedSomething = true

        if(textField == txtHourlyRateCreateBid || textField == txtMaterialMultiplierCreateBid || textField == txtSubFeeMultiplierCreateBid || textField == txtLinearFtSoilTreatmentCreateBid || textField == txtLinearFtDrilledConcreteCreateBid || textField == txtCubicFtRateCreateBid){
            if(textField.text == ""){
                textField.text = "0.00"
            }
            tblviewConfigureProposal.reloadData()
        }
        else if (textField == txtValidFor)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if (textField == txtMonthlyAmount)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if (textField == txtLeadInspectionFee)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else if (textField == txt_other_PriceModificationReason)
        {
            // yaha kuch nh karna h is field ki value saveAction per save ho rahi h local db me
        }
        else
        {
            var cell: ConfigureProposalCell!
            if(textField.superview?.frame.size.height == 60)// discount textfield
            {
                cell = (textField.superview!.superview?.superview?.superview?.superview as! ConfigureProposalCell)
            }
            else
            {
                cell = (textField.superview!.superview?.superview?.superview?.superview as! ConfigureProposalCell)
            }

            let obj = arrayProblemIdentificationPricing.object(at: textField.tag) as! NSMutableDictionary

            if(textField == cell.txtHrs)
            {
                if((textField.text ?? "").count > 0)
                {
                    let hrs = (textField.text! as NSString).doubleValue

                    obj.setValue(String(format: "%.2f", hrs), forKey: "hours")
                    
                }
                else
                {
                    obj.setValue("0.00", forKey: "hours")
                }

            }
            else if(textField == cell.txtMaterials)
            {
                
                if((textField.text ?? "").count > 0)
                {
                    let materials = (textField.text! as NSString).doubleValue

                    obj.setValue(String(format: "%.2f", materials), forKey: "materials")
                }
                else
                {
                    obj.setValue("0.00", forKey: "materials")
                }
    
            }
            else if(textField == cell.txtSubfee)
            {
                if((textField.text ?? "").count > 0)
                {
                    let subfee = (textField.text! as NSString).doubleValue

                    obj.setValue(String(format: "%.2f", subfee), forKey: "subFee")
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "subFee")
                    
                }
                obj.setValue(false, forKey: "isChangedCubicRate")

            }
            else if(textField == cell.txtLinearFtSoilTreatment)
            {

                if((textField.text ?? "").count > 0)
                {
                    let val = (textField.text! as NSString).doubleValue
                    obj.setValue(String(format: "%.2f", val), forKey: "linearFtSoilTreatment")
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "linearFtSoilTreatment")
                    
                }
                
            }
            else if(textField == cell.txtFtDrilledConcrete)
            {
                if((textField.text ?? "").count > 0)
                {
                    
                    let val = (textField.text! as NSString).doubleValue
                    
                    obj.setValue(String(format: "%.2f", val), forKey: "linearFtDrilledConcrete")
                    
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "linearFtDrilledConcrete")
                    
                }
                
            }
            else if(textField == cell.txtCubicFt)
            {
                
                if((textField.text ?? "").count > 0)
                    
                {
                    
                    let val = (textField.text! as NSString).doubleValue
                    
                    obj.setValue(String(format: "%.2f", val), forKey: "cubicFt")
                    
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "cubicFt")
                    
                }
                obj.setValue(true, forKey: "isChangedCubicRate")
                
                // Changes subfee value also
                
                if let subFeeMultiplier = txtCubicFtRateCreateBid.text , let cubicFt = textField.text
                {
                    
                    var totalSubFeeLocal = 0.0
                    totalSubFeeLocal = Double((subFeeMultiplier as NSString).floatValue * (cubicFt as NSString).floatValue)
                    obj.setValue(String(format: "%.2f", totalSubFeeLocal), forKey: "subFee")
                    
                }

            }
            else if(textField == cell.txtDiscount)
            {
                
                if((textField.text ?? "").count > 0)
                {
                    
                    let discount = (textField.text! as NSString).doubleValue
                   
                    obj.setValue(String(format: "%.2f", discount), forKey: "discount")
                    
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "discount")
                    
                }
                
            }
            else if(textField == cell.txtSubTotal)
            {
                
                if((textField.text ?? "").count > 0)
                {
                    
                    let discount = (textField.text! as NSString).doubleValue
                    
                    obj.setValue(String(format: "%.2f", discount), forKey: "subTotal")
                    
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "subTotal")
                    
                }
                
            }
            else if(textField == cell.txtTotal)
            {
                
                if((textField.text ?? "").count > 0)
                {
                    
                    let discount = (textField.text! as NSString).doubleValue
                    
                    obj.setValue(String(format: "%.2f", discount), forKey: "total")
                    
                }
                else
                {
                    
                    obj.setValue("0.00", forKey: "total")
                    
                }
                
            }
            arrayProblemIdentificationPricing.replaceObject(at: textField.tag, with: obj)
            
           
            DispatchQueue.main.async { [self] in
                tblviewConfigureProposal.reloadData()
            }
        }
        
    }
    
}

extension ConfigureProposal_WDOVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayProblemIdentification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfigureProposalCell") as! ConfigureProposalCell
        cell.viewNoBidGiven.isHidden = true

        //for hide discount buttion
        cell.btnDiscount.isHidden = true
        
        // configure border of view
        makeCornerRadius(value: 2.0, view: cell.viewContainer, borderWidth: 1.0, borderColor: UIColor.lightGray)
        //makeCornerRadius(value: 2.0, view: cell.txtviewDescription, borderWidth: 1.0, borderColor: UIColor.lightGray)
        
        let dataPI = arrayProblemIdentification.object(at: indexPath.row) as! NSMutableDictionary
        
        let dataPIPricing = arrayProblemIdentificationPricing.object(at: indexPath.row) as! NSMutableDictionary
        
        var totalCalculatedAmt = 0.0
        var discoutVal = 0.0
        
        // assigning tag
        cell.lblIssueCode.text = "\(dataPI.value(forKey: "issuesCode") ?? "")"
        //cell.txtviewDescription.text = "\(dataPI.value(forKey: "pricingDescription") ?? "")"

        let strFillingLocal = "\(dataPI.value(forKey: "recommendationFillIn") ?? "")"
        
        let strRecommendationLocal = "\(dataPI.value(forKey: "proposalDescription") ?? "")"
        
        var strReplacedString = replaceTags(strFillingLocal: strFillingLocal, strFindingLocal: strRecommendationLocal)
       
      
        
        cell.txtviewDescription.attributedText = attributedTextFontUpdate(str: getAttributedHtmlStringUnicode(strText: strReplacedString), strMinFont: fontMin)
        
        
        

        let isBidOnRequest = dataPIPricing.value(forKey: "isBidOnRequest") as? Bool
      
      
        
        let isNoBidGiven = "\(dataPIPricing.value(forKey: "isNoBidGiven") ?? "")"
        if(isBidOnRequest! && (isNoBidGiven.lowercased() == "true" || isNoBidGiven == "1")){
                strReplacedString =  strReplacedString.replacingOccurrences(of: "NO BID GIVEN", with: "")

                let noBidGivenboldText  = " NO BID GIVEN"
                let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
                let boldString = NSMutableAttributedString(string: noBidGivenboldText, attributes:attrs)
                let atributed = getAttributedHtmlStringUnicode(strText: strReplacedString).mutableCopy() as! NSMutableAttributedString
                atributed.append(boldString)
                cell.txtviewDescription.attributedText  = attributedTextFontUpdate(str: atributed, strMinFont: fontMin)
            
            
            }
     
  
        //htmlAttributedString(strHtmlString: strReplacedString)
//        cell.txtviewDescription.layer.borderWidth = 1.0
//        cell.txtviewDescription.layer.borderColor = UIColor.lightGray.cgColor
        cell.txtHrs.text = "\(dataPIPricing.value(forKey: "hours") ?? "")"
        cell.txtMaterials.text = "\(dataPIPricing.value(forKey: "materials") ?? "")"
        cell.txtSubfee.text = "\(dataPIPricing.value(forKey: "subFee") ?? "")"
        cell.txtLinearFtSoilTreatment.text = "\(dataPIPricing.value(forKey: "linearFtSoilTreatment") ?? "")"
        cell.txtFtDrilledConcrete.text = "\(dataPIPricing.value(forKey: "linearFtDrilledConcrete") ?? "")"
        cell.txtCubicFt.text = "\(dataPIPricing.value(forKey: "cubicFt") ?? "")"
        
        
        if let hourlyRate = txtHourlyRateCreateBid.text , let hours = cell.txtHrs.text
        {
            totalCalculatedAmt = Double((hourlyRate as NSString).floatValue * (hours as NSString).floatValue)
        }
        
        if let materialMultiplier = txtMaterialMultiplierCreateBid.text , let material = cell.txtMaterials.text
        {
            totalCalculatedAmt = totalCalculatedAmt + Double((materialMultiplier as NSString).floatValue * (material as NSString).floatValue)
        }
        
        /*if let subFeeMultiplier = txtSubFeeMultiplierCreateBid.text , let subFee = cell.txtSubfee.text
        {
            totalCalculatedAmt = totalCalculatedAmt + Double((subFeeMultiplier as NSString).floatValue * (subFee as NSString).floatValue)
        }*/
        
        // Changes For Sub - Fee
        
        if let subFeeMultiplier = txtCubicFtRateCreateBid.text , let cubicFt = cell.txtCubicFt.text
        {
            
            var totalSubFeeLocal = 0.0
            totalSubFeeLocal = Double((subFeeMultiplier as NSString).floatValue * (cubicFt as NSString).floatValue)
            
            if(dataPIPricing.value(forKey: "isChangedCubicRate") as! Bool == true)
            {
                
                cell.txtSubfee.text = "\(totalSubFeeLocal)"

            }
            //cell.txtSubfee.text = "\(totalSubFeeLocal)"
            
        }
        
        if let subFeeMultiplier = txtSubFeeMultiplierCreateBid.text , let subFee = cell.txtSubfee.text
        {
            totalCalculatedAmt = totalCalculatedAmt + Double((subFeeMultiplier as NSString).floatValue * (subFee as NSString).floatValue)
        }
        
        if let linearFtSoilTreatmentRate = txtLinearFtSoilTreatmentCreateBid.text , let linearFtSoilTreatmente = cell.txtLinearFtSoilTreatment.text
        {
            totalCalculatedAmt = totalCalculatedAmt + Double((linearFtSoilTreatmentRate as NSString).floatValue * (linearFtSoilTreatmente as NSString).floatValue)
        }
        
        if let linearFtDrillConcreteRate = txtLinearFtDrilledConcreteCreateBid.text , let linearFtDrillConcrete = cell.txtFtDrilledConcrete.text
        {
            totalCalculatedAmt = totalCalculatedAmt + Double((linearFtDrillConcreteRate as NSString).floatValue * (linearFtDrillConcrete as NSString).floatValue)
        }
        
        if let cubicFtRate = txtCubicFtRateCreateBid.text , let cubicFt = cell.txtCubicFt.text
        {
            //totalCalculatedAmt = totalCalculatedAmt + Double((cubicFtRate as NSString).floatValue * (cubicFt as NSString).floatValue)
        }
        
        if(dataPIPricing.value(forKey: "isCalculate") as! Bool == true)
        {
            
            cell.txtSubTotal.text = String(format: "%.2f", totalCalculatedAmt)
            dataPIPricing.setValue(cell.txtSubTotal.text, forKey: "subTotal")

            cell.btnCalculate.setImage(UIImage(named: "checked.png"), for: .normal)
            cell.txtHrs.isUserInteractionEnabled = true
            cell.txtMaterials.isUserInteractionEnabled = true
            cell.txtSubfee.isUserInteractionEnabled = true
            cell.txtLinearFtSoilTreatment.isUserInteractionEnabled = true
            cell.txtFtDrilledConcrete.isUserInteractionEnabled = true
            cell.txtCubicFt.isUserInteractionEnabled = true
            cell.txtSubTotal.isUserInteractionEnabled = false

            if(dataPIPricing.value(forKey: "isDiscount") as! Bool == true)
            {
                cell.btnDiscount.setImage(UIImage(named: "check_ipad"), for: .normal)
                cell.txtDiscount.isHidden = false
                cell.txtTotal.isHidden = false
                cell.txtSubTotal.labelPlaceholder.text = "SubTotal($)"
                
                if("\(dataPIPricing.value(forKey: "discount") ?? "")".count > 0)
                {
                    discoutVal = ((dataPIPricing.value(forKey: "discount") as! NSString).doubleValue)
                    
                    if((totalCalculatedAmt - discoutVal) < 0)
                    {
                        cell.txtTotal.text = "0.00"
                    }
                    else
                    {
                        cell.txtTotal.text = String(format: "%.2f", totalCalculatedAmt - discoutVal)
                    }
                    
                    let disc = (dataPIPricing.value(forKey: "discount") as! NSString).doubleValue
                    
                    cell.txtDiscount.text = String(format: "%.2f", disc)
                    
                    dataPIPricing.setValue(cell.txtTotal.text, forKey: "total")
                    
                }
                else
                {
                    
                    discoutVal = 0.0
                    
                    cell.txtDiscount.text = "0.00"
                    
                    cell.txtTotal.text = String(format: "%.2f", totalCalculatedAmt)
                    
                    dataPIPricing.setValue(cell.txtTotal.text, forKey: "total")
                    
                }
            }
            else
            {
                
                cell.btnDiscount.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                cell.txtDiscount.isHidden = true
                cell.txtTotal.isHidden = true
                cell.txtSubTotal.labelPlaceholder.text = "Total($)"
                
            }
        }
        else{
            
            cell.btnCalculate.setImage(UIImage(named: "uncheck.png"), for: .normal)
            cell.txtHrs.isUserInteractionEnabled = false
            cell.txtMaterials.isUserInteractionEnabled = false
            cell.txtSubfee.isUserInteractionEnabled = false
            cell.txtLinearFtSoilTreatment.isUserInteractionEnabled = false
            cell.txtFtDrilledConcrete.isUserInteractionEnabled = false
            cell.txtCubicFt.isUserInteractionEnabled = false
            cell.txtSubTotal.isUserInteractionEnabled = true
            
            var totalCalculatedAmtNew = 0.0
            
            cell.txtSubTotal.text = "\(dataPIPricing.value(forKey: "subTotal") ?? "")"
            
            totalCalculatedAmtNew = Double((cell.txtSubTotal.text! as NSString).floatValue)

            if(dataPIPricing.value(forKey: "isDiscount") as! Bool == true)
            {
                cell.btnDiscount.setImage(UIImage(named: "check_ipad"), for: .normal)
                cell.txtDiscount.isHidden = false
                cell.txtTotal.isHidden = false
                cell.txtSubTotal.labelPlaceholder.text = "SubTotal($)"
                
                if("\(dataPIPricing.value(forKey: "discount") ?? "")".count > 0)
                {
                    discoutVal = ((dataPIPricing.value(forKey: "discount") as! NSString).doubleValue)
                    
                    if((totalCalculatedAmtNew - discoutVal) < 0)
                    {
                        cell.txtTotal.text = "0.00"
                    }
                    else
                    {
                        cell.txtTotal.text = String(format: "%.2f", totalCalculatedAmtNew - discoutVal)
                    }
                    
                    let disc = (dataPIPricing.value(forKey: "discount") as! NSString).doubleValue
                    
                    cell.txtDiscount.text = String(format: "%.2f", disc)
                    
                    dataPIPricing.setValue(cell.txtTotal.text, forKey: "total")
                    
                }
                else
                {
                    discoutVal = 0.0
                    
                    cell.txtDiscount.text = "0.00"
                    
                    cell.txtTotal.text = String(format: "%.2f", totalCalculatedAmtNew)
                    
                    dataPIPricing.setValue(cell.txtTotal.text, forKey: "total")
                    
                }
            }
            else
            {
                cell.btnDiscount.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
                cell.txtDiscount.isHidden = true
                cell.txtTotal.isHidden = true
                cell.txtSubTotal.labelPlaceholder.text = "Total($)"
            }
            
        }
        
        if(dataPIPricing.value(forKey: "isBidOnRequest") as! Bool == true)
        {
            cell.btnBidOnRequest.setImage(UIImage(named: "checked.png"), for: .normal)
            cell.viewNoBidGiven.isHidden = false
            let isNoBidGiven = "\(dataPIPricing.value(forKey: "isNoBidGiven") ?? "")"
            if(isNoBidGiven.lowercased() == "true" || isNoBidGiven == "1")
            {
                cell.btnNoBidGiven.setImage(UIImage(named: "checked.png"), for: .normal)
            }else{
                cell.btnNoBidGiven.setImage(UIImage(named: "uncheck.png"), for: .normal)

            }

        }else{
            cell.btnBidOnRequest.setImage(UIImage(named: "uncheck.png"), for: .normal)
            cell.viewNoBidGiven.isHidden = true
            cell.btnNoBidGiven.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
        
        
        cell.txtviewDescription.tag = indexPath.row
        cell.txtHrs.tag = indexPath.row
        cell.txtMaterials.tag = indexPath.row
        cell.txtSubfee.tag = indexPath.row
        cell.txtLinearFtSoilTreatment.tag = indexPath.row
        cell.txtFtDrilledConcrete.tag = indexPath.row
        cell.txtCubicFt.tag = indexPath.row
        cell.txtDiscount.tag = indexPath.row
        cell.btnDiscount.tag = indexPath.row
        cell.btnCalculate.tag = indexPath.row
        cell.btnEditProposalDesc.tag = indexPath.row
        cell.txtTotal.tag = indexPath.row
        cell.txtSubTotal.tag = indexPath.row
        cell.btnBidOnRequest.tag = indexPath.row
        cell.btnNoBidGiven.tag = indexPath.row

        cell.btnDiscount.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_Discount(_:)), for: .touchUpInside)
        
        cell.btnCalculate.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_Calculate(_:)), for: .touchUpInside)

        cell.btnEditProposalDesc.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_EditPropsalDesc(_:)), for: .touchUpInside)

        cell.btnBidOnRequest.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_BidOnRequest(_:)), for: .touchUpInside)
        cell.btnNoBidGiven.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_NoBidGiven(_:)), for: .touchUpInside)

        //
        cell.txtTotal.isUserInteractionEnabled = false
        
        /*  cell.btnAddToAgreement.tag = indexPath.row
         
         cell.btnAddToAgreement.isUserInteractionEnabled = true
         
         cell.btnAddToAgreement.addTarget(self, action: #selector(ConfigureProposal_WDOVC.action_AddToAgreement(_:)), for: .touchUpInside)
         
         if let isAgreement = dataPI.value(forKey: "isAddToAgreement")
         {
         print(isAgreement)
         
         if(dataPI.value(forKey: "isAddToAgreement") as! Bool == true)
         {
         cell.btnAddToAgreement.setImage(UIImage(named: "check_ipad"), for: .normal)
         }
         else
         {
         cell.btnAddToAgreement.setImage(UIImage(named: "uncheck_ipad"), for: .normal)
         }
         }*/
        
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dictData = arrayProblemIdentificationPricing[indexPath.row] as! NSDictionary
        
        let isBidOnRequestLocal = dictData.value(forKey: "isBidOnRequest") as! Bool
        
        if isBidOnRequestLocal {
            
            return 285
            

        } else {
            
            return 550

        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

extension ConfigureProposal_WDOVC : CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        yesEditedSomething = true
        if tag == 45 {
            
            isEditedTIPWarranty = true
            
            if dictData["TIPWarrantyType"] as? String  == strSelectString {
                
                dictOfTipWarranty = NSDictionary()
                btnTipWarrantyType.setTitle("\(strSelectString)", for: .normal)
                strTipMasterId = ""
                strTipMasterName = ""
                
                // check to show monthly amount textfireld IsMonthlyPrice
                txtMonthlyAmount.isHidden = true
                txtMonthlyAmount.text = ""
                
            }else{
                
                dictOfTipWarranty = dictData
                btnTipWarrantyType.setTitle("\(dictOfTipWarranty.value(forKey: "TIPWarrantyType")!)", for: .normal)
                strTipMasterId = "\(dictOfTipWarranty.value(forKey: "TIPMasterId")!)"
                strTipMasterName = "\(dictOfTipWarranty.value(forKey: "TIPWarrantyType")!)"
                
                // check to show monthly amount textfireld IsMonthlyPrice
                
                let isMonthlyAmount = dictOfTipWarranty.value(forKey: "IsMonthlyPrice") as! Bool
                
                if isMonthlyAmount {
                    
                    txtMonthlyAmount.isHidden = false
                    
                } else {
                    
                    txtMonthlyAmount.isHidden = true
                    
                }
                
            }
            
        }
        if(tag == 101)// Cover Letter
        {
            btnCoverLetter.setTitle("\(dictData.value(forKey: "TemplateName")!)", for: .normal)
            dictCoverLetter = dictData

        }
        
        if(tag == 102)// Introduction
        {
            btnIntroduction.setTitle("\(dictData.value(forKey: "TemplateName")!)", for: .normal)
            
            dictIntroduction = dictData
            
            txtviewIntroduction.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictData.value(forKey: "TemplateContent")!)")//htmlAttributedString(strHtmlString: "\(dictData.value(forKey: "TemplateContent")!)")
            
        }
        if(tag == 103)// marketing content
        {
            
            arrayMarketingContentMultipleSelected = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            var termsTitle = ""
            for item in arrayMarketingContentMultipleSelected
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "Title")!)" + ","
            }
            if(arrayMarketingContentMultipleSelected.count > 0)
            {
                termsTitle.removeLast()
                btnSalesMarketingContent.setTitle(termsTitle, for: .normal)
            }
            else
            {
                btnSalesMarketingContent.setTitle("---Select---", for: .normal)
            }
            
        }
        if(tag == 104)// terms of service
        {
            btnTermsOfService.setTitle("\(dictData.value(forKey: "Title")!)", for: .normal)
            
            dictTermsOfService = dictData
            
            txtviewTermsOfService.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictData.value(forKey: "Description")!)")//htmlAttributedString(strHtmlString: "\(dictData.value(forKey: "Description")!)")
            
        }
        if(tag == 105)// terms and conditions
        {
            arrayTermsAndConditionsMultipleSelected = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            var termsTitle = ""
            
            for item in arrayTermsAndConditionsMultipleSelected
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                termsTitle.removeLast()
                btnTermsAndConditions.setTitle(termsTitle, for: .normal)
            }
            else
            {
                btnTermsAndConditions.setTitle("---Select---", for: .normal)
            }
        }
        if(tag == 106)
        {
            if dictData.count != 0 {
                txtPriceModificationReason.text = "\(dictData.value(forKey: "Title") ?? "")"
                txtPriceModificationReason.tag = Int("\(dictData.value(forKey: "PriceChangeReasonId") ?? "0")")!
                if "\(dictData.value(forKey: "Title") ?? "")".lowercased() == strOtherStringlowerCase {
                    txt_other_PriceModificationReason.text = ""
                    width_txt_other_PriceModificationReason.constant = self.view.frame.width/2
                }else{
                    width_txt_other_PriceModificationReason.constant = 0

                }
            }else{
                txtPriceModificationReason.text = ""
                txtPriceModificationReason.tag = 0
            }
        }
    }
    
}

