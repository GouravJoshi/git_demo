//
//  WDO_Graph_iPhoneVC.swift
//  DPS
//
//  Created by NavinPatidar on 6/8/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  saavan test

import UIKit
 import WebKit
class WDO_Graph_iPhoneVC: UIViewController {

    @IBOutlet weak var graphDrawingWebView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadWebGraphView()
      

    }
    
    // MARK:
       // MARK: ----------IBAction
       @IBAction func actionOnBack(_ sender: UIButton) {
           self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)

       }
   func loadWebGraphView() {
       
       self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

       self.graphDrawingWebView.scrollView.bounces = false
       self.graphDrawingWebView.allowsBackForwardNavigationGestures = false
       
       //let xmlFileName = (self.strWoId as String) + ".xml"
     //  var isXmlFileExists = false
       
//       var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
//
//       if arrayOfImagesXML.count == 0 {
//           arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
//       }
//       if arrayOfImagesXML.count == 0 {
//           arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
// }
//
//  if arrayOfImagesXML.count > 0 {
//     let dictData = arrayOfImagesXML[0] as! NSManagedObject
//     let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
//
//  isXmlFileExists = checkIfImageExistAtPath(strFileName: xmlFileName)
//
//
//       }
//
       let bundle = Bundle.main
       
       let filename = "index"
       let fileExtension = "html"
       let directory = "graphing"
       
     //  let isGraphDebugEnabled = nsud.bool(forKey: "graphDebug")
//       
//       if isGraphDebugEnabled {
//           
//           // general catermite
//           
//           let url = URL(string: "https://graphing.z30.web.core.windows.net/graphing/index.html?selectedCategory=catermite")!
//           self.graphDrawingWebView.load(URLRequest(url: url))
//           
//       } else {
           
           let indexUrl = bundle.url(forResource: filename, withExtension: fileExtension, subdirectory: directory)
           
           let fullUrl = URL(string: "?selectedCategory=catermite", relativeTo: indexUrl)
           let request = URLRequest(url: fullUrl!)
           self.graphDrawingWebView.load(request)
           
       //}
       
       
       self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"

     self.graphDrawingWebView.scrollView.isScrollEnabled = true
       
       self.view.layoutIfNeeded()
       
    //  if isXmlFileExists {
    //   RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
    //   self.loadSavedXMLGraph()
    // }else{
    //RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
    //loadGraphCategory()
    // }
    }
}
