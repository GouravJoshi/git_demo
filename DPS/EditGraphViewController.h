//
//  EditGraphViewController.h
//  DPS
//
//  Created by Saavan Patidar on 09/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//fdgfdgfd

#import <UIKit/UIKit.h>
#import "AllImportsViewController.h"

@interface EditGraphViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityTask;
    NSEntityDescription *entityLeadDetail,
    *entityImageDetail,
    *entityEmailDetail,
    *entityPaymentInfo,
    *entityLeadPreference,
    *entitySoldServiceNonStandardDetail,
    *entityCurrentService,
    *entitySoldServiceStandardDetail;

}
@property (weak, nonatomic) IBOutlet UIView *viewSignature;
- (IBAction)actionOnSaveSign:(id)sender;
- (IBAction)actionOnClearSign:(id)sender;
- (IBAction)actionOnExitSign:(id)sender;
@property (nonatomic, strong) UIImageView *mySignatureImage;
@property (nonatomic, assign) CGPoint lastContactPoint1, lastContactPoint2, currentPoint;
@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) BOOL fingerMoved;
@property (weak, nonatomic) IBOutlet UILabel *lblInpectorName;
@property(weak,nonatomic)NSString *strType;
@property(weak,nonatomic)UIImage *imageToEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)action_SelectColor:(id)sender;
- (IBAction)action_SelectWidth:(id)sender;
@property (strong, nonatomic) NSString *strLeadId;
@property (strong, nonatomic) NSString *strCompanyKey;
@property (strong, nonatomic) NSString *strUserName;
- (IBAction)action_UploadGraph:(id)sender;

@end
