//
//  GeneralInfoAppointmentView.h
//  DPS
//  
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Patidar 2021 Ji
//  Saavan Patidar 2021
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface GeneralInfoAppointmentView : UIViewController<NSFetchedResultsControllerDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSEntityDescription *entityWorkOrder;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNewWorkOrder;
    NSSortDescriptor *sortDescriptorWorkOrder;
    NSArray *sortDescriptorsWorkOrder;
    NSManagedObject *matchesWorkOrder;
    NSArray *arrAllObjWorkOrder;
    
    NSEntityDescription *entityImageDetail,*entityModifyDateServiceAuto,*entityEmailDetailService;
    NSFetchRequest *requestImageDetail;
    NSSortDescriptor *sortDescriptorImageDetail;
    NSArray *sortDescriptorsImageDetail;
    NSManagedObject *matchesImageDetail;
    NSArray *arrAllObjImageDetail;

    NSFetchRequest *requestServiceModifyDate;
    NSSortDescriptor *sortDescriptorServiceModifyDate;
    NSArray *sortDescriptorsServiceModifyDate;
    NSManagedObject *matchesServiceModifyDate;
    NSArray *arrAllObjServiceModifyDate;

    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;

    NSEntityDescription *entityMechanicalServiceAddressPOCDetailDcs,*entityMechanicalBillingAddressPOCDetailDcs;
    NSFetchRequest *requestMechanicalServiceAddressPOCDetailDcs,*requestMechanicalBillingAddressPOCDetailDcs;
    NSSortDescriptor *sortDescriptorMechanicalServiceAddressPOCDetailDcs,*sortDescriptorMechanicalBillingAddressPOCDetailDcs;
    NSArray *sortDescriptorsMechanicalServiceAddressPOCDetailDcs,*sortDescriptorsMechanicalBillingAddressPOCDetailDcs;
    NSManagedObject *matchesMechanicalServiceAddressPOCDetailDcs,*matchesMechanicalBillingAddressPOCDetailDcs;
    NSArray *arrAllObjMechanicalServiceAddressPOCDetailDcs,*arrAllObjMechanicalBillingAddressPOCDetailDcs;

}
- (IBAction)action_Back:(id)sender;
- (IBAction)action_PreviewBeforeImage:(id)sender;
- (IBAction)action_SavenContinue:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@property (strong, nonatomic)  NSDictionary *dictOfWorkOrders;
@property (strong, nonatomic) IBOutlet UILabel *lbl_FName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_CompanyName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PrimEmail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SecEmail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_PrimePhone;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SeconPhone;
@property (strong, nonatomic) IBOutlet UILabel *lbl_WorkOrderNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SubCategory;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Services;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EmpNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SchStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SchEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_BillingAddress;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ServiceAddress;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SpecialInstruction;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ServiceInstruction;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Directions;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Attributes;
@property (strong, nonatomic) IBOutlet UILabel *lbl_OtherInstructions;
@property (strong, nonatomic) IBOutlet UILabel *lbl_TechComment;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerImageDetail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_StartTime;
- (IBAction)action_StartTime:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_NoEmail;
- (IBAction)action_NoEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryEmailId;
- (IBAction)hideKeyBoard:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVieww;
@property (strong, nonatomic) IBOutlet UIView *view_SavenContinue;
@property (strong, nonatomic) IBOutlet UIView *view_ResetReasons;
@property (strong, nonatomic) IBOutlet UITextView *txtView_ResetDescription;
@property (strong, nonatomic) IBOutlet UIButton *btn_ResetReason;
- (IBAction)action_ResetReasonMaster:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_Status;
- (IBAction)action_ServiceDocuments:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txtView_TechComment;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccountNo;
- (IBAction)action_BtnStatus:(id)sender;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceModifyDate;
@property (strong, nonatomic) IBOutlet UIButton *btn_StartTime;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblOne;
@property (strong, nonatomic) IBOutlet UILabel *lblTwo;
@property (strong, nonatomic) IBOutlet UILabel *lblThree;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Services_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_BillingAddress_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceAddress_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SpecialInstruction_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ServiceInstruction_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Directions_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Attributes_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_OtherInstructions_H;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_PrimaryPhone;
@property (strong, nonatomic) IBOutlet UITextField *txt_SecondaryPhone;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Targets;
@property (strong, nonatomic) IBOutlet UILabel *lbl_EmpName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Routes;
@property (strong, nonatomic) IBOutlet UILabel *lblRotesName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Middlename;
@property (strong, nonatomic) IBOutlet UIView *viewForBeforeImages;
@property (strong, nonatomic) IBOutlet UIView *imgViewBefore1;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_ImgView_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_SaveContinue_B;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg1;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg2;
@property (strong, nonatomic) IBOutlet UIButton *buttonImg3;
- (IBAction)action_ButtonImg1:(id)sender;
- (IBAction)action_ButtonImg2:(id)sender;
- (IBAction)action_ButtonImg3:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *crewView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Crew_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_Crew_B;
@property (weak, nonatomic) IBOutlet UILabel *lblCrewView;
@property (weak, nonatomic) IBOutlet UILabel *lblKeyMapValue;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceDescriptionValue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_ScrollVieww_H;
@property (strong, nonatomic) IBOutlet UIButton *btnPrimaryEmail;
- (IBAction)action_PrimaryEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondaryEmail;
- (IBAction)action_SecondaryEmail:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *btn_Cancel;
@property (strong, nonatomic) IBOutlet UIButton *btn_SavenContinue;
@property (strong, nonatomic) IBOutlet UILabel *txtViewAccountDescription;

@property (strong, nonatomic) IBOutlet UIButton *btnServiceDocs;
- (IBAction)action_GraphImage:(id)sender;
- (IBAction)action_AddGraph:(id)sender;
- (IBAction)action_CloseGraphView:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewGraph;
@property (strong, nonatomic) IBOutlet UIView *viewGraphImage;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (strong, nonatomic) IBOutlet UIButton *btnAddGraphhh;

//21 Nov Nilind POC CHANGES
@property (weak, nonatomic) IBOutlet UITextField *txtCellNo;
@property (weak, nonatomic) IBOutlet UILabel *billingContactName;
@property (weak, nonatomic) IBOutlet UILabel *billingContactCellNo;
@property (weak, nonatomic) IBOutlet UILabel *billingContactMapCode;

@property (weak, nonatomic) IBOutlet UILabel *billingContactPrimaryEmail;

@property (weak, nonatomic) IBOutlet UILabel *billingContactSecondayEmail;
@property (weak, nonatomic) IBOutlet UILabel *billingContactPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *billingContactSecondaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactName;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactCellNo;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactMapCode;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactPrimaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactSecondaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactSecondaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactGateCode;
@property (weak, nonatomic) IBOutlet UILabel *serviceContactNotes;

//22 Nov Service Address Image
@property (weak, nonatomic) IBOutlet UIButton *btnUploadServiceAddImage;
- (IBAction)actionOnUploadServiceAddImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewServiceAddress;
- (IBAction)actionOnNotesHistory:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *const_H_Notes;
- (IBAction)actionOnClock:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClock;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingCounty;
@property (strong, nonatomic) IBOutlet UILabel *lblBillingSchoolDistrict;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceCounty;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceSchoolDistrict;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalServiceAddressPOCDetailDcs;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerMechanicalBillingAddressPOCDetailDcs;
@property (strong, nonatomic) IBOutlet UIButton *btn_ServiceAddressPOCDetailDcs;
@property (strong, nonatomic) IBOutlet UIButton *btn_BillingAddressPOCDetailDcs;
- (IBAction)action_ServiceAddressPOCDetailDcs:(id)sender;
- (IBAction)action_BillingAddressPOCDetailDcs:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *lblDriveTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEarliestStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblLatestStartTime;


@end
