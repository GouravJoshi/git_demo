//
//  CreditCardIntegration.m
//  DPS
//
//  Created by Rakesh Jain on 22/09/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "CreditCardIntegration.h"
#import "AllImportsViewController.h"
#import "XMLConverter.h"
#import "ServiceSendMailViewController.h"
#import "SendMailViewController.h"
#import "SendMailViewControlleriPad.h"
#import "DPS-Swift.h"


@interface CreditCardIntegration ()
{
    NSString *strCompanyId,*strCompanyKey,*strUserName,*strEmployeeNo,*strServiceUrlMain;
    NSString *strAccountNo,*strWorkorderNo,*strLeadNo,*strBillingAdd1,*strBillingCity,*strBillingState,*strBillingZipcode,*strBillingName,*strBillingEmail;
    Global *global;
    NSURLRequest *requestnew;
    NSString *strTransationId,*strWorkOrderNo;
    BOOL oneTimeCall;
}
@end

@implementation CreditCardIntegration

- (void)viewDidLoad
{
    [super viewDidLoad];
    oneTimeCall=YES;
    global = [[Global alloc] init];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];

    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strUserName  =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strCompanyId=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];
    strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    
    [_activityIndicator setHidden:NO];
    //_strTypeOfService=@"service";
    //strAccountNo=@"9875445682";strWorkOrderNo=@"3457";
    if ([_strTypeOfService isEqualToString:@"service"])
    {
        [self fetchWorkOrderDetails];
    }
    else  //for sales
    {
        [self fetchLeadDetail];
    }
    [self creditCardApi];
    //[self creditCardApi];
    /* NSString* strUrlCreditCard=@"https://certtransaction.hostedpayments.com/mobile/?TransactionSetupID=CDD40B2B-BECE-4D3E-BF51-E5FD1493CCF5";
    NSString* strUrlWeb=[NSString stringWithFormat:@"%@",strUrlCreditCard];
    NSURL *url = [NSURL URLWithString:strUrlWeb];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:urlRequest];*/
    



}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionOnBack:(id)sender
{
    
    [self goBack];
    
}
#pragma mark- ----------CREDIT CARD INTEGRAITON-----------------
-(void)creditCardApi
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        NSString *strUrl;
        NSDictionary *dictForLeadDetail;
        NSDictionary *dictForWorkOrderDetail;
        NSDictionary *dictForBillingAdd;
        NSDictionary *dict_ToSend;

        
        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/MobileAppToCore/ProcessPayment"];
       
        if ([_strTypeOfService isEqualToString:@"service"])  //For Service
        {
            NSArray *keyWorkOrderDetail,*valWorkOrderDetail;
            keyWorkOrderDetail=[NSArray arrayWithObjects:
                                @"AccountNo",
                                @"WorkOrderNo",//WorkOrderNo //LeadNumber
                                @"InvoicePayType",
                                @"PaymentMode",
                                @"Companykey",nil];
            valWorkOrderDetail=[NSArray arrayWithObjects:
                                strAccountNo,
                                strWorkOrderNo,
                                @"CurrentInvoice",
                                @"CreditCard",
                                strCompanyKey,nil];
            
            dictForWorkOrderDetail = [[NSDictionary alloc] initWithObjects:valWorkOrderDetail forKeys:keyWorkOrderDetail];
            
            NSArray *keyBillingAdd,*valBillingAdd;
            keyBillingAdd=[NSArray arrayWithObjects:
                           @"BillingAddress1",
                           @"BillingCity",
                           @"BillingState",
                           @"BillingZipcode",
                           @"BillingName",
                           @"BillingEmail",nil];
           /* valBillingAdd=[NSArray arrayWithObjects:
                           @"15600 San Pedro Ave., Suite # 200",
                          @"SanAntonio",
                          @"Texas",
                          @"78232",
                          strUserName,
                          @"nilind@infcoratsweb.com",nil];*/
            valBillingAdd=[NSArray arrayWithObjects:
                           strBillingAdd1,
                           strBillingCity,
                           strBillingState,
                           strBillingZipcode,
                           strBillingName,
                           strBillingEmail,nil];
            dictForBillingAdd = [[NSDictionary alloc] initWithObjects:valBillingAdd forKeys:keyBillingAdd];
            
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"TransactionId",
                 @"CompanyId",
                 @"TransactionAmount",
                 @"PaymentAccountID",
                 @"PaymentAccountReferenceNumber",
                 @"ReturnURL",
                 @"CustomCss",
                 @"CreatedBy",//technicial name
                 @"IsSaveCard",//false
                 @"AppointmentType",
                 @"WorkorderDetail", //for service dictionary
                 @"LeadDetail", //for sales dictionary
                 @"PaymentGateway",
                 @"PaymentMethod",
                 @"RequestType",
                 @"BillingAddress" ///Dictionary
                 ,nil];
            
            value=[NSArray arrayWithObjects:
                   @"0",
                   strCompanyId,
                   _strAmount,
                   @"",
                   @"",
                   @"",//http://dps.com/Sale/Lead
                   @"",
                   strUserName,
                   @"false",
                   @"WorkOrder",
                   dictForWorkOrderDetail, //for service dictionary
                   @"",
                   @"Element",
                   @"CC",
                   @"Setup",
                   dictForBillingAdd,
                   nil];
             dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        }
        else  //For Lead
        {
            NSArray *keyLead,*valLead;
            keyLead=[NSArray arrayWithObjects:
                     @"AccountNo",
                     @"LeadNumber",
                     @"InvoicePayType",
                     @"PaymentMode",
                     @"Companykey",nil];
            valLead=[NSArray arrayWithObjects:
                     strAccountNo,
                     strLeadNo,
                     @"CurrentInvoice",
                     @"CreditCard",
                     strCompanyKey,nil];
            dictForLeadDetail = [[NSDictionary alloc] initWithObjects:valLead forKeys:keyLead];
            
            NSArray *keyBillingAdd,*valBillingAdd;
            keyBillingAdd=[NSArray arrayWithObjects:
                           @"BillingAddress1",
                           @"BillingCity",
                           @"BillingState",
                           @"BillingZipcode",
                           @"BillingName",
                           @"BillingEmail",nil];
            valBillingAdd=[NSArray arrayWithObjects:
                           strBillingAdd1,
                           strBillingCity,
                           strBillingState,
                           strBillingZipcode,
                           strBillingName,
                           strBillingEmail,nil];
            dictForBillingAdd = [[NSDictionary alloc] initWithObjects:valBillingAdd forKeys:keyBillingAdd];
            
            NSArray *key,*value;
            key=[NSArray arrayWithObjects:
                 @"TransactionId",
                 @"CompanyId",
                 @"TransactionAmount",
                 @"PaymentAccountID",
                 @"PaymentAccountReferenceNumber",
                 @"ReturnURL",
                 @"CustomCss",
                 @"CreatedBy",//technicial name
                 @"IsSaveCard",//false
                 @"AppointmentType",
                 @"WorkorderDetail", //for service dictionary
                 @"LeadDetail", //for sales dictionary
                 @"PaymentGateway",
                 @"PaymentMethod",
                 @"RequestType",
                 @"BillingAddress" ///Dictionary
                 ,nil];
            
            value=[NSArray arrayWithObjects:
                   @"0",
                   strCompanyId,
                   _strAmount,
                   @"",
                   @"",
                   @"",//http://dps.com/Sale/Lead
                   @"",
                   strUserName,
                   @"false",
                   @"Lead",
                   @"", //for service dictionary
                   dictForLeadDetail,
                   @"Element",
                   @"CC",
                   @"Setup",
                   dictForBillingAdd,
                   nil];
             dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];

        }
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End

        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil) {
            
            
        }
        else
        {
            
            NSDictionary *dictionary = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            NSLog(@"RESPONSE >> %@",dictionary);
            if (dictionary.count==0 || dictionary==nil)
            {
                
                //Changes when no response comes for frame URl
                
                [global AlertMethod:Alert :@"Unable to process Payment.Please try in shortly"];
                
                [self goBackMethod];
                
            }
            else
            {
                NSString *strTransactionIdCreditCard,*strUrlCreditCard,*strSuccessCreditCard,*strUrlWeb;
                strTransactionIdCreditCard=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TransactionID"]];
                strUrlCreditCard=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"IframeUrl"]];
                strSuccessCreditCard=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"IsSuccess"]];
                strTransationId=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TransactionID"]];
                if ([strSuccessCreditCard isEqualToString:@"True"]||[strSuccessCreditCard isEqualToString:@"true"]||[strSuccessCreditCard isEqualToString:@"1"])
                {
                    
                    //Temp
                    
                    NSString *url=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"IframeUrl"]];
                    NSString *strTid;
                    NSArray *comp1 = [url componentsSeparatedByString:@"?"];
                    NSString *query = [comp1 lastObject];
                    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
                    for (NSString *element in queryElements) {
                        NSArray *keyVal = [element componentsSeparatedByString:@"="];
                        if (keyVal.count > 0) {
                            NSString *variableKey = [keyVal objectAtIndex:0];
                            NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : nil;
                            strTid=value;
                        }
                        
                    }
                    NSString *strUrl=[NSString stringWithFormat:@"https://certtransaction.hostedpayments.com/mobile/?TransactionSetupID=%@",strTid];
                    NSURL *url2 = [NSURL URLWithString:strUrl];
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url2];
                    [_webView loadRequest:urlRequest];
                   
                    
                    //End
                    
                   
                    /* //Old Working
                     strUrlWeb=[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"IframeUrl"]];
                    NSURL *url = [NSURL URLWithString:strUrlWeb];
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
                    [_webView loadRequest:urlRequest];
                     */

                }
                /*
                 
                 {
                 "TransactionID": "31673",
                 "PaymentAccountID": null,
                 "PaymentAccountReferenceNumber": null,
                 "LastFour": null,
                 "IsSuccess": true,
                 "ResponseMessage": "Success",
                 "IframeUrl": "https://certtransaction.hostedpayments.com/?TransactionSetupID=CDD40B2B-BECE-4D3E-BF51-E5FD1493CCF5"
                 }
                 
                 {
                 IframeUrl = "https://certtransaction.hostedpayments.com/?TransactionSetupID=C1D37B56-DCE2-4DA6-9F9D-B061553BA699";
                 IsSuccess = 1;
                 LastFour = "<null>";
                 PaymentAccountID = "<null>";
                 PaymentAccountReferenceNumber = "<null>";
                 ResponseMessage = Success;
                 TransactionID = 34666;
                 }
                 */
                
            }
        }
        [DejalActivityView removeView];
        
    }
    
}


-(void)creditCardApiForSaveTransaction:(NSString *)responseString : (NSString *)typeFlag
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
    }
    else
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateNow;
        dateNow=[dateFormatter stringFromDate:[NSDate date]];
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
        NSString *strUrl;
        
     

        strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/MobileAppToCore/SaveTrancation"];
        strUrl = [strUrl stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *key,*value;
        key=[NSArray arrayWithObjects:
             @"CreatedBy",
             @"CreatedDate",
             @"IsActive",
             @"IsSuccess",
             @"ModifiedBy",
             @"ModifiedDate",
             @"PaymentGateway",
             @"PaymentMethod",
             @"RequestParam",
             @"RequestType",
             @"ResponseParam",
             @"TransactionId",nil];
        
        value=[NSArray arrayWithObjects:
               strUserName,
               dateNow,//CreatedDate
               @"true",
               typeFlag,
               strUserName,
               dateNow,
               @"Element",
               @"CC",
               @"",
               @"Setup Payment",
               responseString,// Iframe response Param in xml form
               strTransationId,nil];
        
        NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
        
        NSError *errorr;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
        
        NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
        
        NSLog(@"JSON DICT %@",strdatafirst);
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //New Header Change
        [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
        [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
        [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
        [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

        //End

        //Nilind 1 Feb
        [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
        [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
        //ENd
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        if (jsonData==nil)
        {
            
        }
        else
        {

            NSString *finalResponsestring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSLog(@"RESPONSE IN STRING FORM %@",finalResponsestring);
            if([finalResponsestring isEqualToString:@"true"])
            {
               // [self finalAPI];
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:@"Alert"
                                           message:@"Payment done successfully"
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yes = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action)
                                      {
                                          //[self.navigationController popViewControllerAnimated:YES];
                                          /*UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
                                          ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
                                          [self.navigationController pushViewController:objSendMail animated:NO];*/
                                          
                                          
                                      }];
                [alert addAction:yes];
                //[self presentViewController:alert animated:YES completion:nil];
            }
        
            else
            {
                //[self.navigationController popViewControllerAnimated:YES];
            }



        }
        [DejalActivityView removeView];
        
    }
}
-(void)apiIForActiveReceivePaymentTransaction
{
    
    NSString *strUrl;

    strUrl=[NSString stringWithFormat:@"%@%@",MainUrl,@"api/MobileAppToCore/ActivePayment"];
    strUrl = [strUrl stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSArray *key,*value;

    if ([_strTypeOfService isEqualToString:@"service"])
    {
        key=[NSArray arrayWithObjects:
             @"TransactionId",
             @"AccountNo",
             @"EntityNo",
             @"Companykey",
             @"PaymentMode",
             @"TransactionAmount",
             @"InvoicePayType",
             @"AppointmentType",nil];
        
        value=[NSArray arrayWithObjects:
               strTransationId,
               strAccountNo,
               strWorkOrderNo,
               strCompanyKey,
               @"CreditCard",
               _strAmount,
               @"CurrentInvoice",
               @"WorkOrder",nil];
    }
    else
    {
        key=[NSArray arrayWithObjects:
             @"TransactionId",
             @"AccountNo",
             @"EntityNo",
             @"Companykey",
             @"PaymentMode",
             @"TransactionAmount",
             @"InvoicePayType",
             @"AppointmentType",nil];
        
        value=[NSArray arrayWithObjects:
               strTransationId,
               strAccountNo,
               strLeadNo,
               strCompanyKey,
               @"CreditCard",
               _strAmount,
               @"CurrentInvoice",
               @"Lead",nil];
    }
    
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:value forKeys:key];
    
    NSError *errorr;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dict_ToSend options:NSJSONWritingPrettyPrinted error:&errorr];
    
    NSString *jsonString1 = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString1];
    
    NSLog(@"JSON DICT ACTIVE TRAN %@",strdatafirst);
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //End

    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //ENd
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    NSError *error = nil;
    NSURLResponse * response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSData* jsonData = [NSData dataWithData:data];
    if (jsonData==nil)
    {
        
    }
    else
    {
        
        NSString *finalResponsestring = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"RESPONSE For Active Transaction  %@",finalResponsestring);
        if([finalResponsestring isEqualToString:@"true"])
        {
            // [self finalAPI];
           /* UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Payment done successfully"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];*/

        }
        else
        {
          /*  UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"Alert"
                                       message:@"Your Payment is in progress "
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yes = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
            [alert addAction:yes];
            [self presentViewController:alert animated:YES completion:nil];*/

        }
        
    }
    [DejalActivityView removeView];

}
#pragma mark- -------- WEBVIEW DELEGATE METHOD----------

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator setHidden:NO];

    [_activityIndicator startAnimating];
    NSLog(@"start");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"finish");
    [_activityIndicator setHidden:YES];
    [_activityIndicator stopAnimating];
    
    if(!webView.isLoading)
    {
        
        NSString *webViewString = [_webView stringByEvaluatingJavaScriptFromString:
                                   @"document.body.innerText"];
        NSLog(@"webViewString>>%@",webViewString);
        
        NSData *data = [webViewString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@",json);
        
        NSData *data2 = [webViewString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arrayJson = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil],nil];

        NSLog(@"string: %@",arrayJson);
        
        
        if([webViewString isEqualToString:@"Transaction Cancelled"])
        {
            //[self apiIForInActiveReceivePaymentTransaction];
            ///[self.navigationController popViewControllerAnimated:YES];
        }
        else if ([webViewString isEqualToString:@"Transaction Complete"])
        {
        }
        
    }

}
-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if([request.URL.path isEqualToString:@"certtransaction.hostedpayments.com"])
    {
        NSURLComponents *components = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        NSArray *queryItems = [components queryItems];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        
        for (NSURLQueryItem *item in queryItems)
        {
            [dict setObject:[item value] forKey:[item name]];
        }
        NSLog(@"Dictionary Three --->%@",dict);
        return YES;
    }
    else
    {
        NSURLComponents *components = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
        NSArray *queryItems = [components queryItems];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        
        for (NSURLQueryItem *item in queryItems)
        {
            [dict setObject:[item value] forKey:[item name]];
        }
        NSLog(@"FINAL--->%@",dict);
        
        if ([NSString stringWithFormat:@"%@",[dict valueForKey:@"ApprovalNumber"]].length>0||[NSString stringWithFormat:@"%@",[dict valueForKey:@"ExpressResponseMessage"]].length>0)
        {
            NSString *typeFlag;
            NSError * err;
            NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
            NSLog(@"%@",myString);
            myString=[myString stringByReplacingOccurrencesOfString:@"" withString:@"{"];
            myString=[myString stringByReplacingOccurrencesOfString:@"" withString:@"}"];
            if([[dict valueForKey:@"ExpressResponseCode"]isEqualToString:@"0"]||[[dict valueForKey:@"ExpressResponseMessage"]isEqualToString:@"Approved"])
            {
                if (oneTimeCall==YES)
                {
                    oneTimeCall=NO;
                    typeFlag=@"true";
                    [self creditCardApiForSaveTransaction:myString :typeFlag];
                    [self apiIForActiveReceivePaymentTransaction];
                    
                    if ([_strTypeOfService isEqualToString:@"service"])
                    {
                        
                        [_workOrderDetailNew setValue:@"Completed" forKey:@"workorderStatus"];
                        NSError *error;
                        [context save:&error];
                        
                        [global AlertMethod:Info :@"Payment done successfully"];

                        [self goToServiceSendMail];
                        
                    }
                    else
                    {
                        
                        [self updateLeadIdDetail];
                        
                        [global AlertMethod:Info :@"Payment done successfully"];
                        
                        [self goToSalesSendMail];
                        
                    }

                }
                
            }
            else if([[dict valueForKey:@"ExpressResponseCode"]isEqualToString:@"1"]||[[dict valueForKey:@"ExpressResponseMessage"]isEqualToString:@"Declined"])
            {
                if (oneTimeCall==YES)
                {
                    oneTimeCall=NO;
                    typeFlag=@"false";
                    [self creditCardApiForSaveTransaction:myString:typeFlag];
                    //[self.navigationController popViewControllerAnimated:YES];
                    
                    [global AlertMethod:Info :@"Payment failed"];
                    
                    [self goBack];

                }
            }

        }
        return YES;

    }
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_activityIndicator stopAnimating];

    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

-(void)goToServiceSendMail{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"ServiceiPad" bundle:nil];
        ServiceSendMailViewControlleriPad *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewControlleriPad"];
        objSendMail.fromWhere = _fromWhere;
        objSendMail.isCustomerPresent=_isCustomerPresent;
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }else{
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
        ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
        objSendMail.fromWhere = _fromWhere;
        objSendMail.isCustomerPresent=_isCustomerPresent;
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }
}

-(void)goToSalesSendMail{
    
    if ([_strDeviceType isEqualToString:@"iPhone"])
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
        SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }
    else
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMainiPad" bundle:nil];
        SendMailViewControlleriPad *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewControlleriPad"];
        
        if ([_fromWhere isEqualToString:@"WdoFlow"]) {
            
            objSendMail.strFromWdo = @"yes";
            
        }else if ([_fromWhere isEqualToString:@"NPMA"]) {
            
            objSendMail.strFromWdo = @"yes";
            
        } else {
            
            objSendMail.strFromWdo = @"no";
            
        }
        
        [self.navigationController pushViewController:objSendMail animated:NO];
        
    }

}


-(void)goBackMethod{
    
    if ([_strTypeOfService isEqualToString:@"service"])
    {
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            if ([_strFromWhichView isEqualToString:@"PestFlow"])
            {
                
                // Go back to pest flow jo ki abhi bana nhi hai
                
            }else if ([_strFromWhichView isEqualToString:@"WdoFlow"]) {
                
                // Go back to Wdo flow i.e Finalize Report WDO VC
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[FinalizeReport_WDOVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                FinalizeReport_WDOVC *myController = (FinalizeReport_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            else if ([_strFromWhichView isEqualToString:@"NpmaFlow"]) {
                
                // Go back to Wdo flow i.e Finalize Report WDO VC
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[NPMA_FinlizedReportVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                NPMA_FinlizedReportVC *myController = (NPMA_FinlizedReportVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }else{
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[InvoiceAppointmentViewiPad class]]) {
                        index=k1;
                        //break;
                    }
                }
                InvoiceAppointmentViewiPad *myController = (InvoiceAppointmentViewiPad *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            
            
        }else{
            
            if ([_strFromWhichView isEqualToString:@"PestFlow"])
            {
                
                // Go back to pest flow i.e invoicedetailVC
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[InvoiceDetailVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                InvoiceDetailVC *myController = (InvoiceDetailVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }else if ([_strFromWhichView isEqualToString:@"WdoFlow"]) {
                
                // Go back to Wdo flow i.e Finalize Report WDO jo abhi bana nhi hai
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[FinalizeReport_WDOVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                FinalizeReport_WDOVC *myController = (FinalizeReport_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            else if ([_strFromWhichView isEqualToString:@"NpmaFlow"]) {
                
                // Go back to Wdo flow i.e Finalize Report WDO jo abhi bana nhi hai
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[NPMA_FinlizedReportVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                NPMA_FinlizedReportVC *myController = (NPMA_FinlizedReportVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }else{
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[InvoiceAppointmentView class]]) {
                        index=k1;
                        //break;
                    }
                }
                InvoiceAppointmentView *myController = (InvoiceAppointmentView *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            
        }
        
    }
    else{
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            if ([_strFromWhichView isEqualToString:@"WdoFlow"]) {
             
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }else if ([_strFromWhichView isEqualToString:@"NpmaFlow"]) {
                
                // Agreement Par Jana Hai Npma k
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[Agreement_WDOVC class]]) {
                        index=k1;
                        //break;
                    }
                }
                Agreement_WDOVC *myController = (Agreement_WDOVC *)[self.navigationController.viewControllers objectAtIndex:index];
                [self.navigationController popToViewController:myController animated:NO];
                
            }else {
                
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposaliPad class]]) {
                        index=k1;
                        //break;
                    }
                }
                SalesAutomationAgreementProposaliPad *myController = (SalesAutomationAgreementProposaliPad *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            
        }
        else{
            
            if ([_strFromWhichView isEqualToString:@"WdoFlow"]) {
                

                // go to agreement wdo vc jo ki abhi iphone mai nhi bana hai
                
            }
            else if ([_strFromWhichView isEqualToString:@"NpmaFlow"]) {
                

                // go to agreement wdo vc jo ki abhi iphone mai nhi bana hai
                
            }else {
             
                int index = 0;
                NSArray *arrstack=self.navigationController.viewControllers;
                for (int k1=0; k1<arrstack.count; k1++) {
                    if ([[arrstack objectAtIndex:k1] isKindOfClass:[SalesAutomationAgreementProposal class]]) {
                        index=k1;
                        //break;
                    }
                }
                SalesAutomationAgreementProposal *myController = (SalesAutomationAgreementProposal *)[self.navigationController.viewControllers objectAtIndex:index];
                // myController.typeFromBack=_lbl_LeadInfo_Status.text;
                [self.navigationController popToViewController:myController animated:NO];
                
            }
            
        }
    }
}

-(void)goBack{
    
    UIAlertView *alerttt=[[UIAlertView alloc]initWithTitle:Alert message:@"Are you sure to quit payment process" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes-Quit", nil];
    [alerttt show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        
        
    }
    else{
        
        [self goBackMethod];
        
    }
}

#pragma mark- ------------ FETCH WORKORDER DETAIL ---------------

-(void)fetchWorkOrderDetails
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    //strWorkOrderId=@"201";
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",_strGlobalWorkOrderId];
    
    [requestNewService setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        matches=[arrAllObj objectAtIndex:0];
        
        strAccountNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
        strWorkOrderNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workOrderNo"]];
        strBillingAdd1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingAddress1"]];
        strBillingCity=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingCity"]];
        strBillingState=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingState"]];
        strBillingZipcode=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingZipcode"]];
        strBillingName=strUserName;
        strBillingEmail=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]];
    }
}
#pragma mark -SalesAuto Fetch Core Data
-(void)fetchLeadDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",_strGlobalLeadId];
    
    [requestNewService setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        
    }
    else
    {
        
        matches=arrAllObj[0];
        strAccountNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"accountNo"]];
        strLeadNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"leadNumber"]];
        strBillingAdd1=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingAddress1"]];
        strBillingCity=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingCity"]];
        strBillingState=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingState"]];
        strBillingZipcode=[NSString stringWithFormat:@"%@",[matches valueForKey:@"billingZipcode"]];
        strBillingName=strUserName;
        strBillingEmail=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]];
    }
        
}
-(void)updateLeadIdDetail
{
#pragma mark- Note
    // visibility and Inspector alag se save krna he
    //**** For Lead Detail *****//
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",_strGlobalLeadId];
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjNew = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSManagedObject *matchesNew;
    if (arrAllObjNew.count==0)
    {
        
    }else
    {
        
        matchesNew=arrAllObjNew[0];
        [matchesNew setValue:@"Complete" forKey:@"statusSysName"];
        [context save:&error1];
        
    }
}

@end
