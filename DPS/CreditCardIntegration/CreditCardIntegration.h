//
//  CreditCardIntegration.h
//  DPS
//
//  Created by Rakesh Jain on 22/09/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface CreditCardIntegration : UIViewController<UIWebViewDelegate,NSFetchedResultsControllerDelegate>
{
    
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSEntityDescription *entityWorkOderDetailServiceAuto,*entityLeadDetail;
    NSFetchRequest *requestNewService;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)actionOnBack:(id)sender;
@property (strong, nonatomic) NSManagedObject *workOrderDetail;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong,nonatomic)NSString *strGlobalWorkOrderId,*strGlobalLeadId,*strAmount,*strTypeOfService,*strDeviceType,*strFromWhichView;
@property(strong,nonatomic)NSManagedObject *objectMatches;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerServiceAutomation,*fetchedResultsControllerSalesInfo;
@property (strong, nonatomic) NSManagedObject *workOrderDetailNew;
@property (weak, nonatomic) NSString *isCustomerPresent;
@property (strong, nonatomic) NSString *fromWhere;


@end
