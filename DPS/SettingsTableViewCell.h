//
//  SettingsTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 28/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *LblDataSettings;

@end
