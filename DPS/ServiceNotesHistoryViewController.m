//
//  ServiceNotesHistoryViewController.m
//  DPS
//
//  Created by Saavan Patidar on 13/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import "ServiceNotesHistoryViewController.h"
#import "AllImportsViewController.h"
#import "DPS-Bridging-Header.h"
#import "DPS-Swift.h"
#import "DPS-Swift.h"
@interface ServiceNotesHistoryViewController ()
{
    Global *global;
    NSString *strServiceUrlMainServiceAutomation,*strCompanyKeyy,*strAcctNo;
    NSArray *arrData;
   
   
}
@end

@implementation ServiceNotesHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    global=[[Global alloc]init];
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];

    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strServiceUrlMainServiceAutomation=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"]];
    strCompanyKeyy     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    if ([_strTypeOfService isEqualToString:@"sales"])
    {
        strAcctNo=[defs valueForKey:@"leadAccountNo"];
    }
    else
    {
        strAcctNo=[defs valueForKey:@"workOrderAccountNo"];
    }
    if([UIScreen mainScreen].bounds.size.height>1000)
    {
        _lblNotesHistory.font=[UIFont systemFontOfSize:25];
    }
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];

     //[self performSelector:@selector(getServiceHistory) withObject:nil afterDelay:1.0];
    
    
    _tblData.rowHeight=UITableViewAutomaticDimension;
    _tblData.estimatedRowHeight=200;
    _tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: NO];
    [self getServiceHistory];
    [[NSNotificationCenter defaultCenter] addObserver:self
            selector:@selector(receiveTestNotification:)
            name:@"AddedNotes_Notification_For_WDO"
            object:nil];
    
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"AddedNotes_Notification_For_WDO"]){
        NSLog (@"Successfully received the test notification!");
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self getServiceHistory];
    }
}
#pragma mark-  -------------------- GET SERVICE HISTORY -------------------
-(void)getServiceHistory
{
    //[DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    
    if (netStatusWify1== NotReachable)
    {
        [global AlertMethod:@"ALert" :ErrorInternetMsg];
        [DejalActivityView removeView];
        [DejalBezelActivityView removeView];
    }
    else
    {

        //strAcctNo = @"7878802239";
        NSUserDefaults *defsAddImage=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginDataAddImage=[defsAddImage valueForKey:@"LoginDetails"];
        NSString *strServiceAddImageUrl=[dictLoginDataAddImage valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@%@&companyKey=%@",strServiceAddImageUrl,UrlServiceNotesHistory,strAcctNo,strCompanyKeyy];
        
        NSLog(@"GET Service History  URl-----%@",strUrl);
        
        strUrl =[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        
        if (jsonData==nil)
        {
            NSString *strTitle = Alert;
            NSString *strMsg = NoDataAvailableee;
            [global AlertMethod:strTitle :strMsg];
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
            
        }
        else
        {
            NSArray* responseArr = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
            
            if (responseArr.count==0)
            {
                NSString *strTitle = Alert;
                NSString *strMsg = NoDataAvailableee;
                [global AlertMethod:strTitle :strMsg];
                
            }
            else
            {
                NSLog(@"Plan Response %@",responseArr);

                NSString *strException;
                @try {
                    strException=[responseArr valueForKey:@"ExceptionMessage"];
                } @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
                
                if ([strException isKindOfClass:[NSArray class]])
                {
                        arrData=responseArr;
                        [_tblData reloadData];
                    
                }
                
                else
                {
                    if (strException.length==0)
                    {
                        arrData=responseArr;
                        [_tblData reloadData];
                    }
                    else
                    {
                        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"No record found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alert show];
                    }

                    
                }
                
                
               /* @try {
                    strException=[ResponseDict valueForKey:@"ExceptionMessage"];
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
                if (strException.length==0) {
                    
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Your Lead has been sent to server." message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    
                } else {
                    
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to send lead to server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }

                */
                
            }
            [DejalActivityView removeView];
            [DejalBezelActivityView removeView];
        }
        
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return arrData.count;
        
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    ServiceHistoryTableViewCell *cell = (ServiceHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ServiceHistoryTableViewCell" forIndexPath:indexPath];
    
    NSDictionary *dict=[arrData objectAtIndex:indexPath.row];
    //For Notes
    NSString *strNotes=[NSString stringWithFormat:@"%@",[dict valueForKey:@"Note"]];
    if(strNotes.length==0 || [strNotes isEqualToString:@"<null>"])
    {
        strNotes=@"";
    }
    cell.lblValueForName.text=strNotes;
    
    //For Emp Name
    NSString *strEmpName=[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeName"]];
    if(strEmpName.length==0 || [strEmpName isEqualToString:@"<null>"])
    {
        strEmpName=@"";
    }
    
    cell.lblValueFroEmployeeNo.text=strEmpName;
    
    //For Date
   // NSString *strNoteDate=[self ChangeDateToLocalDateFull :[NSString stringWithFormat:@"%@",[dict valueForKey:@"CreatedDate"]]];
    
    NSString *strNoteDate=[self ChangeDateToLocalDateFull :[NSString stringWithFormat:@"%@",[dict valueForKey:@"ClientCreatedDate"]]];
    
    if(strNoteDate.length==0 || [strNoteDate isEqualToString:@"<null>"])
    {
        strNoteDate=@"";
    }
 cell.lblValueForScheduleStartDateTime.text=strNoteDate;//"2017-12-08T07:00:56.567"
    
    if ([UIScreen mainScreen].bounds.size.height>1000)
    {
        cell.lblValueForName.font=[UIFont systemFontOfSize:16];
        cell.lblValueFroEmployeeNo.font=[UIFont systemFontOfSize:16];
        cell.lblValueForScheduleStartDateTime.font=[UIFont systemFontOfSize:16];
        cell.lblNotes1ForNoteshistory.font=[UIFont systemFontOfSize:16];
        cell.lblEmployee1ForNotesHistory.font=[UIFont systemFontOfSize:16];
        cell.lblDate1ForNotesHistory.font=[UIFont systemFontOfSize:16];

    }
    
    return cell;
}
-(NSString *)ChangeDateToLocalDateFull :(NSString*)strDateToConvert{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    //2017-12-08T07:06:39.893
    NSDate* newTime = [dateFormatter dateFromString:strDateToConvert];
    //Add the following line to display the time in the local time zone
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
    //[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];

    NSString* finalTime = [dateFormatter stringFromDate:newTime];
    
    return finalTime;
}
- (IBAction)actionOnBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionOnAddNotes:(id)sender
{
    /*
     RefId = WorkorderId
     &
     RefType = "WorkOrder"
     */
    
    if ([_isFromWDO isEqualToString: @"YES"]){
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM_New_iPhone"
                                                                 bundle: nil];
        AddNotesVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddNotesVC"];
        objByProductVC.isFromWDO = YES;
        objByProductVC.tempstrRefType = @"WorkOrder" ;
        objByProductVC.tempstrRefId = _strWorkOrderNo;
        objByProductVC.modalTransitionStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:objByProductVC animated:NO completion:nil];
    }
    else{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"CRM_New_iPhone"
                                                                 bundle: nil];
        AddNotesVC *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddNotesVC"];
        objByProductVC.tempstrRefType = @"WorkOrder" ;
        objByProductVC.tempstrRefId = _strWoId;
        
        NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
        
        NSString *strRefId = [defs valueForKey:@"StrRefId"];
        
        objByProductVC.tempstrRefId = strRefId;
        
        objByProductVC.modalTransitionStyle = UIModalPresentationOverCurrentContext;
        
        [self presentViewController:objByProductVC animated:NO completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
