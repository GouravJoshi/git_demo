//
//  UpcomingAppointmentTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 22/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavvan patidar 007
//  Saavan Patidar 2021
//  Saavan Patidar 2021

#import "UpcomingAppointmentTableViewCell.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"

@implementation UpcomingAppointmentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
