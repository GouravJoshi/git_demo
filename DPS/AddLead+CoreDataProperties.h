//
//  AddLead+CoreDataProperties.h
//  DPS
//
//  Created by Saavan Patidar on 06/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AddLead.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddLead (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *companyId;
@property (nullable, nonatomic, retain) NSString *companyKey;
@property (nullable, nonatomic, retain) NSString *createdBy;
@property (nullable, nonatomic, retain) NSString *createdDateTime;
@property (nullable, nonatomic, retain) NSString *customerId;
@property (nullable, nonatomic, retain) NSString *isAgreementGenerated;
@property (nullable, nonatomic, retain) NSString *leadId;
@property (nullable, nonatomic, retain) NSString *pocId;
@property (nullable, nonatomic, retain) NSString *primaryServiceId;
@property (nullable, nonatomic, retain) NSString *userName;
@property (nullable, nonatomic, retain) id customerExtDcDict;
@property (nullable, nonatomic, retain) id leadServiceExtDcsArray;//customerAddress
@property (nullable, nonatomic, retain) id customerAddress;
@end

NS_ASSUME_NONNULL_END
