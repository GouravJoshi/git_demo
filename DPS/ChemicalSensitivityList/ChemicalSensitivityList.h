//
//  ChemicalSensitivityList.h
//  DPS
// changes 
//  Created by Rakesh Jain on 03/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>
#import "Global.h"
#import "AllImportsViewController.h"

@interface ChemicalSensitivityList : UIViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
{
    AppDelegate *appDelegate;
    NSManagedObjectContext *context;
    NSFetchRequest *requestNew;
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    NSManagedObject *matches;
    NSArray *arrAllObj;
    NSEntityDescription *entityLeadDetail;
}
 @property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerSalesInfo;
@property (weak, nonatomic) IBOutlet UITableView *tblData;
- (IBAction)actionOnBack:(id)sender;
@property(strong,nonatomic)NSString *strFrom, *strId;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleHeader;

@end
