//
//  ChemicalSensitivityList.m
//  DPS
// Changes
//  Created by Rakesh Jain on 03/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  Saavan 2021 Patidar 

#import "ChemicalSensitivityList.h"
#import "Global.h"
#import "ChemicalSensitivityListTableViewCell.h"
#import "AllImportsViewController.h"
@interface ChemicalSensitivityList ()
{
    Global *global;
    NSString *strBillingAddress1,*strBillingAddress2,*strCityName,*strCountryId,*strStateId,*strZipcode,*strLat,*strLong,*strCompanyKey;
    NSMutableArray *arrState;
    NSDictionary *dictStateIdFromName;
    NSArray *arrData;
}
@end

@implementation ChemicalSensitivityList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    global=[[Global alloc]init];
    
    
    if ([UIScreen mainScreen].bounds.size.height<600)
    {
        _lblTitleHeader.font=[UIFont systemFontOfSize:14];
    }
    
    [self fetchJsonState];
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strCompanyKey =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];

    if ([_strFrom isEqualToString:@"sales"])
    {
        [self salesFetch];
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        
        if (netStatusWify1== NotReachable)
        {
            [global AlertMethod:@"ALert" :ErrorInternetMsg];
            [DejalActivityView removeView];
        }
        else
        {
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [DejalBezelActivityView removeView];
                [self getContactDetail];
            });
           
            
        }
    }
    
    
    if (_strId.length==0 || [_strId isEqual:nil ] || [_strId isEqualToString:@""])
    {
        _strId=@"";
    }
    // Do any additional setup after loading the view.
    _tblData.estimatedRowHeight=150;
    _tblData.rowHeight=UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-  ------------- TABLE VIEW DELEGAT METHOD -------------

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrData.count;
        
   
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"ChemicalSensitivityListTableViewCell";
    ChemicalSensitivityListTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell==nil)
    {
        cell=[[ChemicalSensitivityListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSDictionary *dict=[arrData objectAtIndex:indexPath.row];
    
    
    cell.lblValueForContactName.text=[dict valueForKey:@"FirstName"];
    
    
    [cell.btnAddress setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FullAddress"]] forState:UIControlStateNormal];
    cell.btnAddress.titleLabel. numberOfLines =2; // Dynamic number of lines
    cell.btnAddress.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.lblValueForAddress.text=@"";
    
    
    [cell.btnCellNo setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"CellPhone1"]] forState:UIControlStateNormal];
    
    cell.lblValueForCell.text=@"";
    
    
    
    [cell.btnPrimaryPhone setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PrimaryPhone"]] forState:UIControlStateNormal];

    cell.lblValueForPrimaryPhone.text=@"";
    
    
    [cell.btnSecondaryPhone setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SecondaryPhone"]] forState:UIControlStateNormal];
    cell.lblValueForSecondaryPhone.text=@"";
    
    
    [cell.btnPrimaryEmail setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PrimaryEmail"]] forState:UIControlStateNormal];
    cell.lblVlaueForPrimaryEmail.text=@"";
    [cell.btnSecondaryEmail setTitle:[NSString stringWithFormat:@"%@",[dict valueForKey:@"SecondaryEmail"]] forState:UIControlStateNormal];
    cell.lblValueForSecondaryEmail.text=@"";
    
    [cell.btnAddress addTarget:self
                                    action:@selector(buttonClickedAddress:) forControlEvents:UIControlEventTouchDown];
    cell.btnAddress.tag=indexPath.row;
    
    [cell.btnCellNo addTarget:self
                        action:@selector(buttonClickedCell:) forControlEvents:UIControlEventTouchDown];
    cell.btnCellNo.tag=indexPath.row;

    
    [cell.btnPrimaryPhone addTarget:self
                        action:@selector(buttonClickedPrimaryPhone:) forControlEvents:UIControlEventTouchDown];
    cell.btnPrimaryPhone.tag=indexPath.row;

    
    [cell.btnSecondaryPhone addTarget:self
                        action:@selector(buttonClickedSecondaryPhone:) forControlEvents:UIControlEventTouchDown];
    cell.btnSecondaryPhone.tag=indexPath.row;

    
    [cell.btnPrimaryEmail addTarget:self
                        action:@selector(buttonClickedPrimaryEmail:) forControlEvents:UIControlEventTouchDown];
    cell.btnPrimaryEmail.tag=indexPath.row;

    
    [cell.btnSecondaryEmail addTarget:self
                             action:@selector(buttonClickedSecondaryEmail:) forControlEvents:UIControlEventTouchDown];
    cell.btnSecondaryEmail.tag=indexPath.row;

    
    
    return cell;

}
- (void)buttonClickedAddress:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    
    if(button.titleLabel.text.length>0)
    {
        [global redirectOnAppleMap:self :button.titleLabel.text];
    }
    
    
}
- (void)buttonClickedCell:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    if (button.titleLabel.text.length>0)
    {
        [global calling:button.titleLabel.text];
        
    }

}
- (void)buttonClickedPrimaryPhone:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    if (button.titleLabel.text.length>0)
    {
        [global calling:button.titleLabel.text];
        
    }

}
- (void)buttonClickedSecondaryPhone:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    if (button.titleLabel.text.length>0)
    {
        [global calling:button.titleLabel.text];
        
    }

}
- (void)buttonClickedPrimaryEmail:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    if (button.titleLabel.text.length>0)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            [global emailComposer:button.titleLabel.text :@"" :@"" :self];
        }
        else
        {
            [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
            
            
        }
    }
}
- (void)buttonClickedSecondaryEmail:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    if (button.titleLabel.text.length>0)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            [global emailComposer:button.titleLabel.text :@"" :@"" :self];
        }
        else
        {
            [global AlertMethod:@"Alert" :@"Please configure your device to send mail" ];
            
            
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


-(void)getContactDetail
{
    //http://tcrms.stagingsoftware.com//api/crmcontact/GetChemicalSensitiveContactListByAddress?address1=1108%20Carlock%20St&address2=&address2=&cityName=Austin&countryId=1&stateId=302&zipCode=48701&Latitude=&Longitude=
    
   /* strBillingAddress1=@"1108 Carlock St";
    strBillingAddress2=@"";
    strCityName=@"Austin";
    strCountryId=@"1";
    strStateId=@"302";
    strZipcode=@"48701";*/
    
    NSUserDefaults *defsLogindDetailNew=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetailNew valueForKey:@"LoginDetails"];
    
    NSString *strServiceUrlMain =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];

    
    NSString *strUrlwithString=[NSString stringWithFormat:@"%@/api/crmcontact/GetChemicalSensitiveContactListByAddress?companykey=%@&address1=%@&address2=%@&cityName=%@&countryId=%@&stateId=%@&zipCode=%@&Latitude=%@&Longitude=%@",strServiceUrlMain,strCompanyKey,strBillingAddress1,strBillingAddress2,strCityName,strCountryId,strStateId,strZipcode,strLat,strLong];
    
    strUrlwithString = [strUrlwithString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

   // strUrlwithString = [strUrlwithString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];

    
    NSString *strType=@"";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrlwithString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //Nilind 1 Feb
    [request addValue:[global getIPAddress] forHTTPHeaderField:@"VisitorIP"];
    [request addValue:@"IOS" forHTTPHeaderField:@"Browser"];
    //New Header Change
    [request addValue:[global strEmpBranchID] forHTTPHeaderField:@"BranchId"];
    [request addValue:[global strEmpBranchSysName] forHTTPHeaderField:@"BranchSysName"];
    [request addValue:[global strIsCorporateUser] forHTTPHeaderField:@"IsCorporateUser"];
    [request addValue:[global strClientTimeZone] forHTTPHeaderField:@"ClientTimeZone"];

    //End
    
    @try
    {
        NSError *error = nil;
        NSURLResponse * response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSData* jsonData = [NSData dataWithData:data];
        NSDictionary *ResponseDict;
        ResponseDict = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
        
        if(ResponseDict.count==0)
        {
        }
       /* else if([[ResponseDict valueForKey:@"Message"] isEqualToString:@"An error has occurred."])
        {
            [global AlertMethod:Alert :@"Some thing went wrong, try again later."];
        }*/
        else
        {
            
            NSMutableDictionary *dictTempResponse=[[NSMutableDictionary alloc]init];
            [dictTempResponse setObject:ResponseDict forKey:@"response"];
            NSDictionary *dict=[[NSDictionary alloc]init];
            dict=[global nestedDictionaryByReplacingNullsWithNil:dictTempResponse];
            NSMutableDictionary *dictData=[dict valueForKey:@"response"];
            ResponseDict =dictData;

            NSLog(@"Response clock data >> %@",ResponseDict);
            
            
            NSString *strException;
            @try {
                strException=[ResponseDict valueForKey:@"ExceptionMessage"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            @try {
                strException=[ResponseDict valueForKey:@"Message"];
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            if (![strException isKindOfClass:[NSString class]]) {
                
                strException=@"";
                
            }
            
            if (strException.length==0) {
                
               arrData=(NSArray*)ResponseDict;
                
            } else {
                ResponseDict=nil;
                arrData=nil;
            }

            
            
           // arrData=(NSArray*)ResponseDict;
        }
        [DejalBezelActivityView removeView];
        
        if (arrData.count==0)
        {
            [global AlertMethod:Alert :@"No record found"];
            
        }
        else
        {
            [_tblData reloadData];
        }
        
    }
    @catch (NSException *exception)
    {
        
        [global AlertMethod:Alert :Sorry];
        
    }
    @finally
    {
        
    }
}
- (IBAction)actionOnBack:(id)sender
{
      [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)fetchJsonState
{
    arrState=[[NSMutableArray alloc]init];
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:StateJson];
    NSError * error;
    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
    NSArray *arrOfState = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
   
    [arrState addObjectsFromArray:arrOfState];
    
    NSMutableArray *arrKey,*arrValue;
    arrKey=[[NSMutableArray alloc]init];
    arrValue=[[NSMutableArray alloc]init];
    
    for(int i=0;i<arrState.count;i++)
    {
        NSDictionary *dict=[arrState objectAtIndex:i];
        [arrKey addObject:[dict valueForKey:@"Name"]];
        [arrValue addObject:[dict valueForKey:@"StateId"]];
    }
    dictStateIdFromName=[NSDictionary dictionaryWithObjects:arrValue forKeys:arrKey];
}
#pragma mark - SALES FETCH

-(void)salesFetch
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",_strId];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray* arrAllObjSales = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObjSales.count==0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjSales.count; k++)
        {
            matches=arrAllObjSales[k];

            strBillingAddress1=[matches valueForKey:@"servicesAddress1"];
            strBillingAddress2=[matches valueForKey:@"serviceAddress2"];
            strCityName=[matches valueForKey:@"serviceCity"];
            strCountryId=@"1";
            strStateId=[NSString stringWithFormat:@"%@",[dictStateIdFromName valueForKey:[matches valueForKey:@"serviceState"]]];
            strZipcode=[matches valueForKey:@"serviceZipcode"];
            strLat=@"";
            strLong=@"";
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        }
    }
}
@end
