//
//  ChemicalSensitivityListTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 03/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//  // Saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface ChemicalSensitivityListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContactName;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForContactName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCell;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForCell;
@property (weak, nonatomic) IBOutlet UILabel *lblPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForPrimaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSecondaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblPrimaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblVlaueForPrimaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondaryEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblValueForSecondaryEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondaryEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnCellNo;
@property (weak, nonatomic) IBOutlet UIButton *btnCellNoNew;
//For Contact List Search

@property (weak, nonatomic) IBOutlet UILabel *lbl1_ContactName;
@property (weak, nonatomic) IBOutlet UILabel *lbl1CompanyNameForContactListSearch;

@end
