//
//  ChemicalSensitivityListTableViewCell.m
//  DPS
//
//  Created by Rakesh Jain on 03/07/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "ChemicalSensitivityListTableViewCell.h"

@implementation ChemicalSensitivityListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
 
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

@end
