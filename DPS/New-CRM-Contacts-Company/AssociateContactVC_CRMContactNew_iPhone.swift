//
//  AssociateContactVC_CRMContactNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 05/02/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AssociateContactVC_CRMContactNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    weak var delegateTaskAssociate: AddTaskAssociateProtocol?
    weak var delegateActivityAssociate: AddActivityAssociateProtocol?
    weak var delegateLeadAssociate: AddLeadAssociateProtocol?
    
    var selectionTag = Int()
    var strSelectionTag = ""
    
    // outlets
    @IBOutlet weak var searchBarContact: UISearchBar!
    @IBOutlet weak var tblviewContact: UITableView!
    
    
    // variables
    var dictLoginData = NSDictionary()
    var arrOfContacts = NSArray()
    var refresher = UIRefreshControl()
    var arrOfSection = NSMutableArray()
    var arrOfFilteredDataTemp = [NSDictionary]()
    var arrOfToday = [NSDictionary]()
    var arrOfYesterday = [NSDictionary]()
    var arrOfThisMonth = [NSDictionary]()
    var arrOfAllOther = [NSDictionary]()
    var strFromWhere = ""
    var strRefType = String()
    var strRefId = String()
    var lbl = UILabel()
    var strFromWhereForAssociation = ""
    var loader = UIAlertController()
    
    //view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            searchBarContact.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
        } else {
            // Fallback on earlier versions
        }
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        tblviewContact.tableFooterView = UIView()
        tblviewContact.estimatedRowHeight = 50.0
        
        arrOfSection.add("  Created Today")
        arrOfSection.add("  Created Yesterday")
        arrOfSection.add("  Created This Month")
        arrOfSection.add("  All")
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tblviewContact!.addSubview(refresher)
        
        setUpView()
        
        UserDefaults.standard.set(true, forKey: "RefreshContacts_AssociateContactList")
        
        setDefaultValuesForContactFilterAssociation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if nsud.bool(forKey: "DismissContacts_AssociateContactList") {
            
            UserDefaults.standard.set(false, forKey: "DismissContacts_AssociateContactList")
            
            if strFromWhere == "DirectAssociate" || strFromWhere == "DirectAssociateFromCompany"{
                
                UserDefaults.standard.set(true, forKey: "RefreshAssociations_CompanyDetails")
                
            } else {
                
                let dictOfContactsLocal = nsud.value(forKey: "ContactData") as! NSDictionary
                
                if strFromWhereForAssociation == "AddLeadCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedLeadProspectCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                    
                } else if strFromWhereForAssociation == "ConvertOpportunityCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedConvertOpportunityCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                } else if strFromWhereForAssociation == "EditOpportunityCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedEditOpportunityCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                } else if strFromWhereForAssociation == "EditLeadCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedEditLeadCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                } else if strFromWhereForAssociation == "AddActivityCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddActivityCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                } else if strFromWhereForAssociation == "AddTaskCRM" {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddTaskCRM_Notification"), object: nil, userInfo: dictOfContactsLocal as? [AnyHashable : Any])
                    
                }  //-------Association Task Activity------
                else if(strFromWhereForAssociation == "AddActivityCRM_Associations"){
                    self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictOfContactsLocal, tag: selectionTag)
                }
                else if(strFromWhereForAssociation == "AddTaskCRM_Associations"){
                    self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictOfContactsLocal, tag: selectionTag)
                }
                    
                    //---------------------------//
                else if(strFromWhereForAssociation == "AddLeadCRM_Associations"){
                    self.delegateLeadAssociate?.getDataAddLeadAssociateProtocol(notification: dictOfContactsLocal, tag: strSelectionTag)
                }
                
            }
            self.navigationController?.popViewController(animated: false)
            
        }else if nsud.bool(forKey: "RefreshContacts_AssociateContactList") {
            
            if (isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    UserDefaults.standard.set(false, forKey: "RefreshContacts_AssociateContactList")
                    
                    self.callApiToGetContacts()
                    
                })
                
            }else{
                
                self.noDataLbl()
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    
    // MARK: UItableView's delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrOfSection.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 0) {
            
            if (arrOfToday.count == 0) {
                
                return 0
                
            } else {
                
                return (DeviceType.IS_IPAD ? 50 : 40)
                
            }
            
        } else if (section == 1) {
            
            if (arrOfYesterday.count == 0) {
                
                return 0
                
            } else {
                
                return  (DeviceType.IS_IPAD ? 50 : 40)
                
            }
            
        } else if (section == 2) {
            
            if (arrOfThisMonth.count == 0) {
                
                return 0
                
            } else {
                
                return  (DeviceType.IS_IPAD ? 50 : 40)
                
            }
            
        } else if (section == 3) {
            
            if (arrOfAllOther.count == 0) {
                
                return 0
                
            } else {
                
                return  (DeviceType.IS_IPAD ? 50 : 40)
                
            }
            
        } else {
            
            return  (DeviceType.IS_IPAD ? 50 : 40)
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: (DeviceType.IS_IPAD ? 50 : 40)))
        //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        vw.backgroundColor = UIColor.lightTextColorTimeSheet()
        
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = arrOfSection[section] as? String
        lblHeader.font = UIFont.systemFont(ofSize: (DeviceType.IS_IPAD ? 21 : 17))
        lblHeader.font = UIFont(name: "Helvetica-Bold", size: (DeviceType.IS_IPAD ? 21 : 17))
        lblHeader.textColor = UIColor.theme()
        vw.addSubview(lblHeader)
        
        return vw
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: (DeviceType.IS_IPAD ? 50.0 : 40.0)))
        
        
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if (section == 0) {
            
            if (arrOfToday.count == 0) {
                
                return 0
                
            } else {
                
                return 0
                
            }
            
        } else if (section == 1) {
            
            if (arrOfYesterday.count == 0) {
                
                return 0
                
            } else {
                
                return 0
                
            }
            
        } else if (section == 2) {
            
            if (arrOfThisMonth.count == 0) {
                
                return 0
                
            } else {
                
                return 0
                
            }
            
        } else if (section == 3) {
            
            if (arrOfAllOther.count == 0) {
                
                return 0
                
            } else {
                
                return 0
                
            }
            
        } else {
            
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DeviceType.IS_IPAD ? UITableView.automaticDimension : 56
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return arrOfToday.count
            
        } else if section == 1 {
            
            return arrOfYesterday.count
            
        } else if section == 2 {
            
            return arrOfThisMonth.count
            
        } else  {
            
            return arrOfAllOther.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellContact") as! CellContact
        
        var dictOfContacts = NSDictionary()
        
        if indexPath.section == 0 {
            
            dictOfContacts = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictOfContacts = arrOfYesterday[indexPath.row]
            
        } else if indexPath.section == 2 {
            
            dictOfContacts = arrOfThisMonth[indexPath.row]
            
        } else  {
            
            dictOfContacts = arrOfAllOther[indexPath.row]
            
        }
        
        if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
        {
            
            cell.lblName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
            
            if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
                
                cell.lblName.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any])) + ", \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
                
            }
        }else{
            
            cell.lblName.text = ""
            
        }
        
        cell.lblCompanyName.text = "\(dictOfContacts.value(forKey: "CrmCompanyName") ?? "")"
        cell.btnEdit.titleLabel?.tag = indexPath.section
        cell.btnEdit.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(
            self.EditAction(sender:)), for: .touchUpInside)
        return cell
        
    }
    @objc func EditAction(sender: UIButton) {
        
        var dictOfContacts = NSDictionary()
        
        if sender.titleLabel?.tag == 0 {
            
            dictOfContacts = self.arrOfToday[sender.tag]
            
        } else if sender.titleLabel?.tag == 1 {
            
            dictOfContacts = self.arrOfYesterday[sender.tag]
            
        } else if sender.titleLabel?.tag == 2 {
            
            dictOfContacts = self.arrOfThisMonth[sender.tag]
            
        } else  {
            
            dictOfContacts = self.arrOfAllOther[sender.tag]
            
        }
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
            controller.strFrom = "Update"
            controller.dictContact = dictOfContacts
            controller.dictContactBasicInfo = NSDictionary()
            self.navigationController?.pushViewController(controller, animated: false)
            
        }
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 76
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        var dictOfContacts = NSDictionary()
        
        if indexPath.section == 0 {
            
            dictOfContacts = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictOfContacts = arrOfYesterday[indexPath.row]
            
        } else if indexPath.section == 2 {
            
            dictOfContacts = arrOfThisMonth[indexPath.row]
            
        } else  {
            
            dictOfContacts = arrOfAllOther[indexPath.row]
            
        }
        
        if strFromWhere == "DirectAssociate" || strFromWhere == "DirectAssociateFromCompany"{
            
            if strFromWhere == "DirectAssociateFromCompany" {
                
                UserDefaults.standard.set(true, forKey: "RefreshContacts_CompanyDetails")
                
            }
            
            self.associateContact(CrmContactId: "\(dictOfContacts.value(forKey: "CrmContactId") ?? "")")
            
        } else {
            
            if strFromWhereForAssociation == "AddLeadCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedLeadProspectCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
                
            } else if strFromWhereForAssociation == "ConvertOpportunityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedConvertOpportunityCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "EditOpportunityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedEditOpportunityCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "EditLeadCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedEditLeadCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "AddActivityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddActivityCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "AddTaskCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddTaskCRM_Notification"), object: nil, userInfo: dictOfContacts as? [AnyHashable : Any])
                
            }  //-------Association Task Activity------
            else if(strFromWhereForAssociation == "AddActivityCRM_Associations"){
                self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictOfContacts, tag: selectionTag)
            }
            else if(strFromWhereForAssociation == "AddTaskCRM_Associations"){
                self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictOfContacts, tag: selectionTag)
            }
                
                //---------------------------//
            else if(strFromWhereForAssociation == "AddLeadCRM_Associations"){
                self.delegateLeadAssociate?.getDataAddLeadAssociateProtocol(notification: dictOfContacts, tag: strSelectionTag)
            }
            
            self.navigationController?.popViewController(animated: false)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            var dictOfContacts = NSDictionary()
            
            if indexPath.section == 0 {
                
                dictOfContacts = self.arrOfToday[indexPath.row]
                
            } else if indexPath.section == 1 {
                
                dictOfContacts = self.arrOfYesterday[indexPath.row]
                
            } else if indexPath.section == 2 {
                
                dictOfContacts = self.arrOfThisMonth[indexPath.row]
                
            } else  {
                
                dictOfContacts = self.arrOfAllOther[indexPath.row]
                
            }
            
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                controller.strFrom = "Update"
                controller.dictContact = dictOfContacts
                controller.dictContactBasicInfo = NSDictionary()
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [edit]
        
    }
    
    // MARK: UIbutton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnCompanies(_ sender: UIButton) {
        self.view.endEditing(true)
        
        gotoCompany()
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        gotoContactFilterVC()
        
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            //self.constHghtSearchBar.constant = 56.0
        }
    }
    
    @IBAction func actionOnAddContact(_ sender: UIButton) {
        self.view.endEditing(true)
        
        goToAddContact()
        
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnNearBy(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedAgreement()
            
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnDashBoard(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.goToDashboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.goToAppointment()
        
    }
    
    // MARK: Functions
    
    func setUpView()
    {
        
        if let txfSearchField = searchBarContact.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none//.roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15//txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
            
        }
        searchBarContact.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarContact.layer.borderWidth = 1
        searchBarContact.layer.opacity = 1.0
        
    }
    func logicForFilterContacts(arrToFilter : NSArray) {
        
        // For Today Date Filter
        
        let datee = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == datee)
            
            } as! [NSDictionary]
        
        var sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfToday = sortedArray
        
        // For YesterDay Date Filter
        
        let date1 = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == date1)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfYesterday = sortedArray
        
        // For Current Month Date Filter
        
        let dictCurrentMonthDate = Global().getCureentMonthDate()
        
        let dateStart = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "StartDate") as? String, "MM/dd/yyyy", "UTC") as Date
        
        let dateEnd = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "EndDate") as? String, "MM/dd/yyyy", "UTC") as Date
        
        let yesterDay = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date
        
        let todayDate = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  ((Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! >= dateStart) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! <= dateEnd) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != todayDate) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != yesterDay))
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfThisMonth = sortedArray
        
        // For AllOther Date Filter
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! < dateStart)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfAllOther = sortedArray
        
        self.tblviewContact.reloadData()
        
    }
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.callApiToGetContacts()
                
            })
            
        }else{
            
            self.noDataLbl()
            
        }
        
    }
    
    func noDataLbl() {
        
        self.tblviewContact.isHidden = true
        
        lbl.removeFromSuperview()
        
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        lbl.center = self.view.center
        lbl.text = "No Data Found"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 20)
        //   self.view.addSubview(lbl)
        
    }
    
    func associateContact(CrmContactId : String)
    {
        
        if !isInternetAvailable()
        {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAssociateContact + CrmContactId + "&RefType=" + strRefType + "&RefId=" + strRefId + "&EmployeeId=" + "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            
            strURL = strURL.trimmingCharacters(in: .whitespaces)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "AssociateContactCompany") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status)
                    {
                        
                        if Response["data"] is String {
                            
                            guard let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)  else {
                                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                            
                            // let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)
                            
                            if dictResponse.count > 0{
                                let strOperationResult = dictResponse["OperationResult"] as? Bool ?? false
                                var strMessage = dictResponse["Message"] as? String ?? ""
                                
                            
                            if strOperationResult {
                                
                                if self.strFromWhere == "DirectAssociateFromCompany" {
                                    
                                    // Notification to refresh Contact list if something added or edited.
                                    
                                }
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Associated Successfully", preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedContact_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                    
                                    UserDefaults.standard.set(true, forKey: "RefreshAssociations_CompanyDetails")
                                    
                                    self.navigationController?.popViewController(animated: false)
                                })
                                
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            } else {
                                
                                if strMessage.count > 0 {
                                    
                                    
                                    
                                } else {
                                    
                                    strMessage = alertSomeError
                                    
                                }
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)
                                
                                
                            }
                            
                            }
                            
                           else {
                               
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                               
                           }
                           
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
                
            }
            
        }
        
    }
    func callApiToGetContacts(){
        
        lbl.removeFromSuperview()
        
        //setDefaultValuesForContactFilterAssociation()
        
        let dictToSend = nsud.value(forKey: "CrmContactFilterAssociation")
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend!) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmContacts
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmContactsNew"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.loader.dismiss(animated: false) {
                
                self.refresher.endRefreshing()
                
                if(success)
                {
                    //Contacts
                    
                    let dictResponse = (response as NSDictionary?)!
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("Contacts") {
                            
                            self.arrOfContacts = dictResponse.value(forKey: "Contacts") as! NSArray
                            
                            if self.arrOfContacts.count > 0 {
                                
                                self.tblviewContact.isHidden = false
                                //self.tblviewContact.reloadData()
                                self.logicForFilterContacts(arrToFilter: self.arrOfContacts)
                                
                            } else {
                                
                                self.noDataLbl()
                                
                            }
                            
                            
                        } else {
                            
                            self.noDataLbl()
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }
                        
                        
                    } else {
                        
                        self.noDataLbl()
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                    
                    
                }else {
                    
                    self.noDataLbl()
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
                
            }
        }
    }
    
    func gotoCompany()
    {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyVC_CRMContactNew_iPhone") as! CompanyVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToAddContact(){
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
        controller.strFromAssociate = "true"
        controller.strRefId = strRefId
        controller.strRefType = strRefType
        controller.strFromWhere = strFromWhere
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToContactDetails(dictContact : NSDictionary){
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        
        
        
        controller.strCRMContactIdGlobal = "\(dictContact.value(forKey: "CrmContactId")!)"
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToDashboard(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToAppointment(){
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    
    func goToNearBy(){
        /*
         if(DeviceType.IS_IPAD){
         let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
         self.navigationController?.pushViewController(vc!, animated: false)
         }else{
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
         self.navigationController?.pushViewController(vc!, animated: false)
         }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
        
    }
    
    func goToSignedAgreement(){
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
        
    }
    func gotoContactFilterVC()
    {
        let mainStoryboard = UIStoryboard(
            name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "ContactFilterVC") as! ContactFilterVC
        self.navigationController?.present(controller, animated: false, completion: nil)
    }
    // MARK: UISearchBar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count == 0)
        {
            
            logicForFilterContacts(arrToFilter: arrOfContacts)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.view.endEditing(true)
                
            })
            
        }else {
            
            let aryTemp = arrOfContacts.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "FirstName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "MiddleName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "LastName")!)".lowercased().contains(searchText.lowercased())
                
                } as NSArray
            
            logicForFilterContacts(arrToFilter: aryTemp)
            
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        
        if(searchBar.text?.count == 0)
        {
            
            logicForFilterContacts(arrToFilter: arrOfContacts)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.view.endEditing(true)
                
            })
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
            
        }
        else
        {
            
            let aryTemp = arrOfContacts.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "FirstName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "MiddleName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "LastName")!)".lowercased().contains(searchBar.text!.lowercased())
                } as NSArray
            
            logicForFilterContacts(arrToFilter: aryTemp)
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBarContact.text = ""
        
        logicForFilterContacts(arrToFilter: arrOfContacts)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.view.endEditing(true)
            
        })
        
    }
}
