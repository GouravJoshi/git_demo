//
//  AddCompanyVC_CRMContactNew_iPhone.swift
//  DPS
//  peSTReam 2020
//  Created by Rakesh Jain on 31/01/20.
//  Copyright © 2020 Saavan. All rights reserved.


import UIKit

class AddCompanyVC_CRMContactNew_iPhone: UIViewController
{

      // MARK: --------------------------- Outlets  ---------------------------
    
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextField!
    @IBOutlet weak var txtfldWebsite: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhoneExt: ACFloatingTextField!
    @IBOutlet weak var txtfldEmail: ACFloatingTextField!
    @IBOutlet weak var btnMoreDetails: UIButton!
    @IBOutlet weak var imgBtnMoreDetail: UIImageView!
    @IBOutlet weak var txtfldSecondaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldSecondaryPhoneExt: ACFloatingTextField!
    @IBOutlet weak var txtfldSecondaryEmail: ACFloatingTextField!
    @IBOutlet weak var txtfldNoOfEmployee: ACFloatingTextField!
    @IBOutlet weak var txtfldAnnualRevenue: ACFloatingTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var constHghtViewMoreDetails: NSLayoutConstraint!
    @IBOutlet weak var txtfldSourceName: ACFloatingTextField!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var txtfldAddress: ACFloatingTextField!
    @IBOutlet weak var constHghtLessDetailView: NSLayoutConstraint!
    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var imgViewContactProfile: UIImageView!
    @IBOutlet weak var lblProfileImageName: UILabel!

    
    @IBOutlet weak var txtfldPropertyType: ACFloatingTextField!

    // MARK: --------------------------- Variables  ---------------------------

     private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
     private var placeType: PlaceType = .all
     private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
     private var radius: Double = 0.0
     private var strictBounds: Bool = false
     
     private var places = [Place]() {
         didSet { tableView.reloadData() }
     }
    @IBOutlet weak var tableView: UITableView!
    
    var moreDetails = MoreDetails.more
    
    var dictLoginData = NSDictionary()
    var strSourceId = ""
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strAddressLine2 = ""
    var strZipCode = ""
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strFrom = ""
    var dictCompany = NSDictionary()
    var dictCompanyBasicInfo = NSDictionary()
    var imagePicker = UIImagePickerController()
    var strGlobalImageName = ""
    let dispatchGroup = DispatchGroup()
    var strGlobalImageNameIfAlreadyPresent = ""
    var strFromAssociate = ""
    var strRefType = String()
    var strRefId = String()
    var strFromWhereForAssociation = ""
    var strFromWhere = ""
    var loader = UIAlertController()
    
    var aryPropertySelected = NSMutableArray()
    // MARK: --------------------------- View's life cycle  ---------------------------

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfldEmail.placeholder = "Primary Email"
        constHghtViewMoreDetails.constant = 0.0
        constHghtLessDetailView.constant = DeviceType.IS_IPAD ? 255.0 : 165.0
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"

   //     self.allTextFldActionsAddCompanyView()
        txtAddressMaxY = (txtfldAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
        
        if strFrom == "Update" {
            
            btnSave.setTitle("Update", for: .normal)
            lblMainHeader.text = "Update Company"

            if(isInternetAvailable() == false)
            {
                
                if dictCompanyBasicInfo.count == 0 {
                    
                    self.fillCompanyDetailToUpdate()

                }else{
                    
                    self.navigationController?.popViewController(animated: false)

                }
                //self.navigationController?.popViewController(animated: false)

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            else
            {
                
                if dictCompanyBasicInfo.count == 0 {
                    
                    // Call API to fetch Company Data

                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.callAPIToGetCompanyBasicInfo()

                    })
                                        
                } else {
                    
                    self.fillCompanyDetailToUpdate()
                    
                }
                
            }
            
        }else{
            btnSave.setTitle("Save", for: .normal)
            lblMainHeader.text = "Add Company"
            imgViewContactProfile.isHidden = false
            lblProfileImageName.isHidden = true
            
        }
        lblProfileImageName.layer.borderWidth = 1.0
        lblProfileImageName.layer.borderColor = UIColor.darkGray.cgColor
        lblProfileImageName.layer.cornerRadius = lblProfileImageName.frame.size.width / 2
        imgViewContactProfile.layer.borderWidth = 1.0
        imgViewContactProfile.layer.masksToBounds = false
        imgViewContactProfile.layer.borderColor = UIColor.darkGray.cgColor
        imgViewContactProfile.layer.cornerRadius = imgViewContactProfile.frame.size.width / 2
        imgViewContactProfile.clipsToBounds = true
        
        self.btnMoreDetails.setTitle("More Details", for: .normal)
        self.imgBtnMoreDetail.image = UIImage(named: "drop_down_ipad")
       self.constHghtViewMoreDetails.constant = 0.0
     scrollView.contentSize = CGSize(width: self.view.frame.width, height: DeviceType.IS_IPAD ? 300.0 : 250.0)

    }
    
    override func viewDidLayoutSubviews() {

        btnMoreDetails.layer.cornerRadius =  btnMoreDetails.frame.size.height/2
        btnMoreDetails.layer.borderColor = UIColor.gray.cgColor
        btnMoreDetails.layer.borderWidth = 0.5
        btnMoreDetails.layer.masksToBounds = true
        
        /*btnSource.layer.cornerRadius = 5.0
        btnSource.layer.borderColor = UIColor.gray.cgColor
        btnSource.layer.borderWidth = 0.5*/
        
       /* btnCity.layer.cornerRadius = 5.0
        btnCity.layer.borderColor = UIColor.gray.cgColor
        btnCity.layer.borderWidth = 0.5
        
        btnState.layer.cornerRadius = 5.0
        btnState.layer.borderColor = UIColor.gray.cgColor
        btnState.layer.borderWidth = 0.5*/
        
        txtviewDescription.layer.cornerRadius = 5.0
        txtviewDescription.layer.borderColor = UIColor.gray.cgColor
        txtviewDescription.layer.borderWidth = 0.5
        
        btnSave.layer.cornerRadius = btnSave.frame.size.height/2.0
        btnSave.layer.masksToBounds = true
        
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
   
        
    }
    // MARK: --------------------------- UIButton action ---------------------------

    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnMoreDetails(_ sender: UIButton) {
        
        self.view.endEditing(true)
        showLessAndMore()
        
    }
    func showLessAndMore() {
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        switch moreDetails {
             case .more:
                 self.btnMoreDetails.setTitle("Less Details", for: .normal)
                 self.imgBtnMoreDetail.image = UIImage(named: "drop_down_up_ipad")
               //  constHghtLessDetailView.constant = 0.0
                 self.constHghtViewMoreDetails.constant = DeviceType.IS_IPAD ? 750.0 : 520.0
               //  scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                scrollView.contentSize = CGSize(width: self.view.frame.width, height: DeviceType.IS_IPAD ? 800.0 : 600.0)
                 moreDetails = MoreDetails.less
                 return
             case .less:
                 
                 self.btnMoreDetails.setTitle("More Details", for: .normal)
                 self.imgBtnMoreDetail.image = UIImage(named: "drop_down_ipad")
             
                self.constHghtViewMoreDetails.constant = 0.0
             //   constHghtLessDetailView.constant = DeviceType.IS_IPAD ? 255.0 : 165.0
              //  scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
              scrollView.contentSize = CGSize(width: self.view.frame.width, height: DeviceType.IS_IPAD ? 300.0 : 250.0)
                 moreDetails = MoreDetails.more
                 return
             }
        
         
    }
    
    @IBAction func actionOnSave(_ sender: UIButton)
    {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        let ifError = self.validationCompany()
        
        if ifError {
           // showLessAndMore()
            // Error do no call API
           // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid data.", viewcontrol: self)
            
        }else{
            
            // check if profile image taken then upload image first and then upload data
            
            if strGlobalImageName.count > 0 {
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    
                    self.callAPIToUploadCompanyProfileImage()

                    self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                        
                        //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                        self.loader.dismiss(animated: false) {

                            

                        }
                        
                    })
                    
                })
                
            } else {
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    
                    self.convertEnteredDetailsToArrayCompanyView()
                    
                })

            }

        }
        
    }
    
    @IBAction func actionOnSource(_ sender: UIButton)
    {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

            self.view.endEditing(true)

        })
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count > 0)
                {

                    //arrOfData = addSelectInArray(strName: "Name", array: arrOfData)

                    goToNewSelectionVC(arrOfData: returnFilteredArray(array: arrOfData), tag: 101, titleHeader: "Source")

                    //openTableViewPopUp(tag: 801, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
                    
                }
                else
                {
                    // show alert no data available
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
                
            }

            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnCompanyProfileImage(_ sender: UIButton) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
            
            let imageView = UIImageView(image: self.imgViewContactProfile.image)
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            self.present(testController!, animated: false, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
         if let popoverController = alert.popoverPresentationController {
                     popoverController.sourceView = sender as UIView
                     popoverController.sourceRect = sender.bounds
                    // popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
                 }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func action_AddAddress(_ sender: Any) {
          
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
        
    }
    
    @IBAction func action_SelectPropertyType(_ sender: Any) {
        if(self.getAddressTypeFromMasterSalesAutomation().count != 0){
            openTableViewPopUp(tag: 65, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: aryPropertySelected)

        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Property type not found!!!", viewcontrol: self)
            
        }
        
    }

    
    // MARK: --------------------------- Local Function ---------------------------
    
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func getPropertyTypeNameFromID_FromMasterSalesAutomation(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "AddressPropertyTypeId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    

    
    func goToNewSelectionVC(arrOfData : NSMutableArray, tag : Int, titleHeader : String) {
        
        if(arrOfData.count != 0){
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectGlobalVC") as! SelectGlobalVC
            controller.titleHeader = titleHeader
            controller.arrayData = arrOfData
            controller.arraySelectionData = arrOfData
            controller.arraySelected = NSMutableArray()
            controller.delegate = self
            controller.tag = tag
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    func downloadProfileImage() {
        
        if let imgURL = dictCompanyBasicInfo.value(forKey: "ProfileImage")
        {
            if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
            {
                
                self.createCompanyLogoFromName(dictData: dictCompanyBasicInfo)
                
            }
            else
            {
                
                imgViewContactProfile.isHidden = false
                lblProfileImageName.isHidden = true
                
                // Remove Logic for image URL
                
                let imgUrlNew = replaceBackSlasheFromUrl(strUrl: imgURL as! String)
                
                let arrImg = imgUrlNew.components(separatedBy: "/")
                
                if arrImg.count > 0 {
                    
                    strGlobalImageNameIfAlreadyPresent = arrImg.last!
                    
                }
                
                //cell.imgviewProfile.sd_setImage(with: URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), options: .refreshCached)
                imgViewContactProfile.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                
            }
        }else{
            
            self.createCompanyLogoFromName(dictData: dictCompanyBasicInfo)
            
        }
        
    }
    
    func createCompanyLogoFromName(dictData : NSDictionary) {
        
        lblProfileImageName.layer.borderWidth = 1.0
        lblProfileImageName.layer.masksToBounds = false
        lblProfileImageName.layer.borderColor = UIColor.white.cgColor
        lblProfileImageName.layer.cornerRadius = 30//imgviewProfile.frame.size.width / 2
        lblProfileImageName.clipsToBounds = true
        
        
        imgViewContactProfile.isHidden = true
        lblProfileImageName.isHidden = false
        let firstName = "\(dictData.value(forKey: "Name") ?? "")"
        
        lblProfileImageName.text = firstCharactersFromString(type: "CompanyName", first: firstName, second: "")
        
    }
    
    fileprivate func callAPIToUploadCompanyProfileImage(){

        self.dispatchGroup.enter()

        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlUploadCompanyProfileImage
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.callAPIWithImage(parameter: NSDictionary(), url: strURL, image: imgViewContactProfile.image!, fileName: strGlobalImageName, withName: strGlobalImageName) { (response, status) in
            
            self.convertEnteredDetailsToArrayCompanyView()

            self.dispatchGroup.leave()

            if(status == "success")
            {
                print(response)
            }
            else
            {
                // Failed to uplaod image
            }
            
        }
    }
    
    func fillCompanyDetailToUpdate() {
        
        if dictCompanyBasicInfo.count == 0 {
            
            
            
        } else {
            
            dictCompanyBasicInfo = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictCompanyBasicInfo as! [AnyHashable : Any]))! as NSDictionary

            txtfldCompanyName.text = "\(dictCompanyBasicInfo.value(forKey: "Name")!)"
            txtfldAddress.text = Global().strCombinedAddress(dictCompanyBasicInfo as? [AnyHashable : Any])
            txtfldWebsite.text = "\(dictCompanyBasicInfo.value(forKey: "Website")!)"
            txtfldEmail.text = "\(dictCompanyBasicInfo.value(forKey: "PrimaryEmail")!)"
            strSourceId = "\(dictCompanyBasicInfo.value(forKey: "SourceId")!)"
            txtfldSourceName.text = getSourceNameFromId(sourceId: "\(dictCompanyBasicInfo.value(forKey: "SourceId")!)")
            
            let formattedPrimaryPhoneNo = formattedNumber(number: "\(dictCompanyBasicInfo.value(forKey: "PrimaryPhone")!)")

            txtfldPrimaryPhone.text = formattedPrimaryPhoneNo
            txtfldPrimaryPhoneExt.text = "\(dictCompanyBasicInfo.value(forKey: "PrimaryPhoneExt")!)"
            
            let formattedSecondaryPhoneNo = formattedNumber(number: "\(dictCompanyBasicInfo.value(forKey: "SecondaryPhone")!)")

            txtfldSecondaryPhone.text = formattedSecondaryPhoneNo
            txtfldSecondaryPhoneExt.text = "\(dictCompanyBasicInfo.value(forKey: "SecondaryPhoneExt")!)"
            txtfldSecondaryEmail.text = "\(dictCompanyBasicInfo.value(forKey: "SecondaryEmail")!)"
            txtfldNoOfEmployee.text = "\(dictCompanyBasicInfo.value(forKey: "EmployeeCount")!)"
            txtfldAnnualRevenue.text = "\(dictCompanyBasicInfo.value(forKey: "AnnualRevenue")!)"
            txtviewDescription.text = "\(dictCompanyBasicInfo.value(forKey: "Description")!)"
            
            
            let propertyTypeID =  "\(dictCompanyBasicInfo.value(forKey: "AddressPropertyTypeId")!)"
            self.aryPropertySelected = NSMutableArray()
            self.aryPropertySelected = getPropertyTypeNameFromID_FromMasterSalesAutomation(id: propertyTypeID)
            if(self.aryPropertySelected .count != 0){
                let dictData = self.aryPropertySelected.object(at: 0)as! NSDictionary
                txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
            }

        }
        
        downloadProfileImage()

    }
    
    fileprivate func callAPIToGetCompanyBasicInfo(){

        var strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetCompanyBasicInfo + "\(dictCompany.value(forKey: "CrmCompanyId")!)"

        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrl, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
         self.loader.dismiss(animated: false) {

            if(status)
            {
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if((response.value(forKey: "data") as! NSDictionary).count > 0)
                    {
                        self.dictCompanyBasicInfo = (response.value(forKey: "data") as! NSDictionary)
                        self.fillCompanyDetailToUpdate()
                        
                    }
                    
                }
                
            }

         }
            
        }
    }
    
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func convertEnteredDetailsToArrayCompanyView() {
        
        var crmCompanyIdToUpdate = ""
        
        if strFrom == "Update" {

            crmCompanyIdToUpdate = "\(dictCompanyBasicInfo.value(forKey: "CrmCompanyId")!)"
            
        }
        
        var imageNameToBeSent = ""
        
        if strGlobalImageName.count > 0 {
            
            strGlobalImageName = "/" + strGlobalImageName
            
            imageNameToBeSent = strGlobalImageName
            
        } else {
            
            //loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            //self.present(loader, animated: false, completion: nil)
            
        }
        
        if strFrom == "Update" && strGlobalImageName.count == 0 {

            if strGlobalImageNameIfAlreadyPresent.count > 0 {
                
                strGlobalImageNameIfAlreadyPresent = "/" + strGlobalImageNameIfAlreadyPresent
                
                imageNameToBeSent = strGlobalImageNameIfAlreadyPresent

            }
            
        }
        

        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyKey",
                "Name",
                "SourceId",
                "PrimaryPhone",
                "PrimaryPhoneExt",
                "SecondaryPhone",
                "SecondaryPhoneExt",
                "CellPhone1",
                "PrimaryEmail",
                "SecondaryEmail",
                "Address1",
                "Address2",
                "CityName",
                "StateId",
                "ZipCode",
                "CountryId",
                "ProfileImage",
                "EmployeeCount",
                "AnnualRevenue",
                "Description",
                "CrmContactId",
                "EmployeeId",
                "Website",
                "CrmCompanyId","AddressPropertyTypeId"]
        
        values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                  (txtfldCompanyName.text?.count)! > 0 ? txtfldCompanyName.text! : "",
                  strSourceId,
                  (txtfldPrimaryPhone.text?.count)! > 0 ? getOnlyNumberFromString(number: txtfldPrimaryPhone.text!) : "",
                  (txtfldPrimaryPhoneExt.text?.count)! > 0 ? txtfldPrimaryPhoneExt.text! : "",
                  (txtfldSecondaryPhone.text?.count)! > 0 ? getOnlyNumberFromString(number: txtfldSecondaryPhone.text!) : "",
                  (txtfldSecondaryPhoneExt.text?.count)! > 0 ? txtfldSecondaryPhoneExt.text! : "",
                  "",
                  (txtfldEmail.text?.count)! > 0 ? txtfldEmail.text! : "",
                  (txtfldSecondaryEmail.text?.count)! > 0 ? txtfldSecondaryEmail.text! : "",
                  strAddressLine1,
                  strAddressLine2,
                  strCityName,
                  strStateId,
                  strZipCode,
                  "1",
                  imageNameToBeSent,
                  (txtfldNoOfEmployee.text?.count)! > 0 ? txtfldNoOfEmployee.text! : "",
                  (txtfldAnnualRevenue.text?.count)! > 0 ? txtfldAnnualRevenue.text! : "",
                  (txtviewDescription.text?.count)! > 0 ? txtviewDescription.text! : "",
                  "",
                  "\(dictLoginData.value(forKeyPath: "EmployeeId")!)",
                  (txtfldWebsite.text?.count)! > 0 ? txtfldWebsite.text! : "",
                  crmCompanyIdToUpdate,"\(txtfldPropertyType.tag)" == "0" ? "" : "\(txtfldPropertyType.tag)"]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
                
        if dictToSend.count > 0 {
            
            callApiToAddCompany(dictOfCompany: dictToSend)
            
        } else {
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

            }
            
        }
        
    }
    
    func associateCompany(CrmCompanyId : String)
    {
        
        if !isInternetAvailable()
        {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAssociateCompany + CrmCompanyId + "&RefType=" + strRefType + "&RefId=" + strRefId + "&EmployeeId=" + "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            
            strURL = strURL.trimmingCharacters(in: .whitespaces)
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "AssociateContactCompany") { (Response, Status) in
                
                
                self.loader.dismiss(animated: false) {
                    
                    if(Status)
                    {
                        
                        if Response["data"] is String {
                            guard let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)  else {
                                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                            //let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)
                            if dictResponse.count > 0{
                                let strOperationResult = dictResponse["OperationResult"] as? Bool ?? false
                                var strMessage = dictResponse["Message"] as? String ?? ""
                                
                                
                                if strOperationResult {
                                    
                                    let alertCOntroller = UIAlertController(title: Info, message: "Company created and associated successfully", preferredStyle: .alert)
                                    
                                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                        
                                        UserDefaults.standard.set(true, forKey: "DismissCompany_AssociateCompanyList")
                                        self.navigationController?.popViewController(animated: false)
                                    })
                                    
                                    alertCOntroller.addAction(alertAction)
                                    self.present(alertCOntroller, animated: true, completion: nil)
                                    
                                } else {
                                    
                                    if strMessage.count > 0 {
                                        
                                        
                                        
                                    } else {
                                        
                                        strMessage = alertSomeError
                                        
                                    }
                                    
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)
                                    
                                    
                                }}
                            else {
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
                
                
                
            }
            
        }
        
    }
    
    func associateCompanyFromContact(CrmCompanyId : String)
    {
        
        if !isInternetAvailable()
        {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)

        }
        else
        {

            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAssociateContact + strRefId + "&RefType=" + enumRefTypeCrmCompany + "&RefId=" + CrmCompanyId + "&EmployeeId=" + "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            
            strURL = strURL.trimmingCharacters(in: .whitespaces)
            
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "AssociateContactCompany") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {

                    if(Status)
                    {
                        
                        if Response["data"] is NSString {
                            
                            let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)
                            
                            let strOperationResult = dictResponse!["OperationResult"] as! Bool
                            var strMessage = dictResponse!["Message"] as! String
                            
                            if strOperationResult {

                                let alertCOntroller = UIAlertController(title: Info, message: "Company created and associated successfully", preferredStyle: .alert)
                                                  
                                                  let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                                      
                                                      UserDefaults.standard.set(true, forKey: "DismissCompany_AssociateCompanyList")
                                                      self.navigationController?.popViewController(animated: false)
                                                  })
                                                  
                                                  alertCOntroller.addAction(alertAction)
                                                  self.present(alertCOntroller, animated: true, completion: nil)
                                
                            } else {
                                
                                if strMessage.count > 0 {
                                    
                                    
                                    
                                } else {
                                    
                                    strMessage = alertSomeError
                                    
                                }
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)

                                
                            }


                            
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                        }

                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }

                
            }
            
        }
        
    }
    
    func finalDismissViewCompany(response : NSDictionary) {
        
        
        if self.strFromWhere == "DirectAssociate" {
            
            if response.value(forKey: "Response")! is NSDictionary {
                
                let data = response.value(forKey: "Response")! as! NSDictionary
                self.associateCompany(CrmCompanyId: "\(data.value(forKey: "CrmCompanyId")!)")
                
            }
            
        }else if self.strFromWhere == "DirectAssociateFromContact" {
               
            if response.value(forKey: "Response")! is NSDictionary {
                    
                let data = response.value(forKey: "Response")! as! NSDictionary
                self.associateCompanyFromContact(CrmCompanyId: "\(data.value(forKey: "CrmCompanyId")!)")

            }
            
        } else {
            
            self.loader.dismiss(animated: false) {

                if self.strFromAssociate == "true" {

                    if response.value(forKey: "Response")! is NSDictionary {
                        
                        let data = response.value(forKey: "Response")! as! NSDictionary
                        
                        let alertCOntroller = UIAlertController(title: Info, message: "Company Added Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            UserDefaults.standard.set(true, forKey: "DismissCompany_AssociateCompanyList")
                            
                            UserDefaults.standard.set(data, forKey: "CompanyData")

                            self.navigationController?.popViewController(animated: false)
                            
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    } else {
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }

                }else{
                    
                    // Notification to refresh Contact list if something added or edited.
                    UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
                    UserDefaults.standard.set(true, forKey: "RefreshCompany_AssociateCompanyList")
                    UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyDetails")

                    if self.strFrom == "Update" {

                        let alertCOntroller = UIAlertController(title: Info, message: "Company Updated Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
                            
                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }else{
                        
                        let alertCOntroller = UIAlertController(title: Info, message: "Company Added Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
                            
                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                    
                }


            }
            
        }
        
    }
    
    func callApiToAddCompany(dictOfCompany : NSDictionary) {
        
        if strGlobalImageName.count > 0 {
            
            
            
        } else {
            
            self.dispatchGroup.enter()

        }
        
        if strFromAssociate == "true" {

            self.dispatchGroup.enter()

        }
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictOfCompany) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictOfCompany, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddCrmCompaniesV2

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "CrmCompanyV2"  // CrmCompanyV2
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            if self.strGlobalImageName.count > 0 {
                
                
                
            } else {
                
                self.dispatchGroup.leave()

            }
            
            if(success)
            {
                //Response
                                
               let strResponse = "\((response as NSDictionary?)!.value(forKey: "ReturnMsg")!)"
                
                if strResponse == "true" {
                    
                    self.finalDismissViewCompany(response: (response as NSDictionary?)!)
                    
                } else {
                    
                    self.loader.dismiss(animated: false) {

                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                    }
                }
                
            }else {
                
                self.loader.dismiss(animated: false) {

                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                }
            }

        }
        
    }
    func validationCompany() -> Bool {

        var error = false
    
        if txtfldCompanyName.text?.count == 0 {
            
            error = true
          
            txtfldCompanyName.becomeFirstResponder()

            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_CompanyName, viewcontrol: self)
            return true
        }
        
        if (txtfldAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtfldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                return true
                
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return true
                    
                }
                if strCityName.count == 0 {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return true
                    
                }
                if strStateId.count == 0 {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return true
                    
                }
                if strZipCode.count == 0 {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return true
                    
                }
                if strZipCode.count != 5 {

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return true
                    
                }
                
            }
                        
        }
        
        /*if (txtfldAddress.text!.count > 0) {
            
                // Fetch Address
                
            let arrOfAddress = self.getAddress(from: txtfldAddress.text!)
            
            let tempArrayOfKeys = NSMutableArray()
            let tempDictData = NSMutableDictionary()

            for k in 0 ..< arrOfAddress.count {
             
                      if arrOfAddress[k] is NSDictionary {
                
                         let tempDict = arrOfAddress[k] as! NSDictionary
                    
                         tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                         tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                      }
                
            }
            
            if tempArrayOfKeys.contains("City") {
                
                strCityName = "\(tempDictData.value(forKey: "City")!)"

            }
            if tempArrayOfKeys.contains("ZIP") {
                
                strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

            }
            if tempArrayOfKeys.contains("State") {
                
                let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                    
                }else{
                    
                    strStateId = ""
                    //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                }

            }
            if tempArrayOfKeys.contains("Street") {
                
                strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

            }
            
            if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
                
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
                
                return true
                
            }
            
        }*/
        
        if txtfldPrimaryPhone.text!.count > 0 {

            let noLocal = getOnlyNumberFromString(number: txtfldPrimaryPhone.text!)
            
            if noLocal.count != 10 {
                
                error = true
                txtfldPrimaryPhone.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Primary phone invalid!", viewcontrol: self)

                return true
            }
            
        }
        
        if txtfldEmail.text!.count > 0 {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtfldEmail.text!)
            
            if !isValid {
                
                error = true
                txtfldEmail.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Primary email address invalid!", viewcontrol: self)

         
                             return true
            }
            
            /*let isvalidateEmail = validateEmail(email: txtfldEmail.text!)
            
            if !isvalidateEmail {
                
                error = true
                txtfldEmail.errorTextColor = UIColor.red
                txtfldEmail.errorLineColor = UIColor.red
                txtfldEmail.showError(withText: Alert_EmailAddress)
                
            }*/

        }
        if txtfldSecondaryPhone.text!.count > 0 {

            let noLocal = getOnlyNumberFromString(number: txtfldSecondaryPhone.text!)
            
            if noLocal.count != 10 {
                
                error = true
                txtfldSecondaryPhone.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Secondary phone invalid!", viewcontrol: self)

                             return true
            }
            
        }
        
        if txtfldSecondaryEmail.text!.count > 0 {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtfldSecondaryEmail.text!)
            
            if !isValid {
                
                error = true
                txtfldSecondaryEmail.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Secondary email address invalid!", viewcontrol: self)

                             return true
            }
            
            /*let isvalidateEmail = validateEmail(email: txtfldSecondaryEmail.text!)
            
            if !isvalidateEmail {
                
                error = true
                txtfldSecondaryEmail.errorTextColor = UIColor.red
                txtfldSecondaryEmail.errorLineColor = UIColor.red
                txtfldSecondaryEmail.showError(withText: Alert_EmailAddress)
                
            }*/

        }
        
        return error
        
    }
//
//    func allTextFldActionsAddCompanyView() {
//
//        txtfldSourceName.addTarget(self, action: #selector(selectSourceName), for: .allEvents)
//
//    }
//
//    @objc func selectSourceName(textField: UITextField) {
//
//        self.view.endEditing(true)
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//
//            self.view.endEditing(true)
//
//        })
//
//        let defsLogindDetail = UserDefaults.standard
//
//        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
//
//            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
//
//            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
//
//                var arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
//
//                if(arrOfData.count > 0)
//                {
//
//                    arrOfData = addSelectInArray(strName: "Name", array: arrOfData)
//
//                    openTableViewPopUp(tag: 801, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
//
//                }
//                else
//                {
//                    // show alert no data available
//
//                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
//
//                }
//
//            } else {
//
//                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
//
//            }
//
//
//        } else {
//
//            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
//
//        }
//
//    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
}

// MARK: --------------------------- Extensions ---------------------------
extension AddCompanyVC_CRMContactNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        // For PropertyType
         if(tag == 65)
        {
            if(dictData.count == 0){
                txtfldPropertyType.text = ""
                txtfldPropertyType.tag = 0
                aryPropertySelected = NSMutableArray()
            }else{
                aryPropertySelected = NSMutableArray()
                aryPropertySelected.add(dictData)
                txtfldPropertyType.text = "\(dictData.value(forKey: "Name") ?? "")"
                txtfldPropertyType.tag = Int("\(dictData.value(forKey: "AddressPropertyTypeId") ?? "0")")!
            }
        }
    }
}


extension AddCompanyVC_CRMContactNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if(tag == 302)
        {
            //btnDueDate.setTitle(strDate, for: .normal)
        }
        if(tag == 303)
        {
            // btnDueTime.setTitle(strDate, for: .normal)
        }
        if(tag == 304)
        {
            // btnReminderDate.setTitle(strDate, for: .normal)
        }
        if(tag == 305)
        {
            // btnReminderTime.setTitle(strDate, for: .normal)
        }
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AddCompanyVC_CRMContactNew_iPhone : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == txtfldSourceName  {
            
            return false
            
        } else if textField == txtfldPrimaryPhone  {
            
            txtfldPrimaryPhone.hideError()

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldPrimaryPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldPrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        } else if textField == txtfldPrimaryPhoneExt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 10)
            
        } else if textField == txtfldSecondaryPhone  {
            
            txtfldSecondaryPhone.hideError()

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldSecondaryPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldSecondaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        } else if textField == txtfldSecondaryPhoneExt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 10)
            
        } else if (textField == txtfldCompanyName) {
            
            txtfldCompanyName.hideError()

            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 150)

        } else if (textField == txtfldWebsite){
            
            txtfldWebsite.hideError()

            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)

        } else if (textField == txtfldEmail){
            
            txtfldEmail.hideError()

            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)

        } else if (textField == txtfldSecondaryEmail){
            
            txtfldSecondaryEmail.hideError()

            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)

        } else if (textField == txtfldNoOfEmployee){
            
            txtfldNoOfEmployee.hideError()

            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)

        } else if (textField == txtfldAnnualRevenue){
            
            txtfldAnnualRevenue.hideError()

            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk

        } else if(textField == txtfldAddress){
            if let char = string.cString(using: String.Encoding.utf8) {
                  let isBackSpace = strcmp(char, "\\b")
                  if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                    return true

                   //   print("Backspace was pressed")
                  }
              }
            var txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                
                self.imgPoweredBy.removeFromSuperview()

                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                        self.places = $0
                    }
            }
            

        }
        
        return true
        
    }
    
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtfldAddress){
            let txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
                
//            self.getPlaces(with: parameters) {
//                    self.places = $0
//                }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension AddCompanyVC_CRMContactNew_iPhone {

     func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
     func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
          doRequest(
                  "https://maps.googleapis.com/maps/api/place/autocomplete/json",
                  params: parameters,
                  completion: {
                      guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                      completion(predictions.map { Place(prediction: $0)
                    
                      })
                    if (self.places.count == 0){
                        self.tableView.removeFromSuperview()
                        self.imgPoweredBy.removeFromSuperview()

                    }else{
                        for item in self.scrollView.subviews {
                            if(item is UITableView){
                                item.removeFromSuperview()
                            }
                        }
                        self.imgPoweredBy.removeFromSuperview()
                        self.tableView.frame = CGRect(x: self.txtfldAddress.frame.origin.x, y: self.txtfldAddress.frame.maxY+5, width: self.scrollView.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : (DeviceType.IS_IPAD ? 380 : 250))
                        self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                        
                        self.scrollView.addSubview(self.tableView)
                        self.scrollView.addSubview(self.imgPoweredBy)
                    }
          }
        )
        
     
    }
   
     func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
   var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}






// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddCompanyVC_CRMContactNew_iPhone : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView
                 .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
                guard let value = $0 else { return }
                self.txtfldAddress.text = value.formattedAddress
                self.txtfldAddress.text = addressFormattedByGoogle(value: value)

                self.places = [];
        }
        self.tableView.removeFromSuperview()
        self.imgPoweredBy.removeFromSuperview()

        self.view.endEditing(true)

    }
  
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddCompanyVC_CRMContactNew_iPhone : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        //let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
        
        self.imgViewContactProfile.isHidden = false
        self.lblProfileImageName.isHidden = true
        
        let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        let imageResized = Global().resizeImageGloballl(img)
        
        self.imgViewContactProfile.image = imageResized
                        
        self.strGlobalImageName = "Contact" + "\(getUniqueValueForId())" + ".png"
        
    }
    
}

// MARK: -
// MARK: - -----------------------Source Selction Delgates

extension AddCompanyVC_CRMContactNew_iPhone : SelectGlobalVCDelegate{
    
    func getDataSelectGlobalVC(dictData: NSMutableDictionary, tag: Int) {
        
        if tag == 101 {
            
            if(dictData.count == 0){
                
                txtfldSourceName.text = ""
                strSourceId = ""
                
            }else{
                
                txtfldSourceName.text = "\(dictData.value(forKey: "Name")!)"
                strSourceId = "\(dictData.value(forKey: "SourceId")!)"
                
            }
            
        }
        
    }

}

// MARK: -
// MARK: - -----------------------Add Address Delgates

extension AddCompanyVC_CRMContactNew_iPhone : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {

        print(dictData)
        
        self.txtfldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])

    }

}
