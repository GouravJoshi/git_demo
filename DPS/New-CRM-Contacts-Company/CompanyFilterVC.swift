//
//  CompanyFilterVC.swift
//  DPS
//
//  Created by Saavan Patidar on 05/03/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class CompanyFilterVC: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var txtfldFromDate: ACFloatingTextField!
    @IBOutlet weak var txtfldToDate: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextField!
    @IBOutlet weak var txtfldCellPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryEmail: ACFloatingTextField!
    @IBOutlet weak var txtfldWebsite: ACFloatingTextField!
    @IBOutlet weak var txtfldPropertyType: ACFloatingTextField!

    @IBOutlet weak var btnSelf: UIButton!
    @IBOutlet weak var btnAll: UIButton!

    
    var aryselectedPropertyType = NSMutableArray()

    // MARK: Variables
    
    
    
    // MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        loadSavedSettings()
        txtfldCellPhone.keyboardType = .phonePad
        txtfldPrimaryPhone.keyboardType = .phonePad
        txtfldPrimaryEmail.keyboardType = .emailAddress

        // Do any additional setup after loading the view.
    }
    
    
    
    // MARK: Button Actions
    @IBAction func actionOnAll(_ sender: UIButton) {
        btnAll.setImage(UIImage(named: "redio_button_2.png"), for: .normal)
        btnSelf.setImage(UIImage(named: "redio_button_1.png"), for: .normal)
        btnSelf.tag = 0
    }
    @IBAction func actionOnSelf(_ sender: UIButton) {
        btnAll.setImage(UIImage(named: "redio_button_1.png"), for: .normal)
        btnSelf.setImage(UIImage(named: "redio_button_2.png"), for: .normal)
        btnSelf.tag = 99
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        backContactFilter()
    }
    
    @IBAction func actionOnClear(_ sender: UIButton) {
        
        let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in

            setDefaultValuesForCompanyFilterAssociation()
            setDefaultValuesForCompanyFilter()
        
            nsud.synchronize()
            
            UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
            UserDefaults.standard.set(true, forKey: "RefreshCompany_AssociateCompanyList")
            
            self.backContactFilter()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnApply(_ sender: UIButton) {
        
        if(self.txtfldFromDate.text!.count > 0 || self.txtfldToDate.text!.count > 0){
            
            // set default date for filter
            
            if (self.txtfldFromDate.text!.count > 0 && self.txtfldToDate.text!.count == 0) {
                
                self.txtfldToDate.text = Global().strCurrentDateFormatted("MM/dd/yyyy", "EST")
                
            }
            
            if (self.txtfldFromDate.text!.count == 0 && self.txtfldToDate.text!.count > 0) {
                
                self.txtfldFromDate.text = Global().strCurrentDateFormatted("MM/dd/yyyy", "EST")
                
            }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let fromDate = dateFormatter.date(from: self.txtfldFromDate.text!)
        let toDate = dateFormatter.date(from: self.txtfldToDate.text!)
        
        if(fromDate! > toDate!){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
            return
            
        }else{
            
            applyFilterContact()

            }
            
        }else{
            
            applyFilterContact()

        }
        
    }
    @IBAction func actionOnFromDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // Setting Default Dates
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : result))!
        self.gotoDatePickerView(strTag: 101, strType: "Date",dateToSet: fromDate)
        
    }
    
    @IBAction func actionOnToDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : result))!
        self.gotoDatePickerView(strTag: 102, strType: "Date",dateToSet: fromDate)
        
    }
    @IBAction func action_SelectPropertyType(_ sender: Any) {
        if(self.getAddressTypeFromMasterSalesAutomation().count != 0){
            openTableViewPopUp(tag: 66, ary: self.getAddressTypeFromMasterSalesAutomation() , aryselectedItem: self.aryselectedPropertyType)

        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Property type not found!!!", viewcontrol: self)
            
        }
        
    }
    // MARK: Functions
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func backContactFilter() {
        
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func applyFilterContact() {
        if(validation()){
            var strType = "Self"
            
            var dictLoginData = NSDictionary()
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            
            let aryAddredID = NSMutableArray()
            for item in aryselectedPropertyType {
                aryAddredID.add("\((item as AnyObject).value(forKey: "AddressPropertyTypeId")!)")
            }
            
            var strCreatedBy = ""
            if(btnSelf.tag == 99){
                strCreatedBy = Global().getEmployeeId()
                strType = "Self"
            }else{
                strType = "All"

            }
            let strCellnumber = ("\(txtfldCellPhone.text ?? "")".replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: " ", with: "").trimmed
            let strPrimaryPhoneNumber = ("\(txtfldPrimaryPhone.text ?? "")".replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: " ", with: "").trimmed

            var keys = NSArray()
            var values = NSArray()
            keys = ["CompanyKey",
                    "FromDate",
                    "ToDate",
                    "Name",
                    "PrimaryEmail",
                    "PrimaryPhone",
                    "CellPhone1",
                    "Website",
                    "AddressPropertyTypeId", "CreatedBy","filtertype"]
            
            values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                      (txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : "",
                      (txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : "",
                      (txtfldCompanyName.text?.count)! > 0 ? txtfldCompanyName.text! : "",
                      (txtfldPrimaryEmail.text?.count)! > 0 ? txtfldPrimaryEmail.text! : "",
                      strPrimaryPhoneNumber,
                      strCellnumber,
                      (txtfldWebsite.text?.count)! > 0 ? txtfldWebsite.text! : "",aryAddredID, strCreatedBy , strType]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            if(btnSelf.currentImage == UIImage(named: "")){
                
            }
            nsud.setValue(dictToSend, forKey: "CrmCompanyFilter")
            nsud.setValue(dictToSend, forKey: "CrmCompanyFilterAssociation")

            nsud.synchronize()
            
            UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
            UserDefaults.standard.set(true, forKey: "RefreshCompany_AssociateCompanyList")

            backContactFilter()
        }
        
       
        
   
        
    }
    func validation() -> Bool {
        if(txtfldCellPhone.text?.count != 0 &&  txtfldCellPhone.text!.count < 12){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid cell phone!", viewcontrol: self)
            return false

        }
        else if(txtfldPrimaryPhone.text?.count != 0 &&  txtfldPrimaryPhone.text!.count < 12){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid primary phone!", viewcontrol: self)
            return false

        }
        else if(txtfldPrimaryEmail.text?.count != 0 &&  !(Global().checkIfCommaSepartedEmailsAreValid(txtfldPrimaryEmail.text!)) ){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid primary email!", viewcontrol: self)
            return false

        }
    return true
    }
    func loadSavedSettings() {
        if(nsud.value(forKey: "CrmCompanyFilter")is NSDictionary){
            let dictToSend = nsud.value(forKey: "CrmCompanyFilter") as! NSDictionary
            txtfldFromDate.text = dictToSend.value(forKey: "FromDate") as? String
            txtfldToDate.text = dictToSend.value(forKey: "ToDate") as? String
            txtfldCompanyName.text = dictToSend.value(forKey: "Name") as? String
            txtfldWebsite.text = dictToSend.value(forKey: "Website") as? String
            txtfldPrimaryEmail.text = dictToSend.value(forKey: "PrimaryEmail") as? String
            txtfldPrimaryPhone.text = dictToSend.value(forKey: "PrimaryPhone") as? String
            txtfldCellPhone.text = dictToSend.value(forKey: "CellPhone1") as? String
            
            txtfldPrimaryPhone.text = formattedNumber(number: txtfldPrimaryPhone.text ?? "")
            txtfldCellPhone.text = formattedNumber(number: txtfldCellPhone.text ?? "")
            
            self.aryselectedPropertyType = NSMutableArray()
            let propertyTypeID =  dictToSend.value(forKey: "AddressPropertyTypeId")as! NSArray
            var name = ""
            for item in propertyTypeID {
                let aryTemp = getPropertyTypeNameFromID_FromMasterSalesAutomation(id: "\(item)")
                if(aryTemp.count != 0){
                    let dict = aryTemp.object(at: 0)as! NSDictionary
                    name = "\(name)\(dict.value(forKey: "Name")!),"
                    self.aryselectedPropertyType.add(dict)
                }
            }
            if name.count != 0 {
                name = String(name.dropLast())
            }
            txtfldPropertyType.text = name
            
            btnAll.setImage(UIImage(named: "redio_button_1.png"), for: .normal)
            btnSelf.setImage(UIImage(named: "redio_button_2.png"), for: .normal)
            btnSelf.tag = 99
            if dictToSend.value(forKey: "CreatedBy") != nil {
                let createdbyID = "\(dictToSend.value(forKey: "CreatedBy") ?? "")"
                if(createdbyID == ""){
                    btnAll.setImage(UIImage(named: "redio_button_2.png"), for: .normal)
                    btnSelf.setImage(UIImage(named: "redio_button_1.png"), for: .normal)
                    btnSelf.tag = 0
                }
            }
           
        }
 
    }
    
    func getPropertyTypeNameFromID_FromMasterSalesAutomation(id : String) -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")) && "\((task as! NSDictionary).value(forKey: "AddressPropertyTypeId")!)".contains("\(id)")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    func gotoDatePickerView(strTag: Int, strType:String, dateToSet:Date )  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = strTag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
    func getAddressTypeFromMasterSalesAutomation() -> NSMutableArray {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if(dictMaster.value(forKey: "AddressPropertyTypeMaster") != nil){
                let aryTemp = (dictMaster.value(forKey: "AddressPropertyTypeMaster") as! NSArray).filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("1")  || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("true") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".contains("True")}
                return (aryTemp as NSArray).mutableCopy()as! NSMutableArray
            }
            
            return NSMutableArray()
        }
        
        return NSMutableArray()

    }
    
}

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

extension CompanyFilterVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if tag == 101 {
            
            txtfldFromDate.text = strDate
            
        } else if tag == 102 {
            
            txtfldToDate.text = strDate
            
        }
        
    }
    
}
// MARK: --------------------------- Extensions ---------------------------


extension CompanyFilterVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        // For PropertyType
         if(tag == 66)
        {
            
            if(dictData.count == 0){
                txtfldPropertyType.text = ""
                aryselectedPropertyType = NSMutableArray()

            }else{
                aryselectedPropertyType = NSMutableArray()
                aryselectedPropertyType = dictData.value(forKey: "multi") as! NSMutableArray
                
                var strName = ""
                for item in aryselectedPropertyType {
                    let dict = (item as AnyObject) as! NSDictionary
                    strName = "\(strName)" + "\(dict.value(forKey: "Name")!),"
                }
                
                if strName.count != 0 {
                    strName = String(strName.dropLast())
                }
                txtfldPropertyType.text = strName
            }
            // Source
        }
        
    }
}
//MARK:------UITextFieldDelegate-----------
//MARK:
extension CompanyFilterVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == txtfldCellPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = txtfldCellPhone.text!
                strTextt = String(strTextt.dropLast())
                txtfldCellPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        else if textField == txtfldPrimaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = txtfldPrimaryPhone.text!
                strTextt = String(strTextt.dropLast())
                txtfldPrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
       
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 100)
    }
    
}
// MARK: - Selection CustomTableView
// MARK: -
