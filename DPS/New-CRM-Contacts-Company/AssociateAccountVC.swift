//
//  AssociateAccountVC.swift
//  DPS
//
//  Created by Saavan Patidar on 17/03/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class CellAssociateAccount:UITableViewCell{
    
    @IBOutlet weak var lblAccountNo: UILabel!
    @IBOutlet weak var lblAccountNamee: UILabel!

}

class AssociateAccountVC: UIViewController {

      weak var delegateTaskAssociate: AddTaskAssociateProtocol?
      weak var delegateActivityAssociate: AddActivityAssociateProtocol?
      weak var delegateLeadAssociate: AddLeadAssociateProtocol?

      var selectionTag = Int()
      var strSelectionTag = ""

    
   // MARK: --------------------------- Outlets  ---------------------------
    
    @IBOutlet weak var searchBarAccount: UISearchBar!
    @IBOutlet weak var tblviewAccount: UITableView!
    
    // MARK: --------------------------- Varibale  ---------------------------
    
    var arrayAccounts = NSMutableArray()
    var arrayAccountData = NSMutableArray()
    var refresher = UIRefreshControl()
    var dictLoginData = NSDictionary()
    var strFromWhere = ""
    var strRefType = String()
    var strRefId = String()
    var lbl = UILabel()
    var arrOfSection = NSMutableArray()
    var arrOfFilteredDataTemp = [NSDictionary]()
    var arrOfToday = [NSDictionary]()
    var arrOfYesterday = [NSDictionary]()
    var arrOfThisMonth = [NSDictionary]()
    var arrOfAllOther = [NSDictionary]()
    var strFromWhereForAssociation = ""
    var loader = UIAlertController()

    // MARK: --------------------------- View's life cycle  ---------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            searchBarAccount.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
        } else {
            // Fallback on earlier versions
        }
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        tblviewAccount.tableFooterView = UIView()
        tblviewAccount.estimatedRowHeight = 50.0
        tblviewAccount.rowHeight = UITableView.automaticDimension;
        
        arrOfSection.add("  Created Today")
        arrOfSection.add("  Created Yesterday")
        arrOfSection.add("  Created This Month")
        arrOfSection.add("  Other")
        
        setupViews()
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tblviewAccount!.addSubview(refresher)
        
        self.view.endEditing(true)
        
        UserDefaults.standard.set(false, forKey: "RefreshAccounts_AccountList")
        
        UserDefaults.standard.set(true, forKey: "Initial_RefreshAccounts_AccountList")

        UserDefaults.standard.set(false, forKey: "Dismiss_AccountList")

        setDefaultValuesForAccountFilter()

        gotoAccountFilterVC()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // change in logic to call API
        if nsud.bool(forKey: "Dismiss_AccountList") {
            
            UserDefaults.standard.set(false, forKey: "Dismiss_AccountList")
            self.navigationController?.popViewController(animated: false)
            
        }else if nsud.bool(forKey: "RefreshAccounts_AccountList") {
            
            if (isInternetAvailable()){
                
                loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAccounts_AccountList")

                    self.callApiToGetAccounts()

                })

            }else{
                
                self.noDataLbl()
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
    // MARK: --------------------------- Local Funcitons ---------------------------
    
    func setupViews()
    {
        
        /*viewTopButtom.backgroundColor = UIColor.white
        btnCompany.layer.cornerRadius = btnCompany.frame.size.height/2
        btnCompany.layer.borderWidth = 0
        
        viewTopButtom.layer.cornerRadius = viewTopButtom.frame.size.height/2
        viewTopButtom.layer.borderWidth = 0
                
        btnContact.layer.cornerRadius = btnContact.frame.size.height/2
        btnContact.layer.borderWidth = 0
        
        viewTopButtom.bringSubviewToFront(btnCompany)*/
        
        if let txfSearchField = searchBarAccount.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
        }
        searchBarAccount.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarAccount.layer.borderWidth = 1
        searchBarAccount.layer.opacity = 1.0
        
    }
    
    func goToCompanyDetails(){
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }

    func goToTaskActivity()
    {
          let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    func goToWebLead()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func goToDashboard(){
       
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToAppointment(){
       
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    
    func goToNearBy(){
       /*
         if(DeviceType.IS_IPAD){
                            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                            self.navigationController?.pushViewController(vc!, animated: false)
                        }else{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                            self.navigationController?.pushViewController(vc!, animated: false)
                        }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

        
    }
    
    func goToSignedAgreement(){
       
  if(DeviceType.IS_IPAD){
                           let mainStoryboard = UIStoryboard(
                               name: "CRMiPad",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }else{
                           let mainStoryboard = UIStoryboard(
                               name: "CRM",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }
        
    }
    func noDataLbl() {
        
        self.tblviewAccount.isHidden = true
        
        lbl.removeFromSuperview()
        
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        lbl.center = self.view.center
        lbl.text = "No Data Found"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 20)
     //   self.view.addSubview(lbl)
        
    }
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.callApiToGetAccounts()

            })

        }else{
            
            self.noDataLbl()
            
        }
        
    }
    
    func gotoAccountFilterVC()
       {
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountFilterVC") as! AccountFilterVC
       
            self.navigationController?.present(controller, animated: false, completion: nil)
       }
    
    func logicForFilterAccounts(arrToFilter : NSArray) {
        
        // For Today Date Filter
        
        let datee = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == datee)
            
            } as! [NSDictionary]
        
        var sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["CreatedDate"] as? String)! > (($1 )["CreatedDate"] as? String)!
        })
        
        arrOfToday = sortedArray
        
        // For YesterDay Date Filter
        
        let date1 = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == date1)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["CreatedDate"] as? String)! > (($1 )["CreatedDate"] as? String)!
        })
        
        arrOfYesterday = sortedArray
        
        // For Current Month Date Filter
        
        let dictCurrentMonthDate = Global().getCureentMonthDate()
        
        let dateStart = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "StartDate") as? String, "MM/dd/yyyy", "UTC") as Date
        
        let dateEnd = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "EndDate") as? String, "MM/dd/yyyy", "UTC") as Date

        let yesterDay = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date

        let todayDate = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  ((Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! >= dateStart) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! <= dateEnd) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != todayDate) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != yesterDay))
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["CreatedDate"] as? String)! > (($1 )["CreatedDate"] as? String)!
        })
        
        arrOfThisMonth = sortedArray
        
        // For AllOther Date Filter

        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! < dateStart)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["CreatedDate"] as? String)! > (($1 )["CreatedDate"] as? String)!
        })
        
        arrOfAllOther = sortedArray
        
        self.tblviewAccount.reloadData()
        
    }
    
    // MARK: --------------------------- Web Service ---------------------------

    func associateAccount(CrmAccountId : String)
    {
        
        if !isInternetAvailable()
        {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)

        }
        else
        {

            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAssociateContact + strRefId + "&RefType=" + strRefType + "&RefId=" + CrmAccountId + "&EmployeeId=" + "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            
            strURL = strURL.trimmingCharacters(in: .whitespaces)
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "AssociateContactCompany") { (Response, Status) in
                
                self.loader.dismiss(animated: false) {

                    if(Status)
                    {
                        
                        if Response["data"] is String {
                            
                            guard let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)  else {
                                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                            //let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)
                            if dictResponse.count > 0{
                                
                                let strOperationResult = dictResponse["OperationResult"] as? Bool ?? false
                                var strMessage = dictResponse["Message"] as? String ?? ""
                                
                            if strOperationResult {
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Associated Successfully", preferredStyle: .alert)
                                                  
                                                  let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                                      
                                                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAccount_Notification"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])
                                                      
                                                      self.navigationController?.popViewController(animated: false)
                                                  })
                                                  
                                                  alertCOntroller.addAction(alertAction)
                                                  self.present(alertCOntroller, animated: true, completion: nil)
                                
                            } else {
                                
                                if strMessage.count > 0 {
                                    
                                    
                                    
                                } else {
                                    
                                    strMessage = alertSomeError
                                    
                                }
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)

                                
                            }
                            }

                            else {
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                            }
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                        }

                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                }
                
            }
            
        }
        
    }
    func callApiToGetAccounts(){
        
        let startGlobal = CFAbsoluteTimeGetCurrent()

        lbl.removeFromSuperview()
        
        //setDefaultValuesForAccountFilterAssociation()

        let dictToSend = nsud.value(forKey: "CrmAccountFilter")
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend!) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmAccount

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmContactsNew"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            self.loader.dismiss(animated: false) {

                self.refresher.endRefreshing()

                if(success)
                {
                    //Contacts
                    
                    let dictResponse = (response as NSDictionary?)!
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("Accounts") {
                            
                            self.arrayAccounts = dictResponse.value(forKey: "Accounts") as! NSMutableArray
                            
                            if self.arrayAccounts.count > 0 {
                                
                                self.tblviewAccount.isHidden = false
                                //self.tblviewContact.reloadData()
                                self.logicForFilterAccounts(arrToFilter: self.arrayAccounts)
                                
                                let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                                print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Accounts :-- \(self.arrayAccounts.count)")
                                
                            } else {
                                
                                self.noDataLbl()
                                
                            }
                            
                            
                        } else {
                            
                            self.noDataLbl()
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }

                        
                    } else {
                        
                        self.noDataLbl()
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                
                    
                }else {
                    
                    self.noDataLbl()
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
            }

        }
    }
       
    // MARK: --------------------------- UIButton action ---------------------------

    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnContact(_ sender: UIButton)
    {
        self.view.endEditing(true)
        var chk = Bool()
        chk = false
        
        for controller in self.navigationController!.viewControllers as NSArray
        {
            if controller is ContactVC_CRMContactNew_iPhone
            {
                chk = true
                self.navigationController!.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        if !chk
        {
         let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone

                       self.navigationController?.pushViewController(testController, animated: false)
        }

    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoAccountFilterVC()
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.3) {
            //self.constHghtSearchBar.constant = 56.0
        }
    }
    
    @IBAction func actionOnAddCompany(_ sender: UIButton)
    {
        self.view.endEditing(true)
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton)
    {
        self.view.endEditing(true)
        goToWebLead()
        
    }
    
    @IBAction func actionOnNearBy(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        self.view.endEditing(true)
        goToTaskActivity()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
           alert.view.tintColor = UIColor.black
      let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
                         Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                         alert.addAction(Near)
        
         let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedAgreement()
            
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
                                     Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                                     alert.addAction(Signed)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
                            popoverController.sourceView = sender as UIView
                            popoverController.sourceRect = sender.bounds
                            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnHome(_ sender: Any)
    {
        self.view.endEditing(true)
        goToDashboard()
        
    }

    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        self.view.endEditing(true)
        goToAppointment()

    }
}


// MARK: --------------------------- Extensions ---------------------------


extension AssociateAccountVC:UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    // MARK: UItableView's delegate and datasource
       
       func numberOfSections(in tableView: UITableView) -> Int {
           
           return arrOfSection.count
           
       }
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return DeviceType.IS_IPAD ? UITableView.automaticDimension : 76
       }
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           
           if (section == 0) {
               
               if (arrOfToday.count == 0) {
                   
                   return 0
                   
               } else {
                   
                    return  DeviceType.IS_IPAD ? 50 : 40
                   
               }
               
           } else if (section == 1) {
               
               if (arrOfYesterday.count == 0) {
                   
                   return 0
                   
               } else {
                   
                   return  DeviceType.IS_IPAD ? 50 : 40

               }
               
           } else if (section == 2) {
               
               if (arrOfThisMonth.count == 0) {
                   
                   return 0
                   
               } else {
                   
                   return  DeviceType.IS_IPAD ? 50 : 40

               }
               
           } else if (section == 3) {
               
               if (arrOfAllOther.count == 0) {
                   
                   return 0
                   
               } else {
                   
                   return  DeviceType.IS_IPAD ? 50 : 40

               }
               
           } else {
               
               return  DeviceType.IS_IPAD ? 50 : 40

           }
           
       }
       
       
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: DeviceType.IS_IPAD ? 50 : 40))
           //let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
           vw.backgroundColor = UIColor.lightTextColorTimeSheet()
           
           let lblHeader = UILabel()
           lblHeader.frame = vw.frame
           lblHeader.text = arrOfSection[section] as? String
          
           lblHeader.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 21 : 17)
           lblHeader.textColor = UIColor.theme()
           vw.addSubview(lblHeader)
           
           return vw
       }
       
       // set view for footer
       func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
           let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: DeviceType.IS_IPAD ? 50 : 40))
           footerView.backgroundColor = UIColor.clear
           return footerView
       }
       
       // set height for footer
       func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
                  
           if (section == 0) {
               
               if (arrOfToday.count == 0) {
                   
                   return 0
                   
               } else {
                   
                   return  DeviceType.IS_IPAD ? 25 : 20
                   
               }
               
           } else if (section == 1) {
               
               if (arrOfYesterday.count == 0) {
                   
                   return 0
                   
               } else {
                   
                 return  DeviceType.IS_IPAD ? 25 : 20
                   
               }
               
           } else if (section == 2) {
               
               if (arrOfThisMonth.count == 0) {
                   
                   return 0
                   
               } else {
                   
                    return  DeviceType.IS_IPAD ? 25 : 20
                   
               }
               
           } else if (section == 3) {
               
               if (arrOfAllOther.count == 0) {
                   
                   return 0
                   
               } else {
                   
                    return  DeviceType.IS_IPAD ? 25 : 20
                   
               }
               
           } else {
               
                 return  DeviceType.IS_IPAD ? 25 : 20
               
           }
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
           if section == 0 {
               
               return arrOfToday.count
               
           } else if section == 1 {
               
               return arrOfYesterday.count
               
           } else if section == 2 {
               
               return arrOfThisMonth.count
               
           } else  {
               
               return arrOfAllOther.count
               
           }
           
       }
           
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAssociateAccount") as! CellAssociateAccount
       
        var dictData = NSDictionary()

        if indexPath.section == 0 {
            
            dictData = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictData = arrOfYesterday[indexPath.row]

        } else if indexPath.section == 2 {
            
            dictData = arrOfThisMonth[indexPath.row]

        } else  {
            
            dictData = arrOfAllOther[indexPath.row]

        }
        
        let strName = "\(dictData.value(forKey: "AccountName") ?? "")"
        
        if strName.count == 0 {
            
            let strCrmContactName = "\(dictData.value(forKey: "CrmContactName") ?? "")"
            
            if strCrmContactName.count == 0 {

                let strCrmCompanyName = "\(dictData.value(forKey: "CrmCompanyName") ?? "")"
                
                if strCrmCompanyName.count == 0 {

                    cell.lblAccountNamee.text = " "

                }else{
                    
                    cell.lblAccountNamee.text = "\(strCrmCompanyName)"
                    
                }

            }else{
                
                cell.lblAccountNamee.text = "\(strCrmContactName)"

            }

        }else{
            
            cell.lblAccountNamee.text = "\(strName)"

        }
        
        cell.lblAccountNo.text = "\(dictData.value(forKey: "AccountNo") ?? "")"

        
        return cell
           
       }
       
       
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: false)
        
        var dictData = NSDictionary()

        if indexPath.section == 0 {
            
            dictData = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictData = arrOfYesterday[indexPath.row]

        } else if indexPath.section == 2 {
            
            dictData = arrOfThisMonth[indexPath.row]

        } else  {
            
            dictData = arrOfAllOther[indexPath.row]

        }

        if strFromWhere == "DirectAssociate" {
            
            self.associateAccount(CrmAccountId: "\(dictData.value(forKey: "AccountId") ?? "")")
            
        } else {
            
            if strFromWhereForAssociation == "AddLeadCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedLeadProspectCRM_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])

                
            } else if strFromWhereForAssociation == "ConvertOpportunityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedConvertOpportunityCRM_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "AddActivityCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddActivityCRM_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            } else if strFromWhereForAssociation == "AddTaskCRM" {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AssociatedAddTaskCRM_Notification"), object: nil, userInfo: dictData as? [AnyHashable : Any])
                
            }
            //-------Association Task Activity------
            else if(strFromWhereForAssociation == "AddActivityCRM_Associations"){
                self.delegateActivityAssociate?.getDataAddActivityAssociateProtocol(notification: dictData, tag: selectionTag)
            }
            else if(strFromWhereForAssociation == "AddTaskCRM_Associations"){
                self.delegateTaskAssociate?.getDataAddTaskAssociateProtocol(notification: dictData, tag: selectionTag)
            }
            //------------------------//
            else if(strFromWhereForAssociation == "AddLeadCRM_Associations"){
                self.delegateLeadAssociate?.getDataAddLeadAssociateProtocol(notification: dictData, tag: strSelectionTag)
            }
            
            self.navigationController?.popViewController(animated: false)
            
        }
           
       }
    
    // MARK: UISearchBar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count == 0)
        {
            
            logicForFilterAccounts(arrToFilter: arrayAccounts)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                       
                       self.view.endEditing(true)
                       
                   })
            
        }else {
            
            let aryTemp = arrayAccounts.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "AccountName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "AccountNo")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "CrmCompanyName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "CrmContactName")!)".lowercased().contains(searchBar.text!.lowercased())

            } as NSArray
            
            logicForFilterAccounts(arrToFilter: aryTemp)
            
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        
        if(searchBar.text?.count == 0)
        {
            
            logicForFilterAccounts(arrToFilter: arrayAccounts)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                       
                       self.view.endEditing(true)
                       
                   })
            
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
            
        }
        else
        {
                   
            let aryTemp = arrayAccounts.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "AccountName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "AccountNo")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "CrmCompanyName")!)".lowercased().contains(searchBar.text!.lowercased()) || "\((task as! NSDictionary).value(forKey: "CrmContactName")!)".lowercased().contains(searchBar.text!.lowercased())
            } as NSArray
            
            logicForFilterAccounts(arrToFilter: aryTemp)
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        searchBarAccount.text = ""
                    
        logicForFilterAccounts(arrToFilter: arrayAccounts)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                   
                   self.view.endEditing(true)
                   
               })
        
    }
    
}

extension AssociateAccountVC : UITextFieldDelegate
{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text field did end calls")
    }
    
}
