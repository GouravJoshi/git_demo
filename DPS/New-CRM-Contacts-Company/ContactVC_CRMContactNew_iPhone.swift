//
//  ContactVC_CRMContactNew_iPhone.swift
//  DPS
//  peSTream 2020
//  Created by Rakesh Jain on 30/01/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Contacts

class CellContact:UITableViewCell{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblNameBold: UILabel!
    @IBOutlet weak var btnEdit: UIButton!

}


class ContactVC_CRMContactNew_iPhone: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    
    // outlets
    @IBOutlet weak var searchBarContact: UISearchBar!
    @IBOutlet weak var tblviewContact: UITableView!
    @IBOutlet weak var constHghtSearchBar: NSLayoutConstraint!
    @IBOutlet weak var viewTopButton: UIView!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var headerView: TopView!

    // variables
    var dictLoginData = NSDictionary()
    var arrOfContacts = NSArray()
    var refresher = UIRefreshControl()
    var arrOfSection = NSMutableArray()
    var arrOfFilteredDataTemp = [NSDictionary]()
    var arrOfToday = [NSDictionary]()
    var arrOfYesterday = [NSDictionary]()
    var arrOfThisMonth = [NSDictionary]()
    var arrOfAllOther = [NSDictionary]()
    var lbl = UILabel()
    var loader = UIAlertController()
    @IBOutlet weak var lbltaskCount: UILabel!
       @IBOutlet weak var lblScheduleCount: UILabel!
    var strSearchText = ""

    //view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                         lblScheduleCount.text = "0"
                            lbltaskCount.layer.cornerRadius = 18.0
                                  lbltaskCount.backgroundColor = UIColor.red
                                  lblScheduleCount.layer.cornerRadius = 18.0
                                  lblScheduleCount.backgroundColor = UIColor.red
                                  lbltaskCount.layer.masksToBounds = true
                                  lblScheduleCount.layer.masksToBounds = true

                        }
            if #available(iOS 13.0, *) {
                             searchBarContact.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
                         } else {
                             // Fallback on earlier versions
                         }
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            tblviewContact.tableFooterView = UIView()
            tblviewContact.estimatedRowHeight = 50.0
            
            arrOfSection.add("  Created Today")
            arrOfSection.add("  Created Yesterday")
            arrOfSection.add("  Created This Month")
            arrOfSection.add("  All")
            
            //constHghtSearchBar.constant = 0.0
            
            btnCompany.layer.cornerRadius = btnCompany.frame.size.height/2
            btnCompany.layer.borderWidth = 0
            
            viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
            viewTopButton.layer.borderWidth = 0
                    
            btnContact.layer.cornerRadius = btnContact.frame.size.height/2
            btnContact.layer.borderWidth = 0
            
            self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
            self.tblviewContact!.addSubview(refresher)
            
            setUpView()

            UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")
            
            //createBadgeView()
        }
        
  

    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            if nsud.bool(forKey: "RefreshContacts_ContactList") {
                searchBarContact.text = ""
                if (isInternetAvailable()){
                    
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        UserDefaults.standard.set(false, forKey: "RefreshContacts_ContactList")
                        self.callApiToGetContacts()
                        
                    })
                    
                }else{
                    
                    //self.noDataLbl()
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                    
                }
            }
        }
       
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //super.viewWillDisappear(animated)
        //searchBarContact.text = ""
    }
    
    
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Account", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        footorView.onClickHomeButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToDashboard()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToLeadOpportunity()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            self.view.endEditing(true)
          //  self.GotoAccountViewController()
        }
        
       
    }
    
    func setTopMenuOption() {
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Contacts", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
        headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        self.headerView.addSubview(headerViewtop)
        headerViewtop.onClickFilterButton = {() -> Void in
            self.gotoContactFilterVC()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.view.endEditing(true)
            self.goToAddContact()
        }
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.arrOfSection.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
        
       
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "Map":
            self.goToNearBy()
                break
            case "Lead":
                self.goToLeadOpportunity()
                break
            case "Opportunity":
                self.GotoOpportunityViewContoller()
                break
            case "Tasks":
                self.GotoTaskViewContoller()
                break
            case "Activity":
                self.GotoActivityViewContoller()
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.goToSignedAgreement()
                break
            case "Company":
                self.gotoCompany()
                break
            case "Contacts":
               
                break
            default:
                break
            }
           
        }

    }
    
    //MARK:
    //MARK:------Jump Other viewController----
  
 
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoOpportunityViewContoller() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
  
       self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func GotoTaskViewContoller() {
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

      //testController.strFromVC = strFromVC
      self.navigationController?.pushViewController(testController, animated: false)
    }
    func GotoActivityViewContoller() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
     //   vc.strFromVC = strFromVC
        //nsud.setValue(nil, forKey: "dictFilterActivity")
        //nsud.synchronize()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
     // MARK: UItableView's delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrOfSection.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if (section == 0) {
                if (arrOfToday.count == 0) {
                    return 0
                } else {
                    return DeviceType.IS_IPAD ? 55 : 40
                }
            } else if (section == 1) {
                if (arrOfYesterday.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else if (section == 2) {
                
                if (arrOfThisMonth.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else if (section == 3) {
                
                if (arrOfAllOther.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else {
                
                return DeviceType.IS_IPAD ? 55 : 40
                
            }
            
      
 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: DeviceType.IS_IPAD ? 55 : 40))
        vw.backgroundColor = hexStringToUIColor(hex: "EFEFF4")
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = arrOfSection[section] as? String
        lblHeader.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        lblHeader.textColor = UIColor.theme()
        vw.addSubview(lblHeader)
        return vw
    }
    
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if section == 0 {
            
            return arrOfToday.count
            
        } else if section == 1 {
            
            return arrOfYesterday.count
            
        } else if section == 2 {
            
            return arrOfThisMonth.count
            
        } else  {
            
            return arrOfAllOther.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellContact") as! CellContact
        
        var dictOfContacts = NSDictionary()

        if indexPath.section == 0 {
            
            dictOfContacts = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictOfContacts = arrOfYesterday[indexPath.row]

        } else if indexPath.section == 2 {
            
            dictOfContacts = arrOfThisMonth[indexPath.row]

        } else  {
            
            dictOfContacts = arrOfAllOther[indexPath.row]

        }
        cell.lblNameBold.text = ""
        cell.lblName.text = ""
        if((Global().strFullName(dictOfContacts as AnyHashable as? [AnyHashable : Any])!).count > 0)
        {
            
            cell.lblNameBold.text = Global().strFullName((dictOfContacts as AnyHashable as! [AnyHashable : Any]))
            
            
            
        }
        if "\(dictOfContacts.value(forKey: "JobTitle") ?? "")".count > 0 {
            
            cell.lblName.text = "  \u{2022}  \(dictOfContacts.value(forKey: "JobTitle") ?? "")"
            
        }
        let rangeTime =  getTotalTimeAfterCreated(strdate: "\(dictOfContacts.value(forKey: "CreatedDate") ?? "")")
        
        var strFinal = ""
        
        if(rangeTime != ""){
            strFinal = rangeTime
        }
        if ("\(dictOfContacts.value(forKey: "CrmCompanyName") ?? "")" != ""){
             strFinal = "\(strFinal)  \u{2022}  \(dictOfContacts.value(forKey: "CrmCompanyName") ?? "")"
        }
        cell.lblCompanyName.text = strFinal

        cell.btnEdit.titleLabel?.tag = indexPath.section
        cell.btnEdit.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(
            self.EditAction(sender:)), for: .touchUpInside)
        return cell
        
    }
    @objc func EditAction(sender: UIButton) {
        var dictOfContacts = NSDictionary()
        
        if sender.titleLabel!.tag == 0 {
            
            dictOfContacts = self.arrOfToday[sender.tag]
            
        } else if sender.titleLabel!.tag == 1 {
            
            dictOfContacts = self.arrOfYesterday[sender.tag]
            
        } else if sender.titleLabel!.tag == 2 {
            
            dictOfContacts = self.arrOfThisMonth[sender.tag]
            
        } else  {
            
            dictOfContacts = self.arrOfAllOther[sender.tag]
            
        }
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
              let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
            controller.strFrom = "Update"
            controller.dictContact = dictOfContacts
            controller.dictContactBasicInfo = NSDictionary()
            self.navigationController?.pushViewController(controller, animated: false)
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
     
        var dictOfContacts = NSDictionary()

        if indexPath.section == 0 {
            
            dictOfContacts = arrOfToday[indexPath.row]
            
        } else if indexPath.section == 1 {
            
            dictOfContacts = arrOfYesterday[indexPath.row]

        } else if indexPath.section == 2 {
            
            dictOfContacts = arrOfThisMonth[indexPath.row]

        } else  {
            
            dictOfContacts = arrOfAllOther[indexPath.row]

        }
        
        goToContactDetails(dictContact: dictOfContacts)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            var dictOfContacts = NSDictionary()

            if indexPath.section == 0 {
                
                dictOfContacts = self.arrOfToday[indexPath.row]
                
            } else if indexPath.section == 1 {
                
                dictOfContacts = self.arrOfYesterday[indexPath.row]

            } else if indexPath.section == 2 {
                
                dictOfContacts = self.arrOfThisMonth[indexPath.row]

            } else  {
                
                dictOfContacts = self.arrOfAllOther[indexPath.row]

            }
                    
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                        
                  let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
                controller.strFrom = "Update"
                controller.dictContact = dictOfContacts
                controller.dictContactBasicInfo = NSDictionary()
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [edit]
        
    }
    
    // MARK: UIbutton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func actionOnCompanies(_ sender: UIButton) {
        self.view.endEditing(true)
        
            gotoCompany()
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        /*let testController = storyboardLeadProspect.instantiateViewController(withIdentifier: "EditOpportunityCRM_VC") as! EditOpportunityCRM_VC
        self.navigationController?.pushViewController(testController, animated: false)*/
        
        gotoContactFilterVC()
        
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            //self.constHghtSearchBar.constant = 56.0
        }
    }
    
    @IBAction func actionOnAddContact(_ sender: UIButton) {
       self.view.endEditing(true)
      
        goToAddContact()
        
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton) {
        self.view.endEditing(true)
        goToLeadOpportunity()
    }
    
    @IBAction func actionOnNearBy(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        self.view.endEditing(true)
        
          let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone

        //testController.strFromVC = strFromVC
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)

        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
           
       let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
           Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
               Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
               alert.addAction(Near)
      
               
        
       let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedAgreement()
            
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
                     Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
                     alert.addAction(Signed)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
      
                    if let popoverController = alert.popoverPresentationController {
                        popoverController.sourceView = sender as UIView
                        popoverController.sourceRect = sender.bounds
                        popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                    }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    @IBAction func actionOnDashBoard(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.goToDashboard()
    }
    
    @IBAction func actionOnAppointment(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.goToAppointment()
        
    }
    
    // MARK: Functions
 
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
                          lbltaskCount.isHidden = true
                          lblScheduleCount.isHidden = true
               }
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }

                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                  lblScheduleCount.text = strScheduleCount
                                                  lblScheduleCount.isHidden = false
                                              }else{
                        let badgeSchedule = SPBadge()
                        badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                        badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeSchedule.badge = strScheduleCount
                        btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    

                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                                                lbltaskCount.text = strTaskCount
                                                lbltaskCount.isHidden = false
                                                
                                            }else{
                        let badgeTasks = SPBadge()
                        badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                        badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                        badgeTasks.badge = strTaskCount
                        btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func setUpView()
    {
        
        viewTopButton.backgroundColor = UIColor.white
        
        btnContact.layer.cornerRadius = btnContact.frame.size.height/2
        btnContact.layer.borderWidth = 0
        
        viewTopButton.layer.cornerRadius = viewTopButton.frame.size.height/2
        viewTopButton.layer.borderWidth = 0
        
        btnCompany.layer.cornerRadius = btnCompany.frame.size.height/2
        btnCompany.layer.borderWidth = 0
        
        viewTopButton.bringSubviewToFront(btnContact)
        
        if let txfSearchField = searchBarContact.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .none//.roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = 15//txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
            txfSearchField.layer.borderWidth = 1
            txfSearchField.layer.borderColor = UIColor.white.cgColor
            
        }
        searchBarContact.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarContact.layer.borderWidth = 1
        searchBarContact.layer.opacity = 1.0
     
    }
    func logicForFilterContacts(arrToFilter : NSArray) {
        
        // For Today Date Filter
        
        let datee = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == datee)
            
            } as! [NSDictionary]
        
        var sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfToday = sortedArray
        
        // For YesterDay Date Filter
        
        let date1 = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! == date1)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfYesterday = sortedArray
        
        // For Current Month Date Filter
        
        let dictCurrentMonthDate = Global().getCureentMonthDate()
        
        let dateStart = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "StartDate") as? String, "MM/dd/yyyy", "UTC") as Date
        
        let dateEnd = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "EndDate") as? String, "MM/dd/yyyy", "UTC") as Date

        let yesterDay = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDate(Global().getCurrentDate()), "MM/dd/yyyy", "UTC") as Date

        let todayDate = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDate(), "MM/dd/yyyy", "UTC") as Date
        
        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  ((Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! >= dateStart) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! <= dateEnd) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != todayDate) && (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! != yesterDay))
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfThisMonth = sortedArray
        
        // For AllOther Date Filter

        arrOfFilteredDataTemp = arrToFilter.filter { (dict) -> Bool in
            
            return  (Global().convertStringDateToDate(forCrmNewFlow: (dict as! NSDictionary).value(forKey: "CreatedDate") as? String, "MM/dd/yyyy" , "UTC")! < dateStart)
            
            } as! [NSDictionary]
        
        sortedArray = arrOfFilteredDataTemp.sorted(by: {
            (($0 )["FirstName"] as? String)! < (($1 )["FirstName"] as? String)!
        })
        
        arrOfAllOther = sortedArray
        
        self.tblviewContact.reloadData()
        
    }
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.callApiToGetContacts()

            })

        }else{
            
            //self.noDataLbl()
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
            
        }
        
    }
    
    func noDataLbl() {
        
//        self.tblviewContact.isHidden = true
//
//        lbl.removeFromSuperview()
//
//        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
//        lbl.center = self.view.center
//        lbl.text = "No Data Found"
//        lbl.textAlignment = .center
//        lbl.textColor = UIColor.lightGray
//        lbl.font = UIFont.systemFont(ofSize: 20)
//        self.view.addSubview(lbl)
        
    }
    
    func callApiToGetContacts(){
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        let start1 = CFAbsoluteTimeGetCurrent()

        //lbl.removeFromSuperview()
        
        if nsud.value(forKey: "CrmContactFilter") == nil {
            
            setDefaultValuesForContactFilter()
            
        }
        
        let dictToSend = nsud.value(forKey: "CrmContactFilter")
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(dictToSend!) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend!, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmContacts

        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "SearchCrmContactsNew"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            let diff1 = CFAbsoluteTimeGetCurrent() - start1
            print("Took \(diff1) seconds to get Data From SQL Server.")
            
            self.loader.dismiss(animated: false) {

                self.refresher.endRefreshing()

                if(success)
                {
                    //Contacts
                    
                    let dictResponse = (response as NSDictionary?)!
                    
                    var jsonString = String()
                    
                    if(JSONSerialization.isValidJSONObject(dictResponse) == true)
                    {
                        let jsonData = try! JSONSerialization.data(withJSONObject: dictResponse, options: JSONSerialization.WritingOptions.prettyPrinted)
                        
                        jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                        
                        print(jsonString)
                        
                    }
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("Contacts") {
                            
                            self.arrOfContacts = dictResponse.value(forKey: "Contacts") as! NSArray
                            
                            if self.arrOfContacts.count > 0 {
                                
                                self.tblviewContact.isHidden = false
                                //self.tblviewContact.reloadData()
                                self.logicForFilterContacts(arrToFilter: self.arrOfContacts)
                                
                                let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                                print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server. Count of Contacts :-- \(self.arrOfContacts.count)")
                                
                            } else {
                                
                                //self.noDataLbl()
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No data found", viewcontrol: self)
                                
                            }
                            
                            
                        } else {
                            
                            //self.noDataLbl()
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }

                        
                    } else {
                        
                        //self.noDataLbl()
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }
                
                    
                }else {
                    
                    //self.noDataLbl()
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    
                }
            }
        }
    }
    
     func gotoCompany()
     {
       let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyVC_CRMContactNew_iPhone") as! CompanyVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
    }
     func gotoContactFilterVC()
    {
        let mainStoryboard = UIStoryboard(
                  name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "ContactFilterVC") as! ContactFilterVC
         self.navigationController?.present(controller, animated: false, completion: nil)
        
        /*let controller = storyboardTask.instantiateViewController(withIdentifier: "CommentList_iPhoneVC") as! CommentList_iPhoneVC
        controller.iD_PreviousView = "630891"
        self.navigationController?.present(controller, animated: false, completion: nil)*/
        
    }
    func goToAddContact(){
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddContactVC_CRMContactNew_iPhone") as! AddContactVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToContactDetails(dictContact : NSDictionary){
    
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        controller.strCRMContactIdGlobal = "\(dictContact.value(forKey: "CrmContactId")!)"
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToDashboard(){
         let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func goToAppointment(){
       
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    
    func goToNearBy(){
        /*
        if(DeviceType.IS_IPAD){
                           let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                           let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
                           self.navigationController?.pushViewController(vc!, animated: false)
                       }else{
                           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                           let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
                           self.navigationController?.pushViewController(vc!, animated: false)
                       }*/
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)

        
    }
    
    func goToSignedAgreement(){
       
        if(DeviceType.IS_IPAD){
                           let mainStoryboard = UIStoryboard(
                               name: "CRMiPad",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }else{
                           let mainStoryboard = UIStoryboard(
                               name: "CRM",
                               bundle: nil)
                           let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
                           self.navigationController?.present(objByProductVC!, animated: true)
                       }
        
    }
    
    func goToLeadOpportunity(){
       
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
          let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)

    }
    
    // MARK: --------UISearchBar delegate
    // MARK: -------------------------
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchAutocomplete(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchAutocomplete(searchText: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchAutocomplete(searchText: "")
        searchBar.resignFirstResponder()
    }
    
    func searchAutocomplete(searchText: String) -> Void{
        if(searchText.count == 0)
        {
            self.view.endEditing(true)

            logicForFilterContacts(arrToFilter: arrOfContacts)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.endEditing(true)
            }
        }else {
            
            let aryTemp = arrOfContacts.filter { (task) -> Bool in
                
                return "\((task as! NSDictionary).value(forKey: "FirstName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "MiddleName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "LastName")!)".lowercased().contains(searchText.lowercased()) || "\((task as! NSDictionary).value(forKey: "CrmCompanyName")!)".lowercased().contains(searchText.lowercased())
                
            } as NSArray
            
            logicForFilterContacts(arrToFilter: aryTemp)
            
        }
    }
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension ContactVC_CRMContactNew_iPhone : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           self.setTopMenuOption()
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}
