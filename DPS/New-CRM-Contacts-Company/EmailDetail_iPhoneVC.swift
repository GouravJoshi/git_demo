//
//  EmailDetailVC.swift
//  DPS
//
//  Created by Navin Patidar on 3/20/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class EmailDetail_iPhoneVC: UIViewController {
 
    // MARK:
     // MARK: ----------IBOutlet
     @IBOutlet weak var lbl_From: UILabel!
     @IBOutlet weak var lbl_Subject: UILabel!
     @IBOutlet weak var txtv_Contain: UITextView!
     var dictEmailContain = NSDictionary()
    
    // MARK:
    // MARK: ----------Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(dictEmailContain.count != 0){
            
            dictEmailContain = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictEmailContain as! [AnyHashable : Any]))! as NSDictionary

            lbl_From.text = "\(dictEmailContain.value(forKey: "EmailFrom")!)"
            lbl_Subject.text = "\(dictEmailContain.value(forKey: "EmailSubject")!)"
            txtv_Contain.attributedText = htmlAttributedString(strHtmlString: "\(dictEmailContain.value(forKey: "EmailNotes")!)")
            
            txtv_Contain.layer.cornerRadius = 8.0
            txtv_Contain.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            txtv_Contain.layer.borderWidth = 1.0
        }
    }
    
   // MARK:
      // MARK: ----------IBAction
      @IBAction func actionOnBack(_ sender: UIButton) {
          self.view.endEditing(true)
          self.dismiss(animated: false, completion: nil)
      }
      
    
}
