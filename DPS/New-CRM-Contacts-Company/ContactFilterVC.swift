//
//  ContactFilterVC.swift
//  DPS
//
//  Created by Saavan Patidar on 05/03/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ContactFilterVC: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var txtfldFromDate: ACFloatingTextField!
    @IBOutlet weak var txtfldToDate: ACFloatingTextField!
    @IBOutlet weak var txtfldFirstName: ACFloatingTextField!
    @IBOutlet weak var txtfldMiddleName: ACFloatingTextField!
    @IBOutlet weak var txtfldLastName: ACFloatingTextField!
    @IBOutlet weak var txtfldCellPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryEmail: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextField!

    // MARK: Variables
    
    
    
    // MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        txtfldCellPhone.keyboardType = .phonePad
        txtfldPrimaryPhone.keyboardType = .phonePad
        txtfldPrimaryEmail.keyboardType = .emailAddress
        txtfldFirstName.delegate = self
        txtfldMiddleName.delegate = self
        txtfldLastName.delegate = self
        txtfldCellPhone.delegate = self
        txtfldPrimaryPhone.delegate = self
        txtfldPrimaryEmail.delegate = self
        txtfldCompanyName.delegate = self
        loadSavedSettings()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: Button Actions
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        backContactFilter()
        
    }
    
    @IBAction func actionOnApply(_ sender: UIButton) {
        
        if(self.txtfldFromDate.text!.count > 0 && self.txtfldToDate.text!.count > 0){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let fromDate = dateFormatter.date(from: self.txtfldFromDate.text!)
            let toDate = dateFormatter.date(from: self.txtfldToDate.text!)
        
        if(fromDate! > toDate!){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
            return
            
        }else{
            
            applyFilterContact()

            }
            
        }else{
            
            applyFilterContact()

        }
        
    }
    
    @IBAction func actionOnClear(_ sender: UIButton) {
        
        let alert = UIAlertController(title: alertMessage, message: alertFilterClear, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in

            setDefaultValuesForContactFilterAssociation()
            setDefaultValuesForContactFilter()
        
            nsud.synchronize()
            
            UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")
            UserDefaults.standard.set(true, forKey: "RefreshContacts_AssociateContactList")
            
            self.backContactFilter()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnFromDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // Setting Default Dates
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : result))!
        self.gotoDatePickerView(strTag: 101, strType: "Date",dateToSet: fromDate)
        
    }
    
    @IBAction func actionOnToDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : result))!
        self.gotoDatePickerView(strTag: 102, strType: "Date",dateToSet: fromDate)
        
    }
    
    // MARK: Functions

    func backContactFilter() {
        
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func applyFilterContact() {
        if(validation()){
            var dictLoginData = NSDictionary()
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

            let strCellnumber = ("\(txtfldCellPhone.text ?? "")".replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: " ", with: "").trimmed
            let strPrimaryPhoneNumber = ("\(txtfldPrimaryPhone.text ?? "")".replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: " ", with: "").trimmed
            
            var keys = NSArray()
            var values = NSArray()
            keys = ["CompanyKey",
                    "FromDate",
                    "ToDate",
                    "FirstName",
                    "MiddleName",
                    "LastName",
                    "PrimaryEmail",
                    "PrimaryPhone",
                    "CellPhone1",
                    "CrmCompanyName",
                    "Website",
                    "Owner"]
            
            values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                      (txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : "",
                      (txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : "",
                      (txtfldFirstName.text?.count)! > 0 ? txtfldFirstName.text! : "",
                      (txtfldMiddleName.text?.count)! > 0 ? txtfldMiddleName.text! : "",
                      (txtfldLastName.text?.count)! > 0 ? txtfldLastName.text! : "",
                      (txtfldPrimaryEmail.text?.count)! > 0 ? txtfldPrimaryEmail.text! : "",
                      strPrimaryPhoneNumber,
                      strCellnumber,
                      (txtfldCompanyName.text?.count)! > 0 ? txtfldCompanyName.text! : "",
                      "",
                      ""]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            
            nsud.setValue(dictToSend, forKey: "CrmContactFilter") //CrmContactFilterAssociation
            nsud.setValue(dictToSend, forKey: "CrmContactFilterAssociation")
            nsud.synchronize()
                    
            UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")
            UserDefaults.standard.set(true, forKey: "RefreshContacts_AssociateContactList")

            backContactFilter()
        }
      
        
    }
    func validation() -> Bool {
        if(txtfldCellPhone.text?.count != 0 &&  txtfldCellPhone.text!.count < 12){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid cell phone!", viewcontrol: self)
            return false

        }
        else if(txtfldPrimaryPhone.text?.count != 0 &&  txtfldPrimaryPhone.text!.count < 12){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid primary phone!", viewcontrol: self)
            return false

        }
        else if(txtfldPrimaryEmail.text?.count != 0 &&  !(Global().checkIfCommaSepartedEmailsAreValid(txtfldPrimaryEmail.text!)) ){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid primary email!", viewcontrol: self)
            return false

        }
    return true
    }
    func loadSavedSettings() {
                    
        let dictToSend = nsud.value(forKey: "CrmContactFilter") as! NSDictionary

        txtfldFromDate.text = dictToSend.value(forKey: "FromDate") as? String
        txtfldToDate.text = dictToSend.value(forKey: "ToDate") as? String
        txtfldFirstName.text = dictToSend.value(forKey: "FirstName") as? String
        txtfldMiddleName.text = dictToSend.value(forKey: "MiddleName") as? String
        txtfldLastName.text = dictToSend.value(forKey: "LastName") as? String
        txtfldPrimaryEmail.text = dictToSend.value(forKey: "PrimaryEmail") as? String
        txtfldPrimaryPhone.text = dictToSend.value(forKey: "PrimaryPhone") as? String
        txtfldCellPhone.text = dictToSend.value(forKey: "CellPhone1") as? String
        txtfldCompanyName.text = dictToSend.value(forKey: "CrmCompanyName") as? String

        
        txtfldPrimaryPhone.text = formattedNumber(number: txtfldPrimaryPhone.text ?? "")
        txtfldCellPhone.text = formattedNumber(number: txtfldCellPhone.text ?? "")

    }
    
    func gotoDatePickerView(strTag: Int, strType:String, dateToSet:Date )  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = strTag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
    
}

// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

extension ContactFilterVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if tag == 101 {
            
            txtfldFromDate.text = strDate
            
        } else if tag == 102 {
            
            txtfldToDate.text = strDate
            
        }
        
    }
    
}
//MARK:------UITextFieldDelegate-----------
//MARK:
extension ContactFilterVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == txtfldCellPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = txtfldCellPhone.text!
                strTextt = String(strTextt.dropLast())
                txtfldCellPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
        else if textField == txtfldPrimaryPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = txtfldPrimaryPhone.text!
                strTextt = String(strTextt.dropLast())
                txtfldPrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
        }
       
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 100)
    }
    
}
