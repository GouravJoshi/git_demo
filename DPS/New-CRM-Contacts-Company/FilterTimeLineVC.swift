//
//  FilterTimeLineVC.swift
//  peSTream
//  Created by Navin Patidar on 3/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan Patidar 2020
//  Test Saavan 2020
// Test Daasj

import UIKit

protocol FilterTimeLineData : class
{
    func getFilterTimeLineDataOnSelection(dictData : NSMutableDictionary ,strType : String)
}

class FilterTimeLineVC: UIViewController {
    
    weak var delegate: FilterTimeLineData?
    
    fileprivate  var aryUserName = NSArray()
    fileprivate  var arySelectedUserName = NSMutableArray()
    
    fileprivate  let aryDays  = ["1 Week","1 Month", "3 Months", "1 Year"] as NSArray
    fileprivate  var arySelectedDays = NSMutableArray()
    
    fileprivate  let aryInteraction  = ["Note", "Task", "Activity" ,"Email"] as NSArray
    fileprivate  var arySelectedInteraction = NSMutableArray()
    
    fileprivate  var strFromDate = ""
    fileprivate  var strToDate = ""
    
    var dictFilterData = NSMutableDictionary()
    var dictLoginData = NSDictionary()
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 200
        }
    }
    
    // MARK:
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        aryUserName = [["title":"\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")","id":"\(dictLoginData.value(forKey: "EmployeeId") ?? "")"] , ["title":"EveryOne","id": ""]]
        
        if(nsud.value(forKey: "DPS_FilterTimeLine") != nil){
            
            self.dictFilterData = (nsud.value(forKey: "DPS_FilterTimeLine")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            arySelectedUserName = NSMutableArray()
            
            if("\(self.dictFilterData.value(forKey: "SelectedUserName")!)" == ""){
                arySelectedUserName.add(aryUserName.object(at: 1))
            }else{
                arySelectedUserName.add(aryUserName.object(at: 0))

            }
            
            arySelectedInteraction = NSMutableArray()
            //arySelectedInteraction = (self.dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray
            
            if dictFilterData.value(forKey: "SelectedInteraction") is NSArray {
                
                arySelectedInteraction = (self.dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray

            }else {
                
                
            }
        
            
            strFromDate = "\(self.dictFilterData.value(forKey: "FromDate")!)"
            strToDate =  "\(self.dictFilterData.value(forKey: "Todate")!)"
     
            arySelectedDays = NSMutableArray()
            //arySelectedDays = (self.dictFilterData.value(forKey: "SelectedDate")as! NSArray).mutableCopy()as! NSMutableArray
            
            if dictFilterData.value(forKey: "SelectedDate") is NSArray {
                
                arySelectedDays = (self.dictFilterData.value(forKey: "SelectedDate")as! NSArray).mutableCopy()as! NSMutableArray

            }else {
                
                
            }
            
            if(arySelectedDays.count == 0){
                strFromDate = ""
                strToDate =  ""
            }
            
            self.tableView.reloadData()
        }
        
        
    }
    
    
    
    
    // MARK:
    // MARK: IBAction
    @IBAction func actionOnApply(_ sender: UIButton)
    {
        
        if(arySelectedUserName.count == 0 && arySelectedInteraction.count == 0 && (strFromDate == "" || strToDate == "" )){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one status", viewcontrol: self)
        }else{
            
            dictFilterData = NSMutableDictionary()
           
            dictFilterData.setValue(self.arySelectedInteraction, forKey: "SelectedInteraction")
            dictFilterData.setValue(self.arySelectedDays, forKey: "SelectedDate")
            
            
            self.arySelectedDays.count != 0 ?  dictFilterData.setValue(((arySelectedUserName.object(at: 0)as! NSDictionary).value(forKey: "id")!), forKey: "SelectedUserName"):  dictFilterData.setValue("", forKey: "SelectedUserName")
            
            if self.arySelectedDays.count != 0 {
                let strValue = "\(self.arySelectedDays.object(at: 0))"
                
                if(strValue == "1 Week"){
                    self.strFromDate = setDate(date: Date().startOfWeek!)
                    self.strToDate =  setDate(date:Date().endOfWeek!)
                }else if (strValue == "1 Month"){
                    self.strFromDate = setDate(date: Date().startOfMonth())
                    self.strToDate =  setDate(date:Date().endOfMonth())
                    
                }else if (strValue == "3 Months"){
                    self.strFromDate = setDate(date: Date().startOfMonth())
                    self.strToDate =  setDate(date:Date().endOfThreeMonth())
                    
                }else if (strValue == "1 Year"){
                    self.strFromDate = setDate(date: Date().startOfMonth())
                    self.strToDate =  setDate(date:Date().endOfYear())
                }
            }else{
              
                if(self.strFromDate.count > 0 && self.strToDate.count > 0){
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                let fromDate = dateFormatter.date(from: self.strFromDate)
                let toDate = dateFormatter.date(from: self.strToDate)
                
                if(fromDate! > toDate!){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
                    return
                     }
                    
                 }
                
            }
            
            dictFilterData.setValue(self.strFromDate, forKey: "FromDate")
            dictFilterData.setValue(self.strToDate, forKey: "Todate")
            nsud.setValue(dictFilterData, forKey: "DPS_FilterTimeLine")
            
            //Remove Key Value
            dictFilterData.removeObject(forKey: "SelectedDate")
            print(dictFilterData)
            delegate?.getFilterTimeLineDataOnSelection(dictData: dictFilterData, strType: "")
            self.dismiss(animated: false, completion: nil)
            
        }

    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
        
    }
    
    //MARK
    // MARK: Extra Function
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    func setDate(date : Date ) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/yy"
        let strDate = dateFormatter.string(from:date)
        
        if DeviceType.IS_IPAD{
            return  dateTimeConvertor(str: strDate, formet: "dd/MM/yy" , strFormatToBeConverted: "MM/dd/yyyy")
        }else{
            return  dateTimeConvertor(str: strDate, formet: "dd/MM/yy" , strFormatToBeConverted: "MM/dd/yyyy")
        }
    }
    
}

extension FilterTimeLineVC: UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "  User"
        case 1:
            return "  Days"
        case 2:
            return "  Date"
        case 3:
            return "  Interaction"
        default:
            return ""
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DeviceType.IS_IPAD ? 50 : 40
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return aryUserName.count
        case 1:
            return aryDays.count
        case 2:
            return 1
        case 3:
            return aryInteraction.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        
        if(indexPath.section == 0)
        {
            let dict = aryUserName.object(at: indexPath.row)as! NSDictionary
            cell.lblFilterName.text =  "\(dict.value(forKey: "title")!)"
            (arySelectedUserName.contains(dict)) ? cell.accessoryType = .checkmark : (cell.accessoryType = .none)
            
        }
        else  if(indexPath.section == 1)
        {
            cell.lblFilterName.text =  "\(aryDays[indexPath.row])"
            (arySelectedDays.contains("\(aryDays[indexPath.row])")) ? cell.accessoryType = .checkmark : (cell.accessoryType = .none)
          /*  if self.arySelectedDays.count != 0 {
                           let strValue = "\(self.arySelectedDays.object(at: 0))"
                           
                           if(strValue == "1 Week"){
                                self.strFromDate = setDate(date: Date().startOfWeek!)
                                 self.strToDate =  setDate(date:Date().endOfWeek!)
                           }else if (strValue == "1 Month"){
                               self.strFromDate = setDate(date: Date().startOfMonth())
                               self.strToDate =  setDate(date:Date().endOfMonth())
                               
                           }else if (strValue == "3 Months"){
                               self.strFromDate = setDate(date: Date().startOfMonth())
                               self.strToDate =  setDate(date:Date().endOfThreeMonth())
                               
                           }else if (strValue == "1 Year"){
                               self.strFromDate = setDate(date: Date().startOfMonth())
                               self.strToDate =  setDate(date:Date().endOfYear())
                           }
                       }*/
        }
        else  if(indexPath.section == 2)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "FilterDateCell") as! FilterCell
            cell1.btnFromDate.addTarget(self, action: #selector(actionOnFromDate), for: .touchUpInside)
            cell1.btnToDate.addTarget(self, action: #selector(actionOnToDate), for: .touchUpInside)
            cell1.txtFromDate.text = strFromDate
            cell1.txtToDate.text = strToDate
            cell1.btnClearDate.addTarget(self, action: #selector(actionOnClearDate), for: .touchUpInside)
            return cell1
        }
        else
        {
            let str = aryInteraction.object(at: indexPath.row)as! String
            cell.lblFilterName.text =  str
            (arySelectedInteraction.contains(str)) ? cell.accessoryType = .checkmark : (cell.accessoryType = .none)
        
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(DeviceType.IS_IPAD){
            return UITableView.automaticDimension
        }
        
        if(indexPath.section == 2)
        {
            return 125
        }
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0)
        {
            if(arySelectedUserName.contains(aryUserName.object(at: indexPath.row)))
            {
                arySelectedUserName.remove(aryUserName.object(at: indexPath.row))
            }
            else
            {
                arySelectedUserName.removeAllObjects()
                arySelectedUserName.add(aryUserName.object(at: indexPath.row))
            }
        }
        else if(indexPath.section == 1)
        {
            strFromDate = ""
            strToDate = ""
            if(arySelectedDays.contains(aryDays.object(at: indexPath.row)))
            {
                arySelectedDays.remove(aryDays.object(at: indexPath.row))
            }
            else
            {
                arySelectedDays.removeAllObjects()
                arySelectedDays.add(aryDays.object(at: indexPath.row))
            }
        }
        else if(indexPath.section == 2)
        {
            
        }
        else
        {
            
            if(arySelectedInteraction.contains(aryInteraction.object(at: indexPath.row)))
            {
                arySelectedInteraction.remove(aryInteraction.object(at: indexPath.row))
            }
            else
            {
                arySelectedInteraction.add(aryInteraction.object(at: indexPath.row))
            }
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    @objc func actionOnFromDate(sender: UIButton!) {
        sender.tag = 1
        arySelectedDays.removeAllObjects()
        gotoDatePickerView(sender: sender, strType: "Date")
        
    }
    @objc func actionOnToDate(sender: UIButton!) {
        sender.tag = 2
        arySelectedDays.removeAllObjects()
        gotoDatePickerView(sender: sender, strType: "Date")
    }
    
    @objc func actionOnClearDate(sender: UIButton!)
    {
        strFromDate = ""
        strToDate = ""
        tableView.reloadData()
    }
    
}
// MARK:
// MARK: FilterCell
class FilterCell:UITableViewCell
{
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var btnClearDate: UIButton!
    @IBOutlet weak var txtFromDate: ACFloatingTextField!
    @IBOutlet weak var txtToDate: ACFloatingTextField!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK:
// MARK: DatePickerProtocol
extension FilterTimeLineVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.arySelectedDays.removeAllObjects()
        self.view.endEditing(true)
        tag == 1 ? strFromDate = strDate : (strToDate = strDate)
        tableView.reloadData()
    }
}

extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
   
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func endOfThreeMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 3, day: -1), to: self.startOfMonth())!
    }
    func endOfYear() -> Date {
        
        return Calendar.current.date(byAdding: DateComponents(year: 1, month: 0, day: -1), to: self.startOfMonth())!
        
    }
   var startOfWeek: Date? {
       let gregorian = Calendar(identifier: .gregorian)
       guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
       return gregorian.date(byAdding: .day, value: 1, to: sunday)
   }

   var endOfWeek: Date? {
       let gregorian = Calendar(identifier: .gregorian)
       guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
       return gregorian.date(byAdding: .day, value: 7, to: sunday)
   }
}
