//
//  AccountFilterVC.swift
//  DPS
//
//  Created by Saavan Patidar on 18/03/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class AccountFilterVC: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var txtfldFromDate: ACFloatingTextField!
    @IBOutlet weak var txtfldToDate: ACFloatingTextField!
    @IBOutlet weak var txtfldFirstName: ACFloatingTextField!
    @IBOutlet weak var txtfldMiddleName: ACFloatingTextField!
    @IBOutlet weak var txtfldLastName: ACFloatingTextField!
    @IBOutlet weak var txtfldAccountName: ACFloatingTextField!
    @IBOutlet weak var txtfldAccountNo: ACFloatingTextField!
    @IBOutlet weak var txtfldCompanyName: ACFloatingTextField!

    @IBOutlet weak var txtfldPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldEmail: ACFloatingTextField!
    @IBOutlet weak var txtfldAddress: ACFloatingTextField!
    @IBOutlet weak var txtfldCity: ACFloatingTextField!
    @IBOutlet weak var txtfldState: ACFloatingTextField!
    @IBOutlet weak var txtfldZip: ACFloatingTextField!
    @IBOutlet weak var txtfldContactName: ACFloatingTextField!
    
    // MARK: Variables
    // variables
    var strStateId = ""
    
    
    // MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        loadSavedSettings()
        
        // Do any additional setup after loading the view.
        
    }
    
    // MARK: Button Actions
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        backContactFilter()
        
    }
    
    @IBAction func actionOnApply(_ sender: UIButton) {
        
        if(validationOnView()){

            applyFilterContact()
            
        }
        
    }
    @IBAction func actionOnFromDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        // Setting Default Dates
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : result))!
        self.gotoDatePickerView(strTag: 101, strType: "Date",dateToSet: fromDate)
        
    }
    
    @IBAction func actionOnToDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = dateFormatter.date(from:((txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : result))!
        self.gotoDatePickerView(strTag: 102, strType: "Date",dateToSet: fromDate)
        
    }
    
    @IBAction func actionOnState(_ sender: UIButton) {

        self.view.endEditing(true)
        
        if let path = Bundle.main.path(forResource: StateJsonSwift, ofType: "json") {
            do {
                
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                let stateList = (jsonResult as! NSArray).mutableCopy() as! NSMutableArray
                
                openTableViewPopUp(tag: 64, ary: stateList, aryselectedItem: NSMutableArray())
                
            } catch {
                
                Global().displayAlertController(Alert, NoDataAvailableee, self)
                
            }
        }
        
    }
    
    // MARK: Functions

    func validationOnView() -> Bool {

        if(txtfldFromDate.text!.count > 0)
        {
            
            if(txtfldToDate.text!.count == 0)
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select created to date.", viewcontrol: self)
                return false
                
            }
            
        }
        if(txtfldToDate.text!.count > 0)
        {
            
            if(txtfldFromDate.text!.count == 0)
            {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select created from date.", viewcontrol: self)
                return false
                
            }
            
        }
        
        if(self.txtfldFromDate.text!.count > 0 && self.txtfldToDate.text!.count > 0){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let fromDate = dateFormatter.date(from: self.txtfldFromDate.text!)
            let toDate = dateFormatter.date(from: self.txtfldToDate.text!)
        
        if(fromDate! > toDate!){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To Date should be greater than From Date.", viewcontrol: self)
            return false
            
        }
            
        }
        
        return true
        
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)

            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func backContactFilter() {
        
        self.view.endEditing(true)
        
        if nsud.bool(forKey: "Initial_RefreshAccounts_AccountList") {

            UserDefaults.standard.set(false, forKey: "Initial_RefreshAccounts_AccountList")
            UserDefaults.standard.set(true, forKey: "RefreshAccounts_AccountList")
            UserDefaults.standard.set(true, forKey: "Dismiss_AccountList")

        }
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func applyFilterContact() {
        
        UserDefaults.standard.set(false, forKey: "Initial_RefreshAccounts_AccountList")

        nsud.set(true, forKey: "isInitalAccount")
        nsud.synchronize()
        
        let noLocal = getOnlyNumberFromString(number: txtfldPhone.text!)

        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyKey",
                "FromDate",
                "ToDate",
                "FirstName",
                "MiddleName",
                "LastName",
                "AccountName",
                "AccountNo",
                "CrmCompanyName",
                "Phone",
                "Email",
                "Address",
                "CityName",
                "StateId",
                "Zipcode"]
        
        values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                  (txtfldFromDate.text?.count)! > 0 ? txtfldFromDate.text! : "",
                  (txtfldToDate.text?.count)! > 0 ? txtfldToDate.text! : "",
                  (txtfldFirstName.text?.count)! > 0 ? txtfldFirstName.text! : "",
                  (txtfldMiddleName.text?.count)! > 0 ? txtfldMiddleName.text! : "",
                  (txtfldLastName.text?.count)! > 0 ? txtfldLastName.text! : "",
                  (txtfldAccountName.text?.count)! > 0 ? txtfldAccountName.text! : "",
                  (txtfldAccountNo.text?.count)! > 0 ? txtfldAccountNo.text! : "",
                  (txtfldCompanyName.text?.count)! > 0 ? txtfldCompanyName.text! : "",
                  (txtfldPhone.text?.count)! > 0 ? noLocal : "",
                  (txtfldEmail.text?.count)! > 0 ? txtfldEmail.text! : "",
                  (txtfldAddress.text?.count)! > 0 ? txtfldAddress.text! : "",
                  (txtfldCity.text?.count)! > 0 ? txtfldCity.text! : "",
                  (strStateId.count) > 0 ? strStateId : "",
                  (txtfldZip.text?.count)! > 0 ? txtfldZip.text! : ""]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        nsud.setValue(dictToSend, forKey: "CrmAccountFilter")
        nsud.synchronize()
        
        //txtFld_FN.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        UserDefaults.standard.set(true, forKey: "RefreshAccounts_AccountList")

        backContactFilter()
        
    }
    
    func loadSavedSettings() {
                    
        let dictToSend = nsud.value(forKey: "CrmAccountFilter") as! NSDictionary

        txtfldFromDate.text = dictToSend.value(forKey: "FromDate") as? String
        txtfldToDate.text = dictToSend.value(forKey: "ToDate") as? String
        txtfldFirstName.text = dictToSend.value(forKey: "FirstName") as? String
        txtfldMiddleName.text = dictToSend.value(forKey: "MiddleName") as? String
        txtfldLastName.text = dictToSend.value(forKey: "LastName") as? String
        txtfldAccountName.text = dictToSend.value(forKey: "AccountName") as? String
        txtfldAccountNo.text = dictToSend.value(forKey: "AccountNo") as? String
        txtfldCompanyName.text = dictToSend.value(forKey: "CrmCompanyName") as? String
        
        let formattedCellPhoneNo = formattedNumber(number: "\(dictToSend.value(forKey: "Phone")!)")
        
        txtfldPhone.text = formattedNumber(number: formattedCellPhoneNo)
        
        //txtfldPhone.text = dictToSend.value(forKey: "Phone") as? String
        txtfldEmail.text = dictToSend.value(forKey: "Email") as? String
        txtfldAddress.text = dictToSend.value(forKey: "Address") as? String
        txtfldCity.text = dictToSend.value(forKey: "CityName") as? String
        strStateId = (dictToSend.value(forKey: "StateId") as? String)!
        
        if strStateId.count > 0 {
            
            txtfldState.text = Global().strStatName(fromID: strStateId)

        } else {
            
            txtfldState.text = ""

        }
        txtfldZip.text = dictToSend.value(forKey: "Zipcode") as? String
        txtfldContactName.text = dictToSend.value(forKey: "ContactName") as? String
        
    }
    
    func gotoDatePickerView(strTag: Int, strType:String, dateToSet:Date )  {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = strTag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        //vc.handleOnSelectionOfDate = self
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
        
    }
    
}


// MARK: - -----------------------------------Date Picker Protocol-----------------------------------

extension AccountFilterVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if tag == 101 {
            
            txtfldFromDate.text = strDate
            
        } else if tag == 102 {
            
            txtfldToDate.text = strDate
            
        }
        
    }
    
}

// MARK: -
// MARK: - Selection CustomTableView
extension AccountFilterVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 64)
        {
            if(dictData.count == 0){
                txtfldState.text = ""
                strStateId = ""
            }else{
                txtfldState.text = "\(dictData.value(forKey: "Name")!)"
                strStateId = "\(dictData.value(forKey: "StateId")!)"
            }
            
        }

    }
    
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AccountFilterVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == txtfldPhone  {

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        }
        if textField == txtfldZip  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)
            
        }
        return true
        
    }
    
}
