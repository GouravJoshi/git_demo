//
//  AddContactVC_CRMContactNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 30/01/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan 2020

import UIKit
import Contacts
import ContactsUI
import VisionKit
import Vision

enum MoreDetails{
    case more,less
}

@available(iOS 13.0, *)
var textRecognitionRequest = VNRecognizeTextRequest()
@available(iOS 13.0, *)
var recognizedText = ""


class AddContactVC_CRMContactNew_iPhone: UIViewController {
    
    // outlets
    
    @IBOutlet weak var txtfldFirstName: ACFloatingTextField!
    @IBOutlet weak var txtfldLastName: ACFloatingTextField!
    @IBOutlet weak var txtfldCellPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldEmail: ACFloatingTextField!
    @IBOutlet weak var btnMoreDetails: UIButton!
    @IBOutlet weak var btnImportFromPhone: UIButton!
    @IBOutlet weak var btnScanBusinessCard: UIButton!
    @IBOutlet weak var constHghtViewMoreDetails: NSLayoutConstraint!
    @IBOutlet weak var constHghtViewLessDetails: NSLayoutConstraint!

    
    @IBOutlet weak var txtfldJobTitle: ACFloatingTextField!
    @IBOutlet weak var txtfldMiddleName: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldPrimaryPhoneExt: ACFloatingTextField!
    @IBOutlet weak var txtfldSecondaryPhone: ACFloatingTextField!
    @IBOutlet weak var txtfldSecondaryPhoneExt: ACFloatingTextField!
    @IBOutlet weak var txtfldSecondaryEmail: ACFloatingTextField!
    @IBOutlet weak var btnAddNow: UIButton!
    @IBOutlet weak var txtfldAssociateCompany: ACFloatingTextField!
    @IBOutlet weak var btnAssociateCompany: UIButton!
    @IBOutlet weak var txtfldSourceName: ACFloatingTextField!
    @IBOutlet weak var txtfldOwnerName: ACFloatingTextField!
    @IBOutlet weak var txtfldSalutation: ACFloatingTextField!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var imgBtnMoreDetail: UIImageView!
    @IBOutlet weak var txtfldAddress: ACFloatingTextField!
    @IBOutlet weak var constHghtAssociateCompany: NSLayoutConstraint!
    @IBOutlet weak var constHghtViewImportContacts: NSLayoutConstraint!
    @IBOutlet weak var constHghtViewImportContactsMainView: NSLayoutConstraint!
    @IBOutlet weak var imgAssocaiteCompanyDetail: UIImageView!
    @IBOutlet weak var lblMainHeader: UILabel!
    @IBOutlet weak var imgViewContactProfile: UIImageView!
    @IBOutlet weak var lblProfileImageName: UILabel!

    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    @IBOutlet weak var tableView: UITableView!
    
    // variables
    var moreDetails = MoreDetails.more
    var dictLoginData = NSDictionary()
    var strSourceId = ""
    var strOwnerId = ""
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strAddressLine2 = ""
    var strZipCode = ""
    var strCrmCompanyIdToAssociateCompanyToContact = ""
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strFrom = ""
    var dictContact = NSDictionary()
    var dictContactBasicInfo = NSDictionary()
    var imagePicker = UIImagePickerController()
    var strGlobalImageName = ""
    let dispatchGroup = DispatchGroup()
    var isSecondaryEmail = false
    var strGlobalImageNameIfAlreadyPresent = ""
    var strFromAssociate = ""
    var strRefType = String()
    var strRefId = String()
    var strFromWhereForAssociation = ""
    var strFromWhere = ""
    var loader = UIAlertController()
    var arrSelectedSource = NSMutableArray()

    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            textRecognitionRequest = VNRecognizeTextRequest(completionHandler: { (request, error) in
                if let results = request.results, !results.isEmpty {
                    if let requestResults = request.results as? [VNRecognizedTextObservation] {
                        recognizedText = ""
                        for observation in requestResults {
                            guard let candidiate = observation.topCandidates(1).first else { return }
                            recognizedText += candidiate.string
                            recognizedText += "\n"
                        }
                        //print(recognizedText)
                        //self.txtfldAddressLine1.text = recognizedText
                        self.extractFromScannedText(scannedText: recognizedText)
                        
                    }
                }
            })
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 13.0, *) {
            textRecognitionRequest.usesLanguageCorrection = false
            textRecognitionRequest.recognitionLevel = .accurate
            textRecognitionRequest.customWords = ["@gmail.com", "@outlook.com", "@yahoo.com", "@icloud.com"]
            
        } else {
            // Fallback on earlier versions
        }
        
        allTextFldActionsAddContactView()
        
        // If Associated Company Exist
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddContact_AssociateCompany_Notification"),
                                               object: nil,
                                               queue: nil,
                                               using:catchNotification)
        
        txtAddressMaxY = (txtfldAddress.frame.origin.y) - 35
        imgPoweredBy.image = UIImage(named: "powered-by-google-on-white")
        imgPoweredBy.contentMode = .center
        
        if strFrom == "Update" {
            
            txtfldAssociateCompany.isHidden = true
            btnAssociateCompany.isHidden = true
            imgAssocaiteCompanyDetail.isHidden = true
            //constHghtAssociateCompany.constant = 0.0
            constHghtViewImportContacts.constant = 0.0
            constHghtViewImportContactsMainView.constant = 80.0
            btnAddNow.setTitle("Update Now", for: .normal)
            lblMainHeader.text = "Update Contact"
            
            if(isInternetAvailable() == false)
            {
                
                if dictContactBasicInfo.count == 0 {
                    
                    self.fillContactDetailToUpdate()

                }else{
                    
                    self.navigationController?.popViewController(animated: false)

                }
                //self.navigationController?.popViewController(animated: false)

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            else
            {
                
                if dictContactBasicInfo.count == 0 {
                    
                    // Call API to fetch Contact Data

                    loader = loader_Show(controller: self, strMessage: "please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        self.callAPIToGetContactBasicInfo()
                    })
                } else {
                    self.fillContactDetailToUpdate()
                }
            }
            
        }else{
            //constHghtAssociateCompany.constant = 40.0
            constHghtViewImportContacts.constant = DeviceType.IS_IPAD ? 170 :  112.0
            constHghtViewImportContactsMainView.constant = DeviceType.IS_IPAD ? 250 :  176
            btnAddNow.setTitle("Add Now", for: .normal)
            txtfldAssociateCompany.isHidden = false
            btnAssociateCompany.isHidden = false
            imgAssocaiteCompanyDetail.isHidden = false
            lblMainHeader.text = "Add Contact"
            imgViewContactProfile.isHidden = false
            lblProfileImageName.isHidden = true
            strOwnerId = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            txtfldOwnerName.text = "\(dictLoginData.value(forKeyPath: "EmployeeName")!)"
        }
        
        lblProfileImageName.layer.borderWidth = 1.0
        lblProfileImageName.layer.borderColor = UIColor.darkGray.cgColor
        lblProfileImageName.layer.cornerRadius = lblProfileImageName.frame.size.width / 2
        imgViewContactProfile.layer.borderWidth = 1.0
        imgViewContactProfile.layer.masksToBounds = false
        imgViewContactProfile.layer.borderColor = UIColor.darkGray.cgColor
        imgViewContactProfile.layer.cornerRadius = imgViewContactProfile.frame.size.width / 2
        imgViewContactProfile.clipsToBounds = true
        
        
        constHghtViewMoreDetails.constant = 0.0
    }
    
    override func viewDidLayoutSubviews() {
        

      
        
       
        btnMoreDetails.layer.cornerRadius =  btnMoreDetails.frame.size.height/2
        btnMoreDetails.layer.borderColor = UIColor.gray.cgColor
        btnMoreDetails.layer.borderWidth = 0.5
        btnMoreDetails.layer.masksToBounds = true
        
        btnImportFromPhone.layer.borderColor = UIColor.gray.cgColor
        btnImportFromPhone.layer.cornerRadius = 2
        btnImportFromPhone.layer.borderWidth = 0.5
        
        btnScanBusinessCard.layer.borderColor = UIColor.gray.cgColor
        btnScanBusinessCard.layer.cornerRadius = 2
        btnScanBusinessCard.layer.borderWidth = 0.5

        btnAddNow.layer.cornerRadius =  btnAddNow.frame.size.height/2
        btnAddNow.layer.masksToBounds = true
        
    }
    
    // MARK: UIButton action
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnSource(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count > 0)
                {
                    
                    //arrOfData = addSelectInArray(strName: "Name", array: arrOfData)
                    
                    goToNewSelectionVC(arrOfData: returnFilteredArray(array: arrOfData), tag: 101, titleHeader: "Source")

                    //openTableViewPopUp(tag: 601, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
                    
                }
                else
                {
                    // show alert no data available
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
                
            }
            
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
            
        }
        
    }
    
    
    @IBAction func actionOnOwnerName(_ sender: UIButton) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var arrOfOwnerName = NSMutableArray()
        
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        
        if nsud.value(forKeyPath: "EmployeeList") is NSArray {
            
            if(aryTemp.count > 0)
            {
                for item in aryTemp
                {
                    let dict = item as! NSDictionary
                    if(dict.value(forKey: "IsActive") as! Bool == true)
                    {
                        arrOfOwnerName.add(dict)
                    }
                }
                
                if(arrOfOwnerName.count > 0)
                {
                    
                    arrOfOwnerName = addSelectInArray(strName: "FullName", array: arrOfOwnerName)
                    
                    openTableViewPopUp(tag: 602, ary: arrOfOwnerName , aryselectedItem: NSMutableArray())
                }
                else
                {
                    // show alert no data available
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            }else{
                // show alert no data available
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
            
        }
        
        
    }
    
     @IBAction func actionOnMoreDetails(_ sender: UIButton) {
      //    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
              self.view.endEditing(true)
        //  })
           showLessAndMore()
      }
      
      func showLessAndMore() {
          self.imgPoweredBy.removeFromSuperview()
          self.tableView.removeFromSuperview()
          switch moreDetails {
          case .more:
              self.btnMoreDetails.setTitle("Less Details", for: .normal)
              /*self.constHghtViewMoreDetails.constant = 345.0
               scrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: false)
               scrollView.contentSize = CGSize(width: self.view.frame.width, height: 800)*/
              moreDetails = MoreDetails.less
              self.imgBtnMoreDetail.image = UIImage(named: "drop_down_up_ipad")
              if (DeviceType.IS_IPAD){
                  constHghtViewMoreDetails.constant = 485.0
              }else{
                  constHghtViewMoreDetails.constant = 345.0
              }
              return
          case .less:
              
              self.btnMoreDetails.setTitle("More Details", for: .normal)
              /*self.constHghtViewMoreDetails.constant = 0.0
               scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
               scrollView.contentSize = CGSize(width: self.view.frame.width, height: 400)*/
              moreDetails = MoreDetails.more
              constHghtViewMoreDetails.constant = 0.0

              self.imgBtnMoreDetail.image = UIImage(named: "drop_down_ipad")
              return
          }
          
       
      }
      
    
    @IBAction func actionOnImportFromPhone(_ sender: UIButton) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)
        
        if (isInternetAvailable()){
            
            checkIfContactPermissionAllowed()
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnScanBusinessCard(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if #available(iOS 13.0, *) {
            
            let documentCameraViewController = VNDocumentCameraViewController()
            documentCameraViewController.delegate = self
            self.present(documentCameraViewController, animated: true, completion: nil)
            
        } else {
            
            // Fallback on earlier versions
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Only available for iOS 13 and above", viewcontrol: self)
            
        }
        
        
    }
    
    @IBAction func actionOnCity(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnState(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnAssociateCompany(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.view.endEditing(true)
            
        })
        
           let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        controller.strFromWhereForAssociation = "AddContact"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @IBAction func actionOnContactProfileImage(_ sender: UIButton) {
        
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
          alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
            
            let imageView = UIImageView(image: self.imgViewContactProfile.image)
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imageView.image!
            self.present(testController!, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = sender as UIView
                    popoverController.sourceRect = sender.bounds
                  //  popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                }
                        self.present(alert, animated: true, completion: {
        })
        
    }
    @IBAction func actionOnAddNow(_ sender: UIButton) {
        self.view.endEditing(true)
                self.imgPoweredBy.removeFromSuperview()
                self.tableView.removeFromSuperview()
        
        UserDefaults.standard.set(true, forKey: "RefreshAssociations_ContactDetails")
        
        let ifError = self.validation()
        
        if ifError {
             // showLessAndMore()
            // Error do no call API
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid data.", viewcontrol: self)
            
        }else{
            
            // check if profile image taken then upload image first and then upload data
            
            if strGlobalImageName.count > 0 {
                
                loader = loader_Show(controller: self, strMessage: "please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    
                    self.callAPIToUploadContactProfileImage()

                    self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                        
                        //all asynchronous tasks added to this DispatchGroup are completed. Proceed as required.
                        
                        self.loader.dismiss(animated: false) {


                        }

                    })
                    
                })
                
            } else {
                
                convertEnteredDetailsToArray()

            }
            
        }
        
    }
    
    @IBAction func action_AddAddress(_ sender: Any) {
          
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
        
    }
    
    func validation() -> Bool {
        
        var error = false
        
        if txtfldFirstName.text?.count == 0 {
            
            error = true
            txtfldFirstName.becomeFirstResponder()

            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_FirstName, viewcontrol: self)

            return true
        }
        if (txtfldAddress.text!.count > 0) {

            let dictOfEnteredAddress = formatEnteredAddress(value: txtfldAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                return true
                
            } else {
                
                
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"

                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"

                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""

                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                    return true
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                    return true
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                    return true
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                    return true
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)

                    return true
                    
                }
                
            }
            
            
        }
        /*if (txtfldAddress.text!.count > 0) {
            
                // Fetch Address
                
            let arrOfAddress = self.getAddress(from: txtfldAddress.text!)
            
            let tempArrayOfKeys = NSMutableArray()
            let tempDictData = NSMutableDictionary()

            for k in 0 ..< arrOfAddress.count {
             
                      if arrOfAddress[k] is NSDictionary {
                
                         let tempDict = arrOfAddress[k] as! NSDictionary
                    
                         tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                         tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])

                      }
                
            }
            
            if tempArrayOfKeys.contains("City") {
                
                strCityName = "\(tempDictData.value(forKey: "City")!)"

            }
            if tempArrayOfKeys.contains("ZIP") {
                
                strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"

            }
            if tempArrayOfKeys.contains("State") {
                
                let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                    
                }else{
                    
                    strStateId = ""
                    //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)

                }

            }
            if tempArrayOfKeys.contains("Street") {
                
                strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"

            }
            
            if strAddressLine1.count == 0 || strCityName.count == 0 || strZipCode.count == 0 || strStateId.count == 0 {
                
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter proper address (address, city, zipcode, state).", viewcontrol: self)
                
                return true
                
            }
            
        }*/
        
        if txtfldCellPhone.text!.count > 0 {

            let noLocal = getOnlyNumberFromString(number: txtfldCellPhone.text!)
            
            if noLocal.count != 10 {
                
                error = true
                txtfldCellPhone.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Cell # invalid!", viewcontrol: self)


                return true
            }
            
        }
        if txtfldEmail.text!.count > 0 {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtfldEmail.text!)
            
            if !isValid {
                
                error = true
                txtfldEmail.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Primary email address invalid!", viewcontrol: self)

                return true
                
            }
            
            /*let isvalidateEmail = validateEmail(email: txtfldEmail.text!)
            
            if !isvalidateEmail {
                
                error = true
                txtfldEmail.errorTextColor = UIColor.red
                txtfldEmail.errorLineColor = UIColor.red
                txtfldEmail.showError(withText: Alert_EmailAddress)
                
            }*/
            
        }
        if txtfldPrimaryPhone.text!.count > 0 {

            let noLocal = getOnlyNumberFromString(number: txtfldPrimaryPhone.text!)
            
            if noLocal.count != 10 {
                
                error = true
                txtfldPrimaryPhone.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Primary phone invalid!", viewcontrol: self)


                return true
                
            }
            
        }
        if txtfldSecondaryPhone.text!.count > 0 {

            let noLocal = getOnlyNumberFromString(number: txtfldSecondaryPhone.text!)
            
            if noLocal.count != 10 {
                
                error = true
                txtfldSecondaryPhone.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Secondary phone invalid!", viewcontrol: self)

                
            
                return true
            }
            
        }
        if txtfldSecondaryEmail.text!.count > 0 {
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtfldSecondaryEmail.text!)
            
            if !isValid {
                
                error = true
                txtfldSecondaryEmail.becomeFirstResponder()

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Secondary email address invalid!", viewcontrol: self)


                return true
            }
            
            /*let isvalidateEmail = validateEmail(email: txtfldSecondaryEmail.text!)
            
            if !isvalidateEmail {
                
                error = true
                txtfldSecondaryEmail.errorTextColor = UIColor.red
                txtfldSecondaryEmail.errorLineColor = UIColor.red
                txtfldSecondaryEmail.showError(withText: Alert_EmailAddress)
                
            }*/
            
        }
        
        return error
        
    }
    
    // MARK: Functions
    
    func fetchSourceFromMasterViaSourceId(sourceIdToFetch : String) -> NSDictionary {
        
        var dictOfDataa = NSDictionary()
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                let arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count > 0)
                {
                    for item in arrOfData {
                        
                        if item is NSDictionary {
                            
                            if "\((item as! NSDictionary).value(forKey: "SourceId") ?? "")" == sourceIdToFetch{
                                
                                dictOfDataa = (item as! NSDictionary)
                                
                            }
                            
                        }
                        
                    }
                                                            
                }
                else
                {
                    // show alert no data available
                                        
                }
                
            } else {
                
                
            }
            
            
        } else {
            
            
        }
        
        return dictOfDataa
        
    }
    
    func goToNewSelectionVC(arrOfData : NSMutableArray, tag : Int, titleHeader : String) {
        
        if(arrOfData.count != 0){
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Lead-Prospect", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectGlobalVC") as! SelectGlobalVC
            controller.titleHeader = titleHeader
            controller.arrayData = arrOfData
            controller.arraySelectionData = arrOfData
            controller.arraySelected = arrSelectedSource
            controller.delegate = self
            controller.tag = tag
            self.navigationController?.present(controller, animated: false, completion: nil)
            
        }else {
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    func downloadProfileImage() {
        
        if let imgURL = dictContactBasicInfo.value(forKey: "ProfileImage")
        {
            if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
            {
                
                self.createContactLogoFromName(dictData: dictContactBasicInfo)
                
            }
            else
            {
                
                imgViewContactProfile.isHidden = false
                lblProfileImageName.isHidden = true
                
                // Remove Logic for image URL
                
                let imgUrlNew = replaceBackSlasheFromUrl(strUrl: imgURL as! String)
                
                let arrImg = imgUrlNew.components(separatedBy: "/")
                
                if arrImg.count > 0 {
                    
                    strGlobalImageNameIfAlreadyPresent = arrImg.last!
                    
                }
                
                //cell.imgviewProfile.sd_setImage(with: URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), options: .refreshCached)
                imgViewContactProfile.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                
            }
        }else{
            
            self.createContactLogoFromName(dictData: dictContactBasicInfo)
            
        }
        
    }
    
    func createContactLogoFromName(dictData : NSDictionary) {
        
        lblProfileImageName.layer.borderWidth = 1.0
        lblProfileImageName.layer.masksToBounds = false
        lblProfileImageName.layer.borderColor = UIColor.white.cgColor
        lblProfileImageName.layer.cornerRadius = 30//imgviewProfile.frame.size.width / 2
        lblProfileImageName.clipsToBounds = true
        
        
        imgViewContactProfile.isHidden = true
        lblProfileImageName.isHidden = false
        let firstName = "\(dictData.value(forKey: "FirstName") ?? "")"
        let lastName = "\(dictData.value(forKey: "LastName") ?? "")"
        
        lblProfileImageName.text = firstCharactersFromString(type: "FirstLastName", first: firstName, second: lastName)
        
    }
    
    func catchNotification(notification:Notification) {
        
        let dictDataCompany = (notification.userInfo as AnyObject)as! NSDictionary
        
        strCrmCompanyIdToAssociateCompanyToContact = "\(dictDataCompany.value(forKey: "CrmCompanyId") ?? "")"
        
        txtfldAssociateCompany.text = "\(dictDataCompany.value(forKey: "Name") ?? "")"
        
    }
    
    func allTextFldActionsAddContactView() {
        
        //txtfldAssociateCompany.addTarget(self, action: #selector(goToAssociateCompanyAddContactView), for: .allEvents)
        txtfldSourceName.addTarget(self, action: #selector(selectSourceName), for: .allEvents)
        txtfldOwnerName.addTarget(self, action: #selector(selectOwnerName), for: .allEvents)
        
    }
    
    @objc func goToAssociateCompanyAddContactView(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateCompanyVC_CRMContactNew_iPhone") as! AssociateCompanyVC_CRMContactNew_iPhone
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @objc func selectSourceName(textField: UITextField) {
        
        self.view.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            
            self.view.endEditing(true)
            
        })
        
        let defsLogindDetail = UserDefaults.standard
        
        if defsLogindDetail.value(forKey: "LeadDetailMaster") is NSDictionary {
            
            let dictLeadDetailMaster = defsLogindDetail.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictLeadDetailMaster.value(forKey: "SourceMasters") is NSArray {
                
                var arrOfData = (dictLeadDetailMaster.value(forKey: "SourceMasters") as! NSArray).mutableCopy() as! NSMutableArray
                
                if(arrOfData.count > 0)
                {
                    
                    arrOfData = addSelectInArray(strName: "Name", array: arrOfData)
                    
                    openTableViewPopUp(tag: 601, ary: returnFilteredArray(array: arrOfData) , aryselectedItem: NSMutableArray())
                    
                }
                else
                {
                    // show alert no data available
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            } else {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
                
            }
            
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
            
        }
        
    }
    @objc func selectOwnerName(textField: UITextField) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.view.endEditing(true)
            
        })
        
        var arrOfOwnerName = NSMutableArray()
        
        let aryTemp = nsud.value(forKeyPath: "EmployeeList") as! NSArray
        
        if nsud.value(forKeyPath: "EmployeeList") is NSArray {
            
            if(aryTemp.count > 0)
            {
                for item in aryTemp
                {
                    let dict = item as! NSDictionary
                    if(dict.value(forKey: "IsActive") as! Bool == true)
                    {
                        arrOfOwnerName.add(dict)
                    }
                }
                
                if(arrOfOwnerName.count > 0)
                {
                    
                    arrOfOwnerName = addSelectInArray(strName: "FullName", array: arrOfOwnerName)
                    
                    openTableViewPopUp(tag: 602, ary: arrOfOwnerName , aryselectedItem: NSMutableArray())
                }
                else
                {
                    // show alert no data available
                    
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                }
                
            }else{
                // show alert no data available
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
                
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: NoDataAvailable, viewcontrol: self)
            
        }
        
        
    }
    func getAddress(from dataString: String) ->  NSMutableArray {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        let resultsArray = NSMutableArray()
        // put matches into array of Strings
        for match in matches {
            if match.resultType == .address,
                let components = match.addressComponents {
                resultsArray.add(components)
            } else {
                print("no components found")
            }
        }
        return resultsArray
    }
    
    func extractFromScannedText(scannedText : String) {
        
        var strReplacedString = String()
        strReplacedString = scannedText
        
        //let tempString = NSMutableString()
        
        let arrEmail = scannedText.emailAddresses()
        
        for k in 0 ..< arrEmail.count {
            
            let strTemp = arrEmail[k]
            
            if k == 0 {
                
                self.txtfldEmail.text = strTemp
                
            }
            if k == 1 {
                
                self.txtfldSecondaryEmail.text = strTemp
                
            }
            
            strReplacedString = strReplacedString.replacingOccurrences(of: strTemp, with: "")
            
        }
        
        let arrUrl = scannedText.extractURLs()
        
        for k in 0 ..< arrUrl.count {
            
            let strTemp = arrUrl[k]
            
            let urlString = strTemp.absoluteString
            
            strReplacedString = strReplacedString.replacingOccurrences(of: urlString, with: "")
            
        }
        
        let arrPhoneNo = scannedText.phoneNo()
        
        for k in 0 ..< arrPhoneNo.count {
            
            let strTemp = arrPhoneNo[k]
            
            if k == 0 {
                
                self.txtfldCellPhone.text = strTemp
                
            }
            if k == 1 {
                
                self.txtfldPrimaryPhone.text = strTemp
                
            }
            if k == 2 {
                
                self.txtfldSecondaryPhone.text = strTemp
                
            }
            
            strReplacedString = strReplacedString.replacingOccurrences(of: strTemp, with: "")
            
        }
        
        // Fetch Address
        
        let arrOfAddress = self.getAddress(from: scannedText)
        
        let tempArrayOfKeys = NSMutableArray()
        let tempDictData = NSMutableDictionary()
        
        for k in 0 ..< arrOfAddress.count {
            
            if arrOfAddress[k] is NSDictionary {
                
                let tempDict = arrOfAddress[k] as! NSDictionary
                
                tempArrayOfKeys.addObjects(from: tempDict.allKeys)
                tempDictData.addEntries(from: tempDict as! [AnyHashable : Any])
                
            }
            
        }
        
        if tempArrayOfKeys.contains("City") {
            
            strCityName = "\(tempDictData.value(forKey: "City")!)"
            strReplacedString = strReplacedString.replacingOccurrences(of: "\(tempDictData.value(forKey: "City")!)", with: "")
            
        }
        if tempArrayOfKeys.contains("ZIP") {
            
            strZipCode = "\(tempDictData.value(forKey: "ZIP")!)"
            strReplacedString = strReplacedString.replacingOccurrences(of: "\(tempDictData.value(forKey: "ZIP")!)", with: "")
            
        }
        if tempArrayOfKeys.contains("State") {
            
            let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(tempDictData.value(forKey: "State")!)")
            
            if dictStateDataTemp.count > 0 {
                
                strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                //btnState.setTitle("\(dictStateDataTemp.value(forKey: "Name")!)", for: .normal)
                
            }else{
                
                strStateId = ""
                //btnState.setTitle("\(tempDictData.value(forKey: "State")!)", for: .normal)
                
            }
            
            strReplacedString = strReplacedString.replacingOccurrences(of: "\(tempDictData.value(forKey: "State")!)", with: "")
            
        }
        if tempArrayOfKeys.contains("Street") {
            
            strAddressLine1 = "\(tempDictData.value(forKey: "Street")!)"
            strReplacedString = strReplacedString.replacingOccurrences(of: "\(tempDictData.value(forKey: "Street")!)", with: "")
            
        }
        
        /*strReplacedString = strReplacedString.replacingOccurrences(of: "United States", with: "")
         strReplacedString = strReplacedString.replacingOccurrences(of: "Phone", with: "")
         strReplacedString = strReplacedString.replacingOccurrences(of: "Fax", with: "")
         strReplacedString = strReplacedString.replacingOccurrences(of: "\n", with: "")
         
         let name =  strReplacedString
         let nameFormatter = PersonNameComponentsFormatter()
         if let nameComps  = nameFormatter.personNameComponents(from: name), let firstName = nameComps.givenName {
         
         let middleName = nameComps.middleName
         let lastName = nameComps.familyName
         
         if firstName.count > 0 {
         
         txtfldFirstName.text = firstName
         
         }
         if middleName!.count > 0 {
         
         txtfldMiddleName.text = middleName
         
         }
         if lastName!.count > 0 {
         
         txtfldLastName.text = lastName
         
         }
         
         }*/
        
    }
    
    func checkIfContactPermissionAllowed(){
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            // show contacts list to Add
            contactPickerView()
            
        case .denied:
            
            showSettingsAlert()
            
        case .restricted, .notDetermined:
            
            CNContactStore().requestAccess(for: .contacts) { (access, error) in
                print("Access: \(access)")
                
                if access == true {
                    
                    self.contactPickerView()
                    
                }
                
            }
            
        }
        
    }
    
    private func showSettingsAlert() {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            
        })
        present(alert, animated: true)
    }
    
    func contactPickerView(){
        
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self// as? CNContactPickerDelegate
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func convertFetchedContactsToArray(contact : [CNContact]) {
        
        strGlobalImageName = ""
        
        let multipleContacts = NSMutableArray()
        
        for item in contact {
            
            //print(item)
            
            // user name
            let userName:String = item.givenName
            let userMiddleName:String = item.middleName
            let userLastName:String = item.familyName
            let userJobTitle:String = item.jobTitle
            
            // user phone number
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = item.phoneNumbers
            
            var primaryPhoneNumberStr = String()
            var secondaryPhoneNumberStr = String()
            var cellPhoneNumberStr = String()
            
            for k in 0 ..< userPhoneNumbers.count {
                
                if k==0 {
                    
                    let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
                    primaryPhoneNumberStr = firstPhoneNumber.stringValue
                    
                    let cleanPhoneNumber = primaryPhoneNumberStr.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                    
                    if cleanPhoneNumber.count >= 10 {
                        
                        let last10 = cleanPhoneNumber.suffix(10)
                        
                        primaryPhoneNumberStr = String(last10)
                        
                    }else{
                        
                        primaryPhoneNumberStr = String(cleanPhoneNumber)
                        
                    }
    
                } else if k==1 {
                    
                    let secondPhoneNumber:CNPhoneNumber = userPhoneNumbers[1].value
                    secondaryPhoneNumberStr = secondPhoneNumber.stringValue
                                        
                    let cleanPhoneNumber = secondaryPhoneNumberStr.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                    
                    if cleanPhoneNumber.count >= 10 {
                        
                        let last10 = cleanPhoneNumber.suffix(10)
                        
                        secondaryPhoneNumberStr = String(last10)
                        
                    }else{
                        
                        secondaryPhoneNumberStr = String(cleanPhoneNumber)
                        
                    }
                    
                } else if k==2 {
                    
                    let thirdPhoneNumber:CNPhoneNumber = userPhoneNumbers[2].value
                    cellPhoneNumberStr = thirdPhoneNumber.stringValue
                                        
                    let cleanPhoneNumber = cellPhoneNumberStr.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                    
                    if cleanPhoneNumber.count >= 10 {
                        
                        let last10 = cleanPhoneNumber.suffix(10)
                        
                        cellPhoneNumberStr = String(last10)
                        
                    }else{
                        
                        cellPhoneNumberStr = String(cleanPhoneNumber)
                        
                    }
                    
                }
                
            }
            
            // user emailAddresses
            let userEmailAddress:[CNLabeledValue<NSString>] = item.emailAddresses
            
            var primaryEmailStr = NSString()
            var secondaryEmailStr = NSString()
            
            for k in 0 ..< userEmailAddress.count {
                
                if k==0 {
                    
                    primaryEmailStr = userEmailAddress[0].value
                    
                } else if k==1 {
                    
                    secondaryEmailStr = userEmailAddress[1].value
                    
                }
                
            }
            
            // user emailAddresses
            let userPostalAddress:[CNLabeledValue<CNPostalAddress>] = item.postalAddresses
            
            var address1Str = NSString()
            var address2Str = NSString()
            var cityNameStr = NSString()
            var stateIdStr = NSString()
            var zipCodeStr = NSString()
            var countryIdStr = NSString()
            countryIdStr = "1"
            address2Str = ""
            
            for k in 0 ..< userPostalAddress.count {
                
                if k==0 {
                    
                    let postalAddress1:CNPostalAddress = userPostalAddress[0].value
                    address1Str = postalAddress1.street as NSString
                    cityNameStr = postalAddress1.city as NSString
                    stateIdStr = postalAddress1.state as NSString
                    zipCodeStr = postalAddress1.postalCode as NSString
                    
                }
                
            }
            
            var keys = NSArray()
            var values = NSArray()
            keys = ["CompanyKey",
                    "Title",
                    "JobTitle",
                    "FirstName",
                    "MiddleName",
                    "LastName",
                    "PrimaryPhone",
                    "PrimaryPhoneExt",
                    "SecondaryPhone",
                    "SecondaryPhoneExt",
                    "CellPhone1",
                    "PrimaryEmail",
                    "SecondaryEmail",
                    "SourceId",
                    "Owner",
                    "Address1",
                    "Address2",
                    "CityName",
                    "StateId",
                    "ZipCode",
                    "CountryId",
                    "CrmCompanyId",
                    "EmployeeId"]
            
            values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
                "",
                userJobTitle,
                userName,
                userMiddleName,
                userLastName,
                primaryPhoneNumberStr,
                "",
                secondaryPhoneNumberStr,
                "",
                cellPhoneNumberStr,
                primaryEmailStr,
                secondaryEmailStr,
                "",
                "",
                address1Str,
                address2Str,
                cityNameStr,
                stateIdStr,
                zipCodeStr,
                countryIdStr,
                "",
                "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"]
            
            let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
            
            multipleContacts.add(dictToSend)
            
        }
        
        if multipleContacts.count > 0 {
            
            callApiToAddContacts(arrOfContacts: multipleContacts)
            
        } else {
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

            }
            
        }
        
    }
    
    func convertEnteredDetailsToArray() {
        
        var crmContactIdToUpdate = ""
        
        if strFrom == "Update" {

            crmContactIdToUpdate = "\(dictContactBasicInfo.value(forKey: "CrmContactId")!)"
            
        }
        
        var imageNameToBeSent = ""
        
        if strGlobalImageName.count > 0 {
            
            strGlobalImageName = "/" + strGlobalImageName
            
            imageNameToBeSent = strGlobalImageName
            
        } else {
            
            loader = loader_Show(controller: self, strMessage: "please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)

        }
        
        if strFrom == "Update" && strGlobalImageName.count == 0 {

            if strGlobalImageNameIfAlreadyPresent.count > 0 {
                
                strGlobalImageNameIfAlreadyPresent = "/" + strGlobalImageNameIfAlreadyPresent
                
                imageNameToBeSent = strGlobalImageNameIfAlreadyPresent

            }
            
        }
                
        let multipleContacts = NSMutableArray()
        
        var keys = NSArray()
        var values = NSArray()
        keys = ["CompanyKey",
                "Title",
                "JobTitle",
                "FirstName",
                "MiddleName",
                "LastName",
                "PrimaryPhone",
                "PrimaryPhoneExt",
                "SecondaryPhone",
                "SecondaryPhoneExt",
                "CellPhone1",
                "PrimaryEmail",
                "SecondaryEmail",
                "SourceId",
                "Owner",
                "Address1",
                "Address2",
                "CityName",
                "StateId",
                "ZipCode",
                "CountryId",
                "CrmCompanyId",
                "EmployeeId",
                "CrmContactId",
                "ProfileImage"]
        
        values = ["\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)",
            (txtfldSalutation.text?.count)! > 0 ? txtfldSalutation.text! : "",
            (txtfldJobTitle.text?.count)! > 0 ? txtfldJobTitle.text! : "",
            (txtfldFirstName.text?.count)! > 0 ? txtfldFirstName.text! : "",
            (txtfldMiddleName.text?.count)! > 0 ? txtfldMiddleName.text! : "",
            (txtfldLastName.text?.count)! > 0 ? txtfldLastName.text! : "",
            (txtfldPrimaryPhone.text?.count)! > 0 ? getOnlyNumberFromString(number: txtfldPrimaryPhone.text!) : "",
            (txtfldPrimaryPhoneExt.text?.count)! > 0 ? txtfldPrimaryPhoneExt.text! : "",
            (txtfldSecondaryPhone.text?.count)! > 0 ? getOnlyNumberFromString(number: txtfldSecondaryPhone.text!) : "",
            (txtfldSecondaryPhoneExt.text?.count)! > 0 ? txtfldSecondaryPhoneExt.text! : "",
            (txtfldCellPhone.text?.count)! > 0 ? getOnlyNumberFromString(number: txtfldCellPhone.text!) : "",
            (txtfldEmail.text?.count)! > 0 ? txtfldEmail.text! : "",
            (txtfldSecondaryEmail.text?.count)! > 0 ? txtfldSecondaryEmail.text! : "",
            strSourceId,
            strOwnerId,
            (strAddressLine1.count) > 0 ? strAddressLine1 : "",
            (strAddressLine2.count) > 0 ? strAddressLine2 : "",
            (strCityName.count) > 0 ? strCityName : "",
            strStateId,
            (strZipCode.count) > 0 ? strZipCode : "",
            "1",
            strCrmCompanyIdToAssociateCompanyToContact,
            "\(dictLoginData.value(forKeyPath: "EmployeeId")!)",
            crmContactIdToUpdate,
            imageNameToBeSent]
        
        let dictToSend = NSDictionary.init(objects: values as! [Any], forKeys: keys as! [NSCopying])
        
        multipleContacts.add(dictToSend)
        
        if multipleContacts.count > 0 {
            
            callApiToAddContacts(arrOfContacts: multipleContacts)
            
        } else {
            
            self.loader.dismiss(animated: false) {

                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

            }
            
        }
        
    }
    
    func fillContactDetailToUpdate() {
        
        if dictContactBasicInfo.count == 0 {
            
            
            
        } else {
            
            dictContactBasicInfo = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictContactBasicInfo as! [AnyHashable : Any]))! as NSDictionary

            txtfldFirstName.text = "\(dictContactBasicInfo.value(forKey: "FirstName")!)"
            txtfldLastName.text = "\(dictContactBasicInfo.value(forKey: "LastName")!)"
            txtfldAddress.text = Global().strCombinedAddress(dictContactBasicInfo as? [AnyHashable : Any])
            
            let formattedCellPhoneNo = formattedNumber(number: "\(dictContactBasicInfo.value(forKey: "CellPhone1")!)")
            
            txtfldCellPhone.text = formattedNumber(number: formattedCellPhoneNo)
            txtfldEmail.text = "\(dictContactBasicInfo.value(forKey: "PrimaryEmail")!)"
            txtfldJobTitle.text = "\(dictContactBasicInfo.value(forKey: "JobTitle")!)"
            txtfldAssociateCompany.text = "\(dictContactBasicInfo.value(forKey: "FirstName")!)"
            strCrmCompanyIdToAssociateCompanyToContact = "\(dictContactBasicInfo.value(forKey: "FirstName")!)"
            strSourceId = "\(dictContactBasicInfo.value(forKey: "SourceId")!)"
            
            let dictOfSourceSelectedd = fetchSourceFromMasterViaSourceId(sourceIdToFetch: strSourceId)
            
            if dictOfSourceSelectedd is NSDictionary {
                
                if dictOfSourceSelectedd.count > 0 {
                    
                    arrSelectedSource = NSMutableArray()
                    arrSelectedSource.add(dictOfSourceSelectedd)
                    
                }
                
            }
            
            txtfldSourceName.text = getSourceNameFromId(sourceId: "\(dictContactBasicInfo.value(forKey: "SourceId")!)")
            txtfldOwnerName.text = Global().getEmployeeName(viaEmployeeID: "\(dictContactBasicInfo.value(forKey: "Owner")!)")//Global().getEmployeeName(viaId: "\(dictContactBasicInfo.value(forKey: "Owner")!)")
            strOwnerId = "\(dictContactBasicInfo.value(forKey: "Owner")!)"
            txtfldSalutation.text = "\(dictContactBasicInfo.value(forKey: "Title")!)"
            txtfldMiddleName.text = "\(dictContactBasicInfo.value(forKey: "MiddleName")!)"
                        
            let formattedPrimaryPhoneNo = formattedNumber(number: "\(dictContactBasicInfo.value(forKey: "PrimaryPhone")!)")

            txtfldPrimaryPhone.text = formattedPrimaryPhoneNo
            txtfldPrimaryPhoneExt.text = "\(dictContactBasicInfo.value(forKey: "PrimaryPhoneExt")!)"
            
            let formattedSecondaryPhoneNo = formattedNumber(number: "\(dictContactBasicInfo.value(forKey: "SecondaryPhone")!)")

            txtfldSecondaryPhone.text = formattedSecondaryPhoneNo
            txtfldSecondaryPhoneExt.text = "\(dictContactBasicInfo.value(forKey: "SecondaryPhoneExt")!)"
            txtfldSecondaryEmail.text = "\(dictContactBasicInfo.value(forKey: "SecondaryEmail")!)"
            
        }
        
        downloadProfileImage()
        
    }
    
    // MARK: -----------------------------APi Calling Functions---------------------------

    func associateContact(CrmContactId : String)
    {
        
        if !isInternetAvailable()
        {
            
            self.dispatchGroup.leave()

            showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)

        }
        else
        {

            var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAssociateContact + CrmContactId + "&RefType=" + strRefType + "&RefId=" + strRefId + "&EmployeeId=" + "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
            
            strURL = strURL.trimmingCharacters(in: .whitespaces)
            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "AssociateContactCompany") { (Response, Status) in
                
                self.dispatchGroup.leave()
                
                self.loader.dismiss(animated: false) {

                    if(Status)
                    {
                        
                        if Response["data"] is String {
                            
                            guard let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)  else {
                                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                            //let dictResponse = convertToDictionaryyy(text: Response["data"] as! String)
                            if dictResponse.count > 0{
                                
                                let strOperationResult = dictResponse["OperationResult"] as? Bool ?? false
                                var strMessage = dictResponse["Message"] as? String ?? ""
                                
                            
                            if strOperationResult {
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Contact added and associated successfully", preferredStyle: .alert)
                                                  
                                                  let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                                     
                                                    UserDefaults.standard.set(true, forKey: "DismissContacts_AssociateContactList")
                                                    
                                                    self.navigationController?.popViewController(animated: false)
                                                  })
                                                  
                                                  alertCOntroller.addAction(alertAction)
                                                  self.present(alertCOntroller, animated: true, completion: nil)
                                
                            } else {
                                
                                if strMessage.count > 0 {
                                    
                                    
                                    
                                } else {
                                    
                                    strMessage = alertSomeError
                                    
                                }
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMessage, viewcontrol: self)

                                
                            }

                            }
                            else {
                               
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                           }
                        } else {
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func finalDismissView(response : NSDictionary) {
        
        if self.strFromWhere == "DirectAssociate" || self.strFromWhere == "DirectAssociateFromCompany"{
            
            if (response.value(forKey: "Response")!) is NSArray {
                
                let arrOfContact = (response.value(forKey: "Response")!) as! NSArray

                if arrOfContact.count > 0 {
                    
                    if arrOfContact[0] is NSDictionary {
                        
                        let dictDataOfContacts = arrOfContact[0] as! NSDictionary
                        
                        self.associateContact(CrmContactId: "\(dictDataOfContacts.value(forKey: "CrmContactId")!)")
                        
                    }
                    
                }
                
            }
                                    
        }else{
            
            self.loader.dismiss(animated: false) {

                if self.strFromAssociate == "true" {

                    // Call API To Associate contact CrmContactId
                    
                    if (response.value(forKey: "Response")!) is NSArray {
                        
                        let arrOfContact = (response.value(forKey: "Response")!) as! NSArray

                        if arrOfContact.count > 0 {
                            
                            if arrOfContact[0] is NSDictionary {
                                
                                let dictDataOfContacts = arrOfContact[0] as! NSDictionary
                                
                                let alertCOntroller = UIAlertController(title: Info, message: "Contact Added Successfully", preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                    
                                    UserDefaults.standard.set(true, forKey: "DismissContacts_AssociateContactList")
                                    
                                    UserDefaults.standard.set(dictDataOfContacts, forKey: "ContactData")

                                    self.navigationController?.popViewController(animated: false)
                                    
                                })
                                
                                alertCOntroller.addAction(alertAction)
                                self.present(alertCOntroller, animated: true, completion: nil)
                                
                            }else{
                                
                                showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                                
                            }
                            
                        }else{
                            
                            showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            
                        }
                        
                    }else{
                        
                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        
                    }

                }else{
                    
                    UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")
                    
                    UserDefaults.standard.set(true, forKey: "RefreshContacts_AssociateContactList")
                    
                    if self.strFrom == "Update" {

                        let alertCOntroller = UIAlertController(title: Info, message: "Contact Updated Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                                        
                            UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")
                            
                            self.navigationController?.popViewController(animated: false)
                            
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }else{
                        
                        let alertCOntroller = UIAlertController(title: Info, message: "Contact Added Successfully", preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            UserDefaults.standard.set(true, forKey: "RefreshContacts_ContactList")

                            self.navigationController?.popViewController(animated: false)
                        })
                        
                        alertCOntroller.addAction(alertAction)
                        self.present(alertCOntroller, animated: true, completion: nil)
                        
                    }
                    
                }


            }
            
        }
        
    }
    
    func callApiToAddContacts(arrOfContacts : NSMutableArray) {
        
        if strGlobalImageName.count > 0 {
            
            
            
        } else {
            
            self.dispatchGroup.enter()

        }
        
        if strFromAssociate == "true" {

            self.dispatchGroup.enter()

        }
        
        var jsonString = String()
        
        if(JSONSerialization.isValidJSONObject(arrOfContacts) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: arrOfContacts, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlAddCrmContacts
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let strTypee = "CrmContactsNew"
        
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            
            if self.strGlobalImageName.count > 0 {
                
                
                
            } else {
                
                self.dispatchGroup.leave()

            }
            
            if(success)
            {
                
                let strResponse = "\((response as NSDictionary?)!.value(forKey: "ReturnMsg")!)"
                
                if strResponse == "true" {
                    
                    self.finalDismissView(response: (response as NSDictionary?)!)
                    
                } else {
                    
                    self.loader.dismiss(animated: false) {

                        showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                    }
                }
                
            }else {
                
                self.loader.dismiss(animated: false) {

                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)

                }
            }
            
        }
        
    }
    
    fileprivate func callAPIToGetContactBasicInfo(){

        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetContactBasicInfo + "\(dictContact.value(forKey: "CrmContactId")!)"
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
            self.loader.dismiss(animated: false) {

                if(status)
                {
                    
                        let arrKeys = response.allKeys as NSArray
                        
                        if arrKeys.contains("data") {
                            
                            print("Yes Data is there ")
                            if((response.value(forKey: "data") as! NSDictionary).count > 0)
                            {
                                self.dictContactBasicInfo = (response.value(forKey: "data") as! NSDictionary)
                                self.fillContactDetailToUpdate()
                            }
                        }
                
                }

            }

        }
    }
    
    fileprivate func callAPIToUploadContactProfileImage(){

        self.dispatchGroup.enter()

        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlUploadContactProfileImage
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.callAPIWithImage(parameter: NSDictionary(), url: strURL, image: imgViewContactProfile.image!, fileName: strGlobalImageName, withName: strGlobalImageName) { (response, status) in
            
            self.convertEnteredDetailsToArray()

            self.dispatchGroup.leave()

            if(status == "success")
            {
                print(response)
            }
            else
            {
                // Failed to uplaod image
            }
            
        }
    }
    
}
extension AddContactVC_CRMContactNew_iPhone: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 601)
        {
            
            if dictData["Name"] as? String  == strSelectString {
                
                txtfldSourceName.text = ""
                strSourceId = ""
                
            }else{
                
                txtfldSourceName.text = "\(dictData.value(forKey: "Name")!)"
                strSourceId = "\(dictData.value(forKey: "SourceId")!)"
                
            }
            
        }
        if(tag == 602)
        {
            
            if dictData["FullName"] as? String  == strSelectString {
                
                txtfldOwnerName.text = ""
                strOwnerId = ""
                
            }else{
                
                txtfldOwnerName.text = "\(dictData.value(forKey: "FullName")!)"
                strOwnerId = "\(dictData.value(forKey: "EmployeeId")!)"
                
            }
            
        }
        
    }
}


extension AddContactVC_CRMContactNew_iPhone: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        
        self.view.endEditing(true)
        
        if(tag == 302)
        {
            //btnDueDate.setTitle(strDate, for: .normal)
        }
        if(tag == 303)
        {
            // btnDueTime.setTitle(strDate, for: .normal)
        }
        if(tag == 304)
        {
            // btnReminderDate.setTitle(strDate, for: .normal)
        }
        if(tag == 305)
        {
            // btnReminderTime.setTitle(strDate, for: .normal)
        }
    }
}

extension AddContactVC_CRMContactNew_iPhone:CNContactPickerDelegate
{
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: [CNContact]) {
        // You can fetch selected name and number in the following way
        
        self.dismiss(animated: false, completion: {
            
            if contact.count > 0 {
                
                self.loader = loader_Show(controller: self, strMessage: "please wait...", title: "", style: .alert)
                self.present(self.loader, animated: false, completion: nil)
                
                self.convertFetchedContactsToArray(contact: contact)
                
            }
            
        })
        
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
    
}
extension AddContactVC_CRMContactNew_iPhone:VNDocumentCameraViewControllerDelegate
{
    
    @available(iOS 13.0, *)
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        
        let image = scan.imageOfPage(at: 0)
        let handler = VNImageRequestHandler(cgImage: image.cgImage!, options: [:])
        do {
            try handler.perform([textRecognitionRequest])
        } catch {
            print(error)
        }
        controller.dismiss(animated: true)
        
    }
    
}
extension String {
    /** Get email addresses in a string, discard any other content. */
    func emailAddresses() -> [String] {
        var addresses = [String]()
        if let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) {
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            for match in matches {
                if let matchURL = match.url,
                    let matchURLComponents = URLComponents(url: matchURL, resolvingAgainstBaseURL: false),
                    matchURLComponents.scheme == "mailto"
                {
                    let address = matchURLComponents.path
                    addresses.append(String(address))
                }
            }
        }
        return addresses
    }
    
    func extractURLs() -> [URL] {
        var urls : [URL] = []
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            detector.enumerateMatches(in: self, options: [], range: NSMakeRange(0, self.count), using: { (result, _, _) in
                if let match = result, let url = match.url {
                    
                    urls.append(url)
                    
                }
            })
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return urls
        
    }
    
    func phoneNo() -> [String] {
        var addresses = [String]()
        if let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue) {
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            for match in matches {
                
                if match.resultType == .phoneNumber, let number = match.phoneNumber {
                    print(number)
                    addresses.append(String(number))
                    
                }
                
            }
        }
        return addresses
    }
}


// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension AddContactVC_CRMContactNew_iPhone : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            
        }
        if(textField == txtfldSecondaryEmail){

            isSecondaryEmail = true
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField == txtfldAddress){

            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
            
        }
        
        if(textField == txtfldSecondaryEmail){

            isSecondaryEmail = false
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        if textField == txtfldAssociateCompany  {
            
            return false
            
        } else if textField == txtfldSourceName  {
            
            return false
            
        } else if textField == txtfldOwnerName  {
            
            return false
            
        } else if textField == txtfldCellPhone  {
            
            txtfldCellPhone.hideError()

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldCellPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldCellPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        } else if textField == txtfldPrimaryPhone  {
            
            txtfldPrimaryPhone.hideError()

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldPrimaryPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldPrimaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        } else if textField == txtfldPrimaryPhoneExt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 10)
            
        } else if textField == txtfldSecondaryPhone  {
            
            txtfldSecondaryPhone.hideError()

            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            var strTextt = txtfldSecondaryPhone.text!
            strTextt = String(strTextt.dropLast())
            txtfldSecondaryPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 20)
            
        } else if textField == txtfldSecondaryPhoneExt  {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 10)
            
        } else if (textField == txtfldFirstName) {
            
            txtfldFirstName.hideError()
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 50)
            
        } else if (textField == txtfldLastName){
            
            txtfldLastName.hideError()
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 50)
            
        } else if (textField == txtfldMiddleName){
            
            txtfldMiddleName.hideError()
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 50)
            
        } else if (textField == txtfldJobTitle){
            
            txtfldJobTitle.hideError()
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        } else if (textField == txtfldSalutation){
            
            txtfldSalutation.hideError()
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        } else if(textField == txtfldAddress){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()

                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()

                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            
            
        }
        
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtfldAddress){
            let txtAfterUpdate:NSString = txtfldAddress.text! as NSString
            if(txtAfterUpdate == ""){
                places = [];
                return true
            }
            let parameters = getParameters(for: txtAfterUpdate as String)
            
//            self.getPlaces(with: parameters) {
//                self.places = $0
//            }
            return true
        }
        return true
    }
    
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
    
}
extension AddContactVC_CRMContactNew_iPhone {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }else{
                    for item in self.scrollView.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.frame = CGRect(x: self.txtfldAddress.frame.origin.x, y: self.txtfldAddress.frame.maxY+5, width: self.scrollView.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : (DeviceType.IS_IPAD ? 380 : 250))
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
        }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}






// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddContactVC_CRMContactNew_iPhone : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtfldAddress.text = value.formattedAddress
            self.txtfldAddress.text = addressFormattedByGoogle(value: value)
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()

        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
// MARK: -
// MARK: -ReminderCell
class addressCell: UITableViewCell {
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension AddContactVC_CRMContactNew_iPhone : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        //let imageData = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!.jpegData(compressionQuality:1.0)
        
        self.imgViewContactProfile.isHidden = false
        self.lblProfileImageName.isHidden = true
        
        let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        let imageResized = Global().resizeImageGloballl(img)
        
        self.imgViewContactProfile.image = imageResized
                        
        self.strGlobalImageName = "Contact" + "\(getUniqueValueForId())" + ".png"
        
    }
    
}

// MARK: -
// MARK: - -----------------------Source Selction Delgates

extension AddContactVC_CRMContactNew_iPhone : SelectGlobalVCDelegate{
    
    func getDataSelectGlobalVC(dictData: NSMutableDictionary, tag: Int) {
        
        if tag == 101 {
            
            if(dictData.count == 0){
                
                txtfldSourceName.text = ""
                strSourceId = ""
                arrSelectedSource = NSMutableArray()

            }else{
                
                txtfldSourceName.text = "\(dictData.value(forKey: "Name")!)"
                strSourceId = "\(dictData.value(forKey: "SourceId")!)"
                
                arrSelectedSource = NSMutableArray()
                arrSelectedSource.add(dictData)

            }
            
        }
        
    }

}

// MARK: -
// MARK: - -----------------------Add Address Delgates

extension AddContactVC_CRMContactNew_iPhone : AddNewAddressiPhoneDelegate{

    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {

        print(dictData)
        
        self.txtfldAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])


    }

}
