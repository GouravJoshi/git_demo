//
//  CompanyVC_CRMContactNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 31/01/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
class CellCompany:UITableViewCell{
    
    @IBOutlet weak var imgviewProfile: UIImageView!
    
    
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEdit: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgviewProfile.layer.borderWidth = 1.0
        imgviewProfile.layer.masksToBounds = false
        imgviewProfile.layer.borderColor = UIColor.white.cgColor
        imgviewProfile.layer.cornerRadius = imgviewProfile.frame.size.width / 2
        imgviewProfile.clipsToBounds = true
        
        lblImageName.layer.borderWidth = 1.0
        lblImageName.layer.masksToBounds = false
        lblImageName.layer.borderColor = UIColor.white.cgColor
        lblImageName.layer.cornerRadius = imgviewProfile.frame.size.width / 2
        lblImageName.clipsToBounds = true
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

class CompanyVC_CRMContactNew_iPhone: UIViewController, UISearchBarDelegate {
    
    
    // MARK: --------------------------- Outlets  ---------------------------
    
    @IBOutlet weak var searchBarCompany: UISearchBar!
    @IBOutlet weak var tblviewCompany: UITableView!
    @IBOutlet weak var constHghtSearchBar: NSLayoutConstraint!
    
    @IBOutlet weak var viewTopButtom: UIView!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    @IBOutlet weak var headerView: TopView!

    // MARK: --------------------------- Varibale  ---------------------------
    

    var refresher = UIRefreshControl()
    var dictLoginData = NSDictionary()
    var lbl = UILabel()
    var loader = UIAlertController()
    
    var arrOfSection = NSMutableArray()
    
    var arrOfTodayDB = NSArray()
    var arrOfYesterdayDB = NSArray()
    var arrOfThisMonthDB = NSArray()
    var arrOfAllOtherDB = NSArray()
    
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    
    var strSearchText = ""
    var companyLastSyncedDateTime = ""
    var companyCurrentSyncedDateTime = ""


    // MARK: --------------------------- View's life cycle  ---------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            arrOfSection.add("  Created Today")
            arrOfSection.add("  Created Yesterday")
            arrOfSection.add("  Created This Month")
            arrOfSection.add("  All")
            
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                lblScheduleCount.text = "0"
                lbltaskCount.layer.cornerRadius = 18.0
                lbltaskCount.backgroundColor = UIColor.red
                lblScheduleCount.layer.cornerRadius = 18.0
                lblScheduleCount.backgroundColor = UIColor.red
                lbltaskCount.layer.masksToBounds = true
                lblScheduleCount.layer.masksToBounds = true
                
            }
            if #available(iOS 13.0, *) {
                searchBarCompany.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
            } else {
                // Fallback on earlier versions
            }
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            tblviewCompany.tableFooterView = UIView()
            tblviewCompany.estimatedRowHeight = 75.0
            setupViews()
            
            self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
            self.tblviewCompany!.addSubview(refresher)
            
            // check if Company added to refresh view
            
            //setDefaultValuesForCompanyFilter()
            //setDefaultValuesForTimeLineFilter()
            
            UserDefaults.standard.set(true, forKey: "RefreshCompany_CompanyList")
            
            //createBadgeView()
        }
      

    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setTopMenuOption()
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setTopMenuOption()
                self.setFooterMenuOption()
            })
            
            if nsud.bool(forKey: "RefreshCompany_CompanyList") {
                searchBarCompany.text = ""
                if (isInternetAvailable()){
                    
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        UserDefaults.standard.set(false, forKey: "RefreshCompany_CompanyList")
                        
                        self.callApiToGetCompanies()

                    })
                    
                }else{
                    
                    self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")
                    
                    //self.noDataLbl()
                    //showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)
                    
                }
                
            }        }
      
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //super.viewWillDisappear(animated)
        //searchBarCompany.text = ""
    }
    
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Account", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        footorView.onClickHomeButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToDashboard()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToAppointment()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            self.view.endEditing(true)
            self.goToWebLead()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            self.view.endEditing(true)
          //  self.GotoAccountViewController()
        }
        
       
    }
    
    func setTopMenuOption() {
        for view in self.view.subviews {
            if(view is HeaderView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Company", forKey: "DPS_TopMenuOptionTitle")
        nsud.synchronize()
        var headerViewtop = HeaderView()
        headerViewtop = HeaderView.init(frame: CGRect(x: 0, y: 0, width: self.headerView.frame.size.width, height:self.headerView.frame.size.height))
        headerViewtop.searchBarTask.delegate = self
        if #available(iOS 13.0, *) {
            headerViewtop.searchBarTask.searchTextField.text = self.strSearchText
        } else {
            headerViewtop.searchBarTask.text = self.strSearchText
        }
        self.headerView.addSubview(headerViewtop)
        
        headerViewtop.onClickFilterButton = {() -> Void in
            self.view.endEditing(true)
            self.gotoCompanyFilterVC()
        }
        headerViewtop.onClickAddButton = {() -> Void in
            self.view.endEditing(true)
            self.GotoAddCompany()
        }
        
        headerViewtop.onClickVoiceRecognizationButton = { () -> Void in
            
            self.view.endEditing(true)
            if self.arrOfSection.count != 0 {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.delegate = self
                vc.modalTransitionStyle = .coverVertical
                self.present(vc, animated: true, completion: {})
            }
        }
       
        headerViewtop.onClickTopMenuButton = {(str) -> Void in
            print(str)
            switch str {
            case "Map":
            self.goToNearBy()
                break
            case "Lead":
                self.goToLeadOpportunity()
                break
            case "Opportunity":
                self.GotoOpportunityViewContoller()
                break
            case "Tasks":
                self.goToTaskActivity()
                break
            case "Activity":
                self.GotoActivityViewContoller()
                break
            case "View Schedule":
                self.GotoViewScheduleViewController()
                break
            case "Signed Agreements":
                self.goToSignedAgreement()
                break
            case "Company":
               
                break
            case "Contacts":
                self.GotoContactViewController()
                break
            default:
                break
            }
           
        }

    }

    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoAddCompany()  {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoOpportunityViewContoller() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
  
       self.navigationController?.pushViewController(controller, animated: false)
        
    }
    

    func GotoActivityViewContoller() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
     //   vc.strFromVC = strFromVC
        //nsud.setValue(nil, forKey: "dictFilterActivity")
        //nsud.synchronize()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func GotoContactViewController()  {
        
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        
        self.navigationController?.pushViewController(testController, animated: false)
        
        /*var chk = Bool()
        chk = false
        
        for controller in self.navigationController!.viewControllers as NSArray
        {
            if controller is ContactVC_CRMContactNew_iPhone
            {
                chk = true
                self.navigationController!.popToViewController(controller as! UIViewController, animated: false)
                break
            }
        }
        if !chk
        {
            let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
            
            self.navigationController?.pushViewController(testController, animated: false)
        }*/
    }
    
    // MARK: --------------------------- Local Funcitons ---------------------------
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                            let badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func setupViews()
    {
        
        viewTopButtom.backgroundColor = UIColor.white
        btnCompany.layer.cornerRadius = btnCompany.frame.size.height/2
        btnCompany.layer.borderWidth = 0
        
        viewTopButtom.layer.cornerRadius = viewTopButtom.frame.size.height/2
        viewTopButtom.layer.borderWidth = 0
        
        btnContact.layer.cornerRadius = btnContact.frame.size.height/2
        btnContact.layer.borderWidth = 0
        
        viewTopButtom.bringSubviewToFront(btnCompany)
        
        if let txfSearchField = searchBarCompany.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .white
            txfSearchField.layer.cornerRadius = txfSearchField.frame.size.height/2
            txfSearchField.layer.masksToBounds = true
        }
        searchBarCompany.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBarCompany.layer.borderWidth = 1
        searchBarCompany.layer.opacity = 1.0
        
    }
    
    func goToCompanyDetails(dictCompanyData : NSManagedObject){
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyDetailsVC_CRMContactNew_iPhone") as! CompanyDetailsVC_CRMContactNew_iPhone
        controller.strCRMCompanyIdGlobal = "\(dictCompanyData.value(forKey: "crmCompanyId")!)"
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToTaskActivity()
    {
        
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        
        //testController.strFromVC = strFromVC
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    func goToWebLead()
    {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToDashboard(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToAppointment(){
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
    }
    
    func goToNearBy(){
        /*
         if(DeviceType.IS_IPAD){
         let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
         self.navigationController?.pushViewController(vc!, animated: false)
         }else{
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
         self.navigationController?.pushViewController(vc!, animated: false)
         }*/
        
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
        
    }
    
    func goToSignedAgreement(){
        
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
        
    }
    
    func goToLeadOpportunity(){
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func noDataLbl() {
        
        self.tblviewCompany.isHidden = true
        
        lbl.removeFromSuperview()
        
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        lbl.center = self.view.center
        lbl.text = "No Data Found"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 20)
        self.view.addSubview(lbl)
        
    }
    
    @objc func RefreshloadData() {
        
        if (isInternetAvailable()){
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.callApiToGetCompanies()
                
            })
            
        }else{
            
            self.noDataLbl()
            
        }
        
    }
    func gotoCompanyFilterVC()
    {
        let mainStoryboard = UIStoryboard(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: nil)
        
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "CompanyFilterVC") as! CompanyFilterVC
        self.navigationController?.present(controller, animated: false, completion: nil)
    }
        
    // MARK: --------------------------- Core Data Functions ---------------------------

    func logicToFilterAndSortInDB(strTypee : String, strSearchText : String) {
    
        var yesFilterView = false
        
        var dictCrmCompanyFilter = NSDictionary()

        if nsud.value(forKey: "CrmCompanyFilter") == nil {
            
            // Filter is Blank No Need To Filter
            yesFilterView = false
            
        }else{
            
            // Filter is Applied We have to apply filter loccally also
            dictCrmCompanyFilter = nsud.value(forKey: "CrmCompanyFilter") as! NSDictionary
            
            let arrOfAddressPropertyIds = dictCrmCompanyFilter.value(forKey: "AddressPropertyTypeId") as! NSArray
                            
            if "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")".count == 0 && "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")".count == 0 && arrOfAddressPropertyIds.count == 0 {
                
                yesFilterView = false
                
            }else{
                
                yesFilterView = true

            }
            
        }
                
        let startGlobal = CFAbsoluteTimeGetCurrent()

        let sort = NSSortDescriptor(key: "name", ascending: true)
        
        // For Today Date Filter
        
        let dateTodayL = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDateEST(), "MM/dd/yyyy", "EST") as Date
        
        var andPredicate : [NSPredicate] = []
        var orPredicate : [NSPredicate] = []
        var yesProprtyType = false

        
        let subPredicate1 = NSPredicate(format: "createdDateNew == %@", dateTodayL as CVarArg)
        let subPredicate2 = NSPredicate(format: "companyKey == %@", Global().getCompanyKey())
        let subPredicate3 = NSPredicate(format: "userName == %@", Global().getUserName())
        let subPredicate4 = NSPredicate(format: "name CONTAINS %@ || addressPropertyName CONTAINS %@ || combinedAddress CONTAINS %@", strSearchText,strSearchText,strSearchText)
        let subPredicate5 = NSPredicate(format: "isActive == YES && isDelete == NO")
        let subPredicate6 = NSPredicate(format: "name CONTAINS[c] %@", "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")")
        let subPredicate7 = NSPredicate(format: "primaryEmail CONTAINS[c] %@", "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")")
        let subPredicate8 = NSPredicate(format: "primaryPhone CONTAINS[c] %@", "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")")
        let subPredicate9 = NSPredicate(format: "cellPhone CONTAINS[c] %@", "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")")
        let subPredicate10 = NSPredicate(format: "website CONTAINS[c] %@", "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")")

        andPredicate.append(subPredicate1)
        andPredicate.append(subPredicate2)
        andPredicate.append(subPredicate3)
        andPredicate.append(subPredicate5)

        if strTypee == "Search" {
            
            andPredicate.append(subPredicate4)
            
        }
        
        if yesFilterView {
            
            if "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")".count > 0 {
                
                andPredicate.append(subPredicate6)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")".count > 0 {
                
                andPredicate.append(subPredicate7)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")".count > 0 {
                
                andPredicate.append(subPredicate8)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")".count > 0 {
                
                andPredicate.append(subPredicate9)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")".count > 0 {
                
                andPredicate.append(subPredicate10)
                
            }
            
            let arrOfAddressPropertyIds = dictCrmCompanyFilter.value(forKey: "AddressPropertyTypeId") as! NSArray
            
            if arrOfAddressPropertyIds.count > 0 {
                
                for k in 0 ..< arrOfAddressPropertyIds.count {

                    let subPredicateLL = NSPredicate(format: "addressPropertyTypeId == %@", "\(arrOfAddressPropertyIds[k])")
                    
                    //andPredicate.append(subPredicateLL)
                    orPredicate.append(subPredicateLL)
                    yesProprtyType = true
                    
                }
                
            }
            
            if "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")".count > 0 && "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")".count > 0{
                
                let dateFromDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let dateToDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let subPredicateLL1 = NSPredicate(format: "createdDateNew >= %@ && createdDateNew <= %@", dateFromDate as CVarArg, dateToDate as CVarArg)
                
                andPredicate.append(subPredicateLL1)

            }

            
        }
        
        let andPredicateCompany = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)

        let orPredicateMerged = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicate)

        let andPredicateCompanyNew = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicate)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicateMerged, andPredicateCompanyNew])
        
        if yesProprtyType {
            
            arrOfTodayDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: predicate, sort: sort)

        }else{
            
            arrOfTodayDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: andPredicateCompany, sort: sort)

        }

        //  Fliter for YesterDay
        
        let dateYesterDayL = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDateEST(Global().getCurrentDateEST()), "MM/dd/yyyy", "EST") as Date
        
        var andPredicateY : [NSPredicate] = []
        var orPredicateY : [NSPredicate] = []
        yesProprtyType = false

        
        let subPredicate1Y = NSPredicate(format: "createdDateNew == %@", dateYesterDayL as CVarArg)
        let subPredicate2Y = NSPredicate(format: "companyKey == %@", Global().getCompanyKey())
        let subPredicate3Y = NSPredicate(format: "userName == %@", Global().getUserName())
        let subPredicate4Y = NSPredicate(format: "name CONTAINS %@ || addressPropertyName CONTAINS %@ || combinedAddress CONTAINS %@", strSearchText,strSearchText,strSearchText)
        let subPredicate5Y = NSPredicate(format: "isActive == YES && isDelete == NO")

        andPredicateY.append(subPredicate1Y)
        andPredicateY.append(subPredicate2Y)
        andPredicateY.append(subPredicate3Y)
        andPredicateY.append(subPredicate5Y)

        if strTypee == "Search" {
            
            andPredicateY.append(subPredicate4Y)

        }
        if yesFilterView {
            
            if "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")".count > 0 {
                
                andPredicateY.append(subPredicate6)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")".count > 0 {
                
                andPredicateY.append(subPredicate7)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")".count > 0 {
                
                andPredicateY.append(subPredicate8)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")".count > 0 {
                
                andPredicateY.append(subPredicate9)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")".count > 0 {
                
                andPredicateY.append(subPredicate10)
                
            }
            
            let arrOfAddressPropertyIds = dictCrmCompanyFilter.value(forKey: "AddressPropertyTypeId") as! NSArray
            
            if arrOfAddressPropertyIds.count > 0 {
                
                for k in 0 ..< arrOfAddressPropertyIds.count {

                    let subPredicateLL = NSPredicate(format: "addressPropertyTypeId == %@", "\(arrOfAddressPropertyIds[k])")
                    
                    //andPredicateY.append(subPredicateLL)
                    orPredicateY.append(subPredicateLL)
                    yesProprtyType = true

                }
                
            }
            
            if "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")".count > 0 && "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")".count > 0{
                
                let dateFromDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let dateToDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let subPredicateLL1 = NSPredicate(format: "createdDateNew >= %@ && createdDateNew <= %@", dateFromDate as CVarArg, dateToDate as CVarArg)
                
                andPredicateY.append(subPredicateLL1)

            }
            
        }
        
        let andPredicateCompanyY = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateY)

        let orPredicateMergedY = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicateY)

        let andPredicateCompanyYNew = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateY)
        let predicateY = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicateMergedY, andPredicateCompanyYNew])
        
        if yesProprtyType {
            
            arrOfYesterdayDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: predicateY, sort: sort)

        }else{
            
            arrOfYesterdayDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: andPredicateCompanyY, sort: sort)

        }
        
        //  Filter for this Month
        
        // For Current Month Date Filter
        
        let dictCurrentMonthDate = Global().getCureentMonthDateEST()
        
        let dateStart = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "StartDate") as? String, "MM/dd/yyyy", "EST") as Date
        
        let dateEnd = Global().convertStringDateToDate(forCrmNewFlow: dictCurrentMonthDate?.value(forKey: "EndDate") as? String, "MM/dd/yyyy", "EST") as Date

        let yesterDay = Global().convertStringDateToDate(forCrmNewFlow: Global().getYesterdayDateEST(Global().getCurrentDateEST()), "MM/dd/yyyy", "EST") as Date

        let todayDate = Global().convertStringDateToDate(forCrmNewFlow: Global().getCurrentDateEST(), "MM/dd/yyyy", "EST") as Date
        
        
        var andPredicateM : [NSPredicate] = []
        var orPredicateM : [NSPredicate] = []
        yesProprtyType = false

        let subPredicate1M = NSPredicate(format: "companyKey == %@", Global().getCompanyKey())
        let subPredicate2M = NSPredicate(format: "userName == %@", Global().getUserName())
        let subPredicate3M = NSPredicate(format: "createdDateNew >= %@", dateStart as CVarArg)
        let subPredicate4M = NSPredicate(format: "createdDateNew <= %@", dateEnd as CVarArg)
        let subPredicate5M = NSPredicate(format: "createdDateNew != %@", todayDate as CVarArg)
        let subPredicate6M = NSPredicate(format: "createdDateNew != %@", yesterDay as CVarArg)
        let subPredicate7M = NSPredicate(format: "name CONTAINS %@ || addressPropertyName CONTAINS %@ || combinedAddress CONTAINS %@", strSearchText,strSearchText,strSearchText)
        let subPredicate8M = NSPredicate(format: "isActive == YES && isDelete == NO")

        andPredicateM.append(subPredicate1M)
        andPredicateM.append(subPredicate2M)
        andPredicateM.append(subPredicate3M)
        andPredicateM.append(subPredicate4M)
        andPredicateM.append(subPredicate5M)
        andPredicateM.append(subPredicate6M)
        andPredicateM.append(subPredicate8M)

        if strTypee == "Search" {
            
            andPredicateM.append(subPredicate7M)

        }
        if yesFilterView {
            
            if "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")".count > 0 {
                
                andPredicateM.append(subPredicate6)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")".count > 0 {
                
                andPredicateM.append(subPredicate7)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")".count > 0 {
                
                andPredicateM.append(subPredicate8)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")".count > 0 {
                
                andPredicateM.append(subPredicate9)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")".count > 0 {
                
                andPredicateM.append(subPredicate10)
                
            }
            
            let arrOfAddressPropertyIds = dictCrmCompanyFilter.value(forKey: "AddressPropertyTypeId") as! NSArray
            
            if arrOfAddressPropertyIds.count > 0 {
                
                for k in 0 ..< arrOfAddressPropertyIds.count {

                    let subPredicateLL = NSPredicate(format: "addressPropertyTypeId == %@", "\(arrOfAddressPropertyIds[k])")
                    
                    //andPredicateM.append(subPredicateLL)
                    orPredicateM.append(subPredicateLL)
                    yesProprtyType = true

                }
                
            }
            
            if "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")".count > 0 && "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")".count > 0{
                
                let dateFromDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let dateToDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let subPredicateLL1 = NSPredicate(format: "createdDateNew >= %@ && createdDateNew <= %@", dateFromDate as CVarArg, dateToDate as CVarArg)
                
                andPredicateM.append(subPredicateLL1)

            }
            
        }
        
        let andPredicateCompanyM = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateM)

        let orPredicateMergedM = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicateM)

        let andPredicateCompanyMNew = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateM)
        let predicateM = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicateMergedM, andPredicateCompanyMNew])
        
        if yesProprtyType {
            
            arrOfThisMonthDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: predicateM, sort: sort)

        }else{
            
            arrOfThisMonthDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: andPredicateCompanyM, sort: sort)

        }
        
        // Filter for All Remainings
        
        var andPredicateAll : [NSPredicate] = []
        var orPredicateAll : [NSPredicate] = []
        yesProprtyType = false

        
        let subPredicate1All = NSPredicate(format: "createdDateNew < %@", dateStart as CVarArg)
        let subPredicate2All = NSPredicate(format: "companyKey == %@", Global().getCompanyKey())
        let subPredicate3All = NSPredicate(format: "userName == %@", Global().getUserName())
        let subPredicate4All = NSPredicate(format: "name CONTAINS %@ || addressPropertyName CONTAINS %@ || combinedAddress CONTAINS %@", strSearchText,strSearchText,strSearchText)
        let subPredicate5All = NSPredicate(format: "isActive == YES && isDelete == NO")

        andPredicateAll.append(subPredicate1All)
        andPredicateAll.append(subPredicate2All)
        andPredicateAll.append(subPredicate3All)
        andPredicateAll.append(subPredicate5All)

        if strTypee == "Search" {
            
            andPredicateAll.append(subPredicate4All)

        }
        if yesFilterView {
            
            if "\(dictCrmCompanyFilter.value(forKey: "Name") ?? "")".count > 0 {
                
                andPredicateAll.append(subPredicate6)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryEmail") ?? "")".count > 0 {
                
                andPredicateAll.append(subPredicate7)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "PrimaryPhone") ?? "")".count > 0 {
                
                andPredicateAll.append(subPredicate8)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "CellPhone1") ?? "")".count > 0 {
                
                andPredicateAll.append(subPredicate9)
                
            }
            if "\(dictCrmCompanyFilter.value(forKey: "Website") ?? "")".count > 0 {
                
                andPredicateAll.append(subPredicate10)
                
            }
            
            let arrOfAddressPropertyIds = dictCrmCompanyFilter.value(forKey: "AddressPropertyTypeId") as! NSArray
            
            if arrOfAddressPropertyIds.count > 0 {
                
                for k in 0 ..< arrOfAddressPropertyIds.count {

                    let subPredicateLL = NSPredicate(format: "addressPropertyTypeId == %@", "\(arrOfAddressPropertyIds[k])")
                    
                    //andPredicateAll.append(subPredicateLL)
                    orPredicateAll.append(subPredicateLL)
                    yesProprtyType = true

                }
                
            }
            
            if "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")".count > 0 && "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")".count > 0{
                
                let dateFromDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "FromDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let dateToDate = Global().convertStringDateToDate(forCrmNewFlow: "\(dictCrmCompanyFilter.value(forKey: "ToDate") ?? "")", "MM/dd/yyyy", "EST") as Date
                
                let subPredicateLL1 = NSPredicate(format: "createdDateNew >= %@ && createdDateNew <= %@", dateFromDate as CVarArg, dateToDate as CVarArg)
                
                andPredicateAll.append(subPredicateLL1)

            }
            
        }
        
        let orPredicateMergedAll = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicateAll)

        let andPredicateCompanyAll = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateAll)

        let andPredicateAllNew = NSCompoundPredicate(andPredicateWithSubpredicates: andPredicateAll)
        let predicateAll = NSCompoundPredicate(andPredicateWithSubpredicates: [orPredicateMergedAll, andPredicateAllNew])

        if yesProprtyType {
            
            arrOfAllOtherDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: predicateAll, sort: sort)

        }else{
            
            arrOfAllOtherDB = getDataFromCoreDataBaseArraySorted(strEntity: "Company", predicate: andPredicateCompanyAll, sort: sort)

        }
        
        let dicfilter = nsud.value(forKey: "CrmCompanyFilter") as! NSDictionary
        if(dicfilter.value(forKey: "CreatedBy") != nil){
            let createdby = "\(dicfilter.value(forKey: "CreatedBy") ?? "")"
            
            if(createdby != ""){
                let aryTempToda = arrOfTodayDB.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "createdBy") ?? "")" == (createdby) }
                let aryTempYesterday = arrOfYesterdayDB.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "createdBy") ?? "")" == (createdby) }
                let aryTempThisMonth = arrOfThisMonthDB.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "createdBy") ?? "")" == (createdby) }
                let aryAllOther = arrOfAllOtherDB.filter { (task) -> Bool in
                    return "\((task as! NSManagedObject).value(forKey: "createdBy") ?? "")" == (createdby) }
                 arrOfTodayDB = NSArray()
                 arrOfYesterdayDB = NSArray()
                 arrOfThisMonthDB = NSArray()
                 arrOfAllOtherDB = NSArray()
                arrOfTodayDB = aryTempToda as NSArray
                arrOfYesterdayDB = aryTempYesterday as NSArray
                arrOfThisMonthDB = aryTempThisMonth as NSArray
                arrOfAllOtherDB = aryAllOther as NSArray
                
            }
        }
        
        
        
        self.tblviewCompany.reloadData()

        let diffGlobal1 = CFAbsoluteTimeGetCurrent() - startGlobal
        print("Took \(diffGlobal1) seconds to Filter Data and Render")
        
        // check if all array data is blank then display no data label
        
        if arrOfTodayDB.count == 0 && arrOfYesterdayDB.count == 0 && arrOfThisMonthDB.count == 0 && arrOfAllOtherDB.count == 0 {
            
            // no data available
            
            self.noDataLbl()
            
        }else{
            
            self.tblviewCompany.isHidden = false
            
            lbl.removeFromSuperview()
            
            self.tblviewCompany.setContentOffset(.zero, animated: true)
            
        }
        
        
    }
    
    func saveCompaniesToDB(arrCompanyDataL : NSArray) {
        
        let startGlobal = CFAbsoluteTimeGetCurrent()

        let arrCompanyDataInDB = getDataFromCoreDataBaseArray(strEntity: "Company", predicate: NSPredicate(format: "companyKey == %@ && userName == %@", Global().getCompanyKey(),Global().getUserName()))
        
        if arrCompanyDataInDB.count > 0 {
            
            // Data exist in DB so need to check and then save
            
            for k in 0 ..< arrCompanyDataL.count {
                
                let dictOfData = arrCompanyDataL[k] as! NSDictionary
                
                let arrTempDataL = getDataFromCoreDataBaseArray(strEntity: "Company", predicate: NSPredicate(format: "companyKey == %@ && userName == %@ && crmCompanyId == %@", Global().getCompanyKey(),Global().getUserName(), "\(dictOfData.value(forKey: "CrmCompanyId") ?? "")"))
                
                if arrTempDataL.count > 0 {
                    
                    // If Data Exist Update in DB
                    
                    if !(self.updateCompanyDataDB(dictOfData: dictOfData))
                    {
                        // Not Saved Delete And Save In DB.
                        
                        deleteAllRecordsFromDB(strEntity: "Company", predicate: NSPredicate(format: "companyKey == %@ && userName == %@ && crmCompanyId == %@", Global().getCompanyKey(),Global().getUserName(), "\(dictOfData.value(forKey: "CrmCompanyId") ?? "")"))
                        
                        // Save In DB

                        self.saveCompanyDataDB(dictOfData: dictOfData)
                        
                    }
                    
                } else {
                    
                    // If Data Not Exist Save in DB
                    
                    self.saveCompanyDataDB(dictOfData: dictOfData)
                    
                }
                
            }
            
        } else {
            
            // No Comapany data in DB will insert full Data in DB
            
            for k in 0 ..< arrCompanyDataL.count {
                
                let dictOfData = arrCompanyDataL[k] as! NSDictionary
                
                self.saveCompanyDataDB(dictOfData: dictOfData)

            }
            
        }
        
        let diffGlobal1 = CFAbsoluteTimeGetCurrent() - startGlobal
        print("Took \(diffGlobal1) seconds to Save Company Data to Core Data. Count of Companies :-- \(arrCompanyDataL.count)")
        
        self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")
        
    }
    
    func saveCompanyDataDB(dictOfData : NSDictionary) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("combinedAddress")
        arrOfKeys.add("addressPropertyName")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("crmCompanyId")
        arrOfKeys.add("name")
        arrOfKeys.add("createdDateNew")
        arrOfKeys.add("profileImage")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isDelete")
        arrOfKeys.add("addressPropertyTypeId")
        arrOfKeys.add("cellPhone")
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("website")
        arrOfKeys.add("createdBy")

        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add("\(dictOfData.value(forKey: "CombinedAddress") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "AddressPropertyName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CrmCompanyId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Name") ?? "")")

        let createdDateNewL = Global().convertStringDateToDate(forCrmNewFlow: "\(dictOfData.value(forKey: "CreatedDate") ?? "")", "MM/dd/yyyy" , "EST")!
        
        arrOfValues.add(createdDateNewL)
        
        arrOfValues.add("\(dictOfData.value(forKey: "ProfileImage") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDelete") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "AddressPropertyTypeId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CellPhone") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "PrimaryEmail") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "PrimaryPhone") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Website") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")

        saveDataInDB(strEntity: "Company", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func updateCompanyDataDB(dictOfData : NSDictionary) -> Bool {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        

        arrOfKeys.add("combinedAddress")
        arrOfKeys.add("addressPropertyName")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("crmCompanyId")
        arrOfKeys.add("name")
        arrOfKeys.add("createdDateNew")
        arrOfKeys.add("profileImage")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isDelete")
        arrOfKeys.add("addressPropertyTypeId")
        arrOfKeys.add("cellPhone")
        arrOfKeys.add("primaryEmail")
        arrOfKeys.add("primaryPhone")
        arrOfKeys.add("website")
        arrOfKeys.add("createdBy")


        arrOfValues.add("\(dictOfData.value(forKey: "CombinedAddress") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "AddressPropertyName") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedDate") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CrmCompanyId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Name") ?? "")")

        let createdDateNewL = Global().convertStringDateToDate(forCrmNewFlow: "\(dictOfData.value(forKey: "CreatedDate") ?? "")", "MM/dd/yyyy" , "EST")!
        
        arrOfValues.add(createdDateNewL)
        
        arrOfValues.add("\(dictOfData.value(forKey: "ProfileImage") ?? "")")
        arrOfValues.add(dictOfData.value(forKey: "IsActive") as! Bool)
        arrOfValues.add(dictOfData.value(forKey: "IsDelete") as! Bool)
        arrOfValues.add("\(dictOfData.value(forKey: "AddressPropertyTypeId") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CellPhone") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "PrimaryEmail") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "PrimaryPhone") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "Website") ?? "")")
        arrOfValues.add("\(dictOfData.value(forKey: "CreatedBy") ?? "")")

        let isSuccess =  getDataFromDbToUpdate(strEntity: "Company", predicate: NSPredicate(format: "companyKey == %@ && userName == %@ && crmCompanyId == %@", Global().getCompanyKey(),Global().getUserName(), "\(dictOfData.value(forKey: "CrmCompanyId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        return isSuccess
        
    }
    
    // MARK: --------------------------- Web Service ---------------------------
    
    func callApiToGetCompanies(){
        
        //Change logics for Last Synced Date and Time
        if nsud.value(forKey: "CrmCompanyLastSyncDateTime") == nil ||  nsud.value(forKey: "CrmCompanyLastSyncDateTimeTemp") == nil {
            

            deleteAllRecordsFromDB(strEntity: "Company", predicate: NSPredicate(format: "companyKey = %@",Global().getCompanyKey()))

            companyCurrentSyncedDateTime = Global().strCurrentDateFormatted("MM/dd/yyyy hh:mm:ss.SSS a",UserCurrentTimeZone)

            companyLastSyncedDateTime = ""
            
        }else{
            
            companyCurrentSyncedDateTime = Global().strCurrentDateFormatted("MM/dd/yyyy hh:mm:ss.SSS a",UserCurrentTimeZone)
            
            companyLastSyncedDateTime = "\(nsud.value(forKey: "CrmCompanyLastSyncDateTime") ?? companyCurrentSyncedDateTime)"
            
        }
        
        let startGlobal = CFAbsoluteTimeGetCurrent()
        
        lbl.removeFromSuperview()
        
        if nsud.value(forKey: "CrmCompanyFilter") == nil {
            
            setDefaultValuesForCompanyFilter()
            
        }
        
        var dictToSend = nsud.value(forKey: "CrmCompanyFilter")
        let dictTempNew = NSMutableDictionary()
        dictTempNew.addEntries(from: dictToSend as! [AnyHashable : Any])
        //Change Company filter logic
        if dictTempNew["filtertype"] == nil {
            dictTempNew.setValue("Self", forKey: "filtertype")
        }
        if (nsud.value(forKey: "DPS_CompanyFilterType")) == nil {
            nsud.setValue("Self", forKey: "DPS_CompanyFilterType")
        }
        let filtertype = "\(dictTempNew.value(forKey: "filtertype") ?? "")"
        let previousfiltertype = "\(nsud.value(forKey: "DPS_CompanyFilterType") ?? "")"

        if filtertype == previousfiltertype {
            dictTempNew.setValue(companyLastSyncedDateTime, forKey: "LastSyncDateTime")

        }else{
            dictTempNew.setValue("", forKey: "LastSyncDateTime")
        }
        nsud.setValue(filtertype, forKey: "DPS_CompanyFilterType")

        
        dictToSend = dictTempNew
        
        var jsonString = String()
        if(JSONSerialization.isValidJSONObject(dictToSend!) == true)
        {
            let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend!, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonString)
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlSearchCrmCompaniesV2
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        let strTypee = "SearchCrmCompanyNew"
        Global().getServerResponseForSendLead(toServer: strURL, requestData, (NSDictionary() as! [AnyHashable : Any]), "", strTypee) { (success, response, error) in
            self.loader.dismiss(animated: false) {
                self.refresher.endRefreshing()
                if(success)
                {
                    
                    let diffGlobal1 = CFAbsoluteTimeGetCurrent() - startGlobal
                    print("Took \(diffGlobal1) seconds to get Data From SQL Server.")
                    
                    //Contacts
                    let dictResponse = (response as NSDictionary?)!
                    
                    if (dictResponse.count > 0) && (dictResponse.isKind(of: NSDictionary.self)) {
                        
                        let arrOfKey = dictResponse.allKeys as NSArray
                        
                        if arrOfKey.contains("CrmCompanies") {
                            
                            if dictResponse.value(forKey: "CrmCompanies") is NSArray {
                                
                                let arrData = dictResponse.value(forKey: "CrmCompanies") as! NSArray
                                
                                if arrData.count > 0 {
                                    
                                    self.tblviewCompany.isHidden = false
                                    
                                    let diffGlobal = CFAbsoluteTimeGetCurrent() - startGlobal
                                    print("Took \(diffGlobal) seconds to get Data OverAll To Render From SQL Server")
                                    
                                    // Update Last Synced Date Time
                                    
                                    nsud.setValue(self.companyCurrentSyncedDateTime, forKey: "CrmCompanyLastSyncDateTime")
                                    nsud.setValue(self.companyCurrentSyncedDateTime, forKey: "CrmCompanyLastSyncDateTimeTemp")

                                    nsud.synchronize()
                                    
                                    // Save Data in DB
                                    
                                    self.saveCompaniesToDB(arrCompanyDataL: arrData)
                                    
                                } else {
                                    
                                    self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

                                }
                                
                            }else{
                                
                                //self.noDataLbl()
                                self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

                            }
                            
                            
                        } else {
                            
                            //self.noDataLbl()
                            //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                            self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

                        }
                        
                        
                    } else {
                        
                        //self.noDataLbl()
                        //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                        self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

                    }
                }else {
                    
                    //self.noDataLbl()
                    //showAlertWithoutAnyAction(strtitle: Alert, strMessage: Sorry, viewcontrol: self)
                    self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

                }
                
            }
                        
        }
        
    }
    
    // MARK: --------------------------- UIButton action ---------------------------
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnContact(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.GotoContactViewController()
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        gotoCompanyFilterVC()
    }
    
    @IBAction func actionOnSearch(_ sender: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.3) {
            //self.constHghtSearchBar.constant = 56.0
        }
    }
    
    @IBAction func actionOnAddCompany(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.GotoAddCompany()
    }
    
    @IBAction func actionOnLeadOpportunity(_ sender: UIButton)
    {
        self.view.endEditing(true)
        //goToWebLead()
        goToLeadOpportunity()
        
    }
    
    @IBAction func actionOnNearBy(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnTaskActivity(_ sender: UIButton) {
        self.view.endEditing(true)
        goToTaskActivity()
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        
        let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            
            self.goToSignedAgreement()
            
        }))
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @IBAction func actionOnAdd(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func actionOnHome(_ sender: Any)
    {
        self.view.endEditing(true)
        goToDashboard()
        
    }
    
    
    @IBAction func actionOnAppointment(_ sender: Any)
    {
        self.view.endEditing(true)
        goToAppointment()
        
    }
    
    // MARK: --------UISearchBar delegate
    // MARK: -------------------------
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchAutocomplete(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchAutocomplete(searchText: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchAutocomplete(searchText: "")
    }
    
    
    func searchAutocomplete(searchText: String) -> Void{
        if(searchText.count == 0)
        {
            self.logicToFilterAndSortInDB(strTypee: "filter", strSearchText: "")

            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.endEditing(true)
            }
        }else {
            
            self.logicToFilterAndSortInDB(strTypee: "Search", strSearchText: searchText)
            
        }
    }
    // --------End-----------------

}

// MARK: --------------------------- Extensions ---------------------------


extension CompanyVC_CRMContactNew_iPhone:UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UItableView's delegate and datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrOfSection.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if (section == 0) {
                if (arrOfTodayDB.count == 0) {
                    return 0
                } else {
                    return DeviceType.IS_IPAD ? 55 : 40
                }
            } else if (section == 1) {
                if (arrOfYesterdayDB.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else if (section == 2) {
                
                if (arrOfThisMonthDB.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else if (section == 3) {
                
                if (arrOfAllOtherDB.count == 0) {
                    
                    return 0
                    
                } else {
                    
                    return DeviceType.IS_IPAD ? 55 : 40
                    
                }
                
            } else {
                
                return DeviceType.IS_IPAD ? 55 : 40
                
            }
            
      
 
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: DeviceType.IS_IPAD ? 55 : 40))
        vw.backgroundColor = hexStringToUIColor(hex: "EFEFF4")
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = arrOfSection[section] as? String
        lblHeader.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        lblHeader.textColor = UIColor.theme()
        vw.addSubview(lblHeader)
        return vw
        
    }
    
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if section == 0 {
            
            return arrOfTodayDB.count
            
        } else if section == 1 {
            
            return arrOfYesterdayDB.count
            
        } else if section == 2 {
            
            return arrOfThisMonthDB.count
            
        } else  {
            
            return arrOfAllOtherDB.count
            
        }
        
    }
    
    /*func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayCompanies.count
        
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCompany") as! CellCompany
                
        //var dictData = NSDictionary()
        var dictData = NSManagedObject()

        if indexPath.section == 0 {
            
            dictData = arrOfTodayDB[indexPath.row] as! NSManagedObject
            
        } else if indexPath.section == 1 {
            
            dictData = arrOfYesterdayDB[indexPath.row] as! NSManagedObject

        } else if indexPath.section == 2 {
            
            dictData = arrOfThisMonthDB[indexPath.row] as! NSManagedObject

        } else  {
            
            dictData = arrOfAllOtherDB[indexPath.row] as! NSManagedObject
 
        }
                
        if let name = dictData.value(forKey: "name")
        {
            if("\(name)" == "<null>" || "\(name)".count == 0 || "\(name)" == " ")
            {
                cell.lblCompany.text = ""
            }
            else
            {
                cell.lblCompany.text = "\(name)"
            }
        }
         
        cell.lblAddress.text = "\(dictData.value(forKey: "combinedAddress") ?? "")"

        if let imgURL = dictData.value(forKey: "profileImage")
        {
            if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
            {
                cell.imgviewProfile.image = UIImage(named: "no_image.jpg")
                cell.imgviewProfile.isHidden = true
                cell.lblImageName.isHidden = false
                let name = "\(dictData.value(forKey: "name") ?? "")"
                cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")
                
            }
            else
            {
                
                cell.imgviewProfile.isHidden = false
                cell.lblImageName.isHidden = true
                
                // Remove Logic for image URL
                
                let imgUrlNew = replaceBackSlasheFromUrl(strUrl: imgURL as! String)//imgURL as! String
                
                cell.imgviewProfile.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                
            }
        }else{
            
            cell.imgviewProfile.isHidden = true
            cell.lblImageName.isHidden = false
            let name = dictData.value(forKey: "name") as! String
            cell.lblImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")
            
        }

        
        cell.imgviewProfile.layer.borderWidth = 1.0
        cell.imgviewProfile.layer.masksToBounds = false
        cell.imgviewProfile.layer.borderColor = UIColor.white.cgColor
        cell.imgviewProfile.layer.cornerRadius = cell.imgviewProfile.frame.size.width / 2
        cell.imgviewProfile.clipsToBounds = true
        
        cell.btnEdit.titleLabel?.tag = indexPath.section
        cell.btnEdit.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(
            self.EditAction(sender:)), for: .touchUpInside)
        return cell
        
    }
    @objc func EditAction(sender: UIButton) {
        
        //let dictData = self.arrayCompanies[sender.tag] as! NSDictionary
        
        //var dictData = NSDictionary()
        var dictData = NSManagedObject()

        if sender.titleLabel!.tag == 0 {
            
            dictData = self.arrOfTodayDB[sender.tag] as! NSManagedObject
            
        } else if sender.titleLabel!.tag == 1 {
            
            dictData = self.arrOfYesterdayDB[sender.tag] as! NSManagedObject
            
        } else if sender.titleLabel!.tag == 2 {
            
            dictData = self.arrOfThisMonthDB[sender.tag] as! NSManagedObject
            
        } else  {
            
            dictData = self.arrOfAllOtherDB[sender.tag] as! NSManagedObject
            
        }
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
            controller.strFrom = "Update"
            let dictTemp = NSMutableDictionary()
            dictTemp.setValue("\(dictData.value(forKey: "crmCompanyId")!)", forKey: "CrmCompanyId")
            controller.dictCompany = dictTemp as NSDictionary
            //controller.dictCompany = dictData // Change krna hai esko
            controller.dictCompanyBasicInfo = NSDictionary()
            self.navigationController?.pushViewController(controller, animated: false)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        //var dictData = NSDictionary()
        var dictData = NSManagedObject()

        if indexPath.section == 0 {
            
            dictData = arrOfTodayDB[indexPath.row] as! NSManagedObject
            
        } else if indexPath.section == 1 {
            
            dictData = arrOfYesterdayDB[indexPath.row] as! NSManagedObject

        } else if indexPath.section == 2 {
            
            dictData = arrOfThisMonthDB[indexPath.row] as! NSManagedObject

        } else  {
            
            dictData = arrOfAllOtherDB[indexPath.row] as! NSManagedObject

        }
                
        UserDefaults.standard.set(false, forKey: "RefreshCompany_CompanyDetails")
        
        goToCompanyDetails(dictCompanyData: dictData)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            
            //let dictData = self.arrayCompanies[indexPath.row] as! NSDictionary
            
            //var dictData = NSDictionary()
            var dictData = NSManagedObject()

            if indexPath.section == 0 {
                
                dictData = self.arrOfTodayDB[indexPath.row] as! NSManagedObject
                
            } else if indexPath.section == 1 {
                
                dictData = self.arrOfYesterdayDB[indexPath.row] as! NSManagedObject

            } else if indexPath.section == 2 {
                
                dictData = self.arrOfThisMonthDB[indexPath.row] as! NSManagedObject

            } else  {
                
                dictData = self.arrOfAllOtherDB[indexPath.row] as! NSManagedObject

            }
            
            if(isInternetAvailable() == false)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
            else
            {
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
                controller.strFrom = "Update"
                let dictTemp = NSMutableDictionary()
                dictTemp.setValue("\(dictData.value(forKey: "crmCompanyId")!)", forKey: "CrmCompanyId")
                controller.dictCompany = dictTemp as NSDictionary
                //controller.dictCompany = dictData
                controller.dictCompanyBasicInfo = NSDictionary()
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
        edit.backgroundColor = UIColor.lightGray
        
        return [edit]
        
    }
    
}

/*
extension  CompanyVC_CRMContactNew_iPhone: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
        //const_ViewTop_H.constant = 60
        //const_SearchBar_H.constant = 0
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: searchBar.text! as NSString)
    }
    func searchAutocomplete(Searching: NSString) -> Void
    {
        
        let resultPredicate = NSPredicate(format: "Name contains[c] %@", argumentArray: [Searching])
        if !(Searching.length == 0)
        {
            let arrayfilter = (self.arrayCompaniesData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.arrayCompanies = NSArray()
            self.arrayCompanies = nsMutableArray
            self.tblviewCompany.reloadData()
        }
        else
        {
            self.arrayCompanies = NSArray()
            self.arrayCompanies = self.arrayCompaniesData
            self.tblviewCompany.reloadData()
            self.view.endEditing(true)
            searchBarCompany.text = ""
        }
        if(arrayCompanies.count == 0){
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty//if searchText.count == 0
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                
                self.view.endEditing(true)
                
            })
            self.searchAutocomplete(Searching: "")
            searchBar.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
            
        }
    }
}*/
extension CompanyVC_CRMContactNew_iPhone : UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Text field did end calls")
    }
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension CompanyVC_CRMContactNew_iPhone : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        strSearchText = str
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           self.setTopMenuOption()
            self.searchAutocomplete(searchText: str)
        }
    }
    
    
}
