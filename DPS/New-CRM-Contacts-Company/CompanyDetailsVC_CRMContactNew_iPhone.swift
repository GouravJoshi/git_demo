//
//  CompanyDetailsVC_CRMContactNew_iPhone.swift
//  DPS
//
//  Created by Rakesh Jain on 05/02/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import CallKit

enum CompanyDetailsType{
    
    case association,details,timeline,tasks
}
class CompanyDetailsVC_CRMContactNew_iPhone: UIViewController,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate {
    
    //Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgviewLogo: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnAssociation: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnTimeline: UIButton!
    @IBOutlet weak var btnTasks: UIButton!
    @IBOutlet weak var viewSlider: UIView!
    @IBOutlet weak var tblviewCompanyDetails: UITableView!
    @IBOutlet weak var constLeadingViewSlider: NSLayoutConstraint!
    @IBOutlet weak var constWidthViewSlider: NSLayoutConstraint!
    @IBOutlet weak var btnFilterTimeLine: UIButton!
    @IBOutlet weak var lblProfileImageName: UILabel!
    @IBOutlet weak var btnScheduleFooter: UIButton!
    @IBOutlet weak var btnTasksFooter: UIButton!
    
    // variables
    var companyDetailsType = CompanyDetailsType.association
    var dictAssociations = NSDictionary()
    var strServiceUrlMain = ""
    var dictLoginData = NSDictionary()
    var arrayNotes = NSArray()
    var dictAbout = NSDictionary()
    //var dictAbout = NSDictionary()
    var arrayTitles = [String]()
    var strCompanyId = ""
    
    var dictTimeLine = NSDictionary()
    var arrayTimeline = NSMutableArray()
    var filterTimelineAry = [NSMutableDictionary]()
    var dictGroupedTimeline = Dictionary<AnyHashable, [NSMutableDictionary]>()
    var aryAllKeys = Array<Any>()
    var aryLogType = NSMutableArray()
    var arrayTasks = NSMutableArray()
    var lbl = UILabel()
    var strCRMCompanyIdGlobal = ""
    var loader = UIAlertController()
    var callObserver = CXCallObserver()
    
    
    @IBOutlet weak var lbltaskCount: UILabel!
    @IBOutlet weak var lblScheduleCount: UILabel!
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if Check_Login_Session_expired() {
            Login_Session_Alert(viewcontrol: self)
        }else{
            callObserver.setDelegate(self, queue: nil)
            
            if(DeviceType.IS_IPAD){
                lbltaskCount.text = "0"
                lblScheduleCount.text = "0"
                lbltaskCount.layer.cornerRadius = 18.0
                lbltaskCount.backgroundColor = UIColor.red
                lblScheduleCount.layer.cornerRadius = 18.0
                lblScheduleCount.backgroundColor = UIColor.red
                lbltaskCount.layer.masksToBounds = true
                lblScheduleCount.layer.masksToBounds = true
                
            }
            self.constLeadingViewSlider.constant = btnAssociation.frame.origin.x
            self.constWidthViewSlider.constant = btnAssociation.frame.size.width
            
            dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            tblviewCompanyDetails.tableFooterView = UIView()
            tblviewCompanyDetails.estimatedRowHeight = 50.0
            
            arrayTitles = ["Primary Number","Secondary Number", "Cell", "Primary Email", "Secondary Email", "Website", "Report To", "Owner", "Source", "Employee Count", "Annual Revenue", "Description"]
            
            NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationChanged), name: UIDevice.orientationDidChangeNotification, object: nil)
            
            if isInternetAvailable()
            {
                
                self.callAPIToGetCompanyAssociation()
                self.callAPIToGetCompanyBasicInfo()
                
            }else{
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                let alertCOntroller = UIAlertController(title: alertMessage, message: alertInternet, preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    
                    self.navigationController?.popViewController(animated: false)
                    
                })
                
                alertCOntroller.addAction(alertAction)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
            
            /*if isInternetAvailable()
             {
             lblName.text = "\(dictAbout.value(forKey: "Name")!)"
             
             if let url = dictAbout.value(forKey: "ProfileImage"){
             
             if("\(url)".count > 0 && "\(url)" != "<null>")
             {
             let modifiedURL = "\(url)".replacingOccurrences(of: "\\", with: "/")
             self.imgviewLogo!.sd_setImage(with: URL(string: modifiedURL ), completed: nil)
             }else{
             
             self.createCompanyLogoFromName(dictData: dictAbout)
             
             }
             
             }
             
             FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
             
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
             
             self.callAPIToGetCompanyAssociation()
             
             }
             }else{
             
             self.createCompanyLogoFromName(dictData: dictAbout)
             
             }*/
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AssociatedContact_Notification"),
                                                   object: nil,
                                                   queue: nil,
                                                   using:catchNotification)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedNotes_Notification"),
                                                   object: nil,
                                                   queue: nil,
                                                   using:catchNotification1)
            
            lblProfileImageName.isHidden = true
            
            UserDefaults.standard.set(false, forKey: "RefreshTasks_CompanyDetails")
            
            //createBadgeView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                self.setFooterMenuOption()
            })
            
        }
     
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
          super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            self.setFooterMenuOption()
        })
      }
    override func viewWillAppear(_ animated: Bool) {
        if !Check_Login_Session_expired() {
            if (nsud.bool(forKey: "RefreshCompany_CompanyDetails")) || (nsud.bool(forKey: "RefreshCompany_CompanyDetails")) {
                
                if (isInternetAvailable()){
                    
                    //if companyDetailsType == .timeline {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        UserDefaults.standard.set(false, forKey: "RefreshCompany_CompanyDetails")
                        
                        self.callAPIToGetCompanyBasicInfo()
                        
                    }
                    
                    //}
                    
                }
                
            }
            
            if nsud.bool(forKey: "RefreshContacts_CompanyDetails") {
                
                if (isInternetAvailable()){
                    
                    if companyDetailsType == .association {
                        
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            UserDefaults.standard.set(false, forKey: "RefreshContacts_CompanyDetails")
                            self.callAPIToGetCompanyAssociation()
                            
                        }
                        
                    }
                    
                }
                
            }else{
                
                refreshAssociation()
                
            }
            
            
            
            if (nsud.bool(forKey: "RefreshTasks_CompanyDetails")) {
                
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                    tblviewCompanyDetails.reloadData()
                }
                else
                {
                    
                    UserDefaults.standard.set(false, forKey: "RefreshTasks_CompanyDetails")
                    self.callAPToGetTimeLine() // getTasks call from  GetTimeLine function
                    //  self.getTasks()
                    
                    
                }
                
            }
        }
        
    }
    override func viewDidLayoutSubviews() {
        
        makeLblCircle()
        constWidthViewSlider.constant = btnAssociation.frame.size.width
        self.view.layoutIfNeeded()
        
    }
    // MARK: Functions
    func setFooterMenuOption() {
        for view in self.view.subviews {
            if(view is FootorView){
                view.removeFromSuperview()
            }
        }
        nsud.setValue("Account", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))

        self.view.addSubview(footorView)
        
        footorView.onClickHomeButtonAction = {() -> Void in
            print("call home")
           self.goToDashboard()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            print("call home")
          self.goToAppointment()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            print("call home")
           // self.GotoAccountViewController()
        }
        footorView.onClickSalesButtonAction = {() -> Void in
            print("call home")
           self.goToLeadOpportunity()
        }
       
    }
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone

       self.navigationController?.pushViewController(controller, animated: false)
    }

    // MARK: Functions
    
    func createBadgeView() {
        if(DeviceType.IS_IPAD){
            lbltaskCount.isHidden = true
            lblScheduleCount.isHidden = true
        }
        if(nsud.value(forKey: "DashBoardPerformance") != nil){
            
            if nsud.value(forKey: "DashBoardPerformance") is NSDictionary {
                
                let dictData = nsud.value(forKey: "DashBoardPerformance") as! NSDictionary
                
                if(dictData.count != 0){
                    
                    var strScheduleCount = "\(dictData.value(forKey: "ScheduleCount")!)"
                    if strScheduleCount == "0" {
                        strScheduleCount = ""
                    }
                    var strTaskCount = "\(dictData.value(forKey: "Task_Today")!)"
                    if strTaskCount == "0" {
                        strTaskCount = ""
                    }
                    
                    if strScheduleCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lblScheduleCount.text = strScheduleCount
                            lblScheduleCount.isHidden = false
                        }else{
                            let badgeSchedule = SPBadge()
                            badgeSchedule.frame = CGRect(x: btnScheduleFooter.frame.size.width-20, y: 0, width: 20, height: 20)
                            badgeSchedule.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeSchedule.badge = strScheduleCount
                            btnScheduleFooter.addSubview(badgeSchedule)
                        }
                    }
                    
                    
                    if strTaskCount.count > 0 {
                        if(DeviceType.IS_IPAD){
                            lbltaskCount.text = strTaskCount
                            lbltaskCount.isHidden = false
                            
                        }else{
                            let badgeTasks = SPBadge()
                            badgeTasks.frame = CGRect(x: btnTasksFooter.frame.size.width-15, y: 0, width: 20, height: 20)
                            badgeTasks.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
                            badgeTasks.badge = strTaskCount
                            btnTasksFooter.addSubview(badgeTasks)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }

    
    func catchNotification(notification:Notification) {
        
        if (isInternetAvailable()){
            
            //if companyDetailsType == .association {
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPIToGetCompanyAssociation()
                
            }
            
            //}
            
        }
        
    }
    func catchNotification1(notification:Notification) {
        
        if (isInternetAvailable()){
            
            //if companyDetailsType == .timeline {
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPToGetTimeLine()
                
            }
            
            //}
            
        }
        
    }
    
    func catchNotification2(notification:Notification) {
        
        if (isInternetAvailable()){
            
            //if companyDetailsType == .timeline {
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPIToGetCompanyBasicInfo()
                
            }
            
            //}
            
        }
        
    }
    
    
    @objc private func deviceOrientationChanged(notification: NSNotification){
        
        print("Orientation changed")
        
        switch companyDetailsType {
        case .association:
            self.constLeadingViewSlider.constant = self.btnAssociation.frame.origin.x
            self.constWidthViewSlider.constant = self.btnAssociation.frame.size.width
            self.view.layoutIfNeeded()
            
        case .details:
            self.constLeadingViewSlider.constant = self.btnDetails.frame.origin.x
            self.constWidthViewSlider.constant = self.btnDetails.frame.size.width
            self.view.layoutIfNeeded()
            
        case .timeline:
            self.constLeadingViewSlider.constant = self.btnTimeline.frame.origin.x
            self.constWidthViewSlider.constant = self.btnTimeline.frame.size.width
            self.view.layoutIfNeeded()
            
        default:// tasks
            self.constLeadingViewSlider.constant = self.btnTasks.frame.origin.x
            self.constWidthViewSlider.constant = self.btnTasks.frame.size.width
            self.view.layoutIfNeeded()
        }
        
        tblviewCompanyDetails.reloadData()
    }
    
    fileprivate func gotoAssociateContact(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AssociateContactVC_CRMContactNew_iPhone") as! AssociateContactVC_CRMContactNew_iPhone
        
        controller.strFromWhere = "DirectAssociateFromCompany"
        controller.strRefId = "\(dictAbout.value(forKey: "CrmCompanyId") ?? "")"
        controller.strRefType = enumRefTypeCrmCompany
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    @objc func viewMoreTask(sender:UIButton)
    {
        /* let vc = storyboardNewCRM.instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
         vc.strFromVC = "WebLeadVC"
         vc.strleadId = strRefId
         self.navigationController?.pushViewController(vc, animated: false)*/
        
    }
    @objc func viewMoreActivity(sender:UIButton)
    {
        /* let vc = storyboardNewCRM.instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
         vc.strleadId = strRefId
         vc.strFromVC = "WebLeadVC"
         self.navigationController?.pushViewController(vc, animated: false)*/
    }
    
    fileprivate func makeCall()
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictAbout.value(forKey: "CellPhone1") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "CellPhone1") ?? "")")
        }
        if "\(dictAbout.value(forKey: "CellPhone2") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "CellPhone2") ?? "")")
        }
        if "\(dictAbout.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictAbout.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        
        if arrNoToCall.count > 0
        {
            
            let alert = UIAlertController(title: "", message: "Make your call on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        Global().calling(strNo)
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    
    fileprivate func sendMail()
    {
        let arrEmail = NSMutableArray()
        
        if "\(dictAbout.value(forKey: "PrimaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictAbout.value(forKey: "PrimaryEmail") ?? "")")
        }
        if "\(dictAbout.value(forKey: "SecondaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(dictAbout.value(forKey: "SecondaryEmail") ?? "")")
        }
        
        if arrEmail.count > 0
            
        {
            let alert = UIAlertController(title: "", message: "Make your email on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            for item in arrEmail
            {
                let strEmail = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strEmail)", style: .default , handler:{ (UIAlertAction)in
                    
                    if (strEmail.count > 0)
                    {
                        //Global().emailComposer(strEmail, "", "", self)
                        self.sendEmail(strEmail: strEmail)
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No email found", viewcontrol: self)
            
        }
    }
    
    fileprivate  func sendMessage()
    {
        let arrNoToCall = NSMutableArray()
        
        if "\(dictAbout.value(forKey: "Cell") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "Cell") ?? "")")
        }
        if "\(dictAbout.value(forKey: "PrimaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "PrimaryPhone") ?? "")")
        }
        if "\(dictAbout.value(forKey: "SecondaryPhone") ?? "")".count > 0
        {
            arrNoToCall.add("\(dictAbout.value(forKey: "SecondaryPhone") ?? "")")
        }
        
        if arrNoToCall.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your message on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            for item in arrNoToCall
            {
                let strNo = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                    
                    
                    if (strNo.count > 0)
                    {
                        self.displayMessageInterface(strNo: "\(strNo)")
                    }
                    
                }))
                
                
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    
    fileprivate func goToAddNote(){
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
        
        vc.strRefId = "\(dictAbout.value(forKey: "CrmCompanyId") ?? "")"
        vc.strRefType = enumRefTypeCrmCompany
        vc.strLeadType = enumRefTypeCrmContact
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func goToContactDetails(dictContact : NSDictionary){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsVC_CRMContactNew_iPhone") as! ContactDetailsVC_CRMContactNew_iPhone
        controller.strCRMContactIdGlobal = "\(dictContact.value(forKey: "CrmContactId")!)"
        controller.dictContactDetailFromList = dictContact
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
 
    
    func goToEmailDetails(dictEmail : NSDictionary){
        
        let controller = storyboardNewCRMContact.instantiateViewController(withIdentifier: "EmailDetail_iPhoneVC") as? EmailDetail_iPhoneVC
        controller?.dictEmailContain = dictEmail
        self.navigationController?.present(controller!, animated: false, completion: nil)
        
    }
    
    func goToTaskDetails(strId : String){
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsVC_CRMNew_iPhone") as! TaskDetailsVC_CRMNew_iPhone
        vc.taskId = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToActivityDetails(strId : String){
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityDetailsVC_CRMNew_iPhone") as! ActivityDetailsVC_CRMNew_iPhone
        
        vc.activityID = "\(strId)"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToWebLeadDetails(strId : String){
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
        vc.strReftype = enumRefTypeWebLead
        
        vc.strRefIdNew = "\(strId)"
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToOpportunityDetails(strId : String){
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityDetailsVC_CRMNew_iPhone") as! OpportunityDetailsVC_CRMNew_iPhone
        vc.strRefIdNew = "\(strId)"
        vc.strReftype = enumRefTypeOpportunity
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToLeadOpportunity(){
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func changeDateToString(date: Date)-> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    fileprivate func getLogTypeFromMaster()
    {
        
        let dict = nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary
        
        if(dict.count > 0)
        {
            let aryTemp = (dict.value(forKey: "ActivityLogTypeMasters") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in aryTemp
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    aryLogType.add((item as! NSDictionary))
                }
            }
            
            print(aryLogType)
        }
    }
    
    fileprivate func noDataLbl() {
        
        self.tblviewCompanyDetails.isHidden = false
        
        lbl.removeFromSuperview()
        
        lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        lbl.center = self.view.center
        lbl.text = "No Data Found"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.lightGray
        lbl.font = UIFont.systemFont(ofSize: 20)
        //    self.view.addSubview(lbl)
        
    }
    
    func makeLblCircle() {
        
        // Make Lable cirecle if Profile Image not present
        lblProfileImageName.layer.borderWidth = 1.0
        lblProfileImageName.layer.masksToBounds = false
        lblProfileImageName.layer.borderColor = UIColor.white.cgColor
        lblProfileImageName.layer.cornerRadius = imgviewLogo.frame.size.width / 2
        lblProfileImageName.clipsToBounds = true
        
        imgviewLogo.layer.borderWidth = 1.0
        imgviewLogo.layer.masksToBounds = false
        imgviewLogo.layer.borderColor = UIColor.white.cgColor
        imgviewLogo.layer.cornerRadius = imgviewLogo.frame.size.width / 2
        imgviewLogo.clipsToBounds = true
    }
    
    func showCompanyBasicInfo() {
        
        if dictAbout.count > 0 {
            
            dictAbout = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictAbout as! [AnyHashable : Any]))! as NSDictionary
            
            lblName.text = "\(dictAbout.value(forKey: "Name") ?? "")"
            
            if let imgURL = dictAbout.value(forKey: "ProfileImage")
            {
                if("\(imgURL)" == "<null>" || "\(imgURL)".count == 0 || "\(imgURL)" == " ")
                {
                    
                    self.createCompanyLogoFromName(dictData: dictAbout)
                    
                }
                else
                {
                    
                    imgviewLogo.isHidden = false
                    lblProfileImageName.isHidden = true
                    
                    // Remove Logic for image URL
                    
                    var imgUrlNew = imgURL as! String
                    
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\", with: "/")
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\", with: "/")
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\", with: "/")
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\", with: "/")
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\", with: "/")
                    imgUrlNew = imgUrlNew.replacingOccurrences(of: "\\\\\\\\\\\\", with: "/")
                    
                    //cell.imgviewProfile.sd_setImage(with: URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), options: .refreshCached)
                    imgviewLogo.setImageWith(URL(string: imgUrlNew), placeholderImage: UIImage(named: "no_image.jpg"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                    
                }
            }else{
                
                self.createCompanyLogoFromName(dictData: dictAbout)
                
            }
            
            lblAddress.text = Global().strCombinedAddress(dictAbout as? [AnyHashable : Any])
            lbl.removeFromSuperview()
            self.tblviewCompanyDetails.isHidden = false
            tblviewCompanyDetails.reloadData()
            
        }else{
            
            self.createCompanyLogoFromName(dictData: dictAbout)
            self.noDataLbl()
            
        }
        
    }
    
    func createCompanyLogoFromName(dictData : NSDictionary) {
        
        makeLblCircle()
        
        imgviewLogo.isHidden = true
        lblProfileImageName.isHidden = false
        
        if dictData.count > 0 {
            
            let name = dictData.value(forKey: "Name") as! String
            lblProfileImageName.text = firstCharactersFromString(type: "CompanyName", first: name, second: "")
            
        } else {
            
            lblProfileImageName.text = firstCharactersFromString(type: "CompanyName", first: "C", second: "")
            
        }
        
        
    }
    
    func goToDashboard(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    func goToAppointment(){
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                     let mainStoryboard = UIStoryboard(
                         name: "Main",
                         bundle: nil)
                     let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                     self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
                 }
        
    }
    func goToNearBy(){
        /*
         if(DeviceType.IS_IPAD){
         let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPad") as? NearByMeViewControlleriPad
         self.navigationController?.pushViewController(vc!, animated: false)
         }else{
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "NearByMeViewControlleriPhone") as? NearByMeViewControlleriPhone
         self.navigationController?.pushViewController(vc!, animated: false)
         }*/
        
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    func goToSignedAgreement(){
        
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
        
    }
    func goToUpdateCompany(){
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddCompanyVC_CRMContactNew_iPhone") as! AddCompanyVC_CRMContactNew_iPhone
        controller.strFrom = "Update"
        controller.dictCompany = dictAbout
        controller.dictCompanyBasicInfo = dictAbout
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func isAssociationAvailabel(dictData : NSDictionary) -> Bool {
        
        var isValuePresent = false
        
        if dictData.count > 0 {
            
            if let Accounts = dictData.value(forKey: "Accounts")
            {
                if(Accounts is NSArray)
                {
                    if (Accounts as! NSArray).count > 0 {
                        
                        isValuePresent = true
                        
                    }
                }
                
            }
            if let CrmContacts = dictData.value(forKey: "Contacts")
            {
                if(CrmContacts is NSArray)
                {
                    if (CrmContacts as! NSArray).count > 0 {
                        
                        isValuePresent = true
                        
                    }
                }
                
            }
            
            if let Leads = dictData.value(forKey: "Leads")
            {
                if(Leads is NSArray)
                {
                    if (Leads as! NSArray).count > 0 {
                        
                        isValuePresent = true
                        
                    }
                }
                
            }
            
            if let Opportunities = dictData.value(forKey: "Opportunities")
            {
                if(Opportunities is NSArray)
                {
                    if (Opportunities as! NSArray).count > 0 {
                        
                        isValuePresent = true
                        
                    }
                }
                
            }
            
        }
        
        return isValuePresent
        
    }
    
    func diAssociateContact(strCrmContactId : String)
    {
        
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlDissociateContact + strCrmContactId + "&RefType=" + enumRefTypeCrmCompany + "&RefId=" + strCRMCompanyIdGlobal
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "GrabLead") { (Response, Status) in
            
            self.loader.dismiss(animated: false) {
                if(Status)
                {
                    let strResponse = Response["data"] as! String
                    
                    if (strResponse.count > 0) && strResponse == "true"
                    {
                        UserDefaults.standard.set(true, forKey: "RefreshAssociations_ContactDetails")
                        
                        // Successfully deassociated
                        // call APi for Fetching assocaitions again
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            
                            self.callAPIToGetCompanyAssociation()
                            
                        }
                        
                    }
                    else
                    {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
            }
            
        }
        
    }
    func gotoCreateFollowUp(dictData : NSDictionary) {
        
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(2, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountId")!)", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadId")!)", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadId")!)", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactId")!)", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyId")!)", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "AccountDisplayName")!)", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadName")!)", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "WebLeadName")!)", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmContactName")!)", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("\(dictData.value(forKey: "CrmCompanyName")!)", forKeyPath: "Pre_CrmCompanyName")
        
        dictTaskDetailsData.addEntries(from: dictData as! [AnyHashable : Any])
        
        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "LeadTaskId")!)", forKeyPath: "LeadTaskId")
        //dictTaskDetailsData.setValue("\(dictData.value(forKey: "status")!)", forKeyPath: "status")
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    func goToLogAsActivity() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
            vc.strType = "LogAsActivity"
            vc.strRefType = enumRefTypeCrmCompany
            vc.strRefTypeId = self.strCRMCompanyIdGlobal
            self.navigationController?.pushViewController(vc, animated: false)
       }
        
    }
    
    func refreshTimeline() {
        
        if nsud.bool(forKey: "RefreshTimeline_CompanyDetails") {
            
            if (isInternetAvailable()){
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    UserDefaults.standard.set(false, forKey: "RefreshTimeline_CompanyDetails")
                    
                    self.callAPToGetTimeLine()
                    
                })
                
            }
            
        }
        
    }
    
    func refreshAssociation() {
        
        if nsud.bool(forKey: "RefreshAssociations_CompanyDetails") {
            
            if (isInternetAvailable()){
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    UserDefaults.standard.set(false, forKey: "RefreshAssociations_CompanyDetails")
                    
                    self.callAPIToGetCompanyAssociation()
                    
                })
                
            }
            
        }
        
    }
    
    // MARK: -  ------------------------------ Message Composer Delegate ------------------------------
    
    func displayMessageInterface(strNo: String)
    {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["\(strNo)"]
        composeVC.body = ""
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        //dismiss(animated: false, completion: nil)
        
        dismiss(animated: false) {
            
            if result == .sent || result == .failed{
                
                self.goToLogAsActivity()
                
            }
            
        }
        
    }
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // MARK: Web Service Calling
    
    fileprivate func callAPIToGetCompanyAssociation()
    {
        
        
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "/api/LeadNowAppToSalesProcess/GetCrmCompanyAssociation?CrmCompanyId=\(strCRMCompanyIdGlobal)"
        
        
        if !(self.presentedViewController is UIAlertController){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
        }
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_Association") { (response, status) in
            self.loader.dismiss(animated: false) {}
            if(status == true)
            {
                print(response)
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    print("Yes Data is there ")
                    if((response.value(forKey: "data") as! NSDictionary).count > 0)
                    {
                        self.dictAssociations = (response.value(forKey: "data") as! NSDictionary)
                        
                        self.tblviewCompanyDetails.reloadData()
                        
                        let isValuePresent = self.isAssociationAvailabel(dictData: self.dictAssociations)
                        
                        if(!isValuePresent){
                            
                            self.noDataLbl()
                            
                        }
                        
                    }
                    else{
                        
                        
                        self.tblviewCompanyDetails.reloadData()
                        
                        
                        self.noDataLbl()
                    }
                    
                    
                } else {
                    
                    
                    self.tblviewCompanyDetails.reloadData()
                    
                    
                    self.noDataLbl()
                }
                
            }
            else
            {
                
                self.tblviewCompanyDetails.reloadData()
                
                self.noDataLbl()
            }
            
        }
    }
    
    fileprivate func callAPIToGetAllNotes(){
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        let strReftype = "CrmCompany"
        let strRefId = strCRMCompanyIdGlobal
        
        var strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "/api/LeadNowAppToSalesProcess/GetLeadNotesListV2?refid=\(strRefId)&reftype=\(strReftype)"
        
        /*
         var strUrl = "\(strServiceUrlMain)\(UrlGetAllNotes)\(strRefId)&reftype=\(strReftype)"  as String
         */
        
        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
        
        
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strUrl , responseStringComing: "Description") { (Response, Status) in
            
            //            DispatchQueue.main.async{
            //
            //                FTIndicator.dismissProgress()
            //            }
            if(Status)
            {
                let arrData = Response["data"] as! NSArray
                
                if arrData.isKind(of: NSArray.self)
                {
                    let arrAllData = NSMutableArray()
                    
                    for item in arrData
                    {
                        let  dict = item as! NSDictionary
                        
                        let dictNew = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dict as! [AnyHashable : Any]))
                        
                        arrAllData.add(dictNew as Any)
                        
                    }
                    if(arrAllData.count > 0)
                    {
                        self.arrayNotes = arrAllData
                        self.tblviewCompanyDetails.reloadData()
                        // self.calculateTableViewHeight()
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        self.arrayNotes = arrAllData
                        self.tblviewCompanyDetails.reloadData()
                        // self.calculateTableViewHeight()
                    }
                    
                }
                else
                {
                    
                    self.tblviewCompanyDetails.reloadData()
                    //                    DispatchQueue.main.async{
                    //
                    //                        FTIndicator.dismissProgress()
                    //                    }
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                }
                
            }
            else
            {
                
                self.tblviewCompanyDetails.reloadData()
                //                DispatchQueue.main.async{
                //
                //                    FTIndicator.dismissProgress()
                //                }
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                
            }
            
        }
    }
    
    fileprivate func callAPIToGetTaskList()
    {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            let  strRefType = "CRMCompany"
            let  strRefID =   strCRMCompanyIdGlobal
            
            
            let dictJson = ["RefId":strRefID,
                            "RefType":strRefType,
                            "IsDefault":true,
                            "SkipDefaultDate":false,
                            "Status":"",
                            "FromDate":"",
                            "ToDate":"",
                            "EmployeeId":"",
                            "TakeRecords":"",
                            "TaskTypeId":"",
                            "PriorityIds":""] as [String : Any]
            
            print(dictJson)
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "/api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3_SubObjs"
            
            
            loader = loader_Show(controller: self, strMessage: "Fetching Tasks...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeaders(dictJson: dictJson as NSDictionary, url: strURL, responseStringComing: "TaskVC") { (response, status) in
                self.loader.dismiss(animated: false) {
                    if(status == true)
                    {
                        if let aryTask = response.value(forKeyPath: "data.Tasks")
                        {
                            if((aryTask as! NSArray).count > 0)
                            {
                                print(aryTask)
                            }
                            else{
                                
                                
                                self.noDataLbl()
                                
                            }
                        }
                        else{
                            
                            self.noDataLbl()
                            
                        }
                    }
                    else
                    {
                        self.noDataLbl()
                        
                        
                    }
                }
                
                
            }
        }
    }
    
    fileprivate func callAPIToGetCompanyBasicInfo(){
        
        
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        var strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetCompanyBasicInfo + strCRMCompanyIdGlobal
        
        strUrl = strUrl.trimmingCharacters(in: .whitespaces)
        
        if !(self.presentedViewController is UIAlertController){
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
        }
        
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strUrl, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            self.loader.dismiss(animated: false) {
                self.showCompanyBasicInfo()
                if(status)
                {
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        print("Yes Data is there ")
                        if((response.value(forKey: "data") as! NSDictionary).count > 0)
                        {
                            self.dictAbout = (response.value(forKey: "data") as! NSDictionary)
                            
                            self.lblAddress.text = Global().strCombinedAddress(self.dictAbout as? [AnyHashable : Any])
                            
                            self.showCompanyBasicInfo()
                            
                            self.tblviewCompanyDetails.reloadData()
                            
                        }
                        else{
                            
                            
                            self.tblviewCompanyDetails.reloadData()
                            
                            self.noDataLbl()
                            
                        }
                        
                    } else {
                        
                        
                        self.tblviewCompanyDetails.reloadData()
                        
                        self.noDataLbl()
                        
                    }
                    
                }
                else
                {
                    
                    self.tblviewCompanyDetails.reloadData()
                    
                    
                    self.noDataLbl()
                    
                }
            }
            
            
        }
    }
    
    fileprivate func callAPToGetTimeLine(){
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        if(isInternetAvailable() == false)
        {
            
            
            tblviewCompanyDetails.reloadData()
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTimeline
            
            var dict = [String:Any]()
            
            if nsud.value(forKey: "DPS_FilterTimeLine") == nil {
                
                setDefaultValuesForTimeLineFilter()
                
            }
            
            let dictFilterData = (nsud.value(forKey: "DPS_FilterTimeLine")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            var arySelectedInteraction = NSMutableArray()
            
            if dictFilterData.value(forKey: "SelectedInteraction") is NSArray {
                
                arySelectedInteraction = (dictFilterData.value(forKey: "SelectedInteraction")as! NSArray).mutableCopy()as! NSMutableArray
                
            }else {
                
                
            }
            
            dict = ["fromDate":dictFilterData.value(forKey: "FromDate") ?? "",
                    "toDate":dictFilterData.value(forKey: "Todate") ?? "",
                    "refId":strCRMCompanyIdGlobal,
                    "refType":enumRefTypeCrmCompany,
                    "employeeid":dictFilterData.value(forKey: "SelectedUserName") ?? "",
                    "Entities":arySelectedInteraction
            ]
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                self.loader.dismiss(animated: false) {
                    if(status == true){
                        
                        self.dictTimeLine = response.value(forKey: "data") as! NSDictionary
                        let aryTemp = NSMutableArray()
                        
                        if((self.dictTimeLine.value(forKey: "Activities") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Activities") as! NSArray{
                                
                                aryTemp.add(item)
                            }
                        }
                        
                        if((self.dictTimeLine.value(forKey: "Emails") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Emails") as! NSArray{
                                aryTemp.add(item)
                            }
                        }
                        
                        if((self.dictTimeLine.value(forKey: "Notes") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Notes") as! NSArray{
                                aryTemp.add(item)
                            }
                        }
                        
                        if((self.dictTimeLine.value(forKey: "Tasks") as! NSArray).count > 0)
                        {
                            for item in self.dictTimeLine.value(forKey: "Tasks") as! NSArray{
                                aryTemp.add(item)
                            }
                        }
                        
                        self.arrayTimeline.removeAllObjects()
                        
                        for item in aryTemp{
                            
                            let innerItem = (item as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            
                            let dateClientCreated = Global().convertDate("\(innerItem.value(forKey: "ClientCreatedDate")!)")
                            
                            let dateFormatter = DateFormatter()
                            
                            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                            dateFormatter.dateFormat = "MM/dd/yyyy"
                            
                            let date1 = dateFormatter.date(from: dateClientCreated!)
                            print(date1!)
                            
                            let date = Global().getOnlyDate(date1)
                            print(date!)
                            
                            innerItem.setValue(date, forKey: "ClientCreatedDate")
                            
                            self.arrayTimeline.add(innerItem)
                        }
                        
                        if(self.arrayTimeline.count > 0){
                            
                            let sortedArray = self.arrayTimeline.sorted(by: {
                                (($0 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)! >= (($1 as! NSMutableDictionary)["ClientCreatedDate"] as? Date)!
                            })
                            
                            self.filterTimelineAry = sortedArray as! [NSMutableDictionary]
                            
                            var arr = [NSMutableDictionary]()
                            
                            arr = sortedArray as! [NSMutableDictionary]
                            
                            self.dictGroupedTimeline = Dictionary(grouping: arr) { $0["ClientCreatedDate"] as! Date }
                            
                            self.aryAllKeys = Array(self.dictGroupedTimeline.keys)
                            
                            let aryAllKeysCopy = self.aryAllKeys
                            
                            self.aryAllKeys = aryAllKeysCopy.sorted(by: { (($0 as! Date) ) > (($1 as! Date) ) })
                            
                        }
                        
                        self.tblviewCompanyDetails.reloadData()
                        
                        if self.aryAllKeys.count == 0 {
                            
                            self.noDataLbl()
                            
                        }
                        
                    }
                    else
                    {
                        self.noDataLbl()
                    }
                    self.getTasks()
                }
            }
        }
    }
    
    fileprivate func getTasks()
    {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        if(isInternetAvailable() == false)
        {
            
            
            
        }
        else
        {
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "api/LeadNowAppToSalesProcess/GetTasksByRefIdAndRefTypeAsyncV3"
            
            var dict = [String:Any]()
            
            dict = ["RefId":strCRMCompanyIdGlobal,
                    "RefType":enumRefTypeCrmCompany,
                    "IsDefault":"false",
                    "SkipDefaultDate":"false",
                    "Status":"",
                    "FromDate":"",
                    "ToDate":"",
                    "EmployeeId":"",
                    "TakeRecords":"",
                    "TaskTypeId":"",
                    "PriorityIds":""
            ]
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            if !(self.presentedViewController is UIAlertController){
                self.present(loader, animated: false, completion: nil)
            }
            
            
            WebService.postRequestWithHeaders(dictJson: dict as NSDictionary, url: strURL, responseStringComing: "TimeLine") { (response, status) in
                
                self.loader.dismiss(animated: false) {
                    if(status == true){
                        
                        var dictTemp = NSDictionary()
                        dictTemp = response.value(forKey: "data") as! NSDictionary
                        
                        let aryTemp = NSMutableArray()
                        
                        if((dictTemp.value(forKey: "Tasks") as! NSArray).count > 0)
                        {
                            for item in dictTemp.value(forKey: "Tasks") as! NSArray
                            {
                                let dictData = removeNullFromDict(dict: (item as! NSDictionary).mutableCopy() as! NSMutableDictionary)
                                aryTemp.add(dictData)
                            }
                        }
                        
                        self.arrayTasks = NSMutableArray()
                        self.arrayTasks = aryTemp.mutableCopy() as! NSMutableArray
                        self.tblviewCompanyDetails.reloadData()
                        
                        if self.arrayTasks.count == 0 {
                            
                            self.noDataLbl()
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        self.noDataLbl()
                        
                    }
                }
                
            }
        }
        
    }
    
    fileprivate func callAPIToGetTaskDetailById(strId : String){
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        
        var strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetTaskById + strId
        
        strURL = strURL.trimmingCharacters(in: .whitespaces)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "CRMConactNew_ContactDetails_About") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                if(status)
                {
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        print("Yes Data is there ")
                        if((response.value(forKey: "data") as! NSDictionary).count > 0)
                        {
                            
                            var dictTaskDetailData = (response.value(forKey: "data") as! NSDictionary)
                            
                            dictTaskDetailData = Global().nestedDictionaryByReplacingNullsWithNilReplica(forArray: (dictTaskDetailData as! [AnyHashable : Any]))! as NSDictionary
                            
                            // goto create follow up
                            
                            self.gotoCreateFollowUp(dictData: dictTaskDetailData)
                            
                        }
                        else{
                            
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                            
                        }
                        
                    } else {
                        
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    
                    
                    
                }
            }
            
        }
    }
    
    
    func callAPIToUpdateTaskMarkAsDone(dictData:NSMutableDictionary, tag:Int)
    {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        var strStatus = ""
        
        if("\(dictData.value(forKey: "Status")!)" == "Open")
        {
            strStatus = "Done"
        }
        else
        {
            strStatus = "Open"
        }
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + "api/LeadNowAppToSalesProcess/ChangeTaskStatus?TaskId=\(dictData.value(forKey: "LeadTaskId")!)&Status=\(strStatus)"
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "TaskVC_CRMNew_iPhone") { (response, status) in
            
            self.loader.dismiss(animated: false) {
                if(status == true)
                {
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        UserDefaults.standard.set(true, forKey: "RefreshTimeline_CompanyDetails")
                        
                        print("Yes Data is there ")
                        if("\(response.value(forKey: "data")!)" == "true")
                        {
                            
                            var dictMutable = NSMutableDictionary()
                            dictMutable = dictData.mutableCopy() as! NSMutableDictionary
                            dictMutable.setValue(strStatus, forKey: "Status")
                            
                            self.arrayTasks.replaceObject(at: tag, with: dictMutable)
                            self.tblviewCompanyDetails.reloadData()
                            
                            if "\((dictData.value(forKey: "Status")!))" == "Open"
                            {
                                /*let alertController = UIAlertController(title: "Alert", message: "Do you want to create follow up", preferredStyle: .alert)
                                 // Create the actions
                                 
                                 let okAction = UIAlertAction(title: "Yes - Create", style: UIAlertAction.Style.default)
                                 {
                                 UIAlertAction in
                                 
                                 //self.gotoCreateFollowUp(dictData: dictData)
                                 
                                 self.callAPIToGetTaskDetailById(strId: "\((dictData.value(forKey: "LeadTaskId")!))")
                                 
                                 }
                                 let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                                 {
                                 UIAlertAction in
                                 NSLog("Cancel Pressed")
                                 }
                                 // Add the actions
                                 alertController.addAction(okAction)
                                 alertController.addAction(cancelAction)
                                 
                                 // Present the controller
                                 self.present(alertController, animated: true, completion: nil)*/
                                
                                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "LogAsActivityVC") as! LogAsActivityVC
                                
                                controller.dictLeadTaskData = NSDictionary()
                                controller.leadTaskId = "\((dictData.value(forKey: "LeadTaskId")!))"
                                controller.dictOfAssociations = NSMutableDictionary()
                                self.navigationController?.pushViewController(controller, animated: false)
                                
                                
                            }
                            
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                        
                    } else {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    // something went wrong
                }
            }
            
            
        }
        
    }
    
    // MARK: UIButton action
    
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnAssociations(_ sender: UIButton) {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        
        UIView.animate(withDuration: 0.5) {
            
            self.constLeadingViewSlider.constant = sender.frame.origin.x
            self.constWidthViewSlider.constant = sender.frame.size.width
            self.view.layoutIfNeeded()
        }
        
        btnAssociation.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 18 : 13)
        btnDetails.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 13.0)
        btnTimeline.titleLabel?.font = UIFont.systemFont(ofSize:DeviceType.IS_IPAD ? 18 :  13.0)
        btnTasks.titleLabel?.font = UIFont.systemFont(ofSize:DeviceType.IS_IPAD ? 18 :  13.0)
        companyDetailsType = CompanyDetailsType.association
        
        
        //calculateTableViewHeight()
        tblviewCompanyDetails.reloadData()
        
        let isValuePresent = self.isAssociationAvailabel(dictData: dictAssociations)
        
        if(!isValuePresent){
            self.callAPIToGetCompanyAssociation()
        }else{
            tblviewCompanyDetails.isHidden = false
            tblviewCompanyDetails.reloadData()
            refreshAssociation()
        }
    }
    
    @IBAction func actionOnDetails(_ sender: UIButton) {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        // btnFilterTimeLine.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            
            self.constLeadingViewSlider.constant = sender.frame.origin.x
            self.constWidthViewSlider.constant = sender.frame.size.width
            self.view.layoutIfNeeded()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.constWidthViewSlider.constant = sender.frame.size.width
                self.view.layoutIfNeeded()
                
            }
            
        }
        btnDetails.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 18 : 13)
        btnAssociation.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnTimeline.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnTasks.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        companyDetailsType = CompanyDetailsType.details
        
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            tblviewCompanyDetails.reloadData()
        }
        else
        {
            
            if(dictAbout.count == 0){
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.callAPIToGetCompanyBasicInfo()
                    
                }
                
            }else{
                
                tblviewCompanyDetails.isHidden = false
                showCompanyBasicInfo()
                
            }
            
        }
        
    }
    
    @IBAction func actionOnTimeline(_ sender: UIButton) {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        //btnFilterTimeLine.isHidden = false
        
        UIView.animate(withDuration: 0.5) {
            
            self.constLeadingViewSlider.constant = sender.frame.origin.x
            self.constWidthViewSlider.constant = sender.frame.size.width
            self.view.layoutIfNeeded()
        }
        
        btnTimeline.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 18 : 13)
        btnAssociation.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnDetails.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnTasks.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        companyDetailsType = CompanyDetailsType.timeline
        tblviewCompanyDetails.reloadData()
        
        if(aryAllKeys.count == 0){
            
            if(isInternetAvailable() == false)
            {
                
                tblviewCompanyDetails.reloadData()
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            else
            {
                
                if self.aryAllKeys.count == 0 {
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        
                        self.callAPToGetTimeLine()
                        
                    }
                    
                }else{
                    
                    tblviewCompanyDetails.isHidden = false
                    tblviewCompanyDetails.reloadData()
                    
                    self.refreshTimeline()
                    
                }
                
            }
            
        }else{
            
            self.refreshTimeline()
            
        }
        
    }
    
    @IBAction func actionOnNotes(_ sender: UIButton) {
        
        // btnFilterTimeLine.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            
            self.constLeadingViewSlider.constant = sender.frame.origin.x
            self.constWidthViewSlider.constant = self.btnTasks.frame.size.width
            self.view.layoutIfNeeded()
        }
        
        btnTimeline.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 18 : 13)
        btnAssociation.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnDetails.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnTasks.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        companyDetailsType = CompanyDetailsType.timeline
        tblviewCompanyDetails.reloadData()
        
        /*   if(arrayNotes.count > 0)
         {
         // calculateTableViewHeight()
         tblviewCompanyDetails.reloadData()
         }
         else{
         
         //FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
         
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
         self.callAPIToGetAllNotes()
         }
         }*/
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.callAPIToGetAllNotes()
        }
    }
    
    @IBAction func actionOnTasks(_ sender: UIButton) {
        
        lbl.removeFromSuperview()
        self.tblviewCompanyDetails.isHidden = false
        
        // btnFilterTimeLine.isHidden = true
        
        UIView.animate(withDuration: 0.5) {
            
            self.constLeadingViewSlider.constant = sender.frame.origin.x
            self.constWidthViewSlider.constant = sender.frame.size.width
            
            self.view.layoutIfNeeded()
            
        }
        
        btnTasks.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: DeviceType.IS_IPAD ? 18 : 13)
        btnDetails.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnAssociation.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        btnTimeline.titleLabel?.font = UIFont(name: "Helvetica", size: DeviceType.IS_IPAD ? 18 : 13)
        
        companyDetailsType = CompanyDetailsType.tasks
        tblviewCompanyDetails.reloadData()
        
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            tblviewCompanyDetails.reloadData()
        }
        else
        {
            
            if(arrayTasks.count == 0){
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    self.getTasks()
                    
                }
                
            }else{
                
                tblviewCompanyDetails.isHidden = false
                tblviewCompanyDetails.reloadData()
                
            }
            
        }
        
    }
    
    @IBAction func actionOnFilterTimeLine(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
        controller.delegate = self
        self.navigationController?.present(controller, animated: false, completion: nil)
        
    }
    @objc func goToFilterTimeLine(_ selectedDate: Any) {
        self.view.endEditing(true)
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterTimeLineVC") as! FilterTimeLineVC
        controller.delegate = self
        self.navigationController?.present(controller, animated: false, completion: nil)
        //...
    }
    /*  @IBAction func actionOnAdd(_ sender: UIButton) {
     
     if(companyDetailsType == .associations)
     {
     let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
     
     alert.addAction(UIAlertAction(title: "Associate Contact", style: .default , handler:{ (UIAlertAction)in
     
     self.gotoAssociateContact()
     
     }))
     
     alert.addAction(UIAlertAction(title: "Add Note", style: .default , handler:{ (UIAlertAction)in
     
     self.goToAddNote()
     
     }))
     
     alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
     
     }))
     
     alert.popoverPresentationController?.sourceView = self.view
     
     self.present(alert, animated: true, completion: {
     
     
     })
     }
     }
     
     @IBAction func actionOnEdit(_ sender: UIButton) {
     }
     
     @IBAction func actionOnCall(_ sender: UIButton) {
     makeCall()
     }
     
     @IBAction func actionOnEmail(_ sender: UIButton) {
     sendMail()
     }
     
     @IBAction func actionOnText(_ sender: UIButton) {
     sendMessage()
     }
     */
    
    
    @IBAction func actionOnAdd(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        let Account = UIAlertAction(title: "Associate Contact", style: .default , handler:{ (UIAlertAction)in
            self.gotoAssociateContact()
        })
        Account.setValue(#imageLiteral(resourceName: "Icon ionic-md-person"), forKey: "image")
        Account.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Account)
        
        let Note = UIAlertAction(title: "Add Note", style: .default , handler:{ (UIAlertAction)in
            self.goToAddNote()
        })
        Note.setValue(#imageLiteral(resourceName: "Add Notes"), forKey: "image")
        Note.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Note)
        
        
        let Task = UIAlertAction(title: "Add Task", style: .default , handler:{ (UIAlertAction)in
            
            let dictTaskDetailsData = NSMutableDictionary()
            dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
            dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
            
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
            
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
            dictTaskDetailsData.setValue(self.strCRMCompanyIdGlobal, forKeyPath: "Pre_CrmCompanyId")
            
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
            dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
            dictTaskDetailsData.setValue("\(self.dictAbout.value(forKey: "Name")!)", forKeyPath: "Pre_CrmCompanyName")
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
            controller.dictTaskDetailsData = dictTaskDetailsData
            self.navigationController?.pushViewController(controller, animated: false)
            
        })
        Task.setValue(#imageLiteral(resourceName: "Task"), forKey: "image")
        Task.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Task)
        
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
            
        }
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    @IBAction func actionOnEdit(_ sender: UIButton) {
    }
    
    @IBAction func actionOnEditCompany(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            
            self.goToUpdateCompany()
            
        }
        
    }
    
    @IBAction func actionOnDashboard(_ sender: UIButton) {
        
        goToDashboard()
        
    }
    
    @IBAction func actionOnTasksFooter(_ sender: UIButton) {
        
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        
        //testController.strFromVC = strFromVC
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    
    @IBAction func actionOnLeads(_ sender: UIButton) {
        
        self.view.endEditing(true)
        goToLeadOpportunity()
        
    }
    
    @IBAction func actionOnAppointments(_ sender: UIButton) {
        
        goToAppointment()
        
    }
    
    @IBAction func actionOnMore(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: Alert_SelectOption, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        let Near = (UIAlertAction(title: enumNearBy, style: .default , handler:{ (UIAlertAction)in
            
            self.goToNearBy()
            
        }))
        Near.setValue(#imageLiteral(resourceName: "Near by me"), forKey: "image")
        Near.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Near)
        
        let Signed = (UIAlertAction(title: enumSignedAgreement, style: .default , handler:{ (UIAlertAction)in
            self.goToSignedAgreement()
        }))
        
        Signed.setValue(#imageLiteral(resourceName: "Sign Agreement"), forKey: "image")
        Signed.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Signed)
        
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            
        }
        
        self.present(alert, animated: true, completion: {
            
            
        })
        
    }
    
    
    @objc func actionOnPrimaryPhone(sender:UIButton){
        
        print("primary phone clicked")
    }
    
    @objc func actionOnSecondaryPhone(sender:UIButton){
        print("secondary phone clicked")
    }
    
    @objc func actionOnCellPhone(sender:UIButton){
        print("cell phone clicked")
    }
    
    @objc func actionOnPrimaryEmail(sender:UIButton){
        print("primary email clicked")
    }
    
    @objc func actionOnSecondaryEmail(sender:UIButton){
        print("secondary email clicked")
    }
    
    @objc func actionOnWebsite(sender:UIButton){
        print("website clicked")
    }
    
    @IBAction func actionOnCompanyLogo(_ sender: UIButton) {
        
        if (imgviewLogo.image == UIImage(named: "no_image.jpg")) {
            
            
            
        } else {
            
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
            testController!.img = imgviewLogo.image!
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }
        
    }
    
    @IBAction func actionOnCompanyAddress(_ sender: UIButton) {
        
        let strAddress = Global().strCombinedAddress(dictAbout as? [AnyHashable : Any])
        
        if strAddress!.count > 0
        {
            
            let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
            // Create the actions
            
            let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                Global().redirect(onAppleMap: self, strAddress)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
        
    }
    // MARK: --------------Cell Button Actions-------------------
    
    @objc func actionOnDeAssociateContact(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to remove.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
                
                let ary = self.dictAssociations.value(forKey: "Contacts") as! NSArray
                
                let dict = ary[sender.tag] as! NSDictionary
                
                let crmContactIdLocal = "\(dict.value(forKey: "CrmContactId") ?? "")"
                
                self.diAssociateContact(strCrmContactId: crmContactIdLocal)
                
            }))
            alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            /*let ary = dictAssociations.value(forKey: "Contacts") as! NSArray
             
             let dict = ary[sender.tag] as! NSDictionary
             
             let crmContactIdLocal = "\(dict.value(forKey: "CrmContactId") ?? "")"
             
             self.diAssociateContact(strCrmContactId: crmContactIdLocal)*/
            
        }
        
    }
    
    
    @objc func actionOnCalling(sender: UIButton!)
    {
        
        if(sender.tag == 0)
        {
            if let primaryPhone = dictAbout.value(forKey: "PrimaryPhone")
            {
                if("\(primaryPhone)".count > 0 && "\(primaryPhone)" != "<null>")
                {
                    
                    Global().calling(getOnlyNumberFromString(number: primaryPhone as! String))
                    
                }
            }
        }
        if(sender.tag == 1)
        {
            if let secondaryPhone = dictAbout.value(forKey: "SecondaryPhone")
            {
                if("\(secondaryPhone)".count > 0 && "\(secondaryPhone)" != "<null>")
                {
                    Global().calling(getOnlyNumberFromString(number: secondaryPhone as! String))
                }
            }
            
        }
        if(sender.tag == 2)
        {
            if let cellPhone1 = dictAbout.value(forKey: "CellPhone1")
            {
                if("\(cellPhone1)".count > 0 && "\(cellPhone1)" != "<null>")
                {
                    Global().calling(getOnlyNumberFromString(number: cellPhone1 as! String))
                }
            }
        }
        if(sender.tag == 3)
        {
            
            if let primaryEmail = dictAbout.value(forKey: "PrimaryEmail")
            {
                if("\(primaryEmail)".count > 0 && "\(primaryEmail)" != "<null>")
                {
                    self.sendEmail(strEmail: primaryEmail as! String)
                }
            }
        }
        
        if(sender.tag == 4)
        {
            if let secondaryEmail = dictAbout.value(forKey: "SecondaryEmail")
            {
                if("\(secondaryEmail)".count > 0 && "\(secondaryEmail)" != "<null>")
                {
                    self.sendEmail(strEmail: secondaryEmail as! String)
                }
            }
        }
        if(sender.tag == 5)
        {
            
            if let website = dictAbout.value(forKey: "Website")
            {
                if("\(website)".count > 0 && "\(website)" != "<null>")
                {
                    
                    var strUrl : String = website as! String
                    if strUrl.contains("http") == false{
                        strUrl = "http://" + strUrl
                    }
                    
                    guard let url = URL(string: strUrl) else {
                        return
                    }
                    
                    let safariVC = SFSafariViewController(url: url)
                    present(safariVC, animated: true, completion: nil)
                    
                }
            }
        }
        
    }
    @objc func actionOnMessage(sender: UIButton!)
    {
        
        if(sender.tag == 0)
        {
            if let primaryPhone = dictAbout.value(forKey: "PrimaryPhone")
            {
                if("\(primaryPhone)".count > 0 && "\(primaryPhone)" != "<null>")
                {
                    self.displayMessageInterface(strNo: "\(primaryPhone)")
                }
            }
        }
        if(sender.tag == 1)
        {
            if let secondaryPhone = dictAbout.value(forKey: "SecondaryPhone")
            {
                if("\(secondaryPhone)".count > 0 && "\(secondaryPhone)" != "<null>")
                {
                    self.displayMessageInterface(strNo: "\(secondaryPhone)")
                }
            }
            
        }
        if(sender.tag == 2)
        {
            if let cellPhone1 = dictAbout.value(forKey: "CellPhone1")
            {
                if("\(cellPhone1)".count > 0 && "\(cellPhone1)" != "<null>")
                {
                    self.displayMessageInterface(strNo: "\(cellPhone1)")
                }
            }
        }
        
    }
    
    @objc func actionOnTaskCheckMark(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        else
        {
            //let dict = (aryTasks.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            let dict = (arrayTasks.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
            callAPIToUpdateTaskMarkAsDone(dictData: dict ,tag: sender.tag)
            
        }
        
    }
    
    @objc func actionOnActvityComment(sender: UIButton!)
    {
        
        if(isInternetAvailable() == false)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            //indexPath.row*1000+indexPath.section
            
            let row = sender.tag/1000
            let section = sender.tag%1000
            
            let key = aryAllKeys[section - 1]
            var valueForKey = dictGroupedTimeline[key as! AnyHashable]
            
            let list = [Int](1...4)
            let arrayLoc = NSMutableArray()
            
            for (index, element) in list.enumerated() {
                if(index == 0){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                    
                }
                if(index == 1){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                
                if(index == 2){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
                if(index == 3){
                    
                    for item in valueForKey!{
                        
                        let dictLoc = item as NSDictionary
                        
                        if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                            
                            arrayLoc.add(dictLoc)
                        }
                        
                    }
                }
            }
            
            valueForKey = (arrayLoc as! [NSMutableDictionary])
            let dictData = valueForKey![row]
            if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                
                
                
                let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentList_iPhoneVC") as! CommentList_iPhoneVC
                
                controller.iD_PreviousView = "\(dictData.value(forKey: "ActivityId")!)"
                self.navigationController?.pushViewController(controller, animated: false)
                
            }
            
        }
        
    }
    
}

extension CompanyDetailsVC_CRMContactNew_iPhone:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch companyDetailsType {
        case .association:
            return 4
        case .details:
            return 1
        case .timeline:
            return aryAllKeys.count + 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch companyDetailsType {
        case .association:
            if(section == 0)
            {
                if let contacts = dictAssociations.value(forKey: "Contacts")
                {
                    if(contacts is NSArray)
                    {
                        return (contacts as! NSArray).count
                    }
                }
            }
            if(section == 1)
            {
                if let leads = dictAssociations.value(forKey: "Leads")
                {
                    if(leads is NSArray)
                    {
                        return (leads as! NSArray).count
                    }
                }
            }
            if(section == 2)
            {
                if let opportunity = dictAssociations.value(forKey: "Opportunities")
                {
                    if(opportunity is NSArray)
                    {
                        return (opportunity as! NSArray).count
                    }
                }
            }
            if(section == 3)
            {
                if let accounts = dictAssociations.value(forKey: "Accounts")
                {
                    if(accounts is NSArray)
                    {
                        return (accounts as! NSArray).count
                    }
                }
            }
        case .details:
            return arrayTitles.count
            
        case .timeline:
            if(section == 0){
                return arrayTasks.count
            }else{
                let key = aryAllKeys[section - 1]
                let valueForKey = dictGroupedTimeline[key as! AnyHashable]
                return (valueForKey?.count)!
            }
            
            
        case .tasks:
            return arrayTasks.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        viewHeader.backgroundColor = UIColor.groupTableViewBackground
        
        let lblTitle = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        //  lblTitle.backgroundColor = hexStringToUIColor(hex: "3E977A")
        lblTitle.textAlignment = .left
        lblTitle.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        lblTitle.textColor = UIColor.theme()
        let lblSaprator = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
        lblSaprator.backgroundColor = UIColor.white
        viewHeader.addSubview(lblSaprator)
        
        switch companyDetailsType {
        case .association:
            if(section == 0)
            {
                lblTitle.text = "Contact"
            }
            else if(section == 1)
            {
                lblTitle.text = "Leads"
            }
            else if(section == 2)
            {
                lblTitle.text = "Opportunity"
            }
            else if(section == 3)
            {
                lblTitle.text = "Account"
            }
            break
        case .details:
            
            lblTitle.text = "Details"
            break
            
        case .timeline:
            if(section == 0){
                lblTitle.text = "Open Task"
            }else{
                lblTitle.textColor = UIColor.black
                let key = aryAllKeys[section - 1]
                
                if((key as? Date) != nil)
                {
                    let strDate = changeDateToString(date: key as! Date)
                    
                    lblTitle.text = strDate
                    // viewHeader.backgroundColor = UIColor.white
                    
                }
            }
            
            break
            
        default:// task
            lblTitle.text = ""
        }
        
        viewHeader.addSubview(lblTitle)
        return viewHeader
        
    }
    
    /* func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
     {
     if companyDetailsType == .activities
     {
     let viewheader = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
     viewheader.backgroundColor = UIColor.lightColorForCell()
     
     if(section == 0)
     {
     
     let button = UIButton()
     button.frame = CGRect( x: tblviewContactDetails.frame.maxX - 100, y: 0, width: 100, height: 30)
     button.setTitle("View More...", for: .normal)
     button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
     button.addTarget(self, action: #selector(viewMoreTask), for: .touchUpInside)
     
     button.setTitleColor(UIColor.theme(), for: .normal)
     viewheader.addSubview(button)
     /*  if aryTasks.count > 0
     {
     viewheader.addSubview(button)
     }*/
     }
     else if(section == 1)
     {
     
     let button = UIButton()
     button.frame = CGRect( x: tblviewContactDetails.frame.maxX - 100, y: 0, width: 100, height: 30)
     button.setTitleColor(UIColor.theme(), for: .normal)
     button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
     
     button.addTarget(self, action: #selector(viewMoreActivity), for: .touchUpInside)
     
     button.setTitle("View More...", for: .normal)
     viewheader.addSubview(button)
     
     /*if aryActivities.count > 0
     {
     viewheader.addSubview(button)
     }*/
     
     }
     return viewheader
     }
     else
     {
     let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 2))
     viewFooter.backgroundColor = UIColor.lightColorForCell()
     
     return viewFooter
     }
     }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch companyDetailsType {
        case .association:
            if(section == 0){
                if let contacts = dictAssociations.value(forKey: "Contacts")
                {
                    if(contacts is NSArray)
                    {
                        if((contacts as! NSArray).count > 0)
                        {
                            return DeviceType.IS_IPAD ? 55 : 40
                        }
                        else
                        {
                            return 0
                        }
                    }
                    else{
                        return 0
                    }
                }
            }
            if(section == 1){
                if let leads = dictAssociations.value(forKey: "Leads")
                {
                    if(leads is NSArray)
                    {
                        if((leads as! NSArray).count > 0)
                        {
                            return DeviceType.IS_IPAD ? 55 : 40
                        }
                        else
                        {
                            return 0
                        }
                    }
                    else{
                        return 0
                    }
                }
            }
            
            if(section == 2){
                
                if let opportunities = dictAssociations.value(forKey: "Opportunities")
                {
                    if(opportunities is NSArray)
                    {
                        if((opportunities as! NSArray).count > 0)
                        {
                            return DeviceType.IS_IPAD ? 55 : 40
                        }
                        else
                        {
                            return 0
                        }
                    }
                    else{
                        return 0
                    }
                }
            }
            if(section == 3){
                
                if let accounts = dictAssociations.value(forKey: "Accounts")
                {
                    if(accounts is NSArray)
                    {
                        if((accounts as! NSArray).count > 0)
                        {
                            return DeviceType.IS_IPAD ? 55 : 40
                        }
                        else
                        {
                            return 0
                        }
                    }
                    else{
                        return 0
                    }
                }
            }
            return 0
        case .timeline:
            return DeviceType.IS_IPAD ? 55 : 40
            
        case .tasks:
            return 0
            
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        viewFooter.backgroundColor = UIColor.groupTableViewBackground
        let lblTitle = UILabel(frame: CGRect(x: 10, y: 0, width: self.view.frame.size.width, height: DeviceType.IS_IPAD ? 55 : 40))
        lblTitle.textAlignment = .left
        lblTitle.textColor = UIColor.theme()
        lblTitle.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 18)
        lblTitle.text = "History"
        lblTitle.numberOfLines = 0
        
        let lblSaprator = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
        lblSaprator.backgroundColor = UIColor.white
        viewFooter.addSubview(lblSaprator)
        
        let btnFilter = UIButton(type: .system)
        btnFilter.frame = CGRect(x: self.view.frame.size.width - 50, y: 0, width: 40, height: DeviceType.IS_IPAD ? 55 : 40)
        btnFilter.setImage(UIImage(named: "filter_CRM"), for: .normal)
        btnFilter.addTarget(self, action: #selector(goToFilterTimeLine), for: .touchUpInside)
        btnFilter.tintColor = UIColor.theme()
        
        switch companyDetailsType {
        case .timeline:
            if(section == 0){
                viewFooter.addSubview(lblTitle)
                viewFooter.addSubview(btnFilter)
                break
            }else{
                break
            }
        case .details: break
        case .association:break
        case .tasks:break
        }
        return viewFooter
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch companyDetailsType {
        case .timeline:
            if(section == 0){
                return DeviceType.IS_IPAD ? 55 : 40
            }else{
                return 0
            }
        case .details:
            return 0
        case .association:
            return 0
        case .tasks:
            return 0
        }
    }
    
    /* func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     switch companyDetailsType {
     case .activities:
     return 30
     case .associations:
     return 2
     default:
     return 2
     }
     }
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch companyDetailsType {
            
        case .association:
            if(indexPath.section == 0)// contact
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellContact_CompanyDetails") as! LeadVCCell
                
                let ary = dictAssociations.value(forKey: "Contacts") as! NSArray
                let dictData = ary[indexPath.row] as! NSDictionary
                cell.lblName.text = Global().strFullName(dictData as AnyHashable as? [AnyHashable : Any])
                if dictAbout.count > 0 {
                    cell.lblCompanyName.text = "\(dictAbout.value(forKey: "Name")!)"
                }
                cell.btnDeAssociateContact.tag = indexPath.row
                cell.btnDeAssociateContact.addTarget(self, action: #selector(actionOnDeAssociateContact), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
            }
            if(indexPath.section == 1)// leads
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellLeads_CompanyDetails") as! LeadVCCell
                let ary = dictAssociations.value(forKey: "Leads") as! NSArray
                let dict = ary[indexPath.row] as! NSDictionary
                if let leadName = dict.value(forKey: "LeadName")
                {
                    cell.lblLeadName.text = "\(leadName)"
                }
                if let companyName = dict.value(forKey: "CrmCompanyName")
                {
                    if("\(companyName)" == "<null>" || "\(companyName)".count == 0 || "\(companyName)" == " ")
                    {
                        cell.lblCompanyName.text = ""
                    }
                    else
                    {
                        cell.lblCompanyName.text = "\(companyName)"
                    }
                }
                
                cell.lblCustomerName.text = Global().strFullName(dict as? [AnyHashable : Any])
                
                if(cell.lblCustomerName.text?.count != 0){
                    cell.lblCustomerName.text = "  \u{2022}  " + Global().strFullName(dict as? [AnyHashable : Any])
                }
                
                if let assignTo = dict.value(forKey: "AssignedToName")
                {
                    if("\(assignTo)" == "<null>" || "\(assignTo)".count == 0 || "\(assignTo)" == " ")
                    {
                        cell.lblAssignTo.text = "N/A"
                    }
                    else
                    {
                        cell.lblAssignTo.text = "\(assignTo)"
                    }
                }
                cell.selectionStyle = .none
                return cell
            }
            if(indexPath.section == 2)// opportunity
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellOpportunity_CompanyDetails") as! LeadVCCell
                
                let ary = dictAssociations.value(forKey: "Opportunities") as! NSArray
                let dict = ary[indexPath.row] as! NSDictionary
                
                if let oppName = dict.value(forKey: "OpportunityName")
                {
                    if("\(oppName)" == "<null>" || "\(oppName)".count == 0 || "\(oppName)" == " ")
                    {
                        cell.lblAccountNumber.text = ""
                    }
                    else
                    {
                        cell.lblAccountNumber.text = "\(oppName)" + "  \u{2022}  "
                    }
                }
                cell.lblOpportunityNumber.text = ""
                
                if let stage = dict.value(forKey: "OpportunityStage")
                {
                    if("\(stage)" == "<null>" || "\(stage)".count == 0 || "\(stage)" == " ")
                    {
                        cell.lblOpportunityStage.text = ""
                    }
                    else
                    {
                        cell.lblOpportunityStage.text = "\(stage)"
                    }
                }
                
                if let companyName = dict.value(forKey: "CrmCompanyName")
                {
                    if("\(companyName)" == "<null>" || "\(companyName)".count == 0 || "\(companyName)" == " ")
                    {
                        cell.lblCompanyName.text = ""
                    }
                    else
                    {
                        cell.lblCompanyName.text = "\(companyName)  \u{2022}  "
                    }
                    
                }
                cell.lblAccContactName.text =   Global().strFullName(dict as? [AnyHashable : Any])
                if(cell.lblAccContactName.text?.count != 0){
                    cell.lblAccContactName.text =   Global().strFullName(dict as? [AnyHashable : Any]) + "  \u{2022}  "
                }
                
                if let confidenceLevel = dict.value(forKey: "ConfidenceLevel")
                {
                    if("\(confidenceLevel)" == "<null>" || "\(confidenceLevel)".count == 0 || "\(confidenceLevel)" == " ")
                    {
                        cell.lblConfidenceLevel.text =  "0%  \u{2022}  "
                    }
                    else
                    {
                        cell.lblConfidenceLevel.text =  "\(confidenceLevel)%  \u{2022}  "
                    }
                    
                }
                
                if let proposedAmt = dict.value(forKey: "ProposedAmount")
                {
                    if("\(proposedAmt)" == "<null>" || "\(proposedAmt)".count == 0 || "\(proposedAmt)" == " ")
                    {
                        cell.lblProposedAmount.text = "$0"
                    }
                    else
                    {
                        cell.lblProposedAmount.text =  "$\(proposedAmt)"
                    }
                }
                
                
                
                cell.selectionStyle = .none
                
                return cell
            }
            
            if(indexPath.section == 3)// accounts
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellAccount_CompanyDetails") as! LeadVCCell
                
                let ary = dictAssociations.value(forKey: "Accounts") as! NSArray
                
                let dict = ary[indexPath.row] as! NSDictionary
                
                if let accountNo = dict.value(forKey: "AccountNo")
                {
                    if("\(accountNo)" == "<null>" || "\(accountNo)".count == 0 || "\(accountNo)" == " ")
                    {
                        cell.lblAccountNumber.text = "Account #:"
                        
                    }
                    else
                    {
                        cell.lblAccountNumber.text = "Account #: \(accountNo)"
                    }
                }
                if let companyName = dict.value(forKey: "CrmCompanyName")
                {
                    if("\(companyName)" == "<null>" || "\(companyName)".count == 0 || "\(companyName)" == " ")
                    {
                        cell.lblCompanyName.text = ""
                    }
                    else
                    {
                        cell.lblCompanyName.text = "\(companyName)"
                    }
                }
                cell.selectionStyle = .none
                
                return cell
            }
            
        case .details:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellDetails_CompanyDetails") as! LeadVCCell
            
            cell.lblDetailTitle.text = arrayTitles[indexPath.row]
            cell.constWidthCallBtnContainer.constant = DeviceType.IS_IPAD ? 100 : 80.0
            cell.btnMessage.isHidden = true
            cell.btnCall.isHidden = true
            cell.btnCall.setImage(UIImage(named: "call_2_CRM"), for: .normal)
            cell.btnCall.tag = indexPath.row
            cell.btnMessage.tag = indexPath.row
            cell.btnMessage.setImage(UIImage(named: "chat_2_CRM"), for: .normal)//chat_2_CRM
            cell.btnCall.addTarget(self, action: #selector(actionOnCalling), for: .touchUpInside)
            cell.btnMessage.addTarget(self, action: #selector(actionOnMessage), for: .touchUpInside)
            
            if(indexPath.row == 0)
            {
                cell.lblDetail.text = " "
                if let primaryPhone = dictAbout.value(forKey: "PrimaryPhone")
                {
                    if("\(primaryPhone)".count > 0 && "\(primaryPhone)" != "<null>")
                    {
                        cell.btnMessage.isHidden = false
                        cell.btnCall.isHidden = false
                        
                        var formattedPrimaryPhoneNo = getOnlyNumberFromString(number: primaryPhone as! String)
                        formattedPrimaryPhoneNo = formattedNumber(number: formattedPrimaryPhoneNo)
                        
                        cell.lblDetail.text = "\(formattedPrimaryPhoneNo)"
                    }
                }
            }
            if(indexPath.row == 1)
            {
                cell.lblDetail.text = " "
                if let secondaryPhone = dictAbout.value(forKey: "SecondaryPhone")
                {
                    if("\(secondaryPhone)".count > 0 && "\(secondaryPhone)" != "<null>")
                    {
                        cell.btnMessage.isHidden = false
                        cell.btnCall.isHidden = false
                        
                        var formattedSecondaryPhoneNo = getOnlyNumberFromString(number: secondaryPhone as! String)
                        formattedSecondaryPhoneNo = formattedNumber(number: formattedSecondaryPhoneNo)
                        
                        cell.lblDetail.text = "\(formattedSecondaryPhoneNo)"
                    }
                }
                
            }
            if(indexPath.row == 2)
            {
                cell.lblDetail.text = " "
                if let cellPhone1 = dictAbout.value(forKey: "CellPhone1")
                {
                    if("\(cellPhone1)".count > 0 && "\(cellPhone1)" != "<null>")
                    {
                        cell.btnMessage.isHidden = false
                        cell.btnCall.isHidden = false
                        
                        var formattedCellPhoneNo = getOnlyNumberFromString(number: cellPhone1 as! String)
                        formattedCellPhoneNo = formattedNumber(number: formattedCellPhoneNo)
                        
                        cell.lblDetail.text = "\(formattedCellPhoneNo)"
                    }
                }
            }
            if(indexPath.row == 3)
            {
                cell.lblDetail.text = " "
                
                if let primaryEmail = dictAbout.value(forKey: "PrimaryEmail")
                {
                    if("\(primaryEmail)".count > 0 && "\(primaryEmail)" != "<null>")
                    {
                        cell.btnCall.isHidden = false
                        cell.lblDetail.text = "\(primaryEmail)"
                        cell.btnCall.setImage(UIImage(named: "mail_2_CRM"), for: .normal)
                    }
                }
            }
            
            if(indexPath.row == 4)
            {
                cell.btnMessage.isHidden = true
                cell.lblDetail.text = " "
                if let secondaryEmail = dictAbout.value(forKey: "SecondaryEmail")
                {
                    if("\(secondaryEmail)".count > 0 && "\(secondaryEmail)" != "<null>")
                    {
                        cell.btnCall.isHidden = false
                        cell.lblDetail.text = "\(secondaryEmail)"
                        cell.btnCall.setImage(UIImage(named: "mail_2_CRM"), for: .normal)
                    }
                }
            }
            if(indexPath.row == 5)
            {
                cell.constWidthCallBtnContainer.constant = 80.0
                cell.lblDetail.text = " "
                if let website = dictAbout.value(forKey: "Website")
                {
                    if("\(website)".count > 0 && "\(website)" != "<null>")
                    {
                        cell.btnCall.isHidden = false
                        cell.lblDetail.text = "\(website)"
                        cell.btnCall.setImage(UIImage(named: "website_CRM"), for: .normal)
                    }
                }
            }
            if(indexPath.row == 6)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                cell.lblDetail.text = " "
                if let reportTo = dictAbout.value(forKey: "ReportToContactFullName")
                {
                    if("\(reportTo)".count > 0 && "\(reportTo)" != "<null>")
                    {
                        cell.lblDetail.text = "\(reportTo)"
                    }
                }
            }
            if(indexPath.row == 7)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                cell.lblDetail.text = " "
                if let owner = dictAbout.value(forKey: "OwnerFullName")
                {
                    if("\(owner)".count > 0 && "\(owner)" != "<null>")
                    {
                        cell.lblDetail.text = "\(owner)"
                    }
                }
            }
            if(indexPath.row == 8)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                cell.lblDetail.text = " "
                if let sourceId = dictAbout.value(forKey: "SourceId")
                {
                    if("\(sourceId)".count > 0 && "\(sourceId)" != "<null>")
                    {
                        cell.lblDetail.text = getSourceNameFromId(sourceId: "\(sourceId)")
                    }
                }
            }
            if(indexPath.row == 9)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                if let employeeCount = dictAbout.value(forKey: "EmployeeCount")
                {
                    if("\(employeeCount)".count > 0 && "\(employeeCount)" != "<null>")
                    {
                        cell.lblDetail.text = "\(employeeCount)"
                    }
                }
            }
            if(indexPath.row == 10)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                if let annualRevenue = dictAbout.value(forKey: "AnnualRevenue")
                {
                    if("\(annualRevenue)".count > 0 && "\(annualRevenue)" != "<null>")
                    {
                        cell.lblDetail.text = "\(annualRevenue)"
                    }
                }
            }
            if(indexPath.row == 11)
            {
                cell.constWidthCallBtnContainer.constant = 0.0
                if let description = dictAbout.value(forKey: "Description")
                {
                    if("\(description)".count > 0 && "\(description)" != "<null>")
                    {
                        cell.lblDetail.text = "\(description)"
                    }
                }
            }
            return cell
            
        case .timeline:
            if(indexPath.section == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew_CompanyDetails") as! LeadVCCell
                
                let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
                
                cell.btnTaskCheckMark.tag = indexPath.row
                cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
                
                cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
                cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
                cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
                
                if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                {
                    cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                    
                }
                else
                {
                    cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                    
                }
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                // TaskTypeId
                
                let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                
                cell.imgViewTaskType.image = UIImage(named: strImageName)
                
                if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>"){
                    cell.imgViewTaskType.isHidden = false
                }
                else{
                    cell.imgViewTaskType.isHidden = true
                }
                
                return cell
            }else{
                let key = aryAllKeys[indexPath.section - 1]
                var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                
                let list = [Int](1...4)
                let arrayLoc = NSMutableArray()
                
                for (index, element) in list.enumerated() {
                    if(index == 0){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                        
                    }
                    if(index == 1){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                    
                    if(index == 2){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                    if(index == 3){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                }
                
                valueForKey = (arrayLoc as! [NSMutableDictionary])
                let dictData = valueForKey![indexPath.row]
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellNotesNew_CompanyDetails") as! LeadVCCell
                    cell.lblNoteName.text = ""
                    
                    if let noteName = dictData.value(forKey: "Note"){
                        
                        cell.lblNoteName.text = "Note - \(noteName)"
                    }
                    return cell
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellActivity_CompanyDetails") as! LeadVCCell
                    
                    if let agenda = dictData.value(forKey: "Agenda"){
                        cell.lblActivityName.text = "\(agenda)"
                    }else{
                        cell.lblActivityName.text = ""
                    }
                    
                    
                    cell.lblLogType.text = ""
                    for item in aryLogType
                    {
                        if("\(dictData.value(forKey: "LogTypeId")!)" == "\((item as! NSDictionary).value(forKey: "LogTypeId")!)" )
                        {
                            cell.lblLogType.text = "Activity Type: \((item as! NSDictionary).value(forKey: "Name")!)"  + "  \u{2022}  "
                            
                            break
                        }
                    }
                    if let clientCreatedDate = dictData.value(forKey: "ClientCreatedDate"){
                        
                        cell.lblDate.text = changeDateToString(date: clientCreatedDate as! Date)
                        if(cell.lblDate.text!.count != 0){
                            cell.lblDate.text = changeDateToString(date: clientCreatedDate as! Date) + "  \u{2022}  "
                        }
                        
                    }else{
                        cell.lblDate.text = ""
                    }
                    //indexPath.row*1000+indexPath.section
                    cell.btnActivityComment.tag = indexPath.row*1000+indexPath.section
                    cell.btnActivityComment.addTarget(self, action: #selector(actionOnActvityComment), for: .touchUpInside)
                    
                    return cell
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew_CompanyDetails_TimeLine") as! LeadVCCell
                    
                    if let taskname = dictData.value(forKey: "TaskName"){
                        cell.lblTaskName.text = "\(taskname)"
                    }else{
                        cell.lblTaskName.text = ""
                    }
                    
                    if let companyName = dictData.value(forKey: "CompanyName"){
                        
                        if("\(companyName)" == "<null>")
                        {
                            cell.lblCompanyName.text = ""
                        }
                        else
                        {
                            cell.lblCompanyName.text = "\(companyName)"  + "  \u{2022}  "
                        }
                    }
                    else
                    {
                        cell.lblCompanyName.text = ""
                    }
                    
                    if let assignTo = dictData.value(forKey: "AssignedToStr"){
                        
                        if("\(assignTo)" == "<null>")
                        {
                            cell.lblAssignTo.text = ""
                        }
                        else
                        {
                            cell.lblAssignTo.text = "\(assignTo)"
                        }
                    }
                    else
                    {
                        cell.lblAssignTo.text = ""
                    }
                    
                    // TaskTypeId
                    
                    let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
                    
                    cell.imgViewTaskType.image = UIImage(named: strImageName)
                    
                    if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>"){
                        cell.imgViewTaskType.isHidden = false
                    }
                    else{
                        cell.imgViewTaskType.isHidden = true
                    }
                    
                    return cell
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellEmail_CompanyDetails") as! LeadVCCell
                    
                    if let emailSubject = dictData.value(forKey: "EmailSubject"){
                        
                        if("\(emailSubject)" == "<null>")
                        {
                            cell.lblSubject.text = ""
                        }
                        else
                        {
                            cell.lblSubject.text = "Subject: \(emailSubject)"
                        }
                    }
                    else
                    {
                        cell.lblSubject.text = ""
                    }
                    
                    if let emailFrom = dictData.value(forKey: "EmailFrom"){
                        
                        if("\(emailFrom)" == "<null>")
                        {
                            cell.lblEmailFrom.text = ""
                        }
                        else
                        {
                            cell.lblEmailFrom.text = "From: \(emailFrom)"
                        }
                    }
                    else
                    {
                        cell.lblEmailFrom.text = ""
                    }
                    
                    if let emailContent = dictData.value(forKey: "EmailNotes"){
                        
                        if("\(emailContent)" == "<null>")
                        {
                            cell.constHghtTxtviewEmailContent.constant = 0.0
                            cell.txtviewEmailContent.text = ""
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                
                                let noSpaceAttributedString =
                                    htmlAttributedString(strHtmlString: "\(emailContent)").trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
                                
                                cell.txtviewEmailContent.attributedText = noSpaceAttributedString
                                cell.constHghtTxtviewEmailContent.constant = 100.0
                            }
                        }
                    }
                    else
                    {
                        cell.txtviewEmailContent.text = ""
                        cell.constHghtTxtviewEmailContent.constant = 100.0
                    }
                    
                    cell.txtviewEmailContent.textContainerInset = .zero
                    cell.txtviewEmailContent.textContainer.lineFragmentPadding = 0
                    cell.txtviewEmailContent.layer.cornerRadius = 2.0
                    cell.txtviewEmailContent.layer.borderWidth = 0.5
                    cell.txtviewEmailContent.layer.borderColor = UIColor.lightGray.cgColor
                    
                    return cell
                }
                
            }
            
        case .tasks:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeadVCCellTaskNew_CompanyDetails") as! LeadVCCell
            
            let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
            
            cell.btnTaskCheckMark.tag = indexPath.row
            cell.btnTaskCheckMark.addTarget(self, action: #selector(actionOnTaskCheckMark), for: .touchUpInside)
            
            cell.lblTaskName.text = "\(dictData.value(forKey: "TaskName") ?? "")"
            cell.lblDueDate.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")//
            cell.lblDueTime.text = changeStringDateToGivenFormat(strDate: "\(dictData.value(forKey: "DueDate") ?? "")", strRequiredFormat: "hh:mm a")
            
            if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "uncheckNewCRM"), for: .normal)
                
            }
            else
            {
                cell.btnTaskCheckMark.setImage(UIImage(named: "checkNewCRM"), for: .normal)
                
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            // TaskTypeId
            
            let strImageName = getIconNameViaTaskTypeId(strId: "\(dictData.value(forKey: "TaskTypeId") ?? "")")
            
            cell.imgViewTaskType.image = UIImage(named: strImageName)
            
            if("\(dictData.value(forKey: "TaskTypeId") ?? "")".count > 0 && "\(dictData.value(forKey: "TaskTypeId") ?? "")" != "<null>"){
                cell.imgViewTaskType.isHidden = false
            }
            else{
                cell.imgViewTaskType.isHidden = true
            }
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch companyDetailsType {
        case .association:
            return UITableView.automaticDimension
        case .details:
            
            if(indexPath.row == 11)
            {
                if let description = dictAbout.value(forKey: "Description"){
                    if("\(description)".count > 0 && "\(description)" != "<null>"){
                        
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 0)
            {
                if let primaryPhone = dictAbout.value(forKey: "PrimaryPhone")
                {
                    if("\(primaryPhone)".count > 0 && "\(primaryPhone)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 1)
            {
                if let secondaryPhone = dictAbout.value(forKey: "SecondaryPhone")
                {
                    if("\(secondaryPhone)".count > 0 && "\(secondaryPhone)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 2)
            {
                if let cellPhone1 = dictAbout.value(forKey: "CellPhone1")
                {
                    if("\(cellPhone1)".count > 0 && "\(cellPhone1)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 3)
            {
                
                if let primaryEmail = dictAbout.value(forKey: "PrimaryEmail")
                {
                    if("\(primaryEmail)".count > 0 && "\(primaryEmail)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            
            if(indexPath.row == 4)
            {
                if let secondaryEmail = dictAbout.value(forKey: "SecondaryEmail")
                {
                    if("\(secondaryEmail)".count > 0 && "\(secondaryEmail)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 5)
            {
                if let website = dictAbout.value(forKey: "Website")
                {
                    if("\(website)".count > 0 && "\(website)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 6)
            {
                if let reportTo = dictAbout.value(forKey: "ReportToContactFullName")
                {
                    if("\(reportTo)".count > 0 && "\(reportTo)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 7)
            {
                if let owner = dictAbout.value(forKey: "OwnerFullName")
                {
                    if("\(owner)".count > 0 && "\(owner)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 8)
            {
                if let sourceId = dictAbout.value(forKey: "SourceId")
                {
                    if("\(sourceId)".count > 0 && "\(sourceId)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 9)
            {
                if let employeeCount = dictAbout.value(forKey: "EmployeeCount")
                {
                    if("\(employeeCount)".count > 0 && "\(employeeCount)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            if(indexPath.row == 10)
            {
                if let annualRevenue = dictAbout.value(forKey: "AnnualRevenue")
                {
                    if("\(annualRevenue)".count > 0 && "\(annualRevenue)" != "<null>")
                    {
                        return UITableView.automaticDimension
                    }
                    return 0
                }
                return 0
            }
            return UITableView.automaticDimension
        case .timeline:
            if(indexPath.section == 0){
                let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
                
                if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
                {
                    return UITableView.automaticDimension
                }
                else
                {
                    return 0
                }
            }else{
                return UITableView.automaticDimension
            }
        case .tasks:
            let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
            
            if("\(dictData.value(forKey: "Status")!)" == "Open" || "\(dictData.value(forKey: "Status")!)" == "open")
            {
                return UITableView.automaticDimension
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        switch companyDetailsType {
            
        case .association:
            
            if(indexPath.section == 0)// company
            {
                
                let ary = dictAssociations.value(forKey: "Contacts") as! NSArray
                
                let dictData = ary[indexPath.row] as! NSDictionary
                
                goToContactDetails(dictContact: dictData)
                
            }
            if(indexPath.section == 1)// leads
            {
                
                let ary = dictAssociations.value(forKey: "Leads") as! NSArray
                
                let dict = ary[indexPath.row] as! NSDictionary
                
                goToWebLeadDetails(strId: "\(dict.value(forKey: "LeadId") ?? "")")
                
            }
            if(indexPath.section == 2)// opportunity
            {
                
                let ary = dictAssociations.value(forKey: "Opportunities") as! NSArray
                
                let dict = ary[indexPath.row] as! NSDictionary
                
                goToOpportunityDetails(strId: "\(dict.value(forKey: "OpportunityId") ?? "")")
                
            }
            
        case .timeline:
            if(indexPath.section == 0){
                let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
                self.goToTaskDetails(strId: "\(dictData.value(forKey: "LeadTaskId")!)")
            }else{
                let key = aryAllKeys[indexPath.section - 1]
                var valueForKey = dictGroupedTimeline[key as! AnyHashable]
                
                let list = [Int](1...4)
                let arrayLoc = NSMutableArray()
                
                for (index, element) in list.enumerated() {
                    if(index == 0){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Task"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                    if(index == 1){
                        for item in valueForKey!{
                            let dictLoc = item as NSDictionary
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Note"){
                                arrayLoc.add(dictLoc)
                            }
                        }
                    }
                    
                    if(index == 2){
                        for item in valueForKey!{
                            let dictLoc = item as NSDictionary
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Activity"){
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                    if(index == 3){
                        
                        for item in valueForKey!{
                            
                            let dictLoc = item as NSDictionary
                            
                            if("\(dictLoc.value(forKey: "EntityType")!)" == "Email"){
                                
                                arrayLoc.add(dictLoc)
                            }
                            
                        }
                    }
                }
                valueForKey = (arrayLoc as! [NSMutableDictionary])
                
                let dictData = valueForKey![indexPath.row]
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Note"){
                    
                    
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Activity"){
                    
                    self.goToActivityDetails(strId: "\(dictData.value(forKey: "ActivityId")!)")//ActivityId
                    
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Task"){
                    
                    self.goToTaskDetails(strId: "\(dictData.value(forKey: "LeadTaskId")!)")
                    
                }
                
                if("\(dictData.value(forKey: "EntityType")!)" == "Email"){
                    
                    self.goToEmailDetails(dictEmail: dictData)
                    
                }
            }
            
            
        case .tasks:
            
            let dictData = arrayTasks.object(at: indexPath.row) as! NSDictionary
            self.goToTaskDetails(strId: "\(dictData.value(forKey: "LeadTaskId")!)")
            
        default: break
            
        }
        
    }
    
}

extension CompanyDetailsVC_CRMContactNew_iPhone : FilterTimeLineData
{
    
    func getFilterTimeLineDataOnSelection(dictData: NSMutableDictionary, strType: String) {
        
        
        if(isInternetAvailable() == false)
        {
            
            tblviewCompanyDetails.reloadData()
            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }
        else
        {
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.callAPToGetTimeLine()
                
            }
            
        }
        
    }
    
}

extension CompanyDetailsVC_CRMContactNew_iPhone : CXCallObserverDelegate{
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasConnected {
               print("Call Connect -> \(call.uuid)")
           }

           if call.isOutgoing {
               print("Call outGoing \(call.uuid)")
           }

           if call.hasEnded {
                self.goToLogAsActivity()
               print("Call hasEnded \(call.uuid)")
           }

           if call.isOnHold {
               print("Call onHold \(call.uuid)")
             }
         }
       
}

