//
//  UITextField+textFieldSettings.m
//  DPS
//
//  Created by Saavan Patidar on 11/05/18.
//  Copyright © 2018 Saavan. All rights reserved.
//

#import "UITextField+textFieldSettings.h"

@implementation UITextField (textFieldSettings)

- (BOOL)canPerformAction:(SEL) action withSender:(id) sender
{
    if (action == @selector(paste:))
    {
        return YES;
    }
    
    return [super canPerformAction:action withSender:sender];
}

@end
