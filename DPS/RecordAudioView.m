//
//  RecordAudioView.m
//  DPS
//  safhsadhfaghjfg
//  Created by Rakesh Jain on 23/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import "RecordAudioView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "DPS-Swift.h"

@interface RecordAudioView ()
{
    Global *global;
    MPMoviePlayerController *movieplayer;
    NSString *strAudioName;
}

@end

@implementation RecordAudioView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _Btn_Stop.enabled = NO;
    _Btn_Record.enabled = YES;
    _lbl_Title.text=@"To start recording press ""Record"" button, to stop recording press ""Stop"" button.";
    //============================================================================
    //============================================================================

    NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
    [formatterDate setDateFormat:@"MMddyyyy"];
    [formatterDate setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strDate = [formatterDate stringFromDate:[NSDate date]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *strTime = [formatter stringFromDate:[NSDate date]];
    
    strAudioName = [NSString stringWithFormat:@"Audio%@%@.mp4",strDate,strTime];
    
    if ([_strFromWhere isEqualToString:@"PestConditionDocument"])
    {
        
        strAudioName = [NSString stringWithFormat:@"AudioC%@%@.mp4",strDate,strTime];
        
    }
    else if ([_strFromWhere isEqualToString:@"PestDocument"]){
        
        strAudioName = [NSString stringWithFormat:@"AudioW%@%@.mp4",strDate,strTime];
        
    }

    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSURL *tmpFileUrl = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:strAudioName]];
    
//    if ([_strFromWhere isEqualToString:@"ServiceInvoice"]) {
//        
//        NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"\\\\Documents\\\\UploadAudio\\\\%@",strAudioName];
//        tmpFileUrl = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:strAudioNameServiceNew]];
//        
//    }
    
    
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithFloat:16000.0], AVSampleRateKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    nil];
    NSError *errorr = nil;
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:tmpFileUrl settings:recordSettings error:&errorr];
    [_audioRecorder prepareToRecord];
    _audioRecorder.delegate = self;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session setActive:YES error:nil];

    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actionBack:(id)sender {
    if (twoMinTimer) {
        
        [twoMinTimer invalidate];
        [_audioRecorder pause];

        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Info."
                                   message:@"Are you sure to discard Recording"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  [self timerStarAfterPause];
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 [twoMinTimer invalidate];
                                 NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                                 [defs setBool:YES forKey:@"isRecorder"];
                                 [defs synchronize];
                                 [self dismissViewControllerAnimated:NO completion:nil];
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [twoMinTimer invalidate];
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setBool:YES forKey:@"isRecorder"];
        [defs synchronize];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
- (IBAction)action_Record:(id)sender {
    if (!_audioRecorder.recording)
    {
        [self timerStarthere];
        _Btn_Stop.enabled = YES;
        _Btn_Record.enabled=NO;
        _lbl_Title.text=@"Speak Now.";
        [_audioRecorder record];
    }
}

- (IBAction)action_Stop:(id)sender {
    [twoMinTimer invalidate];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"yesAudio"];
    [defaults setBool:YES forKey:@"isRecorder"];
    if ([_strFromWhere isEqualToString:@"ServiceInvoice"]) {
       // \\Documents\\UploadImages\\
        
        NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"\\Documents\\UploadAudio\\%@",strAudioName];
        [defaults setValue:strAudioNameServiceNew forKey:@"AudioNameService"];
    }
    else if ([_strFromWhere isEqualToString:@"SalesInvoice"])
    {
        // \\Documents\\UploadImages\\
        
        //NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"\\Documents\\UploadAudio\\%@",strAudioName];
        
        NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"UploadAudio\\%@",strAudioName];
        [defaults setValue:strAudioNameServiceNew forKey:@"AudioNameService"];
    }
    else if ([_strFromWhere isEqualToString:@"PestConditionDocument"])
    {
        
        [defaults setValue:strAudioName forKey:@"AudioNameDocument"];
        [defaults setBool:YES forKey:@"yesAudioDocument"];

    }
    else if ([_strFromWhere isEqualToString:@"PestDocument"]){
        
        [defaults setValue:strAudioName forKey:@"AudioNameDocument"];
        [defaults setBool:YES forKey:@"yesAudioDocument"];
        
    }
    else if ([_strFromWhere isEqualToString:@"AddDocumentNewCRM"]){
        
        [defaults setValue:strAudioName forKey:@"AudioName"];
        [defaults setBool:YES forKey:@"yesAudioDocument"];
        
    }
    else {
        [defaults setValue:strAudioName forKey:@"AudioName"];
    }
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Audio recorded successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)timerStarthere

{
    totalsecond=60;
    _lbl_Timer.text=[NSString stringWithFormat:@"%d Sec",totalsecond];
    twoMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(timer)
                                                 userInfo:nil
                                                  repeats:YES];
}

-(void)timerStarAfterPause
{
     [_audioRecorder record];
    _lbl_Timer.text=[NSString stringWithFormat:@"%d Sec",totalsecond];
    twoMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(timer)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (void)timer {
   totalsecond--;
   _lbl_Timer.text =[NSString stringWithFormat:@"%d Sec",totalsecond];
   if (  totalsecond == 0 ) {
       
       [twoMinTimer invalidate];
       
       NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       [defaults setBool:YES forKey:@"yesAudio"];
       [defaults setBool:YES forKey:@"isRecorder"];
       if ([_strFromWhere isEqualToString:@"ServiceInvoice"]) {
           NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"\\Documents\\UploadAudio\\%@",strAudioName];
           [defaults setValue:strAudioNameServiceNew forKey:@"AudioNameService"];
       }
       else if ([_strFromWhere isEqualToString:@"SalesInvoice"])
       {
           // \\Documents\\UploadImages\\
           
           //NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"\\Documents\\UploadAudio\\%@",strAudioName];
           
           NSString *strAudioNameServiceNew=[NSString stringWithFormat:@"UploadAudio\\%@",strAudioName];
           [defaults setValue:strAudioNameServiceNew forKey:@"AudioNameService"];
       }
       else {
           [defaults setValue:strAudioName forKey:@"AudioName"];
       }
       [defaults synchronize];
       
       [self dismissViewControllerAnimated:YES completion:nil];
       
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Audio recorded successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       [alert show];
   }
}
-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"Decode Error occurred");
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    NSLog(@"Finished recording");
}

-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder error:(NSError *)error
{
    NSLog(@"Encode Error occurred");
}

@end
