//
//  GeneralInfoImagesCollectionViewCell.h
//  DPS
//  
//  Created by User on 06/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralInfoImagesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageBefore;
@property (weak, nonatomic) IBOutlet UILabel *lblCpationOne;
@property (weak, nonatomic) IBOutlet UILabel *lblCaptionTwo;
@end
