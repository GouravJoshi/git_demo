//
//  ImageDetailsServiceAuto+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 02/12/17.
//
//

#import "ImageDetailsServiceAuto+CoreDataProperties.h"

@implementation ImageDetailsServiceAuto (CoreDataProperties)

+ (NSFetchRequest<ImageDetailsServiceAuto *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ImageDetailsServiceAuto"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic imageCaption;
@dynamic imageDescription;
@dynamic latitude;
@dynamic longitude;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic woImageId;
@dynamic woImagePath;
@dynamic woImageType;
@dynamic workorderId;
@dynamic isProblemIdentifaction;
@dynamic graphXmlPath;
@dynamic isNewGraph;

@end
