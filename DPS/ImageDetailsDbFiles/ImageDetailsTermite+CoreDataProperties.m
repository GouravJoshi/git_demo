//
//  ImageDetailsTermite+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 02/12/17.
//
//

#import "ImageDetailsTermite+CoreDataProperties.h"

@implementation ImageDetailsTermite (CoreDataProperties)

+ (NSFetchRequest<ImageDetailsTermite *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ImageDetailsTermite"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic imageCaption;
@dynamic imageDescription;
@dynamic latitude;
@dynamic longitude;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
//@dynamic woImageId;
@dynamic woImagePath;
@dynamic woImageType;
@dynamic workorderId;
@dynamic woTexasTermiteImageId;
@dynamic isNewGraph;
@end
