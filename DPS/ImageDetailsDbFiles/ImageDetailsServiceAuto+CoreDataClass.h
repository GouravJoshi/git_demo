//
//  ImageDetailsServiceAuto+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 02/12/17.
//
////

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageDetailsServiceAuto : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ImageDetailsServiceAuto+CoreDataProperties.h"
