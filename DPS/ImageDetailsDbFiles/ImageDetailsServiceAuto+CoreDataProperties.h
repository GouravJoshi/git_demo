//
//  ImageDetailsServiceAuto+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 02/12/17.
//
//

#import "ImageDetailsServiceAuto+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ImageDetailsServiceAuto (CoreDataProperties)

+ (NSFetchRequest<ImageDetailsServiceAuto *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *imageCaption;
@property (nullable, nonatomic, copy) NSString *imageDescription;
@property (nullable, nonatomic, copy) NSString *latitude;
@property (nullable, nonatomic, copy) NSString *longitude;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *woImageId;
@property (nullable, nonatomic, copy) NSString *woImagePath;
@property (nullable, nonatomic, copy) NSString *woImageType;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *isProblemIdentifaction;
@property (nullable, nonatomic, copy) NSString *graphXmlPath;//isNewGraph
@property (nullable, nonatomic, copy) NSString *isNewGraph;

@end

NS_ASSUME_NONNULL_END
