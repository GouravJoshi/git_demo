//
//  ChangePasswordView.h
//  DPS
//
//  Created by Rakesh Jain on 01/07/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordView : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txt_OldPassword;
@property (strong, nonatomic) IBOutlet UITextField *txt_NewPassword;
@property (strong, nonatomic) IBOutlet UITextField *txt_ConfirmPassword;
-(IBAction)hideKeyBoard:(id)sender;
- (IBAction)action_Back:(id)sender;
- (IBAction)action_ChangePassword:(id)sender;
@end
