//
//  HistoryTableViewCell.h
//  DPS
//
//  Created by Rakesh Jain on 29/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//  Saavan Patidar
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_LeadName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_LeadStatus;
@property (strong, nonatomic) IBOutlet UILabel *lbl_AccountNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_TechNote;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Date;

@end
