//
//  ServiceNotesHistoryViewController.h
//  DPS
//
//  Created by Saavan Patidar on 13/12/17.
//  Copyright © 2017 Saavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceNotesHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblData;
- (IBAction)actionOnBack:(id)sender;
@property(strong,nonatomic)NSString *strTypeOfService;
@property (strong, nonatomic) IBOutlet UILabel *lblNotesHistory;
@property (strong, nonatomic)  NSString *strWoId;
@property (strong, nonatomic)  NSString *strWorkOrderNo;
@property (strong, nonatomic)  NSString *isFromWDO;

@end
