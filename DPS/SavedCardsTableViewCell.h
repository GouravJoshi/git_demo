//
//  SavedCardsTableViewCell.h
//  DPS
//
//  Created by Saavan Patidar on 04/05/17.
//  Copyright © 2017 Saavan. All rights reserved.
//
//  Saavan Patidar 2021

#import <UIKit/UIKit.h>

@interface SavedCardsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnSelectCards;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldAmount;

@end
