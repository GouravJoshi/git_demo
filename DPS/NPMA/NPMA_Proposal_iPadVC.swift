//
//  NPMA_Proposal_iPadVC.swift
//  DPS
//  Created by Akshay Hastekar on 13/10/20.
//  Copyright © 2020 Saavan. All rights reserved.


import UIKit

import UIKit

enum ServiceType
{
    case standard, customized
    
}

class NPMA_Proposal_iPadVC: UIViewController {
    
    //MARK:
    //MARK: -------------IBOutlet --------------
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var segment: UISegmentedControl!
  
    @IBOutlet weak var btnSaveAndContinue: UIButton!
    // ---General Des
    @IBOutlet weak var viewInspectionDetail: CardView!
    @IBOutlet weak var txtBlockEqual: UITextField!
    @IBOutlet weak var txtApproxSqft: UITextField!
    @IBOutlet weak var txtLinerft: UITextField!
    @IBOutlet weak var txtOfStories: UITextField!
    @IBOutlet weak var txtOrignalTreatmentDate: UITextField!
    @IBOutlet weak var txtInspectedBy: UITextField!
    @IBOutlet weak var txtInspectedBDate: UITextField!
    @IBOutlet weak var txtStructureType: UITextField!
    @IBOutlet weak var txtWorkCompletedBy: UITextField!
    @IBOutlet weak var txtWorkCompletedDate: UITextField!
    @IBOutlet weak var txtSlab: UITextField!
    @IBOutlet weak var btn_PerimeterPluse: UIButton!
    @IBOutlet weak var btn_HP: UIButton!
    @IBOutlet weak var txtCrawlDoorHeight: UITextField!
    @IBOutlet weak var btn_Inside: UIButton!
    @IBOutlet weak var btn_OutSide: UIButton!
    @IBOutlet weak var btn_RemoveWoodYes: UIButton!
    @IBOutlet weak var btn_RemoveWoodNo: UIButton!
    @IBOutlet weak var txtBrickVeneer: UITextField!
    @IBOutlet weak var txtHollowBlock: UITextField!
    @IBOutlet weak var txtStoneVeneer: UITextField!
    @IBOutlet weak var txtPorches: UITextField!
    @IBOutlet weak var txtGarege: UITextField!
    @IBOutlet weak var txtMoistureMeterReading: UITextField!
    @IBOutlet weak var btn_MoistureDry: UIButton!
    @IBOutlet weak var btn_MoistureDamp: UIButton!
    @IBOutlet weak var btn_MoistureWet: UIButton!
    @IBOutlet weak var btn_InstallMoistureBarrierYes: UIButton!
    @IBOutlet weak var btn_InstallMoistureBarrierNo: UIButton!
    @IBOutlet weak var btn_DryerVentedOutsideYes: UIButton!
    @IBOutlet weak var btn_DryerVentedOutsideNo: UIButton!
    @IBOutlet weak var btn_VentsNecessaryYes: UIButton!
    @IBOutlet weak var btn_VentsNecessaryNo: UIButton!
    @IBOutlet weak var txtVents: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtColor: UITextField!
    @IBOutlet weak var txtSectionInstalled: UITextField!
    @IBOutlet weak var txtTypeOfSystem: UITextField!
    @IBOutlet weak var txtExtraMaterialNecessary: UITextView!
    @IBOutlet weak var btn_TreatMentType_ControlofExistingInfestation: UIButton!
    @IBOutlet weak var btn_TreatMentType_PreventionofExistingInfestation: UIButton!
    @IBOutlet weak var btn_ConstructionType_Brick_Veneer: UIButton!
    @IBOutlet weak var btn_ConstructionType_Stone_Veneer: UIButton!
    @IBOutlet weak var btn_ConstructionType_Stucco_Drivit: UIButton!
    @IBOutlet weak var btn_ConstructionType_WeatherBoard: UIButton!
    @IBOutlet weak var btn_ConstructionType_Hollow_Block: UIButton!
    @IBOutlet weak var btn_ConstructionType_Alum_Viny: UIButton!
    @IBOutlet weak var btn_FoundationType_Brick: UIButton!
    @IBOutlet weak var btn_FoundationType_Stone: UIButton!
    @IBOutlet weak var btn_FoundationType_ConcreteSlab: UIButton!
    @IBOutlet weak var btn_FoundationType_HollowBlock: UIButton!
    @IBOutlet weak var btn_FoundationType_Concrete: UIButton!
    @IBOutlet weak var btn_FoundationType_Basement: UIButton!
    @IBOutlet weak var viewGraphView: CardView!
    @IBOutlet weak var viewOtherDetails: CardView!
    @IBOutlet weak var viewMoistureDetails: CardView!

    // ---------FindingView-------------
    @IBOutlet weak var viewFindings: CardView!
    @IBOutlet weak var tv_Finding: UITableView!
    var aryFinding = NSMutableArray()
    var strForProposal = ""

    //MARK: ------ Outlet Standard Service --------
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var tblStandardService: UITableView!
    
    @IBOutlet weak var btnStandardService: UIButton!
    
    @IBOutlet weak var btnCustomizedService: UIButton!
    
    @IBOutlet weak var viewStandardService: UIView!
    
    @IBOutlet weak var viewCustomizeService: UIView!
    
    @IBOutlet weak var const_ViewStanDardService_H: NSLayoutConstraint!
    @IBOutlet weak var const_CustomizeService_H: NSLayoutConstraint!
    
    @IBOutlet weak var const_TblService_H: NSLayoutConstraint!
    
    @IBOutlet weak var lblServiceListName: UILabel!
    
//MARK: Standard Service Outlet
    
    @IBOutlet weak var btnCategoryStanadard: UIButton!
    @IBOutlet weak var btnServiceStanadard: UIButton!

    @IBOutlet weak var btnFrequencyStanadard: UIButton!

    @IBOutlet weak var txtFldInitialPriceStandard: ACFloatingTextField!
    
    @IBOutlet weak var txtFldMaintPriceStandard: ACFloatingTextField!
    
    @IBOutlet weak var btnAddStandard: UIButton!
    
    @IBOutlet weak var viewService: CardView!
    
    @IBOutlet weak var btnUpdateStandardService: UIButton!
    
    @IBOutlet weak var btnCancelStandard: UIButton!
    
    //MARK: ------------- Customized Service Outlet -------------

    @IBOutlet weak var btnDepartmentCustomized: UIButton!

    @IBOutlet weak var btnFrequencyCustomized: UIButton!
    
    @IBOutlet weak var txtFldServiceNameCustomized: ACFloatingTextField!


    @IBOutlet weak var txtFldInitialPriceCustomized: ACFloatingTextField!
    
    @IBOutlet weak var txtFldMaintPriceCustomized: ACFloatingTextField!
    
    @IBOutlet weak var btnAddCustomized: UIButton!
    
    @IBOutlet weak var viewSelectService: UIView!
    
    @IBOutlet weak var tblSelectService: UITableView!
    
    @IBOutlet weak var btnUpdateCustomized: UIButton!
    
    @IBOutlet weak var btnCancelCustomized: UIButton!
    
    //MARK: ---- Renewal Service Outelet ------
    
    @IBOutlet weak var viewRenewalService: CardView!
    @IBOutlet weak var lblRenewalService: UILabel!
    @IBOutlet weak var tblRenewalService: UITableView!
    @IBOutlet weak var const_TblRenewal_H: NSLayoutConstraint!
    
    //MARK: ---- Cover Letter Outelet ------
    @IBOutlet weak var viewCoverLetter: CardView!
    
    @IBOutlet weak var btnCoverletter: UIButton!
    
    @IBOutlet weak var btnDropDownCoverLetter: UIButton!
    
    @IBOutlet weak var viewCoverLetterStack: UIView!
    
    @IBOutlet weak var const_BtnCoverLetter_H: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgBtnCovertLetter: UIImageView!
    

    //MARK: ---- Introduction Letter Outelet ------
    
    @IBOutlet weak var viewIntroductionLetter: CardView!
    
    @IBOutlet weak var btnIntroductionLetter: UIButton!
    
    @IBOutlet weak var btnDropDownIntroductionLetter: UIButton!
    
    @IBOutlet weak var txtViewIntroductionLetter: UITextView!
    
    
    @IBOutlet weak var viewIntroductionLetterStack: UIView!
    

    
    //MARK: ---- Sales Marketing Content Outelet ------
    
    @IBOutlet weak var viewSalesMarketingContent: CardView!
    
    @IBOutlet weak var btnSalesMarketingContent: UIButton!
    
    @IBOutlet weak var btnDropDownSalesMarketingContent: UIButton!
    
    @IBOutlet weak var viewSalesMareketingStack: UIView!
    

    
    //MARK: ---- Service Month Outelet ------

    @IBOutlet weak var viewServiceMonth: CardView!
    
    @IBOutlet weak var btnDropDownServiceMonth: UIButton!
    
    @IBOutlet weak var const_viewServiceMonth_H: NSLayoutConstraint!
    
    @IBOutlet weak var btnJanuary: UIButton!
    
    @IBOutlet weak var btnFebruary: UIButton!
    
    @IBOutlet weak var btnMarch: UIButton!
    
    @IBOutlet weak var btnApril: UIButton!
    @IBOutlet weak var btnMay: UIButton!

    @IBOutlet weak var btnJune: UIButton!
    @IBOutlet weak var btnJuly: UIButton!

    @IBOutlet weak var btnAugust: UIButton!

    @IBOutlet weak var btnSeptember: UIButton!
    @IBOutlet weak var btnOctober: UIButton!
    @IBOutlet weak var btnNovember: UIButton!
    @IBOutlet weak var btnDecember: UIButton!
    
    @IBOutlet weak var viewServiceMonthStack: UIView!
    


    //MARK: -------------Additional Notes Outlet--------------

    @IBOutlet weak var viewAdditionalNotes: CardView!
    
    @IBOutlet weak var btnDropDownAdditionalNotes: UIButton!
    
    @IBOutlet weak var txtViewAdditionalNotes: UITextView!
    
    
    @IBOutlet weak var btnCheckBoxAgreementValidFor: UIButton!
    
    @IBOutlet weak var viewAdditionalNotesStack: UIView!
    
  
    @IBOutlet weak var txtNoOfDays: UITextField!
    

    //MARK: -------------Terms & Condition Outlet --------------

    @IBOutlet weak var viewTermsCondition: CardView!
    
    @IBOutlet weak var btnDropDownTermsCondition: UIButton!
    
    @IBOutlet weak var viewTermsConditionStack: UIView!
    @IBOutlet weak var btnTermsCondition: UIButton!
    

    
    //MARK: -------------Terms of Service Outlet --------------
    
    @IBOutlet weak var viewTermsOfService: CardView!
    
    @IBOutlet weak var btnDropDownTermsService: UIButton!
    
    @IBOutlet weak var viewTermsServiceStack: UIView!
    @IBOutlet weak var btnTermsService: UIButton!
    
    @IBOutlet weak var txtViewTermsService: UITextView!
    
    @IBOutlet weak var const_ViewTermsServiceStack: NSLayoutConstraint!
    

    //MARK: ----------------------- Varibales -----------------------
    
    
    //MARK: - --------- Global Variables ---------
    
    @objc var strWoId = NSString ()
    @objc var strBranchSysName = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    @objc var objWorkorderDetail = NSManagedObject()
    let global = Global()
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strEmpName = String()
    var dictLoginData = NSDictionary()
    var strWoLeadId = String()
    var strEmpID = String()
    //MARK: - ---------Dictionary ---------
    
    var dictCategory = NSDictionary()
    var dictService = NSDictionary()
    var dictFrequency = NSDictionary()
    
    var dictDepartment = NSDictionary()
    var dictFrequencyNonStan = NSDictionary()
    var dictCoverLetter = NSDictionary()
    var dictIntroduction = NSDictionary()
    var dictMarketingContent = NSDictionary()
    var dictTermsOfService = NSDictionary()
    var dictTermsAndConditions = NSDictionary()
    var dictServiceNameFromSysName = NSDictionary()
    var dictServiceNameFromId = NSDictionary()

    var dictFreqNameFromSysName = NSDictionary()
    var dictFreqYearlyOccurenceFromSysName = NSDictionary()
    var dictCategoryObjectFromSysName = NSDictionary()
    var dictServiceObjectFromSysName = NSDictionary()
    var dictFreqObjectFromSysName = NSDictionary()


    
    //MARK: - ---------Array ---------
    
    var arrCategoryStandard = NSArray()
    var arrServiceStandard = NSArray()
    var arrFrequencyStandard = NSArray()
    var arrServiceData = NSArray()
    var arrCustomizedServiceData = NSArray()
    
    var arrDepartment = NSMutableArray()
    var arrayCoverLetter = NSMutableArray()
    var arrayIntroduction = NSMutableArray()
    var arrayMarketingContent = NSMutableArray()
    var arrayMarketingContentMultipleSelected  = NSMutableArray()
    var arrayTermsOfService = NSMutableArray()
    var arrayTermsAndConditions =  NSMutableArray()
    var arrayTermsAndConditionsMultipleSelected = NSMutableArray()
    
    var arrayServiceMonths = NSMutableArray()
    
    var arrServiceMasterRenewalPrices = NSArray()
    var arrRenewalData = NSArray()


    
    //MARK: - ---------String ---------
    
    var strCategorySysName = String()
    var strCategoryName = String()
    var strServiceNameStandard = String()
    var strServiceIdStandard = String()
    
    var strServiceSysNameStandard = String()
    var strFreqencySysNameStandard = String()
    var strFreqencyNameStandard = String()
    
    var strDeptSysName = String()
    var strFreqencySysNameCustomized = String()
    var strFreqencyNameCustomized = String()
    var strLeadStatusGlobal = String()
    var strStageSysName = String()
    var strAccountNo = String()
    var strTotalInitialPrice = String()
    var strSoldServiceStandardId = String()
    
    //MARK: - ---------Bool ---------
    
    var isChangeServiceDesc = Bool()
    var yesEditedSomething = false
    var isIntroductionLetterEdited = false
    
    // MARK: ----------Enum --------------
    
    var serviceType = ServiceType.standard
    
    //MARK: -------------LifeCycle--------------
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tv_Finding.tableFooterView = UIView()
        
        btnStandardService.setImage((UIImage(named: "radio_check_ipad")), for: .normal)
        btnCustomizedService.setImage((UIImage(named: "radio_uncheck_ipad")), for: .normal)
        strWoId = "\(objWorkorderDetail.value(forKey: "workorderId") ?? "")" as NSString
        
        //strBranchSysName = global.strEmpBranchSysName()! as NSString//"ProductionPestCompanyBranch"
        btnUpdateStandardService.isHidden = true
        btnCancelStandard.isHidden = true
        
        btnUpdateCustomized.isHidden = true
        btnCancelCustomized.isHidden = true
        
        txtViewIntroductionLetter.text = ""
        txtViewAdditionalNotes.text = ""
        txtViewTermsService.text = ""
        txtNoOfDays.isHidden = true
        
        fetchLeadData()
        fetchServiceDetail()
        setupInitialFunction()
        setUpView()
        fetchStandardService()
        
        // Template Method
        getLetterTemplateAndTermsOfService()
        fetchLetterTemplateFromLocalDB()
        fetchMarketingContentFromLocalDB()
        fetchLeadDetailFromLocalDB()
        fetchMultiTermsAndConditionsFromLocalDB()
        
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            disableUserInteration()
        }
        lblHeader.text = nsud.value(forKey: "lblName") as? String
        
        txtViewIntroductionLetter.isEditable = false
        txtViewTermsService.isEditable = false
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
        {
            disableUserInteration()
        }
        
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          //  self.ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)
        }
    }
    override func viewWillLayoutSubviews()
    {
 
    }
    func loadViewInspection()
    {
        //viewInspectionDetail.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewInspectionDetail.frame.height)
        
        viewInspectionDetail.frame = CGRect(x: scrollView.frame.origin.x, y: 10, width: 768, height: viewInspectionDetail.frame.height)
        self.scrollView.addSubview(self.viewInspectionDetail)
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.viewInspectionDetail.frame.height)
    }
    
    //MARK:
    //MARK: --------------------- Actions ---------------------

    @IBAction func action_Back(_ sender: Any)
    {
        //self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @objc func indexChanged(_ sender: UISegmentedControl)
    {
        ReloadViewOnScrollAccordingCondition(tag: sender.selectedSegmentIndex)
    }
    
    //MARK: ------STANDARD SERVICE-------
    
    @IBAction func actionOnStandardService(_ sender: Any)
    {
        serviceType = ServiceType.standard
        
        btnStandardService.setImage((UIImage(named: "radio_check_ipad")), for: .normal)
        btnCustomizedService.setImage((UIImage(named: "radio_uncheck_ipad")), for: .normal)
        fetchStandardService()
        setUpView()
        
        //viewRenewalService.isHidden = false
    }
    
    @IBAction func actionOnCategoryStandard(_ sender: Any)
    {
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrCategoryStandard)
        
        
        if arrOfData.count > 0
        {
            btnCategoryStanadard.tag = 200
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No category found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnSelectServiceStandard(_ sender: Any)
    {
        endEditing()
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrServiceStandard = getServiceNameFromCategory(strCategoryName: strCategorySysName)
            
            arrOfData = NSMutableArray(array: arrServiceStandard)
            
            
            if arrOfData.count > 0
            {
                btnServiceStanadard.tag = 201
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No service found", viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnFrequencyStandard(_ sender: Any)
    {
        endEditing()
        if strCategorySysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            
        }
        else if  strServiceSysNameStandard == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrOfData = NSMutableArray(array: arrFrequencyStandard)
            
            
            if arrOfData.count > 0
            {
                btnFrequencyStanadard.tag = 202
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfData , strTitle: "")
                
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No frequency found", viewcontrol: self)
            }
        }
        
    }
    
    @IBAction func actionOnAddStandard(_ sender: Any)
    {
        endEditing()
        if strCategorySysName == "" || btnCategoryStanadard.currentTitle == "Select Category"
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category", viewcontrol: self)
            
        }
        else if  strServiceSysNameStandard == "" || btnServiceStanadard.currentTitle == "Select Service"
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service", viewcontrol: self)
            
        }
        else if  strFreqencySysNameStandard == "" || btnFrequencyStanadard.currentTitle == "Select Frequency"
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            
        }
        else if  txtFldInitialPriceStandard.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            
        }
        /*else if  txtFldMaintPriceStandard.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
        }*/
        else if  chkserviceExistOrNot() == true
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Service already exist", viewcontrol: self)
        }
        else
        {
            saveStandardServiceToCoreData()
            addRenewalService()
            
            fetchStandardService()
            
            btnCategoryStanadard.setTitle("Select Category", for: .normal)
            btnServiceStanadard.setTitle("Select Service", for: .normal)
            btnFrequencyStanadard.setTitle("Select Frequency", for: .normal)
            
            txtFldInitialPriceStandard.text = ""
            txtFldMaintPriceStandard.text = ""

        }
    }
    @IBAction func actionOnUpdateStandardService(_ sender: Any) {
    }
    
    @IBAction func actionOnCancelStandardService(_ sender: Any) {
    }
    
    //MARK: ------CUSTOMIZED SERVICE-------
    
    @IBAction func actionOnCustomizedService(_ sender: Any)
    {
        serviceType = ServiceType.customized
        
        btnStandardService.setImage((UIImage(named: "radio_uncheck_ipad")), for: .normal)
        btnCustomizedService.setImage((UIImage(named: "radio_check_ipad")), for: .normal)
        fetchCustomizedService()
        setUpView()
        
        //viewRenewalService.isHidden = true

    }
    
    @IBAction func actionOnDepartmentCustomized(_ sender: Any)
    {
        endEditing()
        var arrOfData = NSMutableArray()
        arrOfData = NSMutableArray(array: arrDepartment)
        
        
        if arrOfData.count > 0
        {
            btnDepartmentCustomized.tag = 203
            gotoPopViewWithArray(sender: sender as! UIButton, aryData: returnFilteredArray(array: arrOfData ) , strTitle: "")
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No category found", viewcontrol: self)
        }
    }
    
    @IBAction func actionOnFrequencyCustomized(_ sender: Any)
    {
        endEditing()
        if strDeptSysName == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department first", viewcontrol: self)
            
        }
        else
        {
            var arrOfData = NSMutableArray()
            
            arrOfData = NSMutableArray(array: arrFrequencyStandard)
            
            
            if arrOfData.count > 0
            {
                btnFrequencyCustomized.tag = 204
                gotoPopViewWithArray(sender: sender as! UIButton, aryData: arrOfData , strTitle: "")
                
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No frequency found", viewcontrol: self)
            }
        }
        
    }
    
    @IBAction func actionOnAddCustomized(_ sender: Any)
    {
        endEditing()
        if strDeptSysName == "" || btnDepartmentCustomized.titleLabel?.text == "Select Department"
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department", viewcontrol: self)
            
        }
            
        else if  strFreqencySysNameCustomized == "" || btnFrequencyCustomized.titleLabel?.text == "Select Frequency"
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            
        }
        else if  txtFldServiceNameCustomized.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter service name", viewcontrol: self)
            
        }
        else if  txtFldInitialPriceCustomized.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            
        }
       /* else if  txtFldMaintPriceCustomized.text == ""
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
        }*/
        else
        {
            saveNonStandardServiceToCoreData()
            fetchCustomizedService()
            
            btnDepartmentCustomized.setTitle("Select Department", for: .normal)
            btnFrequencyCustomized.setTitle("Select Frequency", for: .normal)
            btnFrequencyStanadard.setTitle("Select Frequency", for: .normal)
            
            txtFldServiceNameCustomized.text = ""
            txtFldInitialPriceCustomized.text = ""
            txtFldMaintPriceCustomized.text = ""
        }
        
    }
    
    @IBAction func actionOnUpdateCustomizedService(_ sender: Any) {
    }
    
    @IBAction func actionOnCancelCustomizedService(_ sender: Any) {
    }
    //MARK: ------COVER LETTER-------
    
    @IBAction func actionOnCoverLetter(_ sender: Any)
    {
        openTableViewPopUp(tag: 101, ary: arrayCoverLetter, aryselectedItem: NSMutableArray())
       
    }
    
    @IBAction func actionOnDropDownCoverLetter(_ sender: Any)
    {
        if btnDropDownCoverLetter.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownCoverLetter.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewCoverLetterStack.isHidden = true
        }
        else
        {
            btnDropDownCoverLetter.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewCoverLetterStack.isHidden = false

        }
    }
    
    //MARK: ------INTRODUCTION LETTER-------
    
    @IBAction func actionOnIntroductionLetter(_ sender: Any)
    {
        openTableViewPopUp(tag: 102, ary: arrayIntroduction, aryselectedItem: NSMutableArray())
      
    }
    
    @IBAction func actionOnDropDownIntroductionLetter(_ sender: Any)
    {
        if btnDropDownIntroductionLetter.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownIntroductionLetter.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            //btnIntroductionLetter.isHidden = true
            //txtViewIntroductionLetter.isHidden = true
            viewIntroductionLetterStack.isHidden = true


        }
        else
        {
            btnDropDownIntroductionLetter.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
           // btnIntroductionLetter.isHidden = false
           // txtViewIntroductionLetter.isHidden = false
            viewIntroductionLetterStack.isHidden = false



        }
    }
    
    
    //MARK: ------SALES MARKETING CONTENT -------
    
    @IBAction func actionOnSalesMarketingContent(_ sender: Any)
    {
        openTableViewPopUp(tag: 103, ary: arrayMarketingContent, aryselectedItem: arrayMarketingContentMultipleSelected)
        
    }
    @IBAction func actionOnDropDownSalesMarketingContent(_ sender: Any)
    {
        if btnDropDownSalesMarketingContent.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownSalesMarketingContent.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewSalesMareketingStack.isHidden = true


        }
        else
        {
            btnDropDownSalesMarketingContent.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewSalesMareketingStack.isHidden = false



        }
    }
    
    //MARK: ------SERVICE MONTH -------
    
    @IBAction func actionOnDropDownServiceMonth(_ sender: Any)
    {
        if btnDropDownServiceMonth.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownServiceMonth.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewServiceMonthStack.isHidden = true


        }
        else
        {
            btnDropDownServiceMonth.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewServiceMonthStack.isHidden = false



        }
    }
    
    @IBAction func actionOnJanuary(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnJanuary.currentImage == UIImage(named: "NPMA_Check"))
        { //NPMA_UnCheck  //NPMA_Check
            btnJanuary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("January")
        }
        else
        {
            
            arrayServiceMonths.add("January")
            btnJanuary.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnFebruary(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnFebruary.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnFebruary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("February")
        }
        else
        {
            
            arrayServiceMonths.add("February")
            btnFebruary.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnMarch(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnMarch.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnMarch.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("March")
        }
        else
        {
            
            arrayServiceMonths.add("March")
            btnMarch.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnApril(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnApril.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnApril.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("April")
        }
        else
        {
            
            arrayServiceMonths.add("April")
            btnApril.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnMay(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnMay.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnMay.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("May")
        }
        else
        {
            
            arrayServiceMonths.add("May")
            btnMay.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnJune(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnJune.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnJune.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("June")
        }
        else
        {
            
            arrayServiceMonths.add("June")
            btnJune.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnJuly(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnJuly.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnJuly.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("July")
        }
        else
        {
            
            arrayServiceMonths.add("July")
            btnJuly.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnAugust(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnAugust.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnAugust.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("August")
        }
        else
        {
            
            arrayServiceMonths.add("August")
            btnAugust.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnSeptember(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnSeptember.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnSeptember.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("September")
        }
        else
        {
            
            arrayServiceMonths.add("September")
            btnSeptember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnOctober(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnOctober.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnOctober.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("October")
        }
        else
        {
            
            arrayServiceMonths.add("October")
            btnOctober.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnNovember(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnNovember.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnNovember.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("November")
        }
        else
        {
            
            arrayServiceMonths.add("November")
            btnNovember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnDecember(_ sender: Any)
    {
        yesEditedSomething = true
        
        if(btnDecember.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnDecember.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            arrayServiceMonths.remove("December")
        }
        else
        {
            
            arrayServiceMonths.add("December")
            btnDecember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    //MARK: ------ADDITIONAL NOTES-------
    
    
    @IBAction func actionOnCheckBoxAgreementValidFor(_ sender: Any)
    {
        yesEditedSomething = true
        if(btnCheckBoxAgreementValidFor.currentImage == UIImage(named: "NPMA_Check"))
        {
            btnCheckBoxAgreementValidFor.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            txtNoOfDays.isHidden = true
        }
        else
        {
            txtNoOfDays.isHidden = false
            btnCheckBoxAgreementValidFor.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    
    @IBAction func actionOnDropDownAdditionalNotes(_ sender: Any)
    {
        if btnDropDownAdditionalNotes.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownAdditionalNotes.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewAdditionalNotesStack.isHidden = true


        }
        else
        {
            btnDropDownAdditionalNotes.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewAdditionalNotesStack.isHidden = false



        }
    }
    
    //MARK: ------TERMS CONDITION-------
    
    @IBAction func actionOnTermsCondition(_ sender: Any)
    {
        openTableViewPopUp(tag: 105, ary: arrayTermsAndConditions, aryselectedItem: arrayTermsAndConditionsMultipleSelected)
    }
    
    @IBAction func actionOnDropDownTermsCondition(_ sender: Any)
    {
        if btnDropDownTermsCondition.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownTermsCondition.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewTermsConditionStack.isHidden = true


        }
        else
        {
            btnDropDownTermsCondition.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewTermsConditionStack.isHidden = false



        }
    }
    
    //MARK: ------TERMS OF SERVICES-------
    
    @IBAction func actionOnDropDownTermsService(_ sender: Any)
    {
        if btnDropDownTermsService.currentImage == UIImage(named: "open_arrow_ipad")
        {
            btnDropDownTermsService.setImage(UIImage(named: "close_arrow_ipad"), for: .normal)
            viewTermsServiceStack.isHidden = true
            const_ViewTermsServiceStack.constant = 0

        }
        else
        {
            btnDropDownTermsService.setImage(UIImage(named: "open_arrow_ipad"), for: .normal)
            viewTermsServiceStack.isHidden = false
            const_ViewTermsServiceStack.constant = 230



        }
    }
    @IBAction func actionOnTermsService(_ sender: Any)
    {
        openTableViewPopUp(tag: 104, ary: arrayTermsOfService, aryselectedItem: NSMutableArray())

    }
    
    @IBAction func actionOnSaveContinue(_ sender: Any)
    {
        if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
        {
            goToAgreementAfterAlert()
        }
        else
        {
            if arrServiceData.count == 0 && arrCustomizedServiceData.count == 0
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add atleast one service", viewcontrol: self)

            }
            else
            {
                saveLetterTemplateCoverLetterIntroLetterAndTermsOfService(dictCoverLetter: dictCoverLetter, dictIntroLetter: dictIntroduction, dictTermsOfService: dictTermsOfService)
                
                saveMarketingContent(arraySeletecMarketingContent: arrayMarketingContentMultipleSelected)
                
                saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
                
                updateLeadDetails()
                updatePaymentInfo()
                
                goToAgreement()
            }
        }
    }
    

    
    
    //MARK:
    //MARK: -------------Reload View--------------
    func ReloadViewOnScrollAccordingCondition(tag:Int)
    {
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        
        if(tag == 0){ // General
            viewInspectionDetail.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewInspectionDetail.frame.height)
            self.scrollView.addSubview(self.viewInspectionDetail)
        }else if(tag == 1){ // Graph
            viewGraphView.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewGraphView.frame.height)
            self.scrollView.addSubview(self.viewGraphView)
        }else if(tag == 2){ // Finding
            viewFindings.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: scrollView.frame.height - 20)
            self.scrollView.addSubview(self.viewFindings)
            self.tv_Finding.reloadData()
            
            
        }else if(tag == 3){ // Other Detail
            viewOtherDetails.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewOtherDetails.frame.height)
            self.scrollView.addSubview(self.viewOtherDetails)
        }else if(tag == 4){ //Moisture Detail
            viewMoistureDetails.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: scrollView.frame.height - 20)
            self.scrollView.addSubview(self.viewMoistureDetails)
        }
    }
    func UIInitialization()  {
      
        
        //GeneralDes--
        self.txtExtraMaterialNecessary.layer.cornerRadius = 4.0
        self.txtExtraMaterialNecessary.layer.borderWidth = 1.0
        self.txtExtraMaterialNecessary.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        
        //Finding--
    

    }
    
    //MARK:
    //MARK: -------------ActionOnGeneral-----------
    @IBAction func action_CheckBOX(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.transform = CGAffineTransform.init(scaleX: 0.6, y: 0.6)
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
        if(sender.currentImage == UIImage(named: "check_box_2New.png")){
            sender.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
        }
    }
    
    
    //MARK:
    //MARK: -------------ActionOnGraph----------
 
    
    //MARK:
    //MARK: -------------ActionOnFinding----------
    @IBAction func action_AddNew(_ sender: UIButton)
    {
        self.view.endEditing(true)
        
    
    }
    
    @IBAction func action_Resolved(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func action_Unresloved(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func action_OnSelectFindingCode(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func action_OnSelectRecommendationCode(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func action_OnSaveAddFinding(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func action_OnAddImage(_ sender: UIButton) {
        self.view.endEditing(true)

    }
    @IBAction func action_image(_ sender: UIButton) {
        
        self.goToGlobalmage(strType: "Before")
    }
    
    @IBAction func action_ServiceHistory(_ sender: UIButton) {
        goToServiceHistory()
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: UIButton) {
        goToCustomerSalesDocuments()
    }
    
    @IBAction func action_Graph(_ sender: UIButton) {
        
        self.goToGlobalmage(strType: "Graph")
    }
    
    @IBAction func action_NotesHistory(_ sender: UIButton) {
        
        goToNotesHistory()
    }
    
    // MARK: - ---------------- Other Function's ----------------
    
    
    
    
    func goToServiceHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
                testController?.modalPresentationStyle = .fullScreen
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = strAccountNo
        objServiceDocumentsVC?.modalPresentationStyle = .fullScreen
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
                testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    func fetchLeadData()
    {
        let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
        
        if(arrayLeadDetail.count > 0)
        {
            
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            
            strWoLeadId = "\(match.value(forKey: "leadId")!)"
            strBranchSysName = "\(match.value(forKey: "branchSysName")!)" as NSString
            
            nsud.setValue(strBranchSysName, forKey: "branchSysName")
            nsud.synchronize()
            
           // NSUserDefaults *defsBranch=[NSUserDefaults standardUserDefaults];
            //[defsBranch setObject:strBranchSysName forKey:@"branchSysName"];
            //[defsBranch synchronize];
        }else{
                        
            let alert = UIAlertController(title: Alert, message: "Opportunity not available", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                
                self.navigationController?.popViewController(animated: false)
                
            }))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
            
        }
        
    }
    
    
    func setUpView()
    {
        if btnStandardService.currentImage == UIImage(named: "radio_check_ipad")
        {
           // radio_uncheck_ipad
            viewStandardService.isHidden = false
            viewCustomizeService.isHidden = true
        }
        if btnCustomizedService.currentImage == UIImage(named: "radio_check_ipad")
        {
           // radio_uncheck_ipad
            viewStandardService.isHidden = true
            viewCustomizeService.isHidden = false
        }
        
        buttonRoundNew(sender: btnCategoryStanadard)
        buttonRoundNew(sender: btnServiceStanadard)
        buttonRoundNew(sender: btnFrequencyStanadard)
        buttonRoundNew(sender: btnDepartmentCustomized)
        buttonRoundNew(sender: btnFrequencyCustomized)

        
        buttonRoundNew(sender: btnCoverletter)
        buttonRoundNew(sender: btnIntroductionLetter)
        buttonRoundNew(sender: btnSalesMarketingContent)
        buttonRoundNew(sender: btnTermsService)
        buttonRoundNew(sender: btnTermsCondition)
        
        setTextViewCorner(textView: txtViewTermsService)
        setTextViewCorner(textView: txtViewAdditionalNotes)
        setTextViewCorner(textView: txtViewAdditionalNotes)
        setTextViewCorner(textView: txtViewIntroductionLetter)

        buttonRoundNew(sender: btnAddStandard)
        buttonRoundNew(sender: btnAddCustomized)
        buttonRoundNew(sender: btnSaveAndContinue)

        
           


    }
    func setTextViewCorner(textView: UITextView)
    {
        textView.layer.borderColor = (UIColor.lightGray).cgColor
        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = 15.0
        textView.layer.masksToBounds = true
    }
    func setupInitialFunction()
    {
        strCategorySysName = ""
        strServiceSysNameStandard = ""
        strFreqencySysNameStandard = ""
        strServiceIdStandard = ""
        strDeptSysName = ""
        strFreqencyNameCustomized = ""
        strFreqencySysNameCustomized = ""
        isChangeServiceDesc = false
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"

        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        arrCategoryStandard = global.getCategoryDeptWiseGlobal()! as NSArray
        arrFrequencyStandard = getFrequency()
        getDepartmentNonStan()
    }
    func buttonRoundNew(sender : UIButton){
        
        sender.layer.cornerRadius = 10
        sender.layer.borderWidth = 1
        sender.layer.borderColor = UIColor.gray.cgColor
        sender.layer.masksToBounds = true
        //sender.layer.shadowColor = UIColor.darkGray.cgColor
        //sender.layer.shadowOffset = CGSize(width: 1, height: 1);
        //sender.layer.shadowOpacity = 0.5
        //sender.backgroundColor = UIColor.theme()
        
    }
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)
    {
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
        else
        {
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func getServiceNameFromCategory(strCategoryName: String) -> NSArray
    {
        for item in arrCategoryStandard
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == strCategorySysName
            {
                arrServiceStandard = dict.value(forKey: "Services") as! NSArray
                break
            }
        }
        return arrServiceStandard
    }
    func getFrequency() -> NSArray
    {
            
            if(nsud.value(forKey: "MasterSalesAutomation") != nil){
                
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                
                if(dictMaster.value(forKey: "Frequencies") is NSArray){
                    arrFrequencyStandard = NSArray()
                    arrFrequencyStandard = dictMaster.value(forKey: "Frequencies") as! NSArray
                    
                    //Frequency as per type
                    
                    let arrTemp = NSMutableArray()
                    
                    if (arrFrequencyStandard.count  > 0)
                    {
                        for item in arrFrequencyStandard
                        {
                            let dict = item as! NSDictionary
                            let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                            if strType == "Service" || strType == "Both" || strType == ""
                            {
                                arrTemp.add(dict)
                            }
                        }
                    }
                    arrFrequencyStandard = arrTemp as NSArray
                    
                    //End
                }
            }
        return arrFrequencyStandard
        
    }
    
    func fetchServiceDetail()
    {
        let arrServiceSysName = NSMutableArray()
        let arrServiceName = NSMutableArray()
        let arrServiceId = NSMutableArray()
        let arrFreqName = NSMutableArray()
        let arrFreqSysName = NSMutableArray()
        let arrFreqYearlyOccurence = NSMutableArray()


        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
            
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
                
                if dictCategory.value(forKey: "Services") is NSArray
                {
                    let arrServices = dictCategory.value(forKey: "Services") as! NSArray
                    
                    for itemService in arrServices
                    {
                        let dictServiceTemp = itemService as! NSDictionary

                        arrServiceSysName.add("\(dictServiceTemp.value(forKey: "SysName") ?? "")")
                        arrServiceName.add("\(dictServiceTemp.value(forKey: "Name") ?? "")")
                        arrServiceId.add("\(dictServiceTemp.value(forKey: "ServiceMasterId") ?? "")")
                    }
                }
            }
        }
        
        dictServiceNameFromSysName = NSDictionary(objects:arrServiceName as! [Any], forKeys:arrServiceSysName as! [NSCopying]) as NSDictionary
        
        dictServiceNameFromId = NSDictionary(objects:arrServiceName as! [Any], forKeys:arrServiceId as! [NSCopying]) as NSDictionary
        
        
        //For Frequency
        
        let arrFrequency = dictMaster.value(forKey: "Frequencies") as! NSArray

        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
            arrFreqName.add("\(dict.value(forKey: "FrequencyName") ?? "")")
            arrFreqSysName.add("\(dict.value(forKey: "SysName") ?? "")")
            arrFreqYearlyOccurence.add("\(dict.value(forKey: "YearlyOccurrence") ?? "")")
        }
        dictFreqNameFromSysName = NSDictionary(objects:arrFreqName as! [Any], forKeys:arrFreqSysName as! [NSCopying]) as NSDictionary

        
        dictFreqYearlyOccurenceFromSysName = NSDictionary(objects:arrFreqYearlyOccurence as! [Any], forKeys:arrFreqSysName as! [NSCopying]) as NSDictionary

        
      

    }
    
    func getDepartmentNonStan()
    {
        arrDepartment = NSMutableArray()
        
        let  dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
        
        let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
        
        for itemBranch in arrDept
        {
            let dictBranch = itemBranch as! NSDictionary
            
            let arrTemp = dictBranch.value(forKey: "Departments") as! NSArray
            
            for itemDept in arrTemp
            {
                let dictDept = itemDept as! NSDictionary
                
                if ("\(dictBranch.value(forKey: "SysName")!)") == strBranchSysName as String
                {
                    arrDepartment.add(dictDept)
                }
            }
            
        }
    }
    
    func chkserviceExistOrNot() -> Bool
    {
        var chkExist = Bool()
        
        chkExist = false
        
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND serviceSysName == %@", strWoId,strServiceSysNameStandard))
        
        if arryOfWorkOrderData.count > 0
        {
            
            chkExist = true
            
        }
        else
        {
            chkExist = false
            
        }
        
        return chkExist
    }
    
    func saveStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        let arrAdditionalPara = NSMutableArray()
        
        var strIsChange = String()
        var strIntialPrice = String()
        var strMaintPrice = String()
        var strDescription = String()
        var strInternalNotes = String()
        
        strIntialPrice = txtFldInitialPriceStandard.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strTotalInitialPrice = strIntialPrice
        
        strMaintPrice = txtFldMaintPriceStandard.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        
        
        
        strDescription = "\(dictService.value(forKey: "Description") ?? "")"//txtViewServiceDescStandService.text ?? ""
        strInternalNotes = ""//txtViewInternalNotesStandService.text ?? ""
        
        if strDescription == "\(dictService.value(forKey: "Description") ?? "")".html2String
        {
            isChangeServiceDesc = false
        }
        else
        {
            isChangeServiceDesc = true
        }
        
        
        if isChangeServiceDesc
        {
            strIsChange = "true"
        }
        else
        {
            strIsChange = "false"
            
        }
        strSoldServiceStandardId = global.getReferenceNumber()
        
        var strBilllingFreqSysName = String()
        strBilllingFreqSysName = getBillingFrequency(ServiceSysName: strServiceSysNameStandard, ServiceFreqSysName: strFreqencySysNameStandard)
        
        var strBilllingFreqPrice = String()
        
        strBilllingFreqPrice = billingFrequencyPriceCalculation(ServiceFrequencySysName: strFreqencySysNameStandard, BillingFrequencySysName: strBilllingFreqSysName, TotalInitialPrice: strIntialPrice, TotalMaintPrice: strMaintPrice)
        

        
        arrOfKeys = ["additionalParameterPriceDcs",
                     "billingFrequencyPrice",
                     "billingFrequencySysName",
                     "bundleDetailId",
                     "bundleId",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "discount",
                     "discountPercentage",
                     "finalUnitBasedInitialPrice",
                     "finalUnitBasedMaintPrice",
                     "frequencySysName",
                     "initialPrice",
                     "isChangeServiceDesc",
                     "isCommercialTaxable",
                     "isResidentialTaxable",
                     "isSold",
                     "isTBD",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "packageId",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceId",
                     "servicePackageName",
                     "serviceSysName",
                     "serviceTermsConditions",
                     "soldServiceStandardId",
                     "totalInitialPrice",
                     "totalMaintPrice",
                     "unit",
                     "userName",
                     "internalNotes",
                     "categorySysName"]
        
        arrOfValues = [arrAdditionalPara,
                       strBilllingFreqPrice, //billingFrequencyPrice
                       strBilllingFreqSysName, //billingFrequencySysName //Monthly
                       "0",
                       "0",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       "0",
                       "0",
                       strIntialPrice,
                       strMaintPrice,
                       strFreqencySysNameStandard,
                       strIntialPrice,
                       strIsChange,
                       "false",
                       "false",
                       "false",
                       "false",
                       strWoLeadId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       "",
                       "",
                       "0",
                       strDescription,
                       strFreqencyNameStandard,
                       strServiceIdStandard,
                       "",
                       strServiceSysNameStandard,
                       "\(dictService.value(forKey: "TermsConditions") ?? "")", //serviceTermsConditions
                       strSoldServiceStandardId,
                       strIntialPrice,
                       strMaintPrice,
                       "1",
                       strUserName,
                       strInternalNotes,
                       strCategorySysName]
        
        saveDataInDB(strEntity: "SoldServiceStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func updateStandardService()
    {
        
    }
    
    func saveNonStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
        var strDescription = String()
        var strInternalNotes = String()
        var strTermsCondition = String()
        
        
        strIntialPrice = txtFldInitialPriceCustomized.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtFldMaintPriceCustomized.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        
        strDescription = ""//txtViewServiceDescNonStandService.text ?? ""
        strInternalNotes = ""//txtViewInternalNotesNonStandService.text ?? ""
        
        strTermsCondition = ""//txtViewTermsConditionNonStan.text ?? ""
        
        
        arrOfKeys = ["billingFrequencyPrice",
                     "billingFrequencySysName",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "departmentSysname",
                     "discount",
                     "discountPercentage",
                     "frequencySysName",
                     "initialPrice",
                     "isSold",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "nonStdServiceTermsConditions",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceName",
                     "soldServiceNonStandardId",
                     "userName",
                     "internalNotes"]
        
        arrOfValues = ["",
                       "",
                       strCompanyKey,
                       strUserName,
                       global.strCurrentDate(),
                       strDeptSysName,
                       "0",
                       "0",
                       strFreqencySysNameCustomized,
                       strIntialPrice,
                       "false",
                       strWoLeadId,
                       strMaintPrice,
                       strUserName,
                       global.modifyDate(),
                       strIntialPrice,
                       strMaintPrice,
                       strTermsCondition,
                       strDescription,
                       strFreqencyNameCustomized,
                       txtFldServiceNameCustomized.text ?? "", //service name
                       global.getReferenceNumber(),
                       strUserName,
                       strInternalNotes]
        
        saveDataInDB(strEntity: "SoldServiceNonStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    func updateCustomizedService()
    {
        
    }
    func endEditing()
    {
        self.view.endEditing(true)
    }
    func fetchStandardService()
    {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
        
        arrServiceData = arryOfData
        
        const_TblService_H.constant = 70
        
        if arrServiceData.count == 0
        {
            const_TblService_H.constant = 0
            tblStandardService.isHidden = true
            lblServiceListName.isHidden = true
            //const_TblService_H.constant = CGFloat(arrServiceData.count * 50) + 90
            viewService.isHidden = true
        }
        else
        {
            tblStandardService.isHidden = false
            lblServiceListName.isHidden = false
            viewService.isHidden = false

            if arrServiceData.count == 1
            {
                const_TblService_H.constant = CGFloat(CGFloat(arrServiceData.count) *  const_TblService_H.constant) + 70

            }
            else
            {
                const_TblService_H.constant = CGFloat(CGFloat(arrServiceData.count) *  const_TblService_H.constant) + 50

            }
        }
        
        tblStandardService.reloadData()
        
        fetchRenewalService()
        
    }
    func fetchCustomizedService()
    {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
        
        arrCustomizedServiceData = arryOfData
        const_TblService_H.constant = 70
        if arrCustomizedServiceData.count == 0
        {
            const_TblService_H.constant = 0
            tblStandardService.isHidden = true
            lblServiceListName.isHidden = true
            viewService.isHidden = true

            //const_TblService_H.constant = CGFloat(arrServiceData.count * 50) + 90
        }
        else
        {
            tblStandardService.isHidden = false
            lblServiceListName.isHidden = false
            viewService.isHidden = false

            
            if arrServiceData.count == 1
            {
                const_TblService_H.constant = CGFloat(CGFloat(arrCustomizedServiceData.count) *  const_TblService_H.constant) + 70 + 30

            }
            else
            {
                const_TblService_H.constant = CGFloat(CGFloat(arrCustomizedServiceData.count) *  const_TblService_H.constant) + 70

            }
            
        }
        tblStandardService.reloadData()
    }
    func fetchRenewalService()
    {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
        
        arrRenewalData = arryOfData
        
        const_TblRenewal_H.constant = 70
        
        if arrRenewalData.count == 0
        {
            const_TblRenewal_H.constant = 0
            tblRenewalService.isHidden = true
            viewRenewalService.isHidden = true
        }
        else
        {
            tblRenewalService.isHidden = false
            viewRenewalService.isHidden = false
            
            if arrRenewalData.count == 1
            {
                const_TblRenewal_H.constant = CGFloat(CGFloat(arrRenewalData.count) *  const_TblRenewal_H.constant) + 90

            }
            else
            {
                const_TblRenewal_H.constant = CGFloat(CGFloat(arrRenewalData.count) *  const_TblRenewal_H.constant) + 50

            }
            
        }
        
        tblRenewalService.reloadData()
        
    }
    func fetchCoverLetter()
    {
        
    }
    func fetchSalesMarketingContent()
    {
        
    }
    func fetchIntroductionLetter()
    {
        
    }
    func getLetterTemplateAndTermsOfService()
    {
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        let arrayLetterTemplateMaster = (dictMaster.value(forKey: "LetterTemplateMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        let arraySalesMarketingContentMaster = (dictMaster.value(forKey: "SalesMarketingContentMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        arrayTermsOfService = (dictMaster.value(forKey: "TermsOfServiceMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        
        // cover letter and Introduction
        for item in arrayLetterTemplateMaster
        {
            if("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "CoverLetter")
            {
                arrayCoverLetter.add(item as! NSDictionary)
            }
            else if ("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "Introduction")
            {
                arrayIntroduction.add(item as! NSDictionary)
            }
        }
        
        // Sales Marketing Content
        
        for item in arraySalesMarketingContentMaster
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
                else
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
            }
        }
        
        // Terms & Conditions
        
        let arrayMultipleGeneralTermsConditions = (dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray).mutableCopy() as! NSMutableArray
        
        for item in arrayMultipleGeneralTermsConditions
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayTermsAndConditions.add(item as! NSDictionary)
                }
            }
            
            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true && (item as! NSDictionary).value(forKey: "IsDefault") as! Bool == true)
            {
                arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                
                btnTermsCondition.setTitle("\((item as! NSDictionary).value(forKey: "TermsTitle")!)", for: .normal)
                
            }
        }
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func getCategoryObjectFromSysName(strSysName: String) -> NSDictionary
    {
        var dictFinal = NSDictionary()
        
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
            
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
                
                if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
                {
                    dictFinal = dictCategory
                    break
                }
                
            }
            
        }
        
        return dictFinal
    }
    
    func getServiceObjectFromSysName(strSysName: String) -> NSDictionary
    {
        var dictFinal = NSDictionary()
        
        var result = false
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
            
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
    
                if dictCategory.value(forKey: "Services") is NSArray
                {
                    let arrService = dictCategory.value(forKey: "Services") as! NSArray
                    
                    for itemService in arrService
                    {
                        let dictService = itemService as! NSDictionary
                        
                        if strSysName == "\(dictService.value(forKey: "SysName") ?? "")"
                        {
                            dictFinal = dictService
                            result = true
                            break
                        }
                        
                    }
                }
                
                if result == true
                {
                    break
                }
                
            }
            
        }
        
        return dictFinal
    }
    
    func getFrequencyObjectFromSysName(strSysName: String) -> NSDictionary
    {
        var dictFinal = NSDictionary()
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMaster.value(forKey: "Frequencies") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Frequencies") as! NSArray
            
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
                
                if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
                {
                    dictFinal = dictCategory
                    break
                }
                
            }
            
        }
        
        return dictFinal
    }
    
    func setDefaultValue()
    {
        
    }
    func goToAgreement()
    {
        if(yesEditedSomething)
        {
            global.updateSalesModifydate(strWoLeadId)
            nsud.set(true, forKey: "synAgreementNPMA")
            nsud.synchronize()
        }
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Agreement_iPadVC") as! NPMA_Agreement_iPadVC
        testController.strLeadId = strWoLeadId
        testController.strWoId = strWoId
        testController.strForProposal = strForProposal
        testController.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    func goToAgreementAfterAlert()
    {
        if(yesEditedSomething)
        {
            global.updateSalesModifydate(strWoLeadId)
        }
        
        if(yesEditedSomething)
        {
            // sett true to sync sales data after wdo data
            nsud.set(true, forKey: "synAgreementNPMA")
            nsud.synchronize()
            
        }
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Agreement_iPadVC") as! NPMA_Agreement_iPadVC
        testController.strLeadId = strWoLeadId
        testController.strWoId = strWoId
        testController.strForProposal = strForProposal
        testController.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(testController, animated: false)
        
    }
    func updateLeadDetails()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        arrOfKeys.add("strPreferredMonth")

        // values
        arrOfValues.add(arrayServiceMonths.componentsJoined(by: ","))

        if(yesEditedSomething)
        {
            
            arrOfKeys.add("zSync")
            arrOfValues.add("yes")
            
        }
    
        let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strWoLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if(isSuccess == true)
        {
            print("updated successsfully")
        }
        else
        {
            print("updates failed")
        }
        
    }
    func updatePaymentInfo()
    {
        // Update Payment Info
                
        let arrayAllObject = getDataFromLocal(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strWoLeadId))
        
        if (arrayAllObject.count==0)
        {
            savePaymentInfo()
        }
        else
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            arrOfKeys.add("specialInstructions")
            
            
            arrOfValues.add(strCompanyKey)
            arrOfValues.add(strUserName)
            arrOfValues.add(strEmpID)
            arrOfValues.add(global.modifyDate())
            arrOfValues.add(txtViewAdditionalNotes.text)
            
            
            let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", strWoLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess
            {
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    func savePaymentInfo()
    {
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadPaymentDetailId")
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("amount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("licenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("specialInstructions")
        arrOfKeys.add("agreement")
        arrOfKeys.add("proposal")
        arrOfKeys.add("customerSignature")
        arrOfKeys.add("salesSignature")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strWoLeadId)
        arrOfValues.add("") // leadPaymentDetailId
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add(txtViewAdditionalNotes.text) // special instruction
        arrOfValues.add("") // agreement
        arrOfValues.add("") // proposal
        arrOfValues.add("")
        arrOfValues.add("")
        
        arrOfValues.add(strEmpID) // createdBy
        arrOfValues.add(global.modifyDate()) // createdDate
        arrOfValues.add(strEmpID) // modifiedBy
        arrOfValues.add(global.modifyDate())
        
        arrOfValues.add("")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "PaymentInfo", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
    
    func disableUserInteration()
    {
        btnCoverletter.isUserInteractionEnabled = false
        btnIntroductionLetter.isUserInteractionEnabled = false
        btnSalesMarketingContent.isUserInteractionEnabled = false
        btnJanuary.isUserInteractionEnabled = false
        btnFebruary.isUserInteractionEnabled = false
        btnMarch.isUserInteractionEnabled = false
        btnApril.isUserInteractionEnabled = false
        btnMay.isUserInteractionEnabled = false
        btnJune.isUserInteractionEnabled = false
        btnJuly.isUserInteractionEnabled = false
        btnAugust.isUserInteractionEnabled = false
        btnSeptember.isUserInteractionEnabled = false
        btnOctober.isUserInteractionEnabled = false
        btnNovember.isUserInteractionEnabled = false
        btnDecember.isUserInteractionEnabled = false
        txtViewAdditionalNotes.isUserInteractionEnabled = false
        btnCheckBoxAgreementValidFor.isUserInteractionEnabled = false
        txtNoOfDays.isUserInteractionEnabled = false
        btnTermsService.isUserInteractionEnabled = false
        btnTermsCondition.isUserInteractionEnabled = false
        txtViewAdditionalNotes.isUserInteractionEnabled = false
        //btnEditIntroductionLetter.isUserInteractionEnabled = false
        
        viewStandardService.isUserInteractionEnabled = false
        viewCustomizeService.isUserInteractionEnabled = false
    }
    
    func billingFrequencyPriceCalculation(ServiceFrequencySysName strServiceFreqSysName: String, BillingFrequencySysName strBillingFreqSysName: String, TotalInitialPrice strTotalInitialPrice: String, TotalMaintPrice strTotalMaintPrice: String ) -> String
    {

        var totalBillingFreqCharge = Float()
        totalBillingFreqCharge = 0.0
        
        var chkFreqConfiguration = Bool()
        
        chkFreqConfiguration = ("\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1") ?? "")" as NSString).boolValue
    
        let strServiceFreqYearlyOccuerence = "\(dictFrequency.value(forKey: "YearlyOccurrence") ?? "")"
        
        var dictBillingFreq = NSDictionary()
        
        dictBillingFreq = getFrequencyObjectFromSysName(strSysName: strBillingFreqSysName)
        
        var strBillingFreqYearlyOccuerence = ""
        
        if dictBillingFreq.count > 0
        {
            strBillingFreqYearlyOccuerence = "\(dictBillingFreq.value(forKey: "YearlyOccurrence") ?? "")"
        }
        
        if strServiceFreqSysName.caseInsensitiveCompare("OneTime") == .orderedSame
        {
            
            totalBillingFreqCharge =  ( (strTotalInitialPrice as NSString).floatValue * (strServiceFreqYearlyOccuerence as NSString).floatValue ) / ((strBillingFreqYearlyOccuerence as NSString).floatValue)
            
            if totalBillingFreqCharge < 0
            {
                totalBillingFreqCharge = 0.0
            }

        }
        else
        {
            if chkFreqConfiguration
            {
                if strServiceFreqSysName.caseInsensitiveCompare("Yearly") == .orderedSame
                {
                    
                    totalBillingFreqCharge =  ( (strTotalMaintPrice as NSString).floatValue * (strServiceFreqYearlyOccuerence as NSString).floatValue ) / ((strBillingFreqYearlyOccuerence as NSString).floatValue)
                    
                    if totalBillingFreqCharge < 0
                    {
                        totalBillingFreqCharge = 0.0
                    }


                }
                else
                {
                    totalBillingFreqCharge =  ( (strTotalMaintPrice as NSString).floatValue * ((strServiceFreqYearlyOccuerence as NSString).floatValue - 1) ) / ((strBillingFreqYearlyOccuerence as NSString).floatValue)
                    
                    if totalBillingFreqCharge < 0
                    {
                        totalBillingFreqCharge = 0.0
                    }
                }
            }
            else
            {
                totalBillingFreqCharge =  ( (strTotalMaintPrice as NSString).floatValue * ((strServiceFreqYearlyOccuerence as NSString).floatValue) ) / ((strBillingFreqYearlyOccuerence as NSString).floatValue)
                
                if totalBillingFreqCharge < 0
                {
                    totalBillingFreqCharge = 0.0
                }
            }
        }

        
        return String(format: "%.2f", totalBillingFreqCharge)//"totalBillingFreqCharge"
    }
    func getBillingFrequency(ServiceSysName strServiceSysName: String , ServiceFreqSysName strServiceFreqSysName: String ) -> String
    {
        var strBillingFreqSysName = String()
        
        var strDefaultBillingFreq = String()
        var strDefaultBillingFreqSysName = String()

        strDefaultBillingFreq = "\(dictService.value(forKey: "DefaultBillingFrequency") ?? "")"
        strDefaultBillingFreqSysName = "\(dictService.value(forKey: "DefaultServiceFrequencySysName") ?? "")"

       if strDefaultBillingFreq == "SameAsService"
       {
            strBillingFreqSysName = strServiceFreqSysName
       }
       else if strDefaultBillingFreq == "Other"
       {
            strBillingFreqSysName = strDefaultBillingFreqSysName
       }
       else
       {
            strBillingFreqSysName = strServiceFreqSysName
       }
        
        return strBillingFreqSysName
    }
    
    
    // MARK: ----------------------------  Fetch from local DB ----------------------------
    
    func fetchLetterTemplateFromLocalDB()
    {
        let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        
        if(arrayLetterTemplate.count > 0)
        {
            let match = arrayLetterTemplate.firstObject as! NSManagedObject
            
            // cover letter
            if("\(match.value(forKey: "coverLetterSysName") ?? "")".count > 0)
            {
                for item in arrayCoverLetter
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "coverLetterSysName")!)")
                    {
                        dictCoverLetter = item as! NSDictionary
                        btnCoverletter.setTitle("\(dictCoverLetter.value(forKey: "TemplateName") ?? "")", for: .normal)
                        break
                    }
                }
            }
            else
            {
                btnCoverletter.setTitle("---Select---", for: .normal)
            }
            
            // Introduction letter
            
            if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
            {
                for item in arrayIntroduction
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                    {
                        dictIntroduction = item as! NSDictionary
                        
                        btnIntroductionLetter.setTitle("\(dictIntroduction.value(forKey: "TemplateName")!)", for: .normal)
                        
                        //let description = "\(dictIntroduction.value(forKey: "TemplateContent")!)".trimmingCharacters(in: .whitespacesAndNewlines)
                        
                        // Change By Saavan
                        
                        txtViewIntroductionLetter.attributedText = htmlAttributedString(strHtmlString: "\(match.value(forKey: "introContent")!)")
                        
                    }
                }
            }
            else
            {
                btnIntroductionLetter.setTitle("---Select---", for: .normal)
            }
            
            // Terms Of Service
            if("\(match.value(forKey: "termsOfServiceSysName") ?? "")".count > 0)
            {
                for item in arrayTermsOfService
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "termsOfServiceSysName")!)")
                    {
                        dictTermsOfService = item as! NSDictionary
                        
                        btnTermsService.setTitle("\(dictTermsOfService.value(forKey: "Title")!)", for: .normal)
                        
                        txtViewTermsService.attributedText = htmlAttributedString(strHtmlString: "\(dictTermsOfService.value(forKey: "Description")!)")
                        
                    }
                }
            }
            else
            {
                btnTermsService.setTitle("---Select---", for: .normal)
            }
            
            // isAgreementvalidfor(Other)
            
            if("\(match.value(forKey: "isAgreementValidFor") ?? "")" == "true" || "\(match.value(forKey: "isAgreementValidFor") ?? "")" == "1" || "\(match.value(forKey: "isAgreementValidFor") ?? "")" == "True")
            {
                txtNoOfDays.isHidden = false
                btnCheckBoxAgreementValidFor.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                txtNoOfDays.text = "\(match.value(forKey: "validFor") ?? "")"
            }
                
            else
            {
                txtNoOfDays.isHidden = true
                btnCheckBoxAgreementValidFor.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
        }
    }
    
    
    fileprivate func fetchMarketingContentFromLocalDB()
    {
        let arrayMatches = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        
        if(arrayMatches.count > 0)
        {
            for match in arrayMatches
            {
                for item in arrayMarketingContent
                {
                    if("\((match as! NSManagedObject).value(forKey: "contentSysName")!)" == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                    {
                        arrayMarketingContentMultipleSelected.add(item as! NSDictionary)
                        break
                    }
                }
            }
            if(arrayMarketingContentMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayMarketingContentMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "Title")!)" + ","
                }
                termsTitle.removeLast()
                btnSalesMarketingContent.setTitle(termsTitle, for: .normal)
            }
        }
    }
    
    fileprivate func fetchMultiTermsAndConditionsFromLocalDB()
    {
        let arrayMultiTermsConditions = getDataFromLocal(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        
        if(arrayMultiTermsConditions.count > 0)
        {
            for match in arrayMultiTermsConditions
            {
                for item in arrayTermsAndConditions
                {
                    if("\((match as! NSManagedObject).value(forKey: "termsId")!)" == "\((item as! NSDictionary).value(forKey: "Id")!)")
                    {
                        if(arrayTermsAndConditionsMultipleSelected.contains(item as! NSDictionary) == false)
                        {
                            arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                            
                            break
                        }
                        
                        
                        
                    }
                }
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayTermsAndConditionsMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
                }
                termsTitle.removeLast()
                btnTermsCondition.setTitle(termsTitle, for: .normal)
            }
        }
    }
    fileprivate func fetchLeadDetailFromLocalDB()
    {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        
        if(arrayLeadDetail.count > 0)
        {
            let match = arrayLeadDetail.firstObject as! NSManagedObject
            
            let strMonths = "\(match.value(forKey: "strPreferredMonth") ?? "")"
            
            if(strMonths.count > 0)
            {
                if(strMonths.contains(","))
                {
                    arrayServiceMonths = (strMonths.components(separatedBy: ",") as NSArray).mutableCopy() as! NSMutableArray
                }
                else
                {
                    arrayServiceMonths.add(strMonths)
                }
            }
            
            for item in arrayServiceMonths
            {
                if("\(item)".trimmingCharacters(in: .whitespaces) == "January")
                {
                    btnJanuary.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "February")
                {
                    btnFebruary.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "March")
                {
                    btnMarch.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "April")
                {
                    btnApril.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "May")
                {
                    btnMay.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "June")
                {
                    btnJune.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "July")
                {
                    btnJuly.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "August")
                {
                    btnAugust.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "September")
                {
                    btnSeptember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "October")
                {
                    btnOctober.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "November")
                {
                    btnNovember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
                if("\(item)".trimmingCharacters(in: .whitespaces) == "December")
                {
                    btnDecember.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }
            }
            
            strLeadStatusGlobal = "\(match.value(forKey: "statusSysName") ?? "")"
            
            strStageSysName = "\(match.value(forKey: "stageSysName") ?? "")"
            
            strAccountNo = "\(match.value(forKey: "accountNo") ?? "")"
            
            // Lead Inspetion Fee
            
            let strLeadInspectionFee = "\(match.value(forKey: "leadInspectionFee") ?? "")"
            
            let doubleLeadInspectionFee = (strLeadInspectionFee as NSString).doubleValue
            
            if doubleLeadInspectionFee > 0 {
                
               // txtLeadInspectionFee.text = String(format: "%.2f", doubleLeadInspectionFee)
                
            }else {
                
                //txtLeadInspectionFee.text = "0.00"
                
            }

        }
    }
    
    // MARK: Save to local DB
    func saveLetterTemplateCoverLetterIntroLetterAndTermsOfService(dictCoverLetter:NSDictionary, dictIntroLetter:NSDictionary,dictTermsOfService:NSDictionary)
    {
        
        deleteAllRecordsFromDB(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        // keys
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadCommercialDetailId")
        arrOfKeys.add("coverLetterSysName")
        arrOfKeys.add("introSysName")
        arrOfKeys.add("introContent")
        arrOfKeys.add("termsOfServiceSysName")
        arrOfKeys.add("termsOfServiceContent")
        arrOfKeys.add("isAgreementValidFor")
        arrOfKeys.add("validFor")
        arrOfKeys.add("isInitialTaxApplicable")
        arrOfKeys.add("isMaintTaxApplicable")
        
        // values
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoLeadId)
        arrOfValues.add("")
        
        // cover letter
        if(dictCoverLetter.count > 0)
        {
            arrOfValues.add("\(dictCoverLetter.value(forKey: "SysName")!)")
        }
        else
        {
            arrOfValues.add("")
        }
        
        // Intro Letter
        if(dictIntroLetter.count > 0)
        {
            
            arrOfValues.add("\(dictIntroLetter.value(forKey: "SysName")!)")
            //arrOfValues.add("\(dictIntroLetter.value(forKey: "TemplateContent")!)")
            
            if isIntroductionLetterEdited {
                
                let description = "\(nsud.value(forKey: "htmlContentsIntroduction")!)"
                arrOfValues.add("\(description)")
                
            }else{
                
                arrOfValues.add("\(dictIntroLetter.value(forKey: "TemplateContent")!)")
                
            }

        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        // Terms Of Service
        
        if(dictTermsOfService.count > 0)
        {
            arrOfValues.add("\(dictTermsOfService.value(forKey: "SysName")!)")
            arrOfValues.add("\(converHTML(html: dictTermsOfService.value(forKey: "Description")! as! String))")
        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        // isAgreementValidFor
        
        if(btnCheckBoxAgreementValidFor.currentImage == UIImage(named: "NPMA_Check"))
        {
            if txtNoOfDays.text == ""
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter valid for", viewcontrol: self)
                
                return
                
            }
            else
            {
                arrOfValues.add("true")
                arrOfValues.add("\(txtNoOfDays.text!)")
            }
            
            
        }
        else
        {
            arrOfValues.add("false")
            arrOfValues.add("")
        }
        
        // isInitialTaxApplicable and isMaintTaxApplicable
        
        arrOfValues.add("")
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "LeadCommercialDetailExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func saveMarketingContent(arraySeletecMarketingContent:NSMutableArray)
    {
        if(arraySeletecMarketingContent.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
            
            for item in arraySeletecMarketingContent
            {
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                // keys
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("leadId")
                arrOfKeys.add("contentSysName")
                
                
                // values
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strWoLeadId)
                arrOfValues.add("\((item as! NSDictionary).value(forKey: "SysName")!)")
                
                saveDataInDB(strEntity: "LeadMarketingContentExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }
    }
    
    func saveTermsAndConditions(arraySeletecTermsAndConditions:NSMutableArray)
    {
        
        let dictLogInDetail = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeBranchSysName = "\(dictLogInDetail.value(forKey: "EmployeeBranchSysName")!)"
        
        
        if(arraySeletecTermsAndConditions.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
        }
        
        for item in arraySeletecTermsAndConditions
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadCommercialTermsId")
            arrOfKeys.add("branchSysName")
            arrOfKeys.add("termsnConditions")
            arrOfKeys.add("termsId")
            
            // values
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoLeadId)
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            arrOfValues.add("\(employeeBranchSysName)")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "TermsConditions")!)")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            
            saveDataInDB(strEntity: "LeadCommercialTermsExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
    }
    
    // MARK: - ----------------Renewal Service Method --------------------
    
    func addRenewalService()
    {
        
        if !checkRenewalServiceExistence(strRenewalServiceId: strServiceIdStandard)
        {
            var chkRenewalStatus = Bool()
            chkRenewalStatus = chkRenewalService(strCategorySysName: strCategorySysName, strServiceSysName: strServiceSysNameStandard)
            
            if arrServiceMasterRenewalPrices.count > 0 && chkRenewalStatus == true
            {
                let dict = arrServiceMasterRenewalPrices.object(at: 0) as! NSDictionary
                        
                saveRenewalServiceInToCoreData(dictRenewalData: dict, TotalInitialPrice: strTotalInitialPrice, ServiceId: strSoldServiceStandardId, ServiceMasterId: strServiceIdStandard)
                
            }
            
        }

    }
    func checkRenewalServiceExistence(strRenewalServiceId : String) -> Bool
    {
        var chkRenewalServiceExist = false
        
        let arrayData = getDataFromLocal(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", "serviceId == %@", strWoLeadId, strRenewalServiceId))
        
        if arrayData.count > 0
        {
            chkRenewalServiceExist = true
        }
        
        return chkRenewalServiceExist
    }

    func chkRenewalService(strCategorySysName : String, strServiceSysName: String) -> Bool
    {
        
        var isRenwal = Bool()
        isRenwal = false
        
        
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

        if dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
           
            for item in arrCategory
            {
                let dict = item as! NSDictionary
                
                if strCategorySysName == "\(dict.value(forKey: "SysName") ?? "")"
                {
                    if dict.value(forKey: "Services") is NSArray
                    {
                        let arrService = dict.value(forKey: "Services") as! NSArray
                        for itemService in arrService
                        {
                            let dictService = itemService as! NSDictionary
                            
                            if strServiceSysName == "\(dictService.value(forKey: "SysName") ?? "")"
                            {
                                if "\(dictService.value(forKey: "IsRenewal") ?? "")" == "1" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "true" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "True"
                                {
                                    arrServiceMasterRenewalPrices = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                                    
                                    isRenwal = true
                                    
                                    break
                                }
                            }
                        }
                    }
                }
                
                if isRenwal == true
                {
                    break
                }
                
            }
            
        }
        
        return isRenwal
        
    }
    
    func saveRenewalServiceInToCoreData(dictRenewalData : NSDictionary , TotalInitialPrice strInitialPrice: String, ServiceId strServiceId: String, ServiceMasterId strServiceMasterIdRenewal: String)
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strRenewalAmount = String()
        var strRenewalPercentage = String()
                
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
        {
            strRenewalPercentage = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")"
            
            var price = Float()
            var per = Float()
            per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
            price =  (Float(strInitialPrice) ?? 0.0 ) * per / 100
            
            if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            {
                price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            }
            
            strRenewalAmount = String(format: "%.2f", price)
        }
        else
        {
            strRenewalPercentage = "0.00"
        }
        
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
        {
            strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
        }
       
        arrOfKeys = ["companyKey",
                     "leadId",
                     "renewalAmount",
                     "renewalDescription",
                     "renewalFrequencySysName",
                     "renewalPercentage",
                     "renewalServiceId",
                     "serviceId",
                     "soldServiceStandardId",
                     "userName"]
        
        arrOfValues = [strCompanyKey,
                       strWoLeadId,
                       strRenewalAmount,
                       "\(dictRenewalData.value(forKey: "Description") ?? "")",
                       "\(dictRenewalData.value(forKey: "FrequencySysName") ?? "")",
                       strRenewalPercentage,
                       strServiceMasterIdRenewal,
                       strServiceMasterIdRenewal,
                       strServiceId,
                       strUserName]
        
        saveDataInDB(strEntity: "RenewalServiceExtDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }


}

// MARK: - ----------------UITableViewDelegate --------------------
// MARK: -

extension NPMA_Proposal_iPadVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if(tableView == tv_Finding)
        {
           // return aryFinding.count
            return 15
        }
        else if(tableView == tblStandardService)
        {
           // return aryFinding.count
            if(serviceType == .standard)
            {
                return arrServiceData.count
            }
            else if(serviceType == .customized)
            {
                return arrCustomizedServiceData.count
            }
            else
            {
                return 0
            }
            
            
        }
        else if(tableView == tblRenewalService)
        {
            return arrRenewalData.count
        }
        return  0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(tableView == tv_Finding){
            let cell = tv_Finding.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "IncepectionCellFinding" : "IncepectionCellFinding", for: indexPath as IndexPath) as! IncepectionCell
            cell.finding_btn_Resolved.tag = indexPath.row
            cell.finding_btn_Resolved.addTarget(self, action: #selector(actionFindingResolved), for: .touchUpInside)
            return cell
        }
        else if(tableView == tblStandardService)
        {
            let cell = tblStandardService.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "StandardTableViewCell" : "StandardTableViewCell", for: indexPath as IndexPath) as! StandardTableViewCell
            
            if serviceType == .standard
            {
                //arrServiceData
                
                let dict = arrServiceData.object(at: indexPath.row) as! NSManagedObject
            
                cell.lblServiceName.text = "\(dictServiceNameFromSysName.value(forKey: "\(dict.value(forKey: "serviceSysName") ?? "")") ?? "")"
                //"termite singature pest "
                
                cell.lblInitialPrice.text = stringToFloat(strValue: "\(dict.value(forKey: "totalInitialPrice") ?? "")")
                
                cell.lblMaintPrice.text = stringToFloat(strValue: "\(dict.value(forKey: "totalMaintPrice") ?? "")")

                
                cell.lblFrequency.text =  "\(dict.value(forKey: "serviceFrequency") ?? "")"
            
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                
                cell.btnEdit.addTarget(self, action: #selector(actionEditStandardService(sender:)), for: .touchUpInside)

                cell.btnDelete.addTarget(self, action: #selector(actionDeleteStandardService(sender:)), for: .touchUpInside)
                
                cell.btnEdit.isHidden = true

                
            }
            else if serviceType == .customized
            {
                //arrCustomizedServiceData
                let dict = arrCustomizedServiceData.object(at: indexPath.row) as! NSManagedObject
            
                cell.lblServiceName.text =  "\(dict.value(forKey: "serviceName") ?? "")"//"termite singature pest "
                cell.lblInitialPrice.text =  stringToFloat(strValue: "\(dict.value(forKey: "initialPrice") ?? "")")

                cell.lblMaintPrice.text =  stringToFloat(strValue: "\(dict.value(forKey: "maintenancePrice") ?? "")")

                
                
                cell.lblFrequency.text =  "\(dict.value(forKey: "serviceFrequency") ?? "")"

                
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                
                cell.btnEdit.addTarget(self, action: #selector(actionEditCustomizedService(sender:)), for: .touchUpInside)

                cell.btnDelete.addTarget(self, action: #selector(actionDeleteCustomizedService(sender:)), for: .touchUpInside)
                
                cell.btnEdit.isHidden = true

            }
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
            {
                cell.btnEdit.isEnabled = false
                cell.btnDelete.isEnabled = false
            }
           
            return cell
        }
        else if(tableView == tblRenewalService)
        {
            let cell = tblRenewalService.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "StandardTableViewCell" : "StandardTableViewCell", for: indexPath as IndexPath) as! StandardTableViewCell
            
                let dict = arrRenewalData.object(at: indexPath.row) as! NSManagedObject
            
            cell.lblServiceName.text = "\(dictServiceNameFromId.value(forKey: "\(dict.value(forKey: "serviceId") ?? "")") ?? "")"
            
            cell.lblFrequency.text = "\(dictFreqNameFromSysName.value(forKey: "\(dict.value(forKey: "renewalFrequencySysName") ?? "")") ?? "")"
            
            cell.lblInitialPrice.text = stringToFloat(strValue: "\(dict.value(forKey: "renewalAmount") ?? "")")

            
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnEdit.isHidden = true
            
           // cell.btnEdit.addTarget(self, action: #selector(actionEditStandardService(sender:)), for: .touchUpInside)
            
            cell.btnDelete.addTarget(self, action: #selector(actionDeleteRenewalService(sender:)), for: .touchUpInside)
            
            return cell
        }
        else
        {
            let cell = tv_Finding.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "IncepectionCellFinding" : "IncepectionCellFinding", for: indexPath as IndexPath) as! IncepectionCell
       
           
            return cell
        }
         
  
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: -
    // MARK: ----------Cell Action---------
    
    @objc func actionFindingResolved(sender : UIButton) {
        self.view.endEditing(true)
    }
    
    @objc func actionDeleteStandardService(sender : UIButton)
    {
        if serviceType == .standard
        {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.view.endEditing(true)

            let dict = self.arrServiceData.object(at: sender.tag) as! NSManagedObject
            
            let arrayData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@", self.strWoLeadId, "\(dict.value(forKey: "soldServiceStandardId") ?? "")"))
            
            let arrayDataRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@", self.strWoLeadId, "\(dict.value(forKey: "soldServiceStandardId") ?? "")"))

            if arrayData.count > 0
            {
                let objData = arrayData[0] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
            if arrayDataRenewal.count > 0
            {
                let objData = arrayDataRenewal[0] as! NSManagedObject
                deleteDataFromDB(obj: objData)
            }
            self.fetchStandardService()
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
        }
        
    }
    @objc func actionEditStandardService(sender : UIButton)
    {
        btnUpdateStandardService.isHidden = false
        btnCancelStandard.isHidden = false
        btnAddStandard.isHidden = true
        
        
        self.view.endEditing(true)
        
        let dict = self.arrServiceData.object(at: sender.tag) as! NSManagedObject
        
        strCategorySysName = "\(dict.value(forKey: "categorySysName") ?? "")"
        let dictCategoryTemp = getCategoryObjectFromSysName(strSysName: strCategorySysName)
        strCategoryName = "\(dictCategoryTemp.value(forKey: "Name") ?? "")"
        btnCategoryStanadard.setTitle("\(dictCategoryTemp.value(forKey: "Name") ?? "")", for: .normal)
        
        strServiceSysNameStandard = "\(dictService.value(forKey: "SysName") ?? "")"
        let dictServiceTemp = getServiceObjectFromSysName(strSysName: strServiceSysNameStandard)
        strServiceIdStandard = "\(dictServiceTemp.value(forKey: "ServiceMasterId") ?? "")"
        strServiceNameStandard = "\(dictServiceTemp.value(forKey: "Name") ?? "")"
        btnServiceStanadard.setTitle("\(dictServiceTemp.value(forKey: "Name") ?? "")", for: .normal)
        
        
        
        strFreqencySysNameStandard = "\(dictFrequency.value(forKey: "SysName") ?? "")"
        let dictFreqTemp = getFrequencyObjectFromSysName(strSysName: strFreqencySysNameStandard)
        strFreqencyNameStandard = "\(dictFreqTemp.value(forKey: "FrequencyName") ?? "")"
        btnFrequencyStanadard.setTitle("\(dictFreqTemp.value(forKey: "FrequencyName") ?? "")", for: .normal)
        
        
        txtFldInitialPriceStandard.text = "\(dict.value(forKey: "initialPrice") ?? "")"
        txtFldMaintPriceStandard.text = "\(dict.value(forKey: "maintenancePrice") ?? "")"

        //fetchStandardService()
    }
    @objc func actionDeleteCustomizedService(sender : UIButton)
    {
        
        if serviceType == .customized
        {
            let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                
                self.view.endEditing(true)
                let dict = self.arrCustomizedServiceData.object(at: sender.tag) as! NSManagedObject
                
                let arrayData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", self.strWoLeadId, "\(dict.value(forKey: "soldServiceNonStandardId") ?? "")"))
                
                if arrayData.count > 0
                {
                    let objData = arrayData[0] as! NSManagedObject
                    deleteDataFromDB(obj: objData)
                    self.fetchCustomizedService()
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
        }
        
    }
    @objc func actionDeleteRenewalService(sender : UIButton)
    {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
            
            self.view.endEditing(true)

            let dict = self.arrRenewalData.object(at: sender.tag) as! NSManagedObject
                    
            let arrayData = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@", self.strWoLeadId, "\(dict.value(forKey: "soldServiceStandardId") ?? "")"))

            if arrayData.count > 0
            {
                let objData = arrayData[0] as! NSManagedObject
                deleteDataFromDB(obj: objData)
                self.fetchStandardService()

            }
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
        }))

        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
        
        
       
    }
    @objc func actionEditCustomizedService(sender : UIButton)
    {
        self.view.endEditing(true)
        fetchCustomizedService()
    }
}





// MARK: -
// MARK: ----------IncepectionCell---------
/*class IncepectionCell: UITableViewCell {
    @IBOutlet weak var finding_btn_Resolved: UIButton!
    @IBOutlet weak var finding_lbl_FindingCode: UILabel!
    @IBOutlet weak var finding_lbl_Observation: UILabel!
    @IBOutlet weak var finding_lbl_CreatedDate: UILabel!
    @IBOutlet weak var finding_lbl_ResolvedDate: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}*/
class StandardTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var lblServiceName: UILabel!
    
    @IBOutlet weak var lblInitialPrice: UILabel!
    
    @IBOutlet weak var lblMaintPrice: UILabel!
    
    @IBOutlet weak var lblFrequency: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    //Agreement
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var btnAddToAgreement: UIButton!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var viewRenewalData: UIView!
    
    @IBOutlet weak var lblRenewalPrice: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK: - ---------------------- Extenstion ----------------------

//MARK: - --------- Extenstion TableView ------------

extension NPMA_Proposal_iPadVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        yesEditedSomething = true
        
        if tag == 200
        {
            dictCategory = dictData
            strCategorySysName = "\(dictCategory.value(forKey: "SysName") ?? "")"
            strCategoryName = "\(dictCategory.value(forKey: "Name") ?? "")"
            
            btnCategoryStanadard.setTitle("\(dictCategory.value(forKey: "Name") ?? "")", for: .normal)
            
            strServiceSysNameStandard = ""
            strFreqencySysNameStandard = ""
            btnServiceStanadard.setTitle("Select Service", for: .normal)
            //btnFrequencyStanadard.setTitle("--Select Frequency--", for: .normal)
            
            
        }
        else if tag == 201
        {
            dictService = dictData
            strServiceSysNameStandard = "\(dictService.value(forKey: "SysName") ?? "")"
            
            strServiceIdStandard = "\(dictService.value(forKey: "ServiceMasterId") ?? "")"
            
            strServiceNameStandard = "\(dictService.value(forKey: "Name") ?? "")"
            btnServiceStanadard.setTitle("\(dictService.value(forKey: "Name") ?? "")", for: .normal)
            
           // txtViewServiceDescStandService.text = "\(dictService.value(forKey: "Description") ?? "")".html2String
        }
        else if tag == 202
        {
            dictFrequency = dictData
            strFreqencySysNameStandard = "\(dictFrequency.value(forKey: "SysName") ?? "")"
            strFreqencyNameStandard = "\(dictFrequency.value(forKey: "FrequencyName") ?? "")"
            btnFrequencyStanadard.setTitle("\(dictFrequency.value(forKey: "FrequencyName") ?? "")", for: .normal)
        }
        else if tag == 203
        {
            dictDepartment = dictData
            strDeptSysName = "\(dictDepartment.value(forKey: "SysName") ?? "")"
            btnDepartmentCustomized.setTitle("\(dictDepartment.value(forKey: "Name") ?? "")", for: .normal)
            
            strFreqencySysNameCustomized = ""
            //btnFrequencyCustomized.setTitle("--Select Frequency--", for: .normal)
            
        }
        else if tag == 204
        {
            dictFrequencyNonStan = dictData
            strFreqencySysNameCustomized = "\(dictFrequencyNonStan.value(forKey: "SysName") ?? "")"
            strFreqencyNameCustomized = "\(dictFrequencyNonStan.value(forKey: "FrequencyName") ?? "")"
            btnFrequencyCustomized.setTitle("\(dictFrequencyNonStan.value(forKey: "FrequencyName") ?? "")", for: .normal)
        }
        
        
        else if(tag == 101)// Cover Letter
        {
            btnCoverletter.setTitle("\(dictData.value(forKey: "TemplateName")!)", for: .normal)
            dictCoverLetter = dictData

        }
        
        else if(tag == 102)// Introduction
        {
            btnIntroductionLetter.setTitle("\(dictData.value(forKey: "TemplateName")!)", for: .normal)
            
            dictIntroduction = dictData
            
            txtViewIntroductionLetter.attributedText = htmlAttributedString(strHtmlString: "\(dictData.value(forKey: "TemplateContent")!)")
            
        }
        else if(tag == 103)// marketing content
        {
            
            arrayMarketingContentMultipleSelected = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            var termsTitle = ""
            for item in arrayMarketingContentMultipleSelected
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "Title")!)" + ","
            }
            if(arrayMarketingContentMultipleSelected.count > 0)
            {
                termsTitle.removeLast()
                btnSalesMarketingContent.setTitle(termsTitle, for: .normal)
            }
            else
            {
                btnSalesMarketingContent.setTitle("---Select---", for: .normal)
            }
            
        }
        else if(tag == 104)// terms of service
        {
            btnTermsService.setTitle("\(dictData.value(forKey: "Title")!)", for: .normal)
            
            dictTermsOfService = dictData
            
            txtViewTermsService.attributedText = htmlAttributedString(strHtmlString: "\(dictData.value(forKey: "Description")!)")
            
        }
        else if(tag == 105)// terms and conditions
        {
            arrayTermsAndConditionsMultipleSelected = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            
            var termsTitle = ""
            
            for item in arrayTermsAndConditionsMultipleSelected
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                termsTitle.removeLast()
                btnTermsCondition.setTitle(termsTitle, for: .normal)
            }
            else
            {
                btnTermsCondition.setTitle("---Select---", for: .normal)
            }

        }
    }
}

// MARK: -
// MARK: ------- NPMA_UITextFieldDelegate---------

extension NPMA_Proposal_iPadVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {


        if ( textField == txtFldInitialPriceStandard || textField == txtFldInitialPriceCustomized || textField == txtFldMaintPriceStandard || textField == txtFldMaintPriceCustomized)
        {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)

            return chk
            
        }
        return true
    }
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
