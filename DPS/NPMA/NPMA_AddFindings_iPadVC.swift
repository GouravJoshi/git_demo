//  NPMA_AddFindings_iPadVC.swift
//  Created by Rakesh Jain on 12/09/19.
//  Copyright © 2019 Saavan. All rights reserved. Saavan Changes to commit


import UIKit
import MobileCoreServices

class NPMA_AddFindings_iPadVC: UIViewController ,UIDocumentPickerDelegate {
    
    // outlets
                
    @IBOutlet weak var btnSubsectionCode: UIButton!
    
    @IBOutlet weak var txtFilling_IssueCode: UITextField!
    
    @IBOutlet weak var txtviewFinding: UITextView!
    
    @IBOutlet weak var btnRecommendationCode: UIButton!
    
    @IBOutlet weak var txtFilling_Recommendation: UITextField!
    
    @IBOutlet weak var txtviewRecommendation: UITextView!
                    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var btnDetailSubsectionCode: UIButton!
    
    @IBOutlet weak var btnDetailRecommendationCode: UIButton!
    
    @IBOutlet weak var problemImgCollectionView: UICollectionView!
    
    @IBOutlet weak var hghtConstCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var hghtConstScrollView: NSLayoutConstraint!
        
    @IBOutlet weak var btnEditSubsectionFinding: UIButton!
    
    @IBOutlet weak var btnEditRecommendationFinding: UIButton!
                    
    @IBOutlet weak var btnImgPickerBtn: UIButton!
    
    @IBOutlet weak var btnShowFavouriteSubsectionCode: UIButton!
    

    @IBOutlet weak var btnShowFavouriteRecmmendationCode: UIButton!
    
    //MARK: - -----------------------------------Global Variables -----------------------------------
    
    @objc  var strWoId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    var objWorkorderDetail = NSManagedObject()
    var strToUpdate = String()
    var dictDataSubsecutionCode = NSDictionary()
    var dictDataRecommendationCode = NSDictionary()
    var strProblemIdentificationIdToUpdate = String()
    var strMobileProblemIdentificationIdToUpdate = String()
    var strRecommendationCodeId = String()
    var strSubsectionCodeId = String()
    //var strInfestation = String()
    //var isLeadTest = "Yes"
    var isChanged = false
    //var showLeadTest = false
    //var showInfestation = false
    var isRecommendationSelected = false
    @objc  var strWoLeadId = NSString ()
    var arrOfProblemImages = NSArray()
    var imagePicker = UIImagePickerController()
    @objc var strWoStatus = NSString ()
    var strMobileProblemIdentificationIdInitial = String()
    var strMobileLeadCommercialIdIdInitial = String()
    var isAddedProblemId = false
    var isEditedSubsectionFinding = false
    var isEditedRecommendationFinding = false
    //var isBuldingPermit = true
    var dictDataGeneralDescription = NSDictionary()
    var isSelectedSubsectionFindingFromDropDown = false
    var isSelectedRecommendationFindingFromDropDown = false
    var arrOfFavouriteCodes = NSArray()
    //MARK: - -----------------------------------Life Cycle -----------------------------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureUI()
        btnSubsectionCode.setTitle(strSelectString, for: .normal)
        btnRecommendationCode.setTitle(strSelectString, for: .normal)
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            strWoStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")" as NSString

            if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
                //testController?.strWoStatus = "Completed"
            }else{
                strWoStatus = "InComplete"
            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            self.back()
        }
        
        if strToUpdate == "Yes" {
            
            setValuesToUpdate()
            
        }else{
            
            // Create this id to add images before saving Problem in DB
            strMobileProblemIdentificationIdInitial = Global().getReferenceNumber()
            strMobileLeadCommercialIdIdInitial = Global().getReferenceNumber()
            
        }
        
        disableControls()
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddToFavourite_Notification"),
        object: nil,
        queue: nil,
        using:catchNotification1)
        
        
        if (isInternetAvailable())
        {
            if nsud.bool(forKey: "isSyncFavourite") == true
            {
                callAPIMarkAsFavourite()
            }
            else
            {
                getFinalCodes()
            }
        }
        else
        {
            getFinalCodes()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        fetchProblemImages()
        
        // If From Editing Html Contents
        if (nsud.value(forKey: "EditedHtmlContents") != nil) {
            
            let isScannedCode = nsud.bool(forKey: "EditedHtmlContents")
            
            if isScannedCode {
                
                nsud.set(false, forKey: "EditedHtmlContents")
                nsud.synchronize()
                
                //htmlContents
                
                    let strCodeType = "\(nsud.value(forKey: "CodeType")!)"
                    
                    if strCodeType == "Subsection" {
                        
                        txtviewFinding.attributedText = htmlAttributedString(strHtmlString: "\(nsud.value(forKey: "htmlContents")!)")
                        
                        nsud.set("\(nsud.value(forKey: "htmlContents")!)", forKey: "SubsectionCode")
                        nsud.synchronize()
                        
                        //strHtmlEditedSubsectionFinding = "\(attributedString)"
                        isEditedSubsectionFinding = true
                        
                        isChanged = true
                        
                    }else{
                        
                        txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(nsud.value(forKey: "htmlContents")!)")
                        
                        isEditedRecommendationFinding = true
                        
                        nsud.set("\(nsud.value(forKey: "htmlContents")!)", forKey: "RecommendationCode")
                        nsud.synchronize()
                        
                        //strHtmlEditedRecommendationFinding = "\(attributedString)"
                        
                        isChanged = true

                    }
                
            }
            
        }
        
    }
    
    
    //MARK: - -----------------------------------Button Actions -----------------------------------
    
    @IBAction func action_FurtherInspection(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
                
    }
    
    
    @IBAction func action_RecommendationFillingHelp(_ sender: Any) {
        
        if dictDataRecommendationCode.count > 0 {
            
            let alert = UIAlertController(title: "Filling Help", message: "\(dictDataRecommendationCode.value(forKey: "RecommendationFillInHelp")!)", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
            
        }
        
    }
    
    
    @IBAction func action_FillingHelp(_ sender: Any) {
        
        if dictDataSubsecutionCode.count > 0 {
            
            let alert = UIAlertController(title: "Filling Help", message: "\(dictDataSubsecutionCode.value(forKey: "FindingFillInHelp")!)", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                
                
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
            
        }
        
    }
    @IBAction func action_SubsectionCode(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    @IBAction func action_RecommendationCode(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if ary.count != 0{
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    
    @IBAction func action_Live_Infastation(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)

        
    }
    
    @IBAction func action_ConduciveCondition_Infastation(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
    }
    
    @IBAction func action_FurtherInspection_Infastation(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
        
    }
    
    @IBAction func action_NonSectionfinding_Infastation(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)
    }
    
    @IBAction func action_Yes_LeadTest(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)

    }
    
    @IBAction func action_No_LeadTest(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)

    }
    
    @IBAction func action_YesBuildingPermit(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)

    }
    
    @IBAction func action_NoBuildingPermit(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        sender.setImage(UIImage(named: "radio_check_ipad"), for: .normal)

    }
    
    @IBAction func action_Cancel(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        back()
        
    }
    
    @IBAction func action_Add(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(validationForAddProblemIdentification()){
            
            var strServiceJobTitleId = ""
            var strGeneralNotesDesc = ""

            strServiceJobTitleId = ""
            strGeneralNotesDesc = ""
            
            if strToUpdate == "Yes" {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("isActive")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("finding")
                arrOfKeys.add("findingFillIn")
                arrOfKeys.add("infestation")
                arrOfKeys.add("isChanged")
                arrOfKeys.add("isLeadTest")
                arrOfKeys.add("issuesCode")
                arrOfKeys.add("recommendation")
                arrOfKeys.add("recommendationCode")
                arrOfKeys.add("recommendationCodeId")
                arrOfKeys.add("recommendationFillIn")
                arrOfKeys.add("subsectionCode")
                arrOfKeys.add("subsectionCodeId")
                arrOfKeys.add("isAddToAgreement")
                arrOfKeys.add("isBuildingPermit")
                arrOfKeys.add("generalNotes")
                arrOfKeys.add("generalNoteId")

                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strWoId)
                arrOfValues.add(true)
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                //arrOfValues.add(txtviewFinding.text!)
                
                if isEditedSubsectionFinding {
                    
                    arrOfValues.add("\(nsud.value(forKey: "SubsectionCode")!)")

                }else{
                    
                    var arrOfProblemIdentification = NSArray()
                    
                    if strProblemIdentificationIdToUpdate.count > 0 {
                        
                        arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@ && subsectionCodeId == %@", strWoId , strProblemIdentificationIdToUpdate , "\(dictDataSubsecutionCode.value(forKey: "CodeMasterId")!)"))
                        
                    }else{
                        
                        arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@ && subsectionCodeId == %@", strWoId , strMobileProblemIdentificationIdToUpdate, "\(dictDataSubsecutionCode.value(forKey: "CodeMasterId")!)"))
                        
                    }
                    
                    if isSelectedSubsectionFindingFromDropDown {
                        
                        arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
                        
                    }else if arrOfProblemIdentification.count > 0 {
                        
                        let objData = arrOfProblemIdentification[0] as! NSManagedObject
                        
                        arrOfValues.add("\(objData.value(forKey: "Finding")!)")

                    }else{
                        
                        arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
                        
                    }
                    
                }
                
                arrOfValues.add(txtFilling_IssueCode.text!)
                
                arrOfValues.add("")
                
                if isChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                
                arrOfValues.add("")
                
                arrOfValues.add("")
                //arrOfValues.add(txtviewRecommendation.text!)
                
                if isEditedRecommendationFinding {
                    
                    arrOfValues.add("\(nsud.value(forKey: "RecommendationCode")!)")
                    
                }else{
                    
                    var arrOfProblemIdentification = NSArray()
                    
                    if strProblemIdentificationIdToUpdate.count > 0 {
                        
                        arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@ && recommendationCodeId == %@", strWoId , strProblemIdentificationIdToUpdate, "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"))
                        
                    }else{
                        
                        arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@ && recommendationCodeId == %@", strWoId , strMobileProblemIdentificationIdToUpdate, "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"))
                        
                    }
                    
                    if isSelectedRecommendationFindingFromDropDown {
                        
                        arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                        
                    }else if arrOfProblemIdentification.count > 0 {
                        
                        let objData = arrOfProblemIdentification[0] as! NSManagedObject
                        
                        arrOfValues.add("\(objData.value(forKey: "Recommendation")!)")

                    }else{
                        
                        arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")

                    }
                    
                    //arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                    
                }
                
                arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "CodeName")!)")
                arrOfValues.add(strRecommendationCodeId)
                arrOfValues.add(txtFilling_Recommendation.text!)
                arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)")
                arrOfValues.add(strSubsectionCodeId)
                arrOfValues.add(false)
                arrOfValues.add(false)

                arrOfValues.add(strGeneralNotesDesc)
                arrOfValues.add(strServiceJobTitleId)

                var isSuccess = false
                
                if strProblemIdentificationIdToUpdate.count > 0 {
                    
                    isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId, strProblemIdentificationIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    checkIssueCodeToSetTermiteCodeSelected(strProblemId: strProblemIdentificationIdToUpdate, strType: "problemIdentificationId")

                    self.updateTarget(strWdoProblemIdentificationId: strProblemIdentificationIdToUpdate , strWdoProblemIdentificationIdSecond: strMobileProblemIdentificationIdToUpdate)

                }else{
                    
                    isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId, strMobileProblemIdentificationIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                    checkIssueCodeToSetTermiteCodeSelected(strProblemId: strMobileProblemIdentificationIdToUpdate, strType: "mobileId")

                    self.updateTarget(strWdoProblemIdentificationId: strMobileProblemIdentificationIdToUpdate , strWdoProblemIdentificationIdSecond: strProblemIdentificationIdToUpdate)

                }
                
                //let isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId, strProblemIdentificationIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                    back()
                    
                } else {
                    
                    showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
                    
                }
                
            }else{
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("workOrderId")
                arrOfKeys.add("isActive")
                arrOfKeys.add("createdBy")
                arrOfKeys.add("createdDate")
                arrOfKeys.add("modifiedBy")
                arrOfKeys.add("modifiedDate")
                arrOfKeys.add("finding")
                arrOfKeys.add("findingFillIn")
                arrOfKeys.add("infestation")
                arrOfKeys.add("isChanged")
                arrOfKeys.add("isLeadTest")
                arrOfKeys.add("issuesCode")
                arrOfKeys.add("problemIdentificationId")
                arrOfKeys.add("mobileId")
                arrOfKeys.add("recommendation")
                arrOfKeys.add("recommendationCode")
                arrOfKeys.add("recommendationCodeId")
                arrOfKeys.add("recommendationFillIn")
                arrOfKeys.add("subsectionCode")
                arrOfKeys.add("subsectionCodeId")
                arrOfKeys.add("isAddToAgreement")
                arrOfKeys.add("isBuildingPermit")
                arrOfKeys.add("proposalDescription")
                arrOfKeys.add("generalNotes")
                arrOfKeys.add("generalNoteId")
                arrOfKeys.add("isResolved")
                arrOfKeys.add("resolvedDate")

                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strWoId)
                arrOfValues.add(true)
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                arrOfValues.add(Global().getEmployeeId())
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                //arrOfValues.add(txtviewFinding.text!)
                
                if isEditedSubsectionFinding {
                    
                    arrOfValues.add("\(nsud.value(forKey: "SubsectionCode")!)")
                    
                }else{
                    
                    arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
                    
                }
                
                arrOfValues.add(txtFilling_IssueCode.text!)
                //arrOfValues.add(strInfestation)
                
                arrOfValues.add("")
                
                if isChanged {
                    
                    arrOfValues.add(true)
                    
                }else{
                    
                    arrOfValues.add(false)
                    
                }
                
                arrOfValues.add("")
                
                arrOfValues.add("")
                
                //let problemId = Global().getReferenceNumber()
                let problemId = strMobileProblemIdentificationIdInitial

                arrOfValues.add("")
                arrOfValues.add(problemId)
                //arrOfValues.add(txtviewRecommendation.text!)
                
                if isEditedRecommendationFinding {
                    
                    arrOfValues.add("\(nsud.value(forKey: "RecommendationCode")!)")
                    
                }else{
                    
                    arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                    
                }
                
                arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "CodeName")!)")
                arrOfValues.add(strRecommendationCodeId)
                arrOfValues.add(txtFilling_Recommendation.text!)
                arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)")
                arrOfValues.add(strSubsectionCodeId)
                arrOfValues.add(false)
                arrOfValues.add(false)

                arrOfValues.add("\(dictDataRecommendationCode.value(forKey: "PricingDescription")!)")//PricingDescription
                arrOfValues.add(strGeneralNotesDesc)
                arrOfValues.add(strServiceJobTitleId)
                arrOfValues.add(false)
                arrOfValues.add("")

                // Saving Disclaimer In DB
                saveDataInDB(strEntity: Entity_ProblemIdentifications, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                //saveProblePricingDefaultBlank(strProblemId: problemId)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                checkIssueCodeToSetTermiteCodeSelected(strProblemId: problemId, strType: "mobileId")
                
                isAddedProblemId = true
                
                self.saveTarget(strMobileTargetId: strMobileLeadCommercialIdIdInitial, strWdoProblemIdentificationId: problemId)
                
                back()
                
                
            }
            
            nsud.set(true, forKey: "addedProblem")
            nsud.synchronize()

        }
        
    }
    
    @IBAction func action_back(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        back()
        
    }
    
    @IBAction func actionOnShowDetailedSubsectionCode(_ sender: Any)
    {
        var array = NSMutableArray()
        
        // array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        
        if array.count > 0 {
            
            self.goToSelection(strTags: 107 , strTitleToShow: "Finding Code" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
    }
    
    @IBAction func actionOnShowDetailedRecommendationCode(_ sender: Any)
    {
        var array = NSMutableArray()
        
        // array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        
        if array.count > 0 {
            
            self.goToSelection(strTags: 108 , strTitleToShow: "Recommendation Code" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
        
    }
    @IBAction func actionOnShowDetailFavouriteSubsectionCode(_ sender: Any)
    {
        
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        
        
        let arrMutable = NSMutableArray()
        
        for item in array
        {
            let dict = item as! NSDictionary
            
            for itemSecond in arrOfFavouriteCodes
            {

                let str = itemSecond as! String
                
                if "\(dict.value(forKey: "CodeMasterId") ?? "")" == str
                {
                    arrMutable.add(dict)
                }

            }
            
        }
        
        array = NSMutableArray()
        array = arrMutable
        
        
        if array.count > 0 {
            
            self.goToSelection(strTags: 107 , strTitleToShow: "Finding Code" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
        
    }
    @IBAction func actionOnShowDetailFavouriteRecmmendationCode(_ sender: Any)
    {
        var array = NSMutableArray()
        
        array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
        
        
        let arrMutable = NSMutableArray()
        
        for item in array
        {
            let dict = item as! NSDictionary
            
            for itemSecond in arrOfFavouriteCodes
            {

                let str = itemSecond as! String
                
                if "\(dict.value(forKey: "CodeMasterId") ?? "")" == str
                {
                    arrMutable.add(dict)
                }

            }
            
        }
        
        array = NSMutableArray()
        array = arrMutable
        
        if array.count > 0 {
            
            self.goToSelection(strTags: 108 , strTitleToShow: "Recommendation Code" , array: array)
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
            
        }
        
        
    }
    
    @IBAction func action_Image(_ sender: UIButton) {
        
        if arrOfProblemImages.count < 10 {
            
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
                self.imagePicker = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }))
            
            
            alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
                self.imagePicker = UIImagePickerController()
                
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }))
            
            
//            alert.addAction(UIAlertAction(title: "More...", style: .default , handler:{ (UIAlertAction)in
//
//                let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypeImage)], in: .import)
//                importMenu.delegate = self
//                importMenu.modalPresentationStyle = .formSheet
//                self.present(importMenu, animated: true, completion: nil)
//
//            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            //alert.popoverPresentationController?.sourceView = self.view
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as UIView
                popoverController.sourceRect = sender.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            
            self.present(alert, animated: true, completion: {
            })
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Max 10 no. of images can be chosen", viewcontrol: self)
            
        }
        
    }
    
    @IBAction func action_EditSubsectionFinding(_ sender: Any) {
        
        //if txtviewFinding.text.count > 0 {
            
            nsud.set("Subsection", forKey: "CodeType")
            nsud.synchronize()
            
            if isEditedSubsectionFinding {
                
                let description = "\(nsud.value(forKey: "SubsectionCode")!)"
                htmlEditorView(htmlString: description)
                
            }else{
                
                var arrOfProblemIdentification = NSArray()
                
                if strProblemIdentificationIdToUpdate.count > 0 {
                    
                    arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdentificationIdToUpdate))
                    
                }else{
                    
                    arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId , strMobileProblemIdentificationIdToUpdate))
                    
                }
                
                if arrOfProblemIdentification.count > 0 {
                    
                    let objData = arrOfProblemIdentification[0] as! NSManagedObject
                    
                    htmlEditorView(htmlString: "\(objData.value(forKey: "Finding")!)")

                }else{
                    
                    if dictDataSubsecutionCode.count > 0 {
                        
                        htmlEditorView(htmlString: "\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
                        
                    }

                }
                
                //htmlEditorView(htmlString: "\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
                
            }
            
        //}
        
    }
    
    @IBAction func action_EditRecommendationFinding(_ sender: Any) {
        
        //if txtviewRecommendation.text.count > 0 {
            
            nsud.set("Recommendation", forKey: "CodeType")
            nsud.synchronize()
            
            if isEditedRecommendationFinding {
                
                let description = "\(nsud.value(forKey: "RecommendationCode")!)"
                htmlEditorView(htmlString: description)
                
            }else{
                
                var arrOfProblemIdentification = NSArray()
                
                if strProblemIdentificationIdToUpdate.count > 0 {
                    
                    arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdentificationIdToUpdate))
                    
                }else{
                    
                    arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId , strMobileProblemIdentificationIdToUpdate))
                    
                }
                
                if arrOfProblemIdentification.count > 0 {
                    
                    let objData = arrOfProblemIdentification[0] as! NSManagedObject
                    
                    htmlEditorView(htmlString: "\(objData.value(forKey: "Recommendation")!)")

                }else{
                    
                    if dictDataRecommendationCode.count > 0 {
                        
                        htmlEditorView(htmlString: "\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                        
                    }

                }
                
            }
            
        //}
        
    }
    
    
    @IBAction func action_SelectGeneralDescriptions(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = sender.tag
        if array.count != 0{
            
            array = addSelectInArray(strName: "Title", array: array)

            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = array
            self.present(vc, animated: false, completion: {})
            
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
            
        }
        
    }
    
    
    //MARK: - -----------------------------------Functions-----------------------------------
    
    func disableControls() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            btnSubsectionCode.isUserInteractionEnabled = false
            txtFilling_IssueCode.isUserInteractionEnabled = false
            txtviewFinding.isUserInteractionEnabled = false
            btnRecommendationCode.isUserInteractionEnabled = false
            txtFilling_Recommendation.isUserInteractionEnabled = false
            txtviewRecommendation.isUserInteractionEnabled = false
            btnImgPickerBtn.isUserInteractionEnabled = false
            btnAdd.isUserInteractionEnabled = false
            btnDetailSubsectionCode.isUserInteractionEnabled = false
            btnDetailRecommendationCode.isUserInteractionEnabled = false
            btnEditSubsectionFinding.isUserInteractionEnabled = false
            btnEditRecommendationFinding.isUserInteractionEnabled = false

        }
        
        
    }
    
    func htmlEditorView(htmlString : String) {
        
        let storyboard = UIStoryboard(name: "WDOiPad", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        
        controller.strHtml = htmlString
        
        self.present(controller, animated: false, completion: nil)
        
    }
    
    func checkIssueCodeToSetTermiteCodeSelected(strProblemId : String , strType : String) {
        
        let arrTempLocal = NSMutableArray()

        let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
        
        var arrOfProblemIdentification = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId , strProblemId)), sort: sort)
        
        if strType == "mobileId" {
            
            arrOfProblemIdentification = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId , strProblemId)), sort: sort)
            
        }
        
        if arrOfProblemIdentification.count > 0 {
            
            for k1 in 0 ..< arrOfProblemIdentification.count {
                
                let dictDataProblem = arrOfProblemIdentification[k1] as! NSManagedObject

                let issueCodeLocal = "\(dictDataProblem.value(forKey: "issuesCode") ?? "")"
                
                let characters = Array(issueCodeLocal)
                
                let arrTempChar = NSMutableArray()
                
                if characters.count > 0 {
                    
                    for k in 0 ..< characters.count {
                        
                        let chr = "\(characters[k])"
                        
                        arrTempChar.add(chr)
                        
                    }
                    
                }
                
                let arrTemp = NSMutableArray()
                
                
                if arrTempChar.count > 0 {
                    
                    for k in 0 ..< arrTempChar.count {
                        
                        let char = Int("\(arrTempChar[k])")
                        
                        if char != nil
                        {
                            
                            arrTemp.add(arrTempChar[k])
                            
                        }else{
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                var strStartingCharcters = ""
                
                if arrTemp.count > 0 {
                    
                    strStartingCharcters = arrTemp.componentsJoined(by: "")
                    
                }
                
                if strStartingCharcters.count > 0 {
                    
                    var array = NSMutableArray()
                    
                    array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "TermiteIssueCodeExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "No", strToCheckBranch: "No", strBranchParameterName: "BranchId")
                    
                    if array.count > 0 {
                        
                        for k in 0 ..< array.count {
                            
                            let dictData = array[k] as! NSDictionary
                            
                            if "\(dictData.value(forKey: "IssueCode") ?? "")" == strStartingCharcters && ("\(dictData.value(forKey: "IsActive") ?? "")" == "1" || "\(dictData.value(forKey: "IsActive") ?? "")" == "true" || "\(dictData.value(forKey: "IsActive") ?? "")" == "True") {
                                
                                let arrOfWdoInspection = getDataFromCoreDataBaseArray(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId))
                                
                                if arrOfWdoInspection.count > 0 {
                                    
                                    let objData = arrOfWdoInspection[0] as! NSManagedObject
                                    
                                    let checkBoxes = "\(objData.value(forKey: "wdoTermiteIssueCodeSysNames") ?? "")"
                                    
                                    var tempArray = NSArray()
                                    
                                    if checkBoxes.count > 1 {
                                        
                                        tempArray = checkBoxes.components(separatedBy: ",") as NSArray
                                        
                                    }
                                    
                                    /*
                                     
                                    let arrTempLocal = NSMutableArray()
                                     
                                    */
                                     
                                    if tempArray.count > 0 {
                                        
                                        for p in 0 ..< tempArray.count {
                                            
                                            arrTempLocal.add(tempArray[p])
                                            
                                        }
                                        
                                    }
 
                                    
                                    arrTempLocal.add("\(dictData.value(forKey: "CodeSysName") ?? "")")
                                    
                                    if arrTempLocal.count > 0{
                                        
                                        let strTermiteIssuesCode = arrTempLocal.componentsJoined(by: ",")
                                        
                                        let arrOfKeys = NSMutableArray()
                                        let arrOfValues = NSMutableArray()
                                        
                                        arrOfKeys.add("wdoTermiteIssueCodeSysNames")
                                        arrOfValues.add(strTermiteIssuesCode)
                                        
                                        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WdoInspection, predicate: NSPredicate(format: "workOrderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                                        
                                        if isSuccess {
                                            
                                            //Update Modify Date In Work Order DB
                                            updateServicePestModifyDate(strWoId: self.strWoId as String)
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func setValuesToUpdate() {
        
        var arrOfProblemIdentification = NSArray()
        
        if strProblemIdentificationIdToUpdate.count > 0 {
            
            arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdentificationIdToUpdate))
            
        }else{
            
            arrOfProblemIdentification = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", strWoId , strMobileProblemIdentificationIdToUpdate))
            
        }
        
        if arrOfProblemIdentification.count > 0 {
            
            let objData = arrOfProblemIdentification[0] as! NSManagedObject
            
            btnSubsectionCode.setTitle("\(objData.value(forKey: "subsectionCode") ?? "")", for: .normal)
            strSubsectionCodeId = "\(objData.value(forKey: "subsectionCodeId") ?? "")"
            txtFilling_IssueCode.text = "\(objData.value(forKey: "findingFillIn") ?? "")"
            txtviewFinding.text = "\(objData.value(forKey: "finding") ?? "")"
            
            txtviewFinding.attributedText = htmlAttributedString(strHtmlString: "\(objData.value(forKey: "finding")!)")
            
            btnRecommendationCode.setTitle("\(objData.value(forKey: "recommendationCode") ?? "")", for: .normal)
            strRecommendationCodeId = "\(objData.value(forKey: "recommendationCodeId") ?? "")"
            
            txtFilling_Recommendation.text = "\(objData.value(forKey: "recommendationFillIn") ?? "")"
            
            txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(objData.value(forKey: "recommendation")!)")
            
            isChanged = objData.value(forKey: "isChanged") as! Bool
            
            
            var array = NSMutableArray()
            
            array = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "CodeMasterExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "Yes", strBranchParameterName: "BranchId")
            
            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "CodeMasterId") ?? "")" == "\(objData.value(forKey: "recommendationCodeId") ?? "")"{
                        
                        dictDataRecommendationCode = dictData
                        
                        //break
                        
                    }
                    
                    if "\(dictData.value(forKey: "CodeMasterId") ?? "")" == "\(objData.value(forKey: "subsectionCodeId") ?? "")"{
                        
                        dictDataSubsecutionCode = dictData
                        
                        //break
                        
                    }
                }
                
            }
            
            isRecommendationSelected = true
                        
        }else{
            
            back()
            
        }
        
        
    }
    
    func back() {
        
        if strToUpdate == "Yes" {

        }else{
            
            if !isAddedProblemId {
                
                // No Problem added initially so delete imaged if added without saving
                
                deleteAllRecordsFromDB(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", self.strWoId,strMobileProblemIdentificationIdInitial))
                
                deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId,strMobileLeadCommercialIdIdInitial))

            }
            
        }
        
        nsud.set(true, forKey: "addedProblem")
        nsud.synchronize()
        
        dismiss(animated: false, completion: nil)
        
    }
    
    func saveProblePricingDefaultBlank(strProblemId : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("workOrderId")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        
        
        arrOfKeys.add("problemIdentificationPricingId")
        arrOfKeys.add("problemIdentificationId")
        arrOfKeys.add("hours")
        arrOfKeys.add("materials")
        arrOfKeys.add("subFee")
        arrOfKeys.add("linearFtSoilTreatment")
        arrOfKeys.add("linearFtDrilledConcrete")
        arrOfKeys.add("cubicFt")
        arrOfKeys.add("subTotal")
        arrOfKeys.add("discount")
        arrOfKeys.add("total")
        arrOfKeys.add("isActive")
        arrOfKeys.add("isCalculate")
        arrOfKeys.add("isDiscount")
        arrOfKeys.add("isBidOnRequest")

        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoId)
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfValues.add("")
        arrOfValues.add(strProblemId)
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add("0.00")
        arrOfValues.add("0.00")
        arrOfValues.add("0.00")
        arrOfValues.add(true)
        arrOfValues.add(true)
        arrOfValues.add(false)
        arrOfValues.add(false)

        // Jugad for change in cubic Rate
        
        arrOfKeys.add("isChangedCubicRate")
        arrOfValues.add(false)
        
        saveDataInDB(strEntity: Entity_ProblemIdentificationPricing, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
    }
    
    func configureUI()
    {
        
        makeCornerRadius(value: 2.0, view: btnSubsectionCode, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtFilling_IssueCode, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewFinding, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnRecommendationCode, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtFilling_Recommendation, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: txtviewRecommendation, borderWidth: 1.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnCancel, borderWidth: 0.0, borderColor: UIColor.lightGray)
        makeCornerRadius(value: 2.0, view: btnAdd, borderWidth: 0.0, borderColor: UIColor.lightGray)

        // padding
        txtFilling_IssueCode.setLeftPaddingPoints(5.0)
        txtFilling_Recommendation.setLeftPaddingPoints(5.0)
        
    }
    
    func goToSelection(strTags : Int , strTitleToShow : String , array : NSMutableArray)  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.view.endEditing(true)
            
        })
        
        self.view.endEditing(true)
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc: SelectionClass = storyboardIpad.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitleToShow
        vc.strTag = strTags
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = array
        
        if strTags == 107 || strTags == 108
        {
            let arrCode = NSMutableArray()
            for item in arrOfFavouriteCodes
            {
                //let dict = item as! NSDictionary
                //arrCode.add("\(dict.value(forKey: "CodeMasterId") ?? "")")

                let str = item as! String
                arrCode.add(str)
            }
            vc.arrMarkAsFaourite = arrCode
        }
        //vc.arySelectedTargets = arrayOfCorrectiveActionsSelected
        present(vc, animated: false, completion: {})
        
    }
    
    func saveTarget(strMobileTargetId : String , strWdoProblemIdentificationId : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("isChangedTargetDesc")
        arrOfKeys.add("leadId")
        arrOfKeys.add("leadCommercialTargetId")
        arrOfKeys.add("mobileTargetId")
        arrOfKeys.add("name")
        arrOfKeys.add("targetDescription")
        arrOfKeys.add("targetSysName")
        arrOfKeys.add("wdoProblemIdentificationId")

        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add("false")
        arrOfValues.add(strWoLeadId)
        arrOfValues.add("")
        arrOfValues.add(strMobileTargetId)
        arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)")
        
        // Check if tags are replaced
        
        let strReplacedString = replaceTags(strFillingLocal: "\(txtFilling_IssueCode.text!)", strFindingLocal: "\(txtviewFinding.text!)")
        
        if strReplacedString.count != 0 {
            
            arrOfValues.add(strReplacedString)
            
        }else {
            
            arrOfValues.add(txtviewFinding.text!)
            
        }
        
        arrOfValues.add("")
        arrOfValues.add(strWdoProblemIdentificationId)

        // Saving Target In DB
        saveDataInDB(strEntity: Entity_LeadCommercialTargetExtDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        Global().updateSalesModifydate(strWoLeadId as String)
        
    }
    
    func updateTarget(strWdoProblemIdentificationId : String , strWdoProblemIdentificationIdSecond : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("name")
        arrOfKeys.add("targetDescription")
        arrOfKeys.add("wdoProblemIdentificationId")
        
        arrOfValues.add("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)")
        
        // Check if tags are replaced
        
        let strReplacedString = replaceTags(strFillingLocal: "\(txtFilling_IssueCode.text!)", strFindingLocal: "\(txtviewFinding.text!)")

        
        if strReplacedString.count != 0 {
            
            arrOfValues.add(strReplacedString)
            
        }else {
            
            arrOfValues.add(txtviewFinding.text!)
            
        }
        
        arrOfValues.add(strWdoProblemIdentificationId)
        
        // Updating Target In DB
        let isSuccess = getDataFromDbToUpdate(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId, strWdoProblemIdentificationId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess{
            
            //Updated
            
        }else{
            
            let isSuccess1 = getDataFromDbToUpdate(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId, strWdoProblemIdentificationIdSecond), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess1{
                                
                
            }
            
        }

        Global().updateSalesModifydate(strWoLeadId as String)

    }
    
    
    func saveTargetImage(strLeadCommercialTargetId : String , strMobileTargetId : String , strImagePath : String , imageResized : UIImage , woImageId : String) {
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("leadCommercialTargetId")
        arrOfKeys.add("mobileTargetId")
        arrOfKeys.add("leadImagePath")
        arrOfKeys.add("leadImageType")
        arrOfKeys.add("targetImageDetailId")
        arrOfKeys.add("targetSysName")
        arrOfKeys.add("mobileTargetImageId")
        arrOfKeys.add("leadImageCaption")
        arrOfKeys.add("descriptionImageDetail")

        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strWoLeadId)
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfValues.add(Global().getEmployeeId())
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        arrOfValues.add(latitude)
        arrOfValues.add(longitude)
        arrOfValues.add(strLeadCommercialTargetId)
        arrOfValues.add(strMobileTargetId)
        arrOfValues.add(strImagePath)
        arrOfValues.add("Target")
        arrOfValues.add("")
        arrOfValues.add("")
        arrOfValues.add(woImageId)
        arrOfValues.add("")
        arrOfValues.add("")

        // Saving Target In DB
        saveDataInDB(strEntity: Entity_TargetImageDetail, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        saveImageDocumentDirectory(strFileName: strImagePath, image: imageResized)

        Global().updateSalesModifydate(strWoLeadId as String)

    }
    
    //MARK: ----------------------- API CALLING --------------------
    
    func getFavouriteCodes()
    {
        
        if !isInternetAvailable()
        {
            self.getFinalCodes()
        }
        else
        {
                        
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            let strEmpNumber = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
            
            let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/GetWdoFindingFavoriteDetail?CompanyKey=\(strCompanyKey)&EmployeeNo=\(strEmpNumber)"

            
            WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "Description") { [self] (Response, Status) in
                
                DispatchQueue.main.async{
                    
                }
                if(Status){
                    
                    let arrData = Response["data"] as! NSArray
                    
                    if arrData.isKind(of: NSArray.self)
                    {
                        if arrData.count > 0
                        {
                            //arrOfFavouriteCodes = arrData
                            self.arrOfFavouriteCodes = NSArray()
                            //Temp
                            
                            let arrTemp = NSMutableArray()
                            for item in arrData
                            {
                                let dict = item as! NSDictionary
                                arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                            }
                            self.arrOfFavouriteCodes = arrTemp
                            nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                            nsud.set(false, forKey: "isSyncFavourite")
                            nsud.synchronize()
                            
                            self.getFinalCodes()
                            //ENd
                        }
                        else
                        {
                            
                            //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertDataNotFound, viewcontrol: self)
                            self.getFinalCodes()
                        }
                    }
                    else
                    {
                        
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        self.getFinalCodes()
                        
                    }
                    
                }
                else
                {
                    
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                    self.getFinalCodes()
                    
                }
                
            }
            
        }
        
    }
    func catchNotification1(notification:Notification) {
        
        if (isInternetAvailable()){
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.getFinalCodes()
            }

        }
        
    }
    func getFinalCodes()
    {
        if nsud.value(forKey: "favouriteCode") is NSArray
        {
            let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
            
            let arrUniqueDocuments = arrFvouriteCodesTemp.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        
            arrOfFavouriteCodes = arrUniqueDocuments//arrFvouriteCodesTemp
            
        }
        else
        {
            arrOfFavouriteCodes = NSArray()
        }

    }
    func getPostData() -> NSDictionary
    {
        let arr = NSMutableArray()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        let strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"

        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
        
        if nsud.value(forKey: "favouriteCode") is NSArray
        {
            let arrFvouriteCodesTemp = nsud.value(forKey: "favouriteCode") as! NSArray
            arrOfFavouriteCodes = arrFvouriteCodesTemp
        }
        else
        {
            arrOfFavouriteCodes = NSArray()
        }
        
        for item in arrOfFavouriteCodes
        {
            let strId = item as! String
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("WoWdoFindingFavoriteDetailId")
            arrOfKeys.add("CodeMasterId")
            arrOfKeys.add("EmployeeId")
            arrOfKeys.add("CompanyId")
            arrOfKeys.add("CreatedDate")
            arrOfKeys.add("CreatedBy")
            arrOfKeys.add("ModifiedDate")
            arrOfKeys.add("ModifiedBy")
            
            arrOfValues.add("")
            arrOfValues.add("\(strId)")
            arrOfValues.add("\(strEmpID)")
            arrOfValues.add("\(strCompanyId)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            arrOfValues.add(Global().getCurrentDate())
            arrOfValues.add("\(strUserName)")
            
            let dict_ToSend = NSDictionary(objects:arrOfValues as! [Any], forKeys:arrOfKeys as! [NSCopying]) as Dictionary
            arr.add(dict_ToSend)
            
        }

        var dict = [String:Any]()
        
        dict = [
                "WoWdoFindingFavoriteDetailExtSerDcs":arr
        ]
        
        return dict as NSDictionary
    }
    
    func callAPIMarkAsFavourite()
    {

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        let strEmpNo = "\(dictLoginData.value(forKey: "EmployeeNumber") ?? "")"
        
        let strCompanyId = "\(dictLoginData.value(forKeyPath: "Company.ServiceAutoCompanyId") ?? "")"
                
        //let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)"
        
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl")!)" + "/api/MobileToServiceAuto/AddUpdateNoteAndFindingFavoriteDetail?EmoloyeeId=\(strEmpID)&CompanyId=\(strCompanyId)&EmployeeNo=\(strEmpNo)&Type=\("Finding")"


        WebService.postRequestWithHeaders(dictJson: getPostData() , url: strURL, responseStringComing: "TimeLine") { [self] (response, status) in
            
            if(status == true)
            {
                
                let dictResponse = response.value(forKey: "data") as! NSDictionary
                
                let arrData = dictResponse.value(forKey: "WoWdoFindingFavoriteDetailExtSerDcs") as! NSArray
                let arrTemp = NSMutableArray()
                if arrData.count > 0
                {
                    for item in arrData
                    {
                        let dict = item as! NSDictionary
                        arrTemp.add("\(dict.value(forKey: "CodeMasterId") ?? "")")
                    }
                }
                var arrOfFavouriteCodes = NSArray()
                arrOfFavouriteCodes = arrTemp
                nsud.set(arrOfFavouriteCodes, forKey: "favouriteCode")
                nsud.set(false, forKey: "isSyncFavourite")
                nsud.synchronize()
                
                self.getFinalCodes()
                
            }

        }
        
    }
    
    //MARK:-
    //MARK:-Validation
    
    func validationForAddProblemIdentification() -> Bool {
        
        var isHash = false
        var isHash1 = false

        if (txtFilling_IssueCode.text?.count == 0){
            
            isHash = checkIfStringContains(strString: txtviewFinding.text!, strChar: "##", countt: 2)
            
        }
        
        if (txtFilling_Recommendation.text?.count == 0){
            
            isHash1 = checkIfStringContains(strString: txtviewRecommendation.text!, strChar: "##", countt: 2)
            
        }
        
        /*if (txtIssueCode.text?.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterIssuesCode, viewcontrol: self)
            
            return false
            
        }
        else */
        
        if(btnSubsectionCode.title(for: .normal) == strSelectString){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectSubsectionCode, viewcontrol: self)
            
            return false
            
        }
        else if isHash {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterFillingSubsection, viewcontrol: self)
            
            return false
            
        }
        else if (txtviewFinding.text?.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterFinding, viewcontrol: self)
            
            return false
            
        }
        else if(btnRecommendationCode.title(for: .normal) == strSelectString){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectRecommendationcode, viewcontrol: self)
            
            return false
            
        }
        else if isHash1 {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterFillingRecommendation, viewcontrol: self)
            
            return false
            
        }
        else if (txtviewRecommendation.text?.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterRecommendation, viewcontrol: self)
            
            return false
            
        }
        /*
        else if btnGeneralDescription.titleLabel?.text != strSelectString {
            
            if (txtViewGeneralDescription.text?.count == 0){
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterGeneralNotesDesc, viewcontrol: self)
                
                return false
                
            }
            
        }*/
        /*else if(btnGeneralDescription.title(for: .normal) == strSelectString){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_SelectGeneralNotes, viewcontrol: self)
            
            return false
            
        }
        else if (txtViewGeneralDescription.text?.count == 0){
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EnterGeneralNotesDesc, viewcontrol: self)
            
            return false
            
        }*/
        return true
    }
    
    //:---------------------------------------------------------------------------------------------------------
    // MARK: ----------------------------------------Functions Problem Image Details------------------------------------------
    //:---------------------------------------------------------------------------------------------------------
    
    func goToCaptionDescriptions(strWoImageId : String)  {
        
        var storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        var testController = storyboardIpad.instantiateViewController(withIdentifier: "EditCaptionDescriptionVC") as? EditCaptionDescriptionVC
        
        if DeviceType.IS_IPAD {
            
            storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
            testController = storyboardIpad.instantiateViewController(withIdentifier: "EditCaptionDescriptioniPadVC") as? EditCaptionDescriptionVC
            
        }
        
        testController?.strWoId = self.strWoId
        testController?.strModuleType = flowTypeWdoProblemId as NSString
        testController?.strImageId = strWoImageId as NSString
        testController?.strHeaderTitle = "Problem Image"
        testController?.strWoStatus = strWoStatus
        testController?.strWoLeadId = strWoLeadId
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func deleteImage(indexx : Int) {
        
        let alert = UIAlertController(title: AlertDelete, message: AlertDeleteMsg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            
            let arrayOfImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@", self.strWoId))
            
            if arrayOfImages.count > indexx {
                
                let objTempSales = arrayOfImages[indexx] as! NSManagedObject
                
                deleteDataFromDB(obj: objTempSales)
                
            }
            
            // Also Delete Target Images
            
            let arrayOfImagesTarget = getDataFromCoreDataBaseArray(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@", self.strWoLeadId))
            
            if arrayOfImagesTarget.count > indexx {
                
                let objTempSalesTarget = arrayOfImagesTarget[indexx] as! NSManagedObject
                
                deleteDataFromDB(obj: objTempSalesTarget)
                
            }
            
            self.fetchProblemImages()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    func fetchProblemImages() {
        
        if strToUpdate == "Yes" {
            
            if strProblemIdentificationIdToUpdate.count > 0 {
                
                arrOfProblemImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", strWoId , strProblemIdentificationIdToUpdate))

            }else{
                
                arrOfProblemImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdentificationIdToUpdate))

            }
            
        }else{
            
            arrOfProblemImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", strWoId , strMobileProblemIdentificationIdInitial))
            
        }
        
        if arrOfProblemImages.count > 0 {
            
            problemImgCollectionView.reloadData()
            
            /*
            let count = round(Double(1000*arrOfProblemImages.count))/3000
            
            let roundupp = count.rounded(.up)
        
            let yourWidth = self.view.bounds.width/1.0
            let yourHeight = Double(yourWidth)
            
            hghtConstCollectionView.constant = CGFloat(roundupp*yourHeight)
            */
            
            hghtConstCollectionView.constant = CGFloat(arrOfProblemImages.count*240)

            hghtConstScrollView.constant = hghtConstCollectionView.constant + 1200
            
        }else{
            
            hghtConstCollectionView.constant = 0

            hghtConstScrollView.constant = hghtConstCollectionView.constant + 1200

        }
        
    }
    
    
    func SaveImageToDb(image : UIImage) {
        
        let strHeaderTitle = "Problem"
        
        let strImageName = "\\Documents\\ProblemIdentificationImages\\Img"  + "\(strHeaderTitle)" + "\(strWoId)\(getUniqueValueForId())" + ".jpg"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("woImagePath")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("woImageId")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("problemIdentificationId")
        arrOfKeys.add("mobileId")
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let woImageId = "Mobile" + "\(strWoId)" + getUniqueValueForId()
        let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
        
        
        arrOfValues.add("") //createdBy
        arrOfValues.add("") //createdDate
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strImageName)
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(woImageId) // woImageId
        arrOfValues.add(modifiedDate ?? "")
        arrOfValues.add(strWoId)
        arrOfValues.add("") // imageCaption
        arrOfValues.add("") // imageDescription
        arrOfValues.add(latitude) // latitude
        arrOfValues.add(longitude) //  longitude
        
        var strLeadCommercialId = String()
        var strLeadCommercialMobileId = String()
        
        if strToUpdate == "Yes" {
            
            if strProblemIdentificationIdToUpdate.count > 0 {
                
                arrOfValues.add(strProblemIdentificationIdToUpdate)
                arrOfValues.add("")
                
                let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId , strProblemIdentificationIdToUpdate))
                
                if arrayOfTargetsLocal.count > 0 {
                    
//                    arrOfValues.add(strProblemIdentificationIdToUpdate)
//                    arrOfValues.add("")
                    
                    for k1 in 0 ..< arrayOfTargetsLocal.count {
                        
                        let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                        
                        let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                        let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"

                        if id1.count > 0 {
                            
                            strLeadCommercialId = id1

                        }else{
                            
                            strLeadCommercialMobileId = id2

                        }
                        
                        break
                        
                    }
                    
                }else{
                    
//                    arrOfValues.add("")
//                    arrOfValues.add(strMobileProblemIdentificationIdToUpdate)
                    
                    
                    let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId , strMobileProblemIdentificationIdToUpdate))
                    
                    if arrayOfTargetsLocal.count > 0 {
                        
//                        arrOfValues.add("")
//                        arrOfValues.add(strProblemIdentificationIdToUpdate)
                        
                        for k1 in 0 ..< arrayOfTargetsLocal.count {
                            
                            let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                            
                            let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                            let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                            
                            if id1.count > 0 {
                                
                                strLeadCommercialId = id1
                                
                            }else{
                                
                                strLeadCommercialMobileId = id2
                                
                            }
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
            }else{
                
                arrOfValues.add("")
                arrOfValues.add(strMobileProblemIdentificationIdToUpdate)
                
                
                let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId , strMobileProblemIdentificationIdToUpdate))
                
                if arrayOfTargetsLocal.count > 0 {
                    
//                    arrOfValues.add("")
//                    arrOfValues.add(strMobileProblemIdentificationIdToUpdate)
                    
                    for k1 in 0 ..< arrayOfTargetsLocal.count {
                        
                        let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                        
                        let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                        let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                        
                        if id1.count > 0 {
                            
                            strLeadCommercialId = id1
                            
                        }else{
                            
                            strLeadCommercialMobileId = id2
                            
                        }
                        
                        break
                        
                    }
                    
                }else{
                    
                    let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", strWoLeadId , strProblemIdentificationIdToUpdate))
                    
                    if arrayOfTargetsLocal.count > 0 {
                        
//                        arrOfValues.add(strMobileProblemIdentificationIdToUpdate)
//                        arrOfValues.add("")
                        
                        for k1 in 0 ..< arrayOfTargetsLocal.count {
                            
                            let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                            
                            let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                            let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                            
                            if id1.count > 0 {
                                
                                strLeadCommercialId = id1
                                
                            }else{
                                
                                strLeadCommercialMobileId = id2
                                
                            }
                            
                            break
                            
                        }
                        
                    }
                    
                }

            }
            
        }else{
            
            arrOfValues.add("")
            arrOfValues.add(strMobileProblemIdentificationIdInitial)
            
            strLeadCommercialMobileId = strMobileLeadCommercialIdIdInitial

        }
        
        saveDataInDB(strEntity: Entity_ProblemImageDetails, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        let imageResized = Global().resizeImageGloballl(image)
        
        saveImageDocumentDirectory(strFileName: strImageName, image: imageResized!)
        
        //Update Modify Date In Work Order DB
        updateServicePestModifyDate(strWoId: self.strWoId as String)
        
        fetchProblemImages()
        
        // Save Target Images To DB
        
        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\ProblemIdentificationImages\\", with: "")
        
        self.saveTargetImage(strLeadCommercialTargetId: strLeadCommercialId, strMobileTargetId: strLeadCommercialMobileId, strImagePath: strImageNameSales, imageResized: imageResized!, woImageId: woImageId)
        
        goToCaptionDescriptions(strWoImageId: woImageId)

    }
    
    //MARK: - -----------------------------------General Notes Based on TIP and Follow Up Inspection-----------------------------------
    
    func checkWoWdoGeneralNoteMapping(strMappingType : String , strIdToCheck : String) {
        
        //WoWdoGeneralNoteMappingExtSerDcs FollowUpMaster TIPMaster BuildingPermit
        // MasterId MappingType IsActive GeneralNoteId
        
        var ary = NSMutableArray()
        
        ary = getWDOMasters(strMasterNameInDefaults: "MasterServiceAutomation", strTypeOfMaster: "WoWdoGeneralNoteMappingExtSerDcs", strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String, strToCheckActive: "Yes", strToCheckBranch: "No", strBranchParameterName: "BranchId")
        
        if ary.count > 0 {
            
            for k in 0 ..< ary.count {
                
                let dictData = ary[k] as! NSDictionary
                
                if strMappingType == "BuildingPermit" {
                    
                    if strMappingType == "\(dictData.value(forKey: "MappingType") ?? "")" {
                        
                        let generalNoteIdTemp = "\(dictData.value(forKey: "GeneralNoteId") ?? "")"
                        
                        let arrOfGeneralNoteTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp))
                        
                        if arrOfGeneralNoteTemp.count == 0 {
                            
                            // General Note Id Does not exist so insert data of General Notes in DC
                            //saveGeneralNotesDB(strGeneralNotedId: generalNoteIdTemp, strFillIn: txtIssueCode.text!, strMappingType: strMappingType)
                            
                        }else{
                            
                            let sort = NSSortDescriptor(key: "issuesCode", ascending: true)
                            
                            var arrOfProblemIdentification = NSArray()
                            
                            arrOfProblemIdentification = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ &&  isBuildingPermit == YES &&  isActive == YES", strWoId)), sort: sort)


                            var strIssuCodeLocall = ""
                            
                            
                            if arrOfProblemIdentification.count > 0 {
                                
                                for k1 in 0 ..< arrOfProblemIdentification.count {
                                    
                                    let dictDataProblem = arrOfProblemIdentification[k1] as! NSManagedObject
                                    
                                    if strIssuCodeLocall.count > 0 {
                                        
                                        strIssuCodeLocall = strIssuCodeLocall + ", " + "\(dictDataProblem.value(forKey: "issuesCode") ?? "")"
                                        
                                    }else {
                                        
                                        strIssuCodeLocall = "\(dictDataProblem.value(forKey: "issuesCode") ?? "")"

                                        
                                    }
                                    
                                }
                                
                            }
                            
                            // fetch General Note via Id And Update
                            
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("fillIn")//fillIn
                            arrOfValues.add(strIssuCodeLocall)//fillIn

                            arrOfKeys.add("type")
                            arrOfValues.add(strMappingType)
                            
                            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, predicate: NSPredicate(format: "workOrderId == %@ &&  isActive == YES &&  generalNoteMasterId == %@", strWoId , generalNoteIdTemp), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess {
                            
                                // Updated Notes Fillins
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func saveGeneralNotesDB(strGeneralNotedId : String , strFillIn : String , strMappingType : String) {
        
        var array = NSMutableArray()
        var dictDataOfGeneralNotesTemp = NSDictionary()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ServiceJobDescriptionTemplate" , strBranchId: "")
        
        if array.count > 0 {
            
            for k in 0 ..< array.count {
                
                let dictData = array[k] as! NSDictionary
                
                if strGeneralNotedId == "\(dictData.value(forKey: "ServiceJobDescriptionId") ?? "")" {
                    
                    dictDataOfGeneralNotesTemp = dictData
                    
                }
                
            }
            
        }
        
        if dictDataOfGeneralNotesTemp.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workOrderId")
            arrOfKeys.add("isActive")
            
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(true)
            
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate") // ModifiedDate
            
            arrOfKeys.add("generalNoteMasterId")
            arrOfKeys.add("generalNoteDescription")
            arrOfKeys.add("title")
            arrOfKeys.add("wdoGeneralNoteId")
            arrOfKeys.add("fillIn")//fillIn
            arrOfKeys.add("type")
            arrOfKeys.add("isChanged")

            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescriptionId")!)")
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "ServiceJobDescription")!)")
            
            arrOfValues.add("\(dictDataOfGeneralNotesTemp.value(forKey: "Title")!)")
            
            arrOfValues.add(Global().getReferenceNumber())
            arrOfValues.add(strFillIn)
            arrOfValues.add(strMappingType)
            arrOfValues.add(false)

            // Saving Disclaimer In DB
            saveDataInDB(strEntity: Entity_WoWdoGeneralNotesExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            //Update Modify Date In Work Order DB
            updateServicePestModifyDate(strWoId: self.strWoId as String)
            
        }
        
    }

    
    //MARK: - -----------------------------------UIDocumentPickerViewController Delegate Method-----------------------------------
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let data = try Data(contentsOf: myURL)
            let uiImage: UIImage = UIImage(data: data)!
            
            self.SaveImageToDb(image: uiImage)
            
            
        } catch _ as NSError  {
            
        }catch {
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
         print("view was cancelled")
         //dismiss(animated: true, completion: nil)
     }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    
    /*public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        print("import result : \(myURL)")
        
        do {
            
            let data = try Data(contentsOf: myURL)
            let uiImage: UIImage = UIImage(data: data)!
            
            self.SaveImageToDb(image: uiImage)
            
            
        } catch _ as NSError  {
            
        }catch {
        }
        
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        //dismiss(animated: true, completion: nil)
    }
    */
    
}

extension NPMA_AddFindings_iPadVC:UITextFieldDelegate
{
    
}

extension NPMA_AddFindings_iPadVC:UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView) {
        
        isChanged = true
        
    }
    
}

//MARK: - -----------------------------------Selection Delegate -----------------------------------


extension NPMA_AddFindings_iPadVC : CustomTableView
{
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if tag == 46 {
            
            isSelectedSubsectionFindingFromDropDown = true
            
            dictDataSubsecutionCode = dictData
            btnSubsectionCode.setTitle("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)", for: .normal)

            txtviewFinding.attributedText = htmlAttributedString(strHtmlString: "\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
            
            strSubsectionCodeId = "\(dictDataSubsecutionCode.value(forKey: "CodeMasterId")!)"
            
            if !isRecommendationSelected {
                
                isSelectedRecommendationFindingFromDropDown = true
                
                dictDataRecommendationCode = dictData
                btnRecommendationCode.setTitle("\(dictDataRecommendationCode.value(forKey: "CodeName")!)", for: .normal)
                
                txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                
                strRecommendationCodeId = "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"
                
            }
            
        }else if(tag == 56){
            
            if dictData["Title"] as? String  == strSelectString {
                
                dictDataGeneralDescription = NSDictionary ()
                
            }else{
                
                dictDataGeneralDescription = dictData
                
            }
            
        }else {
            
            isSelectedRecommendationFindingFromDropDown = true
            
            dictDataRecommendationCode = dictData
            btnRecommendationCode.setTitle("\(dictDataRecommendationCode.value(forKey: "CodeName")!)", for: .normal)
            
            txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
            
            strRecommendationCodeId = "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"
            
        }
        
        
    }
    
}

//MARK: - -----------------------------------Selection Delegate -----------------------------------

extension NPMA_AddFindings_iPadVC : PopUpDelegate
{
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        if(tag == 107){
            
            // Subsection Code
            
            isSelectedSubsectionFindingFromDropDown = true
            
            dictDataSubsecutionCode = dictData
            btnSubsectionCode.setTitle("\(dictDataSubsecutionCode.value(forKey: "CodeName")!)", for: .normal)
            
            txtviewFinding.attributedText = htmlAttributedString(strHtmlString: "\(dictDataSubsecutionCode.value(forKey: "Finding")!)")
            
            strSubsectionCodeId = "\(dictDataSubsecutionCode.value(forKey: "CodeMasterId")!)"
            
            if !isRecommendationSelected {
                
                isSelectedRecommendationFindingFromDropDown = true
                
                dictDataRecommendationCode = dictData
                btnRecommendationCode.setTitle("\(dictDataRecommendationCode.value(forKey: "CodeName")!)", for: .normal)
                
                txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
                
                strRecommendationCodeId = "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"
                
            }
            
        }else{
            
            isSelectedRecommendationFindingFromDropDown = true
            
            // Recommendation code
            dictDataRecommendationCode = dictData
            btnRecommendationCode.setTitle("\(dictDataRecommendationCode.value(forKey: "CodeName")!)", for: .normal)
            
            txtviewRecommendation.attributedText = htmlAttributedString(strHtmlString: "\(dictDataRecommendationCode.value(forKey: "Recommendation")!)")
            
            strRecommendationCodeId = "\(dictDataRecommendationCode.value(forKey: "CodeMasterId")!)"
            
            isRecommendationSelected = true
            
        }
        
    }
}


class FindingImgCell: UICollectionViewCell {

    @IBOutlet weak var problemImgView: UIImageView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var lblDesc: UILabel!

}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension NPMA_AddFindings_iPadVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        self.SaveImageToDb(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)
        
    }
}


//MARK: - -----------------------------------Collection View Delegates -----------------------------------

extension NPMA_AddFindings_iPadVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrOfProblemImages.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {

        let yourWidth = collectionView.bounds.width/1.0
        let yourHeight = yourWidth
        return CGSize(width: yourWidth-10, height: 240)

    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FindingImgCell", for: indexPath as IndexPath) as! FindingImgCell
        
        let objTemp = arrOfProblemImages[indexPath.row] as! NSManagedObject
        
        
        cell.lblCaption.text = (objTemp.value(forKey: "imageCaption") as! String).count>0 ? "" + (objTemp.value(forKey: "imageCaption") as! String) : ""
        cell.lblDesc.text = (objTemp.value(forKey: "imageDescription") as! String).count>0 ? "" + (objTemp.value(forKey: "imageDescription") as! String) : ""
        
        let strImagePath = objTemp.value(forKey: "woImagePath") as! String
        
        if strImagePath.count == 0 {
            
            let image: UIImage = UIImage(named: "NoImage.jpg")!
            
            cell.problemImgView.image = image
            
        } else {
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
            
            if isImageExists!  {
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)

                cell.problemImgView.image = image
                
            }else {
                
                let defsLogindDetail = UserDefaults.standard
                
                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                
                var strURL = String()
                
                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                    
                    strURL = "\(value)"
                    
                }
                
                strURL = strURL + "\(strImagePath)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                cell.problemImgView.image = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)
                    
                    if data != nil && (data?.count)! > 0 {
                        
                        let image: UIImage = UIImage(data: data!)!
                        
                        DispatchQueue.main.async {
                            
                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                            
                            cell.problemImgView.image = image
                            
                        }}
                    
                }
                
            }
            
        }

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        if self.strWoStatus == "Completed" {
            
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "View Caption/Description", style: .default , handler:{ (UIAlertAction)in
                
                let objTemp = self.arrOfProblemImages[indexPath.row] as! NSManagedObject
                
                let woImageId = "\(objTemp.value(forKey: "woImageId")!)"
                
                self.goToCaptionDescriptions(strWoImageId: woImageId)
                
            }))
            
            alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                var imageView = UIImageView(image: image)
                
                let objTemp = self.arrOfProblemImages[indexPath.row] as! NSManagedObject
                
                let strImagePath = objTemp.value(forKey: "woImagePath")
                
                let image1: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image1 != nil  {
                    
                    imageView = UIImageView(image: image1)
                    
                }else {
                    
                    
                }
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = imageView.image!
                self.present(testController!, animated: false, completion: nil)
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: AlertDelete, style: .default , handler:{ (UIAlertAction)in
                
                self.deleteImage(indexx: indexPath.row)
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Edit Image", style: .default , handler:{ (UIAlertAction)in
                
                let objTemp = self.arrOfProblemImages[indexPath.row] as! NSManagedObject
                
                let strWoImagePath = objTemp.value(forKey: "woImagePath")
                nsud.set(strWoImagePath, forKey: "editImagePath")
                nsud.synchronize()
                
                if DeviceType.IS_IPAD {
                    // MechanicaliPad
                    
                    let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                    testController?.strModuleType = flowTypeWdoProblemId
                    testController?.strWdoLeadId = self.strWoLeadId as String
                    self.present(testController!, animated: false, completion: nil)
                    
                }else{
                    
                    let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
                    let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController
                    testController?.strModuleType = flowTypeWdoProblemId
                    testController?.strWdoLeadId = self.strWoLeadId as String
                    self.present(testController!, animated: false, completion: nil)
                    
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Edit Caption/Description", style: .default , handler:{ (UIAlertAction)in
                
                let objTemp = self.arrOfProblemImages[indexPath.row] as! NSManagedObject
                
                var strWoImageId = ""
                strWoImageId = objTemp.value(forKey: "woImageId") as! String
                
                self.goToCaptionDescriptions(strWoImageId: strWoImageId)
                
            }))
            
            alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                var imageView = UIImageView(image: image)
                
                let objTemp = self.arrOfProblemImages[indexPath.row] as! NSManagedObject
                
                let strImagePath = objTemp.value(forKey: "woImagePath")
                
                let image1: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath as! String)
                
                if image1 != nil  {
                    
                    imageView = UIImageView(image: image1)
                    
                }else {
                    
                    
                }
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = imageView.image!
                self.present(testController!, animated: false, completion: nil)
                
            }))
            
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
            })
            
        }
        
    }
    
    
}
