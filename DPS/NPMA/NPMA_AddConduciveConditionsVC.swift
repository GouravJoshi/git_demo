//
//  NPMA_AddConduciveConditionsVC.swift
//  DPS
//  2021
//  Created by NavinPatidar on 10/14/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  Saavan 20201
//  Saavan 2021 Test 


import UIKit

protocol NPMA_AddConduciveConditionsProtocol : class
{
    
    func getDataFromAddConduciveConditions(dictData : NSDictionary ,tag : Int)
    
}



class NPMA_AddConduciveConditionsVC: UIViewController {
   
    var imagePicker = UIImagePickerController()
    var imageUpload = UIImage()
    @objc var objWorkorderDetail = NSManagedObject() , dictData = NSManagedObject()
    @objc  var strWoId = NSString () , strCorrectiveActionIds = "" , strViewFor = ""
    weak var delegate: NPMA_AddConduciveConditionsProtocol?

    
    var aryselectedCorrectiveAction =  NSMutableArray()
    var str_sAConduciveConditionId = ""
    
    //MARK:
    //MARK: -------------IBOutlet-----------
    @IBOutlet weak var txt_AddCondition_ParentArea: UITextField!
    @IBOutlet weak var txt_AddCondition_Category: UITextField!
    @IBOutlet weak var txt_AddCondition_ConduciveCondition: UITextField!
    @IBOutlet weak var txt_AddCondition_Detail: UITextView!
    @IBOutlet weak var txt_AddCondition_Status: UITextField!
    @IBOutlet weak var txt_AddCondition_Responsibility: UITextField!
    @IBOutlet weak var txt_AddCondition_Priority: UITextField!
    @IBOutlet weak var txt_AddCondition_CorrectiveAction: UITextField!
    @IBOutlet weak var txt_AddCondition_Title: UITextField!

    @IBOutlet weak var lblUploadImageName: UILabel!
    @IBOutlet weak var txt_AddCondition_Comments: UITextView!
    
    //MARK:
    //MARK: -------------LifeCycle-----------
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_AddCondition_Comments.layer.cornerRadius = 8.0
        txt_AddCondition_Comments.layer.borderWidth = 1.0
        txt_AddCondition_Comments.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        txt_AddCondition_Detail.layer.cornerRadius = 8.0
        txt_AddCondition_Detail.layer.borderWidth = 1.0
        txt_AddCondition_Detail.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        self.lblUploadImageName.text = ""

        txt_AddCondition_Status.text = "Open"
        txt_AddCondition_Status.tag = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(strViewFor == "EDIT"){
            self.UIInitializationForEdit()
        }
    }
    
    func UIInitializationForEdit() {
        txt_AddCondition_ParentArea.text = "\(dictData.value(forKey: "parentAreaName")!)"
        
        txt_AddCondition_ParentArea.tag = Int("\(dictData.value(forKey: "serviceAddressAreaId")!)" == "" ? "0" : "\(dictData.value(forKey: "serviceAddressAreaId")!)")!
        
        txt_AddCondition_Category.tag = Int("\(dictData.value(forKey: "conditionGroupId")!)" == "" ? "0" : "\(dictData.value(forKey: "conditionGroupId")!)")!
     
        let arrayCategory = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayCategory.count != 0){
            let  array = arrayCategory.filter { (activity) -> Bool in
                return "\((activity as! NSDictionary).value(forKey: "ConditionGroupId")!)".contains("\(txt_AddCondition_Category.tag)")}
            
            if(array.count == 0){
              txt_AddCondition_Category.text = ""
            } else{
                let dict = array[0]as! NSDictionary
                txt_AddCondition_Category.text = "\(dict.value(forKey: "ConditionGroupName")!)"
            }
        }

        //txt_AddCondition_ConduciveCondition.tag = Int("\(dictData.value(forKey: "conduciveConditionId")!)")!
        
        txt_AddCondition_ConduciveCondition.tag = Int("\(dictData.value(forKey: "conduciveConditionId")!)" == "" ? "0" : "\(dictData.value(forKey: "conduciveConditionId")!)")!

        let arrayconduciveCondition = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayconduciveCondition.count != 0){
            let  array = arrayconduciveCondition.filter { (activity) -> Bool in
                return "\((activity as! NSDictionary).value(forKey: "ConditionId")!)".contains("\(txt_AddCondition_ConduciveCondition.tag)")}
            
            
            if(array.count == 0){
                txt_AddCondition_ConduciveCondition.text = ""
            } else{
                let dict = array[0]as! NSDictionary
                txt_AddCondition_ConduciveCondition.text = "\(dict.value(forKey: "ConditionName")!)"
            }
        }
        
        txt_AddCondition_Detail.text = "\(dictData.value(forKey: "conduciveConditiondDetail")!)"

        txt_AddCondition_Status.text = "\(dictData.value(forKey: "conditionStatus")!)"
        
        if "\(dictData.value(forKey: "conditionStatus")!)" == "1" {
            
            txt_AddCondition_Status.text = "Open"
            txt_AddCondition_Status.tag = 1

        } else if "\(dictData.value(forKey: "conditionStatus")!)" == "2" {
            
            txt_AddCondition_Status.text = "Void"
            txt_AddCondition_Status.tag = 2

        } else if "\(dictData.value(forKey: "conditionStatus")!)" == "3" {
            
            txt_AddCondition_Status.text = "Resolve"
            txt_AddCondition_Status.tag = 3

        } else {
            
            txt_AddCondition_Status.text = "Open"
            txt_AddCondition_Status.tag = 1

        }

        txt_AddCondition_Responsibility.tag = Int("\(dictData.value(forKey: "responsibilityId")!)" == "" ? "0" : "\(dictData.value(forKey: "responsibilityId")!)")!

        txt_AddCondition_Responsibility.text = "\(dictData.value(forKey: "responsibilityName")!)"

        txt_AddCondition_Priority.tag = Int("\(dictData.value(forKey: "priorityId")!)" == "" ? "0" : "\(dictData.value(forKey: "priorityId")!)")!
        if(txt_AddCondition_Priority.tag == 1){
            txt_AddCondition_Priority.text = "Low"
        }else if(txt_AddCondition_Priority.tag == 2){
            txt_AddCondition_Priority.text = "Medium"
        }
        else if(txt_AddCondition_Priority.tag == 3){
            txt_AddCondition_Priority.text = "High"
        }
        
        
        
        
        let arrayCondition_Priority = getDataFromServiceAutoMasters(strTypeOfMaster: "Prioritys" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayconduciveCondition.count != 0){
            let  array = arrayCondition_Priority.filter { (activity) -> Bool in
                return "\((activity as! NSDictionary).value(forKey: "Value")!)".contains("\(txt_AddCondition_Priority.tag)")}
            
            if(array.count == 0){
                txt_AddCondition_Priority.text = ""
            } else{
                let dict = array[0]as! NSDictionary
                txt_AddCondition_Priority.text = "\(dict.value(forKey: "Text")!)"
            }
        }
        
        
        self.strCorrectiveActionIds = "\(dictData.value(forKey: "strCorrectiveActionIds")!)"
      
        aryselectedCorrectiveAction = NSMutableArray()
        var arrayCorrectiveActionMaster = NSMutableArray()
        
        arrayCorrectiveActionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        let arySelected = strCorrectiveActionIds.split(separator: ",") as NSArray
        var strName = ""
        self.aryselectedCorrectiveAction = NSMutableArray()
        for item in arrayCorrectiveActionMaster {
            let strid = "\((item as AnyObject).value(forKey: "CorrectiveActionId")!)"
            if(arySelected.contains(strid)){
                strName = strName + "\((item as AnyObject).value(forKey: "CorrectiveActionName")!),"
                self.aryselectedCorrectiveAction.add(item)
            }
            
        }
        
        if(strName.count != 0){
            strName = String(strName.dropLast())
        }
        txt_AddCondition_CorrectiveAction.text = strName

        //txt_AddCondition_Title.text = "\(dictData.value(forKey: "sAConduciveConditionName")!)"

        var woNPMInspectionConduciveConditionId = "\(dictData.value(forKey: "sAConduciveConditionId")!)"

        if woNPMInspectionConduciveConditionId.count == 0 {
            
            woNPMInspectionConduciveConditionId = "\(dictData.value(forKey: "mobileSAConduciveConditionId")!)"
            
        }
        
       // get Data for comment
        let arrOtherDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionServiceConditionComments, predicate: (NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId ,woNPMInspectionConduciveConditionId )))
        if(arrOtherDetail.count != 0){
            let dictGeneral = arrOtherDetail.object(at: 0)as! NSManagedObject
            print(dictGeneral)
            txt_AddCondition_Comments.text = "\(dictGeneral.value(forKey: "commentDetail")!)"

        }
        // get Data for Document
         let arrOtherDetailDocument = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionServiceConditionDocuments, predicate: (NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId ,woNPMInspectionConduciveConditionId )))
         if(arrOtherDetailDocument.count != 0){
            
             let dictGeneral = arrOtherDetailDocument.object(at: 0)as! NSManagedObject
             print(dictGeneral)
            txt_AddCondition_Title.text = "\(dictGeneral.value(forKey: "documnetTitle")!)"

            var strURL = ""
            
            let defsLogindDetail = UserDefaults.standard
            
            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
            
            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceAutoModule.ServiceUrl") {
                
                strURL = "\(value)"
                
            }
            
            let strDocPathLocal = "\(dictGeneral.value(forKey: "documentPath")!)"
            let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
            self.lblUploadImageName.text = strImagePath

            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath!)

            if image != nil && strImagePath?.count != 0 && isImageExists! {
                let strImagePath = Global().strDocName(fromPath: strDocPathLocal)
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath!)
                self.imageUpload = image!
                // kch nhi krna
                
            }else {
                
                strURL = strURL + "\(strDocPathLocal)"
                
                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                
                let image: UIImage = UIImage(named: "NoImage.jpg")!
                
                self.imageUpload = image
                
                DispatchQueue.global(qos: .background).async {
                    
                    let url = URL(string:strURL)
                    if(url != nil){
                        let data = try? Data(contentsOf: url!)
                        if data != nil && (data?.count)! > 0 {
                            let image: UIImage = UIImage(data: data!)!
                            DispatchQueue.main.async {
                                saveImageDocumentDirectory(strFileName: strImagePath! , image: image)
                                self.imageUpload = image
                            }
                        }
                    }
                    
                }
                
            }

         }
    }
    
    //MARK:
    //MARK: -------------IBAction-----------
    @IBAction func action_Back(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func action_Add(_ sender: UIButton) {
        if(txt_AddCondition_ParentArea.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "ParentArea is required!", viewcontrol: self)
        }
        else if(txt_AddCondition_Category.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Category is required!", viewcontrol: self)

        }
        else if(txt_AddCondition_ConduciveCondition.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Conducive Condition is required!", viewcontrol: self)

        }
        else if(txt_AddCondition_Status.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Status is required!", viewcontrol: self)

        }
        else if(txt_AddCondition_Responsibility.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Responsibility is required!", viewcontrol: self)

        }
        else if(txt_AddCondition_Priority.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Priority is required!", viewcontrol: self)

        }
        else if(txt_AddCondition_CorrectiveAction.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "CorrectiveAction is required!", viewcontrol: self)

        }else{
            self.strViewFor == "EDIT" ? self.SaveOtherDetailConduciveConditions(statusForAdd_Edit_Delete: 2, obj: dictData) : self.SaveOtherDetailConduciveConditions(statusForAdd_Edit_Delete: 1, obj: dictData)
            
         
        }
    
    }
    @IBAction func action_ChooseFile(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
            alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = self.imageUpload
                self.present(testController!, animated: false, completion: nil)
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(alert, animated: true, completion: { })
    }
    
    @IBAction func action_ParentArea(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryAreaMaster = NSMutableArray()
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dict = nsud.value(forKey: "MasterServiceAutomation")as! NSDictionary
            if (dict.value(forKey: "AreaMaster") != nil){
                aryAreaMaster = NSMutableArray()
                aryAreaMaster = (dict.value(forKey: "AreaMaster") as! NSArray).mutableCopy() as! NSMutableArray
            }
        }
        
        if(aryAreaMaster.count != 0){
            sender.tag = 503
            openTableViewPopUp(tag: sender.tag, ary: (aryAreaMaster as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_Category(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrayCategory = NSMutableArray()
        arrayCategory = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionGroupMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayCategory.count != 0){
            sender.tag = 504
            openTableViewPopUp(tag: sender.tag, ary: (arrayCategory as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_ConduciveCondition(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrayConditionMaster = NSMutableArray()
        
        arrayConditionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
    
        if(txt_AddCondition_Category.tag != 0){
            let  array = arrayConditionMaster.filter { (activity) -> Bool in
                return "\((activity as! NSDictionary).value(forKey: "ConditionGroupId")!)".contains("\(txt_AddCondition_Category.tag)")}
            arrayConditionMaster = NSMutableArray()
            arrayConditionMaster = (array as NSArray).mutableCopy()as! NSMutableArray
        }
        
        if(arrayConditionMaster.count != 0){
            sender.tag = 505
            openTableViewPopUp(tag: sender.tag, ary: (arrayConditionMaster as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_Status(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = NSMutableArray()
        var dict = NSMutableDictionary()
        dict.setValue("Open", forKey: "name")
        dict.setValue("1", forKey: "id")
        aryTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Void", forKey: "name")
        dict.setValue("2", forKey: "id")
        aryTemp.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Resolve", forKey: "name")
        dict.setValue("3", forKey: "id")
        aryTemp.add(dict)
        
        if(aryTemp.count != 0){
            sender.tag = 506
            openTableViewPopUp(tag: sender.tag, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_Responsibility(_ sender: UIButton) {
        self.view.endEditing(true)
       
        var arrayRespMaster = NSMutableArray()
        arrayRespMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "responsibilityMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayRespMaster.count != 0){
            sender.tag = 507
            openTableViewPopUp(tag: sender.tag, ary: (arrayRespMaster as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_Priority(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrayPriorityMaster = NSMutableArray()
        
        arrayPriorityMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "Prioritys" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        if(arrayPriorityMaster.count != 0){
            sender.tag = 508
            openTableViewPopUp(tag: sender.tag, ary: (arrayPriorityMaster as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_CorrectiveAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrayCorrectiveActionMaster = NSMutableArray()
        
        arrayCorrectiveActionMaster = getDataFromServiceAutoMasters(strTypeOfMaster: "correctiveActionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if(arrayCorrectiveActionMaster.count != 0){
            sender.tag = 509
            openTableViewPopUp(tag: sender.tag, ary: (arrayCorrectiveActionMaster as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: aryselectedCorrectiveAction)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }

    //MARK:
    //MARK: -------------Core Data Function-------------

    
    func SaveOtherDetailConduciveConditions(statusForAdd_Edit_Delete : Int , obj : NSManagedObject) {
   
        //1 for Add , 2 for Edit , 3 For Delete
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        if(statusForAdd_Edit_Delete == 1){
            arrOfKeys.add("workOrderId")
            arrOfValues.add(self.strWoId)
           
            arrOfKeys.add("companyKey")
            arrOfValues.add(Global().getCompanyKey())
           
            arrOfKeys.add("createdBy")
            arrOfValues.add(Global().getEmployeeId())
           
            arrOfKeys.add("createdDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
           
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
           
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
           
            arrOfKeys.add("strCreatedDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
           
            
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("strModifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("isActive")
            arrOfValues.add(true)
           
            arrOfKeys.add("associationId")
            arrOfValues.add("")
           
            arrOfKeys.add("parentAreaName")
            arrOfValues.add(txt_AddCondition_ParentArea.text!)
         
            arrOfKeys.add("serviceAddressAreaId")
            arrOfValues.add("\(txt_AddCondition_ParentArea.tag)")
            
            arrOfKeys.add("conditionGroupId")
            arrOfValues.add("\(txt_AddCondition_Category.tag)")
           
            arrOfKeys.add("conduciveConditionId")
            arrOfValues.add("\(txt_AddCondition_ConduciveCondition.tag)")
            
            arrOfKeys.add("conduciveConditiondDetail")
            arrOfValues.add(txt_AddCondition_Detail.text!)
            
            arrOfKeys.add("conditionStatus")
            arrOfValues.add("\(txt_AddCondition_Status.text!)")
            
            arrOfKeys.add("responsibilityId")
            arrOfValues.add("\(txt_AddCondition_Responsibility.tag)")
            
            arrOfKeys.add("responsibilityName")
            arrOfValues.add(txt_AddCondition_Responsibility.text!)
            
            arrOfKeys.add("priorityId")
            arrOfValues.add("\(txt_AddCondition_Priority.tag)")
            
            arrOfKeys.add("correctiveActionId")
            arrOfValues.add("")
           
            arrOfKeys.add("strCorrectiveActionIds")
            arrOfValues.add(strCorrectiveActionIds)
          
            arrOfKeys.add("correctiveActionName")
            arrOfValues.add(txt_AddCondition_CorrectiveAction.text!)
            
            arrOfKeys.add("sAConduciveConditionName")
            arrOfValues.add(txt_AddCondition_ConduciveCondition.text!)
          
            arrOfKeys.add("serviceAddressId")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
           
            str_sAConduciveConditionId = Global().getReferenceNumber()
            arrOfKeys.add("mobileSAConduciveConditionId")
            arrOfValues.add(str_sAConduciveConditionId)
           
            arrOfKeys.add("mobileServiceAddressAreaId")
            arrOfValues.add("")
           
            arrOfKeys.add("sAConduciveConditionId")
            arrOfValues.add("")
            
            saveDataInDB(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            updateServicePestModifyDate(strWoId: self.strWoId as String)


        }
        else if(statusForAdd_Edit_Delete == 2){
           
            arrOfKeys.add("modifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("strModifyDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("parentAreaName")
            arrOfValues.add(txt_AddCondition_ParentArea.text!)
         
            arrOfKeys.add("serviceAddressAreaId")
            arrOfValues.add("\(txt_AddCondition_ParentArea.tag)")
            
            arrOfKeys.add("conditionGroupId")
            arrOfValues.add("\(txt_AddCondition_Category.tag)")
           
            arrOfKeys.add("conduciveConditionId")
            arrOfValues.add("\(txt_AddCondition_ConduciveCondition.tag)")
            
            arrOfKeys.add("conduciveConditiondDetail")
            arrOfValues.add(txt_AddCondition_Detail.text!)
            
            arrOfKeys.add("conditionStatus")
            arrOfValues.add("\(txt_AddCondition_Status.text!)")
            
            arrOfKeys.add("responsibilityId")
            arrOfValues.add("\(txt_AddCondition_Responsibility.tag)")
            
            arrOfKeys.add("responsibilityName")
            arrOfValues.add(txt_AddCondition_Responsibility.text!)
            
            arrOfKeys.add("priorityId")
            arrOfValues.add("\(txt_AddCondition_Priority.tag)")
            
            arrOfKeys.add("correctiveActionId")
            arrOfValues.add("")
           
            arrOfKeys.add("strCorrectiveActionIds")
            arrOfValues.add(strCorrectiveActionIds)
          
            arrOfKeys.add("correctiveActionName")
            arrOfValues.add(txt_AddCondition_CorrectiveAction.text!)
            
            arrOfKeys.add("sAConduciveConditionName")
            arrOfValues.add(txt_AddCondition_ConduciveCondition.text!)
          
            arrOfKeys.add("serviceAddressId")
            arrOfValues.add("\(objWorkorderDetail.value(forKey: "serviceAddressId") ?? "")")
           
            arrOfKeys.add("mobileServiceAddressAreaId")
            arrOfValues.add("")
            
            var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"
            var woNPMInspectionConduciveConditionIdName = "sAConduciveConditionId"

            if woNPMInspectionConduciveConditionId.count == 0 {
                
                woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                woNPMInspectionConduciveConditionIdName = "mobileSAConduciveConditionId"
                str_sAConduciveConditionId = woNPMInspectionConduciveConditionId
                
            }
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, predicate: NSPredicate(format: "workOrderId == %@ && \(woNPMInspectionConduciveConditionIdName) == %@", strWoId,woNPMInspectionConduciveConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
        }
        else if(statusForAdd_Edit_Delete == 3){
            
            var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"
            var woNPMInspectionConduciveConditionIdName = "sAConduciveConditionId"

            if woNPMInspectionConduciveConditionId.count == 0 {
                
                woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                woNPMInspectionConduciveConditionIdName = "mobileSAConduciveConditionId"
                
            }
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, predicate: NSPredicate(format: "workOrderId == %@ && \(woNPMInspectionConduciveConditionIdName) == %@", strWoId,woNPMInspectionConduciveConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
        }
      
        
        //For save Comment Data
        //1 for Add , 2 for Edit
         arrOfKeys = NSMutableArray()
         arrOfValues = NSMutableArray()
        
        if(statusForAdd_Edit_Delete == 1){
            
            saveComment(strMobileSAConditionId: str_sAConduciveConditionId)
            
        }
        else if(statusForAdd_Edit_Delete == 2){
           
            // Check if comment already exist or not
            
            var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"

            if woNPMInspectionConduciveConditionId.count == 0 {
                
                woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                
            }

            let arrOtherDetailComments = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionServiceConditionComments, predicate: (NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId ,woNPMInspectionConduciveConditionId )))

            if arrOtherDetailComments.count == 0 {
                
                saveComment(strMobileSAConditionId: woNPMInspectionConduciveConditionId)
                
            } else {
                
                arrOfKeys.add("modifyDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
              
                arrOfKeys.add("commentDetail")
                arrOfValues.add("\(txt_AddCondition_Comments.text!)")
                
                var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"

                if woNPMInspectionConduciveConditionId.count == 0 {
                    
                    woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                    
                }
                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionServiceConditionComments, predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId,woNPMInspectionConduciveConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess {
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                }
                
            }

        }
 
        //For save Document Data
        //1 for Add , 2 for Edit
        if(self.lblUploadImageName.text != ""){
            arrOfKeys = NSMutableArray()
            arrOfValues = NSMutableArray()
           if(statusForAdd_Edit_Delete == 1){
               
            //
            saveDocument(strMobileSAConditionId: str_sAConduciveConditionId)

           }
           else if(statusForAdd_Edit_Delete == 2){
            
            // Check if document already exist or not
            
            var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"

            if woNPMInspectionConduciveConditionId.count == 0 {
                
                woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                
            }
            
            let arrOtherDetailDocument = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionServiceConditionDocuments, predicate: (NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId ,woNPMInspectionConduciveConditionId )))

            if arrOtherDetailDocument.count == 0 {
                
                saveDocument(strMobileSAConditionId: woNPMInspectionConduciveConditionId)
                
            } else {
                
                arrOfKeys.add("modifyDate")
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
                
                let docPath = "\\Documents\\ConditionImage\\" + self.lblUploadImageName.text!
                let uiImage: UIImage = self.imageUpload
                let imageResized = Global().resizeImageGloballl(uiImage)
                saveImageDocumentDirectory(strFileName: self.lblUploadImageName.text!, image: imageResized!)
                
                
                arrOfKeys.add("documentPath")
                arrOfValues.add(docPath)
                
                arrOfKeys.add("isToUpload")
                arrOfValues.add("Yes")
                                
                var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"

                if woNPMInspectionConduciveConditionId.count == 0 {
                    
                    woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
                    
                }
                
                let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionServiceConditionDocuments, predicate: NSPredicate(format: "workOrderId == %@ && mobileSAConditionId == %@", strWoId,woNPMInspectionConduciveConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                if isSuccess {
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                }
                
            }
           
           }
           
        }
         
        

        delegate?.getDataFromAddConduciveConditions(dictData: NSDictionary(), tag: 0)
        
        self.dismiss(animated: false, completion: nil)
        
    }
  
    func saveComment(strMobileSAConditionId : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("workOrderId")
        arrOfValues.add(self.strWoId)
       
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
       
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
       
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
       
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
       
        arrOfKeys.add("modifyDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
      
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
       
        arrOfKeys.add("sAConditionCommentId")
        arrOfValues.add(Global().getReferenceNumber())
        
        arrOfKeys.add("mobileSAConditionId")
        arrOfValues.add(strMobileSAConditionId)
        
        arrOfKeys.add("commentDetail")
        arrOfValues.add("\(txt_AddCondition_Comments.text!)")
        saveDataInDB(strEntity: Entity_WoNPMAInspectionServiceConditionComments, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        updateServicePestModifyDate(strWoId: self.strWoId as String)


    }
    
    func saveDocument(strMobileSAConditionId : String) {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("workOrderId")
        arrOfValues.add(self.strWoId)
       
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
       
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
       
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
       
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
       
        arrOfKeys.add("modifyDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
      
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
       
        let docPath = "\\Documents\\ConditionImage\\" + self.lblUploadImageName.text!

        arrOfKeys.add("documentPath")
        arrOfValues.add(docPath)
        
        arrOfKeys.add("documnetTitle")
        arrOfValues.add(txt_AddCondition_Title.text!)
        
        arrOfKeys.add("sAConditionDocumentId")
        arrOfValues.add(Global().getReferenceNumber())
        
        arrOfKeys.add("mobileSAConditionId")
        arrOfValues.add(strMobileSAConditionId)
        
        arrOfKeys.add("documentType")
        arrOfValues.add("image")
        
        arrOfKeys.add("isToUpload")
        arrOfValues.add("Yes")
        
        let uiImage: UIImage = self.imageUpload
        let imageResized = Global().resizeImageGloballl(uiImage)
       
        if(imageResized != nil){
            saveImageDocumentDirectory(strFileName: self.lblUploadImageName.text!, image: imageResized!)
        }
        
        saveDataInDB(strEntity: Entity_WoNPMAInspectionServiceConditionDocuments, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
        updateServicePestModifyDate(strWoId: self.strWoId as String)


    }
 
    
    //MARK:
    //MARK: -------------Other Function-------------
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
       
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension NPMA_AddConduciveConditionsVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in })
        let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.imageUpload = img!
        self.lblUploadImageName.text = "imgC" + "\(getUniqueValueForId())" + ".png"
        
    }
    
}
// MARK: -
// MARK: ------- Selection CustomTableView---------
extension NPMA_AddConduciveConditionsVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 503)
        {
            if dictData.count == 0 {
                txt_AddCondition_ParentArea.text = ""
                txt_AddCondition_ParentArea.tag = 0
            }else{
                txt_AddCondition_ParentArea.text = "\(dictData.value(forKey: "AreaName")!)"
                txt_AddCondition_ParentArea.tag = Int("\(dictData.value(forKey: "AreaId")!)" == "" ? "0" : "\(dictData.value(forKey: "AreaId")!)")!
                
                
            }
        }
    
        else if(tag == 504)
        {
            txt_AddCondition_ConduciveCondition.text = ""
            txt_AddCondition_ConduciveCondition.tag = 0
            
            if dictData.count == 0 {
                txt_AddCondition_Category.text = ""
                txt_AddCondition_Category.tag = 0
            }else{
                txt_AddCondition_Category.text = "\(dictData.value(forKey: "ConditionGroupName")!)"
                txt_AddCondition_Category.tag = Int("\(dictData.value(forKey: "ConditionGroupId")!)" == "" ? "0" : "\(dictData.value(forKey: "ConditionGroupId")!)")!
            }
        }
        else if(tag == 505)
        {
            if dictData.count == 0 {
                txt_AddCondition_ConduciveCondition.text = ""
                txt_AddCondition_ConduciveCondition.tag = 0
            }else{
                
                txt_AddCondition_ConduciveCondition.text = "\(dictData.value(forKey: "ConditionName")!)"
                txt_AddCondition_ConduciveCondition.tag = Int("\(dictData.value(forKey: "ConditionId")!)" == "" ? "0" : "\(dictData.value(forKey: "ConditionId")!)")!
            }
        }
        else if(tag == 506)
        {
            if dictData.count == 0 {
                txt_AddCondition_Status.text = ""
                txt_AddCondition_Status.tag = 0
            }else{
                txt_AddCondition_Status.text = "\(dictData.value(forKey: "name")!)"
                txt_AddCondition_Status.tag = Int("\(dictData.value(forKey: "id")!)" == "" ? "0" : "\(dictData.value(forKey: "id")!)")!
            }
        }
        else if(tag == 507)
        {
            if dictData.count == 0 {
                txt_AddCondition_Responsibility.text = ""
                txt_AddCondition_Responsibility.tag = 0
            }else{
                txt_AddCondition_Responsibility.text = "\(dictData.value(forKey: "ResponsibilityName")!)"
                txt_AddCondition_Responsibility.tag = Int("\(dictData.value(forKey: "ResponsibilityId")!)" == "" ? "0" : "\(dictData.value(forKey: "ResponsibilityId")!)")!
            }
        }
        else if(tag == 508)
        {
            if dictData.count == 0 {
                txt_AddCondition_Priority.text = ""
                txt_AddCondition_Priority.tag = 0
            }else{
                txt_AddCondition_Priority.text = "\(dictData.value(forKey: "Text")!)"
                txt_AddCondition_Priority.tag = Int("\(dictData.value(forKey: "Value")!)" == "" ? "0" : "\(dictData.value(forKey: "Value")!)")!
            }
        }
        else if(tag == 509)
        {
            if dictData.count == 0 {
                txt_AddCondition_CorrectiveAction.text = ""
                txt_AddCondition_CorrectiveAction.tag = 0
            }else{
                self.aryselectedCorrectiveAction = NSMutableArray()
                self.aryselectedCorrectiveAction = (dictData.value(forKey: "multi")as! NSArray).mutableCopy()as! NSMutableArray
                var strName = ""
                self.strCorrectiveActionIds = ""
                for item in self.aryselectedCorrectiveAction {
                    strName = strName + "\((item as AnyObject).value(forKey: "CorrectiveActionName")!),"
                    self.strCorrectiveActionIds = self.strCorrectiveActionIds + "\((item as AnyObject).value(forKey: "CorrectiveActionId")!),"
                }
                if(strName.count != 0){
                    strName = String(strName.dropLast())
                }
                if(self.strCorrectiveActionIds.count != 0){
                    self.strCorrectiveActionIds = String(self.strCorrectiveActionIds.dropLast())
                }
                txt_AddCondition_CorrectiveAction.text = strName
            }
        }
        else if(tag == 510)
        {
            
            
        }
    }
    
}
