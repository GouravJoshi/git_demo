//
//  WoNPMAServicePricingExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 09/11/20.
//
//

#import "WoNPMAServicePricingExtSerDcs+CoreDataProperties.h"

@implementation WoNPMAServicePricingExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAServicePricingExtSerDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAServicePricingExtSerDcs"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic isActive;
@dynamic isDiscount;
@dynamic isRenewal;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic serviceId;
@dynamic serviceName;
@dynamic servicePrice;
@dynamic serviceSysName;
@dynamic userName;
@dynamic woNPMAServicePricingId;
@dynamic workorderId;

@end
