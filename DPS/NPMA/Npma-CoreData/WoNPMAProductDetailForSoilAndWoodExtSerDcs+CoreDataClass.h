//
//  WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAProductDetailForSoilAndWoodExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataProperties.h"
