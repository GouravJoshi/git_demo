//
//  WoNPMAInspectionGeneralDescriptionExtSerDc+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionGeneralDescriptionExtSerDc+CoreDataProperties.h"

@implementation WoNPMAInspectionGeneralDescriptionExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionGeneralDescriptionExtSerDc *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAInspectionGeneralDescriptionExtSerDc"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic workorderId;
@dynamic woNPMAInspectionId;
@dynamic blockEquals;
@dynamic approxSqFt;
@dynamic linearFt;
@dynamic numberOfStories;
@dynamic originalTreatmentDate;
@dynamic inspectedBy;
@dynamic inspectedDate;
@dynamic structureType;
@dynamic workCompletedBy;
@dynamic workCompletedDate;
@dynamic slab;
@dynamic isPerimeter;
@dynamic isHP;
@dynamic crawlDoorHeight;
@dynamic isInside;
@dynamic isOutside;
@dynamic isRemoveWoodDebris;
@dynamic brickVeneer;
@dynamic hollowBlack;
@dynamic stoneVeneer;
@dynamic porches;
@dynamic garege;
@dynamic moistureMeterReading;
@dynamic isDry;
@dynamic isDamp;
@dynamic isWet;
@dynamic isInstallMoistureBarrier;
@dynamic isDryerVentedOutside;
@dynamic isVentsNecessary;
@dynamic ventsNumber;
@dynamic type;
@dynamic color;
@dynamic numberOfSectionsInstalled;
@dynamic typeOfSystem;
@dynamic extraMaterialToCompleteJob;
@dynamic isControlOfExistingInfestation;
@dynamic isPreventionOfInfestation;
@dynamic constructionTypeSysName;
@dynamic foundationTypeSysName;
@dynamic userName;
@dynamic crawlSpaceHeight;
@dynamic isSlab;
@dynamic isBrickVeneer;
@dynamic isHollowBlack;
@dynamic isStoneVeneer;
@dynamic isPorches;
@dynamic isGarege;

@end
