//
//  WoNPMAInspectionOtherDetailExtSerDc+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionOtherDetailExtSerDc+CoreDataProperties.h"

@implementation WoNPMAInspectionOtherDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionOtherDetailExtSerDc *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAInspectionOtherDetailExtSerDc"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic isInspectorMakeCompleteInspection;
@dynamic isNoTreatmentRecommended;
@dynamic isRecommendTreatment;
@dynamic locationListOfInfestation;
@dynamic locationListOfVisibleAndHiddenDamage;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic noTreatmentRecommendedDescription;
@dynamic recommendTreatmentDescription;
@dynamic userName;
@dynamic woNPMInspectionOtherDetailId;
@dynamic workorderId;

@end
