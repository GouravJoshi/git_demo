//
//  WoNPMAFinalizeReportDetailExtSerDc+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAFinalizeReportDetailExtSerDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAFinalizeReportDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAFinalizeReportDetailExtSerDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMFinalizeReportDetailId;
@property (nullable, nonatomic, copy) NSNumber *isSlab;
@property (nullable, nonatomic, copy) NSNumber *isBasement;
@property (nullable, nonatomic, copy) NSNumber *isOther;
@property (nullable, nonatomic, copy) NSString *commentFor99B;//CommentFor99B
@property (nullable, nonatomic, copy) NSString *otherComment;
@property (nullable, nonatomic, copy) NSNumber *isCrawl;

@end

NS_ASSUME_NONNULL_END
