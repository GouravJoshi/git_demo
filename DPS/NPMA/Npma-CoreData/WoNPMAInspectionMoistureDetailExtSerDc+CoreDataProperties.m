//
//  WoNPMAInspectionMoistureDetailExtSerDc+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionMoistureDetailExtSerDc+CoreDataProperties.h"

@implementation WoNPMAInspectionMoistureDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionMoistureDetailExtSerDc *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAInspectionMoistureDetailExtSerDc"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic workorderId;
@dynamic woNPMMoistureDetailId;
@dynamic isEvidenceOfMoisture;
@dynamic isNoVisibleEvidence;
@dynamic isVisibleEvidence;
@dynamic isFungiObserved;
@dynamic fungiObservedDescriptionAndLocation;
@dynamic isDamage;
@dynamic damageDescriptionAndLocation;
@dynamic isCorrectiveAction;
@dynamic correctiveActionDescriptionAndLocation;
@dynamic isEvidenceOfPresence;
@dynamic evidenceOfPresenceDescription;

@end
