//
//  WoNPMAInspectionOtherDetailExtSerDc+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionOtherDetailExtSerDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAInspectionOtherDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionOtherDetailExtSerDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSNumber *isInspectorMakeCompleteInspection;
@property (nullable, nonatomic, copy) NSNumber *isNoTreatmentRecommended;
@property (nullable, nonatomic, copy) NSNumber *isRecommendTreatment;
@property (nullable, nonatomic, copy) NSString *locationListOfInfestation;
@property (nullable, nonatomic, copy) NSString *locationListOfVisibleAndHiddenDamage;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *noTreatmentRecommendedDescription;
@property (nullable, nonatomic, copy) NSString *recommendTreatmentDescription;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *woNPMInspectionOtherDetailId;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
