//
//  WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs+CoreDataProperties.h"

@implementation WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic workorderId;
@dynamic woNPMAProductDetailId;
@dynamic isBaitStation;
@dynamic nameOfSystem;
@dynamic ePARegistrationNumber;
@dynamic numberOfStationInstalled;
@dynamic isPrimary;
@dynamic isActive;

@end
