//
//  WoNPMAInspectionGeneralDescriptionExtSerDc+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionGeneralDescriptionExtSerDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAInspectionGeneralDescriptionExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionGeneralDescriptionExtSerDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMAInspectionId;
@property (nullable, nonatomic, copy) NSString *blockEquals;
@property (nullable, nonatomic, copy) NSString *approxSqFt;
@property (nullable, nonatomic, copy) NSString *linearFt;
@property (nullable, nonatomic, copy) NSString *numberOfStories;
@property (nullable, nonatomic, copy) NSString *originalTreatmentDate;
@property (nullable, nonatomic, copy) NSString *inspectedBy;
@property (nullable, nonatomic, copy) NSString *inspectedDate;
@property (nullable, nonatomic, copy) NSString *structureType;
@property (nullable, nonatomic, copy) NSString *workCompletedBy;
@property (nullable, nonatomic, copy) NSString *workCompletedDate;
@property (nullable, nonatomic, copy) NSString *slab;
@property (nullable, nonatomic, copy) NSNumber *isPerimeter;
@property (nullable, nonatomic, copy) NSNumber *isHP;
@property (nullable, nonatomic, copy) NSString *crawlDoorHeight;
@property (nullable, nonatomic, copy) NSNumber *isInside;
@property (nullable, nonatomic, copy) NSNumber *isOutside;
@property (nullable, nonatomic, copy) NSNumber *isRemoveWoodDebris;
@property (nullable, nonatomic, copy) NSString *brickVeneer;
@property (nullable, nonatomic, copy) NSString *hollowBlack;
@property (nullable, nonatomic, copy) NSString *stoneVeneer;
@property (nullable, nonatomic, copy) NSString *porches;
@property (nullable, nonatomic, copy) NSString *garege;
@property (nullable, nonatomic, copy) NSString *moistureMeterReading;
@property (nullable, nonatomic, copy) NSNumber *isDry;
@property (nullable, nonatomic, copy) NSNumber *isDamp;
@property (nullable, nonatomic, copy) NSNumber *isWet;
@property (nullable, nonatomic, copy) NSNumber *isInstallMoistureBarrier;
@property (nullable, nonatomic, copy) NSNumber *isDryerVentedOutside;
@property (nullable, nonatomic, copy) NSNumber *isVentsNecessary;
@property (nullable, nonatomic, copy) NSString *ventsNumber;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *color;
@property (nullable, nonatomic, copy) NSString *numberOfSectionsInstalled;
@property (nullable, nonatomic, copy) NSString *typeOfSystem;
@property (nullable, nonatomic, copy) NSString *extraMaterialToCompleteJob;
@property (nullable, nonatomic, copy) NSNumber *isControlOfExistingInfestation;
@property (nullable, nonatomic, copy) NSNumber *isPreventionOfInfestation;
@property (nullable, nonatomic, copy) NSString *constructionTypeSysName;
@property (nullable, nonatomic, copy) NSString *foundationTypeSysName;
@property (nullable, nonatomic, copy) NSString *userName;//crawlSpaceHeight
@property (nullable, nonatomic, copy) NSString *crawlSpaceHeight;
@property (nullable, nonatomic, copy) NSNumber *isSlab;
@property (nullable, nonatomic, copy) NSNumber *isBrickVeneer;
@property (nullable, nonatomic, copy) NSNumber *isHollowBlack;
@property (nullable, nonatomic, copy) NSNumber *isStoneVeneer;
@property (nullable, nonatomic, copy) NSNumber *isPorches;
@property (nullable, nonatomic, copy) NSNumber *isGarege;

@end

NS_ASSUME_NONNULL_END
