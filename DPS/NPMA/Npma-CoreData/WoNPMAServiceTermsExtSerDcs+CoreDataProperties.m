//
//  WoNPMAServiceTermsExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 09/11/20.
//
//

#import "WoNPMAServiceTermsExtSerDcs+CoreDataProperties.h"

@implementation WoNPMAServiceTermsExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAServiceTermsExtSerDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAServiceTermsExtSerDcs"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic departmentId;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic termsId;
@dynamic termsnConditions;
@dynamic userName;
@dynamic woNPMAServiceTermsId;
@dynamic workorderId;
@dynamic termsTitle;
@dynamic isActive;

@end
