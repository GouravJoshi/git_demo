//
//  WoNPMAServiceTermsExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 09/11/20.
//
//

#import "WoNPMAServiceTermsExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAServiceTermsExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAServiceTermsExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *departmentId;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *termsId;
@property (nullable, nonatomic, copy) NSString *termsnConditions;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *woNPMAServiceTermsId;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *termsTitle;
@property (nullable, nonatomic, copy) NSNumber *isActive;

@end

NS_ASSUME_NONNULL_END
