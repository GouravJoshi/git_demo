//
//  WoNPMAServicePricingExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 09/11/20.
//
//

#import "WoNPMAServicePricingExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAServicePricingExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAServicePricingExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSNumber *isActive;
@property (nullable, nonatomic, copy) NSNumber *isDiscount;
@property (nullable, nonatomic, copy) NSNumber *isRenewal;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *serviceId;
@property (nullable, nonatomic, copy) NSString *serviceName;
@property (nullable, nonatomic, copy) NSString *servicePrice;
@property (nullable, nonatomic, copy) NSString *serviceSysName;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *woNPMAServicePricingId;
@property (nullable, nonatomic, copy) NSString *workorderId;

@end

NS_ASSUME_NONNULL_END
