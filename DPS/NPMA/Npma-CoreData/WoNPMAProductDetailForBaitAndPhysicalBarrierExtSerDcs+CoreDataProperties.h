//
//  WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMAProductDetailId;
@property (nullable, nonatomic, copy) NSNumber *isBaitStation;
@property (nullable, nonatomic, copy) NSString *nameOfSystem;
@property (nullable, nonatomic, copy) NSString *ePARegistrationNumber;
@property (nullable, nonatomic, copy) NSString *numberOfStationInstalled;
@property (nullable, nonatomic, copy) NSNumber *isPrimary;
@property (nullable, nonatomic, copy) NSNumber *isActive;

@end

NS_ASSUME_NONNULL_END
