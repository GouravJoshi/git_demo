//
//  WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAProductDetailForSoilAndWoodExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAProductDetailForSoilAndWoodExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMAProductDetailId;
@property (nullable, nonatomic, copy) NSString *productMasterId;
@property (nullable, nonatomic, copy) NSNumber *isSoil;
@property (nullable, nonatomic, copy) NSString *quantity;
@property (nullable, nonatomic, copy) NSString *unitId;
@property (nullable, nonatomic, copy) NSString *ePARegistrationNumber;
@property (nullable, nonatomic, copy) NSString *concentration;
@property (nullable, nonatomic, copy) NSNumber *isPrimary;
@property (nullable, nonatomic, copy) NSNumber *isTreatmentCompleted;
@property (nullable, nonatomic, copy) NSNumber *isActive;
@property (nullable, nonatomic, copy) NSString *productCategoryId;

@end

NS_ASSUME_NONNULL_END
