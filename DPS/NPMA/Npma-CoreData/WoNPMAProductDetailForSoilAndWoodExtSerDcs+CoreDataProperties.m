//
//  WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAProductDetailForSoilAndWoodExtSerDcs+CoreDataProperties.h"

@implementation WoNPMAProductDetailForSoilAndWoodExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAProductDetailForSoilAndWoodExtSerDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAProductDetailForSoilAndWoodExtSerDcs"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy; 
@dynamic modifiedDate;
@dynamic userName;
@dynamic workorderId;
@dynamic woNPMAProductDetailId;
@dynamic productMasterId;
@dynamic isSoil;
@dynamic quantity;
@dynamic unitId;
@dynamic ePARegistrationNumber;
@dynamic concentration;
@dynamic isPrimary;
@dynamic isTreatmentCompleted;
@dynamic isActive;
@dynamic productCategoryId;

@end
