//
//  WoNPMAInspectionObstructionExtSerDcs+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionObstructionExtSerDcs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAInspectionObstructionExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionObstructionExtSerDcs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMAInspectionObstructionId;
@property (nullable, nonatomic, copy) NSString *areaId;
@property (nullable, nonatomic, copy) NSString *locations;
@property (nullable, nonatomic, copy) NSString *inspectionObstructionDescription;
@property (nullable, nonatomic, copy) NSNumber *isActive;


@end

NS_ASSUME_NONNULL_END
