//
//  WoNPMAFinalizeReportDetailExtSerDc+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAFinalizeReportDetailExtSerDc : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WoNPMAFinalizeReportDetailExtSerDc+CoreDataProperties.h"
