//
//  WoNPMAFinalizeReportDetailExtSerDc+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAFinalizeReportDetailExtSerDc+CoreDataProperties.h"

@implementation WoNPMAFinalizeReportDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAFinalizeReportDetailExtSerDc *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAFinalizeReportDetailExtSerDc"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic workorderId;
@dynamic woNPMFinalizeReportDetailId;
@dynamic isSlab;
@dynamic isBasement;
@dynamic isOther;
@dynamic commentFor99B;//commentFor99B
@dynamic otherComment;
@dynamic isCrawl;

@end
