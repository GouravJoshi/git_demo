//
//  WoNPMAInspectionMoistureDetailExtSerDc+CoreDataProperties.h
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionMoistureDetailExtSerDc+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAInspectionMoistureDetailExtSerDc (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionMoistureDetailExtSerDc *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *companyKey;
@property (nullable, nonatomic, copy) NSString *createdBy;
@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSString *modifiedBy;
@property (nullable, nonatomic, copy) NSString *modifiedDate;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSString *workorderId;
@property (nullable, nonatomic, copy) NSString *woNPMMoistureDetailId;
@property (nullable, nonatomic, copy) NSNumber *isEvidenceOfMoisture;
@property (nullable, nonatomic, copy) NSNumber *isNoVisibleEvidence;
@property (nullable, nonatomic, copy) NSNumber *isVisibleEvidence;
@property (nullable, nonatomic, copy) NSNumber *isFungiObserved;
@property (nullable, nonatomic, copy) NSString *fungiObservedDescriptionAndLocation;
@property (nullable, nonatomic, copy) NSNumber *isDamage;
@property (nullable, nonatomic, copy) NSString *damageDescriptionAndLocation;
@property (nullable, nonatomic, copy) NSNumber *isCorrectiveAction;
@property (nullable, nonatomic, copy) NSString *correctiveActionDescriptionAndLocation;
@property (nullable, nonatomic, copy) NSNumber *isEvidenceOfPresence;
@property (nullable, nonatomic, copy) NSString *evidenceOfPresenceDescription;

@end

NS_ASSUME_NONNULL_END
