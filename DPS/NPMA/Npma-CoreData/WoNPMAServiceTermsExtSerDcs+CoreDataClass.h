//
//  WoNPMAServiceTermsExtSerDcs+CoreDataClass.h
//  
//
//  Created by Saavan Patidar on 09/11/20.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface WoNPMAServiceTermsExtSerDcs : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WoNPMAServiceTermsExtSerDcs+CoreDataProperties.h"
