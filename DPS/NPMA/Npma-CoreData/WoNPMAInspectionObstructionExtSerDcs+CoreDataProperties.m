//
//  WoNPMAInspectionObstructionExtSerDcs+CoreDataProperties.m
//  
//
//  Created by Saavan Patidar on 14/10/20.
//
//

#import "WoNPMAInspectionObstructionExtSerDcs+CoreDataProperties.h"

@implementation WoNPMAInspectionObstructionExtSerDcs (CoreDataProperties)

+ (NSFetchRequest<WoNPMAInspectionObstructionExtSerDcs *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"WoNPMAInspectionObstructionExtSerDcs"];
}

@dynamic companyKey;
@dynamic createdBy;
@dynamic createdDate;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic userName;
@dynamic workorderId;
@dynamic woNPMAInspectionObstructionId;
@dynamic areaId;
@dynamic locations;
@dynamic inspectionObstructionDescription;
@dynamic isActive;

@end
