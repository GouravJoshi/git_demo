//
//  NPMA_Inspection_iPadVC.swift
//  DPS
//  Saavan Patidar
//  Created by NavinPatidar on 10/12/20.
//  Copyright © 2020 Saavan. All rights reserved.


import UIKit

class NPMA_Inspection_iPadVC: UIViewController {
    
    //MARK: - -----------------------------------Global Variables -----------------------------------
    @objc  var strWoId = NSString ()
    var strWoLeadId = String ()
    var objWorkorderDetail = NSManagedObject()
    var graphXmlGlobalIfExist = String()
    var graphXmlGlobalIfExistNew = String()

    var isEditGeneralDes = Bool() , isEditOtherDetail = Bool() , isEditMoistureDetail = Bool()
    
    //MARK:
    //MARK: -------------IBOutlet --------------
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnClock: UIButton!
    @IBOutlet weak var btnServiceDocs: UIButton!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnFooterPropsal: UIButton!
    @IBOutlet weak var btnFooterFinalize: UIButton!

    // ---General Des
    @IBOutlet weak var viewInspectionDetail: CardView!
    @IBOutlet weak var txtBlockEqual: UITextField!
    @IBOutlet weak var txtApproxSqft: UITextField!
    @IBOutlet weak var txtLinerft: UITextField!
    @IBOutlet weak var txtOfStories: UITextField!
    @IBOutlet weak var txtOrignalTreatmentDate: UITextField!
    @IBOutlet weak var txtInspectedBy: UITextField!
    @IBOutlet weak var txtInspectedBDate: UITextField!
    @IBOutlet weak var txtStructureType: UITextField!
    @IBOutlet weak var txtWorkCompletedBy: UITextField!
    @IBOutlet weak var txtWorkCompletedDate: UITextField!
    
    @IBOutlet weak var btn_SlabYes: UIButton!
    @IBOutlet weak var btn_SlabNo: UIButton!
    
    @IBOutlet weak var btn_PerimeterPluse: UIButton!
    @IBOutlet weak var btn_HP: UIButton!
    @IBOutlet weak var txtCrawlDoorHeight: UITextField!
    @IBOutlet weak var txtCrawlSpaceHeight: UITextField!
    @IBOutlet weak var btn_Inside: UIButton!
    @IBOutlet weak var btn_OutSide: UIButton!
    @IBOutlet weak var btn_RemoveWoodYes: UIButton!
    @IBOutlet weak var btn_RemoveWoodNo: UIButton!
    @IBOutlet weak var btn_BrickVeneer: UIButton!
    @IBOutlet weak var btn_HollowBlock: UIButton!
    @IBOutlet weak var btn_StoneVeneer: UIButton!
    @IBOutlet weak var btn_Porches: UIButton!
    @IBOutlet weak var btn_Garege: UIButton!
    
    
    @IBOutlet weak var txtMoistureMeterReading: UITextField!
    @IBOutlet weak var btn_MoistureDry: UIButton!
    @IBOutlet weak var btn_MoistureDamp: UIButton!
    @IBOutlet weak var btn_MoistureWet: UIButton!
    @IBOutlet weak var btn_InstallMoistureBarrierYes: UIButton!
    @IBOutlet weak var btn_InstallMoistureBarrierNo: UIButton!
    @IBOutlet weak var btn_DryerVentedOutsideYes: UIButton!
    @IBOutlet weak var btn_DryerVentedOutsideNo: UIButton!
    @IBOutlet weak var btn_VentsNecessaryYes: UIButton!
    @IBOutlet weak var btn_VentsNecessaryNo: UIButton!
    @IBOutlet weak var txtVents: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtColor: UITextField!
    @IBOutlet weak var txtSectionInstalled: UITextField!
    @IBOutlet weak var txtTypeOfSystem: UITextField!
    @IBOutlet weak var txtExtraMaterialNecessary: UITextView!
    @IBOutlet weak var btn_TreatMentType_ControlofExistingInfestation: UIButton!
    @IBOutlet weak var btn_TreatMentType_PreventionofExistingInfestation: UIButton!
    
    @IBOutlet weak var view_ConstructionType: UIView!
    @IBOutlet weak var height_view_ConstructionType: NSLayoutConstraint!
    var aryConstructionTypeOption = NSMutableArray() , arySelectedConstructionTypeOption = NSMutableArray()
    
    @IBOutlet weak var view_FoundationType: UIView!
    @IBOutlet weak var height_view_FoundationType: NSLayoutConstraint!
    @IBOutlet weak var height_view_VentNecessary: NSLayoutConstraint!

    
    var aryFoundationTypeOption = NSMutableArray() ,arySelectedFoundationTypeOption = NSMutableArray()
    
    
    
    // ---------GraphView-------------
    @IBOutlet weak var viewGraphView: CardView!
    @IBOutlet weak var graphDrawingWebView: WKWebView!
    //Graph Legend

        @IBOutlet weak var viewGraphLegend: UIView!
        @IBOutlet weak var heightGraphLegend: NSLayoutConstraint!
        @IBOutlet weak var heightScrollGraphLegend: NSLayoutConstraint!


    // ---------FindingView-------------
    @IBOutlet weak var viewFindings: CardView!
    @IBOutlet weak var tv_Finding: UITableView!
    var aryFinding = NSMutableArray()
    @IBOutlet weak var btn_Resolved: UIButton!
    @IBOutlet weak var btn_UnResolved: UIButton!
    
    // ---------OtherDetails-------------
    @IBOutlet weak var viewOtherDetails: CardView!
    
    @IBOutlet weak var btn_OtherDetails_TheInspectoreCouldnotCompleteInspection: UIButton!
    
    @IBOutlet weak var txt_OtherDetails_Repair: UITextField!
    @IBOutlet weak var btn_OtherDetails_novisibleEvidence: UIButton!
    
    @IBOutlet weak var btn_OtherDetails_LiveInsects: UIButton!
    @IBOutlet weak var txt_OtherDetails_LiveInsects: UITextField!
    
    @IBOutlet weak var btn_OtherDetails_DeadInsects: UIButton!
    @IBOutlet weak var txt_OtherDetails_DeadInsects: UITextField!
    @IBOutlet weak var btn_OtherDetails_VisibleDamage: UIButton!
    @IBOutlet weak var txt_OtherDetails_VisibleDamage: UITextField!
    @IBOutlet weak var btn_OtherDetails_NoTreatment: UIButton!
    @IBOutlet weak var txt_OtherDetails_NoTreatment: UITextField!
    @IBOutlet weak var btn_OtherDetails_ReCommendedTreatment: UIButton!
    @IBOutlet weak var txt_OtherDetails_ReCommendedTreatment: UITextField!
    
    
    @IBOutlet weak var txt_OtherDetails_ListOfLocationInfestation: UITextField!
    @IBOutlet weak var txt_OtherDetails_ListOfLocationProbableHiddenDamage: UITextField!
    
    @IBOutlet weak var btn_OtherDetails_AddNewCondition: UIButton!
    
    @IBOutlet weak var tv_OtherDetails_ConduciveConditions: UITableView!
    @IBOutlet weak var Height_tv_OtherDetails_ConduciveConditions: NSLayoutConstraint!
    
    
    var aryOtherDetails_ConduciveConditions = NSMutableArray()
    
    @IBOutlet weak var tv_OtherDetails_Locations: UITableView!
    @IBOutlet weak var Height_tv_OtherDetails_Locations: NSLayoutConstraint!
    @IBOutlet weak var btn_OtherDetails_AddLocation: UIButton!
    @IBOutlet weak var txt_OtherDetails_Location: UITextField!
    @IBOutlet weak var txt_OtherDetails_Description: UITextField!
    var aryOtherDetails_Locations = NSMutableArray() ,aryOtherDetails_LocationsType = NSMutableArray()
    
    
    
    // ---------MoistureDetail-------------
    @IBOutlet weak var viewMoistureDetails: CardView!
    @IBOutlet weak var btn_EvidenceOfMoisture: UIButton!
    
    @IBOutlet weak var btn_NoVisible: UIButton!
    @IBOutlet weak var btn_Visible: UIButton!
    @IBOutlet weak var btn_FungiObserved: UIButton!
    @IBOutlet weak var heightView_B_Section: NSLayoutConstraint!
    
    @IBOutlet weak var txt_FungiObserved_Description: UITextField!
    @IBOutlet weak var btn_DamageFromWood: UIButton!
    @IBOutlet weak var txt_DamageFromWood_Description: UITextField!
    @IBOutlet weak var btn_CorrectiveAction: UIButton!
    @IBOutlet weak var txt_CorrectiveAction_Description: UITextField!
    @IBOutlet weak var btn_ThereIsEvidence: UIButton!
    @IBOutlet weak var txt_ThereIsEvidence_Description: UITextField!
    
    
    //MARK:
    //MARK: -------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        segment.addTarget(self, action: #selector(self.indexChanged(_:)), for: .valueChanged)
        segment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)], for: .normal)
       
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            lblHeaderTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"

            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            //strWoLeadId = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                strWoLeadId = "\(match.value(forKey: "leadId")!)"
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            self.back()
            
        }
        disableControls()
        UIInitialization()
        btn_Resolved.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        btn_UnResolved.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        
        //let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        //self.lblHeaderTitle.isUserInteractionEnabled = true
        //self.lblHeaderTitle.addGestureRecognizer(longPressRecognizer)
        
        // Saavan Patidar Changes to save data if App entered background or Terminated
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(onAppTerminatedSaveDataNPMA), name: Notification.Name("AppTerminatedSaveData"), object: nil)
        nc.addObserver(self, selector: #selector(onAppTerminatedSaveDataNPMA), name: Notification.Name("AppEnterdInBackgroundSaveData"), object: nil)
        
    }
    
    //
    @objc func onAppTerminatedSaveDataNPMA(_ notification:Notification) {
        
        // App DisAppeared
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            // Not To Edit
            
        }else{
            
            if isBackGround {
                saveGraphAsXML()
           }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)
        }
        isBackGround = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        isBackGround = false
    }
    
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
           self.view.endEditing(true)
           let board = UIPasteboard.general
           board.string = lblHeaderTitle.text
           showToastForSomeTime(title: "Copied....", message: "", time: 3, viewcontrol: self)
       }
    
    //MARK:
    //MARK: -------------Core Data Fetch Save Delete Functions-------------
    func SaveAllDataInCoreDAta()  {
        
        if(isEditGeneralDes){
            self.SaveGeneralDesc()
            self.isEditGeneralDes = false
        }
        
        if(isEditOtherDetail){
            self.SaveOtherDetail()
            self.isEditOtherDetail = false
        }
        
        if(isEditMoistureDetail){
            self.SaveMoistureDetail()
            self.isEditMoistureDetail = false
        }
        
        saveGraphAsXML()
        
    }
    
    //MARK:
    //MARK: -------------------GeneralDesc-----------------

    func fetchGeneralDesc() {
        let arrGeneralDesc = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        
        if(arrGeneralDesc.count != 0){
            let dictGeneral = arrGeneralDesc.object(at: 0)as! NSManagedObject
            print(dictGeneral)
            
            txtBlockEqual.text = "\(dictGeneral.value(forKey: "blockEquals")!)"
            
            txtApproxSqft.text = "\(dictGeneral.value(forKey: "approxSqFt")!)"
            
            txtLinerft.text = "\(dictGeneral.value(forKey: "linearFt")!)"
            
            txtOfStories.text = "\(dictGeneral.value(forKey: "numberOfStories")!)"
            
            txtOrignalTreatmentDate.text = "\(dictGeneral.value(forKey: "originalTreatmentDate")!)"
            txtOrignalTreatmentDate.text = changeStringDateToGivenFormat(strDate: "\(dictGeneral.value(forKey: "originalTreatmentDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")

            txtInspectedBy.text = "\(dictGeneral.value(forKey: "inspectedBy")!)"
            
            txtInspectedBDate.text = "\(dictGeneral.value(forKey: "inspectedDate")!)"
            txtInspectedBDate.text = changeStringDateToGivenFormat(strDate: "\(dictGeneral.value(forKey: "inspectedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            
            txtStructureType.text = "\(dictGeneral.value(forKey: "structureType")!)"
            
            
            txtWorkCompletedBy.text = "\(dictGeneral.value(forKey: "workCompletedBy")!)"
            
            
            txtWorkCompletedDate.text = "\(dictGeneral.value(forKey: "workCompletedDate")!)"
            
            txtWorkCompletedDate.text = changeStringDateToGivenFormat(strDate: "\(dictGeneral.value(forKey: "workCompletedDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            
           // txtSlab.text = "\(dictGeneral.value(forKey: "slab")!)"
            if(dictGeneral.value(forKey: "isSlab")!) as! Bool{
                btn_SlabYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_SlabNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            else{
                btn_SlabYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btn_SlabNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isPerimeter")!) as! Bool{
                btn_PerimeterPluse.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_PerimeterPluse.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isHP")!) as! Bool{
                btn_HP.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_HP.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            
            txtCrawlDoorHeight.text = "\(dictGeneral.value(forKey: "crawlDoorHeight")!)"
            txtCrawlSpaceHeight.text = "\(dictGeneral.value(forKey: "crawlSpaceHeight")!)"

            if(dictGeneral.value(forKey: "isInside")!) as! Bool{
                btn_Inside.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_Inside.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isOutside")!) as! Bool{
                btn_OutSide.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OutSide.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isRemoveWoodDebris")!) as! Bool{
                btn_RemoveWoodYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_RemoveWoodNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            else{
                btn_RemoveWoodYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btn_RemoveWoodNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            }
            
            
            //--forchange textfiled to check box
            if(dictGeneral.value(forKey: "isBrickVeneer")!) as! Bool{
                btn_BrickVeneer.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }else{
                btn_BrickVeneer.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isHollowBlack")!) as! Bool{
                btn_HollowBlock.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }else{
                btn_HollowBlock.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
           
            if(dictGeneral.value(forKey: "isStoneVeneer")!) as! Bool{
                btn_StoneVeneer.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }else{
                btn_StoneVeneer.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isPorches")!) as! Bool{
                btn_Porches.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }else{
                btn_Porches.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isGarege")!) as! Bool{
                btn_Garege.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }else{
                btn_Garege.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
           
            txtMoistureMeterReading.text = "\(dictGeneral.value(forKey: "moistureMeterReading")!)"
            
            if(dictGeneral.value(forKey: "isDry")!) as! Bool{
                btn_MoistureDry.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_MoistureDry.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isDamp")!) as! Bool{
                btn_MoistureDamp.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_MoistureDamp.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isWet")!) as! Bool{
                btn_MoistureWet.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_MoistureWet.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            
            if(dictGeneral.value(forKey: "isInstallMoistureBarrier")!) as! Bool{
                btn_InstallMoistureBarrierYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_InstallMoistureBarrierNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            else{
                btn_InstallMoistureBarrierYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btn_InstallMoistureBarrierNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isDryerVentedOutside")!) as! Bool{
                btn_DryerVentedOutsideYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_DryerVentedOutsideNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            else{
                btn_DryerVentedOutsideYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btn_DryerVentedOutsideNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            }
            
            
            if(dictGeneral.value(forKey: "isVentsNecessary")!) as! Bool{
                btn_VentsNecessaryYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_VentsNecessaryNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                self.height_view_VentNecessary.constant = 55.0

            }
            else{
                btn_VentsNecessaryYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btn_VentsNecessaryNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                self.height_view_VentNecessary.constant = 0.0

            }
            
            txtVents.text = "\(dictGeneral.value(forKey: "ventsNumber")!)"
            
            txtType.text = "\(dictGeneral.value(forKey: "type")!)"
            
            
            txtColor.text = "\(dictGeneral.value(forKey: "color")!)"
            
            txtSectionInstalled.text = "\(dictGeneral.value(forKey: "numberOfSectionsInstalled")!)"
            txtTypeOfSystem.text = "\(dictGeneral.value(forKey: "typeOfSystem")!)"
            
            
            
            txtExtraMaterialNecessary.text = "\(dictGeneral.value(forKey: "extraMaterialToCompleteJob")!)"
            
            if(dictGeneral.value(forKey: "isControlOfExistingInfestation")!) as! Bool{
                btn_TreatMentType_ControlofExistingInfestation.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_TreatMentType_ControlofExistingInfestation.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            
            if(dictGeneral.value(forKey: "isPreventionOfInfestation")!) as! Bool{
                btn_TreatMentType_PreventionofExistingInfestation.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_TreatMentType_PreventionofExistingInfestation.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            let constructionTypeSysName = "\(dictGeneral.value(forKey: "constructionTypeSysName")!)"
            let foundationTypeSysName = "\(dictGeneral.value(forKey: "foundationTypeSysName")!)"
            
            let aryTempCons = constructionTypeSysName.split(separator: ",")
            let aryTempFound = foundationTypeSysName.split(separator: ",")
            
            arySelectedConstructionTypeOption = NSMutableArray()
            arySelectedFoundationTypeOption = NSMutableArray()
            arySelectedConstructionTypeOption = (aryTempCons as NSArray).mutableCopy()as! NSMutableArray
            arySelectedFoundationTypeOption = (aryTempFound as NSArray).mutableCopy()as! NSMutableArray
        }
        // Putting Default Values
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        var strUserName = String()
        
        if let value = dictLoginData.value(forKeyPath: "EmployeeName") {
            
            strUserName = "\(value)"
            
        }
        
        if txtInspectedBy.text?.count == 0 {
            
            txtInspectedBy.text = strUserName
            
        }
        
        if txtWorkCompletedBy.text?.count == 0 {
            
            txtWorkCompletedBy.text = strUserName
            
        }
        
        if txtInspectedBDate.text?.count == 0 {
            
            txtInspectedBDate.text = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "scheduleStartDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy")//Global().strGetCurrentDate(inFormat: "MM/dd/yyyy")
            
        }
        
        if txtWorkCompletedDate.text?.count == 0 {
            
            txtWorkCompletedDate.text = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "scheduleEndDateTime") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            
        }
        
        
    }
    
    func SaveGeneralDesc() {
        //---Add-----
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("workorderId")
        arrOfValues.add(strWoId)
        
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        
        
        
        arrOfKeys.add("blockEquals")
        arrOfValues.add("\(txtBlockEqual.text!)")
        
        arrOfKeys.add("approxSqFt")
        arrOfValues.add("\(txtApproxSqft.text!)")
        
        arrOfKeys.add("linearFt")
        arrOfValues.add("\(txtLinerft.text!)")
        
        arrOfKeys.add("numberOfStories")
        arrOfValues.add("\(txtOfStories.text!)")
        
        arrOfKeys.add("originalTreatmentDate")
        arrOfValues.add("\(txtOrignalTreatmentDate.text!)")
        
        arrOfKeys.add("inspectedBy")
        arrOfValues.add("\(txtInspectedBy.text!)")
        
        arrOfKeys.add("inspectedDate")
        arrOfValues.add("\(txtInspectedBDate.text!)")
        
        arrOfKeys.add("structureType")
        arrOfValues.add("\(txtStructureType.text!)")
        
        arrOfKeys.add("workCompletedBy")
        arrOfValues.add("\(txtWorkCompletedBy.text!)")
        
        arrOfKeys.add("workCompletedDate")
        arrOfValues.add("\(txtWorkCompletedDate.text!)")
        
        arrOfKeys.add("slab")
        arrOfValues.add("")
       
        arrOfKeys.add("isSlab")
        arrOfValues.add((btn_SlabYes.currentImage == UIImage(named: "NPMA_Radio_Yes")) ? true : false)

        
        arrOfKeys.add("isPerimeter")
        arrOfValues.add((btn_PerimeterPluse.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isHP")
        arrOfValues.add((btn_HP.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("crawlDoorHeight")
        arrOfValues.add("\(txtCrawlDoorHeight.text!)")
        
        arrOfKeys.add("crawlSpaceHeight")
        arrOfValues.add("\(txtCrawlSpaceHeight.text!)")
        
        arrOfKeys.add("isInside")
        arrOfValues.add((btn_Inside.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isOutside")
        arrOfValues.add((btn_OutSide.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isRemoveWoodDebris")
        arrOfValues.add((btn_RemoveWoodYes.currentImage == UIImage(named: "NPMA_Radio_Yes")) ? true : false)
        
        arrOfKeys.add("brickVeneer")
        arrOfValues.add("")
        
        arrOfKeys.add("hollowBlack")
        arrOfValues.add("")
        
        arrOfKeys.add("stoneVeneer")
        arrOfValues.add("")
        
        arrOfKeys.add("porches")
        arrOfValues.add("")
        
        arrOfKeys.add("garege")
        arrOfValues.add("")
        

        
        arrOfKeys.add("isBrickVeneer")
        arrOfValues.add((btn_BrickVeneer.currentImage == UIImage(named: "NPMA_Check")) ? true : false)

        arrOfKeys.add("isHollowBlack")
        arrOfValues.add((btn_HollowBlock.currentImage == UIImage(named: "NPMA_Check")) ? true : false)

        arrOfKeys.add("isStoneVeneer")
        arrOfValues.add((btn_StoneVeneer.currentImage == UIImage(named: "NPMA_Check")) ? true : false)

        arrOfKeys.add("isPorches")
        arrOfValues.add((btn_Porches.currentImage == UIImage(named: "NPMA_Check")) ? true : false)

        arrOfKeys.add("isGarege")
        arrOfValues.add((btn_Garege.currentImage == UIImage(named: "NPMA_Check")) ? true : false)


        arrOfKeys.add("moistureMeterReading")
        arrOfValues.add("\(txtMoistureMeterReading.text!)")
        
        arrOfKeys.add("isDry")
        arrOfValues.add((btn_MoistureDry.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isDamp")
        arrOfValues.add((btn_MoistureDamp.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isWet")
        arrOfValues.add((btn_MoistureWet.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isInstallMoistureBarrier")
        arrOfValues.add((btn_InstallMoistureBarrierYes.currentImage == UIImage(named: "NPMA_Radio_Yes")) ? true : false)
        
        arrOfKeys.add("isDryerVentedOutside")
        arrOfValues.add((btn_DryerVentedOutsideYes.currentImage == UIImage(named: "NPMA_Radio_Yes")) ? true : false)
        
        arrOfKeys.add("isVentsNecessary")
        arrOfValues.add((btn_VentsNecessaryYes.currentImage == UIImage(named: "NPMA_Radio_Yes")) ? true : false)
        
        arrOfKeys.add("ventsNumber")
        arrOfValues.add("\(txtVents.text!)")
        
        arrOfKeys.add("type")
        arrOfValues.add("\(txtType.text!)")
        
        arrOfKeys.add("color")
        arrOfValues.add("\(txtColor.text!)")
        
        arrOfKeys.add("numberOfSectionsInstalled")
        arrOfValues.add("\(txtSectionInstalled.text!)")
        
        arrOfKeys.add("typeOfSystem")
        arrOfValues.add("\(txtTypeOfSystem.text!)")
        
        arrOfKeys.add("extraMaterialToCompleteJob")
        arrOfValues.add("\(txtExtraMaterialNecessary.text!)")
        
        arrOfKeys.add("isControlOfExistingInfestation")
        arrOfValues.add((btn_TreatMentType_ControlofExistingInfestation.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isPreventionOfInfestation")
        arrOfValues.add((btn_TreatMentType_PreventionofExistingInfestation.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        var constructionTypeSysName = ""
        var foundationTypeSysName = ""
        
        for item in arySelectedConstructionTypeOption {
            constructionTypeSysName = constructionTypeSysName + "\(item),"
        }
        constructionTypeSysName = constructionTypeSysName == "" ? constructionTypeSysName : String(constructionTypeSysName.dropLast())
        for item in arySelectedFoundationTypeOption {
            foundationTypeSysName = foundationTypeSysName + "\(item),"
        }
        foundationTypeSysName = foundationTypeSysName == "" ? foundationTypeSysName :  String(foundationTypeSysName.dropLast())
        
        
        arrOfKeys.add("constructionTypeSysName")
        arrOfValues.add(constructionTypeSysName)
        
        arrOfKeys.add("foundationTypeSysName")
        arrOfValues.add(foundationTypeSysName)
        
        
        
        let arrGeneralDesc = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        //For Update
        
        if(arrGeneralDesc.count != 0){
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
        }
        //For ADD
        else{
            arrOfKeys.add("woNPMAInspectionId")
            arrOfValues.add("")
            saveDataInDB(strEntity: Entity_WoNPMAInspectionGeneralDescriptionExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
        // Saving Disclaimer In DB
        
        
    }
    
    //MARK:
    //MARK: -------------------OtherDetail-----------------

    func fetchOtherDetail() {
        let arrOtherDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        
        if(arrOtherDetail.count != 0){
            let dictGeneral = arrOtherDetail.object(at: 0)as! NSManagedObject
            print(dictGeneral)
            
          
            if(dictGeneral.value(forKey: "isDeadInsect")!) as! Bool{
                btn_OtherDetails_DeadInsects.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_DeadInsects.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            txt_OtherDetails_DeadInsects.text = "\(dictGeneral.value(forKey: "deadInsectDecriptionAndLocation")!)"

            if(dictGeneral.value(forKey: "isInspectorMakeCompleteInspection")!) as! Bool{
                btn_OtherDetails_TheInspectoreCouldnotCompleteInspection.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_TheInspectoreCouldnotCompleteInspection.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
        
            if(dictGeneral.value(forKey: "isLiveInsect")!) as! Bool{
                btn_OtherDetails_LiveInsects.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_LiveInsects.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isNoTreatmentRecommended")!) as! Bool{
                btn_OtherDetails_NoTreatment.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_NoTreatment.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }

            if(dictGeneral.value(forKey: "isRecommendTreatment")!) as! Bool{
                btn_OtherDetails_ReCommendedTreatment.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_ReCommendedTreatment.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isVisibleDemage")!) as! Bool{
                btn_OtherDetails_VisibleDamage.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_VisibleDamage.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isVisibleEvidence")!) as! Bool{
                btn_OtherDetails_novisibleEvidence.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_OtherDetails_novisibleEvidence.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            txt_OtherDetails_LiveInsects.text = "\(dictGeneral.value(forKey: "liveInsectDescriptionAndLocation")!)"

            txt_OtherDetails_ListOfLocationInfestation.text = "\(dictGeneral.value(forKey: "locationListOfInfestation")!)"


            txt_OtherDetails_ListOfLocationProbableHiddenDamage.text = "\(dictGeneral.value(forKey: "locationListOfVisibleAndHiddenDamage")!)"

            txt_OtherDetails_NoTreatment.text = "\(dictGeneral.value(forKey: "noTreatmentRecommendedDescription")!)"

            txt_OtherDetails_ReCommendedTreatment.text = "\(dictGeneral.value(forKey: "recommendTreatmentDescription")!)"
          
            txt_OtherDetails_Repair.text = "\(dictGeneral.value(forKey: "repairToBeDone")!)"
          
            txt_OtherDetails_VisibleDamage.text = "\(dictGeneral.value(forKey: "visibleDemageDescriptionAndLocation")!)"

        }
    }
    
    func SaveOtherDetail() {
        //---Add-----
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("workorderId")
        arrOfValues.add(strWoId)
        
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
       
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
 
        arrOfKeys.add("isDeadInsect")
        arrOfValues.add((btn_OtherDetails_DeadInsects.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
       
        arrOfKeys.add("deadInsectDecriptionAndLocation")
        arrOfValues.add("\(txt_OtherDetails_DeadInsects.text!)")
        
        arrOfKeys.add("isInspectorMakeCompleteInspection")
        arrOfValues.add((btn_OtherDetails_TheInspectoreCouldnotCompleteInspection.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isLiveInsect")
        arrOfValues.add((btn_OtherDetails_LiveInsects.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isNoTreatmentRecommended")
        arrOfValues.add((btn_OtherDetails_NoTreatment.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isRecommendTreatment")
        arrOfValues.add((btn_OtherDetails_ReCommendedTreatment.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
      
        arrOfKeys.add("isVisibleDemage")
        arrOfValues.add((btn_OtherDetails_VisibleDamage.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isVisibleEvidence")
        arrOfValues.add((btn_OtherDetails_novisibleEvidence.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("liveInsectDescriptionAndLocation")
        arrOfValues.add("\(txt_OtherDetails_LiveInsects.text!)")
      
        arrOfKeys.add("locationListOfInfestation")
        arrOfValues.add("\(txt_OtherDetails_ListOfLocationInfestation.text!)")
        
        arrOfKeys.add("locationListOfVisibleAndHiddenDamage")
        arrOfValues.add("\(txt_OtherDetails_ListOfLocationProbableHiddenDamage.text!)")

        arrOfKeys.add("noTreatmentRecommendedDescription")
        arrOfValues.add("\(txt_OtherDetails_NoTreatment.text!)")
        
        arrOfKeys.add("recommendTreatmentDescription")
        arrOfValues.add("\(txt_OtherDetails_ReCommendedTreatment.text!)")
        
        arrOfKeys.add("repairToBeDone")
        arrOfValues.add("\(txt_OtherDetails_Repair.text!)")
        
        arrOfKeys.add("visibleDemageDescriptionAndLocation")
        arrOfValues.add("\(txt_OtherDetails_VisibleDamage.text!)")
        
        let arrMoistureDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        //For Update
        if(arrMoistureDetail.count != 0){
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
        }
        //For ADD
        else{
            arrOfKeys.add("woNPMInspectionOtherDetailId")
            arrOfValues.add("")
            saveDataInDB(strEntity: Entity_WoNPMAInspectionOtherDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
        
    }
    
    func fetchOtherDetailObstruction() {
    
        txt_OtherDetails_Location.text = ""
        txt_OtherDetails_Description.text = ""
        txt_OtherDetails_Location.tag = 0
        self.btn_OtherDetails_AddLocation.setTitle("Add", for: .normal)
        let arrOtherDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, predicate: (NSPredicate(format: "workorderId == %@ && isActive == YES", strWoId)))
        self.aryOtherDetails_Locations = NSMutableArray()
        self.aryOtherDetails_Locations = arrOtherDetail.mutableCopy()as! NSMutableArray
        self.tv_OtherDetails_Locations.reloadData()
    }
    
    func SaveOtherDetailObstruction(statusForAdd_Edit_Delete : Int , obj : NSManagedObject) {
      
        //1 for Add , 2 for Edit , 3 For Delete
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if(statusForAdd_Edit_Delete == 1){
           
            arrOfKeys.add("workorderId")
            arrOfValues.add(strWoId)
            
            arrOfKeys.add("companyKey")
            arrOfValues.add(Global().getCompanyKey())
            
            arrOfKeys.add("createdBy")
            arrOfValues.add(Global().getEmployeeId())
            
            arrOfKeys.add("createdDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("userName")
            arrOfValues.add(Global().getUserName())
            
            arrOfKeys.add("modifiedBy")
            arrOfValues.add(Global().getEmployeeId())
            
            arrOfKeys.add("modifiedDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfKeys.add("isActive")
            arrOfValues.add(true)
            
            arrOfKeys.add("woNPMAInspectionObstructionId")
            arrOfValues.add(Global().getReferenceNumber())
            
            arrOfKeys.add("areaId")
            arrOfValues.add("\(txt_OtherDetails_Location.tag)")
            
            arrOfKeys.add("inspectionObstructionDescription")
            arrOfValues.add("\(txt_OtherDetails_Description.text!)")
            
            arrOfKeys.add("locations")
            arrOfValues.add("\(txt_OtherDetails_Location.text!)")
           
            saveDataInDB(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        else if(statusForAdd_Edit_Delete == 2){
          
            arrOfKeys.add("modifiedDate")
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
           
            arrOfKeys.add("areaId")
            arrOfValues.add("\(txt_OtherDetails_Location.tag)")
            
            arrOfKeys.add("inspectionObstructionDescription")
            arrOfValues.add("\(txt_OtherDetails_Description.text!)")
            
            arrOfKeys.add("locations")
            arrOfValues.add("\(txt_OtherDetails_Location.text!)")
            
            let woNPMAInspectionObstructionId = "\(obj.value(forKey: "woNPMAInspectionObstructionId")!)"

            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && woNPMAInspectionObstructionId == %@", strWoId,woNPMAInspectionObstructionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
        }
        
        else if(statusForAdd_Edit_Delete == 3){
          
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            let woNPMAInspectionObstructionId = "\(obj.value(forKey: "woNPMAInspectionObstructionId")!)"

            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionObstructionExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && woNPMAInspectionObstructionId == %@", strWoId,woNPMAInspectionObstructionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
        }
    
       
        ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)
    }
    
    func fetchOtherDetailConduciveConditions() {
        let arrOtherDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, predicate: (NSPredicate(format: "workOrderId == %@ && isActive == YES", self.strWoId)))
        self.aryOtherDetails_ConduciveConditions = arrOtherDetail.mutableCopy()as! NSMutableArray
        self.tv_OtherDetails_ConduciveConditions.reloadData()
    }
    
    func DeleteOtherDetailConduciveConditions(statusForAdd_Edit_Delete : Int , obj : NSManagedObject) {
   
        //1 for Add , 2 for Edit , 3 For Delete
     
        //let woNPMInspectionConduciveConditionId = statusForAdd_Edit_Delete != 1 ? "\(obj.value(forKey: "sAConduciveConditionId")!)" : ""

        var woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "sAConduciveConditionId")!)"
        var woNPMInspectionConduciveConditionIdName = "sAConduciveConditionId"

        if woNPMInspectionConduciveConditionId.count == 0 {
            
            woNPMInspectionConduciveConditionId = "\(obj.value(forKey: "mobileSAConduciveConditionId")!)"
            woNPMInspectionConduciveConditionIdName = "mobileSAConduciveConditionId"
            
        }
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        

        arrOfKeys.add("modifyDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfKeys.add("strModifyDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        let arrInspectionObstruction = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, predicate: (NSPredicate(format: "workOrderId == %@ && \(woNPMInspectionConduciveConditionIdName) == %@", strWoId,woNPMInspectionConduciveConditionId)))
        //For Update
        if(arrInspectionObstruction.count != 0){
            if statusForAdd_Edit_Delete == 3 {
                arrOfKeys.add("isActive")
                arrOfValues.add(false)
            }
            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionConduciveConditionExtSerDc, predicate: NSPredicate(format: "workOrderId == %@ && \(woNPMInspectionConduciveConditionIdName) == %@", strWoId,woNPMInspectionConduciveConditionId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
            
            // Delete Comment and Document related to condtion
            //Entity_WoNPMAInspectionServiceConditionDocuments
            deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionServiceConditionComments, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))

            deleteAllRecordsFromDB(strEntity: Entity_WoNPMAInspectionServiceConditionDocuments, predicate: NSPredicate(format: "workOrderId == %@ && companyKey == %@", strWoId,Global().getCompanyKey()))
            
        }
       
        ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)
    }
  
    //MARK:
    //MARK: -------------------MoistureDetail-----------------
    func fetchMoistureDetail() {
        let arrMoistureDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        
        if(arrMoistureDetail.count != 0){
            let dictGeneral = arrMoistureDetail.object(at: 0)as! NSManagedObject
            print(dictGeneral)
            
            txt_CorrectiveAction_Description.text = "\(dictGeneral.value(forKey: "correctiveActionDescriptionAndLocation")!)"
            txt_DamageFromWood_Description.text = "\(dictGeneral.value(forKey: "damageDescriptionAndLocation")!)"
            txt_ThereIsEvidence_Description.text = "\(dictGeneral.value(forKey: "evidenceOfPresenceDescription")!)"
            txt_FungiObserved_Description.text = "\(dictGeneral.value(forKey: "fungiObservedDescriptionAndLocation")!)"
            if(dictGeneral.value(forKey: "isFungiObserved")!) as! Bool{
                btn_FungiObserved.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_FungiObserved.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isCorrectiveAction")!) as! Bool{
                btn_CorrectiveAction.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_CorrectiveAction.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            if(dictGeneral.value(forKey: "isDamage")!) as! Bool{
                btn_DamageFromWood.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_DamageFromWood.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isEvidenceOfMoisture")!) as! Bool{
                btn_EvidenceOfMoisture.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_EvidenceOfMoisture.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isEvidenceOfPresence")!) as! Bool{
                btn_ThereIsEvidence.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_ThereIsEvidence.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isNoVisibleEvidence")!) as! Bool{
                btn_NoVisible.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            }
            else{
                btn_NoVisible.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if(dictGeneral.value(forKey: "isVisibleEvidence")!) as! Bool{
                btn_Visible.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                heightView_B_Section.constant = 305.0
            }
            else{
                btn_Visible.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                heightView_B_Section.constant = 0.0
            }
            
            
        }
    }
    
    func SaveMoistureDetail() {
        
        
        //---Add-----
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("workorderId")
        arrOfValues.add(strWoId)
        
        arrOfKeys.add("companyKey")
        arrOfValues.add(Global().getCompanyKey())
        
        arrOfKeys.add("createdBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("createdDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfKeys.add("modifiedBy")
        arrOfValues.add(Global().getEmployeeId())
        
        arrOfKeys.add("modifiedDate")
        arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
        
        arrOfKeys.add("userName")
        arrOfValues.add(Global().getUserName())
        
        arrOfKeys.add("isFungiObserved")
        arrOfValues.add((btn_FungiObserved.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        arrOfKeys.add("isCorrectiveAction")
        arrOfValues.add((btn_CorrectiveAction.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        arrOfKeys.add("isDamage")
        arrOfValues.add((btn_DamageFromWood.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        
        arrOfKeys.add("isEvidenceOfMoisture")
        arrOfValues.add((btn_EvidenceOfMoisture.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isEvidenceOfPresence")
        arrOfValues.add((btn_ThereIsEvidence.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        
        
        arrOfKeys.add("isNoVisibleEvidence")
        arrOfValues.add((btn_NoVisible.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        arrOfKeys.add("isVisibleEvidence")
        arrOfValues.add((btn_Visible.currentImage == UIImage(named: "NPMA_Check")) ? true : false)
        
        
        arrOfKeys.add("correctiveActionDescriptionAndLocation")
        arrOfValues.add("\(txt_CorrectiveAction_Description.text!)")
        arrOfKeys.add("damageDescriptionAndLocation")
        arrOfValues.add("\(txt_DamageFromWood_Description.text!)")
        
        arrOfKeys.add("evidenceOfPresenceDescription")
        arrOfValues.add("\(txt_ThereIsEvidence_Description.text!)")
        arrOfKeys.add("fungiObservedDescriptionAndLocation")
        arrOfValues.add("\(txt_FungiObserved_Description.text!)")
        
        
        let arrMoistureDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, predicate: (NSPredicate(format: "workorderId == %@", strWoId)))
        //For Update
        
        if(arrMoistureDetail.count != 0){
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            if isSuccess {
                updateServicePestModifyDate(strWoId: self.strWoId as String)
            }
        }
        //For ADD
        else{
            arrOfKeys.add("woNPMMoistureDetailId")
            arrOfValues.add("")
            saveDataInDB(strEntity: Entity_WoNPMAInspectionMoistureDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
        // Saving Disclaimer In DB
        
        
    }
    
    
    
    
    
    //MARK: ----Findings-----
    
    func fetchFindings() {
        
        let sort = NSSortDescriptor(key: "subsectionCode", ascending: true)
        
        var arrFindingLocal = NSArray()
        
        if((btn_Resolved.currentImage == UIImage(named: "NPMA_Check")) && (btn_UnResolved.currentImage == UIImage(named: "NPMA_Check"))){
            
            arrFindingLocal = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ && isActive == YES", strWoId)), sort: sort)
            
        }else if((btn_Resolved.currentImage == UIImage(named: "NPMA_UnCheck")) && (btn_UnResolved.currentImage == UIImage(named: "NPMA_UnCheck"))){
            
            arrFindingLocal = NSArray()
            
        }else if(btn_Resolved.currentImage == UIImage(named: "NPMA_Check")){
            
            arrFindingLocal = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ && isActive == YES && isResolved == YES", strWoId)), sort: sort)
            
        }else if(btn_UnResolved.currentImage == UIImage(named: "NPMA_Check")){
            
            arrFindingLocal = getDataFromCoreDataBaseArraySorted(strEntity: Entity_ProblemIdentifications, predicate: (NSPredicate(format: "workOrderId == %@ && isActive == YES && isResolved == NO", strWoId)), sort: sort)
            
        }
        
        aryFinding = NSMutableArray()
        
        aryFinding.addObjects(from: arrFindingLocal as! [Any])
        
        tv_Finding.reloadData()
        
    }
    
    //MARK:
    //MARK: -------------Functions-------------
    
    func getClockStatus()
    {
        var strClockStatus = String()
        
        if !isInternetAvailable()
        {
            
            strClockStatus = Global().fetchClockInOutDetailCoreData()
            
            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
            {
                btnClock.setImage(UIImage(named: "white"), for: .normal)
            }
            else if strClockStatus == "WorkingTime"
            {
                btnClock.setImage(UIImage(named: "green"), for: .normal)
                
            }
            else if strClockStatus == "BreakTime"
            {
                btnClock.setImage(UIImage(named: "orange"), for: .normal)
            }
            else
            {
                btnClock.setImage(UIImage(named: "red"), for: .normal)
                
            }
        }
        else
        {
            DispatchQueue.global(qos: .background).async
                {
                    strClockStatus = Global().getCurrentTimerOfClock()
                    
                    DispatchQueue.main.async
                        {
                            if strClockStatus == "" || strClockStatus.count == 0 || strClockStatus == "<null>"
                            {
                                self.btnClock.setImage(UIImage(named: "white"), for: .normal)
                            }
                            else if strClockStatus == "WorkingTime"
                            {
                                self.btnClock.setImage(UIImage(named: "green"), for: .normal)
                                
                            }
                            else if strClockStatus == "BreakTime"
                            {
                                self.btnClock.setImage(UIImage(named: "orange"), for: .normal)
                            }
                            else
                            {
                                self.btnClock.setImage(UIImage(named: "red"), for: .normal)
                                
                            }
                            
                    }
            }
        }
        
    }
    
    func disableControls() {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            graphDrawingWebView.isUserInteractionEnabled =  false
            btnClock.isEnabled = false
            
        }else{
            
        }
        
    }
    
    func back() {
        
        //SaveAllDataInCoreDAta()
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func goToNPMA_AddFindingView(strToUpdate : String , strProblemIdentificationIdLocal : String , strMobileProblemIdentificationIdLocal : String)  {
        
        saveGraphAsXML()
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_AddFindings_iPadVC") as? NPMA_AddFindings_iPadVC
        testController?.strWoId = strWoId
        testController?.strToUpdate = strToUpdate
        testController?.strProblemIdentificationIdToUpdate = strProblemIdentificationIdLocal
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strMobileProblemIdentificationIdToUpdate = strMobileProblemIdentificationIdLocal
        
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    //MARK:
    //MARK: -------------Actions-------------
    @objc func indexChanged(_ sender: UISegmentedControl) {
        
        SaveAllDataInCoreDAta()
        ReloadViewOnScrollAccordingCondition(tag: sender.selectedSegmentIndex)
        scrollView.setContentOffset(.zero, animated: true)
        
    }
    
    @IBAction func action_Back(_ sender: Any) {
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            // Not To Edit
            
        }else{
            
            saveGraphAsXML()
            
        }
        
        //self.dismiss(animated: false, completion: nil)
        self.back()
        
    }
    // MARK: ---------------------------------------showGraphLegend------------------------------------------

    func showGraphLegend() {
        //For graph Legend
      
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { [self] in
            
            LegendGraphView().CreatFormLegend(viewLegend: viewGraphLegend, controllor: self) { viewcontian  in
                           
                let heightScroll = (viewcontian.tag) + 50
                         
                           if getGraphLegendCategory().count != 0 {
                               self.heightGraphLegend.constant = 150.0
                             
                           }else{
                               self.heightGraphLegend.constant = 0.0
                           }
                           self.heightScrollGraphLegend.constant = CGFloat(heightScroll)
                           
                          
                       }
            
            
            
        
        })
    }
    //MARK:
    //MARK: -------------Reload View-AND Other Function-------------
    
    func UIInitialization()  {
        
        buttonRound(sender: btnFooterPropsal)
        buttonRound(sender: btnFooterFinalize)

        //GeneralDes--
        self.height_view_VentNecessary.constant = 0.0
        self.txtExtraMaterialNecessary.layer.cornerRadius = 4.0
        self.txtExtraMaterialNecessary.layer.borderWidth = 1.0
        self.txtExtraMaterialNecessary.layer.borderColor = UIColor.lightGray.cgColor
        
        txtBlockEqual.delegate = self
        txtApproxSqft.delegate = self
        txtLinerft.delegate = self
        txtOfStories.delegate = self
        txtOrignalTreatmentDate.delegate = self
        txtInspectedBy.delegate = self
        txtInspectedBDate.delegate = self
        txtStructureType.delegate = self
        txtWorkCompletedBy.delegate = self
        txtWorkCompletedDate.delegate = self
       // txtSlab.delegate = self
        txtCrawlDoorHeight.delegate = self
        txtCrawlSpaceHeight.delegate = self
//        txtBrickVeneer.delegate = self
//        txtHollowBlock.delegate = self
//        txtStoneVeneer.delegate = self
//        txtPorches.delegate = self
//        txtGarege.delegate = self
        txtMoistureMeterReading.delegate = self
        txtVents.delegate = self
        txtType.delegate = self
        txtColor.delegate = self
        txtSectionInstalled.delegate = self
        txtTypeOfSystem.delegate = self
        txtExtraMaterialNecessary.delegate = self
        
        //Finding--
        self.tv_Finding.layer.cornerRadius = 10.0
        self.tv_Finding.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_Finding.layer.borderWidth = 1.0
        self.tv_Finding.layer.masksToBounds = true
        self.tv_Finding.tableFooterView = UIView()
        
        
        
        //OtherDetail--
        self.tv_OtherDetails_ConduciveConditions.layer.cornerRadius = 10.0
        self.tv_OtherDetails_ConduciveConditions.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_OtherDetails_ConduciveConditions.layer.borderWidth = 1.0
        self.tv_OtherDetails_ConduciveConditions.layer.masksToBounds = true
        self.tv_OtherDetails_ConduciveConditions.tableFooterView = UIView()
        self.tv_OtherDetails_Locations.layer.cornerRadius = 10.0
        self.tv_OtherDetails_Locations.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_OtherDetails_Locations.layer.borderWidth = 1.0
        self.tv_OtherDetails_Locations.layer.masksToBounds = true
        self.tv_OtherDetails_Locations.tableFooterView = UIView()
        
        if(nsud.value(forKey: "MasterServiceAutomation") != nil){
            let dict = nsud.value(forKey: "MasterServiceAutomation")as! NSDictionary
            if(dict.value(forKey: "ConstructionTypeExtSerDcs") != nil){
                
                if dict.value(forKey: "ConstructionTypeExtSerDcs") is NSArray {
                    
                    aryConstructionTypeOption = NSMutableArray()
                    aryConstructionTypeOption = (dict.value(forKey: "ConstructionTypeExtSerDcs") as! NSArray).mutableCopy() as! NSMutableArray
                    
                }

            }
            
            if (dict.value(forKey: "FoundationTypeExtSerDcs") != nil){
                
                if dict.value(forKey: "FoundationTypeExtSerDcs") is NSArray {
                    
                    aryFoundationTypeOption = NSMutableArray()
                    aryFoundationTypeOption = (dict.value(forKey: "FoundationTypeExtSerDcs") as! NSArray).mutableCopy() as! NSMutableArray
                    
                }

            }
            
            if (dict.value(forKey: "AreaMaster") != nil){
                
                if dict.value(forKey: "AreaMaster") is NSArray {
                    
                    aryOtherDetails_LocationsType = NSMutableArray()
                    aryOtherDetails_LocationsType = (dict.value(forKey: "AreaMaster") as! NSArray).mutableCopy() as! NSMutableArray
                    
                }

            }
            
        }
      
        txt_OtherDetails_Repair.delegate = self
        txt_OtherDetails_LiveInsects.delegate = self
        txt_OtherDetails_DeadInsects.delegate = self
        txt_OtherDetails_VisibleDamage.delegate = self
        txt_OtherDetails_NoTreatment.delegate = self
        txt_OtherDetails_ReCommendedTreatment.delegate = self
        txt_OtherDetails_ListOfLocationInfestation.delegate = self
        txt_OtherDetails_ListOfLocationProbableHiddenDamage.delegate = self
        txt_OtherDetails_Location.delegate = self
        txt_OtherDetails_Description.delegate = self
        
        //MoistureDetail--
        txt_FungiObserved_Description.delegate = self
        txt_DamageFromWood_Description.delegate = self
        txt_CorrectiveAction_Description.delegate = self
        txt_ThereIsEvidence_Description.delegate = self
        
        
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            viewInspectionDetail.isUserInteractionEnabled = false
            viewGraphView.isUserInteractionEnabled = false
            viewFindings.isUserInteractionEnabled = false
            viewOtherDetails.isUserInteractionEnabled = false
            viewMoistureDetails.isUserInteractionEnabled = false

        }else{
            
            viewInspectionDetail.isUserInteractionEnabled = true
            viewGraphView.isUserInteractionEnabled = true
            viewFindings.isUserInteractionEnabled = true
            viewOtherDetails.isUserInteractionEnabled = true
            viewMoistureDetails.isUserInteractionEnabled = true

        }
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            //self.setTopMenuOption()
            self.ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)

        })
    }
    
    
    func ReloadViewOnScrollAccordingCondition(tag:Int) {
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        for item in view_ConstructionType.subviews {
            if(item is UIButton) {
                item.removeFromSuperview()
            }
        }
        for item in view_FoundationType.subviews {
            if(item is UIButton) {
                item.removeFromSuperview()
            }
        }
        if(tag == 0){ // General
            self.fetchGeneralDesc()
            //Creat ConstructionTypeOption
            var yPossition = 60 , xPossition = 0
            let widthBtn = self.view.frame.width/3 - 50
            
            for (index, element) in aryConstructionTypeOption.enumerated() {
                let dict = (element as AnyObject)as! NSDictionary
                print(index%3)
                
                
                if(index == 0){
                    xPossition = 0
                }else{
                    xPossition = xPossition + Int(widthBtn) + 12
                }
                if(index%3 == 0 && index != 0){
                    yPossition = yPossition + 60
                    xPossition = 0
                }
                
                
                let btnConstructionType = UIButton(frame: CGRect(x: xPossition, y: yPossition, width: Int(widthBtn), height: 50))
                btnConstructionType.setTitle("\(dict.value(forKey: "ConstructionTypeName")!)", for: .normal)
                btnConstructionType.backgroundColor = .white
                btnConstructionType.setTitleColor(UIColor.black, for: .normal)
                btnConstructionType.titleLabel?.font = UIFont.systemFont(ofSize: 20)
                btnConstructionType.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                btnConstructionType.titleEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
                
                btnConstructionType.tag = index
                btnConstructionType.addTarget(self, action: #selector(self.actionOnConstructionType(sender:)), for: .touchUpInside)
                
                let strSysName = "\(dict.value(forKey: "ConstructionTypeSysName")!)"
                if(arySelectedConstructionTypeOption.contains(strSysName)){
                    btnConstructionType.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }else{
                    btnConstructionType.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                }
                
                self.height_view_ConstructionType.constant = CGFloat(yPossition + 50)
                self.view_ConstructionType.addSubview(btnConstructionType)
            }
            
            //Creat FoundationType
            var yFoundationTypePossition = 60 , xFoundationTypePossition = 0
            let widthFoundationTypeBtn = self.view.frame.width/3 - 54
            
            for (index, element) in aryFoundationTypeOption.enumerated() {
                let dict = (element as AnyObject)as! NSDictionary
                
                
                if(index == 0){
                    xFoundationTypePossition = 0
                }else{
                    xFoundationTypePossition = xFoundationTypePossition + Int(widthBtn) + 12
                }
                if(index%3 == 0 && index != 0){
                    yFoundationTypePossition = yFoundationTypePossition + 60
                    xFoundationTypePossition = 0
                }
                let btnFoundationType = UIButton(frame: CGRect(x: xFoundationTypePossition, y: yFoundationTypePossition, width: Int(widthFoundationTypeBtn), height: 50))
                btnFoundationType.setTitle("\(dict.value(forKey: "FoundationTypeName")!)", for: .normal)
                btnFoundationType.backgroundColor = .white
                btnFoundationType.setTitleColor(UIColor.black, for: .normal)
                btnFoundationType.titleLabel?.font = UIFont.systemFont(ofSize: 20)
                
                btnFoundationType.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                btnFoundationType.titleEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
                
                
                btnFoundationType.tag = index
                btnFoundationType.addTarget(self, action: #selector(self.actionOnFoundationType(sender:)), for: .touchUpInside)
                let strSysName = "\(dict.value(forKey: "FoundationTypeSysName")!)"
                if(arySelectedFoundationTypeOption.contains(strSysName)){
                    btnFoundationType.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                }else{
                    btnFoundationType.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                }
                self.height_view_FoundationType.constant = CGFloat(yFoundationTypePossition + 50)
                self.view_FoundationType.addSubview(btnFoundationType)
            }
            
            
            viewInspectionDetail.frame = CGRect(x: 15, y: 10, width: scrollView.frame.width - 30, height: 1900 + (self.height_view_FoundationType.constant + self.height_view_ConstructionType.constant))
            self.scrollView.addSubview(self.viewInspectionDetail)
            
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.viewInspectionDetail.frame.height)
            
            saveGraphAsXML()
            
        }
        else if(tag == 1){ // Graph
            
            self.view.isUserInteractionEnabled = false
//            viewGraphView.backgroundColor = .red
//            graphDrawingWebView.backgroundColor = .blue
            if UIDevice.current.orientation.isLandscape {
                viewGraphView.frame = CGRect(x: 15, y: 10, width: scrollView.frame.width - 60, height: UIScreen.main.bounds.width - 300)

            } else {
                viewGraphView.frame = CGRect(x: 15, y: 10, width: scrollView.frame.width - 30, height: UIScreen.main.bounds.height - 300)
            }
       
            
            loadWebGraphView()
            self.scrollView.addSubview(self.viewGraphView)
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.viewGraphView.frame.height)
          showGraphLegend()
            self.view.isUserInteractionEnabled = true

        }
        else if(tag == 2){ // Finding
            viewFindings.frame = CGRect(x: 15, y: 10, width: scrollView.frame.width - 30, height: scrollView.frame.height - 20)
            self.scrollView.addSubview(self.viewFindings)
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.scrollView.frame.height - 20)
            
            self.tv_Finding.reloadData()
            saveGraphAsXML()
            fetchFindings()
            
        }
        else if(tag == 3){ // Other Detail
            
            self.fetchOtherDetail()
            self.fetchOtherDetailObstruction()
            self.fetchOtherDetailConduciveConditions()
            
            
            self.Height_tv_OtherDetails_ConduciveConditions.constant = CGFloat(self.aryOtherDetails_ConduciveConditions.count * 100) + (self.aryOtherDetails_ConduciveConditions.count == 0 ? 150.0 : 75.0)
            
            self.Height_tv_OtherDetails_Locations.constant = CGFloat(self.aryOtherDetails_Locations.count * 100) + (self.aryOtherDetails_Locations.count == 0 ? 150.0 : 75.0)
            
            self.viewOtherDetails.frame = CGRect(x: 15, y: 10, width: scrollView.frame.width - 30, height: 1800 + self.Height_tv_OtherDetails_Locations.constant + self.Height_tv_OtherDetails_ConduciveConditions.constant)
            
            self.scrollView.addSubview(self.viewOtherDetails)
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.viewOtherDetails.frame.height)
            
            self.tv_OtherDetails_Locations.reloadData()
            self.tv_OtherDetails_ConduciveConditions.reloadData()
            
            saveGraphAsXML()
            
            
        }
        else if(tag == 4){
            //Moisture Detail
            self.fetchMoistureDetail()
            
            self.viewMoistureDetails.frame = CGRect(x: 15, y: 10, width: self.scrollView.frame.width - 30, height: self.viewMoistureDetails.frame.height + heightView_B_Section.constant)
            self.scrollView.addSubview(self.viewMoistureDetails)
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.viewMoistureDetails.frame.height + heightView_B_Section.constant)
            self.saveGraphAsXML()
        }
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    //MARK:
    //MARK: -------------ActionOnGeneral-----------
    @IBAction func action_CheckBOX(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.isEditGeneralDes = true
        
//        sender.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
//        })
        
        
        if(sender == btn_SlabYes || sender == btn_SlabNo){
            
            if(sender == btn_SlabYes){
                btn_SlabYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_SlabNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }else{
                btn_SlabNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_SlabYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            return
        }
        
        if(sender == btn_RemoveWoodYes || sender == btn_RemoveWoodNo){
            
            if(sender == btn_RemoveWoodYes){
                btn_RemoveWoodYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_RemoveWoodNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }else{
                btn_RemoveWoodNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_RemoveWoodYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            return
        }
        
        if(sender == btn_InstallMoistureBarrierYes || sender == btn_InstallMoistureBarrierNo){
            
            if(sender == btn_InstallMoistureBarrierYes){
                btn_InstallMoistureBarrierYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_InstallMoistureBarrierNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }else{
                btn_InstallMoistureBarrierNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_InstallMoistureBarrierYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            return
        }
        
        if(sender == btn_DryerVentedOutsideYes || sender == btn_DryerVentedOutsideNo){
            
            if(sender == btn_DryerVentedOutsideYes){
                btn_DryerVentedOutsideYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_DryerVentedOutsideNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }else{
                btn_DryerVentedOutsideNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_DryerVentedOutsideYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            }
            return
        }
        
        if(sender == btn_VentsNecessaryYes || sender == btn_VentsNecessaryNo){
            
            if(sender == btn_VentsNecessaryYes){
                btn_VentsNecessaryYes.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_VentsNecessaryNo.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                self.height_view_VentNecessary.constant = 55.0
            }else{
                btn_VentsNecessaryNo.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btn_VentsNecessaryYes.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                self.height_view_VentNecessary.constant = 0.0

                txtVents.text = ""
                txtType.text = ""
                txtColor.text = ""
                
            }
            return
        }
        
        //For Check Box
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
        
    }
    
    
    @IBAction func action_OnDate(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.isEditGeneralDes = true
        
        sender.tag = sender.tag
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var fromDate = Date()
        if(sender.tag == 1){
            fromDate = dateFormatter.date(from:((txtInspectedBDate.text!.count) > 0 ? txtInspectedBDate.text! : result))!
            
        }else if(sender.tag == 2){
            fromDate = dateFormatter.date(from:((txtWorkCompletedDate.text!.count) > 0 ? txtWorkCompletedDate.text! : result))!
            
        }
        else if(sender.tag == 3){
            fromDate = dateFormatter.date(from:((txtOrignalTreatmentDate.text!.count) > 0 ? txtOrignalTreatmentDate.text! : result))!
            
        }
        gotoDatePickerView(sender: sender, strType: "Date" , dateToSet: fromDate)
    }
    
    
    @objc func actionOnConstructionType(sender : UIButton) {
        self.isEditGeneralDes = true
        self.view.endEditing(true)
        
        let dict = aryConstructionTypeOption.object(at: sender.tag) as! NSDictionary
        let strSysName = "\(dict.value(forKey: "ConstructionTypeSysName")!)"
        if(arySelectedConstructionTypeOption.contains(strSysName)){
            arySelectedConstructionTypeOption.remove(strSysName)
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            
            arySelectedConstructionTypeOption.add(strSysName)
        }
    }
    @objc func actionOnFoundationType(sender : UIButton) {
        self.isEditGeneralDes = true
        self.view.endEditing(true)
        let dict = aryFoundationTypeOption.object(at: sender.tag) as! NSDictionary
        let strSysName = "\(dict.value(forKey: "FoundationTypeSysName")!)"
        if(arySelectedFoundationTypeOption.contains(strSysName)){
            arySelectedFoundationTypeOption.remove(strSysName)
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            
            arySelectedFoundationTypeOption.add(strSysName)
        }
    }
    //MARK:
    //MARK: -------------ActionOnGraph----------
    
    func loadWebGraphView() {
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"
        
        self.graphDrawingWebView.scrollView.bounces = false
        self.graphDrawingWebView.allowsBackForwardNavigationGestures = false
        
        //let xmlFileName = (self.strWoId as String) + ".xml"
        var isXmlFileExists = false
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
            
            isXmlFileExists = checkIfImageExistAtPath(strFileName: xmlFileName)
            
            
        }
        
        let bundle = Bundle.main
        
        let filename = "index"
        let fileExtension = "html"
        let directory = "graphing"
        
        let isGraphDebugEnabled = nsud.bool(forKey: "graphDebug")
        
        if isGraphDebugEnabled {
            
            // general catermite
            
            let url = URL(string: "https://graphing.z30.web.core.windows.net/index.html?selectedCategory=catermite")!
            
            //https://graphing.z30.web.core.windows.net/index.html
            //
            
            self.graphDrawingWebView.load(URLRequest(url: url))
            
        } else {
            
            let indexUrl = bundle.url(forResource: filename, withExtension: fileExtension, subdirectory: directory)
            
            let fullUrl = URL(string: "?selectedCategory=catermite", relativeTo: indexUrl)
            let request = URLRequest(url: fullUrl!)
            self.graphDrawingWebView.load(request)
            
        }
        
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"
        
        //self.graphDrawingWebView.scrollView.isScrollEnabled = false
        
        self.view.layoutIfNeeded()
        
        if isXmlFileExists {
            
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
            
            self.loadSavedXMLGraph()
            
        }else{
            
            //RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
            
            //loadGraphCategory()
            
        }
        
    }
    
    func loadSavedXMLGraph() {
        
        do {
            
            var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
            
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
                
            }
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
                
            }
            
            //loadGraphCategory()
            
            if arrayOfImagesXML.count > 0 {
                
                let dictData = arrayOfImagesXML[0] as! NSManagedObject
                
                let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
                
                let path = (getDirectoryPath() as NSString).appendingPathComponent(xmlFileName)
                
                let xmlContents = try String(contentsOfFile: path)
                
                graphXmlGlobalIfExist = xmlContents
                
                print(xmlContents)
                
                let xmlContentEdited = "'\(xmlContents.trimmingCharacters(in: .whitespacesAndNewlines))'"

                print(xmlContentEdited)
                
                let fileName = "'\(xmlFileName)'"
                
                print("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")
                
                graphDrawingWebView.evaluateJavaScript("window.graphEditor.openGraphXML(\(xmlContentEdited), \(fileName))")  { (result, error) in
                    guard error == nil else {
                        print("there was an error")
                        print(error!.localizedDescription)
                        print(error!)
                        return
                    }
                    
                }
                
            }
            
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func saveGraphAsXML() {
        
        graphDrawingWebView.evaluateJavaScript("window.graphEditor.saveGraphXML()", completionHandler: { (results, error) in
            
            //            print(results ?? "results")
            //            print("Results")
            
            if results != nil {
                
                //Update Modify Date In Sales Oppo DB
                Global().updateSalesModifydate(self.strWoLeadId as String)
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
                
            }
            //self.saveGraphToDocumentDirectoryAsXML(stringXML: results as! String)
            
        })
        
    }
    
    func loadSavedXMLGraphToCheckIfToUpdatedModifyDate() {
        
        do {
            
            var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
            
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
                
            }
            if arrayOfImagesXML.count == 0 {
                
                arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
                
            }
                        
            if arrayOfImagesXML.count > 0 {
                
                let dictData = arrayOfImagesXML[0] as! NSManagedObject
                
                let xmlFileName = "\(dictData.value(forKey: "graphXmlPath") ?? "")"
                
                let path = (getDirectoryPath() as NSString).appendingPathComponent(xmlFileName)
                
                let xmlContents = try String(contentsOfFile: path)
                
                graphXmlGlobalIfExistNew = xmlContents
                
                if graphXmlGlobalIfExistNew == graphXmlGlobalIfExist {
                    
                }else{
                    
                    //Update Modify Date In Sales DB
                    Global().updateSalesModifydate(strWoLeadId)

                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }
                                                
            }
            
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func saveGraphToDocumentDirectoryAsXML(stringXML : String) {
        
        let str = stringXML
        
        let xmlFileName = "\\Documents\\GraphXMLFiles\\XML"  + "Graph" + "\(strWoId)\(getUniqueValueForId())" + ".xml"
        
        saveGraphXmlToImageDetail(graphXmlPath: xmlFileName)
        
        let filename = getDocumentsDirectory().appendingPathComponent(xmlFileName)
        
        do {
            try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
        
        //loadSavedXMLGraphToCheckIfToUpdatedModifyDate()
        
    }
    
    func saveGraphXmlToImageDetail(graphXmlPath : String) {
        
        deleteMainGraphImageBeforeSaving()
        
        let strImageName = "\\Documents\\UploadImages\\Img"  + "Graph" + "\(strWoId)\(getUniqueValueForId())" + ".jpeg"
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("createdBy")
        arrOfKeys.add("createdDate")
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("woImagePath")
        arrOfKeys.add("woImageType")
        arrOfKeys.add("modifiedBy")
        arrOfKeys.add("woImageId")
        arrOfKeys.add("modifiedDate")
        arrOfKeys.add("workorderId")
        arrOfKeys.add("imageCaption")
        arrOfKeys.add("imageDescription")
        arrOfKeys.add("latitude")
        arrOfKeys.add("longitude")
        arrOfKeys.add("isProblemIdentifaction")
        arrOfKeys.add("graphXmlPath")
        
        
        let defsLogindDetail = UserDefaults.standard
        
        let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
        
        var strCompanyKey = String()
        var strUserName = String()
        var strEmployeeid = String() // EmployeeId
        
        if let value = dictLoginData.value(forKeyPath: "Company.CompanyKey") {
            
            strCompanyKey = "\(value)"
        }
        
        if let value = dictLoginData.value(forKeyPath: "Company.Username") {
            
            strUserName = "\(value)"
        }
        
        if let value = dictLoginData.value(forKey: "EmployeeId") {
            
            strEmployeeid = "\(value)"
            
        }
        
        let coordinate = Global().getLocation()
        let latitude = "\(coordinate.latitude)"
        let longitude = "\(coordinate.longitude)"
        
        let woImageId = "Mobile" + "\(strWoId)" + getUniqueValueForId()
        let modifiedDate =  Global().modifyDate() //"\(Global().modifyDate)"
        
        
        arrOfValues.add("") //createdBy
        arrOfValues.add("") //createdDate
        arrOfValues.add(strCompanyKey)
        arrOfValues.add(strUserName)
        arrOfValues.add(strImageName)
        arrOfValues.add("Graph")
        arrOfValues.add(strEmployeeid)
        arrOfValues.add(woImageId) // woImageId
        arrOfValues.add(modifiedDate ?? "")
        arrOfValues.add(strWoId)
        arrOfValues.add("") // imageCaption
        arrOfValues.add("") // imageDescription
        arrOfValues.add(latitude) // latitude
        arrOfValues.add(longitude) //  longitude
        arrOfValues.add("true") //  isProblemIdentifaction
        arrOfValues.add(graphXmlPath) //  isProblemIdentifaction
        
        // Fetch Image From WebView Graph
        
        /*graphDrawingWebView.evaluateJavaScript("window.graphEditor.exportImage()", completionHandler: { (results, error) in
            
            //print(results ?? "results")
            //print("Results")
            
            self.getImageFromXMl(strImageName: strImageName)
            
            if results != nil {
                
                
            }
            
        })*/
        
        self.getImageFromXMl(strImageName: strImageName)

        
        saveDataInDB(strEntity: "ImageDetailsServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        // Save Main Graph to service auto Also
        
        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
        
        let arrOfKeysSales = NSMutableArray()
        let arrOfValuesSales = NSMutableArray()
        
        arrOfKeysSales.add("createdBy")
        arrOfKeysSales.add("createdDate")
        arrOfKeysSales.add("companyKey")
        arrOfKeysSales.add("userName")
        arrOfKeysSales.add("leadImagePath")
        arrOfKeysSales.add("leadImageType")
        arrOfKeysSales.add("modifiedBy")
        arrOfKeysSales.add("leadImageId")
        arrOfKeysSales.add("modifiedDate")
        arrOfKeysSales.add("leadId")
        arrOfKeysSales.add("leadImageCaption")
        arrOfKeysSales.add("descriptionImageDetail")
        arrOfKeysSales.add("latitude")
        arrOfKeysSales.add("longitude")
        
        arrOfValuesSales.add("") //createdBy
        arrOfValuesSales.add("") //createdDate
        arrOfValuesSales.add(strCompanyKey)
        arrOfValuesSales.add(strUserName)
        arrOfValuesSales.add(strImageNameSales)
        arrOfValuesSales.add("Graph")
        arrOfValuesSales.add(strEmployeeid)
        arrOfValuesSales.add(woImageId) // woImageId
        arrOfValuesSales.add(modifiedDate ?? "")
        arrOfValuesSales.add(strWoLeadId)
        arrOfValuesSales.add("") // imageCaption
        arrOfValuesSales.add("") // imageDescription
        arrOfValuesSales.add(latitude) // latitude
        arrOfValuesSales.add(longitude) //  longitude
        
        saveDataInDB(strEntity: "ImageDetail", arrayOfKey: arrOfKeysSales, arrayOfValue: arrOfValuesSales)
        
    }
    
    func getImageFromXMl(strImageName : String) {
        
        self.graphDrawingWebView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36(KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36)"
        
        self.graphDrawingWebView.evaluateJavaScript("window.graphEditor.convertToImage(\(false))", completionHandler: { (results, error) in
            
            //print(results ?? "results")
            //print("Results")
            
            if results != nil {
                
                var strBase64Img = String()
                
                strBase64Img = results as! String
                
                if strBase64Img == "Not able to save image." {
                    
                    //
                    
                }else{
                    
                    strBase64Img = strBase64Img.replacingOccurrences(of: "data:image/jpeg;base64,", with: "")
                    
                    if strBase64Img == "/9j/4AAQSkZJRgABAQAASABIAAD/4QBMRXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAAaADAAQAAAABAAAAAQAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgAAQABAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAgICAgICAwICAwUDAwMFBgUFBQUGCAYGBgYGCAoICAgICAgKCgoKCgoKCgwMDAwMDA4ODg4ODw8PDw8PDw8PD//bAEMBAgICBAQEBwQEBxALCQsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEP/dAAQAAf/aAAwDAQACEQMRAD8A+wKKKK/rQ/mM/9k=" {
                        
                        // No Image Added for Graph so delete Image Detail Obj from DB
                        
                        self.deleteMainGraphImageBeforeSaving()
                        
                    }else{
                        
                        let dataDecoded:NSData = NSData(base64Encoded: strBase64Img, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                        
                        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
                        
                        //let imageResized = Global().resizeImageGloballl(decodedimage)
                        
                        saveImageDocumentDirectory(strFileName: strImageName, image: decodedimage)
                        
                        // Save Image To Sales Auto Also
                        
                        let strImageNameSales = strImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
                        
                        saveImageDocumentDirectory(strFileName: strImageNameSales, image: decodedimage)
                        
                    }
                    
                }
                
            }
            
        })
        
    }
    
    func deleteMainGraphImageBeforeSaving() {
        
        var arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "true"))
        
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "True"))
            
        }
        if arrayOfImagesXML.count == 0 {
            
            arrayOfImagesXML = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@ && isProblemIdentifaction == %@", strWoId, "Graph", "1"))
            
        }
        
        if arrayOfImagesXML.count > 0 {
            
            let dictData = arrayOfImagesXML[0] as! NSManagedObject
            
            let xmlImageName = "\(dictData.value(forKey: "woImagePath") ?? "")"
            
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"true"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"True"))
            deleteAllRecordsFromDB(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && isProblemIdentifaction == %@", self.strWoId,"1"))
            
            Global().removeImage(fromDocumentDirectory: xmlImageName)
            
            let strImageNameSales = xmlImageName.replacingOccurrences(of: "\\Documents\\UploadImages\\", with: "")
            
            Global().removeImage(fromDocumentDirectory: strImageNameSales)
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strImageNameSales))
            
            // jugad for deleting sales image if synced from web
            
            let strNameTemp = "UploadImages\\" + strImageNameSales
            
            deleteAllRecordsFromDB(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strWoLeadId,strNameTemp))
            
        }
        
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
        
    }
    
    
    //MARK:
    //MARK: -------------ActionOnFinding----------
    @IBAction func action_AddNew(_ sender: UIButton) {
        self.view.endEditing(true)
        
        SaveAllDataInCoreDAta()

        goToNPMA_AddFindingView(strToUpdate: "No", strProblemIdentificationIdLocal: "", strMobileProblemIdentificationIdLocal: "")
        
    }
    @IBAction func action_Resolved(_ sender: UIButton) {
        self.view.endEditing(true)
//        sender.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
//        })
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
        
        fetchFindings()
        
    }
    @IBAction func action_Unresloved(_ sender: UIButton) {
        self.view.endEditing(true)
//        sender.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
//        })
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
        
        fetchFindings()
        
    }
    //MARK:
    //MARK: -------------MoistureDetail-----------
    @IBAction func action_CheckBOX_MoistureDetail(_ sender: UIButton) {
        self.view.endEditing(true)
        self.isEditMoistureDetail = true
        
//        sender.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
//        })
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            if(sender == btn_Visible){
                heightView_B_Section.constant = 0.0
            }
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            if(sender == btn_Visible){
                heightView_B_Section.constant = 300.0
            }
        }
    }
    //MARK:
    //MARK: -------------OtherDetail-----------
    @IBAction func action_CheckBOX_OtherDetail(_ sender: UIButton) {
        self.view.endEditing(true)
        self.isEditOtherDetail = true
//        sender.transform = CGAffineTransform.init(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4, animations: { () -> Void in
//            sender.transform = CGAffineTransform.init(scaleX: 1, y: 1)
//        })
        
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
    }
    @IBAction func action_AddNewConditionList(_ sender: UIButton) {
        self.view.endEditing(true)
        self.isEditOtherDetail = true
        
        SaveAllDataInCoreDAta()

        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "NPMA" : "NPMA", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPMA_AddConduciveConditionsVC") as? NPMA_AddConduciveConditionsVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        vc!.strWoId = self.strWoId
        vc!.delegate = self
        vc!.objWorkorderDetail = objWorkorderDetail
        self.present(vc!, animated: false, completion: nil)
    }
    @IBAction func action_AddLocationType(_ sender: UIButton) {
        self.view.endEditing(true)
        self.isEditOtherDetail = true
        
        
        if(aryOtherDetails_LocationsType.count != 0){
            sender.tag = 502
            openTableViewPopUp(tag: 502, ary: aryOtherDetails_LocationsType , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    
    @IBAction func action_AddNewObstructions(_ sender: UIButton) {
        self.isEditOtherDetail = true
        
        if(txt_OtherDetails_Location.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Location is required!", viewcontrol: self)
        }else if(txt_OtherDetails_Description.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Location description is required!", viewcontrol: self)
            
        }else{
           
            if(sender.titleLabel?.text == "Update"){
                self.SaveOtherDetailObstruction(statusForAdd_Edit_Delete: 2, obj: aryOtherDetails_Locations.object(at: sender.tag)as! NSManagedObject)
                sender.setTitle("Add", for: .normal)
            }
            else{
                self.SaveOtherDetailObstruction(statusForAdd_Edit_Delete: 1, obj: NSManagedObject())
            }
            
        }
    }
}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NPMA_Inspection_iPadVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tv_Finding){
            return aryFinding.count
        } else if(tableView == tv_OtherDetails_ConduciveConditions){
            return aryOtherDetails_ConduciveConditions.count
        }else if(tableView == tv_OtherDetails_Locations){
            return aryOtherDetails_Locations.count
        }
        return  0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == tv_Finding){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "IncepectionCellFinding" : "IncepectionCellFinding", for: indexPath as IndexPath) as! IncepectionCell
            cell.finding_btn_Resolved.tag = indexPath.row
            cell.finding_btn_Resolved.addTarget(self, action: #selector(actionFindingResolved), for: .touchUpInside)
            
            let objFinding = aryFinding[indexPath.row] as! NSManagedObject
            
            cell.finding_lbl_FindingCode.text = "\(objFinding.value(forKey: "subsectionCode") ?? "N/A")"
            cell.finding_lbl_Observation.text = "\(objFinding.value(forKey: "finding") ?? "N/A")"
            cell.finding_txtView_Observation.attributedText = htmlAttributedString(strHtmlString: "\(objFinding.value(forKey: "finding") ?? "")")
            cell.finding_lbl_CreatedDate.text = Global().convertDate("\(objFinding.value(forKey: "createdDate") ?? "")")
            
            if objFinding.value(forKey: "isResolved") is Bool {
                
                if objFinding.value(forKey: "isResolved") as! Bool == true {
                    
                    cell.finding_btn_Resolved.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                    cell.finding_lbl_ResolvedDate.text = Global().convertDate("\(objFinding.value(forKey: "resolvedDate") ?? "")")
                    
                } else {
                    
                    cell.finding_btn_Resolved.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                    cell.finding_lbl_ResolvedDate.text = ""
                    
                }
                
            } else {
                
                cell.finding_btn_Resolved.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                cell.finding_lbl_ResolvedDate.text = ""
                
            }
            
            return cell
        }
        
        else if(tableView == tv_OtherDetails_ConduciveConditions){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OtherDetails_ConduciveConditions" : "OtherDetails_ConduciveConditions", for: indexPath as IndexPath) as! IncepectionCell
            
            cell.Other_ConduciveConditions_Btn_Edit.tag = indexPath.row
            cell.Other_ConduciveConditions_Btn_Edit.addTarget(self, action: #selector(actionOther_ConduciveConditionsEdit(sender:)), for: .touchUpInside)
            
            cell.Other_ConduciveConditions_Btn_Delete.tag = indexPath.row
            cell.Other_ConduciveConditions_Btn_Delete.addTarget(self, action: #selector(actionOther_ConduciveConditionsDelete(sender:)), for: .touchUpInside)
            
            let dict = aryOtherDetails_ConduciveConditions.object(at: indexPath.row)as! NSManagedObject
            cell.Other_ConduciveConditions_lbl_PArea.text = "\(dict.value(forKey: "parentAreaName")!)"

           var  conduciveConditionId = "\(dict.value(forKey: "conduciveConditionId")!)"
            let arrayconduciveCondition = getDataFromServiceAutoMasters(strTypeOfMaster: "conduciveConditionMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
            if(arrayconduciveCondition.count != 0){
                let  array = arrayconduciveCondition.filter { (activity) -> Bool in
                    return "\((activity as! NSDictionary).value(forKey: "ConditionId")!)".contains("\(conduciveConditionId)")}
                if(array.count == 0){
                    cell.Other_ConduciveConditions_lbl_Condition.text = ""
                } else{
                    let dict = array[0]as! NSDictionary
                    cell.Other_ConduciveConditions_lbl_Condition.text = "\(dict.value(forKey: "ConditionName")!)"
                }
            }
            
            cell.Other_ConduciveConditions_lbl_Detail.text = "\(dict.value(forKey: "conduciveConditiondDetail")!)"
            
            return cell
        }
        else if(tableView == tv_OtherDetails_Locations){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OtherDetails_Locations" : "OtherDetails_Locations", for: indexPath as IndexPath) as! IncepectionCell
            cell.Other_Location_btn_Delete.tag = indexPath.row
            cell.Other_Location_btn_Delete.addTarget(self, action: #selector(actionOther_LocationDelete(sender:)), for: .touchUpInside)
            cell.Other_Location_btn_Edit.tag = indexPath.row
            cell.Other_Location_btn_Edit.addTarget(self, action: #selector(actionOther_LocationEdit(sender:)), for: .touchUpInside)
            let dict = aryOtherDetails_Locations.object(at: indexPath.row)as! NSManagedObject
            cell.Other_Location_lbl_Name.text = "\(dict.value(forKey: "locations")!)"
            cell.Other_Location_lbl_Description.text = "\(dict.value(forKey: "inspectionObstructionDescription")!)"
            return cell
        }
        
        else{
            let cell = tv_Finding.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "IncepectionCellFinding" : "IncepectionCellFinding", for: indexPath as IndexPath) as! IncepectionCell
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Please (+)Add New"
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textAlignment = .center
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = UIColor.lightGray
        (view as? UITableViewHeaderFooterView)?.backgroundColor = UIColor.white
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(tableView == tv_Finding){
            return aryFinding.count == 0 ? 80 : 0
        }
        else if(tableView == tv_OtherDetails_ConduciveConditions){
            return aryOtherDetails_ConduciveConditions.count == 0 ? 80 : 0
        }
        else if(tableView == tv_OtherDetails_Locations){
            return aryOtherDetails_Locations.count == 0 ? 80 : 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if(tableView == tv_Finding){
            return true
        }else{
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
            alert.view.tintColor = UIColor.black
            
            
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                
                let objArea = self.aryFinding[indexPath.row] as! NSManagedObject
                let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
                let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("isActive")
                arrOfValues.add(false)
                
                var isSuccess = false
                
                if strProblemIdentificationIdLocal.count > 0 {
                    
                    isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }else{
                    
                    isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", self.strWoId, strMobileProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
                //let isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }
                                
                /*var isSuccessPricing = false
                
                if strProblemIdentificationIdLocal.count > 0 {
                    
                    isSuccessPricing = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }else{
                    
                    isSuccessPricing = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentificationPricing, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strMobileProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                }
                
                if isSuccessPricing {
                    
                    //Update Modify Date In Work Order DB
                    updateServicePestModifyDate(strWoId: self.strWoId as String)
                    
                }*/
                
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
                
                // Delete Target and Target Images
                
                //if strProblemIdentificationIdLocal.count > 0 {
                
                let arrayOfTarget1 = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strProblemIdentificationIdLocal))
                
                for k in 0 ..< arrayOfTarget1.count {
                    
                    let dictData = arrayOfTarget1[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strProblemIdentificationIdLocal))
                
                //}else{
                
                let arrayOfTarget = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strMobileProblemIdentificationIdLocal))
                
                for k in 0 ..< arrayOfTarget.count {
                    
                    let dictData = arrayOfTarget[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strMobileProblemIdentificationIdLocal))
                
                self.fetchFindings()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = self.view.bounds
                popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
            }
            self.present(alert, animated: true)
        }
        
        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
            print("index path of edit: \(indexPath)")
            let objArea = self.aryFinding[indexPath.row] as! NSManagedObject
            let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
            let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
            
            self.goToNPMA_AddFindingView(strToUpdate: "Yes", strProblemIdentificationIdLocal: strProblemIdentificationIdLocal,strMobileProblemIdentificationIdLocal: strMobileProblemIdentificationIdLocal)
            completionHandler(true)
        }
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: -
    // MARK: ----------FindingCell Action---------
    @objc func actionFindingResolved(sender : UIButton) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: Alert, message: AlertChangeReolveStatusMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        
        alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
            
            let objArea = self.aryFinding[sender.tag] as! NSManagedObject
            let strProblemIdentificationIdLocal = "\(objArea.value(forKey: "problemIdentificationId") ?? "")"
            let strMobileProblemIdentificationIdLocal = "\(objArea.value(forKey: "mobileId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isResolved")
            arrOfKeys.add("resolvedDate")
            
            if(sender.currentImage == UIImage(named: "NPMA_Check")){
                arrOfValues.add(false)
                arrOfValues.add("")
            }else{
                arrOfValues.add(true)
                arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy HH:mm:ss", ""))
            }
            
            var isSuccess = false
            
            if strProblemIdentificationIdLocal.count > 0 {
                
                isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }else{
                
                isSuccess = getDataFromDbToUpdate(strEntity: Entity_ProblemIdentifications, predicate: NSPredicate(format: "workOrderId == %@ && mobileId == %@", self.strWoId, strMobileProblemIdentificationIdLocal), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
            }
                        
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            // If Reolved SO delete Target and target images from DB else Add in DB
            
            if(sender.currentImage == UIImage(named: "NPMA_Check")){
                
                // Un Resolved hai Add Krna padega
                
                // Add Target and images here
                
                WebService().saveTargetForProblem(strMobileTargetId: Global().getReferenceNumber(), strWdoProblemIdentificationId: "\(objArea.value(forKey: "ProblemIdentificationId") ?? "")", strWoLeadId: self.strWoLeadId, strIssueCode: "\(objArea.value(forKey: "IssuesCode") ?? "")", strFinding: "\(objArea.value(forKey: "Finding") ?? "")", strFilling_IssueCode: "\(objArea.value(forKey: "FindingFillIn") ?? "")")
                
                // Adding target Images
                
                var arrOfFindingImages = NSArray()
                
                if strProblemIdentificationIdLocal.count > 0 {
                    
                    arrOfFindingImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && problemIdentificationId == %@", self.strWoId, strProblemIdentificationIdLocal))
                    
                }else{
                    
                    arrOfFindingImages = getDataFromCoreDataBaseArray(strEntity: Entity_ProblemImageDetails, predicate: NSPredicate(format: "workorderId == %@ && mobileId == %@", self.strWoId, strMobileProblemIdentificationIdLocal))
                    
                }
                
                let arrOfProblemAndTargetImages = NSMutableArray()
                
                if arrOfFindingImages.count > 0 {
                    
                    for k in 0 ..< arrOfFindingImages.count {
                        
                        if arrOfFindingImages[k] is NSDictionary {
                            
                          let woImageIdLocalNew = "\(Global().getReferenceNumber() ?? "")"

                            let dictProblemImageDetail = arrOfFindingImages[k] as! NSDictionary
                            
                            // Save Target Images also
                            
                            var strLeadCommercialId = String()
                            var strLeadCommercialMobileId = String()
                            
                            let arrayOfTargetsLocal = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , "\(dictProblemImageDetail.value(forKey: "ProblemIdentificationId") ?? "")"))
                            
                            if arrayOfTargetsLocal.count > 0 {
                                
                                for k1 in 0 ..< arrayOfTargetsLocal.count {
                                    
                                    let dictData = arrayOfTargetsLocal[k1] as! NSManagedObject
                                    
                                    let id1 = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                                    let id2 = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                                    
                                    if id1.count > 0 {
                                        
                                        strLeadCommercialId = id1
                                        
                                    }else{
                                        
                                        strLeadCommercialMobileId = id2
                                        
                                    }
                                    
                                    break
                                    
                                }
                                
                            }

                            arrOfProblemAndTargetImages.add("\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")")

                            let strImageNameSales = "\(dictProblemImageDetail.value(forKey: "WoImagePath") ?? "")".replacingOccurrences(of: "\\Documents\\ProblemIdentificationImages\\", with: "")
                            
                            WebService().saveTargetImageProblem(strLeadCommercialTargetId: strLeadCommercialId, strMobileTargetId: strLeadCommercialMobileId, strImagePath: strImageNameSales, imageResized: UIImage(), woImageId: "\(dictProblemImageDetail.value(forKey: "WoImageId") ?? "")", strWoLeadId: self.strWoLeadId)
                            
                        }
                    }
                }
                
                // Download All Problem Images and Save in Document Directory arrOfWdoImagesToDownloadAndSaveInLead
                
                for k in 0 ..< arrOfProblemAndTargetImages.count {
                    
                    WebService().downloadProblemImage(from: arrOfProblemAndTargetImages[k] as! String)
                    
                }

                
            }else{
                
                //  Resolved hai delete krna hai
                
                // Delete Target and Target Images
                
                //if strProblemIdentificationIdLocal.count > 0 {
                
                let arrayOfTarget1 = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strProblemIdentificationIdLocal))
                
                for k in 0 ..< arrayOfTarget1.count {
                    
                    let dictData = arrayOfTarget1[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strProblemIdentificationIdLocal))
                
                //}else{
                
                let arrayOfTarget = getDataFromCoreDataBaseArray(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId , strMobileProblemIdentificationIdLocal))
                
                for k in 0 ..< arrayOfTarget.count {
                    
                    let dictData = arrayOfTarget[k] as! NSManagedObject
                    
                    let leadCommercialTargetId = "\(dictData.value(forKey: "leadCommercialTargetId") ?? "")"
                    let mobileTargetId = "\(dictData.value(forKey: "mobileTargetId") ?? "")"
                    
                    if leadCommercialTargetId.count > 0 {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && leadCommercialTargetId == %@", self.strWoLeadId , leadCommercialTargetId))
                        
                    }else {
                        
                        deleteAllRecordsFromDB(strEntity: Entity_TargetImageDetail, predicate: NSPredicate(format: "leadId == %@ && mobileTargetId == %@", self.strWoLeadId , mobileTargetId))
                        
                    }
                    
                }
                
                deleteAllRecordsFromDB(strEntity: Entity_LeadCommercialTargetExtDc, predicate: NSPredicate(format: "leadId == %@ && wdoProblemIdentificationId == %@", self.strWoLeadId,strMobileProblemIdentificationIdLocal))
                
            }
            
            self.fetchFindings()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)
        
    }
    // MARK: -
    // MARK: ----------Other detailCell Action---------
    
    @objc func actionOther_LocationEdit(sender : UIButton) {
        self.view.endEditing(true)
        let dict = aryOtherDetails_Locations.object(at: sender.tag)as! NSManagedObject
        txt_OtherDetails_Location.text = "\(dict.value(forKey: "locations")!)"
        txt_OtherDetails_Description.text = "\(dict.value(forKey: "inspectionObstructionDescription")!)"
        txt_OtherDetails_Location.tag = Int("\(dict.value(forKey: "areaId")!)")!
        self.btn_OtherDetails_AddLocation.setTitle("Update", for: .normal)
        self.btn_OtherDetails_AddLocation.tag = sender.tag
        
    }
    @objc func actionOther_LocationDelete(sender : UIButton) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { [self] action in
            
            self.SaveOtherDetailObstruction(statusForAdd_Edit_Delete: 3, obj: aryOtherDetails_Locations.object(at: sender.tag)as! NSManagedObject)
          
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)
        
    }
    
    @objc func actionOther_ConduciveConditionsEdit(sender : UIButton) {
        self.view.endEditing(true)
     
        let dict = aryOtherDetails_ConduciveConditions.object(at: sender.tag)as! NSManagedObject
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "NPMA" : "NPMA", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPMA_AddConduciveConditionsVC") as? NPMA_AddConduciveConditionsVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        vc!.strViewFor = "EDIT"
        vc!.strWoId = self.strWoId
        vc!.objWorkorderDetail = objWorkorderDetail
        vc!.dictData = dict
        vc!.delegate = self
        self.present(vc!, animated: false, completion: nil)
        
    }
    
    @objc func actionOther_ConduciveConditionsDelete(sender : UIButton) {
        self.view.endEditing(true)
  
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
        
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { [self] action in
            self.DeleteOtherDetailConduciveConditions(statusForAdd_Edit_Delete: 3, obj: aryOtherDetails_ConduciveConditions.object(at: sender.tag)as! NSManagedObject)
            self.ReloadViewOnScrollAccordingCondition(tag: self.segment.selectedSegmentIndex)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)
    }
    // MARK: -
    // MARK: ----------Header Button Action---------
    
    @IBAction func actionOnServiceDoc(_ sender: Any)
    {
        
        self.view.endEditing(true)
        
        goToServiceDocument()
        
    }
    
    @IBAction func actionOnClock(_ sender: Any)
    {

        self.view.endEditing(true)

        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        
    }
    
    // MARK: -
    // MARK: ----------Footer Button Action---------
    
    @IBAction func action_Proposal(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            goToConfigureProposalView(strForProposal: "Yes")
            
        }else{
            
            SaveAllDataInCoreDAta()
            goToConfigureProposalView(strForProposal: "Yes")
            
        }
        
    }
    
    @IBAction func action_Finalize(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            
            
        }else{
            
            SaveAllDataInCoreDAta()
            
        }
        
        goToFinalizeReport()

    }
    
    @IBAction func action_Image(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Before")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.goToNotesHistory()
        
    }
    
    //MARK: - -----------------------------------Footer Functions -----------------------------------
    
    func goToFinalizeReport() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "NPMA" : "NPMA", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPMA_FinlizedReportVC") as? NPMA_FinlizedReportVC
        vc?.strWoId = strWoId
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    
    func goToProposal() {
        
        let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
               let objNPMA_Proposal_iPadVC = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Proposal_iPadVC") as? NPMA_Proposal_iPadVC
        objNPMA_Proposal_iPadVC?.objWorkorderDetail = objWorkorderDetail
        self.navigationController?.pushViewController(objNPMA_Proposal_iPadVC!, animated: false)
        
    }
    
    func goToConfigureProposalView(strForProposal : String)  {
        
        if strWoLeadId.count > 0 && strWoLeadId != "0" {
            
            let storyboardIpad = UIStoryboard.init(name: "NPMA", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "NPMA_Proposal_iPadVC") as? NPMA_Proposal_iPadVC
            testController?.strWoId = strWoId
            testController?.strForProposal = strForProposal
            testController?.objWorkorderDetail = objWorkorderDetail
            self.navigationController?.pushViewController(testController!, animated: false)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Opportunity not available", viewcontrol: self)
            
        }
        
    }
    
    func goToServiceDocument() {
        
        SaveAllDataInCoreDAta()
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToServiceHistory()  {
        
        SaveAllDataInCoreDAta()
                
        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToNotesHistory()  {
        
        SaveAllDataInCoreDAta()
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        
        SaveAllDataInCoreDAta()
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType : String)  {
        
        SaveAllDataInCoreDAta()
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        self.present(testController!, animated: false, completion: nil)
        
    }
    
  
}
// MARK: -
// MARK: ----------IncepectionCell---------
class IncepectionCell: UITableViewCell {
    // Finding
    @IBOutlet weak var finding_btn_Resolved: UIButton!
    @IBOutlet weak var finding_lbl_FindingCode: UILabel!
    @IBOutlet weak var finding_lbl_Observation: UILabel!
    @IBOutlet weak var finding_txtView_Observation: UITextView!
    
    @IBOutlet weak var finding_lbl_CreatedDate: UILabel!
    @IBOutlet weak var finding_lbl_ResolvedDate: UILabel!
    
    //OtherDetails_ConduciveConditions
    @IBOutlet weak var Other_ConduciveConditions_lbl_Condition: UILabel!
    @IBOutlet weak var Other_ConduciveConditions_lbl_PArea: UILabel!
    @IBOutlet weak var Other_ConduciveConditions_lbl_Detail: UILabel!
    @IBOutlet weak var Other_ConduciveConditions_Btn_Edit: UIButton!
    @IBOutlet weak var Other_ConduciveConditions_Btn_Delete: UIButton!
    
    // Location
    @IBOutlet weak var Other_Location_lbl_Name: UILabel!
    @IBOutlet weak var Other_Location_lbl_Description: UILabel!
    @IBOutlet weak var Other_Location_btn_Edit: UIButton!
    @IBOutlet weak var Other_Location_btn_Delete: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: -
// MARK: ----------DatePickerProtocol---------
extension NPMA_Inspection_iPadVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        
        if(tag == 1)
        {
            txtInspectedBDate.text = strDate
        }
        if(tag == 2)
        {
            txtWorkCompletedDate.text = strDate
        }
        if(tag == 3)
        {
            txtOrignalTreatmentDate.text = strDate
        }
    }
}
// MARK: -
// MARK: ------- Selection CustomTableView---------
extension NPMA_Inspection_iPadVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 502)
        {
            if dictData.count == 0 {
                txt_OtherDetails_Location.text = ""
                txt_OtherDetails_Location.tag = 0
            }else{
                txt_OtherDetails_Location.text = "\(dictData.value(forKey: "AreaName")!)"
                txt_OtherDetails_Location.tag = Int("\(dictData.value(forKey: "AreaId")!)")!
            }
        }
        
        
    }
    
}

// MARK: - //  // // //
// MARK: ------- NPMA_UITextFieldDelegate---------

extension NPMA_Inspection_iPadVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(segment.selectedSegmentIndex == 0){
            self.isEditGeneralDes = true
        }
        if(segment.selectedSegmentIndex == 3){
            self.isEditOtherDetail = true
        }
        if(segment.selectedSegmentIndex == 4){
            self.isEditMoistureDetail = true
        }
        
        if (textField == txtOfStories)
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 100)
            
        }
        else if (textField == txtCrawlSpaceHeight || textField == txtCrawlDoorHeight)
        {
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 35)

        }
        
        else if ( textField == txtApproxSqft || textField == txtLinerft)
        {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 100)
            
        }
        else if ( textField == txtMoistureMeterReading)
        {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)

            return chk
            
        }
        return true
    }
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
// MARK: -
// MARK: ------- NPMA_UITextViewDelegate---------

extension NPMA_Inspection_iPadVC : UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(segment.selectedSegmentIndex == 0){
            self.isEditGeneralDes = true
        }
        if(segment.selectedSegmentIndex == 3){
            self.isEditOtherDetail = true
        }
        if(segment.selectedSegmentIndex == 4){
            self.isEditMoistureDetail = true
        }
        return true
    }
 
}
// MARK: -
// MARK: ------- NPMA_AddConduciveConditionsProtocol---------
extension NPMA_Inspection_iPadVC: NPMA_AddConduciveConditionsProtocol{
    func getDataFromAddConduciveConditions(dictData: NSDictionary, tag: Int) {
        self.ReloadViewOnScrollAccordingCondition(tag: segment.selectedSegmentIndex)
    }
    
    
}

