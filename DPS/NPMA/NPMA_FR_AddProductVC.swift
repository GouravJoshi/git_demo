//
//  NPMA_FR_AddProductVC.swift
//  DPS 20201
//  saavvan Patidar
//  Created by NavinPatidar on 10/19/20.
//  Copyright © 2020 Saavan. All rights reserved.
//  2021 

import UIKit


protocol NPMA_AddFR_ProductProtocol : class
{
    
    func getDataFromAddFR_Product(dictData : NSDictionary ,editIndex : Int,tag : Int , statusForEdit : Bool)
    
}


class NPMA_FR_AddProductVC: UIViewController {

    //MARK:
    //MARK: -------------Global Variables --------------
    
    var strViewFor = ""
    var indexForEdit = Int()
    @objc  var strWoId = NSString ()
    @objc var strWdoLeadId = NSString ()
    var objWorkorderDetail = NSManagedObject()
    var strIdToUpdate = ""
    var strType = ""
    var dictProductSoil = NSDictionary()
    var dictProductWood = NSDictionary()

    //MARK:
    //MARK: -------------IBOutlet --------------
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var viewSoil: UIView!
    @IBOutlet weak var viewWood: UIView!
    @IBOutlet weak var viewBaitStation: UIView!
    @IBOutlet weak var viewPhysicalBarrier: UIView!

    @IBOutlet weak var btnSoil: UIButton!
    @IBOutlet weak var btnWood: UIButton!
    @IBOutlet weak var btnBaitStation: UIButton!
    @IBOutlet weak var btnPhysicalBarrier: UIButton!

    //--ViewSoil
    @IBOutlet weak var txt_Soil_ProductCategory: ACFloatingTextField!
    @IBOutlet weak var txt_Soil_Product: ACFloatingTextField!
    @IBOutlet weak var txt_Soil_Qty: ACFloatingTextField!
    @IBOutlet weak var txt_Soil_UnitMeasurement: ACFloatingTextField!
    @IBOutlet weak var txt_Soil_EPAReg: ACFloatingTextField!
    @IBOutlet weak var txt_Soil_Dilution: ACFloatingTextField!
    @IBOutlet weak var btn_Soil_Yes: UIButton!
    @IBOutlet weak var btn_Soil_No: UIButton!
    @IBOutlet weak var btn_Soil_Primary: UIButton!

    //--ViewWood
    @IBOutlet weak var txt_Wood_ProductCategory: ACFloatingTextField!
    @IBOutlet weak var txt_Wood_Product: ACFloatingTextField!
    @IBOutlet weak var txt_Wood_Qty: ACFloatingTextField!
    @IBOutlet weak var txt_Wood_UnitMeasurement: ACFloatingTextField!
    @IBOutlet weak var txt_Wood_EPAReg: ACFloatingTextField!
    @IBOutlet weak var txt_Wood_Dilution: ACFloatingTextField!
    @IBOutlet weak var btn_Wood_Primary: UIButton!
    //--BaitStation
    @IBOutlet weak var txt_BaitStation_NameOfSystem: ACFloatingTextField!
    @IBOutlet weak var txt_BaitStation_EPAReg: ACFloatingTextField!
    @IBOutlet weak var txt_BaitStation_StatioInstalled: ACFloatingTextField!
    @IBOutlet weak var btn_BaitStation_Primary: UIButton!
    //--PhysicalBarrier
    @IBOutlet weak var txt_PhysicalBarrier_NameOfSystem: ACFloatingTextField!
    @IBOutlet weak var btn_PhysicalBarrier_Primary: UIButton!
    
    
    weak var delegate: NPMA_AddFR_ProductProtocol?

    
    //MARK:
    //MARK: ------Life Cycle -------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            //strWoLeadId = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                strWdoLeadId = "\(match.value(forKey: "leadId")!)" as NSString
                
            }
            
        }
        
        if strViewFor == "EDIT" {
            
            loadValuesOnUpdate()
            
        } else {
          
            btn_Soil_Yes.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            btn_Soil_No.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            self.initializeView()
        }
    }
    
    func initializeView() {
        if(strViewFor == ""){  // For Add New
            
            btnSoil.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            
            viewSoil.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewSoil.frame.height)
            self.scrollView.addSubview(self.viewSoil)
        }else if(strType == "Soil"){  //
            viewSoil.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewSoil.frame.height)
            self.scrollView.addSubview(self.viewSoil)
        }else if(strType == "Wood"){  //
            viewWood.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewWood.frame.height)
            self.scrollView.addSubview(self.viewWood)
        }else if(strType == "Bait"){  //
            viewBaitStation.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewBaitStation.frame.height)
            self.scrollView.addSubview(self.viewBaitStation)
        }else {  //
            viewPhysicalBarrier.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewPhysicalBarrier.frame.height)
            self.scrollView.addSubview(self.viewPhysicalBarrier)
        }
        
      
    }
    //MARK:
    //MARK: -------------Other Function-------------
    
    func loadValuesOnUpdate() {
        
        if strType == "Soil" {
            
            btnSoil.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            
            fetchSoil()
            
            btnWood.isEnabled = false
            btnBaitStation.isEnabled = false
            btnPhysicalBarrier.isEnabled = false
            
        }else if strType == "Wood" {
            
            btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnWood.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            
            fetchWood()
            
            btnSoil.isEnabled = false
            btnBaitStation.isEnabled = false
            btnPhysicalBarrier.isEnabled = false
            
        }else if strType == "Bait" {
            
            btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnBaitStation.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            
            btnSoil.isEnabled = false
            btnWood.isEnabled = false
            btnPhysicalBarrier.isEnabled = false
            
            self.fetchBaitStationDB()
            
        } else {
            
            btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
            btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
            
            btnSoil.isEnabled = false
            btnWood.isEnabled = false
            btnBaitStation.isEnabled = false
            self.fetchPhysicalBarrierDB()
        }
        
    }
    
    func fetchSoil() {
        
        let arrSoil = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate))

        if arrSoil.count > 0 {
            
            let objFinalize = arrSoil[0] as! NSManagedObject
            
            let strProductCategoryId = "\(objFinalize.value(forKey: "productCategoryId") ?? "")"
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductCategoryId") ?? "")" == strProductCategoryId {
                        
                        txt_Soil_ProductCategory.text = "\(dictData.value(forKey: "ProductCategoryName")!)"
                        txt_Soil_ProductCategory.tag = Int("\(dictData.value(forKey: "ProductCategoryId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductCategoryId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            let strProductMasterId = "\(objFinalize.value(forKey: "productMasterId") ?? "")"
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductId") ?? "")" == strProductMasterId {
                        
                        txt_Soil_Product.text = "\(dictData.value(forKey: "ProductName")!)"
                        txt_Soil_Product.tag = Int("\(dictData.value(forKey: "ProductId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            txt_Soil_Qty.text = "\(objFinalize.value(forKey: "quantity") ?? "")"
            let strUnitId = "\(objFinalize.value(forKey: "unitId") ?? "")"
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "UnitId") ?? "")" == strUnitId {
                        
                        txt_Soil_UnitMeasurement.text = "\(dictData.value(forKey: "UnitName")!)"
                        txt_Soil_UnitMeasurement.tag = Int("\(dictData.value(forKey: "UnitId")!)" == "" ? "0" : "\(dictData.value(forKey: "UnitId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            txt_Soil_EPAReg.text = "\(objFinalize.value(forKey: "ePARegistrationNumber") ?? "")"
            txt_Soil_Dilution.text = "\(objFinalize.value(forKey: "concentration") ?? "")"

            if objFinalize.value(forKey: "isTreatmentCompleted") as! Bool == true{
                
                btn_Soil_Yes.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                btn_Soil_No.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

            }else{
                
                btn_Soil_Yes.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                btn_Soil_No.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                
            }
            
            if objFinalize.value(forKey: "isPrimary") as! Bool == true{
                
                btn_Soil_Primary.setImage(UIImage(named: "NPMA_Check"), for: .normal)

            }else{
                
                btn_Soil_Primary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                
            }
            
            if objFinalize.value(forKey: "isSoil") as! Bool == true{
                
                btnSoil.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)

            }else{
                
                btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btnWood.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                
            }
            
        }
        
    }
    
    func saveSoilDB() {
        
        if strViewFor == "EDIT" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
                        
            arrOfKeys.add("productCategoryId")
            arrOfKeys.add("productMasterId")
            arrOfKeys.add("quantity")
            arrOfKeys.add("unitId")
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("concentration")
            arrOfKeys.add("isTreatmentCompleted")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("isSoil")

            arrOfValues.add(txt_Soil_ProductCategory.tag)
            arrOfValues.add(txt_Soil_Product.tag)
            arrOfValues.add(txt_Soil_Qty.text ?? "")
            arrOfValues.add(txt_Soil_UnitMeasurement.tag)
            arrOfValues.add(txt_Soil_EPAReg.text ?? "")
            arrOfValues.add(txt_Soil_Dilution.text ?? "")
            arrOfValues.add(btn_Soil_Yes.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btn_Soil_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSoil.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)

            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }else{
                        
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("productCategoryId")
            arrOfKeys.add("productMasterId")
            arrOfKeys.add("quantity")
            arrOfKeys.add("unitId")
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("concentration")
            arrOfKeys.add("isTreatmentCompleted")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("isSoil")
            arrOfKeys.add("woNPMAProductDetailId")
            arrOfKeys.add("isActive")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(txt_Soil_ProductCategory.tag)")
            arrOfValues.add("\(txt_Soil_Product.tag)")
            arrOfValues.add(txt_Soil_Qty.text ?? "")
            arrOfValues.add("\(txt_Soil_UnitMeasurement.tag)")
            arrOfValues.add(txt_Soil_EPAReg.text ?? "")
            arrOfValues.add(txt_Soil_Dilution.text ?? "")
            arrOfValues.add(btn_Soil_Yes.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btn_Soil_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSoil.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(Global().getReferenceNumber())
            arrOfValues.add(true)

            //deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

            saveDataInDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            updateServicePestModifyDate(strWoId: self.strWoId as String)

            
        }
        
    }
    
    func fetchWood() {
        
        let arrSoil = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate))

        if arrSoil.count > 0 {
            
            let objFinalize = arrSoil[0] as! NSManagedObject
            
            let strProductCategoryId = "\(objFinalize.value(forKey: "productCategoryId") ?? "")"
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductCategoryId") ?? "")" == strProductCategoryId {
                        
                        txt_Wood_ProductCategory.text = "\(dictData.value(forKey: "ProductCategoryName")!)"
                        txt_Wood_ProductCategory.tag = Int("\(dictData.value(forKey: "ProductCategoryId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductCategoryId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            let strProductMasterId = "\(objFinalize.value(forKey: "productMasterId") ?? "")"
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductId") ?? "")" == strProductMasterId {
                        
                        txt_Wood_Product.text = "\(dictData.value(forKey: "ProductName")!)"
                        txt_Wood_Product.tag = Int("\(dictData.value(forKey: "ProductId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            txt_Wood_Qty.text = "\(objFinalize.value(forKey: "quantity") ?? "")"
            let strUnitId = "\(objFinalize.value(forKey: "unitId") ?? "")"
            
            array = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "UnitId") ?? "")" == strUnitId {
                        
                        txt_Wood_UnitMeasurement.text = "\(dictData.value(forKey: "UnitName")!)"
                        txt_Wood_UnitMeasurement.tag = Int("\(dictData.value(forKey: "UnitId")!)" == "" ? "0" : "\(dictData.value(forKey: "UnitId")!)")!
                        
                        break
                        
                    }
                    
                }
                
            }
            
            txt_Wood_EPAReg.text = "\(objFinalize.value(forKey: "ePARegistrationNumber") ?? "")"
            txt_Wood_Dilution.text = "\(objFinalize.value(forKey: "concentration") ?? "")"
            
            if objFinalize.value(forKey: "isPrimary") as! Bool == true{
                
                btn_Wood_Primary.setImage(UIImage(named: "NPMA_Check"), for: .normal)

            }else{
                
                btn_Wood_Primary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                
            }
            
            if objFinalize.value(forKey: "isSoil") as! Bool == false{
                
                btnWood.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)

            }else{
                
                btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btnSoil.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                
            }
            
        }
        
    }
    
    func saveWoodDB() {
        
        if strViewFor == "EDIT" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
                        
            arrOfKeys.add("productCategoryId")
            arrOfKeys.add("productMasterId")
            arrOfKeys.add("quantity")
            arrOfKeys.add("unitId")
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("concentration")
            arrOfKeys.add("isTreatmentCompleted")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("isSoil")

            arrOfValues.add("\(txt_Wood_ProductCategory.tag)")
            arrOfValues.add("\(txt_Wood_Product.tag)")
            arrOfValues.add(txt_Wood_Qty.text ?? "")
            arrOfValues.add("\(txt_Wood_UnitMeasurement.tag)")
            arrOfValues.add(txt_Wood_EPAReg.text ?? "")
            arrOfValues.add(txt_Wood_Dilution.text ?? "")
            arrOfValues.add(false)
            arrOfValues.add(btn_Wood_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSoil.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)

            
            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }else{
                        
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("productCategoryId")
            arrOfKeys.add("productMasterId")
            arrOfKeys.add("quantity")
            arrOfKeys.add("unitId")
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("concentration")
            arrOfKeys.add("isTreatmentCompleted")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("isSoil")
            arrOfKeys.add("woNPMAProductDetailId")
            arrOfKeys.add("isActive")

            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(txt_Wood_ProductCategory.tag)")
            arrOfValues.add("\(txt_Wood_Product.tag)")
            arrOfValues.add(txt_Wood_Qty.text ?? "")
            arrOfValues.add("\(txt_Wood_UnitMeasurement.tag)")
            arrOfValues.add(txt_Wood_EPAReg.text ?? "")
            arrOfValues.add(txt_Wood_Dilution.text ?? "")
            arrOfValues.add(false)
            arrOfValues.add(btn_Wood_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSoil.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(Global().getReferenceNumber())
            arrOfValues.add(true)

            //deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

            saveDataInDB(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
    }
    func fetchBaitStationDB() {
        
        let arrSoil = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate))

        if arrSoil.count > 0 {
            
            let objFinalize = arrSoil[0] as! NSManagedObject
            
            txt_BaitStation_NameOfSystem.text = "\(objFinalize.value(forKey: "nameOfSystem") ?? "")"
            txt_BaitStation_EPAReg.text = "\(objFinalize.value(forKey: "ePARegistrationNumber") ?? "")"
            txt_BaitStation_StatioInstalled.text = "\(objFinalize.value(forKey: "numberOfStationInstalled") ?? "")"
            
            
            if objFinalize.value(forKey: "isPrimary") as! Bool == true{
                
                btn_BaitStation_Primary.setImage(UIImage(named: "NPMA_Check"), for: .normal)

            }else{
                
                btn_BaitStation_Primary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                
            }
            
            if objFinalize.value(forKey: "isBaitStation") as! Bool == false{
                
                btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)

            }else{
                
                btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btnBaitStation.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                
            }
            
        }
        
    }
    
    func saveBaitStationDB() {
        
        if strViewFor == "EDIT" {
     
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
                        
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("isBaitStation")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("nameOfSystem")
            arrOfKeys.add("numberOfStationInstalled")
            arrOfValues.add("\(txt_BaitStation_EPAReg.text!)")
            arrOfValues.add(btnBaitStation.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(btn_BaitStation_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(txt_BaitStation_NameOfSystem.text ?? "")
            arrOfValues.add(txt_BaitStation_StatioInstalled.text ?? "")

            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }else{
                        
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
      
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
           
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("isActive")
            arrOfKeys.add("isBaitStation")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("nameOfSystem")
            arrOfKeys.add("numberOfStationInstalled")
            arrOfKeys.add("woNPMAProductDetailId")
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("\(txt_BaitStation_EPAReg.text!)")
            arrOfValues.add(true)
            arrOfValues.add(btnBaitStation.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(btn_BaitStation_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(txt_BaitStation_NameOfSystem.text ?? "")
            arrOfValues.add(txt_BaitStation_StatioInstalled.text ?? "")

            arrOfValues.add(Global().getReferenceNumber())

            //deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

            saveDataInDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
    }
    func fetchPhysicalBarrierDB() {
        
        let arrSoil = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate))

        if arrSoil.count > 0 {
            
            let objFinalize = arrSoil[0] as! NSManagedObject
            
            txt_PhysicalBarrier_NameOfSystem.text = "\(objFinalize.value(forKey: "nameOfSystem") ?? "")"
            
            if objFinalize.value(forKey: "isPrimary") as! Bool == true{
                
                btn_PhysicalBarrier_Primary.setImage(UIImage(named: "NPMA_Check"), for: .normal)

            }else{
                
                btn_PhysicalBarrier_Primary.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                
            }
            
            if objFinalize.value(forKey: "isBaitStation") as! Bool == false{
                
                btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)

            }else{
                
                btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
                btnBaitStation.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
                
            }
            
        }
        
    }
    
    func savePhysicalBarrierDB() {
        
        if strViewFor == "EDIT" {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
                        
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("isBaitStation")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("nameOfSystem")
            arrOfKeys.add("numberOfStationInstalled")
            arrOfValues.add("")
            arrOfValues.add(btnBaitStation.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(btn_PhysicalBarrier_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(txt_PhysicalBarrier_NameOfSystem.text ?? "")
            arrOfValues.add("")

            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", strWoId,Global().getUserName(), strIdToUpdate), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }else{
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
      
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
           
            arrOfKeys.add("ePARegistrationNumber")
            arrOfKeys.add("isActive")
            arrOfKeys.add("isBaitStation")
            arrOfKeys.add("isPrimary")
            arrOfKeys.add("nameOfSystem")
            arrOfKeys.add("numberOfStationInstalled")
            arrOfKeys.add("woNPMAProductDetailId")
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add("")
            arrOfValues.add(true)
            arrOfValues.add(btnBaitStation.currentImage == UIImage(named: "NPMA_Radio_Yes") ? true : false)
            arrOfValues.add(btn_PhysicalBarrier_Primary.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(txt_PhysicalBarrier_NameOfSystem.text ?? "")
            arrOfValues.add("")

            arrOfValues.add(Global().getReferenceNumber())

            //deleteAllRecordsFromDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

            saveDataInDB(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            updateServicePestModifyDate(strWoId: self.strWoId as String)

        }
        
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    //MARK:
    //MARK: -------------Actions-------------

    @IBAction func action_Back(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func action_TopButton(_ sender: UIButton) {
        self.view.endEditing(true)
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        btnSoil.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
        btnWood.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
        btnBaitStation.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
        btnPhysicalBarrier.setImage(UIImage(named: "NPMA_Radio_No"), for: .normal)
        sender.setImage(UIImage(named: "NPMA_Radio_Yes"), for: .normal)
        // Soil
        if(sender.tag == 1){
            viewSoil.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewSoil.frame.height)
            self.scrollView.addSubview(self.viewSoil)
        }
        // Wood
        else if(sender.tag == 2){
            viewWood.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewWood.frame.height)
            self.scrollView.addSubview(self.viewWood)
        }
        // viewBaitStation
        else if(sender.tag == 3){
            viewBaitStation.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewBaitStation.frame.height)
            self.scrollView.addSubview(self.viewBaitStation)
        }
        // viewPhysicalBarrier
        else if(sender.tag == 4){
            viewPhysicalBarrier.frame = CGRect(x: 10, y: 10, width: scrollView.frame.width - 20, height: viewPhysicalBarrier.frame.height)
            self.scrollView.addSubview(self.viewPhysicalBarrier)
        }
        
    }
    
    //----------Soil View--Actions-------------
    
    @IBAction func action_OnAddSoil(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if(txt_Soil_ProductCategory.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Product Category is required!", viewcontrol: self)
        }else if(txt_Soil_Product.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Product is required!", viewcontrol: self)

        }
        else if(txt_Soil_Qty.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Quantity is required!", viewcontrol: self)

        }
        else if(txt_Soil_UnitMeasurement.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Unit of measurement is required!", viewcontrol: self)

        }else{

            self.saveSoilDB()

            self.dismiss(animated: false, completion: nil)
            delegate?.getDataFromAddFR_Product(dictData: NSDictionary(), editIndex: indexForEdit, tag: 1, statusForEdit: strViewFor == "EDIT" ? true : false)
        }
     
    }
    
    @IBAction func action_OnProductCategorySoil(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()
                
        aryTemp = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if(aryTemp.count != 0){
            sender.tag = 512
            openTableViewPopUp(tag: 512, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_OnProductSoil(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()

        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            if txt_Soil_ProductCategory.text!.count > 0 {
                
                let nsGroupid = txt_Soil_ProductCategory.tag as NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryTemp = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid , strKeyName: "ProductCategoryId")
                
            } else {
                
                aryTemp = array
                
            }
            
        }
        
        if(aryTemp.count != 0){
            sender.tag = 513
            openTableViewPopUp(tag: 513, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_OnUnityMeasurementSoil(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()
        
        aryTemp = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")
        
        if(aryTemp.count != 0){
            sender.tag = 514
            openTableViewPopUp(tag: 514, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
        
    }
    
    
    @IBAction func action_OnUnityDilutionSoil(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = NSMutableArray()
        let dict = NSMutableDictionary()
        let dict1 = NSMutableDictionary()
        let dict2 = NSMutableDictionary()

        if dictProductSoil.count > 0 {
            
            if "\(dictProductSoil.value(forKey: "FirstDilutedPercent") ?? "")".count > 0 {
                
                dict.setValue("\(dictProductSoil.value(forKey: "FirstDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict)
                
            }
            if "\(dictProductSoil.value(forKey: "SecondDilutedPercent") ?? "")".count > 0 {
                
                dict1.setValue("\(dictProductSoil.value(forKey: "SecondDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict1)
                
            }
            if "\(dictProductSoil.value(forKey: "ThirdDilutedPercent") ?? "")".count > 0 {
                
                dict2.setValue("\(dictProductSoil.value(forKey: "ThirdDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict2)
                
            }
            
            if(aryTemp.count != 0){
                sender.tag = 515
                openTableViewPopUp(tag: 515, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select product.", viewcontrol: self)

        }

    }
    @IBAction func action_OnYes_NoSoil(_ sender: UIButton) {
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }
        if sender == btn_Soil_Yes {
            if(sender.currentImage == UIImage(named: "NPMA_Check")){
                btn_Soil_No.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
        }
        if sender == btn_Soil_No {
            if(sender.currentImage == UIImage(named: "NPMA_Check")){
                btn_Soil_Yes.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
        }
    }
    @IBAction func action_OnPrimarySoil(_ sender: UIButton) {
        
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            
        }else{
                        
           let ary_SoilList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isSoil == YES &&  isPrimary == YES", strWoId,Global().getUserName()))
            
            if ary_SoilList.count == 0 {
                
                sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                
            } else {
                
                //let objArea = ary_SoilList[0] as! NSManagedObject
                let strMsg = "Primary already exist for this WO. Do you won't to replace it?"
                //let idddd = "\(objArea.value(forKey: "mobileId") ?? "")"
                
                let alert = UIAlertController(title: Alert, message: strMsg, preferredStyle: .alert)
                alert.view.tintColor = UIColor.black
                
                
                alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
                    

                    
                    let arrOfKeys = NSMutableArray()
                    let arrOfValues = NSMutableArray()
                    
                    arrOfKeys.add("isPrimary")
                    arrOfValues.add(false)
                    
                    let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isSoil == YES &&  isPrimary == YES", self.strWoId,Global().getUserName()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                    
                    if isSuccess {
                        
                    }
                    
                    sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                    
                }))
                if let popoverController = alert.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = self.view.bounds
                    popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                }
                self.present(alert, animated: true)
                
            }

        }
    }
    //----------Wood View--Actions-------------

    @IBAction func action_OnAddWood(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if(txt_Wood_ProductCategory.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Product Category is required!", viewcontrol: self)
        }else if(txt_Wood_Product.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Product is required!", viewcontrol: self)

        }
        else if(txt_Wood_Qty.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Quantity is required!", viewcontrol: self)

        }
        else if(txt_Wood_UnitMeasurement.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Unit of measurement is required!", viewcontrol: self)

        }else{
        
        self.saveWoodDB()
            
        self.dismiss(animated: false, completion: nil)
            delegate?.getDataFromAddFR_Product(dictData: NSDictionary(), editIndex: indexForEdit, tag: 2, statusForEdit: strViewFor == "EDIT" ? true : false)
            
        }
    }
    
    @IBAction func action_OnProductCategoryWood(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()
        aryTemp = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductCategoryMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

        if(aryTemp.count != 0){
            sender.tag = 516
            openTableViewPopUp(tag: 516, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_OnProductWood(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()
        var array = NSMutableArray()
        
        array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)
        
        if array.count > 0 {
            
            if txt_Soil_ProductCategory.text!.count > 0 {
                
                let nsGroupid = txt_Soil_ProductCategory.tag as NSNumber
                let strGroupid =  String("\(nsGroupid)")
                
                aryTemp = getGroupDataOnly(arrOfData: array, strGroupId: strGroupid , strKeyName: "ProductCategoryId")
                
            } else {
                
                aryTemp = array
                
            }
            
        }
        if(aryTemp.count != 0){
            sender.tag = 517
            openTableViewPopUp(tag: 517, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_OnUnityMeasurementWood(_ sender: UIButton) {
        self.view.endEditing(true)
        var aryTemp = NSMutableArray()
        aryTemp = getDataFromServiceAutoMasters(strTypeOfMaster: "UnitMasters" , strBranchId: "")

        if(aryTemp.count != 0){
            sender.tag = 518
            openTableViewPopUp(tag: 518, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
        }
    }
    @IBAction func action_OnUnityDilutionWood(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTemp = NSMutableArray()
        let dict = NSMutableDictionary()
        let dict1 = NSMutableDictionary()
        let dict2 = NSMutableDictionary()
        
        if dictProductWood.count > 0 {
            
            if "\(dictProductWood.value(forKey: "FirstDilutedPercent") ?? "")".count > 0 {
                
                dict.setValue("\(dictProductWood.value(forKey: "FirstDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict)
                
            }
            if "\(dictProductWood.value(forKey: "SecondDilutedPercent") ?? "")".count > 0 {
                
                dict1.setValue("\(dictProductWood.value(forKey: "SecondDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict1)
                
            }
            if "\(dictProductWood.value(forKey: "ThirdDilutedPercent") ?? "")".count > 0 {
                
                dict2.setValue("\(dictProductWood.value(forKey: "ThirdDilutedPercent") ?? "")", forKey: "DilutedPercent")
                aryTemp.add(dict2)
                
            }
            
            if(aryTemp.count != 0){
                sender.tag = 519
                openTableViewPopUp(tag: 519, ary: (aryTemp as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            }
            
        }
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select product.", viewcontrol: self)

        }

    }
    
    @IBAction func action_OnPrimaryWood(_ sender: UIButton) {
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            
            let ary_SoilList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isSoil == NO &&  isPrimary == YES", strWoId,Global().getUserName()))
             
             if ary_SoilList.count == 0 {
                 
                 sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                 
             } else {
                 
                 //let objArea = ary_SoilList[0] as! NSManagedObject
                 let strMsg = "Primary already exist for this WO. Do you won't to replace it?"
                 //let idddd = "\(objArea.value(forKey: "mobileId") ?? "")"
                 
                 let alert = UIAlertController(title: Alert, message: strMsg, preferredStyle: .alert)
                 alert.view.tintColor = UIColor.black
                 
                 
                 alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
                     

                     
                     let arrOfKeys = NSMutableArray()
                     let arrOfValues = NSMutableArray()
                     
                     arrOfKeys.add("isPrimary")
                     arrOfValues.add(false)
                     
                     let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isSoil == NO &&  isPrimary == YES", self.strWoId,Global().getUserName()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                     
                     if isSuccess {
                         
                     }
                     
                     sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                     
                 }))
                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                     
                 }))
                 if let popoverController = alert.popoverPresentationController {
                     popoverController.sourceView = self.view
                     popoverController.sourceRect = self.view.bounds
                     popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                 }
                 self.present(alert, animated: true)
                 
             }

         }
    }

    //----------BaitStation View--Actions-------------

    @IBAction func action_OnAddBaitStation(_ sender: Any) {
        self.view.endEditing(true)
    
        
        if(txt_BaitStation_NameOfSystem.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Name Of System is required!", viewcontrol: self)
        }else if(txt_BaitStation_StatioInstalled.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "#of station Installed is required!", viewcontrol: self)

        }
       else{
            saveBaitStationDB()
            self.dismiss(animated: false, completion: nil)
            delegate?.getDataFromAddFR_Product(dictData: NSDictionary(), editIndex: indexForEdit, tag: 3, statusForEdit: strViewFor == "EDIT" ? true : false)
        }
      
    }
    @IBAction func action_OnPrimaryBaitStation(_ sender: UIButton) {
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            
            let ary_SoilList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isBaitStation == YES &&  isPrimary == YES", strWoId,Global().getUserName()))
             
             if ary_SoilList.count == 0 {
                 
                 sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                 
             } else {
                 
                 //let objArea = ary_SoilList[0] as! NSManagedObject
                 let strMsg = "Primary already exist for this WO. Do you won't to replace it?"
                 //let idddd = "\(objArea.value(forKey: "mobileId") ?? "")"
                 
                 let alert = UIAlertController(title: Alert, message: strMsg, preferredStyle: .alert)
                 alert.view.tintColor = UIColor.black
                 
                 
                 alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
                     
                     let arrOfKeys = NSMutableArray()
                     let arrOfValues = NSMutableArray()
                     
                     arrOfKeys.add("isPrimary")
                     arrOfValues.add(false)
                     
                     let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isBaitStation == YES &&  isPrimary == YES", self.strWoId,Global().getUserName()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                     
                     if isSuccess {
                         
                     }
                     
                     sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                     
                 }))
                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                     
                 }))
                 if let popoverController = alert.popoverPresentationController {
                     popoverController.sourceView = self.view
                     popoverController.sourceRect = self.view.bounds
                     popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                 }
                 self.present(alert, animated: true)
                 
             }

         }
    }
    
    //----------PhysicalBarrier View--Actions-------------

    @IBAction func action_OnAddPhysicalBarrier(_ sender: Any) {
        self.view.endEditing(true)
        
        if(txt_PhysicalBarrier_NameOfSystem.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Name Of System is required!", viewcontrol: self)
        }
        else{
            self.savePhysicalBarrierDB()
            self.dismiss(animated: false, completion: nil)
            delegate?.getDataFromAddFR_Product(dictData: NSDictionary(), editIndex: indexForEdit, tag: 4, statusForEdit: strViewFor == "EDIT" ? true : false)
        }
     
    }
    @IBAction func action_OnPrimaryPhysicalBarrier(_ sender: UIButton) {
        if(sender.currentImage == UIImage(named: "NPMA_Check")){ //2 wala check
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            
            let ary_SoilList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isBaitStation == NO &&  isPrimary == YES", strWoId,Global().getUserName()))
             
             if ary_SoilList.count == 0 {
                 
                 sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                 
             } else {
                 
                 //let objArea = ary_SoilList[0] as! NSManagedObject
                 let strMsg = "Primary already exist for this WO. Do you won't to replace it?"
                 //let idddd = "\(objArea.value(forKey: "mobileId") ?? "")"
                 
                 let alert = UIAlertController(title: Alert, message: strMsg, preferredStyle: .alert)
                 alert.view.tintColor = UIColor.black
                 
                 
                 alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
                     

                     
                     let arrOfKeys = NSMutableArray()
                     let arrOfValues = NSMutableArray()
                     
                     arrOfKeys.add("isPrimary")
                     arrOfValues.add(false)
                     
                     let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isBaitStation == NO &&  isPrimary == YES", self.strWoId,Global().getUserName()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                     
                     if isSuccess {
                         
                     }
                     
                     sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                     
                 }))
                 alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                     
                 }))
                 if let popoverController = alert.popoverPresentationController {
                     popoverController.sourceView = self.view
                     popoverController.sourceRect = self.view.bounds
                     popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                 }
                 self.present(alert, animated: true)
                 
             }

         }
    }
}
// MARK: -
// MARK: ------- Selection CustomTableView---------
extension NPMA_FR_AddProductVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        //---------For Soil Product

        if(tag == 512)
        {
            if dictData.count == 0 {
                
                txt_Soil_ProductCategory.text = ""
                txt_Soil_ProductCategory.tag = 0
                txt_Soil_Product.text = ""
                txt_Soil_Product.tag = 0
                
            }else{
                txt_Soil_ProductCategory.text = "\(dictData.value(forKey: "ProductCategoryName")!)"
                txt_Soil_ProductCategory.tag = Int("\(dictData.value(forKey: "ProductCategoryId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductCategoryId")!)")!
                txt_Soil_Product.text = ""
                txt_Soil_Product.tag = 0
                
            }

        }
       else if(tag == 513)
        {
            if dictData.count == 0 {
                txt_Soil_Product.text = ""
                txt_Soil_Product.tag = 0
                dictProductSoil = NSDictionary()
            }else{
                txt_Soil_Product.text = "\(dictData.value(forKey: "ProductName")!)"
                txt_Soil_Product.tag = Int("\(dictData.value(forKey: "ProductId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductId")!)")!
                dictProductSoil = dictData
            }

        }
       else if(tag == 514)
        {
            if dictData.count == 0 {
                txt_Soil_UnitMeasurement.text = ""
                txt_Soil_UnitMeasurement.tag = 0
            }else{
                txt_Soil_UnitMeasurement.text = "\(dictData.value(forKey: "UnitName")!)"
                txt_Soil_UnitMeasurement.tag = Int("\(dictData.value(forKey: "UnitId")!)" == "" ? "0" : "\(dictData.value(forKey: "UnitId")!)")!
            }

        }
       else if(tag == 515)
        {
            if dictData.count == 0 {
                txt_Soil_Dilution.text = ""
                txt_Soil_Dilution.tag = 0
            }else{
                txt_Soil_Dilution.text = "\(dictData.value(forKey: "DilutedPercent")!)"
                //txt_Soil_Dilution.tag = Int("\(dictData.value(forKey: "locationID")!)")!
            }

        }
        
        //---------For Wood Product
       else if(tag == 516)
        {
            if dictData.count == 0 {
                txt_Wood_ProductCategory.text = ""
                txt_Wood_ProductCategory.tag = 0
                dictProductWood = NSDictionary()
                txt_Wood_Product.text = ""
                txt_Wood_Product.tag = 0
            }else{
                txt_Wood_ProductCategory.text = "\(dictData.value(forKey: "ProductCategoryName")!)"
                txt_Wood_ProductCategory.tag = Int("\(dictData.value(forKey: "ProductCategoryId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductCategoryId")!)")!
                dictProductWood = dictData
                txt_Wood_Product.text = ""
                txt_Wood_Product.tag = 0
                
            }

        }
        
       
      else if(tag == 517)
       {
           if dictData.count == 0 {
               txt_Wood_Product.text = ""
            txt_Wood_Product.tag = 0
            dictProductWood = NSDictionary()
           }else{
            txt_Wood_Product.text = "\(dictData.value(forKey: "ProductName")!)"
            txt_Wood_Product.tag = Int("\(dictData.value(forKey: "ProductId")!)" == "" ? "0" : "\(dictData.value(forKey: "ProductId")!)")!
            dictProductWood = dictData
           }

       }
      else if(tag == 518)
       {
           if dictData.count == 0 {
               txt_Wood_UnitMeasurement.text = ""
            txt_Wood_UnitMeasurement.tag = 0
           }else{
            txt_Wood_UnitMeasurement.text = "\(dictData.value(forKey: "UnitName")!)"
            txt_Wood_UnitMeasurement.tag = Int("\(dictData.value(forKey: "UnitId")!)" == "" ? "0" : "\(dictData.value(forKey: "UnitId")!)")!
           }

       }
      else if(tag == 519)
       {
           if dictData.count == 0 {
               txt_Wood_Dilution.text = ""
            txt_Wood_Dilution.tag = 0
           }else{
            txt_Wood_Dilution.text = "\(dictData.value(forKey: "DilutedPercent")!)"
            //txt_Wood_Dilution.tag = Int("\(dictData.value(forKey: "locationID")!)")!
           }
       }
        
    }
}
// MARK: -
// MARK: ----------DatePickerProtocol---------
extension NPMA_FR_AddProductVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        //txtDateForCheck.text = strDate
        
    }
}
// MARK: -
// MARK: ----------UITextFieldDelegate---------
extension NPMA_FR_AddProductVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
            if(textField == txt_Soil_Qty){
                return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 14)

            }

            if(textField == txt_Wood_Qty){
                return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 14)

            }

            if(textField == txt_BaitStation_StatioInstalled){
                return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 4)

            }
       
        return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 999)

        
    }
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
