//
//  NPMA_FinlizedReportVC.swift
//  DPS
//  peSTream 2020 
//  Created by NavinPatidar on 10/16/20.
//  Copyright © 2020 Saavan. All rights reserved.

import UIKit

class NPMA_FinlizedReportVC: UIViewController {

    //MARK:
    //MARK: -------------Global Variables --------------
    
    let global = Global()
    var dictServiceNameFromId = NSDictionary()

    //MARK:     ---------  NSManagedObject   ---------
    
    var paymentInfoDetail = NSManagedObject()
    @objc var objWdoLeadDetail = NSManagedObject()
    var objWorkorderDetail = NSManagedObject()

    //MARK:     ---------  Bool   ---------
    
    var chkCustomerNotPresent = Bool()
    var chkFrontImage = Bool()
    var isPreSetSignGlobal = Bool()
    var yesEditedSomething = Bool()
    
    //MARK:     ---------  String  ---------
    
    var strWorkOrderStatus = String()
    var strServiceAddImageName = String()
    var strAudioName = String()
    var strAudioNameSales = String()
    
    @objc  var strWoId = NSString ()
    @objc var strWdoLeadId = NSString ()
    var strCompanyKey = String()
    var strUserName = String()
    
    var strEmpID = String()
    var strEmpName = String()
    var strFromEmail = String()
    var strGlobalPaymentMode = String()
    var strPaymentModes = String()
    var strPaidAmount = String()
    var strCustomerSign = String()
    var strTechnicianSign = String()
    var strCheckFrontImage = String()
    var strCheckBackImage = String()
    var signatureType = String()
    
    //MARK:     ---------  Dictionary   ---------
    
    
    var dictLoginData = NSDictionary()
    var dictDepartmentNameFromSysName = NSDictionary()
    
    
    //MARK:
    //MARK: -------------IBOutlet --------------
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var view_TypeOfConstruction: CardView!
    @IBOutlet weak var view_ProductList: CardView!
    @IBOutlet weak var view_OtherDetail: CardView!
    @IBOutlet weak var view_PaymentMode: CardView!
    @IBOutlet weak var view_PaymentDetail: CardView!
    @IBOutlet weak var view_Signature: CardView!
    @IBOutlet weak var view_TermsCondition: CardView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnSaveContinue: UIButton!
    @IBOutlet weak var btnAudio: UIButton!

    var imagePicker = UIImagePickerController()
   
    //--TypeOfConstruction
    @IBOutlet weak var btnSlab: UIButton!
    @IBOutlet weak var btnBasement: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var txtOther: ACFloatingTextField!
    @IBOutlet weak var txtCommentFor99B: UITextView!
    @IBOutlet weak var btnIsCrawl: UIButton!

    //--ProductList
    @IBOutlet weak var tv_SoilList: UITableView!
    @IBOutlet weak var tv_WoodList: UITableView!
    @IBOutlet weak var tv_BaitStationList: UITableView!
    @IBOutlet weak var tv_PhysiCalBarrierList: UITableView!
    
    @IBOutlet weak var height_tv_SoilList: NSLayoutConstraint!
    @IBOutlet weak var height_tv_WoodList: NSLayoutConstraint!
    @IBOutlet weak var height_tv_BaitStationList: NSLayoutConstraint!
    @IBOutlet weak var height_tv_PhysiCalBarrierList: NSLayoutConstraint!
    
    var ary_SoilList = NSArray(),ary_WoodList = NSArray(),ary_BaitStationList = NSArray(),ary_PhysicalBarrierList = NSArray()
    
    
    //--OtherDetail
    @IBOutlet weak var txtTimein: ACFloatingTextField!
    @IBOutlet weak var txtTimeOut: ACFloatingTextField!
    @IBOutlet weak var txtTechComment: UITextView!
    @IBOutlet weak var txtTechOfficeNote: UITextView!
    
    
    //--PaymentMode
    @IBOutlet weak var txtSelectPaymentMOde: ACFloatingTextField!
    @IBOutlet weak var txtAmount: ACFloatingTextField!
    @IBOutlet weak var txtCheck: ACFloatingTextField!
    @IBOutlet weak var txtDrivingLicence: ACFloatingTextField!
    @IBOutlet weak var txtDateForCheck: ACFloatingTextField!
    @IBOutlet weak var btnCheckFrontImage: UIButton!
    @IBOutlet weak var btnCheckBackImage: UIButton!
    
    @IBOutlet weak var height_viewPayAmount: NSLayoutConstraint!
    
    //--PaymentDetail
    @IBOutlet weak var tv_PaymentDetail: UITableView!
    @IBOutlet weak var height_tv_PaymentDetail: NSLayoutConstraint!
    var aryPaymentDetail = NSMutableArray() , aryPaymentDetailCombineData = NSMutableArray()

    
    //--Signature
    @IBOutlet weak var btn_CustomerNotPresent: UIButton!
    @IBOutlet weak var img_TechSign: UIImageView!
    @IBOutlet weak var img_CustomerSign: UIImageView!
    @IBOutlet weak var btn_TechSign: UIButton!
    @IBOutlet weak var btn_CustomerSign: UIButton!

    //--Terms&Condition
    @IBOutlet weak var txtTermsAndCondition: ACFloatingTextField!
    var arySelectedTermsAndCondition = NSMutableArray()
    //MARK:
    //MARK: -------------LifeCycle--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        UIInitialization()
        
        callOnLoad()
        

        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfData.count > 0 {
            
            objWorkorderDetail = arryOfData[0] as! NSManagedObject
            
            //strWoLeadId = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let strWoLeadNo = "\(objWorkorderDetail.value(forKey: "relatedOpportunityNo") ?? "")"
            
            let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadNumber == %@", strWoLeadNo))
            
            if(arrayLeadDetail.count > 0)
            {
                
                let match = arrayLeadDetail.firstObject as! NSManagedObject
                
                strWdoLeadId = "\(match.value(forKey: "leadId")!)" as NSString
                
            }
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNoWo, viewcontrol: self)
            
            self.back()
            
        }
        
        fetchServiceDetail()
        fetchFinalizeReportDetail()
        fetchPaymentInfoDetailFromCoreData()
        fetchSoilDB()
        fetchWoodDB()
        fetchBaitDB()
        fetchPhysicalBarrierDB()
        
        fetchPaymentDetailDB()
        arySelectedTermsAndCondition = NSMutableArray()
        arySelectedTermsAndCondition = getTermsAndServiceSelected()
        

        chkCustomerNotPresent = false
        isPreSetSignGlobal = false
        yesEditedSomething = false
        
        assignValues()
     
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            viewContain.isUserInteractionEnabled = false
        }else{
            viewContain.isUserInteractionEnabled = true
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            setUPheightForTable()
        }
        
        if (nsud.value(forKey: "loadeDataRefreshFinalizeViewNPMA") != nil) {
            let yesupdatedWdoInspection = nsud.bool(forKey: "loadeDataRefreshFinalizeViewNPMA")
            if yesupdatedWdoInspection {
                
                nsud.set(false, forKey: "loadeDataRefreshFinalizeViewNPMA")
                nsud.synchronize()

                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    

                })
                
            }
            
        }
        
        //callOnLoad()

        if strTechnicianSign.count>0
        {
            downloadTechnicianSign(strImageName: strTechnicianSign)
        }
        
        if strCustomerSign.count>0
        {
            downloadCustomerSign(strImageName: strCustomerSign)
        }
        
        
        //For Preset
        var strImageName = String()
        
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)))
        {

            strImageName = Global().strTechSign(strSignUrl)
            strTechnicianSign = Global().strTechSign(strSignUrl)
            
            let nsUrl = URL(string: strSignUrl)
            
            img_TechSign.load(url: nsUrl! as URL , strImageName: strImageName)
            
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: img_TechSign.image!)
            
            btn_TechSign.isEnabled = false
            isPreSetSignGlobal = true
            
        }
        else
        {
            isPreSetSignGlobal = false
            btn_TechSign.isEnabled = true
            
            if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
            {
                btn_TechSign.isEnabled = false
            }
            if strTechnicianSign.count>0
            {
                let strIsPresetWO = objWorkorderDetail.value(forKey: "isEmployeePresetSignature") as! String
                
                if strIsPresetWO == "true" || strIsPresetWO == "1"
                {
                    downloadTechnicianPreset(strImageName: strTechnicianSign, stringUrl: strSignUrl)
                }
                else
                {
                    downloadTechnicianSign(strImageName: strTechnicianSign)
                }
                
                
            }
        }
        
        //For Audio
        if (nsud.value(forKey: "yesAudio") != nil)
        {
            yesEditedSomething = true
            let isAudio = nsud.bool(forKey: "yesAudio")
            
            if isAudio
            {
                
                strAudioName = nsud.value(forKey: "AudioNameService") as! String
                strAudioNameSales = nsud.value(forKey: "AudioNameWdoSales") as! String
                
            }
            
        }
        
        methodForDisable()
        
    }

    //MARK:
    //MARK: -------------UIInitialization--------------
    func UIInitialization()  {
     
        txtCommentFor99B.layer.cornerRadius = 8.0
        txtCommentFor99B.layer.borderWidth = 1.0
        txtCommentFor99B.layer.borderColor = UIColor.lightGray.cgColor
        
        self.tv_SoilList.layer.cornerRadius = 10.0
        self.tv_SoilList.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_SoilList.layer.borderWidth = 1.0
        self.tv_SoilList.layer.masksToBounds = true
        self.tv_SoilList.tableFooterView = UIView()
        
        self.tv_WoodList.layer.cornerRadius = 10.0
        self.tv_WoodList.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_WoodList.layer.borderWidth = 1.0
        self.tv_WoodList.layer.masksToBounds = true
        self.tv_WoodList.tableFooterView = UIView()
 
        self.tv_BaitStationList.layer.cornerRadius = 10.0
        self.tv_BaitStationList.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_BaitStationList.layer.borderWidth = 1.0
        self.tv_BaitStationList.layer.masksToBounds = true
        self.tv_BaitStationList.tableFooterView = UIView()
        
        self.tv_PhysiCalBarrierList.layer.cornerRadius = 10.0
        self.tv_PhysiCalBarrierList.layer.borderColor = UIColor.lightGray.cgColor
        self.tv_PhysiCalBarrierList.layer.borderWidth = 1.0
        self.tv_PhysiCalBarrierList.layer.masksToBounds = true
        self.tv_PhysiCalBarrierList.tableFooterView = UIView()
        
        txtTechComment.layer.cornerRadius = 8.0
        txtTechComment.layer.borderWidth = 1.0
        txtTechComment.layer.borderColor = UIColor.lightGray.cgColor
        
        txtTechOfficeNote.layer.cornerRadius = 8.0
        txtTechOfficeNote.layer.borderWidth = 1.0
        txtTechOfficeNote.layer.borderColor = UIColor.lightGray.cgColor
        
        img_TechSign.layer.cornerRadius = 8.0
        img_TechSign.layer.borderWidth = 1.0
        img_TechSign.layer.borderColor = UIColor.lightGray.cgColor
        
        img_CustomerSign.layer.cornerRadius = 8.0
        img_CustomerSign.layer.borderWidth = 1.0
        img_CustomerSign.layer.borderColor = UIColor.lightGray.cgColor
   
        buttonRound(sender: btnSaveContinue)

    }
    
    func getTermsAndServiceSelected() -> NSMutableArray {
        let aryTermsAndCondition = NSMutableArray()
        var arySelectedTerms = NSMutableArray()

        let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
        {
            let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
            for item in arrOfServiceJobDescriptionTemplate
            {
                let dict = item as! NSDictionary
                if ( "\((dict.value(forKey: "DepartmentId"))!)" == "\((objWorkorderDetail.value(forKey: "departmentId"))!)" )
                {
                    aryTermsAndCondition.add(item)
                }
            }
        }
        if(aryTermsAndCondition.count != 0){
            for item in aryTermsAndCondition {
                if let isDefault = (item as AnyObject).value(forKey: "Isdefault"){
                    if("\(isDefault)" == "1"){
                        arySelectedTerms.add(item)
                        break
                    }
                }
            }
            if(arySelectedTerms.count == 0){
                arySelectedTerms.add(aryTermsAndCondition.object(at: 0))
            }
        }
        
        if(aryTermsAndCondition.count != 0){
            let arrServiceTermsDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAServiceTermsExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ &&  isActive == YES", strWoId,Global().getUserName()))
            if arrServiceTermsDetail.count != 0 {
                arySelectedTerms = NSMutableArray()
                arySelectedTerms =   getMutableDictionaryFromNSManagedObjectArray(ary: arrServiceTermsDetail)
            }
        }
        var strName = ""
        
        for item in arySelectedTerms {
            
            if item is NSDictionary {
                
                let dictArrTemp = item as! NSDictionary
                
                let tempArr = dictArrTemp.allKeys as NSArray
                
                var strTemp = ""
                
                if tempArr.contains("TermsId") {
                    
                    strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "TermsId")!)")//"\((item as AnyObject).value(forKey: "TermsId")!)"
                    
                    strName = strName + "\(strTemp),"

                } else if tempArr.contains("termsId") {
                    
                    strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "termsId")!)")
                    
                    strName = strName + "\(strTemp),"

                } else if tempArr.contains("TermsTitle") {
                    
                    strTemp = "\((item as AnyObject).value(forKey: "TermsTitle")!)"
                    
                    strName = strName + "\(strTemp),"

                } else {
                    
                    strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "termsId")!)")
                    
                    strName = strName + "\(strTemp),"

                }
                
            } else {
                
                strName = strName + "\((item as AnyObject).value(forKey: "TermsTitle")!),"
                
            }
            
        }
        
        if strName.count > 0 {
            
            if(strName.last == ","){
                strName = String(strName.dropLast())
            }
            if(strName.last == ","){
                strName = String(strName.dropLast())
            }
            
        }
        
        txtTermsAndCondition.text = strName
        return arySelectedTerms
    }
    
    func getTermsNameViaId(strTermsId : String) -> String {
        
        var termsName = ""
        
        let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
        {
            let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
            for item in arrOfServiceJobDescriptionTemplate
            {
                let dict = item as! NSDictionary
                if ( "\((dict.value(forKey: "ServiceReportTermsAndConditionsId"))!)" == strTermsId )
                {
                    
                    termsName = "\((dict.value(forKey: "TermsTitle"))!)"
                    break
                }
            }
        }
        
        return termsName
        
    }
    
    func fetchSoilDB() {
        
        ary_SoilList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@  &&  isActive == YES &&  isSoil == YES", strWoId,Global().getUserName()))
        
        tv_SoilList.reloadData()

    }
    
    func fetchWoodDB() {
        
        ary_WoodList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ &&  isActive == YES &&  isSoil == NO", strWoId,Global().getUserName()))

        tv_WoodList.reloadData()
    }
    
    func fetchBaitDB() {

        ary_BaitStationList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ &&  isActive == YES &&  isBaitStation == YES", strWoId,Global().getUserName()))
        
        tv_BaitStationList.reloadData()
        
    }
    
    func fetchPhysicalBarrierDB() {
        
        ary_PhysicalBarrierList = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ &&  isActive == YES &&  isBaitStation == NO", strWoId,Global().getUserName()))

        tv_PhysiCalBarrierList.reloadData()
        
    }
    
    func fetchPaymentDetailDB() {
        
        let aryTemp = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAServicePricingExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))
        aryPaymentDetail = NSMutableArray()
        aryPaymentDetail = aryTemp.mutableCopy()as! NSMutableArray
        aryPaymentDetailCombineData = NSMutableArray()
        aryPaymentDetailCombineData = aryTemp.mutableCopy()as! NSMutableArray
        let tax = Double("\((objWorkorderDetail.value(forKey: "tax") ?? ""))")!

        if(aryTemp.count != 0){
            var strBilling_Amount = Double() , strRenewal_Amount = Double()
            for item in aryPaymentDetailCombineData {
                var strAmount = "\((item as AnyObject).value(forKey: "servicePrice")!)"
                
                if strAmount.count == 0 {
                    
                    strAmount = "0.00"
                    
                }
                
                if ((item as AnyObject).value(forKey: "isRenewal") as! Bool) == false{
                    strBilling_Amount =  strBilling_Amount + Double(strAmount)!
                }else{
                    strRenewal_Amount = strRenewal_Amount + Double(strAmount)!
                }
            }
            if(strBilling_Amount != nil){
                strBilling_Amount = strBilling_Amount - tax
            }
            var dict = NSMutableDictionary()
            dict.setValue("Tax ($):", forKey: "serviceName")
            dict.setValue(tax, forKey: "servicePrice")
            dict.setValue(aryPaymentDetailCombineData.count + 1, forKey: "id")
            aryPaymentDetailCombineData.add(dict)
            
            var dict1 = NSMutableDictionary()
            dict1 = NSMutableDictionary()
            dict1.setValue("Billing Amount ($):", forKey: "serviceName")
            dict1.setValue(strBilling_Amount, forKey: "servicePrice")
            dict1.setValue(aryPaymentDetailCombineData.count + 1, forKey: "id")
            aryPaymentDetailCombineData.add(dict1)
            
            var dict2 = NSMutableDictionary()
            dict2 = NSMutableDictionary()
            dict2.setValue("Renewal Amount ($):", forKey: "serviceName")
            dict2.setValue(strRenewal_Amount, forKey: "servicePrice")
            dict2.setValue(aryPaymentDetailCombineData.count + 1, forKey: "id")
            aryPaymentDetailCombineData.add(dict2)
            
            tv_PaymentDetail.reloadData()
            
        }
     
    }
    
    
    
    func setUPheightForTable()  {
        
        // Product Listr Detail Show
        self.height_tv_SoilList.constant = CGFloat(self.ary_SoilList.count * 85) + (self.ary_SoilList.count == 0 ? 150.0 : 75.0)
        self.height_tv_WoodList.constant = CGFloat(self.ary_WoodList.count * 85) + (self.ary_WoodList.count == 0 ? 150.0 : 75.0)
        self.height_tv_BaitStationList.constant = CGFloat(self.ary_BaitStationList.count * 85) + (self.ary_BaitStationList.count == 0 ? 150.0 : 75.0)
        self.height_tv_PhysiCalBarrierList.constant = CGFloat(self.ary_PhysicalBarrierList.count * 85) + (self.ary_PhysicalBarrierList.count == 0 ? 150.0 : 75.0)
      
        // Payment Detail Show
        self.height_tv_PaymentDetail.constant = CGFloat(self.aryPaymentDetailCombineData.count * 80) + (self.aryPaymentDetailCombineData.count == 0 ? 75.0 : 0.0)
        self.tv_PaymentDetail.reloadData()
        
        // Payment Mode Show According paymode selection
        if(self.txtSelectPaymentMOde.tag == 2){
            self.height_viewPayAmount.constant = CGFloat(400)
        }else if (self.txtSelectPaymentMOde.tag == 1 || self.txtSelectPaymentMOde.tag == 3){
            self.height_viewPayAmount.constant = CGFloat(55)
        }else{
            self.height_viewPayAmount.constant = CGFloat(0)
        }
       
        //Calculate All Height
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        let heightTbl = self.height_tv_SoilList.constant + self.height_tv_WoodList.constant + self.height_tv_BaitStationList.constant + self.height_tv_PhysiCalBarrierList.constant + self.height_tv_PaymentDetail.constant + self.height_viewPayAmount.constant
        let heightMain2 =  ((view_TypeOfConstruction.frame.height + view_OtherDetail.frame.height) + (view_Signature.frame.height + view_TermsCondition.frame.height))
        viewContain.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: heightTbl + (heightMain2 + 1100))
        self.scrollView.addSubview(self.viewContain)
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height:  viewContain.frame.height)
    }
    
    //MARK:
    //MARK: -------------Fetch Core Data Functions-------------
    
    func fetchFinalizeReportDetail() {
        
        let arrFinalizeReportDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@", strWoId))

        if arrFinalizeReportDetail.count > 0 {
            
            let objFinalize = arrFinalizeReportDetail[0] as! NSManagedObject
            
            if objFinalize.value(forKey: "isSlab") is Bool {
                
                if objFinalize.value(forKey: "isSlab") as! Bool == true{
                    
                    btnSlab.setImage(UIImage(named: "NPMA_Check"), for: .normal)

                }else{
                    
                    btnSlab.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

                }
                
            }else{
                btnSlab.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if objFinalize.value(forKey: "isBasement") is Bool {
                
                if objFinalize.value(forKey: "isBasement") as! Bool == true{
                    
                    btnBasement.setImage(UIImage(named: "NPMA_Check"), for: .normal)

                }else{
                    
                    btnBasement.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

                }
                
            }else{
                btnBasement.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if objFinalize.value(forKey: "isCrawl") is Bool {
                
                if objFinalize.value(forKey: "isCrawl") as! Bool == true{
                    
                    btnIsCrawl.setImage(UIImage(named: "NPMA_Check"), for: .normal)

                }else{
                    
                    btnIsCrawl.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

                }
                
            }else{
                btnIsCrawl.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            if objFinalize.value(forKey: "isOther") is Bool {
                
                if objFinalize.value(forKey: "isOther") as! Bool == true{
                    
                    btnOther.setImage(UIImage(named: "NPMA_Check"), for: .normal)

                }else{
                    
                    btnOther.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

                }
                
            }else{
                btnOther.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            }
            
            txtCommentFor99B.text = "\(objFinalize.value(forKey: "commentFor99B") ?? "")"
            txtOther.text = "\(objFinalize.value(forKey: "otherComment") ?? "")"

        }else{
            
            btnSlab.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            btnBasement.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            btnOther.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            txtCommentFor99B.text = ""
            txtOther.text = ""
            btnIsCrawl.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

        }
        
    }
    
    func fetchPaymentInfoDetailFromCoreData()
    {
        
        let arryOfWorkOrderPaymentData = getDataFromCoreDataBaseArray(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId))
        
        if arryOfWorkOrderPaymentData.count>0
        {
            let objPaymentInfoData = arryOfWorkOrderPaymentData[0] as! NSManagedObject
            paymentInfoDetail = objPaymentInfoData
            strPaymentModes = paymentInfoDetail.value(forKey: "paymentMode")  as! String
            strPaidAmount = paymentInfoDetail.value(forKey: "paidAmount") as! String
            
            txtAmount.text = strPaidAmount
            
        }
        else
        {
            savePaymentInfoInCoreData()
            fetchPaymentInfoDetailFromCoreData()
        }
        
        if strPaymentModes == "Cash"
        {
            
            txtSelectPaymentMOde.text = "Cash"
            txtSelectPaymentMOde.tag = 1
            strGlobalPaymentMode = "Cash"
            
        }
        else if strPaymentModes == "Check"
        {
            txtSelectPaymentMOde.text = "Check"
            txtSelectPaymentMOde.tag = 2
            strGlobalPaymentMode = "Check"

        }
        else if strPaymentModes == "CreditCard"
        {
            txtSelectPaymentMOde.text = "Credit Card"
            txtSelectPaymentMOde.tag = 3
            strGlobalPaymentMode = "CreditCard"

        }
        else if strPaymentModes == "NoBill"
        {
            txtSelectPaymentMOde.text = "NoBill"
            txtSelectPaymentMOde.tag = 4
            strGlobalPaymentMode = "NoBill"

        }
        else if strPaymentModes == "AutoChargeCustomer"
        {
            txtSelectPaymentMOde.text = "Auto Charge Customer"
            txtSelectPaymentMOde.tag = 6
            strGlobalPaymentMode = "AutoChargeCustomer"

        }
        else if strPaymentModes == "PaymentPending"
        {
            txtSelectPaymentMOde.text = "Payment Pending"
            txtSelectPaymentMOde.tag = 7
            strGlobalPaymentMode = "PaymentPending"

        }
        else if strPaymentModes == "NoCharge"
        {
            txtSelectPaymentMOde.text = "NoCharge"
            txtSelectPaymentMOde.tag = 8
            strGlobalPaymentMode = "NoCharge"

        }
        else
        {
            txtSelectPaymentMOde.text = "Cash"
            txtSelectPaymentMOde.tag = 1
            strGlobalPaymentMode = "Cash"

        }
        
        txtCheck.text =  "\(paymentInfoDetail.value(forKey: "checkNo") ?? "")"
        txtDrivingLicence.text = "\(paymentInfoDetail.value(forKey: "drivingLicenseNo") ?? "")"
        
        let strExpirationDate = "\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")"
        
        if strExpirationDate.count == 0 || strExpirationDate == ""
        {
            
        }
        else
        {
            txtDateForCheck.text = "\(paymentInfoDetail.value(forKey: "expirationDate") ?? "")"
            
        }
        
        
    }
    
    func savePaymentInfoInCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["CheckBackImagePath",
                     "CheckFrontImagePath",
                     "CheckNo",
                     "CreatedBy",
                     "CreatedDate",
                     "DrivingLicenseNo",
                     "ExpirationDate",
                     "ModifiedBy",
                     "ModifiedDate",
                     "PaidAmount",
                     "PaymentMode",
                     "WoPaymentId"]
        
        arrOfValues = ["",
                       "",
                       "",
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "" ,
                       strUserName,
                       global.getCurrentDate(),
                       "",
                       "Cash",
                       ""]
        
        saveDataInDB(strEntity: "PaymentInfoServiceAuto", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func saveTermsAndConditionInCoreData()
    {
        deleteAllRecordsFromDB(strEntity: Entity_WoNPMAServiceTermsExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

        for item in self.arySelectedTermsAndCondition {
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            arrOfKeys = ["companyKey",
                         "createdBy",
                         "createdDate",
                         "departmentId",
                         "modifiedBy",
                         "modifiedDate",
                         "termsId",
                         "termsnConditions",
                         "userName",
                         "woNPMAServiceTermsId",
                         "termsTitle",
                         "isActive",
                         "workorderId"
                         ]
            arrOfValues = [Global().getCompanyKey(),
                           Global().getEmployeeId(),
                           Global().strCurrentDateFormatted("MM/dd/yyyy", ""),
                           "\((item as AnyObject).value(forKey: "DepartmentId")!)",
                           Global().getEmployeeId(),
                           Global().strCurrentDateFormatted("MM/dd/yyyy", ""),
                           "\((item as AnyObject).value(forKey: "ServiceReportTermsAndConditionsId")!)" ,
                           "\((item as AnyObject).value(forKey: "SR_TermsAndConditions")!)",
                           global.getUserName(),
                           Global().getReferenceNumber(),
                           "\((item as AnyObject).value(forKey: "TermsTitle")!)",
                           true,
                           strWoId
                        ]
            saveDataInDB(strEntity: Entity_WoNPMAServiceTermsExtSerDcs, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
 
    func saveDataOnBack()  {
    
    var arrOfKeys = NSMutableArray()
    var arrOfValues = NSMutableArray()
    
    arrOfKeys = ["technicianComment",
    "officeNotes"
    ]
    //strWorkOrderStatusFinal = "InCompleted"
    arrOfValues = [txtTechComment.text,
    txtTechOfficeNote.text
    ]
    
    let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    
    
    if isSuccess
    {
    
    updateModifyDate()
    
    //Update Modify Date In Work Order DB
    updateServicePestModifyDate(strWoId: self.strWoId as String)
    
    }
    
    }
    
    func updateModifyDate()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        var strCurrentDateFormatted = String()
        strCurrentDateFormatted = global.modifyDateService()
        
        arrOfKeys = ["modifyDate"]
        
        arrOfValues = [strCurrentDateFormatted]
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAutoModifyDate", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            
            global.fetchWorkOrderFromDataBaseForService(toUpdateModifydate: strWoId as String, strCurrentDateFormatted)
            
        }
        else
        {
            
            
        }
        
    }
    
    func saveFinalizeReportDetail() {
        
        let arrFinalizeReportDetail = getDataFromCoreDataBaseArray(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@", strWoId))

        if arrFinalizeReportDetail.count > 0 {
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("commentFor99B")//CommentFor99B
            arrOfKeys.add("isBasement")
            arrOfKeys.add("isOther")
            arrOfKeys.add("isSlab")
            arrOfKeys.add("otherComment")
            arrOfKeys.add("isCrawl")

            arrOfValues.add(txtCommentFor99B.text ?? "")//CommentFor99B
            arrOfValues.add(btnBasement.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnOther.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSlab.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(txtOther.text ?? "")//OtherComment
            arrOfValues.add(btnIsCrawl.currentImage == UIImage(named: "NPMA_Check") ? true : false)

            let isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
        }else{
                        
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("workorderId")
            arrOfKeys.add("createdBy")
            arrOfKeys.add("createdDate")
            arrOfKeys.add("modifiedBy")
            arrOfKeys.add("modifiedDate")
            
            arrOfKeys.add("commentFor99B")//CommentFor99B
            arrOfKeys.add("isBasement")
            arrOfKeys.add("isOther")
            arrOfKeys.add("isSlab")
            arrOfKeys.add("woNPMFinalizeReportDetailId")
            arrOfKeys.add("otherComment")
            arrOfKeys.add("isCrawl")


            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strWoId)
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            arrOfValues.add(Global().getEmployeeId())
            arrOfValues.add(Global().strCurrentDateFormatted("MM/dd/yyyy", ""))
            
            arrOfValues.add(txtCommentFor99B.text ?? "")//CommentFor99B
            arrOfValues.add(btnBasement.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnOther.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add(btnSlab.currentImage == UIImage(named: "NPMA_Check") ? true : false)
            arrOfValues.add("")
            arrOfValues.add(txtOther.text ?? "")//OtherComment
            arrOfValues.add(btnIsCrawl.currentImage == UIImage(named: "NPMA_Check") ? true : false)

            deleteAllRecordsFromDB(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, predicate: NSPredicate(format: "workorderId == %@ && userName == %@", strWoId,Global().getUserName()))

            saveDataInDB(strEntity: Entity_WoNPMAFinalizeReportDetailExtSerDc, arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
        }
        
    }
    
    func finalSave(strPaymentMode: String)
    {
        if strGlobalPaymentMode == "Cash" || strGlobalPaymentMode == "Check" || strGlobalPaymentMode == "CreditCard"
        {
            
            let valTextAmount = Float(txtAmount.text!)
            
            if txtAmount.text == "" || txtAmount.text == "0" || txtAmount.text == "00.00" ||  (Double(txtAmount.text!) == nil || valTextAmount == 0  )
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter amount", viewcontrol: self)
            }
                
            else
            {
                if chkCustomerNotPresent == true
                {
                    /*if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                        
                    else*/
                        if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtCheck.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                         
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                    }
                    
                }
                else
                {
                    /*if strTechnicianSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                        
                    }
                    else if strCustomerSign.count == 0
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                        
                    }
                    else*/
                        if checkImage() == true
                    {
                        showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                    }
                    else
                    {
                        
                        if strGlobalPaymentMode == "Check"
                        {
                            if (txtCheck.text == "" )
                            {
                                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter check no", viewcontrol: self)
                            }
                            else
                            {
                                updatePaymentInfo()
                                updateWorkOrderDetail()
                            }
                        }else{
                            
                            updatePaymentInfo()
                            updateWorkOrderDetail()
                            
                        }
                        
                    }
                }
            }
            
        }
        else if strGlobalPaymentMode == "Bill" || strGlobalPaymentMode == "NoCharge" || strGlobalPaymentMode == "AutoChargeCustomer" || strGlobalPaymentMode == "PaymentPending"
        {
            if chkCustomerNotPresent == true
            {
                /*if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else*/
                    if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
            else
            {
                /*if strTechnicianSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please Sign the Work Order", viewcontrol: self)
                    
                }
                else if strCustomerSign.count == 0
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take the Customer Signature", viewcontrol: self)
                    
                }
                else*/
                    if checkImage() == true
                {
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please take atleast one after image", viewcontrol: self)
                }
                else
                {
                    updatePaymentInfo()
                    updateWorkOrderDetail()
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Payment Mode", viewcontrol: self)
        }
        
        
    }
    
    func updatePaymentInfo()
    {
        // Update Payment Info
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("paymentMode")
        arrOfKeys.add("paidAmount")
        arrOfKeys.add("checkNo")
        arrOfKeys.add("drivingLicenseNo")
        arrOfKeys.add("expirationDate")
        arrOfKeys.add("checkFrontImagePath")
        arrOfKeys.add("checkBackImagePath")
        
        
        arrOfValues.add(strGlobalPaymentMode)
        arrOfValues.add(txtAmount.text ?? "")
        arrOfValues.add(txtCheck.text ?? "")
        arrOfValues.add(txtDrivingLicence.text ?? "")
        arrOfValues.add(txtDateForCheck.text ?? "")
        arrOfValues.add(strCheckFrontImage)
        arrOfValues.add(strCheckBackImage)
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "PaymentInfoServiceAuto", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess
        {
            
        }
        else
        {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    
    func updateLeadDetail() {
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", strWdoLeadId))
        
        if(arrayLeadDetail.count > 0){
            
            objWdoLeadDetail = arrayLeadDetail[0] as! NSManagedObject
            
            let strLeadStatusGlobal = "\(objWdoLeadDetail.value(forKey: "statusSysName") ?? "")"
            
            let strStageSysName = "\(objWdoLeadDetail.value(forKey: "stageSysName") ?? "")"
            
            if(strLeadStatusGlobal.lowercased() == "complete" && strStageSysName.lowercased() == "won")
            {
                
                
            }else {
                
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                arrOfKeys.add("audioFilePath")
                
                if(strAudioNameSales.count > 0)
                {
                    arrOfValues.add(strAudioNameSales)
                }
                else
                {
                    arrOfValues.add("")
                }
                
                let isSuccess =  getDataFromDbToUpdate(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@",strWdoLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                
                if isSuccess
                {
                    
                    //Update Modify Date In Work Order DB
                    global.updateSalesModifydate(strWdoLeadId as String)
                    
                }
                
            }
            
        }
        
    }
    func updateWorkOrderDetail()
    {
        
        updateLeadDetail()
        saveTermsAndConditionInCoreData()
        saveFinalizeReportDetail()
        
        if strCustomerSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strCustomerSign, image: img_CustomerSign.image!)
        }
        if strTechnicianSign.count > 0
        {
            saveImageDocumentDirectory(strFileName: strTechnicianSign, image: img_TechSign.image!)
        }
        
        // Update Payment Info
        
        if chkCustomerNotPresent == true
        {
            strCustomerSign = ""
        }
        
        
        var strWorkOrderStatusFinal = String()
        strWorkOrderStatusFinal = "Completed"
        //strWorkOrderStatusFinal = "InComplete"
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let date = Date()
        var strTimeOut = String()
        strTimeOut = dateFormatter.string(from: date)
        
        var coordinate = CLLocationCoordinate2D()
        coordinate = Global().getLocation()
        let strTimeOutLat = "\(coordinate.latitude)"
        let strTimeOutLong = "\(coordinate.longitude)"
        
        
        var strResendStatus = String()
        strResendStatus = "0"
        if strWorkOrderStatus == "Incomplete"
        {
            strResendStatus = "0"
        }
        
        
        var isElementIntegraiton = Bool()
        isElementIntegraiton = Bool("\((dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsElementIntegration")) ?? "false")") ?? false
        
        if isElementIntegraiton == true
        {
            
            if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
            {
                strWorkOrderStatusFinal = "InComplete"
            }
            
        }
        
        var strCustomerNotPresent = String()
        
        if chkCustomerNotPresent == false
        {
            strCustomerNotPresent = "false"
        }
        else
        {
            strCustomerNotPresent = "true"
        }
        
        var strPresetStatus = String()
        
        if isPreSetSignGlobal == true
        {
            strPresetStatus = "true"
        }
        else
        {
            strPresetStatus = "false"
        }
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        
        let amount = Double("\(txtAmount.text ?? "0.0")")!
        let taxPercentLocal = Double("\((objWorkorderDetail.value(forKey: "taxPercent") ?? ""))")!
        
        let taxPercentAmount = (amount * taxPercentLocal) / 100
        
        arrOfKeys = ["technicianSignaturePath",
                     "customerSignaturePath",
                     "invoiceAmount",
                     "tax",
                     "audioFilePath",
                     "technicianComment",
                     "officeNotes",
                     "timeOut",
                     "timeOutLatitude",
                     "timeOutLongitude",
                     "isResendInvoiceMail",
                     "workorderStatus",
                     "isCustomerNotPresent",
                     "IsRegularPestFlow",
                     "isEmployeePresetSignature"
        ]
        //strWorkOrderStatusFinal = "InCompleted"
        arrOfValues = [strTechnicianSign,
                       strCustomerSign,
                       (txtAmount.text?.count)! > 0 ? txtAmount.text! : "0.0",
                       "\(taxPercentAmount)",
                       strAudioName,
                       txtTechComment.text,
                       txtTechOfficeNote.text,
                       strTimeOut,
                       strTimeOutLat,
                       strTimeOutLong,
                       strResendStatus,
                       strWorkOrderStatusFinal,
                       strCustomerNotPresent,
                       "true",
                       strPresetStatus
            
            
        ]
        
        
        let isSuccess =  getDataFromDbToUpdate(strEntity: "WorkOrderDetailsService", predicate: NSPredicate(format: "workorderId == %@", strWoId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        if isSuccess
        {
            if yesEditedSomething
            {
                updateModifyDate()
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            if isElementIntegraiton == true
            {
                if strGlobalPaymentMode == "CreditCard" || strGlobalPaymentMode == "Credit Card"
                {
                    print("Go to credit card ciew")
                    goToCreditCardScreen()
                }
                else
                {
                    print("Go to send mail")
                    goToSendEmailScreen()
                }
            }
            else
            {
                print("Go to send mail")
                goToSendEmailScreen()
            }
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: alertSomeError, viewcontrol: self)
            
        }
    }
    //MARK:
    //MARK: -------------Actions-------------

    
    @IBAction func action_Back(_ sender: Any) {
        self.view.endEditing(true)
        
        // Save Office Notes and Comments On Back in DB
        
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {
            
        }else{
            
            saveDataOnBack()
            
        }
        
        self.back()
        
    }
    //--TypeOfConstruction
   
    @IBAction func action_CheckBoxTypeOfConstruction(_ sender: UIButton) {
      
        self.view.endEditing(true)
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
        }

    }

    //--ProductList

  
    @IBAction func action_AddProduct(_ sender: Any) {
        self.view.endEditing(true)

        self.goToAddProductScreen(strForUpdate: "" , strId: "" , strType: "")
        
    }
    
    //--OtherDetail
    
    //--PaymentMode
    @IBAction func action_SelectPaymentMethod(_ sender: Any) {
        self.view.endEditing(true)
        var aryPaymetMode = NSMutableArray()
        aryPaymetMode = [["title":"Cash","id":"1","SysName":"Cash"],["title":"Check","id":"2","SysName":"Check"],["title":"Credit Card","id":"3","SysName":"CreditCard"],["title":"Bill","id":"4","SysName":"Bill"],["title":"Auto Charge Customer","id":"6","SysName":"AutoChargeCustomer"],["title":"Payment Pending","id":"7","SysName":"PaymentPending"],["title":"No Charge","id":"8","SysName":"NoCharge"]]
        self.openTableViewPopUp(tag: 511, ary: aryPaymetMode , aryselectedItem: NSMutableArray())
    }
    @IBAction func action_SelectDateForPaymentMOde(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.tag = sender.tag
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var fromDate = Date()
        fromDate = dateFormatter.date(from:((txtDateForCheck.text!.count) > 0 ? txtDateForCheck.text! : result))!
        gotoDatePickerView(sender: sender, strType: "Date" , dateToSet: fromDate)
    }
    @IBAction func action_CheckFrontImage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imagePicker.view.tag = 1
        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
            alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = self.img_TechSign.image!
                self.present(testController!, animated: false, completion: nil)
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(alert, animated: true, completion: { })
    }
    
    @IBAction func action_CheckBackImage(_ sender: UIButton) {
        self.view.endEditing(true)
        self.imagePicker.view.tag = 2

        let alert = UIAlertController(title: "", message: "Please select an option", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
            alert.addAction(UIAlertAction(title: "View Image", style: .default , handler:{ (UIAlertAction)in
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC
                testController!.img = self.img_CustomerSign.image!
                self.present(testController!, animated: false, completion: nil)
            }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.up
        }
        self.present(alert, animated: true, completion: { })
    }
    
    //--PaymentDetail
    
    //--Signature
    @IBAction func action_Check_CusNotPresent(_ sender: UIButton) {
        
        yesEditedSomething = true

        self.view.endEditing(true)
        if(sender.currentImage == UIImage(named: "NPMA_Check")){
            sender.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
            img_CustomerSign.isHidden = false
            btn_CustomerSign.isHidden = false
        }else{
            sender.setImage(UIImage(named: "NPMA_Check"), for: .normal)
            img_CustomerSign.isHidden = true
            btn_CustomerSign.isHidden = true
            img_CustomerSign.image = UIImage()
        }
        
    }
    
    @IBAction func action_Browse_TechSign(_ sender: UIButton) {
        
        self.view.endEditing(true)

        yesEditedSomething = true
        signatureType = "technician"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func action_Browse_CustomerSign(_ sender: UIButton) {
        
        self.view.endEditing(true)

        yesEditedSomething = true
        signatureType = "customer"
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        let vc = storyboardIpad.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        vc!.strMessage = strEmpName
        vc!.delegate = self
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    
    //--Terms&Condition
    @IBAction func action_SelectTermsAndCondition(_ sender: UIButton) {
        self.view.endEditing(true)
        let aryTermsAndCondition = NSMutableArray()
        let dictData = nsud.value(forKey: "MasterServiceAutomation") as! NSDictionary
        if dictData.value(forKey: "ServiceReportTermsAndConditions") is NSArray
        {
            let arrOfServiceJobDescriptionTemplate = dictData.value(forKey: "ServiceReportTermsAndConditions") as! NSArray
            for item in arrOfServiceJobDescriptionTemplate
            {
                let dict = item as! NSDictionary
                if ( "\((dict.value(forKey: "DepartmentId"))!)" == "\((objWorkorderDetail.value(forKey: "departmentId"))!)" )
                {
                    aryTermsAndCondition.add(item)
                }
            }
        }
      
        self.openTableViewPopUp(tag: 520, ary: aryTermsAndCondition , aryselectedItem: self.arySelectedTermsAndCondition)
        }
   
    @IBAction func actionOnAudio(_ sender: Any)
    {
        yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {
            
        }else{
            alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                testController?.strFromWhere = flowTypeWdoSalesService
                testController?.modalPresentationStyle = .fullScreen
                self.present(testController!, animated: false, completion: nil)
            }))
        }
        alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
            var audioName = nsud.value(forKey: "AudioNameService")
            audioName = self.strAudioName
            let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
            let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
            testController!.strAudioName = audioName as! String
            testController?.modalPresentationStyle = .fullScreen
            self.present(testController!, animated: false, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in }))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: { })
    }
    
    //MARK:
    //MARK: -------------Other Function-------------
    
    func goToAddProductScreen(strForUpdate : String , strId : String , strType : String) {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "NPMA" : "NPMA", bundle: Bundle.main).instantiateViewController(withIdentifier: "NPMA_FR_AddProductVC") as? NPMA_FR_AddProductVC
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.modalTransitionStyle = .coverVertical
        vc!.delegate = self
        vc?.strWoId = strWoId
        vc?.strIdToUpdate = strId
        vc?.strViewFor = strForUpdate
        vc?.strType = strType
        self.present(vc!, animated: false, completion: nil)
        
    }
    
    func goToCreditCardScreen()
    {
        
        if isInternetAvailable() == true
        {
            let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
            let objCreditCardView = storyboardIpad.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
            
            var strValue = String()
            if chkCustomerNotPresent == true
            {
                strValue = "no"
            }
            else
            {
                strValue = "yes"
            }
            objCreditCardView?.isCustomerPresent = strValue as NSString
            objCreditCardView?.strAmount = txtAmount.text
            objCreditCardView?.strGlobalWorkOrderId = strWoId as String
            objCreditCardView?.strTypeOfService = "service"
            objCreditCardView?.workOrderDetailNew = objWorkorderDetail
            objCreditCardView?.strFromWhichView = "NpmaFlow"
            objCreditCardView?.fromWhere = "NpmaTermite"
            self.navigationController?.pushViewController(objCreditCardView!, animated: false)
            
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
        
    }
    
    func goToSendEmailScreen()
    {
        
        //let strTimeInFromDb = "\(objWorkorderDetail.value(forKey: "timeIn"))"
        
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objSendMail = storyboardIpad.instantiateViewController(withIdentifier: "ServiceSendMailViewControlleriPad") as? ServiceSendMailViewControlleriPad
        
        var strValue = String()
        var strFromWheree = NSString()
        strFromWheree = "NPMA"
        
        if chkCustomerNotPresent == true
        {
            strValue = "no"
        }
        else
        {
            strValue = "yes"
        }
        objSendMail?.isCustomerPresent = strValue as NSString
        objSendMail?.fromWhere = strFromWheree  as NSString as String
        objSendMail?.strWoIdd = strWoId as String
        objSendMail?.strHeaderValue = "NPMA SERVICE REPORT"
        //self.navigationController?.pushViewController(objSendMail!, animated: false)
        self.navigationController?.pushViewController(objSendMail!, animated: false)
        
    }
    func callOnLoad() {
        
        lblHeaderTitle.text = "\(nsud.value(forKey: "lblName") ?? "")"
        
        btn_CustomerNotPresent.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)

        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strEmpID = "\(dictLoginData.value(forKey: "EmployeeId") ?? "")"
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        let strServiceReportType = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportType"))" as String
        
        if strServiceReportType == "CompanyEmail"
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.ServiceReportEmail")!)"
        }
        else
        {
            strFromEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        }
        
    }
    
    func back() {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "ApplicationRateName", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String , dateToSet:Date)  {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: DateTimePicker = storyboardIpad.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.chkForMinDate = false
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDateSelectionForDatePickerProtocol = self
        vc.strType = strType
        vc.dateToSet = dateToSet
        self.present(vc, animated: true, completion: {})
    }
    
    func assignValues()
    {
                
        strWorkOrderStatus = "\(objWorkorderDetail.value(forKey: "workorderStatus") ?? "")"
        
        txtTechOfficeNote.text = "\(objWorkorderDetail.value(forKey: "officeNotes") ?? "")"
        txtTechComment.text = "\(objWorkorderDetail.value(forKey: "technicianComment") ?? "")"

        let strCustomerNotPresent = "\(objWorkorderDetail.value(forKey: "isCustomerNotPresent") ?? "" )"
        if strCustomerNotPresent == "true" || strCustomerNotPresent == "1"
        {
            chkCustomerNotPresent = true
            
            btn_CustomerNotPresent.setImage(UIImage (named: "NPMA_Check"), for: .normal)
            img_CustomerSign.isHidden = true
            btn_CustomerSign.isHidden = true
            
        }
        else
        {
            chkCustomerNotPresent = false
            btn_CustomerNotPresent.setImage(UIImage (named: "NPMA_UnCheck"), for: .normal)
            img_CustomerSign.isHidden = false
            btn_CustomerSign.isHidden = false
            //check_box_1New.png
        }
        
        
        strCustomerSign = "\(objWorkorderDetail.value(forKey: "customerSignaturePath") ?? "")"
        strTechnicianSign = "\(objWorkorderDetail.value(forKey: "technicianSignaturePath") ?? "")"
        
        strAudioName = "\(objWorkorderDetail.value(forKey: "audioFilePath") ?? "")"
        
        let strTimeIn = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a") as String
        if strTimeIn.count > 0
        {
            txtTimein.text = "Time In: " + strTimeIn
            
        }
        else
        {
            txtTimein.text = "Time In: " + "\(objWorkorderDetail.value(forKey: "timeIn") ?? "")"
            
        }
        
        let strTimeOut = changeStringDateToGivenFormat(strDate: "\(objWorkorderDetail.value(forKey: "timeOut") ?? "")", strRequiredFormat: "MM/dd/yyyy hh:mm a") as String
        
        if strTimeOut.count > 0
        {
            txtTimeOut.text = "Time Out: " + strTimeOut
            
        }
        else
        {
            txtTimeOut.text = "Time Out: " + "\(objWorkorderDetail.value(forKey: "timeOut") ?? "")"
        }
        
        var total = Double()
        total = Double("\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "previousBalance") ?? ""))")! + Double("\((objWorkorderDetail.value(forKey: "tax") ?? ""))")!
        
        //lblChargesOfCurrentServices.text = "Charges for current services: " + "$" +  "\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))"
        
        //lblTaxValue.text = "Tax: " + "$" + "\((objWorkorderDetail.value(forKey: "tax") ?? ""))"
        
        //lblTotalDueValue.text = "Total Due: " + "$" + "\(total)"
        
        txtAmount.text = ("\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))")
        
        calculateTotalDue()
        
    }
    
    func calculateTotalDue() {
        
        var total = Double()
        
        let amount = Double("\(txtAmount.text ?? "0.0")")!
        let taxPercentLocal = Double("\((objWorkorderDetail.value(forKey: "taxPercent") ?? ""))")!

        let taxPercentAmount = (amount * taxPercentLocal) / 100
        
        total = Double("\(txtAmount.text ?? "0.0")")! + Double("\((objWorkorderDetail.value(forKey: "previousBalance") ?? ""))")! + Double("\(taxPercentAmount)")!
        
        //lblChargesOfCurrentServices.text = "Charges for current services: " + "$" +  "\((objWorkorderDetail.value(forKey: "invoiceAmount") ?? ""))"
                
        let strTaxPercent = String(format: "%.02f", taxPercentAmount)
        
        //lblTaxValue.text = "Tax: " + "$" + "\((strTaxPercent))"
        
        let strTotalLocal = String(format: "%.02f", total)

        //lblTotalDueValue.text = "Total Due: " + "$" + "\(strTotalLocal)"
        
        
    }
    

    func downloadCustomerSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            img_CustomerSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            
            let nsUrl = URL(string: strUrl)
            img_CustomerSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func downloadTechnicianSign(strImageName: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            img_TechSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            strUrl = strUrl + "//Documents/" + strImageName
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            
            let nsUrl = URL(string: strUrl)
            img_TechSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    func downloadTechnicianPreset(strImageName: String, stringUrl: String)
    {
        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImageName)
        
        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImageName)
        
        if isImageExists!
        {
            img_TechSign.image = image
        }
        else
        {
            //Working // imgViewCustinerSignature.dowloadFromServer(link: strUrl, contentMode: .scaleAspectFill)
            var strUrl = String()
            strUrl = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"
            
            //strUrl = strUrl + "//Documents/" + strImageName
            strUrl = stringUrl
            
            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
            
            let nsUrl = URL(string: strUrl)
            
            img_TechSign.load(url: nsUrl! as URL , strImageName: strImageName)
        }
    }
    
    func methodForDisable()
    {
        //txtViewTermsCondition.isEditable = false
        
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)
        {
            
            btn_CustomerSign.isEnabled = false
            btn_TechSign.isEnabled = false
            txtTechComment.isEditable = false
            txtTechOfficeNote.isEditable = false
            txtAmount.isEnabled = false
            txtCheck.isEnabled = false
            txtDrivingLicence.isEnabled = false
            txtDateForCheck.isEnabled = false
            btnCheckFrontImage.isEnabled = false
            btnCheckBackImage.isEnabled = false
            btn_CustomerNotPresent.isEnabled = false
            txtAmount.isEnabled = false
            
        }
        else
        {
            btn_CustomerSign.isEnabled = true
            btn_TechSign.isEnabled = true
            txtTechComment.isEditable = true
            txtTechOfficeNote.isEditable = true
            txtAmount.isEnabled = true
            txtCheck.isEnabled = true
            txtDrivingLicence.isEnabled = true
            txtDateForCheck.isEnabled = true
            btnCheckFrontImage.isEnabled = true
            btnCheckBackImage.isEnabled = true
            btn_CustomerNotPresent.isEnabled = true
            txtAmount.isEnabled = true
        }
        let isPreSetSign = nsud.bool(forKey: "isPreSetSignService")
        
        var strSignUrl = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
        strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
        if ((isPreSetSign) && (strSignUrl.count>0) && !(WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail)))
        {

            btn_TechSign.isEnabled = false
            
        }
        
    }
    
    func checkImage() -> Bool
    {
        
        let chkImage = nsud.bool(forKey: "isCompulsoryAfterImageService")
        
        if chkImage == true
        {
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "ImageDetailsServiceAuto", predicate: NSPredicate(format: "workorderId == %@ && woImageType == %@", strWoId, "After"))
            
            if arryOfData.count > 0
            {
                return false
            }
            else
            {
                return true
            }
            
        }
        else
        {
            
            return false
            
        }
        
    }
    
    func fetchServiceDetail()
    {
        let arrServiceSysName = NSMutableArray()
        let arrServiceName = NSMutableArray()
        let arrServiceId = NSMutableArray()

        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMaster.value(forKey: "Categories") is NSArray
        {
            let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
            
            for itemCategory in arrCategory
            {
                let dictCategory = itemCategory as! NSDictionary
                
                if dictCategory.value(forKey: "Services") is NSArray
                {
                    let arrServices = dictCategory.value(forKey: "Services") as! NSArray
                    
                    for itemService in arrServices
                    {
                        let dictServiceTemp = itemService as! NSDictionary

                        arrServiceSysName.add("\(dictServiceTemp.value(forKey: "SysName") ?? "")")
                        arrServiceName.add("\(dictServiceTemp.value(forKey: "Name") ?? "")")
                        arrServiceId.add("\(dictServiceTemp.value(forKey: "ServiceMasterId") ?? "")")
                    }
                }
            }
        }
        
        dictServiceNameFromId = NSDictionary(objects:arrServiceName as! [Any], forKeys:arrServiceId as! [NSCopying]) as NSDictionary

    }
    // MARK: -
    // MARK: ----------Footer Button Action---------
    
    @IBAction func action_SaveContinue(_ sender: UIButton){
                
        self.view.endEditing(true)
        
        if (WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail))
        {

            goToSendEmailScreen()
            
        }
        else
        {
            
            finalSave(strPaymentMode: strGlobalPaymentMode)
            
        }
        
    }
    
    @IBAction func actionOnServiceDoc(_ sender: Any)
    {
        goToServiceDocument()
    }
    
    @IBAction func actionOnClock(_ sender: Any)
    {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {

            let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
            let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
            self.navigationController?.pushViewController(clockInOutVC!, animated: false)
        }
        
    }
    
    @IBAction func action_Image(_ sender: Any) {
                
        self.goToGlobalmage(strType: "After")
        
    }
    
    @IBAction func action_ServiceHistory(_ sender: Any) {
        
        self.goToServiceHistory()
        
    }
    
    @IBAction func action_CustomerSalesDocument(_ sender: Any) {
        
        self.goToCustomerSalesDocuments()
        
    }
    
    @IBAction func action_Graph(_ sender: Any) {
        
        self.goToGlobalmage(strType: "Graph")
        
    }
    
    @IBAction func action_NotesHistory(_ sender: Any) {
        
        self.goToNotesHistory()
        
    }
    
    //MARK: - -----------------------------------Footer Functions -----------------------------------
    
    func goToServiceDocument() {
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        
    }
    
    func goToServiceHistory()  {
                
        //UserDefaults.standard.set(true, forKey: "RefreshGraph_ProblemAdd")

        let storyboardIpad = UIStoryboard.init(name: "WDOiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistory_iPadVC") as? ServiceHistory_iPadVC
        testController?.strGlobalWoId = strWoId as String
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
        /*let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "PestNewFlow"
        self.present(testController!, animated: false, completion: nil)*/
        
    }
    
    func goToNotesHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceNotesHistoryViewController") as? ServiceNotesHistoryViewController
        testController?.strWoId = "\(strWoId)"
        testController?.strWorkOrderNo = "\(objWorkorderDetail.value(forKey: "workOrderNo") ?? "")"
        testController?.isFromWDO = "YES"
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToCustomerSalesDocuments()
    {
        
        let storyboardIpad = UIStoryboard.init(name: "ServiceiPad", bundle: nil)
        let objServiceDocumentsVC = storyboardIpad.instantiateViewController(withIdentifier: "ServiceDocumentsViewControlleriPad") as? ServiceDocumentsViewControlleriPad
        objServiceDocumentsVC?.strAccountNo = "\(objWorkorderDetail.value(forKey: "accountNo") ?? "")"
        //self.navigationController?.pushViewController(objServiceDocumentsVC!, animated: false)
        self.present(objServiceDocumentsVC!, animated: false, completion: nil)
        // Commit
    }
    
    func goToGlobalmage(strType : String)  {
        
        let storyboardIpad = UIStoryboard.init(name: "PestiPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "GlobalImageiPadVC") as? GlobalImageVC
        testController?.strHeaderTitle = strType as NSString
        testController?.strWoId = strWoId
        testController?.strWoLeadId = strWdoLeadId as NSString
        testController?.strModuleType = flowTypeWdoSalesService as NSString
        if WebService().wdoWorkOrderStatus(woObj: objWorkorderDetail) {
            testController?.strWoStatus = "Completed"
        }else{
            testController?.strWoStatus = "InComplete"
        }
        self.present(testController!, animated: false, completion: nil)
        
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NPMA_FinlizedReportVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tv_SoilList){
            return ary_SoilList.count
        } else if(tableView == tv_WoodList){
            return ary_WoodList.count
        }else if(tableView == tv_BaitStationList){
            return ary_BaitStationList.count
        }else if(tableView == tv_PhysiCalBarrierList){
            return ary_PhysicalBarrierList.count
        }else if(tableView == tv_PaymentDetail){
            return aryPaymentDetailCombineData.count
        }
        return  0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        //-------SoilList---------

        if(tableView == tv_SoilList){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "FR_SoilList" : "FR_SoilList", for: indexPath as IndexPath) as! FinalizeReportCell
            
            let dict = ary_SoilList.object(at: indexPath.row)as! NSManagedObject
            
            let strProductMasterId = "\(dict.value(forKey: "productMasterId") ?? "")"
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductId") ?? "")" == strProductMasterId {
                        
                        cell.FR_Soil_lbl_Product.text = "\(dictData.value(forKey: "ProductName")!)"

                        break
                        
                    }
                    
                }
                
            }
            
            cell.FR_Soil_lbl_Qty.text = "\(dict.value(forKey: "quantity")!)"

            cell.FR_Soil_lbl_EPAReg.text = "\(dict.value(forKey: "ePARegistrationNumber")!)"
            cell.FR_Soil_lbl_Dilution.text = "\(dict.value(forKey: "concentration")!)"
            
            cell.FR_Soil_btn_Primary.setImage(UIImage(named: dict.value(forKey: "isPrimary") as! Bool == true ? "NPMA_Radio_Yes" : "NPMA_Radio_No"), for: .normal)

            cell.FR_Soil_btn_Primary.tag = indexPath.row
            cell.FR_Soil_btn_Primary.addTarget(self, action: #selector(actionSoilListPrimary), for: .touchUpInside)
          
            cell.FR_Soil_btn_Dlt.tag = indexPath.row
            cell.FR_Soil_btn_Dlt.addTarget(self, action: #selector(actionSoilListDelete), for: .touchUpInside)
            
            cell.FR_Soil_btn_Edit.tag = indexPath.row
            cell.FR_Soil_btn_Edit.addTarget(self, action: #selector(actionSoilListEdit), for: .touchUpInside)
            
            
           
            
            
            return cell
        }
        //------- WoodList---------

        else if(tableView == tv_WoodList){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "FR_WoodList" : "FR_WoodList", for: indexPath as IndexPath) as! FinalizeReportCell
            
            let dict = ary_WoodList.object(at: indexPath.row)as! NSManagedObject
            
            let strProductMasterId = "\(dict.value(forKey: "productMasterId") ?? "")"
            
            var array = NSMutableArray()

            array = getDataFromServiceAutoMasters(strTypeOfMaster: "ProductMasters" , strBranchId: objWorkorderDetail.value(forKey: "branchId") as! String)

            if array.count > 0 {
                
                for k in 0 ..< array.count {
                    
                    let dictData = array[k] as! NSDictionary
                    
                    if "\(dictData.value(forKey: "ProductId") ?? "")" == strProductMasterId {
                        
                        cell.FR_Wood_lbl_Product.text = "\(dictData.value(forKey: "ProductName")!)"

                        break
                        
                    }
                    
                }
                
            }
            
            cell.FR_Wood_lbl_Qty.text = "\(dict.value(forKey: "quantity")!)"
            cell.FR_Wood_lbl_EPAReg.text = "\(dict.value(forKey: "ePARegistrationNumber")!)"
            cell.FR_Wood_lbl_Dilution.text = "\(dict.value(forKey: "concentration")!)"

            
            cell.FR_Wood_btn_Primary.setImage(UIImage(named: dict.value(forKey: "isPrimary") as! Bool == true ? "NPMA_Radio_Yes" : "NPMA_Radio_No"), for: .normal)

           
            cell.FR_Wood_btn_Primary.tag = indexPath.row
            cell.FR_Wood_btn_Primary.addTarget(self, action: #selector(actionWoodListPrimary), for: .touchUpInside)
            
            cell.FR_Wood_btn_Dlt.tag = indexPath.row
            cell.FR_Wood_btn_Dlt.addTarget(self, action: #selector(actionWoodListDelete), for: .touchUpInside)
            
            cell.FR_Wood_btn_Edit.tag = indexPath.row
            cell.FR_Wood_btn_Edit.addTarget(self, action: #selector(actionWoodListEdit), for: .touchUpInside)
       
            return cell
        }
        //------- _BaitStationList---------

        else if(tableView == tv_BaitStationList){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "FR_BaitStationList" : "FR_BaitStationList", for: indexPath as IndexPath) as! FinalizeReportCell
            
            let dict = ary_BaitStationList.object(at: indexPath.row)as! NSManagedObject
            
            cell.FR_Bait_lbl_nameOfSystem.text = "\(dict.value(forKey: "nameOfSystem")!)"
            cell.FR_Bait_lbl_EPAReg.text = "\(dict.value(forKey: "ePARegistrationNumber")!)"
            cell.FR_Bait_lbl_StationInstall.text = "\(dict.value(forKey: "numberOfStationInstalled")!)"
            cell.FR_Bait_btn_Primary.setImage(UIImage(named: dict.value(forKey: "isPrimary") as! Bool == true ? "NPMA_Radio_Yes" : "NPMA_Radio_No"), for: .normal)
            cell.FR_Bait_btn_Primary.tag = indexPath.row
            cell.FR_Bait_btn_Primary.addTarget(self, action: #selector(actionBaitStationListPrimary), for: .touchUpInside)
            
            cell.FR_Bait_btn_Dlt.tag = indexPath.row
            cell.FR_Bait_btn_Dlt.addTarget(self, action: #selector(actionBaitStationListDelete), for: .touchUpInside)
            
            cell.FR_Bait_btn_Edit.tag = indexPath.row
            cell.FR_Bait_btn_Edit.addTarget(self, action: #selector(actionBaitStationListEdit), for: .touchUpInside)
          
            return cell
        }
       //------- PhysiCalBarrierList---------
        else if(tableView == tv_PhysiCalBarrierList){
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "FR_PhysicalBarrier" : "FR_PhysicalBarrier", for: indexPath as IndexPath) as! FinalizeReportCell
            
            let dict = ary_PhysicalBarrierList.object(at: indexPath.row)as! NSManagedObject
            
            cell.FR_PhysicalBarrier_lbl_nameOfSystem.text = "\(dict.value(forKey: "nameOfSystem")!)"
            
            cell.FR_PhysicalBarrier_btn_Primary.setImage(UIImage(named: dict.value(forKey: "isPrimary") as! Bool == true ? "NPMA_Radio_Yes" : "NPMA_Radio_No"), for: .normal)
            
            cell.FR_PhysicalBarrier_btn_Primary.tag = indexPath.row
            cell.FR_PhysicalBarrier_btn_Primary.addTarget(self, action: #selector(actionPhysiCalBarrierListPrimary), for: .touchUpInside)
            
            cell.FR_PhysicalBarrier_btn_Dlt.tag = indexPath.row
            cell.FR_PhysicalBarrier_btn_Dlt.addTarget(self, action: #selector(actionPhysiCalBarrierListDelete), for: .touchUpInside)
            
            cell.FR_PhysicalBarrier_btn_Edit.tag = indexPath.row
            cell.FR_PhysicalBarrier_btn_Edit.addTarget(self, action: #selector(actionPhysiCalBarrierListEdit), for: .touchUpInside)
            
            return cell
        }
        //------- PaymentDetail---------
         else {
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "FR_PaymentDetail" : "FR_PaymentDetail", for: indexPath as IndexPath) as! FinalizeReportCell
            
            var strServiceName = ""
            var strServiceAmount = ""
            
            if aryPaymentDetailCombineData.object(at: indexPath.row) is NSManagedObject {
                
                let dict = aryPaymentDetailCombineData.object(at: indexPath.row)as! NSManagedObject
                strServiceName = "\(dict.value(forKey: "serviceName")!)"
                
                if strServiceName.count == 0 {
                    
                    strServiceName = "\(dictServiceNameFromId.value(forKey: "\(dict.value(forKey: "serviceId") ?? "")") ?? "")"
                    
                }
                
                strServiceAmount = "\(dict.value(forKey: "servicePrice")!)"
                
            } else {
                
                let dict = aryPaymentDetailCombineData.object(at: indexPath.row)as! NSDictionary
                strServiceName = "\(dict.value(forKey: "serviceName")!)"
                
                if strServiceName.count == 0 {
                    
                    strServiceName = "\(dictServiceNameFromId.value(forKey: "\(dict.value(forKey: "serviceId") ?? "")") ?? "")"
                    
                }
                strServiceAmount = "\(dict.value(forKey: "servicePrice")!)"
                
            }
            
            cell.lbl_ServiceName.text = strServiceName
            cell.txt_PayMentDetail.text = strServiceAmount
            cell.txt_PayMentDetail.placeholder = strServiceName

            return cell
         }
    
    }
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if(tableView == tv_SoilList){
            return "Please (+)Add Product"
        } else if(tableView == tv_WoodList){
            return "Please (+)Add Product"
        }else if(tableView == tv_BaitStationList){
            return "Please (+)Add Product"
        }else if(tableView == tv_PhysiCalBarrierList){
            return "Please (+)Add Product"
        }else if(tableView == tv_PaymentDetail){
            return "No Payment Detail"
        }
       return ""
    }
     func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textAlignment = .center
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = UIColor.lightGray
        (view as? UITableViewHeaderFooterView)?.backgroundColor = UIColor.white
    }
  
    
   
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if(tableView == tv_SoilList){
            return ary_SoilList.count == 0 ? 80.0 : 0.0
        } else if(tableView == tv_WoodList){
            return ary_WoodList.count  == 0 ? 80.0 : 0.0
        }else if(tableView == tv_BaitStationList){
            return ary_BaitStationList.count  == 0 ? 80.0 : 0.0
        }else if(tableView == tv_PhysiCalBarrierList){
            return ary_PhysicalBarrierList.count  == 0 ? 80.0 : 0.0
        }else if(tableView == tv_PaymentDetail){
            return aryPaymentDetail.count  == 0 ? 80.0 : 0.0
        }
        return 0
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == tv_PaymentDetail ? 80 : 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: -
    // MARK: ----------SoilList Action---------
    @objc func actionSoilListPrimary(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertChangeStatusMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
           
            let dict = self.ary_SoilList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isPrimary") //"radio_check_ipad" : "radio_uncheck_ipad"
            if(sender.currentImage == UIImage(named: "NPMA_Radio_Yes")){
                arrOfValues.add(false)
            }else{
                arrOfValues.add(true)
            }
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchSoilDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionSoilListDelete(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
           
            let dict = self.ary_SoilList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchSoilDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionSoilListEdit(sender : UIButton) {
        self.view.endEditing(true)
        
        let dict = self.ary_SoilList.object(at: sender.tag)as! NSManagedObject
        
        let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
        
        goToAddProductScreen(strForUpdate: "EDIT" , strId: strWoNPMAProductDetailId , strType: "Soil")
        
    }
    // MARK: -
    // MARK: ----------WoodList Action---------
    @objc func actionWoodListPrimary(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertChangeStatusMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
           
            let dict = self.ary_WoodList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isPrimary")
            if(sender.currentImage == UIImage(named: "NPMA_Radio_Yes")){
                arrOfValues.add(false)
            }else{
                arrOfValues.add(true)
            }
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchWoodDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionWoodListDelete(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
           
            let dict = self.ary_WoodList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForSoilAndWoodExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchWoodDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionWoodListEdit(sender : UIButton) {
        self.view.endEditing(true)
        
        let dict = self.ary_WoodList.object(at: sender.tag)as! NSManagedObject
        
        let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
        
        goToAddProductScreen(strForUpdate: "EDIT" , strId: strWoNPMAProductDetailId , strType: "Wood")
        
    }
    // MARK: -
    // MARK: ----------_BaitStationList Action---------
    @objc func actionBaitStationListPrimary(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertChangeStatusMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
           
            let dict = self.ary_BaitStationList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isPrimary")
            if(sender.currentImage == UIImage(named: "NPMA_Radio_Yes")){
                arrOfValues.add(false)
            }else{
                arrOfValues.add(true)
            }
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchBaitDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionBaitStationListDelete(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
           
            let dict = self.ary_BaitStationList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchBaitDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionBaitStationListEdit(sender : UIButton) {
        self.view.endEditing(true)
        
        let dict = self.ary_BaitStationList.object(at: sender.tag)as! NSManagedObject
        
        let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
        
        goToAddProductScreen(strForUpdate: "EDIT" , strId: strWoNPMAProductDetailId , strType: "Bait")
    }
    // MARK: -
    // MARK: ----------PhysiCalBarrier List Action---------
    @objc func actionPhysiCalBarrierListPrimary(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertChangeStatusMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Change", style: .default, handler: { action in
           
            let dict = self.ary_PhysicalBarrierList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isPrimary")
            if(sender.currentImage == UIImage(named: "NPMA_Radio_Yes")){
                arrOfValues.add(false)
            }else{
                arrOfValues.add(true)
            }
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchPhysicalBarrierDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionPhysiCalBarrierListDelete(sender : UIButton) {
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: Alert, message: AlertDeleteDataMsg, preferredStyle: .alert)
        alert.view.tintColor = UIColor.black
      

        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
           
            let dict = self.ary_PhysicalBarrierList.object(at: sender.tag)as! NSManagedObject
            
            let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
            
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            arrOfKeys.add("isActive")
            arrOfValues.add(false)
            
            var isSuccess = false
            
            isSuccess = getDataFromDbToUpdate(strEntity: Entity_WoNPMAProductDetailForBaitAndPhysicalBarrierExtSerDcs, predicate: NSPredicate(format: "workorderId == %@ && userName == %@ && woNPMAProductDetailId == %@", self.strWoId,Global().getUserName(), strWoNPMAProductDetailId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            if isSuccess {
                
                //Update Modify Date In Work Order DB
                updateServicePestModifyDate(strWoId: self.strWoId as String)
                
            }
            
            self.fetchPhysicalBarrierDB()
            self.setUPheightForTable()
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        self.present(alert, animated: true)

        
    }
    @objc func actionPhysiCalBarrierListEdit(sender : UIButton) {
        self.view.endEditing(true)
        
        let dict = self.ary_PhysicalBarrierList.object(at: sender.tag)as! NSManagedObject
        
        let strWoNPMAProductDetailId = "\(dict.value(forKey: "woNPMAProductDetailId") ?? "")"
        
        goToAddProductScreen(strForUpdate: "EDIT" , strId: strWoNPMAProductDetailId , strType: "Physical")
    }
}

// MARK: -
// MARK: ----------FinalizeReportCell---------
class FinalizeReportCell: UITableViewCell {
    
    // FR_SoilList
    @IBOutlet weak var FR_Soil_btn_Primary: UIButton!
    @IBOutlet weak var FR_Soil_lbl_Product: UILabel!
    @IBOutlet weak var FR_Soil_lbl_Qty: UILabel!
    @IBOutlet weak var FR_Soil_lbl_EPAReg: UILabel!
    @IBOutlet weak var FR_Soil_lbl_Dilution: UILabel!
    @IBOutlet weak var FR_Soil_btn_Dlt: UIButton!
    @IBOutlet weak var FR_Soil_btn_Edit: UIButton!

    // FR_WoodList
    @IBOutlet weak var FR_Wood_btn_Primary: UIButton!
    @IBOutlet weak var FR_Wood_lbl_Product: UILabel!
    @IBOutlet weak var FR_Wood_lbl_Qty: UILabel!
    @IBOutlet weak var FR_Wood_lbl_EPAReg: UILabel!
    @IBOutlet weak var FR_Wood_lbl_Dilution: UILabel!
    @IBOutlet weak var FR_Wood_btn_Dlt: UIButton!
    @IBOutlet weak var FR_Wood_btn_Edit: UIButton!

    // FR_BaitStationList
    @IBOutlet weak var FR_Bait_btn_Primary: UIButton!
    @IBOutlet weak var FR_Bait_lbl_nameOfSystem: UILabel!
    @IBOutlet weak var FR_Bait_lbl_EPAReg: UILabel!
    @IBOutlet weak var FR_Bait_lbl_StationInstall: UILabel!
    @IBOutlet weak var FR_Bait_btn_Dlt: UIButton!
    @IBOutlet weak var FR_Bait_btn_Edit: UIButton!

    // FR_PhysicalBarrier
    @IBOutlet weak var FR_PhysicalBarrier_btn_Primary: UIButton!
    @IBOutlet weak var FR_PhysicalBarrier_lbl_nameOfSystem: UILabel!
    @IBOutlet weak var FR_PhysicalBarrier_btn_Dlt: UIButton!
    @IBOutlet weak var FR_PhysicalBarrier_btn_Edit: UIButton!

    // FR_PAymentDetail
    @IBOutlet weak var txt_PayMentDetail: ACFloatingTextField!
    @IBOutlet weak var lbl_ServiceName: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: -
// MARK: ------- NPMA_AddConduciveConditionsProtocol---------
extension NPMA_FinlizedReportVC: NPMA_AddFR_ProductProtocol{
    func getDataFromAddFR_Product(dictData: NSDictionary, editIndex: Int, tag: Int, statusForEdit: Bool) {
        
        //Note: Tag = 1 for Soil
        //Note: Tag = 2 for Wood
        //Note: Tag = 3 for BaitStationList
        //Note: Tag = 4 for PhysicalBarrier

        if(statusForEdit){
            if(tag == 1){
                
                self.fetchSoilDB()
                //self.tv_SoilList.reloadData()

            }else if(tag == 2){
                
                self.fetchWoodDB()
                //self.tv_WoodList.reloadData()

            }else if(tag == 3){
                
                self.fetchBaitDB()
                //self.tv_BaitStationList.reloadData()

            }else if(tag == 4){
                
                self.fetchPhysicalBarrierDB()
                //self.tv_PhysiCalBarrierList.reloadData()

            }
        }else{
            if(tag == 1){
                
                self.fetchSoilDB()
                //self.tv_SoilList.reloadData()
            }else if(tag == 2){
                
                self.fetchWoodDB()
                //self.tv_WoodList.reloadData()

            }else if(tag == 3){
                
                self.fetchBaitDB()
                //self.tv_BaitStationList.reloadData()

            }else if(tag == 4){
                
                self.fetchPhysicalBarrierDB()
                //self.tv_PhysiCalBarrierList.reloadData()
            }

        }
            
        self.setUPheightForTable()
        
    }
 
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension NPMA_FinlizedReportVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            if chkFrontImage
            {
                strCheckFrontImage = "\\Documents\\UploadImages\\Img"  + "CheckFrontImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckFrontImage, image: pickedImage)
            }
            else
            {
                strCheckBackImage = "\\Documents\\UploadImages\\Img"  + "CheckBackImg" + "\(getUniqueValueForId())" + ".jpg"
                saveImageDocumentDirectory(strFileName: strCheckBackImage, image: pickedImage)
            }
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: -
// MARK: ------- Selection CustomTableView---------
extension NPMA_FinlizedReportVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        if(tag == 511)
        {
            if dictData.count == 0 {
                txtSelectPaymentMOde.text = ""
                txtSelectPaymentMOde.tag = 0
            }else{
                txtSelectPaymentMOde.text = "\(dictData.value(forKey: "title")!)"
                txtSelectPaymentMOde.tag = Int("\(dictData.value(forKey: "id")!)")!
                strGlobalPaymentMode = "\(dictData.value(forKey: "SysName")!)"
            }
            setUPheightForTable()

        }
        else if (tag == 520){
            if dictData.count == 0 {
                self.arySelectedTermsAndCondition = NSMutableArray()
                self.txtTermsAndCondition.text = ""
            }else{
                self.arySelectedTermsAndCondition = NSMutableArray()
                self.arySelectedTermsAndCondition = (dictData.value(forKey: "multi")as! NSArray).mutableCopy()as! NSMutableArray
                var strName = ""
                for item in self.arySelectedTermsAndCondition {
                    
                    if item is NSDictionary {
                        
                        let dictArrTemp = item as! NSDictionary
                        
                        let tempArr = dictArrTemp.allKeys as NSArray
                        
                        var strTemp = ""
                        
                        if tempArr.contains("TermsId") {
                            
                            strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "TermsId")!)")//"\((item as AnyObject).value(forKey: "TermsId")!)"
                            
                            strName = strName + "\(strTemp),"

                        } else if tempArr.contains("termsId") {
                            
                            strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "termsId")!)")
                            
                            strName = strName + "\(strTemp),"

                        } else if tempArr.contains("TermsTitle") {
                            
                            strTemp = "\((item as AnyObject).value(forKey: "TermsTitle")!)"
                            
                            strName = strName + "\(strTemp),"

                        } else {
                            
                            strTemp = getTermsNameViaId(strTermsId: "\((item as AnyObject).value(forKey: "termsId")!)")
                            
                            strName = strName + "\(strTemp),"

                        }
                        
                    } else {
                        
                        strName = strName + "\((item as AnyObject).value(forKey: "TermsTitle")!),"
                        
                    }
                    
                    //strName = strName + "\((item as AnyObject).value(forKey: "TermsTitle")!),"
                    
                }
                if(strName.count != 0){
                    strName = String(strName.dropLast())
                }
                txtTermsAndCondition.text = strName
            }
        }
    }
}
// MARK: -
// MARK: ----------DatePickerProtocol---------
extension NPMA_FinlizedReportVC: DatePickerProtocol
{
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int)
    {
        self.view.endEditing(true)
        txtDateForCheck.text = strDate
        
    }
}

// MARK: - -----------------------------------UITextFieldDelegate -----------------------------------
// MARK: -

extension NPMA_FinlizedReportVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " " {
            return false
        }
        
        if textField == txtOther  {
            
            btnOther.setImage(UIImage(named: "NPMA_Check"), for: .normal)

        }
        
        yesEditedSomething = true
        
        if ( textField == txtDrivingLicence || textField == txtCheck  )
        {
            
            return txtFieldValidation(textField: textField, string: string, returnOnly: "All", limitValue: 100)
            
        }
            
        else if ( textField == txtAmount  )
        {
            
            //return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 19)
            
            let chk = decimalValidation(textField: textField, range: range, string: string)
            
            return chk
            
        }
        else if ( textField == txtAmount  )
        {
            
            let chk = decimalValidation(textField: textField, range: range, string: string)

            return chk
            
        }
        else
        {
            
            return true
            
        }
                
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if ( textField == txtAmount  )
        {
            
            self.calculateTotalDue()
            
        }
        
    }
    
}

// MARK: ------------- Signature Delegate --------------

extension NPMA_FinlizedReportVC : SignatureViewDelegate{
    func imageFromSignatureView(image: UIImage)
    {
        if signatureType == "customer"
        {
            
            self.img_CustomerSign.image = image
            strCustomerSign = "\\Documents\\UploadImages\\TechnicianSign_" + "\(strWoId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
        else if signatureType == "technician"
        {
            
            self.img_TechSign.image = image
            strTechnicianSign = "\\Documents\\UploadImages\\TechnicianSign_" + "\(strWoId)" + "_" + global.getReferenceNumber() + ".jpg"
            
        }
        
    }
}

