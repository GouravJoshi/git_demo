import UIKit
import UIKit
import MessageUI


//MARK: -------------------------------------
//MARK: DynemicForm Userdefinefiled

enum Userdefinefiled_DynemicForm_Type{
    static let blank = ""
    static let Account = "Account"
    static let Opportunity = "Opportunity"
    static let Lead = "Lead"
    static let CrmContact = "CrmContact"
    static let CrmCompany = "CrmCompany"
    static let ServiceAddress = "ServiceAddress"
    static let BillingAddress = "BillingAddress"
    static let Setup = "Setup"
    static let WorkOrder = "WorkOrder"
    static let Employee = "Employee"
}

enum Userdefinefiled_DynemicForm{
    static let autocomplete = "autocomplete"
    static let button = "button"
    static let checkbox_group = "checkbox-group"
    static let date = "date"
    static let header = "header"
    static let hidden = "hidden"

    static let number = "number"
    static let paragraph = "paragraph"
    static let radio_group = "radio-group"
    static let select = "select"
    static let text = "text"
    static let textarea = "textarea"
    
    static let text_Sub = "text"
    static let text_SubEmail = "email"
    static let text_SubPhone = "phone"
    static let text_SubPassword = "password"
    static let text_Color = "color"

    
    
}
enum Userdefinefiled_Entity{

    static let Entity_UDFSavedData = "UDFSavedData"
    static let Entity_UDFMasterData = "UDFMasterData"
}

enum Userdefinefiled_StaticValue{

    static let static_Other = "Other"
    static let static_OtherWithSpace = "other     "

}

func convertToArray(text: String) -> NSArray {
    do {
        
        let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? NSArray
        // print(data)
        let firstElement: NSArray = data! as NSArray
        return firstElement
    } catch {
        print(error.localizedDescription)
    }
    return NSArray()
}

extension UILabel {
    
    func addTrailing(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text, attributes: [:])

        string.append(attachmentString)
        self.attributedText = string
    }
    
    func addLeading(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)
        
        let string = NSMutableAttributedString(string: text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}
