//
//  UserDefineFiledVC.swift
//  DPS
//
//  Created by Navin Patidar on 25/01/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit


var aryUDFMasterData = NSMutableArray()
var aryUDFSaveData = NSMutableArray()
var aryOtherUDF = NSMutableArray()

class UserDefineFiledVC: UIViewController {
    
    var viewControllor = UIViewController()
    
    var viewContain = UIView()
    var strID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func GetMasterData() -> NSMutableArray {
        return aryUDFMasterData
    }
    func GetSaveData() -> NSMutableArray {
        
        let arySaveTemp = aryUDFSaveData
        for (_ , item) in aryUDFMasterData.enumerated() {
            let dict = (item as AnyObject) as! NSDictionary
            let nameID = "\(dict.value(forKey: "name") ?? "")"
            let type = "\(dict.value(forKey: "type") ?? "")"
            if type == Userdefinefiled_DynemicForm.radio_group ||  type == Userdefinefiled_DynemicForm.checkbox_group{
                let temp = arySaveTemp.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(nameID)" } as NSArray
                if(temp.count > 0){
                    arySaveTemp.remove(temp[0])
                }
                let aryOption = (dict.value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
                var userData = NSMutableArray() , aryUserDataTemp = NSMutableArray()
                if(temp.count != 0){
                    if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                        userData = ((temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray).mutableCopy() as! NSMutableArray
                        for (_, item) in aryOption.enumerated() {
                            let dictOption =  (item as AnyObject) as! NSDictionary
                            let strvalue = "\(dictOption.value(forKey: "value") ?? "")"
                            for (indexUser, itemuser) in userData.enumerated() {
                                if("\(itemuser)" == "\(strvalue)" ){
                                    userData.removeObject(at: indexUser)
                                    aryUserDataTemp.add(itemuser)
                                    break
                                }
                            }
                        }
                    }
                }
                let dictSave = NSMutableDictionary()
                dictSave.setValue(nameID, forKey: "name")
                dictSave.setValue(aryUserDataTemp, forKey: "userData")
                arySaveTemp.add(dictSave)
            }
            
        }
        for (index , item) in aryOtherUDF.enumerated() {
            if(item as AnyObject) is NSDictionary{
                let dictOther = (item as AnyObject) as! NSDictionary
                let strNameId = "\(dictOther.value(forKey: "nameID") ?? "")"
                let strval = "\(dictOther.value(forKey: "value") ?? "")"
                let temp = arySaveTemp.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strNameId)" } as NSArray
                if(temp.count != 0){
                    if(arySaveTemp.count != 0){
                        arySaveTemp.remove(temp[0])
                    }
                }
                var userData = NSMutableArray()
                if(temp.count != 0){
                    if((temp[0] is NSDictionary)){
                        let dictUser = (temp[0]as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if(dictUser.value(forKey: "userData") is NSArray){
                            userData = (dictUser.value(forKey: "userData") as! NSArray).mutableCopy() as! NSMutableArray
                            if userData.count == 0 {
                                userData.add(strval)
                            }else{
                                if !(userData.contains(strval)){
                                    userData.add(strval)
                                }
                            }
                            dictUser.setValue(userData, forKey: "userData")
                            arySaveTemp.add(dictUser)
                        }
                      
                    }
                 
                }
            }
       
        }
        aryUDFSaveData = NSMutableArray()
        aryUDFSaveData = arySaveTemp
        return aryUDFSaveData
    }
    func GetSaveOtherData() -> NSMutableArray {
        
        return aryOtherUDF
    }
    @objc
    func tapHelpData(sender:UITapGestureRecognizer) {
        guard let getTag = sender.view?.tag else { return }
        print("getTag == \(getTag)")
        if(aryUDFMasterData.object(at: getTag) is NSDictionary){
            let dictMAIN = aryUDFMasterData.object(at: getTag) as! NSDictionary
            
            let description = "\(dictMAIN.value(forKey: "description") ?? "")"
            if(description.count != 0){
                showAlertWithoutAnyAction(strtitle: "Help", strMessage: description, viewcontrol: viewControllor)
                print("tap working")
            }
        }
        
    }
    
    func getOtherValueArray(aryUDFSave  : NSMutableArray , aryUDFMaster : NSMutableArray) -> NSMutableArray {
        let aryOther = NSMutableArray()
        for (headerIndex, item) in aryUDFMaster.enumerated() {
            print("headerIndex---------------\(headerIndex)")
            var dictMAIN = NSMutableDictionary()
            if((item as AnyObject) is NSDictionary){
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let isOther = "\(dictMAIN.value(forKey: "other") ?? "")"
                if isOther.lowercased() == "true" || isOther == "1" {
                    let dictOther = checkOtherOptionValue(aryUDFTempSave: aryUDFSaveData, aryUDF: aryUDFMasterData, dict: dictMAIN)
                    if(dictOther.count != 0){
                        aryOther.add(dictOther)
                    }
                }
            }
           
        }
        
        return aryOther
    }
    
    
    func CreatFormUserdefine(strid : String , aryJson_Master : NSArray, aryResponceObj : NSArray, viewUserdefinefiled:UIView, controllor : UIViewController,OnResultBlock: @escaping (_ aryUserdefineData: NSMutableArray,_ aryUserdefineResponceData : NSMutableArray,_ heightview:Int) -> Void) {
        var othercount = 0
        aryUDFMasterData = NSMutableArray()
        aryUDFSaveData = NSMutableArray()
       // aryOtherUDF = NSMutableArray()
        strID = strid
        viewControllor = controllor
        viewContain = viewUserdefinefiled
        aryUDFSaveData = aryResponceObj.mutableCopy() as! NSMutableArray
        aryUDFMasterData = aryJson_Master.mutableCopy() as! NSMutableArray
        
        
        if(aryOtherUDF.count == 0){
            aryOtherUDF =  getOtherValueArray(aryUDFSave: aryUDFSaveData, aryUDFMaster: aryUDFMasterData)
        }

        //MARK: ---Userdefine filed-------
        
        for v in viewUserdefinefiled.subviews{
            if v is UILabel {
                if (v as! UILabel).tag != 99 {
                    v.removeFromSuperview()
                    
                }
            }else{
                v.removeFromSuperview()
            }
        }
        
        
        var yAxis = 30.0 , extraSpace = DeviceType.IS_IPAD ? 12.0 : 8.0
        let heighttxt = DeviceType.IS_IPAD ? 50.0 : 44.0
        let heighttxtView = DeviceType.IS_IPAD ? 150.0 : 120.0
        let strFont = DeviceType.IS_IPAD ? 18.0 : 16.0
        
        
        
        //MARK: ----- Title
        if(aryUDFMasterData.count != 0){
            let lblSectionDetailTitle = UILabel()
            lblSectionDetailTitle.text = "Other Details"
            lblSectionDetailTitle.frame = CGRect(x: 0.0, y: CGFloat(yAxis), width: (viewUserdefinefiled.frame.width), height: 30)
            lblSectionDetailTitle.numberOfLines = 0
            lblSectionDetailTitle.textColor = hexStringToUIColor(hex: appThemeColor)
            lblSectionDetailTitle.font = UIFont.boldSystemFont(ofSize: strFont)
            viewUserdefinefiled.addSubview(lblSectionDetailTitle)
            yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
        }
        
        for (headerIndex, item) in aryUDFMasterData.enumerated() {
            print("headerIndex---------------\(headerIndex)")
            if((item as AnyObject) is NSDictionary){
                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                let strType = "\(dictMAIN.value(forKey: "type") ?? "")"
                let required = "\(dictMAIN.value(forKey: "required") ?? "")"
                
                //MARK: ------- text
                if strType.lowercased() ==  Userdefinefiled_DynemicForm.text {
                    if "\(dictMAIN.value(forKey: "subtype") ?? "")" !=  Userdefinefiled_DynemicForm.text_Color{
                        //Title
                        
                        var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                        let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                        var strplaceholder = "\(dictMAIN.value(forKey: "placeholder") ?? "")"
                        
                        if(required == "1" || required.lowercased() == "true"){
                            strTitle = "\(strTitle)*"
                        }
                        if( strdescription.count != 0){
                            strTitle = strTitle + "  \u{24D8}"
                        }
                        if(strplaceholder == ""){
                            strplaceholder = "type.."
                        }
                        let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                        let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                        lblTitle.numberOfLines = 0
                        lblTitle.textColor = UIColor.darkGray
                        lblTitle.font = UIFont.systemFont(ofSize: strFont)
                        lblTitle.text = strTitle
                        lblTitle.tag = headerIndex
                        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                        lblTitle.isUserInteractionEnabled = true
                        lblTitle.addGestureRecognizer(tap)
                        viewUserdefinefiled.addSubview(lblTitle)
                        
                        yAxis = yAxis + lblTitle.frame.height
                        //Text Filed
                        let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width ) , height: Int(heighttxt)))
                        textarea?.placeholder = "\(strplaceholder)"
                        textarea?.selectedPlaceHolderColor = UIColor.clear
                        textarea?.placeHolderColor = UIColor.clear
                        textarea!.font = UIFont.systemFont(ofSize: strFont)
                        // textarea!.text = "\(dictMAIN.value(forKey: "label")!)"
                        textarea!.textColor = UIColor.darkGray
                        textarea!.lineColor = UIColor.lightGray
                        textarea!.delegate = self
                        
                        textarea!.tag = headerIndex*10000
                        textarea!.keyboardType = .default
                        
                        viewUserdefinefiled.addSubview(textarea!)
                        
                        yAxis = yAxis + textarea!.frame.height + extraSpace
                        
                        switch "\(dictMAIN.value(forKey: "subtype") ?? "")" {
                        case Userdefinefiled_DynemicForm.text_Sub:
                            textarea!.keyboardType = .default
                        case Userdefinefiled_DynemicForm.text_SubPhone:
                            textarea!.keyboardType = .phonePad
                        case Userdefinefiled_DynemicForm.text_SubEmail:
                            textarea!.keyboardType = .emailAddress
                        case Userdefinefiled_DynemicForm.text_SubPassword:
                            if #available(iOS 13.0, *) {
                                textarea?.isSecureTextEntry = true
                                textarea!.enablePasswordToggle()
                               
                            } else {
                                
                            }
                        default:
                            textarea!.keyboardType = .default
                            break
                        }
                        
                        //--Answer
                        textarea!.text = ""
                        let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                        let temp = aryUDFSaveData.filter { (task) -> Bool in
                            return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(nameID)" } as NSArray
                        
                        var userData = NSArray()
                        if(temp.count != 0){
                            if((temp[0] is NSDictionary)){
                                if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                                    userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                                    if userData.count != 0 {
                                        textarea!.text = "\(userData[0])"
                                    }
                                }
                            }
                        
                            
                        }
                        
                    }
                    
                    
                }
                //MARK: ------- -number
                else if (strType.lowercased() ==  Userdefinefiled_DynemicForm.number){
                    //Title
                    
                    var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    var strplaceholder = "\(dictMAIN.value(forKey: "placeholder") ?? "")"
                    
                    if(required == "1" || required.lowercased() == "true"){
                        strTitle = "\(strTitle)*"
                    }
                    if( strdescription.count != 0){
                        strTitle = strTitle + "  \u{24D8}"
                    }
                    if(strplaceholder == ""){
                        strplaceholder = "type.."
                    }
                    let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    lblTitle.font = UIFont.systemFont(ofSize:strFont)
                    lblTitle.text = strTitle
                    lblTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblTitle.isUserInteractionEnabled = true
                    lblTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblTitle)
                    
                    yAxis = yAxis + lblTitle.frame.height
                    //Text Filed
                    let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height: Int(heighttxt)))
                    textarea?.placeholder = "\(strplaceholder)"
                    textarea!.font = UIFont.systemFont(ofSize:strFont)
                    // textarea!.text = "\(dictMAIN.value(forKey: "label")!)"
                    textarea!.textColor = UIColor.darkGray
                    textarea!.lineColor = UIColor.lightGray
                    textarea!.delegate = self
                    textarea?.selectedPlaceHolderColor = UIColor.clear
                    textarea?.placeHolderColor = UIColor.clear
                    textarea!.tag = headerIndex*10000
                    textarea!.keyboardType = .numberPad
                    
                    viewUserdefinefiled.addSubview(textarea!)
                    yAxis = yAxis + textarea!.frame.height + extraSpace
                    
                    //--Answer
                    textarea!.text = ""
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name") ?? "")" == "\(nameID)" } as NSArray
                    var userData = NSArray()
                    if(temp.count != 0){
                        if((temp[0] is NSDictionary)){
                        if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                            userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                            if userData.count != 0 {
                                textarea!.text = "\(userData[0])"
                            }
                        }
                        }
                    }
                }
                
                //MARK: ------- textarea-------
                
                else if strType.lowercased() ==  Userdefinefiled_DynemicForm.textarea  {
                    var title =  "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    var strplaceholder = "\(dictMAIN.value(forKey: "placeholder") ?? "")"
                    
                    let lblSectionDetailTitle = UILabel()
                    lblSectionDetailTitle.font = UIFont.systemFont(ofSize: strFont)
                    if(required == "1" || required.lowercased() == "true"){
                        title = title + "*"
                    }
                    if( strdescription.count != 0){
                        title = title + "  \u{24D8}"
                    }
                    if(strplaceholder == ""){
                        strplaceholder = "type.."
                    }
                    lblSectionDetailTitle.text = title
                    let heightCalculate = title.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    lblSectionDetailTitle.frame = CGRect(x: 0.0, y: CGFloat(yAxis), width: (viewUserdefinefiled.frame.width), height: heightCalculate)
                    lblSectionDetailTitle.numberOfLines = 0
                    lblSectionDetailTitle.textColor = UIColor.darkGray
                    lblSectionDetailTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblSectionDetailTitle.isUserInteractionEnabled = true
                    lblSectionDetailTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblSectionDetailTitle)
                    yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                    
                    let textarea = UITextView.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height: Int(heighttxtView)))
                    textarea.font = UIFont.systemFont(ofSize: strFont)
                    textarea.placeholder = strplaceholder
                    textarea.placeholderColor = UIColor.lightGray
                    //  textarea.text = "\(dictMAIN.value(forKey: "Value")!)"
                    textarea.textColor = UIColor.darkGray
                    textarea.layer.cornerRadius = 8.0
                    textarea.layer.borderWidth = 1.0
                    textarea.layer.borderColor = UIColor.lightGray.cgColor
                    textarea.delegate = self
                    textarea.tag = headerIndex*10000
                    
                    viewUserdefinefiled.addSubview(textarea)
                    yAxis = yAxis + textarea.frame.height + extraSpace
                    
                    //--Answer
                    textarea.text = ""
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name") ?? "")" == "\(nameID)" } as NSArray
                    var userData = NSArray()
                    if(temp.count != 0){
                        if((temp[0] is NSDictionary)){
                        if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                            userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                            if userData.count != 0 {
                                textarea.text = "\(userData[0])"
                            }
                        }
                        }
                        
                    }
                    
                    
                }
                
                
                //MARK: ------- select-------
                else if strType.lowercased() ==  Userdefinefiled_DynemicForm.select  {
                    
                    
                    var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    var strplaceholder = "\(dictMAIN.value(forKey: "placeholder") ?? "")"
                    
                    if(required == "1" || required.lowercased() == "true"){
                        strTitle = "\(strTitle)*"
                    }
                    if( strdescription.count != 0){
                        strTitle = strTitle + "  \u{24D8}"
                    }
                    if(strplaceholder == ""){
                        strplaceholder = "type.."
                    }
                    
                    let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    lblTitle.font = UIFont.systemFont(ofSize: strFont)
                    lblTitle.text = strTitle
                    lblTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblTitle.isUserInteractionEnabled = true
                    lblTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblTitle)
                    
                    yAxis = yAxis + lblTitle.frame.height
                    //Text Filed
                    
                    let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width)  , height: Int(heighttxt)))
                    textarea!.font = UIFont.systemFont(ofSize: strFont)
                    textarea!.placeholder = strplaceholder
                    
                    textarea!.textColor = UIColor.darkGray
                    textarea!.lineColor = UIColor.lightGray
                    textarea!.isUserInteractionEnabled = false
                    textarea?.selectedPlaceHolderColor = UIColor.clear
                    textarea?.placeHolderColor = UIColor.clear
                    
                    let aryOption = dictMAIN.value(forKey: "values") as! NSArray
                    let multiple = "\(dictMAIN.value(forKey: "multiple") ?? "0")"
                    var slectiontype = false
                    if(multiple.lowercased() == "true" || multiple.lowercased() == "1"){
                        slectiontype = true
                    }
                    //--Answer
                    textarea!.text = ""
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name") ?? "")" == "\(nameID)" } as NSArray

                    var userData = NSArray()
                    if(temp.count != 0){
                        if((temp[0] is NSDictionary)){
                        if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                            userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                            if(dictMAIN.value(forKey: "values") is NSArray){
                                let userDataMain  = dictMAIN.value(forKey: "values") as! NSArray
                                let aryFinalOption = NSMutableArray()
                                for itemValue in userData {
                                    let ary_value = userDataMain.filter { (task) -> Bool in
                                        return "\((task as! NSDictionary).value(forKey: "value")!)" == "\(itemValue)" } as NSArray
                                    if(ary_value.count != 0){
                                        let dict  = ary_value.object(at: 0) as! NSDictionary
                                        aryFinalOption.add("\(dict.value(forKey: "label") ?? " ")")
                                    }
                                }
                                if aryFinalOption.count != 0 {
                                    textarea!.text! = "\(aryFinalOption.componentsJoined(by: ", "))"
                                }
                            }
                         
                        }
                        }
                        
                    }
                    //-------
                    
                    
                    viewUserdefinefiled.addSubview(textarea!)
                    let btn = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height: Int(heighttxt)))
                    btn.setImage(UIImage(named: "Next2"), for: .normal)
                    btn.contentHorizontalAlignment = .right
                    btn.imageView?.contentMode = .scaleAspectFit
                    btn.tag = headerIndex*10000+2
                    
                    btn.addAction(for: .touchUpInside) { [self] in
                        self.view.endEditing(true)
                        
                        if(aryOption.count != 0){
                            let aryUser = NSMutableArray()
                            for item in userData {
                                let temp = aryOption.filter { (task) -> Bool in
                                    return "\((task as! NSDictionary).value(forKey: "value") ?? "")" == "\(item)" } as NSArray
                                if(temp.count != 0){
                                    aryUser.add(temp[0])
                                }
                            }
                            DispatchQueue.main.async {
                                goToNewSalesSelectionVC(arrItem: aryOption.mutableCopy() as! NSMutableArray, arrSelectedItem: aryUser, tag: btn.tag, titleHeader: "Select Option", isMultiSelection: slectiontype, ShowNameKey: "label")
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableee, viewcontrol: self)
                        }
                        
                    }
                    
                    viewUserdefinefiled.addSubview(btn)
                    
                    yAxis = yAxis + heighttxt + extraSpace
                    
                }
                //MARK: ------- date-------
                
                else if  strType.lowercased() ==  Userdefinefiled_DynemicForm.date  {
                    
                    var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    var strplaceholder = "\(dictMAIN.value(forKey: "placeholder") ?? "")"
                    
                    if(required == "1" || required.lowercased() == "true"){
                        strTitle = "\(strTitle)*"
                    }
                    if( strdescription.count != 0){
                        strTitle = strTitle + "  \u{24D8}"
                    }
                    if(strplaceholder == ""){
                        strplaceholder = "dd/mm/yyyy"
                    }
                    
                    let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    lblTitle.font = UIFont.systemFont(ofSize: strFont)
                    lblTitle.text = strTitle
                    lblTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblTitle.isUserInteractionEnabled = true
                    lblTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblTitle)
                    
                    yAxis = yAxis + lblTitle.frame.height
                    //Text Filed
                    
                    let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width)  , height: Int(heighttxt)))
                    textarea!.font = UIFont.systemFont(ofSize: strFont)
                    textarea!.placeholder = strplaceholder == "" ?  "dd/mm/yyyy" : strplaceholder
                    textarea?.selectedPlaceHolderColor = UIColor.clear
                    textarea?.placeHolderColor = UIColor.clear
                    textarea!.textColor = UIColor.darkGray
                    textarea!.lineColor = UIColor.lightGray
                    textarea!.isUserInteractionEnabled = false
                    viewUserdefinefiled.addSubview(textarea!)
                    
                    let btnDate = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height: Int(heighttxt)))
                    btnDate.setImage(UIImage(named: "Calendar"), for: .normal)
                    btnDate.contentHorizontalAlignment = .right
                    btnDate.imageView?.contentMode = .scaleAspectFit
                    //btnDate.tag = headerIndex*10000+headerIndex
                    btnDate.isUserInteractionEnabled = true

                    
                    //--Answer
                    textarea!.text = ""
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(nameID)" } as NSArray
                    var userData = NSArray()
                    if(temp.count != 0){
                        if(temp[0] is NSDictionary){
                            if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                                
                                userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                                if userData.count != 0 {
                                    let strDate  = changeStringDateToGivenFormatWithoutT(strDate: "\(userData[0])", strRequiredFormat: "MM/dd/yyyy")
                                    textarea!.text = "\(strDate)"
                                }
                            }
                        }
                      
                    }
                    //----------
                    
                    btnDate.addAction(for: .touchUpInside) { [self] in
                        print("Hello, Closure!")
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                        formatter.locale = Locale(identifier: "EST")
                        
                        var fromDate = textarea!.text == "" ? Date() : formatter.date(from: textarea!.text!)
                        if fromDate == nil {
                            fromDate = Date()
                        }
                        
                        let datePicker = UIDatePicker()
                        datePicker.minimumDate = Date()
                        datePicker.datePickerMode = UIDatePicker.Mode.date
                        let alertController = SalesNew_SupportFile().InitializePickerInActionSheetWithClearButton(slectedDate: fromDate!,Picker: datePicker, view: viewControllor.view ,strTitle : "Select  Date")
                        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
                            
                            self.UpdateValueWhenClickOn_Date(headertag: headerIndex, type:  Userdefinefiled_DynemicForm.date, strDate: datePicker.date, strClear: false)
                            
                        }))
                        alertController.addAction(UIAlertAction(title: "Clear", style: UIAlertAction.Style.default, handler: { [self] action in
                            print(datePicker.date)
                            
                            self.view.endEditing(true)
                            self.UpdateValueWhenClickOn_Date(headertag: headerIndex, type:  Userdefinefiled_DynemicForm.date, strDate: datePicker.date, strClear: true)
                            
                        }))
                        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
                        viewControllor.present(alertController, animated: true, completion: nil)
                        
                    }
                    viewUserdefinefiled.addSubview(btnDate)
                    yAxis = yAxis + heighttxt + extraSpace
                    
                }
                //MARK: ------ radio_group-------
                
                else if  strType.lowercased() ==  Userdefinefiled_DynemicForm.radio_group  {
                    var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    if(required == "1" || required.lowercased() == "true"){
                        strTitle = "\(strTitle)*"
                    }
                    if( strdescription.count != 0){
                        strTitle = strTitle + "  \u{24D8}"
                    }
                    let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    lblTitle.font = UIFont.systemFont(ofSize: strFont)
                    lblTitle.text = strTitle
                    lblTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblTitle.isUserInteractionEnabled = true
                    lblTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblTitle)
                    
                    yAxis = yAxis + lblTitle.frame.height + extraSpace
                    //option Filed
                    let aryOption = (dictMAIN.value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(nameID)" } as NSArray
                    var userData = NSArray()
                    if(temp.count != 0){
                        if(temp[0] is NSDictionary){
                            if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                                userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                            }
                        }
                       
                    }
                    
                    for (subControlIndex, item) in aryOption.enumerated() {
                        let optionDict = (item as AnyObject) as! NSDictionary
                        let strLabel = "\(optionDict.value(forKey: "label") ?? "")"
                        let strValue = "\(optionDict.value(forKey: "value") ?? "")"
                        let btnRadioBox = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                        if(userData.contains("\(strValue)")){
                            btnRadioBox.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
                        }else{
                            btnRadioBox.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
                        }
                        btnRadioBox.titleLabel?.font = UIFont.systemFont(ofSize: strFont)
                        btnRadioBox.contentHorizontalAlignment = .left
                        //  btnRadioBox.tag = headerIndex*10000+subControlIndex
                        btnRadioBox.addAction(for: .touchUpInside) { [self] in
                            self.view.endEditing(true)
                            updateValueWhenClickOn_Radio_CheckBox(headertag: Int(headerIndex), sectiontag: Int(subControlIndex), type:  Userdefinefiled_DynemicForm.radio_group)
                        }
                        viewUserdefinefiled.addSubview(btnRadioBox)
                        
                        let lblTitle = UILabel()
                        lblTitle.font = UIFont.systemFont(ofSize: strFont)
                        lblTitle.text = "\(strLabel)"
                        lblTitle.numberOfLines = 0
                        lblTitle.textColor = UIColor.darkGray
                        
                        
                        var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewUserdefinefiled.frame.width - btnRadioBox.frame.maxX + 10.0), font: UIFont.systemFont(ofSize: strFont))
                        if(lbltitleHeight < 35.0){
                            lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                        }
                        lblTitle.frame =  CGRect(x: Int(btnRadioBox.frame.maxX + 10), y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) - (Int(btnRadioBox.frame.maxX) + 10) , height: Int(lbltitleHeight)
                        )
                        btnRadioBox.center.y = lblTitle.center.y
                        viewUserdefinefiled.addSubview(lblTitle)
                        yAxis = yAxis + lbltitleHeight + extraSpace
                        
                        
                    }
                    
                    let isOther = "\(dictMAIN.value(forKey: "other") ?? "")"
                    if isOther.lowercased() == "true" || isOther == "1" {
                        othercount = othercount + 1
                        let btnRadioBox = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                        btnRadioBox.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
                        
                        btnRadioBox.titleLabel?.font = UIFont.systemFont(ofSize: strFont)
                        btnRadioBox.contentHorizontalAlignment = .left
                        //  btnRadioBox.tag = headerIndex*10000+subControlIndex
                        btnRadioBox.addAction(for: .touchUpInside) { [self] in
                            self.view.endEditing(true)
                            updateOtherValueClickOn_Radio_CheckBox(headertag: Int(headerIndex), type: Userdefinefiled_DynemicForm.radio_group)
                            
                            
                            
                        }
                        viewUserdefinefiled.addSubview(btnRadioBox)
                        let lblTitle = UILabel()
                        lblTitle.font = UIFont.systemFont(ofSize: strFont)
                        lblTitle.text = "\(Userdefinefiled_StaticValue.static_Other)"
                        lblTitle.numberOfLines = 0
                        lblTitle.textColor = UIColor.darkGray
                        lblTitle.frame =  CGRect(x: Int(btnRadioBox.frame.maxX + 10), y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) - (Int(btnRadioBox.frame.maxX) + 10) , height: Int((DeviceType.IS_IPAD ? 40.0 : 30.0))
                        )
                        btnRadioBox.center.y = lblTitle.center.y
                        viewUserdefinefiled.addSubview(lblTitle)
                        yAxis = yAxis + (DeviceType.IS_IPAD ? 40.0 : 30.0) + extraSpace
                        
                        let temp = aryOtherUDF.filter { (task) -> Bool in
                            return "\((task as! NSDictionary).value(forKey: "nameID") ?? "")" == "\(nameID)" } as NSArray
                        if temp.count != 0 {
                            let dictOthertemp = temp[0] as! NSDictionary
                            let strValue = "\(dictOthertemp.value(forKey: "value") ?? "")"
                            btnRadioBox.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
                            //Text Filed
                            let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width ) , height: Int(heighttxt)))
                            textarea?.placeholder = "\(Userdefinefiled_StaticValue.static_Other)"
                            textarea?.selectedPlaceHolderColor = UIColor.clear
                            textarea?.placeHolderColor = UIColor.clear
                            textarea!.font = UIFont.systemFont(ofSize: strFont)
                            textarea!.text = "\(strValue)"
                            textarea!.textColor = UIColor.darkGray
                            textarea!.lineColor = UIColor.lightGray
                            textarea!.delegate = self
                            textarea!.tag = headerIndex*10000 + 99
                            textarea!.keyboardType = .default
                            viewUserdefinefiled.addSubview(textarea!)
                            yAxis = yAxis + textarea!.frame.height + extraSpace
                        }
                    }
                }
                //MARK: ------ paragraph-------
                
                else if  strType.lowercased() ==  Userdefinefiled_DynemicForm.paragraph  {
                    let strLabel = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewUserdefinefiled.frame.width), font: UIFont.systemFont(ofSize: strFont))
                    
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width)  , height: Int(lbltitleHeight)))
                    lblTitle.font = UIFont.systemFont(ofSize: strFont)
                    lblTitle.text = strLabel
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    viewUserdefinefiled.addSubview(lblTitle)
                    yAxis = yAxis + lbltitleHeight + extraSpace
                    
                }
                
                
                //MARK: ------ header-------
                
                else if  strType.lowercased() ==  Userdefinefiled_DynemicForm.header  {
                    let strLabel = "\(dictMAIN.value(forKey: "label")!)"
                    let lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewUserdefinefiled.frame.width), font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width)  , height: Int(lbltitleHeight)))
                    lblTitle.font = UIFont.systemFont(ofSize:strFont)
                    lblTitle.text = strLabel
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    viewUserdefinefiled.addSubview(lblTitle)
                    yAxis = yAxis + lbltitleHeight + extraSpace
                }
                
                
                //MARK:-------- checkbox_group--------
                
                else if  strType.lowercased() ==  Userdefinefiled_DynemicForm.checkbox_group  {
                    print("\(dictMAIN.value(forKey: "label") ?? "")")
                    var strTitle = "\(dictMAIN.value(forKey: "label") ?? "")".html2String
                    let strdescription = "\(dictMAIN.value(forKey: "description") ?? "")"
                    if(required == "1" || required.lowercased() == "true"){
                        strTitle = "\(strTitle)*"
                    }
                    if( strdescription.count != 0){
                        strTitle = strTitle + "  \u{24D8}"
                    }
                    let heightCalculate = strTitle.height(withConstrainedWidth: viewUserdefinefiled.frame.width, font: UIFont.systemFont(ofSize: strFont))
                    let lblTitle = UILabel.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) , height:Int(heightCalculate)))
                    lblTitle.numberOfLines = 0
                    lblTitle.textColor = UIColor.darkGray
                    lblTitle.font = UIFont.systemFont(ofSize: strFont)
                    lblTitle.text = strTitle
                    lblTitle.tag = headerIndex
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapHelpData(sender:)))
                    lblTitle.isUserInteractionEnabled = true
                    lblTitle.addGestureRecognizer(tap)
                    viewUserdefinefiled.addSubview(lblTitle)
                    
                    yAxis = yAxis + lblTitle.frame.height + extraSpace
                    
                    let aryOption = dictMAIN.value(forKey: "values") as! NSArray
                    let nameID = "\(dictMAIN.value(forKey: "name") ?? "")"
                    let temp = aryUDFSaveData.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name") ?? "")" == "\(nameID)" } as NSArray
                    var userData = NSArray()
                    if(temp.count != 0){
                        if((temp[0] is NSDictionary)){
                            if(temp[0]as! NSDictionary).value(forKey: "userData") is NSArray {
                                
                                userData = (temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray
                            }
                        }
                       
                    }
                    
                    for (subControlIndex, item) in aryOption.enumerated() {
                        if( (item as AnyObject) is NSDictionary){
                            let optionDict = (item as AnyObject) as! NSDictionary
                            let strLabel = "\(optionDict.value(forKey: "label") ?? "")"
                            let strValue = "\(optionDict.value(forKey: "value") ?? "")"
                            let btnCheckBox = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                             if(userData.contains("\(strValue)")){
                                btnCheckBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                            }else{
                                btnCheckBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                            }
                            btnCheckBox.titleLabel?.font = UIFont.systemFont(ofSize: strFont)
                            btnCheckBox.contentHorizontalAlignment = .left
                            //   btnCheckBox.tag = headerIndex*10000+subControlIndex
                            btnCheckBox.addAction(for: .touchUpInside) { [self] in
                                self.view.endEditing(true)
                                updateValueWhenClickOn_Radio_CheckBox(headertag: Int(headerIndex), sectiontag: Int(subControlIndex), type:  Userdefinefiled_DynemicForm.checkbox_group)
                            }
                            viewUserdefinefiled.addSubview(btnCheckBox)
                            let lblTitle = UILabel()
                            lblTitle.font = UIFont.systemFont(ofSize: strFont)
                            lblTitle.text = "\(strLabel)"
                            lblTitle.numberOfLines = 0
                            lblTitle.textColor = UIColor.darkGray
                            var lbltitleHeight = "\(strLabel)\n".height(withConstrainedWidth: (viewUserdefinefiled.frame.width - btnCheckBox.frame.maxX + 10.0), font: UIFont.systemFont(ofSize: strFont))
                            if(lbltitleHeight < 35.0){
                                lbltitleHeight = (DeviceType.IS_IPAD ? 40.0 : 30.0)
                            }
                            lblTitle.frame =  CGRect(x: Int(btnCheckBox.frame.maxX + 10), y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) - (Int(btnCheckBox.frame.maxX) + 10) , height: Int(lbltitleHeight)
                            )
                            btnCheckBox.center.y = lblTitle.center.y
                            viewUserdefinefiled.addSubview(lblTitle)
                            yAxis = yAxis + lbltitleHeight + extraSpace
                        }
                    }
                    let isOther = "\(dictMAIN.value(forKey: "other") ?? "")"
                    if isOther.lowercased() == "true" || isOther == "1" {
                        othercount = othercount + 1
                        let btnRadioBox = UIButton.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                        btnRadioBox.titleLabel?.font = UIFont.systemFont(ofSize: strFont)
                        btnRadioBox.contentHorizontalAlignment = .left
                        //  btnRadioBox.tag = headerIndex*10000+subControlIndex
                        btnRadioBox.addAction(for: .touchUpInside) { [self] in
                            self.view.endEditing(true)
                            updateOtherValueClickOn_Radio_CheckBox(headertag: Int(headerIndex), type: Userdefinefiled_DynemicForm.checkbox_group)
                        }
                        viewUserdefinefiled.addSubview(btnRadioBox)
                        let lblTitle = UILabel()
                        lblTitle.font = UIFont.systemFont(ofSize: strFont)
                        lblTitle.text = "\(Userdefinefiled_StaticValue.static_Other)"
                        lblTitle.numberOfLines = 0
                        lblTitle.textColor = UIColor.darkGray
                        lblTitle.frame =  CGRect(x: Int(btnRadioBox.frame.maxX + 10), y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width) - (Int(btnRadioBox.frame.maxX) + 10) , height: Int((DeviceType.IS_IPAD ? 40.0 : 30.0))
                        )
                        btnRadioBox.center.y = lblTitle.center.y
                        viewUserdefinefiled.addSubview(lblTitle)
                        yAxis = yAxis + (DeviceType.IS_IPAD ? 40.0 : 30.0) + extraSpace
                        btnRadioBox.setImage(UIImage(named: "NPMA_UnCheck"), for: .normal)
                        let temp = aryOtherUDF.filter { (task) -> Bool in
                            return "\((task as! NSDictionary).value(forKey: "nameID") ?? "")" == "\(nameID)" } as NSArray
                        if temp.count != 0 {
                            if(temp[0] is NSDictionary){
                                let dictOthertemp = temp[0] as! NSDictionary
                                let strValue = "\(dictOthertemp.value(forKey: "value") ?? "")"
                                btnRadioBox.setImage(UIImage(named: "NPMA_Check"), for: .normal)
                                //Text Filed
                                let textarea = ACFloatingTextField.init(frame: CGRect(x: 0, y: Int(yAxis), width: Int(viewUserdefinefiled.frame.width ) , height: Int(heighttxt)))
                                textarea?.placeholder = "\(Userdefinefiled_StaticValue.static_Other)"
                                textarea?.selectedPlaceHolderColor = UIColor.clear
                                textarea?.placeHolderColor = UIColor.clear
                                textarea!.font = UIFont.systemFont(ofSize: strFont)
                                textarea!.text = "\(strValue)"
                                textarea!.textColor = UIColor.darkGray
                                textarea!.lineColor = UIColor.lightGray
                                textarea!.delegate = self
                                textarea!.tag =  headerIndex*10000 + 99
                                textarea!.keyboardType = .default
                                viewUserdefinefiled.addSubview(textarea!)
                                yAxis = yAxis + textarea!.frame.height + extraSpace
                            }
                        }
                    }
                }
            }
        }
       // NotificationCenter.default.post(name: Notification.Name("UDF"), object: "\(yAxis)")
       OnResultBlock((aryUDFMasterData) , (aryUDFSaveData) ,(Int(yAxis) + (othercount * 50)) )
    }
    
    func checkOtherOptionValue(aryUDFTempSave : NSMutableArray , aryUDF : NSMutableArray , dict : NSDictionary) -> NSDictionary {
        let nameID = "\(dict.value(forKey: "name") ?? "")"
        let temp = aryUDFTempSave.filter { (task) -> Bool in
            return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(nameID)" } as NSArray
        let aryOption = (dict.value(forKey: "values") as! NSArray).mutableCopy() as! NSMutableArray
        var userData = NSMutableArray()
        if(temp.count != 0){
            if((temp[0] is NSDictionary)){
                if((temp[0]as! NSDictionary).value(forKey: "userData") is NSArray){
                    userData = ((temp[0]as! NSDictionary).value(forKey: "userData") as! NSArray).mutableCopy() as! NSMutableArray
                    for (_, item) in aryOption.enumerated() {
                        if((item as AnyObject) is NSDictionary){
                            let dictOption =  (item as AnyObject) as! NSDictionary
                            let strvalue = "\(dictOption.value(forKey: "value") ?? "")"
                            
                            for (indexUser, itemuser) in userData.enumerated() {
                                if("\(itemuser)" == "\(strvalue)" ){
                                    userData.removeObject(at: indexUser)
                                    break
                                }
                            }
                        }
                    
                    }
                    if(userData.count != 0){
                        let dict = NSMutableDictionary()
                        dict.setValue(Userdefinefiled_StaticValue.static_Other, forKey: "label")
                        dict.setValue("true", forKey: "selected")
                        dict.setValue("\(userData[0])", forKey: "value")
                        dict.setValue(nameID, forKey: "nameID")
                        return dict
                    }
                }
            }
          
        }
        return NSDictionary()
    }
    
    func Update_OtherValueWhenClickOnTextFiled(headertag : Int , strvalue : String)  {
        if(aryUDFMasterData.object(at: headertag) is NSDictionary){
            let objMaster = (aryUDFMasterData.object(at: headertag) as! NSDictionary)
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            let type = "\(objMaster.value(forKey: "type") ?? "")"
            let temp = aryOtherUDF.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "nameID")!)" == "\(strControlID)" } as NSArray
            if temp.count == 0 {
                let dict = NSMutableDictionary()
                dict.setValue(Userdefinefiled_StaticValue.static_Other, forKey: "label")
                dict.setValue("true", forKey: "selected")
                dict.setValue(strvalue, forKey: "value")
                dict.setValue("\(strControlID)", forKey: "nameID")
                aryOtherUDF.add(dict)
            }else{
                if(temp[0] is NSDictionary){
                    let dictOther = (temp[0]as! NSDictionary ).mutableCopy() as! NSMutableDictionary
                    dictOther.setValue(strvalue, forKey: "value")
                    aryOtherUDF.remove(temp[0])
                    aryOtherUDF.add(dictOther)
                }
            }
            
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine( strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                }
            }
        }
      
    }
    
    
    
    func UpdateValueWhenClickOn_TextView_TextFiled(headertag : Int , strvalue : String)  {
        if(aryUDFMasterData.object(at: headertag) is NSDictionary){
            let objMaster = (aryUDFMasterData.object(at: headertag) as! NSDictionary)
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            // let strtype = "\(objMaster.value(forKey: "type") ?? "")"
            let strsubtype = "\(objMaster.value(forKey: "subtype") ?? "")"
            
            let str = email_Mobile_Validation(strsubtype: strsubtype, strvalue: strvalue)
            if(str != ""){
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: email_Mobile_Validation(strsubtype: strsubtype, strvalue: strvalue), viewcontrol: viewControllor)
            } else {
                let temp = aryUDFSaveData.filter { (task) -> Bool in
                    return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
                if temp.count == 0 {
                    let dict = NSMutableDictionary()
                    let ary = NSMutableArray()
                    ary.add(strvalue)
                    dict.setValue(ary, forKey: "userData")
                    dict.setValue(strControlID, forKey: "name")
                    aryUDFSaveData.add(dict)
                }else{
                    for (index, element) in aryUDFSaveData.enumerated() {
                        if((element as AnyObject) is NSDictionary){
                            let objResponce = ((element as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                            let strName = "\(objResponce.value(forKey: "name") ?? "")"
                            if(strName != ""){
                                if(strName  == "\(strControlID)"){
                                    if (objResponce.value(forKey: "userData") is NSArray) {
                                        let userData = (objResponce.value(forKey: "userData")as! NSArray).mutableCopy() as! NSMutableArray
                                        userData.removeAllObjects()
                                        userData.add(strvalue)
                                        objResponce.setValue(userData, forKey: "userData")
                                        aryUDFSaveData.replaceObject(at: index, with: objResponce)
                                    }
                                   
                                }
                            }
                        
                        }
                       
                    }
                }
            }
            
            
            
            
            //  UserDefineFiledVC().CreatFormUserdefine(aryJson_Master: self.aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
            //            self.height_viewUserdefinefiled.constant = CGFloat(height)
            //            self.aryUserdefineData = aryUserdefine
            //   }
        }
        

    }
    
    func email_Mobile_Validation(strsubtype : String , strvalue : String) -> String {
        if(strsubtype == Userdefinefiled_DynemicForm.text_SubEmail && strvalue != ""){
            if strvalue.count != 0{
                let isValid = Global().checkIfCommaSepartedEmailsAreValid(strvalue)
                
                if !(isValid) {
                    return "Please enter valid primary email!"
                }
            }
        }else if(strsubtype == Userdefinefiled_DynemicForm.text_SubPhone && strvalue != ""){
            if strvalue.count != 0{
                if strvalue.count < 12 {
                    return "Please enter valid phone number!"
                }
            }
        }else {
            return ""
        }
        return ""
    }
    func validation_UserdefineFiled(aryMaster : NSMutableArray , aryresponceobj : NSMutableArray) -> String {
        
        for item in aryMaster {
            let dict = (item as AnyObject) as! NSDictionary
            let  name = "\(dict.value(forKey: "name") ?? "")"
            let  type = "\(dict.value(forKey: "type") ?? "")"
            let  subtype = "\(dict.value(forKey: "subtype") ?? "")"
            
            if(type == Userdefinefiled_DynemicForm.checkbox_group || type == Userdefinefiled_DynemicForm.radio_group || type == Userdefinefiled_DynemicForm.text || type == Userdefinefiled_DynemicForm.textarea || type == Userdefinefiled_DynemicForm.select || type == Userdefinefiled_DynemicForm.number || type == Userdefinefiled_DynemicForm.date) && subtype != Userdefinefiled_DynemicForm.text_Color {
                
                let  label = "\(dict.value(forKey: "label") ?? "")"
                let  required = "\(dict.value(forKey: "required") ?? "")"
                if(required == "1" || required.lowercased() == "true"){
                    let temp = aryresponceobj.filter { (task) -> Bool in
                        return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(name)" } as NSArray
                    if(temp.count == 0 ){
                        return "\(label) is required!"
                    }else{
                        let data = temp[0] as! NSDictionary
                        if(data.value(forKey: "userData") is NSArray){
                            
                            if((data.value(forKey: "userData") as! NSArray).count == 0){
                                return "\(label) is required!"
                            }else if ((data.value(forKey: "userData") as! NSArray).count > 0){
                                let strValue = "\((data.value(forKey: "userData") as! NSArray).object(at: 0))"
                                if(strValue == ""){
                                    return "\(label) is required!"
                                    
                                }
                            }
                        }else{
                            return "\(label) is required!"
                        }
                    }
                }
            }
            
            
        }
        return ""
    }
    
    func UpdateValueWhenClickOn_Date(headertag : Int , type : String , strDate : Date , strClear : Bool)  {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "EST")
        var strDate = formatter.string(from: strDate)
        if strClear {
            strDate = ""
        }
        if(aryUDFMasterData.object(at: headertag) is NSDictionary){
            let objMaster = (aryUDFMasterData.object(at: headertag) as! NSDictionary)
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            let temp = aryUDFSaveData.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
            if temp.count == 0 {
                let dict = NSMutableDictionary()
                let ary = NSMutableArray()
                ary.add(strDate)
                dict.setValue(ary, forKey: "userData")
                dict.setValue(strControlID, forKey: "name")
                aryUDFSaveData.add(dict)
            }else{
                for (index, element) in aryUDFSaveData.enumerated() {
                    if(((element as AnyObject) is NSDictionary)){
                        let objResponce = ((element as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if("\(objResponce.value(forKey: "name")!)"  == "\(strControlID)"){
                            if(objResponce.value(forKey: "userData") is NSArray){
                                let userData = (objResponce.value(forKey: "userData")as! NSArray).mutableCopy() as! NSMutableArray
                                userData.removeAllObjects()
                                userData.add(strDate)
                                objResponce.setValue(userData, forKey: "userData")
                                aryUDFSaveData.replaceObject(at: index, with: objResponce)
                            }
            
                        }
                    }
               
                }
            }
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine(strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                    //            self.height_viewUserdefinefiled.constant = CGFloat(height)
                    //            self.aryUserdefineData = aryUserdefine
                }
            }
        }
     
    }
    func updateOtherValueClickOn_Radio_CheckBox(headertag : Int  , type : String){
        if(aryUDFMasterData.object(at: headertag) is NSDictionary){
            let objMaster = (aryUDFMasterData.object(at: headertag) as! NSDictionary)
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            
            
            //Remove from other array
            let tempother = aryUDFSaveData.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
            if(tempother.count != 0){
                if (type == Userdefinefiled_DynemicForm.checkbox_group){
                    
                    
                }else if (type == Userdefinefiled_DynemicForm.radio_group){
                    aryUDFSaveData.remove(tempother[0])
                }
            }
            //----
            
            let temp = aryOtherUDF.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "nameID")!)" == "\(strControlID)" } as NSArray
            
            if temp.count == 0 {
                
                let dict = NSMutableDictionary()
                dict.setValue(Userdefinefiled_StaticValue.static_Other, forKey: "label")
                dict.setValue("true", forKey: "selected")
                dict.setValue("", forKey: "value")
                dict.setValue("\(strControlID)", forKey: "nameID")
                aryOtherUDF.add(dict)
            }else{
                if (type == Userdefinefiled_DynemicForm.checkbox_group){
                    aryOtherUDF.remove(temp[0])
                }
            }
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine( strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                }
            }
        }
       
    }
    
    
    func updateValueWhenClickOn_Radio_CheckBox(headertag : Int , sectiontag : Int , type : String)  {
        self.viewControllor.view.endEditing(true)
        if((aryUDFMasterData.object(at: headertag) is NSDictionary)){
            let objMaster = (aryUDFMasterData.object(at: headertag) as! NSDictionary)
            let objcSub = (objMaster.value(forKey: "values")as! NSArray).object(at: sectiontag) as! NSDictionary
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            let slectionValue = "\(objcSub.value(forKey: "value") ?? "")"
            
            //Remove from other array
            let tempother = aryOtherUDF.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "nameID")!)" == "\(strControlID)" } as NSArray
            if(tempother.count != 0){
                if (type == Userdefinefiled_DynemicForm.checkbox_group){
                    
                    
                }else if (type == Userdefinefiled_DynemicForm.radio_group){
                    aryOtherUDF.remove(tempother[0])
                }
            }
            //----
            let temp = aryUDFSaveData.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
            if temp.count == 0 {
                let dict = NSMutableDictionary()
                let ary = NSMutableArray()
                ary.add("\(objcSub.value(forKey: "value") ?? "")")
                dict.setValue(ary, forKey: "userData")
                dict.setValue(strControlID, forKey: "name")
                aryUDFSaveData.add(dict)
            }else{
                for (index, element) in aryUDFSaveData.enumerated() {
                    if((element as AnyObject) is NSDictionary){
                        let objResponce = ((element as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if("\(objResponce.value(forKey: "name")!)"  == "\(strControlID)"){
                            if(objResponce.value(forKey: "userData") is NSArray){
                                let userData = (objResponce.value(forKey: "userData")as! NSArray).mutableCopy() as! NSMutableArray
                                if userData.count != 0 {
                                    if (type == Userdefinefiled_DynemicForm.checkbox_group){
                                        if userData.contains(slectionValue) {
                                            userData.remove(slectionValue)
                                        }else{
                                            userData.add(slectionValue)
                                        }
                                    }else if (type == Userdefinefiled_DynemicForm.radio_group){
                                        userData.removeAllObjects()
                                        userData.add(slectionValue)
                                    }
                                }else{
                                    userData.removeAllObjects()
                                    userData.add(slectionValue)
                                }
                                objResponce.setValue(userData, forKey: "userData")
                                aryUDFSaveData.replaceObject(at: index, with: objResponce)
                            }
                       
                        }
                    }
                  
                }
            }
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine(strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                    //            self.height_viewUserdefinefiled.constant = CGFloat(height)
                    //            self.aryUserdefineData = aryUserdefine
                    
                    
                }
                
            }
        }
    
    }
    
    
    func goToNewSalesSelectionVC(arrItem : NSMutableArray,arrSelectedItem : NSMutableArray, tag : Int, titleHeader : String , isMultiSelection : Bool, ShowNameKey : String) {
        if(arrItem.count != 0){
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_SelectionVC") as! SalesNew_SelectionVC
            vc.aryItems = arrItem
            vc.arySelectedItems = arrSelectedItem
            vc.isMultiSelection = isMultiSelection
            vc.strTitle = titleHeader
            vc.strTag = tag
            vc.strShowNameKey = ShowNameKey
            vc.delegate = self
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewControllor.present(vc, animated: false, completion: nil)
        }else{
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    func getUserdefineMasterData(type: String) -> NSMutableArray {
        var aryMasterData = NSMutableArray()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: Userdefinefiled_Entity.Entity_UDFMasterData, predicate: NSPredicate(format: "companyKey == %@", Global().getCompanyKey()))
        if(arryOfData.count != 0){
            for item in arryOfData
            {
                if(item is NSManagedObject){
                    let match = item as! NSManagedObject
                    
                    if(match.value(forKey: "udfmasterData") is NSArray){
                        let aryUserdefinedata = match.value(forKey: "udfmasterData") as! NSArray
                        let aryTemp = aryUserdefinedata.filter { (task) -> Bool in
                            return "\((task as! NSDictionary).value(forKey: "Entity")!)".lowercased() ==  type.lowercased()
                        }
                        if(aryTemp.count != 0){
                            if(aryTemp[0]  is NSDictionary){
                                let dict = aryTemp[0] as! NSDictionary
                                if dict.value(forKey: "FormJson") is NSArray {
                                    aryMasterData = (dict.value(forKey: "FormJson") as! NSArray).mutableCopy() as! NSMutableArray
                                }
                                else if (dict.value(forKey: "FormJson") is String) {
                                    aryMasterData  = convertToArray(text: (dict.value(forKey: "FormJson") as! String)).mutableCopy() as! NSMutableArray
                                } else {
                                    
                                }
                            }
                        }
                        
                        print("Totatal Count-------\(aryUserdefinedata.count)")
                        
                        print("aryMasterData-------\(aryMasterData.count)")
                    }
                
                }
              
            }
        }
        return aryMasterData
    }
    
    
    func getUserdefineSaveData(type: String , id : String) -> NSMutableArray {
        var arySaveData = NSMutableArray()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, predicate: NSPredicate(format: "companyKey == %@ && leadId == %@", Global().getCompanyKey(),id))
        if(arryOfData.count != 0){
            for item in arryOfData
            {
                if(item is NSManagedObject){
                    let match = item as! NSManagedObject
                    if(match.value(forKey: "userdefinedata") is NSDictionary){
                        let dict = (match.value(forKey: "userdefinedata") as! NSDictionary)
                        let strtype = "\(dict.value(forKey: "Type") ?? "")"
                        if strtype == type {
                            if dict.value(forKey: "userDefinedFieldObj") is NSArray {
                                arySaveData = (dict.value(forKey: "userDefinedFieldObj") as! NSArray).mutableCopy() as! NSMutableArray
                            }
                        }
                    }
                }
                print("Totatal Count-------\(arySaveData)")
                print("UDFSavedData-------\(arySaveData.count)")
            }
        }
        return arySaveData
    }
    
}
//MARK: -------------------------------------
//MARK: -----UITextViewDelegate

extension UserDefineFiledVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        let headerTag = textView.tag/10000
        UpdateValueWhenClickOn_TextView_TextFiled(headertag: headerTag, strvalue: "\(textView.text!)")
    }
}

//MARK: -------------------------------------
//MARK: -----UITextFieldDelegate

extension UserDefineFiledVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        DispatchQueue.main.async { [self] in
            UserDefineFiledVC().CreatFormUserdefine( strid: self.strID,aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                
            }
        }
        
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let otherTag = textField.tag%10000
        let headerTag = textField.tag/10000
        let objMaster = (aryUDFMasterData.object(at: headerTag) as! NSDictionary)
        let strtype = "\(objMaster.value(forKey: "type") ?? "")"
        let strsubtype = "\(objMaster.value(forKey: "subtype") ?? "")"
        //for othrt filed
        if(otherTag == 99){
            Update_OtherValueWhenClickOnTextFiled(headertag: headerTag, strvalue: "\(textField.text!)")
        }
        //for end Other filed
        else{
            
            
            if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_Sub){
                
            } else if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_SubEmail){
                if textField.text?.count != 0{
                    let isValid = Global().checkIfCommaSepartedEmailsAreValid(textField.text!)
                    
                    if !isValid {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid email!", viewcontrol: self)
                    }
                }
                
            }else if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_SubPhone){
                if textField.text?.count != 0{
                    if textField.text!.count < 12 {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid phone!", viewcontrol: self)
                    }
                }
                print("DynemicFormCheck.phone")
            }
            
            UpdateValueWhenClickOn_TextView_TextFiled(headertag: headerTag, strvalue: "\(textField.text!)")
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        let headerTag = textField.tag/10000
        let objMaster = (aryUDFMasterData.object(at: headerTag) as! NSDictionary)
        let strtype = "\(objMaster.value(forKey: "type") ?? "")"
        let strsubtype = "\(objMaster.value(forKey: "subtype") ?? "")"
        let otherTag = textField.tag%10000
        //for othrt filed
        if(otherTag == 99){
            return true
        }
        //for end Other filed
        
        
        if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_Sub){
            
        } else if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_SubEmail){
            if textField.text?.count != 0{
                let isValid = Global().checkIfCommaSepartedEmailsAreValid(textField.text!)
                
                if !isValid {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid email!", viewcontrol: self)
                }
            }
            
        }else if(strtype == Userdefinefiled_DynemicForm.text && strsubtype == Userdefinefiled_DynemicForm.text_SubPhone){
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = textField.text!
                strTextt = String(strTextt.dropLast())
                textField.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
            
        }
        
        else if(strtype == Userdefinefiled_DynemicForm.number){
            return txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 25)
        }
        
        return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 85)
    }
    func saveUDFInDataBase(type: String , aryKeys : NSMutableArray , aryValues : NSMutableArray , strleadId : String ) {
        
        let strID = strleadId
        let aryBillingAddress = UserDefineFiledVC().getUserdefineSaveData(type: Userdefinefiled_DynemicForm_Type.BillingAddress, id: strID)
        let aryServiceAddress = UserDefineFiledVC().getUserdefineSaveData(type: Userdefinefiled_DynemicForm_Type.ServiceAddress, id: strID)

        deleteAllRecordsFromDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, predicate: NSPredicate(format: "companyKey == %@ && leadId == %@", Global().getCompanyKey(),strleadId))
        
        if type == Userdefinefiled_DynemicForm_Type.ServiceAddress {
            
            
            saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, arrayOfKey: aryKeys, arrayOfValue: aryValues)
            
            if aryBillingAddress.count != 0 {
              
                var arykeysTemp = NSMutableArray()
                var aryValuesTemp = NSMutableArray()
                let dictSave = NSMutableDictionary()
                dictSave.setValue(Userdefinefiled_DynemicForm_Type.BillingAddress, forKey: "Type")
                dictSave.setValue(aryBillingAddress, forKey: "userDefinedFieldObj")

                arykeysTemp = ["leadId",
                             "userName","companyKey",
                             "userdefinedata"]
                aryValuesTemp = [strleadId,
                               Global().getUserName(),
                               Global().getCompanyKey(),
                               dictSave]
                
                    saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, arrayOfKey: arykeysTemp, arrayOfValue: aryValuesTemp)
              
            }
            
        }else if type == Userdefinefiled_DynemicForm_Type.BillingAddress {
            saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, arrayOfKey: aryKeys, arrayOfValue: aryValues)
            if aryServiceAddress.count != 0 {
              
                var arykeysTemp = NSMutableArray()
                var aryValuesTemp = NSMutableArray()
                let dictSave = NSMutableDictionary()
                dictSave.setValue(Userdefinefiled_DynemicForm_Type.ServiceAddress, forKey: "Type")
                dictSave.setValue(aryServiceAddress, forKey: "userDefinedFieldObj")

                arykeysTemp = ["leadId",
                             "userName","companyKey",
                             "userdefinedata"]
                aryValuesTemp = [strleadId,
                               Global().getUserName(),
                               Global().getCompanyKey(),
                               dictSave]
                
                    saveDataInDB(strEntity: Userdefinefiled_Entity.Entity_UDFSavedData, arrayOfKey: arykeysTemp, arrayOfValue: aryValuesTemp)
              
            }
        }
    }
}


// MARK: - SalesNew_SelectionProtocol
// MARK: -

extension UserDefineFiledVC: SalesNew_SelectionProtocol
{
    
    func didSelect(aryData: NSMutableArray, tag: Int) {
        
        let arySleected = NSMutableArray()
        for item in aryData {
            if((item as AnyObject) is NSDictionary){
                let dict = (item as AnyObject) as! NSDictionary
                let  value = "\(dict.value(forKey: "value") ?? "")"
                arySleected.add(value)
            }
         
        }
        let headerTag = tag/10000
        let sectionTag = tag%10000
        print("-----------------\(headerTag)")
        print("-----------------\(sectionTag)")
        if((aryUDFMasterData.object(at: headerTag) is NSDictionary)){
            let objMaster = (aryUDFMasterData.object(at: headerTag) as! NSDictionary)
            
            var objcSub = NSDictionary()
            if(objMaster.value(forKey: "values") is NSArray){
                
                if((objMaster.value(forKey: "values") as! NSArray).object(at: sectionTag) is NSDictionary){
                    
                    objcSub =  (objMaster.value(forKey: "values")as! NSArray).object(at: sectionTag) as! NSDictionary
                }
            }
           
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            
            let temp = aryUDFSaveData.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
            if temp.count == 0 {
                if(objcSub.count != 0){
                    let dict = NSMutableDictionary()
                    let ary = NSMutableArray()
                    ary.add("\(objcSub.value(forKey: "value") ?? "")")
                    dict.setValue(arySleected, forKey: "userData")
                    dict.setValue(strControlID, forKey: "name")
                    aryUDFSaveData.add(dict)
                }
               
            }else{
                for (index, element) in aryUDFSaveData.enumerated() {
                    if(((element as AnyObject) is NSDictionary)){
                        let objResponce = ((element as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if("\(objResponce.value(forKey: "name")!)"  == "\(strControlID)"){
                            objResponce.setValue(arySleected, forKey: "userData")
                            aryUDFSaveData.replaceObject(at: index, with: objResponce)
                        }
                    }
                  
                }
            }
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine( strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                    //            self.height_viewUserdefinefiled.constant = CGFloat(height)
                    //            self.aryUserdefineData = aryUserdefine
                    
                }}
        }
        
   
    }
    
    func didSelect(dictData: NSDictionary, tag: Int) {
        if(dictData.count != 0){
            let arySleected = NSMutableArray()
            let  value = "\(dictData.value(forKey: "value") ?? "")"
            arySleected.add(value)
            
            
            let headerTag = tag/10000
            let sectionTag = tag%10000
            print("-----------------\(headerTag)")
            print("-----------------\(sectionTag)")
            let objMaster = (aryUDFMasterData.object(at: headerTag) as! NSDictionary)
            let objcSub = (objMaster.value(forKey: "values")as! NSArray).object(at: sectionTag) as! NSDictionary
            let strControlID = "\(objMaster.value(forKey: "name") ?? "")"
            
            let temp = aryUDFSaveData.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "name")!)" == "\(strControlID)" } as NSArray
            if temp.count == 0 {
                let dict = NSMutableDictionary()
                let ary = NSMutableArray()
                ary.add("\(objcSub.value(forKey: "value") ?? "")")
                dict.setValue(arySleected, forKey: "userData")
                dict.setValue(strControlID, forKey: "name")
                aryUDFSaveData.add(dict)
            }else{
                for (index, element) in aryUDFSaveData.enumerated() {
                    if((element as AnyObject) is NSDictionary){
                        let objResponce = ((element as AnyObject) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        if("\(objResponce.value(forKey: "name")!)"  == "\(strControlID)"){
                            objResponce.setValue(arySleected, forKey: "userData")
                            aryUDFSaveData.replaceObject(at: index, with: objResponce)
                        }
                    }
                 
                }
            }
            DispatchQueue.main.async { [self] in
                UserDefineFiledVC().CreatFormUserdefine( strid: self.strID, aryJson_Master: aryUDFMasterData, aryResponceObj:  aryUDFSaveData, viewUserdefinefiled:  viewContain , controllor: viewControllor) { aryUserdefine, aryUserdefineResponce, height in
                    //            self.height_viewUserdefinefiled.constant = CGFloat(height)
                    //            self.aryUserdefineData = aryUserdefine
                    
                }}
        }
        
    }
}



extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .touchUpInside, _ closure: @escaping()->()) {
        @objc class ClosureSleeve: NSObject {
            let closure:()->()
            init(_ closure: @escaping()->()) { self.closure = closure }
            @objc func invoke() { closure() }
        }
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, "\(UUID())", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

@available(iOS 13.0, *)
extension ACFloatingTextField {
fileprivate func setPasswordToggleImage(_ button: UIButton) {
    if(!isSecureTextEntry){
        button.setImage(UIImage(systemName: "eye"), for: .normal)
      
    }else{
      
        button.setImage(UIImage(systemName: "eye.slash"), for: .normal)
    }
}
func enablePasswordToggle(){
    let button = UIButton(type: .custom)
    setPasswordToggleImage(button)
    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
    button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
    button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
    button.tintColor = hexStringToUIColor(hex: appThemeColor)
    self.rightView = button
    self.rightViewMode = .always
}
@objc func togglePasswordView(_ sender: Any) {
    self.isSecureTextEntry = !self.isSecureTextEntry
    setPasswordToggleImage(sender as! UIButton)
}
}
