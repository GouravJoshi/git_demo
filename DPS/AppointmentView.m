//
//  AppointmentView.m
//  DPS
//  Saavan Patidar 2021
//  Created by Rakesh Jain on 20/06/16.
//  Copyright © 2016 Saavan. All rights reserved.
//

//  Saavan Patidar 2021

#import "SalesAutomationSelectService.h"
#import "AllImportsViewController.h"
#import "AppointmentView.h"

#import "AppointmentAllView.h"
#import "UpcomingAppointmentTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "GeneralInfoAppointmentView.h"
#import "DejalActivityView.h"
#import "Reachability.h"
#import "Global.h"
#import "Header.h"
#import "ServiceAutomation.h"
#import "ServiceAutomation+CoreDataProperties.h"
#import <MessageUI/MessageUI.h>
#import "SaleAutomationGeneralInfo.h"
#import "AppDelegate.h"
#import "WorkOrderDetailsService.h"
#import "WorkOrderDetailsService+CoreDataProperties.h"
#import "PaymentInfoServiceAuto.h"
#import "PaymentInfoServiceAuto+CoreDataProperties.h"
#import "ServiceAutoCompanyDetails.h"
#import "ServiceAutoCompanyDetails+CoreDataProperties.h"
#import "WorkorderDetailChemicalListService.h"
#import "WorkorderDetailChemicalListService+CoreDataProperties.h"
#import "EmailDetailServiceAuto.h"
#import "EmailDetailServiceAuto+CoreDataProperties.h"
//#import "ImageDetailsServiceAuto.h"
#import "ImageDetailsServiceAuto+CoreDataProperties.h"
#import "ServiceAutoModifyDate.h"
#import "ServiceAutoModifyDate+CoreDataProperties.h"
#import "SalesAutoModifyDate.h"
#import "SalesAutoModifyDate+CoreDataProperties.h"
#import "MBProgressHUD.h"
#import "SalesDynamicInspection+CoreDataProperties.h"
#import "SalesDynamicInspection.h"
#import "ServiceDynamicForm+CoreDataProperties.h"
//#import "ServiceDynamicForm.h"
#import "ServiceDynamicForm+CoreDataClass.h"
#import "InitialSetUp.h"
#import "WorkOrderEquipmentDetails+CoreDataProperties.h"
#import "EquipmentDynamicForm+CoreDataClass.h"
#import "EquipmentDynamicForm+CoreDataProperties.h"
#import "SendMailViewController.h"
//#import "SalesAutomationAppoint+CoreDataProperties.h"
//#import "SalesAutomationAppoint.h"
#import <CoreLocation/CoreLocation.h>
#import "DPS-Swift.h"

@class WebService;

@interface AppointmentView ()<CLLocationManagerDelegate>
{
    NSMutableArray *arrOfTotalWorkerOrder,*arrResponseInspection;
    Global *global;
    NSString *strServiceUrlMainServiceAutomation,*strEmpId,*strCompanyKeyy;
    NSMutableData *responseData;
    NSDictionary *ResponseDict,*dictForAppoint,*dictForWorkOrderAppointment;
    NSString *strCompanyKey,*strEmpID,*strUserName,*strSalesAutoMainUrl,*strEmployeeNumber,*strEmployeeEProfileUrl,*strServiceUrlMainCoreServiceModule;
    NSString *strGlobalWorkOrderId;
    int countForServiceOrder;
    NSMutableArray *arrOfSalesWorkOrderNew,*filteredArray,*arrOfSalesLeadToFetchDynamicData,*arrOfServiceLeadToFetchDynamicData;
    BOOL yesFiltered,isTodayAppointments;
    NSMutableDictionary *dictFinal;
    NSString *strLeadIdGlobal,*strIsGGQIntegration,*strGGQCompanyKey,*strWorkOrderIdGlobal;
    NSString *strModifyDateToSendToServerAll;
    int indexDynamicFormToSend,indexToSendImage,indexToSendImageSign,indexToSendCheckImages , indexToSendDocuments , indexToSendDeviceDynamicForm;
    NSMutableArray *arrOfAllImagesToSendToServer,*arrOfAllSignatureImagesToSendToServer,*arrOfAllCheckImageToSend,*arrOfAllDocumentsToSend;
    BOOL yesTodayAppointments , isWoEquipIdPresent;
    UIDatePicker *pickerDate;
    UIView *viewForDate;
    UIView *viewBackGround;
    UITableView *tblData;
    NSString *strDate;
    NSString *strGlobalFromDate,*strGlobalToDate,*strWorkOrderIdOnMyWaySentSMS,*strAudioNameGlobal,*strConfigFromDateSalesAuto,*strConfigFromDateServiceAuto;
    
    NSMutableArray *arrDataTblView;
    //Nilind 16 Nov
    
    NSString *strEmployeeNo,*strEmployeeNameMain,*strEmpName;
    
    //..........
    NSManagedObject *objServiceWorkOrderGlobal,*objSalesLeadIdGlobal;
    BOOL isDeletedFirstTime,isSalesAutoEnabled,isServiceAutoEnabled,isSearchBarFilter;
    NSDate *fromDateMinimum,*toDateMaximum;
    //Nilind 19 May
    NSMutableArray * arrOnTimeSoldStandard,*arrOneTimeSoldNonStandard,*arrNonOneTime;
    NSMutableArray *arrCheckImage;
    
    //Nilind 08 Dec
    NSString *strTypeOfAppointment;
    NSString
    *strIsServiceActive;
    NSString *
    strServiceUrlMainSales,*strElectronicSign,*strCoreCompanyId,*strWorkOrderNoLifeStyle;
    NSArray *arrOfEmpBlockTime;
    
     NSMutableArray *arrAllTargetImages,*arrUploadOtherDocument,*arrUploadOtherDocumentId;

}

@end

@implementation AppointmentView

- (void)viewDidLoad {
    
    isSearchBarFilter=NO;
    isDeletedFirstTime=YES;
    isSalesAutoEnabled=NO;
    isServiceAutoEnabled=NO;
    indexDynamicFormToSend=0;
    indexToSendImage=0;
    indexToSendImageSign=0;
    indexToSendCheckImages=0;
    indexToSendDocuments=0;
    indexToSendDeviceDynamicForm=0;
    countForServiceOrder=-1;
    strLeadIdGlobal=@"";
    strWorkOrderIdGlobal=@"";
    yesFiltered=NO;
    isTodayAppointments=YES;
    isWoEquipIdPresent=NO;
    
    [super viewDidLoad];
    
    arrOfSalesWorkOrderNew=[[NSMutableArray alloc]init];
    arrOfSalesLeadToFetchDynamicData=[[NSMutableArray alloc]init];
    arrOfServiceLeadToFetchDynamicData=[[NSMutableArray alloc]init];
    filteredArray=[[NSMutableArray alloc]init];
    dictFinal=[[NSMutableDictionary alloc]init];
    arrCheckImage=[[NSMutableArray alloc]init];
    
    
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    //[pickerDate setMinimumDate:[NSDate date]];
    
    //============================================================================
    //============================================================================
    
    tblData=[[UITableView alloc]init];
    tblData.dataSource=self;
    tblData.delegate=self;
    // Uiview Boundary color
    // [tblData.layer setBorderColor: [[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1] CGColor]];
    [tblData.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [tblData.layer setBorderWidth: 2.0];
    tblData.layer.cornerRadius=20;
    tblData.clipsToBounds = YES;
    
    // arrDataTblView=[[NSMutableArray alloc]init];
    
    //hide extra separators
    tblData.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    _tblViewAppointment.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //============================================================================
    //============================================================================
    
    //============================================================================
    //============================================================================
    
    global = [[Global alloc] init];
    
    //============================================================================
    //============================================================================
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    strCompanyKeyy =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpId      =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.CompanyId"];
    strServiceUrlMainServiceAutomation=[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ServiceUrl"];
    //Nilind 16 Nov
    
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    strEmployeeNameMain=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    strServiceUrlMainSales=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    strServiceUrlMainCoreServiceModule =[dictLoginData valueForKeyPath:@"Company.CompanyConfig.CoreServiceModule.ServiceUrl"];

    //................
    strEmpName =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeName"]];
    
    strCompanyKey     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    strEmpID        =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    strUserName       =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    strEmployeeNumber=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeNumber"]];
    strEmployeeEProfileUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"EmployeeEprofile"]];
    strIsGGQIntegration=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.IsGGQIntegration"]];
    
    strGGQCompanyKey=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.GGQCompanyKey"]];
    
    //============================================================================
    //============================================================================
    
    strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
    
    strConfigFromDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.FromDate"]];
    
    strConfigFromDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.FromDate"]];
    strIsServiceActive=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.IsActive"]];
    
    NSString *strConfigTooDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ToDate"]];
    
    NSString *strConfigTooDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.ToDate"]];
    
    strCoreCompanyId =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CoreCompanyId"]];

    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    [dateFormat setTimeZone:outputTimeZone];
    NSDate *fromDateSalesAuto=[dateFormat dateFromString:strConfigFromDateSalesAuto];
    NSDate *fromDateServiceAuto=[dateFormat dateFromString:strConfigFromDateServiceAuto];
    NSDate *tooDateSalesAuto=[dateFormat dateFromString:strConfigTooDateSalesAuto];
    NSDate *tooDateServiceAuto=[dateFormat dateFromString:strConfigTooDateServiceAuto];
    
    BOOL isFromDateSalesGreater=[global isGreaterDate:fromDateSalesAuto :fromDateServiceAuto];
    BOOL isTooDateSalesGreater=[global isGreaterDate:tooDateSalesAuto :tooDateServiceAuto];
    
    if (!isFromDateSalesGreater) {
        [pickerDate setMinimumDate:fromDateSalesAuto];
        fromDateMinimum=fromDateSalesAuto;
    } else {
        [pickerDate setMinimumDate:fromDateServiceAuto];
        fromDateMinimum=fromDateServiceAuto;
    }
    
    if (isTooDateSalesGreater) {
        [pickerDate setMaximumDate:tooDateSalesAuto];
        toDateMaximum=tooDateSalesAuto;
    } else {
        [pickerDate setMaximumDate:tooDateServiceAuto];
        toDateMaximum=tooDateServiceAuto;
    }
    
    _tblViewAppointment.rowHeight=UITableViewAutomaticDimension;
    _tblViewAppointment.estimatedRowHeight=200;
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    // Adding the swipe gesture on image view
    [self.view addGestureRecognizer:swipeLeft];
    
    //[self advanceSearchButton];
    
    [self getEmpBlockTime];

    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        // [self salesFetch];
        [self performSelector:@selector(salesFetch) withObject:nil afterDelay:0.2];
    }
    else{
        
        // [self salesAutomationInfo];
        
        NSString *strActiveServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.IsActive"]];
        NSString *strActiveSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.IsActive"]];
        
        if (([strActiveSalesAuto isEqualToString:@"1"] || [strActiveSalesAuto isEqualToString:@"True"] || [strActiveSalesAuto isEqualToString:@"true"]) && ([strActiveServiceAuto isEqualToString:@"1"] || [strActiveServiceAuto isEqualToString:@"True"] || [strActiveServiceAuto isEqualToString:@"true"])) {
            
            isSalesAutoEnabled=YES;
            isServiceAutoEnabled=YES;
            
            [self performSelector:@selector(salesAutomationInfo) withObject:nil afterDelay:0.2];
            
            
        }else if ([strActiveSalesAuto isEqualToString:@"1"] || [strActiveSalesAuto isEqualToString:@"True"] || [strActiveSalesAuto isEqualToString:@"true"]){
            isSalesAutoEnabled=YES;
            isServiceAutoEnabled=NO;
            
            [self performSelector:@selector(salesAutomationInfo) withObject:nil afterDelay:0.2];
            
        } else if ([strActiveServiceAuto isEqualToString:@"1"] || [strActiveServiceAuto isEqualToString:@"True"] || [strActiveServiceAuto isEqualToString:@"true"]){
            isSalesAutoEnabled=NO;
            isServiceAutoEnabled=YES;
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Service Appointments..."];
            
            if (isServiceAutoEnabled) {
                
                [self performSelector:@selector(downloadTotalWorkOrdersServiceAutomation) withObject:nil afterDelay:0.2];
                
            } else {
                
                [self salesFetch];
                [DejalBezelActivityView removeView];
                
            }
            
        }
        
    }
    
    //    // create a label
    //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 50)];
    //    label.text = @"Drag me!";
    //
    //    // enable touch delivery
    //    label.userInteractionEnabled = YES;
    //
    //    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]
    //                                        initWithTarget:self
    //                                        action:@selector(labelDragged:)];
    //    [label addGestureRecognizer:gesture];
    //
    //    // add it
    //    [self.view addSubview:label];
    // Do any additional setup after loading the view.
    
    _tblViewAppointment.rowHeight=UITableViewAutomaticDimension;
    _tblViewAppointment.estimatedRowHeight=200;
    _tblViewAppointment.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    
    [_tblViewAppointment setSeparatorColor:[UIColor clearColor]];
    
    /* if (_tblViewAppointment.hidden==YES)
     {
     // [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
     
     //Y action Today hai con
     isTodayAppointments=YES;
     [_lblLineAll setHidden:YES];
     [_lblLineToday setHidden:NO];
     [_searchBarr resignFirstResponder];
     _searchBarr.hidden=YES;
     [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
     
     [self salesFetch];
     }*/
    
    NSUserDefaults *defsssNo=[NSUserDefaults standardUserDefaults];
    [defsssNo setValue:@"" forKey:@"lblThirdPartyAccountNo"];
    [defsssNo synchronize];

    //[self getEmpBlockTime];
    
    //[self performSelector:@selector(getEmpBlockTime) withObject:nil afterDelay:0.2];
    
}
- (void)labelDragged:(UIPanGestureRecognizer *)gesture
{
    UILabel *label = (UILabel *)gesture.view;
    CGPoint translation = [gesture translationInView:label];
    
    // move label
    label.center = CGPointMake(label.center.x + translation.x,
                               label.center.y + translation.y);
    
    // reset translation
    [gesture setTranslation:CGPointZero inView:label];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//============================================================================
//============================================================================
#pragma mark Handle Swipe
//============================================================================
//============================================================================

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {
        
    }
    else
    {
        //        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
        //                                                                 bundle: nil];
        //        AppointmentAllView
        //        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentAllView"];
        //        [self.navigationController pushViewController:objByProductVC animated:NO];
    }
}


//============================================================================
#pragma mark- Actions Buttons
//============================================================================

- (IBAction)action_Refresh:(id)sender {
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
    //                                                             bundle: nil];
    //    AppointmentView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    UIImage *imageToCheckFor = [UIImage imageNamed:@"Command-RefreshGroup.png"];
    
    UIImage *img = [_btn_Refreshh imageForState:UIControlStateNormal];
    
    NSData *imgData1 = UIImagePNGRepresentation(imageToCheckFor);
    
    NSData *imgData2 = UIImagePNGRepresentation(img);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    
    if(isCompare==true)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        AppointmentView
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentView"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
        
    }else{
        [_searchBarr resignFirstResponder];
        _searchBarr.hidden=YES;
        yesFiltered=NO;
        [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
        [_tblViewAppointment reloadData];
    }
}

- (IBAction)backAction:(id)sender
{
    //    for (UIViewController *controller in self.navigationController.viewControllers)
    //    {
    //        if ([controller isKindOfClass:[DashBoardView class]])
    //        {
    //            [self.navigationController popToViewController:controller animated:NO];
    //            break;
    //        }
    //    }
     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    if ([defs boolForKey:@"fromCompanyVC"])
    {
        [defs setBool:NO forKey:@"fromCompanyVC"];
        [defs synchronize];
        [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        
        //[self.navigationController popViewControllerAnimated:NO];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"DashBoard"
                                                                 bundle: nil];
        DashBoardNew_iPhoneVC
        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"DashBoardNew_iPhoneVC"];
        [self.navigationController pushViewController:objByProductVC animated:NO];
        
    }
}

- (IBAction)actionAllAppointments:(id)sender {
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
    //                                                             bundle: nil];
    //    AppointmentAllView
    //    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"AppointmentAllView"];
    //    [self.navigationController pushViewController:objByProductVC animated:NO];
    
    //[_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    [_searchBarr resignFirstResponder];
    _searchBarr.hidden=YES;
    // yesFiltered=NO;
    [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
    //   [_tblViewAppointment reloadData];
    
    isTodayAppointments=NO;
    [_lblLineAll setHidden:NO];
    [_lblLineToday setHidden:YES];
    //  yesFiltered=NO;
    [self salesFetch];
    
    if (yesFiltered) {
        
        if (isSearchBarFilter) {
            yesFiltered=NO;
            [_tblViewAppointment reloadData];
        } else {
            
            [self applyFilterIfThr];
            
        }
        
    }
    
}



- (IBAction)action_Todayy:(id)sender {
    // [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    
    //Y action Today hai con
    isTodayAppointments=YES;
    [_lblLineAll setHidden:YES];
    [_lblLineToday setHidden:NO];
    // yesFiltered=NO;
    [_searchBarr resignFirstResponder];
    _searchBarr.hidden=YES;
    [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
    // [_tblViewAppointment reloadData];
    
    [self salesFetch];
    
    if (yesFiltered) {
        
        if (isSearchBarFilter) {
            yesFiltered=NO;
            [_tblViewAppointment reloadData];
        } else {
            
            [self applyFilterIfThr];
            
        }
        
    }
    
}

//============================================================================
#pragma mark- TABLE VIEW DELEGATE METHODS
//============================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==1) {
        
        if (yesFiltered) {
            return filteredArray.count;
        }else{
            return arrAllObj.count;
        }
    } else {
        
        return  [arrDataTblView count];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==1)
    {
        
        NSManagedObject *objmatches;
        
        if (yesFiltered)
        {
            
            objmatches=filteredArray[indexPath.row];
            
        }
        else if ([objmatches isKindOfClass:[LeadDetail class]])
        {
            
            NSString *strIsInvisible = [NSString stringWithFormat:@"%@",[objmatches valueForKey:@"isInvisible"]];
            
            if ([strIsInvisible isEqualToString:@"true"] || [strIsInvisible isEqualToString:@"True"] || [strIsInvisible isEqualToString:@"1"]) {
                
                return 0;
                
            } else {
                
                return UITableViewAutomaticDimension;
                
            }
            
        }
        else
        {
            
            objmatches=arrAllObj[indexPath.row];
            
        }
        
        if (![objmatches isKindOfClass:[NSManagedObject class]])
        {
            
            return UITableViewAutomaticDimension;
            
        }else{
            
            return UITableViewAutomaticDimension;
            
        }
    }else{
        if ([UIScreen mainScreen].bounds.size.height==667) {
            return 50;
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            return 50;
        }
        else
            return 40;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag==1) {
        
        NSManagedObject *objmatches;
        
        if (yesFiltered)
        {
            
            objmatches=filteredArray[indexPath.row];
            
        }
        else
        {
            
            objmatches=arrAllObj[indexPath.row];
            
        }
        
        if (![objmatches isKindOfClass:[NSManagedObject class]])
        {
            
            tblCell *cell = (tblCell *)[tableView dequeueReusableCellWithIdentifier:@"BlockTimeCell" forIndexPath:indexPath];
            
            cell.lblBlockTimeTitle.text=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"Title"]];
            cell.lblBlockTimeScheduleFrom.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"fromDate"]]];
            cell.lblBlockTimeScheduleTo.text=[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"toDate"]]];
            cell.lblBlockTimeDescriptions.text=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"Description"]];
            //cell.lblBlockTimeDescriptions.text=@"asdfjsd asdfjkj asdjkfklsadjf sadkjfklsdaj fksadjfkl sadfkjsda fkjsad lfkjsdaklfj sakldjf lksadjfklsaj fklsajdflkjsad fkljsda klfjsdklajf klsadjf klsadjfs dakfjsaldk fjsad fkjsad fkljsdalk fjdslkf dskjafksajdfkljsadf";
            //cell.lblBlockTimeTitle.text=@"asdfjsd asdfjkj asdjkfklsadjf sadkjfklsdaj fksadjfkl sadfkjsda fkjsad lfkjsdaklfj sakldjf lksadjfklsaj fklsajdflkjsad fkljsda klfjsdklajf klsadjf klsadjfs dakfjsaldk fjsad fkjsad fkljsdalk fjdslkf dskjafksajdfkljsadf";
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor whiteColor];
            [cell setSelectedBackgroundView:bgColorView];
            
            //cell.backgroundColor = [UIColor lightGrayColor];
            
            if(indexPath.row % 2 == 0){
                
                cell.backgroundColor = [UIColor whiteColor];
                
            } else{
                
                cell.backgroundColor = [UIColor lightColorForCell];
                
            }
            
            
            return cell;
            
        }else{
           
            UpcomingAppointmentTableViewCell *cell = (UpcomingAppointmentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UpcomingAppointmentCell" forIndexPath:indexPath];
            // Configure Table View Cell
            [self configureCell:cell atIndexPath:indexPath];
            return cell;
            
        }

        
    } else {
        
        static NSString *identifier=@"cell";
        UITableViewCell *cell=[tblData dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (!(arrDataTblView.count==0)) {
            
            NSInteger i;
            i=tblData.tag;
            switch (i)
            {
                case 101:
                {
                    NSDictionary *dictData=[arrDataTblView objectAtIndex:indexPath.row];
                    cell.textLabel.text=[dictData valueForKey:@"FullName"];
                    //_Btn_SalesPerson
                    break;
                }
                case 102:
                {
                    //_Btn_Priority
                    cell.textLabel.text=[arrDataTblView objectAtIndex:indexPath.row];
                    // cell.textLabel.text=[dictData valueForKey:@"Name"];
                    break;
                }
                default:
                    break;
            }
            
        }
        if ([UIScreen mainScreen].bounds.size.height==667) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }else if ([UIScreen mainScreen].bounds.size.height==736) {
            cell.textLabel.font=[UIFont systemFontOfSize:17];
        }
        else
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        return cell;
        
    }
}
- (void)configureCell:(UpcomingAppointmentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *objmatches;
    
    if (yesFiltered)
    {
        
        objmatches=filteredArray[indexPath.row];
        
    }else
    {
        
        objmatches=arrAllObj[indexPath.row];
    }
    
    if ([objmatches isKindOfClass:[LeadDetail class]])
    {
        cell.const_Lbl_OpportunityType_H.constant=18;
        NSString *strFlowTypeNew;
        strFlowTypeNew=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"flowType"]];
        if ([strFlowTypeNew isEqualToString:@"Commercial"] || [strFlowTypeNew isEqualToString:@"commercial"])
        {
            cell.lblOpportunityType.text=@"Opportunity Type: Commercial"; //Opportunity
        }
        else
        {
            cell.lblOpportunityType.text=@"Opportunity Type: Residential"; //Opportunity
        }
        
        //Nilind 28 Feb
        NSString *strEmail;
        strEmail=[NSString stringWithFormat:@"Email: %@",[objmatches valueForKey:@"primaryEmail"]];
        if(strEmail.length==0 || [strEmail isEqualToString:@""])
        {
            strEmail=@"N/A";
        }
        NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:strEmail];
        [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
        
        cell.lblEmailId.attributedText=titleString;
        
        //cell.lblTechName.text=[NSString stringWithFormat:@"Tech Name: %@",strEmpName];
        //cell.lblTechName.text=[NSString stringWithFormat:@"Tech Name: %@",[global strFullName:(NSDictionary*)objmatches]];
        cell.lblTechName.text=[NSString stringWithFormat:@"Sales Representative: %@",[self getInspectorNameFromId:[objmatches valueForKey:@"salesRepId"]]];
        
        
        cell.lblSpecialInstruction.text=[NSString stringWithFormat:@"Special Instructions: %@",@"N/A"];
        cell.lblSpecialAttribute.text=[NSString stringWithFormat:@"Service Attribute: %@",@"N/A"];
        //End
        
        
        //Nilind 31 Dec
        cell.lblLeadIddd.text=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"leadId"]];
        cell.lblCompanyName.text=[NSString stringWithFormat:@"Company Name: %@",[objmatches valueForKey:@"companyName"]];
        if ([NSString stringWithFormat:@"%@",[objmatches valueForKey:@"companyName"]].length==0)
        {
            cell.lblCompanyName.text=@"Company Name: N/A";
        }
        
        cell.btnCreateInitialSetup.hidden=YES;
        
        NSString *strInitialSetupStatus,*strLeadStatus,*strLeadIdSold,*strIsSendProposalStatus,*strStageSysName;
        
        //Nilind 04 Jan
        strIsSendProposalStatus=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"isProposalFromMobile"]];
        //..........
        
        strStageSysName=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"stageSysName"]];
        
        
        
        
        strLeadIdSold=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"leadId"]];
        NSArray *arrSoldCount;
        arrSoldCount= [self fetchSoldServiceCount:strLeadIdSold];
        strInitialSetupStatus=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"isInitialSetupCreated"]];
        strLeadStatus=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"statusSysName"]];
        
        //Nilind 19 May
        
        BOOL btnGenerateWorkOrder;
        btnGenerateWorkOrder=[self showGenerateWorkorderButton:strLeadIdSold];
        cell.btnCreateInitialSetup.titleLabel.numberOfLines=2;
        if (btnGenerateWorkOrder==YES)
        {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[ cell.btnCreateInitialSetup attributedTitleForState:UIControlStateNormal]];
            
            [attributedString replaceCharactersInRange:NSMakeRange(0, attributedString.length) withString:@"Generate Workorder"];
            
            [ cell.btnCreateInitialSetup setAttributedTitle:attributedString forState:UIControlStateNormal];
            // cell.btnCreateInitialSetup.titleLabel.text=@"Generate Workorder";
            //[cell.btnCreateInitialSetup setAttributedTitle:@"Generate Workorder" forState:UIControlStateNormal];
            
        }
        else
        {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[ cell.btnCreateInitialSetup attributedTitleForState:UIControlStateNormal]];
            
            [attributedString replaceCharactersInRange:NSMakeRange(0, attributedString.length) withString:@"Create Initial Setup"];
            
            [ cell.btnCreateInitialSetup setAttributedTitle:attributedString forState:UIControlStateNormal];
            //cell.btnCreateInitialSetup.titleLabel.text=@"Create Initialsetup";
        }
        //        //End
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            cell.btnCreateInitialSetup.hidden=YES;
            
        }
        else
        {
            if([strInitialSetupStatus isEqualToString:@"false"]&& [strLeadStatus isEqualToString:@"Complete"]&& arrSoldCount.count>0)
            {
                
                cell.btnCreateInitialSetup.hidden=NO;
                if([strLeadStatus isEqualToString:@"Open"])
                {
                    cell.btnCreateInitialSetup.hidden=YES;
                }
                if([strIsSendProposalStatus isEqualToString:@"true"])
                {
                    cell.btnCreateInitialSetup.hidden=YES;
                }
                
            }
            else
            {
                cell.btnCreateInitialSetup.hidden=YES;
            }
        }
        if([strIsSendProposalStatus isEqualToString:@"true"])
        {
            cell.btnCreateInitialSetup.hidden=YES;
        }
        if([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
        {
            cell.btnCreateInitialSetup.hidden=YES;
            
        }
        if([strIsServiceActive isEqualToString:@"false"]||[strIsServiceActive isEqualToString:@"False"]||[strIsServiceActive isEqualToString:@"0"])
        {
            cell.btnCreateInitialSetup.hidden=YES;
            
        }
        
        cell.btnCreateInitialSetup.tag=indexPath.row;
        [cell.btnCreateInitialSetup addTarget:self
                                       action:@selector(clickToInitialSetup:) forControlEvents:UIControlEventTouchDown];
        
        //..........
        
        //676,674,659
        cell.lbl_WorkOrderNo.text=[NSString stringWithFormat:@"Opportunity #: %@",[objmatches valueForKey:@"leadNumber"]]; //done //Opportunity
        NSString * acNumber=[NSString stringWithFormat:@"Ac #: %@",[objmatches valueForKey:@"accountNo"]];
        NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"thirdPartyAccountNo"]];
        if (strThirdPartyAccountNo.length>0) {
            acNumber =[NSString stringWithFormat:@"Ac #: %@",strThirdPartyAccountNo];
        }
        cell.lbl_WorkOrderNo.text = [NSString stringWithFormat:@"%@, %@",cell.lbl_WorkOrderNo.text,acNumber];
        
        
        cell.lbl_ServiceName.text=[NSString stringWithFormat:@"Opportunity Name: %@",[objmatches valueForKey:@"leadName"]];  //Opportunity Name
        
        NSString *strStatus;
        strStatus=[objmatches valueForKey:@"statusSysName"];
        if(strStatus.length==0)
        {
            strStatus=@"N/A";
        }
        else
        {
        }
        
        cell.lbl_Type.text=[NSString stringWithFormat:@"%@ %@-%@",[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"flowType"]],@"Sales",strStatus];
        
        
        
        //    cell.lbl_DAte.text=[NSString stringWithFormat:@"Schedule From : %@ to %@",[global ChangeDateToLocalDateOther:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"scheduleStartDate"]]],[global ChangeDateToLocalDateOther:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"scheduleEndDate"]]]];
        
        cell.lbl_DAte.text=[NSString stringWithFormat:@"Scheduled for: %@",[global ChangeDateToLocalDateOtherSaavan:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"scheduleStartDate"]]]];
        
        
        cell.lbl_Name.text=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"customerName"]];//done
        
        NSString *strFname,*strMname,*strLname;
        strFname=[NSString stringWithFormat:@"%@",[objmatches valueForKeyPath:@"firstName"]];
        
        strMname=[NSString stringWithFormat:@"%@",[objmatches valueForKeyPath:@"middleName"]];
        
        strLname=[NSString stringWithFormat:@"%@",[objmatches valueForKeyPath:@"lastName"]];
        
        NSString *strName = @"";
        
        if(strFname.length>0)
        {
            strName = [NSString stringWithFormat:@"%@",[strFname stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(strMname.length>0)
        {
            strName = [NSString stringWithFormat:@"%@ %@",strName,[strMname stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        if(strLname.length>0)
        {
            strName = [NSString stringWithFormat:@"%@ %@",strName,[strLname stringByReplacingOccurrencesOfString:@" " withString:@""]];
        }
        cell.lbl_Name.text=[NSString stringWithFormat:@"%@",strName];//done
        
        
        
        // cell.lbl_Address.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[objmatches valueForKey:@"billingAddress1"],[objmatches valueForKey:@"billingCity"],[objmatches valueForKey:@"billingState"],[objmatches valueForKey:@"billingZipcode"]]; //done
        cell.lbl_Address.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[objmatches valueForKey:@"servicesAddress1"],[objmatches valueForKey:@"serviceCity"],[objmatches valueForKey:@"serviceState"],[objmatches valueForKey:@"serviceZipcode"]];
        
        NSString *strEstimatedValue;
        strEstimatedValue=[objmatches valueForKey:@"estimatedValue"];
        if (strEstimatedValue.length==0)
        {
            strEstimatedValue=@"N/A";
            cell.lbl_Invoice.text=[NSString stringWithFormat:@"Est: $%@",strEstimatedValue];
        }
        else
        {
            cell.lbl_Invoice.text=[NSString stringWithFormat:@"Est: $%@",strEstimatedValue];
        }
        
        cell.sendSMS.tag=indexPath.row;
        cell.sendEmail.tag=indexPath.row;
        cell.sendMapView.tag=indexPath.row;
        [cell.sendSMS addTarget:self action:@selector(sendSMSForSales:) forControlEvents:UIControlEventTouchDown];
        [cell.sendEmail addTarget:self action:@selector(sendEmailForSales:) forControlEvents:UIControlEventTouchDown];
        [cell.sendMapView addTarget:self action:@selector(sendToMapViewForSales:) forControlEvents:UIControlEventTouchDown];
        //SurveyID
        NSString *strSurveID= [NSString stringWithFormat:@"%@",[objmatches valueForKey:@"surveyID"]];
        
        if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
            
            //            [cell.sendSMS setHidden:NO];
            //            [cell.sendEmail setHidden:NO];
            
            [cell.sendSMS setEnabled:YES];
            [cell.sendEmail setEnabled:YES];
            
            
            cell.const_MapBtn_Leading.constant=10;
            
            cell.backgroundColor=[UIColor clearColor];
            
        } else {
            
            //            [cell.sendSMS setHidden:YES];
            //            [cell.sendEmail setHidden:YES];
            
            [cell.sendSMS setEnabled:NO];
            [cell.sendEmail setEnabled:NO];
            
            
            if ([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568) {
                
                cell.const_MapBtn_Leading.constant=-300;
                
            } else {
                
                cell.const_MapBtn_Leading.constant=-380;
                
            }
            
            if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                
                cell.const_MapBtn_Leading.constant=-750;
                
            }
            
            cell.backgroundColor=[UIColor clearColor];
            
        }
        
        cell.btnResendMail.tag=indexPath.row;
        [cell.btnResendMail addTarget:self action:@selector(resendMailSales:) forControlEvents:UIControlEventTouchDown];
        NSString *strGlobalLeadStatus=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"isResendAgreementProposalMail"]];
        
        if ([strGlobalLeadStatus caseInsensitiveCompare:@"True"] == NSOrderedSame || [strGlobalLeadStatus caseInsensitiveCompare:@"true"] == NSOrderedSame || [strGlobalLeadStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {
            
            //            [cell.btnResendMail setHidden:NO];
            //            cell.btnPrintOption.hidden=NO;
            
            [cell.btnResendMail setEnabled:YES];
            cell.btnPrintOption.enabled=YES;
            
            
            NSString *strStatusWorkOrder=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"statusSysName"]];
            if ([strStatusWorkOrder isEqualToString:@"Incomplete"] || [strStatusWorkOrder isEqualToString:@"InComplete"]) {
                
                //                [cell.btnResendMail setHidden:YES];
                //                cell.btnPrintOption.hidden=YES;
                
                [cell.btnResendMail setEnabled:NO];
                cell.btnPrintOption.enabled=NO;
                
            }else{
                
                //                [cell.btnResendMail setHidden:NO];
                //                cell.btnPrintOption.hidden=NO;
                
                [cell.btnResendMail setEnabled:YES];
                cell.btnPrintOption.enabled=YES;
                
            }
            
        }else{
            
            //            [cell.btnResendMail setHidden:YES];
            //            cell.btnPrintOption.hidden=YES;
            
            [cell.btnResendMail setEnabled:NO];
            cell.btnPrintOption.enabled=NO;
            
        }
        
        if([strStageSysName isEqualToString:@"Lost"]||[strStageSysName isEqualToString:@"lost"])
        {
            [cell.btnResendMail setHidden:YES];
            [cell.btnResendMail setEnabled:NO];
            
        }
        cell.lblServiceInstruction.hidden=YES;
        
        cell.btnPrintOption.tag=indexPath.row;
        //cell.btnPrintOption.hidden=NO;
        [cell.btnPrintOption addTarget:self action:@selector(printOption:) forControlEvents:UIControlEventTouchDown];
        
        cell.btnPrintOption.hidden=NO;
        [cell.btnPrintOption setEnabled:YES];
        
        
        NSString *strFlowType = [NSString stringWithFormat:@"%@",[objmatches valueForKey:@"flowType"]];
        
        if ([strFlowType isEqualToString:@"Commercial"] || [strFlowType isEqualToString:@"commercial"]) {
            
            [cell.btnCreateInitialSetup setHidden:YES];
            
        }
        
        cell.lblDriveTime.text=[NSString stringWithFormat:@"Drive Time: %@",[objmatches valueForKey:@"driveTime"]];
        // cell.lblLatestStartTime.text=[NSString stringWithFormat:@"Latest Start Time: %@",[objmatches valueForKey:@"latestStartTime"]];
        //  cell.lblEarliestStartTime.text=[NSString stringWithFormat:@"Earliest Start Time: %@",[objmatches valueForKey:@"earliestStartTime"]];
        
        cell.lblLatestStartTime.text=[NSString stringWithFormat:@"Latest Start Time: %@",[global ChangeDateToLocalTimeAkshay:[objmatches valueForKey:@"latestStartTime"] type:@"time"]];
        
        cell.lblEarliestStartTime.text=[NSString stringWithFormat:@"Earliest Start Time: %@",[global ChangeDateToLocalTimeAkshay:[objmatches valueForKey:@"earliestStartTime"] type:@"time"]];//[objmatches valueForKey:@"earliestStartTime"]];
        
        
        cell.lblServiceInstruction.text = @"";
        cell.lblSpecialAttribute.text = @"";
        cell.lblSpecialInstruction.text = @"";
        
    }
    else // Service
    {
        cell.const_Lbl_OpportunityType_H.constant=0;
        cell.lblCompanyName.text=[NSString stringWithFormat:@"Company Name: %@",[objmatches valueForKey:@"companyName"]];
        if ([NSString stringWithFormat:@"%@",[objmatches valueForKey:@"companyName"]].length==0)
        {
            cell.lblCompanyName.text=@"Company Name: N/A";
        }
        cell.btnCreateInitialSetup.hidden=YES;
        cell.lbl_WorkOrderNo.text=[NSString stringWithFormat:@"Wo #: %@",[objmatches valueForKey:@"workOrderNo"]]; //done
        NSString * acNumber=[NSString stringWithFormat:@"Ac #: %@",[objmatches valueForKey:@"accountNo"]];
        
        
        NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"thirdPartyAccountNo"]];
        if (strThirdPartyAccountNo.length>0) {
            acNumber =[NSString stringWithFormat:@"Ac #: %@",strThirdPartyAccountNo];
        }
        cell.lbl_WorkOrderNo.text = [NSString stringWithFormat:@"%@, %@",cell.lbl_WorkOrderNo.text,acNumber];
        cell.lbl_ServiceName.text=[NSString stringWithFormat:@"Services: %@",[objmatches valueForKey:@"services"]];
        NSString *strStatus;
        strStatus=[objmatches valueForKey:@"workorderStatus"];
        if(strStatus.length==0)
        {
            strStatus=@"N/A";
        }
        else
        {
        }
        cell.lbl_Type.text=[NSString stringWithFormat:@"%@-%@",@"Service",strStatus];
        
        //Nilind 28 Feb
        NSString *strEmail;
        strEmail=[NSString stringWithFormat:@"Email: %@",[objmatches valueForKey:@"primaryEmail"]];
        if(strEmail.length==0 || [strEmail isEqualToString:@""])
        {
            strEmail=@"N/A";
        }
        NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:strEmail];
        [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
        
        cell.lblEmailId.attributedText=titleString;
        
        //cell.lblTechName.text=[NSString stringWithFormat:@"Tech Name: %@",strEmpName];
        cell.lblTechName.text=[NSString stringWithFormat:@"Tech Name: %@",[global getEmployeeNameViaEMPId:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"employeeNo"]]]];
        
        cell.lblSpecialInstruction.text=[NSString stringWithFormat:@"Special Instructions: %@",[objmatches valueForKey:@"specialInstruction"]];
        cell.lblServiceInstruction.hidden=NO;
        
        cell.lblServiceInstruction.text=[NSString stringWithFormat:@"Service Instruction: %@",[objmatches valueForKey:@"serviceInstruction"]];
        
        cell.lblSpecialAttribute.text=[NSString stringWithFormat:@"Service Attribute: %@",[objmatches valueForKey:@"atributes"]];
        cell.lbl_DAte.text=[NSString stringWithFormat:@"Scheduled for: %@",[global ChangeDateToLocalDateOtherServiceAppointmentsUpdated:[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"scheduleStartDateTime"]]]];
        //End
        
        cell.lbl_Name.text=[NSString stringWithFormat:@"%@",[global strFullName:(NSDictionary*)objmatches]];
        
        
        
        cell.lbl_Address.text=[NSString stringWithFormat:@"%@, %@, %@, %@",[objmatches valueForKey:@"servicesAddress1"],[objmatches valueForKey:@"serviceCity"],[objmatches valueForKey:@"serviceState"],[objmatches valueForKey:@"serviceZipcode"]]; //done ServicesAddress1
        
        NSString *strEstimatedValue;
        strEstimatedValue=[objmatches valueForKey:@"invoiceAmount"];
        if (strEstimatedValue.length==0)
        {
            strEstimatedValue=@"N/A";
            cell.lbl_Invoice.text=[NSString stringWithFormat:@"Invoice: $%@",strEstimatedValue];
        }
        else
        {
            cell.lbl_Invoice.text=[NSString stringWithFormat:@"Invoice: $%@",strEstimatedValue];
        }
        
        cell.sendSMS.tag=indexPath.row;
        cell.sendEmail.tag=indexPath.row;
        cell.sendMapView.tag=indexPath.row;
        [cell.sendSMS addTarget:self action:@selector(sendSMSForSales:) forControlEvents:UIControlEventTouchDown];
        [cell.sendEmail addTarget:self action:@selector(sendEmailForSales:) forControlEvents:UIControlEventTouchDown];
        [cell.sendMapView addTarget:self action:@selector(sendToMapViewForSales:) forControlEvents:UIControlEventTouchDown];
        
        //SurveyID
        NSString *strSurveID= [NSString stringWithFormat:@"%@",[objmatches valueForKey:@"surveyID"]];
        
        if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
            
            //            [cell.sendSMS setHidden:NO];
            //            [cell.sendEmail setHidden:NO];
            
            [cell.sendSMS setEnabled:YES];
            [cell.sendEmail setEnabled:YES];
            
            
            cell.const_MapBtn_Leading.constant=10;
            cell.backgroundColor=[UIColor clearColor];
            
        } else {
            
            //            [cell.sendSMS setHidden:YES];
            //            [cell.sendEmail setHidden:YES];
            
            [cell.sendSMS setEnabled:NO];
            [cell.sendEmail setEnabled:NO];
            
            if ([UIScreen mainScreen].bounds.size.height==480 || [UIScreen mainScreen].bounds.size.height==568) {
                
                cell.const_MapBtn_Leading.constant=-300;
                
            } else {
                
                cell.const_MapBtn_Leading.constant=-380;
                
            }
            if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
                
                cell.const_MapBtn_Leading.constant=-750;
                
            }
            
            cell.backgroundColor=[UIColor clearColor];
            
        }
        
        cell.btnResendMail.tag=indexPath.row;
        [cell.btnResendMail addTarget:self action:@selector(resendMailSales:) forControlEvents:UIControlEventTouchDown];
        
        NSString *strGlobalWorkOrderStatus=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"isResendInvoiceMail"]];
        
        NSString *strStatusWorkOrder=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"workorderStatus"]];
        
        if ([strGlobalWorkOrderStatus caseInsensitiveCompare:@"True"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"true"] == NSOrderedSame || [strGlobalWorkOrderStatus caseInsensitiveCompare:@"1"] == NSOrderedSame) {
            
            [cell.btnResendMail setHidden:NO];
            if ([strStatusWorkOrder isEqualToString:@"Incomplete"] || [strStatusWorkOrder isEqualToString:@"InComplete"] || [strStatusWorkOrder isEqualToString:@"Reset"]) {
                
                //[cell.btnResendMail setHidden:YES];
                [cell.btnResendMail setEnabled:NO];
                
            }else{
                
                //[cell.btnResendMail setHidden:NO];
                [cell.btnResendMail setEnabled:YES];
                
            }
            
        }else{
            
            //[cell.btnResendMail setHidden:YES];
            [cell.btnResendMail setEnabled:NO];
            
        }
        
        NSString *strDepartmentTYPE= [NSString stringWithFormat:@"%@",[objmatches valueForKey:@"departmentType"]];
        
        if ([strDepartmentTYPE isEqualToString:@"Mechanical"]) {
            
            //[cell.btnResendMail setHidden:YES];
            [cell.btnResendMail setEnabled:NO];
            
        }
        
        //cell.btnPrintOption.hidden=YES;
        [cell.btnPrintOption setEnabled:NO];
        
        cell.lblDriveTime.text=[NSString stringWithFormat:@"Drive Time: %@",[objmatches valueForKey:@"DriveTimeStr"]];
        //   cell.lblLatestStartTime.text=[NSString stringWithFormat:@"Latest Start Time: %@",[objmatches valueForKey:@"LatestStartTimeStr"]];
        //  cell.lblEarliestStartTime.text=[NSString stringWithFormat:@"Earliest Start Time: %@",[objmatches valueForKey:@"EarliestStartTimeStr"]];
        
        cell.lblLatestStartTime.text=[NSString stringWithFormat:@"Latest Start Time: %@",[global ChangeDateToLocalTimeAkshay:[objmatches valueForKey:@"LatestStartTimeStr"] type:@"time"]];
        
        cell.lblEarliestStartTime.text=[NSString stringWithFormat:@"Earliest Start Time: %@",[global ChangeDateToLocalTimeAkshay:[objmatches valueForKey:@"EarliestStartTimeStr"] type:@"time"]];
    }
    
    NSString *strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"serviceCellNo"]];
    
    if (strCellNo.length==0) {
        
        strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"servicePrimaryPhone"]];
        
        if (strCellNo.length==0) {
            
            strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"serviceSecondaryPhone"]];
            
            if (strCellNo.length==0) {
                
                strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"cellNo"]];
                
                if (strCellNo.length==0) {
                    
                    strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"primaryPhone"]];
                    
                    if (strCellNo.length==0) {
                        
                        strCellNo=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"secondaryPhone"]];
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    if ((strCellNo.length==0) || [strCellNo isEqualToString:@"(null)"]) {
        
        cell.btnCellNoNew.tag=indexPath.row;
        [cell.btnCellNoNew addTarget:self action:@selector(callOnCellNo:) forControlEvents:UIControlEventTouchDown];
        cell.btnCellNoNew.enabled=NO;
        
    } else {
        cell.btnCellNoNew.tag=indexPath.row;
        [cell.btnCellNoNew addTarget:self action:@selector(callOnCellNo:) forControlEvents:UIControlEventTouchDown];
        cell.btnCellNoNew.enabled=YES;
    }
    cell.lblOpportunityType.text = @"";
    
    cell.btnEmail.tag=indexPath.row;
    [cell.btnEmail addTarget:self action:@selector(emailOnId:) forControlEvents:UIControlEventTouchDown];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor whiteColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    if(indexPath.row % 2 == 0){
        
        cell.backgroundColor = [UIColor whiteColor];
        
    } else{
        
        cell.backgroundColor = [UIColor lightColorForCell];

    }
    
}


-(void)callOnCellNo:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    if (yesFiltered) {
        matches=filteredArray[btn.tag];
    }else{
        matches=arrAllObj[btn.tag];
    }
    
    NSString *strCellNo;
    
    if ([matches isKindOfClass:[LeadDetail class]])
    {
        strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCellNo"]];
        
        if (strCellNo.length==0) {
            
            strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"servicePrimaryPhone"]];
            
            if (strCellNo.length==0) {
                
                strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSecondaryPhone"]];
                
                if (strCellNo.length==0) {
                    
                    strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"cellNo"]];
                    
                    if (strCellNo.length==0) {
                        
                        strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryPhone"]];
                        
                        if (strCellNo.length==0) {
                            
                            strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"secondaryPhone"]];
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }else{
        
        
        strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceCellNo"]];
        
        if (strCellNo.length==0) {
            
            strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"servicePrimaryPhone"]];
            
            if (strCellNo.length==0) {
                
                strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceSecondaryPhone"]];
                
                if (strCellNo.length==0) {
                    
                    strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"cellNo"]];
                    
                    if (strCellNo.length==0) {
                        
                        strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryPhone"]];
                        
                        if (strCellNo.length==0) {
                            
                            strCellNo=[NSString stringWithFormat:@"%@",[matches valueForKey:@"secondaryPhone"]];
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    if ((strCellNo.length==0) || [strCellNo isEqualToString:@"(null)"]) {
        
        
        
    } else {
        
        //        NSString *newString = [[strCellNo componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        //
        //        NSString *strCellNoToCall=[NSString stringWithFormat:@"tel:%@",newString];
        //
        //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strCellNoToCall]];
        
        [global calling:strCellNo];
        
    }
    
}


-(void)emailOnId:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    if (yesFiltered) {
        matches=filteredArray[btn.tag];
    }else{
        matches=arrAllObj[btn.tag];
    }
    
    NSString *strEmailId;
    
    if ([matches isKindOfClass:[LeadDetail class]])
    {
        
        strEmailId=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]];
        
    }else{
        
        strEmailId=[NSString stringWithFormat:@"%@",[matches valueForKey:@"primaryEmail"]];
        
    }
    
    if (strEmailId.length==0) {
        
        
        
    } else {
        
        [global emailComposer:strEmailId :@"" :@"" :self];
        
    }
    
}


-(void)printOption:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    NSManagedObject *objTemp;
    if (yesFiltered) {
        objTemp=filteredArray[btn.tag];
    }else{
        objTemp=arrAllObj[btn.tag];
    }
    
    if ([objTemp isKindOfClass:[LeadDetail class]])
    {
        
        [self oPentPrinterView:[NSString stringWithFormat:@"%@",[objTemp valueForKey:@"leadNumber"]]];
        
    }else{
        
        
        
    }
}

-(void)oPentPrinterView :(NSString*)strLeadToSend{
    
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrintiPhoneViewController  *objSignViewController=[storyboard instantiateViewControllerWithIdentifier:@"PrintiPhoneViewController"];
    objSignViewController.strAgreementName=@"";
    objSignViewController.strProposalName=@"";
    objSignViewController.strInspectionName=@"";
    objSignViewController.strLeadId=strLeadToSend;
    [self.navigationController presentViewController:objSignViewController animated:YES completion:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults *defsAudio=[NSUserDefaults standardUserDefaults];
    [defsAudio setValue:@"" forKey:@"AudioNameService"];
    [defsAudio setBool:NO forKey:@"yesAudio"];
    [defsAudio synchronize];
    
    if (tableView.tag==1) {
        
        NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
        [defss setObject:nil forKey:@"salesDynamicForm"];
        [defss synchronize];
        
        if (yesFiltered) {
            
            matches=filteredArray[indexPath.row];
            
            if (![matches isKindOfClass:[NSManagedObject class]]) {
                
                
                
            }else{
                
                if ([matches isKindOfClass:[LeadDetail class]])
                {
                    
                    /* NSString *strFlowType = [NSString stringWithFormat:@"%@",[matches valueForKey:@"flowType"]];
                     
                     if ([strFlowType isEqualToString:@"Commercial"] || [strFlowType isEqualToString:@"commercial"]) {
                     
                     [global displayAlertController:Alert :@"Commercial flow coming soon..." :self];
                     
                     } else {
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                     bundle: nil];
                     SaleAutomationGeneralInfo
                     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SaleAutomationGeneralInfo"];
                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                     [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
                     [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
                     [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
                     [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
                     NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                     [defs setValue:strSurveID forKey:@"SurveyID"];
                     if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                     
                     [defs setBool:YES forKey:@"YesSurveyService"];
                     
                     }else{
                     
                     [defs setBool:NO forKey:@"YesSurveyService"];
                     
                     }
                     [defs synchronize];
                     
                     objByProductVC.matchesGeneralInfo=matches;
                     [self.navigationController pushViewController:objByProductVC animated:NO];
                     
                     }*/
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                             bundle: nil];
                    SaleAutomationGeneralInfo
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SaleAutomationGeneralInfo"];
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
                    [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
                    [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
                    [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
                    [defs setValue:[matches valueForKey:@"stageSysName"] forKey:@"stageSysNameSales"];
                    NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                    [defs setValue:strSurveID forKey:@"SurveyID"];
                    if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                        
                        [defs setBool:YES forKey:@"YesSurveyService"];
                        
                    }else{
                        
                        [defs setBool:NO forKey:@"YesSurveyService"];
                        
                    }
                    [defs synchronize];
                    
                    objByProductVC.matchesGeneralInfo=matches;
                    [self.navigationController pushViewController:objByProductVC animated:NO];
                    
                }
                else
                {
                    
                    
                    NSString *strDepartmentType=[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentType"]];
                    NSString *strServiceStateLocal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceState"]];
                    
                    if ([strDepartmentType isEqualToString:@"Termite"] && [strServiceStateLocal isEqualToString:@"California"])
                    {
                        
                        [global displayAlertController:Alert :InfoComingSoon :self];
                        
                    }else{
                        
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                                 bundle: nil];
                        GeneralInfoVC
                        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GeneralInfoVC"];
                        
                        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                        [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
                        NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                        NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
                        [defs setValue:strSurveID forKey:@"SurveyID"];
                        if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                            
                            [defs setBool:YES forKey:@"YesSurveyService"];
                            
                        }else{
                            
                            [defs setBool:NO forKey:@"YesSurveyService"];
                            
                        }
                        //departmentSysName
                        [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
                        [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
                        [defs synchronize];
                        
                        objByProductVC.strWoId = [NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderId"]];
                        
                        [self.navigationController pushViewController:objByProductVC animated:NO];
                        
                    }
                    
                    
                }
                
            }
            
            
        }else{
            
            matches=arrAllObj[indexPath.row];
            
            if (![matches isKindOfClass:[NSManagedObject class]]) {
                
                
                
            }else{
                
                
                if ([matches isKindOfClass:[LeadDetail class]]) {
                    
                    /* NSString *strFlowType = [NSString stringWithFormat:@"%@",[matches valueForKey:@"flowType"]];
                     
                     if ([strFlowType isEqualToString:@"Commercial"] || [strFlowType isEqualToString:@"commercial"]) {
                     
                     [global displayAlertController:Alert :@"Commercial flow coming soon..." :self];
                     
                     } else {
                     
                     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                     bundle: nil];
                     SaleAutomationGeneralInfo
                     *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SaleAutomationGeneralInfo"];
                     NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                     [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
                     [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
                     [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
                     [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
                     NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                     [defs setValue:strSurveID forKey:@"SurveyID"];
                     if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                     
                     [defs setBool:YES forKey:@"YesSurveyService"];
                     
                     }else{
                     
                     [defs setBool:NO forKey:@"YesSurveyService"];
                     
                     }
                     
                     [defs synchronize];
                     
                     objByProductVC.matchesGeneralInfo=matches;
                     [self.navigationController pushViewController:objByProductVC animated:NO];
                     
                     }*/
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"SalesMain"
                                                                             bundle: nil];
                    SaleAutomationGeneralInfo
                    *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"SaleAutomationGeneralInfo"];
                    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                    [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
                    [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
                    [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
                    [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
                    [defs setValue:[matches valueForKey:@"stageSysName"] forKey:@"stageSysNameSales"];
                    NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                    [defs setValue:strSurveID forKey:@"SurveyID"];
                    if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                        
                        [defs setBool:YES forKey:@"YesSurveyService"];
                        
                    }else{
                        
                        [defs setBool:NO forKey:@"YesSurveyService"];
                        
                    }
                    
                    [defs synchronize];
                    
                    objByProductVC.matchesGeneralInfo=matches;
                    [self.navigationController pushViewController:objByProductVC animated:NO];
                    
                }
                else{
                    
                    
                    NSString *strDepartmentType=[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentType"]];
                    NSString *strServiceStateLocal=[NSString stringWithFormat:@"%@",[matches valueForKey:@"serviceState"]];
                    
                    if ([strDepartmentType isEqualToString:@"Termite"] && [strServiceStateLocal isEqualToString:@"California"])
                    {
                        
                        [global displayAlertController:Alert :InfoComingSoon :self];
                        
                    }else{
                        
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"PestiPhone"
                                                                                 bundle: nil];
                        GeneralInfoVC
                        *objByProductVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"GeneralInfoVC"];
                        
                        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
                        [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
                        NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
                        NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
                        [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
                        [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
                        [defs setValue:strSurveID forKey:@"SurveyID"];
                        if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                            
                            [defs setBool:YES forKey:@"YesSurveyService"];
                            
                        }else{
                            
                            [defs setBool:NO forKey:@"YesSurveyService"];
                            
                        }
                        
                        [defs synchronize];
                        
                        objByProductVC.strWoId = [NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderId"]];
                        
                        [self.navigationController pushViewController:objByProductVC animated:NO];
                        
                    }
                    
                }
                
                
            }
            
        }
    } else {
        
        [_btn_StatusFilter setTitle:[arrDataTblView objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [viewBackGround removeFromSuperview];
        [tblData removeFromSuperview];
        
    }
}
//This function is where all the magic happens
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    //1. Setup the CATransform3D structure
//    CATransform3D rotation;
//    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
//    rotation.m34 = 1.0/ -600;
//
//
//    //2. Define the initial state (Before the animation)
//    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//
//    cell.layer.transform = rotation;
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//
//
//    //3. Define the final state (After the animation) and commit the animation
//    [UIView beginAnimations:@"rotation" context:NULL];
//    [UIView setAnimationDuration:0.8];
//    cell.layer.transform = CATransform3DIdentity;
//    cell.alpha = 1;
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    [UIView commitAnimations];
//
//}
//============================================================================
#pragma mark- METHODS Button Actions
//============================================================================

-(void)sendSMSs:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm?"
                               message:@"Do you want to send Text"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             UIButton *btn = (UIButton *)sender;
                             if (!(countForServiceOrder==-1)) {
                                 
                                 if (btn.tag<countForServiceOrder) {
                                     
                                     NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
                                     NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
                                     NSString *strNumber=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"PrimaryPhone"]];
                                     
                                     NSArray *recipients = [NSArray arrayWithObjects:strNumber, nil];
                                     
                                     [self sendNumber:recipients :dictofWorkorderDetail];
                                 }
                                 else{
                                     
                                     //Sales Wala YAha Karna hai
                                 }
                             }
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)sendEmail:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm?"
                               message:@"Do you want to send Email"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email..."];
                             
                             UIButton *btn = (UIButton *)sender;
                             
                             if (!(countForServiceOrder==-1)) {
                                 
                                 if (btn.tag<countForServiceOrder) {
                                     
                                     NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
                                     NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
                                     
                                     NSString *strUrl=[NSString stringWithFormat:@"%@%@&CompanyKey=%@",UrlSendEmailServiceAutomation,[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"WorkorderId"]],strCompanyKeyy];
                                     //============================================================================
                                     //============================================================================
                                     
                                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                         
                                         [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendEmailServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [DejalBezelActivityView removeView];
                                                  if (success)
                                                  {
                                                      NSString *returnString =[response valueForKey:@"ReturnMsg"];
                                                      if ([returnString isEqualToString:@"Successful"]) {
                                                          [global AlertMethod:Info :SuccessMailSend];
                                                      } else {
                                                          [global AlertMethod:Info :SuccessMailSend];
                                                      }
                                                  }
                                                  else
                                                  {
                                                      NSString *strTitle = Alert;
                                                      NSString *strMsg = Sorry;
                                                      [global AlertMethod:strTitle :strMsg];
                                                  }
                                              });
                                          }];
                                     });
                                 }
                                 else
                                 {
                                     //Yaha Par sales auto ki karna hai
                                 }
                             }
                             //============================================================================
                             //============================================================================
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)sendToMapView:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    if (!(countForServiceOrder==-1)) {
        
        if (btn.tag<countForServiceOrder) {
            
            // NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
            // NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
        }
        else{
            //Yaha PAr Sales Auto Ka Karna hai
        }
    }
    
    // NSString *url=[NSString stringWithFormat:@"https://maps.google.com/maps?f=q&t=m&q=%@,%@",[dictofWorkorderDetail valueForKey:@"ServiceAddressLatitude"],[dictofWorkorderDetail valueForKey:@"ServiceAddressLongitude"]];
    
    NSString *url=@"https://maps.google.com/maps?f=q&t=m&q=22.730287,75.8763365";
    
    NSURL *URL = [NSURL URLWithString:url];
    if (URL) {
        if ([SFSafariViewController class] != nil) {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (id)self;
            [self presentViewController:sfvc animated:YES completion:nil];
        } else {
            if (![[UIApplication sharedApplication] openURL:URL]) {
            }
        }
    } else {
        // will have a nice alert displaying soon.
    }
}


-(void)saveToCoreDataWorkOrdersServiceAutomation :(NSDictionary*)dictOfWorkOrdersToSave{
    
    //==================================================================================
    //==================================================================================
    
    [self DeleteFromCoreDataTotalWorkOrderssServiceAutomation];
    
    //==================================================================================
    //==================================================================================
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    
    //==================================================================================
    //==================================================================================
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    
    NSString *strEmployeeId    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeId"]];
    NSString *strCompanyKeyNew=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyKey"]];
    NSString *strUserNameNew    =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //==================================================================================
    //==================================================================================
    
    ServiceAutomation *objTotalWorkOrders = [[ServiceAutomation alloc]initWithEntity:entitytotalWorkOrders insertIntoManagedObjectContext:context];
    objTotalWorkOrders.dictOfWorkOrders=dictOfWorkOrdersToSave;
    objTotalWorkOrders.empId=strEmployeeId;
    objTotalWorkOrders.companyKey=strCompanyKeyNew;
    objTotalWorkOrders.userName=strUserNameNew;
    NSError *error1;
    [context save:&error1];
    
    //==================================================================================
    //==================================================================================
    
    // [self FetchFromCoreDataTotalWorkOrdersServiceAutomation:@""];
    
    //==================================================================================
    //==================================================================================
    
}
-(void)FetchFromCoreDataTotalWorkOrdersServiceAutomation :(NSString*)strTypefLead{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitytotalWorkOrders];
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"empId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerTotalWorkOrders = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTotalWorkOrders setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTotalWorkOrders performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerTotalWorkOrders fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        
        matches = arrAllObj[0];
        NSDictionary *dictOfWorkOrderss=[matches valueForKey:@"dictOfWorkOrders"];
        arrOfTotalWorkerOrder=[dictOfWorkOrderss valueForKey:@"WorkOrderExtSerDcs"];
        
        NSMutableArray *arrtempWorkOrder=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfTotalWorkerOrder.count; k++) {
            
            NSDictionary *dictOfWorkOrderExtSerDcs =arrOfTotalWorkerOrder[k];
            NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
            NSString *strDateToConvert=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleEndDateTime"]];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* EndDate = [dateFormatter dateFromString:strDateToConvert];
            
            NSComparisonResult result = [[NSDate date] compare:EndDate];
            if(result == NSOrderedDescending)
            {
                NSLog(@"strLocal is later than Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            else if(result == NSOrderedAscending)
            {
                NSLog(@"Current is later than strLocal");
            }
            else
            {
                NSLog(@"strLocal is equal to Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            
        }
        arrOfTotalWorkerOrder=arrtempWorkOrder;
        
        
        [_tblViewAppointment reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)DeleteFromCoreDataTotalWorkOrderssServiceAutomation
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context]];
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    if (Data.count==0) {
    } else {
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
    }
}

//============================================================================
//============================================================================
#pragma mark- ----------------Message Composer Methods----------------
//============================================================================
//============================================================================

-(void)sendNumber:(NSArray*)arrOfnumber :(NSManagedObject*)dictofWorkorderDetail{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *strOld= [def valueForKey:@"CustomMessage"];
    
    if (strOld.length==0)
    {
        strOld=@"Hello ##CustomerName##, This is ##TechName##, I am on my way to your home and should be arriving within next 30 min on your Address ##Address##.";
    }
    
    NSString *strFirstName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"FirstName"]];
    NSString *strMiddleName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"middleName"]];
    NSString *strLastName=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"lastName"]];
    
    NSMutableArray *arrOfName=[[NSMutableArray alloc]init];
    
    if (!(strFirstName.length==0)) {
        [arrOfName addObject:strFirstName];
    }
    if (!(strMiddleName.length==0)) {
        
        [arrOfName addObject:strMiddleName];
        
    }
    if (!(strLastName.length==0)) {
        
        [arrOfName addObject:strLastName];
        
    }
    
    NSString *strFullName=[arrOfName componentsJoinedByString:@" "];
    
    NSString *strFinalMsg= [strOld stringByReplacingOccurrencesOfString:@"##CustomerName##" withString:strFullName];
    
    NSString *strTech= [strFinalMsg stringByReplacingOccurrencesOfString:@"##TechName##" withString:strEmployeeNameMain];
    
    // NSString *strTime = [self calculateTimeBetweenDistane:[NSString stringWithFormat:@"%f,%f",22.7253,75.8655] :[NSString stringWithFormat:@"%f,%f",22.9600,76.0600]];
    
    NSString *strServiceAddress=[NSString stringWithFormat:@"%@ %@",[dictofWorkorderDetail valueForKey:@"servicesAddress1"],[dictofWorkorderDetail valueForKey:@"serviceAddress2"]];
    
    if (strServiceAddress.length==0)
    {
        strServiceAddress=@"N/A";
    }
    
    NSString  *strFinalMsgF= [strTech stringByReplacingOccurrencesOfString:@"##Address##" withString:strServiceAddress];
    
    if (strEmployeeEProfileUrl.length==0) {
        strEmployeeEProfileUrl=@"http://pestream.com/";
    }
    
    NSString *strFinalMsgLast= [strFinalMsgF stringByReplacingOccurrencesOfString:@"##EprofileUrl##" withString:strEmployeeEProfileUrl];
    
    NSString *strMessageFinal= [strFinalMsgLast stringByReplacingOccurrencesOfString:@"##ServiceAddress##" withString:strServiceAddress];
    
    //##ServiceAddress##
    [self sendSMS:arrOfnumber :strMessageFinal];
    
}

-(void)sendNumberForSales:(NSArray*)arrOfnumber :(NSManagedObject*)dictofWorkorderDetail{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *strOld= [def valueForKey:@"CustomMessage"];
    
    if (strOld.length==0)
    {
        // strOld=@"Hello ##CustomerName##, This is ##TechName##, I am on my way to your home and should be arriving within next 30 min on your Address ##Address##.";
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Custom Message not selected.Please go to Settings and select custom message."
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                                  
                              }];
        [alert addAction:yes];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        
        NSString *strCustomerName=[dictofWorkorderDetail valueForKey:@"CustomerName"];
        if (strCustomerName.length==0)
        {
            strCustomerName=@"N/A";
        }
        NSString *strFinalMsg= [strOld stringByReplacingOccurrencesOfString:@"##CustomerName##" withString:strCustomerName];
        
        NSString *strTechName=[dictofWorkorderDetail valueForKey:@"LeadName"];
        if (strTechName.length==0)
        {
            strTechName=@"N/A";
        }
        NSString *strTech= [strFinalMsg stringByReplacingOccurrencesOfString:@"##TechName##" withString:strEmployeeNameMain];
        
        NSString *strServiceAddress=[NSString stringWithFormat:@"%@ %@",[dictofWorkorderDetail valueForKey:@"servicesAddress1"],[dictofWorkorderDetail valueForKey:@"serviceAddress2"]];
        
        if (strServiceAddress.length==0)
        {
            strServiceAddress=@"N/A";
        }
        NSString  *strFinalMsgF= [strTech stringByReplacingOccurrencesOfString:@"##Address##" withString:strServiceAddress];
        
        if (strEmployeeEProfileUrl.length==0) {
            strEmployeeEProfileUrl=@"http://pestream.com/";
        }
        
        NSString *strFinalMsgLast=[strFinalMsgF stringByReplacingOccurrencesOfString:@"##EprofileUrl##" withString:strEmployeeEProfileUrl];//[dictofWorkorderDetail  valueForKey:@"SpecialInstruction"]];
        
        NSString *strMessageFinal= [strFinalMsgLast stringByReplacingOccurrencesOfString:@"##ServiceAddress##" withString:strServiceAddress];
        
        [self sendSMS:arrOfnumber :strMessageFinal];
        
    }
}


- (void)sendSMS:(NSArray *)recipients :(NSString*)message
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = message;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Info message:@"Your device does not support sending Text Message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [global AlertMethod:Alert :failSMSSend];
            break;
        }
            
        case MessageComposeResultSent:
        {
            [global AlertMethod:Info :SuccessSMSSend];
            if ([strTypeOfAppointment isEqualToString:@"sales"])
            {
                [self fetchSalesLeadDetails:strWorkOrderIdOnMyWaySentSMS];
                
            }
            else if ([strTypeOfAppointment isEqualToString:@"service"])
            {
                [self fetchWorkOrderDetails:strWorkOrderIdOnMyWaySentSMS];
                
            }
            else
            {
                [self fetchWorkOrderDetails:strWorkOrderIdOnMyWaySentSMS];
            }
            
            // For Sending Status in LifeStyle
            
            [self updatingStatusOnMyWayLifeStyle];
            
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


//============================================================================
//============================================================================
#pragma mark- ----------------Calculate Time Between Distane Methods----------------
//============================================================================
//============================================================================

-(NSString *)calculateTimeBetweenDistane : (NSString *)strLat : (NSString *)strLong
{
    NSString *LocationUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%@",strLat,strLong];
    // NSLog(@"Location URL:%@",LocationUrl);
    
    NSURL *finalurl = [NSURL URLWithString: LocationUrl];
    
    NSData *data = [NSData dataWithContentsOfURL:finalurl];
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions error:&error];
    
    
    NSArray *arrayRows = [json valueForKey:@"rows"];
    NSString *strTime;
    for (NSDictionary *dict in arrayRows) {
        NSArray *arrayElement =[dict valueForKey:@"elements"];
        NSDictionary *dicttemp = [arrayElement objectAtIndex:0];
        // NSLog(@"Final URL = %@",[[dicttemp valueForKey:@"duration"] valueForKey:@"text"]);
        strTime =[[dicttemp valueForKey:@"duration"] valueForKey:@"text"];
    }
    return strTime;
}


//============================================================================
//============================================================================
#pragma mark- SalesAutomation API
//============================================================================
//============================================================================

-(void)salesAutomationInfo
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        [self salesFetch];
    }
    else
    {
        
        NSString *strUrl;
        
        strUrl = [NSString stringWithFormat:@"%@%@%@&EmployeeNo=%@",strSalesAutoMainUrl,UrlSalesInfoGeneral,strCompanyKey,strEmployeeNo],//@"terminix"];
        
        NSLog(@"Sales Lead URl-----%@",strUrl);
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Appointments..."];
        
        //============================================================================
        //============================================================================
        
        NSString *strType=@"";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (success)
                     {
                         
                         if (response.count==0) {
                             
                             // [self salesFetch];
                             
                             if (isServiceAutoEnabled) {
                                 
                                 [self downloadTotalWorkOrdersServiceAutomation];
                                 
                             } else {
                                 
                                 [self salesFetch];
                                 [DejalBezelActivityView removeView];
                                 
                             }
                             
                             //                             UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Information..!!" message:@"No data Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             //                             [alert show];
                         }
                         else
                         {
                             
                             dictForAppoint=response;
                             NSLog(@"SALES RESPONSE>>>>%@",dictForAppoint);
                             NSArray *arrResponse=[[NSArray alloc]init];
                             arrResponse=[response valueForKey:@"LeadExtSerDcs"];
                             
                             //Nilind 18 Feb
                             NSDictionary *dictCompanyDetail;
                             dictCompanyDetail=[response valueForKey:@"CompanyDetail"];
                             NSUserDefaults *defsCompanu=[NSUserDefaults standardUserDefaults];
                             [defsCompanu setObject:dictCompanyDetail forKey:@"companyDetail"];
                             [defsCompanu synchronize];
                             //End
                             
                             NSDictionary *dict2=[[NSDictionary alloc]init];
                             if (!(arrResponse.count==0)) {
                                 dict2=[arrResponse objectAtIndex:0];
                             }
                             
                             for(int i=0;i<arrResponse.count;i++)
                             {
                                 NSDictionary *dict2=[[NSDictionary alloc]init];
                                 dict2=[arrResponse objectAtIndex:i];
                                 
                                 NSString *strServiceUrlMain =[dict2 valueForKeyPath:@"LeadDetail.ScheduleStartDate"];
                                 NSLog(@"%@",strServiceUrlMain);
                                 NSString *SoldServiceNonStandardDetail =[dict2 valueForKey:@"SoldServiceNonStandardDetail"];
                                 NSLog(@"%@",SoldServiceNonStandardDetail);
                             }
                             
                             //  [self HudView:@"Downloaded from server Sales Appointment"];
                             
                             [self checkifToSendSalesAppointmentToServer];
                             
                             
                         }
                         
                         // [self fetchSalesAndServiceWorkOrdersFromCoreData];
                         
                     }
                     else
                     {
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                     }
                 });
             }];
        });
    }
}
//============================================================================
//============================================================================

#pragma mark- ---------------------Sales Info Check For Syncing Methods-----------------

//============================================================================
//============================================================================

-(void)checkifToSendSalesAppointmentToServer{
    
    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    
    [requestModifyDate setPredicate:predicate];
    
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjModifyDate.count; k++) {
            
            matchesModifyDate=arrAllObjModifyDate[k];
            
            [arrOfAlreadySavedLeadss addObject:[matchesModifyDate valueForKey:@"leadIdd"]];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    if (arrAllObjModifyDate.count==0) {
        NSArray *arrLeadExtSerDcs=[dictForAppoint valueForKey:@"LeadExtSerDcs"];
        for (int i=0; i<arrLeadExtSerDcs.count; i++)
        {
            NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
            if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
            {
                
            }
            else
            {
                NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
            }
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:arrOfLeadToSaveAnyHow forKey:@"saveTolocalDbAnyHow"];
        [defs synchronize];
        
        
        if (arrOfLeadToSaveAnyHow.count>0) {
            
            [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfLeadToSaveAnyHow];
            
        }
        
        [self saveToCoreDataSalesInfo: dictForAppoint : @"NilHai"];
        
        
    } else {
        
        
        NSArray *arrLeadExtSerDcs=[dictForAppoint valueForKey:@"LeadExtSerDcs"];
        for (int i=0; i<arrLeadExtSerDcs.count; i++)
        {
            NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
            if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
            {
                
            }
            else
            {
                NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                
                NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                
                [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
                
                if (!(arrAllObjModifyDate.count==0)) {
                    
                    for (int k=0; k<arrAllObjModifyDate.count; k++) {
                        
                        matchesModifyDate=arrAllObjModifyDate[k];
                        
                        NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                        NSString *strModifyLeadModifyDate=[matchesModifyDate valueForKey:@"modifyDate"];
                        
                        [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                        
                        if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                            
                            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
                            NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                            NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                            
                            NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                            [inputDateFormatter setTimeZone:inputTimeZone];
                            [inputDateFormatter setDateFormat:dateFormat];
                            
                            NSString *inputString = strModifyDateCompare;
                            NSDate *date = [inputDateFormatter dateFromString:inputString];
                            
                            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                            [outputDateFormatter setTimeZone:outputTimeZone];
                            [outputDateFormatter setDateFormat:dateFormat];
                            NSString *outputString = [outputDateFormatter stringFromDate:date];
                            
                            ServerModifyDate = [dateFormatter dateFromString:outputString];
                            
                            NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                            
                            if(result == NSOrderedDescending)
                            {
                                NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                                //[arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                                //change for is sync only on 27 july 2017
                                
                                BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                
                            }
                            else if(result == NSOrderedAscending)
                            {
                                NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                                BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                            }
                            else
                            {
                                NSLog(@"Equal date");//
                                BOOL isSync=[self isSyncedLeadsSales:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                
                            }
                        }else{
                            [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                        }
                    }
                    
                    [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
                }
            }
        }
        
        
        //Change if No Lead from web
        
        if (arrLeadExtSerDcs.count==0) {
            
            if (!(arrAllObjModifyDate.count==0)) {
                
                for (int k=0; k<arrAllObjModifyDate.count; k++) {
                    
                    matchesModifyDate=arrAllObjModifyDate[k];
                    
                    NSString *strModifyLeadIdd=[matchesModifyDate valueForKey:@"leadIdd"];
                    
                    [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                }
            }
        }
        
        NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
        NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
        
        NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
        [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
        
        [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
        
        for (int kk=0; kk<arrOfWOModify.count; kk++) {
            
            BOOL isSync=[self isSyncedLeadsSales:arrOfWOModify[kk]];
            if (isSync) {
                
                [arrOfLeadToSend addObject:arrOfWOModify[kk]];
                
            }
        }
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
        NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
        
        //Change To Delete void and tech Changed Wali
        
        NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
        for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
        {
            if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
            {
                [arrayListToSendWorkOrdernDelete addObject:speakerID];
            }
        }
        
        NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
        
        [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
        
        [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        
        arrOfWorkOrderIdinModifyDateunique=nil;
        
        arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
        
        for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
            
            BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
            if (isSync) {
                
                [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
                
            }
            
        }
        
        NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
        arrOfLeadToSend=[[NSMutableArray alloc]init];
        NSArray *aRRTemps=[orderedSetNew array];
        [arrOfLeadToSend addObjectsFromArray:aRRTemps];
        
        NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
        [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
        [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        arrOfLeadToSaveInDbandDelete=nil;
        arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
        
        
        NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
        [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        arrOfLeadsToSaveeAnyHow=nil;
        arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:arrOfLeadToSend forKey:@"sendSalesLeadToServer"];
        [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveSalesLeadToLocalDbnDelete"];
        [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHow"];
        [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBSales"];
        [defs synchronize];
        
        if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
            
            [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
            [arrOfSalesLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
            
        }
        
        // NSString *atrrr=[NSString stringWithFormat:@"ArrLeadToSend--%@",arrOfLeadToSend];
        
        //  NSString *atrrr11=[NSString stringWithFormat:@"saveSalesLeadToLocalDbnDelete--%@------saveTolocalDbAnyHow-----%@",arrOfLeadToSaveInDbandDelete,arrOfLeadsToSaveeAnyHow];
        
        // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:atrrr message:atrrr11 delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        // [alert show];
        
        [self saveToCoreDataSalesInfo : dictForAppoint : @""];
        
    }
}


-(void)checkifToSendServiceAppointmentToServer{
    
    NSMutableArray *arrOfWorkOrderIdComingFromServer=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdinModifyDate=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfWorkOrderIdToFetchServiceDynamicForm=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfAlreadySavedLeadss=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityServiceModifyDate=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityServiceModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName = %@",strUsername];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
            
            matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
            
            [arrOfAlreadySavedLeadss addObject:[matchesServiceModifyDate valueForKey:@"workorderId"]];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    NSMutableArray *arrOfLeadToSend=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
    NSMutableArray *arrOfLeadToSaveAnyHow=[[NSMutableArray alloc]init];
    
    if (arrAllObjServiceModifyDate.count==0) {
        NSArray *arrLeadExtSerDcs=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];
        for (int i=0; i<arrLeadExtSerDcs.count; i++)
        {
            NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
            if(![[dictLeadDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
            {
                
            }
            else
            {
                NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
                [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
            }
        }
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:arrOfLeadToSaveAnyHow forKey:@"saveTolocalDbAnyHowService"];
        [defs synchronize];
        
        if (arrOfLeadToSaveAnyHow.count>0) {
            
            [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadToSaveAnyHow];
            
        }
        
        [self saveToCoreDataServiceAutomation: dictForWorkOrderAppointment : @"NilHai"];
        
        
    } else {
        
        
        NSArray *arrLeadExtSerDcs=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];
        for (int i=0; i<arrLeadExtSerDcs.count; i++)
        {
            NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
            if(![[dictLeadDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
            {
                
            }
            else
            {
                
                NSString *strLeadIdCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
                
                [arrOfWorkOrderIdComingFromServer addObject:strLeadIdCompare];
                
                if ([strLeadIdCompare isEqualToString:@"754"]) {
                    
                    NSLog(@"strLeadIdCompare======%@",strLeadIdCompare);
                    
                }
                
                NSString *strModifyDateCompare=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"WorkorderDetail.ModifiedFormatedDate"]];
                
                if (!(arrAllObjServiceModifyDate.count==0)) {
                    
                    for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                        
                        matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                        
                        NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"WorkorderId"];
                        NSString *strModifyLeadModifyDate=[matchesServiceModifyDate valueForKey:@"modifyDate"];
                        
                        [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                        
                        if ([strLeadIdCompare isEqualToString:strModifyLeadIdd]) {
                            
                            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                            NSDate* localDbModifyDate = [dateFormatter dateFromString:strModifyLeadModifyDate];
                            NSDate* ServerModifyDate = [dateFormatter dateFromString:strModifyDateCompare];
                            //                            NSDate *add90Min = [ServerModifyDate dateByAddingTimeInterval:(330*60)];
                            //                            ServerModifyDate = add90Min;
                            
                            NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                            [inputDateFormatter setTimeZone:inputTimeZone];
                            [inputDateFormatter setDateFormat:dateFormat];
                            
                            NSString *inputString = strModifyDateCompare;
                            NSDate *date = [inputDateFormatter dateFromString:inputString];
                            
                            NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                            [outputDateFormatter setTimeZone:outputTimeZone];
                            [outputDateFormatter setDateFormat:dateFormat];
                            NSString *outputString = [outputDateFormatter stringFromDate:date];
                            
                            ServerModifyDate = [dateFormatter dateFromString:outputString];
                            
                            NSComparisonResult result = [localDbModifyDate compare:ServerModifyDate];
                            
                            if(result == NSOrderedDescending)
                            {
                                NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                                //[arrOfLeadToSend addObject:strModifyLeadIdd];
                                
                                //change for is sync only on 27 july 2017
                                
                                BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                
                            }
                            else if(result == NSOrderedAscending)
                            {
                                NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                                BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                
                                [arrOfWorkOrderIdToFetchServiceDynamicForm addObject:strModifyLeadIdd];
                            }
                            else
                            {
                                NSLog(@"Equal date");//
                                BOOL isSync=[self isSyncedWO:strModifyLeadIdd];
                                if (isSync) {
                                    
                                    [arrOfLeadToSend addObject:strModifyLeadIdd];
                                    
                                }else{
                                    [arrOfLeadToSaveInDbandDelete addObject:strModifyLeadIdd];
                                }
                                
                            }
                        }else{
                            [arrOfLeadToSaveAnyHow addObject:strLeadIdCompare];
                        }
                    }
                    
                    [arrOfLeadToSaveAnyHow removeObjectsInArray:arrOfAlreadySavedLeadss];
                }
            }
        }
        
        
        //Change if No workorder from web
        
        if (arrLeadExtSerDcs.count==0) {
            
            if (!(arrAllObjServiceModifyDate.count==0)) {
                
                for (int k=0; k<arrAllObjServiceModifyDate.count; k++) {
                    
                    matchesServiceModifyDate=arrAllObjServiceModifyDate[k];
                    
                    NSString *strModifyLeadIdd=[matchesServiceModifyDate valueForKey:@"WorkorderId"];
                    
                    [arrOfWorkOrderIdinModifyDate addObject:strModifyLeadIdd];
                }
            }
        }
        
        NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:arrOfWorkOrderIdinModifyDate];
        NSArray *arrOfWorkOrderIdinModifyDateunique = [orderedSet1 array];
        
        NSMutableArray *arrOfWOModify=[[NSMutableArray alloc]init];
        [arrOfWOModify addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
        
        [arrOfWOModify removeObjectsInArray:arrOfWorkOrderIdComingFromServer];
        
        for (int kk=0; kk<arrOfWOModify.count; kk++) {
            
            BOOL isSync=[self isSyncedWO:arrOfWOModify[kk]];
            if (isSync) {
                
                [arrOfLeadToSend addObject:arrOfWOModify[kk]];
                
            }
        }
        
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:arrOfLeadToSaveAnyHow];
        NSArray *arrOfLeadsToSaveeAnyHow = [orderedSet array];
        
        
        //Change To Delete void and tech Changed Wali
        
        
        NSMutableArray *arrayListToSendWorkOrdernDelete = [[NSMutableArray alloc] init];
        for(NSString *speakerID in arrOfWorkOrderIdinModifyDateunique)
        {
            if(![arrOfWorkOrderIdComingFromServer containsObject:speakerID])
            {
                [arrayListToSendWorkOrdernDelete addObject:speakerID];
            }
        }
        
        NSMutableArray *arrMutableTemp=[[NSMutableArray alloc]init];
        
        [arrMutableTemp addObjectsFromArray:arrOfWorkOrderIdinModifyDateunique];
        
        [arrMutableTemp removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        
        arrOfWorkOrderIdinModifyDateunique=nil;
        
        arrOfWorkOrderIdinModifyDateunique=arrMutableTemp;
        
        for (int kk=0; kk<arrOfWorkOrderIdinModifyDateunique.count; kk++) {
            
            BOOL isSync=[self isSyncedWO:arrOfWorkOrderIdinModifyDateunique[kk]];
            if (isSync) {
                
                [arrOfLeadToSend addObject:arrOfWorkOrderIdinModifyDateunique[kk]];
                
            }
        }
        
        NSOrderedSet *orderedSetNew = [NSOrderedSet orderedSetWithArray:arrOfLeadToSend];
        arrOfLeadToSend=[[NSMutableArray alloc]init];
        NSArray *aRRTemps=[orderedSetNew array];
        [arrOfLeadToSend addObjectsFromArray:aRRTemps];
        
        NSMutableArray *TemparrOfLeadToSaveInDbandDelete=[[NSMutableArray alloc]init];
        [TemparrOfLeadToSaveInDbandDelete addObjectsFromArray:arrOfLeadToSaveInDbandDelete];
        [TemparrOfLeadToSaveInDbandDelete removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        arrOfLeadToSaveInDbandDelete=nil;
        arrOfLeadToSaveInDbandDelete=TemparrOfLeadToSaveInDbandDelete;
        
        
        NSMutableArray *TemparrOfLeadsToSaveeAnyHow=[[NSMutableArray alloc]init];
        [TemparrOfLeadsToSaveeAnyHow addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
        [TemparrOfLeadsToSaveeAnyHow removeObjectsInArray:arrayListToSendWorkOrdernDelete];
        arrOfLeadsToSaveeAnyHow=nil;
        arrOfLeadsToSaveeAnyHow=TemparrOfLeadsToSaveeAnyHow;
        
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setObject:arrOfLeadToSend forKey:@"sendServiceLeadToServer"];
        [defs setObject:arrOfLeadToSaveInDbandDelete forKey:@"saveServiceLeadToLocalDbnDelete"];
        [defs setObject:arrOfLeadsToSaveeAnyHow forKey:@"saveTolocalDbAnyHowService"];
        [defs setObject:arrayListToSendWorkOrdernDelete forKey:@"DeleteExtraWorkOrderFromDBService"];
        [defs synchronize];
        
        if (arrOfLeadsToSaveeAnyHow.count>0 || arrOfWorkOrderIdToFetchServiceDynamicForm.count>0) {
            
            [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfLeadsToSaveeAnyHow];
            [arrOfServiceLeadToFetchDynamicData addObjectsFromArray:arrOfWorkOrderIdToFetchServiceDynamicForm];
            
        }
        
        [self saveToCoreDataServiceAutomation : dictForWorkOrderAppointment : @""];
    }
}

//============================================================================
//============================================================================

#pragma mark- ---------------------Sales Info Core Data Methods-----------------

//============================================================================
//============================================================================

//============================================================================
//============================================================================

#pragma mark- CoreDataSave

//============================================================================
//============================================================================

#pragma mark -SalesAuto
-(void)saveToCoreDataSalesInfo :(NSDictionary*)arrOfSalesInfoToSave :(NSString*)saveAll
{
    NSMutableArray *arrLeadIdToSaveAlll=[[NSMutableArray alloc]init];
    NSString *strLeadIdToCompare;
    
    if ([saveAll isEqualToString:@"NilHai"]) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSMutableArray *arrLeadIdToSaveAnyHow=[defs objectForKey:@"saveTolocalDbAnyHow"];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSaveAnyHow];
        
    } else {
        
        //==================================================================================
        //==================================================================================
        [self deleteFromCoreDataSalesInfo];
        
        //==================================================================================
        //==================================================================================
        
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSMutableArray *arrLeadIdToSave=[defs objectForKey:@"saveSalesLeadToLocalDbnDelete"];
        NSMutableArray *arrLeadIdToSaveAnyHow=[defs objectForKey:@"saveTolocalDbAnyHow"];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSave];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSaveAnyHow];
        
    }
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    for (int k=0; k<arrLeadIdToSaveAlll.count; k++) {
        
        strLeadIdToCompare=[NSString stringWithFormat:@"%@",arrLeadIdToSaveAlll[k]];
        
#pragma mark- 25 Aug
        
        NSArray *arrLeadExtSerDcs=[dictForAppoint valueForKey:@"LeadExtSerDcs"];
        
        for (int i=0; i<arrLeadExtSerDcs.count; i++)
        {
            
            //**** For Lead Detail *****//
            NSDictionary *dictLeadDetail=[arrLeadExtSerDcs objectAtIndex:i];
            
            NSString *strLeadIddd;
            if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
            {
                strLeadIddd=@"";
            }
            else
            {
                
                strLeadIddd=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                
            }
            
            if ([strLeadIddd isEqualToString:strLeadIdToCompare]) {
                
                if(![[dictLeadDetail valueForKey:@"LeadDetail"] isKindOfClass:[NSDictionary class]])
                {
                    NSLog(@"NOTHING IN LEAD DETAIL");
                }
                else
                {
                    
                    // Lead Detail Entity
                    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
                    LeadDetail *objLeadDetail = [[LeadDetail alloc]initWithEntity:entityLeadDetail insertIntoManagedObjectContext:context];
                    
                    
                    NSLog(@"Saved In Database Lead");
                    
                    objLeadDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objLeadDetail.leadNumber=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadNumber"]];
                    objLeadDetail.companyId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyId"]];
                    objLeadDetail.companyMappingKey=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyMappingKey"]];
                    objLeadDetail.accountNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountNo"]];
                    objLeadDetail.leadName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadName"]];
                    objLeadDetail.firstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FirstName"]];
                    objLeadDetail.middleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.MiddleName"]];
                    
                    objLeadDetail.lastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LastName"]];
                    
                    objLeadDetail.companyName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CompanyName"]];
                    objLeadDetail.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryEmail"]]];
                    objLeadDetail.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.SecondaryEmail"]]];
                    objLeadDetail.primaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryPhone"]];
                    objLeadDetail.secondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SecondaryPhone"]];
                    objLeadDetail.salesRepId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SalesRepId"]];
                    objLeadDetail.scheduleStartDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]];
                    objLeadDetail.scheduleEndDate=[NSString stringWithFormat:@"%@",[dictLeadDetail   valueForKeyPath:@"LeadDetail.ScheduleEndDate"]];
                    objLeadDetail.inspectionDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InspectionDate"]];
                    objLeadDetail.estimatedValue=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.EstimatedValue"]];
                    objLeadDetail.closeDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CloseDate"]];
                    objLeadDetail.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrimaryServiceSysName"]];
                    objLeadDetail.stageSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.StageSysName"]];
                    objLeadDetail.statusSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.StatusSysName"]];
                    // objLeadDetail.serviceGroupSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceGroupSysName"]];   No Need
                    
                    objLeadDetail.prioritySysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PrioritySysName"]];
                    objLeadDetail.sourceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SourceSysName"]];
                    objLeadDetail.tags=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Tags"]];
                    objLeadDetail.descriptionLeadDetail=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Description"]];
                    objLeadDetail.notes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Notes"]];
                    objLeadDetail.isInspectionCompleted=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInspectionCompleted"]];
                    objLeadDetail.inspectionCompletedTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InspectionCompletedTime"]];
                    objLeadDetail.branchSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BranchSysName"]];
                    objLeadDetail.divisionSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DivisionSysName"]];
                    objLeadDetail.departmentSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DepartmentSysName"]];
                    objLeadDetail.billingAddress1=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingAddress1"]];
                    objLeadDetail.billingAddress2=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingAddress2"]];
                    objLeadDetail.billingCountry=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCountry"]];
                    objLeadDetail.billingState=[NSString stringWithFormat:@"%@",    [dictLeadDetail valueForKeyPath:@"LeadDetail.BillingState"]];
                    objLeadDetail.billingCity=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCity"]];
                    objLeadDetail.billingZipcode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingZipcode"]];
                    objLeadDetail.servicesAddress1=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicesAddress1"]];
                    objLeadDetail.serviceAddress2=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddress2"]];
                    objLeadDetail.serviceCountry=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCountry"]];
                    objLeadDetail.serviceState=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceState"]];
                    objLeadDetail.serviceCity=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCity"]];
                    objLeadDetail.serviceZipcode=[NSString stringWithFormat:@"%@",[dictLeadDetail    valueForKeyPath:@"LeadDetail.ServiceZipcode"]];
                    objLeadDetail.isAgreementGenerated=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementGenerated"]];
                    objLeadDetail.collectedAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CollectedAmount"]];
                    
                    objLeadDetail.billedAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BilledAmount"]];
                    objLeadDetail.subTotalAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SubTotalAmount"]];
                    objLeadDetail.couponDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CouponDiscount"]];
                    objLeadDetail.latitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Latitude"]];
                    objLeadDetail.longitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Longitude"]];
                    objLeadDetail.customerName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CustomerName"]];
                    objLeadDetail.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CreatedBy"]];
                    objLeadDetail.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CreatedDate"]];
                    objLeadDetail.surveyID=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SurveyID"]];
                    
                    objLeadDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedBy"]];
                    objLeadDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                    
                    NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
                    [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]]];
                    objLeadDetail.zdateScheduledStart=newTimeSchedule;
                    
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]]];
                    objLeadDetail.dateModified=newTime;
                    
                    
                    //Nilind new add 30 sept
                    objLeadDetail.tax=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Tax"]];
                    
                    objLeadDetail.leadGeneralTermsConditions=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadGeneralTermsConditions"]];
                    
                    //Nilind 7 Nov
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsProposalFromMobile"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isProposalFromMobile=@"true";
                    }
                    else
                    {
                        objLeadDetail.isProposalFromMobile=@"false";
                    }
                    
                    
                    //Nilind 29 Dec //isInitialSetupCreated
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isInitialSetupCreated=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isInitialSetupCreated=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInitialSetupCreated"]] isEqualToString:@""])
                    {
                        objLeadDetail.isInitialSetupCreated=@"false";
                    }
                    else
                    {
                        objLeadDetail.isInitialSetupCreated=@"false";
                    }
                    
                    objLeadDetail.audioFilePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AudioFilePath"]];
                    objLeadDetail.deviceVersion=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DeviceVersion"]];
                    objLeadDetail.deviceName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DeviceName"]];
                    objLeadDetail.versionNumber=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.VersionNumber"]];
                    objLeadDetail.versionDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.VersionDate"]];
                    
                    
                    //Nilind 5 Jan
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.iAgreeTerms=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.iAgreeTerms=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IAgreeTerms"]] isEqualToString:@""])
                    {
                        objLeadDetail.iAgreeTerms=@"false";
                    }
                    else
                    {
                        objLeadDetail.iAgreeTerms=@"false";
                    }
                    //.........................
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"1"]||[[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"true"])
                    {
                        objLeadDetail.isCustomerNotPresent=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isCustomerNotPresent=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCustomerNotPresent"]] isEqualToString:@""])
                    {
                        objLeadDetail.isCustomerNotPresent=@"false";
                    }
                    else
                    {
                        objLeadDetail.isCustomerNotPresent=@"false";
                    }
                    //.....................
                    
                    //.........................
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isBillingAddressSame=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isBillingAddressSame=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsBillingAddressSame"]] isEqualToString:@""])
                    {
                        objLeadDetail.isBillingAddressSame=@"false";
                    }
                    else
                    {
                        objLeadDetail.isBillingAddressSame=@"false";
                    }
                    //.....................
                    
                    //Nilind 16 Nov
                    
                    //.........................
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isAgreementSigned=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isAgreementSigned=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@""])
                    {
                        objLeadDetail.isAgreementSigned=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"true"])
                    {
                        objLeadDetail.isAgreementSigned=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsAgreementSigned"]] isEqualToString:@"false"])
                    {
                        objLeadDetail.isAgreementSigned=@"false";
                    }
                    else
                    {
                        objLeadDetail.isAgreementSigned=@"false";
                    }
                    //.....................
                    
                    objLeadDetail.userName=strUserName;
                    objLeadDetail.companyKey=strCompanyKey;
                    objLeadDetail.employeeNo_Mobile=strEmployeeNo;
                    
                    //........................
                    
                    objLeadDetail.zSync=@"blank";
                    
                    //Nilind 08 Feb
                    
                    objLeadDetail.area=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.Area"]];
                    objLeadDetail.lotSizeSqFt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.lotSizeSqFt"]];
                    objLeadDetail.noOfBedroom=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoofBedroom"]];
                    objLeadDetail.noOfBathroom=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoofBathroom"]];
                    objLeadDetail.yearBuilt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.yearBuilt"]];
                    //End
                    
                    //.........................
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isEmployeePresetSignature=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isEmployeePresetSignature=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@""])
                    {
                        objLeadDetail.isEmployeePresetSignature=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"true"])
                    {
                        objLeadDetail.isEmployeePresetSignature=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsEmployeePresetSignature"]] isEqualToString:@"false"])
                    {
                        objLeadDetail.isEmployeePresetSignature=@"false";
                    }
                    else
                    {
                        objLeadDetail.isEmployeePresetSignature=@"false";
                    }
                    //.....................
                    
                    //Nilind 23 May
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@""])
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"true"])
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsServiceAddrTaxExempt"]] isEqualToString:@"false"])
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"false";
                    }
                    else
                    {
                        objLeadDetail.isServiceAddrTaxExempt=@"false";
                    }
                    objLeadDetail.serviceAddrTaxExemptionNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddrTaxExemptionNo"]];
                    objLeadDetail.serviceAddressSubType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddressSubType"]];
                    objLeadDetail.taxableAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxableAmount"]];
                    objLeadDetail.taxAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxAmount"]];
                    objLeadDetail.totalPrice=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TotalPrice"]];
                    
                    objLeadDetail.serviceGateCode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceGateCode"]];
                    objLeadDetail.serviceNotes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceNotes"]];
                    objLeadDetail.serviceDirection=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceDirection"]];
                    
                    objLeadDetail.billingMapcode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingMapcode"]];
                    objLeadDetail.serviceMapCode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceMapCode"]];
                    objLeadDetail.accountDescription=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountDescription"]];
                    
                    objLeadDetail.linearSqFt=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.linearSqFt"]];
                    
                    objLeadDetail.surveyStatus=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SurveyStatus"]];
                    
                    //End
                    
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isCouponApplied=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isCouponApplied=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@""])
                    {
                        objLeadDetail.isCouponApplied=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"true"])
                    {
                        objLeadDetail.isCouponApplied=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCouponApplied"]] isEqualToString:@"false"])
                    {
                        objLeadDetail.isCouponApplied=@"false";
                    }
                    else
                    {
                        objLeadDetail.isCouponApplied=@"false";
                    }
                    //Nilind 26 Oct
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isResendAgreementProposalMail=@"true";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@"0"])
                    {
                        objLeadDetail.isResendAgreementProposalMail=@"false";
                    }
                    else if ([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsResendAgreementProposalMail"]] isEqualToString:@""])
                    {
                        objLeadDetail.isResendAgreementProposalMail=@"false";
                    }
                    else
                    {
                        objLeadDetail.isResendAgreementProposalMail=@"false";
                    }
                    //.........................
                    
                    objLeadDetail.cellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.CellNo"]];
                    
                    objLeadDetail.billingFirstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingFirstName"]];
                    
                    
                    
                    objLeadDetail.billingMiddleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingMiddleName"]];
                    
                    objLeadDetail.billingLastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingLastName"]];
                    
                    objLeadDetail.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingPrimaryEmail"]]];
                    
                    objLeadDetail.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSecondaryEmail"]]];
                    
                    
                    
                    
                    
                    objLeadDetail.billingCellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCellNo"]];
                    
                    objLeadDetail.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingPrimaryPhone"]];
                    
                    objLeadDetail.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSecondaryPhone"]];
                    
                    
                    
                    objLeadDetail.serviceFirstName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceFirstName"]];
                    
                    objLeadDetail.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceMiddleName"]];
                    
                    objLeadDetail.serviceLastName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceLastName"]];
                    
                    objLeadDetail.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicePrimaryEmail"]]];
                    
                    objLeadDetail.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSecondaryEmail"]]];
                    
                    objLeadDetail.serviceCellNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCellNo"]];
                    
                    objLeadDetail.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServicePrimaryPhone"]];
                    
                    objLeadDetail.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSecondaryPhone"]];
                    //Nilind 08 Dec
                    
                    objLeadDetail.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTime"]];
                    objLeadDetail.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTimeLatitude"]];
                    objLeadDetail.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OnMyWaySentSMSTimeLongitude"]];
                    objLeadDetail.strPreferredMonth=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.strPreferredMonth"]];
                    objLeadDetail.salesType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.SalesType"]];
                    objLeadDetail.problemDescription=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProblemDescription"]];
                    objLeadDetail.taxableMaintAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxableMaintAmount"]];
                    objLeadDetail.taxMaintAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxMaintAmount"]];
                    //end
                    //akshay
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountElectronicAuthorizationFormSigned"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.accountElectronicAuthorizationFormSigned = @"true";
                    }
                    else
                    {
                        objLeadDetail.accountElectronicAuthorizationFormSigned = @"false";
                    }
                    objLeadDetail.billingCounty=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingCounty"]];
                    objLeadDetail.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BillingSchoolDistrict"]];
                    objLeadDetail.serviceCounty=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceCounty"]];
                    objLeadDetail.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceSchoolDistrict"]];

                    objLeadDetail.noOfStory=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NoOfStory"]];
                    objLeadDetail.shrubArea=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.shrubArea"]];
                    objLeadDetail.turfArea=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.turfArea"]];
                    objLeadDetail.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ThirdPartyAccountNo"]];
                    //Commercial FLow
                    objLeadDetail.flowType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FlowType"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FlowType"]] isEqualToString:@""] || [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.FlowType"]].length == 0)
                    {
                        objLeadDetail.flowType = @"Residential";
                    }
                    
                    objLeadDetail.initialServiceDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.InitialServiceDate"]];
                    
                    objLeadDetail.recurringServiceMonth=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.RecurringServiceMonth"]];

                    objLeadDetail.taxSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TaxSysName"]];

                    objLeadDetail.totalMaintPrice=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.totalMaintPrice"]];
                    objLeadDetail.proposalNotes=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProposalNotes"]];
                    objLeadDetail.driveTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.DriveTime"]];
                    objLeadDetail.latestStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LatestStartTime"]];
                    objLeadDetail.earliestStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.EarliestStartTime"]];

                    objLeadDetail.fromDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleStartDate"]];
                    objLeadDetail.toDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ScheduleEndDate"]];
                    objLeadDetail.modifyDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                    
                    objLeadDetail.accountManagerId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerId"]];
                    objLeadDetail.accountManagerNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerNo"]];
                    objLeadDetail.accountManagerName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerName"]];
                    objLeadDetail.accountManagerEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerEmail"]]];
                    objLeadDetail.accountManagerPrimaryPhone=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.AccountManagerPrimaryPhone"]];
                    
                    objLeadDetail.propertyType=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.PropertyType"]];
                    
                objLeadDetail.serviceAddressPropertyTypeSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ServiceAddressPropertyTypeSysName"]];

                    objLeadDetail.profileImage=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ProfileImage"]];

                    objLeadDetail.isSelectionAllowedForCustomer=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsSelectionAllowedForCustomer"]];
                    
                    objLeadDetail.tipDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.TipDiscount"]];

                    objLeadDetail.otherDiscount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.OtherDiscount"]];

                    objLeadDetail.isCommLeadUpdated=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCommLeadUpdated"]];
                    
                    //Nilind
                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsMailSend"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isMailSend = @"true";
                    }
                    else
                    {
                        objLeadDetail.isMailSend = @"false";
                    }

                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsInvisible"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isInvisible = @"true";
                    }
                    else
                    {
                        objLeadDetail.isInvisible = @"false";
                    }
                    
                    objLeadDetail.leadInspectionFee=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadInspectionFee"]];

                    if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsTipEligible"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isTipEligible = @"true";
                    }
                    else
                    {
                        objLeadDetail.isTipEligible = @"false";
                    }
                    
                    objLeadDetail.needTipService=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.NeedTipService"]];

                     if([[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.IsCommTaxUpdated"]] isEqualToString:@"1"])
                    {
                        objLeadDetail.isCommTaxUpdated = @"true";
                    }
                    else
                    {
                        objLeadDetail.isCommTaxUpdated = @"false";
                    }
                    
                    objLeadDetail.buildingPermitAmount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.BuildingPermitAmount"]];

                }
                
                
                //------------------------------------------------------------------------
                //------------------------------------------------------------------------
                
                //For Saving modify Date-------------
                
                
                entitySalesModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
                SalesAutoModifyDate *objSalesModifyDate = [[SalesAutoModifyDate alloc]initWithEntity:entitySalesModifyDate insertIntoManagedObjectContext:context];
                
                objSalesModifyDate.companyKey=strCompanyKeyy;
                objSalesModifyDate.userName=strUserName;
                objSalesModifyDate.leadIdd=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                
                NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                objSalesModifyDate.modifyDate=outputString;//[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.ModifiedDate"]];
                
                //------------------------------------------------------------------------
                //------------------------------------------------------------------------
                
                
                //........................................................................
                
                // For Payment Info
                //PaymentInfo Entity
                entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
                PaymentInfo *objentityPaymentInfo = [[PaymentInfo alloc]initWithEntity:entityPaymentInfo insertIntoManagedObjectContext:context];
                NSLog(@"PaymentInfo>>>>%@",[dictLeadDetail valueForKey:@"PaymentInfo"]);
                
                if(![[dictLeadDetail valueForKey:@"PaymentInfo"] isKindOfClass:[NSDictionary class]])
                {
                    NSLog(@"NOTHING IN PAYMENT INFO");
                }
                else
                {
                    objentityPaymentInfo.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objentityPaymentInfo.leadPaymentDetailId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.LeadPaymentDetailId"]];
                    objentityPaymentInfo.paymentMode=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.PaymentMode"]];
                    objentityPaymentInfo.amount=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Amount"]];
                    objentityPaymentInfo.checkNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckNo"]];
                    objentityPaymentInfo.licenseNo=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.LicenseNo"]];
                    
                    objentityPaymentInfo.expirationDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ExpirationDate"]];
                    objentityPaymentInfo.specialInstructions=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.SpecialInstructions"]];
                    objentityPaymentInfo.agreement=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Agreement"]];
                    objentityPaymentInfo.proposal=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Proposal"]];
                    objentityPaymentInfo.customerSignature=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CustomerSignature"]];
                    
                    objentityPaymentInfo.salesSignature=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.SalesSignature"]];
                    objentityPaymentInfo.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CreatedBy"]];
                    objentityPaymentInfo.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CreatedDate"]];
                    objentityPaymentInfo.modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ModifiedBy"]];
                    objentityPaymentInfo.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.ModifiedDate"]];
                    objentityPaymentInfo.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckFrontImagePath"]];
                    objentityPaymentInfo.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.CheckBackImagePath"]];
                    
                    objentityPaymentInfo.inspection=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"PaymentInfo.Inspection"]];

                    //Nilind 16 Nov
                    
                    objentityPaymentInfo.userName=strUserName;
                    objentityPaymentInfo.companyKey=strCompanyKey;
                    
                    //........................
                }
                //........................................................................
                
                // For LeadPreference
                
                //LeadPreference Entiy
                entityLeadPreference=[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
                
                LeadPreference *objentityLeadPreference = [[LeadPreference alloc]initWithEntity:entityLeadPreference insertIntoManagedObjectContext:context];
                if(![[dictLeadDetail valueForKey:@"LeadPreference"] isKindOfClass:[NSDictionary class]])
                {
                    NSLog(@"NOTHING IN LeadPreference");
                }
                else
                {
                    objentityLeadPreference.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentityLeadPreference.createdBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.CreatedBy"]];
                    objentityLeadPreference.createdDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.CreatedDate"]];
                    
                    objentityLeadPreference.leadPreferenceId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.LeadPreferenceId"]];
                    
                    objentityLeadPreference. modifiedBy=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.ModifiedBy"]];
                    objentityLeadPreference.modifiedDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.ModifiedDate"]];
                    objentityLeadPreference.preferredDays=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredDays"]];
                    objentityLeadPreference.preferredEndTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredEndTime"]];
                    objentityLeadPreference.preferredStartDate=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredStartDate"]];
                    objentityLeadPreference.preferredStartTime=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredStartTime"]];
                    objentityLeadPreference.preferredTechnician=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadPreference.PreferredTechnician"]];
                    
                    //Nilind 16 Nov
                    
                    objentityLeadPreference.userName=strUserName;
                    objentityLeadPreference.companyKey=strCompanyKey;
                    
                    //........................
                    
                }
                
                //........................................................................
                
                
                // For DocumentsDetail
                
                NSArray *arrDocumentsDetail=[dictLeadDetail valueForKey:@"DocumentsDetail"];
                for (int z=0; z<arrDocumentsDetail.count; z++)
                {
                    entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
                    
                    DocumentsDetail *objentityDocumentsDetail = [[DocumentsDetail alloc]initWithEntity:entityDocumentsDetail insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictDocumentsDetail=[[NSMutableDictionary alloc]init];
                    dictDocumentsDetail=[arrDocumentsDetail objectAtIndex:z];
                    
                    //if(![[dictLeadDetail valueForKey:@"DocumentsDetail"] isKindOfClass:[NSDictionary class]])
                    // {
                    // NSLog(@"NOTHING IN DocumentsDetail");
                    // }
                    //else
                    //{
                    objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadId"]];
                    
                    objentityDocumentsDetail.accountNo=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"AccountNo"]];
                    
                    objentityDocumentsDetail.createdBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedBy"]];
                    
                    objentityDocumentsDetail.createdDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"CreatedDate"]];
                    
                    objentityDocumentsDetail.descriptionDocument=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Description"]];
                    
                    objentityDocumentsDetail. docType=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"DocType"]];
                    
                    objentityDocumentsDetail.fileName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"FileName"]];
                    
                    objentityDocumentsDetail.leadDocumentId=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"LeadDocumentId"]];
                    
                    objentityDocumentsDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedBy"]];
                    
                    objentityDocumentsDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ModifiedDate"]];
                    
                    objentityDocumentsDetail.scheduleStartDate=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"ScheduleStartDate"]];
                    
                    objentityDocumentsDetail.title=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"Title"]];
                    objentityDocumentsDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"OtherDocSysName"]];
                    objentityDocumentsDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentityDocumentsDetail.docFormatType =  [NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"DocFormatType"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"IsAddendum"]] isEqualToString:@"1"])
                    {
                        objentityDocumentsDetail.isAddendum =  @"true";

                    }
                    else
                    {
                        objentityDocumentsDetail.isAddendum =  @"false";

                        
                    }
                    
                    if([[NSString stringWithFormat:@"%@",[dictDocumentsDetail valueForKey:@"IsSync"]] isEqualToString:@"1"])

                    {

                    objentityDocumentsDetail.isSync =  @"true";
                        
                    }
                    else
                    {
                        objentityDocumentsDetail.isSync =  @"false";

                    }
                    objentityDocumentsDetail.isToUpload = @"false";
                    //Nilind 16 Nov
                    
                    objentityDocumentsDetail.userName=strUserName;
                    objentityDocumentsDetail.companyKey=strCompanyKey;
                    
                    //........................
                    NSError *error10;
                    [context save:&error10];
                    //}
                }
                //........................................................................
                // For Email Detail /
                // 26 Aug 2016
                
                NSArray *arrEmailDetail=[dictLeadDetail valueForKey:@"EmailDetail"];//EmailDetail
                for (int j=0; j<arrEmailDetail.count; j++)
                {
                    //Email Detail Entity
                    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
                    
                    EmailDetail *objEmailDetail = [[EmailDetail alloc]initWithEntity:entityEmailDetail insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                    dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                    
                    objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                    objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                    objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                    //objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                    // objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                    objEmailDetail.leadMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"LeadMailId"]];
                   // objEmailDetail.isDefaultEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]];
                    if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"True"])
                    {
                        objEmailDetail.isCustomerEmail=@"true";

                    }
                    else
                    {
                        objEmailDetail.isCustomerEmail=@"false";

                    }
                    if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"True"])
                    {
                        objEmailDetail.isDefaultEmail=@"true";

                    }
                    else
                    {
                        objEmailDetail.isDefaultEmail=@"false";

                    }
                    
                    
                    
                    objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                    objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                    objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                    objEmailDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"])
                    {
                        objEmailDetail.isMailSent=@"true";
                    }
                    else
                    {
                        objEmailDetail.isMailSent=@"false";
                    }
                    //Nilind 16 Nov
                    
                    objEmailDetail.userName=strUserName;
                    objEmailDetail.companyKey=strCompanyKey;
                    
                    //........................
                    
                    //.................
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
                
                // For Image Detail //
                
                NSArray *arrImageDetail=[dictLeadDetail valueForKey:@"ImagesDetail"];
                for (int k=0; k<arrImageDetail.count; k++)
                {
                    // Image Detail Entity
                    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
                    
                    ImageDetail *objImageDetail = [[ImageDetail alloc]initWithEntity:entityImageDetail insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictImageDetail=[[NSMutableDictionary alloc]init];
                    dictImageDetail=[arrImageDetail objectAtIndex:k];
                    
                    objImageDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"CreatedBy"]];
                    objImageDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"CreatedDate"]];
                    objImageDetail.descriptionImageDetail=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Description"]];
                    objImageDetail.leadImageId=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageId"]];
                    objImageDetail.leadImagePath=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImagePath"]];
                    objImageDetail.leadImageType=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageType"]];
                    objImageDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"ModifiedBy"]];
                    objImageDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"ModifiedDate"]];
                    objImageDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    //Nilind 16 Nov
                    
                    objImageDetail.userName=strUserName;
                    objImageDetail.companyKey=strCompanyKey;
                    
                    objImageDetail.leadImageCaption=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadImageCaption"]];
                    
                    objImageDetail.latitude=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Latitude"]];
                    objImageDetail.longitude=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"Longitude"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsAddendum"]] isEqualToString:@"1"])
                    {
                        objImageDetail.isAddendum=@"true";
                    }
                    else
                    {
                        objImageDetail.isAddendum=@"false";
                    }
                    
                    if([[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"IsNewGraph"]] isEqualToString:@"1"])
                    {
                        objImageDetail.isNewGraph=@"true";
                    }
                    else
                    {
                        objImageDetail.isNewGraph=@"false";
                    }
                    objImageDetail.leadXmlPath=[NSString stringWithFormat:@"%@",[dictImageDetail valueForKey:@"LeadXmlPath"]];
                    
                    //........................
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
                
                // For SoldServiceNonStandardDetail //
                
                NSArray *arrSoldServiceNonStandardDetail=[dictLeadDetail valueForKey:@"SoldServiceNonStandardDetail"];
                for (int l=0; l<arrSoldServiceNonStandardDetail.count; l++)
                {
                    
                    //SoldServiceNonStandardDetail Entiy
                    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
                    
                    SoldServiceNonStandardDetail *objentitySoldServiceNonStandardDetail = [[SoldServiceNonStandardDetail alloc]initWithEntity:entitySoldServiceNonStandardDetail insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictSoldServiceNonStandardDetail=[[NSMutableDictionary alloc]init];
                    dictSoldServiceNonStandardDetail=[arrSoldServiceNonStandardDetail objectAtIndex:l];
                    objentitySoldServiceNonStandardDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objentitySoldServiceNonStandardDetail.createdBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedBy"]];
                    objentitySoldServiceNonStandardDetail.createdDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedDate"]];
                    objentitySoldServiceNonStandardDetail.departmentSysname=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DepartmentSysname"]];
                    objentitySoldServiceNonStandardDetail.discount=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Discount"]];
                    objentitySoldServiceNonStandardDetail.initialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InitialPrice"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]] isEqualToString:@"1"])
                    {
                        objentitySoldServiceNonStandardDetail.isSold=@"true";
                    }
                    else
                    {
                        objentitySoldServiceNonStandardDetail.isSold=@"false";
                    }
                    
                    //objentitySoldServiceNonStandardDetail.isSold=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]];
                    
                    
                    objentitySoldServiceNonStandardDetail.maintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MaintenancePrice"]];
                    objentitySoldServiceNonStandardDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedBy"]];
                    objentitySoldServiceNonStandardDetail. modifiedDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedDate"]];
                    objentitySoldServiceNonStandardDetail.modifiedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedInitialPrice"]];
                    objentitySoldServiceNonStandardDetail.modifiedMaintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedMaintenancePrice"]];
                    objentitySoldServiceNonStandardDetail.serviceDescription=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceDescription"]];
                    objentitySoldServiceNonStandardDetail.serviceFrequency=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceFrequency"]];
                    objentitySoldServiceNonStandardDetail.serviceName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceName"]];
                    objentitySoldServiceNonStandardDetail. soldServiceNonStandardId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"SoldServiceNonStandardId"]];
                    objentitySoldServiceNonStandardDetail.discountPercentage=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DiscountPercentage"]];
                    objentitySoldServiceNonStandardDetail.billingFrequencyPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencyPrice"]];
                    objentitySoldServiceNonStandardDetail.billingFrequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencySysName"]];
                    objentitySoldServiceNonStandardDetail.nonStdServiceTermsConditions=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"NonStdServiceTermsConditions"]];

                    objentitySoldServiceNonStandardDetail.frequencySysName = [NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FrequencySysName"]];

                    objentitySoldServiceNonStandardDetail.internalNotes=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InternalNotes"]];

                    //Nilind 16 Nov
                    
                    objentitySoldServiceNonStandardDetail.userName=strUserName;
                    objentitySoldServiceNonStandardDetail.companyKey=strCompanyKey;
                    
                    //........................
                    
                    objentitySoldServiceNonStandardDetail.wdoProblemIdentificationId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"WdoProblemIdentificationId"]];
                    objentitySoldServiceNonStandardDetail.mobileId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MobileId"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsBidOnRequest"]] isEqualToString:@"1"])
                    {
                        objentitySoldServiceNonStandardDetail.isBidOnRequest=@"true";
                    }
                    else
                    {
                        objentitySoldServiceNonStandardDetail.isBidOnRequest=@"false";
                    }
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsTaxable"]] isEqualToString:@"1"])
                    {
                        objentitySoldServiceNonStandardDetail.isTaxable=@"true";
                    }
                    else
                    {
                        objentitySoldServiceNonStandardDetail.isTaxable=@"false";
                    }
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
                
                
                // For SoldServiceStandardDetail //
                
                NSArray *arrSoldServiceStandardDetail=[dictLeadDetail valueForKey:@"SoldServiceStandardDetail"];
                for (int m=0; m<arrSoldServiceStandardDetail.count; m++)
                {
                    //SoldServiceStandardDetail Entiy
                    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
                    
                    
                    SoldServiceStandardDetail *objentitySoldServiceStandardDetail = [[SoldServiceStandardDetail alloc]initWithEntity:entitySoldServiceStandardDetail insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictSoldServiceNonStandardDetail=[[NSMutableDictionary alloc]init];
                    dictSoldServiceNonStandardDetail=[arrSoldServiceStandardDetail objectAtIndex:m];
                    
                    objentitySoldServiceStandardDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentitySoldServiceStandardDetail.createdBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedBy"]];
                    objentitySoldServiceStandardDetail.createdDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CreatedDate"]];
                    objentitySoldServiceStandardDetail.discount=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Discount"]];
                    objentitySoldServiceStandardDetail.initialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InitialPrice"]];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]]isEqualToString:@"1"] )
                    {
                        objentitySoldServiceStandardDetail.isSold=@"true";
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isSold=@"false";
                    }
                    // objentitySoldServiceStandardDetail.isSold=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsSold"]];
                    
                    
                    objentitySoldServiceStandardDetail.maintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"MaintenancePrice"]];
                    objentitySoldServiceStandardDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedBy"]];
                    objentitySoldServiceStandardDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedDate"]];
                    objentitySoldServiceStandardDetail.modifiedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedInitialPrice"]];
                    objentitySoldServiceStandardDetail.modifiedMaintenancePrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ModifiedMaintenancePrice"]];
                    
                    objentitySoldServiceStandardDetail.packageId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"PackageId"]];
                    
                    objentitySoldServiceStandardDetail.serviceFrequency=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceFrequency"]];
                    objentitySoldServiceStandardDetail.serviceId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceId"]];
                    objentitySoldServiceStandardDetail.serviceSysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceSysName"]];
                    objentitySoldServiceStandardDetail.soldServiceStandardId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"SoldServiceStandardId"]];
                    
                    //Nilind  30Sept
                    
                    objentitySoldServiceStandardDetail.serviceTermsConditions=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceTermsConditions"]];
                    
                    //.........................
                    
                    //Nilind 6 Oct
                    if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsTBD"]]isEqualToString:@"1"] )
                    {
                        objentitySoldServiceStandardDetail.isTBD=@"true";
                        
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isTBD=@"false";
                    }
                    //..........................
                    
                    //Nilind 16 Nov
                    
                    objentitySoldServiceStandardDetail.userName=strUserName;
                    objentitySoldServiceStandardDetail.companyKey=strCompanyKey;
                    
                    //........................
                    
                    //Nilind 22 May
                    
                    objentitySoldServiceStandardDetail.frequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FrequencySysName"]];
                    //End
                    
                    //Nilind 24 May
                    objentitySoldServiceStandardDetail.unit=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]];
                    objentitySoldServiceStandardDetail.finalUnitBasedInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FinalUnitBasedInitialPrice"]];
                    objentitySoldServiceStandardDetail.finalUnitBasedMaintPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"FinalUnitBasedMaintPrice"]];
                    
                    objentitySoldServiceStandardDetail.bundleId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]];
                    objentitySoldServiceStandardDetail.bundleDetailId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]];
                    objentitySoldServiceStandardDetail.billingFrequencySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencySysName"]];
                    objentitySoldServiceStandardDetail.discountPercentage=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"DiscountPercentage"]];
                    
                    //End
                    
                    //Bundle Change
                    
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]] isEqualToString:@""])
                        
                    {
                        
                        objentitySoldServiceStandardDetail.bundleId=@"0";
                        
                        
                    }
                    
                    else
                        
                    {
                        
                        objentitySoldServiceStandardDetail.bundleId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleId"]];
                        
                        
                    }
                    
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]] isEqualToString:@""] || [[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]] isEqualToString:@"0"])
                        
                    {
                        
                        //objentitySoldServiceStandardDetail.unit=@"0";
                        objentitySoldServiceStandardDetail.unit=@"1";

                        
                    }
                    
                    else
                        
                    {
                        
                        objentitySoldServiceStandardDetail.unit=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"Unit"]];
                        
                    }
                    
                    
                    
                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]] isEqualToString:@""])
                        
                    {
                        
                        objentitySoldServiceStandardDetail.bundleDetailId=@"0";
                        
                        
                    }
                    
                    else
                        
                    {
                        
                        objentitySoldServiceStandardDetail.bundleDetailId=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BundleDetailId"]];
                        
                        
                    }
                    
                    NSArray *arrTemp=[dictSoldServiceNonStandardDetail valueForKey:@"AdditionalParameterPriceDcs"];
                    
                    objentitySoldServiceStandardDetail.additionalParameterPriceDcs=arrTemp;
                    objentitySoldServiceStandardDetail.totalInitialPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"TotalInitialPrice"]];
                    objentitySoldServiceStandardDetail.totalMaintPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"TotalMaintPrice"]];
                    objentitySoldServiceStandardDetail.billingFrequencyPrice=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"BillingFrequencyPrice"]];
                    objentitySoldServiceStandardDetail.servicePackageName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServicePackageName"]];
                    
                    objentitySoldServiceStandardDetail.serviceDescription=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"ServiceDescription"]];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsChangeServiceDesc"]]isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsChangeServiceDesc"]] caseInsensitiveCompare:@"true"]==NSOrderedSame )
                    {
                        objentitySoldServiceStandardDetail.isChangeServiceDesc=@"true";
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isChangeServiceDesc=@"false";
                    }
                    
                    if([NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]].length==0 ||[[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]] isEqualToString:@"" ])
                    {
                        objentitySoldServiceStandardDetail.isResidentialTaxable=@"0";
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isResidentialTaxable=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsResidentialTaxable"]];
                    }
                    if([NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]].length==0 ||[[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]] isEqualToString:@"" ])
                    {
                        objentitySoldServiceStandardDetail.isCommercialTaxable=@"0";
                        
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isCommercialTaxable=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsCommercialTaxable"]];
                        
                    }

                    objentitySoldServiceStandardDetail.internalNotes=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"InternalNotes"]];

                    if([[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"IsBidOnRequest"]] isEqualToString:@"1"])
                    {
                        objentitySoldServiceStandardDetail.isBidOnRequest=@"true";
                    }
                    else
                    {
                        objentitySoldServiceStandardDetail.isBidOnRequest=@"false";
                    }
                    
                    objentitySoldServiceStandardDetail.categorySysName=[NSString stringWithFormat:@"%@",[dictSoldServiceNonStandardDetail valueForKey:@"CategorySysName"]];

                    
                    NSError *error1;
                    [context save:&error1];
                }
                
                
                //........Nilind 3 Oct................................................................
                
                // For CurrentService //
                
                
                
                NSArray *arrCurrentService=[dictLeadDetail valueForKey:@"CurrentService"];
                
                for (int m=0; m<arrCurrentService.count; m++)
                    
                {
                    
                    //SoldServiceStandardDetail Entiy
                    
                    entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
                    CurrentService *objentityCurrentService = [[CurrentService alloc]initWithEntity:entityCurrentService insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                    dictCurrentService=[arrCurrentService objectAtIndex:m];
                    objentityCurrentService.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objentityCurrentService.serviceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceName"]];
                    objentityCurrentService.serviceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceSysName"]];
                    
                    objentityCurrentService.frequency=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Frequency"]];
                    
                    objentityCurrentService.frequencyId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"FrequencyId"]];
                    
                    objentityCurrentService.technicianName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"TechnicianName"]];
                    
                    objentityCurrentService.serviceDateTime=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceDateTime"]];
                    
                    objentityCurrentService.serviceTimeSlot=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceTimeSlot"]];
                    
                    objentityCurrentService.serviceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceId"]];
                    
                    objentityCurrentService.serviceKey=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceKey"]];
                    
                    
                    //Nilind 16 Nov
                    
                    objentityCurrentService.userName=strUserName;
                    objentityCurrentService.companyKey=strCompanyKey;
                    
                    //........................
                    
                    
                    NSError *error1;
                    
                    [context save:&error1];
                    
                }
                
                //Nilind 16 Feb
                //ServiceFollowUp
                
                
                NSArray *arrServiceFollowUp=[dictLeadDetail valueForKeyPath:@"LeadFollowup.serviceFollowUpDcs"];
                
                for (int m=0; m<arrServiceFollowUp.count; m++)
                    
                {
                    
                    //SoldServiceStandardDetail Entiy
                    
                    entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
                    ServiceFollowUpDcs *objentityServiceFollowUp = [[ServiceFollowUpDcs alloc]initWithEntity:entityServiceFollowUp insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                    dictCurrentService=[arrServiceFollowUp objectAtIndex:m];
                    
                    objentityServiceFollowUp.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentityServiceFollowUp.departmentId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentId"]];
                    
                    objentityServiceFollowUp.departmentName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentName"]];
                    
                    objentityServiceFollowUp.departmentSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"DepartmentSysName"]];
                    
                    objentityServiceFollowUp.followupDate=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"FollowupDate"]];
                    
                    objentityServiceFollowUp.id=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Id"]];
                    
                    objentityServiceFollowUp.notes=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"Notes"]];
                    
                    objentityServiceFollowUp.serviceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceId"]];
                    
                    objentityServiceFollowUp.serviceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceName"]];
                    
                    objentityServiceFollowUp.serviceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ServiceSysName"]];
                    NSError *error1;
                    
                    [context save:&error1];
                    
                }
                
                //........................................................................
                
                
                //End
                
                //Nilind 16 Feb
                //ServiceFollowUp
                
                
                NSArray *arrProposalFollowUp=[dictLeadDetail valueForKeyPath:@"LeadFollowup.proposalFollowUpDcs"];
                
                for (int m=0; m<arrProposalFollowUp.count; m++)
                    
                {
                    
                    //SoldServiceStandardDetail Entiy
                    
                    entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
                    ProposalFollowUpDcs *objentityProposalFollowUp = [[ProposalFollowUpDcs alloc]initWithEntity:entityProposalFollowUp insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictCurrentService=[[NSMutableDictionary alloc]init];
                    dictCurrentService=[arrProposalFollowUp objectAtIndex:m];
                    
                    objentityProposalFollowUp.proposalLeadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentityProposalFollowUp.proposalFollowupDate=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalFollowupDate"]];
                    
                    objentityProposalFollowUp.proposalId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalId"]];
                    
                    objentityProposalFollowUp.proposalNotes=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalNotes"]];
                    
                    objentityProposalFollowUp.proposalServiceId=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceId"]];
                    
                    objentityProposalFollowUp.proposalServiceName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceName"]];
                    
                    objentityProposalFollowUp.proposalServiceSysName=[NSString stringWithFormat:@"%@",[dictCurrentService valueForKey:@"ProposalServiceSysName"]];
                    
                    NSError *error1;
                    
                    [context save:&error1];
                    
                }
                
                //........................................................................
                
                //Agreement CheckList
                
                NSArray *arrAgreementCheckList=[dictLeadDetail valueForKeyPath:@"LeadAgreementChecklistSetups"];
                
                for (int m=0; m<arrAgreementCheckList.count; m++)
                {
                    
                    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
                    LeadAgreementChecklistSetups *objentityLeadAgreementChecklistSetups = [[LeadAgreementChecklistSetups alloc]initWithEntity:entityLeadAgreementChecklistSetups insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictAgreementCheckList=[[NSMutableDictionary alloc]init];
                    dictAgreementCheckList=[arrAgreementCheckList objectAtIndex:m];
                    
                    objentityLeadAgreementChecklistSetups.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objentityLeadAgreementChecklistSetups.leadAgreementChecklistSetupId=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"LeadAgreementChecklistSetupId"]];
                    
                    objentityLeadAgreementChecklistSetups.agreementChecklistId=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"AgreementChecklistId"]];
                    
                    objentityLeadAgreementChecklistSetups.agreementChecklistSysName=[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"AgreementChecklistSysName"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictAgreementCheckList valueForKey:@"IsActive"]] isEqualToString:@"1"])
                    {
                        objentityLeadAgreementChecklistSetups.isActive=@"true";
                    }
                    else
                    {
                        objentityLeadAgreementChecklistSetups.isActive=@"false";
                    }
                    
                    objentityLeadAgreementChecklistSetups.userName=strUserName;
                    objentityLeadAgreementChecklistSetups.companyKey=strCompanyKey;
                    
                    //........................
                    NSError *error1;
                    [context save:&error1];
                    
                }
                
                //........................................................................
                //........................................................................
                
                // Electronic Authorized Form
                NSDictionary *dictAuthorizedForm;
                if([[dictLeadDetail valueForKey:@"ElectronicAuthorizationForm"] isKindOfClass:[NSDictionary class]])
                {
                    dictAuthorizedForm = [dictLeadDetail valueForKey:@"ElectronicAuthorizationForm"];
                }
                if(dictAuthorizedForm.count>0)
                {
                    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
                    
                    ElectronicAuthorizedForm *objMechanicalPOGeneric = [[ElectronicAuthorizedForm alloc] initWithEntity:entityElectronicAuthorizedForm insertIntoManagedObjectContext:context];
                    
                    objMechanicalPOGeneric.accountNo =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"AccountNo"]];
                    objMechanicalPOGeneric.leadNo = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"LeadNo"]];
                    objMechanicalPOGeneric.title = [dictAuthorizedForm valueForKey:@"Title"];
                    
                    objMechanicalPOGeneric.preNotes = [dictAuthorizedForm valueForKey:@"PreNotes"];
                    objMechanicalPOGeneric.postNotes = [dictAuthorizedForm valueForKey:@"PostNotes"];
                    objMechanicalPOGeneric.terms = [dictAuthorizedForm valueForKey:@"Terms"];
                    objMechanicalPOGeneric.termsSignUp =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"TermsSignUp"]];
                    
                    objMechanicalPOGeneric.frequency = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Frequency"]];
                    objMechanicalPOGeneric.monthlyDate =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"MonthlyDate"]];
                    objMechanicalPOGeneric.paymentMethod = [dictAuthorizedForm valueForKey:@"PaymentMethod"];
                    objMechanicalPOGeneric.bankName = [dictAuthorizedForm valueForKey:@"BankName"];            objMechanicalPOGeneric.bankAccountNo = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"BankAccountNo"]];
                    // [dictData valueForKey:@"BankAccountNo"];
                    objMechanicalPOGeneric.routingNo =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"RoutingNo"]];
                    objMechanicalPOGeneric.signaturePath = [dictAuthorizedForm valueForKey:@"SignaturePath"];            objMechanicalPOGeneric.firstName = [dictAuthorizedForm valueForKey:@"FirstName"];
                    objMechanicalPOGeneric.middleName = [dictAuthorizedForm valueForKey:@"MiddleName"];            objMechanicalPOGeneric.lastName = [dictAuthorizedForm valueForKey:@"LastName"];
                    objMechanicalPOGeneric.address1 = [dictAuthorizedForm valueForKey:@"Address1"];
                    objMechanicalPOGeneric.address2 = [dictAuthorizedForm valueForKey:@"Address2"];            objMechanicalPOGeneric.stateName = [dictAuthorizedForm valueForKey:@"StateName"];
                    objMechanicalPOGeneric.cityName = [dictAuthorizedForm valueForKey:@"CityName"];
                    objMechanicalPOGeneric.zipCode =[NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Zipcode"]];
                    objMechanicalPOGeneric.phone =[NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"Phone"]];
                    objMechanicalPOGeneric.email = [global strReplacedEmail:[dictAuthorizedForm valueForKey:@"Email"]];
                    objMechanicalPOGeneric.date =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"Date"]];
                    objMechanicalPOGeneric.createdBy =[NSString stringWithFormat:@"%@" ,[dictAuthorizedForm valueForKey:@"CreatedBy"]];
                    objMechanicalPOGeneric.signatureName =[NSString stringWithFormat:@"%@", [dictAuthorizedForm valueForKey:@"SignatureName"]];
                    objMechanicalPOGeneric.stateId = [NSString stringWithFormat:@"%@",[dictAuthorizedForm valueForKey:@"StateId"]];
                    objMechanicalPOGeneric.leadId =[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    NSError *error2;
                    [context save:&error2];
                    
                    
                }
                // For LeadAppliedDiscounts /
                // 26 Aug 2016
                
                NSArray *arrLeadAppliedDiscounts=[dictLeadDetail valueForKey:@"LeadAppliedDiscounts"];//EmailDetail
                for (int j=0; j<arrLeadAppliedDiscounts.count; j++)
                {
                    //Email Detail Entity
                    entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
                    
                    LeadAppliedDiscounts *objLeadAppliedDiscounts = [[LeadAppliedDiscounts alloc]initWithEntity:entityLeadAppliedDiscounts insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictobjLeadAppliedDiscounts=[[NSMutableDictionary alloc]init];
                    dictobjLeadAppliedDiscounts=[arrLeadAppliedDiscounts objectAtIndex:j];
                    
                    
                    objLeadAppliedDiscounts.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objLeadAppliedDiscounts.leadAppliedDiscountId=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKeyPath:@"LeadAppliedDiscountId"]];
                    objLeadAppliedDiscounts.serviceSysName=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ServiceSysName"]];
                    objLeadAppliedDiscounts.discountSysName=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountSysName"]];
                    objLeadAppliedDiscounts.discountType=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountType"]];
                    objLeadAppliedDiscounts.discountCode=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountCode"]];
                    objLeadAppliedDiscounts.discountAmount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountAmount"]];
                    objLeadAppliedDiscounts.discountDescription=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountDescription"]];
                    objLeadAppliedDiscounts.discountPercent=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"DiscountPercent"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsActive"]] isEqualToString:@"1"])
                    {
                        objLeadAppliedDiscounts.isActive=@"true";
                    }
                    else
                    {
                        objLeadAppliedDiscounts.isActive=@"false";
                    }
                    if([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
                    {
                        objLeadAppliedDiscounts.isDiscountPercent=@"true";
                    }
                    else
                    {
                        objLeadAppliedDiscounts.isDiscountPercent=@"false";
                    }
                    //Nilind 16 Nov
                    objLeadAppliedDiscounts.soldServiceId=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"SoldServiceId"]];
                    objLeadAppliedDiscounts.serviceType=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ServiceType"]];
                    
                    objLeadAppliedDiscounts.userName=strUserName;
                    objLeadAppliedDiscounts.companyKey=strCompanyKey;
                    objLeadAppliedDiscounts.appliedInitialDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AppliedInitialDiscount"]];
                    objLeadAppliedDiscounts.appliedMaintDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AppliedMaintDiscount"]];
                    //........................
                    //objLeadAppliedDiscounts.applicableForInitial=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForInitial"]];
                    //objLeadAppliedDiscounts.applicableForMaintenance=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForMaintenance"]];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForInitial"]] isEqualToString:@"1"])
                    {
                        objLeadAppliedDiscounts.applicableForInitial=@"true";
                    }
                    else
                    {
                        objLeadAppliedDiscounts.applicableForInitial=@"false";
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"ApplicableForMaintenance"]] isEqualToString:@"1"])
                    {
                        objLeadAppliedDiscounts.applicableForMaintenance=@"true";
                    }
                    else
                    {
                        objLeadAppliedDiscounts.applicableForMaintenance=@"false";
                        
                    }
                    
                    
                    
                    
                    objLeadAppliedDiscounts.isApplied=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"IsApplied"]];
                    objLeadAppliedDiscounts.accountNo=[NSString stringWithFormat:@"%@",[dictobjLeadAppliedDiscounts valueForKey:@"AccountNo"]];

                    //.................
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //......................
                
#pragma mark- SAVE CLARK PEST DATA
#pragma mark- -- LeadCommercialScopeExtDc --
                
                NSArray *arrLeadCommercialScopeExtDc=[dictLeadDetail valueForKey:@"LeadCommercialScopeExtDc"];//EmailDetail
                for (int j=0; j<arrLeadCommercialScopeExtDc.count; j++)
                {
                    //Email Detail Entity
                    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialScopeExtDc *objLeadCommercial = [[LeadCommercialScopeExtDc alloc]initWithEntity:entityLeadCommercialScopeExtDc insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictobjLeadCommercialScopeExtDc=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercialScopeExtDc=[arrLeadCommercialScopeExtDc objectAtIndex:j];
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    objLeadCommercial.scopeSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"ScopeSysName"]];
                    
                    objLeadCommercial.scopeDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"ScopeDescription"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"IsChangedScopeDesc"]] isEqualToString:@"1"])
                    {
                        objLeadCommercial.isChangedScopeDesc=@"true";
                    }
                    else
                    {
                        objLeadCommercial.isChangedScopeDesc=@"false";
                    }
                    objLeadCommercial.title=[NSString stringWithFormat:@"%@",[dictobjLeadCommercialScopeExtDc valueForKey:@"Title"]];
                    
                    NSError *error1;
                    [context save:&error1];
                }
                
                
                //........................................................................
#pragma mark- -- LeadCommercialTargetExtDc --
                
                NSArray *arrLeadCommercialTargetExtDc=[dictLeadDetail valueForKey:@"LeadCommercialTargetExtDc"];//EmailDetail
                for (int j=0; j<arrLeadCommercialTargetExtDc.count; j++)
                {
                    //Email Detail Entity
                    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialTargetExtDc *objLeadCommercial = [[LeadCommercialTargetExtDc alloc]initWithEntity:entityLeadCommercialTargetExtDc insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadCommercialTargetExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.targetSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TargetSysName"]];
                    
                    objLeadCommercial.targetDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKeyPath:@"TargetDescription"]];
                    
                    if([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"IsChangedTargetDesc"]] isEqualToString:@"1"])
                    {
                        objLeadCommercial.isChangedTargetDesc=@"true";
                    }
                    else
                    {
                        objLeadCommercial.isChangedTargetDesc=@"false";
                    }
                    objLeadCommercial.name=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"Name"]];
                    
                    objLeadCommercial.leadCommercialTargetId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommercialTargetId"]];
                    
                    objLeadCommercial.mobileTargetId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"MobileTargetId"]];
                    objLeadCommercial.wdoProblemIdentificationId = [NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"WdoProblemIdentificationId"]];
                    
#pragma mark- -- Target Image Detail --
                    
                    NSArray *arrTargetImageDetail=[dictobjLeadCommercial valueForKey:@"TargetImageDetailExtDc"];//EmailDetail
                    
                    WebService *objWebService = [[WebService alloc] init];
                    [objWebService saveTargetImageToDBWithStrLeadId:[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]] strUserName:strUserName strCompanyKey:strCompanyKey arrTargetImageDetail:arrTargetImageDetail];
                    
                    //.......................................................................................
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
                
#pragma mark- -- LeadCommercialInitialInfoExtDc --
                
                NSArray *arrLeadCommercialInitialInfoExtDc=[dictLeadDetail valueForKey:@"LeadCommercialInitialInfoExtDc"];
                for (int j=0; j<arrLeadCommercialInitialInfoExtDc.count; j++)
                {
                    //Email Detail Entity
                    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialInitialInfoExtDc *objLeadCommercial = [[LeadCommercialInitialInfoExtDc alloc]initWithEntity:entityLeadCommercialInitialInfoExtDc insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadCommercialInitialInfoExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.initialDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"initialDescription"]];
                    
                    objLeadCommercial.leadCommInitialInfoId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommInitialInfoId"]];
                    
                    objLeadCommercial.initialPrice=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"InitialPrice"]];
                    
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //.......................................................................................
#pragma mark- -- LeadCommercialMaintInfoExtDc --
                
                NSArray *arrLeadCommercialMaintInfoExtDc=[dictLeadDetail valueForKey:@"LeadCommercialMaintInfoExtDc"];
                for (int j=0; j<arrLeadCommercialMaintInfoExtDc.count; j++)
                {
                    //Email Detail Entity
                    entityLeadCommercialMaintInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialMaintInfoExtDc *objLeadCommercial = [[LeadCommercialMaintInfoExtDc alloc]initWithEntity:entityLeadCommercialMaintInfoExtDc insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadCommercialMaintInfoExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.maintenanceDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"maintenanceDescription"]];
                    
                    objLeadCommercial.maintenancePrice=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"MaintenancePrice"]];
                    
                    objLeadCommercial.frequencySysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"FrequencySysName"]];
                    
                    objLeadCommercial.leadCommMaintInfoId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommMaintInfoId"]];
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
#pragma mark- -- LeadCommercialDiscountExtDc --
                
                NSArray *arrLeadCommercialDiscountExtDc=[dictLeadDetail valueForKey:@"LeadCommercialDiscountExtDc"];
                for (int j=0; j<arrLeadCommercialDiscountExtDc.count; j++)
                {
                    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialDiscountExtDc *objLeadCommercial = [[LeadCommercialDiscountExtDc alloc]initWithEntity:entityLeadCommercialDiscountExtDc insertIntoManagedObjectContext:context];
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadCommercialDiscountExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.discountSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountSysName"]];
                    
                    objLeadCommercial.discountType=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountType"]];
                    
                    objLeadCommercial.discountDescription=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountDescription"]];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"IsDiscountPercent"]] isEqualToString:@"1"])
                    {
                        objLeadCommercial.isDiscountPercent=@"true";
                    }
                    else
                    {
                        objLeadCommercial.isDiscountPercent=@"false";
                        
                    }
                    objLeadCommercial.discountPercent=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountPercent"]];
                    objLeadCommercial.discountCode=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"DiscountCode"]];
                    
                    
                    objLeadCommercial.appliedInitialDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AppliedInitialDiscount"]];
                    
                    
                    objLeadCommercial.appliedMaintDiscount=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AppliedMaintDiscount"]];
                    
                    objLeadCommercial.accountNo=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"AccountNo"]];
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ApplicableForInitial"]] isEqualToString:@"1"])
                    {
                        objLeadCommercial.applicableForInitial=@"true";
                    }
                    else
                    {
                        objLeadCommercial.applicableForInitial=@"false";
                        
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ApplicableForMaintenance"]] isEqualToString:@"1"])
                    {
                        objLeadCommercial.applicableForMaintenance=@"true";
                    }
                    else
                    {
                        objLeadCommercial.applicableForMaintenance=@"false";
                        
                    }
                    
                    
                    NSError *error1;
                    [context save:&error1];
                }
                
                //........................................................................
#pragma mark- -- LeadCommercialTermsExtDc --
                
                NSArray *arrLeadCommercialTermsExtDc=[dictLeadDetail valueForKey:@"LeadCommercialTermsExtDc"];
                for (int j=0; j<arrLeadCommercialTermsExtDc.count; j++)
                {
                    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
                    
                    LeadCommercialTermsExtDc *objLeadCommercial = [[LeadCommercialTermsExtDc alloc]initWithEntity:entityLeadCommercialTermsExtDc insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadCommercialTermsExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.branchSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"BranchSysName"]];
                    
                    objLeadCommercial.termsnConditions=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TermsnConditions"]];
                    
                    objLeadCommercial.leadCommercialTermsId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"LeadCommercialTermsId"]];
                    
                    objLeadCommercial.termsId=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"TermsId"]];
                    
                    
                    NSError *error1;
                    [context save:&error1];
                }
                //........................................................................
#pragma mark- -- LeadCommercialDetailExtDc --
                
                entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
                
                LeadCommercialDetailExtDc *objLeadCommercialDetailExtDc = [[LeadCommercialDetailExtDc alloc]initWithEntity:entityLeadCommercialDetailExtDc insertIntoManagedObjectContext:context];
                if(![[dictLeadDetail valueForKey:@"LeadCommercialDetailExtDc"] isKindOfClass:[NSDictionary class]])
                {
                    NSLog(@"NOTHING IN LeadCommercialDetailExtDc");
                }
                else
                {
                    objLeadCommercialDetailExtDc.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercialDetailExtDc.userName=strUserName;
                    
                    objLeadCommercialDetailExtDc.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercialDetailExtDc.leadCommercialDetailId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.LeadCommercialDetailId"]];
                    
                    objLeadCommercialDetailExtDc.coverLetterSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.CoverLetterSysName"]];
                    
                    objLeadCommercialDetailExtDc.introSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IntroSysName"]];
                    
                    objLeadCommercialDetailExtDc.introContent=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IntroContent"]];
                    
                    objLeadCommercialDetailExtDc.termsOfServiceSysName=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.TermsOfServiceSysName"]];
                    
                    objLeadCommercialDetailExtDc.termsOfServiceContent=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.TermsOfServiceContent"]];
                    
                    objLeadCommercialDetailExtDc.isAgreementValidFor=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsAgreementValidFor"]];
                    
                    objLeadCommercialDetailExtDc.validFor=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.ValidFor"]];
                    
                    objLeadCommercialDetailExtDc.isInitialTaxApplicable=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsInitialTaxApplicable"]];
                    objLeadCommercialDetailExtDc.isMaintTaxApplicable=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadCommercialDetailExtDc.IsMaintTaxApplicable"]];
                    
                }
                
                //End
                
                //........................................................................
#pragma mark- -- LeadMarketingContentExtDc --
                
                NSArray *arrLeadMarketingContentExtDc=[dictLeadDetail valueForKey:@"LeadMarketingContentExtDc"];
                for (int j=0; j<arrLeadMarketingContentExtDc.count; j++)
                {
                    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
                    
                    LeadMarketingContentExtDc *objLeadCommercial = [[LeadMarketingContentExtDc alloc]initWithEntity:entityLeadMarketingContentExtDc insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictobjLeadCommercial=[[NSMutableDictionary alloc]init];
                    dictobjLeadCommercial=[arrLeadMarketingContentExtDc objectAtIndex:j];
                    
                    
                    
                    objLeadCommercial.companyKey=strCompanyKey;
                    
                    
                    objLeadCommercial.userName=strUserName;
                    
                    objLeadCommercial.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    
                    
                    objLeadCommercial.contentSysName=[NSString stringWithFormat:@"%@",[dictobjLeadCommercial valueForKey:@"ContentSysName"]];
                    
                    
                    NSError *error1;
                    [context save:&error1];
                }

                #pragma mark- -- LeadCotact Details --

                // For LeadContactDetailExtSerDc
                
                NSArray *arrLeadContactDetail=[dictLeadDetail valueForKey:@"LeadContactDetailExtSerDc"];
                for (int z=0; z<arrLeadContactDetail.count; z++)
                {
                    entityContactNumberDetail=[NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
                    
                    LeadContactDetailExtSerDc *objLeadContactDetail = [[LeadContactDetailExtSerDc alloc]initWithEntity:entityContactNumberDetail insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictLeadContactDetail=[[NSMutableDictionary alloc]init];
                    dictLeadContactDetail=[arrLeadContactDetail objectAtIndex:z];
                  
                    
                    
                    objLeadContactDetail.leadContactId = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"LeadContactId"]];
                    objLeadContactDetail.contactNumber = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ContactNumber"]];
                    objLeadContactDetail.subject = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"Subject"]];
                    
                    objLeadContactDetail.createdBy = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"CreatedBy"]];

                    objLeadContactDetail.modifiedBy = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ModifiedBy"]];

                    objLeadContactDetail.createdDate = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"CreatedDate"]];
                    objLeadContactDetail.modifiedDate = [NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"ModifiedDate"]];
                    
                    if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsMessageSent"]] isEqualToString:@"1"])
                    {
                        objLeadContactDetail.isMessageSent = @"true";

                    }
                    else
                    {
                        objLeadContactDetail.isMessageSent = @"false";

                    }
                    if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsCustomerNumber"]] isEqualToString:@"1"])
                    {
                        objLeadContactDetail.isCustomerNumber = @"true";
                        
                    }
                    else
                    {
                        objLeadContactDetail.isCustomerNumber = @"false";
                        
                    }
                    if ([[NSString stringWithFormat:@"%@",[dictLeadContactDetail valueForKey:@"IsDefaultNumber"]] isEqualToString:@"1"])
                    {
                        objLeadContactDetail.isDefaultNumber = @"true";
                        
                    }
                    else
                    {
                        objLeadContactDetail.isDefaultNumber = @"false";
                        
                    }
                    //Nilind 16 Nov
                     objLeadContactDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objLeadContactDetail.userName=strUserName;
                    objLeadContactDetail.companyKey=strCompanyKey;
                    
                    //........................
                    NSError *error10;
                    [context save:&error10];
                    //}
                }
                //........................................................................
                //........................................................................
                
                
            #pragma mark- -- Renewal Service Details --

                                       
                NSArray *arrRenewalServiceDetail=[dictLeadDetail valueForKey:@"RenewalServiceExtDcs"];
                for (int z=0; z<arrRenewalServiceDetail.count; z++)
                {
                    entityRenewalServiceDetail=[NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
                    
                    RenewalServiceExtDcs *objRenewalServiceDetail = [[RenewalServiceExtDcs alloc]initWithEntity:entityRenewalServiceDetail insertIntoManagedObjectContext:context];
                    
                    NSMutableDictionary *dictRenewalServiceDetail=[[NSMutableDictionary alloc]init];
                    dictRenewalServiceDetail=[arrRenewalServiceDetail objectAtIndex:z];
                    
                    
                    objRenewalServiceDetail.renewalAmount = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalAmount"]];
                    objRenewalServiceDetail.renewalDescription = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalDescription"]];
                    objRenewalServiceDetail.renewalFrequencySysName = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalFrequencySysName"]];
                    objRenewalServiceDetail.renewalPercentage = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalPercentage"]];
                    objRenewalServiceDetail.renewalServiceId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"RenewalServiceId"]];
                    objRenewalServiceDetail.serviceId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"ServiceId"]];
                    objRenewalServiceDetail.soldServiceStandardId = [NSString stringWithFormat:@"%@",[dictRenewalServiceDetail valueForKey:@"SoldServiceStandardId"]];
                    objRenewalServiceDetail.leadId=[NSString stringWithFormat:@"%@",[dictLeadDetail valueForKeyPath:@"LeadDetail.LeadId"]];
                    objRenewalServiceDetail.userName=strUserName;
                    objRenewalServiceDetail.companyKey=strCompanyKey;
                    
                    //........................
                    NSError *error10;
                    [context save:&error10];
                    
                }
                //........................................................................
                //........................................................................
                
                
        
                NSError *error;
                [context save:&error];
                
                break;
                
            }
        }
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendSalesLeadToServer"];
    
    if (arrOfLeadsTosendToServer.count==0) {
        
        if (isServiceAutoEnabled) {
            
            [self downloadTotalWorkOrdersServiceAutomation];
            
        } else {
            
            [self salesFetch];
            [DejalBezelActivityView removeView];
            
        }
        
        // [self salesFetch];
        
    } else {
        
        // NSString *strrr=[NSString stringWithFormat:@"Sales Dynamic %lu",(unsigned long)arrOfLeadsTosendToServer.count];
        //  [self HudView:strrr];
        
        [self sendingDynamicFormDataToServer:0];
        
    }
}

//============================================================================
//============================================================================

#pragma mark- Sales Info CoreData Delete

//============================================================================
//============================================================================


-(void)deleteFromCoreDataSalesInfo
{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrLeadIdToDelete=[defs objectForKey:@"saveSalesLeadToLocalDbnDelete"];
    
    NSLog(@"Following Are being Deleted====%@",arrLeadIdToDelete);
    
    for (int k=0; k<arrLeadIdToDelete.count; k++)
    {
        
        NSString *strLeadIDD=[NSString stringWithFormat:@"%@",arrLeadIdToDelete[k]];
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setEntity:[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND employeeNo_Mobile = %@",strLeadIDD,strEmployeeNo];
        [allData setPredicate:predicate];
        
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //  Delete Email Detail Data
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
        [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataEmail setPredicate:predicateEmail];
        
        [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1 = nil;
        NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
        //error handling goes here
        for (NSManagedObject * data in DataEmail) {
            [context deleteObject:data];
        }
        NSError *saveError1 = nil;
        [context save:&saveError1];
        
        
        //  Delete Image Detail Data
        entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
        [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataImage setPredicate:predicateImage];
        
        [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error2 = nil;
        NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
        //error handling goes here
        for (NSManagedObject * data in DataImage) {
            [context deleteObject:data];
        }
        NSError *saveError2 = nil;
        [context save:&saveError2];
        
        
        //  Delete PaymentInfo Detail Data
        entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
        NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
        [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context]];
        
        NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataPaymentInfo setPredicate:predicatePaymentInfo];
        
        [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorPaymentInfo = nil;
        NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
        //error handling goes here
        for (NSManagedObject * data in DataPaymentInfo) {
            [context deleteObject:data];
        }
        NSError *saveErrorPaymentInfo = nil;
        [context save:&saveErrorPaymentInfo];
        
        
        
        //  Delete LeadPreference Detail Data
        entityLeadPreference=[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
        NSFetchRequest *allDataLeadPreference = [[NSFetchRequest alloc] init];
        [allDataLeadPreference setEntity:[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadPreference =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataLeadPreference setPredicate:predicateLeadPreference];
        
        [allDataLeadPreference setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadPreference = nil;
        NSArray * DataLeadPreference = [context executeFetchRequest:allDataLeadPreference error:&errorLeadPreference];
        //error handling goes here
        for (NSManagedObject * data in DataLeadPreference) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadPreference = nil;
        [context save:&saveErrorLeadPreference];
        
        
        //  Delete LeadPreference Detail Data
        entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataSoldServiceNonStandardDetail = [[NSFetchRequest alloc] init];
        [allDataSoldServiceNonStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateSoldServiceNonStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSoldServiceNonStandardDetail setPredicate:predicateSoldServiceNonStandardDetail];
        
        [allDataSoldServiceNonStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSoldServiceNonStandardDetail = nil;
        NSArray * DataSoldServiceNonStandardDetail = [context executeFetchRequest:allDataSoldServiceNonStandardDetail error:&errorSoldServiceNonStandardDetail];
        //error handling goes here
        for (NSManagedObject * data in DataSoldServiceNonStandardDetail) {
            [context deleteObject:data];
        }
        NSError *saveErrorSoldServiceNonStandardDetail = nil;
        [context save:&saveErrorSoldServiceNonStandardDetail];
        
        
        //  Delete SoldServiceStandardDetail Detail Data
        entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataSoldServiceStandardDetail = [[NSFetchRequest alloc] init];
        [allDataSoldServiceStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateSoldServiceStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSoldServiceStandardDetail setPredicate:predicateSoldServiceStandardDetail];
        
        [allDataSoldServiceStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSoldServiceStandardDetail = nil;
        NSArray * DataSoldServiceStandardDetail = [context executeFetchRequest:allDataSoldServiceStandardDetail error:&errorSoldServiceStandardDetail];
        //error handling goes here
        for (NSManagedObject * data in DataSoldServiceStandardDetail)
        {
            [context deleteObject:data];
        }
        NSError *saveErrorSoldServiceStandardDetail = nil;
        [context save:&saveErrorSoldServiceStandardDetail];
        
        
        //Delete Documents Detail Data
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        NSFetchRequest *allDataDocumentsDetail = [[NSFetchRequest alloc] init];
        [allDataDocumentsDetail setEntity:[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateDocumentsDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataDocumentsDetail setPredicate:predicateDocumentsDetail];
        
        [allDataDocumentsDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorDocumentsDetail = nil;
        NSArray * DataDocumentsDetail = [context executeFetchRequest:allDataDocumentsDetail error:&errorDocumentsDetail];
        //error handling goes here
        for (NSManagedObject * data in DataDocumentsDetail) {
            [context deleteObject:data];
        }
        NSError *saveErrorDocumentsDetail = nil;
        [context save:&saveErrorDocumentsDetail];
        
        
        //Delete ModifyDate Sales Data
        entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
        [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"leadIdd = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
        
        [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesModifyDate = nil;
        NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
        //error handling goes here
        for (NSManagedObject * data in DataSalesModifyDate) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesModifyDate = nil;
        [context save:&saveErrorSalesModifyDate];
        
        //......................................................
        
        //Delete Current Service Data
        entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
        
        NSFetchRequest *allDataCurrentService = [[NSFetchRequest alloc] init];
        [allDataCurrentService setEntity:[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context]];
        NSPredicate *predicateCurrentService =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateCurrentService];
        [allDataCurrentService setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorCurrentService= nil;
        NSArray * DataCurrentService = [context executeFetchRequest:allDataCurrentService error:&errorCurrentService];
        //error handling goes here
        for (NSManagedObject * data in DataCurrentService) {
            [context deleteObject:data];
        }
        NSError *saveErrorCurrentService = nil;
        [context save:&saveErrorCurrentService];
        
        //......................................................
        
        //Nilind 16 Feb
        
        //Delete ServiceFollowUp Data
        
        entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataServiceFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceFollowUpDcs =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataServiceFollowUpDcs setPredicate:predicateServiceFollowUpDcs];
        [allDataServiceFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceFollowUpDcs= nil;
        NSArray * DataServiceFollowUpDcs = [context executeFetchRequest:allDataServiceFollowUpDcs error:&errorServiceFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataServiceFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceFollowUpDcs = nil;
        [context save:&saveErrorServiceFollowUpDcs];
        
        //......................................................
        
        
        //Nilind 16 Feb
        
        //Delete ProposalFollowUp Data
        
        entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataProposalFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataProposalFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateProposalFollowUpDcs =[NSPredicate predicateWithFormat:@"proposalLeadId = %@",strLeadIDD];
        
        [allDataProposalFollowUpDcs setPredicate:predicateProposalFollowUpDcs];
        [allDataProposalFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorProposalFollowUpDcs= nil;
        NSArray * DataProposalFollowUpDcs = [context executeFetchRequest:allDataProposalFollowUpDcs error:&errorProposalFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataProposalFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorProposalFollowUpDcs = nil;
        [context save:&saveErrorProposalFollowUpDcs];
        
        //......................................................
        
        //Delete AgreementChecklist Data
        
        entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadAgreementChecklistSetups = [[NSFetchRequest alloc] init];
        [allDataLeadAgreementChecklistSetups setEntity:[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadAgreementChecklistSetups =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadAgreementChecklistSetups setPredicate:predicateLeadAgreementChecklistSetups];
        [allDataLeadAgreementChecklistSetups setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadAgreementChecklistSetups= nil;
        NSArray * DataLeadAgreementChecklistSetups = [context executeFetchRequest:allDataLeadAgreementChecklistSetups error:&errorLeadAgreementChecklistSetups];
        //error handling goes here
        for (NSManagedObject * data in DataLeadAgreementChecklistSetups) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadAgreementChecklistSetups = nil;
        [context save:&saveErrorLeadAgreementChecklistSetups];
        
        //......................................................
        
        // delete electronic authorized form
        entityElectronicAuthorizedForm=[NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
        NSFetchRequest *electronicAuthorizedForm = [[NSFetchRequest alloc] init];
        [electronicAuthorizedForm setEntity:[NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context]];
        
        NSPredicate *predicateElectronicAuthorizedForm =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [electronicAuthorizedForm setPredicate:predicateElectronicAuthorizedForm];
        
        [electronicAuthorizedForm setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error3 = nil;
        NSArray * arrForm = [context executeFetchRequest:electronicAuthorizedForm error:&error3];
        //error handling goes here
        for (NSManagedObject * data in arrForm) {
            [context deleteObject:data];
        }
        NSError *saveError3 = nil;
        [context save:&saveError3];
        
        //Delete LeadAppliedDiscounts Data
        
        entityLeadAppliedDiscounts=[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadAppliedDiscounts = [[NSFetchRequest alloc] init];
        [allDataLeadAppliedDiscounts setEntity:[NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadAppliedDiscounts =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadAppliedDiscounts setPredicate:predicateLeadAppliedDiscounts];
        [allDataLeadAppliedDiscounts setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadAppliedDiscounts= nil;
        NSArray * DataLeadAppliedDiscounts = [context executeFetchRequest:allDataLeadAppliedDiscounts error:&errorLeadAppliedDiscounts];
        //error handling goes here
        for (NSManagedObject * data in DataLeadAppliedDiscounts) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadAppliedDiscounts = nil;
        [context save:&saveErrorLeadAppliedDiscounts];
        
        //......................................................
#pragma mark- Clark Pest
        //Delete LeadCommercialScopeExtDc Data
        
        entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialScopeExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialScopeExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialScopeExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialScopeExtDc setPredicate:predicateLeadCommercialScopeExtDc];
        [allDataLeadCommercialScopeExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialScopeExtDc= nil;
        NSArray * DataLeadCommercialScopeExtDc = [context executeFetchRequest:allDataLeadCommercialScopeExtDc error:&errorLeadCommercialScopeExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialScopeExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialScopeExtDc = nil;
        [context save:&saveErrorLeadCommercialScopeExtDc];
        
        //......................................................
        
        
        //Delete LeadCommercialTargetExtDc Data
        
        entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialTargetExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialTargetExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialTargetExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialTargetExtDc setPredicate:predicateLeadCommercialTargetExtDc];
        [allDataLeadCommercialTargetExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialTargetExtDc= nil;
        NSArray * DataLeadCommercialTargetExtDc = [context executeFetchRequest:allDataLeadCommercialTargetExtDc error:&errorLeadCommercialTargetExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialTargetExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialTargetExtDc = nil;
        [context save:&saveErrorLeadCommercialTargetExtDc];
        
        //......................................................
        
        
        //Delete LeadCommercialInitialInfoExtDc Data
        
        entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialInitialInfoExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialInitialInfoExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialInitialInfoExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialInitialInfoExtDc setPredicate:predicateLeadCommercialInitialInfoExtDc];
        [allDataLeadCommercialInitialInfoExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialInitialInfoExtDc= nil;
        NSArray * DataLeadCommercialInitialInfoExtDc = [context executeFetchRequest:allDataLeadCommercialInitialInfoExtDc error:&errorLeadCommercialInitialInfoExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialInitialInfoExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialInitialInfoExtDc = nil;
        [context save:&saveErrorLeadCommercialInitialInfoExtDc];
        
        //......................................................
        
        
        //Delete LeadCommercialMaintInfoExtDc Data
        
        entityLeadCommercialMaintInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialMaintInfoExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialMaintInfoExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialMaintInfoExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialMaintInfoExtDc setPredicate:predicateLeadCommercialMaintInfoExtDc];
        [allDataLeadCommercialMaintInfoExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialMaintInfoExtDc= nil;
        NSArray * DataLeadCommercialMaintInfoExtDc = [context executeFetchRequest:allDataLeadCommercialMaintInfoExtDc error:&errorLeadCommercialMaintInfoExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialMaintInfoExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialMaintInfoExtDc = nil;
        [context save:&saveErrorLeadCommercialMaintInfoExtDc];
        
        //......................................................
        
        //Delete LeadCommercialDiscountExtDc Data
        
        entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialDiscountExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialDiscountExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialDiscountExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialDiscountExtDc setPredicate:predicateLeadCommercialDiscountExtDc];
        [allDataLeadCommercialDiscountExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialDiscountExtDc= nil;
        NSArray * DataLeadCommercialDiscountExtDc = [context executeFetchRequest:allDataLeadCommercialDiscountExtDc error:&errorLeadCommercialDiscountExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialDiscountExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialDiscountExtDc = nil;
        [context save:&saveErrorLeadCommercialDiscountExtDc];
        
        //......................................................
        
        //Delete LeadCommercialTermsExtDc Data
        
        entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialTermsExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialTermsExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialTermsExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialTermsExtDc setPredicate:predicateLeadCommercialTermsExtDc];
        [allDataLeadCommercialTermsExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialTermsExtDc= nil;
        NSArray * DataLeadCommercialTermsExtDc = [context executeFetchRequest:allDataLeadCommercialTermsExtDc error:&errorLeadCommercialTermsExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialTermsExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialTermsExtDc = nil;
        [context save:&saveErrorLeadCommercialTermsExtDc];
        
        //......................................................
        
        
        //Delete LeadCommercialDetailExtDc Data
        
        entityLeadCommercialDetailExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadCommercialDetailExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadCommercialDetailExtDc setEntity:[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadCommercialDetailExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadCommercialDetailExtDc setPredicate:predicateLeadCommercialDetailExtDc];
        [allDataLeadCommercialDetailExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadCommercialDetailExtDc= nil;
        NSArray * DataLeadCommercialDetailExtDc = [context executeFetchRequest:allDataLeadCommercialDetailExtDc error:&errorLeadCommercialDetailExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadCommercialDetailExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadCommercialDetailExtDc = nil;
        [context save:&saveErrorLeadCommercialDetailExtDc];
        
        //......................................................

        
        //Delete LeadMarketingContentExtDc Data
        
        entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadMarketingContentExtDc = [[NSFetchRequest alloc] init];
        [allDataLeadMarketingContentExtDc setEntity:[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadMarketingContentExtDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadMarketingContentExtDc setPredicate:predicateLeadMarketingContentExtDc];
        [allDataLeadMarketingContentExtDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadMarketingContentExtDc= nil;
        NSArray * DataLeadMarketingContentExtDc = [context executeFetchRequest:allDataLeadMarketingContentExtDc error:&errorLeadMarketingContentExtDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadMarketingContentExtDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadMarketingContentExtDc = nil;
        [context save:&saveErrorLeadMarketingContentExtDc];
        
        //Delete LeadContact Detail Data
        
        entityContactNumberDetail=[NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
        
        NSFetchRequest *allDataLeadContactDetailExtSerDc = [[NSFetchRequest alloc] init];
        [allDataLeadContactDetailExtSerDc setEntity:[NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadContactDetailExtSerDc =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataLeadContactDetailExtSerDc setPredicate:predicateLeadContactDetailExtSerDc];
        [allDataLeadContactDetailExtSerDc setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadContactDetailExtSerDc= nil;
        NSArray * DataLeadContactDetailExtSerDc = [context executeFetchRequest:allDataLeadContactDetailExtSerDc error:&errorLeadContactDetailExtSerDc];
        //error handling goes here
        for (NSManagedObject * data in DataLeadContactDetailExtSerDc) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadContactDetailExtSerDc = nil;
        [context save:&saveErrorLeadContactDetailExtSerDc];
        
        
        //......................................................
        //Delete Target Image
        
        WebService *objWebService = [[WebService alloc] init];
        [objWebService deleteAllTargetImageDetailWithStrWoId:strLeadIDD];
        
        //........................................................
        //Delete RenewalService Detail Data
             
        [self deleteSalesDataWithEntityName:@"RenewalServiceExtDcs" WithPredicate:[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD]];
    }
}
-(void)deleteSalesDataWithEntityName : (NSString *)strEntityName WithPredicate:(NSPredicate *)predicate
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    
    [allData setEntity:[NSEntityDescription entityForName:strEntityName inManagedObjectContext:context]];
    
    //NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND employeeNo_Mobile = %@",strLeadIDD,strEmployeeNo];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}


-(void)deleteFromCoreDataServiceInfo{
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrLeadIdToDelete=[defs objectForKey:@"saveServiceLeadToLocalDbnDelete"];
    
    NSLog(@"Following Are being Deleted====%@",arrLeadIdToDelete);
    
    for (int k=0; k<arrLeadIdToDelete.count; k++) {
        
        NSString *strLeadIDD=[NSString stringWithFormat:@"%@",arrLeadIdToDelete[k]];
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context]];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allData setPredicate:predicate];
        
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //  Delete Email Detail Data
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
        [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataEmail setPredicate:predicateEmail];
        
        [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1 = nil;
        NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
        //error handling goes here
        for (NSManagedObject * data in DataEmail) {
            [context deleteObject:data];
        }
        NSError *saveError1 = nil;
        [context save:&saveError1];
        
        //  Delete Equipment Detail Data
        entityWOEquipmentServiceAuto=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
        NSFetchRequest *allDataWOEquipment = [[NSFetchRequest alloc] init];
        [allDataWOEquipment setEntity:[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context]];
        
        NSPredicate *predicateWOEquipment =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataWOEquipment setPredicate:predicateWOEquipment];
        
        [allDataWOEquipment setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1WOEquipment = nil;
        NSArray * DataWOEquipment = [context executeFetchRequest:allDataWOEquipment error:&error1WOEquipment];
        //error handling goes here
        for (NSManagedObject * data in DataWOEquipment) {
            [context deleteObject:data];
        }
        NSError *saveError1WOEquipment = nil;
        [context save:&saveError1WOEquipment];
        
        //  Delete Image Detail Data
        entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
        [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataImage setPredicate:predicateImage];
        
        [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error2 = nil;
        NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
        //error handling goes here
        for (NSManagedObject * data in DataImage) {
            [context deleteObject:data];
        }
        NSError *saveError2 = nil;
        [context save:&saveError2];
        
        
        //Nilind   Delete Termite Image Detail Data
        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
        NSFetchRequest *allDataImageTermite = [[NSFetchRequest alloc] init];
        [allDataImageTermite setEntity:[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context]];
        
        NSPredicate *predicateImageTermite =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataImageTermite setPredicate:predicateImageTermite];
        
        [allDataImageTermite setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error2Termite = nil;
        NSArray * DataImageTermite = [context executeFetchRequest:allDataImageTermite error:&error2Termite];
        //error handling goes here
        for (NSManagedObject * data in DataImageTermite) {
            [context deleteObject:data];
        }
        NSError *saveError2Termite = nil;
        [context save:&saveError2Termite];
        
        //End
        
        //  Delete PaymentInfo Detail Data
        entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
        [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataPaymentInfo setPredicate:predicatePaymentInfo];
        
        [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorPaymentInfo = nil;
        NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
        //error handling goes here
        for (NSManagedObject * data in DataPaymentInfo) {
            [context deleteObject:data];
        }
        NSError *saveErrorPaymentInfo = nil;
        [context save:&saveErrorPaymentInfo];
        
        
        //Delete Chemical List Sales Data
        entityChemicalListDetailServiceAuto=[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceChemical = [[NSFetchRequest alloc] init];
        [allDataServiceChemical setEntity:[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceChemical =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataServiceChemical setPredicate:predicateServiceChemical];
        
        [allDataServiceChemical setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceChemical = nil;
        NSArray * DataServiceChemical = [context executeFetchRequest:allDataServiceChemical error:&errorServiceChemical];
        //error handling goes here
        for (NSManagedObject * data in DataServiceChemical) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceChemical = nil;
        [context save:&saveErrorServiceChemical];
        
        //......................................................
        
        
        
        //Delete ModifyDate Service Data
        entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
        [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
        
        [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesModifyDate = nil;
        NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
        //error handling goes here
        for (NSManagedObject * data in DataSalesModifyDate) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesModifyDate = nil;
        [context save:&saveErrorSalesModifyDate];
        
        
        //Delete Service Documentss
        
        //Delete ModifyDate Service Data
        NSEntityDescription *entityDocumentsDetail12=[NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesModifyDate123 = [[NSFetchRequest alloc] init];
        [allDataSalesModifyDate123 setEntity:[NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesModifyDate123 =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataSalesModifyDate123 setPredicate:predicateSalesModifyDate123];
        
        [allDataSalesModifyDate123 setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesModifyDate123 = nil;
        NSArray * DataSalesModifyDate123 = [context executeFetchRequest:allDataSalesModifyDate123 error:&errorSalesModifyDate123];
        //error handling goes here
        for (NSManagedObject * data in DataSalesModifyDate123) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesModifyDate123 = nil;
        [context save:&saveErrorSalesModifyDate123];
        
        
        //Delete Texas termite flow
        entityTermiteTexas=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context];
        
        NSFetchRequest *allDataTexasTermite = [[NSFetchRequest alloc] init];
        [allDataTexasTermite setEntity:[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateTexasTermite =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataTexasTermite setPredicate:predicateTexasTermite];
        
        [allDataTexasTermite setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorTexasTermite = nil;
        NSArray * DataTexasTermite = [context executeFetchRequest:allDataTexasTermite error:&errorTexasTermite];
        //error handling goes here
        for (NSManagedObject * data in DataTexasTermite) {
            [context deleteObject:data];
        }
        NSError *saveErrorTexasTermite = nil;
        [context save:&saveErrorTexasTermite];
        
        //......................................................
        
        //Florida
        //Delete Florida termite flow
        entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
        
        NSFetchRequest *allDataTermiteFlorida = [[NSFetchRequest alloc] init];
        [allDataTermiteFlorida setEntity:[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateTermiteFlorida =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataTermiteFlorida setPredicate:predicateTermiteFlorida];
        
        [allDataTermiteFlorida setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorTermiteFlorida = nil;
        NSArray * DataTermiteFlorida = [context executeFetchRequest:allDataTermiteFlorida error:&errorTermiteFlorida];
        //error handling goes here
        for (NSManagedObject * data in DataTermiteFlorida) {
            [context deleteObject:data];
        }
        NSError *saveErrorTermiteFlorida = nil;
        [context save:&saveErrorTermiteFlorida];
        
        [self deleteServiceAddressPOCDetailDcsService:strLeadIDD];
        
        //......................................................
        /*
         //Delete ServiceDynamicForm Service Data
         entityServiceDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
         
         NSFetchRequest *allDataServiceDynamic = [[NSFetchRequest alloc] init];
         [allDataServiceDynamic setEntity:[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context]];
         
         NSPredicate *predicateServiceDynamic =[NSPredicate predicateWithFormat:@"workOrderId = %@",strLeadIDD];
         [allDataServiceDynamic setPredicate:predicateServiceDynamic];
         
         [allDataServiceDynamic setIncludesPropertyValues:NO]; //only fetch the managedObjectID
         
         NSError * errorServiceDynamic = nil;
         NSArray * DataServiceDynamic = [context executeFetchRequest:allDataServiceDynamic error:&errorServiceDynamic];
         //error handling goes here
         for (NSManagedObject * data in DataServiceDynamic) {
         [context deleteObject:data];
         }
         NSError *saveErrorServiceDynamic = nil;
         [context save:&saveErrorServiceDynamic];
         
         //......................................................
         
         */
    }
}


-(void)deleteServiceAddressPOCDetailDcsService :(NSString*)strServiceWorkOrderId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalServiceAddressPOCDetailDcs Data
    entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strServiceWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];

    [self deleteBillingAddressPOCDetailDcsService:strServiceWorkOrderId];
    
}


-(void)deleteBillingAddressPOCDetailDcsService :(NSString*)strServiceWorkOrderId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete entityMechanicalBillingAddressPOCDetailDcs Data
    entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strServiceWorkOrderId];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}


-(void)deleteFromCoreDataSalesInfoExtra{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrLeadIdToDelete=[defs objectForKey:@"DeleteExtraWorkOrderFromDBSales"];
    
    [defs setObject:nil forKey:@"DeleteExtraWorkOrderFromDBSales"];
    [defs synchronize];
    
    NSLog(@"Following Are being Deleted====%@",arrLeadIdToDelete);
    
    for (int k=0; k<arrLeadIdToDelete.count; k++) {
        
        NSString *strLeadIDD=[NSString stringWithFormat:@"%@",arrLeadIdToDelete[k]];
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setEntity:[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND employeeNo_Mobile = %@",strLeadIDD,strEmployeeNo];
        [allData setPredicate:predicate];
        
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //  Delete Email Detail Data
        entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
        [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataEmail setPredicate:predicateEmail];
        
        [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1 = nil;
        NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
        //error handling goes here
        for (NSManagedObject * data in DataEmail) {
            [context deleteObject:data];
        }
        NSError *saveError1 = nil;
        [context save:&saveError1];
        
        
        //  Delete Image Detail Data
        entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
        [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataImage setPredicate:predicateImage];
        
        [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error2 = nil;
        NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
        //error handling goes here
        for (NSManagedObject * data in DataImage) {
            [context deleteObject:data];
        }
        NSError *saveError2 = nil;
        [context save:&saveError2];
        
        
        //  Delete PaymentInfo Detail Data
        entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
        NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
        [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context]];
        
        NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataPaymentInfo setPredicate:predicatePaymentInfo];
        
        [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorPaymentInfo = nil;
        NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
        //error handling goes here
        for (NSManagedObject * data in DataPaymentInfo) {
            [context deleteObject:data];
        }
        NSError *saveErrorPaymentInfo = nil;
        [context save:&saveErrorPaymentInfo];
        
        
        
        //  Delete LeadPreference Detail Data
        entityLeadPreference=[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
        NSFetchRequest *allDataLeadPreference = [[NSFetchRequest alloc] init];
        [allDataLeadPreference setEntity:[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context]];
        
        NSPredicate *predicateLeadPreference =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataLeadPreference setPredicate:predicateLeadPreference];
        
        [allDataLeadPreference setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorLeadPreference = nil;
        NSArray * DataLeadPreference = [context executeFetchRequest:allDataLeadPreference error:&errorLeadPreference];
        //error handling goes here
        for (NSManagedObject * data in DataLeadPreference) {
            [context deleteObject:data];
        }
        NSError *saveErrorLeadPreference = nil;
        [context save:&saveErrorLeadPreference];
        
        
        //  Delete LeadPreference Detail Data
        entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataSoldServiceNonStandardDetail = [[NSFetchRequest alloc] init];
        [allDataSoldServiceNonStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateSoldServiceNonStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSoldServiceNonStandardDetail setPredicate:predicateSoldServiceNonStandardDetail];
        
        [allDataSoldServiceNonStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSoldServiceNonStandardDetail = nil;
        NSArray * DataSoldServiceNonStandardDetail = [context executeFetchRequest:allDataSoldServiceNonStandardDetail error:&errorSoldServiceNonStandardDetail];
        //error handling goes here
        for (NSManagedObject * data in DataSoldServiceNonStandardDetail) {
            [context deleteObject:data];
        }
        NSError *saveErrorSoldServiceNonStandardDetail = nil;
        [context save:&saveErrorSoldServiceNonStandardDetail];
        
        
        //  Delete SoldServiceStandardDetail Detail Data
        entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
        NSFetchRequest *allDataSoldServiceStandardDetail = [[NSFetchRequest alloc] init];
        [allDataSoldServiceStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateSoldServiceStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSoldServiceStandardDetail setPredicate:predicateSoldServiceStandardDetail];
        
        [allDataSoldServiceStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSoldServiceStandardDetail = nil;
        NSArray * DataSoldServiceStandardDetail = [context executeFetchRequest:allDataSoldServiceStandardDetail error:&errorSoldServiceStandardDetail];
        //error handling goes here
        for (NSManagedObject * data in DataSoldServiceStandardDetail)
        {
            [context deleteObject:data];
        }
        NSError *saveErrorSoldServiceStandardDetail = nil;
        [context save:&saveErrorSoldServiceStandardDetail];
        
        
        //Delete Documents Detail Data
        entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
        
        NSFetchRequest *allDataDocumentsDetail = [[NSFetchRequest alloc] init];
        [allDataDocumentsDetail setEntity:[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context]];
        
        NSPredicate *predicateDocumentsDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataDocumentsDetail setPredicate:predicateDocumentsDetail];
        
        [allDataDocumentsDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorDocumentsDetail = nil;
        NSArray * DataDocumentsDetail = [context executeFetchRequest:allDataDocumentsDetail error:&errorDocumentsDetail];
        //error handling goes here
        for (NSManagedObject * data in DataDocumentsDetail) {
            [context deleteObject:data];
        }
        NSError *saveErrorDocumentsDetail = nil;
        [context save:&saveErrorDocumentsDetail];
        
        
        //Delete ModifyDate Sales Data
        entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
        [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"leadIdd = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
        
        [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesModifyDate = nil;
        NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
        //error handling goes here
        for (NSManagedObject * data in DataSalesModifyDate) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesModifyDate = nil;
        [context save:&saveErrorSalesModifyDate];
        
        //......................................................
        
        //Delete Current Service Data
        entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
        
        NSFetchRequest *allDataCurrentService = [[NSFetchRequest alloc] init];
        [allDataCurrentService setEntity:[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context]];
        NSPredicate *predicateCurrentService =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateCurrentService];
        [allDataCurrentService setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorCurrentService= nil;
        NSArray * DataCurrentService = [context executeFetchRequest:allDataCurrentService error:&errorCurrentService];
        //error handling goes here
        for (NSManagedObject * data in DataCurrentService) {
            [context deleteObject:data];
        }
        NSError *saveErrorCurrentService = nil;
        [context save:&saveErrorCurrentService];
        
        //......................................................
        
        //Nilind 16 Feb
        
        //Delete ServiceFollowUp Data
        
        entityServiceFollowUp=[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataServiceFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ServiceFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceFollowUpDcs =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        
        [allDataServiceFollowUpDcs setPredicate:predicateServiceFollowUpDcs];
        [allDataServiceFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceFollowUpDcs= nil;
        NSArray * DataServiceFollowUpDcs = [context executeFetchRequest:allDataServiceFollowUpDcs error:&errorServiceFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataServiceFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceFollowUpDcs = nil;
        [context save:&saveErrorServiceFollowUpDcs];
        
        //......................................................
        
        
        //Nilind 16 Feb
        
        //Delete ProposalFollowUp Data
        
        entityProposalFollowUp=[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context];
        
        NSFetchRequest *allDataProposalFollowUpDcs = [[NSFetchRequest alloc] init];
        [allDataProposalFollowUpDcs setEntity:[NSEntityDescription entityForName:@"ProposalFollowUpDcs" inManagedObjectContext:context]];
        
        NSPredicate *predicateProposalFollowUpDcs =[NSPredicate predicateWithFormat:@"proposalLeadId = %@",strLeadIDD];
        
        [allDataProposalFollowUpDcs setPredicate:predicateProposalFollowUpDcs];
        [allDataProposalFollowUpDcs setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorProposalFollowUpDcs= nil;
        NSArray * DataProposalFollowUpDcs = [context executeFetchRequest:allDataProposalFollowUpDcs error:&errorProposalFollowUpDcs];
        //error handling goes here
        for (NSManagedObject * data in DataProposalFollowUpDcs) {
            [context deleteObject:data];
        }
        NSError *saveErrorProposalFollowUpDcs = nil;
        [context save:&saveErrorProposalFollowUpDcs];
        
        //......................................................
        
        //Delete SalesDynamicForm Service Data
        entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesDynamic = [[NSFetchRequest alloc] init];
        [allDataSalesDynamic setEntity:[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesDynamic =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
        [allDataSalesDynamic setPredicate:predicateSalesDynamic];
        
        [allDataSalesDynamic setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesDynamic = nil;
        NSArray * DataSalesDynamic = [context executeFetchRequest:allDataSalesDynamic error:&errorSalesDynamic];
        //error handling goes here
        for (NSManagedObject * data in DataSalesDynamic) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesDynamic = nil;
        [context save:&saveErrorSalesDynamic];
        
        //......................................................
        
    }
}

-(void)deleteFromCoreDataServiceInfoExtra{
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrLeadIdToDelete=[defs objectForKey:@"DeleteExtraWorkOrderFromDBService"];
    
    [defs setObject:nil forKey:@"DeleteExtraWorkOrderFromDBService"];
    [defs synchronize];
    
    NSLog(@"Following Are being Deleted====%@",arrLeadIdToDelete);
    
    for (int k=0; k<arrLeadIdToDelete.count; k++) {
        
        NSString *strLeadIDD=[NSString stringWithFormat:@"%@",arrLeadIdToDelete[k]];
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        //  Delete Lead Detail Data
        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
        NSFetchRequest *allData = [[NSFetchRequest alloc] init];
        [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context]];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allData setPredicate:predicate];
        
        [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error = nil;
        NSArray * Data = [context executeFetchRequest:allData error:&error];
        //error handling goes here
        for (NSManagedObject * data in Data) {
            [context deleteObject:data];
        }
        NSError *saveError = nil;
        [context save:&saveError];
        
        //  Delete Email Detail Data
        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
        [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataEmail setPredicate:predicateEmail];
        
        [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1 = nil;
        NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
        //error handling goes here
        for (NSManagedObject * data in DataEmail) {
            [context deleteObject:data];
        }
        NSError *saveError1 = nil;
        [context save:&saveError1];
        
        
        //  Delete Image Detail Data
        entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
        [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataImage setPredicate:predicateImage];
        
        [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error2 = nil;
        NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
        //error handling goes here
        for (NSManagedObject * data in DataImage) {
            [context deleteObject:data];
        }
        NSError *saveError2 = nil;
        [context save:&saveError2];
        
        //  Delete Equipment Detail Data
        entityWOEquipmentServiceAuto=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
        NSFetchRequest *allDataWOEquipment = [[NSFetchRequest alloc] init];
        [allDataWOEquipment setEntity:[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context]];
        
        NSPredicate *predicateWOEquipment =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataWOEquipment setPredicate:predicateWOEquipment];
        
        [allDataWOEquipment setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * error1WOEquipment = nil;
        NSArray * DataWOEquipment = [context executeFetchRequest:allDataWOEquipment error:&error1WOEquipment];
        //error handling goes here
        for (NSManagedObject * data in DataWOEquipment) {
            [context deleteObject:data];
        }
        NSError *saveError1WOEquipment = nil;
        [context save:&saveError1WOEquipment];
        
        
        //  Delete PaymentInfo Detail Data
        entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
        NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
        [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context]];
        
        NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataPaymentInfo setPredicate:predicatePaymentInfo];
        
        [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorPaymentInfo = nil;
        NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
        //error handling goes here
        for (NSManagedObject * data in DataPaymentInfo) {
            [context deleteObject:data];
        }
        NSError *saveErrorPaymentInfo = nil;
        [context save:&saveErrorPaymentInfo];
        
        
        //Delete Chemical List Sales Data
        entityChemicalListDetailServiceAuto=[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceChemical = [[NSFetchRequest alloc] init];
        [allDataServiceChemical setEntity:[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceChemical =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataServiceChemical setPredicate:predicateServiceChemical];
        
        [allDataServiceChemical setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceChemical = nil;
        NSArray * DataServiceChemical = [context executeFetchRequest:allDataServiceChemical error:&errorServiceChemical];
        //error handling goes here
        for (NSManagedObject * data in DataServiceChemical) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceChemical = nil;
        [context save:&saveErrorServiceChemical];
        
        //......................................................
        
        
        
        //Delete ModifyDate Service Data
        entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
        
        NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
        [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context]];
        
        NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
        [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
        
        [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorSalesModifyDate = nil;
        NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
        //error handling goes here
        for (NSManagedObject * data in DataSalesModifyDate) {
            [context deleteObject:data];
        }
        NSError *saveErrorSalesModifyDate = nil;
        [context save:&saveErrorSalesModifyDate];
        
        //......................................................
        
        
        //Delete ServiceDynamicForm Service Data
        entityServiceDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
        
        NSFetchRequest *allDataServiceDynamic = [[NSFetchRequest alloc] init];
        [allDataServiceDynamic setEntity:[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context]];
        
        NSPredicate *predicateServiceDynamic =[NSPredicate predicateWithFormat:@"workOrderId = %@",strLeadIDD];
        [allDataServiceDynamic setPredicate:predicateServiceDynamic];
        
        [allDataServiceDynamic setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError * errorServiceDynamic = nil;
        NSArray * DataServiceDynamic = [context executeFetchRequest:allDataServiceDynamic error:&errorServiceDynamic];
        //error handling goes here
        for (NSManagedObject * data in DataServiceDynamic) {
            [context deleteObject:data];
        }
        NSError *saveErrorServiceDynamic = nil;
        [context save:&saveErrorServiceDynamic];
        
        //......................................................
    }
}

//============================================================================
//============================================================================

#pragma mark- SalesInfo CoreDataFetch

//============================================================================
//============================================================================

-(void)FetchFromCoreDataToShowSalesInfo{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesInfo=[NSEntityDescription entityForName:@"SalesAutomationAppoint" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"companyKey = %@",strCompanyKey];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        matches=arrAllObj[0];
        
        NSDictionary *dictList = [matches valueForKey:@"appointResponseDict"];
        NSLog(@"%@",dictList);
        
        
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}


//============================================================================
//============================================================================

#pragma mark- Merging Sales And Service Work Orders

//============================================================================
//============================================================================


-(void)fetchSalesAndServiceWorkOrdersFromCoreData{
    
    arrOfTotalWorkerOrder=[[NSMutableArray alloc]init];
    
    //-----------------*****************-----------------Service WALA--------------****************************
    
    //============================================================================
    //============================================================================
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitytotalWorkOrders=[NSEntityDescription entityForName:@"ServiceAutomation" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitytotalWorkOrders];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userEmailId = %@",[dictData valueForKey:@"Email"]];
    //    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"empId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerTotalWorkOrders = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerTotalWorkOrders setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerTotalWorkOrders performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerTotalWorkOrders fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        
        matches = arrAllObj[0];
        NSDictionary *dictOfWorkOrderss=[matches valueForKey:@"dictOfWorkOrders"];
        arrOfTotalWorkerOrder=[dictOfWorkOrderss valueForKey:@"WorkOrderExtSerDcs"];
        
        NSMutableArray *arrtempWorkOrder=[[NSMutableArray alloc]init];
        
        for (int k=0; k<arrOfTotalWorkerOrder.count; k++) {
            
            NSDictionary *dictOfWorkOrderExtSerDcs =arrOfTotalWorkerOrder[k];
            NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
            NSString *strDateToConvert=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleEndDateTime"]];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* EndDate = [dateFormatter dateFromString:strDateToConvert];
            
            NSComparisonResult result = [[NSDate date] compare:EndDate];
            if(result == NSOrderedDescending)
            {
                NSLog(@"strLocal is later than Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            else if(result == NSOrderedAscending)
            {
                NSLog(@"Current is later than strLocal");
            }
            else
            {
                NSLog(@"strLocal is equal to Current");
                [arrtempWorkOrder addObject:arrOfTotalWorkerOrder[k]];
            }
            
        }
        
        //  arrOfTotalWorkerOrder=arrtempWorkOrder;
        
        [arrOfTotalWorkerOrder addObjectsFromArray:arrtempWorkOrder];
        
        if (!(arrOfTotalWorkerOrder.count==0)) {
            
            countForServiceOrder=arrOfTotalWorkerOrder.count;
            
        }
        
        
        // [_tblViewAppointment reloadData];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    
    
    //-----------------*****************-----------------Sales WALA--------------****************************
    
    //============================================================================
    //============================================================================
    
    /*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     entitySalesInfo=[NSEntityDescription entityForName:@"SalesAutomationAppoint" inManagedObjectContext:context];
     requestNew = [[NSFetchRequest alloc] init];
     [requestNew setEntity:entitySalesInfo];
     NSPredicate *predicate =[NSPredicate predicateWithFormat:@"companyKey = %@",strCompanyKey];*/
    // entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    //   NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",@"85"];
    
    //  [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if ([arrAllObj count] == 0)
    {
        
    }
    else
    {
        // NSArray *arrOfTotalWorkOrderService;
        
        // matches=arrAllObj[0];
        
        NSLog(@"Matches%@",matches);
        //
        for (int i=0; i<arrAllObj.count; i++)
        {
            matches=arrAllObj[i];
            NSLog(@"sadfhadsjkfhasdjkf djfhgajsdf %@",[matches valueForKey:@"leadId"]);
        }
        
        NSArray *result = [context executeFetchRequest:requestNew error:&error];
        
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
        } else {
            NSLog(@"%@", result);
        }
        
        //
        //        arrOfTotalWorkOrderService=[dictOfWorkOrderssSales valueForKey:@"LeadExtSerDcs"];
        //
        //        NSMutableArray *arrtempWorkOrder=[[NSMutableArray alloc]init];
        //
        //        for (int k=0; k<arrOfTotalWorkOrderService.count; k++)
        //        {
        //
        //            NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkOrderService objectAtIndex:k];
        //            NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"LeadDetail"];
        //            NSString *strDateToConvert=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"ScheduleEndDateTime"]];
        //            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        //            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        //            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //            NSDate* EndDate = [dateFormatter dateFromString:strDateToConvert];
        //
        //            NSComparisonResult result = [[NSDate date] compare:EndDate];
        //            if(result == NSOrderedDescending)
        //            {
        //                NSLog(@"strLocal is later than Current");
        //                [arrtempWorkOrder addObject:arrOfTotalWorkOrderService[k]];
        //            }
        //            else if(result == NSOrderedAscending)
        //            {
        //                NSLog(@"Current is later than strLocal");
        //            }
        //            else
        //            {
        //                NSLog(@"strLocal is equal to Current");
        //                [arrtempWorkOrder addObject:arrOfTotalWorkOrderService[k]];
        //            }
        //
        //        }
        //        //arrOfTotalWorkerOrder=arrtempWorkOrder;
        //
        //        [arrOfTotalWorkerOrder addObjectsFromArray:arrtempWorkOrder];
        //
        [_tblViewAppointment reloadData];
        
    }
    
    
    if (error)
    {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else
    {
    }
}
#pragma mark -SalesAuto Fetch Core Data

-(void)salesFetch
{
    
    if (isDeletedFirstTime) {
        isDeletedFirstTime=NO;
        [self deleteFromCoreDataSalesInfoExtra];
        [self deleteFromCoreDataServiceInfoExtra];
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo_Mobile=%@",strEmployeeNo];
    //[NSPredicate predicateWithFormat:@"typeOfActivity = %@ AND userName = %@",strTypeOfActivityToFetch,strUsername];
    [requestNew setPredicate:predicate];
    
    //    // Wapis se phle jaisa krna hai jaisa tha mtlb schedule dtae se krna hai sorting jo ki modified date se kr di thi
    //
    //    if (isTodayAppointments) {
    //
    //        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    //
    //    } else {
    //        //@"dateModified"
    //        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
    //
    //    }
    
    
    // fetching from settings
    
    if (isTodayAppointments) {
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
        
    } else {
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
        
        if (isSortByScheduleDate) {
            
            BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
            
            if (isSortByScheduleDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
                
            }
            
        } else {
            
            BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
            
            if (isSortByModifiedDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
                
            }
            
        }
        
    }
    
    //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    
    //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        if (isTodayAppointments) {
            
            NSMutableArray *arrTodayDates=[[NSMutableArray alloc]init];
            
            NSMutableArray *arrComplete=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
                
                NSString *strStage=[NSString stringWithFormat:@"%@",[matches valueForKey:@"stageSysName"]];
                BOOL chkProposal=NO;
                if ([strStatusComplete caseInsensitiveCompare:@"Open"] == NSOrderedSame && [strStage caseInsensitiveCompare:@"Proposed"] == NSOrderedSame)
                {
                    chkProposal=YES;
                }
                
                if ([strStatusComplete caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Completed"] == NSOrderedSame || chkProposal) {
                    
                    [arrComplete addObject:arrAllObj[k]];
                    
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:[matches valueForKey:@"scheduleStartDate"]];
                
                // NSComparisonResult result = [[NSDate date] compare:dateFromString];
                
                BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
                
                if (yesSameDay) {
                    
                    [arrTodayDates addObject:arrAllObj[k]];
                    
                    NSLog(@"Yes Today date=====%@",[matches valueForKey:@"scheduleStartDate"]);
                    
                }
            }
            if (!(arrComplete.count==0)) {
                
                [arrTodayDates removeObjectsInArray:arrComplete];
                
            }
            arrAllObj=arrTodayDates;
        }
        
        
        // Settings View Change
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isAllAppoitments = [defsApp boolForKey:@"AllAppointments"];
        
        if ((!isTodayAppointments) && (!isAllAppoitments)) {
            
            NSMutableArray *arrSettingStatus=[[NSMutableArray alloc]init];
            
            NSUserDefaults *defsAppp =[NSUserDefaults standardUserDefaults];
            
            NSArray *tempArr = [defsAppp objectForKey:@"AppointmentStatus"];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"statusSysName"]];
                
                //                if ([tempArr containsObject:strStatusComplete]) {
                //
                //                    [arrSettingStatus addObject:arrAllObj[k]];
                //
                //                }
                
                for (int k1=0; k1<tempArr.count; k1++) {
                    
                    NSString *statusLocal = tempArr[k1];
                    
                    if ([statusLocal caseInsensitiveCompare:strStatusComplete] ==NSOrderedSame) {
                        
                        [arrSettingStatus addObject:arrAllObj[k]];
                        
                        break;
                    }
                    
                }
                
            }
            
            if (arrSettingStatus.count>0) {
                
                arrAllObj=arrSettingStatus;
                
            }else{
                
                arrAllObj = nil;
                
            }
        }
    }
    
    [DejalBezelActivityView removeViewAnimated:NO];
    
    // [_tblViewAppointment reloadData];
    
    [self ServiceAutomationFetch];
    
    
}


-(void)ServiceAutomationFetch
{
    
    NSMutableArray *arrTempAllObj=[[NSMutableArray alloc]init];
    [arrTempAllObj addObjectsFromArray:arrAllObj];
    
    arrAllObj=nil;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"dbUserName=%@",strEmployeeNo];
    
    [requestNewService setPredicate:predicate];
    
    //    // Wapis se phle jaisa krna hai jaisa tha mtlb schedule dtae se krna hai sorting jo ki modified date se kr di thi
    //
    //    if (isTodayAppointments) {
    //
    //        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    //
    //    } else {
    //        //@"dateModified"
    //        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
    //
    //    }
    
    
    // fetching from settings
    
    if (isTodayAppointments) {
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
        
    } else {
        
        NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
        
        BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
        
        if (isSortByScheduleDate) {
            
            BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
            
            if (isSortByScheduleDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
                
            }
            
        } else {
            
            BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
            
            if (isSortByModifiedDateAscending) {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
                
            } else {
                
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
                
            }
            
        }
        
    }
    
    
    //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    
    //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
    
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrAllObj.count==0)
    {
        
    }else
    {
        
        if (isTodayAppointments) {
            
            NSMutableArray *arrTodayDates=[[NSMutableArray alloc]init];
            
            NSMutableArray *arrComplete=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrAllObj.count; k++) {
                
                matches=arrAllObj[k];
                
                NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderStatus"]];
                if ([strStatusComplete caseInsensitiveCompare:@"Complete"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Completed"] == NSOrderedSame || [strStatusComplete caseInsensitiveCompare:@"Reset"] == NSOrderedSame) {
                    
                    [arrComplete addObject:arrAllObj[k]];
                    
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:[matches valueForKey:@"scheduleStartDateTime"]];
                
                // NSComparisonResult result = [[NSDate date] compare:dateFromString];
                
                BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
                
                if (yesSameDay) {
                    
                    [arrTodayDates addObject:arrAllObj[k]];
                    
                    NSLog(@"Yes Today date=====%@",[matches valueForKey:@"scheduleStartDateTime"]);
                    
                }
            }
            
            if (!(arrComplete.count==0)) {
                
                [arrTodayDates removeObjectsInArray:arrComplete];
                
            }
            
            arrAllObj=arrTodayDates;
        }
        
    }
    
    //[arrTempAllObj addObjectsFromArray:arrAllObj];
    
    // Settings View Change
    
    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isAllAppoitments = [defsApp boolForKey:@"AllAppointments"];
    
    if ((!isTodayAppointments) && (!isAllAppoitments)) {
        
        NSMutableArray *arrSettingStatus=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defsAppp =[NSUserDefaults standardUserDefaults];
        
        NSArray *tempArr = [defsAppp objectForKey:@"AppointmentStatus"];
        
        for (int k=0; k<arrAllObj.count; k++) {
            
            matches=arrAllObj[k];
            
            NSString *strStatusComplete=[NSString stringWithFormat:@"%@",[matches valueForKey:@"workorderStatus"]];
            
            //            if ([tempArr containsObject:strStatusComplete]) {
            //
            //                [arrSettingStatus addObject:arrTempAllObj[k]];
            //
            //            }
            
            for (int k1=0; k1<tempArr.count; k1++) {
                
                NSString *statusLocal = tempArr[k1];
                
                if ([strStatusComplete caseInsensitiveCompare:@"Completed"] ==NSOrderedSame) {
                    
                    strStatusComplete=@"Complete";
                    
                }
                
                if ([statusLocal caseInsensitiveCompare:strStatusComplete] ==NSOrderedSame) {
                    
                    [arrSettingStatus addObject:arrAllObj[k]];
                    
                    break;
                }
                
            }
            
        }
//        arrTempAllObj = nil;
//        arrTempAllObj = [[NSMutableArray alloc]init];
        if (arrSettingStatus.count>0) {
            
            [arrTempAllObj addObjectsFromArray:arrSettingStatus];
            
        }
    }else{
        
        [arrTempAllObj addObjectsFromArray:arrAllObj];
        
    }
    
    
    arrAllObj=arrTempAllObj;
    
    [DejalBezelActivityView removeViewAnimated:YES];
    
    //[_tblViewAppointment reloadData];
    
    // [self salesFetchCountLeads];
    
    // [global getUnsignedDocumentsWoNos:strServiceUrlMainServiceAutomation :strCompanyKeyy];
    
    BOOL isNetThr=[global isNetReachable];
    
    if (isNetThr) {
        
        [global getUnsignedDocumentsWoNos:strServiceUrlMainServiceAutomation :strCompanyKeyy];
        
    }
    
    [self filterAllAppointmentsIncludingBlockTime];
    
}

//============================================================================
//============================================================================
#pragma mark- ------------Same Day Find Karna METHODS--------------
//============================================================================
//============================================================================

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


//============================================================================
#pragma mark- ResendMail Service
//============================================================================

-(void)resendMailService:(id)sender
{
    
    NSString *strCCustomerNotPresent;
    
    UIButton *btn = (UIButton *)sender;
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setObject:nil forKey:@"salesDynamicForm"];
    [defss synchronize];
    
    if (yesFiltered) {
        
        matches=filteredArray[btn.tag];
        
        if ([matches isKindOfClass:[LeadDetail class]]) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
            [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
            [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
            [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            [defs synchronize];
            
        }else{
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            //departmentSysName
            [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
            [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
            [defs synchronize];
            
            
            strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
        }
        
    }else{
        
        matches=arrAllObj[btn.tag];
        if ([matches isKindOfClass:[LeadDetail class]]) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
            [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
            [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
            [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            
            [defs synchronize];
            
        }
        else{
            
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
            [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
            [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            
            [defs synchronize];
            
            strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
            
        }
    }
    
    if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
        
        [self goToSendMailService:@"yes"];
        
    }else{
        
        [self goToSendMailService:@"no"];
        
    }
    
}

-(void)goToSendMailService :(NSString*)strCustomerPresent{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Service_iPhone" bundle:nil];
    ServiceSendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"ServiceSendMailViewController"];
    objSendMail.isCustomerPresent=strCustomerPresent;
    objSendMail.fromWhere=@"Appointment";
    [self.navigationController pushViewController:objSendMail animated:NO];
    
}

#pragma mark- RESEND EMAIL FOR SALES

-(void)resendMailSales:(id)sender
{
    
    NSString *strCCustomerNotPresent;
    
    UIButton *btn = (UIButton *)sender;
    
    NSUserDefaults *defss=[NSUserDefaults standardUserDefaults];
    [defss setObject:nil forKey:@"salesDynamicForm"];
    [defss synchronize];
    
    if (yesFiltered) {
        
        matches=filteredArray[btn.tag];
        
        if ([matches isKindOfClass:[LeadDetail class]]) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
            [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
            [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
            [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            [defs synchronize];
            
            
            if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
                
                [self goToSendMailSales:@"yes"];
                
            }else{
                
                [self goToSendMailSales:@"no"];
                
            }
            
        }else{
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            //departmentSysName
            [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
            [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
            [defs synchronize];
            
            
            strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
            
            if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
                
                [self goToSendMailService:@"yes"];
                
            }else{
                
                [self goToSendMailService:@"no"];
                
            }
            
        }
        
    }else{
        
        matches=arrAllObj[btn.tag];
        if ([matches isKindOfClass:[LeadDetail class]]) {
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"leadId"] forKey:@"LeadId"];
            [defs setValue:[matches valueForKey:@"accountNo"] forKey:@"AccountNos"];
            [defs setValue:[matches valueForKey:@"LeadNumber"] forKey:@"LeadNumber"];
            [defs setValue:[matches valueForKey:@"statusSysName"] forKey:@"leadStatusSales"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            
            [defs synchronize];
            
            if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
                
                [self goToSendMailSales:@"yes"];
                
            }else{
                
                [self goToSendMailSales:@"no"];
                
            }
            
        }
        else{
            
            
            NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
            [defs setValue:[matches valueForKey:@"workorderId"] forKey:@"LeadId"];
            NSString *strSurveID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"surveyID"]];
            NSString *strDepartmentID= [NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentId"]];
            [defs setValue:[NSString stringWithFormat:@"%@",[matches valueForKey:@"departmentSysName"]] forKey:@"departmentSysNameWO"];
            [defs setValue:strDepartmentID forKey:@"strDepartmentId"];
            [defs setValue:strSurveID forKey:@"SurveyID"];
            if (([strIsGGQIntegration isEqualToString:@"1"]) && (strSurveID.length>0)) {
                
                [defs setBool:YES forKey:@"YesSurveyService"];
                
            }else{
                
                [defs setBool:NO forKey:@"YesSurveyService"];
                
            }
            
            [defs synchronize];
            
            strCCustomerNotPresent=[NSString stringWithFormat:@"%@",[matches valueForKey:@"isCustomerNotPresent"]];
            
            
            if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
                
                [self goToSendMailService:@"yes"];
                
            }else{
                
                [self goToSendMailService:@"no"];
                
            }
            
        }
    }
    
//    if ([strCCustomerNotPresent isEqualToString:@"true"] || [strCCustomerNotPresent isEqualToString:@"1"]) {
//
//        [self goToSendMailSales:@"yes"];
//
//    }else{
//
//        [self goToSendMailSales:@"no"];
//
//    }
    
}
-(void)goToSendMailSales :(NSString*)strCustomerPresent{
    
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    SendMailViewController *objSendMail=[storyBoard instantiateViewControllerWithIdentifier:@"SendMailViewController"];
    objSendMail.isCustomerPresent=strCustomerPresent;
    objSendMail.strFromWhere=@"Appointment";
    [self.navigationController pushViewController:objSendMail animated:NO];
    
}


//============================================================================
#pragma mark- 5 Sept Nilind
//============================================================================
#pragma mark- ----------SALES Methods EMAIL, MESSAGE, MAP------------
//============================================================================
#pragma mark- METHODS Button Actions
//============================================================================


-(void)sendSMSForSales:(id)sender
{
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Confirm?"
                               message:@"Do you want to send Text"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                          {
                              
                          }];
    [alert addAction:yes];
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                         {
                             
                             UIButton *btn = (UIButton *)sender;
                             if (yesFiltered) {
                                 matches=filteredArray[btn.tag];
                             }else{
                                 matches=arrAllObj[btn.tag];
                             }
                             
                             // For Sending Status in LifeStyle
                             
                             strWorkOrderNoLifeStyle =[NSString stringWithFormat:@"%@",[matches valueForKey:@"workOrderNo"]];
                             
                             NSString *strPhoneNo;
                             strPhoneNo=[matches valueForKey:@"primaryPhone"];
                             NSArray *recipients = [NSArray arrayWithObjects:strPhoneNo, nil];
                             if (strPhoneNo.length==0) {
                                 
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Number not available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [alert show];
                                 
                             } else {
                                 if ([matches isKindOfClass:[LeadDetail class]])
                                 {
                                     strTypeOfAppointment=@"sales";
                                     strWorkOrderIdOnMyWaySentSMS=[matches valueForKey:@"leadId"];
                                     [self sendNumberForSales:recipients :matches];
                                     
                                 }else{
                                     strTypeOfAppointment=@"service";
                                     
                                     strWorkOrderIdOnMyWaySentSMS=[matches valueForKey:@"workorderId"];
                                     [self sendNumber:recipients :matches];
                                     
                                 }
                                 
                             }
                             //   if (!(countForServiceOrder==-1))
                             //                             {
                             //
                             //                                 if (btn.tag<countForServiceOrder)
                             //                                 {
                             //
                             //                                     NSDictionary *dictOfWorkOrderExtSerDcs =[arrOfTotalWorkerOrder objectAtIndex:btn.tag];
                             //                                     NSDictionary *dictofWorkorderDetail=[dictOfWorkOrderExtSerDcs valueForKey:@"WorkorderDetail"];
                             //                                     NSString *strNumber=[NSString stringWithFormat:@"%@",[dictofWorkorderDetail valueForKey:@"PrimaryPhone"]];
                             //
                             //                                     NSArray *recipients = [NSArray arrayWithObjects:strNumber, nil];
                             //
                             //                                     [self sendNumber:recipients :dictofWorkorderDetail];
                             //                                 }
                             //                                 else{
                             //
                             //                                     //Sales Wala YAha Karna hai
                             //                                     matches=arrAllObj[btn.tag];
                             //                                     NSDictionary *dictMatches=(NSDictionary*)matches;
                             //                                     NSString *strPhoneNo;
                             //                                     strPhoneNo=[matches valueForKey:@"PrimaryPhone"];
                             //                                     NSArray *recipients = [NSArray arrayWithObjects:strPhoneNo, nil];
                             //                                     [self sendNumber:recipients :dictMatches];
                             //                                 }
                             //                             }
                         }];
    [alert addAction:no];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)sendEmailForSales:(id)sender
{
    
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Confirm?"
                                   message:@"Do you want to send Email"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yes = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  
                              }];
        [alert addAction:yes];
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                                 [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending Email..."];
                                 
                                 UIButton *btn = (UIButton *)sender;
                                 
                                 if (yesFiltered) {
                                     matches=filteredArray[btn.tag];
                                 }else
                                 {
                                     matches=arrAllObj[btn.tag];
                                 }
                                 
                                 // For Sending Status in LifeStyle
                                 
                                 strWorkOrderNoLifeStyle =[NSString stringWithFormat:@"%@",[matches valueForKey:@"workOrderNo"]];
                                 
                                 [self updatingStatusOnMyWayLifeStyle];
                                 
                                 NSString *strEmailKey;
                                 strEmailKey=@"remindersms";
                                 NSString *strUrl=[NSString stringWithFormat:@"%@%@&CompanyKey=%@&aId=%@",UrlSalesEmail,strEmailKey,strGGQCompanyKey,[matches valueForKey:@"surveyID"]];
                                 NSLog(@"Email url >>%@",strUrl);
                                 //http://www.gogetquality.com/survey/GoHandler.ashx?key=remindersms&companyKey=agt&aId=83
                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                     
                                     [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SendEmailServiceAutomation" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [DejalBezelActivityView removeView];
                                              if (success)
                                              {
                                                  NSString *returnString =[response valueForKey:@"ReturnMsg"];
                                                  if ([returnString isEqualToString:@"Successful"]) {
                                                      [global AlertMethod:Info :SuccessMailSend];
                                                  } else {
                                                      [global AlertMethod:Info :SuccessMailSend];
                                                  }
                                              }
                                              else
                                              {
                                                  NSString *strTitle = Alert;
                                                  NSString *strMsg = Sorry;
                                                  [global AlertMethod:strTitle :strMsg];
                                              }
                                          });
                                      }];
                                 });
                                 
                                 //..................................................................
                                 
                             }];
        [alert addAction:no];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}
-(void)sendToMapViewForSales:(id)sender
{
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
        
    }
    else
    {
        
        UIButton *btn = (UIButton *)sender;
        NSString *strLat,*strLong;
        if (yesFiltered) {
            matches=filteredArray[btn.tag];
        }else{
            matches=arrAllObj[btn.tag];
        }
        if ([matches isKindOfClass:[LeadDetail class]]) {
            strLat=[matches valueForKey:@"latitude"];
            strLong=[matches valueForKey:@"longitude"];
        }else{
            strLat=[matches valueForKey:@"serviceAddressLatitude"];
            strLong=[matches valueForKey:@"serviceAddressLongitude"];
            
        }
        if (strLat.length==0 && strLong.length==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No location coordinates found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            
            //http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f
            
            NSString *url;//=@"https://maps.google.com/maps?f=q&t=m&q=22.730287,75.8763365";
            //url=[NSString stringWithFormat:@"https://maps.google.com/maps?f=q&t=m&q=%@,%@",strLat,strLong];
            
            url=[NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%@,%@",strLat,strLong];
            
            NSURL *URL = [NSURL URLWithString:url];
            
            if (![[UIApplication sharedApplication] canOpenURL:URL]) {
                
                url=[NSString stringWithFormat:@"https://maps.google.com/maps?daddr=%@,%@",strLat,strLong];
                
                NSURL *URL = [NSURL URLWithString:url];
                
                if (URL) {
                    if ([SFSafariViewController class] != nil) {
                        SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
                        sfvc.delegate = (id)self;
                        [self presentViewController:sfvc animated:YES completion:nil];
                    } else {
                        if (![[UIApplication sharedApplication] openURL:URL]) {
                        }
                    }
                } else {
                    // will have a nice alert displaying soon.
                }
            }else{
                
                [[UIApplication sharedApplication] openURL:URL];
                
            }
        }
    }
}

//============================================================================
//============================================================================
#pragma mark- ---------------------------Service Automation @29 SEP By Saavan Patidar iOS Developer--------------------------------
//============================================================================
//============================================================================

-(void)downloadTotalWorkOrdersServiceAutomation{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlGetTotalWorkOrdersServiceAutomation,strCompanyKeyy,UrlGetTotalWorkOrdersServiceAutomationEmployeeNo,strEmployeeNumber];
    
    NSLog(@"Service Work Order URl-----%@",strUrl);
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Fetching Service Appointments..."];
    
    //============================================================================
    //============================================================================
    
    NSString *strType=@"TotalWorkOrderServiceAutomation";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForUrl:strUrl :strType withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     dictForWorkOrderAppointment=response;
                     NSString *strException;
                     @try {
                         if([dictForWorkOrderAppointment objectForKey:@"ExceptionMessage"] != nil) {
                             // The key existed...
                             strException=[dictForWorkOrderAppointment valueForKey:@"ExceptionMessage"];
                             
                         }else{
                             
                         }
                     } @catch (NSException *exception) {
                         
                     } @finally {
                         
                     }
                     if (strException.length==0) {
                         
                         [self checkifToSendServiceAppointmentToServer];
                         
                         //
#pragma mark- NILIND TEMP CHANGE
                         if (_tblViewAppointment.hidden==YES)
                         {
                             isTodayAppointments=YES;
                             [_lblLineAll setHidden:YES];
                             [_lblLineToday setHidden:NO];
                             yesFiltered=NO;
                             [_searchBarr resignFirstResponder];
                             _searchBarr.hidden=YES;
                             yesFiltered=NO;
                             [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
                             // [_tblViewAppointment reloadData];
                             
                             [self salesFetch];
                         }
                         
                         //End
                         
                         
                         
                     } else {
                         
                         
                         //UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Unable to fetch data from server.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //[alert show];
                         [DejalBezelActivityView removeView];
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    //============================================================================
    //============================================================================
    
}

-(void)saveToCoreDataServiceAutomation :(NSDictionary*)dictWorkOrderServiceAppointment :(NSString*)saveAll{
    
    NSMutableArray *arrLeadIdToSaveAlll=[[NSMutableArray alloc]init];
    NSString *strLeadIdToCompare;
    
    if ([saveAll isEqualToString:@"NilHai"]) {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSMutableArray *arrLeadIdToSaveAnyHow=[defs objectForKey:@"saveTolocalDbAnyHowService"];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSaveAnyHow];
        
    } else {
        
        //==================================================================================
        //==================================================================================
        [self deleteFromCoreDataServiceInfo];
        //==================================================================================
        //==================================================================================
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        NSMutableArray *arrLeadIdToSave=[defs objectForKey:@"saveServiceLeadToLocalDbnDelete"];
        NSMutableArray *arrLeadIdToSaveAnyHow=[defs objectForKey:@"saveTolocalDbAnyHowService"];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSave];
        [arrLeadIdToSaveAlll addObjectsFromArray:arrLeadIdToSaveAnyHow];
        
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    
    for (int k=0; k<arrLeadIdToSaveAlll.count; k++) {
        
        strLeadIdToCompare=[NSString stringWithFormat:@"%@",arrLeadIdToSaveAlll[k]];
        
        
        NSArray *arrWorkOrderExtSerDcs=[dictWorkOrderServiceAppointment valueForKey:@"WorkOrderExtSerDcs"];
        
        for (int i=0; i<arrWorkOrderExtSerDcs.count; i++)
        {
            //============================================================================
            //============================================================================
#pragma mark- ---------******** Work Order Detail *******----------------
            //============================================================================
            //============================================================================
            
            NSDictionary *dictWorkOrderDetail=arrWorkOrderExtSerDcs[i];
            
            NSDictionary *dictDataWorkOrders=[dictWorkOrderDetail valueForKey:@"WorkorderDetail"];
            
            NSString *strLeadIddd;
            if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
            {
                strLeadIddd=@"";
            }
            else
            {
                
                strLeadIddd=[NSString stringWithFormat:@"%@",[dictWorkOrderDetail valueForKeyPath:@"WorkorderDetail.WorkorderId"]];
                
            }
            
            if ([strLeadIddd isEqualToString:strLeadIdToCompare]) {
                
                // save service pest new flow data in DB
                
                WebService *objWebService = [[WebService alloc] init];
                
                [objWebService savingServicePestAllDataDBWithDictWorkOrderDetail:dictWorkOrderDetail strWoId:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]] strType:@"WithWoDetail" strToDeleteAreasDevices:@"No" ];
                
                if(![[dictWorkOrderDetail valueForKey:@"WorkorderDetail"] isKindOfClass:[NSDictionary class]])
                {
                    strGlobalWorkOrderId=@"";
                }
                else
                {
                    
                    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
                    WorkOrderDetailsService *objWorkOrderDetailsService = [[WorkOrderDetailsService alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
                    
                    
                    objWorkOrderDetailsService.workorderId =[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                    
                    strGlobalWorkOrderId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderId"]];
                    
                    objWorkOrderDetailsService.workOrderNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkOrderNo"]];
                    
                    objWorkOrderDetailsService.companyId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyId"]];
                    
                    objWorkOrderDetailsService.surveyID=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyID"]];
                    
                    objWorkOrderDetailsService.departmentId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentId"]];
                    
                    objWorkOrderDetailsService.accountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountNo"]];
                    objWorkOrderDetailsService.thirdPartyAccountNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ThirdPartyAccountNo"]];

                    
                    objWorkOrderDetailsService.employeeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EmployeeNo"]];
                    
                    objWorkOrderDetailsService.firstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"FirstName"]];
                    
                    objWorkOrderDetailsService.middleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"MiddleName"]];
                    
                    objWorkOrderDetailsService.lastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastName"]];
                    
                    objWorkOrderDetailsService.companyName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CompanyName"]];
                    
                    NSString *strEmailPrimaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryEmail"]];
                    
                    if (strEmailPrimaryServer.length==0) {
                        
                        objWorkOrderDetailsService.primaryEmail=@"";
                        
                    } else {
                        
                        objWorkOrderDetailsService.primaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"PrimaryEmail"]]];
                        
                    }
                    
                    
                    NSString *strEmailSecondaryServer=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryEmail"]];
                    
                    if (strEmailSecondaryServer.length==0) {
                        
                        objWorkOrderDetailsService.secondaryEmail=@"";
                        
                    } else {
                        
                        objWorkOrderDetailsService.secondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"SecondaryEmail"]]];
                        
                    }
                    
                    
                    objWorkOrderDetailsService.primaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryPhone"]];
                    
                    objWorkOrderDetailsService.secondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SecondaryPhone"]];
                    
                    objWorkOrderDetailsService.scheduleOnStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
                    
                    objWorkOrderDetailsService.scheduleOnEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
                    
                    objWorkOrderDetailsService.scheduleStartDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleStartDateTime"]];
                    
                    objWorkOrderDetailsService.scheduleEndDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleEndDateTime"]];
                    
                    objWorkOrderDetailsService.serviceDateTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDateTime"]];
                    
                    objWorkOrderDetailsService.totalEstimationTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalEstimationTime"]];
                    
                    objWorkOrderDetailsService.branchId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BranchId"]];
                    
                    objWorkOrderDetailsService.workorderStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"WorkorderStatus"]];
                    
                    objWorkOrderDetailsService.atributes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Attributes"]];
                    
                    objWorkOrderDetailsService.specialInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SpecialInstruction"]];
                    
                    objWorkOrderDetailsService.serviceInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceInstruction"]];
                    
                    objWorkOrderDetailsService.direction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Direction"]];
                    
                    objWorkOrderDetailsService.otherInstruction=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OtherInstruction"]];
                    
                    objWorkOrderDetailsService.categoryName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategoryName"]];
                    
                    objWorkOrderDetailsService.subCategory=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SubCategory"]];
                    
                    objWorkOrderDetailsService.services=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Services"]];
                    
                    objWorkOrderDetailsService.currentServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CurrentServices"]];
                    
                    objWorkOrderDetailsService.lastServices=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LastServices"]];
                    
                    objWorkOrderDetailsService.technicianComment=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianComment"]];
                    
                    objWorkOrderDetailsService.officeNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OfficeNotes"]];
                    
                    objWorkOrderDetailsService.timeIn=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeIn"]];
                    
                    objWorkOrderDetailsService.timeOut=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOut"]];
                    
                    objWorkOrderDetailsService.resetId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetId"]];
                    
                    objWorkOrderDetailsService.resetDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ResetDescription"]];
                    
                    objWorkOrderDetailsService.audioFilePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AudioFilePath"]];
                    
                    objWorkOrderDetailsService.invoiceAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"InvoiceAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoiceAmount"]];
                    
                    objWorkOrderDetailsService.productionAmount=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"ProductionAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProductionAmount"]];
                    
                    objWorkOrderDetailsService.previousBalance=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"PreviousBalance"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PreviousBalance"]];
                    
                    objWorkOrderDetailsService.tax=[NSString stringWithFormat:@"%.02f", [[dictDataWorkOrders valueForKey:@"Tax"] floatValue]];//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Tax"]];
                    
                    objWorkOrderDetailsService.customerSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CustomerSignaturePath"]];
                    
                    objWorkOrderDetailsService.technicianSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TechnicianSignaturePath"]];
                    
                    objWorkOrderDetailsService.electronicSignaturePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ElectronicSignaturePath"]];
                    
                    objWorkOrderDetailsService.invoicePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"InvoicePath"]];
                    
                    objWorkOrderDetailsService.billingAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress1"]];
                    
                    objWorkOrderDetailsService.billingAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingAddress2"]];
                    
                    objWorkOrderDetailsService.billingCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCountry"]];
                    
                    objWorkOrderDetailsService.billingState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingState"]];
                    
                    objWorkOrderDetailsService.billingCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCity"]];
                    
                    objWorkOrderDetailsService.billingZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingZipcode"]];
                    
                    objWorkOrderDetailsService.servicesAddress1=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicesAddress1"]];
                    
                    objWorkOrderDetailsService.serviceAddress2=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddress2"]];
                    
                    objWorkOrderDetailsService.serviceCountry=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCountry"]];
                    
                    objWorkOrderDetailsService.serviceState=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceState"]];
                    
                    objWorkOrderDetailsService.serviceCity=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCity"]];
                    
                    objWorkOrderDetailsService.serviceZipcode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceZipcode"]];
                    
                    objWorkOrderDetailsService.serviceAddressLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLatitude"]];
                    
                    objWorkOrderDetailsService.serviceAddressLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressLongitude"]];
                    
                    objWorkOrderDetailsService.isPDFGenerated=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsPDFGenerated"]];
                    
                    objWorkOrderDetailsService.isMailSent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsMailSent"]];
                    
                    objWorkOrderDetailsService.isElectronicSignatureAvailable=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsElectronicSignatureAvailable"]];
                    
                    objWorkOrderDetailsService.isFail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsFail"]];
                    
                    objWorkOrderDetailsService.operateMedium=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OperateMedium"]];
                    
                    objWorkOrderDetailsService.isActive=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsActive"]];
                    
                    objWorkOrderDetailsService.surveyStatus=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"SurveyStatus"]];
                    
                    objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                    
                    objWorkOrderDetailsService.createdBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CreatedBy"]];
                    
                    objWorkOrderDetailsService.modifiedFormatedDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                    
                    objWorkOrderDetailsService.modifiedBy=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedBy"]];
                    
                    objWorkOrderDetailsService.userName=strUserName;
                    objWorkOrderDetailsService.onMyWaySentSMSTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTime"]];
                    objWorkOrderDetailsService.routeNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteNo"]];
                    objWorkOrderDetailsService.routeName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RouteName"]];
                    objWorkOrderDetailsService.targets=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"Targets"]];
                    
                    objWorkOrderDetailsService.deviceVersion=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceVersion"]];
                    objWorkOrderDetailsService.deviceName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DeviceName"]];
                    objWorkOrderDetailsService.versionNumber=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionNumber"]];
                    objWorkOrderDetailsService.versionDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"VersionDate"]];
                    
                    NSDateFormatter* dateFormatterSchedule = [[NSDateFormatter alloc] init];
                    [dateFormatterSchedule setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatterSchedule setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTimeSchedule = [dateFormatterSchedule dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ScheduleStartDateTime"]]];
                    objWorkOrderDetailsService.zdateScheduledStart=newTimeSchedule;
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss'"];
                    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
                    NSDate* newTime = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKeyPath:@"ModifiedFormatedDate"]]];
                    objWorkOrderDetailsService.dateModified=newTime;
                    
                    objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCustomerNotPresent"]];
                    
                    objWorkOrderDetailsService.totalallowCrew=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TotalallowCrew"]];
                    
                    objWorkOrderDetailsService.assignCrewIds=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AssignCrewIds"]];
                    
                    objWorkOrderDetailsService.routeCrewList=[dictDataWorkOrders valueForKey:@"RouteCrewList"];
                    
                    objWorkOrderDetailsService.zSync=@"blank";
                    
                    // objWorkOrderDetailsService.isCustomerNotPresent=[NSString stringWithFormat:@"%@",@"false"];
                    
                    //Nilind 27 Feb
                    
                    objWorkOrderDetailsService.arrivalDuration=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ArrivalDuration"]];
                    objWorkOrderDetailsService.keyMap=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"KeyMap"]];
                    objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                    objWorkOrderDetailsService.problemDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ProblemDescription"]];
                    objWorkOrderDetailsService.serviceDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceDescription"]];
                    //End
                    
                    objWorkOrderDetailsService.primaryServiceSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"PrimaryServiceSysName"]];
                    objWorkOrderDetailsService.categorySysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CategorySysName"]];
                    
                    NSUserDefaults *defsLogin = [NSUserDefaults standardUserDefaults];
                    NSString *dbCompanyKey=[defsLogin valueForKey:@"companyKey"];
                    // NSString *dbUserName =[defsLogin valueForKey:@"username"];
                    
                    objWorkOrderDetailsService.dbCompanyKey=dbCompanyKey;
                    objWorkOrderDetailsService.dbUserName=strEmployeeNo;
                    
                    objWorkOrderDetailsService.noChemical=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"NoChemical"]];
                    
                    objWorkOrderDetailsService.serviceJobDescriptionId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescriptionId"]];
                    
                    objWorkOrderDetailsService.serviceJobDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceJobDescription"]];
                    
                    objWorkOrderDetailsService.isEmployeePresetSignature=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsEmployeePresetSignature"]];
                    
                    objWorkOrderDetailsService.accountDescription=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountDescription"]];
                    
                    //DepartmentSysName  isResendInvoiceMail
                    objWorkOrderDetailsService.departmentSysName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentSysName"]];
                    
                    objWorkOrderDetailsService.departmentType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DepartmentType"]];
                    
                    objWorkOrderDetailsService.isResendInvoiceMail=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsResendInvoiceMail"]];
                    
                    //
                    
                    objWorkOrderDetailsService.cellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"CellNo"]];
                    objWorkOrderDetailsService.billingFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingFirstName"]];
                    objWorkOrderDetailsService.billingMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMiddleName"]];
                    objWorkOrderDetailsService.billingLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingLastName"]];
                    objWorkOrderDetailsService.billingCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCellNo"]];
                    objWorkOrderDetailsService.billingPrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingPrimaryEmail"]]];
                    objWorkOrderDetailsService.billingSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"BillingSecondaryEmail"]]];
                    objWorkOrderDetailsService.billingPrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPrimaryPhone"]];
                    objWorkOrderDetailsService.billingSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSecondaryPhone"]];
                    objWorkOrderDetailsService.billingMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingMapCode"]];
                    objWorkOrderDetailsService.billingPOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingPOCId"]];

                    
                    objWorkOrderDetailsService.serviceFirstName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceFirstName"]];
                    objWorkOrderDetailsService.serviceMiddleName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMiddleName"]];
                    objWorkOrderDetailsService.serviceLastName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceLastName"]];
                    objWorkOrderDetailsService.serviceCellNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCellNo"]];
                    objWorkOrderDetailsService.servicePrimaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServicePrimaryEmail"]]];
                    objWorkOrderDetailsService.serviceSecondaryEmail=[NSString stringWithFormat:@"%@",[global strReplacedEmail:[dictDataWorkOrders valueForKey:@"ServiceSecondaryEmail"]]];
                    objWorkOrderDetailsService.servicePrimaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePrimaryPhone"]];
                    objWorkOrderDetailsService.serviceSecondaryPhone=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSecondaryPhone"]];
                    objWorkOrderDetailsService.serviceMapCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceMapCode"]];
                    objWorkOrderDetailsService.serviceGateCode=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceGateCode"]];
                    objWorkOrderDetailsService.serviceNotes=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceNotes"]];
                    objWorkOrderDetailsService.serviceAddressImagePath=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressImagePath"]];
                    objWorkOrderDetailsService.servicePOCId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServicePOCId"]];

                    //Nilind 08 Dec
                    
                    objWorkOrderDetailsService.timeInLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLatitude"]];
                    objWorkOrderDetailsService.timeInLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeInLongitude"]];
                    objWorkOrderDetailsService.timeOutLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLatitude"]];
                    objWorkOrderDetailsService.timeOutLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TimeOutLongitude"]];
                    objWorkOrderDetailsService.onMyWaySentSMSTimeLatitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLatitude"]];
                    objWorkOrderDetailsService.onMyWaySentSMSTimeLongitude=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"OnMyWaySentSMSTimeLongitude"]];
                    objWorkOrderDetailsService.termiteStateType=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TermiteStateType"]];
                    //End
                    
                    objWorkOrderDetailsService.billingCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingCounty"]];
                    objWorkOrderDetailsService.serviceCounty=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceCounty"]];
                    objWorkOrderDetailsService.billingSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"BillingSchoolDistrict"]];
                    objWorkOrderDetailsService.serviceSchoolDistrict=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceSchoolDistrict"]];

                    objWorkOrderDetailsService.earliestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"EarliestStartTimeStr"]];
                    objWorkOrderDetailsService.latestStartTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"LatestStartTimeStr"]];
                    objWorkOrderDetailsService.driveTimeStr=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTimeStr"]];
                    
                    
                    objWorkOrderDetailsService.fromDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnStartDateTime"]];
                    objWorkOrderDetailsService.toDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ScheduleOnEndDateTime"]];
                    objWorkOrderDetailsService.modifyDate=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                    
                    NSString *strIsRegularPestFlow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
                    
                    if ([strIsRegularPestFlow isEqualToString:@"1"] || [strIsRegularPestFlow isEqualToString:@"True"] || [strIsRegularPestFlow isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",@"false"];

                    }
                    
                    NSString *strIsRegularPestFlowOnBranch = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];

                    
                    if ([strIsRegularPestFlowOnBranch isEqualToString:@"1"] || [strIsRegularPestFlowOnBranch isEqualToString:@"True"] || [strIsRegularPestFlowOnBranch isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"true"];

                    } else {
                        
                        objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    

                    NSString *strIsCollectPayment = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsCollectPayment"]];
                    
                    if ([strIsCollectPayment isEqualToString:@"1"] || [strIsCollectPayment isEqualToString:@"True"] || [strIsCollectPayment isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isCollectPayment=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    
                    NSString *strIsWhetherShow = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsWhetherShow"]];
                    
                    if ([strIsWhetherShow isEqualToString:@"1"] || [strIsWhetherShow isEqualToString:@"True"] || [strIsWhetherShow isEqualToString:@"true"]) {
                        
                        objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"true"];
                        
                    } else {
                        
                        objWorkOrderDetailsService.isWhetherShow=[NSString stringWithFormat:@"%@",@"false"];
                        
                    }
                    
                    /*
                    objWorkOrderDetailsService.isRegularPestFlow=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlow"]];
                    objWorkOrderDetailsService.isRegularPestFlowOnBranch=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsRegularPestFlowOnBranch"]];
                    */
                    
                    objWorkOrderDetailsService.serviceAddressId=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceAddressId"]];
                    
                    objWorkOrderDetailsService.relatedOpportunityNo=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"RelatedOpportunityNo"]];

                    objWorkOrderDetailsService.taxPercent=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"TaxPercent"]];

                    objWorkOrderDetailsService.serviceReportFormat=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ServiceReportFormat"]];

                    objWorkOrderDetailsService.isBatchReleased=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"IsBatchReleased"]];

                    objWorkOrderDetailsService.accountName=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"AccountName"]];
                    objWorkOrderDetailsService.driveTime=[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"DriveTime"]];

                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Payment Info Save *******----------------
                //============================================================================
                //============================================================================
                
                entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
                
                PaymentInfoServiceAuto *objPaymentInfoServiceAuto = [[PaymentInfoServiceAuto alloc]initWithEntity:entityPaymentInfoServiceAuto insertIntoManagedObjectContext:context];
                
                //PaymentInfo
                if(![[dictWorkOrderDetail valueForKey:@"PaymentInfo"] isKindOfClass:[NSDictionary class]])
                {
                    
                }
                else
                {
                    NSDictionary *dictOfPaymentInfo = [dictWorkOrderDetail valueForKey:@"PaymentInfo"];
                    objPaymentInfoServiceAuto.woPaymentId=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"WoPaymentId"]];
                    
                    objPaymentInfoServiceAuto.workorderId=strGlobalWorkOrderId;
                    
                    objPaymentInfoServiceAuto.paymentMode=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaymentMode"]];
                    
                    objPaymentInfoServiceAuto.paidAmount=[NSString stringWithFormat:@"%.02f", [[dictOfPaymentInfo valueForKey:@"PaidAmount"] floatValue]];//[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"PaidAmount"]];
                    
                    objPaymentInfoServiceAuto.checkNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckNo"]];
                    
                    objPaymentInfoServiceAuto.drivingLicenseNo=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"DrivingLicenseNo"]];
                    
                    objPaymentInfoServiceAuto.expirationDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ExpirationDate"]];
                    
                    objPaymentInfoServiceAuto.checkFrontImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckFrontImagePath"]];
                    
                    objPaymentInfoServiceAuto.checkBackImagePath=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CheckBackImagePath"]];
                    
                    objPaymentInfoServiceAuto.createdDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedDate"]];
                    
                    objPaymentInfoServiceAuto.createdBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"CreatedBy"]];
                    
                    objPaymentInfoServiceAuto.modifiedDate=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedDate"]];
                    
                    objPaymentInfoServiceAuto.modifiedBy=[NSString stringWithFormat:@"%@",[dictOfPaymentInfo valueForKey:@"ModifiedBy"]];
                    
                    objPaymentInfoServiceAuto.userName=strUserName;
                    
                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Chemical List Save *******----------------
                //============================================================================
                //============================================================================
                //WoProductDetail
                if(![[dictWorkOrderDetail valueForKey:@"WoProductDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSDictionary *dictOfWoProductDetail = [dictWorkOrderDetail valueForKey:@"WoProductDetail"];
                    
                    NSDictionary *dictOfWoOtherProductDetail = [dictWorkOrderDetail valueForKey:@"WoOtherProductDetail"];
                    
                    entityChemicalListDetailServiceAuto=[NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
                    
                    WorkorderDetailChemicalListService *objServiceAutoCompanyDetails = [[WorkorderDetailChemicalListService alloc]initWithEntity:entityChemicalListDetailServiceAuto insertIntoManagedObjectContext:context];
                    
                    objServiceAutoCompanyDetails.companyKey=strCompanyKeyy;
                    objServiceAutoCompanyDetails.userName=strUserName;
                    objServiceAutoCompanyDetails.chemicalList=dictOfWoProductDetail;
                    objServiceAutoCompanyDetails.workorderId=strGlobalWorkOrderId;
                    objServiceAutoCompanyDetails.otherChemicalList=dictOfWoOtherProductDetail;
                    
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Email Id Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"EmailDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"EmailDetail"];//EmailDetail
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //Email Detail Entity
                        entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
                        
                        EmailDetailServiceAuto *objEmailDetail = [[EmailDetailServiceAuto alloc]initWithEntity:entityEmailDetailServiceAuto insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        
                        objEmailDetail.userName=strUserName;
                        
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        
                        objEmailDetail.woInvoiceMailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"WoInvoiceMailId"]];
                        
                        objEmailDetail.emailId=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"EmailId"]];
                        
                        objEmailDetail.subject=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"Subject"]];
                        
                        //  objEmailDetail.isMailSent=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]];
                        
                        //objEmailDetail.isCustomerEmail=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsCustomerEmail"]] isEqualToString:@"True"])
                        {
                            objEmailDetail.isCustomerEmail=@"true";

                        }
                        else
                        {
                            objEmailDetail.isCustomerEmail=@"false";

                        }

                                        
                        objEmailDetail.createdDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedDate"]];
                        
                        objEmailDetail.createdBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"CreatedBy"]];
                        
                        objEmailDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedDate"]];
                        
                        objEmailDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"ModifiedBy"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsMailSent"]] isEqualToString:@"True"])
                        {
                            objEmailDetail.isMailSent=@"true";
                        }
                        else
                        {
                            objEmailDetail.isMailSent=@"false";
                        }
                        //IsDefaultEmail
                        if([[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictEmailDetail valueForKey:@"IsDefaultEmail"]] isEqualToString:@"True"])
                        {
                            objEmailDetail.isDefaultEmail=@"true";
                        }
                        else
                        {
                            objEmailDetail.isDefaultEmail=@"false";
                        }
                        
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Image Detail Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"ImagesDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"ImagesDetail"];//EmailDetail
                    for (int j=0; j<arrImagesDetail.count; j++)
                    {
                        //Images Detail Entity
                        entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
                        
                        ImageDetailsServiceAuto *objImagesDetail = [[ImageDetailsServiceAuto alloc]initWithEntity:entityImageDetailServiceAuto insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                        dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        objImagesDetail.woImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageId"]];
                        
                        objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                        
                        objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                        
                        objImagesDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                        
                        objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                        
                        objImagesDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]];
                        
                        objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                        
                        objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                        
                        objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                        
                        objImagesDetail.latitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Latitude"]];
                        
                        objImagesDetail.longitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Longitude"]];
                        
                        if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"1"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsProblemIdentifaction"]] isEqualToString:@"True"])
                        {
                            objImagesDetail.isProblemIdentifaction=@"true";
                        }
                        else
                        {
                            objImagesDetail.isProblemIdentifaction=@"false";
                        }
                        
                        objImagesDetail.graphXmlPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"GraphXmlPath"]];
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Termite Image Detail Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"TexasTermiteImagesDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"TexasTermiteImagesDetail"];//EmailDetail
                    for (int j=0; j<arrImagesDetail.count; j++)
                    {
                        //Images Detail Entity
                        entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
                        
                        ImageDetailsTermite *objImagesDetail = [[ImageDetailsTermite alloc]initWithEntity:entityImageDetailsTermite insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                        dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        objImagesDetail.woTexasTermiteImageId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoTexasTermiteImageId"]];
                        
                        objImagesDetail.woImagePath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImagePath"]];
                        
                        objImagesDetail.woImageType=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoImageType"]];
                        
                        objImagesDetail.createdDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                        
                        objImagesDetail.createdBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"CreatedBy"]];
                        
                        objImagesDetail.modifiedDate=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedDate"]];
                        
                        objImagesDetail.modifiedBy=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ModifiedBy"]];
                        
                        objImagesDetail.imageCaption=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageCaption"]];
                        
                        objImagesDetail.imageDescription=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"ImageDescription"]];
                        
                        objImagesDetail.latitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Latitude"]];
                        
                        objImagesDetail.longitude=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Longitude"]];
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                
                
                // Wo Other Document Saving
                
                //============================================================================
                //============================================================================
#pragma mark- --------- Wo Other Document  Detail Save ----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"WoOtherDocuments"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"WoOtherDocuments"];//WoOtherDocuments
                    for (int j=0; j<arrImagesDetail.count; j++)
                    {
                        //Images Detail Entity
                        entityWorkOrderDocuments=[NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
                        
                        WoOtherDocuments *objImagesDetail = [[WoOtherDocuments alloc]initWithEntity:entityWorkOrderDocuments insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictImagesDetail=[[NSMutableDictionary alloc]init];
                        dictImagesDetail=[arrImagesDetail objectAtIndex:j];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        objImagesDetail.woOtherDocumentId=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"WoOtherDocumentId"]];
                        
                        objImagesDetail.title=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"Title"]];
                        
                        objImagesDetail.otherDocumentPath=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"OtherDocumentPath"]];
                        
                        
                        //IsDefaultEmail
                        if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsDefault"]] isEqualToString:@"1"])
                        {
                            objImagesDetail.isDefault=@"true";
                        }
                        else
                        {
                            objImagesDetail.isDefault=@"false";
                        }
                        
                        if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsChecked"]] isEqualToString:@"1"])
                        {
                            objImagesDetail.isChecked=@"true";
                        }
                        else
                        {
                            objImagesDetail.isChecked=@"false";
                        }
                        
                        if([[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"IsActive"]] isEqualToString:@"1"])
                        {
                            objImagesDetail.isActive=@"true";
                        }
                        else
                        {
                            objImagesDetail.isActive=@"false";
                        }
                        
                        objImagesDetail.otherDocSysName=[NSString stringWithFormat:@"%@",[dictImagesDetail valueForKey:@"OtherDocSysName"]];
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                }
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Equipment Detail Save *******----------------
                //============================================================================
                //============================================================================
                
                //Equipment Save  WoEquipmentDetail
                if(![[dictWorkOrderDetail valueForKey:@"WoEquipmentDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    //                    NSDictionary *dictOfEquipmentList=[dictWorkOrderDetail valueForKey:@"WOEquipmentList"];
                    //                    if(![[dictOfEquipmentList valueForKey:@"WOEquipmentDcs"] isKindOfClass:[NSArray class]])
                    //                    {
                    //
                    //                    }
                    //                    else
                    //                    {
                    
                    NSArray *arrImagesDetail=[dictWorkOrderDetail valueForKey:@"WoEquipmentDetail"];//EmailDetail
                    for (int j=0; j<arrImagesDetail.count; j++)
                    {
                        //Equipment Detail Entity
                        entityWOEquipmentServiceAuto=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
                        
                        WorkOrderEquipmentDetails *objImagesDetail = [[WorkOrderEquipmentDetails alloc]initWithEntity:entityWOEquipmentServiceAuto insertIntoManagedObjectContext:context];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        objImagesDetail.arrOfEquipmentList=arrImagesDetail;
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                    
                    //}
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** TexasTermiteServiceDetail Save *******----------------
                //============================================================================
                //============================================================================
                
                //Equipment Save  WoEquipmentDetail
                if(![[dictWorkOrderDetail valueForKey:@"TexasTermiteServiceDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    
                    //Changes To Save Termite Static form for texas
                    
                    NSArray *arrTermiteTexas=[dictWorkOrderDetail valueForKey:@"TexasTermiteServiceDetail"];//EmailDetail
                    for (int j=0; j<arrTermiteTexas.count; j++)
                    {
                        //Equipment Detail Entity
                        entityTermiteTexas=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context];
                        
                        TexasTermiteServiceDetail *objImagesDetail = [[TexasTermiteServiceDetail alloc]initWithEntity:entityTermiteTexas insertIntoManagedObjectContext:context];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        NSDictionary *dictTermiteTexas;
                        
                        if (!(arrTermiteTexas.count==0)) {
                            
                            dictTermiteTexas=arrTermiteTexas[0];
                            
                        }else{
                            
                            dictTermiteTexas=nil;
                            
                        }
                        
                        objImagesDetail.texasTermiteServiceDetail=dictTermiteTexas;
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                    
                }
                //============================================================================
#pragma mark- ---------******** FloridaTermiteServiceDetail Save *******----------------
                //============================================================================
                //
                if(![[dictWorkOrderDetail valueForKey:@"FloridaTermiteServiceDetail"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    
                    //Changes To Save Termite Static form for texas
                    
                    NSArray *arrTermiteFlorida=[dictWorkOrderDetail valueForKey:@"FloridaTermiteServiceDetail"];//EmailDetail
                    for (int j=0; j<arrTermiteFlorida.count; j++)
                    {
                        //Equipment Detail Entity
                        entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
                        
                        FloridaTermiteServiceDetail *objImagesDetail = [[FloridaTermiteServiceDetail alloc]initWithEntity:entityTermiteFlorida insertIntoManagedObjectContext:context];
                        
                        objImagesDetail.companyKey=strCompanyKeyy;
                        
                        objImagesDetail.userName=strUserName;
                        
                        objImagesDetail.workorderId=strGlobalWorkOrderId;
                        
                        NSDictionary *dictTermiteTexas;
                        
                        if (!(arrTermiteFlorida.count==0)) {
                            
                            dictTermiteTexas=arrTermiteFlorida[0];
                            
                        }else{
                            
                            dictTermiteTexas=nil;
                            
                        }
                        
                        objImagesDetail.floridaTermiteServiceDetail=dictTermiteTexas;
                        
                        NSError *error1;
                        [context save:&error1];
                    }
                    
                }
                
                
                //ServiceAutoModifyDate
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Modify Date Save *******----------------
                //============================================================================
                //============================================================================
                
                entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
                
                ServiceAutoModifyDate *objServiceAutoCompanyDetails = [[ServiceAutoModifyDate alloc]initWithEntity:entityModifyDateServiceAuto insertIntoManagedObjectContext:context];
                
                objServiceAutoCompanyDetails.companyKey=strCompanyKeyy;
                objServiceAutoCompanyDetails.userName=strUserName;
                objServiceAutoCompanyDetails.workorderId=strGlobalWorkOrderId;
                
                NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"EST"];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                objServiceAutoCompanyDetails.modifyDate=outputString;//[NSString stringWithFormat:@"%@",[dictDataWorkOrders valueForKey:@"ModifiedFormatedDate"]];;
                
                //Final Save
                NSError *error;
                [context save:&error];
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Service Auto ServiceAddressPOCDetailDcs Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"ServiceAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalServiceAddressPOCDetailDcs=[NSEntityDescription entityForName:@"ServiceAddressPOCDetailDcs" inManagedObjectContext:context];
                        
                        ServiceAddressPOCDetailDcs *objEmailDetail = [[ServiceAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalServiceAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        objEmailDetail.userName=strUserName;
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        objEmailDetail.pocDetails=arrEmailDetail;
                        
                        NSError *error2222;
                        [context save:&error2222];
                    }
                }
                
                
                //============================================================================
                //============================================================================
#pragma mark- ---------******** Service Auto BillingAddressPOCDetailDcs Save *******----------------
                //============================================================================
                //============================================================================
                
                //Email Id Save
                if(![[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"] isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    
                    NSArray *arrEmailDetail=[dictWorkOrderDetail valueForKey:@"BillingAddressPOCDetailDcs"];//AccountItemHistoryExtSerDcs
                    for (int j=0; j<arrEmailDetail.count; j++)
                    {
                        //AccountItemHistoryExtSerDcs Detail Entity
                        entityMechanicalBillingAddressPOCDetailDcs=[NSEntityDescription entityForName:@"BillingAddressPOCDetailDcs" inManagedObjectContext:context];
                        
                        BillingAddressPOCDetailDcs *objEmailDetail = [[BillingAddressPOCDetailDcs alloc]initWithEntity:entityMechanicalBillingAddressPOCDetailDcs insertIntoManagedObjectContext:context];
                        NSMutableDictionary *dictEmailDetail=[[NSMutableDictionary alloc]init];
                        dictEmailDetail=[arrEmailDetail objectAtIndex:j];
                        
                        objEmailDetail.companyKey=strCompanyKeyy;
                        objEmailDetail.userName=strUserName;
                        objEmailDetail.workorderId=strGlobalWorkOrderId;
                        objEmailDetail.pocDetails=arrEmailDetail;
                        
                        NSError *error22222;
                        [context save:&error22222];
                    }
                }
                
                //Final Save
                NSError *error123;
                [context save:&error123];

            }
        }
        
        //============================================================================
        //============================================================================
#pragma mark- ---------******** ServiceAutoCompanyDetails Save *******----------------
        //============================================================================
        //============================================================================
        
        if(![[dictWorkOrderServiceAppointment valueForKey:@"CompanyDetail"] isKindOfClass:[NSString class]])
        {
            
        }
        else
        {
            
            //  NSDictionary *dictCompanyServiceAuto =[dictWorkOrderServiceAppointment valueForKey:@"CompanyDetail"];
            
            //  entityCompanyDetailServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoCompanyDetails" inManagedObjectContext:context];
            
            //   ServiceAutoCompanyDetails *objServiceAutoCompanyDetails = [[ServiceAutoCompanyDetails alloc]initWithEntity:entityWorkOderDetailServiceAuto insertIntoManagedObjectContext:context];
            
            //objServiceAutoCompanyDetails.companyKey=strCompanyKeyy;
            // objServiceAutoCompanyDetails.userName=strUserName;
            // objServiceAutoCompanyDetails.companyDetails=dictCompanyServiceAuto;
            
            // NSError *error;
            // [context save:&error];
            
        }
        
        
        
    }
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServer"];
    
    if (arrOfLeadsTosendToServer.count==0) {
        
        if (arrOfSalesLeadToFetchDynamicData.count>0) {
            
            [self performSelector:@selector(getDynamicFormDataSales) withObject:nil afterDelay:1.0];
            
            
        }else{
            
            if (arrOfServiceLeadToFetchDynamicData.count>0) {
                
                [self getDynamicFormDataService];
                
            }else{
                
                //[self SalesAutomationFetchAfterSyncingData];
                
            }
            
            
        }
        
        [self salesFetch];
        
    } else {
        
        // Yaha Par Phle Device Dynamic Send Kr deta hun then baki send kr denge
        // indexToSendDeviceDynamicForm
        
//        indexToSendDeviceDynamicForm = 0;
//
//        [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
        
        [self sendingServiceDynamicFormDataToServer:0];
        
    }
    
}

- (IBAction)action_Search:(id)sender {
    _searchBarr.text=@"";
    _searchBarr.hidden=NO;
    [_btn_Refreshh setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [_searchBarr becomeFirstResponder];
}

- (IBAction)action_Filter:(id)sender {
    
    if (yesFiltered) {
        
        if ([_txt_LastName.text isEqualToString:@"#`%&*"]) {
            
            _txt_LastName.text=@"";
            
        }
        if ([_txt_AccountNo.text isEqualToString:@"#`%&*"]) {
            
            _txt_AccountNo.text=@"";
            
        }
        if ([_txt_FirstName.text isEqualToString:@"#`%&*"]) {
            
            _txt_FirstName.text=@"";
            
        }
        if ([_txt_WorkOrderNo.text isEqualToString:@"#`%&*"]) {
            
            _txt_WorkOrderNo.text=@"";
            
        }
        if ([_Txt_Leadd.text isEqualToString:@"#`%&*"]) {
            
            _Txt_Leadd.text=@"";
            
        }
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        frameFor_view_CustomerInfo.origin.x=0;
        frameFor_view_CustomerInfo.origin.y=20;
        
        [_view_FilterCriteria setFrame:frameFor_view_CustomerInfo];
        //  _view_FilterCriteria.backgroundColor=[UIColor lightGrayColor];
        //  _scrollView_Filter.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-75);
        // _scrollView_Filter.backgroundColor=[UIColor redColor];
        [self.view addSubview:_view_FilterCriteria];
        
    } else {
        
        [_searchBarr resignFirstResponder];
        _searchBarr.hidden=YES;
        yesFiltered=NO;
        [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
        //   [_tblViewAppointment reloadData];
        
        isTodayAppointments=NO;
        [_lblLineAll setHidden:NO];
        [_lblLineToday setHidden:YES];
        yesFiltered=NO;
        [self salesFetch];
        
        arrDataTblView=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
        
        NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
        
        [arrDataTblView addObject:@"All"];
        
        for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
            NSDictionary *dictData=arrOfLeadStatusMasters[k];
            [arrDataTblView addObject:[dictData valueForKey:@"StatusName"]];
        }
        if (arrDataTblView.count==0) {
            
            [_btn_StatusFilter setTitle:@"Choose Status" forState:UIControlStateNormal];
            
        } else {
            
            [_btn_StatusFilter setTitle:arrDataTblView[0] forState:UIControlStateNormal];
            
        }
        _txt_LastName.text=@"";
        _txt_AccountNo.text=@"";
        _txt_FirstName.text=@"";
        _txt_WorkOrderNo.text=@"";
        _Txt_Leadd.text=@"";
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        strDate = [dateFormat stringFromDate:[NSDate date]];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDateComponents *components=[[NSDateComponents alloc] init];
        components.day=1;
        NSDate *targetDatePlus =[calendar dateByAddingComponents:components toDate:[NSDate date] options: 0];
        NSString *strToDate = [dateFormat stringFromDate:targetDatePlus];
        
        [_btn_ToDateFilter setTitle:strToDate forState:UIControlStateNormal];
        [_btn_FromDateFilter setTitle:strDate forState:UIControlStateNormal];
        strGlobalToDate=strToDate;
        strGlobalFromDate=strDate;
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        frameFor_view_CustomerInfo.origin.x=0;
        //        frameFor_view_CustomerInfo.origin.y=20;// commented by Akshay 23 Aug 2018
        frameFor_view_CustomerInfo.origin.y=35;// Akshay 23 Aug 2018
        
        
        [_view_FilterCriteria setFrame:frameFor_view_CustomerInfo];
        //  _view_FilterCriteria.backgroundColor=[UIColor lightGrayColor];
        //  _scrollView_Filter.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-75);
        // _scrollView_Filter.backgroundColor=[UIColor redColor];
        [self.view addSubview:_view_FilterCriteria];
        
    }
    
}

//============================================================================
//============================================================================
#pragma mark- SEARCH BAR DELEGATE METHODS
//============================================================================
//============================================================================

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //Search hota hai
    isSearchBarFilter=YES;
    [searchBar resignFirstResponder];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length==0){
        //cancel hota hai
        yesFiltered=NO;
        isSearchBarFilter=NO;
        [_tblViewAppointment setHidden:NO];
        // [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
        [_tblViewAppointment reloadData];
        
    }else
    {
        isSearchBarFilter=YES;
        yesFiltered=YES;
        filteredArray=[[NSMutableArray alloc]init];
        
        for(int i=0;i<[arrAllObj count];i++){
            
            NSManagedObject *objWOSales=[arrAllObj objectAtIndex:i];
            
            if (![objWOSales isKindOfClass:[NSManagedObject class]]) {
                
                
                
            } else {
             
                NSString *str11;
                NSString *str22;
                NSString *str33;
                NSString *str111;
                if ([objWOSales isKindOfClass:[LeadDetail class]]) {
                    
                    str11=[objWOSales valueForKey:@"leadNumber"];
                    str22=[objWOSales valueForKey:@"leadName"];
                    
                    NSString *strFullName =[NSString stringWithFormat:@"%@ %@",[objWOSales valueForKey:@"firstName"],[objWOSales valueForKey:@"lastName"]];
                    
                    str33=strFullName;//[objWOSales valueForKey:@"customerName"];
                    str111=[objWOSales valueForKey:@"accountNo"];//[dict valueForKeyPath:@"LeadContact.FullName"];
                    
                    
                }else{
                    
                    str11=[objWOSales valueForKey:@"workorderId"];
                    
                    NSString *strFullName =[NSString stringWithFormat:@"%@ %@",[objWOSales valueForKey:@"firstName"],[objWOSales valueForKey:@"lastName"]];
                    
                    str22=strFullName;//[objWOSales valueForKey:@"firstName"];
                    str33=[objWOSales valueForKey:@"workOrderNo"];
                    str111=[objWOSales valueForKey:@"accountNo"];//[dict valueForKeyPath:@"LeadContact.FullName"];
                    
                    NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[objWOSales valueForKey:@"thirdPartyAccountNo"]];
                    if (strThirdPartyAccountNo.length>0) {
                        str111=strThirdPartyAccountNo;
                    }
                    
                    
                }
                
                NSRange strRange1=[str11 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange strRange2=[str22 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange strRange3=[str33 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange strRange4=[str111 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if(strRange1.location!=NSNotFound||strRange2.location!=NSNotFound||strRange3.location!=NSNotFound||strRange4.location!=NSNotFound)
                {
                    [filteredArray addObject:objWOSales];
                }
                
            }

        }
    }
    
    [_tblViewAppointment setHidden:NO];
    //  [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    [_tblViewAppointment reloadData];
    
    if (filteredArray.count>0) {
        
        NSIndexPath *indexpat= [NSIndexPath indexPathForRow:0 inSection:0];
        [_tblViewAppointment scrollToRowAtIndexPath:indexpat
                                   atScrollPosition:UITableViewScrollPositionTop
                                           animated:YES
         ];
        
    }

}
- (void)searchBarTextDidBeginEditing:(UISearchBar *) bar
{
    UITextField *searchBarTextField = nil;
    NSArray *views = ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f) ? bar.subviews : [[bar.subviews objectAtIndex:0] subviews];
    for (UIView *subview in views)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            searchBarTextField = (UITextField *)subview;
            break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}

-(void)saveSalesModifydate{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",@"138",strUsername];
    //
    //    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
        
    }
    else
    {
        for (int k=0; k<arrAllObjModifyDate.count; k++) {
            
            matchesModifyDate=arrAllObjModifyDate[k];
            
            NSDate *currentDate=[NSDate date];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
            [formatter setTimeZone:[NSTimeZone localTimeZone]];
            NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
            
            //            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            //            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
            //            NSDate* modifyDateToSet = [dateFormatter dateFromString:stringCurrentDate];
            
            [matchesModifyDate setValue:stringCurrentDate forKey:@"modifyDate"];
            
            //Final Save
            NSError *error;
            [context save:&error];
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

//============================================================================
//============================================================================
#pragma mark- Sending Sales Lead To Server METHODS
//============================================================================
//============================================================================

-(void)sendingSalesFinalJsonToServerNew :(int)indexToSend{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendSalesLeadToServer"];
    
    strLeadIdGlobal=arrOfLeadsTosendToServer[indexToSend];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    requestModifyDate = [[NSFetchRequest alloc] init];
    [requestModifyDate setEntity:entityModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadIdd = %@ AND userName = %@",strLeadIdGlobal,strUsername];
    
    [requestModifyDate setPredicate:predicate];
    
    sortDescriptorModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsModifyDate = [NSArray arrayWithObject:sortDescriptorModifyDate];
    
    [requestModifyDate setSortDescriptors:sortDescriptorsModifyDate];
    
    self.fetchedResultsControllerModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerModifyDate performFetch:&error];
    arrAllObjModifyDate = [self.fetchedResultsControllerModifyDate fetchedObjects];
    if ([arrAllObjModifyDate count] == 0)
    {
        
        strModifyDateToSendToServerAll=@"";
        
    }
    else
    {
        matchesModifyDate=arrAllObjModifyDate[0];
        strModifyDateToSendToServerAll=[matchesModifyDate valueForKey:@"modifyDate"];
        if (strModifyDateToSendToServerAll.length==0) {
            
            strModifyDateToSendToServerAll=@"";
            
        }
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend=[[NSMutableArray alloc]init];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    indexToSendCheckImages=0;
    indexToSendDocuments=0;
    
    [self fetchLeadDetail];
    [self fetchPaymentInfo];
    [self fetchImageDetail];
    [self fetchEmailDetail];
    [self fetchDocumentsDetail];
    [self fetchLeadPreference];
    [self fetchSoldServiceStandardDetail];
    [self fetchSoldServiceNonStandardDetail];
    [self fetchAgreementCheckListSales];
    [self fetchElectronicAuthorizedForm];
    [self fetchForAppliedDiscountFromCoreData];
    [self fetchLeadContactDetail];
    [self fetchRenewalServiceDetail];
    //For  ClarkPest
    
    [self fetchScopeFromCoreData];
    [self fetchTargetFromCoreData];
    [self fetchLeadCommercialInitialInfoFromCoreData];
    [self fetchLeadCommercialMaintInfoFromCoreData];
    [self fetchLeadCommercialDetailExtDcFromCoreData];
    [self fetchMultiTermsFromCoreDataClarkPest];
    [self fetchForAppliedDiscountFromCoreDataClarkPest];
    [self fetchSalesMarketingContentFromCoreDataClarkPest];
    
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
    }
    else
    {
        if (arrOfAllImagesToSendToServer.count==0) {
            
            if (arrOfAllSignatureImagesToSendToServer.count==0) {
                
                if (!(strAudioNameGlobal.length==0)) {
                    
                    [self uploadAudioSales : strAudioNameGlobal : strSalesAutoMainUrl];
                    
                } else {
                    
                    [self finalJson];
                    
                }
                
            } else {
                
                [self uploadingAllImagesSignature:0];
                
            }
            
        } else {
            
            [self uploadingAllImages:0];
            
        }
    }
}

-(void)sendingSalesFinalJsonToServerNewServiceAuto :(int)indexToSend{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServer"];
    
    strWorkOrderIdGlobal=arrOfLeadsTosendToServer[indexToSend];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityServiceModifyDate=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    requestServiceModifyDate = [[NSFetchRequest alloc] init];
    [requestServiceModifyDate setEntity:entityServiceModifyDate];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND userName = %@",strWorkOrderIdGlobal,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestServiceModifyDate setPredicate:predicate];
    
    sortDescriptorServiceModifyDate = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsServiceModifyDate = [NSArray arrayWithObject:sortDescriptorServiceModifyDate];
    
    [requestServiceModifyDate setSortDescriptors:sortDescriptorsServiceModifyDate];
    
    self.fetchedResultsControllerServiceModifyDate = [[NSFetchedResultsController alloc] initWithFetchRequest:requestServiceModifyDate managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceModifyDate setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceModifyDate performFetch:&error];
    arrAllObjServiceModifyDate = [self.fetchedResultsControllerServiceModifyDate fetchedObjects];
    if ([arrAllObjServiceModifyDate count] == 0)
    {
        
        strModifyDateToSendToServerAll=@"";
        
    }
    else
    {
        matchesServiceModifyDate=arrAllObjServiceModifyDate[0];
        strModifyDateToSendToServerAll=[matchesServiceModifyDate valueForKey:@"modifyDate"];
        
        if (strModifyDateToSendToServerAll.length==0) {
            
            strModifyDateToSendToServerAll=@"";
            
        }
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
    
    arrOfAllImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllSignatureImagesToSendToServer=[[NSMutableArray alloc]init];
    arrOfAllCheckImageToSend=[[NSMutableArray alloc]init];
    arrOfAllDocumentsToSend = [[NSMutableArray alloc]init];
    
    indexToSendImage=0;
    indexToSendImageSign=0;
    indexToSendCheckImages=0;
    indexToSendDocuments=0;
    
    dictFinal=[[NSMutableDictionary alloc]init];
    [self fetchWorkOrderDetailServiceAuto];
    [self fetchWOProductDetail];
    [self fetchPaymentInfoServiceAuto];
    [self fetchTermiteFromDb];
    [self fetchTermiteFloridaFromDb];
    [self fetchImageDetailServiceAuto];
    [self fetchEquipmentDetailServiceAuto];
    [self fetchEmailDetailServiceAuto];
    [self fetchWorkOrderDocuments];
    [self fetchTermiteImageDetailServiceAuto];
    
    [self fetchServicePestDataAllFromDBObjC];
    
    NSLog(@"Final Dictionary >>>>>>%@",dictFinal);
    Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
    if (netStatusWify1== NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [DejalBezelActivityView removeView];
    }
    else
    {
        
        // To Send Documents First and then all other images and all as of before
        
        if (arrOfAllDocumentsToSend.count == 0) {
            
            if (arrOfAllImagesToSendToServer.count==0) {
                
                if (arrOfAllSignatureImagesToSendToServer.count==0) {
                    
                    [self finalJsonServiceAutomation];
                    
                } else {
                    
                    [self uploadingAllImagesServiceAutoCheck:0];
                    
                }
                
            } else {
                
                [self uploadingAllImagesServiceAuto:0];
                
            }
            
        } else {
            
            // Yaha par documents Syn Honge Web Par
            
            [self uploadingAllDocumentsPest:0];
            
        }
        
        
        /*
        if (arrOfAllImagesToSendToServer.count==0) {
            
            if (arrOfAllSignatureImagesToSendToServer.count==0) {
                
                [self finalJsonServiceAutomation];
                
            } else {
                
                [self uploadingAllImagesServiceAutoCheck:0];
                
            }
            
        } else {
            
            [self uploadingAllImagesServiceAuto:0];
            
        }
        */
        
    }
}

#pragma mark- FINAL IMAGE SEND

-(void)uploadingAllImages :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesSignature:0];
        
    } else {
        
        [self uploadImage:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesSignature :(int)indexToSendimages{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages) {
        
        if (!(strAudioNameGlobal.length==0)) {
            
            [self uploadAudioSales : strAudioNameGlobal : strSalesAutoMainUrl];
            
        } else {
            
            [self finalJson];
            
        }
        
        
    } else {
        
        [self uploadImageSignature:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
}
-(void)uploadingAllImagesServiceAuto :(int)indexToSendimages{
    
    if (arrOfAllImagesToSendToServer.count==indexToSendimages) {
        
        [self uploadingAllImagesServiceAutoCheck:0];
        
    } else {
        
        [self uploadImageServiceAutomation:arrOfAllImagesToSendToServer[indexToSendimages]];
        
    }
}

-(void)uploadingAllImagesServiceAutoCheck :(int)indexToSendimages{
    
    if (arrOfAllCheckImageToSend.count==indexToSendimages) {
        
        [self uploadingAllImagesServiceAutoSignatures:0];
        
    } else {
        
        [self uploadImageServiceAutomationCheck:arrOfAllCheckImageToSend[indexToSendimages]];
        
    }
    
}
-(void)uploadingAllImagesServiceAutoSignatures :(int)indexToSendimages{
    
    if (arrOfAllSignatureImagesToSendToServer.count==indexToSendimages) {
        
        if (!(strAudioNameGlobal.length==0)) {
            
            [self uploadAudio : strAudioNameGlobal : strServiceUrlMainServiceAutomation];
            
        } else {
            
            [self finalJsonServiceAutomation];
            
        }
        
    } else {
        
        [self uploadImageServiceAutomationSignatures:arrOfAllSignatureImagesToSendToServer[indexToSendimages]];
        
    }
    
}

#pragma mark- -------------- FETCHING ALL RECORDS CORE DATA --------------------

#pragma mark- -------- Fetching Sales Record ----------

-(void)fetchLeadDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND employeeNo_Mobile=%@",strLeadIdGlobal,strEmployeeNo];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            objSalesLeadIdGlobal=nil;
            objSalesLeadIdGlobal=matches;
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrLeadDetailKey);
            
            NSLog(@"all keys %@",arrLeadDetailValue);
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                //[arrLeadDetailValue addObject:str];
                if([[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"billingCountry"]||[[arrLeadDetailKey objectAtIndex:i]isEqualToString:@"serviceCountry"])
                {
                    [arrLeadDetailValue addObject:[str stringByReplacingOccurrencesOfString:@" " withString:@""]];
                }
                else
                {
                    [arrLeadDetailValue addObject:str];
                }
                
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            //  [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            NSLog(@"%@",dictTemp);
            [dictFinal setObject:dictTemp forKey:@"LeadDetail"];
            
            
        }
    }
}
-(void)fetchPaymentInfo
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrCheckImage=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSString *strCustomerImage=[matches valueForKey:@"customerSignature"];
            NSString *strSalesPersonImage=[matches valueForKey:@"salesSignature"];
            NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
            if (!(strCustomerImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerImage];//salesSignature
                
            }
            if (!(strSalesPersonImage.length==0)) {
                
                [arrOfAllSignatureImagesToSendToServer addObject:strSalesPersonImage];//salesSignature
                
            }
            if (!(strCheckFrontImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckFrontImage];//salesSignature
                
            }
            if (!(strCheckBackImage.length==0))
            {
                
                [arrCheckImage addObject:strCheckBackImage];//salesSignature
                
            }
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"PaymentInfo%@",dictPaymentInfo);
            [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
        }
    }
}
-(void)fetchImageDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSString *strImageName=[matches valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}
-(void)fetchEmailDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetail= [NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    [request setEntity:entityEmailDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchLeadContactDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityContactNumberDetail= [NSEntityDescription entityForName:@"LeadContactDetailExtSerDc" inManagedObjectContext:context];
    [request setEntity:entityContactNumberDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"LeadContactDetailExtSerDc%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadContactDetailExtSerDc"];
        
    }
}
-(void)fetchDocumentsDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrDocumentsDetail addObject:dictPaymentInfo];
            NSLog(@"DocumentsDetail%@",arrDocumentsDetail);
        }
        [dictFinal setObject:arrDocumentsDetail forKey:@"DocumentsDetail"];
    }
}
-(void)fetchLeadPreference
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadPreference= [NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
    [request setEntity:entityLeadPreference];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadPreference"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadPreference%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadPreference"];
        }
        
        
    }
}
-(void)fetchSoldServiceStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrSoldServiceStandardDetail;
    arrSoldServiceStandardDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrPaymentInfoValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 if([str isEqual:nil] || str.length==0 )
                 {
                 str=@"";
                 }
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrSoldServiceStandardDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"SoldServiceStandardDetail%@",arrSoldServiceStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceStandardDetail forKey:@"SoldServiceStandardDetail"];
    }
}
-(void)fetchSoldServiceNonStandardDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceNonStandardDetail= [NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrSoldServiceNonStandardDetail;
    arrSoldServiceNonStandardDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"leadId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            NSDictionary *dictSoldServiceNonStandardDetail;
            dictSoldServiceNonStandardDetail = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictSoldServiceNonStandardDetail);
            [arrSoldServiceNonStandardDetail addObject:dictSoldServiceNonStandardDetail];
            NSLog(@"SoldServiceNonStandardDetail%@",arrSoldServiceNonStandardDetail);
        }
        [dictFinal setObject:arrSoldServiceNonStandardDetail forKey:@"SoldServiceNonStandardDetail"];
        
    }
}
-(void)fetchRenewalServiceDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityRenewalServiceDetail= [NSEntityDescription entityForName:@"RenewalServiceExtDcs" inManagedObjectContext:context];
    [request setEntity:entityRenewalServiceDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrRenewalServiceDetail;
    arrRenewalServiceDetail=[[NSMutableArray alloc]init];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
        
    }else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);

            
            NSArray *arrRenewalServiceKey;
            NSMutableArray *arrRenewalServiceValue;
            
            arrRenewalServiceValue=[[NSMutableArray alloc]init];
            arrRenewalServiceKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrRenewalServiceKey);
            
            NSLog(@"all keys %@",arrRenewalServiceValue);
            for (int i=0; i<arrRenewalServiceKey.count; i++)
            {
                //NSString *str;
                id str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrRenewalServiceKey objectAtIndex:i]]];
                if([str isEqual:nil])// || str.length==0 )
                {
                    str=@"";
                }
                if([str isKindOfClass:[NSArray class]])
                {
                    
                }
                else
                {
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                }
                
                [arrRenewalServiceValue addObject:str];
                
                /* NSString *str;
                 str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                 
                 BOOL isTrue=[global isStringNullNilBlank:str];
                 if (isTrue) {
                 str=@"";
                 }
                 
                 [arrPaymentInfoValue addObject:str];*/
            }
            
            
            NSDictionary *dictSoldServiceStandardDetail;
            dictSoldServiceStandardDetail = [NSDictionary dictionaryWithObjects:arrRenewalServiceValue forKeys:arrRenewalServiceKey];
            
            NSLog(@"%@",dictSoldServiceStandardDetail);
            [arrRenewalServiceDetail addObject:dictSoldServiceStandardDetail];
            NSLog(@"RenewalServiceExtDcs%@",arrRenewalServiceDetail);
        }
        [dictFinal setObject:arrRenewalServiceDetail forKey:@"RenewalServiceExtDcs"];
    }
}

#pragma mark- fetching for service Automation


-(void)fetchWorkOrderDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"WorkorderDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            
            objServiceWorkOrderGlobal=nil;
            objServiceWorkOrderGlobal=matches;
            
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            strAudioNameGlobal=[matches valueForKey:@"audioFilePath"];
            NSArray *arrLeadDetailKey;
            //arrLeadDetailKey=[[NSMutableArray alloc]init];
            NSMutableArray *arrLeadDetailValue;
            arrLeadDetailValue=[[NSMutableArray alloc]init];
            arrLeadDetailKey = [[[matches entity] attributesByName] allKeys];
            
            
            NSString *strCustomerSign=[matches valueForKey:@"customerSignaturePath"];
            NSString *strTechSign=[matches valueForKey:@"technicianSignaturePath"];
            
            if (!(strCustomerSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strCustomerSign];
            }
            
            if (!(strTechSign.length==0)) {
                [arrOfAllSignatureImagesToSendToServer addObject:strTechSign];
            }
            
            
            for (int i=0; i<arrLeadDetailKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrLeadDetailKey objectAtIndex:i]]];
                if ([str isKindOfClass:[NSDate class]]) {
                    
                    
                }else if ([str isKindOfClass:[NSArray class]]) {
                    
                    
                }
                else{
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                }
                [arrLeadDetailValue addObject:str];
            }
            NSDictionary *dictLeadDetail;
            dictLeadDetail = [NSDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            NSLog(@"LeadDetail%@",dictLeadDetail);
            //[dictFinal setObject:dictLeadDetail forKey:@"LeadDetail"];
            
            int indexToRemove=-1;
            int indexToRemoveDate=-1;
            int indexToReplaceModifyDate=-1;
            int indexToReplaceScheduleDated=-1;
            
            for (int k=0; k<arrLeadDetailKey.count; k++) {
                
                NSString *strKeyLeadId=arrLeadDetailKey[k];
                //dateModified
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"dateModified"]) {
                    
                    indexToRemoveDate=k;
                    
                }
                
                if ([strKeyLeadId isEqualToString:@"modifiedFormatedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"zdateScheduledStart"]) {
                    
                    indexToReplaceScheduleDated=k;
                    
                }
                
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrLeadDetailKey];
            [arrKeyTemp removeObjectAtIndex:indexToRemoveDate];
            // [arrKeyTemp removeObjectAtIndex:indexToRemove-1];
            arrLeadDetailKey=arrKeyTemp;
            [arrLeadDetailValue removeObjectAtIndex:indexToRemoveDate];
            // [arrLeadDetailValue removeObjectAtIndex:indexToRemove-1];
            
            
            //Date ka bhut bada change kara hai yaha  p bhai yad rakhna [global modifyDateService]
            //  [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:[global modifyDateService]];
            
            [arrLeadDetailValue replaceObjectAtIndex:indexToReplaceScheduleDated-1 withObject:@""];
            
            // [arrKeyTemp replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:@"ModifiedDate"];
            
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrLeadDetailValue forKeys:arrLeadDetailKey];
            
            //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
            NSLog(@"%@",dictTemp);
            
            [dictFinal setObject:dictTemp forKey:@"WorkorderDetail"];
            
        }
    }
}
-(void)fetchWOProductDetail
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWOProductServiceAuto= [NSEntityDescription entityForName:@"WorkorderDetailChemicalListService" inManagedObjectContext:context];
    [request setEntity:entityWOProductServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        NSMutableArray *arrPaymentInfoValue;
        
        arrPaymentInfoValue=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:arrPaymentInfoValue forKey:@"WoProductDetail"];
        
    }else
    {
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        // matches=arrAllObj[k];
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        //            for (int k=0; k<arrAllObj.count; k++)
        //            {
        matches=arrAllObj[0];
        
        [dictFinal setObject:[matches valueForKey:@"chemicalList"] forKey:@"WoProductDetail"];
        
        [dictFinal setObject:[matches valueForKey:@"otherChemicalList"] forKey:@"WoOtherProductDetail"];
        
        //            }
        
        //        }
    }
}

-(void)fetchPaymentInfoServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"PaymentInfo"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
                
                NSString *strCheckFrontImage=[matches valueForKey:@"checkFrontImagePath"];
                NSString *strCheckBackImage=[matches valueForKey:@"checkBackImagePath"];
                
                if (!(strCheckFrontImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckFrontImage];//salesSignature
                    
                }
                if (!(strCheckBackImage.length==0)) {
                    
                    [arrOfAllCheckImageToSend addObject:strCheckBackImage];//salesSignature
                    
                }
                
                NSArray *arrPaymentInfoKey;
                NSMutableArray *arrPaymentInfoValue;
                
                arrPaymentInfoValue=[[NSMutableArray alloc]init];
                arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrPaymentInfoKey);
                
                NSLog(@"all keys %@",arrPaymentInfoValue);
                for (int i=0; i<arrPaymentInfoKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                    if([str isEqual:nil] || str.length==0 )
                    {
                        str=@"";
                    }
                    [arrPaymentInfoValue addObject:str];
                }
                
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                int indexToRemove=-1;
                int indexToReplaceModifyDate=-1;
                
                for (int k=0; k<arrPaymentInfoKey.count; k++) {
                    
                    NSString *strKeyLeadId=arrPaymentInfoKey[k];
                    
                    if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                        
                        indexToRemove=k;
                        
                    }
                    if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                        
                        indexToReplaceModifyDate=k;
                        
                    }
                }
                
                NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
                
                [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
                
                [arrKeyTemp removeObjectAtIndex:indexToRemove];
                arrPaymentInfoKey=arrKeyTemp;
                [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
                
                [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
                NSLog(@"PaymentInfo%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"PaymentInfo"];
            }
            
        }
    }
}


-(void)fetchTermiteFromDb{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetails;
    
    appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context1 = [appDelegate1 managedObjectContext];
    
    entityWorkOrder=[NSEntityDescription entityForName:@"TexasTermiteServiceDetail" inManagedObjectContext:context1];
    requestNewWorkOrder = [[NSFetchRequest alloc] init];
    [requestNewWorkOrder setEntity:entityWorkOrder];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestNewWorkOrder setPredicate:predicate];
    
    sortDescriptorWorkOrder = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsWorkOrder = [NSArray arrayWithObject:sortDescriptorWorkOrder];
    
    [requestNewWorkOrder setSortDescriptors:sortDescriptorsWorkOrder];
    
    fetchedResultsControllerWorkOrderDetails = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewWorkOrder managedObjectContext:context1 sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetails setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetails performFetch:&error];
    arrAllObjWorkOrder = [fetchedResultsControllerWorkOrderDetails fetchedObjects];
    if ([arrAllObjWorkOrder count] == 0)
    {
        
        //        NSMutableArray *arrTexasTermiteServiceDetail;
        //
        //        arrTexasTermiteServiceDetail=[[NSMutableArray alloc]init];
        
        [dictFinal setObject:@"" forKey:@"TexasTermiteServiceDetail"];
        
        
    }
    else
    {
        matchesWorkOrder=arrAllObjWorkOrder[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"];
        
        
        NSString *strBuyersInitials_SignaturePath_10B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_10B"]];
        if (!(strBuyersInitials_SignaturePath_10B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_10B];
            
        }
        
        
        NSString *strGraphPath__TexasOffiicial_WoodDestroyingInsect=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"GraphPath__TexasOffiicial_WoodDestroyingInsect"]];
        if (!(strGraphPath__TexasOffiicial_WoodDestroyingInsect.length==0)){
            
            [arrOfAllImagesToSendToServer addObject:strGraphPath__TexasOffiicial_WoodDestroyingInsect];
            
            
        }
        
        
        NSString *strInspector_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"Inspector_SignaturePath"]];
        if (!(strInspector_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strInspector_SignaturePath];
            
        }
        
        
        NSString *strCertifiedApplicator_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"CertifiedApplicator_SignaturePath"]];
        if (!(strCertifiedApplicator_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strCertifiedApplicator_SignaturePath];
            
        }
        
        
        NSString *strSignatureOfPurchaserOfProperty_SignaturePath=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfPurchaserOfProperty_SignaturePath"]];
        if (!(strSignatureOfPurchaserOfProperty_SignaturePath.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureOfPurchaserOfProperty_SignaturePath];
            
        }
        
        
        NSString *strBuyersInitials_SignaturePath_7B=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"BuyersInitials_SignaturePath_7B"]];
        if (!(strBuyersInitials_SignaturePath_7B.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strBuyersInitials_SignaturePath_7B];
            
        }
        
        [dictFinal setObject:[matchesWorkOrder valueForKey:@"texasTermiteServiceDetail"] forKey:@"TexasTermiteServiceDetail"];
        
    }
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}
-(void)fetchTermiteFloridaFromDb
{
    
    NSFetchedResultsController *fetchedResultsControllerWorkOrderDetailFlorida;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    entityTermiteFlorida=[NSEntityDescription entityForName:@"FloridaTermiteServiceDetail" inManagedObjectContext:context];
    requestTermiteFlorida = [[NSFetchRequest alloc] init];
    [requestTermiteFlorida setEntity:entityTermiteFlorida];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestTermiteFlorida setPredicate:predicate];
    
    sortDescriptorTermiteFlorida = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsTermiteFlorida = [NSArray arrayWithObject:sortDescriptorTermiteFlorida];
    
    [requestTermiteFlorida setSortDescriptors:sortDescriptorsTermiteFlorida];
    
    fetchedResultsControllerWorkOrderDetailFlorida = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTermiteFlorida managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsControllerWorkOrderDetailFlorida setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [fetchedResultsControllerWorkOrderDetailFlorida performFetch:&error];
    arrAllObjTermiteFlorida = [fetchedResultsControllerWorkOrderDetailFlorida fetchedObjects];
    if ([arrAllObjTermiteFlorida count] == 0)
    {
        
        [dictFinal setObject:@"" forKey:@"FloridaTermiteServiceDetail"];
        
    }
    else
    {
        
        matchesWorkOrderFlorida=arrAllObjTermiteFlorida[0];
        
        NSMutableDictionary *dictGlobalTermiteTexas;
        
        dictGlobalTermiteTexas=[[NSMutableDictionary alloc]init];
        
        dictGlobalTermiteTexas=(NSMutableDictionary*)[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"];
        
        NSString *strSignatureLicenseAgrent=[NSString stringWithFormat:@"%@",[dictGlobalTermiteTexas valueForKey:@"SignatureOfLicenseOrAgent"]];
        if (!(strSignatureLicenseAgrent.length==0)) {
            
            [arrOfAllImagesToSendToServer addObject:strSignatureLicenseAgrent];
            
        }
        
        [dictFinal setObject:[matchesWorkOrderFlorida valueForKey:@"floridaTermiteServiceDetail"] forKey:@"FloridaTermiteServiceDetail"];
        
    }
    
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
    }
}

-(void)fetchImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"ImagesDetail"];
        
    }
}


-(void)fetchWorkOrderDocuments{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOrderDocuments= [NSEntityDescription entityForName:@"WoOtherDocuments" inManagedObjectContext:context];
    [request setEntity:entityWorkOrderDocuments];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptorWorkOrderDocuments = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptorsWorkOrderDocuments = [NSArray arrayWithObject:sortDescriptorWorkOrderDocuments];
    
    [request setSortDescriptors:sortDescriptorsWorkOrderDocuments];
    
    self.fetchedResultsWorkOrderDocuments = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsWorkOrderDocuments setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsWorkOrderDocuments performFetch:&error1];
    arrAllObjWorkOrderDocuments = [self.fetchedResultsWorkOrderDocuments fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObjWorkOrderDocuments.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }else
    {
        
        for (int k=0; k<arrAllObjWorkOrderDocuments.count; k++)
        {
            matchesWorkOrderDocuments=arrAllObjWorkOrderDocuments[k];
            NSLog(@"Lead IDDDD====%@",[matchesWorkOrderDocuments valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesWorkOrderDocuments entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesWorkOrderDocuments valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"WoOtherDocuments%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"WoOtherDocuments"];
        
    }
}

-(void)fetchEmailDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrEmailDetail addObject:dictPaymentInfo];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"EmailDetail"];
        
    }
}
-(void)fetchEquipmentDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityEmailDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
    [request setEntity:entityEmailDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"WoEquipmentDetail"];
        
    }else
    {
        
        //        for (int k=0; k<arrAllObj.count; k++)
        //        {
        matches=arrAllObj[0];
        NSArray *arrToSend=[matches valueForKey:@"arrOfEquipmentList"];
        
        NSArray *objValue=[NSArray arrayWithObjects:
                           arrToSend,
                           arrToSend,nil];
        NSArray *objKey=[NSArray arrayWithObjects:
                         @"WOEquipmentDcs",
                         @"WOEquipmentDc",nil];
        
        NSMutableDictionary *dictTemp;
        dictTemp=[[NSMutableDictionary alloc]init];
        dictTemp=[NSMutableDictionary dictionaryWithObjects:objValue forKeys:objKey];
        
        //  [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictLeadDetail objectForKey: @"descriptionLeadDetail"]] forKey: @"description"];//
        //  [dictTemp removeObjectForKey:@"descriptionLeadDetail"];
        NSLog(@"%@",dictTemp);
        [dictFinal setObject:arrToSend forKey:@"WoEquipmentDetail"];
        
        //        }
    }
}

#pragma mark- FINAL JSON SENDING

-(void)finalJson
{
    for (int i=0; i<arrCheckImage.count; i++)
    {
        [self uploadImageCheckImage:[arrCheckImage objectAtIndex:i]];
    }
    for (int i=0; i<arrAllTargetImages.count; i++)
    {
        [self uploadTargetImage:[arrAllTargetImages objectAtIndex:i]];
    }
    if ([strElectronicSign isKindOfClass:[NSString class]])
    {
        [self uploadSignatureImageOnServer:strElectronicSign];
        
    }
    //For OtherDocument Upload
    [self fetchOtherDocument];
    for (int i=0; i<arrUploadOtherDocument.count; i++)
    {
        [self uploadOtherDocuments:[arrUploadOtherDocument objectAtIndex:i] DocumentId:[arrUploadOtherDocumentId objectAtIndex:i]];
        //[self uploadOtherDocumentImage:[arrUploadOtherDocument objectAtIndex:i]];
    }
    [self finalJsonSalesAfterCheckImageSync];
    
}
-(void)finalJsonSalesAfterCheckImageSync
{
    if ([dictFinal isKindOfClass:[NSString class]])
    {
        
        indexDynamicFormToSend++;
        
        [self sendingDynamicFormDataToServer:indexDynamicFormToSend];
        
    }
    else
    {
        
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [DejalBezelActivityView removeView];
        }
        else
        {
            //NSString *strrr=[NSString stringWithFormat:@"Final Lead Sending"];
            // [self HudView:strrr];
            
            // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];
            
            //Nilind 27 Sept
            NSError *errorNew = nil;
            NSData *json;
            NSString *jsonString;
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:dictFinal])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
                
                // If no errors, let's view the JSON
                if (json != nil && errorNew == nil)
                {
                    jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    NSLog(@"Update Task JSON: %@", jsonString);
                }
            }
            
            NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesFinalUpdate];
            
            // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending..."];
            
            //============================================================================
            //============================================================================
            NSDictionary *dictTemp;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSLog(@"Dict Response for Sales Seding Lead===%@",response);
                         
                         indexDynamicFormToSend++;
                         
                         [self sendingDynamicFormDataToServer:indexDynamicFormToSend];
                         
                         if (success)
                         {
                             
                             //Yaha Par zSyn ko no karna hai
                             
                             [objSalesLeadIdGlobal setValue:@"no" forKey:@"zSync"];
                             [objSalesLeadIdGlobal setValue:@"false" forKey:@"isMailSend"];

                             NSError *error;
                             [context save:&error];
                             
                             
                             // Save target in DB
                             
                             NSArray *arrTargets=[response valueForKey:@"leadCommercialTargetExtDcs"];
                             
                             if ([arrTargets isKindOfClass:[NSArray class]]){
                                 
                                 NSString *strTempLeadId=[NSString stringWithFormat:@"%@",[response valueForKey:@"LeadId"]];
                                 
                                 WebService *objWebService = [[WebService alloc] init];
                                 
                                 [objWebService savingTargetsWithArrTarget:arrTargets strLeadId:strTempLeadId];
                                 
                             }
                             
                             //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Info" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                             //                         [alert show];
                             //                         [self salesFetch];
                             //                         // [self goToAddTAskViewNew];
                             //                         NSLog(@"Response on Add Update Method ========= %@",response);
                         }
                         else
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                         }
                     });
                 }];
            });
            
            //============================================================================
            //============================================================================
        }
    }
    
}
-(void)finalJsonServiceAutomation
{
    ///api/MobileToSaleAuto/UpdateLeadDetail
    if ([dictFinal isKindOfClass:[NSString class]]) {
        
        NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
        
        BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
        
        if (isEquipEnabled) {
            
            [self FetchFromCoreDataToSendServiceDynamicEquipment :YES];
            
        }else{
            
            indexDynamicFormToSend++;
            
            [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
            
        }
    }else{
        Reachability *reachableForWiFy1=[Reachability reachabilityForInternetConnection];
        NetworkStatus netStatusWify1 = [reachableForWiFy1 currentReachabilityStatus];
        if (netStatusWify1== NotReachable)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No internet connection " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [DejalBezelActivityView removeView];
        }
        else
        {
            
            NSDictionary *dictServiceAddress=[dictFinal valueForKey:@"WorkorderDetail"];
            
            if ([dictServiceAddress isKindOfClass:[NSDictionary class]]) {

                NSString *strImageNameserviceAddress=[dictServiceAddress valueForKey:@"serviceAddressImagePath"];
                
                if (!(strImageNameserviceAddress.length==0)) {
                    
                    [self uploadImageServiceAddImage:strImageNameserviceAddress];
                    
                }
                
            }

            //Nilind upload Termite Image
            NSArray *arrTermiteImage=[dictFinal valueForKey:@"TexasTermiteImagesDetail"];
            for (int i=0; i<arrTermiteImage.count; i++)
            {
                NSDictionary *dict=[arrTermiteImage objectAtIndex:i];
                NSString *strImageNameTermite=[dict valueForKey:@"woImagePath"];
                if (!(strImageNameTermite.length==0)) {
                    
                    [self uploadImageTermite:strImageNameTermite];
                    
                }
            }
            
            
            
            // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Logging In..."];
            
            //Nilind 27 Sept
            NSError *errorNew = nil;
            NSData *json;
            NSString *jsonString;
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:dictFinal])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:dictFinal options:NSJSONWritingPrettyPrinted error:&errorNew];
                
                // If no errors, let's view the JSON
                if (json != nil && errorNew == nil)
                {
                    jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    NSLog(@"Update Final Sevice json JSON: %@", jsonString);
                }
            }
            
            NSData *requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlUpdateWorkorderDetail];
            
            //strUrl = [NSString stringWithFormat:@"%@%@",@"http://192.168.0.83",UrlUpdateWorkorderDetail];
            
            // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending..."];
            
            //============================================================================
            //============================================================================
            NSDictionary *dictTemp;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForSendLeadToServer:strUrl :requestData :dictTemp :@"" :@"" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                         
                         BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                         
                         if (isEquipEnabled) {
                             
                             NSString *strResult=[NSString stringWithFormat:@"%@",[response valueForKey:@"Result"]];
                             
                             NSString *strResultWorkOrderId=[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]];
                             
                             BOOL isUpdatedd=YES;
                             
                             if ([strResult isEqualToString:strResultWorkOrderId]) {
                                 
                                 isUpdatedd=YES;
                                 
                             } else {
                                 
                                 isUpdatedd=NO;
                                 
                             }
                             
                             [self FetchFromCoreDataToSendServiceDynamicEquipment :isUpdatedd];
                             
                             
                         }else{
                             
                             NSLog(@"Dict Response for ServiceAutomation Sending Lead===%@",response);
                             
                             indexDynamicFormToSend++;
                             
                             [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
                             
                         }
                         
                         
                         if (success)
                         {
                             
                             NSString *strResult=[NSString stringWithFormat:@"%@",[response valueForKey:@"Result"]];
                             
                             NSString *strResultWorkOrderId=[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]];
                             
                             if ([strResult isEqualToString:strResultWorkOrderId]) {
                                 
                                 //Yaha Par zSyn ko no karna hai
                                 
                                 [objServiceWorkOrderGlobal setValue:@"no" forKey:@"zSync"];
                                 NSError *error;
                                 [context save:&error];
                                 
                                 NSLog(@"ZSYNC on Appointment = = = = %@",objServiceWorkOrderGlobal);
                                 
                             }
                             
                             // To Save For Service Pest Flow Area n All In DB
                             
                             if ([[response valueForKey:@"RegularflowResults"] isKindOfClass:[NSDictionary class]]) {
                                 
                                 NSDictionary *dictRegularPestFlow=[response valueForKey:@"RegularflowResults"];

                                 WebService *objWebService = [[WebService alloc] init];
                                 
                                 [objWebService savingServicePestAllDataDBWithDictWorkOrderDetail:dictRegularPestFlow strWoId:[NSString stringWithFormat:@"%@",[response valueForKey:@"WorkOrderId"]] strType:@"WithoutWoDetail" strToDeleteAreasDevices:@"Yes" ];

                             }

                         }
                         else
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                             [DejalBezelActivityView removeView];
                         }
                     });
                 }];
            });
            
            //============================================================================
            //============================================================================
        }
    }
}

// Nilind 21 Sept
//============================================================================
//============================================================================
#pragma mark- Upload Image METHOD
//============================================================================
//============================================================================

-(void)uploadImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            
            //NSString *strrr=[NSString stringWithFormat:@"Image Sales %@",strImageName];
            // [self HudView:strrr];
            
        }
        
    }
    indexToSendImage++;
    [self uploadingAllImages:indexToSendImage];
}
-(void)uploadImageSignature :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesSignatureImageUpload];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            //NSString *strrr=[NSString stringWithFormat:@"Image Sales Signature %@",strImageName];
            //[self HudView:strrr];
            
        }
        
    }
    
    indexToSendImageSign++;
    [self uploadingAllImagesSignature:indexToSendImageSign];
}
#pragma mark- UPLOAD CHECK IMAGE
//============================================================================
//============================================================================
-(void)uploadImageCheckImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
            NSString *strCheckUrl;
            strCheckUrl=@"/api/File/UploadCheckImagesAsync";
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,strCheckUrl];//UrlSalesImageUpload];//
            
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Image Sent");
            }
            //[self HudView:strrr];
            
        }
        
    }
    
    
}

-(void)uploadImageServiceAutomation :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
            
        }
        
    }
    indexToSendImage++;
    [self uploadingAllImagesServiceAuto:indexToSendImage];
}
-(void)uploadImageServiceAutomationCheck :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
            
        }
        
    }
    indexToSendImageSign++;
    [self uploadingAllImagesServiceAutoCheck:indexToSendImageSign];
}
-(void)uploadImageServiceAutomationSignatures :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        
        }
    }
    
    indexToSendCheckImages++;
    [self uploadingAllImagesServiceAutoSignatures:indexToSendCheckImages];
}
//============================================================================
//============================================================================
#pragma mark- Upload Audio METHOD
//============================================================================
//============================================================================


-(void)uploadAudio :(NSString*)strAudioName :(NSString*)ServerUrl
{
    
    if ((strAudioName.length==0) && [strAudioName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",ServerUrl,UrlAudioUploadAsync];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Audio Sent");
        }
        NSLog(@"Audio Sent Return Message is = = = %@",returnString);
    }
    [self finalJsonServiceAutomation];
}


-(void)uploadAudioSales :(NSString*)strAudioName :(NSString*)ServerUrl
{
    
    if ((strAudioName.length==0) && [strAudioName isEqualToString:@"(null)"]) {
        
    } else {
        NSRange equalRange = [strAudioName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strAudioName = [strAudioName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strAudioName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",ServerUrl,UrlAudioUploadAsync];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strAudioName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Audio Sent");
        }
        NSLog(@"Audio Sent Return Message is = = = %@",returnString);
    }
    
    [self finalJson];
    
}

-(void)sendingDynamicFormDataToServer :(int)indextoSend{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendSalesLeadToServer"];
    if (arrOfLeadsTosendToServer.count==indextoSend) {
        
        
        //        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alert show];
        
        //yaha change karna hai
        indexDynamicFormToSend=0;
        
        //[self salesFetch];
        
        if (isServiceAutoEnabled) {
            
            [self downloadTotalWorkOrdersServiceAutomation];
            
        } else {
            
            [self salesFetch];
            [DejalBezelActivityView removeView];
            
        }
        
    } else {
        
        strLeadIdGlobal=arrOfLeadsTosendToServer[indextoSend];
        [self FetchFromCoreDataToSendSalesDynamic];
        
    }
}

-(void)callMethodAfterSyncingDeviceInspection{
    
    [DejalBezelActivityView removeView];
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Appointments synced successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    if (arrOfSalesLeadToFetchDynamicData.count>0) {
        
        [self performSelector:@selector(getDynamicFormDataSales) withObject:nil afterDelay:0.5];
        
    }else{
        
        if (arrOfServiceLeadToFetchDynamicData.count>0) {
            
            [self getDynamicFormDataService];
            
        }else{
            
            // [self SalesAutomationFetchAfterSyncingData];
            
        }
        
    }
    
    [self salesFetch];
    
}

-(void)sendingServiceDynamicFormDataToServer :(int)indextoSend{
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServer"];
    if (arrOfLeadsTosendToServer.count==indextoSend) {
        
        
        indexToSendDeviceDynamicForm = 0;
        
        [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
        
        /*
        [DejalBezelActivityView removeView];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Appointments synced successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        if (arrOfSalesLeadToFetchDynamicData.count>0) {
            
            [self performSelector:@selector(getDynamicFormDataSales) withObject:nil afterDelay:0.5];
            
        }else{
            
            if (arrOfServiceLeadToFetchDynamicData.count>0) {
                
                [self getDynamicFormDataService];
                
            }else{
                
                // [self SalesAutomationFetchAfterSyncingData];
                
            }
            
        }
        
        [self salesFetch];
        */
        
    } else {
        
        strWorkOrderIdGlobal=arrOfLeadsTosendToServer[indextoSend];
        
        [self FetchFromCoreDataToSendServiceDynamic];
        
    }
}


//============================================================================
//============================================================================
#pragma mark- Dynamic Form Send Method
//============================================================================
//============================================================================
-(void)FetchFromCoreDataToSendSalesDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNewSalesDynamic = [[NSFetchRequest alloc] init];
    [requestNewSalesDynamic setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadIdGlobal,strUsername];
    
    [requestNewSalesDynamic setPredicate:predicate];
    
    sortDescriptorSalesDynamic = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptorsSalesDynamic = [NSArray arrayWithObject:sortDescriptorSalesDynamic];
    
    [requestNewSalesDynamic setSortDescriptors:sortDescriptorsSalesDynamic];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewSalesDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    arrAllObjSalesDynamic = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrAllObjSalesDynamic count] == 0)
    {
        [self sendingSalesFinalJsonToServerNew:indexDynamicFormToSend];
    }
    else
    {
        
        matchesSalesDynamic=arrAllObjSalesDynamic[0];
        NSArray *arrTemp = [matchesSalesDynamic valueForKey:@"arrFinalInspection"];
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrTemp];
        [self sendingFinalDynamicJsonToServer];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)FetchFromCoreDataToSendServiceDynamic{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderIdGlobal,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderIdGlobal];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        //yaha change Equipment
        //        NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
        //
        //        BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
        //
        //        if (isEquipEnabled) {
        //
        //            [self FetchFromCoreDataToSendServiceDynamicEquipment];
        //
        //        }else{
        
        [self sendingSalesFinalJsonToServerNewServiceAuto:indexDynamicFormToSend];
        
        //        }
    }
    else
    {
        matchesServiceDynamic=arrAllObjServiceDynamic[0];
        NSDictionary *dictDataServiceForm;
        NSArray *arrTemp;
        if ([[matchesServiceDynamic valueForKey:@"arrFinalInspection"] isKindOfClass:[NSArray class]]) {
            
            arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
            // changes for crashing on nil array
            if (arrTemp.count>0) {
                
                dictDataServiceForm=arrTemp[0];
                
            }
            
        } else if ([[matchesServiceDynamic valueForKey:@"arrFinalInspection"] isKindOfClass:[NSDictionary class]]) {
            
            dictDataServiceForm = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
        }
        
        arrResponseInspection =[[NSMutableArray alloc]init];
        [arrResponseInspection addObjectsFromArray:arrTemp];
        
        [self sendingFinalDynamicJsonToServerService :dictDataServiceForm];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)FetchFromCoreDataToSendServiceDynamicEquipment :(BOOL)isUpdated{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityServiceDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNewServiceDynamic = [[NSFetchRequest alloc] init];
    [requestNewServiceDynamic setEntity:entityServiceDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    // NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderIdGlobal,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestNewServiceDynamic setPredicate:predicate];
    
    sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
    
    [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
    
    self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerServiceDynamic performFetch:&error];
    arrAllObjServiceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
    if ([arrAllObjServiceDynamic count] == 0)
    {
        
        indexDynamicFormToSend++;
        
        [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
        
        
    }
    else
    {
        
        NSMutableArray *arrOfEquipToSend=[[NSMutableArray alloc] init];
        
        for (int k=0; k<arrAllObjServiceDynamic.count; k++) {
            
            matchesServiceDynamic=arrAllObjServiceDynamic[k];
            NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
            
            NSDictionary *dictDataService;
            
            if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                
                dictDataService=(NSDictionary*)arrTemp;
                
            } else if ([arrTemp isKindOfClass:[NSArray class]]) {
                
                // changes for crashing on nil array
                if (arrTemp.count>0) {
                    dictDataService=arrTemp[0];
                }
                
            }
            
            [arrOfEquipToSend addObject:dictDataService];
            
            
        }
        
        if (arrOfEquipToSend.count==0) {
            
            indexDynamicFormToSend++;
            
            [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
            
            
        } else {
            
            //Condition For Not Sending EquipmentDynamic Form To Server if workorder response is failed
            
            
            if (isUpdated) {
                
                [self sendingFinalDynamicJsonToServerEquipments:arrOfEquipToSend];
                
            } else {
                
                indexDynamicFormToSend++;
                [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
                
            }
            
            
        }
        
        //        arrResponseInspection =[[NSMutableArray alloc]init];
        //        [arrResponseInspection addObjectsFromArray:arrTemp];
        //
        //        [self sendingFinalDynamicJsonToServerEquipments :dictDataServiceForm];
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)sendingFinalDynamicJsonToServer{
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrResponseInspection,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"InspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL FORM JSON In Appointment View: %@", jsonString);
        }
    }
    
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesInspectionDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Syncing Data..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 //  [self HudView:@"Response Sales Dynamic Form ka"];
                 
                 [self sendingSalesFinalJsonToServerNew:indexDynamicFormToSend];
                 
                 if (success)
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         
                         [matchesSalesDynamic setValue:@"Yes" forKey:@"isSentToServer"];
                         NSError *error1;
                         [context save:&error1];
                         
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"Data Synced Successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }else{
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong,Some data didn't synced properly.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}
-(void)sendingFinalDynamicJsonToServerService :(NSDictionary*)dictFinalDynamicJson{
    
    
    //  NSDictionary *dictFinalDynamicJson=(NSDictionary*)arrTemp;
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServiceDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     [matchesServiceDynamic setValue:@"Yes" forKey:@"isSentToServer"];
                     NSError *error1;
                     [context save:&error1];
                     
                     //                     NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
                     //
                     //                     BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
                     //
                     //                     if (isEquipEnabled) {
                     //
                     //                         [self FetchFromCoreDataToSendServiceDynamicEquipment];
                     //
                     //                     }else{
                     
                     [self sendingSalesFinalJsonToServerNewServiceAuto:indexDynamicFormToSend];
                     
                     //                     }
                     
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     if ([str containsString:@"Success"]) {
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                     }else{
                         
                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         //                         [alert show];
                         
                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

-(void)sendingFinalDynamicJsonToServerEquipments :(NSArray*)arrFinalDynamicJson{
    
    
    NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                       arrFinalDynamicJson,nil];
    
    NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                     @"EquipmentInspectionFormsData",nil];
    
    NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];
    
    NSError *errorNew = nil;
    NSData *json;
    NSString *jsonString;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dictFinalDynamicJson])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dictFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
        
        // If no errors, let's view the JSON
        if (json != nil && errorNew == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"FINAL Service FORM JSON: %@", jsonString);
        }
    }
    
    NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
    
    NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
    
    //http://tdps.stagingsoftware.com/
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlEquipmentDynamicFormSubmission];
    
    //  NSString *strUrl = [NSString stringWithFormat:@"%@%@",@"http://tdps.stagingsoftware.com/",UrlSalesInspectionDynamicFormSubmission];
    
    // [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sending To Server..."];
    
    //============================================================================
    //============================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (success)
                 {
                     
                     indexDynamicFormToSend++;
                     
                     [self sendingServiceDynamicFormDataToServer:indexDynamicFormToSend];
                     
                     //                     [matchesServiceDynamic setValue:@"Yes" forKey:@"isSentToServer"];
                     //                     NSError *error1;
                     //                     [context save:&error1];
                     //
                     //                     [self sendingSalesFinalJsonToServerNewServiceAuto:indexDynamicFormToSend];
                     //
                     //                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"ReturnMsg"]];
                     //                     if ([str containsString:@"Success"]) {
                     //                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Success" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         //                         [alert show];
                     //                     }else{
                     //
                     //                         //                         UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Sorry something went wrong.Please try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     //                         //                         [alert show];
                     //
                     //                     }
                 }
                 else
                 {
                     NSString *strTitle = Alert;
                     NSString *strMsg = Sorry;
                     [global AlertMethod:strTitle :strMsg];
                 }
             });
         }];
    });
    
    //============================================================================
    //============================================================================
}

//============================================================================
#pragma mark- Hud Viewwwww
//============================================================================


-(void)HudView :(NSString*)strTextToShow{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = strTextToShow;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            hud.label.font=[UIFont systemFontOfSize:28];
            hud.margin = 10.f;//10.f
            hud.yOffset = 350.f;//150.f
        }
        else if ([UIScreen mainScreen].bounds.size.height==1366)
        {
            hud.label.font=[UIFont systemFontOfSize:32];
            hud.margin = 10.f;//10.f
            hud.yOffset = 500.f;//150.f
        }
        
        
    }
    else
    {
        if([UIScreen mainScreen].bounds.size.height==736)
        {
            hud.margin = 10.f;
            hud.yOffset = 250.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==667)
        {
            hud.margin = 10.f;
            hud.yOffset = 230.f;
        }
        else if ([UIScreen mainScreen].bounds.size.height==568)
        {
            hud.margin = 10.f;
            hud.yOffset = 200.f;
        }
        else
        {
            hud.margin = 10.f;
            hud.yOffset = 150.f;
        }
        
    }
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:2];
    
}

//============================================================================
//============================================================================
#pragma mark- Advance Search Method
//============================================================================
//============================================================================

-(void)advanceSearchButton{
    
    UIView *view_AddTask=[[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-80,[UIScreen mainScreen].bounds.size.height-120,60,60)];
    view_AddTask.layer.cornerRadius = view_AddTask.frame.size.width/2;
    view_AddTask.clipsToBounds = YES;
    view_AddTask.backgroundColor=[UIColor colorWithRed:115/255.0f green:176/255.0f blue:175/255.0f alpha:1];
    [self.view addSubview:view_AddTask];
    
    UIButton *buttonAddTask=[[UIButton alloc]initWithFrame:CGRectMake(13, 12, 35, 35)];
    [buttonAddTask addTarget:self action:@selector(goToAddTask:) forControlEvents:UIControlEventTouchDown];
    [buttonAddTask setImage:[UIImage imageNamed:@"advanceFilter.png"] forState:UIControlStateNormal];
    [view_AddTask addSubview:buttonAddTask];
    
}
-(void)goToAddTask:(id)sender
{
    
    if (yesFiltered) {
        
        if ([_txt_LastName.text isEqualToString:@"#`%&*"]) {
            
            _txt_LastName.text=@"";
            
        }
        if ([_txt_AccountNo.text isEqualToString:@"#`%&*"]) {
            
            _txt_AccountNo.text=@"";
            
        }
        if ([_txt_FirstName.text isEqualToString:@"#`%&*"]) {
            
            _txt_FirstName.text=@"";
            
        }
        if ([_txt_WorkOrderNo.text isEqualToString:@"#`%&*"]) {
            
            _txt_WorkOrderNo.text=@"";
            
        }
        if ([_Txt_Leadd.text isEqualToString:@"#`%&*"]) {
            
            _Txt_Leadd.text=@"";
            
        }
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        frameFor_view_CustomerInfo.origin.x=0;
        frameFor_view_CustomerInfo.origin.y=20;
        
        [_view_FilterCriteria setFrame:frameFor_view_CustomerInfo];
        //  _view_FilterCriteria.backgroundColor=[UIColor lightGrayColor];
        //  _scrollView_Filter.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-75);
        // _scrollView_Filter.backgroundColor=[UIColor redColor];
        [self.view addSubview:_view_FilterCriteria];
        
    } else {
        
        [_searchBarr resignFirstResponder];
        _searchBarr.hidden=YES;
        yesFiltered=NO;
        [_btn_Refreshh setImage:[UIImage imageNamed:@"Command-RefreshGroup.png"] forState:UIControlStateNormal];
        //   [_tblViewAppointment reloadData];
        
        isTodayAppointments=NO;
        [_lblLineAll setHidden:NO];
        [_lblLineToday setHidden:YES];
        yesFiltered=NO;
        [self salesFetch];
        
        arrDataTblView=[[NSMutableArray alloc]init];
        
        NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
        
        NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
        
        [arrDataTblView addObject:@"All"];
        
        for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
            NSDictionary *dictData=arrOfLeadStatusMasters[k];
            [arrDataTblView addObject:[dictData valueForKey:@"StatusName"]];
        }
        if (arrDataTblView.count==0) {
            
            [_btn_StatusFilter setTitle:@"Choose Status" forState:UIControlStateNormal];
            
        } else {
            
            [_btn_StatusFilter setTitle:arrDataTblView[0] forState:UIControlStateNormal];
            
        }
        _txt_LastName.text=@"";
        _txt_AccountNo.text=@"";
        _txt_FirstName.text=@"";
        _txt_WorkOrderNo.text=@"";
        _Txt_Leadd.text=@"";
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        strDate = [dateFormat stringFromDate:[NSDate date]];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDateComponents *components=[[NSDateComponents alloc] init];
        components.day=1;
        NSDate *targetDatePlus =[calendar dateByAddingComponents:components toDate:[NSDate date] options: 0];
        NSString *strToDate = [dateFormat stringFromDate:targetDatePlus];
        
        [_btn_ToDateFilter setTitle:strToDate forState:UIControlStateNormal];
        [_btn_FromDateFilter setTitle:strDate forState:UIControlStateNormal];
        strGlobalToDate=strToDate;
        strGlobalFromDate=strDate;
        
        CGRect frameFor_view_CustomerInfo=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        frameFor_view_CustomerInfo.origin.x=0;
        //        frameFor_view_CustomerInfo.origin.y=20;// commented by Akshay 23 Aug 2018
        frameFor_view_CustomerInfo.origin.y=35;// Akshay 23 Aug 2018
        
        
        [_view_FilterCriteria setFrame:frameFor_view_CustomerInfo];
        //  _view_FilterCriteria.backgroundColor=[UIColor lightGrayColor];
        //  _scrollView_Filter.contentSize=CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-75);
        // _scrollView_Filter.backgroundColor=[UIColor redColor];
        [self.view addSubview:_view_FilterCriteria];
        
    }
    
}
-(void)resignAllTxtFields{
    
    [_txt_FirstName resignFirstResponder];
    [_txt_LastName resignFirstResponder];
    [_txt_AccountNo resignFirstResponder];
    [_txt_WorkOrderNo resignFirstResponder];
    [_Txt_Leadd resignFirstResponder];
    
}

- (IBAction)action_StatusFlter:(id)sender {
    
    [self resignAllTxtFields];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    arrDataTblView=[[NSMutableArray alloc]init];
    
    NSUserDefaults *defsLead=[NSUserDefaults standardUserDefaults];
    
    NSArray *arrOfLeadStatusMasters=[defsLead valueForKey:@"LeadStatusMasters"];
    
    [arrDataTblView addObject:@"All"];
    
    for (int k=0;k<arrOfLeadStatusMasters.count; k++) {
        NSDictionary *dictData=arrOfLeadStatusMasters[k];
        [arrDataTblView addObject:[dictData valueForKey:@"StatusName"]];
    }
    
    [arrDataTblView addObject:@"InComplete"];
    [arrDataTblView addObject:@"Reset"];
    //Nilind 24 Feb
    //[arrDataTblView addObject:@"Completed"];
    //End
    
    
    
    if (arrDataTblView.count==0) {
        [global AlertMethod:Info :NoDataAvailable];
    }else{
        tblData.tag=102;
        [self tableLoad:tblData.tag];
    }
    
}
-(void)tableLoad:(NSInteger)btntag
{
    
    NSInteger i;
    i=btntag;
    switch (i)
    {
        case 101:
        {
            [self setTableFrame];
            break;
        }
        case 102:
        {
            [self setTableFrame];
            break;
        }
            
        default:
            break;
    }
    //  [tblData setContentOffset:CGPointZero animated:YES];
    tblData.dataSource=self;
    tblData.delegate=self;
    tblData.backgroundColor=[UIColor whiteColor];
    [tblData reloadData];
    
}

-(void)setTableFrame
{
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    tblData.frame=CGRectMake(20, 185, [UIScreen mainScreen].bounds.size.width-40, 300);
    if ([UIScreen mainScreen].bounds.size.height==480||[UIScreen mainScreen].bounds.size.height==568 )
    {
        tblData.frame=CGRectMake(40, 140, [UIScreen mainScreen].bounds.size.width-80, 200);
    }
    [self.view addSubview:viewBackGround];
    [viewBackGround addSubview:tblData];
    
}

- (IBAction)action_FromDateFilter:(id)sender {
    
    [self resignAllTxtFields];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=108;
    [self addPickerViewDateTo :strGlobalFromDate];
    /*
     
     NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
     NSDate *currentDate = [NSDate date];
     NSDateComponents *comps = [[NSDateComponents alloc] init];
     [comps setYear:30];
     NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
     [comps setYear:-30];
     NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
     
     [datePicker setMaximumDate:maxDate];
     [datePicker setMinimumDate:minDate];
     
     */
    
    
}
- (IBAction)action_ToDateFilter:(id)sender {
    
    [self resignAllTxtFields];
    
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    [tblData removeFromSuperview];
    
    tblData.tag=109;
    [self addPickerViewDateTo :strGlobalToDate];
    
    
}

- (IBAction)action_ApplyFilter:(id)sender
{
    isSearchBarFilter=NO;
    [self resignAllTxtFields];
    
    /*  NSDateFormatter* dateFormatter12 = [[NSDateFormatter alloc] init];
     [dateFormatter12 setTimeZone:[NSTimeZone localTimeZone]];
     [dateFormatter12 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     context = [appDelegate managedObjectContext];
     [self allEntriesInContext:context fromDate:[dateFormatter12 dateFromString:_btn_FromDateFilter.titleLabel.text] toDate:[dateFormatter12 dateFromString:_btn_ToDateFilter.titleLabel.text]];*/
    
    if (_txt_WorkOrderNo.text.length>0 && _Txt_Leadd.text.length>0) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter either WO# Or Lead#" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]; //Opportunity
        [alert show];
        
        
    } else {
        
        [self applyFilterIfThr];
        
        [_view_FilterCriteria removeFromSuperview];
        
    }
}

-(void)applyFilterIfThr{
    
    yesFiltered=YES;
    
    filteredArray=[[NSMutableArray alloc]init];
    
    for(int i=0;i<[arrAllObj count];i++){
        
        NSManagedObject *objWOSales=[arrAllObj objectAtIndex:i];
        NSString *str11;
        NSString *str22;
        NSString *str33;
        NSString *str111;
        NSString *str222;
        NSString *strDateToMatch;
        
        NSDate *startDateFromString = [[NSDate alloc] init];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        
        [dateFormatter1 setTimeZone:[NSTimeZone localTimeZone]];
        
        if ([objWOSales isKindOfClass:[LeadDetail class]])
        {
            
            str11=[objWOSales valueForKey:@"firstName"];
            str22=[objWOSales valueForKey:@"lastName"];
            str33=[objWOSales valueForKey:@"accountNo"];
            str111=[NSString stringWithFormat:@"%@",[objWOSales valueForKey:@"leadNumber"]];//[dict valueForKeyPath:@"LeadContact.FullName"];
            str222=[objWOSales valueForKey:@"statusSysName"];
            strDateToMatch=[objWOSales valueForKey:@"scheduleStartDate"];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate* newTime = [dateFormatter dateFromString:strDateToMatch];
            //Add the following line to display the time in the local time zone
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSString* finalTime = [dateFormatter stringFromDate:newTime];
            strDateToMatch=finalTime;
            
            startDateFromString = [dateFormatter dateFromString:strDateToMatch];
        }
        else if ([objWOSales isKindOfClass:[WorkOrderDetailsService class]])
        {
            
            str11=[objWOSales valueForKey:@"firstName"];
            str22=[objWOSales valueForKey:@"lastName"];
            str33=[objWOSales valueForKey:@"accountNo"];
            
            NSString *strThirdPartyAccountNo=[NSString stringWithFormat:@"%@",[objWOSales valueForKey:@"thirdPartyAccountNo"]];
            if (strThirdPartyAccountNo.length>0) {
                str33=strThirdPartyAccountNo;
            }

            str111=[NSString stringWithFormat:@"%@",[objWOSales valueForKey:@"workOrderNo"]];//[dict valueForKeyPath:@"LeadContact.FullName"];
            str222=[objWOSales valueForKey:@"workorderStatus"];
            strDateToMatch=[objWOSales valueForKey:@"scheduleStartDateTime"];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            
            
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            
            
            //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* newTime = [dateFormatter dateFromString:strDateToMatch];
            //Add the following line to display the time in the local time zone
            
            
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            
            
            
            //[dateFormatter setDateFormat:@"M/d/yy 'at' h:mma"];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSString* finalTime = [dateFormatter stringFromDate:newTime];
            strDateToMatch=finalTime;
            
            startDateFromString = [dateFormatter dateFromString:strDateToMatch];
            
        }else if ([objWOSales isKindOfClass:[NSDictionary class]])
        {
            
            str11=@"";
            str22=@"";
            str33=@"";
            str111=@"";
            str222=@"";
            strDateToMatch=[objWOSales valueForKey:@"fromDate"];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate* newTime = [dateFormatter dateFromString:strDateToMatch];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSString* finalTime = [dateFormatter stringFromDate:newTime];
            strDateToMatch=finalTime;
            startDateFromString = [dateFormatter dateFromString:strDateToMatch];
            
        }

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        
        NSDate *ToDateToCheck = [[NSDate alloc] init];
        ToDateToCheck = [dateFormatter dateFromString:_btn_ToDateFilter.titleLabel.text];
        
        NSDate *FromDateToCheck = [[NSDate alloc] init];
        FromDateToCheck = [dateFormatter dateFromString:_btn_FromDateFilter.titleLabel.text];
        
        //apne kam ka
        
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDateComponents *components=[[NSDateComponents alloc] init];
        NSDateComponents *components1=[[NSDateComponents alloc] init];
        components.day=1;
        components1.day=-1;
        NSDate *targetDatePlus =[calendar dateByAddingComponents:components toDate:ToDateToCheck options: 0];
        NSDate *targetDatePrevious =[calendar dateByAddingComponents:components1 toDate:FromDateToCheck options: 0];
        
        
        BOOL isDateInRange=[self isDate:startDateFromString inRangeFirstDate:targetDatePrevious lastDate:targetDatePlus];
        
        
        //end
        
        BOOL isSameDate=[self isSameDay:ToDateToCheck otherDay:FromDateToCheck];
        
        if (isSameDate)
        {
            
            NSComparisonResult result = [startDateFromString compare:FromDateToCheck];
            
            if(result == NSOrderedDescending)
            {
                NSLog(@"LocalDbDate is Greater");//y wali sab bhejna hai server p
                isDateInRange=false;
                //isDateInRange=true;
            }
            else if(result == NSOrderedAscending)
            {
                NSLog(@"ServerDate is greater");//y wali local se delete krna hai and save krna hai
                // isDateInRange=true;
                isDateInRange=false;
            }
            else
            {
                NSLog(@"Equal date");//
                isDateInRange=true;
            }
        }
       
        NSString *LeadorWorkOrderId;
        
        if (_txt_FirstName.text.length==0) {
            _txt_FirstName.text=@"#`%&*";
        }
        if (_txt_LastName.text.length==0)
        {
            _txt_LastName.text=@"#`%&*";
        }
        if (!(_txt_WorkOrderNo.text.length==0))
        {
            
            LeadorWorkOrderId=[NSString stringWithFormat:@"%@",_txt_WorkOrderNo.text];
            
        }
        else if (!(_Txt_Leadd.text.length==0))
        {
            
            LeadorWorkOrderId=[NSString stringWithFormat:@"%@",_Txt_Leadd.text];
            
        }
        else
        {
            
            LeadorWorkOrderId=@"#`%&*";
            
        }
        if (_txt_AccountNo.text.length==0)
        {
            _txt_AccountNo.text=@"#`%&*";
        }
        if (_Txt_Leadd.text.length==0)
        {
            //_Txt_Leadd.text=@"@#`%&*";
        }
        if ([_btn_StatusFilter.titleLabel.text isEqualToString:@""])
        {
            _btn_StatusFilter.titleLabel.text=@"#`%&*";
        }
        
        NSRange strRange1;
        if ([_txt_FirstName.text isEqualToString:@"#`%&*"])
        {
            
            strRange1=[@"#`%&*" rangeOfString:_txt_FirstName.text options:NSCaseInsensitiveSearch];
            
        }
        else
        {
            
            strRange1=[str11 rangeOfString:_txt_FirstName.text options:NSCaseInsensitiveSearch];
            
        }
        
        NSRange strRange2;
        if ([_txt_LastName.text isEqualToString:@"#`%&*"])
        {
            
            strRange2=[@"#`%&*" rangeOfString:_txt_LastName.text options:NSCaseInsensitiveSearch];
            
        }
        else
        {
            
            strRange2=[str22 rangeOfString:_txt_LastName.text options:NSCaseInsensitiveSearch];
            
        }
        
        NSRange strRange3;
        if ([_txt_AccountNo.text isEqualToString:@"#`%&*"])
        {
            
            strRange3=[@"#`%&*" rangeOfString:[NSString stringWithFormat:@"%@",_txt_AccountNo.text] options:NSCaseInsensitiveSearch];
            
        }
        else
        {
            
            strRange3=[str33 rangeOfString:[NSString stringWithFormat:@"%@",_txt_AccountNo.text] options:NSCaseInsensitiveSearch];
            
        }
        NSRange strRange4;
        if ([LeadorWorkOrderId isEqualToString:@"#`%&*"])
        {
            
            strRange4=[@"#`%&*" rangeOfString:[NSString stringWithFormat:@"%@",LeadorWorkOrderId] options:NSCaseInsensitiveSearch];
            
        }
        else
        {
            
            strRange4=[str111 rangeOfString:[NSString stringWithFormat:@"%@",LeadorWorkOrderId] options:NSCaseInsensitiveSearch];
            
        }
        // NSRange strRange5=[str111 rangeOfString:[NSString stringWithFormat:@"%@",_txt_WorkOrderNo.text] options:NSCaseInsensitiveSearch];
        //_btn_StatusFilter.titleLabel.text=@"InComplete";
        
        NSRange strRange6;
        BOOL isStatusCompare;
        isStatusCompare=YES;
        
        if ([str222 caseInsensitiveCompare:@"Completed"] == NSOrderedSame) {
            
            str222=@"Complete";
            
        }
        if ([_btn_StatusFilter.titleLabel.text caseInsensitiveCompare:@"New"] == NSOrderedSame) {
            
            _btn_StatusFilter.titleLabel.text=@"InComplete";
            
        }
        
        if ([_btn_StatusFilter.titleLabel.text isEqualToString:@"#`%&*"])
        {
            
            strRange6=[@"#`%&*" rangeOfString:_btn_StatusFilter.titleLabel.text options:NSCaseInsensitiveSearch];
            
        }
        else if ([_btn_StatusFilter.titleLabel.text isEqualToString:@"All"])
        {
            
            strRange6=[@"All" rangeOfString:_btn_StatusFilter.titleLabel.text options:NSCaseInsensitiveSearch];
            
        }
        else
        {
            strRange6=[str222 rangeOfString:_btn_StatusFilter.titleLabel.text options:NSCaseInsensitiveSearch];
            
            if ([str222 caseInsensitiveCompare:_btn_StatusFilter.titleLabel.text] == NSOrderedSame)
            {
                
                
                
            }else
            {
                
                isStatusCompare=NO;
                
            }
        }

        
        if(strRange1.location!=NSNotFound&&strRange2.location!=NSNotFound&&strRange3.location!=NSNotFound&&strRange4.location!=NSNotFound&&strRange6.location!=NSNotFound&&isDateInRange&&isStatusCompare)
        {
            NSLog(@"isDateInRange>>%d",isDateInRange);
            [filteredArray addObject:objWOSales];
        }
    }
    
    //    NSLog(@"Finally Filtered Array List====%@",filteredArray);
    
    [_tblViewAppointment setHidden:NO];
    [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    [_tblViewAppointment reloadData];
    
    [_view_FilterCriteria removeFromSuperview];
    
    if (filteredArray.count>0) {
        
        NSIndexPath *indexpat= [NSIndexPath indexPathForRow:0 inSection:0];
        [_tblViewAppointment scrollToRowAtIndexPath:indexpat
                                   atScrollPosition:UITableViewScrollPositionTop
                                           animated:YES
         ];
        
    }

}

- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLog(@"%d for first date %@\n last date %@",[date compare:firstDate] == NSOrderedDescending &&[date compare:lastDate]  == NSOrderedAscending,[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:firstDate]],[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:lastDate]]);
    
    
    return [date compare:firstDate] == NSOrderedDescending &&
    [date compare:lastDate]  == NSOrderedAscending;
}


- (IBAction)action_CancelFilter:(id)sender {
    
    [_view_FilterCriteria removeFromSuperview];
    //cancel hota hai
    // yesFiltered=NO;
    [_tblViewAppointment setHidden:NO];
    // [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    // [_tblViewAppointment reloadData];
    
}
- (IBAction)action_ClearAllFilter:(id)sender {
    
    [_view_FilterCriteria removeFromSuperview];
    //cancel hota hai
    yesFiltered=NO;
    [_tblViewAppointment setHidden:NO];
    // [_tblViewAppointment setContentOffset:CGPointZero animated:YES];
    [_tblViewAppointment reloadData];
    
}

//============================================================================
//============================================================================
#pragma mark- ---------------------DATE PICKER METHOD-----------------
//============================================================================
//============================================================================

-(void)addPickerViewDateTo :(NSString*)strDateString
{
    pickerDate=[[UIDatePicker alloc]init];
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    pickerDate.frame=CGRectMake(0,0, 320, 350);
    // [pickerDate setMinimumDate:[NSDate date]];
    
    if (!(strDateString.length==0))
    {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSDate *dateToSett = [dateFormat dateFromString:strDateString];
        if (dateToSett==nil) {
            
        }else
        {
            
            pickerDate.date =dateToSett;
            
        }
        
    }
    //Nilind 23 Feb
    
    // NSDate *today=[NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *today = [dateFormat dateFromString:strDateString];
    
    if (today==nil)
    {
    }
    else
    {
        pickerDate.date =today;
    }
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components=[[NSDateComponents alloc] init];
    NSDateComponents *components1=[[NSDateComponents alloc] init];
    components.day=1;
    components1.day=-1;
    NSDate *targetDatePlus =[calendar dateByAddingComponents:components toDate:today options: 0];
    //NSDate *targetDatePrevious =[calendar dateByAddingComponents:components1 toDate:today options: 0];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    [pickerDate setMaximumDate:toDateMaximum];
    [pickerDate setMinimumDate:fromDateMinimum];
    NSString* finalTime = [dateFormatter stringFromDate:targetDatePlus];
    NSLog(@"%@",finalTime);
    
    //End
    
        pickerDate.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        pickerDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } else {
        // Fallback on earlier versions
    }
    [viewForDate setHidden:NO];
    
    viewBackGround=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    viewBackGround.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:0.5];
    [self.view addSubview: viewBackGround];
    
    //============================================================================
    //============================================================================
    
    /*
     UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnBackGroundView)];
     singleTap1.numberOfTapsRequired = 1;
     [viewBackGround setUserInteractionEnabled:YES];
     [viewBackGround addGestureRecognizer:singleTap1];
     */
    
    viewForDate=[[UIView alloc]initWithFrame:CGRectMake(35, [UIScreen mainScreen].bounds.size.height/4, [UIScreen mainScreen].bounds.size.width-70, [UIScreen mainScreen].bounds.size.height/2)];
    [viewBackGround addSubview: viewForDate];
    
    viewForDate.backgroundColor=[UIColor whiteColor];//[UIColor colorWithRed:244.0f/255 green:244.0f/255 blue:244.0f/255 alpha:1];
    viewForDate.layer.cornerRadius=20.0;
    viewForDate.clipsToBounds=YES;
    [viewForDate.layer setBorderWidth:2.0];
    // [viewForDate.layer setBorderColor:[[UIColor colorWithRed:115/255.0f  green:175/255.0f blue:176/255.0f alpha:1] CGColor]];
    [viewForDate.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    UILabel *lblTitle;
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0,0, viewForDate.frame.size.width, 50)];
    
    lblTitle.text=@"SELECT DATE";
    lblTitle.font=[UIFont boldSystemFontOfSize:20];
    lblTitle.textAlignment=NSTextAlignmentCenter;
    lblTitle.textColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    [viewForDate addSubview:lblTitle];
    UILabel *lblLineUp=[[UILabel alloc]initWithFrame:CGRectMake(0,lblTitle.frame.size.height, viewForDate.frame.size.width, 2)];
    [lblLineUp setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLineUp];
    
    UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, viewForDate.frame.size.height-50, viewForDate.frame.size.width, 2)];
    [lblLine setBackgroundColor:[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1]];
    [viewForDate addSubview:lblLine];
    
    UIButton *btnClose=[[UIButton alloc]initWithFrame:CGRectMake(35, lblLine.frame.origin.y+5, 100, 40)];
    if([UIScreen mainScreen].bounds.size.height==736)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(57, lblLine.frame.origin.y+5, 100, 40)];
    }
    else if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(40, lblLine.frame.origin.y+10, 70, 30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnClose=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMidX([viewForDate bounds])-200-15, lblLine.frame.origin.y+5, 200, 40)];
        
    }
    
    
    [btnClose setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //    btnClose.layer.cornerRadius=10.0;
    //    btnClose.clipsToBounds=YES;
    //    [btnClose.layer setBorderWidth:2.0];
    //    [btnClose.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    btnClose.backgroundColor=[UIColor colorWithRed:95.0f/255 green:178.0f/255 blue:175.0f/255 alpha:1];
    
    [viewForDate addSubview:btnClose];
    
    UIButton *btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 100,40)];
    
    if ([UIScreen mainScreen].bounds.size.height==568||[UIScreen mainScreen].bounds.size.height==480)
    {
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 70,30)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        
        btnDone=[[UIButton alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+btnClose.frame.size.width+30, btnClose.frame.origin.y, 200,40)];
        
    }
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    btnDone.layer.cornerRadius=10.0;
    //    btnDone.clipsToBounds=YES;
    //    [btnDone.layer setBorderWidth:2.0];
    //    [btnDone.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    btnDone.backgroundColor=[UIColor colorWithRed:55.0f/255 green:64.0f/255 blue:78.0f/255 alpha:1];
    
    [viewForDate addSubview:btnDone];
    [btnDone addTarget: self action: @selector(setDateOnDone:)forControlEvents: UIControlEventTouchDown];
    [btnClose addTarget: self action: @selector(dismissPickerSheet:)forControlEvents: UIControlEventTouchDown];
    
    pickerDate.frame=CGRectMake(0, 35, viewForDate.frame.size.width, viewForDate.frame.size.height-100);
    
    
    
    [viewForDate addSubview:pickerDate];
}
- (void)dismissPickerSheet:(id)sender
{
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
}
-(void)setDateOnDone:(id)sender
{
    NSInteger i;
    i=tblData.tag;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    strDate = [dateFormat stringFromDate:pickerDate.date];
    
    BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:pickerDate.date];
    if (yesSameDay) {
        
        [self settingDate:i];
        
    }else{
        
        [self settingDate:i];
        
        /*
         NSDate *dateSELECTED = [dateFormat dateFromString:strDate];
         
         if ([[NSDate date] compare:dateSELECTED] == NSOrderedDescending) {
         
         [global AlertMethod:@"Alert" :@"Back dates cant be selected"];
         
         }else{
         
         [self settingDate:i];
         
         
         NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
         NSDate *currentDate = [NSDate date];
         NSDateComponents *comps = [[NSDateComponents alloc] init];
         [comps setYear:30];
         NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
         [comps setYear:-30];
         NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
         
         [datePicker setMaximumDate:maxDate];
         [datePicker setMinimumDate:minDate];
         
         
         }
         */
    }
}
-(void)settingDate :(NSInteger)tag{
    
    if (tag==108)
    {
        if ([_btn_ToDateFilter.titleLabel.text isEqualToString:@"To Date"]) {
            [_btn_FromDateFilter setTitle:strDate forState:UIControlStateNormal];
            strGlobalFromDate=strDate;
        } else {
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MM/dd/yyyy"];
            NSDate *date2 = [dateFormat dateFromString:_btn_ToDateFilter.titleLabel.text];
            if ([_btn_ToDateFilter.titleLabel.text isEqualToString:@"To Date"]) {
                
                [_btn_FromDateFilter setTitle:strDate forState:UIControlStateNormal];
                strGlobalFromDate=strDate;
                
            } else {
                
                if (!([pickerDate.date compare:date2] == NSOrderedDescending)) {
                    NSLog(@"pickerDate.date is earlier than date2");
                    [_btn_FromDateFilter setTitle:strDate forState:UIControlStateNormal];
                    strGlobalFromDate=strDate;
                } else {
                    [global AlertMethod:@"Alert" :@"From Date should be less than To Date"];
                }
            }
        }
    }
    else if(tag==109)
    {
        if ([_btn_FromDateFilter.titleLabel.text isEqualToString:@"From Date"]) {
            [_btn_ToDateFilter setTitle:strDate forState:UIControlStateNormal];
            strGlobalToDate=strDate;
        } else {
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MM/dd/yyyy"];
            NSDate *date2 = [dateFormat dateFromString:_btn_FromDateFilter.titleLabel.text];
            
            if ([_btn_FromDateFilter.titleLabel.text isEqualToString:@"From Date"]) {
                
                [_btn_ToDateFilter setTitle:strDate forState:UIControlStateNormal];
                strGlobalToDate=strDate;
                
            } else {
                
                if (!([pickerDate.date compare:date2] == NSOrderedAscending)) {
                    NSLog(@"pickerDate.date is later than date2");
                    [_btn_ToDateFilter setTitle:strDate forState:UIControlStateNormal];
                    strGlobalToDate=strDate;
                } else {
                    [global AlertMethod:@"Alert" :@"To Date should be Greater than From Date"];
                }
            }
        }
    }
    [viewForDate removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [viewForDate removeFromSuperview];
    [tblData removeFromSuperview];
    [viewBackGround removeFromSuperview];
    
}
- (IBAction)hideKeyBoard:(id)sender{
    
    [self resignFirstResponder];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    
    if (textField.tag==1){
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 100) ? NO : YES;
    }else
        return YES;
}
-(void)fetchWorkOrderDetails :(NSString*)workOrderIdd{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",workOrderIdd];
    
    [requestNewService setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrObjOnMyWaySentSMS = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrObjOnMyWaySentSMS.count==0)
    {
        
    }else
    {
        NSManagedObject *objTemp =arrObjOnMyWaySentSMS[0];
        NSDate *currentDate=[NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
        [objTemp setValue:stringCurrentDate forKey:@"onMyWaySentSMSTime"];
        //Nilind 08 Dec
        CLLocationCoordinate2D coordinate = [global getLocation] ;
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [objTemp setValue:latitude forKey:@"onMyWaySentSMSTimeLatitude"];
        [objTemp setValue:longitude forKey:@"onMyWaySentSMSTimeLongitude"];
        //End
        NSError *error;
        [context save:&error];
    }
}
//Nilind 08 Dec

-(void)fetchSalesLeadDetails :(NSString*)strSalesLeadId{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strSalesLeadId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrObjOnMyWaySentSMS = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //    NSManagedObject *matchesNew;
    if (arrObjOnMyWaySentSMS.count==0)
    {
        
    }else
    {
        NSManagedObject *objTemp =arrObjOnMyWaySentSMS[0];
        NSDate *currentDate=[NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        NSString *stringCurrentDate = [formatter stringFromDate:currentDate];
        [objTemp setValue:stringCurrentDate forKey:@"onMyWaySentSMSTime"];
        //Nilind 08 Dec//OnMyWaySentSMSTime
        CLLocationCoordinate2D coordinate = [global getLocation] ;
        NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        [objTemp setValue:latitude forKey:@"onMyWaySentSMSTimeLatitude"];
        [objTemp setValue:longitude forKey:@"onMyWaySentSMSTimeLongitude"];
        //End
        NSError *error;
        [context save:&error];
    }
}

//End
//============================================================================
//============================================================================
#pragma mark- ---------------------Get Sales Dynamic Form Data All New METHOD-----------------
//============================================================================
//============================================================================

-(void)getDynamicFormDataSales{
    
    //693,696,694,697,699
    if (arrOfSalesLeadToFetchDynamicData.count==0) {
        
        [self getDynamicFormDataService];
        
    } else {
        
        
        NSString *strLeadsOfDynamicData=[arrOfSalesLeadToFetchDynamicData componentsJoinedByString:@","];
        
        if (strLeadsOfDynamicData.length==0) {
            
            [self getDynamicFormDataService];
            
        } else {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Sales Dynamic Forms"];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strSalesAutoMainUrl,UrlSalesDynamicFormAllLeads,strCompanyKey,UrlSalesDynamicFormAllLeadsLeadId,strLeadsOfDynamicData];
            
            NSLog(@"Get All Dynamic Data Sales===%@",strUrl);
            
            //============================================================================
            //============================================================================
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"SalesAllDynamicForm" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [DejalBezelActivityView removeView];
                         if (success)
                         {
                             
                             NSArray *arrOfDynamicData=(NSArray*)response;
                             
                             for (int k=0; k<arrOfDynamicData.count; k++) {
                                 
                                 NSMutableArray *arrOfDynamicFormFinal=[[NSMutableArray alloc]init];
                                 
                                 NSDictionary *dictData=arrOfDynamicData[k];
                                 
                                 NSString *strLeadIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"LeadId"]];
                                 
                                 for (int j=0; j<arrOfDynamicData.count; j++) {
                                     
                                     NSDictionary *dictDataInside=arrOfDynamicData[j];
                                     
                                     NSString *strLeadIdDynamicFormInside=[NSString stringWithFormat:@"%@",[dictDataInside valueForKey:@"LeadId"]];
                                     if ([strLeadIdDynamicFormInside isEqualToString:strLeadIdDynamicForm]) {
                                         
                                         [arrOfDynamicFormFinal addObject:dictDataInside];
                                         
                                     }
                                 }
                                 [self FetchFromCoreDataSalesDynamicForm : strLeadIdDynamicForm : arrOfDynamicFormFinal];
                             }
                             [self getDynamicFormDataService];
                         }
                         else
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                         }
                     });
                 }];
            });
            
        }
    }
    //============================================================================
    //============================================================================
}

-(void)FetchFromCoreDataSalesDynamicForm :(NSString*)strLeadIdToFind :(NSArray*)dictDataInspection{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"SalesDynamicInspection" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND userName = %@",strLeadIdToFind,strUsername];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    NSArray *arrDynamicFormSales = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrDynamicFormSales count] == 0)
    {
        
        SalesDynamicInspection *objSalesDynamic = [[SalesDynamicInspection alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
        objSalesDynamic.arrFinalInspection=dictDataInspection;
        objSalesDynamic.leadId=strLeadIdToFind;
        objSalesDynamic.companyKey=strCompanyKey;
        objSalesDynamic.userName=strUserName;
        objSalesDynamic.empId=strEmpID;
        objSalesDynamic.isSentToServer=@"Yes";
        NSError *error1;
        [context save:&error1];
        
    }
    else
    {
        
        NSManagedObject *objDynamicFormSales=arrDynamicFormSales[0];
        [objDynamicFormSales setValue:dictDataInspection forKey:@"arrFinalInspection"];
        [objDynamicFormSales setValue:@"Yes" forKey:@"isSentToServer"];
        NSError *error1;
        [context save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Get Service Dynamic Form Data All New METHOD-----------------
//============================================================================
//============================================================================

-(void)getDynamicFormDataService{
    
    if (arrOfServiceLeadToFetchDynamicData.count==0) {
        
        //[self getDynamicFormDataServiceEquipments];
        [self getDynamicFormDataDeviceOnlySelectedWo];
        
    } else {
        
        NSString *strWorkOrdersOfDynamicData=[arrOfServiceLeadToFetchDynamicData componentsJoinedByString:@","];
        
        if (strWorkOrdersOfDynamicData.length==0) {
            
        } else {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Service Dynamic Forms"];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlServiceDynamicFormAllWorkOrders,strCompanyKey,UrlServiceDynamicFormAllWorkOrdersWorkOrderID,strWorkOrdersOfDynamicData];
            
            NSLog(@"Get All Dynamic Data Service===%@",strUrl);
            
            //============================================================================
            //============================================================================
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"ServiceAllDynamicForm" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [DejalBezelActivityView removeView];
                         if (success)
                         {
                             NSArray *arrOfDynamicData=(NSArray*)response;
                             
                             for (int k=0; k<arrOfDynamicData.count; k++) {
                                 
                                 NSDictionary *dictData=arrOfDynamicData[k];
                                 
                                 NSString *strWorkOrderIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkorderId"]];
                                 
                                 [self FetchFromCoreDataServiceDynamic : strWorkOrderIdDynamicForm :dictData];
                                 
                             }
                             
                             //[self getDynamicFormDataServiceEquipments];
                             [self getDynamicFormDataDeviceOnlySelectedWo];
                             
                         }
                         else
                         {
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                             
                             //[self getDynamicFormDataServiceEquipments];
                             [self getDynamicFormDataDeviceOnlySelectedWo];
                             
                         }
                         
                         //  [self SalesAutomationFetchAfterSyncingData];
                         
                     });
                 }];
            });
            
        }
    }
    //============================================================================
    //============================================================================
}
//============================================================================
//============================================================================
#pragma mark- ---------------------Equipment Dynamic Forms ALl-----------------
//============================================================================
//============================================================================

-(void)getDynamicFormDataServiceEquipments{
    
    NSUserDefaults *defsEquip=[NSUserDefaults standardUserDefaults];
    
    BOOL isEquipEnabled=[defsEquip boolForKey:@"isEquipmentEnabled"];
    
    if (isEquipEnabled) {
        
        if (arrOfServiceLeadToFetchDynamicData.count==0) {
            
        } else {
            
            NSString *strWorkOrdersOfDynamicData=[arrOfServiceLeadToFetchDynamicData componentsJoinedByString:@","];
            
            if (strWorkOrdersOfDynamicData.length==0) {
                
            } else {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Equipment Dynamic Forms"];
                
                NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlEquipmentsDynamicFormAllWorkOrders,strCompanyKey,UrlServiceDynamicFormAllWorkOrdersWorkOrderID,strWorkOrdersOfDynamicData];
                
                NSLog(@"Get All Dynamic Data Equipments===%@",strUrl);
                
                //============================================================================
                //============================================================================
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                    
                    [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"EquipmentsAllDynamicForm" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [DejalBezelActivityView removeView];
                             if (success)
                             {
                                 NSArray *arrOfDynamicData=(NSArray*)response;
                                 
                                 for (int k=0; k<arrOfDynamicData.count; k++) {
                                     
                                     NSDictionary *dictData=arrOfDynamicData[k];
                                     
                                     NSString *strWorkOrderIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WorkorderId"]];
                                     NSString *strWoEquipIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WOEquipmentId"]];
                                     if (strWoEquipIdDynamicForm.length==0) {
                                         
                                         isWoEquipIdPresent=NO;
                                         strWoEquipIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"MobileUniqueId"]];
                                         
                                     } else {
                                         isWoEquipIdPresent=YES;
                                         strWoEquipIdDynamicForm=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"WOEquipmentId"]];
                                         
                                     }
                                     
                                     [self FetchFromCoreDataEquipmentDynamic : strWorkOrderIdDynamicForm :dictData :strWoEquipIdDynamicForm];
                                     
                                 }
                                 
                                 // Device Dynamic Form Fetch Karna hai
                                //[self getDynamicFormDataDevice];

//                                         indexToSendDeviceDynamicForm = 0;
//
//                                         [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
                                 
                             }
                             else
                             {
                                 NSString *strTitle = Alert;
                                 NSString *strMsg = Sorry;
                                 [global AlertMethod:strTitle :strMsg];
                             }
                         });
                     }];
                });
            }
        }
    }else{
        
        // If Equipment nhi hai to bhi device dynamic form lena hai
        
       // Device Dynamic Form Fetch Karna hai
        //[self getDynamicFormDataDevice];
        
//                indexToSendDeviceDynamicForm = 0;
//
//                [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
        
    }
    //============================================================================
    //============================================================================
}


-(void)FetchFromCoreDataServiceDynamic :(NSString*)strWorkOrderIdToFetch :(NSDictionary*)dictDataInspection{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"ServiceDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    // NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderIdToFetch,strUsername];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",strWorkOrderIdToFetch];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    NSArray *arrDynamicFormService = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrDynamicFormService count] == 0)
    {
        ServiceDynamicForm *objSalesDynamic = [[ServiceDynamicForm alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
        objSalesDynamic.arrFinalInspection=dictDataInspection;
        objSalesDynamic.workOrderId=strWorkOrderIdToFetch;
        objSalesDynamic.companyKey=strCompanyKey;
        objSalesDynamic.userName=strUserName;
        objSalesDynamic.empId=strEmpID;
        // objSalesDynamic.departmentId=strDepartmentId;
        objSalesDynamic.isSentToServer=@"Yes";
        NSError *error1;
        [context save:&error1];
        
    }
    else
    {
        
        NSManagedObject *objDynamicFormService=arrDynamicFormService[0];
        [objDynamicFormService setValue:dictDataInspection forKey:@"arrFinalInspection"];
        [objDynamicFormService setValue:@"Yes" forKey:@"isSentToServer"];
        NSError *error1;
        [context save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

-(void)FetchFromCoreDataEquipmentDynamic :(NSString*)strWorkOrderIdToFetch :(NSDictionary*)dictDataInspection :(NSString*)strWoEquipIdIdToFetch{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySalesDynamic=[NSEntityDescription entityForName:@"EquipmentDynamicForm" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySalesDynamic];
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    //NSString *strUsername     =[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.Username"]];
    
    //    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ AND userName = %@",strWorkOrderIdToFetch,strUsername];
    
    NSString *strTypeOfFilter;
    if (isWoEquipIdPresent) {
        strTypeOfFilter=@"woEquipId";
    } else {
        strTypeOfFilter=@"mobileEquipId";
    }
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND %@ = %@",strWorkOrderIdToFetch,strTypeOfFilter,strWoEquipIdIdToFetch];
    
    //  NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@ AND woEquipId = %@",strWorkOrderIdToFetch,strWoEquipIdIdToFetch];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesDynamic setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsControllerSalesDynamic performFetch:&error];
    NSArray *arrDynamicFormService = [self.fetchedResultsControllerSalesDynamic fetchedObjects];
    if ([arrDynamicFormService count] == 0)
    {
        EquipmentDynamicForm *objSalesDynamic = [[EquipmentDynamicForm alloc]initWithEntity:entitySalesDynamic insertIntoManagedObjectContext:context];
        objSalesDynamic.arrFinalInspection=dictDataInspection;
        objSalesDynamic.workorderId=strWorkOrderIdToFetch;
        objSalesDynamic.companyKey=strCompanyKey;
        objSalesDynamic.userName=strUserName;
        objSalesDynamic.empId=strEmpID;
        if (isWoEquipIdPresent) {
            
            objSalesDynamic.woEquipId=strWoEquipIdIdToFetch;
            
        } else {
            
            objSalesDynamic.mobileEquipId=strWoEquipIdIdToFetch;
            
        }
        // objSalesDynamic.departmentId=strDepartmentId;
        objSalesDynamic.isSentToServer=@"Yes";
        NSError *error1;
        [context save:&error1];
        
    }
    else
    {
        
        NSManagedObject *objDynamicFormService=arrDynamicFormService[0];
        [objDynamicFormService setValue:dictDataInspection forKey:@"arrFinalInspection"];
        [objDynamicFormService setValue:@"Yes" forKey:@"isSentToServer"];
        NSError *error1;
        [context save:&error1];
        
    }
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    } else {
        
    }
}

#pragma mark- Nilind 31 Dec------------InitialSetup---------------
//Nilind 31 Dec
-(void)clickToInitialSetup:(UIButton*)sender
{
    
    NSLog(@"%ld",(long)sender.tag);
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    
    UpcomingAppointmentTableViewCell *tappedCell = (UpcomingAppointmentTableViewCell *)[_tblViewAppointment cellForRowAtIndexPath:indexpath];
    NSString *str;
    str=tappedCell.lblLeadIddd.text;
    //"Generate Workorder"
    NSLog(@"button titile >>>%@",tappedCell.btnCreateInitialSetup.titleLabel.text);
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"SalesMain" bundle:nil];
    
    
    NSManagedObject *objmatches;
    
    if (yesFiltered)
    {
        
        objmatches=filteredArray[indexpath.row];
        
    }
    else
    {
        
        objmatches=arrAllObj[indexpath.row];
        
    }
    
    
    
    
    NSString *strLeadNumberI=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"leadNumber"]]; //
    NSString *strAccountNoI=[NSString stringWithFormat:@"%@",[objmatches valueForKey:@"accountNo"]];
    
    
    //    strLeadNumberI=[strLeadNumberI stringByReplacingOccurrencesOfString:@"Opportunity #:" withString:@""];
    //    strAccountNoI=[strAccountNoI stringByReplacingOccurrencesOfString:@"Ac #:"withString:@""];
    //    strLeadNumberI= [strLeadNumberI stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //    strAccountNoI= [strAccountNoI stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    [defs setObject:str forKey:@"ForInitialSetUpLeadId"];
    [defs setValue:str forKey:@"LeadId"];
    [defs setValue:strAccountNoI forKey:@"AccountNos"];
    [defs setValue:strLeadNumberI forKey:@"LeadNumber"];
    [defs synchronize];
    
    
    
    if([tappedCell.btnCreateInitialSetup.titleLabel.text isEqualToString:@"Generate Workorder"])
    {
        
        NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
        [defs setValue:@"fromGenerateWorkorder" forKey:@"fromGenerateWorkorder"];
        [defs synchronize];
        GenerateWorkOrder *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"GenerateWorkOrder"];
        [self.navigationController pushViewController:objInitialSetUp animated:NO];
    }
    
    else
    {
        [defs setValue:@"forFollowUp" forKey:@"FollowUp"];
        [defs synchronize];
        InitialSetUp *objInitialSetUp=[storyBoard instantiateViewControllerWithIdentifier:@"InitialSetUp"];
        objInitialSetUp.strFromAppointment=str;
        [self.navigationController pushViewController:objInitialSetUp animated:NO];
    }
    
}

-(void)fetchLeadAndInitialSetupStatus
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ AND employeeNo_Mobile=%@",strLeadIdGlobal,strEmployeeNo];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadDetail"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
        }
    }
    
}
-(NSArray *)fetchSoldServiceCount:(NSString *)strLead
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entitySoldServiceStandardDetail= [NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    [request setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLead];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrObj;
    arrObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    NSManagedObject *objmatches;
    
    for(int i=0;    i<arrObj.count;i++)
    {
        objmatches=[arrObj objectAtIndex:i];
        [arr addObject:objmatches];
    }
    
    return arr;
    
    
}
//...............

#pragma mark- ------------Deleting Old Data's---------------

-(void)deleteFromCoreDataSalesInfoDateWiseOldData :(NSString*)strLeadsssIdToDelete{
    
    NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
    NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
    strEmployeeNo=[NSString stringWithFormat:@"%@",[dictLoginData valueForKey:@"EmployeeNumber"]];
    
    NSString *strLeadIDD=[NSString stringWithFormat:@"%@",strLeadsssIdToDelete];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId = %@ AND employeeNo_Mobile = %@",strLeadIDD,strEmployeeNo];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    //  Delete Email Detail Data
    entityEmailDetail=[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
    [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataEmail setPredicate:predicateEmail];
    
    [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error1 = nil;
    NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
    //error handling goes here
    for (NSManagedObject * data in DataEmail) {
        [context deleteObject:data];
    }
    NSError *saveError1 = nil;
    [context save:&saveError1];
    
    
    //  Delete Image Detail Data
    entityImageDetail=[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
    
    //  Delete PaymentInfo Detail Data
    entityPaymentInfo=[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
    [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context]];
    
    NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataPaymentInfo setPredicate:predicatePaymentInfo];
    
    [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorPaymentInfo = nil;
    NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
    //error handling goes here
    for (NSManagedObject * data in DataPaymentInfo) {
        [context deleteObject:data];
    }
    NSError *saveErrorPaymentInfo = nil;
    [context save:&saveErrorPaymentInfo];
    
    
    
    //  Delete LeadPreference Detail Data
    entityLeadPreference=[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context];
    NSFetchRequest *allDataLeadPreference = [[NSFetchRequest alloc] init];
    [allDataLeadPreference setEntity:[NSEntityDescription entityForName:@"LeadPreference" inManagedObjectContext:context]];
    
    NSPredicate *predicateLeadPreference =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataLeadPreference setPredicate:predicateLeadPreference];
    
    [allDataLeadPreference setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorLeadPreference = nil;
    NSArray * DataLeadPreference = [context executeFetchRequest:allDataLeadPreference error:&errorLeadPreference];
    //error handling goes here
    for (NSManagedObject * data in DataLeadPreference) {
        [context deleteObject:data];
    }
    NSError *saveErrorLeadPreference = nil;
    [context save:&saveErrorLeadPreference];
    
    
    //  Delete LeadPreference Detail Data
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataSoldServiceNonStandardDetail = [[NSFetchRequest alloc] init];
    [allDataSoldServiceNonStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateSoldServiceNonStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataSoldServiceNonStandardDetail setPredicate:predicateSoldServiceNonStandardDetail];
    
    [allDataSoldServiceNonStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorSoldServiceNonStandardDetail = nil;
    NSArray * DataSoldServiceNonStandardDetail = [context executeFetchRequest:allDataSoldServiceNonStandardDetail error:&errorSoldServiceNonStandardDetail];
    //error handling goes here
    for (NSManagedObject * data in DataSoldServiceNonStandardDetail) {
        [context deleteObject:data];
    }
    NSError *saveErrorSoldServiceNonStandardDetail = nil;
    [context save:&saveErrorSoldServiceNonStandardDetail];
    
    
    //  Delete SoldServiceStandardDetail Detail Data
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    NSFetchRequest *allDataSoldServiceStandardDetail = [[NSFetchRequest alloc] init];
    [allDataSoldServiceStandardDetail setEntity:[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateSoldServiceStandardDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataSoldServiceStandardDetail setPredicate:predicateSoldServiceStandardDetail];
    
    [allDataSoldServiceStandardDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorSoldServiceStandardDetail = nil;
    NSArray * DataSoldServiceStandardDetail = [context executeFetchRequest:allDataSoldServiceStandardDetail error:&errorSoldServiceStandardDetail];
    //error handling goes here
    for (NSManagedObject * data in DataSoldServiceStandardDetail)
    {
        [context deleteObject:data];
    }
    NSError *saveErrorSoldServiceStandardDetail = nil;
    [context save:&saveErrorSoldServiceStandardDetail];
    
    
    //Delete Documents Detail Data
    entityDocumentsDetail=[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    
    NSFetchRequest *allDataDocumentsDetail = [[NSFetchRequest alloc] init];
    [allDataDocumentsDetail setEntity:[NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context]];
    
    NSPredicate *predicateDocumentsDetail =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataDocumentsDetail setPredicate:predicateDocumentsDetail];
    
    [allDataDocumentsDetail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorDocumentsDetail = nil;
    NSArray * DataDocumentsDetail = [context executeFetchRequest:allDataDocumentsDetail error:&errorDocumentsDetail];
    //error handling goes here
    for (NSManagedObject * data in DataDocumentsDetail) {
        [context deleteObject:data];
    }
    NSError *saveErrorDocumentsDetail = nil;
    [context save:&saveErrorDocumentsDetail];
    
    
    //Delete ModifyDate Sales Data
    entityModifyDate=[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context];
    
    NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
    [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"SalesAutoModifyDate" inManagedObjectContext:context]];
    
    NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"leadIdd = %@",strLeadIDD];
    [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
    
    [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorSalesModifyDate = nil;
    NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
    //error handling goes here
    for (NSManagedObject * data in DataSalesModifyDate) {
        [context deleteObject:data];
    }
    NSError *saveErrorSalesModifyDate = nil;
    [context save:&saveErrorSalesModifyDate];
    
    //......................................................
    
    //Delete Current Service Data
    entityCurrentService=[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context];
    
    NSFetchRequest *allDataCurrentService = [[NSFetchRequest alloc] init];
    [allDataCurrentService setEntity:[NSEntityDescription entityForName:@"CurrentService" inManagedObjectContext:context]];
    NSPredicate *predicateCurrentService =[NSPredicate predicateWithFormat:@"leadId = %@",strLeadIDD];
    [allDataSalesModifyDate setPredicate:predicateCurrentService];
    [allDataCurrentService setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorCurrentService= nil;
    NSArray * DataCurrentService = [context executeFetchRequest:allDataCurrentService error:&errorCurrentService];
    //error handling goes here
    for (NSManagedObject * data in DataCurrentService) {
        [context deleteObject:data];
    }
    NSError *saveErrorCurrentService = nil;
    [context save:&saveErrorCurrentService];
    
}

-(void)deleteFromCoreDataServiceInfoDateWiseOldData :(NSString*)strWorkOrderIdToDelete{
    
    NSString *strLeadIDD=[NSString stringWithFormat:@"%@",strWorkOrderIdToDelete];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    //  Delete Lead Detail Data
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    NSFetchRequest *allData = [[NSFetchRequest alloc] init];
    [allData setEntity:[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context]];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allData setPredicate:predicate];
    
    [allData setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * Data = [context executeFetchRequest:allData error:&error];
    //error handling goes here
    for (NSManagedObject * data in Data) {
        [context deleteObject:data];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
    //  Delete Email Detail Data
    entityEmailDetailServiceAuto=[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allDataEmail = [[NSFetchRequest alloc] init];
    [allDataEmail setEntity:[NSEntityDescription entityForName:@"EmailDetailServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicateEmail =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allDataEmail setPredicate:predicateEmail];
    
    [allDataEmail setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error1 = nil;
    NSArray * DataEmail = [context executeFetchRequest:allDataEmail error:&error1];
    //error handling goes here
    for (NSManagedObject * data in DataEmail) {
        [context deleteObject:data];
    }
    NSError *saveError1 = nil;
    [context save:&saveError1];
    
    
    //  Delete Equipment Detail Data
    entityWOEquipmentServiceAuto=[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context];
    NSFetchRequest *allDataWOEquipment = [[NSFetchRequest alloc] init];
    [allDataWOEquipment setEntity:[NSEntityDescription entityForName:@"WorkOrderEquipmentDetails" inManagedObjectContext:context]];
    
    NSPredicate *predicateWOEquipment =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allDataWOEquipment setPredicate:predicateWOEquipment];
    
    [allDataWOEquipment setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error1WOEquipment = nil;
    NSArray * DataWOEquipment = [context executeFetchRequest:allDataWOEquipment error:&error1WOEquipment];
    //error handling goes here
    for (NSManagedObject * data in DataWOEquipment) {
        [context deleteObject:data];
    }
    NSError *saveError1WOEquipment = nil;
    [context save:&saveError1WOEquipment];
    
    //  Delete Image Detail Data
    entityImageDetailServiceAuto=[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allDataImage = [[NSFetchRequest alloc] init];
    [allDataImage setEntity:[NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicateImage =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allDataImage setPredicate:predicateImage];
    
    [allDataImage setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error2 = nil;
    NSArray * DataImage = [context executeFetchRequest:allDataImage error:&error2];
    //error handling goes here
    for (NSManagedObject * data in DataImage) {
        [context deleteObject:data];
    }
    NSError *saveError2 = nil;
    [context save:&saveError2];
    
    
    //  Delete PaymentInfo Detail Data
    entityPaymentInfoServiceAuto=[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    NSFetchRequest *allDataPaymentInfo = [[NSFetchRequest alloc] init];
    [allDataPaymentInfo setEntity:[NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context]];
    
    NSPredicate *predicatePaymentInfo =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allDataPaymentInfo setPredicate:predicatePaymentInfo];
    
    [allDataPaymentInfo setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorPaymentInfo = nil;
    NSArray * DataPaymentInfo = [context executeFetchRequest:allDataPaymentInfo error:&errorPaymentInfo];
    //error handling goes here
    for (NSManagedObject * data in DataPaymentInfo) {
        [context deleteObject:data];
    }
    NSError *saveErrorPaymentInfo = nil;
    [context save:&saveErrorPaymentInfo];
    
    
    
    
    //Delete ModifyDate Sales Data
    entityModifyDateServiceAuto=[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context];
    
    NSFetchRequest *allDataSalesModifyDate = [[NSFetchRequest alloc] init];
    [allDataSalesModifyDate setEntity:[NSEntityDescription entityForName:@"ServiceAutoModifyDate" inManagedObjectContext:context]];
    
    NSPredicate *predicateSalesModifyDate =[NSPredicate predicateWithFormat:@"workorderId = %@",strLeadIDD];
    [allDataSalesModifyDate setPredicate:predicateSalesModifyDate];
    
    [allDataSalesModifyDate setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * errorSalesModifyDate = nil;
    NSArray * DataSalesModifyDate = [context executeFetchRequest:allDataSalesModifyDate error:&errorSalesModifyDate];
    //error handling goes here
    for (NSManagedObject * data in DataSalesModifyDate) {
        [context deleteObject:data];
    }
    NSError *saveErrorSalesModifyDate = nil;
    [context save:&saveErrorSalesModifyDate];
    
    //......................................................
}


-(void)SalesAutomationFetchAfterSyncingData
{
    
    @try {
        
        //============================================================================
        //============================================================================
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        strConfigFromDateSalesAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.FromDate"]];
        
        strConfigFromDateServiceAuto=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.ServiceAutoModule.FromDate"]];
        
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
        [request setEntity:entityLeadDetail];
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [request setSortDescriptors:sortDescriptors];
        self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerSalesInfo setDelegate:self];
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerSalesInfo performFetch:&error1];
        NSArray *arrOfSalesLeads = [self.fetchedResultsControllerSalesInfo fetchedObjects];
        //    NSManagedObject *matchesNew;
        if (arrOfSalesLeads.count==0)
        {
            
            [self ServiceAutomationFetchAfterSyncingData];
            
        }else
        {
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSDate *date = [inputDateFormatter dateFromString:strConfigFromDateSalesAuto];
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *startFromDate = [[NSDate alloc] init];
            startFromDate = [outputDateFormatter dateFromString:outputString];
            
            NSDate *dateTwoDaysAgo = [date dateByAddingTimeInterval:-3*24*60*60];
            NSLog(@"2 days ago: %@", dateTwoDaysAgo);
            
            NSMutableArray *arrOfImagesNameToDelete=[[NSMutableArray alloc]init];
            NSMutableArray *arrOfDataBeingDeleted=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfSalesLeads.count; k++) {
                
                NSManagedObject *objCoreData =arrOfSalesLeads[k];
                
                NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [objCoreData valueForKey:@"scheduleStartDate"];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                NSDate *scheduleDate = [[NSDate alloc] init];
                scheduleDate = [outputDateFormatter dateFromString:outputString];
                
                NSComparisonResult result = [dateTwoDaysAgo compare:scheduleDate];
                
                if(result == NSOrderedDescending)
                {
                    NSLog(@"dateTwoDaysAgo is Greater");
                    
                    //ZSync No hai to hi delete karna hai
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"leadId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        
                        if (strAudioNameToDelete.length>0 && ![strAudioNameToDelete isEqualToString:@"(null)"]) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDeleteSalesAuto:[objCoreData valueForKey:@"leadId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailSalesAutoToDelete:[objCoreData valueForKey:@"leadId"]]];
                        
                        NSLog(@"images Name to be deleted for Leadssss===%@---%@",[objCoreData valueForKey:@"leadId"],arrOfImagesNameToDelete);
                        
                        [self deleteFromCoreDataSalesInfoDateWiseOldData:[objCoreData valueForKey:@"leadId"]];
                        
                    }
                    
                }
                else if(result == NSOrderedAscending)
                {
                    NSLog(@"scheduleDate is greater");
                }
                else
                {
                    NSLog(@"Equal date");
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"leadId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        
                        if (strAudioNameToDelete.length>0 && ![strAudioNameToDelete isEqualToString:@"(null)"]) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDeleteSalesAuto:[objCoreData valueForKey:@"leadId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailSalesAutoToDelete:[objCoreData valueForKey:@"leadId"]]];
                        
                        
                        [self deleteFromCoreDataSalesInfoDateWiseOldData:[objCoreData valueForKey:@"leadId"]];
                        
                    }
                    
                }
                
                //technicianSignaturePath   audioFilePath customerSignaturePath
                
            }
            
            for (int k=0; k<arrOfImagesNameToDelete.count; k++) {
                
                [self removeImage:arrOfImagesNameToDelete[k]];
                
            }
            
            // NSString *strCountDeleted=[NSString stringWithFormat:@"Sales Lead Deleted----%lu",(unsigned long)arrOfDataBeingDeleted.count];
            
            // NSString *strLeadsss=[arrOfDataBeingDeleted componentsJoinedByString:@","];
            
            // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:strCountDeleted message:strLeadsss delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            // [alert show];
            
            NSLog(@"images Name to be deleted for Leadssss===---%@",arrOfImagesNameToDelete);
            
            [self ServiceAutomationFetchAfterSyncingData];
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)ServiceAutomationFetchAfterSyncingData
{
    
    @try {
        NSMutableArray *arrTempAllObj=[[NSMutableArray alloc]init];
        [arrTempAllObj addObjectsFromArray:arrAllObj];
        
        arrAllObj=nil;
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
        requestNewService = [[NSFetchRequest alloc] init];
        [requestNewService setEntity:entityWorkOderDetailServiceAuto];
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        [requestNewService setSortDescriptors:sortDescriptors];
        
        self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceAutomation setDelegate:self];
        
        // Perform Fetch
        NSError *error1 = nil;
        [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
        NSArray *arrOfServiceWorkOrderId = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
        //    NSManagedObject *matchesNew;
        if (arrOfServiceWorkOrderId.count==0)
        {
            
            [self salesFetch];
            
        }else
        {
            
            NSString *dateFormat = @"MM/dd/yyyy";
            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
            NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
            [inputDateFormatter setTimeZone:inputTimeZone];
            [inputDateFormatter setDateFormat:dateFormat];
            
            NSDate *date = [inputDateFormatter dateFromString:strConfigFromDateServiceAuto];
            
            NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
            NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
            [outputDateFormatter setTimeZone:outputTimeZone];
            [outputDateFormatter setDateFormat:dateFormat];
            NSString *outputString = [outputDateFormatter stringFromDate:date];
            
            NSDate *startFromDate = [[NSDate alloc] init];
            startFromDate = [outputDateFormatter dateFromString:outputString];
            
            NSDate *dateTwoDaysAgo = [date dateByAddingTimeInterval:-3*24*60*60];
            NSLog(@"2 days ago: %@", dateTwoDaysAgo);
            
            NSMutableArray *arrOfImagesNameToDelete=[[NSMutableArray alloc]init];
            NSMutableArray *arrOfDataBeingDeleted=[[NSMutableArray alloc]init];
            
            for (int k=0; k<arrOfServiceWorkOrderId.count; k++) {
                
                NSManagedObject *objCoreData =arrOfServiceWorkOrderId[k];
                
                NSString *dateFormat = @"yyyy-MM-dd HH:mm:ss";
                NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
                [inputDateFormatter setTimeZone:inputTimeZone];
                [inputDateFormatter setDateFormat:dateFormat];
                
                NSString *inputString = [objCoreData valueForKey:@"scheduleStartDateTime"];
                NSDate *date = [inputDateFormatter dateFromString:inputString];
                
                NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
                NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
                [outputDateFormatter setTimeZone:outputTimeZone];
                [outputDateFormatter setDateFormat:dateFormat];
                NSString *outputString = [outputDateFormatter stringFromDate:date];
                
                NSDate *scheduleDate = [[NSDate alloc] init];
                scheduleDate = [outputDateFormatter dateFromString:outputString];
                
                NSComparisonResult result = [dateTwoDaysAgo compare:scheduleDate];
                
                if(result == NSOrderedDescending)
                {
                    NSLog(@"dateTwoDaysAgo is Greater");
                    
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"workorderId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        NSString *strCustSignToDelete=[objCoreData valueForKey:@"customerSignaturePath"];
                        NSString *strTechSignToDelete=[objCoreData valueForKey:@"technicianSignaturePath"];
                        
                        if (strAudioNameToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        if (strCustSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strCustSignToDelete];
                            
                        }
                        if (strTechSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strTechSignToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailServiceAutoToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        NSLog(@"images Name to br deleted for WorkOrderId===%@---%@",[objCoreData valueForKey:@"workorderId"],arrOfImagesNameToDelete);
                        
                        [self deleteFromCoreDataServiceInfoDateWiseOldData:[objCoreData valueForKey:@"workorderId"]];
                        
                    }
                }
                else if(result == NSOrderedAscending)
                {
                    NSLog(@"scheduleDate is greater");
                }
                else
                {
                    NSLog(@"Equal date");
                    if ([[objCoreData valueForKey:@"zSync"] isEqualToString:@"no"] || [objCoreData valueForKey:@"zSync"] ==nil || [[objCoreData valueForKey:@"zSync"] isEqualToString:@"(null)"]) {
                        
                        [arrOfDataBeingDeleted addObject:[objCoreData valueForKey:@"workorderId"]];
                        
                        NSString *strAudioNameToDelete=[objCoreData valueForKey:@"audioFilePath"];
                        NSString *strCustSignToDelete=[objCoreData valueForKey:@"customerSignaturePath"];
                        NSString *strTechSignToDelete=[objCoreData valueForKey:@"technicianSignaturePath"];
                        
                        if (strAudioNameToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strAudioNameToDelete];
                            
                        }
                        if (strCustSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strCustSignToDelete];
                            
                        }
                        if (strTechSignToDelete.length>0) {
                            
                            [arrOfImagesNameToDelete addObject:strTechSignToDelete];
                            
                        }
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchPaymentInfoSignToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        [arrOfImagesNameToDelete addObjectsFromArray:[self fetchImageDetailServiceAutoToDelete:[objCoreData valueForKey:@"workorderId"]]];
                        
                        
                        [self deleteFromCoreDataServiceInfoDateWiseOldData:[objCoreData valueForKey:@"workorderId"]];
                        
                    }
                }
                
                //technicianSignaturePath   audioFilePath customerSignaturePath
                
                
            }
            
            for (int k=0; k<arrOfImagesNameToDelete.count; k++) {
                
                [self removeImage:arrOfImagesNameToDelete[k]];
                
            }
            NSLog(@"images Name to be deleted for Leadssss===---%@",arrOfImagesNameToDelete);
            
            // NSString *strCountDeleted=[NSString stringWithFormat:@"Service Order Deleted----%lu",(unsigned long)arrOfDataBeingDeleted.count];
            
            //NSString *strLeadsss=[arrOfDataBeingDeleted componentsJoinedByString:@","];
            
            // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:strCountDeleted message:strLeadsss delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //[alert show];
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(NSMutableArray*)fetchPaymentInfoSignToDeleteSalesAuto :(NSString *)strLeadOrderIdDelete
{
    NSMutableArray *arrOfSignImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfo= [NSEntityDescription entityForName:@"PaymentInfo" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfo];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadOrderIdDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrPaymentInfo = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrPaymentInfo.count==0)
    {
        
        
    }else
    {
        
        for (int k=0; k<arrPaymentInfo.count; k++)
        {
            NSManagedObject *objCoreData =arrPaymentInfo[k];
            
            NSString *strCustomerImage=[objCoreData valueForKey:@"customerSignature"];
            NSString *strSalesPersonImage=[objCoreData valueForKey:@"salesSignature"];
            
            if (!(strCustomerImage.length==0)) {
                
                [arrOfSignImage addObject:strCustomerImage];//salesSignature
                
            }
            if (!(strSalesPersonImage.length==0)) {
                
                [arrOfSignImage addObject:strSalesPersonImage];//salesSignature
                
            }
            
        }
    }
    
    return arrOfSignImage;
}

-(NSMutableArray*)fetchPaymentInfoSignToDelete :(NSString *)strWorkOrderIdDelete
{
    NSMutableArray *arrOfSignImage=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityPaymentInfoServiceAuto= [NSEntityDescription entityForName:@"PaymentInfoServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityPaymentInfoServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrPaymentInfo = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrPaymentInfo.count==0)
    {
        
        
    }else
    {
        
        for (int k=0; k<arrPaymentInfo.count; k++)
        {
            NSManagedObject *objCoreData =arrPaymentInfo[k];
            
            NSString *strCheckFrontImage=[objCoreData valueForKey:@"checkFrontImagePath"];
            NSString *strCheckBackImage=[objCoreData valueForKey:@"checkBackImagePath"];
            
            if (!(strCheckFrontImage.length==0)) {
                
                [arrOfSignImage addObject:strCheckFrontImage];
                
            }
            if (!(strCheckBackImage.length==0)) {
                
                [arrOfSignImage addObject:strCheckBackImage];
                
            }
        }
    }
    
    return arrOfSignImage;
}

-(NSMutableArray*)fetchImageDetailSalesAutoToDelete :(NSString*)strLeaddIdToDelete
{
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"ImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeaddIdToDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrObjectImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrObjectImage.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrObjectImage.count; k++)
        {
            NSManagedObject *objCoredata=arrObjectImage[k];
            NSLog(@"LeadIsss IDDDD====%@",[objCoredata valueForKey:@"leadId"]);
            
            NSString *strImageName=[objCoredata valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrImageDetail addObject:strImageName];//salesSignature
                
            }
        }
    }
    
    return arrImageDetail;
    
}

-(NSMutableArray*)fetchImageDetailServiceAutoToDelete :(NSString*)strWorkOrderIdToDelete
{
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetailServiceAuto= [NSEntityDescription entityForName:@"ImageDetailsServiceAuto" inManagedObjectContext:context];
    [request setEntity:entityImageDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdToDelete];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrObjectImage = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrObjectImage.count==0)
    {
    }
    else
    {
        for (int k=0; k<arrObjectImage.count; k++)
        {
            NSManagedObject *objCoredata=arrObjectImage[k];
            NSLog(@"WorkOrderID IDDDD====%@",[objCoredata valueForKey:@"workorderId"]);
            
            NSString *strImageName=[objCoredata valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrImageDetail addObject:strImageName];//salesSignature
                
            }
        }
    }
    
    return arrImageDetail;
    
}


- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
        NSLog(@"Deleted filePath -:%@ ",filePath);
        
        //        UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        //        [removedSuccessFullyAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

-(void)salesFetchCountLeads
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadDetail=[NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"employeeNo_Mobile=%@",strEmployeeNo];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrSalesCount = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    [self ServiceAutomationFetchCountWorkOrder:arrSalesCount];
    
}


-(void)ServiceAutomationFetchCountWorkOrder :(NSArray *)arrOfSalesCount
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityWorkOderDetailServiceAuto=[NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    requestNewService = [[NSFetchRequest alloc] init];
    [requestNewService setEntity:entityWorkOderDetailServiceAuto];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNewService setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewService managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *ArrServiceCount = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    NSString *strCounts=[NSString stringWithFormat:@"Sales Count--%lu and Service Count--%lu",(unsigned long)arrOfSalesCount.count,(unsigned long)ArrServiceCount.count];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Count" message:strCounts delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}

-(BOOL)isSyncedWO :(NSString *)strWOid{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityWorkOderDetailServiceAuto= [NSEntityDescription entityForName:@"WorkOrderDetailsService" inManagedObjectContext:context];
    [request setEntity:entityWorkOderDetailServiceAuto];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWOid];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"workorderId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerServiceAutomation = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServiceAutomation setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServiceAutomation performFetch:&error1];
    NSArray *arrWO = [self.fetchedResultsControllerServiceAutomation fetchedObjects];
    
    if (arrWO.count==0)
    {
        
        return NO;
        
    }else
    {
        NSManagedObject *obj=arrWO[0];
        NSString *strzSync=[obj valueForKey:@"zSync"];
        //NSString *strStatusToCheck=[obj valueForKey:@"workorderStatus"];
        
        if ([strzSync isEqualToString:@"yes"]) {
            
//            if ([strStatusToCheck isEqualToString:@"Completed"] || [strStatusToCheck isEqualToString:@"Complete"]) {
//                
//                return NO;
//                
//            }else{
            
                return YES;
                
//            }
            
        }else{
            return NO;
        }
    }
    
}
-(BOOL)isSyncedLeadsSales :(NSString *)strWOid{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"LeadDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strWOid];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrWO = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrWO.count==0)
    {
        
        return NO;
        
    }else
    {
        NSManagedObject *obj=arrWO[0];
        NSString *strzSync=[obj valueForKey:@"zSync"];
        //NSString *strStatusToCheck=[obj valueForKey:@"statusSysName"];
        
        if ([strzSync isEqualToString:@"yes"]) {
            
//            if ([strStatusToCheck isEqualToString:@"Completed"] || [strStatusToCheck isEqualToString:@"Complete"]) {
//                
//                return NO;
//                
//            }else{
            
                return YES;
                
//            }
            
        }else{
            return NO;
        }
    }
    
}
//Nilind 23 Feb
-(NSArray*)allEntriesInContext:(NSManagedObjectContext*)context1 fromDate:(NSDate*)fromDate toDate:(NSDate*)toDate
{
    
    // Create the request
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"LeadDetail"];
    // Build the predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date >= %@ && date <= %@ ", fromDate, toDate];
    request.predicate = predicate;
    // Define sorting
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    request.sortDescriptors = @[sortDesc];
    
    // Execute the request
    NSError *error;
    NSArray *entries = [context1 executeFetchRequest:request error:&error];
    
    if(error){
        //!!!b Error management
    }
    
    return entries;
}

-(NSString *)increaseDateForProposal
{
    NSDate *today=[NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components=[[NSDateComponents alloc] init];
    components.day=1;
    NSDate *targetDatePlus =[calendar dateByAddingComponents:components toDate:today options: 0];
    NSDate *targetDatePrevious =[calendar dateByAddingComponents:components toDate:today options: 0];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    
    [pickerDate setMaximumDate:targetDatePlus];
    [pickerDate setMinimumDate:targetDatePrevious];
    NSString* finalTime = [dateFormatter stringFromDate:targetDatePlus];
    NSLog(@"%@",finalTime);
    return finalTime;
}
//End

//Nilind 19 May
-(void)fetchSoldStandardForOneTime:(NSString*)strLeadId
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"SoldServiceStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    //arrAllObj=[[NSArray alloc]init];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOnTimeSoldStandard=[[NSMutableArray alloc]init];
    arrNonOneTime=[[NSMutableArray alloc]init];
    NSManagedObject *matches21;
    
    if (arrAllObj12.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObj12.count; k++)
        {
            matches21=arrAllObj12[k];
            NSLog(@"Lead IDDDD====%@",[matches21 valueForKey:@"leadId"]);
            if([[matches21 valueForKey:@"serviceFrequency"] isEqualToString:@"OneTime"]||[[matches21 valueForKey:@"serviceFrequency"] isEqualToString:@"One Time"])
                
            {
                [arrOnTimeSoldStandard addObject:@"OneTime"];
            }
            else
            {
                //[arrOnTimeSoldStandard addObject:@"NonTime"];
                [arrNonOneTime addObject:@"NonTime"];
                                
            }
        }
        
    }
    
}

-(void)fetchSoldNonStandardForOneTime:(NSString*)strLeadId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceNonStandardDetail=[NSEntityDescription entityForName:@"SoldServiceNonStandardDetail" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceNonStandardDetail];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@ AND isSold=%@",strLeadId,@"true"];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObj12 = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    arrOneTimeSoldNonStandard=[[NSMutableArray alloc]init];
    if (arrAllObj12.count==0)
    {
    }else
    {
        
        NSManagedObject *matches21;
        for (int k=0; k<arrAllObj12.count; k++)
        {
            matches21=arrAllObj12[k];
            NSLog(@"Lead IDDDD====%@",[matches21 valueForKey:@"leadId"]);
            [arrOneTimeSoldNonStandard addObject:[matches21 valueForKey:@"serviceName"]];
        }
    }
}

-(BOOL)showGenerateWorkorderButton:(NSString*)strLeadId
{
    [self fetchSoldStandardForOneTime:strLeadId];
    [self fetchSoldNonStandardForOneTime:strLeadId];
    
    if(arrNonOneTime.count>0)
    {
        return NO;
    }
    else
    {
        if (arrOnTimeSoldStandard.count>0||arrOneTimeSoldNonStandard.count>0)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    /*if (arrOnTimeSoldStandard.count>0||arrOneTimeSoldNonStandard.count>0)
     {
     return YES;
     }
     else
     {
     return NO;
     }*/
}

-(void)uploadImageServiceAddImage :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        //http://tcrms.stagingsoftware.com/api/File/UploadCustomerImagesAsync
        //NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        
        NSUserDefaults *defsAddImage=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginDataAddImage=[defsAddImage valueForKey:@"LoginDetails"];
        NSString *strServiceAddImageUrl=[dictLoginDataAddImage valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"];
        
        
        NSString *urlString;// =@"http://tcrms.stagingsoftware.com/api/File/UploadCustomerAddressImageAsync";// @"http://tcrms.stagingsoftware.com/api/File/UploadCustomerImagesAsync";
        urlString=[NSString stringWithFormat:@"%@api/File/UploadCustomerAddressImageAsync",strServiceAddImageUrl];
        
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
            
        }
        
    }
}
//Nilind 11 Dec
//Nilind 11 Dec
-(void)fetchTermiteImageDetailServiceAuto
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityImageDetailsTermite=[NSEntityDescription entityForName:@"ImageDetailsTermite" inManagedObjectContext:context];
    requestImageDetail = [[NSFetchRequest alloc] init];
    [requestImageDetail setEntity:entityImageDetailsTermite];
    
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId = %@",strWorkOrderIdGlobal];
    
    [requestImageDetail setPredicate:predicate];
    
    sortDescriptorImageDetail = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];
    sortDescriptorsImageDetail = [NSArray arrayWithObject:sortDescriptorImageDetail];
    
    [requestImageDetail setSortDescriptors:sortDescriptorsImageDetail];
    
    self.fetchedResultsControllerImageDetail = [[NSFetchedResultsController alloc] initWithFetchRequest:requestImageDetail managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerImageDetail setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    NSMutableArray *arrOfTermiteImages;
    arrOfTermiteImages=[[NSMutableArray alloc]init];
    [self.fetchedResultsControllerImageDetail performFetch:&error];
    arrAllObj = [self.fetchedResultsControllerImageDetail fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrImageDetail forKey:@"ImageDetailsTermite"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"workorderId"]);
            
            NSString *strImageName=[matches valueForKey:@"woImagePath"];
            if (!(strImageName.length==0)) {
                
                [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                [arrPaymentInfoValue addObject:str];
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            int indexToRemove=-1;
            int indexToReplaceModifyDate=-1;
            
            for (int k=0; k<arrPaymentInfoKey.count; k++) {
                
                NSString *strKeyLeadId=arrPaymentInfoKey[k];
                
                if ([strKeyLeadId isEqualToString:@"workorderId"]) {
                    
                    indexToRemove=k;
                    
                }
                if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
                    
                    indexToReplaceModifyDate=k;
                    
                }
            }
            
            NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
            
            [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
            
            [arrKeyTemp removeObjectAtIndex:indexToRemove];
            arrPaymentInfoKey=arrKeyTemp;
            [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
            
            [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1+1 withObject:strModifyDateToSendToServerAll];
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            //Nilind
            
            NSMutableDictionary *dictTemp;
            dictTemp=[[NSMutableDictionary alloc]init];
            dictTemp=[NSMutableDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            // [dictTemp setObject: [NSString stringWithFormat:@"%@",[dictPaymentInfo objectForKey: @"descriptionImageDetail"]] forKey: @"description"];//
            //  [dictTemp removeObjectForKey:@"descriptionImageDetail"];
            NSLog(@"%@",dictTemp);
            
            
            //.................................................
            
            //[arrImageName addObject:[NSString stringWithFormat:[dictPaymentInfo valueForKey:@""]]];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictTemp];
            NSLog(@"ImageDetail%@",arrImageDetail);
        }
        [dictFinal setObject:arrImageDetail forKey:@"TexasTermiteImagesDetail"];
        
    }
}

//End
//Nilind 11 Dec
//============================================================================
//============================================================================
#pragma mark- Upload Termite Image METHOD
//============================================================================
//============================================================================

-(void)uploadImageTermite :(NSString*)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    } else {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlSalesImageUpload];
        //http://192.168.0.218:3333//api/File/UploadAsync
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
            
        }
        
    }
    
    // indexToSendImage++;
    
    // [self uploadingAllImages:indexToSendImage];
    
}
-(void)fetchAgreementCheckListSales
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAgreementChecklistSetups=[NSEntityDescription entityForName:@"LeadAgreementChecklistSetups" inManagedObjectContext:context];
    [request setEntity:entityLeadAgreementChecklistSetups];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrEmailDetail;
    arrEmailDetail=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }else
    {
        
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                if([str isEqual:nil] || str.length==0 )
                {
                    str=@"";
                }
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            /*
             int indexToRemove=-1;
             int indexToReplaceModifyDate=-1;
             
             for (int k=0; k<arrPaymentInfoKey.count; k++) {
             
             NSString *strKeyLeadId=arrPaymentInfoKey[k];
             
             if ([strKeyLeadId isEqualToString:@"leadId"]) {
             
             indexToRemove=k;
             
             
             if ([strKeyLeadId isEqualToString:@"modifiedDate"]) {
             
             indexToReplaceModifyDate=k;
             
             }
             }
             
             NSMutableArray *arrKeyTemp=[[NSMutableArray alloc]init];
             
             [arrKeyTemp addObjectsFromArray:arrPaymentInfoKey];
             
             [arrKeyTemp removeObjectAtIndex:indexToRemove];
             arrPaymentInfoKey=arrKeyTemp;
             [arrPaymentInfoValue removeObjectAtIndex:indexToRemove];
             
             [arrPaymentInfoValue replaceObjectAtIndex:indexToReplaceModifyDate-1 withObject:strModifyDateToSendToServerAll];
             */
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            
            NSDictionary *dictAgreement;
            dictAgreement = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictAgreement);
            [arrEmailDetail addObject:dictAgreement];
            NSLog(@"EmailDetail%@",arrEmailDetail);
        }
        [dictFinal setObject:arrEmailDetail forKey:@"LeadAgreementChecklistSetups"];
        
    }
}

-(void)fetchElectronicAuthorizedForm
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityElectronicAuthorizedForm = [NSEntityDescription entityForName:@"ElectronicAuthorizedForm" inManagedObjectContext:context];
    [request setEntity:entityElectronicAuthorizedForm];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    //arrCheckImage=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"ElectronicAuthorizationForm"];
        
    }else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            for (int k=0; k<arrAllObj.count; k++)
            {
                matches=arrAllObj[k];
                NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
                
                NSString  *strCustomerImage=[matches valueForKey:@"signaturePath"];
                if (!(strCustomerImage.length==0)) {
                    
                    strElectronicSign=strCustomerImage;
                    
                }
                
                NSArray *arrElectronicAuthorizedFormKey;
                NSMutableArray *arrElectronicAuthorizedFormValue;
                
                arrElectronicAuthorizedFormValue=[[NSMutableArray alloc]init];
                arrElectronicAuthorizedFormKey = [[[matches entity] attributesByName] allKeys];
                NSLog(@"all keys %@",arrElectronicAuthorizedFormKey);
                
                NSLog(@"all keys %@",arrElectronicAuthorizedFormValue);
                for (int i=0; i<arrElectronicAuthorizedFormKey.count; i++)
                {
                    NSString *str;
                    str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrElectronicAuthorizedFormKey objectAtIndex:i]]];
                    
                    BOOL isTrue=[global isStringNullNilBlank:str];
                    if (isTrue) {
                        str=@"";
                    }
                    
                    [arrElectronicAuthorizedFormValue addObject:str];
                }
                
                //-----------------------------------------------------------------------------
                //-----------------------------------------------------------------------------
                
                NSDictionary *dictPaymentInfo;
                dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrElectronicAuthorizedFormValue forKeys:arrElectronicAuthorizedFormKey];
                NSLog(@"ElectronicAuthorizationForm%@",dictPaymentInfo);
                [dictFinal setObject:dictPaymentInfo forKey:@"ElectronicAuthorizationForm"];
            }
            
        }
    }
}
//Upload Electronic Signature Image
-(void)uploadSignatureImageOnServer:(NSString *)strImageName
{
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
        
    }
    else
    {
        
        NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
        NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
        NSString *strServiceUrlMainSalesProcess=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesProcessModule.ServiceUrl"]];
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
        
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        NSString *strCheckUrl;
        strCheckUrl=@"api/File/UploadSignatureAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strServiceUrlMainSalesProcess,strCheckUrl];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
    }
    
    
}
-(void)fetchForAppliedDiscountFromCoreData
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadAppliedDiscounts= [NSEntityDescription entityForName:@"LeadAppliedDiscounts" inManagedObjectContext:context];
    [request setEntity:entityLeadAppliedDiscounts];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadAppliedDiscount;
    arrLeadAppliedDiscount=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadAppliedDiscounts%@",dictPaymentInfo);
            [arrLeadAppliedDiscount addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadAppliedDiscount forKey:@"LeadAppliedDiscounts"];
    }
}
#pragma mark- ----------- Fetch Record For Clark Pest -------------

#pragma mark- ------ Fetch Scope --------

-(void)fetchScopeFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialScopeExtDc=[NSEntityDescription entityForName:@"LeadCommercialScopeExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialScopeExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialScopeExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialScopeExtDc"];
    }
    
}
#pragma mark- ------- Fetch Target --------

-(void)fetchTargetFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTargetExtDc=[NSEntityDescription entityForName:@"LeadCommercialTargetExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTargetExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
             //Nilind  28 Nov 2019
             NSMutableArray *arrImage;
             for (int i=0; i<arrInfoKey.count; i++)
             {
             NSString *strLeadCommercialTargetId, *strMobileTargetId;
             strLeadCommercialTargetId = [matches valueForKey:@"leadCommercialTargetId"];
             strMobileTargetId = [matches valueForKey:@"mobileTargetId"];
             
             arrImage = [[NSMutableArray alloc]init];
             arrImage = [self fetchTargetImageDetail:strLeadCommercialTargetId MobileTargetId:strMobileTargetId];
             }
             NSMutableDictionary *dictPaymentInfo;
             dictPaymentInfo = [NSMutableDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
             [dictPaymentInfo setObject:arrImage forKey:@"targetImageDetailExtDc"];
             //End
            
//            NSDictionary *dictPaymentInfo;
//            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTargetExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTargetExtDc"];
    }
    
    [self fetchAllTargetImages];
}

#pragma mark- ------- Fetch LeadCommercialInitialInfoFromCoreData --------

-(void)fetchLeadCommercialInitialInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialInitialInfoExtDc=[NSEntityDescription entityForName:@"LeadCommercialInitialInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialInitialInfoExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    // arrAllObj=[[NSArray alloc]init];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialInitialInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialInitialInfoExtDc"];
    }
    
    
    
}
#pragma mark- ------- Fetch LeadCommercialMaintInfoFromCoreData --------

-(void)fetchLeadCommercialMaintInfoFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entitySoldServiceStandardDetail=[NSEntityDescription entityForName:@"LeadCommercialMaintInfoExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entitySoldServiceStandardDetail];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects] ;
    
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialMaintInfoExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialMaintInfoExtDc"];
    }
    
    
}
#pragma mark- ------- Fetch MultiTermsFromCoreDataClarkPest --------

-(void)fetchMultiTermsFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialTermsExtDc=[NSEntityDescription entityForName:@"LeadCommercialTermsExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialTermsExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialTermsExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialTermsExtDc"];
        
    }
}

#pragma mark- ------- Fetch LeadCommercialDiscountExtDc --------

-(void)fetchForAppliedDiscountFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDiscountExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrLeadCommercialScope;
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
        
        
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadCommercialDiscountExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadCommercialDiscountExtDc"];
    }
    
}
#pragma mark- ------- Fetch LeadCommercialDetailExtDc --------


-(void)fetchLeadCommercialDetailExtDcFromCoreData
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadCommercialDiscountExtDc=[NSEntityDescription entityForName:@"LeadCommercialDetailExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadCommercialDiscountExtDc];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:@"" forKey:@"LeadCommercialDetailExtDc"];
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            NSDictionary *dictLeadPreference;
            dictLeadPreference = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            NSLog(@"LeadCommercialDetailExtDc%@",dictLeadPreference);
            [dictFinal setObject:dictLeadPreference forKey:@"LeadCommercialDetailExtDc"];
        }
        
    }
    
}

#pragma mark- ------- Fetch Sales Marketing Content FromCoreDataClarkPest --------

-(void)fetchSalesMarketingContentFromCoreDataClarkPest
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    entityLeadMarketingContentExtDc=[NSEntityDescription entityForName:@"LeadMarketingContentExtDc" inManagedObjectContext:context];
    requestNew = [[NSFetchRequest alloc] init];
    [requestNew setEntity:entityLeadMarketingContentExtDc];
    NSPredicate *predicate;
    predicate=[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    [requestNew setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [requestNew setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNew managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    arrAllObj = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    NSMutableArray *arrLeadCommercialScope;
    
    arrLeadCommercialScope=[[NSMutableArray alloc]init];
    if (arrAllObj.count==0)
    {
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
    else
    {
        for (int k=0; k<arrAllObj.count; k++)
        {
            matches=arrAllObj[k];
            NSLog(@"Lead IDDDD====%@",[matches valueForKey:@"leadId"]);
            
            NSArray *arrInfoKey;
            NSMutableArray *arrInfoValue;
            
            arrInfoValue=[[NSMutableArray alloc]init];
            arrInfoKey = [[[matches entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrInfoKey);
            
            NSLog(@"all keys %@",arrInfoValue);
            for (int i=0; i<arrInfoKey.count; i++)
            {
                NSString *str;
                str=([[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matches valueForKey:[NSString stringWithFormat:@"%@",[arrInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrInfoValue addObject:str];
            }
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrInfoValue forKeys:arrInfoKey];
            NSLog(@"LeadMarketingContentExtDc%@",dictPaymentInfo);
            [arrLeadCommercialScope addObject:dictPaymentInfo];
            
        }
        [dictFinal setObject:arrLeadCommercialScope forKey:@"LeadMarketingContentExtDc"];
    }
}
-(NSString*)getInspectorNameFromId:(NSString *)strInspectorId
{
    NSString *str=@"";
    
    NSArray *arrEmployee;
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    arrEmployee=[defs valueForKey:@"EmployeeList"];
    
    if ([arrEmployee isEqual:@""]|| arrEmployee.count==0 || [arrEmployee isKindOfClass:[NSDictionary class]])
    {
    }
    else
    {
        for (int i=0; i<arrEmployee.count; i++)
        {
            NSDictionary *dict=[arrEmployee objectAtIndex:i];
            if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"EmployeeId"]]isEqualToString:strInspectorId])
            {
                str=[dict valueForKey:@"FullName"];
                break;
            }
        }
    }
    str = [global checkIfStringNull:str];

    return str;
}

-(NSArray*)arrFilter :(NSArray*)arrToFilter :(NSString*)strKey :(BOOL)yesNO {
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: strKey ascending: yesNO];
    NSArray *sortedArray = [arrToFilter sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return sortedArray;
    
}

-(void)getEmpBlockTime{
    
    BOOL isNetAvailable = [global isNetReachable];
    
    if (isNetAvailable) {
        
        // Employee Block Time
        arrOfEmpBlockTime = [global getEmployeeBlockTime:strServiceUrlMainCoreServiceModule :strCompanyKey :strEmployeeNo];
        
        if ([arrOfEmpBlockTime isKindOfClass:[NSArray class]]) {
            
            
            
        }else{
            
            arrOfEmpBlockTime = nil;
            
        }
        
        NSLog(@"EMployee Block Time = = = %@",arrOfEmpBlockTime);
        
    }else{
        
        NSUserDefaults *blockDefs = [NSUserDefaults standardUserDefaults];
        
        arrOfEmpBlockTime = [blockDefs objectForKey:@"BlockTime"];
        
        if ([arrOfEmpBlockTime isKindOfClass:[NSArray class]]) {
            
            
            
        }else{
            
            arrOfEmpBlockTime = nil;
            
        }
        
        NSLog(@"EMployee Block Time = = = %@",arrOfEmpBlockTime);
        
    }
    
//    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
//    NSString * stateJsonPath = [resourcePath stringByAppendingPathComponent:@"TestingJson.json"];
//    NSError * error;
//    NSData *jsonData = [NSData dataWithContentsOfFile:stateJsonPath options:kNilOptions error:&error ];
//    arrOfEmpBlockTime = ([[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL] isKindOfClass:[NSNull class]]) ? @"" : [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
    
}

-(void)filterAllAppointmentsIncludingBlockTime{
    
    NSMutableArray *arrTodayBlockDates = [[NSMutableArray alloc]init];
    
    if (isTodayAppointments) {
        
        for (int P=0; P<arrOfEmpBlockTime.count; P++) {
            //2019-04-03T11:00:00
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            NSDate *dateFromString = [[NSDate alloc] init];
            NSDictionary *dictData = arrOfEmpBlockTime[P];
            dateFromString = [dateFormatter dateFromString:[dictData valueForKey:@"fromDate"]];
            
            BOOL yesSameDay=[self isSameDay:[NSDate date] otherDay:dateFromString];
            
            if (yesSameDay) {
                
                [arrTodayBlockDates addObject:dictData];
                
            }
            
        }
        
    }else{
        
        [arrTodayBlockDates addObjectsFromArray:arrOfEmpBlockTime];
        
    }
    
    NSMutableArray *arrTemp =[[NSMutableArray alloc]init];
    NSArray *arrTempFiltered =[[NSArray alloc]init];
    
    //[arrTemp addObjectsFromArray:arrAllObj];
    
    for (int k=0; k<arrTodayBlockDates.count; k++) {
        
        [arrTemp addObject:arrTodayBlockDates[k]];
        
    }
    for (int k=0; k<arrAllObj.count; k++) {
        
        [arrTemp addObject:arrAllObj[k]];
        
    }
    NSUserDefaults *defsApp = [NSUserDefaults standardUserDefaults];
    
    BOOL isSortByScheduleDate = [defsApp boolForKey:@"SortByScheduleDate"];
    
    if (isSortByScheduleDate) {
        
        BOOL isSortByScheduleDateAscending = [defsApp boolForKey:@"SortByScheduleDateAscending"];
        
        if (isSortByScheduleDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"zdateScheduledStart" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"fromDate" :NO];
            
        }
        
    } else {
        
        BOOL isSortByModifiedDateAscending = [defsApp boolForKey:@"SortByModifiedDateAscending"];
        
        if (isSortByModifiedDateAscending) {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:YES];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        } else {
            
            //sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateModified" ascending:NO];
            
            arrTempFiltered = [self arrFilter:arrTemp :@"modifyDate" :YES];
            
        }
        
    }
    
    arrAllObj = nil;
    
    arrAllObj = arrTempFiltered;
    
    [_tblViewAppointment reloadData];
    
}


-(void)updatingStatusOnMyWayLifeStyle{
    
    CLLocationCoordinate2D coordinate = [global getLocation] ;
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSArray *objects1 = [NSArray arrayWithObjects:
                         strCoreCompanyId,
                         [NSString stringWithFormat:@"%@",@"0"],
                         [NSString stringWithFormat:@"%@",strWorkOrderNoLifeStyle],
                         @"true",
                         strEmpID,
                         latitude,
                         longitude,nil];
    
    NSArray *keys1 = [NSArray arrayWithObjects:
                      @"CompanyId",
                      @"WorkOrderId",
                      @"WorkOrderNo",
                      @"Status",
                      @"EmployeeId",
                      @"OnMyWayLatitude",
                      @"OnMyWayLongitude",nil];
    
    NSDictionary *dict_ToSend = [[NSDictionary alloc] initWithObjects:objects1 forKeys:keys1];
    
    [global updateStatusOnMyWayLifeStyle:strServiceUrlMainCoreServiceModule :dict_ToSend];
    
}


#pragma mark- Service Pest New Flow Fetch Methods

-(void)fetchServicePestDataAllFromDBObjC{
    
    // Areas
    
    NSString *strServiceAddressIdTemp;
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workorderId=%@",strWorkOrderIdGlobal];

    WebService *objWebService = [[WebService alloc] init];

    NSArray *arrTempData = [objWebService getDataFromCoreDataBaseArrayObjectiveCWithStrEntity:@"WorkOrderDetailsService" predicate:predicate];

    if (arrTempData.count>0) {
        
        NSManagedObject *objTemp = arrTempData[0];

        strServiceAddressIdTemp = [NSString stringWithFormat:@"%@",[objTemp valueForKey:@"ServiceAddressId"]];
        
    }
    
    
    NSMutableArray *arrTemp = [self fetchServicePestNewFlowData:@"ServiceAreas" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceAreas"];
    
    // ServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDevices" :@"No" : strServiceAddressIdTemp];
    [dictFinal setObject:arrTemp forKey:@"ServiceDevices"];
    
    // ServiceProducts
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceProducts" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceProducts"];
    
    // ServicePests
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServicePests" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServicePests"];
    
    // ServiceConditions
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditions" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditions"];
    
    // ServiceComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceComments"];
    
    
    // ServiceConditionComments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionComments" :@"No" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionComments"];
    
    
    // ServicePestDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"SAPestDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"SAPestDocuments"];
    
    
    // ServiceConditionDocuments
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceConditionDocuments" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceConditionDocuments"];
    
    // ServiceDeviceInspection
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"ServiceDeviceInspections" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"ServiceDeviceInspections"];
    
    // WOServiceDevices
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceDevices" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceDevices"];
    
    // WOServiceAreas
    arrTemp = [[NSMutableArray alloc] init];
    arrTemp = [self fetchServicePestNewFlowData:@"WOServiceAreas" :@"Yes" : @""];
    [dictFinal setObject:arrTemp forKey:@"WOServiceAreas"];
}


-(NSMutableArray*)fetchServicePestNewFlowData :(NSString*)strEntityName :(NSString*)strIsDocument :(NSString*)strServiceAddressIdForAreaDevice
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription * entityLocal= [NSEntityDescription entityForName:strEntityName inManagedObjectContext:context];
    [request setEntity:entityLocal];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId=%@",strWorkOrderIdGlobal];
    
    if (strServiceAddressIdForAreaDevice.length>0){
        
        predicate =[NSPredicate predicateWithFormat:@"serviceAddressId=%@",strServiceAddressIdForAreaDevice];
        
    }
    
    [request setPredicate:predicate];
    
    NSSortDescriptor * sortDescriptorLocal = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:YES];
    NSArray * sortDescriptorsLocal = [NSArray arrayWithObject:sortDescriptorLocal];
    
    [request setSortDescriptors:sortDescriptorsLocal];
    
    self.fetchedResultsControllerServicePestNewFlowArea = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerServicePestNewFlowArea setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerServicePestNewFlowArea performFetch:&error1];
    NSArray *ObjAllData = [self.fetchedResultsControllerServicePestNewFlowArea fetchedObjects];
    
    NSMutableArray *arrData;
    arrData=[[NSMutableArray alloc]init];
    
    if (ObjAllData.count==0)
    {
        
        //[dictFinal setObject:arrData forKey:@"EmailDetail"];
        
    }else
    {
        
        for (int k=0; k<ObjAllData.count; k++)
        {
            
            NSManagedObject *matchesLocal=ObjAllData[k];
            
            NSArray *keys = [[[matchesLocal entity] attributesByName] allKeys];
            NSDictionary *dictOfData = [matchesLocal dictionaryWithValuesForKeys:keys];
            
            if ([strIsDocument isEqualToString:@"Yes"]) {
                
                NSString *isToUpload = [NSString stringWithFormat:@"%@",[dictOfData valueForKey:@"isToUpload"]];
                
                if ([isToUpload isEqualToString:@"Yes"]) {
                    
                    [arrOfAllDocumentsToSend addObject:dictOfData];
                    
                }
                
            }
            
            NSArray *arrOfKey = [dictOfData allKeys];
            
            NSMutableArray *arrTempKey = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfKey.count; k++) {
                
                NSString *strCaps = [NSString stringWithFormat:@"%@",arrOfKey[k]];
                
                NSString *strSTR =  [NSString stringWithFormat:@"%@%@",[[strCaps substringToIndex:1] capitalizedString],[strCaps substringFromIndex:1]];
                
                if ([strCaps isEqualToString:@"isInspectingFirstTime"]) {
                    
                    [arrTempKey addObject:strCaps];
                    
                } else {
                    
                    [arrTempKey addObject:strSTR];
                    
                }
                
                
            }
            
            /// Array Of Values
            
            NSArray *arrOfValue = [dictOfData allValues];
            
            NSMutableArray *arrTempValue = [[NSMutableArray alloc] init];
            
            for (int k=0; k<arrOfValue.count; k++) {
                
                NSString *strBoolKey = [NSString stringWithFormat:@"%@",arrTempKey[k]];
                
                if ([strBoolKey isEqualToString:@"isInspectingFirstTime"] || [strBoolKey isEqualToString:@"IsActive"] || [strBoolKey isEqualToString:@"IsDeletedArea"] || [strBoolKey isEqualToString:@"IsInspected"] || [strBoolKey isEqualToString:@"IsDeletedDevice"] || [strBoolKey isEqualToString:@"Activity"] || [strBoolKey isEqualToString:@"isDeletedDevice"]) {
                    
                    if ([strBoolKey isEqualToString:@"IsDeletedDevice"]) {
                        
                        [arrTempValue addObject:@"false"];

                    }else{
                        
                        BOOL isTrue = [arrOfValue[k] boolValue];
                        
                        if (isTrue) {
                            
                            [arrTempValue addObject:@"true"];
                            
                        } else {
                            
                            [arrTempValue addObject:@"false"];
                            
                        }
                    }

                    
                } else {
                    
                    [arrTempValue addObject:[NSString stringWithFormat:@"%@",arrOfValue[k]] ];
                    
                }
                
                
            }
            
            
            //NSArray *arrOfValue = [dictOfData allValues];
            
            NSDictionary *dictData;
            dictData = [NSDictionary dictionaryWithObjects:arrTempValue forKeys:arrTempKey];
            
            [arrData addObject:dictData];
            
        }
        
    }
    
    return arrData;
    
}

//============================================================================
//============================================================================
#pragma mark- Upload  METHOD Service Pest New Flow
//============================================================================
//============================================================================


-(void)uploadDocuments :(NSString*)strDocName :(NSString*)ServerUrl
{
    
    if ((strDocName.length==0) && [strDocName isEqualToString:@"(null)"]) {
        
    } else {
        
        strDocName = [global strDocNameFromPath:strDocName];
        
        NSRange equalRange = [strDocName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strDocName = [strDocName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strDocName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",ServerUrl];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strDocName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Document Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Document Sent");
        }
        
    }
    
    // Updating Status yes to no for isToUpload
    
    NSDictionary *dictDataDocument = arrOfAllDocumentsToSend[indexToSendDocuments];
    
    NSArray *allKeys = [dictDataDocument allKeys];
    
    if ([allKeys containsObject:@"serviceAddressDocName"]) {
        
        WebService *objWebService = [[WebService alloc] init];
        
        NSMutableArray *arrTKey = [[NSMutableArray alloc] initWithObjects:@"isToUpload", nil];
        NSMutableArray *arrTValue = [[NSMutableArray alloc] initWithObjects:@"No", nil];
        
        NSString *strDocId = [NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"mobileServiceAddressDocId"]];
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && mobileServiceAddressDocId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"mobileServiceAddressDocId"],[dictDataDocument valueForKey:@"companyKey"]];

        if (strDocId.length<1) {
            
            strDocId = [NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"mobileServiceAddressDocId"]];
            predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && serviceAddressDocId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"serviceAddressDocId"],[dictDataDocument valueForKey:@"companyKey"]];
        }
        
       BOOL isSucess =  [objWebService getDataFromDbToUpdateInObjectiveCWithStrEntity:@"SAPestDocuments" predicate:predicate arrayOfKey:arrTKey arrayOfValue:arrTValue];
        
        if (isSucess) {
            
            
        }
        
        //let isSuccess =  getDataFromDbToUpdate(strEntity: "ServiceAreas", predicate: NSPredicate(format: "workOrderId == %@ && addressAreaId == %@ && companyKey == %@", strWoId, strAddressAreaId,Global().getCompanyKey()), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        
    } else {
        
        WebService *objWebService = [[WebService alloc] init];
        
        NSMutableArray *arrTKey = [[NSMutableArray alloc] initWithObjects:@"isToUpload", nil];
        NSMutableArray *arrTValue = [[NSMutableArray alloc] initWithObjects:@"No", nil];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@ && sAConditionDocumentId = %@ && companyKey = %@",[dictDataDocument valueForKey:@"workOrderId"],[dictDataDocument valueForKey:@"sAConditionDocumentId"],[dictDataDocument valueForKey:@"companyKey"]];
        
        BOOL isSucess =  [objWebService getDataFromDbToUpdateInObjectiveCWithStrEntity:@"ServiceConditionDocuments" predicate:predicate arrayOfKey:arrTKey arrayOfValue:arrTValue];
        
        if (isSucess) {
            
            
        }
        
    }
    
    
    indexToSendDocuments++;

    [self uploadingAllDocumentsPest:indexToSendDocuments];

}


-(void)uploadingAllDocumentsPest :(int)indexToSendimages{
    
    if (arrOfAllDocumentsToSend.count==indexToSendimages) {
        
        if (arrOfAllImagesToSendToServer.count==0) {
            
            if (arrOfAllSignatureImagesToSendToServer.count==0) {
                
                [self finalJsonServiceAutomation];
                
            } else {
                
                [self uploadingAllImagesServiceAutoCheck:0];
                
            }
            
        } else {
            
            [self uploadingAllImagesServiceAuto:0];
            
        }
        
    } else {
        
        // Yaha Par Documents Sync Krna hai web pr
        
        // serviceAddressDocName  WO pr jo docs hai   serviceAddressDocPath
        
        // documnetTitle   Jo Condiotion pr hai documentPath
        
        NSDictionary *dictDataDocument = arrOfAllDocumentsToSend[indexToSendimages];
        
        NSArray *allKeys = [dictDataDocument allKeys];
        
        if ([allKeys containsObject:@"serviceAddressDocName"]) {
            
            [self uploadDocuments:[NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"serviceAddressDocPath"]] :[NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServicePestDocUploadAsync]];
            
        } else {
            
            [self uploadDocuments:[NSString stringWithFormat:@"%@",[dictDataDocument valueForKey:@"documentPath"]] :[NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlServicePestConditionDocUploadAsync]];

        }

        
    }
    
}


//============================================================================
//============================================================================
#pragma mark- ---------------------Get Device Dynamic Form Data-----------------
//============================================================================
//============================================================================

-(void)getDynamicFormDataDevice{
    
    NSArray *arrAllWoTemp=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];

    if (arrAllWoTemp.count==0) {
        
        [self callMethodAfterSyncingDeviceInspection];
        
    } else {
        
        //NSArray *arrAllWoTemp=[dictForWorkOrderAppointment valueForKey:@"WorkOrderExtSerDcs"];
        
        NSMutableArray *arrTempWoIdDevice = [[NSMutableArray alloc] init];
        
        for (int k=0; k<arrAllWoTemp.count; k++) {
            
            NSDictionary *dictData = arrAllWoTemp[k];
            
            NSDictionary *dictWoDataTemp = [dictData valueForKey:@"WorkorderDetail"];
            
            NSString *strWoIdLocal = [NSString stringWithFormat:@"%@",[dictWoDataTemp valueForKey:@"WorkorderId"]];
            
            [arrTempWoIdDevice addObject:strWoIdLocal];
            
        }
        
        NSString *strWorkOrdersOfDynamicData=[arrTempWoIdDevice componentsJoinedByString:@","];
        
        if (strWorkOrdersOfDynamicData.length==0) {
            
            [self callMethodAfterSyncingDeviceInspection];

        } else {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Device Dynamic Forms"];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlDeviceDynamicFormAllWorkOrders,strCompanyKey,UrlServiceDynamicFormAllWorkOrdersWorkOrderID,strWorkOrdersOfDynamicData];
            
            NSLog(@"Get All Dynamic Data Device===%@",strUrl);
            
            //============================================================================
            //============================================================================
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"ServiceAllDynamicForm" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [DejalBezelActivityView removeView];
                         if (success)
                         {
                             
                             NSArray *arrOfDynamicData=(NSArray*)response;
                             
                             if ([arrOfDynamicData isKindOfClass:[NSArray class]]) {
                                 
                                 WebService *objWebService = [[WebService alloc] init];
                                 
                                 [objWebService savingDeviceDynamicAllDataDBWithArrOfDeviceDynamicFormData:arrOfDynamicData];
                                 
                             }
                             
                         }
                         else
                         {
                             
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                             
                         }
                         
                         [self callMethodAfterSyncingDeviceInspection];
                         
                     });
                 }];
            });
            
        }
    }
    //============================================================================
    //============================================================================
}


-(void)sendingDeviceInspectionToServer :(NSArray*)arrFinalDynamicJson{
    
    if (arrFinalDynamicJson.count==0) {
        
        indexToSendDeviceDynamicForm++;
        
        [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
        
    } else {
        
        NSArray *objValueFinalDynamicJson=[NSArray arrayWithObjects:
                                           arrFinalDynamicJson,nil];
        
        NSArray *objKeyFinalDynamicJson=[NSArray arrayWithObjects:
                                         @"DeviceTypeFormInformationJsonExtSerDc",nil];
        
        //NSDictionary *dictFinalDynamicJson=arrFinalDynamicJson[0];

        NSDictionary *dictFinalDynamicJson=[[NSDictionary alloc] initWithObjects:objValueFinalDynamicJson forKeys:objKeyFinalDynamicJson];

        
        NSError *errorNew = nil;
        NSData *json;
        NSString *jsonString;
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:arrFinalDynamicJson])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:arrFinalDynamicJson options:NSJSONWritingPrettyPrinted error:&errorNew];
            
            // If no errors, let's view the JSON
            if (json != nil && errorNew == nil)
            {
                jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"Device JSon: %@", jsonString);
            }
        }
        
        NSString *strdatafirst = [NSString stringWithFormat:@"%@",jsonString];
        
        NSData *requestData = [strdatafirst dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",strServiceUrlMainServiceAutomation,UrlDeviceDynamicFormSubmission];
        
        //============================================================================
        //============================================================================
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            [global getServerResponseForSalesDynamicJson:strUrl :requestData withCallback:^(BOOL success, NSDictionary *response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // [DejalBezelActivityView removeView];
                     if (success)
                     {
                         
                         indexToSendDeviceDynamicForm++;
                         
                         [self FetchFromCoreDataToSendDeviceDynamicForms:true :indexToSendDeviceDynamicForm];
                         
                         //[self sendingServiceDynamicFormDataToServer:0];
                         
                     }
                     else
                     {
                         
                         NSString *strTitle = Alert;
                         NSString *strMsg = Sorry;
                         [global AlertMethod:strTitle :strMsg];
                         
                     }
                 });
             }];
        });
        
        //============================================================================
        //============================================================================
    }
    
}


-(void)FetchFromCoreDataToSendDeviceDynamicForms :(BOOL)isUpdated :(int)indextoSend {
    
    
    NSUserDefaults *defs=[NSUserDefaults standardUserDefaults];
    NSMutableArray *arrOfLeadsTosendToServer=[defs valueForKey:@"sendServiceLeadToServer"];
    
    if (arrOfLeadsTosendToServer.count==indextoSend) {
        
        //[self sendingServiceDynamicFormDataToServer:0];
        
        [self getDynamicFormDataDevice];
        
    }else{
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appDelegate managedObjectContext];
        entityServiceDynamic=[NSEntityDescription entityForName:@"DeviceInspectionDynamicForm" inManagedObjectContext:context];
        requestNewServiceDynamic = [[NSFetchRequest alloc] init];
        [requestNewServiceDynamic setEntity:entityServiceDynamic];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"workOrderId = %@",arrOfLeadsTosendToServer[indextoSend]];
        
        [requestNewServiceDynamic setPredicate:predicate];
        
        sortDescriptorServiceDynamic = [[NSSortDescriptor alloc] initWithKey:@"workOrderId" ascending:NO];
        sortDescriptorsServiceDynamic = [NSArray arrayWithObject:sortDescriptorServiceDynamic];
        
        [requestNewServiceDynamic setSortDescriptors:sortDescriptorsServiceDynamic];
        
        self.fetchedResultsControllerServiceDynamic = [[NSFetchedResultsController alloc] initWithFetchRequest:requestNewServiceDynamic managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        [self.fetchedResultsControllerServiceDynamic setDelegate:self];
        
        // Perform Fetch
        NSError *error = nil;
        [self.fetchedResultsControllerServiceDynamic performFetch:&error];
        arrAllObjDeviceDynamic = [self.fetchedResultsControllerServiceDynamic fetchedObjects];
        if ([arrAllObjDeviceDynamic count] == 0)
        {
            
            //[self sendingServiceDynamicFormDataToServer:0];
            [self getDynamicFormDataDevice];

        }
        else
        {
            
            NSMutableArray *arrOfEquipToSend=[[NSMutableArray alloc] init];
            
            for (int k=0; k<arrAllObjDeviceDynamic.count; k++) {
                
                matchesServiceDynamic=arrAllObjDeviceDynamic[k];
                NSArray *arrTemp = [matchesServiceDynamic valueForKey:@"arrFinalInspection"];
                
                NSDictionary *dictDataService;
                
                if ([arrTemp isKindOfClass:[NSDictionary class]]) {
                    
                    dictDataService=(NSDictionary*)arrTemp;
                    
                } else if ([arrTemp isKindOfClass:[NSArray class]]) {
                    
                    dictDataService=arrTemp[0];
                    
                }
                
                [arrOfEquipToSend addObject:dictDataService];
                
                
            }
            
            if (arrOfEquipToSend.count==0) {
                
                //[self sendingServiceDynamicFormDataToServer:0];
                
                [self getDynamicFormDataDevice];

            } else {
                
                if (isUpdated) {
                    
                    [self sendingDeviceInspectionToServer:arrOfEquipToSend];
                    
                } else {
                    
                    //[self sendingServiceDynamicFormDataToServer:0];
                    
                    [self getDynamicFormDataDevice];

                }
                
            }
        }
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        } else {
            
        }
        
    }
    
}

-(void)getDynamicFormDataDeviceOnlySelectedWo{
    
    if (arrOfServiceLeadToFetchDynamicData.count==0) {
        
       [self getDynamicFormDataServiceEquipments];
        
    } else {

        NSString *strWorkOrdersOfDynamicData=[arrOfServiceLeadToFetchDynamicData componentsJoinedByString:@","];
        
        if (strWorkOrdersOfDynamicData.length==0) {
            
            [self getDynamicFormDataServiceEquipments];
            
        } else {
            
            [DejalBezelActivityView activityViewForView:self.view withLabel:@"Loading Device Dynamic Forms"];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@%@%@%@",strServiceUrlMainServiceAutomation,UrlDeviceDynamicFormAllWorkOrders,strCompanyKey,UrlServiceDynamicFormAllWorkOrdersWorkOrderID,strWorkOrdersOfDynamicData];
            
            NSLog(@"Get All Dynamic Data Device===%@",strUrl);
            
            //============================================================================
            //============================================================================
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                [global getServerResponseForUrlGetAynchronusMethod:strUrl :@"ServiceAllDynamicForm" withCallback:^(BOOL success, NSDictionary *response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [DejalBezelActivityView removeView];
                         if (success)
                         {
                             
                             NSArray *arrOfDynamicData=(NSArray*)response;
                             
                             if ([arrOfDynamicData isKindOfClass:[NSArray class]]) {
                                 
                                 WebService *objWebService = [[WebService alloc] init];
                                 
                                 [objWebService savingDeviceDynamicAllDataDBWithArrOfDeviceDynamicFormData:arrOfDynamicData];
                                 
                             }
                             
                         }
                         else
                         {
                             
                             NSString *strTitle = Alert;
                             NSString *strMsg = Sorry;
                             [global AlertMethod:strTitle :strMsg];
                             
                         }
                         
                         [self getDynamicFormDataServiceEquipments];
                         
                     });
                 }];
            });
            
        }
    }
    //============================================================================
    //============================================================================
}

#pragma mark: ------------- Fetch & Upload Target Image ------------------

-(NSMutableArray *)fetchTargetImageDetail:(NSString *)strLeadCommercialTargetId MobileTargetId:(NSString *)strMobileTargetId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    
    if (strLeadCommercialTargetId.length > 0)
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && leadCommercialTargetId=%@" ,strLeadIdGlobal, strLeadCommercialTargetId];
    }
    else
    {
        predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && mobileTargetId=%@",strLeadIdGlobal,strMobileTargetId];
    }
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                // [arrOfAllImagesToSendToServer addObject:strImageName];//salesSignature
                
            }
            
            NSArray *arrPaymentInfoKey;
            NSMutableArray *arrPaymentInfoValue;
            
            arrPaymentInfoValue=[[NSMutableArray alloc]init];
            arrPaymentInfoKey = [[[matchesTargetImage entity] attributesByName] allKeys];
            NSLog(@"all keys %@",arrPaymentInfoKey);
            
            NSLog(@"all keys %@",arrPaymentInfoValue);
            for (int i=0; i<arrPaymentInfoKey.count; i++)
            {
                NSString *str;
                str=([[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]]isKindOfClass:[NSNull class]]) ? @"" :[matchesTargetImage valueForKey:[NSString stringWithFormat:@"%@",[arrPaymentInfoKey objectAtIndex:i]]];
                
                BOOL isTrue=[global isStringNullNilBlank:str];
                if (isTrue) {
                    str=@"";
                }
                
                [arrPaymentInfoValue addObject:str];
            }
            
            
            NSDictionary *dictPaymentInfo;
            dictPaymentInfo = [NSDictionary dictionaryWithObjects:arrPaymentInfoValue forKeys:arrPaymentInfoKey];
            
            NSLog(@"%@",dictPaymentInfo);
            [arrImageDetail addObject:dictPaymentInfo];
            NSLog(@"TargetImageDetailImageDetail%@",arrImageDetail);
        }
        //[dictFinal setObject:arrImageDetail forKey:@"TargetImageDetail"];
        
    }
    return arrImageDetail;
}
-(void)fetchAllTargetImages
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityImageDetail= [NSEntityDescription entityForName:@"TargetImageDetail" inManagedObjectContext:context];
    [request setEntity:entityImageDetail];
    NSPredicate *predicate;
    predicate =[NSPredicate predicateWithFormat:@"leadId=%@",strLeadIdGlobal];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjtargetImage = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrImageDetail;
    arrImageDetail=[[NSMutableArray alloc]init];
    arrAllTargetImages = [[NSMutableArray alloc]init];
    NSManagedObject *matchesTargetImage;
    if (arrAllObjtargetImage.count==0)
    {
        
        
    }
    else
    {
        NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
        for (int k=0; k<arrAllObjtargetImage.count; k++)
        {
            matchesTargetImage=arrAllObjtargetImage[k];
            NSLog(@"Lead IDDDD====%@",[matchesTargetImage valueForKey:@"leadId"]);
            
            
            NSString *strImageName=[matchesTargetImage valueForKey:@"leadImagePath"];
            if (!(strImageName.length==0))
            {
                
                [arrAllTargetImages addObject:strImageName];
                
            }
        }
        
        
    }
    
}
-(void)uploadTargetImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"]) {
        
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
                    
        NSData *imageData  = UIImageJPEGRepresentation(imagee,.1);
        //NSString *strCheckUrl;
        //strCheckUrl=@"/api/File/UploadCheckImagesAsync";
        NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlUploadTargetImage];//UrlSalesImageUpload];//
        //NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,UrlSalesImageUpload];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:urlString]];
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Image Sent");
        }
        //[self HudView:strrr];
            
        }
        
    }
    
    
}

//============================================================================
#pragma mark- --------------- Upload Other Document METHOD -------------------
//============================================================================
//============================================================================
-(void)uploadOtherDocumentImage :(NSString*)strImageName
{
    
    if ((strImageName.length==0) && [strImageName isEqualToString:@"(null)"])
    {
    }
    else
    {
        if(strSalesAutoMainUrl.length==0 || [strSalesAutoMainUrl isEqual:[NSNull null]])
        {
            NSUserDefaults *defsLogindDetail=[NSUserDefaults standardUserDefaults];
            NSDictionary *dictLoginData=[defsLogindDetail valueForKey:@"LoginDetails"];
            strSalesAutoMainUrl=[NSString stringWithFormat:@"%@",[dictLoginData valueForKeyPath:@"Company.CompanyConfig.SalesAutoModule.ServiceUrl"]];
        }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strImageName]];
        UIImage *imagee = [UIImage imageWithContentsOfFile:path];
        
        if (imagee == nil){
            
        }else{
            
            NSData *imageData  = UIImageJPEGRepresentation(imagee,1);
            
            NSString *urlString = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
            //http://192.168.0.218:3333//api/File/UploadAsync
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            [request setURL:[NSURL URLWithString:urlString]];
            
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strImageName] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[NSData dataWithData:imageData]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            if ([returnString isEqualToString:@"OK"])
            {
                NSLog(@"Document Sent");
            }
            NSLog(@"Document Sent");
            
        }
    
    }
}

-(void)uploadOtherDocuments :(NSString*)strDocName DocumentId:(NSString*)strDocId
{
    NSString* ServerUrl = [NSString stringWithFormat:@"%@%@",strSalesAutoMainUrl,@"/api/File/UploadOtherDocsAsync"];
    
    if ((strDocName.length==0) && [strDocName isEqualToString:@"(null)"]) {
        
    } else {
        
        strDocName = [global strDocNameFromPath:strDocName];
        
        NSRange equalRange = [strDocName rangeOfString:@"\\" options:NSBackwardsSearch];
        if (equalRange.location != NSNotFound) {
            strDocName = [strDocName substringFromIndex:equalRange.location + equalRange.length];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strDocName]];
        NSData *audioData;
        
        audioData = [NSData dataWithContentsOfFile:path];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",ServerUrl];
        
        // setting up the request object now
        NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] init];
        [request1 setURL:[NSURL URLWithString:urlString]];
        [request1 setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request1 addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",strDocName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:audioData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request1 setHTTPBody:body];
        
        // now lets make the connection to the web
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request1 returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response On Document Upload = = = =  = = %@",returnString);
        
        if ([returnString isEqualToString:@"OK"])
        {
            NSLog(@"Document Sent");
        }
        if (returnString.length > 0)
        {
            [self updateOtherDocumentSyncStatus:strDocId];
        }
        //[{"Name":"pdfOther337825_2020618163749.pdf","Extension":".pdf","RelativePath":"UploadImages\\","Length":500337}]
        
        NSLog(@"Document Sent");
        
    }
}
-(void)fetchOtherDocument
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@",strLeadIdGlobal,@"Other"];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    arrUploadOtherDocument = [[NSMutableArray alloc]init];
    arrUploadOtherDocumentId = [[NSMutableArray alloc]init];

    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        for (int k=0; k<arrAllObjOtherDoucment.count; k++)
        {
            NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:k];
            if ([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isToUpload"]] isEqualToString:@"true"])
            {
                [arrUploadOtherDocument addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"fileName"]]];
                [arrUploadOtherDocumentId addObject:[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"leadDocumentId"]]];

            }
        }
    }
}

-(void)updateOtherDocumentSyncStatus : (NSString *)strDocId
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityLeadDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityLeadDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId=%@",strLeadIdGlobal,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];
    NSArray *arrAllObjTemp = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    if (arrAllObjTemp.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp=arrAllObjTemp[0];
        [matchesTemp setValue:@"false" forKey:@"isToUpload"];
        [context save:&error1];
    }
}

-(BOOL)checkOtherDocumentSyncStatus:(NSString *)strDocId
{
    BOOL chkStatus;
    chkStatus = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    entityDocumentsDetail= [NSEntityDescription entityForName:@"DocumentsDetail" inManagedObjectContext:context];
    [request setEntity:entityDocumentsDetail];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"leadId=%@ && docType=%@ && leadDocumentId = %@",strLeadIdGlobal,@"Other",strDocId];
    
    [request setPredicate:predicate];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"leadId" ascending:YES];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    self.fetchedResultsControllerSalesInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    [self.fetchedResultsControllerSalesInfo setDelegate:self];
    
    // Perform Fetch
    NSError *error1 = nil;
    [self.fetchedResultsControllerSalesInfo performFetch:&error1];

    NSArray *arrAllObjOtherDoucment = [self.fetchedResultsControllerSalesInfo fetchedObjects];
    
    NSMutableArray *arrDocumentsDetail;
    arrDocumentsDetail=[[NSMutableArray alloc]init];
    if (arrAllObjOtherDoucment.count==0)
    {
        
    }else
    {
        NSManagedObject *matchesTemp = [arrAllObjOtherDoucment objectAtIndex:0];
        if([[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"true"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"True"] || [[NSString stringWithFormat:@"%@",[matchesTemp valueForKey:@"isSync"]] isEqualToString:@"1"])
        {
            chkStatus = YES;
        }
        else
        {
            chkStatus = NO;
        }
    }
    return chkStatus;
}
@end

