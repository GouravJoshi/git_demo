//
//  NewCommercialMarketingContentDetailVC.swift
//  DPS
//
//  Created by Deepak Shrivas on 24/03/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit
import WebKit

class NewCommercialMarketingContentDetailVC: UIViewController {
    
    var webView: WKWebView!
    var strMarketingContent = NSMutableString()
    @IBOutlet weak var viewForWebView: UIView!

//    override func loadView() {
//        webView = WKWebView()
//        webView.navigationDelegate = self
//        viewForWebView = webView
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(strMarketingContent)
        self.loadWebViewContent()
    }

    func loadWebViewContent() {
            
        webView = WKWebView()
        webView.navigationDelegate = self
        
        webView.loadHTMLString(self.strMarketingContent as String, baseURL: nil)
        self.viewForWebView.addSubview(webView)

        webView.topAnchor.constraint(equalTo: self.viewForWebView.topAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: self.viewForWebView.leftAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: self.viewForWebView.rightAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.viewForWebView.bottomAnchor).isActive = true
        webView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension NewCommercialMarketingContentDetailVC: WKNavigationDelegate {
    
}
