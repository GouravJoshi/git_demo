//
//  newCommericialReviewViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 26/10/21.
//

import UIKit
import WebKit
import Kingfisher

class NewCommericialReviewViewController: UIViewController ,WKNavigationDelegate, CustomTableView  {
    
    //MARK:- Outlets
    
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var btnMarkAsLost: UIButton!
    @IBOutlet weak var lblOppNumbber: UILabel!
    @IBOutlet weak var lblOppTitle: UILabel!
    
    @IBOutlet var btnInitialSetup: UIButton!
    @IBOutlet var heightForHeader: NSLayoutConstraint!
    
    //MARK:- Variables
    var strSalesssUrlMainServiceAutomation = ""
    var arrOfTagests =  NSMutableArray()
    var arrOfScopes =  NSMutableArray()
    var arrayOfImages = NSArray ()
    @objc var strLeadId = NSString()
    @objc var strTitle = NSString()
    @objc var strOppNumber = NSString()
    var strLeadStatusGlobal = String()
    var dataGeneralInfo = NSManagedObject()
    @objc var matchesGeneralInfo = NSManagedObject()
    var strSelectedPropsedDate = ""
    var strSelectedPropsedMonth = ""
    var strAudioName = ""
    var strUserName: String?
    var isInspectionLoaded = false
    var arrayCoverLetter = NSMutableArray()
    private var arrayIntroduction = NSMutableArray()
    private var arrayMarketingContent = NSMutableArray()
    var coverLetterHtmlCode = ""
    var arrayOfMonth: [String] = []
    var graphPics = NSArray ()
    var beforePics = NSArray ()
    //MARK:- Dictionary
    var dictCoverLetter = NSDictionary()
    fileprivate var strIsPresetWO = ""
    let btnMarkAsLostAttributes = [
        NSAttributedString.Key.foregroundColor : UIColor.darkGray,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    var objWorkorderDetail = NSManagedObject()
    var attributedString = NSMutableAttributedString(string:"")
    var strMutableText = NSMutableString()
    var strMutableTextIntroductionLetter = NSMutableString()
    var strMarketingContent = NSMutableString()
    var arrSelectedAgreementChckList = NSMutableArray()
//    var arrMarketingContent: [String] = []
    fileprivate var strCompanyLogoPath = ""
    fileprivate var strBranchLogoImagePath = ""
    fileprivate var strAccountNo = ""
    fileprivate var strIsFormFilled = ""
    fileprivate var strStageSysName = ""
    fileprivate var strCustomerCompanyProfileImage = ""
    fileprivate var strLeadNumber = ""
    fileprivate var strAccountManagerName = ""
    fileprivate var strAccountManagerEmail = ""
    fileprivate var strAccountManagerPrimaryPhone = ""
    fileprivate var strCellNo = ""
    fileprivate var strBillingPocName = ""
    fileprivate var strCompanyAddress = ""
    
    //Services
    var arrStandardServiceReview  : [NSManagedObject] = []
    var arrCustomServiceReview    : [NSManagedObject] = []
    var dictPricingInfo: [String: Any]?
    var dictPricingInfoProposal: [String: Any] = [:]

    //Discount
    var arrAppliedCoupon : [NSManagedObject] = []
    var arrAppliedCredit : [NSManagedObject] = []
    
    @objc  var strHeaderTitle = NSString ()
    var strCustomerName = ""
    fileprivate var strEmpName = ""
    
    var strInspectorId = ""
    // class variable
    fileprivate let global = Global()
    // dictionary
    var dictLoginData = NSDictionary()
    var BillingAddressName = ""
    var CustomerCompanyName = ""
    var CellPhone = ""
    var PrimaryPhone = ""
    var strAgreementCheckList = ""
    var strAddtionalNotes = ""
    var strInternalNotes = ""
    var objPaymentInfo : NSManagedObject!
    var objLeadDetails : NSManagedObject?
    
    var heightForAddTargetImages : CGFloat?
    
    //From Login Details
    var isTaxCodeRequired = false
    var strTaxCodeSysName = ""
    var isIAgreeTAndC = false
    var isCustomerNotPresent = false
    var isEditInSalesAuto = false
    var isAuthFormFilled = false
    var strGlobalAudio = ""
    
    //Signature
    var customerSig: UIImage?
    var techSig: UIImage?
    var isCustomer = false
    var strCustomerSignature = ""
    var strSalesSignature = ""
    var isReloadOnce = false
    var isReloadOnceCustomer = false
    var strPresetSigFromDB = ""

    //Payment
    var selectecPaymentType = ""
    var strPaymentMode = ""
    var strAmount = ""
    var strChequeNo     = ""
    var strLicenseNo    = ""
    var strDate         = ""
    var arrTerms: [String] = []
    
    var isFrontImage = false
    var checkFrontImg: UIImage?
    var checkBackImg: UIImage?
    var arrOFImagesName: [String] = []
    var arrOfCheckBackImage: [String] = []
    
    var strCompanyKey = ""
    
    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""
    var strText = "false"
    var strPhone = "false"
    var strEmail = "false"
    var strIsAllowCustomer = "false"
    var isElementIntegration = false
    var chkFreqConfiguration = false
    var shouldShowElectronicFormLink = false
    
    var isImagePreview = false
    var isGraphPreview = false
    var isRefreshInspector = false
    var isPreferredMonths = false
    var isTermsOfService = false
    var isAllowToMakeSelection = false
    var isCoverLetter = false
    var isIntroductionLetter = false
    var isMarketingContent = false
    var isTermsOfServiceContent = false

    // sourabh  ~~~~~~~~~~ start
    
    var strTermsCondition  = ""

    var arrayTermsOfService = NSMutableArray()
    var dictTermsOfService = NSDictionary()
    
     var arrayTermsAndConditions =  NSMutableArray()
     var arrayTermsAndConditionsMultipleSelected = NSMutableArray()
     var strLblTermsCondition = ""
     var reloadCoverLetterOnce = true
     var reloadMarketingOnce = true
     var reloadIntroductionOnce = true

    // ~~~~~~~~~~ end //
    
    // agreement valid for sourabh 7 feb
     var strValidFor = ""
     var strIsAgreementValidForNotes = ""
    
    
    // changes by sourabh 11 feb
    var billingCellNo = ""
    var billingPrimaryPhone = ""
    var billingSecondaryPhone = ""
    var billingPrimaryEmail = ""
    var BusinessLicenceNo = ""
    // end 11 feb
    var dictSelectedCustomPaymentMode = NSDictionary()
        
    //MARK:- View Controller life cycle
    var isPreview = false
    var strStatusPreview = ""
    var strStagePreview = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(arrStandardServiceReview)
        print("arrCustomServiceReview")
        print(arrCustomServiceReview)
        print(dictPricingInfo)
        print(dictPricingInfoProposal)
        print(arrAppliedCoupon)
        print(arrAppliedCredit)
       
        intialSetup()
        self.basicFunction()
        self.setLeadDetails()
        self.setPaymentDetail()
        //intialSetup()
        delegate_dataSource_tableView()
        getLetterTemplateAndTermsOfService()
        fetchLetterTemplateFromLocalDB()
        fetchMarketingContentFromLocalDB()
        fetchMultiTermsAndConditionsFromLocalDB()
        
        getInspectorAndCustomerName()
    }
    
    func getInspectorAndCustomerName()  {
                
        var strEmpName = ""
        strEmpName = global.getEmployeeName(viaEmployeeID: "\(matchesGeneralInfo.value(forKey: "salesRepId") ?? "")")
        var strCustomerName = ""
        strCustomerName = "\(matchesGeneralInfo.value(forKey: "firstName") ?? "") \(matchesGeneralInfo.value(forKey: "middleName") ?? "") \(matchesGeneralInfo.value(forKey: "lastName") ?? "")"
        nsud.set(strEmpName, forKey: "inspectorName")
        nsud.set(strCustomerName, forKey: "customerName")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        basicFunction()
        
        let defaults = UserDefaults.standard
        let strIns: String = defaults.value(forKey: "fromInspectorSign") as? String ?? ""
        let strCust: String = defaults.value(forKey: "fromCustomerSign") as? String ?? ""
        

        if strIns == "fromInspectorSign" {
            self.isEditInSalesAuto = true
            self.strSalesSignature = defaults.value(forKey: "imagePath") as? String ?? ""
            let imagePath = self.getImage(strImageName: self.strSalesSignature)
            if imagePath != "" {
                self.techSig = UIImage(contentsOfFile: imagePath)
            }
            
            defaults.setValue("abc", forKey: "fromInspectorSign")
            defaults.setValue("abc", forKey: "imagePath")
            defaults.synchronize()
            
        }
        if strCust == "fromCustomerSign" {
            self.isEditInSalesAuto = true
            self.strCustomerSignature = defaults.value(forKey: "imagePath") as? String ?? ""
            let imagePath = self.getImage(strImageName: self.strCustomerSignature)
            if imagePath != "" {
                self.customerSig = UIImage(contentsOfFile: imagePath)
            }
            
            defaults.setValue("xyz", forKey: "fromCustomerSign")
            defaults.setValue("xyz", forKey: "imagePath")
            defaults.synchronize()
        }
        
        
        let yesAudioAvailable = defaults.bool(forKey: "yesAudio")
        if yesAudioAvailable {
            self.isEditInSalesAuto = true
            self.strGlobalAudio = defaults.string(forKey: "AudioNameService") ?? ""
        }
        
        
        self.reviewTableView.reloadData()
        
        /*
        if getOpportunityStatus()
        {
            
            self.btnSendProposal.isHidden = true
            self.btnSendProposal.isUserInteractionEnabled = false
            self.btnMarkAsLost.isUserInteractionEnabled = false
        }
        else
        {
            if isSendProposal {
                self.btnSendProposal.isHidden = true
            }else {
                self.btnSendProposal.isHidden = false
            }
            self.btnSendProposal.isUserInteractionEnabled = true
            self.btnMarkAsLost.isUserInteractionEnabled = true
        }
        manageInitialSetup()*/
        
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
        else
        {
            self.btnMarkAsLost.isUserInteractionEnabled = true
            self.btnMarkAsLost.isHidden = false
        }
        
        handleCheckImage()
    }
    
    func handleCheckImage() {
        
        if nsud.bool(forKey: "frontImageDeleted")
        {
            arrOFImagesName = []
            nsud.setValue(false, forKey: "frontImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkFrontImagePath")
        }
        if nsud.bool(forKey: "backImageDeleted")
        {
            arrOfCheckBackImage = []
            nsud.setValue(false, forKey: "backImageDeleted")
            nsud.synchronize()
            self.objPaymentInfo?.setValue("", forKey: "checkBackImagePath")
        }
        
        if nsud.bool(forKey: "yesEditImage")
        {
            nsud.setValue(false, forKey: "yesEditImage")
            nsud.synchronize()
            
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Service_iPhone" : "Service_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditImageViewController") as! EditImageViewController
            
            self.navigationController?.present(vc, animated: true, completion: nil)
        }


        if nsud.bool(forKey: "frontImageDeleted") || nsud.bool(forKey: "backImageDeleted") {
            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getLeadDetails()
        graphImge()
        imagePreviewBefore()
        reviewTableView.reloadData()
    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func getLostStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Lost") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    // Functions
    func graphImge(){
        let titleHeader = "Graph"
        graphPics = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strLeadId, titleHeader))
    }
    
    func imagePreviewBefore(){
        let titleHeader = "Before"
        beforePics = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@", strLeadId, titleHeader))
    }
    
    func intialSetup(){
        
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: strLeadId as String)
        strAudioName = nsud.value(forKey: "AudioNameService") as! String
        
        matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName!)
        
        BillingAddressName = Global().strCombinedAddressService(for: matchesGeneralInfo)
        CustomerCompanyName = matchesGeneralInfo.value(forKey: "companyName") as? String ?? ""
        CellPhone = matchesGeneralInfo.value(forKey: "billingCellNo") as? String ?? ""
        PrimaryPhone = matchesGeneralInfo.value(forKey: "servicePrimaryPhone") as? String ?? ""
        
        strCompanyLogoPath = "\(dictLoginData.value(forKeyPath: "Company.LogoImagePath") ?? "")"
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        print("dictLoginData")
        print(dictLoginData)
        
       
        
        strCompanyLogoPath = "\(dictLoginData.value(forKeyPath: "Company.LogoImagePath") ?? "")"
        strBranchLogoImagePath = "\(dictLoginData.value(forKeyPath: "EmployeeBranchLogoImagePath") ?? "")"
        
        isPreferredMonths = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPreferredMonth") as! Bool
        isTermsOfService = dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsTermsOfService") as! Bool
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        let strCompanyAdd1 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine1") ?? "")"
        
        let strCompanyAdd2 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine2") ?? "")"
        
        let strCompanyCountry = "\(dictLoginData.value(forKeyPath: "Company.CountryName") ?? "")"
        
        let strCompnayCity = "\(dictLoginData.value(forKeyPath: "Company.CityName") ?? "")"
        
        let strCompanyState = "\(dictLoginData.value(forKeyPath: "Company.StateName") ?? "")"
        
        let strCompnayZipCode = "\(dictLoginData.value(forKeyPath: "Company.ZipCode") ?? "")"
        
        self.billingCellNo = matchesGeneralInfo.value(forKey: "billingCellNo") as? String ?? ""
        
        self.billingPrimaryPhone = matchesGeneralInfo.value(forKey: "billingPrimaryPhone") as? String ?? ""
        
        self.billingSecondaryPhone = matchesGeneralInfo.value(forKey: "billingSecondaryPhone") as? String ?? ""
        
        self.billingPrimaryEmail = matchesGeneralInfo.value(forKey: "billingPrimaryEmail") as? String ?? ""
        
        let BusinessLicenceNo = "\(dictLoginData.value(forKeyPath: "Company.BusinessLicenceNo") ?? "")"
        
        self.BusinessLicenceNo = BusinessLicenceNo
        
        if(strCompanyAdd1.count > 0)
        {
            strCompanyAddress = strCompanyAdd1
        }
        if(strCompanyAdd2.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyAdd2 : strCompanyAdd2
        }
        if(strCompanyCountry.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyCountry : strCompanyCountry
        }
        
        if(strCompnayCity.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayCity : strCompnayCity
        }
        
        if(strCompanyState.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyState : strCompanyState
        }
        
        if(strCompnayZipCode.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayZipCode : strCompnayZipCode
        }
        
    }
    
    
    // MARK: Cover Letter Content WebView , Introdution Letter Content webView
    
    func getLetterTemplateAndTermsOfService()
    {
        let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        let arrayLetterTemplateMaster = (dictMaster.value(forKey: "LetterTemplateMaster") as! NSArray).mutableCopy() as! NSMutableArray
        let arraySalesMarketingContentMaster = (dictMaster.value(forKey: "SalesMarketingContentMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        arrayTermsOfService = (dictMaster.value(forKey: "TermsOfServiceMaster") as! NSArray).mutableCopy() as! NSMutableArray
        
        // cover letter and Introduction
        for item in arrayLetterTemplateMaster
        {
            if("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "CoverLetter")
            {
                arrayCoverLetter.add(item as! NSDictionary)
            }else if ("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "Introduction")
            {
                arrayIntroduction.add(item as! NSDictionary)
            }
            
        }
        
        // Sales Marketing Content
        for item in arraySalesMarketingContentMaster
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
                else
                {
                    arrayMarketingContent.add(item as! NSDictionary)
                }
            }
        }
        
        // Terms & Conditions
        
        let arrayMultipleGeneralTermsConditions = (dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray).mutableCopy() as! NSMutableArray
        
        for item in arrayMultipleGeneralTermsConditions
        {
            if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
            {
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                {
                    arrayTermsAndConditions.add(item as! NSDictionary)
                    
                    
                }
            }
            
            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true && (item as! NSDictionary).value(forKey: "IsDefault") as! Bool == true)
            {
                arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                
                self.strLblTermsCondition = (item as! NSDictionary).value(forKey: "TermsTitle") as? String ?? ""
            }
        }
    }
    
    //MARK: - Login Detail
    func basicFunction() {
        let dictLoginData: [String : Any] = nsud.value(forKey: "LoginDetails") as! [String : Any]
        print(dictLoginData)
        
        self.isTaxCodeRequired = ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["TaxcodeRequired"] as? Bool ?? false
        self.strCompanyKey = (dictLoginData["Company"]
                                as? [String: Any])?["CompanyKey"]
            as? String ?? ""
        self.strUserName = (dictLoginData["Company"]
                                as? [String: Any])?["Username"]
            as? String ?? ""

        
        
        self.strSalesssUrlMainServiceAutomation = (((dictLoginData["Company"]
                                                        as? [String: Any])?["CompanyConfig"]
                                                        as? [String: Any])?["SalesAutoModule"]
            as? [String: Any])?["ServiceUrl"]
                                as? String ?? ""
        
        self.isElementIntegration = ((dictLoginData["Company"]
                                        as? [String: Any])?["CompanyConfig"]
                                        as? [String: Any])?["IsElementIntegration"] as? Bool ?? false

        chkFreqConfiguration =   ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["MaintPriceFreq_1"] as? Bool ?? false

        shouldShowElectronicFormLink =   ((dictLoginData["Company"]
                                    as? [String: Any])?["CompanyConfig"]
                                    as? [String: Any])?["IsElectronicAuthorizationForm"] as? Bool ?? false
        
        strLeadStatusGlobal = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        
    }

    //MARK: - Payment Details Value
    func setPaymentDetail() {
        print(objPaymentInfo)
        self.strPaymentMode = "\(self.objPaymentInfo.value(forKey: "paymentMode") ?? "")"
        
        print(self.selectecPaymentType)
        if self.strPaymentMode == "Cash" {
            self.selectecPaymentType = "Cash"
        }else if self.strPaymentMode == "Check" {
            self.selectecPaymentType = "Check"
        }else if self.strPaymentMode == "CreditCard" {
            self.selectecPaymentType = "Credit Card"
        }else if self.strPaymentMode == "AutoChargeCustomer" {
            self.selectecPaymentType = "Auto Charge Customer"
        }else if self.strPaymentMode == "CollectattimeofScheduling" {
            self.selectecPaymentType = "Collect at time of Scheduling"
        }else if self.strPaymentMode == "Invoice" {
            self.selectecPaymentType = "Invoice"
        }else {
            
            //Fetch Payment Mode From Master
            var paymentModeFound = false
            
            let arrPaymentMode = getPaymentModeFromMaster()
            
            for item in arrPaymentMode
            {
                let dict = item as! NSDictionary
                
                if dict["AddLeadProspect"] == nil
                {
                    if "\(dict.value(forKey: "SysName") ?? "")" == strPaymentMode
                    {
                        self.selectecPaymentType = "\(dict.value(forKey: "Title") ?? "")"
                        paymentModeFound = true
                        dictSelectedCustomPaymentMode = dict
                        break
                    }
                }
                    
            }
            if !paymentModeFound
            {
                self.selectecPaymentType = ""
            }
        }
        
        self.self.strAddtionalNotes = "\(objPaymentInfo?.value(forKey: "specialInstructions")! ?? "")"

        self.strCustomerSignature = "\(self.objPaymentInfo.value(forKey: "customerSignature") ?? "")"
        self.strSalesSignature = "\(self.objPaymentInfo.value(forKey: "salesSignature") ?? "")"
        
        let checkFrontImagePath = "\(self.objPaymentInfo.value(forKey: "checkFrontImagePath") ?? "")"
        if (checkFrontImagePath.count > 0)
        {
            arrOFImagesName.append(checkFrontImagePath)
        }

        let checkBackImagePath = "\(self.objPaymentInfo.value(forKey: "checkBackImagePath") ?? "")"
        if (checkBackImagePath.count > 0)
        {
            arrOfCheckBackImage.append(checkBackImagePath)
        }
        
        self.strChequeNo = "\(objPaymentInfo.value(forKey: "checkNo") ?? "")"
        self.strLicenseNo = "\(objPaymentInfo.value(forKey: "licenseNo") ?? "")"
       // self.strDate = "\(self.objPaymentInfo.value(forKey: "expirationDate") ?? "")"
        
        if "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")".count > 0
        {
            let str = changeStringDateToGivenFormat(strDate: "\(objPaymentInfo.value(forKey: "expirationDate") ?? "")", strRequiredFormat: "MM/dd/yyyy")
            self.strDate = str
            //cell.btnExpDate.setTitle("Expiration Date: \(str)", for: .normal)
        }

    }
    
    //MARK: - Lead Details Value
    func setLeadDetails() {
        print(matchesGeneralInfo)
        
        self.strLeadNumber = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        self.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.strTaxCodeSysName = "\(matchesGeneralInfo.value(forKey: "taxSysName")!)"
        self.strAccountManagerName = "\(matchesGeneralInfo.value(forKey: "accountManagerName") ?? "")"
        self.strAccountManagerEmail = "\(matchesGeneralInfo.value(forKey: "accountManagerEmail") ?? "")"
        self.strAccountManagerPrimaryPhone = "\(matchesGeneralInfo.value(forKey: "accountManagerPrimaryPhone") ?? "")"
        self.strCustomerName = "\(global.strFullName(fromCoreDB: matchesGeneralInfo) ?? "")"//"\(matchesGeneralInfo.value(forKey: "customerName") ?? "")"
        self.strCellNo = "\(matchesGeneralInfo.value(forKey: "cellNo") ?? "")"
        self.strCustomerCompanyProfileImage = "\(matchesGeneralInfo.value(forKey: "profileImage") ?? "")"
        self.strInspectorId = "\(matchesGeneralInfo.value(forKey: "salesRepId") ?? "")"
        self.strPresetSigFromDB = "\(matchesGeneralInfo.value(forKey: "isEmployeePresetSignature") ?? "")"

        let strIsAuthFormFilled = "\(matchesGeneralInfo.value(forKey: "accountElectronicAuthorizationFormSigned") ?? "")"
        if strIsAuthFormFilled == "true" || strIsAuthFormFilled == "1" {
            isAuthFormFilled = true
        }else {
            isAuthFormFilled = false
        }
        
        let strIsCusto = "\(matchesGeneralInfo.value(forKey: "isCustomerNotPresent") ?? "")"
        if strIsCusto == "true" || strIsCusto == "1" {
            self.isCustomerNotPresent = true
        }else {
            self.isCustomerNotPresent = false
        }
        
        let strIsAllowCusto = "\(matchesGeneralInfo.value(forKey: "isSelectionAllowedForCustomer") ?? "false")"
        if strIsAllowCusto == "true" || strIsCusto == "1" {
            self.strIsAllowCustomer  = "true"
        }else {
            self.strIsAllowCustomer  = "false"
        }
        print(self.strIsAllowCustomer)

        self.strText = "\(matchesGeneralInfo.value(forKey: "smsReminders") ?? "false")"
        self.strPhone = "\(matchesGeneralInfo.value(forKey: "phoneReminders") ?? "false")"
        self.strEmail = "\(matchesGeneralInfo.value(forKey: "emailReminders") ?? "false")"

        if self.strText == "" || self.strText == "false" || self.strText == "0" {
            self.strText = "false"
        }else {
            self.strText = "true"
        }
        if self.strPhone == "" || self.strPhone == "false" || self.strPhone == "0" {
            self.strPhone = "false"
        }else {
            self.strPhone = "true"
        }
        if self.strEmail == "" || self.strEmail == "false" || self.strEmail == "0" {
            self.strEmail = "false"
        }else {
            self.strEmail = "true"
        }
        self.strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        self.strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        

        let strBillingPocFirstname = "\(matchesGeneralInfo.value(forKey: "billingFirstName") ?? "")"
        let strBillingPocMiddleName = "\(matchesGeneralInfo.value(forKey: "billingMiddleName") ?? "")"
        let strBillingPocLastName = "\(matchesGeneralInfo.value(forKey: "billingLastName") ?? "")"
        
        if(strBillingPocFirstname.count>0)
        {
            strBillingPocName = strBillingPocFirstname
        }
        
        if(strBillingPocMiddleName.count>0)
        {
            strBillingPocName = strBillingPocName.count > 0 ? strBillingPocName + "," + strBillingPocMiddleName:strBillingPocMiddleName
        }
        
        if(strBillingPocLastName.count>0)
        {
            strBillingPocName = strBillingPocName.count > 0 ? strBillingPocName + "," + strBillingPocLastName:strBillingPocLastName
        }
        
        //Terms & Condition
        arrTerms.append(matchesGeneralInfo.value(forKey: "leadGeneralTermsConditions") as? String ?? "")
        if arrTerms[0] == "" {
            arrTerms.removeAll()
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                //Getting Credit and Coupon Detail
                if(dictMaster.value(forKey: "MultipleGeneralTermsConditions") is NSArray) {
                    let arrTermsTempNew: NSArray = dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray
                    var tempMatch = false
                    
                    if arrTermsTempNew.count > 0 {
                        var dictGneralTerms: [String: Any]?
                        for dict in arrTermsTempNew {
                            print(dict)
                            dictGneralTerms = dict as? [String : Any]
                            if "\(dictGneralTerms!["IsDefault"] ?? "0")" == "1"  && "\(dictGneralTerms!["IsActive"] ?? "0")" == "1" {
                                tempMatch = true
                                break
                            }
                        }
                        
                        if tempMatch {
                            arrTerms.append(dictGneralTerms!["TermsConditions"] as! String)
                        }else {
                            arrTerms.append("")
                        }
                        
                    }else {
                        arrTerms.append("")
                    }
                }
            }
            
        }
        print(arrTerms)
        
        
        let audioPath = "\(matchesGeneralInfo.value(forKey: "audioFilePath") ?? "")"
        if (audioPath.count>0) {
            print(audioPath)
            strGlobalAudio = audioPath;
            self.downloadingAudio(strPath: audioPath)
        }else {
            strGlobalAudio = ""
        }
        
        let myFloat = ("\(matchesGeneralInfo.value(forKey: "collectedAmount") ?? "")" as NSString).floatValue
        if myFloat > 0
        {
            if getOpportunityStatus()
            {
                strAmount = String(format: "%.2f", myFloat)
            }
            else
            {
                strAmount = (self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
            }
            
            //strAmount = String(format: "%.2f", myFloat)
        }
        else
        {
            strAmount = (self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
        }
        
        //Nilind
        
         manageAllowCustomerToSelection()

        //End

    }
        
    func manageAllowCustomerToSelection()  {
        
        isAllowToMakeSelection = dictLoginData.value(forKeyPath: "Company.CompanyConfig.DefaultIsAllowCustomertToMakeSelection") as! Bool
        
        if isCustomerNotPresent == true
        {
            
            if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")" == "Complete" && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")" == "CompletePending"
            {
                let strIsAllowCusto = "\(matchesGeneralInfo.value(forKey: "isSelectionAllowedForCustomer") ?? "false")"
                if strIsAllowCusto == "true" || strIsAllowCusto == "1" {
                    self.strIsAllowCustomer  = "true"
                }else {
                    self.strIsAllowCustomer  = "false"
                }
            }
            else
            {
                if isAllowToMakeSelection
                {
                    self.strIsAllowCustomer  = "true"
                }
                else
                {
                    self.strIsAllowCustomer  = "false"
                }
            }
        }
        else
        {
            self.strIsAllowCustomer  = "false"
        }
    }
    
    func fetchLetterTemplateFromLocalDB()
    {
        let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if(arrayLetterTemplate.count > 0)
        {
            let match = arrayLetterTemplate.firstObject as! NSManagedObject
            
            // cover letter
            if("\(match.value(forKey: "coverLetterSysName") ?? "")".count > 0)
            {
                for item in arrayCoverLetter
                {
                    var strCoverLetterContent = ""
                    
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "coverLetterSysName")!)")
                    {
                        strCoverLetterContent = "\((item as! NSDictionary).value(forKey: "TemplateContent")!)"
                        var strText = NSMutableString()
                        strText = NSMutableString(string: "\((item as! NSDictionary).value(forKey: "TemplateContent")!)")
                        strMutableText = strText
                        
                        print("00000")
                        print(strMutableText)
                        
                        strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
                        
                        for i in 0 ..< 4
                        {   print(i)
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AdditionalNotes]", with: self.strAddtionalNotes))
                           let seprator = arrayOfMonth.joined(separator: ",")
                           strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PreferredMonths]", with: seprator))
                           strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AgreementCheckList]", with: self.strAgreementCheckList))
                            
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath as String))
                            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage as String))
                            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate() as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddress]", with: BillingAddressName))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[ServiceAddressName]", with: BillingAddressName))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddress]", with: BillingAddressName))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingAddressName]", with: BillingAddressName))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountNo]", with: strAccountNo as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber as String))
                            
                            if(dictInspector.count > 0)
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String))
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String))
                            }
                            else
                            {
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "" as String))
                                strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[InspectorEmail]", with: "" as String))
                            }
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Technician]", with: strEmpName as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellPhone]", with: CellPhone))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CellNumber]", with: strCellNo as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[Licenseno]", with: "" as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[PrimaryPhone]", with: PrimaryPhone)) // phone
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName as String))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOCPrimaryPhone]", with: self.billingPrimaryPhone))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOCSecondaryPhone]", with: self.billingSecondaryPhone))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOCCell]", with: self.billingCellNo))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[BillingPOCEmailID]", with: self.billingPrimaryEmail))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[CompanyBusinessLicenseNo]", with: self.BusinessLicenceNo))
                            strMutableText = NSMutableString(string: strMutableText.replacingOccurrences(of: "[AccountManagerEmai]", with: strAccountManagerEmail))
                            
                            
                        }
                    }
                }
            }
            else
            {
            }
            
            // Introduction letter
            
            if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
            {
                
                for itemNew in arrayIntroduction
                {
                    if("\((itemNew as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                    {
                        var strText = NSMutableString()
                        let dict = itemNew as! NSDictionary
                        let strHeader = "\(dict.value(forKey: "TemplateHeader") ?? "")" + "\n"
                        let strFooter = "\n" + "\(dict.value(forKey: "TemplateFooter") ?? "")"
                        
                        //strText = NSMutableString(string: "\((itemNew as! NSDictionary).value(forKey: "TemplateContent")!)")
                        

                        
                        //strText = NSMutableString(string: "\((itemNew as! NSDictionary).value(forKey: "TemplateHeader") ?? "") \((itemNew as! NSDictionary).value(forKey: "TemplateContent")!) \((itemNew as! NSDictionary).value(forKey: "TemplateFooter") ?? "")")

                        // Change By Saavan
                        //strText = NSMutableString(string: "\(match.value(forKey: "introContent")!)")
                        strText = NSMutableString(string: "\(strHeader)\(match.value(forKey: "introContent") ?? "")\(strFooter)")

                        strMutableTextIntroductionLetter = strText
                        print("strMutableTextIntroductionLetter")
                        print(strMutableTextIntroductionLetter)
                        
                        strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
                        for i in 0 ..< 4 {
                            
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName))
                            
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[ServiceAddress]", with: BillingAddressName))
                            
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AdditionalNotes]", with: self.strAddtionalNotes))
                            let seprator = arrayOfMonth.joined(separator: ",")
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[PreferredMonths]", with: seprator))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AgreementCheckList]", with: self.strAgreementCheckList))
                            
                            
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath as String))
                            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage as String))
                            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate() as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[ServiceAddress]", with: BillingAddressName))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate() as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[ServiceAddressName]", with: BillingAddressName))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingAddress]", with: BillingAddressName))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingAddressName]", with: BillingAddressName))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AccountNo]", with: strAccountNo as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber as String))
                            
                            if(dictInspector.count > 0)
                            {
                                strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String))
                                strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String))
                            }
                            else
                            {
                                strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "" as String))
                                strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[InspectorEmail]", with: "" as String))
                            }
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[Technician]", with: strEmpName as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CellPhone]", with: CellPhone))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CellNumber]", with: strCellNo as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[Licenseno]", with: "" as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[PrimaryPhone]", with: PrimaryPhone)) // phone
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress as String))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName as String))
                            
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingPOCPrimaryPhone]", with: self.billingPrimaryPhone))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingPOCSecondaryPhone]", with: self.billingSecondaryPhone))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingPOCCell]", with: self.billingCellNo))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[BillingPOCEmailID]", with: self.billingPrimaryEmail))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CompanyBusinessLicenseNo]", with: self.BusinessLicenceNo))
                            strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[AccountManagerEmai]", with: strAccountManagerEmail))
                            
                            
                        }
                    }
                }
            }
            else
            {
                
            }
            
            // Terms Of Service
            if("\(match.value(forKey: "termsOfServiceSysName") ?? "")".count > 0)
            {
                for item in arrayTermsOfService
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "termsOfServiceSysName")!)")
                    {
                        dictTermsOfService = item as! NSDictionary
                        strTermsCondition = "\(match.value(forKey: "termsOfServiceContent") ?? "")"
                        
                    }
                }
            }
            else
            {
                
            }
            
            // agreement valid for days
            
            self.strIsAgreementValidForNotes = "\(match.value(forKey: "isAgreementValidFor") ?? "")"
            self.strValidFor = "\(match.value(forKey: "validFor") ?? "")"
              
        }
    }
    
    
    //MARK: - Download Audio
    func downloadingAudio(strPath: String) {
        print(self.strSalesssUrlMainServiceAutomation)
        
        var result = ""
        let equalRange = NSString(string: strPath).range(of: "/", options: .backwards)

        if equalRange.location != NSNotFound {
            let fromR = equalRange.location + equalRange.length
            result = strPath.substring(from: fromR)
        }else {
            result = strPath
        }
        print(result)
        if result.count == 0 {
            let range = NSString(string: strPath).range(of: "\\", options: .backwards)

            if range.location != NSNotFound {
                let fromR = range.location + range.length
                result = strPath.substring(from: fromR)
            }
        }
        
        let strUrl = "\(self.strSalesssUrlMainServiceAutomation)/documents/\(strPath)"
        
        print(strUrl)
        let path = (self.getDirectoryPath() as NSString).appendingPathComponent(result)

        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path){
            let defaults = UserDefaults.standard
            defaults.setValue(result, forKey: "AudioNameService")
            defaults.synchronize()
        }else{
            let strNewString = strUrl.replacingOccurrences(of: "\\", with: "/")
            let photoURL = URL(string: strNewString)
            var audioData: Data? = nil
            if let photoURL = photoURL {
                
                do {
                    audioData = try Data(contentsOf: photoURL)
                   } catch {
                       print(error)
                   }
            }
            self.saveAudio(afterDownload: audioData, result)
            
            let defaults = UserDefaults.standard
            defaults.setValue(result, forKey: "AudioNameService")
            defaults.set(true, forKey: "yesAudio")
            defaults.synchronize()
        }
    }
    
    func saveAudio(afterDownload audioData: Data?, _ name: String?) {
        var name = name

        name = global.strNameBackSlashIssue(name)

        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        let documentsDirectory = paths[0]
        let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(name ?? "")").path
        if let audioData = audioData {
            NSData(data: audioData).write(toFile: path, atomically: true)
        }
    }
    
    // MARK: Marketing Content WebView
    fileprivate func fetchMarketingContentFromLocalDB()
    {
        let arrayMatches = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
//        arrMarketingContent.removeAll()
        
        if(arrayMatches.count > 0)
        {
            
            for match in arrayMatches
            {
                for item in arrayMarketingContent
                {
                    if("\((match as! NSManagedObject).value(forKey: "contentSysName")!)" == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                    {
                        strMarketingContent.append("\((item as! NSDictionary).value(forKey: "Description")!)")
//                        arrMarketingContent.append("\((item as! NSDictionary).value(forKey: "Description")!)")
                    }
                }
            }
        }
    }
    
    // MARK: ----------------------------  Additonall Notes   ----------------------------
    func fetchPaymentInfo() {
        
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        //objPaymentInfo
        
        if(arrPaymentInfo.count > 0)
        {
            
             objPaymentInfo = (arrPaymentInfo.firstObject as! NSManagedObject)
               
             self.strAddtionalNotes = "\(objPaymentInfo!.value(forKey: "specialInstructions") ?? "")"
           
            
        }else {
            
        }
    }
    
    // MARK: ----------------------------  Internal Notes   ----------------------------
    func getLeadDetails() {
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        if(arrayLeadDetail.count > 0)
        {
            objLeadDetails = (arrayLeadDetail.firstObject as! NSManagedObject)
            strInternalNotes = "\(objLeadDetails!.value(forKey: "notes")!)"
        }
        reviewTableView.reloadData()
    }
    
    // MARK:- delegate and data source
    
    func delegate_dataSource_tableView(){
        //
        let strleadNumber =  dataGeneralInfo.value(forKey: "leadNumber") ?? ""
        let straccountNo =  dataGeneralInfo.value(forKey: "accountNo") ?? ""
        
        lblOppNumbber.text = Global().strFullName(fromCoreDB: dataGeneralInfo)
        lblOppTitle.text = "Opp# \(strleadNumber)" + " " + "Acc# \(straccountNo)"
        
        self.reviewTableView.delegate = self
        self.reviewTableView.dataSource = self
        reviewTableView.tableFooterView = UIView()
        
        var arrTemp: [String] = []
        for obj in arrSelectedAgreementChckList {
            let dict = obj as? NSDictionary
            let title = dict?.value(forKey: "Title") as? String
            strAgreementCheckList = strAgreementCheckList.appending(title ?? "")
            arrTemp.append(title ?? "")
        }
        self.strAgreementCheckList = arrTemp.joined(separator: ", ")
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getImage(strImageName: String) -> String {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(strImageName)
        if fileManager.fileExists(atPath: imagePAth){
            return imagePAth
        }else{
            return ""
        }
    }
    
    //MARK - Footer func
    
    func goToGlobalmage(strType: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "PestiPhone", bundle:nil)
        let objGlobalImageVC = storyBoard.instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        
        objGlobalImageVC.strHeaderTitle = strType as NSString;
        objGlobalImageVC.strWoId = strLeadId
        objGlobalImageVC.strWoLeadId = strLeadId
        objGlobalImageVC.strModuleType = "SalesFlow"
        
        if strLeadStatusGlobal.lowercased() == "Complete" || strLeadStatusGlobal.lowercased() == "Completed"{
            objGlobalImageVC.strWoStatus = "Complete"
        }
        else{
            objGlobalImageVC.strWoStatus = "InComplete"
        }
        
        self.navigationController?.present(objGlobalImageVC, animated: false, completion: nil)
    }
    
    func goToUploadDocument(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "SalesMainiPad", bundle:nil)
        let objUploadDocumentSales = storyBoard.instantiateViewController(withIdentifier: "UploadDocumentSalesiPad") as! UploadDocumentSales
        objUploadDocumentSales.strWoId = strLeadId
        self.navigationController?.present(objUploadDocumentSales, animated: false, completion: nil)
        
    }
    
    //MARK:- Action
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddImageAction(_ sender: Any)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Add Images", style: .default , handler:{ (UIAlertAction)in
            self.goToGlobalmage(strType: "Before")
        }))
        
        alert.addAction(UIAlertAction(title: "Add Documents", style: .default , handler:{ (UIAlertAction)in
            self.goToUploadDocument()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceView = self.btnAddImage
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    @IBAction func btnAddGraphAction(_ sender: Any)
    {
        goToGlobalmage(strType: "Graph")
    }
    
    
    @IBAction func action_ChemicalSensitive(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
//        let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
//        let objChemicalSen = storyboardIpad.instantiateViewController(withIdentifier: "ChemicalSensitivityList") as? ChemicalSensitivityList
//        objChemicalSen?.strFrom = "sales"
//        objChemicalSen?.strId = strLeadId as String
//        self.navigationController?.present(objChemicalSen!, animated: false, completion: nil)
        
    }
    
    @IBAction func btnCalendar(_ sender: Any) {
        self.goToAppointment()
        
    }
    
    @IBAction func btnHistory(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
            
        }))
        
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
            
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
            
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
            
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
        
    }
    
    
    @IBAction func btnActionOnCancle(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionOnContinue(_ sender: UIButton) {
        
        isPreview = false
        
        if(arrayTermsAndConditionsMultipleSelected.count > 0)
        {
            Global().updateSalesModifydate(strLeadId as String)
            
            //Review
            if self.isTaxCodeRequired == true {
                if self.strTaxCodeSysName == "" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required. Please go to  General Info screen and select Taxcode under Service Address section.", viewcontrol: self)
                    
                }else {
                    self.finalSaveAfterTaxCodeCheck()
                    self.saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
                
                }
            }else {
                self.finalSaveAfterTaxCodeCheck()
                self.saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one Terms & Condition", viewcontrol: self)
        }
        
    }
    
    @IBAction func actionOnPreview(_ sender: Any) {
        
        isPreview = true
        
        self.updatePaymentInfoCoreData()
        self.updateLeadIdDetail()
        self.goToSendMailViewController()
        self.saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
        
//        if(arrayTermsAndConditionsMultipleSelected.count > 0)
//        {
//            Global().updateSalesModifydate(strLeadId as String)
//
//            //Review
//            if self.isTaxCodeRequired == true {
//                if self.strTaxCodeSysName == "" {
//                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required. Please go to  General Info screen and select Taxcode under Service Address section.", viewcontrol: self)
//
//                }else {
//                    self.finalSaveAfterTaxCodeCheck()
//                    self.saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
//
//                }
//            }else {
//                self.finalSaveAfterTaxCodeCheck()
//                self.saveTermsAndConditions(arraySeletecTermsAndConditions: arrayTermsAndConditionsMultipleSelected)
//            }
//        }
//        else
//        {
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select at least one Terms & Condition", viewcontrol: self)
//        }
        
    }
    
    @IBAction func btnActionOnMarkAsLost(_ sender: Any) {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure to mark Opportunity as lost?", preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
            
            self.updateLeadForLost()
            
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
            
            
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    func updateLeadForLost()
    {
        Global().updateSalesModifydate(strLeadId as String)
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        arrOfKeys = ["stageSysName",
                     "statusSysName",
        ]
        
        arrOfValues = ["Lost",
                       "Complete",
        ]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate:  NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            debugPrint("suc")
            
            goToAppointment()
            
        }
        
    }
    
    
    
    @IBAction func btnActionOnRecordAudio(_ sender: Any) {
        //yesEditedSomething = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .alert)
        
        
        if !getOpportunityStatus()
        {
            alert.addAction(UIAlertAction(title: "Record", style: .default , handler:{ (UIAlertAction)in
                let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "RecordAudioViewiPad") as? RecordAudioViewiPad
                testController?.strFromWhere = "SalesInvoice"//flowTypeWdoSalesService
                testController?.modalPresentationStyle = .fullScreen
                self.present(testController!, animated: false, completion: nil)
            }))
        }
        
        
        //        }
        let audioName = "\(nsud.value(forKey: "AudioNameService") ?? "")"
        
        if audioName.count > 0
            
        {
            
            alert.addAction(UIAlertAction(title: "Play", style: .default , handler:{ (UIAlertAction)in
                
                let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
                
                let testController = storyboardIpad.instantiateViewController(withIdentifier: "AudioPlayerVC") as? AudioPlayerVC
                
                testController!.strAudioName = audioName as! String
                
                testController?.modalPresentationStyle = .fullScreen
                
                self.present(testController!, animated: false, completion: nil)
                
            }))
            
        }
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in }))
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: { })
    }
    
    @IBAction func btnBackToSchedule(_ sender: UIButton) {
        
        goToAppointment()
    }
    
    @IBAction func btnIntialSetup(_ sender: UIButton) {
        if DeviceType.IS_IPAD {
            if btnInitialSetup.titleLabel?.text == "Initial Setup" {
                let defs = UserDefaults.standard
                defs.set(true, forKey: "fromAgreementInitialSetup")
                defs.setValue("forFollowUp", forKey: "FollowUp")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMainiPad", bundle: nil)
                let objInitialSetUp = storyBoard.instantiateViewController(withIdentifier: "InitialSetUpiPad") as? InitialSetUpiPad
                if let objInitialSetUp = objInitialSetUp {
                    navigationController?.pushViewController(objInitialSetUp, animated: false)
                }
                
            } else {
                //For Generate Workorder
                let defs = UserDefaults.standard
                defs.setValue("fromGenerateWorkorder", forKey: "fromGenerateWorkorder")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMainiPad", bundle: nil)
                let objGenerateWorkOrder = storyBoard.instantiateViewController(withIdentifier: "GenerateWorkOrderiPad") as? GenerateWorkOrderiPad
                if let objGenerateWorkOrder = objGenerateWorkOrder {
                    navigationController?.pushViewController(objGenerateWorkOrder, animated: false)
                }
            }
            
        }else {
            
            //iPhone
            if btnInitialSetup.titleLabel?.text == "Initial Setup" {
                let defs = UserDefaults.standard
                defs.set(true, forKey: "fromAgreementInitialSetup")
                defs.setValue("forFollowUp", forKey: "FollowUp")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMain", bundle: nil)
                let objInitialSetUp = storyBoard.instantiateViewController(withIdentifier: "InitialSetUp") as? InitialSetUp
                if let objInitialSetUp = objInitialSetUp {
                    navigationController?.pushViewController(objInitialSetUp, animated: false)
                }
                
            } else {
                
                //For Generate Workorder
                let defs = UserDefaults.standard
                defs.setValue("fromGenerateWorkorder", forKey: "fromGenerateWorkorder")
                defs.synchronize()
                
                let storyBoard = UIStoryboard(name: "SalesMain", bundle: nil)
                let objGenerateWorkOrder = storyBoard.instantiateViewController(withIdentifier: "GenerateWorkOrder") as? InitialSetUp
                if let objGenerateWorkOrder = objGenerateWorkOrder {
                    navigationController?.pushViewController(objGenerateWorkOrder, animated: false)
                }
            }
            
        }

        
    }
    
    
    func finalSaveAfterTaxCodeCheck() {
        if !isCustomerNotPresent {
            //present
            if self.isIAgreeTAndC {
                var isNoImage = true
                if self.customerSig != nil {
                    isNoImage = false
                }
                self.paymentConditionMethod(noImage: isNoImage)
            }else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly accept the Terms & Conditions", viewcontrol: self)
            }
            
        }else {
            //not present
            var isNoImage = false
            self.paymentConditionMethod(noImage: isNoImage)
        }
        
    }
    
    func amountMethod(noImage: Bool) {
        if noImage {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
        }else {
            
            if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
            }else {
                //chkChequeClick = NO;
                self.strLicenseNo = "";
                self.strChequeNo = "";
                self.strDate = "";
                
                /*Final Update*/
                self.updatePaymentInfoCoreData()
                
                
                if self.checkForCustomerSignature() == false {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                }else {
                    if self.isEditInSalesAuto == true {
                        global.updateSalesModifydate(self.strLeadId as String)
                    }
                    self.updateLeadIdDetail()
                    
                    /*Complition Alert*/
                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                    self.goToSendMailViewController()
                }
                
            }
        }
    }
    func noAmountMethod(noImage: Bool) {
        if noImage {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
        }else {
            
            //chkChequeClick = NO;
            self.strLicenseNo = "";
            self.strChequeNo = "";
            self.strDate = "";
            self.strAmount = "0";
            
            /*Final Update*/
            self.updatePaymentInfoCoreData()
            
            if self.checkForCustomerSignature() == false {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
                
            }else {
                if self.isEditInSalesAuto == true {
                    global.updateSalesModifydate(self.strLeadId as String)
                }
                self.updateLeadIdDetail()
                
                /*Complition Alert*/
                //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
                
                self.goToSendMailViewController()
            }
            
        }
    }
    
    func paymentConditionMethod(noImage: Bool) {
        
        if self.strPaymentMode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Payment Type", viewcontrol: self)
        }else if self.strPaymentMode == "Cash" || self.strPaymentMode == "CollectattimeofScheduling" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
            }else {
                
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else {
                    //chkChequeClick = NO;
                    self.strLicenseNo = "";
                    self.strChequeNo = "";
                    self.strDate = "";
                    
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId as String)
                        }
                        self.updateLeadIdDetail()
                        
                        /*Complition Alert*/
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                        self.goToSendMailViewController()
                    }
                    
                }
            }
        }else if self.strPaymentMode == "Check" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

            }else {
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else if self.strChequeNo == "" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Check #.", viewcontrol: self)
                }else {
                    print(self.strChequeNo)
                    print(self.strLicenseNo)
                    print(self.strAmount)
                    print(self.strDate)
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId as String)
                        }
                        self.updateLeadIdDetail()
                        
                        /*Complition Alert*/
                        //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                        self.goToSendMailViewController()
                    }
                }
            }
        }else if self.strPaymentMode == "CreditCard" {
            if noImage {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
            }else {
                
                if self.strAmount == "" || self.strAmount == "0" || self.strAmount == "0.0" || self.strAmount == "0.00" {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter Amount.", viewcontrol: self)
                }else {
                    //chkChequeClick = NO;
                    self.strLicenseNo = "";
                    self.strChequeNo = "";
                    self.strDate = "";
                    
                    /*Final Update*/
                    self.updatePaymentInfoCoreData()
                    
                    
                    if self.checkForCustomerSignature() == false {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)

                    }else {
                        if self.isEditInSalesAuto == true {
                            global.updateSalesModifydate(self.strLeadId as String)
                        }
                        self.updateLeadIdDetail()
                        
                        if isElementIntegration {
                            
                            if ReachabilityTest.isConnectedToNetwork() {
                                print("Internet connection available")
                                
                                self.goToCreditCardIntegration()
                            }
                            else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: ErrorInternetMsgPayment, viewcontrol: self)
                            }
                        }else {
                            /*Complition Alert*/
                            //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)

                            self.goToSendMailViewController()
                        }
                    }
                }
            }
            
        }else {
            
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                amountMethod(noImage: noImage)
            }
            else
            {
                noAmountMethod(noImage: noImage)
            }
            
            
//            if noImage {
//                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
//            }else {
//
//                //chkChequeClick = NO;
//                self.strLicenseNo = "";
//                self.strChequeNo = "";
//                self.strDate = "";
//                self.strAmount = "0";
//
//                /*Final Update*/
//                self.updatePaymentInfoCoreData()
//
//                if self.checkForCustomerSignature() == false {
//                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Kindly take the Customer Signature", viewcontrol: self)
//
//                }else {
//                    if self.isEditInSalesAuto == true {
//                        global.updateSalesModifydate(self.strLeadId as String)
//                    }
//                    self.updateLeadIdDetail()
//
//                    /*Complition Alert*/
//                    //showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Appointment completed", viewcontrol: self)
//
//                    self.goToSendMailViewController()
//                }
//
//            }
        }
    }
    
    //MARK: - Update Core Data
    func updatePaymentInfoCoreData() {
        
        print(objPaymentInfo)
//        self.objPaymentInfo!.setValue(self.strLeadId, forKey: "leadId")
        self.objPaymentInfo.setValue(self.strPaymentMode, forKey: "paymentMode")
        self.objPaymentInfo.setValue(self.strAmount, forKey: "amount")
        self.objPaymentInfo.setValue(self.strChequeNo, forKey: "checkNo")
        self.objPaymentInfo.setValue(self.strLicenseNo, forKey: "licenseNo")
        self.objPaymentInfo.setValue(self.strDate, forKey: "expirationDate")

        if self.isCustomerNotPresent {
            self.objPaymentInfo.setValue("", forKey: "customerSignature")
        }else {
            self.objPaymentInfo.setValue(self.strCustomerSignature, forKey: "customerSignature")
        }
        self.objPaymentInfo.setValue(self.strSalesSignature, forKey: "salesSignature")

        let defaults = UserDefaults.standard
        var strSignUrl: String = defaults.value(forKey: "ServiceTechSignPath") as? String ?? ""
        let isPreSetSign: Bool = defaults.bool(forKey: "isPreSetSignSales")
        
        matchesGeneralInfo.setValue("false", forKey: "isEmployeePresetSignature")
        if isPreSetSign && strSignUrl != "" && self.strSalesSignature.count == 0 {
            strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")
            
            var result = ""
            let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

            if equalRange.location != NSNotFound {
                print(equalRange.location)
                print(equalRange.length)
                let fromR = equalRange.location + equalRange.length
                result = strSignUrl.substring(from: fromR)
            }else {
                result = strSignUrl
            }
            print(result)
            self.objPaymentInfo.setValue(result, forKey: "salesSignature")
            matchesGeneralInfo.setValue("true", forKey: "isEmployeePresetSignature")
        }else {
            
            if self.strSalesSignature.count > 0 && self.strSalesSignature.contains("EmployeeSignatures") {
                
                strSignUrl = strSignUrl.replacingOccurrences(of: "\\", with: "/")

                var result = ""
                let equalRange = NSString(string: strSignUrl).range(of: "Documents", options: .backwards)

                if equalRange.location != NSNotFound {
                    print(equalRange.location)
                    print(equalRange.length)
                    let fromR = equalRange.location + equalRange.length
                    result = strSignUrl.substring(from: fromR)
                }else {
                    result = strSignUrl
                }
                self.objPaymentInfo.setValue(result, forKey: "salesSignature")
                
                print(self.strPresetSigFromDB)
                matchesGeneralInfo.setValue(self.strPresetSigFromDB, forKey: "isEmployeePresetSignature")
            }else {
                matchesGeneralInfo.setValue("false", forKey: "isEmployeePresetSignature")
            }
        }
        
        self.objPaymentInfo.setValue(global.modifyDate(), forKey: "modifiedDate")
        self.objPaymentInfo.setValue(self.strAddtionalNotes, forKey: "specialInstructions")
        self.objPaymentInfo.setValue(self.strCompanyKey, forKey: "companyKey")
        self.objPaymentInfo.setValue(self.strUserName, forKey: "userName")

        let defs = UserDefaults.standard
        let arrImageToSend: NSMutableArray = NSMutableArray()
        var arrTemp: NSMutableArray = NSMutableArray()
        
        if let val = defs.value(forKey: "arrImageToSend") {
            arrTemp = NSMutableArray(object: val)
        }
        for i in 0..<arrTemp.count {
            arrImageToSend.add(arrTemp[i])
        }
        
        
        if self.isCustomerNotPresent {
            
        }else {
            arrImageToSend.add(self.strCustomerSignature)
        }
        arrImageToSend.add(self.strSalesSignature)

        print(arrImageToSend)
        
        if self.strPaymentMode == "Check" {
            
            if arrOFImagesName.count > 0 {
                self.objPaymentInfo.setValue(self.arrOFImagesName[0], forKey: "checkFrontImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkFrontImagePath")
            }
            
            if arrOfCheckBackImage.count > 0 {
                self.objPaymentInfo.setValue(self.arrOfCheckBackImage[0], forKey: "checkBackImagePath")
            }else {
                self.objPaymentInfo.setValue("", forKey: "checkBackImagePath")
            }
            
        }
        defs.setValue(arrImageToSend, forKey: "arrImageToSend")
        defs.synchronize()
        
//        self.objPaymentInfo.setValue("", forKey: "leadPaymentDetailId")
//        self.objPaymentInfo.setValue("", forKey: "agreement")
//        self.objPaymentInfo.setValue("", forKey: "proposal")
//        self.objPaymentInfo.setValue("", forKey: "createdBy")
//        self.objPaymentInfo.setValue("", forKey: "createdDate")
//        self.objPaymentInfo.setValue("", forKey: "modifiedBy")
//        self.objPaymentInfo.setValue("", forKey: "inspection")
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.fetchPaymentInfo()
    }
    
    func checkForCustomerSignature() -> Bool {
        var chkCustomerSignPresent = false
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        if(arrPaymentInfo.count > 0)
        {
            for objPayment in arrPaymentInfo {
                let objPaymentIn: NSManagedObject = objPayment as! NSManagedObject
                print(objPaymentIn)
                
                let signCustomer: String = "\(objPaymentIn.value(forKey: "customerSignature") ?? "")"
                if signCustomer.count == 0 || signCustomer == "" {
                    chkCustomerSignPresent = false
                    
                    if isCustomerNotPresent == true {
                        chkCustomerSignPresent = true
                    }
                }else {
                    chkCustomerSignPresent = true
                }
            }
           
        }
        return chkCustomerSignPresent
    }
    
    //MARK: - Go To SendMailViewController
    func goToSendMailViewController() {
        print("GoTO Email")

        if isPreview
        {
            if(isInternetAvailable())
            {
                let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
                vc.strStatusForPreview = strStatusPreview
                vc.strStageForPreview = strStagePreview
                vc.isPreview = isPreview
                self.navigationController?.pushViewController(vc, animated: false)
                isPreview = false
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            }

        }
        else
        {
            
            let alert = UIAlertController(title: Alert, message: "Appointment completed", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default , handler:{ (UIAlertAction)in
                
                let vc = storyboardNewSalesSendMail.instantiateViewController(withIdentifier: "SalesNew_SendEmailVC") as! SalesNew_SendEmailVC
                vc.isPreview = false
                self.navigationController?.pushViewController(vc, animated: false)
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
        }
        
        
    }
    
    func goToCreditCardIntegration() {
        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objCreditCard = mainStoryboard.instantiateViewController(withIdentifier: "CreditCardIntegration") as? CreditCardIntegration
        
        var strValue = ""
        if self.isCustomerNotPresent {
            strValue = "no"
        }else {
            strValue = "yes"
        }
        objCreditCard!.strGlobalLeadId = self.strLeadId as String
        objCreditCard!.strAmount = self.strAmount
        objCreditCard!.strTypeOfService = "Lead"

        self.navigationController?.pushViewController(objCreditCard!, animated: false)
    }
    
    //MARK:  - Footer Funcs
    
    func updateLeadIdDetail()
    {
        if isPreview
        {
           updateLeadIdDetailForPreview()
        }
        else
        {
            let context = getContext()

    //        if isSendProposal {
    //            isCustomerNotPresent = false
    //        }
            
            if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card" {
                if !self.isElementIntegration {
                    matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                }
            }else {
                matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            }
            
            if self.strGlobalAudio.count>0 {
                matchesGeneralInfo.setValue(self.strGlobalAudio, forKey: "audioFilePath")
            }else {
                matchesGeneralInfo.setValue("", forKey: "audioFilePath")
            }
            
            matchesGeneralInfo.setValue("true", forKey: "iAgreeTerms")

            let str = matchesGeneralInfo.value(forKey: "isCustomerNotPresent")
            
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
            }else {
                matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
            }
            
            if self.customerSig == nil {
                matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
            }else {
                matchesGeneralInfo.setValue("true", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("true", forKey: "isAgreementGenerated")
            }
            
            
            
            matchesGeneralInfo.setValue("yes", forKey: "zSync")

            print(self.dictPricingInfo)
            var dictTemp = self.dictPricingInfo
    //        if isSendProposal {
    //            self.sendProposalCalculation()
    //            dictTemp = self.dictPricingInfoProposal
    //        }
            
            //let strFinalBilledAmount = strAmount//(dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strFinalBilledAmount = (dictTemp?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            var strFinalCollectedAmount = strAmount
            var strTotalPrice = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            let strCouponDis: String = (dictTemp?["couponDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
    //        let strOtherDis: String = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let finalDis: Double = (Double(strCouponDis) ?? 0.0) //+ (Double(strOtherDis) ?? 0.0)
            let strCouponDiscount = String(format: "%.2f", finalDis)

            let strFinalSubTotal = (dictTemp?["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strOtherDiscount = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTipDiscount = (dictTemp?["tipDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strLeadInspectionFee = (dictTemp?["leadInspectionFee"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            if strPaymentMode.caseInsensitiveCompare("Cash") == .orderedSame || strPaymentMode.caseInsensitiveCompare("Check") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CreditCard") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CollectattimeofScheduling") == .orderedSame {
                strFinalCollectedAmount = strAmount
            }else {
                if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
                {
                    strFinalCollectedAmount = strAmount
                }
                else
                {
                    strFinalCollectedAmount = "0"
                }
            }
            
            //        if !isSendProposal {
            matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "billedAmount")
            matchesGeneralInfo.setValue(strCouponDiscount, forKey: "couponDiscount")
            
            matchesGeneralInfo.setValue(strOtherDiscount, forKey: "otherDiscount")
            matchesGeneralInfo.setValue(strTipDiscount, forKey: "tipDiscount")
            matchesGeneralInfo.setValue(strLeadInspectionFee, forKey: "leadInspectionFee")
            //        matchesGeneralInfo.setValue("true", forKey: "isTipEligible")
            //        matchesGeneralInfo.setValue("true", forKey: "needTipService")
            
            matchesGeneralInfo.setValue(strFinalCollectedAmount, forKey: "collectedAmount")
            matchesGeneralInfo.setValue(strFinalSubTotal, forKey: "subTotalAmount")
                
                let strTaxAmt = (dictTemp?["taxAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTaxMaintAmt = (dictTemp?["taxAmountMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                
                let strTaxableInitialAmount = (dictTemp?["taxableAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTaxableMaintAmount = (dictTemp?["taxableMaintAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

                let strSubTotalMaintenance = (dictTemp?["subTotalMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                let strTotalPriceMaintenance = (dictTemp?["totalPriceMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

                matchesGeneralInfo.setValue(strTotalPrice, forKey: "totalPrice")
                matchesGeneralInfo.setValue(strTaxAmt, forKey: "taxAmount")
                matchesGeneralInfo.setValue(strTaxMaintAmt, forKey: "taxMaintAmount")
                
                matchesGeneralInfo.setValue(strTaxableInitialAmount, forKey: "taxableAmount")
                matchesGeneralInfo.setValue(strTaxableMaintAmount, forKey: "taxableMaintAmount")
                
                matchesGeneralInfo.setValue(strSubTotalMaintenance, forKey: "subTotalMaintAmount")
                //matchesGeneralInfo.setValue(strTotalPriceMaintenance, forKey: "totalMaintPrice")
                matchesGeneralInfo.setValue((dictTemp?["totalDueAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil), forKey: "totalMaintPrice")
    //        }
            
            matchesGeneralInfo.setValue("Won", forKey: "stageSysName")

        
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue("CompletePending", forKey: "stageSysName")
                if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card"{
                    if !self.isElementIntegration {
                        matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                    }
                }
            }else {
                matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            }
            
            matchesGeneralInfo.setValue("false", forKey: "isProposalFromMobile")

            
            
            let strMonths = arrayOfMonth.joined(separator: ", ")
            matchesGeneralInfo.setValue(strMonths, forKey: "strPreferredMonth")
    //        matchesGeneralInfo.setValue(self.strProposalNote, forKey: "proposalNotes")

            if arrTerms.count>0{
                matchesGeneralInfo.setValue(arrTerms[0], forKey: "leadGeneralTermsConditions")
            }
            
            matchesGeneralInfo.setValue(self.strInternalNotes, forKey: "notes")
            
            if isCustomerNotPresent {
                matchesGeneralInfo.setValue(self.strIsAllowCustomer, forKey: "isSelectionAllowedForCustomer")
            }else {
                matchesGeneralInfo.setValue("false", forKey: "isSelectionAllowedForCustomer")
            }
            matchesGeneralInfo.setValue("true", forKey: "isMailSend")
            matchesGeneralInfo.setValue("false", forKey: "isPreview")
            matchesGeneralInfo.setValue(strText, forKey: "smsReminders")
            matchesGeneralInfo.setValue(strPhone, forKey: "phoneReminders")
            matchesGeneralInfo.setValue(strEmail, forKey: "emailReminders")

            
            if isCustomerNotPresent
            {
                matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
                matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
            }
            
            matchesGeneralInfo.setValue("true", forKey: "isCommLeadUpdated")
            matchesGeneralInfo.setValue("true", forKey: "isCommTaxUpdated")

                    
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            let defaults = UserDefaults.standard
            defaults.setValue(matchesGeneralInfo.value(forKey: "statusSysName"), forKey: "leadStatusSales")
            defaults.setValue(matchesGeneralInfo.value(forKey: "stageSysName"), forKey: "stageSysNameSales")
            defaults.synchronize()

    //        if isSendProposal {
    //
    //        }else {
                self.updateLeadAppliedDiscountForAppliedCoupon()
    //        }
        }
    }
    func updateLeadIdDetailForPreview()
    {
        let context = getContext()
        
        if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card" {
            if !self.isElementIntegration {
                //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                strStatusPreview = "Complete"
            }
        }else {
            //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            strStatusPreview = "Complete"
        }
        
        if self.strGlobalAudio.count>0 {
            matchesGeneralInfo.setValue(self.strGlobalAudio, forKey: "audioFilePath")
        }else {
            matchesGeneralInfo.setValue("", forKey: "audioFilePath")
        }
        
        matchesGeneralInfo.setValue("true", forKey: "iAgreeTerms")

        let str = matchesGeneralInfo.value(forKey: "isCustomerNotPresent")
        
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
        }
        
        if self.customerSig == nil {
            matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
        }else {
            matchesGeneralInfo.setValue("true", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("true", forKey: "isAgreementGenerated")
        }
        
        
        
        matchesGeneralInfo.setValue("yes", forKey: "zSync")

        print(self.dictPricingInfo)
        var dictTemp = self.dictPricingInfo
//        if isSendProposal {
//            self.sendProposalCalculation()
//            dictTemp = self.dictPricingInfoProposal
//        }
        
        //let strFinalBilledAmount = strAmount//(dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strFinalBilledAmount = (dictTemp?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

        var strFinalCollectedAmount = strAmount
        var strTotalPrice = (dictTemp?["totalPrice"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

        let strCouponDis: String = (dictTemp?["couponDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
//        let strOtherDis: String = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let finalDis: Double = (Double(strCouponDis) ?? 0.0) //+ (Double(strOtherDis) ?? 0.0)
        let strCouponDiscount = String(format: "%.2f", finalDis)

        let strFinalSubTotal = (dictTemp?["subTotalInitial"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strOtherDiscount = (dictTemp?["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strTipDiscount = (dictTemp?["tipDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        let strLeadInspectionFee = (dictTemp?["leadInspectionFee"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

        if strPaymentMode.caseInsensitiveCompare("Cash") == .orderedSame || strPaymentMode.caseInsensitiveCompare("Check") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CreditCard") == .orderedSame || strPaymentMode.caseInsensitiveCompare("CollectattimeofScheduling") == .orderedSame {
            strFinalCollectedAmount = strAmount
        }else {
            
            if dictSelectedCustomPaymentMode.count > 0 && "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1"
            {
                strFinalCollectedAmount = strAmount
            }
            else
            {
                strFinalCollectedAmount = "0"
            }
            //strFinalCollectedAmount = "0"
        }
        
//        if !isSendProposal {
            matchesGeneralInfo.setValue(strFinalBilledAmount, forKey: "billedAmount")
            matchesGeneralInfo.setValue(strCouponDiscount, forKey: "couponDiscount")
        
        matchesGeneralInfo.setValue(strOtherDiscount, forKey: "otherDiscount")
        matchesGeneralInfo.setValue(strTipDiscount, forKey: "tipDiscount")
        matchesGeneralInfo.setValue(strLeadInspectionFee, forKey: "leadInspectionFee")
//        matchesGeneralInfo.setValue("true", forKey: "isTipEligible")
//        matchesGeneralInfo.setValue("true", forKey: "needTipService")

            matchesGeneralInfo.setValue(strFinalCollectedAmount, forKey: "collectedAmount")
            matchesGeneralInfo.setValue(strFinalSubTotal, forKey: "subTotalAmount")
            
            let strTaxAmt = (dictTemp?["taxAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTaxMaintAmt = (dictTemp?["taxAmountMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            
            let strTaxableInitialAmount = (dictTemp?["taxableAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTaxableMaintAmount = (dictTemp?["taxableMaintAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            let strSubTotalMaintenance = (dictTemp?["subTotalMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            let strTotalPriceMaintenance = (dictTemp?["totalPriceMaintenance"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)

            matchesGeneralInfo.setValue(strTotalPrice, forKey: "totalPrice")
            matchesGeneralInfo.setValue(strTaxAmt, forKey: "taxAmount")
            matchesGeneralInfo.setValue(strTaxMaintAmt, forKey: "taxMaintAmount")
            
            matchesGeneralInfo.setValue(strTaxableInitialAmount, forKey: "taxableAmount")
            matchesGeneralInfo.setValue(strTaxableMaintAmount, forKey: "taxableMaintAmount")
            
            matchesGeneralInfo.setValue(strSubTotalMaintenance, forKey: "subTotalMaintAmount")
            //matchesGeneralInfo.setValue(strTotalPriceMaintenance, forKey: "totalMaintPrice")
            matchesGeneralInfo.setValue((dictTemp?["totalDueAmount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil), forKey: "totalMaintPrice")
//        }
        
       // matchesGeneralInfo.setValue("Won", forKey: "stageSysName")
        strStagePreview = "Won"
    
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue("CompletePending", forKey: "stageSysName")
            if strPaymentMode == "CreditCard" || strPaymentMode == "Credit Card"{
                if !self.isElementIntegration {
                    //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
                    strStatusPreview = "Complete"
                }
            }
        }else {
            //matchesGeneralInfo.setValue("Complete", forKey: "statusSysName")
            strStatusPreview = "Complete"
        }
        
        matchesGeneralInfo.setValue("false", forKey: "isProposalFromMobile")

        
        
        let strMonths = arrayOfMonth.joined(separator: ", ")
        matchesGeneralInfo.setValue(strMonths, forKey: "strPreferredMonth")
//        matchesGeneralInfo.setValue(self.strProposalNote, forKey: "proposalNotes")

        if arrTerms.count>0{
            matchesGeneralInfo.setValue(arrTerms[0], forKey: "leadGeneralTermsConditions")
        }
        
        matchesGeneralInfo.setValue(self.strInternalNotes, forKey: "notes")
        
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue(self.strIsAllowCustomer, forKey: "isSelectionAllowedForCustomer")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isSelectionAllowedForCustomer")
        }
        //matchesGeneralInfo.setValue("true", forKey: "isMailSend")
        matchesGeneralInfo.setValue("true", forKey: "isPreview")
        
        matchesGeneralInfo.setValue(strText, forKey: "smsReminders")
        matchesGeneralInfo.setValue(strPhone, forKey: "phoneReminders")
        matchesGeneralInfo.setValue(strEmail, forKey: "emailReminders")

        
        if isCustomerNotPresent
        {
            matchesGeneralInfo.setValue("false", forKey: "isAgreementSigned")
            matchesGeneralInfo.setValue("false", forKey: "isAgreementGenerated")
        }
        
        matchesGeneralInfo.setValue("true", forKey: "isCommLeadUpdated")
        matchesGeneralInfo.setValue("true", forKey: "isCommTaxUpdated")

                
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        let defaults = UserDefaults.standard
        defaults.setValue(matchesGeneralInfo.value(forKey: "statusSysName"), forKey: "leadStatusSales")
        defaults.setValue(matchesGeneralInfo.value(forKey: "stageSysName"), forKey: "stageSysNameSales")
        defaults.synchronize()

//        if isSendProposal {
//
//        }else {
            self.updateLeadAppliedDiscountForAppliedCoupon()
//        }
    }
    
    
    func updateLeadAppliedDiscountForAppliedCoupon() {
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadAppliedDiscounts", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        let arrAllSold = self.arrStandardServiceReview
        print(arryOfData)
        var objIn: NSManagedObject?
        if arryOfData.count>0 {
            for data in arryOfData {
                objIn = (data as! NSManagedObject)
                for soldData in arrAllSold {
                    let id1: String = "\(objIn!.value(forKey: "soldServiceId") ?? "")"
                    let id2: String = "\(soldData.value(forKey: "soldServiceStandardId") ?? "")"
                    if id1 == id2 {
                        objIn!.setValue("true", forKey: "isApplied")
                    }
                }
            }
        }
        
        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func goToEmailHistory(){
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
        testController?.modalPresentationStyle = .fullScreen
        testController?.strAccountNo = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController?.strCompanyKey = Global().getCompanyKey()
        testController?.strUserName = Global().getUserName()
        
        self.present(testController!, animated: false, completion: nil)
    }
    
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    
    func goToServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow =  "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
    }
    
    func goToDetailView(){
        
        let storyboardIpad = UIStoryboard.init(name: "NewCommericialReview", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "NewCommercialMarketingContentDetailVC") as? NewCommercialMarketingContentDetailVC

        testController?.strMarketingContent =  strMarketingContent
        self.navigationController?.pushViewController(testController!, animated: false)
    }
    
    // MARK: Go to Appointment
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    
    func fetchImageFromDB(strLeadCommercialTargetId : String ,strMobileTargetId : String )  -> NSArray{
        var arrayOfImages = NSArray ()
        
        //            let strLeadCommercialTargetId = "\(dictTarget.value(forKey: "leadCommercialTargetId") ?? "")"
        //            let strMobileTargetId = "\(dictTarget.value(forKey: "mobileTargetId") ?? "")"
        //
        
        strHeaderTitle = "Target"
        
        if strLeadCommercialTargetId.count > 0
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadCommercialTargetId = %@", strLeadId, strHeaderTitle, strLeadCommercialTargetId))
        }
        else
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetId == %@", strLeadId, strHeaderTitle, strMobileTargetId))
        }
        
        return arrayOfImages
    }
    
    func fetchRenewalServiceFromCoreData(soldServiceId: String) -> String {
        print(soldServiceId)
        var strRenewalDesc = ""
        
        let arrAllObjRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && serviceId = %@", strLeadId, soldServiceId))
        
        if arrAllObjRenewal.count > 0 {
            let matchesRenewal: NSManagedObject = arrAllObjRenewal.object(at: 0) as! NSManagedObject
            print(matchesRenewal)
            var strDesc = "\(matchesRenewal.value(forKey: "renewalDescription") ?? "")"
            if strDesc != "" {
                strDesc = strDesc + ":"
            }
            
            //let strAmount = "\(matchesRenewal.value(forKey: "renewalAmount") ?? "")"
            
            let strAmount =  "\(convertDoubleToCurrency(amount: Double("\(matchesRenewal.value(forKey: "renewalAmount") ?? "")") ?? 0.0))"
            
            let dict = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesRenewal.value(forKey: "renewalFrequencySysName") ?? "")")
            let strFrequency = dict.value(forKey: "FrequencyName") ?? ""
            strRenewalDesc = "\(strDesc) \(strAmount) (\(strFrequency))"
        }
        
        return strRenewalDesc
    }
    
    // MARK: ------------- Pestpac Integration -------------------
    
    func showCreditCardDetail() -> String {
        // if billToLocationId.count > 0 && billToLocationId != 0 && paymentMode = "CreditCard"
        // show add card button else hide
        
        let billToId = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"//"1693164"
        var strTypeNo = "\(matchesGeneralInfo.value(forKey: "creditCardType") ?? "")"//"Discover" //Nedd to add by back end - creditCardType
        let creditCardNumber = "\(matchesGeneralInfo.value(forKey: "cardAccountNumber") ?? "")"//"*************7890" //Nedd to add by back end - creditCardNo
        
        
        
        if creditCardNumber.count >= 4  {
            let last4 = creditCardNumber.suffix(4)
            strTypeNo = strTypeNo + "  \(last4)"
        }
        // For Local Check.
        if(nsud.value(forKey: billToId) != nil){
            
            if nsud.value(forKey: billToId) is NSDictionary {
                
                let dictCreditCardInfo = nsud.value(forKey: billToId) as! NSDictionary
                let accountNumber = String("\(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")".suffix(4))
                strTypeNo = "\(dictCreditCardInfo.value(forKey: "creditCardType") ?? "") " + accountNumber
            }
        }
        
        print(strTypeNo) // Show this value with Add Cart button If strtypeno.count > 0 than only show card value lable
        return strTypeNo
    }
    
    func goToPestPac_PaymentIntegration() {
        
        self.view.endEditing(true)
        
        if isInternetAvailable() {
            
            let firstNameL = "\(matchesGeneralInfo.value(forKey: "billingFirstName") ?? "")"
            let lastNameL = "\(matchesGeneralInfo.value(forKey: "billingLastName") ?? "")"
            let billingAddress1L = "\(matchesGeneralInfo.value(forKey: "billingAddress1") ?? "")"
            let billingZipcodeL = "\(matchesGeneralInfo.value(forKey: "billingZipcode") ?? "")"
            let billToLocationIdL = "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")"
            let webLeadIdL = "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")"//strLeadId
            let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

            let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
            
            let ammountL = Double(strAmount) ?? 0.0
                        
            if contactNameToCheck.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
                
            }
            else if contactName.count > 30 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
                
            }
            else if billingAddress1L.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
                
            }else if billingZipcodeL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
                
            }else if billToLocationIdL.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
                
            }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
                
            }else if ammountL <= 0 {
                
                showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
                
            }else{
                
                let dictPaymentDetails = NSMutableDictionary()

                dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
                dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
                dictPaymentDetails.setValue("\(ammountL)", forKey: "amount")
                dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
                dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
                dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
                
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
                vc?.dictPaymentDetails = dictPaymentDetails
                self.navigationController?.present(vc!, animated: false, completion: nil)

            }
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        
        /*if isInternetAvailable() {
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterDashboardNew_iPhoneVC") as? FilterDashboardNew_iPhoneVC
            vc!.delegate = self
            vc!.strViewComeFrom = "Dashboard"
            self.navigationController?.present(vc!, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }*/
        
    }
    
    // MARK: ------------- Custom Payment Master -------------------
    
    func getPaymentModeFromMaster() -> NSMutableArray {
        
        var arrPaymentMaster = NSArray()
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

            if(dictMaster.value(forKey: "PaymentTypeObj") is NSArray) {
                
                arrPaymentMaster = dictMaster.value(forKey: "PaymentTypeObj") as! NSArray
            }
        }

        let arrPaymentNew = NSMutableArray()
        arrPaymentNew.add(["AddLeadProspect": "Cash"])
        arrPaymentNew.add(["AddLeadProspect": "Check"])
        arrPaymentNew.add(["AddLeadProspect": "Credit Card"])
        arrPaymentNew.add(["AddLeadProspect": "Auto Charge Customer"])
        arrPaymentNew.add(["AddLeadProspect": "Collect at time of Scheduling"])
        arrPaymentNew.add(["AddLeadProspect": "Invoice"])
        
        for item in arrPaymentMaster
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1" || ("\(dict.value(forKey: "IsActive") ?? "")" == "0" && "\(dict.value(forKey: "SysName")!)" == strPaymentMode)
            {
                arrPaymentNew.add(dict)
            }
        }
        print(arrPaymentNew)
        
        return arrPaymentNew
    }
}


extension NewCommericialReviewViewController : UITableViewDelegate , UITableViewDataSource , UIWebViewDelegate ,MFMailComposeViewControllerDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{ // cover letter x
            return 1
        } else if section == 1{ // introduction
            return 1
        }else if section == 2{ // sales marketing
            return 1//arrMarketingContent.count
        }else if section == 3{ // terms service
            if isTermsOfService == false{
                return 0
            }else{
                return 1
            }
        }else if section == 4{ // personal cell
            return 1
        }else if section == 5{ // inspection cell
            return 1
        }else if section == 6{
            if arrOfTagests.count == 0{
                return 0
            }else{
                return arrOfTagests.count
            }
        }else if section == 7{
            if arrOfScopes.count == 0{
                return 0
            }else{
                return arrOfScopes.count
            }
            
        }else if section == 8{
            
            if self.arrStandardServiceReview.count == 0{
                return 0
            }else{
                return self.arrStandardServiceReview.count
            }
            
            
        }else if section == 9{
            
            if self.arrCustomServiceReview.count == 0 {
                return 0
            }else{
                return self.arrCustomServiceReview.count
            }
            
        }else if section == 10{
            return 1
        }else if section == 11{
            return 1
        }else if section == 12{
            return 1
        }else if section == 13{
            if isPreferredMonths == false {
                return 0
            }else{
                
                if arrayOfMonth.count == 0 {
                    return 0
                }else{
                    return 1
                }
                
            }
        }else if section == 14{
            return 1
        }else if section == 15{
            return 1
        }else if section == 16{
            if strAgreementCheckList == ""{
                return 0
            }else{
                return 1
            }
            
        }else if section == 17{
            return beforePics.count
        }else if section == 18{
            return graphPics.count
        }else if section == 19{
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
    
            if reloadCoverLetterOnce
            {
                reloadCoverLetterOnce = false
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialCoverLetterCell", for: indexPath) as? NewCommercialCoverLetterCell
                
                    cell?.viewForCoverLetter.loadHTMLString((strMutableText) as String, baseURL: Bundle.main.bundleURL)
              
                return cell ?? UITableViewCell()
            }
            else
            {
                if isCoverLetter == false {
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialCoverLetterCell", for: indexPath) as? NewCommercialCoverLetterCell
                    
                        cell?.viewForCoverLetter.loadHTMLString((strMutableText) as String, baseURL: Bundle.main.bundleURL)
                  
                    return cell ?? UITableViewCell()
                }
            }
 
        }else if indexPath.section == 1{
            
            if reloadIntroductionOnce
            {
                reloadIntroductionOnce = false
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialIntroductionLetterCell", for: indexPath) as? NewCommercialIntroductionLetterCell
                cell?.webViewIntrodutionLetter.loadHTMLString(strMutableTextIntroductionLetter as String, baseURL: Bundle.main.bundleURL)
                

                return cell ?? UITableViewCell()
            }
            else
            {
                if isIntroductionLetter == false {
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialIntroductionLetterCell", for: indexPath) as? NewCommercialIntroductionLetterCell
                    cell?.webViewIntrodutionLetter.loadHTMLString(strMutableTextIntroductionLetter as String, baseURL: Bundle.main.bundleURL)
                    return cell ?? UITableViewCell()
                }
            }
            
        }else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialMarktingContentCell", for: indexPath) as?
            NewCommercialMarktingContentCell
                        
//            if isMarketingContent == false {
//                cell?.heightForMarktingContent.constant = 0
//            }else{
//                cell?.loadWebView(strMarketingContent: arrMarketingContent[indexPath.row])
//            }
//
//            cell?.callback = { [self] (strSuccessUpdate: String) -> () in
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
//                    reviewTableView.beginUpdates()
//                    reviewTableView.endUpdates()
//                }
//            }
            return cell ?? UITableViewCell()

        }else if indexPath.section == 3{
            
            if isTermsOfServiceContent == false {
                
            }else{
                if isTermsOfService == false {
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialTermsOfServiceCell", for: indexPath) as? NewCommercialTermsOfServiceCell
                    cell?.webViewTermsOfService.loadHTMLString(strTermsCondition as String, baseURL: Bundle.main.bundleURL)
                    return cell ?? UITableViewCell()
                }
            }
            
        }else if indexPath.section == 4{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialPersonalInformationCell", for: indexPath) as? NewCommercialPersonalInformationCell
            cell?.cellConfigure(objLeadDetails : self.matchesGeneralInfo)
            cell?.btn_Email.addTarget(self, action: #selector(actionViewBtnEmail(sender:)), for: .touchUpInside)
            cell?.btn_Mobile.addTarget(self, action: #selector(actionViewBtnCall(sender:)), for: .touchUpInside)
            cell?.btn_Address.addTarget(self, action: #selector(actionViewBtnAddress(sender:)), for: .touchUpInside)
            cell?.img_Profile.layer.cornerRadius = 5
            cell?.img_Profile.clipsToBounds = true
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialInspectionDetailsCell", for: indexPath) as? NewCommercialInspectionDetailsCell
            
            if !isInspectionLoaded {
                isInspectionLoaded = true

                cell?.strLeadId = strLeadId
                cell?.matchesGeneralInfo = self.matchesGeneralInfo
                // Do any additional setup after loading the view.
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
//                    cell?.loadDynamicForm()
//                    reviewTableView.beginUpdates()
//                    reviewTableView.endUpdates()
//                }
            }
            
            return cell ?? UITableViewCell()
        }else if indexPath.section == 6{
            
            if arrOfTagests.count == 0{
                //let cell = tableView.dequeueReusableCell(withIdentifier: "AddTargetImageCell", for: indexPath) as? AddTargetImageCell
                // cell?.tblAddtarget.isHidden = true
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddTargetImageCell", for: indexPath) as? AddTargetImageCell
                let obj = arrOfTagests[indexPath.row]  as! NSManagedObject
                
                cell?.lblTarget.text = "\(obj.value(forKey: "name") ?? "")"
                cell?.lblDescription.attributedText =   getAttributedHtmlStringUnicode(strText: "\(obj.value(forKey: "targetDescription") ?? "")")//htmlAttributedStringNewSales(strHtmlString : "\(obj.value(forKey: "targetDescription") ?? "")")
                
                let arrayImageReturn = fetchImageFromDB(strLeadCommercialTargetId: obj.value(forKey: "leadCommercialTargetId") as? String ?? "", strMobileTargetId: obj.value(forKey: "mobileTargetId") as? String ?? "")
                                
                cell?.arrayOfImages = arrayImageReturn
                cell?.strLeadId = strLeadId
                
                if cell?.arrayOfImages.count == 0{
                    cell?.heightOfTblAddTarget.constant = 0
                }
                
                
                cell?.tblAddTarget.reloadData()
                
                return cell ?? UITableViewCell()
            }
        }else if indexPath.section == 7{
            
            if arrOfScopes.count == 0{
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddScopeImageCell", for: indexPath) as? AddScopeImageCell
                let obj = arrOfScopes[indexPath.row]  as! NSManagedObject
                cell?.lblScope.text = "\(obj.value(forKey: "title") ?? "")"
                let strDesc = "\(obj.value(forKey: "scopeDescription") ?? "")"
                if strDesc.count == 0
                {
                    cell?.lblDiscription.text = ""
                }
                else
                {
                    cell?.lblDiscription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)
                }
                return cell ?? UITableViewCell()
            }
            
        }else if indexPath.section == 8{
            
            if self.arrStandardServiceReview.count == 0{
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddStandardServiceCell", for: indexPath) as? AddStandardServiceCell
                
                let objService = self.arrStandardServiceReview[indexPath.row]
                               
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "serviceSysName") ?? "")")
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "billingFrequencySysName") ?? "")")

                cell?.lblServiceName.text = "\(dictService.value(forKey: "Name") ?? "")"
                
                cell?.lblFinalIntialPrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
                cell?.lblMaintenancePrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
                cell?.lblFrequencyName.text = "\(objService.value(forKey: "serviceFrequency") ?? "")"
                //cell?.lblBillingFrequencyName.text = "Billed: $" + "\(objService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                
                let strBillingFrequncy = String(format: "%.2f", (Float("\(objService.value(forKey: "billingFrequencyPrice") ?? "")") ?? 0.0 ))
                cell?.lblBillingFrequencyName.text = "Billed: $" + strBillingFrequncy + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                
              //  cell?.lblServiceDescription.text = "\(dictService.value(forKey: "serviceDescription") ?? "")"
               // cell?.btnAddToAgreement.addTarget(self, action: #selector(btnAgreementCell), for: .touchUpInside)
                
                if objService.value(forKey: "serviceDescription") as! String == "" {
                    cell?.viewForHeightDescription.constant = 0
                }else{
                    cell?.viewForDescription.removeConstraint((cell?.viewForHeightDescription)!)
                    cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(objService.value(forKey: "serviceDescription") ?? "")")//htmlAttributedStringNewSales(strHtmlString: "\(objService.value(forKey: "serviceDescription") ?? "")")

                }
                
                let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objService.value(forKey: "serviceId") ?? "")")
                
                cell?.lblRenewalContent.isHidden = true
                cell?.lblRenewalSepartor.isHidden = true
                if strRenewal != "" {
                    cell?.lblRenewalContent.isHidden = false
                    cell?.lblRenewalSepartor.isHidden = false
                    var tempFontSize = CGFloat()
                    if DeviceType.IS_IPAD{
                        tempFontSize = 19
                    }
                    else{
                        tempFontSize = 14
                    }
                    let myString = NSAttributedString(string: "Renewal: ", attributes: [
                        NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: tempFontSize)
                    ])

                    let mutableAttString = NSMutableAttributedString()

                    mutableAttString.append(myString)
                    mutableAttString.append(NSAttributedString(string: strRenewal))

                    cell?.lblRenewalContent.attributedText = mutableAttString

                }
                
                if cell?.lblFrequencyName.text?.lowercased() == "OneTime".lowercased() || cell?.lblFrequencyName.text?.lowercased() == "One Time".lowercased()
                {
                    cell?.lblMaintenancePrice.isHidden = true
                    cell?.lblStrMaintenancePrice.isHidden = true
                    cell?.lblStrheightForMaintenancePrice.constant = 0
                    cell?.lblHeightForMaintenancePrice.constant = 0
                   // cell?.viewForBottom.constant = 50
                    
                }else {
                    cell?.lblMaintenancePrice.isHidden = false
                    cell?.lblStrMaintenancePrice.isHidden = false
                    cell?.lblStrheightForMaintenancePrice.constant = 24
                    cell?.lblHeightForMaintenancePrice.constant = 18
                   // cell?.viewForBottom.constant = 82
                }
                

                return cell ?? UITableViewCell()
                
            }
            
        }else if indexPath.section == 9{
            
            if self.arrCustomServiceReview.count == 0 {
                
            }else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddCustomCell", for: indexPath) as? AddCustomCell
             
                let objService = self.arrCustomServiceReview[indexPath.row]
                               
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "billingFrequencySysName") ?? "")")
                
                cell?.lblServiceName.text = "\(objService.value(forKey: "serviceName") ?? "")"
                cell?.lblIntialPrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
                cell?.lblMaintenancePrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
                cell?.lblFrequency.text = "\(objService.value(forKey: "serviceFrequency") ?? "")"
                cell?.lblBillingFrequency.text = "Billed: $" + "\(objService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"//"\(objService.value(forKey: "serviceName") ?? "")"
                
                if objService.value(forKey: "serviceDescription") as! String == "" {
                    //cell?.heightForViewServiceDescription.constant = 0
                    cell?.heightForViewDescription.constant = 0
                }else{
                    cell?.viewDescription.removeConstraint((cell?.heightForViewDescription)!)
                    cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(objService.value(forKey: "serviceDescription") ?? "")")//htmlAttributedStringNewSales(strHtmlString: "\(objService.value(forKey: "serviceDescription") ?? "")")

                }
                
                if cell?.lblFrequency.text?.lowercased() == "OneTime".lowercased() || cell?.lblFrequency.text?.lowercased() == "One Time".lowercased()
                {
                    cell?.lblMaintenancePrice.isHidden = true
                    cell?.lblStrMaintenancePrice.isHidden = true
                    cell?.lblStrheightForMaintenancePrice.constant = 0
                    cell?.lblHeightForMaintenancePrice.constant = 0
                    cell?.viewForBottom.constant = 50
                    
                }else {
                    cell?.lblMaintenancePrice.isHidden = false
                    cell?.lblStrMaintenancePrice.isHidden = false
                    cell?.lblStrheightForMaintenancePrice.constant = 24
                    cell?.lblHeightForMaintenancePrice.constant = 18
                    cell?.viewForBottom.constant = 82
                }
                
                return cell ?? UITableViewCell()
                
            }
            
        }else if indexPath.section == 10{
            //Pricing Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialPricingCell", for: indexPath) as? NewCommercialPricingCell
            if self.arrAppliedCoupon.count == 0 && self.arrAppliedCredit.count == 0 {
                //Coupon && Credit
                cell?.lblCouponDiscount.isHidden = true
                cell?.couponDiscountTitle.isHidden = true
                cell?.lblCredit.isHidden = true
                cell?.creditDiscountTitle.isHidden = true
                
                cell?.otherDisTopSpace.constant = 8
            }else if self.arrAppliedCoupon.count == 0 {
                //Coupon
                cell?.lblCouponDiscount.isHidden = true
                cell?.couponDiscountTitle.isHidden = true
//                cell?.lblCredit.isHidden = true
//                cell?.creditDiscountTitle.isHidden = true
                
                cell?.creditTopSpace.constant = 8
                cell?.otherDisTopSpace.constant = DeviceType.IS_IPAD ? 34 : 30.5
            }else if self.arrAppliedCredit.count == 0 {
                //Credit
                cell?.lblCredit.isHidden = true
                cell?.creditDiscountTitle.isHidden = true
                
                cell?.creditTopSpace.constant = 16
                cell?.otherDisTopSpace.constant = DeviceType.IS_IPAD ? 34 : 30.5
            }
            cell?.lblSubTotalInitial.text        = (self.dictPricingInfo?["subTotalInitial"] as! String)
            cell?.lblCouponDiscount.text         = (self.dictPricingInfo?["couponDiscount"] as! String)
            cell?.lblCredit.text                 = (self.dictPricingInfo?["credit"] as! String)
            cell?.lblOtherDiscount.text          = (self.dictPricingInfo?["otherDiscount"] as! String)
            cell?.lblTipDiscount.text            = (self.dictPricingInfo?["tipDiscount"] as! String)
            cell?.lblLeedInspectionFee.text      = (self.dictPricingInfo?["leadInspectionFee"] as! String)
            cell?.lblTotalPrice.text             = (self.dictPricingInfo?["totalPrice"] as! String)
            cell?.lblTaxAmount.text              = (self.dictPricingInfo?["taxAmount"] as! String)
            cell?.lblBillingAmount.text          = (self.dictPricingInfo?["billingAmount"] as! String)
            
            cell?.lblSubTotalMaintenance.text    = (self.dictPricingInfo?["subTotalMaintenance"] as! String)
            cell?.lblTotalPriceMaintenance.text  = (self.dictPricingInfo?["totalPriceMaintenance"] as! String)
            cell?.lblTaxAmountMaintenance.text   = (self.dictPricingInfo?["taxAmountMaintenance"] as! String)
            cell?.lblTotalDueAmount.text         = (self.dictPricingInfo?["totalDueAmount"] as! String)
            cell?.lblCreditMaint.text            = (self.dictPricingInfo?["creditMaint"] as! String)

            /*Maint billing info*/
            let arrMaintBillingPrice: [String] = self.dictPricingInfo?["arrMaintBillingPrice"] as! [String]
            if arrMaintBillingPrice.count > 0 {
                cell?.tableMaintSetUp(arrMaintBillingPrice : arrMaintBillingPrice)
            }else {
                cell?.arrMaintBillingPrice.removeAll()
                cell?.tblMaintFrqHeight.constant = 20
                cell?.tblMaintFrq.reloadData()
            }
            /**********************/
            
            
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 11{
            //Discount
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell", for: indexPath as IndexPath) as! DiscountTableViewCell
            
            /*Coupon Details*/
            if self.arrAppliedCoupon.count > 0 {
                cell.tableSetUp(arrAppliedCoupon : self.arrAppliedCoupon)
            }else {
                cell.arrAppliedCoupon.removeAll()
                cell.tblCouponHeight.constant = 20
                cell.tblCoupon.reloadData()
            }
            /**********************/
            
            /*Credit Details*/
            if self.arrAppliedCredit.count > 0 {
                cell.tableCreditSetUp(arrAppliedCredit : self.arrAppliedCredit)
            }else {
                cell.arrAppliedCredit.removeAll()
                cell.tblCreditHeight.constant = 20
                cell.tblCredit.reloadData()
            }
            /**********************/
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
//                reviewTableView.beginUpdates()
//                reviewTableView.endUpdates()
//            }
            return cell
        }else if indexPath.section == 12{
            //Payment Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath as IndexPath) as! PaymentTableViewCell
            cell.delegate = self
            
            cell.lblSubTotal.text        = (self.dictPricingInfo?["subTotalInitial"] as! String)
            cell.lblTotal.text          = (self.dictPricingInfo?["totalPrice"] as! String)
            cell.lblTaxAmount.text      = (self.dictPricingInfo?["taxAmount"] as! String)
            cell.lblBillingAmount.text  = (self.dictPricingInfo?["billingAmount"] as! String)
            //cell.lblAmount.text         = (self.dictPricingInfo?["billingAmount"] as! String)
            cell.txtAmount.text         = (self.dictPricingInfo?["billingAmount"] as! String).replacingOccurrences(of: "$", with: "")
            //self.strAmount = cell.txtAmount.text ?? ""
            
            cell.txtAmount.text = strAmount
            
            if self.strChequeNo != "" {
                cell.txtCheckNo.text = self.strChequeNo
            }
            if self.strLicenseNo != "" {
                cell.txtLicenceNo.text = self.strLicenseNo
            }
            if self.strDate != "" {
                cell.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)
            }
            
            cell.lblPaymentType.text = selectecPaymentType
            
            cell.amountViewHightConstraint.constant = 0
            cell.amountView.isHidden = true
            cell.checkView.isHidden = true
            cell.licenceView.isHidden = true
            cell.buttonsView.isHidden = true
            
            //amountViewHightConstraint
            if cell.lblPaymentType.text == "Cash" || cell.lblPaymentType.text == "Collect at time of Scheduling" ||  cell.lblPaymentType.text == "Credit Card" {
                cell.amountView.isHidden = false

                cell.amountViewHightConstraint.constant = 65
            }else if cell.lblPaymentType.text == "Check" {
                cell.amountViewHightConstraint.constant = 350
                
                cell.amountView.isHidden = false
                cell.checkView.isHidden = false
                cell.licenceView.isHidden = false
                cell.buttonsView.isHidden = false
                
            }else {
                cell.amountViewHightConstraint.constant = 0
            }
            
            //MARK: Hide Unhide View For Card
//            if cell.lblPaymentType.text == "Credit Card" {
//
//                if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
//                {
//                    cell.viewForCardType.isHidden = false
//                    cell.cardTypeHeightConstraint.constant = 50
//                    cell.lblCardType.text = self.showCreditCardDetail()
//                }
//                else
//                {
//                    cell.viewForCardType.isHidden = true
//                    cell.cardTypeHeightConstraint.constant = 0
//                }
//
//
//            }
//            else{
//                cell.viewForCardType.isHidden = true
//                cell.cardTypeHeightConstraint.constant = 0
//            }
            
            if strPaymentMode != ""
            {
                if "\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")".count > 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != 0 && Double("\(matchesGeneralInfo.value(forKey: "billToLocationId") ?? "")") != nil && isPestPacIntegartion()
                {
                    cell.viewForCardType.isHidden = false
                    cell.cardTypeHeightConstraint.constant = 50
                    cell.lblCardType.text = self.showCreditCardDetail()
                }
                else
                {
                    cell.viewForCardType.isHidden = true
                    cell.cardTypeHeightConstraint.constant = 0
                }
            }
            else
            {
                cell.viewForCardType.isHidden = true
                cell.cardTypeHeightConstraint.constant = 0
            }
            
            if self.strText == "true" || self.strText == "1" {
                cell.swText.isOn = true
            }else{
                cell.swText.isOn = false
            }
            
            if self.strPhone == "true" || self.strPhone == "1" {
                cell.swPhone.isOn = true
            }else {
                cell.swPhone.isOn = false
            }
            
            if self.strEmail == "true" || self.strEmail == "1" {
                cell.swEmail.isOn = true
            }else {
                cell.swEmail.isOn = false
            }
            
            //Nilind Call For Custom Payment Mode
            if cell.lblPaymentType.text != "Cash" && cell.lblPaymentType.text != "Collect at time of Scheduling" && cell.lblPaymentType.text != "Credit Card" && cell.lblPaymentType.text != "Check" && cell.lblPaymentType.text != "Invoice" && cell.lblPaymentType.text != "Auto Charge Customer"  {
                
                print(dictSelectedCustomPaymentMode)
                
                if "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")" == "1" || "\(dictSelectedCustomPaymentMode.value(forKey: "IsAmount") ?? "")".caseInsensitiveCompare("true") == .orderedSame
                {
                    cell.amountView.isHidden = false
                    cell.amountViewHightConstraint.constant = 65
                }
                else
                {
                    cell.amountViewHightConstraint.constant = 0
                    cell.amountView.isHidden = true
                }
                print(strPaymentMode)
            }
            
            if shouldShowElectronicFormLink {
                if isAuthFormFilled {
                    //Available
                    cell.btnEleTAuth.isHidden = false
                    cell.lblEleAuthStatus.textColor = UIColor.init(_colorLiteralRed: 0.0/155.0, green: 51.0/155.0, blue: 25.0/155.0, alpha: 1.0)
                    cell.lblEleAuthStatus.text = "(Signed Form Available On Account)"
                }else {
                    //Not Available
                    cell.btnEleTAuth.isHidden = false
                    cell.lblEleAuthStatus.textColor = .red
                    cell.lblEleAuthStatus.text = "(Signed Form Not Available On Account)"
                }
            }else {
                //Button Hide
                cell.btnEleTAuth.isHidden = true
                cell.lblEleAuthStatus.isHidden = true
            }
            
            if getOpportunityStatus() {
                cell.btnPayment.isUserInteractionEnabled = false
                cell.btnExpDate.isUserInteractionEnabled = false
                cell.btnPayment.isUserInteractionEnabled = false
                //cell.btnFrontI.isUserInteractionEnabled = false
                //cell.btnBackI.isUserInteractionEnabled = false
                cell.swText.isUserInteractionEnabled = false
                cell.swPhone.isUserInteractionEnabled = false
                cell.swEmail.isUserInteractionEnabled = false
                cell.txtAmount.isEnabled = false
                cell.txtCheckNo.isEnabled = false
                cell.txtLicenceNo.isEnabled = false
            }
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 13{
            if isPreferredMonths == false {
                
            }else{
                if arrayOfMonth.count == 0 {
                    
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialMonthOfServicesCell", for: indexPath) as? NewCommercialMonthOfServicesCell
                    let seprator = arrayOfMonth.joined(separator: ", ")
                    cell?.lblMonthOfService.text = seprator
                    return cell ?? UITableViewCell()
                }
            }
            
        }else if indexPath.section == 16 {
            if self.strAgreementCheckList == "" {
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialReviewAgreementCheckListCell", for: indexPath) as? NewCommercialReviewAgreementCheckListCell
                cell?.lblAgreementCheck.text = self.strAgreementCheckList
                return cell ?? UITableViewCell()
            }
           
            
        }else if indexPath.section == 14{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialReviewNotesCell", for: indexPath) as? NewCommericialReviewNotesCell
            cell?.lblNotes.text = self.strAddtionalNotes
            cell?.btnEditNots.addTarget(self, action: #selector(tapOfAdditionalNotes), for: .touchUpInside)
            
            cell?.setUp(textDayValue: strValidFor, isAgreementValid: strIsAgreementValidForNotes)
            
            if getOpportunityStatus()
            {
                cell?.btnEditNots.isEnabled = false
            }
            else
            {
                cell?.btnEditNots.isEnabled = true
            }
            cell?.btnAgreement.isHidden = true
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 15{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialReviewInternalNotesCell", for: indexPath) as? NewCommericialReviewInternalNotesCell
            cell?.lblInternalNotes.text = strInternalNotes
            cell?.btnEditNots.addTarget(self, action: #selector(tapOfInternalNotes), for: .touchUpInside)

            cell?.btnEditNots.isEnabled = true
            
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 17{
            
            if isImagePreview == false {
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialImagePreviewCell", for: indexPath) as? NewCommericialImagePreviewCell
                
                let objTemp = beforePics[indexPath.row] as! NSManagedObject
                
                cell?.lblCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String)
                cell?.lblDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
                
                let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                
                let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                
                if isImageExists!  {
                    
                    cell?.imagePreview.image = image
                    
                    
                }else {
                    
                    let defsLogindDetail = UserDefaults.standard
                    
                    let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary
                    
                    var strURL = String()
                    
                    if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
                    {
                        
                        strURL = "\(value)"
                        
                    }
                    
                    if strImagePath.contains("Documents")
                    {
                        strURL = strURL + "\(strImagePath)"
                    }
                    else
                    {
                        strURL = strURL + "/Documents/" + "\(strImagePath)"
                    }
                    
                    strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)
                    
                    let image: UIImage = UIImage(named: "NoImage.jpg")!
                    
                    cell?.imagePreview.image = image
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string:strURL)
                        let data = try? Data(contentsOf: url!)
                        
                        if data != nil && (data?.count)! > 0 {
                            
                            let image: UIImage = UIImage(data: data!)!
                            
                            DispatchQueue.main.async {
                                
                                saveImageDocumentDirectory(strFileName: strImagePath , image: image)
                                
                                cell?.imagePreview.image = image
                                
                            }}
                        
                    }
                }
                
                return cell ?? UITableViewCell()
                
            }
            
        } else if indexPath.section == 18{
            
            if graphPics.count == 0 {
                
            }else {
                
                
                
                if isGraphPreview == false {
                    
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialReviewGraphCell", for: indexPath) as? NewCommercialReviewGraphCell
                    
                    
                    let objTemp = graphPics[indexPath.row] as! NSManagedObject
                    
                    cell?.lblCaption.text = (objTemp.value(forKey: "leadImageCaption") as! String)
                    cell?.lblDescription.text = (objTemp.value(forKey: "descriptionImageDetail") as! String)
                    
                    let strImagePath = objTemp.value(forKey: "leadImagePath") as! String
                    
                    let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)
                    
                    let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)
                    
                    if isImageExists!  {
                        
                        cell?.imagePreview.image = image
                        
                        
                    }else {
                        
                        
                    }
                    
                    return cell ?? UITableViewCell()
                }
            }
            
        } else if indexPath.section == 19 {
            if indexPath.row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialTermsCondtionCell", for: indexPath) as? NewCommericialTermsCondtionCell
                
                  cell?.btnTermsCondition.addTarget(self, action: #selector(stackTap), for: .touchUpInside)
                  cell?.btnTermsCondition.setTitle(strLblTermsCondition, for: .normal)
                
                return cell ?? UITableViewCell()
                
            }else {
                //Signature
                let cell = tableView.dequeueReusableCell(withIdentifier: "SignatureCell", for: indexPath as IndexPath) as! SignatureTableViewCell
                            
                cell.delegate = self
                
                if isCustomerNotPresent {
                    cell.btnCustomerNot.isSelected = true
                    cell.custSigHeight.constant =  0
                    cell.tandC_height.constant =  80

                    cell.lblCustSigTitle.isHidden = true
                }
                if cell.btnCustomerNot.isSelected {
                    cell.btnAllowCustomer.isHidden = false
                    cell.lblAllowCustomer.isHidden = false
                    
                    //Nilind
                    if strIsAllowCustomer == "true" || strIsAllowCustomer == "True"
                    {
                        cell.btnAllowCustomer.isSelected = true
                    }
                    else
                    {
                        cell.btnAllowCustomer.isSelected = false
                    }
                    //End
                    
                }else {
                    cell.btnAllowCustomer.isHidden = true
                    cell.lblAllowCustomer.isHidden = true
                }
                
                //Set Signature if available
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

                let strEmployeeSignature = "\(dictLoginData.value(forKeyPath: "EmployeeSignature")!)"
                if nsud.bool(forKey: "isPreSetSignSales") && strEmployeeSignature.count > 0 && self.strSalesSignature.count == 0
                {
                    if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                        
                        self.imageFromServerURL(url: urlString)
                        
                        cell.btnTechSig.setImage(self.techSig, for: .normal)
                    }
                }else {
                    DispatchQueue.main.async() {
                        if self.strSalesSignature != "" {
                            
                            self.strSalesSignature = self.strSalesSignature.replacingOccurrences(of: "\\", with: "/")
                            let salesSig = (self.strSalesSignature as NSString).lastPathComponent
                            let salesSig1 = "\(self.strSalesssUrlMainServiceAutomation)/Documents//Signature/\(salesSig)"
                            var url: URL?
                            //let preSetSig = (strEmployeeSignature as NSString).lastPathComponent
                            let newString = strEmployeeSignature.replacingOccurrences(of: "\\", with: "/")
                            let preSetSig = (newString as NSString).lastPathComponent
                            var strUrl = ""
                            if salesSig == preSetSig {
                                if  let urlString = strEmployeeSignature.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url1 = URL(string: urlString)  {
                                    strUrl = urlString
                                    //url = url1
                                }
                            }else{
                                strUrl = salesSig1
                            }
                            print(url)
                            self.imageFromServerURL(url: strUrl)
                            
                        }
                        cell.btnTechSig.setImage(self.techSig, for: .normal)
                    }
                }
                
                DispatchQueue.main.async() {
                    if self.strCustomerSignature != "" {
                        //let custSig = (self.strCustomerSignature as NSString).lastPathComponent
                        let custSig = ((self.strCustomerSignature.replacingOccurrences(of: "\\", with: "/")) as NSString).lastPathComponent
                        let custSig1 = "\(self.strSalesssUrlMainServiceAutomation)/Documents//Signature/\(custSig)"

                        self.imageCustomerFromServerURL(url: custSig1)
                        
                        /*let url = URL(string: custSig1)
                         let imgCust: UIImageView = UIImageView()
                         imgCust.kf.setImage(with: url)
                         
                         if imgCust.image != nil {
                         self.customerSig = imgCust.image
                         }else {
                         let imagePath = self.getImage(strImageName: self.strCustomerSignature)
                         if imagePath != "" {
                         self.customerSig = UIImage(contentsOfFile: imagePath)
                         }
                         }*/
                        
                    }
                    cell.btnCustomerSig.setImage(self.customerSig, for: .normal)
                }
                
                
                if getOpportunityStatus() {
                    cell.btnIAgree.isSelected = true
                    self.isIAgreeTAndC = true
                    cell.btnIAgree.isUserInteractionEnabled = false
                    cell.btnCustomerNot.isUserInteractionEnabled = false
                    cell.btnTechSig.isUserInteractionEnabled = false
                    cell.btnCustomerSig.isUserInteractionEnabled = false
                }
                
                cell.btnTechSig.isUserInteractionEnabled = true
                
                //DefaultIsAllowCustomertToMakeSelection
                

                
                return cell ?? UITableViewCell()
            }
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let discountCell_IndexPath = IndexPath(row: 0, section: 11)
        let inspectionCell_IndexPath = IndexPath(row: 0, section: 5)
        
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if lastVisibleIndexPath == discountCell_IndexPath || lastVisibleIndexPath == inspectionCell_IndexPath {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                        reviewTableView.beginUpdates()
                        reviewTableView.endUpdates()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.section == 11 {
            if self.arrAppliedCoupon.count == 0 && self.arrAppliedCredit.count == 0 {
                return 0
            }
        }
        
        if indexPath.section == 0 {
            
            if isCoverLetter == false{
                return 0
            }
            
        }
        
        if indexPath.section == 1 {
            
            if isIntroductionLetter == false{
                return 0
            }
            
        }
        
        if indexPath.section == 2 {
            
            if isMarketingContent == false{
                return 0
            }
            
        }
        
        if indexPath.section == 3 {
            
            if isTermsOfServiceContent == false{
                return 0
            }
            
        }
        
        
        if indexPath.section == 17 {
            
            if isImagePreview == false{
                return 0
            }
            
        }
        
        if indexPath.section == 18 {
            
            if isGraphPreview == false{
                return 0
            }
            
        }
        
        return UITableView.automaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // HEADER VIEW
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  50))
        viewheader.backgroundColor = UIColor.white
        
        // INSIDE LABEL
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height:45))
        // HEADER BOTTOM LINE
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 43.0, width: viewheader.frame.size.width, height: 1)
        bottomBorder.backgroundColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        
        if tableView == reviewTableView
        {
            if(section == 0)
            {
              
                lbl.text = "Cover Letter"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
                var addButton = UIButton()
                addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                addButton.addTarget(self, action: #selector(coverLetterAction), for: .touchUpInside)
              
                if isCoverLetter == false{
                    addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                }else{
                    addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                }
                
                viewheader.addSubview(addButton)
            }
            else if(section == 1)
            {
                lbl.text = "Introduction Letter"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
                var addButton = UIButton()
                addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                addButton.addTarget(self, action: #selector(introductionLetterAction), for: .touchUpInside)
              
                if isIntroductionLetter == false{
                    addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                }else{
                    addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                }
                
                viewheader.addSubview(addButton)
            }
            else if(section == 2)
            {
                lbl.text = "Marketing Content"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
                var addButton = UIButton()
                addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                addButton.addTarget(self, action: #selector(marketingContentAction), for: .touchUpInside)
              
                if isMarketingContent == false{
                    addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                }else{
                    addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                }
                
                viewheader.addSubview(addButton)
                 
            }
            else if(section == 3)
            {
                if isTermsOfService == false{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Terms Of Service"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    
                    var addButton = UIButton()
                    addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                    addButton.addTarget(self, action: #selector(termsOfServiceAction), for: .touchUpInside)
                  
                    if isTermsOfServiceContent == false{
                        addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                    }else{
                        addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                    }
                    
                    viewheader.addSubview(addButton)
                }
                
               
            }else if (section == 4)
            {
                lbl.text = "Personal Information"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
            }else if (section == 5)
            {
                lbl.text = "Inspection Details"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
            }
            else if section == 6 {
                
                if arrOfTagests.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Target"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                }
                
            }else if section == 7 {
                
                if arrOfScopes.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Scope"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    
                }
                
            }else if section == 8 {
                
                
                if self.arrStandardServiceReview.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Standard Service"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                }
                
            }else if section == 9 {
                
                if self.arrCustomServiceReview.count == 0 {
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Custom Service"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                }
                
            }
            else if section == 10{
                
                lbl.text = "Pricing Information"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
            }
            
            else if section == 11 {
              
                bottomBorder.isHidden = true
                viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                lbl.text = "Discounts"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
            }
            
            else if section == 12{
                bottomBorder.isHidden = true
                viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                lbl.text = "Payment Infomation"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
            }
            else if section == 13{
                
                if isPreferredMonths == false {
                    viewheader.isHidden = true
                }else{
                    
                    if arrayOfMonth.count == 0 {
                        viewheader.isHidden = true
                    }else{
                        bottomBorder.isHidden = true
                        viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                        lbl.text = "Month Of Services"
                        lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    }
                    
                }
                  
            }
            else if section == 16 {
                
                if strAgreementCheckList == "" {
                    viewheader.isHidden = true
                }else{
                    bottomBorder.isHidden = true
                    viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                    lbl.text = "Agreement Checklist"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                }
                
                
            }
            else if section == 14 {
                
                bottomBorder.isHidden = true
                viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                lbl.text = "Notes"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
            }
            else if section == 15 {
                
                viewheader.isHidden = true
                
            }
            else if section == 17 {
                
                lbl.text = "Image Preview"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
                var addButton = UIButton()
                addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                addButton.addTarget(self, action: #selector(imagePreviewAction), for: .touchUpInside)
              
                if isImagePreview == false{
                    addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                }else{
                    addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                }
                
                viewheader.addSubview(addButton)
            }
            else if section == 18 {
                
                lbl.text = "Graph Image Preview"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                
                var addButton = UIButton()
                addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 40, y: 10, width: 30, height: 30))
                addButton.addTarget(self, action: #selector(graphPreviewAction), for: .touchUpInside)
              
                if isGraphPreview == false{
                    addButton.setImage(#imageLiteral(resourceName: "hide_iPad.png"), for: .normal)
                }else{
                    addButton.setImage(#imageLiteral(resourceName: "show_iPad.png"), for: .normal)
                }
               
                viewheader.addSubview(addButton)
                
            }
            
            else if section == 19 {
                
                if #available(iOS 13.0, *) {
                    bottomBorder.isHidden = true
                    viewheader.backgroundColor = #colorLiteral(red: 0.9523573518, green: 0.9524714351, blue: 0.9523186088, alpha: 1)
                    lbl.text = "Signatures"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                } else {
                    // Fallback on earlier versions
                }
                
                
            }
            
            if DeviceType.IS_IPAD{
                lbl.font = UIFont.boldSystemFont(ofSize: 18)
            }else{
               // lbl.font = UIFont.boldSystemFont(ofSize: 15)
                lbl.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            }
           
            viewheader.addSubview(lbl)
            viewheader.layer.addSublayer(bottomBorder)
            return viewheader
        }
        else
        {
            return viewheader
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         // remove header from tbleview
        if section == 3 {
            if isTermsOfService == false{
                return 0
            }
        }else if section == 6{
            if arrOfTagests.count == 0{
                return 0
            }
        }else if section == 7{
            if arrOfScopes.count == 0{
                return 0
            }
        } else if section == 8{
            
            if self.arrStandardServiceReview.count == 0 {
                return 0
            }
            
        }else if section == 9{
            
            if self.arrCustomServiceReview.count == 0 {
                return 0
            }
            
        }else if section == 11 {
            
            if self.arrAppliedCoupon.count == 0 && self.arrAppliedCredit.count == 0 {
                return 0
            }
            
        }else if section == 13 {
            if isPreferredMonths == false{
                return 0
            }
        }
        else if section == 15 {
            return 0
        }else if section == 16 {
            if strAgreementCheckList == "" {
                return 0
            }
        }
       
        return 40
    }
    
    func imageFromServerURL(url: String){
        let currentUrl = url
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let task = session.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                DispatchQueue.global(qos: .background).async {
                    
                    if let downloadedImage = UIImage(data: data!) {
                        if (url == currentUrl) {//Only cache and set the image view when the downloaded image is the one from last request
                            
                            self.techSig = downloadedImage
                            if self.isReloadOnce == false {
                                self.isReloadOnce = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.reviewTableView.reloadData()
                                }
                            }
                            if self.isRefreshInspector == true {
                                self.isRefreshInspector = false
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.reviewTableView.reloadData()
                                }
                            }
                            
                        }
                    }
                }
            }
            else {
                print(error)
            }
        })
        task.resume()
        
    }
    
    func imageCustomerFromServerURL(url: String){
        let currentUrl = url
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let task = session.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                DispatchQueue.global(qos: .background).async {
                    
                    if let downloadedImage = UIImage(data: data!) {
                        if (url == currentUrl) {//Only cache and set the image view when the downloaded image is the one from last request
                            
                            self.customerSig = downloadedImage

                            if self.isReloadOnceCustomer == false {
                                self.isReloadOnceCustomer = true
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                                    self.reviewTableView.reloadData()
                                }
                            }
                        }
                    }
                }
            }
            else {
                print(error)
            }
        })
        task.resume()
        
    }
    
    // MARK: -----------CustomerInfo Action---------
    
    @objc func actionViewBtnEmail(sender : UIButton) {
        self.view.endEditing(true)
        sendMail()
    }
    
    @objc func actionViewBtnCall(sender : UIButton) {
        self.view.endEditing(true)
        makeCall()
    }
    
    @objc func actionViewBtnAddress(sender : UIButton) {
        self.view.endEditing(true)
        let strAddress = Global().strCombinedAddressService(for: matchesGeneralInfo) ?? ""
        
        if strAddress.count > 0
        {
            
            let alertController = UIAlertController(title: "Alert", message: strAddress, preferredStyle: .alert)
            // Create the actions
            
            let okAction = UIAlertAction(title: "Navigate on map", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                //[global redirectOnAppleMap:self :_txtViewAddress_ServiceAddress.text]
                Global().redirect(onAppleMap: self, strAddress)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No address found", viewcontrol: self)
        }
    }
    
    func sendMail()
    {
        let arrEmail = NSMutableArray()
        
        if "\(self.matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(self.matchesGeneralInfo.value(forKey: "primaryEmail") ?? "")")
        }
        if "\(self.matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")".count > 0
        {
            arrEmail.add("\(self.matchesGeneralInfo.value(forKey: "secondaryEmail") ?? "")")
        }
        
        if arrEmail.count > 0
            
        {
            let alert = UIAlertController(title: "", message: "Make your email on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            for item in arrEmail
            {
                let strEmail = item as! String
                
                alert.addAction(UIAlertAction(title: "\(strEmail)", style: .default , handler:{ (UIAlertAction)in
                    
                    if (strEmail.count > 0)
                    {
                        
                        self.sendEmail(strEmail: strEmail)
                    }
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No email found", viewcontrol: self)
            
        }
    }
    
    
    func makeCall()
    {
        let arrNoToCall = NSMutableArray()
        
        let strCell = "\(self.matchesGeneralInfo.value(forKey: "servicePrimaryPhone") ?? "")"
        
        if strCell.count > 0
            
        {
            
            let alert = UIAlertController(title: "", message: "Make your call on selection", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            //            for item in arrNoToCall
            //            {
            let strNo = strCell
            
            alert.addAction(UIAlertAction(title: "\(strNo)", style: .default , handler:{ (UIAlertAction)in
                
                
                if (strNo.count > 0)
                {
                    Global().calling(strNo)
                }
                
            }))
            
            
            //            }
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
                
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            self.present(alert, animated: true, completion: {
                
                
            })
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No contacts detail found", viewcontrol: self)
            
        }
    }
    
    @objc func tapOfAdditionalNotes(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialConfigureAddtionalNote) as! NewCommercialConfigureAddtionalNote
        nextViewController.strValue = "Addtional Notes"
        nextViewController.delegate = self
        nextViewController.addtionalNote = self.strAddtionalNotes
        nextViewController.strLeadId = strLeadId
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @objc func tapOfInternalNotes(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialConfigureAddtionalNote) as! NewCommercialConfigureAddtionalNote
        nextViewController.strValue = "Internal Notes"
        nextViewController.delegate = self
        nextViewController.addtionalNote = strInternalNotes
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @objc func stackTap(){
        openTableViewPopUp(tag: 105, ary: arrayTermsAndConditions, aryselectedItem: arrayTermsAndConditionsMultipleSelected)
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "NewCommercialServices", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if getOpportunityStatus()
        {
            vc.strStatus = "Complete"
        }
        else
        {
            vc.strStatus = "InComplete"
        }
        vc.strFrom = "NewCommercial"
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = ary
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    
    @objc func imagePreviewAction(){
        if isImagePreview == false{
            isImagePreview = true
        }else{
            isImagePreview = false
        }
        reviewTableView.reloadData()
    }
    
    @objc func graphPreviewAction(){
        if isGraphPreview == false{
            isGraphPreview = true
        }else{
            isGraphPreview = false
        }
        reviewTableView.reloadData()
    }
    
    @objc func coverLetterAction(){
        
        if isCoverLetter == false{
            isCoverLetter = true
        }else{
            isCoverLetter = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            reviewTableView.reloadData()
            reviewTableView.beginUpdates()
            reviewTableView.endUpdates()
        }

        
    }
    
    @objc func introductionLetterAction(){
        if isIntroductionLetter == false{
            isIntroductionLetter = true
        }else{
            isIntroductionLetter = false
        }
        reviewTableView.reloadData()
        
        
    }
    
    @objc func marketingContentAction(){
//        if isMarketingContent == false{
//            isMarketingContent = true
//        }else{
//            isMarketingContent = false
//        }
//        reviewTableView.reloadData()
        self.goToDetailView()
        
    }
    
    @objc func termsOfServiceAction(){
        if isTermsOfServiceContent == false{
            isTermsOfServiceContent = true
        }else{
            isTermsOfServiceContent = false
        }
        reviewTableView.reloadData()
        self.viewWillAppear(false)
    }
    
    // MARK: -  ------------------------------ Send Mail Delegate ------------------------------
    
    func sendEmail(strEmail: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(strEmail)"])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
}

//Protocols

extension NewCommericialReviewViewController : callBackAddtionalNotes {
    
    
    func callBackMethodAdditonalNotes(data: String) {
        print("addtional Nptes")
        print(data)
        self.strAddtionalNotes = data
        self.updatePaymentInfo()
        reviewTableView.reloadData()
    }
    
    func callBakMethodInternalNotes(data: String) {
        self.strInternalNotes = data
        self.updateLeadDetails()
        reviewTableView.reloadData()
    }
    
    func updatePaymentInfo() {
        
        self.objPaymentInfo?.setValue(self.self.strAddtionalNotes, forKey: "specialInstructions")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func updateLeadDetails() {

        self.matchesGeneralInfo.setValue(self.strInternalNotes, forKey: "notes")

        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
   
    //MARK:  == Save Terms And Conditions ==
    
    func saveTermsAndConditions(arraySeletecTermsAndConditions:NSMutableArray)
    {
        
        let dictLogInDetail = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeBranchSysName = "\(dictLogInDetail.value(forKey: "EmployeeBranchSysName")!)"
        
        
        if(arraySeletecTermsAndConditions.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        }
        
        for item in arraySeletecTermsAndConditions
        {
            let arrOfKeys = NSMutableArray()
            let arrOfValues = NSMutableArray()
            
            // keys
            arrOfKeys.add("companyKey")
            arrOfKeys.add("userName")
            arrOfKeys.add("leadId")
            arrOfKeys.add("leadCommercialTermsId")
            arrOfKeys.add("branchSysName")
            arrOfKeys.add("termsnConditions")
            arrOfKeys.add("termsId")
            
            // values
            arrOfValues.add(Global().getCompanyKey())
            arrOfValues.add(Global().getUserName())
            arrOfValues.add(strLeadId)
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            arrOfValues.add("\(employeeBranchSysName)")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "TermsConditions") ?? "")")
            arrOfValues.add("\((item as! NSDictionary).value(forKey: "Id")!)")
            
            saveDataInDB(strEntity: "LeadCommercialTermsExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }

    }
    
    //MARK:  == Fetch Terms And Conditions ==
    fileprivate func fetchMultiTermsAndConditionsFromLocalDB()
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Open") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Scheduled") == .orderedSame) {
            
            print("true")
            
        }else {
            
            arrayTermsAndConditionsMultipleSelected.removeAllObjects()
            strLblTermsCondition = "---Select---"
            
        }
        
        let arrayMultiTermsConditions = getDataFromLocal(strEntity: "LeadCommercialTermsExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayMultiTermsConditions.count > 0)
        {
            for match in arrayMultiTermsConditions
            {
                for item in arrayTermsAndConditions
                {
                    if("\((match as! NSManagedObject).value(forKey: "termsId")!)" == "\((item as! NSDictionary).value(forKey: "Id")!)")
                    {
                        if(arrayTermsAndConditionsMultipleSelected.contains(item as! NSDictionary) == false)
                        {
                            arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                            
                            break
                        }
                        
                        
                        
                    }
                }
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayTermsAndConditionsMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
                }
                termsTitle.removeLast()
                strLblTermsCondition = termsTitle
            }
        }
    }
    
    
}


// MARK: ----------SalesReviewCell Delegate Cell---------
// MARK:
extension NewCommericialReviewViewController: SalesReviewCellDelegate {
    
    func callBackAddCard(cell: PaymentTableViewCell) {
        //MARK: - Add Card Action
        print("Add Card")
        self.goToPestPac_PaymentIntegration()
    }
    
    
    func callBackRefreshInspecotrSignature(cell: SignatureTableViewCell) {
        
        if(isInternetAvailable() == false)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Internet connection is reqiured to refresh Inspector signature", viewcontrol: self)
        }
        else
        {
            let strSignUrl: String = nsud.value(forKey: "ServiceTechSignPath") as? String ?? ""
            let isPreSetSign: Bool = nsud.bool(forKey: "isPreSetSignSales")

            if isPreSetSign && strSignUrl != ""
            {
                self.objPaymentInfo?.setValue("", forKey: "salesSignature")
                self.strSalesSignature = ""
                let context = getContext()
                //save the object
                do { try context.save()
                    print("Record Saved Updated.")
                } catch let error as NSError {
                    debugPrint("Could not save. \(error), \(error.userInfo)")
                }
                
                isRefreshInspector = true
                self.reviewTableView.reloadData()
            }
            else
            {
                self.isEditInSalesAuto = true
                let defaults = UserDefaults.standard
                defaults.set("fromInspector", forKey: "signatureType")
                defaults.synchronize()
                
                self.isCustomer = true
                if DeviceType.IS_IPAD {
                    let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
                    vc?.strType = "inspector"
                    self.present(vc!, animated: true)
                }else {
                    let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
                    vc?.strType = "inspector"
                    self.present(vc!, animated: true)
                }
            }
           
        }

    }
    
    func textFieldEditingChanged(cell: PaymentTableViewCell, value: String) {
        self.isEditInSalesAuto = true
        self.strAmount = value
    }
    
    func textFieldEditingChangedCheck(cell: PaymentTableViewCell, value: String) {
        self.isEditInSalesAuto = true
        self.strChequeNo = value
    }
    
    func textFieldEditingChangedLicence(cell: PaymentTableViewCell, value: String) {
        self.isEditInSalesAuto = true
        self.strLicenseNo = value
    }
    
    func callBackEditNotesMethod(cell: NotesTableViewCell) {
        self.isEditInSalesAuto = true
        
       /* let indexPath = tblView.indexPath(for: cell)
        
        var title = ""
        var message = ""
        if indexPath?.row == 0 {
            if isSendProposal {
                title = NotesForConfigure.proposal.rawValue
                message = strProposalNote
            }else {
                title = NotesForConfigure.customer.rawValue
                message = strCustomerNote
            }
        }else if indexPath?.row == 1 {
            title = NotesForConfigure.proposal.rawValue
            message = strProposalNote
        }else {
            title = NotesForConfigure.internalN.rawValue
            message = strInternalNote
        }
        
        
        //Add Observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.notesValue),
            name: Commons.kNotificationNotes,
            object: nil)
        
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "FormBuilder" : "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNoteViewController") as? SalesNoteViewController
        vc?.strTitle = title
        vc?.strMessage = message
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true)
        */
    }
    
    
    func callBackPaymentMethod(cell: PaymentTableViewCell) {
        print("Payment")
        self.isEditInSalesAuto = true
        //AddLeadProspect
//        let arrPayment = [["AddLeadProspect": "Cash"], ["AddLeadProspect": "Check"], ["AddLeadProspect": "Credit Card"], ["AddLeadProspect": "Auto Charge Customer"], ["AddLeadProspect": "Collect at time of Scheduling"], ["AddLeadProspect": "Invoice"]]
//        openTableViewPopUp(tag: 3335, ary: (arrPayment as NSArray).mutableCopy() as! NSMutableArray)
        
        openTableViewPopUp(tag: 3335, ary: getPaymentModeFromMaster())

    }
    
    func callBackExpDate(cell: PaymentTableViewCell) {
        self.isEditInSalesAuto = true
        print("ExpDate")
        
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
       // let fromDate = formatter.date(from:((tf_Date.text?.count)! > 0 ? tf_Date.text! : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheet(slectedDate: Date(),Picker: datePicker, view: self.view ,strTitle : "Select Service Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
        
            self.strDate = formatter.string(from: datePicker.date)
            print(self.strDate)
            cell.btnExpDate.setTitle("Expiration Date: \(self.strDate)", for: .normal)

        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callBackFronImage(cell: PaymentTableViewCell, sender: Any) {
        self.isEditInSalesAuto = true
        print("FronImage")
        self.isFrontImage = true
        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
        
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
        
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                self.openCamera(sourceDevice: .front)
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    func callBackBackImage(cell: PaymentTableViewCell, sender: Any) {
        self.isEditInSalesAuto = true
        print("BackImage")
        self.isFrontImage = false

        let alert = UIAlertController(title: "Make your selection", message: nil, preferredStyle: .actionSheet)
        
            alert.addAction(UIAlertAction(title: "Preview Image", style: .default, handler: { _ in
                self.openPreviewImage()
            }))
        
            alert.addAction(UIAlertAction(title: "Capture New", style: .default, handler: { _ in
                self.openCamera(sourceDevice: .rear)
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as! UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    func callBackEletronicAuth(cell: PaymentTableViewCell)
    {
        self.isEditInSalesAuto = true
        
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            if DeviceType.IS_IPAD {
                
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPad", bundle: nil)
                
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPad") as! ElectronicAuthorization_iPad
 
                if(self.strGlobalStatusSysName.lowercased() == "complete" && self.strGlobalStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
                
            }else {
                
                let storyboard = UIStoryboard.init(name: "ElectronicAuthorization_iPhone", bundle: nil)
                let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPhone") as! ElectronicAuthorization_iPhone
            
                if(self.strGlobalStatusSysName.lowercased() == "complete" && self.strGlobalStageSysName.lowercased() == "won")
                {
                    if(isInternetAvailable() == false)
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(testController, animated: false)
                    }
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
        
    }


    
    func callBackEletronicAuthOld(cell: PaymentTableViewCell)
    {
        self.isEditInSalesAuto = true
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if(dictMasters.value(forKey: "ElectronicAuthorizationFormMaster") is NSDictionary)
        {
            
//            let storyboardIpad = UIStoryboard.init(name: "ElectronicAuthorization_iPhone", bundle: nil)
//            let testController = storyboardIpad.instantiateViewController(withIdentifier: "ElectronicAuthorization_iPhone") as! ElectronicAuthorization
            let storyboard = UIStoryboard(name: "ElectronicAuthorization", bundle: nil)
            guard let testController = storyboard.instantiateViewController(withIdentifier: "ElectronicAuthorizationViewController") as? ElectronicAuthorizationViewController else { return }
//            destination.delegate = self
//            destination.isEditable = isEditable
//            destination.formDetail = electronicAuthorizationForm
//            destination.propertyDetails = propertyDetails
//            destination.entityDetail = entityDetail
//            self.navigationController?.pushViewController(destination, animated: true)
            // testController.strLeadId = strLeadId
            
            if(self.strGlobalStatusSysName.lowercased() == "complete" && self.strGlobalStageSysName.lowercased() == "won")
            {
                if(isInternetAvailable() == false)
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_EAF, viewcontrol: self)
                }
                else
                {
                    self.navigationController?.pushViewController(testController, animated: false)
                }
            }
            else
            {
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
        }
    }
    
    
    //.......................
    func callBackIAgree(cell: SignatureTableViewCell) {
        self.isEditInSalesAuto = true
        cell.btnIAgree.isSelected = !cell.btnIAgree.isSelected
        
        self.isIAgreeTAndC = cell.btnIAgree.isSelected
    }
    
    func callBackCustomerNotPresent(cell: SignatureTableViewCell) {
        self.isEditInSalesAuto = true
        let indexPath1 = reviewTableView.indexPath(for: cell)
        cell.btnIAgree.isSelected = false
        self.isIAgreeTAndC = false
        
        cell.btnCustomerNot.isSelected = !cell.btnCustomerNot.isSelected
        self.isCustomerNotPresent = cell.btnCustomerNot.isSelected

        if cell.btnCustomerNot.isSelected {
            cell.custSigHeight.constant =  0
            cell.tandC_height.constant =  80

            cell.lblCustSigTitle.isHidden = true
            cell.btnAllowCustomer.isHidden = false
            cell.lblAllowCustomer.isHidden = false

        }else {
            cell.tandC_height.constant =  120
            cell.custSigHeight.constant =  250
            cell.lblCustSigTitle.isHidden = false
            cell.btnAllowCustomer.isHidden = true
            cell.lblAllowCustomer.isHidden = true
        }
        
        
        if isCustomerNotPresent {
            matchesGeneralInfo.setValue("true", forKey: "isCustomerNotPresent")
        }else {
            matchesGeneralInfo.setValue("false", forKey: "isCustomerNotPresent")
        }


        let context = getContext()
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        manageAllowCustomerToSelection()
        
       // let indexP = IndexPath(row: Int(indexPath1?.row ?? 0), section: Int(indexPath1?.section ?? 0))
        self.reviewTableView.reloadData()//reloadRows(at: [indexP], with: .none)
    }
    
    func callBackAllowCustomer(cell: SignatureTableViewCell) {

        self.isEditInSalesAuto = true
        //let indexPath = tblView.indexPath(for: cell)
        cell.btnAllowCustomer.isSelected = !cell.btnAllowCustomer.isSelected

        if cell.btnAllowCustomer.isSelected {
            self.strIsAllowCustomer = "true"
        }else {
            self.strIsAllowCustomer = "false"
        }
    }
    
    func callBackTermAndCondition(cell: SignatureTableViewCell) {
        self.isEditInSalesAuto = true
        let arrSoldStandardService = self.arrStandardServiceReview
        print(arrSoldStandardService)
        var arrSoldServiceNameNew: [String] = []
        var arrSoldTerms: [String] = []

        if arrSoldStandardService.count > 0 {
            for obj in arrSoldStandardService {
                print(obj.value(forKey: "serviceSysName"))
                let sysName: String = obj.value(forKey: "serviceSysName") as! String
                let soldTerms: String = obj.value(forKey: "serviceTermsConditions") as! String
                arrSoldServiceNameNew.append(sysName)
                arrSoldTerms.append(soldTerms)
            }
        }
        print(arrSoldTerms)
        
        var arrNonStanServiceName: [String] = []
        var arrNonStandardTermsCondition: [String] = []

        if self.arrCustomServiceReview.count > 0 {
            for obj in self.arrCustomServiceReview {
                
                let sysName: String = obj.value(forKey: "serviceName") as! String
                let soldTerms: String = obj.value(forKey: "nonStdServiceTermsConditions") as! String
                arrNonStanServiceName.append(sysName)
                arrNonStandardTermsCondition.append(soldTerms)
            }
        }
        print(arrNonStandardTermsCondition)
        
        
        let vc = UIStoryboard.init(name: "SalesMainSelectService", bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesTermsConditionVC") as? SalesTermsConditionVC
       // vc?.strFrom = "Residential"
        vc?.arrGeneralTermsConditionName = arrayTermsAndConditionsMultipleSelected
        vc?.arrStandardServiceTermsConditionName = NSMutableArray(array: arrSoldServiceNameNew)
        vc?.arrStandardServiceTermsConditionDesc = NSMutableArray(array: arrSoldTerms)
        vc?.arrNonStandardServiceTermsConditionName = NSMutableArray(array: arrNonStanServiceName)
        vc?.arrNonStandardServiceTermsConditionDesc = NSMutableArray(array: arrNonStandardTermsCondition)
        vc?.modalPresentationStyle = .fullScreen
        
        self.present(vc!, animated: true)
    }
    
    func callBackCustomerSignature(cell: SignatureTableViewCell) {
        self.isEditInSalesAuto = true
        let defaults = UserDefaults.standard
        defaults.set("fromCustomer", forKey: "signatureType")
        defaults.synchronize()
        //SignViewControlleriPad
        self.isCustomer = true
        
        if DeviceType.IS_IPAD {
            let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
            vc?.strType = "customer"
            self.present(vc!, animated: true)
        }else {
            let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
            vc?.strType = "customer"
            self.present(vc!, animated: true)
        }
        
    }
    
    func callBackTechnationSignature(cell: SignatureTableViewCell) {
        self.isEditInSalesAuto = true
        let defaults = UserDefaults.standard
        defaults.set("fromInspector", forKey: "signatureType")
        defaults.synchronize()
        
        self.isCustomer = true
        if DeviceType.IS_IPAD {
            let vc = UIStoryboard.init(name: "SalesMainiPad", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewControlleriPad") as? SignViewControlleriPad
            vc?.strType = "inspector"
            self.present(vc!, animated: true)
        }else {
            let vc = UIStoryboard.init(name: "SalesMain", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignViewController") as? SignViewController
            vc?.strType = "inspector"
            self.present(vc!, animated: true)
        }

    }
    
    func callBackTextSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strText = "true"
        }else {
            strText = "false"
        }
    }
    
    func callBackPhoneSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strPhone = "true"
        }else {
            strPhone = "false"
        }
    }
    
    func callBackEmailSW(cell: PaymentTableViewCell, value: Bool) {
        if value {
            strEmail = "true"
        }else {
            strEmail = "false"
        }
    }
    
    
    @objc private func signatureValue(notification: NSNotification) {
        NotificationCenter.default.removeObserver(self, name: Commons.kNotificationSignature, object: nil)
        print(notification.object)
        
        if self.isCustomer {
            self.customerSig = (notification.object as! UIImage)
        }else {
            self.techSig = (notification.object as! UIImage)
        }
        
        self.reviewTableView.reloadData()
    }
    
    func openPreviewImage()
    {
        if self.isFrontImage {
            if self.arrOFImagesName.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckFrontImage()
            }
        }else {
            if self.arrOfCheckBackImage.count == 0 {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No Image Available.", viewcontrol: self)
            }else {
                self.goToImagePreviewCheckBackImage()
            }
        }
    }
    
    func openCamera(sourceDevice: UIImagePickerController.CameraDevice)
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imgPicker = UIImagePickerController()
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            imgPicker.sourceType = .camera
            imgPicker.cameraDevice = sourceDevice
            present(imgPicker, animated: true)
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Camera is not available.", viewcontrol: self)
        }
    }

    func openGallary()
    {
        let imgPicker = UIImagePickerController()
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true)
    }
    
    
    func goToImagePreviewCheckFrontImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckFrontImageDelete")
        defs.set(false, forKey: "forCheckBackImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOFImagesName as NSArray).mutableCopy() as! NSMutableArray)//(self.arrOFImagesName as! NSMutableArray)
        objByProductVC?.indexOfImage = "0"
        
        if strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        objByProductVC?.strForCheckImage = "CheckImages"
        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
    
    func goToImagePreviewCheckBackImage() {
        let defs = UserDefaults.standard
        defs.set(true, forKey: "forCheckBackImageDelete")
        defs.set(false, forKey: "forCheckFrontImageDelete")
        defs.synchronize()

        let mainStoryboard = UIStoryboard(
            name: "SalesMain",
            bundle: nil)
        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "ImagePreviewSalesAuto") as? ImagePreviewSalesAuto
        objByProductVC?.arrOfImages = ((self.arrOfCheckBackImage as NSArray).mutableCopy() as! NSMutableArray)//arrOfCheckBackImage
        objByProductVC?.indexOfImage = "0"
        if strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame
        {
            objByProductVC?.statusOfWorkOrder = "Complete"
        }
        else
        {
            objByProductVC?.statusOfWorkOrder = "InComplete"
        }
        objByProductVC?.strForCheckImage = "CheckImages"

        if let objByProductVC = objByProductVC {
            navigationController?.present(objByProductVC, animated: true)
        }
    }
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        print(dictData)
        
        if tag == 3335 {
            
            if dictData["AddLeadProspect"] != nil
            {
                self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
                print(self.selectecPaymentType)
                if self.selectecPaymentType == "Cash" {
                    self.strPaymentMode = "Cash"
                }else if self.selectecPaymentType == "Check" {
                    self.strPaymentMode = "Check"
                }else if self.selectecPaymentType == "Credit Card" {
                    self.strPaymentMode = "CreditCard"
                }else if self.selectecPaymentType == "Auto Charge Customer" {
                    self.strPaymentMode = "AutoChargeCustomer"
                }else if self.selectecPaymentType == "Collect at time of Scheduling" {
                    self.strPaymentMode = "CollectattimeofScheduling"
                }else if self.selectecPaymentType == "Invoice" {
                    self.strPaymentMode = "Invoice"
                }else {
                    self.strPaymentMode = ""
                }
            }
            else
            {
                print("\(dictData)")
                dictSelectedCustomPaymentMode = dictData
                self.selectecPaymentType = dictData.value(forKey: "Title") as! String
                self.strPaymentMode = "\(dictData.value(forKey: "SysName") ?? "")"
            }
            
            /*
             self.selectecPaymentType = dictData.value(forKey: "AddLeadProspect") as! String
             print(self.selectecPaymentType)
             
             if self.selectecPaymentType == "Cash" {
                self.strPaymentMode = "Cash"
            }else if self.selectecPaymentType == "Check" {
                self.strPaymentMode = "Check"
            }else if self.selectecPaymentType == "Credit Card" {
                self.strPaymentMode = "CreditCard"
            }else if self.selectecPaymentType == "Auto Charge Customer" {
                self.strPaymentMode = "AutoChargeCustomer"
            }else if self.selectecPaymentType == "Collect at time of Scheduling" {
                self.strPaymentMode = "CollectattimeofScheduling"
            }else if self.selectecPaymentType == "Invoice" {
                self.strPaymentMode = "Invoice"
            }else {
                self.strPaymentMode = ""
            }*/
            
            self.objPaymentInfo?.setValue(self.strPaymentMode, forKey: "paymentMode")
            let context = getContext()
            //save the object
            do { try context.save()
                print("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
            self.reviewTableView.reloadData()
        }else if (tag == 105) { // terms and conditions
            
            arrayTermsAndConditionsMultipleSelected = (dictData.value(forKey: "multi") as! NSArray).mutableCopy() as! NSMutableArray
            
            var termsTitle = ""
            
            for item in arrayTermsAndConditionsMultipleSelected
            {
                termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "TermsTitle")!)" + ","
            }
            if(arrayTermsAndConditionsMultipleSelected.count > 0)
            {
                termsTitle.removeLast()
                strLblTermsCondition = termsTitle
            }
            else
            {
                strLblTermsCondition = "---Select---"
            }
        
            
            self.reviewTableView.reloadData()

        }
        
    }

}

extension NewCommericialReviewViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        let dateFormatterD = DateFormatter()
        dateFormatterD.dateFormat = "MMddyyyy"
        let strDate = dateFormatterD.string(from: Date())
        
        let dateFormatterT = DateFormatter()
        dateFormatterT.dateFormat = "HHmmss"
        let strTime = dateFormatterT.string(from: Date())

        let strImageNamess = "CheckImg\(strDate)\(strTime).jpg"
        
        
        if self.isFrontImage {
           self.checkFrontImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOFImagesName.append(strImageNamess)
        }else {
            self.checkBackImg = self.resizeImage(image: image, imgName: strImageNamess)
            self.arrOfCheckBackImage.append(strImageNamess)
        }
        // print out the image size as a test
        print(image.size)
    }
    
    func resizeImage(image: UIImage, imgName: String) -> UIImage {
        
        if let imageData = image.jpeg(.lowest) {
            let filename = getDocumentsDirectory().appendingPathComponent(imgName)
            do {
                try imageData.write(to: filename, options: [])
            } catch {
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
            
            return UIImage.init(data: imageData)!
        }else {
            return UIImage()
        }
    }
}
