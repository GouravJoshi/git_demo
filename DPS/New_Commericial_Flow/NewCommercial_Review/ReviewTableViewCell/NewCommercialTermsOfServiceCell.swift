//
//  TermsOfServiceCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 27/10/21.
//

import UIKit
import WebKit

class NewCommercialTermsOfServiceCell: UITableViewCell,UIWebViewDelegate, WKNavigationDelegate ,WKUIDelegate {
    
    @IBOutlet weak var heightOfTermsOfServices: NSLayoutConstraint!
    @IBOutlet weak var webViewTermsOfService: WKWebView!
    
    var tblTermsOfServices = UITableView()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        webViewTermsOfService.uiDelegate = self
        webViewTermsOfService.navigationDelegate = self
         
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.frame.size.height = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if let constraint = (webView.constraints.filter{$0.firstAttribute == .height}.first) {
                constraint.constant = webView.scrollView.contentSize.height
            }
        }
    }

}
