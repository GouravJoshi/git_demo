//
//  IntroductionLetterCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 27/10/21.
//

import UIKit
import WebKit

class NewCommercialIntroductionLetterCell: UITableViewCell , UIWebViewDelegate, WKNavigationDelegate ,WKUIDelegate{
    
    
    //MARK -- Outlets
    @IBOutlet weak var webViewIntrodutionLetter: WKWebView!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
   
    
    //MARK ---
    
    var tblIntroduction = UITableView()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        webViewIntrodutionLetter.uiDelegate = self
        webViewIntrodutionLetter.navigationDelegate = self
          
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.frame.size.height = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if let constraint = (webView.constraints.filter{$0.firstAttribute == .height}.first) {
                constraint.constant = webView.scrollView.contentSize.height
            }
        }
    }
     
}
