//
//  NewCommericialImagePreviewCell.swift
//  DPS
//
//  Created by Saavan Patidar on 30/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialImagePreviewCell: UITableViewCell {
    
    
    @IBOutlet var imagePreview: UIImageView!
    @IBOutlet var lblCaption: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
   

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }

}

