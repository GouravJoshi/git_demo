//
//  PricingCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommercialPricingCell: UITableViewCell {
    
    //MARK:- Outlet
    
    @IBOutlet weak var lblSubTotalInitial       : UILabel!
    @IBOutlet weak var lblCouponDiscount        : UILabel!
    @IBOutlet weak var lblCredit                : UILabel!
    @IBOutlet weak var lblOtherDiscount         : UILabel!
    @IBOutlet weak var lblTipDiscount         : UILabel!
    @IBOutlet weak var lblLeedInspectionFee         : UILabel!
    @IBOutlet weak var lblTotalPrice            : UILabel!
    @IBOutlet weak var lblTaxAmount             : UILabel!
    @IBOutlet weak var lblBillingAmount         : UILabel!
    @IBOutlet weak var lblCreditMaint: UILabel!
    
    
    @IBOutlet weak var lblSubTotalMaintenance   : UILabel!
    @IBOutlet weak var lblTotalPriceMaintenance : UILabel!
    @IBOutlet weak var lblTaxAmountMaintenance  : UILabel!
    @IBOutlet weak var lblTotalDueAmount        : UILabel!
    
    @IBOutlet weak var couponDiscountTitle: UILabel!
    @IBOutlet weak var creditDiscountTitle: UILabel!
    @IBOutlet weak var creditTopSpace: NSLayoutConstraint!
    @IBOutlet weak var otherDisTopSpace: NSLayoutConstraint!
    

    @IBOutlet weak var tblMaintFrqHeight: NSLayoutConstraint!
    @IBOutlet weak var tblMaintFrq: UITableView!
    var arrMaintBillingPrice  : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableMaintSetUp (arrMaintBillingPrice: [String]) {
        //Credit
        if arrMaintBillingPrice.count > 0{
            self.arrMaintBillingPrice.removeAll()
            
            self.tblMaintFrq.delegate = self
            self.tblMaintFrq.dataSource = self
            self.arrMaintBillingPrice = arrMaintBillingPrice
            self.tblMaintFrq.reloadData()
            
            self.tblMaintFrqHeight.constant = CGFloat(arrMaintBillingPrice.count * 25)
        }
    }

}

//MARK:- TablView Delegate for Pricing
extension NewCommercialPricingCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMaintBillingPrice.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath as IndexPath) as! ConfigCouponTableViewCell
        
        cell.lblMaintPrice.text = self.arrMaintBillingPrice[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 25
    }
    
}
