//
//  NewCommercialReviewGraphCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommercialReviewGraphCell: UITableViewCell {
    
    //MARK- Outlets
    
    @IBOutlet var imagePreview: UIImageView!
    @IBOutlet var lblCaption: UILabel!
    @IBOutlet var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
