//
//  AddTargetImageCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 29/10/21.
//

import UIKit

class AddTargetImageCell: UITableViewCell {
    
    @IBOutlet weak var tblAddTarget: UITableView!
    @IBOutlet var lblTarget: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var heightOfTblAddTarget: NSLayoutConstraint!
    var heightForAddTargetImages = CGFloat()
    
    //MARK:- varibales
    
    var arrayOfImages = NSArray ()
    @objc var strLeadId = NSString()
    @objc var strHeaderTitle = NSString()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tblAddTarget.delegate = self
        self.tblAddTarget.dataSource = self
        self.tblAddTarget.tableFooterView = UIView()
        self.tblAddTarget.sizeToFit()
          
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension AddTargetImageCell : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTargetImageTableViewCell", for: indexPath) as? AddTargetImageTableViewCell
        let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
        
        // Caption Attributed String :
        
        let captionString = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "leadImageCaption") as! String) : "Caption :"
        
        let stringToChange = "Caption :"
        
        let range = (captionString as NSString).range(of: stringToChange)

        let mutableAttributedString = NSMutableAttributedString.init(string: captionString)
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: range)
        
        if DeviceType.IS_IPAD {
            mutableAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: range)
        }else{
            mutableAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 12), range: range)
        }
        
        cell?.lblName.attributedText = mutableAttributedString
        
        // Description Attributed String :
        
        let descriptionString = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : "Description :"
        
        let stringToDescription = "Description :"
        
        let rangeToDescription = (descriptionString as NSString).range(of: stringToDescription)

        let mutableDescriptionAttributedString = NSMutableAttributedString.init(string: descriptionString)
        mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: rangeToDescription)
        
        if DeviceType.IS_IPAD {
            mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: range)
        }else{
            mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 12), range: range)
        }
        
        cell?.lblDescription.attributedText = mutableDescriptionAttributedString

        let strImagePath = objTemp.value(forKey: "leadImagePath") as! String

        let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)

        let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)

        if isImageExists!  {

            cell?.imageTargert.image = image


        }else {

            let defsLogindDetail = UserDefaults.standard

            let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary

            var strURL = String()

            if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
            {

                strURL = "\(value)"

            }
            //strURL = strURL + "\(strImagePath)"

            strURL = strURL + "Documents/Targetimages/" + "\(strImagePath)"

            strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

            let image: UIImage = UIImage(named: "NoImage.jpg")!

            cell?.imageTargert.image = image

            DispatchQueue.global(qos: .background).async {

                let url = URL(string:strURL)
                let data = try? Data(contentsOf: url!)

                if data != nil && (data?.count)! > 0 {

                    let image: UIImage = UIImage(data: data!)!

                    DispatchQueue.main.async {

                        saveImageDocumentDirectory(strFileName: strImagePath , image: image)

                        cell?.imageTargert.image = image


                    }}

            }
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if tableView.visibleCells.contains(cell) {
                self.heightOfTblAddTarget?.constant = self.tblAddTarget.contentSize.height
            }
        }
        
    }
    
}


//MARK:- create tableview cell
class AddTargetImageTableViewCell : UITableViewCell {
    
    @IBOutlet weak var imageTargert : UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
}
