//
//  AddStandardServiceCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 01/11/21.
//

import UIKit

class AddStandardServiceCell: UITableViewCell {
    
    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceDescription: UILabel!
    
    @IBOutlet var lblFinalIntialPrice: UILabel!
    @IBOutlet var lblMaintenancePrice: UILabel!
 
    
    @IBOutlet var lblFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
   
    
    @IBOutlet var viewForDescription: UIView!

    @IBOutlet var viewForHeightDescription: NSLayoutConstraint!
    
    @IBOutlet var lblStrMaintenancePrice: UILabel!
    
    @IBOutlet var viewForBottom: NSLayoutConstraint!
    
    @IBOutlet var lblStrheightForMaintenancePrice: NSLayoutConstraint!
    @IBOutlet var lblHeightForMaintenancePrice: NSLayoutConstraint!
    
    @IBOutlet var lblRenewalContent: UILabel!
    @IBOutlet var lblRenewalSepartor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
