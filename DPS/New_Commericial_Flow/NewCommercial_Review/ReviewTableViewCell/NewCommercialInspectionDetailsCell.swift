//
//  InspectionDetailsCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 27/10/21.
//

import UIKit

class NewCommercialInspectionDetailsCell: UITableViewCell {
    
    @objc var matchesGeneralInfo = NSManagedObject()
    @IBOutlet weak var heightDynemicView: NSLayoutConstraint!
    @IBOutlet weak var viewDynemicView: UIView!
    var aryDynemicForm = NSMutableArray()
    
    @objc var strLeadId = NSString()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - -----------loadDynamicForm  OR SAVE
    func loadDynamicForm() {
        
        // Fetch From Core Data if Data Exists
        print("11111")
        let arrayDynamicInspection = getDataFromLocal(strEntity: "SalesDynamicInspection", predicate: NSPredicate(format: "leadId == %@ && userName == %@", strLeadId, "\(Global().getUserName()!)"))
        
        print("11111")
        print(arrayDynamicInspection.count)
        
        if(arrayDynamicInspection.count > 0)
        {
            
            let objSalesInspectionData = arrayDynamicInspection[0] as! NSManagedObject
            if objSalesInspectionData.value(forKey: "arrFinalInspection") is NSArray {
                var aryDynemicFormTemp = NSMutableArray()
                aryDynemicFormTemp = (objSalesInspectionData.value(forKey: "arrFinalInspection") as! NSArray).mutableCopy()as! NSMutableArray
                
                var isSameFlowType = true
                
                for (index, element) in aryDynemicFormTemp.enumerated() {
                    
                    //  print("Item \(index): \(element)")
                    var dict = NSMutableDictionary()
                    dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                    dict.setValue(false, forKey: "viewExpand")
                    dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                    aryDynemicFormTemp.replaceObject(at: index, with: dict)
                    
                    let flowTypeMaster = "\(dict.value(forKey: "FlowType") ?? "")"
                    
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    
                    if (flowTypeMaster != flowTypeOpportunity) && !(flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame) {
                        
                        isSameFlowType = false
                        
                    }
                    
                }
                
                // Change For To check if commercial or residentail flow saved and which flow type is of lead if it is changed then need to delete saved dynamic form and re enter new dynamic form.
                
                if isSameFlowType {
                    var arrOfFilteredDataTemp = [NSDictionary] ()
                    arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
                    let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                        (($0 )["SequenceNo"] as? CGFloat)! > (($1 )["SequenceNo"] as? CGFloat)!
                    })
                    self.aryDynemicForm = NSMutableArray()
                    self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
                    CreatDynemicView(aryData: aryDynemicForm)
                    
                } else {
                    
                    // Get Dynamic Form From Masters and create Dynamic View
                    
                    self.fetchDynamicFormFromMasterAndCreateDynamicView()
                    
                }
                
                
            }
        }else{
            
            //  If Data not exist then check Status and Stage and then fetch From Masters
            
            if "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")".caseInsensitiveCompare("Complete") == .orderedSame && "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")".caseInsensitiveCompare("Won") == .orderedSame{
                
                // Create blank No Dynamic Form Exist
                
            }else{
                
                // Get Dynamic Form From Masters and create Dynamic View
                
                //self.fetchDynamicFormFromMasterAndCreateDynamicView()
                
            }
        }
    }
    
    //MARK: --------------------------
    //MARK: - CreatDynemicView
    func CreatDynemicView(aryData : NSMutableArray) {
        
        
        
        for v in viewDynemicView.subviews{
            v.removeFromSuperview()
        }
        var yAxis = 0.0 , extraSpace = DeviceType.IS_IPAD ? 20.0 : 12.0
        let height = DeviceType.IS_IPAD ? 55.0 : 40.0
        let heighttextView = DeviceType.IS_IPAD ? 135.0 : 100.0
        
        if(aryData.count != 0){
            for (headerIndex, item) in aryData.enumerated() {
                print("headerIndex---------------\(headerIndex)")
                
                var dictMAIN = NSMutableDictionary()
                dictMAIN = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                var isHeader = false
                
                
                //MARK:------------Start Section View ---------
                //                if dictMAIN.value(forKey: "viewExpand") as! Bool {
                let arySECTION = dictMAIN.value(forKey: "Sections")as! NSArray
                for (sectionIndex, item) in arySECTION.enumerated() {
                    print("sectionIndex---------------\(sectionIndex)")
                    
                    
                    let dictSectionData = (item as AnyObject) as! NSDictionary
                    
                    var isSection = false
                    //MARK:-Section Control
                    let arySECTIONControls = dictSectionData.value(forKey: "Controls")as! NSArray
                    for (controlIndex, item) in arySECTIONControls.enumerated() {
                        let dictSectionControl = (item as AnyObject) as! NSDictionary
                        print("COntrolINDEX---------------\(controlIndex)")
                        
                        //Check Value exist or not
                        if "\(dictSectionControl.value(forKey: "Value") ?? "")" != "" {
                            print("VVVVVVVVVVAAAA: \(dictSectionControl.value(forKey: "Value") ?? "")")
                            
                            if isHeader == false {
                                isHeader = true
                                //MARK:-------------Start Header View ---------
                                let viewHeader = UIView.init(frame: CGRect(x: 10, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 20, height: Int(height)))
                                //viewHeader.backgroundColor = .black
                                viewHeader.backgroundColor = SalesNewColorCode.darkBlack
                                let lblHeaderTitle = UILabel.init(frame: CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8))
                                lblHeaderTitle.textAlignment = .center
                                lblHeaderTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                
                                var strTitle = "\(dictMAIN.value(forKey: "FormName") ?? "")"
                                
                                if strTitle.count == 0 || strTitle == ""
                                {
                                    strTitle = "\(dictMAIN.value(forKey: "DepartmentName") ?? "")"
                                }
                                
                                lblHeaderTitle.text = strTitle//"\(dictMAIN.value(forKey: "DepartmentName")!)"
                                lblHeaderTitle.numberOfLines = 2
                                lblHeaderTitle.textColor = UIColor.white
                                viewHeader.addSubview(lblHeaderTitle)
                                self.viewDynemicView.addSubview(viewHeader)
                                yAxis = yAxis + height + extraSpace
                                //MARK:------------End Header View ---------
                            }
                            
                            if isSection == false {
                                isSection = true
                                //MARK:-------------Start Section View ---------
                                let title =  "\(dictSectionData.value(forKey: "Name")!)"
                                let viewSection = UIView.init(frame: CGRect(x: 10, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 20, height: Int(height)))
                                viewSection.backgroundColor = .lightGray
                                let lblSectionTitle = UILabel()
                                lblSectionTitle.frame = CGRect(x: 40, y: 4, width: Int(self.viewDynemicView.frame.width) - 80 , height: Int(height) - 8)
                                
                                lblSectionTitle.textAlignment = .center
                                lblSectionTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionTitle.text = title
                                lblSectionTitle.numberOfLines = 1
                                lblSectionTitle.textColor = UIColor.white
                                viewSection.addSubview(lblSectionTitle)
                                self.viewDynemicView.addSubview(viewSection)
                                yAxis = yAxis + height + extraSpace
                                //MARK:------------End Section View ---------
                            }
                            
                            
                            
                            
                            //MARK:---------Checkbox || Checkbox_combo
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.checkbox_combo){
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                let values = "\(dictSectionControl.value(forKey: "Value")!)"
                                print(values)
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                for (subControlIndex, item) in aryOption.enumerated() {
                                    
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                    let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                    
                                    if(values.contains(strValue)){
                                        let lblTitle = UILabel.init(frame: CGRect(x: Int(20 + 10), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)
                                        ))
                                        lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                        lblTitle.text = "\(strLabel)"
                                        lblTitle.numberOfLines = 0
                                        lblTitle.textColor = UIColor.darkGray
                                        self.viewDynemicView.addSubview(lblTitle)
                                        yAxis = yAxis + (DeviceType.IS_IPAD ? 40.0 : 30.0) + extraSpace
                                        
                                    }else{
                                        
                                    }
                                }
                            }
                            
                            //MARK:---------Radio || Radio_combo
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.radio_combo){
                                
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let values = "\(dictSectionControl.value(forKey: "Value")!)"
                                
                                for (subControlIndex, item) in aryOption.enumerated() {
                                    let optionDict = (item as AnyObject) as! NSDictionary
                                    let strLabel = "\(optionDict.value(forKey: "Label") ?? "")"
                                    let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                    
                                    //                                        let btnRadioBox = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(DeviceType.IS_IPAD ? 40.0 : 30.0), height: Int(DeviceType.IS_IPAD ? 40.0 : 30.0)))
                                    if(values.lowercased() == strValue.lowercased()){
                                        let lblTitle = UILabel.init(frame: CGRect(x: Int(30), y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(25)))
                                        lblTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                        lblTitle.text = "\(strLabel)"
                                        lblTitle.numberOfLines = 0
                                        lblTitle.textColor = UIColor.darkGray
                                        self.viewDynemicView.addSubview(lblTitle)
                                        yAxis = yAxis + (DeviceType.IS_IPAD ? 40.0 : 30.0) + extraSpace
                                    }else{
                                    }
                                    
                                }
                                
                            }
                            
                            //MARK:---------TextView || Paragraph
                            //el-textarea
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textarea) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.paragraph) || ("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.parag)
                            {
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = Value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis - 10), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /* }else{
                                 let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                 let lblSectionDetailTitle = UILabel()
                                 lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 lblSectionDetailTitle.text = title
                                 let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                 lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                 
                                 lblSectionDetailTitle.numberOfLines = 0
                                 lblSectionDetailTitle.textColor = UIColor.darkGray
                                 self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                 yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                 
                                 let textarea = UITextView.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(heighttextView)))
                                 textarea.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                 textarea.textColor = UIColor.darkGray
                                 textarea.layer.cornerRadius = 8.0
                                 textarea.layer.borderWidth = 1.0
                                 textarea.layer.borderColor = UIColor.lightGray.cgColor
                                 //textarea.delegate = self
                                 
                                 textarea.tag = headerIndex*1000+sectionIndex
                                 textarea.placeholderLabel.tag = controlIndex*1000+0
                                 
                                 self.viewDynemicView.addSubview(textarea)
                                 yAxis = yAxis + heighttextView + extraSpace
                                 }*/
                            }
                            
                            //MARK:---------UITextField
                            //el-textbox,el-email,el-phone,el-currency,el-decimal,el-number,el-auto-num,el-precent,el-upload
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.textbox || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.email || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.phone || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.currency || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.decimal || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.number || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.autonum || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.precent ){
                                
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18: 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                //                                    }else{
                                //                                        let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                //                                        textarea?.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                //                                        textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                //                                        textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                //                                        textarea!.textColor = UIColor.darkGray
                                //                                        textarea!.lineColor = UIColor.lightGray
                                //                                        //textarea!.delegate = self
                                //
                                //                                        textarea!.tag = headerIndex*1000+sectionIndex
                                //                                        let lftview = UILabel()
                                //                                        lftview.tag = controlIndex*1000+0
                                //                                        textarea!.leftView = lftview
                                //
                                //                                        switch "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() {
                                //                                        case DynemicFormCheck.email:
                                //                                            textarea!.keyboardType = .emailAddress
                                //                                        case DynemicFormCheck.phone:
                                //                                            textarea!.text = formattedNumber(number: textarea!.text!)
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.currency:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.decimal:
                                //                                            textarea!.keyboardType = .decimalPad
                                //                                        case DynemicFormCheck.number:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.autonum:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        case DynemicFormCheck.precent:
                                //                                            textarea!.keyboardType = .phonePad
                                //                                        default:
                                //                                            textarea!.keyboardType = .default
                                //                                        }
                                //                                        self.viewDynemicView.addSubview(textarea!)
                                //                                        yAxis = yAxis + height + extraSpace
                                //                                    }
                                
                                
                            }
                            
                            //MARK:---------Datetime
                            //el-date , el-date-time
                            
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date || "\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date_time){
                                
                                //     if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let Value =  "\(dictSectionControl.value(forKey: "Value")!)"
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = Value
                                let heightCalculatelblValue = Value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /* }else{
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.text = "\(dictSectionControl.value(forKey: "Value")!)"
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btnDate = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 self.viewDynemicView.addSubview(btnDate)
                                 btnDate.setImage(UIImage(named: "Calendar"), for: .normal)
                                 btnDate.contentHorizontalAlignment = .right
                                 btnDate.imageView?.contentMode = .scaleAspectFit
                                 
                                 btnDate.tag = headerIndex*1000+sectionIndex
                                 btnDate.titleLabel?.tag = controlIndex*1000+0
                                 
                                 if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.date){
                                 //                                        btnDate.addTarget(self, action: #selector(didTap_btnDate(sender:)), for: .touchUpInside)
                                 }else{
                                 //                                        btnDate.addTarget(self, action: #selector(didTap_btnDateTime(sender:)), for: .touchUpInside)
                                 }
                                 yAxis = yAxis + height + extraSpace
                                 
                                 }*/
                            }
                            //MARK:---------URL
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.url){
                                
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                
                                let btnUrl = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                btnUrl.titleLabel?.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                //                                btnUrl.addTarget(self, action: #selector(didTap_btnURL(sender:)), for: .touchUpInside)
                                
                                btnUrl.tag = headerIndex*1000+sectionIndex
                                btnUrl.titleLabel?.tag = controlIndex*1000+0
                                
                                //                                let attributeString = NSMutableAttributedString(
                                //                                    string: "\(dictSectionControl.value(forKey: "Value")!)",
                                //                                    attributes: yourAttributes
                                //                                )
                                //                                btnUrl.setAttributedTitle(attributeString, for: .normal)
                                self.viewDynemicView.addSubview(btnUrl)
                                yAxis = yAxis + 25.0 + extraSpace
                                
                            }
                            //MARK:---------Dropdown
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.dropdown){
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                                
                                for (_, item1) in aryValues.enumerated() {
                                    let strOptionvalue = "\(item1 as AnyObject)"
                                    for (_, item) in aryOption.enumerated() {
                                        let optionDict = (item as AnyObject) as! NSDictionary
                                        let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                        if(strOptionvalue.lowercased() == strValue.lowercased()){
                                            aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                        }
                                    }
                                }
                                
                                let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = value
                                let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                /*  }else{
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                 let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                 let aryTempSlectedOption = NSMutableArray()
                                 
                                 for (_, item1) in aryValues.enumerated() {
                                 let strOptionvalue = "\(item1 as AnyObject)"
                                 for (_, item) in aryOption.enumerated() {
                                 let optionDict = (item as AnyObject) as! NSDictionary
                                 let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                 if(strOptionvalue.lowercased() == strValue.lowercased()){
                                 aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                 }
                                 }
                                 
                                 }
                                 
                                 textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 btn.setImage(UIImage(named: "arrow_2"), for: .normal)
                                 btn.contentHorizontalAlignment = .right
                                 btn.imageView?.contentMode = .scaleAspectFit
                                 btn.tag = headerIndex*1000+sectionIndex
                                 btn.titleLabel?.tag = controlIndex*1000+0
                                 //                                btn.addTarget(self, action: #selector(didTap_btnDropDown(sender:)), for: .touchUpInside)
                                 self.viewDynemicView.addSubview(btn)
                                 yAxis = yAxis + height + extraSpace
                                 }*/
                            }
                            //MARK:---------Multi_select
                            if("\(dictSectionControl.value(forKey: "Element") ?? "")".lowercased() == DynemicFormCheck.multi_select){
                                
                                //                                    if getStatusCompleteWon() {
                                let title =  "\(dictSectionControl.value(forKey: "Label")!)"
                                let lblSectionDetailTitle = UILabel()
                                lblSectionDetailTitle.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                lblSectionDetailTitle.text = title
                                let heightCalculate = title.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16))
                                lblSectionDetailTitle.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculate)
                                
                                lblSectionDetailTitle.numberOfLines = 0
                                lblSectionDetailTitle.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblSectionDetailTitle)
                                yAxis = yAxis + Double(lblSectionDetailTitle.frame.height) + extraSpace
                                
                                let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                let aryTempSlectedOption = NSMutableArray()
                                
                                for (_, item1) in aryValues.enumerated() {
                                    let strOptionvalue = "\(item1 as AnyObject)"
                                    for (_, item) in aryOption.enumerated() {
                                        let optionDict = (item as AnyObject) as! NSDictionary
                                        let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                        if(strOptionvalue.lowercased() == strValue.lowercased()){
                                            aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                        }
                                    }
                                }
                                
                                let value = aryTempSlectedOption.componentsJoined(by: ",").replacingOccurrences(of: ",", with: "\n")
                                let lblValue = UILabel()
                                lblValue.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14)
                                lblValue.text = value
                                let heightCalculatelblValue = value.height(withConstrainedWidth: viewDynemicView.frame.width, font: UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 18 : 14))
                                lblValue.frame = CGRect(x: 20.0, y: CGFloat(yAxis), width: (self.viewDynemicView.frame.width) - 40.0, height: heightCalculatelblValue)
                                lblValue.numberOfLines = 0
                                lblValue.textColor = UIColor.darkGray
                                self.viewDynemicView.addSubview(lblValue)
                                yAxis = yAxis + Double(lblValue.frame.height) + extraSpace
                                
                                
                                
                                /* }else{
                                 
                                 let textarea = ACFloatingTextField.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 textarea!.font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 20 : 16)
                                 textarea!.placeholder = "\(dictSectionControl.value(forKey: "Label")!)"
                                 textarea!.textColor = UIColor.darkGray
                                 textarea!.lineColor = UIColor.lightGray
                                 textarea!.isUserInteractionEnabled = false
                                 
                                 let aryOption = dictSectionControl.value(forKey: "Options") as! NSArray
                                 let aryValues = "\(dictSectionControl.value(forKey: "Value")!)".split(separator: ",") as NSArray
                                 let aryTempSlectedOption = NSMutableArray()
                                 
                                 for (_, item1) in aryValues.enumerated() {
                                 let strOptionvalue = "\(item1 as AnyObject)"
                                 for (_, item) in aryOption.enumerated() {
                                 let optionDict = (item as AnyObject) as! NSDictionary
                                 let strValue = "\(optionDict.value(forKey: "Value") ?? "")"
                                 if(strOptionvalue.lowercased() == strValue.lowercased()){
                                 aryTempSlectedOption.add("\(optionDict.value(forKey: "Name") ?? "")")
                                 }
                                 }
                                 
                                 }
                                 
                                 textarea?.text = aryTempSlectedOption.componentsJoined(by: ",")
                                 
                                 self.viewDynemicView.addSubview(textarea!)
                                 let btn = UIButton.init(frame: CGRect(x: 20, y: Int(yAxis), width: Int(self.viewDynemicView.frame.width) - 40 , height: Int(height)))
                                 btn.setImage(UIImage(named: "Next2"), for: .normal)
                                 btn.contentHorizontalAlignment = .right
                                 btn.imageView?.contentMode = .scaleAspectFit
                                 btn.tag = headerIndex*1000+sectionIndex
                                 btn.titleLabel?.tag = controlIndex*1000+0
                                 //                                btn.addTarget(self, action: #selector(didTap_btnDropDownMultiSelection(sender:)), for: .touchUpInside)
                                 
                                 self.viewDynemicView.addSubview(btn)
                                 
                                 yAxis = yAxis + height + extraSpace
                                 }*/
                            }
                        }
                    }
                }
                //                }
                
                //MARK:------------End Section View ---------
            }
        }
        heightDynemicView.constant = CGFloat(yAxis + 50)
    }
    
    func fetchDynamicFormFromMasterAndCreateDynamicView() {
        
        // Get Dynamic Form From Masters
        
        let arrTempInspection = NSMutableArray()
        
        if let arrayMaster = nsud.value(forKey: "MasterSalesAutomationDynamicForm") as? NSArray {
            
            if arrayMaster.isKind(of: NSArray.self) {
                
                for item in arrayMaster
                {
                    let dictDataL = item as! NSDictionary
                    
                    let branchSysNameMaster = "\(dictDataL.value(forKey: "BranchSysName") ?? "")"
                    let flowTypeMaster = "\(dictDataL.value(forKey: "FlowType") ?? "")"
                    
                    
                    let branchSysNameOpprtunity = "\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")"
                    let flowTypeOpportunity = "\(matchesGeneralInfo.value(forKey: "flowType") ?? "")"
                    
                    
                    if flowTypeOpportunity.caseInsensitiveCompare("Commercial") == .orderedSame {
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Commercial") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)
                            
                        }
                        
                    }else{
                        
                        if (branchSysNameMaster ==  branchSysNameOpprtunity) && ( flowTypeMaster.caseInsensitiveCompare("Residential") == .orderedSame ||  flowTypeMaster.caseInsensitiveCompare("All") == .orderedSame ){
                            
                            // Add All Duynamic Forms Of Commmercail FLow.
                            arrTempInspection.add(dictDataL)
                            
                        }
                        
                    }
                    
                    
                }
            }
        }
        
        if arrTempInspection.count > 0 {
            
            // If found data in Master will create Dynamic View.
            
            let aryDynemicFormTemp = NSMutableArray()
            
            aryDynemicFormTemp.addObjects(from: arrTempInspection as! [Any])
            
            for (index, element) in aryDynemicFormTemp.enumerated() {
                //  print("Item \(index): \(element)")
                var dict = NSMutableDictionary()
                dict = (((element as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary)
                dict.setValue(false, forKey: "viewExpand")
                dict.setValue("\(matchesGeneralInfo.value(forKey: "leadId") ?? "")", forKey: "LeadId")
                aryDynemicFormTemp.replaceObject(at: index, with: dict)
            }
            
            // call Func to crate dynamic Form.
            
            var arrOfFilteredDataTemp = [NSDictionary] ()
            arrOfFilteredDataTemp = aryDynemicFormTemp as! [NSDictionary]
            let  sortedArray = arrOfFilteredDataTemp.sorted(by: {
                (($0 )["SequenceNo"] as? CGFloat)! > (($1 )["SequenceNo"] as? CGFloat)!
            })
            self.aryDynemicForm = NSMutableArray()
            self.aryDynemicForm = (sortedArray as NSArray).mutableCopy()as! NSMutableArray
            CreatDynemicView(aryData: aryDynemicForm)
            
            
        }else{
            
            // No Data Found in Master So blank Screen No Dynamic View We can show alert if required.
            
            
        }
    }
    
    func getStatusCompleteWon() -> Bool {
        let strStatusss =  matchesGeneralInfo.value(forKey: "statusSysName") ?? ""
        let strStageSysName =  matchesGeneralInfo.value(forKey: "stageSysName") ?? ""
        if("\(strStatusss)".lowercased() == "complete" && "\(strStageSysName)".lowercased() == "won"){
            return true
        }else{
            return false
        }
    }


}
