//
//  AddCustomCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 01/11/21.
//

import UIKit

class AddCustomCell: UITableViewCell {
    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceDescription: UILabel!
    @IBOutlet var lblIntialPrice: UILabel!
    @IBOutlet var lblMaintenancePrice: UILabel!
    @IBOutlet var lblFrequency: UILabel!
    @IBOutlet var lblBillingFrequency: UILabel!
    @IBOutlet var viewDescription: UIView!
    @IBOutlet var heightForViewDescription: NSLayoutConstraint!
    
    @IBOutlet var lblStrMaintenancePrice: UILabel!
    
    @IBOutlet var viewForBottom: NSLayoutConstraint!
    
    @IBOutlet var lblStrheightForMaintenancePrice: NSLayoutConstraint!
    @IBOutlet var lblHeightForMaintenancePrice: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



class AddCustomTableViewCell : UITableViewCell {
    
   
   
    
}
