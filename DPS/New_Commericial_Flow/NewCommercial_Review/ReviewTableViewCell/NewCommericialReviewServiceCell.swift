//
//  NewCommericialReviewServiceCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommericialReviewServiceCell: UITableViewCell {

    
    //MARK:- outlet's
    @IBOutlet weak var tblService: UITableView!
    
    @IBOutlet var tblHeight: NSLayoutConstraint!
    //MARK:- Variables
    
    var seaction = ["Add Target" , "Add Scope" , "Standard Service" , "Custom Service"]
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        //self.tblHeight.constant = 1600
        //self.tblService.isScrollEnabled = false
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
  
    }

}

extension NewCommericialReviewServiceCell : UITableViewDataSource , UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddTargetImageCell", for: indexPath) as? AddTargetImageCell
            cell?.tblAddTarget.reloadData()
            return cell ?? UITableViewCell()
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddScopeImageCell", for: indexPath) as? AddScopeImageCell
            return cell!
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddStandardServiceCell", for: indexPath) as? AddStandardServiceCell
            return cell ?? UITableViewCell()
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCustomCell", for: indexPath) as? AddCustomCell
            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        } else if section == 1{
            return 1
        }else if section == 2{
            return 1
        }else if section == 3{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // HEADER VIEW
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:  50))
        viewheader.backgroundColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        // INSIDE LABEL
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height:50))
        // HEADER BOTTOM LINE
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 43.0, width: viewheader.frame.size.width, height: 1)
        bottomBorder.backgroundColor = UIColor.clear.cgColor
        // #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        if tableView == tblService
        {
            if(section == 0)
            {
                lbl.text = "Target"
                lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else if(section == 1)
            {
                lbl.text = "Scope"
                lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else if(section == 2)
            {
                lbl.text = "Standard Service"
                lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else if(section == 3)
            {
                lbl.text = "Custom Service"
                lbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            lbl.font = UIFont.boldSystemFont(ofSize: 18)
            viewheader.addSubview(lbl)
            viewheader.layer.addSublayer(bottomBorder)
            return viewheader
        }
        else
        {
            return viewheader
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
     
}
