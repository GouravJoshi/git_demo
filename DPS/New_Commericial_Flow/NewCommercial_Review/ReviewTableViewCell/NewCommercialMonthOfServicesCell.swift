//
//  NewCommercialMonthOfServicesCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommercialMonthOfServicesCell: UITableViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet var lblMonthOfService: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
