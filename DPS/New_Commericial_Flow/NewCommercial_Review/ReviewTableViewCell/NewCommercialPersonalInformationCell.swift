//
//  PersonalInformationCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 27/10/21.
//

import UIKit

class NewCommercialPersonalInformationCell: UITableViewCell {
    
    //MARK:- Outlets
    
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var btn_CustomerName: UIButton!
    @IBOutlet weak var btn_CompanyName: UIButton!
    @IBOutlet weak var btn_Email: UIButton!
    @IBOutlet weak var btn_Mobile: UIButton!
    @IBOutlet weak var btn_Address: UIButton!
    @IBOutlet weak var lbl_Address: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellConfigure(objLeadDetails : NSManagedObject) {
        print(objLeadDetails)//customerName
        
        //self.img_Profile.downloaded(from: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
        if ((objLeadDetails.value(forKey: "serviceAddressImagePath")) as! String) != "" {
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
            
            if isImageExists!  {
                
                self.img_Profile.image = image
                
            }else {
                
                if let nameurl = URL(string: "\(URL.baseUrl)/Documents/CustomerAddressImages/" + "\(objLeadDetails.value(forKey: "serviceAddressImagePath") ?? "")")
                {
                    self.img_Profile.sd_setImage(with: nameurl, placeholderImage: UIImage(named: "no_image.jpg"), completed: nil)
                }
                else
                {
                    self.img_Profile.image = UIImage(named: "no_image.jpg")!
                }
            }
        }
        
        
        
        //self.btn_CustomerName.setTitle("\(objLeadDetails.value(forKey: "customerName")!)", for: .normal)
        
        let strName = "\(Global().strFullName(fromCoreDB: objLeadDetails) ?? "")"
        self.btn_CustomerName.setTitle("\(strName)", for: .normal)
        self.btn_CompanyName.setTitle("\(objLeadDetails.value(forKey: "companyName")!)", for: .normal)
        self.btn_Email.setTitle("\(objLeadDetails.value(forKey: "primaryEmail")!)", for: .normal)
        
        
        //self.btn_Mobile.setTitle("\(objLeadDetails.value(forKey: "servicePrimaryPhone")!)", for: .normal)
        let strPrimaryPhone =  "\(objLeadDetails.value(forKey: "primaryPhone") ?? "")"
        let strSecondaryPhone =  "\(objLeadDetails.value(forKey: "secondaryPhone") ?? "")"
        let strCellNo =  "\(objLeadDetails.value(forKey: "cellNo") ?? "")"

        if strCellNo != ""{
                 self.btn_Mobile.setTitle("\( formattedNumber(number: strCellNo))", for: .normal)
        }
        else{
            if strPrimaryPhone != ""{
                self.btn_Mobile.setTitle("\( formattedNumber(number: strPrimaryPhone))", for: .normal)
            }
//            else if strSecondaryPhone != ""{
//                self.btn_Mobile.setTitle("\( formattedNumber(number: strSecondaryPhone))", for: .normal)
//            }
            else{
                self.btn_Mobile.setTitle("\( formattedNumber(number: ""))", for: .normal)
            }
        }
        //self.btn_Mobile.setTitle("\( formattedNumber(number: "\(objLeadDetails.value(forKey: "cellNo")!)"))", for: .normal)
        
        // self.btn_Address.setTitle("\(objLeadDetails.value(forKey: "servicesAddress1")!)", for: .normal)
        
        let strAddress = Global().strCombinedAddressService(for: objLeadDetails)
        self.lbl_Address.text = strAddress
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
