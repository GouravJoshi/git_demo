//
//  NewCommericialSignatureCell.swift
//  DPS
//
//  Created by Saavan Patidar on 30/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialSignatureCell: UITableViewCell {

    @IBOutlet var btnTermsCondition: UIButton!
    @IBOutlet var viewTermsConditon: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewTermsConditon.layer.borderWidth = 1.0
        viewTermsConditon.layer.borderColor = UIColor.darkGray.cgColor

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
