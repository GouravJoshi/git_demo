//
//  AddScopeImageCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 29/10/21.
//

import UIKit

class AddScopeImageCell: UITableViewCell {
    
    //MARK:- outlets
    
    @IBOutlet var lblScope: UILabel!
    @IBOutlet var lblDiscription: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
