//
//  NewCommericialTermsCondtionCell.swift
//  DPS
//
//  Created by Saavan Patidar on 14/01/22.
//  Copyright © 2022 Saavan. All rights reserved.
//

import UIKit

class NewCommericialTermsCondtionCell: UITableViewCell {

     
     
    @IBOutlet var viewTermsCondition: UIView!
    @IBOutlet var btnTermsCondition: UIButton!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         
         
        viewTermsCondition.layer.borderWidth = 0.0
        viewTermsCondition.layer.borderColor = UIColor.white.cgColor
         
         
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
