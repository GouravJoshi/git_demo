//
//  marktingContentCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 27/10/21.
//

import UIKit
import WebKit

class NewCommercialMarktingContentCell: UITableViewCell {

    @IBOutlet weak var webViewForMarktingContent: WKWebView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var heightForMarktingContent: NSLayoutConstraint!
    
    var tblMarktingContent = UITableView()
    var strMarketingContent = NSMutableString()
    var callback: ((_ str: String) -> Void)?
    var loadCount: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadWebView(strMarketingContent: String) {
        self.webViewForMarktingContent.uiDelegate = self
        self.webViewForMarktingContent.navigationDelegate = self
        
        self.webViewForMarktingContent.loadHTMLString(strMarketingContent, baseURL: Bundle.main.bundleURL)
    }
}

extension NewCommercialMarktingContentCell: UIWebViewDelegate, WKNavigationDelegate, WKUIDelegate  {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadCount += 1
    }

//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//
//        loadCount -= 1
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
//            if self!.loadCount == 0 {
//                webView.frame.size.height = 1
//                webView.frame.size = webView.sizeThatFits(.zero)
//                webView.scrollView.isScrollEnabled = false
//                print(webView.scrollView.contentSize.height)
//                print(webView.frame.size.height)
//
//                self?.heightForMarktingContent.constant = webView.scrollView.contentSize.height
//                self?.callback?("refresh")
//            }
//        }
//
//    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadCount -= 1
        
        webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (height, error) in
//            self.webViewHeightConstraint?.constant = height as! CGFloat
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                if self!.loadCount == 0 {
                    webView.frame.size.height = 1
                    webView.frame.size = webView.sizeThatFits(.zero)
                    webView.scrollView.isScrollEnabled = false
                    print(webView.scrollView.contentSize.height)
                    print(height as! CGFloat)
                    print(webView.frame.size.height)

                    self?.heightForMarktingContent.constant = webView.scrollView.contentSize.height
//                    self?.heightForMarktingContent.constant = height as! CGFloat
                    self?.callback?("refresh")
                }
            }
        })
    }
      
}
