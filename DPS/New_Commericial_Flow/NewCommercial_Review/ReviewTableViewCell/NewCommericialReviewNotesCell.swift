//
//  NewCommericialReviewNotesCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommericialReviewNotesCell: UITableViewCell {
    
    // MARK :- outlet
    
    @IBOutlet var lblNotes: UILabel!
    @IBOutlet var btnEditNots: UIButton!
    
    @IBOutlet var btnAgreement: UIButton!
    @IBOutlet var txtNoOfDays: UITextField!
    @IBOutlet var lblDays: UILabel!
    @IBOutlet var lblAgreementValidFor: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(textDayValue : String , isAgreementValid : String){
        if isAgreementValid == "true" || isAgreementValid == "1"{
            btnAgreement.isSelected = true
            txtNoOfDays.text = textDayValue
            txtNoOfDays.isHidden = false
            lblDays.isHidden = false
            
            if textDayValue == ""
            {
                self.lblAgreementValidFor.isHidden = true
            }
            else
            {
                self.lblAgreementValidFor.isHidden = false
            }
        }
        else
        {
            self.lblAgreementValidFor.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
