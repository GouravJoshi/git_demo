//
//  NewCommericailReviewPaymentCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 28/10/21.
//

import UIKit

class NewCommericailReviewPaymentCell: UITableViewCell {
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    @IBOutlet weak var lblBillingAmount: UILabel!

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCheckNo: UITextField!
    @IBOutlet weak var txtLicenceNo: UITextField!
    @IBOutlet weak var btnExpDate: UIButton!
    
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var licenceView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    
    @IBOutlet weak var btnEleTAuth: UIButton!
    @IBOutlet weak var btnBackI: UIButton!
    @IBOutlet weak var btnFrontI: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var swText: UISwitch!
    @IBOutlet weak var swPhone: UISwitch!
    @IBOutlet weak var swEmail: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
