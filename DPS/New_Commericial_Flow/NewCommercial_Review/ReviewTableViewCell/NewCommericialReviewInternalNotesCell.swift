//
//  NewCommericialReviewInternalNotesCell.swift
//  DPS
//
//  Created by Saavan Patidar on 29/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialReviewInternalNotesCell: UITableViewCell {
    
    @IBOutlet var lblInternalNotes: UILabel!
    @IBOutlet var btnEditNots: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
