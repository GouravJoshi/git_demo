

import UIKit

//protocol DataProtocol : class {
//    func getCoverLetterData(data : Any)
//    func getIntroductionLetterData(data : Any)
//    func getMarketingContentData(data : [[String : Any]])
//}


class NewCommericial_PopUpView: UIViewController {
    
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var viewFortv: NewCommericial_CardView!
    @IBOutlet weak var heighttvForList: NSLayoutConstraint!
    
    
    @objc var strTitle = String()
    @objc var aryCoverLetter = NSMutableArray()
    @objc var aryIntroductionLetter = NSMutableArray()
    @objc var arrayMarketingContent = NSMutableArray()
    @objc var aryTBLTimeRange = [[String: Any]]()
    @objc var aryPayment = [String]()
    @objc var strTag = Int()
    @objc var arySelectedItems = NSMutableArray()
    @objc var strIdToShowSelected = ""
    
    var dictCoverLetter = NSDictionary()
    weak var delegateReturnData : DataProtocol?
    var tableTag : Int?
    var tbl : UITableView?
    var selectMarketingConternt = [[String : Any]]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print("arrayMarketingContent")
        print(arrayMarketingContent)
        tvForList.estimatedRowHeight = 50.0
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
        self.viewFortv.isHidden = true
        if(DeviceType.IS_IPAD){
            heighttvForList.constant = 400.0
        }else{
            heighttvForList.constant = 280.0
            
        }
        
        if tableTag == 2 {
            tbl?.reloadData()
        }
        selectMarketingConternt = []
        
    }
    
    override func viewWillLayoutSubviews() {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.viewFortv.animShow()
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton)
    {
        
        if tableTag == 3 {
            //delegateReturnData?.getMarketingContentData(data: selectMarketingConternt)
            self.viewFortv.animShow()
            self.dismiss(animated: false) { }
        }else{
            self.viewFortv.animShow()
            self.dismiss(animated: false) { }
        }
       
        
    }
    
    
    func stringDateToString(toAM_PM strDateTime: String?) -> String?
    {
        var strDateTime = strDateTime
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "HH:mm:ss"
        
        let date1: Date? = dateFormat.date(from: strDateTime ?? "")
        
        dateFormat.dateFormat = "hh:mm a"
        
        if let date1 = date1 {
            strDateTime = dateFormat.string(from: date1)
        }
        
        return strDateTime
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NewCommericial_PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableTag == 1 {
            return aryCoverLetter.count
        }
        if tableTag == 2 {
            return aryIntroductionLetter.count
        }
        if tableTag == 3{
            return arrayMarketingContent.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialPopupCell", for: indexPath as IndexPath) as! NewCommericialPopupCell
        
        if(DeviceType.IS_IPAD){
            cell.popupCell_lbl_Title.font = UIFont.systemFont(ofSize: 20)
        }else{
            cell.popupCell_lbl_Title.font = UIFont.systemFont(ofSize: 16)
        }
        
        if tableTag == 1 {
            cell.popupCell_lbl_Title.text = ((aryCoverLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String)
        }
        if tableTag == 2 {
            cell.popupCell_lbl_Title.text = ((aryIntroductionLetter[indexPath.row] as AnyObject).value(forKey: "TemplateName") as? String)
        }
        if tableTag == 3 {
            cell.popupCell_lbl_Title.text = ((arrayMarketingContent[indexPath.row] as AnyObject).value(forKey: "Title") as? String)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableTag == 1 {
            let singleDict = aryCoverLetter[indexPath.row]
            delegateReturnData?.getCoverLetterData(data: singleDict)
            self.viewFortv.animShow()
            self.dismiss(animated: false) { }
        }
        if tableTag == 2 {
            let singleDict = aryIntroductionLetter[indexPath.row]
            delegateReturnData?.getIntroductionLetterData(data: singleDict)
            self.viewFortv.animShow()
            self.dismiss(animated: false) { }
        }
        
        if tableTag == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialPopupCell", for: indexPath as IndexPath) as! NewCommericialPopupCell
            let singleDict = arrayMarketingContent[indexPath.row]
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            selectMarketingConternt.append(singleDict as! [String : Any])
          
        }
        
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}

class NewCommericialPopupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
    
    
}


