//
//  serviceViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 20/10/21.
//

import UIKit

protocol SelectServiceProtocol: class {
    
    func SelectedService(selectedServiceObj: NSDictionary)
}
class NewCommercialServiceViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- outlet
    
    @IBOutlet weak var serviceTableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet var btnCancelSearch: UIButton!

    //MARK:- VARIABLES
    
    var arrOfServicesList = NSArray()
    weak var delegateToSelectServiceProtocol: SelectServiceProtocol?
    var arrOfSearchList = NSArray()
    var isSeaching = Bool()
    var strSelectedservice = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate_dataSource()
    }
    
    //MARK:- delegate datasource tableview
    
    func delegate_dataSource(){
        txtSearch.delegate =  self
        self.serviceTableView.delegate = self
        self.serviceTableView.dataSource = self
        self.serviceTableView.tableFooterView = UIView()
    }
    
    
    @IBAction func actionOnCancle(_ sender: Any) {
        print("btn cancle")
        
        isSeaching = false
        arrOfSearchList = NSArray()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        serviceTableView.reloadData()
        
        
    }
 //MARK: - action
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnOnClickVoiceRecognizationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrOfServicesList.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
}

extension NewCommercialServiceViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
        return arrOfServicesList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isSeaching == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            let dict = arrOfSearchList[indexPath.row] as? NSDictionary
            cell?.textLabel?.text = dict?["Name"] as? String ?? ""
            
            if strSelectedservice == dict?["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
                
            }
            cell?.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return cell ?? UITableViewCell()
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            let dict = arrOfServicesList[indexPath.row] as? NSDictionary
            cell?.textLabel?.text = dict?["Name"] as? String ?? ""
            
            if strSelectedservice == dict?["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
                
            }
            cell?.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loader, animated: false, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            
            if self.isSeaching == true {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                cell?.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                let dict = arrOfSearchList[indexPath.row] as! NSDictionary
                delegateToSelectServiceProtocol?.SelectedService(selectedServiceObj: dict)
                loader.dismiss(animated: false) {
                    self.dismiss(animated: false, completion: nil)
                }
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                cell?.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                let dict = arrOfServicesList[indexPath.row] as! NSDictionary
                delegateToSelectServiceProtocol?.SelectedService(selectedServiceObj: dict)
                loader.dismiss(animated: false) {
                    self.dismiss(animated: false, completion: nil)
                }
            }
        }
    }
    
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList = NSArray()
                self.isSeaching = false
                self.serviceTableView.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", searchedText)
                let filtered = arrOfServicesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceTableView.reloadData()
                
            } else {
                self.arrOfSearchList = NSArray()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                serviceTableView.reloadData()
                
            }
                                                                          
        }
        return true
    }
    
}
extension NewCommercialServiceViewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if str != ""{
            if #available(iOS 13.0, *) {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", str)
                let filtered = arrOfServicesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceTableView.reloadData()
            } else {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", str)
                let filtered = arrOfServicesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceTableView.reloadData()
            }
        }
    }
}
