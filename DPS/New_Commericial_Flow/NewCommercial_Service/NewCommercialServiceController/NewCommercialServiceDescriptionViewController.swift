//
//  NewCommercialServiceDescriptionViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 13/10/21.
//

import UIKit
protocol EditServiceDescriptionProtocol: class {
    
    func EditServiceDescrition(strDescription: String)
}
class NewCommercialServiceDescriptionViewController: UIViewController {
    
    //MARK:- variables
    
    var serviceDis : String?
    var strSysName = ""
    var strIsCameForEdit : String?
    var isTarget = ""
    var strIsChangeDescription = ""
    var strLeadId = ""
    weak var delegateEditDescription : EditServiceDescriptionProtocol?
    //MARK:- outlets
    
    @IBOutlet weak var serviceDiscriptionTxtView: UITextView!
    
    // MARK:- life cycle view controller
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDisply()
    }
    
    func loadDisply(){
        self.serviceDiscriptionTxtView.isUserInteractionEnabled = true
        self.serviceDiscriptionTxtView.isEditable = true
        self.serviceDiscriptionTxtView.attributedText = htmlAttributedString(strHtmlString: serviceDis ?? "")
    }
    

    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        if strIsCameForEdit == "true"{
            
            if htmlAttributedString(strHtmlString: serviceDiscriptionTxtView.text) != htmlAttributedString(strHtmlString: serviceDis ?? ""){
                strIsChangeDescription = "true"
            }
            else{
                strIsChangeDescription = "false"
            }
            
            if isTarget == "true"{
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                
                arrOfKeys.add("isChangedTargetDesc")
                arrOfKeys.add("targetDescription")
                
                arrOfValues.add(strIsChangeDescription)
                arrOfValues.add(serviceDiscriptionTxtView.text ?? "")
                
                let isSuccess = getDataFromDbToUpdate(strEntity: "LeadCommercialTargetExtDc", predicate:  NSPredicate(format: "leadId == %@ && targetSysName == %@", strLeadId, strSysName), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    print("suc")
                }
                self.dismiss(animated: false, completion: nil)

            }
            else{
                let arrOfKeys = NSMutableArray()
                let arrOfValues = NSMutableArray()
                
                
                arrOfKeys.add("isChangedScopeDesc")
                arrOfKeys.add("scopeDescription")
                
                arrOfValues.add(strIsChangeDescription)
                arrOfValues.add(serviceDiscriptionTxtView.text ?? "")
                
                let isSuccess = getDataFromDbToUpdate(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@ && scopeSysName == %@", strLeadId, strSysName) , arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                
                if isSuccess {
                    print("suc")
                }
                self.dismiss(animated: false, completion: nil)
                
            }
        }
        else{
            delegateEditDescription?.EditServiceDescrition(strDescription: serviceDiscriptionTxtView.text)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
