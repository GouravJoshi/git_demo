//
//  NewCommerialSelectServiceViewController.swift
//  Commercial_Flow_Demo Change
//
//  Created by Akshay Hastekar on 08/10/21.
//

import UIKit

class NewCommerialSelectServiceViewController: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var serviceTableView: UITableView!
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var btnGraphImage: UIButton!
    @IBOutlet weak var lblOppNumbber: UILabel!
    @IBOutlet weak var lblOppTitle: UILabel!
    @IBOutlet var btnMarkAsLost: UIButton!
    @IBOutlet weak var height_Segment: NSLayoutConstraint!

    //MARK:- variables
    var addButton : UIButton?
    @objc var strLeadId = NSString()
    @objc var strTitle = NSString()
    @objc var strOppNumber = NSString()
    var strSelectedPropsedDate = ""
    var strSelectedPropsedMonth = ""
    var arrOfTagests =  NSMutableArray()
    var arrOfScopes =  NSMutableArray()
    var arrOfStadardServices =  NSMutableArray()
    var arrOfNonStandardServices =  NSMutableArray()
    var dataGeneralInfo = NSManagedObject()
    var dictLoginData = NSDictionary()
    var strTargetIdForConfigure = NSString()
    var isEditInSalesAuto = false
    
    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""

    let btnMarkAsLostAttributes = [
        NSAttributedString.Key.foregroundColor : UIColor.darkGray,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]

    var attributedString = NSMutableAttributedString(string:"")
    var arrRenewalServices =    NSMutableArray()
    var arrOfTblView = [String]()
    
    weak var refreshOnBack: BackRefreshFormBuilderProtocol?

    //MARK: - view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IntialSetupSelectService()
        CallCoreDatafuncs()
    }
    
    func loadData(){
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: strLeadId as String)
        let recurringServiceMonth = dataGeneralInfo.value(forKey: "recurringServiceMonth") as? String ?? ""
        if recurringServiceMonth != ""{
            if recurringServiceMonth == "1"{
                strSelectedPropsedMonth = ""
            }
            else{
                strSelectedPropsedMonth = recurringServiceMonth
            }
        }
//        else{
//            strSelectedPropsedMonth = "January"
//        }
        
        let strleadNumber =  dataGeneralInfo.value(forKey: "leadNumber") ?? ""
        let straccountNo =  dataGeneralInfo.value(forKey: "accountNo") ?? ""
        
        lblOppNumbber.text = Global().strFullName(fromCoreDB: dataGeneralInfo)
        lblOppTitle.text = "Opp# \(strleadNumber)" + " " + "Acc# \(straccountNo)"
        
        self.tableView_delegate_dataSource()
        self.serviceTableView.tableFooterView = UIView()
        
        
         let initServiceDate = dataGeneralInfo.value(forKey: "initialServiceDate") as? String ?? ""
         let tempDate = changeStringDateToGivenFormat(strDate: initServiceDate, strRequiredFormat: "MM/dd/yyyy")
         if tempDate == "" || tempDate == "01/01/0001"{
             strSelectedPropsedDate = ""
         }
         else{
             strSelectedPropsedDate = tempDate
         }
    }
    func IntialSetupSelectService(){
        

        strGlobalStageSysName = "\(dataGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(dataGeneralInfo.value(forKey: "statusSysName") ?? "")"
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
        else
        {
            self.btnMarkAsLost.isUserInteractionEnabled = true
            self.btnMarkAsLost.isHidden = false
        }

    }
    func CallCoreDatafuncs(){
        getTargetFromCoreData()
        getScopeFromCoreData()
        getStandardServicesFromCoreData()
        getNonStandardServicesFromCoreData()
        getRenewalServiceFromCoreData()
        finalUpdatePricingAndBillingAmount()
        finalUpdatePricingAndBillingAmountCustom()
        createArrayOfTableView()
    }
    
    //MARK: - delegate data source function
    func tableView_delegate_dataSource(){
        self.serviceTableView.delegate = self
        self.serviceTableView.dataSource = self
    }
    
    //MARK: - CoreData Functions
    //==================LEAD DETAILS
    func getSalesWODataFromCoreData(strleadId : String) -> NSManagedObject {
        let arrayLeadDetail = getDataFromLocal(strEntity: Entity_SalesLeadDetail, predicate: NSPredicate(format: "leadId == %@ && userName == %@", strleadId , "\(Global().getUserName()!)"))
        

        if(arrayLeadDetail.count > 0)
        {
            return arrayLeadDetail.firstObject as! NSManagedObject
        }
        return NSManagedObject()
    }
    
    //==================TARGETS
    func getTargetFromCoreData(){
        arrOfTagests.removeAllObjects()
        print("----------")
        print(strLeadId)
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfTagests.add(dict)
            }
        }
    }
    
    func deleteTargetFromCoreData(strTargetSysName: String, strLeadCommericalId: String, strLeadMobileId: String){
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
            
            if arryOfData.count > 0
            {
                for i in 0..<arryOfData.count{
                    
                    var strTargetId = ""
                    var strMobileId = ""
                    
                    let obj = arryOfData[i]  as! NSManagedObject
                    
                    strTargetId = "\(obj.value(forKey: "leadCommercialTargetId") ?? "")"
                    strMobileId = "\(obj.value(forKey: "mobileTargetId") ?? "")"
                    
                    if strLeadCommericalId.count > 0 && strLeadMobileId == ""
                    {
                        if strLeadCommericalId == strTargetId
                        {
                            let objData = arryOfData[i] as! NSManagedObject
                            deleteDataFromDB(obj: objData)
                            isEditInSalesAuto = true
                            CallCoreDatafuncs()
                        }
                    }
                    else
                    {
                        if strLeadMobileId == strMobileId
                        {
                            let objData = arryOfData[i] as! NSManagedObject
                            deleteDataFromDB(obj: objData)
                            CallCoreDatafuncs()
                        }
                    }
                    
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    //==================Scope
    func getScopeFromCoreData(){
        arrOfScopes.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfScopes.add(dict)
            }
        }
    }
    
    func deleteScopeFromCoreData(strScopeSysName: String){
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
            
            if arryOfData.count > 0
            {
                for i in 0..<arryOfData.count{
                    let obj = arryOfData[i]  as! NSManagedObject
                    let  strSysNameService =  "\(obj.value(forKey: "scopeSysName") ?? "")"
                    if strSysNameService.lowercased() == strScopeSysName.lowercased(){
                        let objData = arryOfData[i] as! NSManagedObject
                        deleteDataFromDB(obj: objData)
                        CallCoreDatafuncs()
                        isEditInSalesAuto = true
                    }
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    //=========================Stardard Service
    func getStandardServicesFromCoreData(){
        arrOfStadardServices.removeAllObjects()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfStadardServices.add(dict)
            }
        }
    }
    
    func deleteStandardServiceFromCoreData(strSysName: String){
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
            
            if arryOfData.count > 0
            {
                for i in 0..<arryOfData.count{
                    let obj = arryOfData[i]  as! NSManagedObject
                    let  strSysNameService =  "\(obj.value(forKey: "serviceSysName") ?? "")"
                    let  strSoldServiceId  = "\(obj.value(forKey: "soldServiceStandardId") ?? "")"
                    if strSysNameService.lowercased() == strSysName.lowercased(){
                        let objData = arryOfData[i] as! NSManagedObject
                        deleteDataFromDB(obj: objData)
                        
                        //Deleting renewal which are added in corresponding to the Stand. Service
                        let arrayDataRenwal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ AND soldServiceStandardId = %@", self.strLeadId, strSoldServiceId))
                        
                        if arrayDataRenwal.count > 0 {
                            for item in arrayDataRenwal
                            {
                                let objTempSales = item as! NSManagedObject
                                deleteDataFromDB(obj: objTempSales)
                            }
                        }
                        CallCoreDatafuncs()
                        isEditInSalesAuto = true
                    }
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
    //=========================Non Stardard Service
    func getNonStandardServicesFromCoreData(){
        arrOfNonStandardServices.removeAllObjects()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfNonStandardServices.add(dict)
            }
        }
    }
    
    
    func deleteNonStandardServiceFromCoreData(strSoldServiceId: String){
        
        let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
            
            let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
            
            if arryOfData.count > 0
            {
                for i in 0..<arryOfData.count{
                    let obj = arryOfData[i]  as! NSManagedObject
                    let  strSysNameService =  "\(obj.value(forKey: "soldServiceNonStandardId") ?? "")"
                    if strSysNameService == strSoldServiceId{
                        let objData = arryOfData[i] as! NSManagedObject
                        deleteDataFromDB(obj: objData)
                        CallCoreDatafuncs()
                        isEditInSalesAuto = true
                    }
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: {
        })
        
    }
    
    //=========================Renewal Service
    func getRenewalServiceFromCoreData(){
        
        arrRenewalServices.removeAllObjects()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrRenewalServices.add(dict)
            }
        }
    }
    
    //MARK: - Table Creation function
    func createArrayOfTableView(){
        
        arrOfTblView.removeAll()
        arrOfTblView = ["Targets", "Scope", "Services", "Other"]
       
        if arrRenewalServices.count > 0{
            arrOfTblView.insert("Renewal Service", at: 3)
        }
        if arrOfNonStandardServices.count > 0{
            if arrRenewalServices.count > 0 {
                arrOfTblView.insert("Custom Service", at: 4)
            }
            else{
                arrOfTblView.insert("Custom Service", at: 3)
            }
        }
        serviceTableView.reloadData()
    }
    
   
    //MARK: - IBActions
    
    @IBAction func action_Home(_ sender: Any) {
        self.view.endEditing(true)
        self.goToAppointment()
    }
    
    @IBAction func btnAddImageAction(_ sender: Any)
    {
        self.isEditInSalesAuto = true
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Add Images", style: .default , handler:{ (UIAlertAction)in
            self.goToGlobalmage(strType: "Before")
        }))
        
        alert.addAction(UIAlertAction(title: "Add Documents", style: .default , handler:{ (UIAlertAction)in
            self.goToUploadDocument()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceView = self.btnAddImage
        self.present(alert, animated: true, completion: {
        })
        
    }
     
    @IBAction func btnAddGraphAction(_ sender: Any)
    {
        self.isEditInSalesAuto = true
       goToGlobalmage(strType: "Graph")
    }
     
    @IBAction func btnClockAction(_ sender: Any)
    {
        if !isInternetAvailable()
        {
            Global().displayAlertController("Alert !", "\(ErrorInternetMsg)", self)
        }
        else
        {
            if DeviceType.IS_IPAD {
                let storyboardIpad = UIStoryboard.init(name: "MainiPad", bundle: nil)
                
                let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewControlleriPad") as? ClockInOutViewControlleriPad
                self.navigationController?.pushViewController(clockInOutVC!, animated: false)
            }
            else{
                let storyboardIpad = UIStoryboard.init(name: "Main", bundle: nil)
                let clockInOutVC = storyboardIpad.instantiateViewController(withIdentifier: "ClockInOutViewController") as? ClockInOutViewController
                self.navigationController?.pushViewController(clockInOutVC!, animated: false)
            }
        }
    }
    
    @IBAction func btnChemicalSensitivityAction(_ sender: Any) {
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
//        let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
//        let objChemicalSen = storyboardIpad.instantiateViewController(withIdentifier: "ChemicalSensitivityList") as? ChemicalSensitivityList
//        objChemicalSen?.strFrom = "sales"
//        objChemicalSen?.strId = strLeadId as String
//        self.navigationController?.present(objChemicalSen!, animated: false, completion: nil)
    }
    
    @IBAction func btnHistoryAction(_ sender: UIButton) {
        
       
            
            let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
                self.view.endEditing(true)
                
                self.goToEmailHistory()
                
            }))
            
            alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
            
                self.goToSetupHistory()
            }))
            alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
            
                self.goToServiceNotesHistory()
            }))
            alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
            
                self.goToServiceHistory()
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
                
            }))
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender
               // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = [.up]
            }
            self.present(alert, animated: true)
           
         
    }
    
    @IBAction func btnMarkAsLost(_ sender: UIButton) {
        
        finalUpdatePricingAndBillingAmount()
        finalUpdatePricingAndBillingAmountCustom()
        let alert = UIAlertController(title: Alert, message: "Are you sure to mark Opportunity as lost?", preferredStyle: .alert)


                alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in

                    self.updateLeadIdDetail()
                    
                    
                }))

                

                alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
 

                }))

                alert.popoverPresentationController?.sourceView = self.view

                self.present(alert, animated: true, completion: {

                })
        
    }
    
    //MARK: - Calcuting billing frequncy

    func finalUpdatePricingAndBillingAmountCustom() {
    

       let chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool

        let arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", Global().getUserName(), strLeadId))
        
        var totalInitialPrice = 0.0
        var totalMaintPrice = 0.0
        var totalBillingAmount = 0.0
        
        for item in arryOfServiceData
        {
            totalInitialPrice = 0.0
            totalMaintPrice = 0.0
            totalBillingAmount = 0.0
            
            let matches = item as! NSManagedObject
            
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "billingFrequencySysName") ?? "")")
            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "frequencySysName") ?? "")")
            
            
            let initialPrice = ("\(matches.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue
            let maintPrice = ("\(matches.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue
//            let unit = ("\(matches.value(forKey: "unit") ?? "")" as NSString).doubleValue
            
            
           
            totalInitialPrice = initialPrice
            totalMaintPrice = maintPrice
             

            //Billing Price Calculation
            
            let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"
            
            if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                
            }
            else
            {
                if chkFreqConfiguration
                {
                    if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                    {
                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                    else
                    {
                        totalBillingAmount = ( totalMaintPrice *  (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                else
                {
                    totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
            }
            
            if totalBillingAmount < 0
            {
                totalBillingAmount = 0.0
            }
            
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                "billingFrequencyPrice"
            ]
            
            
            arrOfValues = [
                String(format: "%.2f", totalBillingAmount),
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@",strLeadId, "\(matches.value(forKey: "soldServiceNonStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
             
        }
        

    }
        func finalUpdatePricingAndBillingAmount() {

            let chkFreqConfiguration = (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool

            let arryOfServiceData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@", Global().getUserName(), strLeadId))

            var totalInitialPrice = 0.0

            var totalMaintPrice = 0.0

            var finalUnitBaseInitialPrice = 0.0

            var finalUnitBasedMaintPrice = 0.0

            var totalBillingAmount = 0.0

            for item in arryOfServiceData

            {

                totalInitialPrice = 0.0
                totalMaintPrice = 0.0
                finalUnitBaseInitialPrice = 0.0
                finalUnitBasedMaintPrice = 0.0
                totalBillingAmount = 0.0

                let matches = item as! NSManagedObject

                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "billingFrequencySysName") ?? "")")

                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "frequencySysName") ?? "")")

                let initialPrice = ("\(matches.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue

                let maintPrice = ("\(matches.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue

                let unit = ("\(matches.value(forKey: "unit") ?? "")" as NSString).doubleValue

                if unit > 0

                {
                    totalInitialPrice = totalInitialPrice + (initialPrice * unit)
                    totalMaintPrice = totalMaintPrice + (maintPrice * unit)
                    finalUnitBaseInitialPrice = (initialPrice * unit)
                    finalUnitBasedMaintPrice = (maintPrice * unit)
                }

                else

                {
                    totalInitialPrice = totalInitialPrice + initialPrice
                    totalMaintPrice = totalMaintPrice + maintPrice
                    finalUnitBaseInitialPrice = initialPrice
                    finalUnitBasedMaintPrice = maintPrice
                }

                if matches.value(forKey: "additionalParameterPriceDcs") is NSArray
                {
                    let arrPara = matches.value(forKey: "additionalParameterPriceDcs") as! NSArray

                    if arrPara.count > 0
                    {
                        for item in arrPara
                        {
                            let dict = item as! NSDictionary
                            totalInitialPrice = totalInitialPrice + ("\(dict.value(forKey: "FinalInitialUnitPrice") ?? "")" as NSString).doubleValue

                            totalMaintPrice = totalMaintPrice + ("\(dict.value(forKey: "FinalMaintUnitPrice") ?? "")" as NSString).doubleValue
                        }
                    }
                }

                //Billing Price Calculation
                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"

                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

                if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                    totalBillingAmount = ( totalInitialPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                }
                else
                {
                    if chkFreqConfiguration

                    {

                        if "\(matches.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame

                        {

                            totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue

                        }

                        else

                        {

                            totalBillingAmount = ( totalMaintPrice *  (((strServiceFrqYearOccurence as NSString).doubleValue) - 1) ) /  (strBillingFrqYearOccurence as NSString).doubleValue

                        }

                    }

                    else

                    {

                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue

                    }

                }

                

                if totalBillingAmount < 0

                {

                    totalBillingAmount = 0.0

                }

                

                

                var arrOfKeys = NSMutableArray()

                var arrOfValues = NSMutableArray()

                

                arrOfKeys = [

                    "billingFrequencyPrice",

                    "finalUnitBasedInitialPrice",

                    "finalUnitBasedMaintPrice",

                    "totalInitialPrice",

                    "totalMaintPrice",

                ]

                

                

                arrOfValues = [

                    

                    String(format: "%.2f", totalBillingAmount),

                    String(format: "%.2f", finalUnitBaseInitialPrice),

                    String(format: "%.2f", finalUnitBasedMaintPrice),

                    String(format: "%.2f", totalInitialPrice),

                    String(format: "%.2f", totalMaintPrice)

                ]

                

                var isSuccess = Bool()

                

                isSuccess =  getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId, "\(matches.value(forKey: "soldServiceStandardId") ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)

                

                

                if(isSuccess == true)

                {

                    print("updated successsfully")

                }

                else

                {

                    print("updates failed")

                }

                

            }

        }
    
    
    //MARK:  - Footer Funcs
    
    func saveProposalDateAndMonth(){
        let strInitalServiceDate = strSelectedPropsedDate
        let strRecurringServiceMonth = strSelectedPropsedMonth
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
            "initialServiceDate",
            "recurringServiceMonth"]
        
        arrOfValues = [
            strInitalServiceDate,
            strRecurringServiceMonth]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate:  NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            print("suc")
        }
    }
    func updateLeadIdDetail()

    {
        
        Global().updateSalesModifydate(strLeadId as String)
        
        let strInitalServiceDate = strSelectedPropsedDate
        
        let strRecurringServiceMonth = strSelectedPropsedMonth
        
        var arrOfKeys = NSMutableArray()
        
        var arrOfValues = NSMutableArray()
        
        
        arrOfKeys = ["stageSysName",
                     
                     "statusSysName",
                     
                     "initialServiceDate",
                     
                     "recurringServiceMonth"
                     
        ]
        
        arrOfValues = ["Lost",
                       
                       "Complete",
                       
                       strInitalServiceDate,
                       
                       strRecurringServiceMonth
                       
        ]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate:  NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            print("suc")
            
            goToAppointment()
            
        }
        
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = Global().getCompanyKey()
              testController?.strUserName = Global().getUserName()

              self.present(testController!, animated: false, completion: nil)
    }
    
    
    func goToServiceNotesHistory()  {
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
    
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    func goToEmailHistory()
    {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow =  "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
    }
    
    func goToUploadDocument(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "SalesMainiPad", bundle:nil)
        let objUploadDocumentSales = storyBoard.instantiateViewController(withIdentifier: "UploadDocumentSalesiPad") as! UploadDocumentSales

        objUploadDocumentSales.strWoId = strLeadId
        self.navigationController?.present(objUploadDocumentSales, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "PestiPhone", bundle:nil)
        let objGlobalImageVC = storyBoard.instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        
        objGlobalImageVC.strHeaderTitle = strType as NSString;
        objGlobalImageVC.strWoId = strLeadId;
        objGlobalImageVC.strWoLeadId = strLeadId;
        objGlobalImageVC.strModuleType = "SalesFlow"
        objGlobalImageVC.strWoStatus = "InComplete"
        self.navigationController?.present(objGlobalImageVC, animated: false, completion: nil)
    }
    
    //MARK: -  ADDTIONAL Function Navigation
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    func getLostStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Lost") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    @objc func targetAction(){
        
        isEditInSalesAuto = true
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialSelectTargertViewController) as! NewCommercialSelectTargertViewController
        nextViewController.strLeadId = strLeadId
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func scopetAction(){
        
        isEditInSalesAuto = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommerialAddScopeViewController) as! NewCommerialAddScopeViewController
        nextViewController.strLeadId = strLeadId
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func serviceAction(sender : UIButton){
        
        self.isEditInSalesAuto = true
        let alert = UIAlertController(title: "", message: "---Select---", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Standard Services", style: UIAlertAction.Style.default, handler: { action in
            print("standard serviced")
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialAddServicesViewController) as! NewCommercialAddServicesViewController
            nextViewController.navigationTitle = "Add Standard Services"
            nextViewController.strLeadId = self.strLeadId as String
            nextViewController.strIsFrommCustomService = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Custom Service", style: UIAlertAction.Style.default, handler: { action in
            print("non standard services")
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialAddServicesViewController) as! NewCommercialAddServicesViewController
            nextViewController.navigationTitle = "Add Custom Services"
            nextViewController.strLeadId = self.strLeadId as String
            nextViewController.strIsFrommCustomService = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancle", style: UIAlertAction.Style.cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.permittedArrowDirections = [.right]
        }
        
        self.present(alert, animated: true, completion: nil)
 
    }
    
    //MARK: - Action
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        
        if arrOfStadardServices.count == 0 && arrOfNonStandardServices.count == 0 {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please add service", viewcontrol: self)
        }
        else{
            finalUpdatePricingAndBillingAmount()
            saveProposalDateAndMonth()
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialConfigure", bundle: nil)
            let next = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialConfigureDashBoardVC) as! NewCommercialConfigureDashBoardVC
            next.strLeadId = strLeadId
            next.strTitle = strTitle
            next.strOppNumber  = strOppNumber
            next.strSelectedPropsedDate = strSelectedPropsedDate
            next.strSelectedPropsedMonth = strSelectedPropsedMonth
            self.navigationController?.pushViewController(next, animated: false)
            
            if self.isEditInSalesAuto == true {
                Global().updateSalesModifydate(self.strLeadId as String)
            }
        }
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        
        if getOpportunityStatus()
        {
            refreshOnBack?.refreshView(data: "")
        }
        
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true)
    }
    
    @IBAction func btnCancle(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- extension

extension NewCommerialSelectServiceViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrOfTblView.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrOfTblView[section] == "Targets"{
            return arrOfTagests.count
        } else if arrOfTblView[section] == "Scope"{
            return arrOfScopes.count
        }else if arrOfTblView[section] == "Services"{
            return arrOfStadardServices.count
        }else if arrOfTblView[section] == "Renewal Service"{
            return arrRenewalServices.count
        }else if arrOfTblView[section] == "Custom Service"{
            return arrOfNonStandardServices.count
        }else if arrOfTblView[section] == "Other"{
            return 1
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrOfTblView[indexPath.section] == "Targets"{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialTargetCell, for: indexPath) as? NewCommercialTargetCell
            
            let obj = arrOfTagests[indexPath.row]  as! NSManagedObject
            
            cell?.strGlobalStageSysName = strGlobalStageSysName
            cell?.strGlobalStatusSysName = strGlobalStatusSysName
            cell?.SetUpCellForTarget(obj: obj)
           
            cell?.btnAddImages.tag = indexPath.row
            cell?.btnAddImages.addTarget(self, action: #selector(btnAddImageAction), for: .touchUpInside)
            
            cell?.btnDeleteTarget.tag = indexPath.row
            cell?.btnDeleteTarget.addTarget(self, action: #selector(btnDeleteTargetAction), for: .touchUpInside)
            
            cell?.btnEditTargetDescription.tag = indexPath.row
            cell?.btnEditTargetDescription.addTarget(self, action: #selector(btnEditTargetDescriptionAction), for: .touchUpInside)
            
            return cell ??  UITableViewCell()
        }
        else if  arrOfTblView[indexPath.section] == "Scope" {
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialScopeTableViewCell, for: indexPath) as? NewCommercialScopeTableViewCell
            
            let obj = arrOfScopes[indexPath.row]  as! NSManagedObject
            cell?.strGlobalStageSysName = strGlobalStageSysName
            cell?.strGlobalStatusSysName = strGlobalStatusSysName
            cell?.SetUpCellForScope(obj: obj)
            
            cell?.btnDeleteScope.tag = indexPath.row
            cell?.btnDeleteScope.addTarget(self, action: #selector(btnDeleteScopeAction), for: .touchUpInside)
            
            cell?.btnEditScopeDescription.tag = indexPath.row
            cell?.btnEditScopeDescription.addTarget(self, action: #selector(btnEditScopeDescriptionAction),  for: .touchUpInside)
                             
            return cell ??  UITableViewCell()
            
        }
        else if arrOfTblView[indexPath.section] == "Services"{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialStandardServiceTableViewCell, for: indexPath) as? NewCommercialStandardServiceTableViewCell
            
            
            let dictService = arrOfStadardServices[indexPath.row]  as! NSManagedObject
            
            let objSelectedCategory = getCategoryObjectFromServiceSysName(strServiceSysName: "\(dictService.value(forKey: "serviceSysName") ?? "")")
            let objOfServices = getServiceForCategory(obj: objSelectedCategory)
            let strServiceName = getServiceNameFromSysName(objOfService: objOfServices, strSysName: "\(dictService.value(forKey: "serviceSysName") ?? "")" )
            print("============ \(strServiceName)")
            if strServiceName == ""{
                cell?.lblServiceName.text = "\(dictService.value(forKey: "serviceSysName") ?? "")"
            }
            else{
                cell?.lblServiceName.text = strServiceName
            }
            
            cell?.lblInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(dictService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
            cell?.lblMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(dictService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
            cell?.lblServiceFrequencyName.text = "\(dictService.value(forKey: "serviceFrequency") ?? "")"
            
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(dictService.value(forKey: "billingFrequencySysName") ?? "")")
            
            cell?.lblBillingFrequencyName.text = "Billed: $" + "\(dictService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            
            cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(dictService.value(forKey: "serviceDescription") ?? "")")
            
            let isChanged =  "\(dictService.value(forKey: "isChangeServiceDesc") ?? "")"
            
            if isChanged.lowercased() == "true".lowercased(){
                cell?.imgViewChecked.image = UIImage (named: "check_box_2.png")
            }
            else{
                cell?.imgViewChecked.image = UIImage(named: "check_box_1.png")
            }
            
            if cell?.lblServiceFrequencyName.text?.lowercased() == "OneTime".lowercased() || cell?.lblServiceFrequencyName.text?.lowercased() == "One Time".lowercased()
            {
                cell?.lblMaintPrice.isHidden = true
                cell?.lblMaintPrice.text = ""
                cell?.topOfMaintancePrice.constant = 0
                cell?.heightConstraintForMantainceTitle.constant = 0
                cell?.lblMaintPriceTitle.isHidden = true
            }else {
                cell?.lblMaintPrice.isHidden = false
                cell?.topOfMaintancePrice.constant = 5
                cell?.heightConstraintForMantainceTitle.constant = 25
                cell?.lblMaintPriceTitle.isHidden = false
            }
            
            cell?.btnDelete.tag = indexPath.row
            cell?.btnDelete.addTarget(self, action: #selector(btnDeleteStandardServiceAction), for: .touchUpInside)
            
            cell?.btnEdit.tag = indexPath.row
            cell?.btnEdit.addTarget(self, action: #selector(btnEditStandardServiceAction), for: .touchUpInside)
            
            if getOpportunityStatus() == true
            {
                cell?.btnEdit.isHidden = true
                cell?.btnDelete.isHidden = true
            }
            else{
                cell?.btnEdit.isHidden = false
                cell?.btnDelete.isHidden = false
            }
            
            return cell ?? UITableViewCell()
        }
        else if arrOfTblView[indexPath.section] == "Renewal Service"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialRenewalServiceCell", for: indexPath) as? NewCommercialRenewalServiceCell
            
            let dictRenewal = arrRenewalServices[indexPath.row]  as! NSManagedObject
          
            
            let dictService = getServiceObjectFromIdAndSysName(strId: "\(dictRenewal.value(forKey: "serviceId") ?? "")", strSysName: "")

            let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(dictRenewal.value(forKey: "renewalFrequencySysName") ?? "")")
            
                    
            cell?.lblServiceName.text = "\(dictService.value(forKey: "Name") ?? "")"
            
            cell?.txtInitialPrice.text = stringToFloat(strValue: "\(dictRenewal.value(forKey: "renewalAmount") ?? "")")
                        
            cell?.lblServiceFrequencyName.text =  "\(dictServiceFrequency.value(forKey: "FrequencyName") ?? "")"
            
            cell?.lblServiceDescription.attributedText =  getAttributedHtmlStringUnicode(strText: "\(dictRenewal.value(forKey: "renewalDescription") ?? "")")
        
            
            cell?.btnEditDescription.tag = indexPath.row
            cell?.btnEditDescription.addTarget(self, action: #selector(btnEditRenewalServiceDescription), for: .touchUpInside)
            
            cell?.btnDelete.tag = indexPath.row
            cell?.btnDelete.addTarget(self, action: #selector(btnDeleteRenewalServiceAction), for: .touchUpInside)
            
            cell?.strLeadId = strLeadId as String
            cell?.strSoldStandardServiceId = "\(dictRenewal.value(forKey: "soldServiceStandardId") ?? "")"
            cell?.strUserName = Global().getUserName()
            cell?.callback = { (strSuccessUpdate: String) -> () in
                print(strSuccessUpdate)
                //Added callback by ruchika
                
                if strSuccessUpdate == "sucss"{
                    self.isEditInSalesAuto = true
                    self.CallCoreDatafuncs()
                }
            }
            if getOpportunityStatus() == true
            {
                cell?.btnEditDescription.isHidden = true
                cell?.txtInitialPrice.isEnabled = false
                cell?.btnDelete.isHidden = true
            }
            else{
                cell?.btnEditDescription.isHidden = false
                cell?.txtInitialPrice.isEnabled = true
                cell?.btnDelete.isHidden = false
            }
            
            return cell ?? UITableViewCell()
        }
        else if arrOfTblView[indexPath.section] == "Custom Service"{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialNonStandardServiceTableViewCell, for: indexPath) as? NewCommercialNonStandardServiceTableViewCell
            
            let dictService = arrOfNonStandardServices[indexPath.row]  as! NSManagedObject
          
            cell?.lblServiceName.text = "\(dictService.value(forKey: "serviceName") ?? "")"
            
            cell?.lblInitialPrice.text = convertDoubleToCurrency(amount: Double(("\(dictService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
            
            cell?.lblMaintPrice.text = convertDoubleToCurrency(amount: Double(("\(dictService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
            
            cell?.lblServiceFrequencyName.text =  "\(dictService.value(forKey: "serviceFrequency") ?? "")" //getFrequcnyNameFromSysName(strSysName: "\(dictService.value(forKey: "serviceFrequency") ?? "")")
            
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(dictService.value(forKey: "billingFrequencySysName") ?? "")")
            cell?.lblBillingFrequencyName.text = "Billed: $" + "\(dictService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            cell?.lblServiceDescription.attributedText =  getAttributedHtmlStringUnicode(strText: "\(dictService.value(forKey: "serviceDescription") ?? "")")
            
            
            if cell?.lblServiceFrequencyName.text?.lowercased() == "OneTime".lowercased() || cell?.lblServiceFrequencyName.text?.lowercased() == "One Time".lowercased()
            {
                cell?.lblMaintPrice.isHidden = true
                cell?.lblMaintPrice.text = ""
                cell?.topOfManitancePrice.constant = 0
                cell?.heightConstaintForMaitanceTitle.constant = 0
                cell?.lblMaintPriceTitle.isHidden = true
            }else {
                cell?.lblMaintPrice.isHidden = false
              //  cell?.lblMaintPrice.text = "Maintenance Price"
                cell?.topOfManitancePrice.constant = 5
                cell?.heightConstaintForMaitanceTitle.constant = 25
                cell?.lblMaintPriceTitle.isHidden = false
            }

            
            cell?.btnEdit.tag = indexPath.row
            cell?.btnEdit.addTarget(self, action: #selector(btnEditNonStandardServiceAction), for: .touchUpInside)
            
            cell?.btnDelete.tag = indexPath.row
            cell?.btnDelete.addTarget(self, action: #selector(btnDeleteNonStandardServiceAction), for: .touchUpInside)
            
            if getOpportunityStatus() == true
            {
                cell?.btnEdit.isHidden = true
                cell?.btnDelete.isHidden = true
            }
            else{
                cell?.btnEdit.isHidden = false
                cell?.btnDelete.isHidden = false
            }
            
            return cell ?? UITableViewCell()
        }
        else if arrOfTblView[indexPath.section] == "Other" {
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialOtherCell, for: indexPath) as? NewCommercialOtherCell
            
            cell?.txtPropsedDate.text = strSelectedPropsedDate
            cell?.txtProposedServiceMonth.text = strSelectedPropsedMonth
             
            cell?.btnProposedDate.tag = indexPath.row
            cell?.btnProposedDate.addTarget(self, action: #selector(btnProposedDateAction), for: .touchUpInside)
            cell?.btnProposedServiceMonth.addTarget(self, action: #selector(btnProposedServiceDateAction), for: .touchUpInside)
                
            if getOpportunityStatus() == true
            {
                cell?.btnProposedDate.isUserInteractionEnabled = false
                cell?.btnProposedServiceMonth.isUserInteractionEnabled = false
            }
            else{
                cell?.btnProposedDate.isUserInteractionEnabled = true
                cell?.btnProposedServiceMonth.isUserInteractionEnabled = true
            }
            return cell ?? UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // HEADER VIEW
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  50))
        viewheader.backgroundColor = UIColor.white
        
        // INSIDE LABEL
        let lbl = UILabel(frame: CGRect(x: 15, y: 2, width: 300, height:35))
        
        // INSIDE BUTTON :-
        addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 105, y: 2, width: 90, height: 35))
        addButton?.backgroundColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        addButton?.setTitle("+ Add", for: .normal)
        addButton?.layer.cornerRadius = 17.5
        
        // addButton.center = viewheader.center
        
        // HEADER BOTTOM LINE
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 43.0, width: viewheader.frame.size.width, height: 1.5)
        bottomBorder.backgroundColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        
        if tableView == serviceTableView
        {
            if( arrOfTblView[section] == "Targets")
            {
                lbl.text = "Target"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.addTarget(self, action: #selector(NewCommerialSelectServiceViewController.targetAction), for: .touchUpInside)
                addButton?.alpha = 1
            }
            
            else if(arrOfTblView[section] ==  "Scope")
            {
                lbl.text = "Scope"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.addTarget(self, action: #selector(NewCommerialSelectServiceViewController.scopetAction), for:.touchUpInside)
                addButton?.alpha = 1
            }
            
            else if(arrOfTblView[section] == "Services")
            {
                lbl.text = "Services"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.tag = section
                addButton?.addTarget(self, action: #selector(NewCommerialSelectServiceViewController.serviceAction), for: .touchUpInside)
                addButton?.alpha = 1
            }
            else if(arrOfTblView[section] == "Renewal Service")
            {
                lbl.text = "Renewal Services"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.alpha = 0
            }
            
            else if(arrOfTblView[section] == "Custom Service"){
                lbl.text = "Custom Services"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.alpha = 0
            }
            else if(arrOfTblView[section] == "Other"){
                lbl.text = "Other"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.alpha = 0
            }
            
            if DeviceType.IS_IPAD {
                addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
                lbl.font = UIFont.boldSystemFont(ofSize: 21)
                
            }
            else{
                addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                lbl.font = UIFont.boldSystemFont(ofSize: 18)
                
            }
            viewheader.addSubview(lbl)
            if getOpportunityStatus() == true
            {
                print("Not able to add in lost and complete stage")
            }
            else{
                viewheader.addSubview(addButton!)
            }
            viewheader.layer.addSublayer(bottomBorder)
            return viewheader
        }
        else
        {
            return viewheader
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    //MARK: - Cell Buttons  Actions
    
    @objc func btnDeleteStandardServiceAction(sender: UIButton!){
        //let index = IndexPath(item: sender.tag , section: 2)
        
        let obj =  arrOfStadardServices[sender.tag] as! NSManagedObject
        let sysName = "\(obj.value(forKey: "serviceSysName") ?? "")"
        deleteStandardServiceFromCoreData(strSysName: sysName)
    }
    
    
    @objc func btnDeleteNonStandardServiceAction(sender: UIButton!){
        
        let obj =  arrOfNonStandardServices[sender.tag] as! NSManagedObject
        let strSoldServiceNonStandardId = "\(obj.value(forKey: "soldServiceNonStandardId") ?? "")"
        deleteNonStandardServiceFromCoreData(strSoldServiceId: strSoldServiceNonStandardId)
    }
    
    @objc func btnEditNonStandardServiceAction(sender: UIButton!){

        let obj =  arrOfNonStandardServices[sender.tag] as! NSManagedObject
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialAddServicesViewController) as! NewCommercialAddServicesViewController
        nextViewController.navigationTitle = "Update Services"
        nextViewController.strLeadId = self.strLeadId as String
        nextViewController.strIsFrommCustomService = true
        nextViewController.strForEdit = "NonEditStandard"
        nextViewController.objOfEditService = obj
        self.isEditInSalesAuto = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func btnEditStandardServiceAction(sender: UIButton!){

        let obj =  arrOfStadardServices[sender.tag] as! NSManagedObject

        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialAddServicesViewController) as! NewCommercialAddServicesViewController
        nextViewController.navigationTitle = "Update Services"
        nextViewController.strLeadId = self.strLeadId as String
        nextViewController.strIsFrommCustomService = false
        nextViewController.strForEdit = "EditStandard"
        nextViewController.objOfEditService = obj
        self.isEditInSalesAuto = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func btnEditRenewalServiceDescription(sender: UIButton!){
        
        let obj =  arrRenewalServices[sender.tag] as! NSManagedObject
        self.isEditInSalesAuto = true
        
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "SalesClarkPestiPad" : "SalesClarkPestiPhone", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TextEditorNormal") as! TextEditorNormal
        vc.strfrom = "EditCommericalRenewalDescription"
        vc.strHtml = "\(obj.value(forKey: "renewalDescription") ?? "")"
        vc.matchesObject = obj
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
        
    }
    
    @objc func btnDeleteRenewalServiceAction(sender: UIButton!){
        
        let obj =  arrRenewalServices[sender.tag] as! NSManagedObject
            let alert = UIAlertController(title: Alert, message: "Are you sure want to delete", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                
                self.isEditInSalesAuto = true
                deleteDataFromDB(obj: obj)
                self.CallCoreDatafuncs()
                self.serviceTableView.reloadData()
                self.view.endEditing(true)
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                
            }))

            alert.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: {
            })
            
    }
    
    @objc func btnProposedDateAction(sender: UIButton!){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        formatter.locale = Locale(identifier: "EST")
        let result = formatter.string(from: Date())
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let fromDate = formatter.date(from:((strSelectedPropsedDate.count) > 0 ? strSelectedPropsedDate : result))!
        let datePicker = UIDatePicker()
        datePicker.minimumDate = Date()
        datePicker.date = formatter.date(from: result)!//dateFormatter.string(from: fromDate)
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        let alertController = SalesNew_SupportFile().InitializePickerInActionSheetWithClearButton(slectedDate: fromDate,Picker: datePicker, view: self.view ,strTitle : "Select Date")
        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            self.isEditInSalesAuto = true
            strSelectedPropsedDate = formatter.string(from: datePicker.date)
            serviceTableView.reloadData()
        }))
        alertController.addAction(UIAlertAction(title: "Clear", style: UIAlertAction.Style.default, handler: { [self] action in
            print(datePicker.date)
            self.isEditInSalesAuto = true
            strSelectedPropsedDate = ""
            serviceTableView.reloadData()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnProposedServiceDateAction(sender: UIButton!){
        print("asdasd")
        openMonthPopup()
    }
    
    //MARK: - Actions Of Target Cell
    @objc func btnAddImageAction(sender: UIButton!){
        
        let obj =  arrOfTagests[sender.tag] as! NSManagedObject
        let strTargetId = obj.value(forKey: "targetSysName") as? NSString ?? "" //targetSysName leadCommercialTargetId
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "PestiPhone", bundle:nil)
        let objGlobalVC = storyBoard.instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        
        objGlobalVC.strHeaderTitle = "Target"
        objGlobalVC.strWoId = strLeadId;
        objGlobalVC.strWoLeadId = strLeadId;
        objGlobalVC.strModuleType = "Target"
        objGlobalVC.strTargetId = strTargetId
        
        var strMobileTargetId = ""
        var strMobileTargetImageId = ""
        var strLeadCommercialTargetId = ""
        
        strMobileTargetId = obj.value(forKey: "mobileTargetId") as? String ?? ""
        strMobileTargetImageId = Global().getReferenceNumber()
        strLeadCommercialTargetId = obj.value(forKey: "leadCommercialTargetId") as? String ?? ""
        
        let dictTemp = ["mobileTargetId": strMobileTargetId,
                    "mobileTargetImageId": strMobileTargetImageId,
                    "leadCommercialTargetId": strLeadCommercialTargetId] as [String: Any]
        
        let dictTarget = dictTemp as NSDictionary
        objGlobalVC.dictTarget = dictTarget;
        
        if("\(dataGeneralInfo.value(forKey: "statusSysName") ?? "")".lowercased() == "complete" && "\(dataGeneralInfo.value(forKey: "stageSysName") ?? "")".lowercased() == "won")
        {
            
            objGlobalVC.strWoStatus = "Complete"
            
        }
        else
        {
            objGlobalVC.strWoStatus = "InComplete"
            
        }
        objGlobalVC.strWoStatus = "InComplete"
        
        self.navigationController?.present(objGlobalVC, animated: false, completion: nil)
        
    }
    
    @objc func btnDeleteTargetAction(sender: UIButton!){
        
        let obj =  arrOfTagests[sender.tag] as! NSManagedObject
        self.deleteTargetFromCoreData(strTargetSysName: "\(obj.value(forKey: "targetSysName") ?? "")", strLeadCommericalId: "\(obj.value(forKey: "leadCommercialTargetId") ?? "")", strLeadMobileId: "\(obj.value(forKey: "mobileTargetId") ?? "")")
    }
    @objc func btnEditTargetDescriptionAction(sender: UIButton!){
        
        let obj =  arrOfTagests[sender.tag] as! NSManagedObject
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.strfrom = "EditTargetFromSelectService"
        vc.strLeadId = self.strLeadId as String
        vc.strHtml = "\(obj.value(forKey: "targetDescription") ?? "")"
        vc.strServiceId = "\(obj.value(forKey: "targetSysName") ?? "")"
        vc.modalPresentationStyle = .fullScreen
        self.isEditInSalesAuto = true
        self.present(vc, animated: false, completion: nil)
    }
    
    //MARK: - Actions Of Scope Cell
    
    @objc func btnDeleteScopeAction(sender: UIButton!){
        let obj =  arrOfScopes[sender.tag] as! NSManagedObject
        self.deleteScopeFromCoreData(strScopeSysName: "\(obj.value(forKey: "scopeSysName") ?? "")")
    }
    
    @objc func btnEditScopeDescriptionAction(sender: UIButton!){
        
        let obj = self.arrOfScopes[sender.tag] as! NSManagedObject
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.strfrom = "EditScopeFromSelectService"
        vc.strLeadId = self.strLeadId as String
        vc.strHtml = "\(obj.value(forKey: "scopeDescription") ?? "")"
        vc.strServiceId = "\(obj.value(forKey: "scopeSysName") ?? "")"
        vc.modalPresentationStyle = .fullScreen
        self.isEditInSalesAuto = true
        self.present(vc, animated: false, completion: nil)
    }
}


extension NewCommerialSelectServiceViewController : DatePickerProtocol, PaymentInfoDetails{
    
    
    func openMonthPopup(){
        
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "-----Select-----"
        vc.strTag = 6001
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handelPaymentTable = self
        vc.aryPayment = ["-----Select-----","January","February","March","April","May","June","July","August","September","October","November","December",]
        self.present(vc, animated: false, completion: {})
        
    }
    
    func setDateSelctionForDatePickerProtocol(strDate: String, tag: Int) {
        
        if tag == 1{
            strSelectedPropsedDate = strDate
            serviceTableView.reloadData()
        }
    }
    
    func getPaymentInfo(dictData: String, index: Int) {
        
        if(dictData.count == 0){
        }else{
            if dictData == "-----Select-----"{
                self.isEditInSalesAuto = true
                strSelectedPropsedMonth = ""
                serviceTableView.reloadData()
            }else{
                self.isEditInSalesAuto = true
                strSelectedPropsedMonth = dictData
                serviceTableView.reloadData()
            }
        }
        
    }
}

extension UIView {

  func dropShadow() {
      layer.masksToBounds = false
      layer.shadowColor = UIColor.black.cgColor
      layer.shadowOpacity = 0.6
      layer.shadowOffset = CGSize.zero
      layer.shadowRadius = 2.5
      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = UIScreen.main.scale
  }
}

extension NewCommerialSelectServiceViewController{
    
    func getCategoryList() -> NSMutableArray{
        let arrDepartment = NSMutableArray()
        let arrOfCatergories = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") != nil
        {
            let  dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictSalesLeadMaster.value(forKey: "BranchMasters") is NSArray
            {
                let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
                let strBranchSysName = nsud.value(forKey: "branchSysName") as? String
                
                for itemBranch in arrDept
                {
                    let dictBranch = itemBranch as! NSDictionary
                    
                    let arrTemp = dictBranch.value(forKey: "Departments") as! NSArray
                    
                    for itemDept in arrTemp
                    {
                        let dictDept = itemDept as! NSDictionary
                        
                        if ("\(dictBranch.value(forKey: "SysName")!)") == strBranchSysName
                        {
                            let strIsActive = dictDept.value(forKey: "IsActive") as? String
                            if strIsActive == "false" || strIsActive == "0"
                            {
                                
                            }
                            else
                            {
                                arrDepartment.add(dictDept)
                            }
                        }
                    }
                    
                }
            }
        }
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if dictMaster.value(forKey: "Categories") is NSArray
            {
                let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
                
                for item in arrDepartment{
                    
                    let dictDepartment = item as! NSDictionary
                    
                    let strSysName = "\(dictDepartment.value(forKey: "SysName")!)"
                    
                    for cat in arrCategory{
                        let dictCat = cat as! NSDictionary
                        if "\(dictCat.value(forKey: "DepartmentSysName")!)" == strSysName{
                            
                            if "\(dictCat.value(forKey: "IsActive")!)" == "1" || "\(dictCat.value(forKey: "IsActive")!)".lowercased() == "true".lowercased()
                            {
                                arrOfCatergories.add(dictCat)
                            }
                        }
                    }
                    
                }
            }
        }
        return arrOfCatergories
    }
    
    func getServiceForCategory(obj: NSDictionary) -> NSArray
    {
       let arrOfServicesList = obj.value(forKey: "Services") as? NSArray ?? []
        return arrOfServicesList
    }
    
    
    func getServiceNameFromSysName(objOfService: NSArray , strSysName: String) -> String{
        var result = ""
        
        for i in 0..<objOfService.count{
            
            let dict  = objOfService[i] as! NSDictionary
            if strSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                result = "\(dict.value(forKey: "Name") ?? "")"
                
                break
            }
        }
        
        return result
    }
    
    
    func getBillingFrequency() -> NSMutableArray
    {
        var arrTempFreq = NSMutableArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "Frequencies") is NSMutableArray){
                
                arrTempFreq = dictMaster.value(forKey: "Frequencies")  as! NSMutableArray
                
                //Frequency as per type
                
                let arrTemp = NSMutableArray()
                
                if (arrTempFreq.count  > 0)
                {
                    for item in arrTempFreq
                    {
                        let dict = item as! NSDictionary
                        let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                        if strType == "Billing" || strType == "Both" || strType == ""
                        {
                            arrTemp.add(dict)
                        }
                    }
                }
                arrTempFreq = arrTemp
            }
        }
        return arrTempFreq
    }
    
    func getBillingFrequncyNameFromSysName(strSysName: String) -> String{
        var result = ""
        let arrFrequency = getBillingFrequency()
        
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
                if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
                {
                    result = "\(dict.value(forKey: "FrequencyName") ?? "")"
                    break
                }
              
        }
        return result
    }
    
    func getCategoryNameFromSysName(strSysName: String) -> NSDictionary
    {
        var dictObject = NSDictionary()
        let arrCategory = getCategoryList()
        for itemCategory in arrCategory
        {
            let dictCategory = itemCategory as! NSDictionary
            
            if strSysName == "\(dictCategory.value(forKey: "SysName") ?? "")"
            {
                dictObject = dictCategory
                break
            }
        }
        
        return dictObject
    }

    
    func getFrequency() ->  NSMutableArray
    {
        let arrofFrequency = NSMutableArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "Frequencies") is NSArray){
                let arrTempServiceFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                if (arrTempServiceFreq.count  > 0)
                {
                    for item in arrTempServiceFreq
                    {
                        let dict = item as! NSDictionary
                        let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                        if strType == "Service" || strType == "Both" || strType == ""
                        {
                            arrofFrequency.add(dict)
                        }
                    }
                }
                
            }
        }
        
        return arrofFrequency
    }
    
    func getFrequcnyNameFromSysName(strSysName: String) -> String {
       
        var result = ""
        let arrFrequency = getFrequency()
        
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
                if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
                {
                    result = "\(dict.value(forKey: "FrequencyName") ?? "")"
                    break
                }
              
        }
        return result
    }
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
}


extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
