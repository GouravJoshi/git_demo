//
//  NewCommercialSelectTargertViewController.swift
//  Commercial_Flow_Demo Changes 
//
//  Created by Akshay Hastekar on 12/10/21.
//

import UIKit

class NewCommercialSelectTargertViewController: UIViewController, UIGestureRecognizerDelegate, SelectTargetProtocol, HTMLEditorForCommercialDelegate {
    
    //MARK:- Outlet's
    
    @IBOutlet weak var serviceDiscriptionTxtView: UITextView!
    @IBOutlet weak var txtTarget: ACFloatingTextField!
    @IBOutlet weak var nextImgeView: UIImageView!
    
    //MARK: - Variables
    
    var objTargetsList = NSMutableArray()
    var strIsChangedTargetDesc = ""
    var objOfSelectedTarget = [String : Any]()
    @objc var strLeadId = NSString()
    var globalTextOfTargetDesc = String()

    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         userIntraction()
         gesture()
         getTargetFromCoreData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
    }
    
    // MARK:- User intraction function
    func userIntraction(){
      //  self.serviceDiscriptionTxtView.isUserInteractionEnabled = false
    }
    
    //MARK:- Gesture
    
    func gesture(){
        self.txtTarget.isUserInteractionEnabled = true
        self.nextImgeView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTargert))
        tap.delegate = self
        let tapOnImage = UITapGestureRecognizer(target: self, action: #selector(tapOnTargert))
        tapOnImage.delegate = self
        self.txtTarget.addGestureRecognizer(tap)
        self.nextImgeView.addGestureRecognizer(tapOnImage)
    }
    
    //MARK: - Custom functions
    
    func didReceiveHTMLEditorText(strText: String, strFrom: String) {
        serviceDiscriptionTxtView.attributedText = getAttributedHtmlStringUnicode(strText: strText)
        globalTextOfTargetDesc = strText

    }
    
   //MARK:- Action Button
    
    @IBAction func btnBackAction (_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction (_ sender : UIButton){
        
        let isTargetIsAlreadyExist = checkTargetAvailableInCoreData()
        
        let strTargetSysName = objOfSelectedTarget["SysName"] as? String ?? ""
        
        if (strTargetSysName.count == 0 || strTargetSysName == "")
        {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please select Target", viewcontrol: self)
        }
        else if isTargetIsAlreadyExist == false{
            saveTargetInCoreData()
            self.navigationController?.popViewController(animated: true)
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Target already taken", viewcontrol: self)
        }
    }
    
    @IBAction func btnCancleAction (_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func serviceDiscriptionBtn(_ sender: UIButton) {
        if txtTarget.text?.count == 0{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please select Target", viewcontrol: self)
        }
        else{
            
            let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
            vc.delegateCommerical = self
            vc.strfrom = "EitherTargetOrScope"
            vc.strHtml = globalTextOfTargetDesc
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    //MARK:- Addtional function
    @objc func tapOnTargert(){
        print("TargetViewController")
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommerialTargetViewController) as! NewCommerialTargetViewController
        nextViewController.strSeelctedTarget = txtTarget.text ?? ""
        nextViewController.delegateToSelectTargetProtocol = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    //MARK: -Delegate Functions
    func SelectedTarget(selectedTargetObj: [String : Any]) {
        objOfSelectedTarget = selectedTargetObj
        txtTarget.text = selectedTargetObj["Name"] as? String ?? ""
        serviceDiscriptionTxtView.attributedText = getAttributedHtmlStringUnicode(strText: selectedTargetObj["Description"] as? String ?? "")
        globalTextOfTargetDesc = selectedTargetObj["Description"] as? String ?? ""
    }
    
    func checkIfTargetDescriptionIsChanged(){
        if globalTextOfTargetDesc != objOfSelectedTarget["Description"] as? String ?? ""{
            strIsChangedTargetDesc = "true"
        }
        else{
            strIsChangedTargetDesc = "false"
        }
    }
    
    //MARK: - CoreData Functions
    
    func saveTargetInCoreData(){//LeadCommercialTargetExtDc
        
        checkIfTargetDescriptionIsChanged()
        
        print(objOfSelectedTarget)
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("isChangedTargetDesc")
        arrOfKeys.add("leadCommercialTargetId")
        arrOfKeys.add("leadId")
        arrOfKeys.add("mobileTargetId")
        arrOfKeys.add("name")
        arrOfKeys.add("targetDescription")
        arrOfKeys.add("targetSysName")
        arrOfKeys.add("userName")
        arrOfKeys.add("wdoProblemIdentificationId")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(strIsChangedTargetDesc)
        arrOfValues.add("")
        arrOfValues.add(strLeadId)
        arrOfValues.add(Global().getReferenceNumber())
        arrOfValues.add(objOfSelectedTarget["Name"] as? String ?? "")
        arrOfValues.add(globalTextOfTargetDesc)
        arrOfValues.add(objOfSelectedTarget["SysName"] as? String ?? "")
        arrOfValues.add(Global().getUserName())
        arrOfValues.add("")
        
        saveDataInDB(strEntity: "LeadCommercialTargetExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    
    func getTargetFromCoreData(){
        objTargetsList.removeAllObjects()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objTargetsList.add(dict)
            }
        }
    }
    
    func checkTargetAvailableInCoreData() -> Bool{
        
        var isTargetIsAlreadyExist = Bool()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@ && targetSysName=%@", strLeadId, objOfSelectedTarget["SysName"] as? String ?? ""))
        
        if arryOfData.count > 0 {
            isTargetIsAlreadyExist = true
        }
        else{
            isTargetIsAlreadyExist = false
        }
        
        return isTargetIsAlreadyExist
    }

}
