//
//  NewCommerialTargetViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Nilind Sharma on 13/10/21.
//   change on 15 june 

import UIKit
import Foundation


protocol SelectTargetProtocol: class {
    
    func SelectedTarget(selectedTargetObj: [String: Any])
}
class NewCommerialTargetViewController: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var targetTableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet var btnCancelSearch: UIButton!
    
    //MARK:- VARIABLES
    var arrOfTargets = [[String: Any]]()
    weak var delegateToSelectTargetProtocol: SelectTargetProtocol?
    var arrOfSearchList = [[String: Any]]()
    var isSeaching = Bool()
    var strSeelctedTarget = ""
    //MARK: - Vie life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        delegate_dataSource()
        getTargets()
    }
    
    //MARK:- delegate datasource tableview
    func delegate_dataSource(){
        self.targetTableView.delegate = self
        self.targetTableView.dataSource = self
        self.targetTableView.tableFooterView = UIView()
        txtSearch.delegate = self
    }
    
    //MARK: - Get Targets List changes 
    func getTargets(){
        arrOfTargets.removeAll()
        let defaults = UserDefaults.standard
        let dictMasters = defaults.value(forKey: "MasterSalesAutomation") as? [String: Any]
        let arrOfDictsOfTargets = dictMasters!["Targets"] as? [[String: Any]]
        if (arrOfDictsOfTargets?.count ?? 0) > 0
        {
            for i in 0..<arrOfDictsOfTargets!.count
            {
                let obj = arrOfDictsOfTargets![i]
                if String(describing: obj["IsActive"] as? NSNumber ?? 0) == "1"{
                    arrOfTargets.append(obj)
                }
            }
        }
        print(arrOfTargets)
    }
    
    //MARK: - Action Button
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancleAction(_ sender: UIButton) {
        
        isSeaching = false
        arrOfSearchList.removeAll()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        targetTableView.reloadData()
        
    }
    @IBAction func btnOnClickVoiceRecognizationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrOfTargets.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
}

extension NewCommerialTargetViewController : UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return arrOfTargets.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isSeaching == true {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.text = arrOfSearchList[indexPath.row]["Name"] as? String ?? ""
            
            if strSeelctedTarget == arrOfSearchList[indexPath.row]["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell ?? UITableViewCell()
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.text = arrOfTargets[indexPath.row]["Name"] as? String ?? ""
            
            
            if strSeelctedTarget == arrOfTargets[indexPath.row]["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell ?? UITableViewCell()
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isSeaching == true {
            let obj = arrOfSearchList[indexPath.row]
            delegateToSelectTargetProtocol?.SelectedTarget(selectedTargetObj: obj)
            self.navigationController?.popViewController(animated: true)
            
        }else {
            let obj = arrOfTargets[indexPath.row]
            delegateToSelectTargetProtocol?.SelectedTarget(selectedTargetObj: obj)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                self.targetTableView.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrOfTargets.filter({
                    // this is where you determine whether to include the specific element, $0
                    return ($0["Name"]! as! String).lowercased().contains(searchedText.lowercased()) //&& ($0["compType"]! as! String).contains(txtSearch.text!)
                    // or whatever search method you're using instead
                })
                self.isSeaching = true
                targetTableView.reloadData()
                print(arrOfSearchList)
                
            } else {
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                targetTableView.reloadData()
                
            }
            return true
        }
    }
    
    
}
extension NewCommerialTargetViewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if str != ""{
            if #available(iOS 13.0, *) {
                self.txtSearch.text = str
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrOfTargets.filter({
                    return ($0["Name"]! as! String).lowercased().contains(str.lowercased())
                })
                self.isSeaching = true
                targetTableView.reloadData()
                print(arrOfSearchList)
            } else {
                self.txtSearch.text = str
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrOfTargets.filter({
                    return ($0["Name"]! as! String).lowercased().contains(str.lowercased())
                })
                self.isSeaching = true
                targetTableView.reloadData()
                print(arrOfSearchList)
            }
        }
    }
}
