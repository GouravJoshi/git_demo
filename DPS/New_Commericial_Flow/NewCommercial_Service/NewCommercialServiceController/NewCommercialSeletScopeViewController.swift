//
//  NewCommercialSeletScopeViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Nilnd Sharma on 18/10/21.
// changes

import UIKit

protocol SelectScopeProtocol: class {
    
    func SelectScopeProtocol(selectedScopeObj: [String: Any])
}
class NewCommercialSeletScopeViewController: UIViewController {
    
    //MARK:- outlet
    
    @IBOutlet weak var ScopeTableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet var btnCancelSearch: UIButton!
    
    //MARK:- variables
    var arrofScopes = [[String: Any]]()
    weak var delegateToSelectScopeProtocol: SelectScopeProtocol?
    var arrOfSearchList = [[String: Any]]()
    var isSeaching = Bool()
    var strScopeSelected = ""
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        delegate_dataSource()
        getScopes()
        
    }
    
    //MARK:- delegate datasource tableview
    
    func delegate_dataSource(){
        self.ScopeTableView.delegate = self
        self.ScopeTableView.dataSource = self
        self.ScopeTableView.tableFooterView = UIView()
        txtSearch.delegate = self
    }
    
    func getScopes(){
        arrofScopes.removeAll()
        let defaults = UserDefaults.standard
        let dictMasters = defaults.value(forKey: "MasterSalesAutomation") as? [String: Any]
        let arrOfDictsOfTargets = dictMasters!["ScopeMaster"] as? [[String: Any]]
        
        if (arrOfDictsOfTargets?.count ?? 0) > 0
        {
            for i in 0..<arrOfDictsOfTargets!.count{
                let obj = arrOfDictsOfTargets![i]
                arrofScopes.append(obj)
            }
        }
        print(arrofScopes)
        ScopeTableView.reloadData()
    }

   //MARK:- actions
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancleAction(_ sender: UIButton) {
        
        isSeaching = false
        arrOfSearchList.removeAll()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        ScopeTableView.reloadData()
        
    }
    @IBAction func btnOnClickVoiceRecognizationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrofScopes.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
}


extension NewCommercialSeletScopeViewController : UITableViewDelegate , UITableViewDataSource ,UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return arrofScopes.count
        }
       
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isSeaching == true {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.text = arrOfSearchList[indexPath.row]["Title"] as? String ?? ""
            
            if strScopeSelected == arrOfSearchList[indexPath.row]["Title"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell ?? UITableViewCell()
            
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = arrofScopes[indexPath.row]["Title"] as? String ?? ""
            
            if strScopeSelected == arrofScopes[indexPath.row]["Title"] as? String ?? ""{
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isSeaching == true {
            let obj = arrOfSearchList[indexPath.row]
            delegateToSelectScopeProtocol?.SelectScopeProtocol(selectedScopeObj: obj)
            print("orint")
            self.navigationController?.popViewController(animated: true)
            
        }else {
            let obj = arrofScopes[indexPath.row]
            delegateToSelectScopeProtocol?.SelectScopeProtocol(selectedScopeObj: obj)
            print("orint")
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                self.ScopeTableView.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrofScopes.filter({
                    // this is where you determine whether to include the specific element, $0
                    return ($0["Title"]! as! String).lowercased().contains(searchedText.lowercased()) //&& ($0["compType"]! as! String).contains(txtSearch.text!)
                    // or whatever search method you're using instead
                })
                self.isSeaching = true
                ScopeTableView.reloadData()
                print(arrOfSearchList)
                print(arrOfSearchList.count)
                
            } else {
                self.arrOfSearchList.removeAll()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                ScopeTableView.reloadData()
                
            }
            return true
        }
    }
    
    
}
extension NewCommercialSeletScopeViewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if str != ""{
            if #available(iOS 13.0, *) {
                self.txtSearch.text = str
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrofScopes.filter({
                    return ($0["Title"]! as! String).lowercased().contains(str.lowercased())
                })
                self.isSeaching = true
                ScopeTableView.reloadData()
                print(arrOfSearchList)
            } else {
                self.txtSearch.text = str
                self.arrOfSearchList.removeAll()
                btnCancelSearch.isHidden = false
                arrOfSearchList = arrofScopes.filter({
                    return ($0["Title"]! as! String).lowercased().contains(str.lowercased())
                })
                self.isSeaching = true
                ScopeTableView.reloadData()
                print(arrOfSearchList)
            }
        }
    }
}
