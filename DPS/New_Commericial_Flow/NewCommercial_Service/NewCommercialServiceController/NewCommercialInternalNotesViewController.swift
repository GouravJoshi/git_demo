//
//  NewCommercialInternalNotesViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 20/10/21.
//

import UIKit

protocol EditInternalNotesProtocol: class {
    
    func EditInternalNotes(strDescription: String)
    func EditTermsOfServiceNotes(strDescription: String)
}

class NewCommercialInternalNotesViewController: UIViewController {
    
    
    @IBOutlet var navTitle: UILabel!
    @IBOutlet weak var internalNoteTxtView: UITextView!
    weak var delegateInternalNotes : EditInternalNotesProtocol?
    var navStrTitle = ""
    var InternalNotesStr : String?
    var flag = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.internalNoteTxtView.text = InternalNotesStr
        navTitle.text = navStrTitle
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if flag == true{
            delegateInternalNotes?.EditTermsOfServiceNotes(strDescription: internalNoteTxtView.text)
            self.dismiss(animated: false)
        }else{
            delegateInternalNotes?.EditInternalNotes(strDescription: internalNoteTxtView.text)
            self.dismiss(animated: false)
        }
        

    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    
}
