//
//  NewCommercialServiceCategoryVIewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 19/10/21.
//

import UIKit

protocol SelectServiceCategoryProtocol: class {
    
    func SelectedCategory(selectedCategoryObj: NSDictionary)
}

class NewCommercialServiceCategoryVIewController: UIViewController {
    
    //MARK:- outlets
    
    @IBOutlet weak var serviceCaterogyTableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var btnCancle: UIButton!
    

    //MARK:- VARIABLES
    
    var arrofCategoriesList = NSMutableArray()
    var strTitle = ""
    weak var delegateToSelectCactegoryProtocol: SelectServiceCategoryProtocol?
    var strCategory = ""
    var arrOfSearchList = NSMutableArray()
    var isSeaching = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = strTitle
        delegate_dataSource()
        
    }
    
    //MARK:- delegate datasource tableview
    
    func delegate_dataSource(){
        self.serviceCaterogyTableView.delegate = self
        self.serviceCaterogyTableView.dataSource = self
        self.serviceCaterogyTableView.tableFooterView = UIView()
        txtSearch.delegate =  self
    }
    
    
    //MARK: - Actions
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func actionOnCancle(_ sender: Any) {
        isSeaching = false
        arrOfSearchList.removeAllObjects()
        txtSearch.text = ""
        btnCancle.isHidden = true
        serviceCaterogyTableView.reloadData()
        
    }
    @IBAction func btnOnClickVoiceRecognizationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrofCategoriesList.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }

}
extension NewCommercialServiceCategoryVIewController : UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return arrofCategoriesList.count
        }
       
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if self.isSeaching == true {
            
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            let obj = arrOfSearchList[indexPath.row] as? NSDictionary
            cell?.textLabel?.text = obj?["Name"] as? String ?? ""
            cell?.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            if strCategory == obj?["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell ?? UITableViewCell()
            
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            let dict = arrofCategoriesList[indexPath.row] as? NSDictionary
            
            cell?.textLabel?.text = dict?["Name"] as? String ?? ""
            cell?.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

            if strCategory == dict?["Name"] as? String ?? ""{
                cell?.accessoryType = .checkmark
            }
            else{
                cell?.accessoryType = .none
            }
            
            if DeviceType.IS_IPAD{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
            }
            else{
                cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)

            }
            return cell ?? UITableViewCell()
            
        }
  
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isSeaching == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
            let dict = arrOfSearchList[indexPath.row] as! NSDictionary
            if dict.count > 0{
               delegateToSelectCactegoryProtocol?.SelectedCategory(selectedCategoryObj: dict)
                self.dismiss(animated: false, completion: nil)
            }
            else{
                showToastForSomeTime(title: "Alert!", message: "No service avaialble", time: 2, viewcontrol: self)
            }
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        let dict = arrofCategoriesList[indexPath.row] as! NSDictionary
        if dict.count > 0{
           delegateToSelectCactegoryProtocol?.SelectedCategory(selectedCategoryObj: dict)
            self.dismiss(animated: false, completion: nil)
        }
        else{
            showToastForSomeTime(title: "Alert!", message: "No service avaialble", time: 2, viewcontrol: self)
        }
        }
    }
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancle.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                self.serviceCaterogyTableView.reloadData()
                btnCancle.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList = NSMutableArray()
                btnCancle.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", searchedText)
                let filtered = arrofCategoriesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceCaterogyTableView.reloadData()
                
            } else {
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                btnCancle.isHidden = true
                serviceCaterogyTableView.reloadData()
                
            }
                                                                          
        }
        return true
    }
}

extension NewCommercialServiceCategoryVIewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if str != ""{
            if #available(iOS 13.0, *) {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancle.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", str)
                let filtered = arrofCategoriesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceCaterogyTableView.reloadData()
            } else {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancle.isHidden = false
                let predicate = NSPredicate(format: "Name contains[cd] %@", str)
                let filtered = arrofCategoriesList.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                serviceCaterogyTableView.reloadData()
            }
        }
    }
}
