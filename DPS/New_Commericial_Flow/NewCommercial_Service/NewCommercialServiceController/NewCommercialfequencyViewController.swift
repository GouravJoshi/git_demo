//
//  NewCommercialfequencyViewController.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 20/10/21.
//

import UIKit

protocol SelectFrequencyProtocol: class {
    
    func SelectedFrequncy(selectedFrequncyObj: NSDictionary, tag: Int)
}

class NewCommercialfequencyViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var tblFrequency: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet var btnCancelSearch: UIButton!
    
    //MARK: - VARIABLES
    var arrOfSearchList = NSMutableArray()
    var isSeaching = Bool()
    weak var delegateToSelectFrequecnyProtocol: SelectFrequencyProtocol?
    var arrofFrequency = NSMutableArray()
    var tagTemp = Int()
    var strSelectedFrequncy = ""
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if tagTemp == 2{
            lblTitle.text = "Select Billing Frequency"
        }
        self.tblFrequency.delegate = self
        self.tblFrequency.dataSource = self
        self.txtSearch.delegate = self
    }
    
    //MARK: - IBActions
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionOnCancle(_ sender: Any) {
        isSeaching = false
        arrOfSearchList.removeAllObjects()
        txtSearch.text = ""
        btnCancelSearch.isHidden = true
        tblFrequency.reloadData()
    }
    
    @IBAction func btnOnClickVoiceRecognizationAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.arrofFrequency.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
}

extension NewCommercialfequencyViewController : UITableViewDelegate , UITableViewDataSource, UITextFieldDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSeaching == true {
            return arrOfSearchList.count
        }
        else{
            return arrofFrequency.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialFrequencyCell", for: indexPath) as? NewCommericialFrequencyCell
        
        if self.isSeaching == true {
            let dict = arrOfSearchList.object(at: indexPath.row) as! NSDictionary
            
             if (dict.value(forKey: "FrequencyName") != nil) {
                cell?.textLabel?.text = "\(dict.value(forKey: "FrequencyName")as! String)"
                 if strSelectedFrequncy == "\(dict.value(forKey: "FrequencyName")as! String)"{
                     cell?.accessoryType = .checkmark
                 }
                 else{
                     cell?.accessoryType = .none
                 }
            }
            
           
            
        }
        else{
            let dict = arrofFrequency.object(at: indexPath.row) as! NSDictionary
            
             if (dict.value(forKey: "FrequencyName") != nil) {
                cell?.textLabel?.text = "\(dict.value(forKey: "FrequencyName")as! String)"
                 
                 if strSelectedFrequncy == "\(dict.value(forKey: "FrequencyName")as! String)"{
                     cell?.accessoryType = .checkmark
                 }
                 else{
                     cell?.accessoryType = .none
                 }
                 
            }
           
        }
        
        
        
        return  cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.isSeaching == true {
            let dict = self.arrOfSearchList.object(at: indexPath.row) as! NSDictionary
            self.delegateToSelectFrequecnyProtocol?.SelectedFrequncy(selectedFrequncyObj: dict, tag: tagTemp)
            self.dismiss(animated: false) {}
        }
        else{
            let dict = self.arrofFrequency.object(at: indexPath.row) as! NSDictionary
            self.delegateToSelectFrequecnyProtocol?.SelectedFrequncy(selectedFrequncyObj: dict, tag: tagTemp)
            self.dismiss(animated: false) {}
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if range.location == 0  && (string == " ") {
            btnCancelSearch.isHidden = true
            return false
        }
        else{
            if searchedText == ""{
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                self.tblFrequency.reloadData()
                btnCancelSearch.isHidden = true
                
            }
            else  if searchedText.count > 0 {
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                
                let predicate = NSPredicate(format: "FrequencyName contains[cd] %@", searchedText)
                let filtered = arrofFrequency.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                tblFrequency.reloadData()
                
            } else {
                self.arrOfSearchList.removeAllObjects()
                self.isSeaching = false
                btnCancelSearch.isHidden = true
                tblFrequency.reloadData()
                
            }
            
        }
        return true
    }
    
}
extension NewCommercialfequencyViewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if str != ""{
            if #available(iOS 13.0, *) {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                let predicate = NSPredicate(format: "FrequencyName contains[cd] %@", str)
                let filtered = arrofFrequency.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                tblFrequency.reloadData()
            } else {
                self.txtSearch.text = str
                self.arrOfSearchList = NSMutableArray()
                btnCancelSearch.isHidden = false
                let predicate = NSPredicate(format: "FrequencyName contains[cd] %@", str)
                let filtered = arrofFrequency.filtered(using: predicate)
                arrOfSearchList = (filtered as NSArray).mutableCopy() as! NSMutableArray
                self.isSeaching = true
                tblFrequency.reloadData()
            }
        }
    }
}
