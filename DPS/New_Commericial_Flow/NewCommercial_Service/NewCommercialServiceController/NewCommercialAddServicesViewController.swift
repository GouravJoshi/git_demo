
//
//  NewCommercialAddServicesViewController.swift
//  Commercial_Flow_Demo
//  Changes changes
//  Created by Akshay Hastekar on 19/10/21.
//

import UIKit

class NewCommercialAddServicesViewController: UIViewController ,UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var txtCategory: ACFloatingTextField!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var txtService: ACFloatingTextField!
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var txtFrequency: ACFloatingTextField!
    @IBOutlet weak var imageFrequency: UIImageView!
    @IBOutlet weak var txtPrice: ACFloatingTextField!
    @IBOutlet weak var lblServiceDescription: UILabel!
    @IBOutlet weak var lblInternalNotes: UILabel!
    @IBOutlet weak var txtBillingFrequncy: ACFloatingTextField!
    @IBOutlet weak var txtMaintenence: ACFloatingTextField!
    @IBOutlet var btnService: UIButton!
    @IBOutlet var btnCategory: UIButton!
    @IBOutlet var lblTermsOfService: UILabel!
    @IBOutlet var btnTermsOfService: UIButton!
    @IBOutlet var btnBillingFrequency: UIButton!
    @IBOutlet var lblTermasAndConditionDescription: UILabel!
    @IBOutlet var btnStandard: UIButton!
    @IBOutlet var btnCustom: UIButton!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var lblStandardBottom: UILabel!
    @IBOutlet var lblCustomBottom: UILabel!
    
    //MARK: - Variables
    var strLeadId = ""
    var strIsChangedServiceDesc = ""
    var navigationTitle : String?
    var isHiddenTxtMaintenence = false
    
    var strIsFrommCustomService = Bool()
    
    var arrofCategoriesList = NSMutableArray()
    var objSelectedCategory = NSDictionary()
    
    var arrOfServicesList = NSArray()
    var objSelectedService = NSDictionary()
    
    var arrofFrequency = NSMutableArray()
    var objSelectFrequency = NSDictionary()
    var strFreqencySysName = ""
    var strForEdit = ""
    var objOfEditService = NSManagedObject()
    var strCategory = ""
    var arrOfBillingFrequncy = NSMutableArray()
    var objSelectBillingFrequncy = NSDictionary()
    var strBillingFreqencySysName = ""
    
    var globalServiceDescription = ""
    var globalInternalDescription = ""
    var globalTermsAndCOndition = ""
    
    var arrServiceMasterRenewalPrices = NSArray()
    var strSoldServiceStandardId = String()

    var strServiceDescription = ""
    //MARK: - view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle()
        // userIntraction()
        getFrequency()
        setDefaultBillingFrequency()
        setViewForCustomServices()
        txtPrice.delegate = self
        txtMaintenence.delegate = self
        setUIForEditService()
        
    }
    
    
    // MARK: - UIButton Action
    @IBAction func btnCategory(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialServiceCategoryVIewController) as! NewCommercialServiceCategoryVIewController
        
        nextViewController.delegateToSelectCactegoryProtocol = self
        if strIsFrommCustomService == true{
            nextViewController.arrofCategoriesList = getDepartmentNonStan()
            nextViewController.strTitle = "Select Department"
            nextViewController.strCategory = txtCategory.text ?? ""
            
        }else{
            
            if Global().getCategoryDeptWiseGlobal().count > 0{
                let tempArray = (Global().getCategoryDeptWiseGlobal()! as NSArray).mutableCopy() as! NSMutableArray
                nextViewController.arrofCategoriesList = tempArray
                nextViewController.strTitle = "Select Category"
                nextViewController.strCategory = txtCategory.text ?? ""
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Category list not found", viewcontrol: self)
            }
            
        }
        self.navigationController?.present(nextViewController, animated: false, completion: nil)
    }
    
    @IBAction func btnService(_ sender: Any) {
        
        if strIsFrommCustomService == true{
            if txtCategory.text == ""{
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department first", viewcontrol: self)
            }else{
                if arrOfServicesList.count > 0 {
                let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.serviceViewController) as! NewCommercialServiceViewController
                
                nextViewController.strSelectedservice = txtService.text ?? ""
                nextViewController.arrOfServicesList = arrOfServicesList
                nextViewController.delegateToSelectServiceProtocol = self
                self.navigationController?.present(nextViewController, animated: false, completion: nil)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data available", viewcontrol: self)
                }
            }
            
        }else {
            if txtCategory.text == ""{
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            }else{
                if arrOfServicesList.count > 0 {
                let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.serviceViewController) as! NewCommercialServiceViewController
                
                nextViewController.strSelectedservice = txtService.text ?? ""
                nextViewController.arrOfServicesList = arrOfServicesList
                nextViewController.delegateToSelectServiceProtocol = self
                self.navigationController?.present(nextViewController, animated: false, completion: nil)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "No data available", viewcontrol: self)
                }
            }
        }
        
    }
    @IBAction func btnFrequency(_ sender: UIButton) {
        if strIsFrommCustomService == true{
            
            if txtService.text == "" && txtCategory.text == "" {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Department and enter service name", viewcontrol: self)
            }else if txtService.text == ""  {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service first", viewcontrol: self)
            }else if txtCategory.text == "" {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select Department first", viewcontrol: self)
            } else{

                let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewCommercialfequencyViewController") as! NewCommercialfequencyViewController
                nextViewController.tagTemp = 1
                nextViewController.strSelectedFrequncy = txtFrequency.text ?? ""
                nextViewController.arrofFrequency = arrofFrequency
                nextViewController.delegateToSelectFrequecnyProtocol = self
                self.navigationController?.present(nextViewController, animated: false, completion: nil)
            }
            
        }else {
            if txtService.text == "" && txtCategory.text == "" {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service, category first", viewcontrol: self)
            }
            else if txtService.text == ""  {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service first", viewcontrol: self)
            }
            else if txtCategory.text == "" {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category first", viewcontrol: self)
            }
            else{

                let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewCommercialfequencyViewController") as! NewCommercialfequencyViewController
                nextViewController.arrofFrequency = arrofFrequency
                nextViewController.tagTemp = 1
                nextViewController.strSelectedFrequncy = txtFrequency.text ?? ""
                nextViewController.delegateToSelectFrequecnyProtocol = self
                self.navigationController?.present(nextViewController, animated: false, completion: nil)
            }
        }
        
        
    }
    
    @IBAction func btnBillingFquency(_ sender: UIButton) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewCommercialfequencyViewController") as! NewCommercialfequencyViewController
        nextViewController.arrofFrequency = arrOfBillingFrequncy
        nextViewController.strSelectedFrequncy = txtBillingFrequncy.text ?? ""
        nextViewController.tagTemp = 2
        nextViewController.delegateToSelectFrequecnyProtocol = self
        self.navigationController?.present(nextViewController, animated: false, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditServiceDescriptionAction(_ sender: UIButton) {
        goToNoramlHTMLEditor(strText: globalServiceDescription, strFrom: "EditServiceDescription")
    }
    
    @IBAction func btnEditInternalNotesAction(_ sender: UIButton) {
        goToNoramlHTMLEditor(strText: globalInternalDescription, strFrom: "EditInternalNotes")
    }
    
    @IBAction func btnEditTermsOfServiceAction(_ sender: UIButton) {
        goToNoramlHTMLEditor(strText: globalTermsAndCOndition, strFrom: "EditTermsAndCondition")
    }
    
    @IBAction func actionOnStandard(_ sender: Any) {
        self.navTitle.text = "Add Standard Service"
        strIsFrommCustomService = false
        clearDefault()
        setViewForCustomServices()
        lblStandardBottom.isHidden = false
        lblCustomBottom.isHidden = true
        btnStandard.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnCustom.setTitleColor(UIColor.black, for: .normal)
        
    }
    
    @IBAction func actionOnCustom(_ sender: Any) {
        self.navTitle.text = "Add Custom Service"
        strIsFrommCustomService = true
        clearDefault()
        setViewForCustomServices()
        lblStandardBottom.isHidden = true
        lblCustomBottom.isHidden = false
        btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnStandard.setTitleColor(UIColor.black, for: .normal)
        
    }
    
    @IBAction func btnAddStandardService(_ sender: Any)
    {
        if strIsFrommCustomService == true{
            endEditing()
            if txtCategory.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select department", viewcontrol: self)
            }
            else if  txtService.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter service name", viewcontrol: self)
            }
            else if  txtFrequency.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            }
            else if  txtBillingFrequncy.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select billing frequency", viewcontrol: self)
            }
            else if  txtPrice.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            }
            else if  txtMaintenence.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
            }
            else
            {
                let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                self.present(loader, animated: false, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                    if strForEdit == "NonEditStandard"
                    {
                        updateNonStandardServiceDetail()
                    }
                    else
                    {
                        saveNonStandardServiceToCoreData()
                    }
                    Global().updateSalesModifydate(strLeadId as String)
                    loader.dismiss(animated: false) {
                        back()
                    }
                }
            }
        }
        else{
            if txtCategory.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select category", viewcontrol: self)
            }
            else if  txtService.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select service", viewcontrol: self)
            }
            else if  txtFrequency.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select frequency", viewcontrol: self)
            }
            else if  txtBillingFrequncy.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select billing frequency", viewcontrol: self)
            }
            else if  txtPrice.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter initial price", viewcontrol: self)
            }
            else if  txtMaintenence.text == ""
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter maintenance price", viewcontrol: self)
            }
            else if  chkserviceExistOrNot() == true
            {
                showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Service already exist", viewcontrol: self)
            }
            else
            {
                if strForEdit == "EditStandard"
                {
                    
                    let arrayData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@ ", strLeadId))
                    
                    if arrayData.count > 0
                    {
                        let alert = UIAlertController(title: Alert, message: "Edit in price will delete all the coupon, credit and discount associated with service, Do you want to proceed ?", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ (UIAlertAction)in
                            
                            // later will open self.updateServiceDetail()
                            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(loader, animated: false, completion: nil)

                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                                
                                self.deleteCouponCredit()
                                self.updateStandardServiceDetail()
                                Global().updateSalesModifydate(self.strLeadId as String)
                                loader.dismiss(animated: false) {
                                    self.back()
                                }
                            }
                            
                        }))
                        alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                            
                        }))
                        
                        alert.popoverPresentationController?.sourceView = self.view
                        self.present(alert, animated: true, completion: {
                        })
                    }
                    else
                    {
                        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                        self.present(loader, animated: false, completion: nil)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                            self.updateStandardServiceDetail()
                            Global().updateSalesModifydate(self.strLeadId as String)
                            loader.dismiss(animated: false) {
                                self.back()
                            }
                        }
                    }
                    
                }
                else
                {
                    let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loader, animated: false, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                        saveStandardServiceToCoreData()
                        addRenewalService()
                        Global().updateSalesModifydate(strLeadId as String)
                        loader.dismiss(animated: false) {
                            back()
                        }
                    }
                }
                
            }
        }
    }
    
    //MARK: - Custom Functions
    
    func goToNoramlHTMLEditor(strText : String, strFrom: String) {
        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.delegateCommerical = self
        vc.strfrom = strFrom
        vc.strHtml = strText
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    func getBillingFrequency() -> NSMutableArray
    {
        var arrTempFreq = NSMutableArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "Frequencies") is NSArray){
                
                arrTempFreq = (dictMaster.value(forKey: "Frequencies") as! NSArray).mutableCopy() as! NSMutableArray
                
                //Frequency as per type
                
                let arrTemp = NSMutableArray()
                
                if (arrTempFreq.count  > 0)
                {
                    for item in arrTempFreq
                    {
                        let dict = item as! NSDictionary
                        let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                        if strType == "Billing" || strType == "Both" || strType == ""
                        {
                            arrTemp.add(dict)
                        }
                    }
                }
                arrTempFreq = arrTemp
            }
        }
        return arrTempFreq
    }
    
    func back()
    {
        clearDefault()
        navigationController?.popViewController(animated: false)
    }
    func clearDefault()
    {
        nsud.set("", forKey: "htmlContents")
        nsud.synchronize()
        
        txtCategory.text = ""
        txtService.text = ""
        txtPrice.text = ""
        lblInternalNotes.text = ""
        lblServiceDescription.text = ""
        txtMaintenence.text = ""
        txtFrequency.text = ""
        strIsChangedServiceDesc = ""
        globalServiceDescription = ""
        globalInternalDescription = ""
        globalTermsAndCOndition = ""
        arrofCategoriesList = NSMutableArray()
        objSelectedCategory = NSDictionary()
        arrOfServicesList = NSArray()
        objSelectedService = NSDictionary()
        arrofFrequency = NSMutableArray()
        objSelectFrequency = NSDictionary()
        getFrequency()
        
    }
    
    func setUIForEditService(){
        
        if strForEdit == "EditStandard"{
            if strIsFrommCustomService == false{
                btnSave.setTitle("Save", for: .normal)
                
                btnCategory.isUserInteractionEnabled = false
                btnService.isUserInteractionEnabled = false
                btnCustom.isUserInteractionEnabled = false
                btnStandard.isUserInteractionEnabled = false
                
                
                objSelectedCategory = getCategoryObjectFromServiceSysName(strServiceSysName: "\(objOfEditService.value(forKey: "serviceSysName") ?? "")")
                let objOfServices = getServiceForCategory(obj: objSelectedCategory)
                let strServiceName = getServiceNameFromSysName(objOfService: objOfServices, strSysName: "\(objOfEditService.value(forKey: "serviceSysName") ?? "")" )
                print("============ \(strServiceName)")
                
                txtCategory.text = objSelectedCategory.value(forKey: "Name") as? String ?? ""
                txtService.text =  strServiceName //objOfEditService.value(forKey: "serviceSysName") as? String ?? ""
                
                txtFrequency.text = objOfEditService.value(forKey: "serviceFrequency") as? String ?? ""
                
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objOfEditService.value(forKey: "billingFrequencySysName") ?? "")")
                
                txtBillingFrequncy.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: objOfEditService.value(forKey: "serviceDescription") as? String ?? "")
                globalServiceDescription = objOfEditService.value(forKey: "serviceDescription") as? String ?? ""
                lblInternalNotes.attributedText =  getAttributedHtmlStringUnicode(strText: objOfEditService.value(forKey: "internalNotes") as? String ?? "")
                txtPrice.text = objOfEditService.value(forKey: "initialPrice") as? String ?? ""
                txtMaintenence.text = objOfEditService.value(forKey: "maintenancePrice") as? String ?? ""
                globalInternalDescription = objOfEditService.value(forKey: "internalNotes") as? String ?? ""
                
                if txtFrequency.text?.lowercased() == "OneTime".lowercased() || txtFrequency.text?.lowercased() == "One Time".lowercased()
                {
                    forOneTimeBillingFrequency()
                }
                
                strBillingFreqencySysName = "\(objOfEditService.value(forKey: "billingFrequencySysName") ?? "")"
                strFreqencySysName = "\(objOfEditService.value(forKey: "frequencySysName") ?? "")"
            }
        }
        else if strForEdit == "NonEditStandard"{
            btnSave.setTitle("Save", for: .normal)
            
            txtCategory.text = getDepartmentNameFromSysName(strSysName: objOfEditService.value(forKey: "departmentSysname") as? String ?? "")
            
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objOfEditService.value(forKey: "billingFrequencySysName") ?? "")")
            
            txtBillingFrequncy.text = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            
            txtService.text = objOfEditService.value(forKey: "serviceName") as? String ?? ""
            txtFrequency.text = objOfEditService.value(forKey: "serviceFrequency") as? String ?? ""
            lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: objOfEditService.value(forKey: "serviceDescription") as? String ?? "")
            
            lblInternalNotes.attributedText =  getAttributedHtmlStringUnicode(strText: objOfEditService.value(forKey: "internalNotes") as? String ?? "")
            txtPrice.text = objOfEditService.value(forKey: "initialPrice") as? String ?? ""
            txtMaintenence.text = objOfEditService.value(forKey: "maintenancePrice") as? String ?? ""
            lblTermasAndConditionDescription.attributedText =  getAttributedHtmlStringUnicode(strText:objOfEditService.value(forKey: "nonStdServiceTermsConditions") as? String ?? "")
            
            btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)
            btnStandard.setTitleColor(UIColor.black, for: .normal)
            btnService.isUserInteractionEnabled = false
            txtService.isUserInteractionEnabled = true
            btnStandard.isUserInteractionEnabled = false
            btnCustom.isUserInteractionEnabled = false
            globalServiceDescription = objOfEditService.value(forKey: "serviceDescription") as? String ?? ""
            globalInternalDescription = objOfEditService.value(forKey: "internalNotes") as? String ?? ""
            globalTermsAndCOndition = objOfEditService.value(forKey: "nonStdServiceTermsConditions") as? String ?? ""
            
            if txtFrequency.text?.lowercased() == "OneTime".lowercased() || txtFrequency.text?.lowercased() == "One Time".lowercased()
            {
                forOneTimeBillingFrequency()
            }
            
            strBillingFreqencySysName = "\(objOfEditService.value(forKey: "billingFrequencySysName") ?? "")"
            strFreqencySysName = "\(objOfEditService.value(forKey: "frequencySysName") ?? "")"
        }
        
    }
    //set view accordig to Service
    func setViewForCustomServices(){
        if strIsFrommCustomService == true{
            txtCategory.placeholder = "Department"
            txtService.placeholder = "Enter Service Name"
            imageService.isHidden = true
            // segmentControl.selectedSegmentIndex = 1
            self.lblTermasAndConditionDescription.isHidden = false
            self.lblTermsOfService.isHidden = false
            self.btnTermsOfService.isHidden = false
            self.lblStandardBottom.isHidden = true
            self.lblCustomBottom.isHidden = false
            arrofCategoriesList = getDepartmentNonStan()
            
            btnCustom.setTitleColor(UIColor.appThemeColor, for: .normal)
            btnStandard.setTitleColor(UIColor.black, for: .normal)
            
            btnService.isUserInteractionEnabled = false
            txtService.isUserInteractionEnabled = true
            
        }
        else{
            txtCategory.placeholder = "Category"
            txtService.placeholder = "Service"
            imageService.isHidden = false
            self.lblTermasAndConditionDescription.isHidden = true
            self.lblTermsOfService.isHidden = true
            self.btnTermsOfService.isHidden = true
            self.lblStandardBottom.isHidden = false
            self.lblCustomBottom.isHidden = true
            
            btnService.isUserInteractionEnabled = true
            txtService.isUserInteractionEnabled = false
        }
        
    }
    
    func setTitle(){
        self.navTitle.text = navigationTitle
    }
    
    func userIntraction(){
        self.lblServiceDescription.isUserInteractionEnabled = false
        self.lblInternalNotes.isUserInteractionEnabled = false
    }
    
    func deleteCouponCredit()
    {
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@ ", strLeadId))
        for item in arrayData
        {
            let data = item as! NSManagedObject
            deleteDataFromDB(obj: data)
        }
    }
    
    func chkserviceExistOrNot() -> Bool
    {
        var chkExist = Bool()
        chkExist = false
        let arryOfWorkOrderData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ AND serviceSysName == %@", strLeadId, objSelectedService.value(forKey: "SysName") as? String ?? ""))
        
        if arryOfWorkOrderData.count > 0
        {
            chkExist = true
        }
        else
        {
            chkExist = false
        }
        if strForEdit == "EditStandard"
        {
            chkExist = false
        }
        return chkExist
    }
    
    func endEditing()
    {
        self.view.endEditing(true)
    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        return String(format: "%.2f", myFloat)
    }
    
    func checkIfServiceDescriptionIsChanged(){
        if globalServiceDescription != objSelectedService["Description"] as? String ?? ""{
            strIsChangedServiceDesc = "true"
        }
        else{
            strIsChangedServiceDesc = "false"
        }
    }
    
    
    // MARK: - ----------------Renewal Service Method --------------------
    
    func addRenewalService()
    {
        
        if !checkRenewalServiceExistence(strRenewalServiceId: "\(objSelectedService.value(forKey: "ServiceMasterId") ?? "")")
        {
            var chkRenewalStatus = Bool()
            chkRenewalStatus = chkRenewalService(strCategorySysName: "\(objSelectedCategory.value(forKey: "SysName") ?? "")", strServiceSysName: "\(objSelectedService.value(forKey: "SysName") ?? "")")
            
            if arrServiceMasterRenewalPrices.count > 0 && chkRenewalStatus == true
            {
                let dict = arrServiceMasterRenewalPrices.object(at: 0) as! NSDictionary
                        
                saveRenewalServiceInToCoreData(dictRenewalData: dict, TotalInitialPrice: txtPrice.text!, ServiceId: strSoldServiceStandardId, ServiceMasterId: "\(objSelectedService.value(forKey: "ServiceMasterId") ?? "")")
                
            }
            
        }

    }
    func checkRenewalServiceExistence(strRenewalServiceId : String) -> Bool
    {
        var chkRenewalServiceExist = false
        
        let arrayData = getDataFromLocal(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@", "serviceId == %@", strLeadId, strRenewalServiceId))
        
        if arrayData.count > 0
        {
            chkRenewalServiceExist = true
        }
        
        return chkRenewalServiceExist
    }

    func chkRenewalService(strCategorySysName : String, strServiceSysName: String) -> Bool
    {
        
        var isRenwal = Bool()
        isRenwal = false
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary

            if dictMaster.value(forKey: "Categories") is NSArray
            {
                let arrCategory = dictMaster.value(forKey: "Categories") as! NSArray
               
                for item in arrCategory
                {
                    let dict = item as! NSDictionary
                    
                    if strCategorySysName == "\(dict.value(forKey: "SysName") ?? "")"
                    {
                        if dict.value(forKey: "Services") is NSArray
                        {
                            let arrService = dict.value(forKey: "Services") as! NSArray
                            for itemService in arrService
                            {
                                let dictService = itemService as! NSDictionary
                                
                                if strServiceSysName == "\(dictService.value(forKey: "SysName") ?? "")"
                                {
                                    if "\(dictService.value(forKey: "IsRenewal") ?? "")" == "1" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "true" || "\(dictService.value(forKey: "IsRenewal") ?? "")" == "True"
                                    {
                                        arrServiceMasterRenewalPrices = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                                        
                                        isRenwal = true
                                        
                                        break
                                    }
                                }
                            }
                        }
                    }
                    
                    if isRenwal == true
                    {
                        break
                    }
                    
                }
                
            }
        }
        
        return isRenwal
        
    }
    
    //MARK: - save renewal
    func saveRenewalServiceInToCoreData(dictRenewalData : NSDictionary , TotalInitialPrice strInitialPrice: String, ServiceId strServiceId: String, ServiceMasterId strServiceMasterIdRenewal: String)
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strRenewalAmount = String()
        var strRenewalPercentage = String()
                
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
        {
            strRenewalPercentage = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")"
            
            var price = Float()
            var per = Float()
            per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
            price = (Float(strInitialPrice) ?? 0.0) * per / 100
            if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            {
                price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
            }
            
            strRenewalAmount = String(format: "%.2f", price)
        }
        else
        {
            strRenewalPercentage = "0.00"
        }
        
        if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
        {
            strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
        }
       
        arrOfKeys = ["companyKey",
                     "leadId",
                     "renewalAmount",
                     "renewalDescription",
                     "renewalFrequencySysName",
                     "renewalPercentage",
                     "renewalServiceId",
                     "serviceId",
                     "soldServiceStandardId",
                     "userName"]
        
        arrOfValues = [Global().getCompanyKey(),
                       strLeadId,
                       strRenewalAmount,
                       "\(dictRenewalData.value(forKey: "Description") ?? "")",
                       "\(dictRenewalData.value(forKey: "FrequencySysName") ?? "")",
                       strRenewalPercentage,
                       strServiceMasterIdRenewal,
                       strServiceMasterIdRenewal,
                       strServiceId,
                       Global().getUserName()]
        
        saveDataInDB(strEntity: "RenewalServiceExtDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }

    //MARK: - update renewal
    func updateRenewalServiceDetail(ServiceObject dictService : NSDictionary , TotalInitialPrice strInitialPrice : String, SoldServiceId strSoldServiceStandardId: String)
    {
        let arrRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId = %@", strLeadId, strSoldServiceStandardId))
                
        if arrRenewal.count > 0
        {
            var strRenewalAmount = ""

            if dictService.value(forKey: "ServiceMasterRenewalPrices") is NSArray
            {
                let arrRenewaNaster = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                
                let arr = arrRenewaNaster.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "ServiceMasterId") ?? "")".caseInsensitiveCompare("\(dictService.value(forKey: "ServiceMasterId") ?? "")") == .orderedSame
                } as NSArray

                if arr.count > 0
                {
                    let dictRenewalData = arr.object(at: 0) as! NSDictionary
                    if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
                    {
                        var price = Float()
                        var per = Float()
                        per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
                        price = (Float(strInitialPrice) ?? 0.0 ) * per / 100
                        
                        if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
                        {
                            price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
                        }
                        
                        strRenewalAmount = String(format: "%.2f", price)
                        
                    }
                    else if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
                    {
                        strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
                    }
                    
                }

            }
            
            var arrOfKeys = NSMutableArray()
            var arrOfValues = NSMutableArray()
            
            arrOfKeys = [
                
                "renewalAmount",
            ]
            
            arrOfValues = [
                strRenewalAmount,
            ]
            
            var isSuccess = Bool()
            
            isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId, strSoldServiceStandardId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            
            
            if(isSuccess == true)
            {
                print("updated successsfully")
            }
            else
            {
                print("updates failed")
            }
            

        }
    }
    
    //MARK: - UITextField Delegate
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if ( textField == txtPrice || textField == txtMaintenence)
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            if chk == false{
                self.view.makeToast("Please enter valid entity")
            }
            return chk
        }
        return true
    }
    
}

//MARK: - extension
extension NewCommercialAddServicesViewController{
    
    func getServiceForCategory()
    {
        arrOfServicesList = NSArray()
        var arrServiceData  = NSArray()
        let arrCategory = Global().getCategoryDeptWiseGlobal()! as NSArray
        for item in arrCategory
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "SysName")!)" == "\(objSelectedCategory.value(forKey: "SysName") ?? "")"
            {
                arrServiceData = dict.value(forKey: "Services") as! NSArray
                break
            }
        }
        
        let arrData = NSMutableArray()
        
        for item in arrServiceData
        {
            let dict = item as! NSDictionary
            if "\(dict.value(forKey: "IsActive") ?? "")" == "1"
            {
                arrData.add(dict)
            }
        }
        
        arrServiceData = arrData as NSArray
        arrOfServicesList =  arrServiceData
        
        
        // arrOfServicesList = objSelectedCategory.value(forKey: "Services") as? NSArray ?? []
    }
    
    func getFrequency(){
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil){
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMaster.value(forKey: "Frequencies") is NSArray){
                let arrTempServiceFreq = dictMaster.value(forKey: "Frequencies") as! NSArray
                
                if (arrTempServiceFreq.count  > 0)
                {
                    for item in arrTempServiceFreq
                    {
                        let dict = item as! NSDictionary
                        let strType = "\(dict.value(forKey: "FrequencyType") ?? "")"
                        if strType == "Service" || strType == "Both" || strType == ""
                        {
                            arrofFrequency.add(dict)
                        }
                    }
                }
                
            }
        }
        
    }
    
    
    func getDepartmentNonStan() -> NSMutableArray{
        
        let arrDepartment = NSMutableArray()
        
        if nsud.value(forKey: "LeadDetailMaster") != nil
        {
            let  dictSalesLeadMaster = nsud.value(forKey: "LeadDetailMaster") as! NSDictionary
            
            if dictSalesLeadMaster.value(forKey: "BranchMasters") is NSArray
            {
                let arrDept = dictSalesLeadMaster.value(forKey: "BranchMasters") as! NSArray
                let strBranchSysName = "\(Global().getEmployeeBranchSysName() ?? "")"
                
                for itemBranch in arrDept
                {
                    let dictBranch = itemBranch as! NSDictionary
                    
                    let arrTemp = dictBranch.value(forKey: "Departments") as! NSArray
                    
                    for itemDept in arrTemp
                    {
                        let dictDept = itemDept as! NSDictionary
                        
                        if ("\(dictBranch.value(forKey: "SysName")!)") == strBranchSysName
                        {
                            let strIsActive = dictDept.value(forKey: "IsActive") as? String
                            if strIsActive == "false" || strIsActive == "0"
                            {
                                
                            }
                            else
                            {
                                arrDepartment.add(dictDept)
                            }
                        }
                    }
                    
                }
            }
        }
        
        return arrDepartment
    }
    //MARK: - Standard servicecs Core Data functions
    func saveStandardServiceToCoreData()
    {
        checkIfServiceDescriptionIsChanged()
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        let arrAdditionalPara = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
        
        strIntialPrice = txtPrice.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtMaintenence.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        strSoldServiceStandardId = Global().getReferenceNumberNew()
        
        arrOfKeys = ["additionalParameterPriceDcs",
                     "billingFrequencyPrice",
                     "billingFrequencySysName",
                     "bundleDetailId",
                     "bundleId",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "discount",
                     "discountPercentage",
                     "finalUnitBasedInitialPrice",
                     "finalUnitBasedMaintPrice",
                     "frequencySysName",
                     "initialPrice",
                     "isChangeServiceDesc",
                     "isCommercialTaxable",
                     "isResidentialTaxable",
                     "isSold",
                     "isTBD",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "packageId",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceId",
                     "servicePackageName",
                     "serviceSysName",
                     "serviceTermsConditions",
                     "soldServiceStandardId",
                     "totalInitialPrice",
                     "totalMaintPrice",
                     "unit",
                     "userName",
                     "internalNotes",
                     "categorySysName"]
        
        arrOfValues = [arrAdditionalPara,
                       "",
                       strBillingFreqencySysName,
                       "0",
                       "0",
                       Global().getCompanyKey(),
                       Global().getUserName(),
                       Global().strCurrentDate(),
                       "0",
                       "0",
                       strIntialPrice,
                       strMaintPrice,
                       strFreqencySysName,
                       strIntialPrice,
                       strIsChangedServiceDesc,
                       "false",
                       "false",
                       "false",
                       "false",
                       strLeadId,
                       strMaintPrice,
                       Global().getUserName(),
                       Global().modifyDate(),
                       "",
                       "",
                       "0",
                       globalServiceDescription,
                       txtFrequency.text ?? "" ,
                       "\(objSelectedService.value(forKey: "ServiceMasterId") ?? "")",
                       "",
                       "\(objSelectedService.value(forKey: "SysName") ?? "")",
                       "\(objSelectedService.value(forKey: "TermsConditions") ?? "")", //serviceTermsConditions
                       strSoldServiceStandardId,
                       strIntialPrice,
                       strMaintPrice,
                       "1",
                       Global().getUserName(),
                       globalInternalDescription,
                       "\(objSelectedCategory.value(forKey: "SysName") ?? "")"]
        
        saveDataInDB(strEntity: "SoldServiceStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    //MARK: - update Standard services
    func updateStandardServiceDetail()
    {
        checkIfServiceDescriptionIsChanged()
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
        
        strIntialPrice = txtPrice.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtMaintenence.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
                
        arrOfKeys = [
            "billingFrequencyPrice",
            "billingFrequencySysName",
            "finalUnitBasedInitialPrice",
            "finalUnitBasedMaintPrice",
            "frequencySysName",
            "initialPrice",
            "isChangeServiceDesc",
            "maintenancePrice",
            "serviceDescription",
            "serviceFrequency",
            "totalInitialPrice",
            "totalMaintPrice",
            "internalNotes"
        ]
        
        arrOfValues = [
            "",
            strBillingFreqencySysName,
            strIntialPrice,
            strMaintPrice,
            strFreqencySysName,
            strIntialPrice,
            strIsChangedServiceDesc,
            strMaintPrice,
            globalServiceDescription,
            txtFrequency.text ?? "" ,
            strIntialPrice,
            strMaintPrice,
            globalInternalDescription
        ]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "SoldServiceStandardDetail", predicate:  NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@", strLeadId, objOfEditService.value(forKey: "soldServiceStandardId") as? String ?? ""), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            print("suc")
        }
        updateRenewalServiceDetail(ServiceObject: objSelectedService, TotalInitialPrice: txtPrice.text!, SoldServiceId: objOfEditService.value(forKey: "soldServiceStandardId") as? String ?? "")
        
    }
    
    
    //MARK: - Non stadard save core data
    func saveNonStandardServiceToCoreData()
    {
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
       
        
        strIntialPrice = txtPrice.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtMaintenence.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
        
        
        arrOfKeys = ["billingFrequencyPrice",
                     "billingFrequencySysName",
                     "companyKey",
                     "createdBy",
                     "createdDate",
                     "departmentSysname",
                     "discount",
                     "discountPercentage",
                     "frequencySysName",
                     "initialPrice",
                     "isSold",
                     "leadId",
                     "maintenancePrice",
                     "modifiedBy",
                     "modifiedDate",
                     "modifiedInitialPrice",
                     "modifiedMaintenancePrice",
                     "nonStdServiceTermsConditions",
                     "serviceDescription",
                     "serviceFrequency",
                     "serviceName",
                     "soldServiceNonStandardId",
                     "userName",
                     "internalNotes",
                     "isTaxable"]
        
        arrOfValues = ["",
                       strBillingFreqencySysName,
                       Global().getCompanyKey(),
                       Global().getUserName(),
                       Global().strCurrentDate(),
                       "\(objSelectedCategory.value(forKey: "SysName") ?? "")",
                       "0",
                       "0",
                       strFreqencySysName,
                       strIntialPrice,
                       "false",
                       strLeadId,
                       strMaintPrice,
                       Global().getUserName(),
                       Global().modifyDate(),
                       strIntialPrice,
                       strMaintPrice,
                       globalTermsAndCOndition,
                       globalServiceDescription,
                       txtFrequency.text ?? "",
                       txtService.text ?? "",
                       Global().getReferenceNumber(),
                       Global().getUserName(),
                       globalInternalDescription,
                       "false"]
        
        saveDataInDB(strEntity: "SoldServiceNonStandardDetail", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    //MARK: - update Non Standard services
    func updateNonStandardServiceDetail(){
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        var strIntialPrice = String()
        var strMaintPrice = String()
        
        strIntialPrice = txtPrice.text ?? ""
        strIntialPrice =  strIntialPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strIntialPrice == ""
        {
            strIntialPrice = "0"
        }
        strMaintPrice = txtMaintenence.text ?? ""
        strMaintPrice =  strMaintPrice.trimmingCharacters(in: .whitespacesAndNewlines)
        if strMaintPrice == ""
        {
            strMaintPrice = "0"
        }
                
        arrOfKeys = [
            "billingFrequencyPrice",
            "billingFrequencySysName",
            "departmentSysname",
            "frequencySysName",
            "initialPrice",
            "maintenancePrice",
            "modifiedInitialPrice",
            "modifiedMaintenancePrice",
            "nonStdServiceTermsConditions",
            "serviceDescription",
            "serviceFrequency",
            "serviceName",
            "internalNotes",
        ]
        
        arrOfValues = [
            "",
            strBillingFreqencySysName,
            "\(objSelectedCategory.value(forKey: "SysName") ?? "")",
            strFreqencySysName,
            strIntialPrice,
            strMaintPrice,
            strIntialPrice,
            strMaintPrice,
            globalTermsAndCOndition,
            globalServiceDescription,
            txtFrequency.text ?? "",
            txtService.text ?? "",
            globalInternalDescription,
        ]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "SoldServiceNonStandardDetail", predicate:  NSPredicate(format: "leadId == %@ && soldServiceNonStandardId == %@", strLeadId, objOfEditService.value(forKey: "soldServiceNonStandardId") as? String ?? ""), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            print("suc")
        }
    }
}


extension NewCommercialAddServicesViewController: SelectServiceCategoryProtocol, SelectServiceProtocol, CustomTableView, SelectFrequencyProtocol, HTMLEditorForCommercialDelegate{
    
    
   
    //MARK: - Protocols calling
    func didReceiveHTMLEditorText(strText: String, strFrom: String) {
        if strFrom == "EditServiceDescription"{
            lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: strText)
            globalServiceDescription = strText
        }
        else if strFrom == "EditInternalNotes"{
            lblInternalNotes.attributedText = getAttributedHtmlStringUnicode(strText: strText)
            globalInternalDescription = strText
        }
        else if strFrom == "EditTermsAndCondition"{
            lblTermasAndConditionDescription.attributedText = getAttributedHtmlStringUnicode(strText: strText)
            globalTermsAndCOndition = strText
        }
        else{
            print("Error in prototcol")
        }
    }
    
    func SelectedCategory(selectedCategoryObj: NSDictionary) {
        objSelectedCategory = selectedCategoryObj
        txtCategory.text = selectedCategoryObj["Name"] as? String ?? ""
        strCategory = selectedCategoryObj["Name"] as? String ?? ""
        if strForEdit == "NonEditStandard"{
            if strIsFrommCustomService == true{
                
            }
        }
        else{
            txtService.text = ""
            txtFrequency.text = ""
            if txtBillingFrequncy.text?.lowercased() != "Monthly".lowercased(){
                setDefaultBillingFrequency()
            }
            else{
                print("kuch nahi")
            }
        }
        getServiceForCategory()
        
    }
    
    
    func SelectedFrequncy(selectedFrequncyObj: NSDictionary, tag: Int) {
        if tag == 1{
            objSelectFrequency = selectedFrequncyObj
            arrOfBillingFrequncy =  getBillingFrequency()
            let strDefaultBillingFreqType = "\(objSelectedService.value(forKey: "DefaultBillingFrequency") ?? "")"
            let strDefaultBillingFreqSysName = "\(objSelectedService.value(forKey: "DefaultBillingFrequencySysName") ?? "")"
            
            setDefaultBillingFreqSetupAsPerType(strDefaultBillingFreqType: strDefaultBillingFreqType, strDefaultBillinfFreqSysName: strDefaultBillingFreqSysName)
            
            txtFrequency.text = selectedFrequncyObj["FrequencyName"] as? String ?? ""
            strFreqencySysName = selectedFrequncyObj["SysName"] as? String ?? ""
        }
        else{
            objSelectBillingFrequncy = selectedFrequncyObj
            txtBillingFrequncy.text =  selectedFrequncyObj["FrequencyName"] as? String ?? ""
            strBillingFreqencySysName =  selectedFrequncyObj["SysName"] as? String ?? ""
        }
    }
    
    func SelectedService(selectedServiceObj: NSDictionary) {
        objSelectedService = selectedServiceObj
        txtService.text = selectedServiceObj["Name"] as? String ?? ""
        lblServiceDescription.text = selectedServiceObj["Description"] as? String ?? ""
        globalServiceDescription = selectedServiceObj["Description"] as? String ?? ""
        let strDefaultBillingFreqType = "\(objSelectedService.value(forKey: "DefaultBillingFrequency") ?? "")"
        let strDefaultBillingFreqSysName = "\(objSelectedService.value(forKey: "DefaultBillingFrequencySysName") ?? "")"
        
        if strDefaultBillingFreqType == "OtherFrequency"{
            setDefaultBillingFreqSetupAsPerType(strDefaultBillingFreqType: strDefaultBillingFreqType, strDefaultBillinfFreqSysName: strDefaultBillingFreqSysName)
        }
        else{
            setDefaultBillingFrequency()
        }
        lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: objSelectedService["Description"] as? String ?? "")
    }
    
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        objSelectFrequency = dictData
        txtFrequency.text = dictData["FrequencyName"] as? String ?? ""
        strFreqencySysName = dictData["SysName"] as? String ?? ""
        
    }
    
    //MARK: - maintance and one time serivce checks
    func setDefaultBillingFrequency() {
        
        arrOfBillingFrequncy = getBillingFrequency()
        
        for item in arrOfBillingFrequncy
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("Monthly") == .orderedSame {
                
                objSelectBillingFrequncy = NSDictionary()
                self.objSelectBillingFrequncy = dict
                txtMaintenence.isHidden = false
                txtBillingFrequncy.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                strBillingFreqencySysName = "\(dict.value(forKey: "SysName") ?? "")"
            }
        }
        
        if txtFrequency.text?.lowercased() == "OneTime".lowercased() || txtFrequency.text?.lowercased() == "One Time".lowercased()
        {
            forOneTimeBillingFrequency()
        }
    }
    
    func forOneTimeBillingFrequency() {
        
        let arrFinal = NSMutableArray()
        let arrFrequency = getBillingFrequency()
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame {
                
                self.arrOfBillingFrequncy = NSMutableArray()
                self.objSelectBillingFrequncy = dict
                arrFinal.add(dict)
                txtBillingFrequncy.text = "\(dict.value(forKey: "FrequencyName") ?? "")"
                strBillingFreqencySysName = "\(dict.value(forKey: "SysName") ?? "")"
                txtMaintenence.isHidden = true
                txtMaintenence.text = "0.0"
                arrOfBillingFrequncy = arrFinal
                break
            }
        }
    }
    
    func setDefaultBillingFreqSetupAsPerType(strDefaultBillingFreqType: String, strDefaultBillinfFreqSysName: String)  {
        
        if strDefaultBillingFreqType == "SameAsService"
        {
            if "\(objSelectFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                forOneTimeBillingFrequency()
            }
            else
            {
                let arrTemp  =  getBillingFrequency()
                for item in arrTemp
                {
                    let dict = item as! NSDictionary
                    
                    if "\(dict.value(forKey: "SysName") ?? "")" == "\(objSelectFrequency.value(forKey: "SysName") ?? "")"
                    {
                        var strName = ""
                        strName =  "\(dict.value(forKey: "FrequencyName") ?? "")"
                        self.objSelectBillingFrequncy = NSDictionary()
                        self.objSelectBillingFrequncy = dict
                        txtBillingFrequncy.text = strName
                        txtMaintenence.isHidden = false
                        strBillingFreqencySysName = "\(dict.value(forKey: "SysName") ?? "")"
                        //   dictBillingFrequency = dict
                        
                        break
                    }
                }
                
            }
        }
        else if strDefaultBillingFreqType == "OtherFrequency"
        {
            if "\(objSelectFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                forOneTimeBillingFrequency()
            }
            else
            {
                let arrTemp  =  getBillingFrequency()
                for item in arrTemp
                {
                    let dict = item as! NSDictionary
                    
                    if "\(dict.value(forKey: "SysName") ?? "")" == strDefaultBillinfFreqSysName
                    {
                        var strName = ""
                        strName =  "\(dict.value(forKey: "FrequencyName") ?? "")"
                        self.objSelectBillingFrequncy = NSDictionary()
                        self.objSelectBillingFrequncy = dict
                        txtBillingFrequncy.text = strName
                        txtMaintenence.isHidden = false
                        strBillingFreqencySysName = "\(dict.value(forKey: "SysName") ?? "")"
                        //    dictBillingFrequency = dict
                        
                        break
                    }
                }
                
            }
        }
        else if strDefaultBillingFreqType == ""
        {
            if "\(objSelectFrequency.value(forKey: "SysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
            {
                forOneTimeBillingFrequency()
            }else {
                txtMaintenence.isHidden = false
                
            }
        }
        
    }
}

//MARK: - functions of getting Name fropm their sysname
extension NewCommercialAddServicesViewController{
    
    func getServiceForCategory(obj: NSDictionary) -> NSArray
    {
        let arrOfServicesList = obj.value(forKey: "Services") as? NSArray ?? []
        return arrOfServicesList
    }
    
    func getServiceNameFromSysName(objOfService: NSArray , strSysName: String) -> String{
        var result = ""
        
        for i in 0..<objOfService.count{
            
            let dict  = objOfService[i] as! NSDictionary
            if strSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                objSelectedService = dict
                result = "\(dict.value(forKey: "Name") ?? "")"
                
                break
            }
        }
        
        return result
    }
    
    func getBillingFrequncyNameFromSysName(strSysName: String) -> String{
        var result = ""
        let arrFrequency = getBillingFrequency()
        
        for item in arrFrequency
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
            {
                objSelectBillingFrequncy = dict
                result = "\(dict.value(forKey: "FrequencyName") ?? "")"
                break
            }
            
        }
        return result
    }
    
    func getFreq() ->  NSMutableArray
    {
        var arrofFrequency = NSMutableArray()
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if dictMaster.value(forKey: "Frequencies") is NSMutableArray
            {
                arrofFrequency = dictMaster.value(forKey: "Frequencies") as! NSMutableArray
            }
        }
        
        return arrofFrequency
    }
    
    func getDepartmentNameFromSysName(strSysName: String) -> String{
        
        var result = ""
        let arrOfFrequncy = getDepartmentNonStan()
        for item in arrOfFrequncy
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
            {
                objSelectedCategory = dict
                result = "\(dict.value(forKey: "Name") ?? "")"
                break
            }
            
        }
        return result
        
    }
    func getFrequcnyNameFromSysName(strSysName: String) -> String {
        
        var result = ""
        let arrOfFrequncy = getFreq()
        for item in arrOfFrequncy
        {
            let dict = item as! NSDictionary
            
            if "\(dict.value(forKey: "SysName") ?? "")" == strSysName
            {
                objSelectFrequency = dict
                result = "\(dict.value(forKey: "FrequencyName") ?? "")"
                break
            }
            
        }
        return result
    }
    
}


