//
//  NewCommerialAddScopeViewController.swift
//  Commercial_Flow_Demo Changes
//
//  Created by Akshay Hastekar on 13/10/21.
//

import UIKit

class NewCommerialAddScopeViewController: UIViewController , UIGestureRecognizerDelegate, SelectScopeProtocol, HTMLEditorForCommercialDelegate {
   
    //MARK:- Outlet's
    @IBOutlet weak var serviceDiscriptionTxtView: UITextView!
    @IBOutlet weak var txtTarget: ACFloatingTextField!
    @IBOutlet weak var nextImage: UIImageView!
    
    //MARK: - Variables
    
    var objScopeList = NSMutableArray()
    var strIsChangedScopeDesc = ""
    var objOfSelectedScope = [String : Any]()
    @objc var strLeadId = NSString()
    var globalTextOfScopeDesc = ""

    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        gesture()
    }
    
    
    //MARK: - Gesture
    func gesture(){
        self.txtTarget.isUserInteractionEnabled = true
        self.nextImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTargert))
        tap.delegate = self
        let tapOnImage = UITapGestureRecognizer(target: self, action: #selector(tapOnTargert))
        tapOnImage.delegate = self
        self.txtTarget.addGestureRecognizer(tap)
        self.nextImage.addGestureRecognizer(tapOnImage)
    }
    
    //MARK: - addtional function
    @objc func tapOnTargert(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialSeletScopeViewController) as! NewCommercialSeletScopeViewController
        nextViewController.strScopeSelected = txtTarget.text ?? ""
        nextViewController.delegateToSelectScopeProtocol = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    //MARK: - Custom functions

    func SelectScopeProtocol(selectedScopeObj: [String : Any]) {
        objOfSelectedScope = selectedScopeObj
        txtTarget.text = selectedScopeObj["Title"] as? String ?? ""
        serviceDiscriptionTxtView.attributedText = getAttributedHtmlStringUnicode(strText: selectedScopeObj["Description"] as? String ?? "")
        globalTextOfScopeDesc = selectedScopeObj["Description"] as? String ?? ""

    }

    func checkIfScopeDescriptionIsChanged(){
        if globalTextOfScopeDesc !=  objOfSelectedScope["Description"] as? String ?? ""{
            strIsChangedScopeDesc = "true"
        }
        else{
            strIsChangedScopeDesc = "false"
        }
    }
    
    //MARK: - text Editor Protocol
    func didReceiveHTMLEditorText(strText: String, strFrom: String) {
        serviceDiscriptionTxtView.attributedText = getAttributedHtmlStringUnicode(strText: strText)
        globalTextOfScopeDesc = strText

    }
    
    // MARK: - IBAction's
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction (_ sender : UIButton){
        
        let isScopeIsAlreadyExist = checkScopeAvailableInCoreData()
        
        let strScopeSysName = objOfSelectedScope["SysName"] as? String ?? ""
        
        if (strScopeSysName.count == 0 || strScopeSysName == "")
        {
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please select Scope", viewcontrol: self)
        }
        else if isScopeIsAlreadyExist == false{
            saveScopeInCoreData()
            self.navigationController?.popViewController(animated: true)
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Scope already taken", viewcontrol: self)
        }
    }
    
    @IBAction func btnCancleAction (_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditServiceDescription(_ sender: UIButton) {
        
        if txtTarget.text?.count == 0{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Please select Scope", viewcontrol: self)
        }
        else{

            let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
            vc.delegateCommerical = self
            vc.strfrom = "EitherTargetOrScope"
            vc.strHtml = globalTextOfScopeDesc
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    //MARK: - CoreData Functions
    
    func saveScopeInCoreData(){
        
        checkIfScopeDescriptionIsChanged()
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("isChangedScopeDesc")
        arrOfKeys.add("leadCommercialScopeId")
        arrOfKeys.add("leadId")
        arrOfKeys.add("scopeDescription")
        arrOfKeys.add("scopeSysName")
        arrOfKeys.add("title")
        arrOfKeys.add("userName")
        
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(strIsChangedScopeDesc)
        arrOfValues.add("")
        arrOfValues.add(strLeadId)
        arrOfValues.add(globalTextOfScopeDesc)
        arrOfValues.add(objOfSelectedScope["SysName"] as? String ?? "")
        arrOfValues.add(objOfSelectedScope["Title"] as? String ?? "")
        arrOfValues.add(Global().getUserName())
        
        saveDataInDB(strEntity: "LeadCommercialScopeExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
    }
    
    func getScopeFromCoreData(){
        objScopeList.removeAllObjects()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                objScopeList.add(dict)
            }
        }
    }
    
    func checkScopeAvailableInCoreData() -> Bool{
        
        var isScopeIsAlreadyExist = Bool()
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@ && scopeSysName == %@", strLeadId, objOfSelectedScope["SysName"] as? String ?? ""))
        
        if arryOfData.count > 0 {
            isScopeIsAlreadyExist = true
        }
        else{
            isScopeIsAlreadyExist = false
        }
        
        return isScopeIsAlreadyExist
    }
}
