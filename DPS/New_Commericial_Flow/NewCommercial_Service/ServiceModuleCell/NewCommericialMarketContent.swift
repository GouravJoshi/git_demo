//
//  NewCommericialMarketContent.swift
//  DPS
//
//  Created by Saavan Patidar on 02/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialMarketContent: UICollectionViewCell {
    
    @IBOutlet var viewBackground: UIView!
    @IBOutlet var lblMarketTitle: UILabel!
    @IBOutlet var btnClose: UIButton!
    
}
