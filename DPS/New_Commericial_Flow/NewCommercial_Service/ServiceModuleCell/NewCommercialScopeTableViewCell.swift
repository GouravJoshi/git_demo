//
//  ScopeTableViewCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 08/10/21.
//

import UIKit

class NewCommercialScopeTableViewCell: UITableViewCell {

    //MARK: - IBOUtlts
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesccription: UILabel!
    @IBOutlet weak var imgIsChanged: UIImageView!
    @IBOutlet weak var btnDeleteScope: UIButton!
    @IBOutlet weak var btnEditScopeDescription: UIButton!
    @IBOutlet weak var heightConstraintForTitleParentView: NSLayoutConstraint!

    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func SetUpCellForScope(obj: NSManagedObject){
        lblTitle.text = "\(obj.value(forKey: "title") ?? "")"
        lblDesccription.attributedText =  getAttributedHtmlStringUnicode(strText : "\(obj.value(forKey: "scopeDescription") ?? "")")
        heightConstraintForTitleParentView.constant = 55
        let targetLine = lblTitle.calculateMaxLines()
        if targetLine > 2{
            heightConstraintForTitleParentView.constant = CGFloat(targetLine * 25)
        }
        if getOpportunityStatus() == true
        {
            btnDeleteScope.isHidden = true
            btnEditScopeDescription.isHidden = true
        }
        else
        {
            btnDeleteScope.isHidden = false
            btnEditScopeDescription.isHidden = false
        }
        
        let isChanged =  "\(obj.value(forKey: "isChangedScopeDesc") ?? "")"
        
        if isChanged.lowercased() == "true".lowercased(){
            imgIsChanged.image = UIImage (named: "check_box_2.png")
        }
        else{
            imgIsChanged.image = UIImage(named: "check_box_1.png")
        }
    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
}
