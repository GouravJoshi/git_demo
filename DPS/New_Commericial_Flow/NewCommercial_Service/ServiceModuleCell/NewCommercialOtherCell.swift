//
//  OtherCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 12/10/21.
//

import UIKit

class NewCommercialOtherCell: UITableViewCell {
    
    @IBOutlet var txtPropsedDate: ACFloatingTextField!
    
    @IBOutlet var txtProposedServiceMonth: ACFloatingTextField!
    
    @IBOutlet var btnProposedDate: UIButton!
    
    @IBOutlet var btnProposedServiceMonth: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionOnProposedDate(_ sender: Any) {
        
    }
    
    @IBAction func actionOnProposedServiceMonth(_ sender: Any) {
        
        
    }
    
}
