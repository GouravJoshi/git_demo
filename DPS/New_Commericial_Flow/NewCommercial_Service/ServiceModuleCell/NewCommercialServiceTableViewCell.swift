//
//  ServiceTableViewCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 11/10/21.
//

import UIKit
import CoreData

class NewCommercialStandardServiceTableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
   
    @IBOutlet var lblInitialPrice: UILabel!
    @IBOutlet var lblMaintPrice: UILabel!
    @IBOutlet var lblMaintPriceTitle: UILabel!

    @IBOutlet weak var heightConstraintForMantainceTitle: NSLayoutConstraint!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet weak var topOfMaintancePrice: NSLayoutConstraint!
    @IBOutlet var btnDelete: UIButton!
    
    @IBOutlet var lblServiceDescription: UILabel!
    
    @IBOutlet var imgViewChecked: UIImageView!
    @IBOutlet var lblIsChecked: UIButton!

    //MARK:- Tableview Load View
    override func awakeFromNib() {
        super.awakeFromNib()
       
       // self.delegate_dataSource_tableview()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Delegate dataSource function
    
    func delegate_dataSource_tableview(){
       
    }

}

class NewCommercialNonStandardServiceTableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
   
    @IBOutlet var lblInitialPrice: UILabel!
    @IBOutlet var lblMaintPrice: UILabel!
    @IBOutlet var lblMaintPriceTitle: UILabel!

    @IBOutlet weak var topOfManitancePrice: NSLayoutConstraint!
    @IBOutlet var lblServiceDescription: UILabel!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!

    @IBOutlet weak var heightConstaintForMaitanceTitle: NSLayoutConstraint!
    //MARK:- Tableview Load View
    override func awakeFromNib() {
        super.awakeFromNib()
       
       // self.delegate_dataSource_tableview()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Delegate dataSource function
    
    func delegate_dataSource_tableview(){
       
    }

}

//MARK:- Extension tableview

extension NewCommercialStandardServiceTableViewCell : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:IdentifierTableView.NewCommercialStandardServiceTableViewCell , for: indexPath) as? NewCommercialStandardServiceTableViewCell
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
}
class NewCommercialRenewalServiceCell: UITableViewCell, UITextFieldDelegate {
    
    var callback: ((_ strPrice: String) -> Void)?

    //MARK:- Outlets
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceFrequencyName: UILabel!
    @IBOutlet var txtInitialPrice: ACFloatingTextField!
    @IBOutlet var btnEditDescription: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var lblServiceDescription: UILabel!
    
    var strLeadId = ""
    var strSoldStandardServiceId = ""
    var strUserName = ""
    //MARK:- Tableview Load View
    override func awakeFromNib() {
        super.awakeFromNib()
        txtInitialPrice.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if txtInitialPrice == textField{
            UpdateRenewalPriceAmount(value: txtInitialPrice.text ?? "")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtInitialPrice
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            return chk
        }
       return false
    }
    
    func UpdateRenewalPriceAmount(value:String){
        
        var assignValue = value
        var strInitalPrice = "0"
        var strRenewalAmount = ""
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "userName == %@ && leadId == %@ && soldServiceStandardId == %@", strUserName, strLeadId, strSoldStandardServiceId))
        
//        if arryOfData.count > 0 {
//            if arryOfData[0] is NSManagedObject{
//                let obj = arryOfData[0] as! NSManagedObject
//                strInitalPrice = "\(obj.value(forKey: "initialPrice") ?? "")"
//            }
//        }
        if arryOfData.count > 0
        {
            let matchedService = arryOfData.object(at: 0) as! NSManagedObject
            
            var dictService = NSDictionary()
            dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matchedService.value(forKey: "serviceSysName") ?? "")")
            
            if dictService.value(forKey: "ServiceMasterRenewalPrices") is NSArray
            {
                let arrRenewaNaster = dictService.value(forKey: "ServiceMasterRenewalPrices") as! NSArray
                
                let arr = arrRenewaNaster.filter { (dict) -> Bool in
                    
                    return "\((dict as! NSDictionary).value(forKey: "ServiceMasterId") ?? "")".caseInsensitiveCompare("\(dictService.value(forKey: "ServiceMasterId") ?? "")") == .orderedSame
                } as NSArray
                
                if arr.count > 0
                {
                    let dictRenewalData = arr.object(at: 0) as! NSDictionary
                    if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "Percentage"
                    {
//                        var price = Float()
//                        var per = Float()
//                        per = Float("\(dictRenewalData.value(forKey: "Price_Percentage") ?? "")") ?? 0.0
//                        price = (Float(strInitalPrice) ?? 0.0 ) * per / 100
//
//                        if price < Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
//                        {
//                            price = Float("\(dictRenewalData.value(forKey: "MinimumPrice") ?? "")") ?? 0.0
//                        }
//
//                        strRenewalAmount = String(format: "%.2f", price)
                          strRenewalAmount = String(format: "%.2f", (Float(assignValue) ?? 0.0 ))
                    }
                    else if "\(dictRenewalData.value(forKey: "PriceBasedOn") ?? "")" == "FlatPrice"
                    {
                        //strRenewalAmount = "\(dictRenewalData.value(forKey: "Price_Percentage") ?? "0.00")"
                        strRenewalAmount = String(format: "%.2f", (Float(assignValue) ?? 0.0 ))
                    }
                    
                }
            }
            assignValue = strRenewalAmount
            
        }
        
        if assignValue == "" || (assignValue as NSString).floatValue == 0
        {
            assignValue = stringToFloat(strValue: assignValue)
        }
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = [
            
            "renewalAmount",
        ]
        
        arrOfValues = [
            stringToFloat(strValue: assignValue),
        ]
        
        var isSuccess = Bool()
        
        isSuccess = getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,strSoldStandardServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        if(isSuccess == true)
        {
            print("updated successsfully")
            callback?("sucss")
        }
        else
        {
            print("updates failed")
        }
    }
    
    
    
    
    
//    {
//
//        var assignValue = value
//        if assignValue == "" || (assignValue as NSString).floatValue == 0
//        {
//            assignValue = stringToFloat(strValue: assignValue)
//        }
//        var arrOfKeys = NSMutableArray()
//        var arrOfValues = NSMutableArray()
//
//        arrOfKeys = [
//
//            "renewalAmount",
//        ]
//
//        arrOfValues = [
//            stringToFloat(strValue: assignValue),
//            ]
//
//        var isSuccess = Bool()
//
//        isSuccess =  getDataFromDbToUpdate(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && soldServiceStandardId == %@",strLeadId,strSoldStandardServiceId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
//
//        if(isSuccess == true)
//        {
//            print("updated successsfully")
//            callback?("sucss")
//        }
//        else
//        {
//            print("updates failed")
//        }
//    }
    
    func stringToFloat(strValue : String) -> String
    {
        let myFloat = (strValue as NSString).floatValue
        
        return String(format: "%.2f", myFloat)
    }
}
