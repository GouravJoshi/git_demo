//
//  targetCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 07/10/21.
//

import UIKit

class NewCommercialTargetCell: UITableViewCell {

    //MARK: - IBOUtlts
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBar: UIView!
    @IBOutlet weak var lblDesccription: UILabel!
    @IBOutlet weak var imgIsChanged: UIImageView!
    @IBOutlet weak var btnAddImages: UIButton!
    @IBOutlet weak var btnDeleteTarget: UIButton!
    @IBOutlet weak var btnEditTargetDescription: UIButton!
    @IBOutlet weak var heightConstraintForTitleParentView: NSLayoutConstraint!
    
    var strGlobalStatusSysName = ""
    var strGlobalStageSysName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func SetUpCellForTarget(obj: NSManagedObject){
        lblTitle.text = "\(obj.value(forKey: "name") ?? "")"
        lblDesccription.attributedText =  getAttributedHtmlStringUnicode(strText : "\(obj.value(forKey: "targetDescription") ?? "")")
        heightConstraintForTitleParentView.constant = 55
        let targetLine = lblTitle.calculateMaxLines()
        if targetLine > 2{
            heightConstraintForTitleParentView.constant = CGFloat(targetLine * 25)
        }
        let isChanged =  "\(obj.value(forKey: "isChangedTargetDesc") ?? "")"
        
        if isChanged.lowercased() == "true".lowercased(){
            imgIsChanged.image = UIImage (named: "check_box_2.png")
        }
        else{
            imgIsChanged.image = UIImage(named: "check_box_1.png")
        }
        
        if getOpportunityStatus() == true
        {
            btnAddImages.isHidden = true
            btnDeleteTarget.isHidden = true
            btnEditTargetDescription.isHidden = true
        }
        else
        {
            btnAddImages.isHidden = false
            btnDeleteTarget.isHidden = false
            btnEditTargetDescription.isHidden = false
        }
    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
}
