//
//  constent.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 07/10/21.
//

import Foundation
import UIKit

struct IdentifierTableView {
    static var NewCommercialTargetCell = "NewCommercialTargetCell"
    static var NewCommercialScopeTableViewCell = "NewCommercialScopeTableViewCell"
    static var NewCommercialStandardServiceTableViewCell = "NewCommercialStandardServiceTableViewCell"
    static var NewCommercialNonStandardServiceTableViewCell = "NewCommercialNonStandardServiceTableViewCell"
    static var NewCommercialServiceCell = "NewCommercialServiceCell"
    static var NewCommercialOtherCell = "NewCommercialOtherCell"
    static var NewCommercialProposalCoverCell = "NewCommercialProposalCoverCell"
    static var NewCommercialTermsAndServiceCell = "NewCommercialTermsAndServiceCell"
    static var NewCommercialConfigueServiceCell = "NewCommercialConfigueServiceCell"
    static var NewCommercialAgreementChecklistTableViewCell = "NewCommercialAgreementChecklistTableViewCell"
    static var NewCommercialAgreementCheckListCell = "NewCommercialAgreementCheckListCell"
    static var NewCommericialMonthOfServicesCell = "NewCommericialMonthOfServicesCell"
    static var NewCommericialAddtionalNotesCell = "NewCommericialAddtionalNotesCell"
    static var NewCommericialInternalNotesCell = "NewCommericialInternalNotesCell"
}


struct IdentifierStoryboard {
    static var NewCommercialSelectTargertViewController = "NewCommercialSelectTargertViewController"
    static var ServiceDescriptionViewController = "NewCommercialServiceDescriptionViewController"
    static var NewCommerialTargetViewController = "NewCommerialTargetViewController"
    static var NewCommerialAddScopeViewController = "NewCommerialAddScopeViewController"
    static var NewCommercialSeletScopeViewController = "NewCommercialSeletScopeViewController"
    static var NewCommercialAddServicesViewController = "NewCommercialAddServicesViewController"
    static var NewCommercialServiceCategoryVIewController = "NewCommercialServiceCategoryVIewController"
    static var serviceViewController = "NewCommercialServiceViewController"
    static var NewCommercialfequencyViewController = "NewCommercialfequencyViewController"
    static var NewCommercialInternalNotesViewController = "NewCommercialInternalNotesViewController"
    static var NewCommercialConfigureDashBoardVC = "NewCommercialConfigureDashBoardVC"
    static var NewCommercialConfigureAddtionalNote = "NewCommercialConfigureAddtionalNote"
    static var NewCommericialReviewViewController = "NewCommericialReviewViewController"
   
}
