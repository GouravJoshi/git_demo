//
//  newCommercialConfigureAddtionalNote.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 25/10/21.
//

import UIKit

protocol callBackAddtionalNotes : class{
    
    func callBackMethodAdditonalNotes(data : String)
    func callBakMethodInternalNotes(data : String)
    
}

class NewCommercialConfigureAddtionalNote: UIViewController {
    
    @IBOutlet weak var textViewAddtionalNote: UITextView!
    
    @IBOutlet var lblTitle: UILabel!
    var addtionalNote : String?
    var strValue : String?
    var delegate : callBackAddtionalNotes?
    @objc var strLeadId = NSString()
    var objLeadDetails : NSManagedObject?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblTitle.text = strValue
        self.textViewAddtionalNote.text = addtionalNote
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionOnContinue(_ sender: UIButton) {
        
        if strValue == "Addtional Notes"{
            delegate?.callBackMethodAdditonalNotes(data: textViewAddtionalNote.text)
            savePaymentInfo ()
        }else if strValue == "Internal Notes"{
            delegate?.callBakMethodInternalNotes(data: textViewAddtionalNote.text)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func savePaymentInfo () {
        
        
        deleteAllRecordsFromDB(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        let context = getContext()
        let strUserName = Global().getUserName()
        let strCompanyKey = Global().getCompanyKey()
        
        let entityPaymentInfo = NSEntityDescription.insertNewObject(forEntityName: "PaymentInfo", into: context)
        entityPaymentInfo.setValue(self.strLeadId, forKey: "leadId")
        entityPaymentInfo.setValue(textViewAddtionalNote.text, forKey: "specialInstructions")
        entityPaymentInfo.setValue(strUserName, forKey: "userName")
        entityPaymentInfo.setValue(strCompanyKey, forKey: "companyKey")
      
        //save the object
        do { try context.save()
            print("Record Saved Updated.")
            self.navigationController?.popViewController(animated: true)
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
          
    }
    
//    func saveNotes(){
//
//        print("strLead")
//        print(strLeadId)
//
//        let strUserName = Global().getUserName()
//        let strCompanyKey = Global().getCompanyKey()
//
//        let arrOfKeys = NSMutableArray()
//        let arrOfValues = NSMutableArray()
//
//        arrOfKeys.add("specialInstructions")
//        arrOfKeys.add("userName")
//        arrOfKeys.add("companyKey")
//
//        arrOfValues.add(textViewAddtionalNote.text)
//        arrOfValues.add(strUserName ?? "")
//        arrOfValues.add(strCompanyKey ?? "")
//
//        let isSuccess = getDataFromDbToUpdate(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@ ", strLeadId) , arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
//
//        if isSuccess {
//            print("suc")
//        }
//
//        self.navigationController?.popViewController(animated: true)
//
//    }
//

}
