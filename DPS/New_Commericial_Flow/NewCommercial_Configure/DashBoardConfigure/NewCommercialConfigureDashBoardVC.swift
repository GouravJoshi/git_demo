//
//  newCommercialConfigureDashBoardVC.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 21/10/21.
//

import UIKit
import CoreData
import AVFoundation

class NewCommercialConfigureDashBoardVC: UIViewController , DataProtocol  {
    
    func getTermsOfServiceData(data: Any) {
        self.strTermsOfService = (data as AnyObject).value(forKey: "Title") as? String ?? ""
        self.strDescriptionTermsService = (data as AnyObject).value(forKey: "Description") as? String ?? ""
        dictTermsOfService = data as! NSDictionary
        configureTableView.reloadData()
    }
    // Return Marketing Content
    func getMarketingContentData(data: NSMutableArray) {
        arrayMarketingContentMultipleSelected = data
        configureTableView.reloadData()
    }
        
    //  Return Introdution Letter Data
    func getIntroductionLetterData(data: Any) {
        self.strIntroductionLetter = (data as AnyObject).value(forKey: "TemplateName") as? String ?? ""
        //TemplateContent = "\((data as AnyObject).value(forKey: "TemplateHeader") ?? "") \n \((data as AnyObject).value(forKey: "TemplateContent") ?? "") \n \((data as AnyObject).value(forKey: "TemplateFooter") ?? "")"
        TemplateContent = "\((data as AnyObject).value(forKey: "TemplateContent") ?? "")"

        refreshTegs()
        dictIntroduction = data as! NSDictionary
        configureTableView.reloadData()
    }
    
    // Return Cover Letter Data
    func getCoverLetterData(data: Any) {
        self.strCoverLetter = (data as AnyObject).value(forKey: "TemplateName") as? String ?? ""
        dictCoverLetter = data as! NSDictionary
        configureTableView.reloadData()
    }
    
    
    var loader = UIAlertController()
    private lazy var presenter = {
        return SalesConfigurePresenter(self)
    }()

    //MARK:- Outlets
    
    @IBOutlet weak var configureTableView: UITableView!
    @IBOutlet weak var lblOppNumbber: UILabel!
    @IBOutlet weak var lblOppTitle: UILabel!
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var btnMarkAsLost: UIButton!
   
    
    //MARK:- Variables
    var discStr : String?
    var addButton : UIButton?
    var addtionalNote : String?
    var strCoverLetter = ""
    var strIntroductionLetter = ""
    var strTermsOfService = ""
    var TemplateContent : String?
    var strDescriptionTermsService = ""
    var marketingConternt = [String]()
    var joinedStrMarketingContent  = "Marketing Content"
    var dataGeneralInfo = NSManagedObject()
    var dictLoginData = NSDictionary()
    var arrayOfMonth: [String] = []
    
    //MARK:- Array
    var arrayCoverLetter = NSMutableArray()
    var arrayIntroduction = NSMutableArray()
    var arrayMarketingContent = NSMutableArray()
    var arrayMarketingContentMultipleSelected  = NSMutableArray()
    var arrayTermsOfService = NSMutableArray()
    var arrayTermsAndConditions =  NSMutableArray()
    var arrAgreementCheckList =  NSMutableArray()
    var arrSelectedAgreementChckList = NSMutableArray()
    
    var arrayOfImages = NSArray ()
    var arrayTermsAndConditionsMultipleSelected = NSMutableArray()
    var dictMarketContentCollectionView = NSMutableArray()
    var arrOfTagests =  NSMutableArray()
    var arrOfScopes =  NSMutableArray()
    
//    var arrOfStadardServices =  NSMutableArray()
//    var arrOfNonStandardServices =  NSMutableArray()
    
    var arrStandardService : [NSManagedObject] = []
    var arrCustomService : [NSManagedObject] = []
    var arrStandardServiceSold : [NSManagedObject] = []
    var arrCustomServiceSold : [NSManagedObject] = []
    var arrStandardServiceNotSold : [NSManagedObject] = []
    var arrCustomServiceNotSold : [NSManagedObject] = []
    
    //MARK:- Dictionary
    var dictCoverLetter = NSDictionary()
    var dictIntroduction = NSDictionary()
    var dictTermsOfService = NSDictionary()
  
    @objc var dictTarget = NSDictionary ()
    
    //MARK:- String
    @objc var strLeadId = NSString()
    @objc var strTitle = NSString()
    @objc var strOppNumber = NSString()
    var strLeadIdAgreement = ""
    var strModuleType = ""
    var strTargetId = ""
    var strPreferedMonth = ""
    @objc var strWoStatus = NSString ()
    @objc  var strHeaderTitle = NSString ()
    var strAddtionalNotes = ""
    var strInternalNotes = ""
    var strAccountNoGlobal = ""
    var strLeadNumberLead = ""
    var txtNoOfDays = ""
    var strIsAgreementValidForNotes = ""
    var strValidFor = ""
    var isSoldStandard = ""
    var isSoldCustome = ""
    var strSelectedPropsedDate = ""
    var strSelectedPropsedMonth = ""
    var strBranchSysName = "", strGlobalStageSysName = "", strGlobalStatusSysName = ""
    var strTaxValue = ""
    var strTaxCode = ""
    var strIsServiceAddrTaxExempt = ""
    var strServiceAddressSubType = ""
    var objLeadDetails : NSManagedObject?
    var objPaymentInfo : NSManagedObject?
    
    var arrayAgreementCheckList: [Any] = []
    var arrayAgreementCheckListTemp: NSArray = []
    //MARK:- Boolen
    var isIntroductionLetterEdited = false
    
    let btnMarkAsLostAttributes = [
        NSAttributedString.Key.foregroundColor : UIColor.darkGray,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]

    var attributedString = NSMutableAttributedString(string:"")
    
    // Pricing
    
    var dictPricingInfo: [String: Any] = [:]
    var arrAppliedCoupon : [NSManagedObject] = []
    var arrAppliedCredit : [NSManagedObject] = []
    
    var dictTaxStatus           : NSDictionary = NSDictionary()
    var dictCommercialStatus    : NSDictionary = NSDictionary()
    var dictResidentialStatus   : NSDictionary = NSDictionary()
    
    var dictGlobleCouponDetail: [String: Any]?
    var arrDiscountCredit:[Any] = []
    var arrAppliedCreditDetail:[Any] = []
    var dictSelectedCredit: [String: Any]?
    var dictSelectedTaxCode: [String: Any]?
    var arrTaxCode:[Any]?
    
    //Pricing Information
    var subTotalInitialPrice    = 0.0
    var subTotalMaintenancePrice = 0.0
    var couponDiscountPrice     = 0.0
    var creditAmount            = 0.0
    var creditAmountMaint       = 0.0
    var totalDiscountPrice      = 0.0
    var totalPrice              = 0.0
    var taxAmount               = 0.0
    var taxMaintAmount          = 0.0
    var billingAmount           = 0.0
    var leadInspectionFee       = 0.0
    var otherDiscount           = 0.0
    var tipDiscount             = 0.0
    var taxableInitialAmount    = 0.0
    var taxableMaintAmount      = 0.0
    var initialStanTax          = 0.0
    var initialMaintTax         = 0.0
    
    var appliedDiscountMaint = 0.0
    var appliedDiscountInitial = 0.0
    
    var isCouponSaved = false
    var isEditInSalesAuto = false
    
    var arrayOfFreqPrice    : [String] = []
    var arrayOfFreqSysName  : [String] = []
    var arrMaintBillingPrice  : [String] = []
    
    var chkFreqConfiguration = false
    
    
    //Nilind
    var matchesGeneralInfo = NSManagedObject()
    var strEmpName = "", strUserName = "", strCompanyKey = ""
    var isProposalFoolowUp = false
    var isServiceFollowUp = false
    var serviceFollowDays = 0
    var proposalFollowDays = 0
    var isFromServiceFollowUpDataBase = false
    var isFromProposalFolloupDataBase = false
    var isSendProposal = false
    var chkForLost = false
    var arySalesProposalFollowUpList = NSArray() , arySalesServiceFollowUpList = NSArray()
    var arySalesProposalServices = NSArray()
    var isPreferredMonths = false
    var isTermsOfService = false
    
    
    // sourabh 1 march 2022
    
    var BillingAddressName = ""
    var CustomerCompanyName = ""
    var CellPhone = ""
    var PrimaryPhone = ""
    var billingCellNo = ""
    var billingPrimaryPhone = ""
    var billingSecondaryPhone = ""
    var billingPrimaryEmail = ""
    var BusinessLicenceNo = ""
    fileprivate var strBillingPocName = ""
    fileprivate var strCompanyAddress = ""
    fileprivate let global = Global()
    var strInspectorId = ""
    fileprivate var strAccountManagerEmail = ""
    fileprivate var strAccountManagerPrimaryPhone = ""
    var strCustomerName = ""
    fileprivate var strCellNo = ""
    fileprivate var strAccountManagerName = ""
    fileprivate var strLeadNumber = ""
    fileprivate var strAccountNo = ""
    fileprivate var strCompanyLogoPath = ""
    fileprivate var strBranchLogoImagePath = ""
    fileprivate var strCustomerCompanyProfileImage = ""
    
    //Sourabh From Builder Start
    var strServiceUrlMain = ""
    var arrImageDetails: [[String: Any]] = []
    var arrGraphDetails: [[String: Any]] = []
    // End
    var countCRMImage = 0

    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        basicFunction()
        self.tegsSetup()
        self.serviceName()
        self.serviceTaxableStatus()
        self.tableView_delegate_dataSource()
        self.configureTableView.tableFooterView = UIView()
        getLetterTemplateAndTermsOfService()
        fetchLetterTemplateFromLocalDB()
        fetchMarketingContentFromLocalDB()
        agreementChecklist()
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: strLeadId as String)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getTargetFromCoreData()
        getScopeFromCoreData()
        
        fetchAgreementCheckListSales()
        self.initialSetup()
        self.checkServicesForCouponCredit()
        
        if getOpportunityStatus() || getLostStatus()
        {
            self.btnMarkAsLost.isUserInteractionEnabled = false
            self.btnMarkAsLost.isHidden = true
        }
        else
        {
            self.btnMarkAsLost.isUserInteractionEnabled = true
            self.btnMarkAsLost.isHidden = false
        }
        
    }
    
    func tegsSetup(){
        
        dataGeneralInfo = SalesNew_SupportFile().getSalesWODataFromCoreData(strleadId: strLeadId as String)
      
        
        matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
        
        BillingAddressName = Global().strCombinedAddressService(for: matchesGeneralInfo)
        
        self.strInspectorId = "\(matchesGeneralInfo.value(forKey: "salesRepId") ?? "")"
        CustomerCompanyName = matchesGeneralInfo.value(forKey: "companyName") as? String ?? ""
        CellPhone = matchesGeneralInfo.value(forKey: "billingCellNo") as? String ?? ""
        PrimaryPhone = matchesGeneralInfo.value(forKey: "servicePrimaryPhone") as? String ?? ""
        self.strLeadNumber = "\(matchesGeneralInfo.value(forKey: "leadNumber") ?? "")"
        self.strCustomerCompanyProfileImage = "\(matchesGeneralInfo.value(forKey: "profileImage") ?? "")"
        self.strAccountManagerEmail = "\(matchesGeneralInfo.value(forKey: "accountManagerEmail") ?? "")"
        self.strAccountManagerPrimaryPhone = "\(matchesGeneralInfo.value(forKey: "accountManagerPrimaryPhone") ?? "")"
        self.strCustomerName = "\(global.strFullName(fromCoreDB: matchesGeneralInfo) ?? "")"//"\(matchesGeneralInfo.value(forKey: "customerName") ?? "")"
        self.strCellNo = "\(matchesGeneralInfo.value(forKey: "cellNo") ?? "")"
        self.strAccountManagerName = "\(matchesGeneralInfo.value(forKey: "accountManagerName") ?? "")"
        self.strAccountNo = "\(matchesGeneralInfo.value(forKey: "accountNo") ?? "")"
        
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        strCompanyLogoPath = "\(dictLoginData.value(forKeyPath: "Company.LogoImagePath") ?? "")"
        strBranchLogoImagePath = "\(dictLoginData.value(forKeyPath: "EmployeeBranchLogoImagePath") ?? "")"
       
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        
        let strCompanyAdd1 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine1") ?? "")"
        
        let strCompanyAdd2 = "\(dictLoginData.value(forKeyPath: "Company.CompanyAddressLine2") ?? "")"
        
        let strCompanyCountry = "\(dictLoginData.value(forKeyPath: "Company.CountryName") ?? "")"
        
        let strCompnayCity = "\(dictLoginData.value(forKeyPath: "Company.CityName") ?? "")"
        
        let strCompanyState = "\(dictLoginData.value(forKeyPath: "Company.StateName") ?? "")"
        
        let strCompnayZipCode = "\(dictLoginData.value(forKeyPath: "Company.ZipCode") ?? "")"
        
        self.billingCellNo = matchesGeneralInfo.value(forKey: "billingCellNo") as? String ?? ""
        
        self.billingPrimaryPhone = matchesGeneralInfo.value(forKey: "billingPrimaryPhone") as? String ?? ""
        
        self.billingSecondaryPhone = matchesGeneralInfo.value(forKey: "billingSecondaryPhone") as? String ?? ""
        
        self.billingPrimaryEmail = matchesGeneralInfo.value(forKey: "billingPrimaryEmail") as? String ?? ""
        
        let BusinessLicenceNo = "\(dictLoginData.value(forKeyPath: "Company.BusinessLicenceNo") ?? "")"
        
        self.BusinessLicenceNo = BusinessLicenceNo
        
        if(strCompanyAdd1.count > 0)
        {
            strCompanyAddress = strCompanyAdd1
        }
        if(strCompanyAdd2.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyAdd2 : strCompanyAdd2
        }
        if(strCompanyCountry.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyCountry : strCompanyCountry
        }
        
        if(strCompnayCity.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayCity : strCompnayCity
        }
        
        if(strCompanyState.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompanyState : strCompanyState
        }
        
        if(strCompnayZipCode.count > 0)
        {
            strCompanyAddress = strCompanyAddress.count > 0 ? strCompanyAddress + "," + strCompnayZipCode : strCompnayZipCode
        }
        
    }
    
    
    func refreshTegs(){
        
        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
        
        for i in 0 ..< 4 {
            
           // strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName))
            
            
            
//            if DeviceType.IS_IPAD {
//
//            }else{
//                TemplateContent = TemplateContent?.replacingOccurrences(of: "<img src=\"http://pcrs.pestream.com//Documents/CompanyLogo/Dreamservicelogo.jpg\" style=\"width:170px\" />", with: "<img src=\"http://pcrs.pestream.com//Documents/CompanyLogo/Dreamservicelogo.jpg\" style=\"width:100\" />")
//
//                TemplateContent = TemplateContent?.replacingOccurrences(of: "<div style=\"font-size:35px;color:#7CC34D;border-bottom:4px dashed #7CC34D;padding-bottom:5px;\">", with: "<div style=\"font-size:15px;color:#7CC34D;border-bottom:2px dashed #7CC34D;padding-bottom:5px;\">")
//            }
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName)
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceAddress]", with: BillingAddressName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath)
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath)
            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage)

            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag)

            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage)

            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate())
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate())
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceAddressName]", with: BillingAddressName)
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingAddress]", with: BillingAddressName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingAddressName]", with: BillingAddressName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountNo]", with: strAccountNo)

            TemplateContent = TemplateContent?.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber)
            
            if(dictInspector.count > 0)
            {
                
                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String)
                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String)
            
            }
            else
            {
                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "")
                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorEmail]", with:  "")
            
            }
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Technician]", with: strEmpName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CellPhone]", with: CellPhone)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CellNumber]", with: strCellNo)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Licenseno]", with: "")
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[PrimaryPhone]", with: PrimaryPhone)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCPrimaryPhone]", with: billingPrimaryPhone)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCSecondaryPhone]", with: self.billingSecondaryPhone)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCCell]", with: self.billingCellNo)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCEmailID]", with: self.billingPrimaryEmail)
            
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CompanyBusinessLicenseNo]", with: self.BusinessLicenceNo)
            
            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerEmai]", with: self.strAccountManagerEmail)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                configureTableView.beginUpdates()
                configureTableView.endUpdates()
            }
            
        }
        
    }
    
    func initialSetup() {
                
        let strleadNumber =  dataGeneralInfo.value(forKey: "leadNumber") ?? ""
        let straccountNo =  dataGeneralInfo.value(forKey: "accountNo") ?? ""
        
        lblOppNumbber.text = Global().strFullName(fromCoreDB: dataGeneralInfo)
        lblOppTitle.text = "Opp# \(strleadNumber)" + " " + "Acc# \(straccountNo)"
        
        subTotalInitialPrice    = 0.0
        subTotalMaintenancePrice = 0.0
        couponDiscountPrice     = 0.0
        creditAmount            = 0.0
        creditAmountMaint       = 0.0
        totalDiscountPrice      = 0.0
        totalPrice              = 0.0
        taxAmount               = 0.0
        taxMaintAmount          = 0.0
        billingAmount           = 0.0
//        leadInspectionFee       = 0.0
//        otherDiscount           = 0.0
//        tipDiscount             = 0.0
        taxableInitialAmount    = 0.0
        taxableMaintAmount      = 0.0
        
        appliedDiscountMaint = 0.0
        appliedDiscountInitial = 0.0
        initialStanTax = 0.0
        initialMaintTax = 0.0
      
        self.arrayOfFreqSysName.removeAll()
        self.arrayOfFreqPrice.removeAll()
        self.getLoginDetails()
        self.getLeadDetails()
        self.fetchStandardServiceData()
        self.fetchCustomServiceData()
        self.fetchPaymentInfo()
        self.getAppliedCouponOrCredit()
        
        self.billingFreqancyCalculationMethod()
        self.billingFreqancyCalculationOnCredit()
        
        self.taxCalculation()
        self.updatePricingInfoDict()
        
        configureTableView.reloadData()
    }
    
    func checkServicesForCouponCredit() {
        let arrCreditAndCoupon = arrAppliedCoupon + arrAppliedCredit
       
        let soldService = arrStandardServiceSold + arrCustomServiceSold
        if soldService.count == 0 {
            for couponOrCredit in arrCreditAndCoupon {
                var objCouponCredit = NSManagedObject()
                objCouponCredit = couponOrCredit
                let discountType = "\((objCouponCredit as AnyObject).value(forKey: "discountType") ?? "")"
                if discountType == "Coupon" {
                    //Delete Coupon
                    self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                }else {
                    //Delete Credit
                    self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                }
            }
        }
    }
    
    func serviceName() {
        
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster: [String: Any] = UserDefaults.standard.object(forKey: "MasterSalesAutomation") as! [String : Any]
            
            //Getting Tax Code List
            if dictMaster["TaxMaster"] is NSArray
            {
                self.arrTaxCode = dictMaster["TaxMaster"] as! [[String : Any]]
            }
        }
    }
    
    func serviceTaxableStatus() {
        let name: NSMutableArray = NSMutableArray()
        let sysName: NSMutableArray = NSMutableArray()
        let commercialVal: NSMutableArray = NSMutableArray()
        let residentialVal: NSMutableArray = NSMutableArray()
        
        if(nsud.value(forKey: "MasterSalesAutomation") != nil)
        {
            let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            if(dictMasters.value(forKey: "Categories") != nil) {
                
                let arrCatergory = dictMasters.value(forKey: "Categories") as? [Any]
                if (arrCatergory?.count ?? 0) > 0 {
                    for i in 0..<arrCatergory!.count {
                        let dict: NSDictionary = arrCatergory?[i] as! NSDictionary
                        
                        let arrServices = dict["Services"] as? [AnyHashable]
                        for j in 0..<(arrServices?.count ?? 0) {
                            let dict: NSDictionary = (arrServices?[j] as? NSDictionary)!
                            var str: String?
                            if let value = dict["IsTaxable"] {
                                str = "\(value)"
                            }
            //                debugPrint("str>>>>\(str ?? "")")
                            name.add(str ?? "")
                            if let value = dict["SysName"] {
                                sysName.add("\(value)")
                            }
                            if let value = dict["IsCommercialTaxable"] {
                                commercialVal.add("\(value)")
                            }
                            if let value = dict["IsResidentialTaxable"] {
                                residentialVal.add("\(value)")
                            }
                        }
                    }
                }
            }
        }
        
        self.dictTaxStatus = NSDictionary(objects:name as! [Any], forKeys:sysName as! [NSCopying])
        self.dictCommercialStatus = NSDictionary(objects:commercialVal as! [Any], forKeys:sysName as! [NSCopying])
        self.dictResidentialStatus = NSDictionary(objects:residentialVal as! [Any], forKeys:sysName as! [NSCopying])
//        debugPrint(self.dictTaxStatus, self.dictCommercialStatus, self.dictResidentialStatus)
    }
    
    func taxCalculation() {
        
        self.taxAmount = self.calculateTaxableAmountInitialPrice(otherDiscount: otherDiscount, tipDiscount: tipDiscount, couponDiscount: self.couponDiscountPrice, creditDiscount: self.creditAmount, leadInspectionFee: leadInspectionFee, subtotalInitial: self.subTotalInitialPrice)
        if self.taxAmount < 0
        {
            self.taxAmount = 0
        }
        
        self.taxMaintAmount = self.calculateTaxableAmountMaintPrice(creditDiscount: self.creditAmountMaint, subtotalMaint: self.subTotalMaintenancePrice)
        if self.taxMaintAmount < 0
        {
            self.taxMaintAmount = 0
        }
        
//        debugPrint(self.taxAmount, self.taxMaintAmount)
    }
    
    func calculateTaxableAmountInitialPrice(otherDiscount: Double, tipDiscount: Double, couponDiscount: Double, creditDiscount: Double, leadInspectionFee: Double, subtotalInitial: Double) -> Double {
        
        let totalDiscount = otherDiscount + tipDiscount + couponDiscount + creditDiscount
        let initialAndLeadInspction = (subtotalInitial + leadInspectionFee)
        if initialAndLeadInspction > 0 {
            var discountPerInitial = (totalDiscount / initialAndLeadInspction) * 100;

            if discountPerInitial < 0
            {
                discountPerInitial = 0.0;
            }
            
            var totalTaxableAmountInitial: Double = 0.0
            var amountAfterDiscountInitial: Double = 0.0
            
            self.initialStanTax = self.initialStanTax +  leadInspectionFee;
            amountAfterDiscountInitial = self.initialStanTax - (self.initialStanTax * discountPerInitial/100);
            totalTaxableAmountInitial = totalTaxableAmountInitial + amountAfterDiscountInitial;
            
            var taxAmountInitial = (totalTaxableAmountInitial * (Double(strTaxValue) ?? 0.0)) / 100;
            if taxAmountInitial < 0
            {
                taxAmountInitial = 0
            }
            //Set Taxable Initial Amount
            self.taxableInitialAmount = totalTaxableAmountInitial
            return taxAmountInitial
        }
        else {
            return 0.0
        }
    }
    func calculateTaxableAmountMaintPrice(creditDiscount: Double, subtotalMaint: Double) -> Double {
        
        var discountPerMaint = 0.0
        var totalDiscount = 0.0
        totalDiscount = creditDiscount
        if subtotalMaint > 0 {
            discountPerMaint = (totalDiscount / (subtotalMaint)) * 100
            if discountPerMaint < 0
            {
                discountPerMaint = 0.0
            }
            
            var totalTaxableAmountMaint = 0.0
            var amountAfterDiscountMaint = 0.0
            
            amountAfterDiscountMaint = self.initialMaintTax - (self.initialMaintTax * discountPerMaint/100)
            totalTaxableAmountMaint = totalTaxableAmountMaint + amountAfterDiscountMaint
            var taxAmountMaint = (totalTaxableAmountMaint * (Double(strTaxValue) ?? 0.0)) / 100
            if taxAmountMaint < 0
            {
                taxAmountMaint = 0
            }
            //Set Taxable Maint Amount
            self.taxableMaintAmount = totalTaxableAmountMaint

            return taxAmountMaint
        }else {
            return 0.0
        }
        
    }
    
    func updatePricingInfoDict() {
        
        //****Sub Total****
        self.dictPricingInfo["subTotalInitial"] = String(format: "$%.2f", self.subTotalInitialPrice)
        /**********************/

        //****Coupon Discount****
        self.dictPricingInfo["couponDiscount"] = String(format: "$%.2f", self.couponDiscountPrice)
        /**********************/

        //****Credit****
        if self.creditAmount > self.subTotalInitialPrice {
            self.creditAmount = self.subTotalInitialPrice
        }
        self.dictPricingInfo["credit"] = String(format: "$%.2f", self.creditAmount)
        /**********************/

        //****Other Discount****
        self.dictPricingInfo["otherDiscount"] = String(format: "$%.2f", self.otherDiscount)
        /**********************/
        
        //****Tip Discount****
        self.dictPricingInfo["tipDiscount"] = String(format: "$%.2f", self.tipDiscount)
        /**********************/
        
        //****Other Discount****
        self.dictPricingInfo["leadInspectionFee"] = String(format: "$%.2f", self.leadInspectionFee)
        /**********************/
        
        //****Total Price****
        self.totalPrice = (self.subTotalInitialPrice
                            - (self.creditAmount + self.couponDiscountPrice)) + (leadInspectionFee - otherDiscount - tipDiscount)
        if self.totalPrice < 0 {
            self.totalPrice = 0.0
        }
        let finalTotal = self.totalPrice// + (leadInspectionFee - otherDiscount - tipDiscount)
        self.dictPricingInfo["totalPrice"] = String(format: "$%.2f", finalTotal)
        /**********************/

        //****Tax Amount****
        self.dictPricingInfo["taxAmount"] = String(format: "$%.2f", self.taxAmount)
        /**********************/

        //****Billing Amount****
        self.billingAmount = finalTotal + self.taxAmount
        self.dictPricingInfo["billingAmount"] = String(format: "$%.2f", self.billingAmount)
        /**********************/
        
        
        
        
        //*Maintenance Service Price*
        //****SubTotal Amount****
        self.dictPricingInfo["subTotalMaintenance"] = String(format: "$%.2f", self.subTotalMaintenancePrice)
        /**********************/
        
        //****Credit Maint****
        if self.creditAmountMaint > self.subTotalMaintenancePrice {
            self.creditAmountMaint = self.subTotalMaintenancePrice
        }
        self.dictPricingInfo["creditMaint"] = String(format: "$%.2f", self.creditAmountMaint)
        /**********************/
        
        //****Total Price****y
        var totalMaint = self.subTotalMaintenancePrice - self.creditAmountMaint
        if totalMaint < 0 {
            totalMaint = 0.0
        }
        self.dictPricingInfo["totalPriceMaintenance"] = String(format: "$%.2f", totalMaint)
        /**********************/
        
        //****Tax Amount****
        self.dictPricingInfo["taxAmountMaintenance"] = String(format: "$%.2f", taxMaintAmount)
        /**********************/
        
        //****Billing Amount****
        let dueAmount = self.subTotalMaintenancePrice - self.creditAmountMaint + taxMaintAmount
        self.dictPricingInfo["totalDueAmount"] = String(format: "$%.2f", dueAmount)
        /**********************/
        
        /*Maint billing info*/
        if self.arrMaintBillingPrice.count > 0 {
            self.dictPricingInfo["arrMaintBillingPrice"] = self.arrMaintBillingPrice
        }
        else{
            self.dictPricingInfo["arrMaintBillingPrice"] = []
        }
        /**********************/
        
        self.dictPricingInfo["taxableAmount"] = String(format: "$%.2f", self.taxableInitialAmount)
        self.dictPricingInfo["taxableMaintAmount"] = String(format: "$%.2f", self.taxableMaintAmount)
        debugPrint(self.dictPricingInfo)
    }
    
    //MARK: - Applied Coupon Or Credit---------------------------------
    func getAppliedCouponOrCredit() {
        
        self.arrAppliedCoupon.removeAll()
        self.arrAppliedCredit.removeAll()
        
        self.arrAppliedCoupon = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon")
        self.arrAppliedCredit = self.fetchForAppliedDiscountFromCoreData(strType: "Credit")
        
        if self.arrAppliedCoupon.count > 0 {
            for coupon in self.arrAppliedCoupon {
                self.couponDiscountPrice = self.couponDiscountPrice + (Double("\(coupon.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            }
        }
        
        if self.arrAppliedCredit.count > 0 {
            for credit in self.arrAppliedCredit {
                
                if ("\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "true") && ("\(credit.value(forKey: "applicableForInitial") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForInitial") ?? "0")" == "true") {
                    
                    self.creditAmountMaint = self.creditAmountMaint + (Double("\(credit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)

                    self.creditAmount = self.creditAmount + (Double("\(credit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
                }else if "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "1" || "\(credit.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                    self.creditAmountMaint = self.creditAmountMaint + (Double("\(credit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)
                }else {
                    self.creditAmount = self.creditAmount + (Double("\(credit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
                }
            }
        }
    }
    
    func fetchForAppliedDiscountFromCoreData(strType: String) -> [NSManagedObject] {

        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
                
        var arrDiscountCoupon : [NSManagedObject] = []
        var arrDiscountCredit : [NSManagedObject] = []

        let arrStanAllSoldService = self.arrStandardServiceSold

        if arryOfData.count > 0 {
            for objMetchDiscount in arryOfData {
                let discountType = "\((objMetchDiscount as AnyObject).value(forKey: "discountType") ?? "")"
                if discountType == "Coupon" {
                    arrDiscountCoupon.append(objMetchDiscount as! NSManagedObject)
                }else {
                    arrDiscountCredit.append(objMetchDiscount as! NSManagedObject)
                }
            }
        }
        
        if strType == "Coupon"
        {
            return arrDiscountCoupon
        }else {
            return arrDiscountCredit
        }
    }
    
    //MARK:- delegate data source function
    
    func tableView_delegate_dataSource(){
        
        self.lblOppNumbber.text = strTitle as String
        self.lblOppTitle.text = strOppNumber as String
        self.configureTableView.delegate = self
        self.configureTableView.dataSource = self
    }
    
    //MARK- Fatch Target Image
    
    func fetchImageFromDB(strLeadCommercialTargetId : String ,strMobileTargetId : String )  -> NSArray{
        var arrayOfImages = NSArray ()
        
        //            let strLeadCommercialTargetId = "\(dictTarget.value(forKey: "leadCommercialTargetId") ?? "")"
        //            let strMobileTargetId = "\(dictTarget.value(forKey: "mobileTargetId") ?? "")"
        //
        
        strHeaderTitle = "Target"
        
        if strLeadCommercialTargetId.count > 0
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadCommercialTargetId = %@", strLeadId, strHeaderTitle, strLeadCommercialTargetId))
        }
        else
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetId == %@", strLeadId, strHeaderTitle, strMobileTargetId))
        }
        
        return arrayOfImages
    }
    
    //MARK:- Fatch Templeates
    
    func getLetterTemplateAndTermsOfService()
    {
        if(nsud.value(forKey: "MasterSalesAutomation") != nil)
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            
            let arrayLetterTemplateMaster = (dictMaster.value(forKey: "LetterTemplateMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            let arraySalesMarketingContentMaster = (dictMaster.value(forKey: "SalesMarketingContentMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            arrayTermsOfService = (dictMaster.value(forKey: "TermsOfServiceMaster") as! NSArray).mutableCopy() as! NSMutableArray
            
            
            // cover letter and Introduction
            for item in arrayLetterTemplateMaster
            {
                if("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "CoverLetter")
                {
                    arrayCoverLetter.add(item as! NSDictionary)
                }
                else if ("\((item as! NSDictionary).value(forKey: "LetterTemplateType") ?? "")" == "Introduction")
                {
                    arrayIntroduction.add(item as! NSDictionary)
                }
            }
            
            // Sales Marketing Content
            
            for item in arraySalesMarketingContentMaster
            {
                if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
                {
                    if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                    {
                        arrayMarketingContent.add(item as! NSDictionary)
                    }
                    else
                    {
                        arrayMarketingContent.add(item as! NSDictionary)
                    }
                }
            }
            
            // Terms & Conditions
            
            let arrayMultipleGeneralTermsConditions = (dictMaster.value(forKey: "MultipleGeneralTermsConditions") as! NSArray).mutableCopy() as! NSMutableArray
            
            for item in arrayMultipleGeneralTermsConditions
            {
                if((item as! NSDictionary).value(forKey: "IsActive") is Bool)
                {
                    if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true)
                    {
                        arrayTermsAndConditions.add(item as! NSDictionary)
                    }
                }
                
                if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true && (item as! NSDictionary).value(forKey: "IsDefault") as! Bool == true)
                {
                    arrayTermsAndConditionsMultipleSelected.add(item as! NSDictionary)
                    
                    //btnTermsCondition.setTitle("\((item as! NSDictionary).value(forKey: "TermsTitle")!)", for: .normal)
                    
                }
            }
            
        }
    }
    
    // MARK: ----------------------------  Agreement Checklist ----------------------------
    func agreementChecklist() {
        if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
        {
            let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
            if dictMaster.value(forKey: "AgreementChecklist") is NSArray
            {
                let arrChecklist = dictMaster.value(forKey: "AgreementChecklist") as! NSArray
                
                for agreeMentCheckIsActive in arrChecklist{
                    
                    let dictAgreementDetail: [String: Any] = agreeMentCheckIsActive as! [String : Any]
                    let strIsActive = "\(dictAgreementDetail["IsActive"] ?? "0")"
                    if strIsActive == "1" || strIsActive == "true" {
        
                        arrAgreementCheckList.add(agreeMentCheckIsActive as! NSDictionary)
                    }
                    
                }
                
            }
        }
    }
    
    
    func fetchAgreementCheckListSales() {
        
        arrSelectedAgreementChckList = []
        
        let arrayAgreementDetail = getDataFromCoreDataBaseArray(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        if(arrayAgreementDetail.count > 0)
        {
          
            for match in arrayAgreementDetail
            {
               
                for obj in arrAgreementCheckList{
                   
                    
                    let matches = match as! NSManagedObject
                    
                    //if("\((match as! NSManagedObject).value(forKey: "agreementChecklistSysName")!)" == "\((obj as! NSDictionary).value(forKey: "SysName")!)")
                    if("\((match as! NSManagedObject).value(forKey: "agreementChecklistId")!)" == "\((obj as! NSDictionary).value(forKey: "AgreementChecklistId")!)")
                    {
                        
                        arrSelectedAgreementChckList.add(obj as! NSDictionary)
                        
                        
                        
                        break
                    }
                      
                }
            }
        
        }else {
            
        }
        
    }
    
    //MARK: - Billing Freqancy Calculation
    func billingFreqancyCalculationMethod() {
        self.arrMaintBillingPrice.removeAll()
        
        let arrUniqFreqSysName = Set(self.arrayOfFreqSysName)
        for value in arrUniqFreqSysName {
            var price = 0.0
            for i in 0..<self.arrayOfFreqSysName.count
            {
                let strVal = self.arrayOfFreqSysName[i]
                let strValPrice = self.arrayOfFreqPrice[i]
                if value == strVal {
                    price = price + (Double(strValPrice) ?? 0.0)
                }
            }
            let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: value)
            let frqName = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
            let strMaintBillingPrice = "\(String(format: "$%.2f", price))/\(frqName)"
            self.arrMaintBillingPrice.append(strMaintBillingPrice)
        }
    }
        
    func billingFreqancyCalculationOnCredit() {
        if self.arrAppliedCredit.count > 0 {
            var maintApplied = 0.0
            for objApplied in self.arrAppliedCredit {
                var objApplied1 = NSManagedObject()
                objApplied1 = objApplied
                let strMaint: String = "\(objApplied1.value(forKey: "appliedMaintDiscount") ?? "0")"
                maintApplied = maintApplied + (Double(strMaint) ?? 0.0)
            }

            let serviceSold: [NSManagedObject] = (self.arrStandardServiceSold + self.arrCustomServiceSold)
            debugPrint(serviceSold)
            var strBilled = ""
            
            if serviceSold.count > 0 {
                var objCustomServiceDetail = NSManagedObject()
                objCustomServiceDetail = serviceSold[0]
                

                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "billingFrequencySysName") ?? "")")
                let dictServiceFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")")

                
                var maint: Double = (Double(objCustomServiceDetail.value(forKey: "maintenancePrice") as! String) ?? 0.0) - maintApplied//(((Double(objCustomServiceDetail.value(forKey: "totalMaintPrice") as! String) ?? 0.0) * unit) + sumFinalMaintPrice)
                if maint < 0
                {
                    maint = 0
                }
                
                
                //Billing Amount Calculation
                
                var totalMaintPrice = 0.0
                var totalBillingAmount = 0.0
                
                totalMaintPrice = Double(maint)

                let strServiceFrqYearOccurence = "\(dictServiceFrequency.value(forKey: "YearlyOccurrence") ?? "")"
                let strBillingFrqYearOccurence = "\(dictBillingFrequency.value(forKey: "YearlyOccurrence") ?? "")"

                if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("OneTime") == .orderedSame
                {
                }
                else
                {
                    if self.chkFreqConfiguration
                    {
                        if "\(objCustomServiceDetail.value(forKey: "frequencySysName") ?? "")".caseInsensitiveCompare("Yearly") == .orderedSame
                        {
                            totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                        else
                        {
                            totalBillingAmount = ( totalMaintPrice * (((strServiceFrqYearOccurence as NSString).doubleValue) - 1)) /  (strBillingFrqYearOccurence as NSString).doubleValue
                        }
                    }
                    else
                    {
                        totalBillingAmount = ( totalMaintPrice * (strServiceFrqYearOccurence as NSString).doubleValue) /  (strBillingFrqYearOccurence as NSString).doubleValue
                    }
                }
                
                if totalBillingAmount < 0
                {
                    totalBillingAmount = 0.0
                }
                
                strBilled = "\(convertDoubleToCurrency(amount: Double(("\(totalBillingAmount)" as NSString).doubleValue)))" + "/" +  "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                
                debugPrint(strBilled)
                
                //BillingFrequencyPrice - Billing Amount TotalMaintPrice - Total Maintenance Price
            }
            
            //Replace Object
            let arrBilled = strBilled.components(separatedBy: "/")
            
            debugPrint(self.arrMaintBillingPrice)
            debugPrint(self.arrayOfFreqSysName)
            debugPrint(self.arrayOfFreqPrice)

            for i in 0..<self.arrayOfFreqSysName.count
            {
                if self.arrayOfFreqSysName[i] == arrBilled[1] {
                    self.arrayOfFreqPrice[i] = arrBilled[0].replacingOccurrences(of: "$", with: "", options: NSString.CompareOptions.literal, range: nil)
                    break
                }
            }
            debugPrint(self.arrayOfFreqPrice)

            self.arrMaintBillingPrice.removeAll()
            let arrUniqFreqSysName = Set(self.arrayOfFreqSysName)
            for value in arrUniqFreqSysName {
                var price = 0.0
                for i in 0..<self.arrayOfFreqSysName.count
                {
                    let strVal = self.arrayOfFreqSysName[i]
                    let strValPrice = self.arrayOfFreqPrice[i]
                    if value == strVal {
                        price = price + (Double(strValPrice) ?? 0.0)
                    }
                }
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: value)
                let frqName = "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                debugPrint(frqName)
                let strMaintBillingPrice = "\(String(format: "$%.2f", price))/\(frqName)"
                self.arrMaintBillingPrice.append(strMaintBillingPrice)
            }
            debugPrint(self.arrMaintBillingPrice)
        }
    }
    
    func checkForAppliedDiscount(strType: String, strDiscountCode: String, couponUsage: String) -> Bool {
        debugPrint(strType, strDiscountCode, couponUsage)
        
        
        var predicate: NSPredicate?
        if strType == "Credit" {
            if couponUsage == "OneTime" {
                predicate = NSPredicate(format: "accountNo == %@ && discountSysName == %@", self.strAccountNoGlobal, strDiscountCode)
            }else if couponUsage == "Multiple" {
                predicate = NSPredicate(format: "leadId == %@ && discountSysName == %@", self.strLeadId, strDiscountCode)
            }else {
                predicate = NSPredicate(format: "leadId == %@ && discountSysName == %@", self.strLeadId, strDiscountCode)
            }
        }else {
            if couponUsage == "OneTime" {
                predicate = NSPredicate(format: "accountNo == %@ && discountCode == %@", self.strAccountNoGlobal, strDiscountCode)
            }else if couponUsage == "Multiple" {
                predicate = NSPredicate(format: "leadId == %@ && discountCode == %@", self.strLeadId, strDiscountCode)
            }else {
                predicate = NSPredicate(format: "leadId == %@ && discountCode == %@", self.strLeadId, strDiscountCode)
            }
        }
        
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialDiscountExtDc", predicate: predicate!)
        
        debugPrint(arryOfData)
        
        if couponUsage == "OneTime" {
            if arryOfData.count == 0 {
                return false
            }else {
                return true
            }
        }else if couponUsage == "Multiple" {
            if arryOfData.count == 0 {
                return false
            }else {
                return true
            }
        }else {
            return true
        }
    }
    
    func checkCouponValidity(dictCouponDetail: [String: Any]) -> Bool {
        
        
        var strValidFrom =  changeStringDateToGivenFormat(strDate: "\(dictCouponDetail["ValidFrom"] ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidFrom.count == 0 || strValidFrom == ""
        {
            strValidFrom = Global().getCurrentDate()
        }
        
        var strValidTo = changeStringDateToGivenFormat(strDate: "\(dictCouponDetail["ValidTo"] ?? "")", strRequiredFormat: "MM/dd/yyyy")
        
        if strValidTo.count == 0 || strValidTo == ""
        {
            strValidTo = Global().getCurrentDate()
        }
        
        let strCurrentDate = changeStringDateToGivenFormat(strDate: Global().getCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        
        //01 July -  05 July   02 July
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let currentDate = dateFormatter.date(from:strCurrentDate)!
        let validFrom = dateFormatter.date(from:strValidFrom)!
        let validTo = dateFormatter.date(from:strValidTo)!

        if currentDate >= validFrom && currentDate <= validTo
        {
            return true
        }
        else
        {
            return false
        }
       
    }
    
    // MARK: ----------------------------  Month Of Services  ----------------------------
    func getLoginDetails() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        self.chkFreqConfiguration =   (dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1")) as! Bool
    }
    
    func getLeadDetails() {
        
        self.objLeadDetails = getLeadDetail(strLeadId: strLeadId as String, strUserName: Global().getUserName() ?? "")

        self.strGlobalStageSysName = "\(self.objLeadDetails?.value(forKey: "stageSysName") ?? "")"
        self.strGlobalStatusSysName = "\(self.objLeadDetails?.value(forKey: "statusSysName") ?? "")"
        
        self.strAccountNoGlobal = "\(self.objLeadDetails!.value(forKey: "accountNo")!)"
        self.strLeadNumberLead = "\(self.objLeadDetails!.value(forKey: "leadNumber")!)"

        //Get months from local DB
        self.strPreferedMonth = "\(self.objLeadDetails!.value(forKey: "strPreferredMonth")!)"
        if self.strPreferedMonth == "0" || self.strPreferedMonth == "" {
        }else{
            arrayOfMonth = strPreferedMonth.components(separatedBy: ", ")
        }
        
        strInternalNotes = "\(self.objLeadDetails!.value(forKey: "notes")!)"
        
        self.strTaxValue = "\(objLeadDetails!.value(forKey: "tax")!)"
        self.strTaxCode = "\(objLeadDetails!.value(forKey: "taxSysName")!)"
        self.strIsServiceAddrTaxExempt = "\(objLeadDetails!.value(forKey: "isServiceAddrTaxExempt")!)"
        self.strServiceAddressSubType = "\(objLeadDetails!.value(forKey: "serviceAddressSubType")!)"
        
        let otherDis = "\(objLeadDetails!.value(forKey: "otherDiscount")!)"
        let tipDis = "\(objLeadDetails!.value(forKey: "tipDiscount")!)"
        let leadInsFee = "\(objLeadDetails!.value(forKey: "leadInspectionFee")!)"
        if otherDis != "" {
            self.otherDiscount = Double(otherDis.replacingOccurrences(of: "$", with: "", options: .literal, range: nil)) ?? 0.0
        }
        if tipDis != "" {
            self.tipDiscount = Double(tipDis.replacingOccurrences(of: "$", with: "", options: .literal, range: nil)) ?? 0.0
        }
        if leadInsFee != "" {
            self.leadInspectionFee = Double(leadInsFee.replacingOccurrences(of: "$", with: "", options: .literal, range: nil)) ?? 0.0
        }
        
       
        if self.strTaxValue == "" && self.strTaxCode != "" {
            self.getTaxCodeDetailWithApi(strTaxCode: self.strTaxCode)
        }
    }
    
    func fetchPaymentInfo() {
        
        let arrPaymentInfo = getDataFromCoreDataBaseArray(strEntity: "PaymentInfo", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        //objPaymentInfo
        debugPrint(arrPaymentInfo)
        if(arrPaymentInfo.count > 0)
        {
            objPaymentInfo = (arrPaymentInfo.firstObject as! NSManagedObject)
            
            //
            self.strAddtionalNotes = "\(objPaymentInfo!.value(forKey: "specialInstructions")!)"
            
        }else {
            self.savePaymentInfo()
        }
    }
    
    func savePaymentInfo () {
        let context = getContext()
        // let entityPaymentInfo = PaymentInfo(context: context)
        //        let entityPaymentInfo = NSManagedObject(entity: PaymentInfo.Type, insertInto: context)
        let entityPaymentInfo = NSEntityDescription.insertNewObject(forEntityName: "PaymentInfo", into: context)
        
        entityPaymentInfo.setValue(self.strLeadId, forKey: "leadId")
        entityPaymentInfo.setValue("", forKey: "leadPaymentDetailId")
        entityPaymentInfo.setValue("", forKey: "paymentMode")
        entityPaymentInfo.setValue("", forKey: "amount")
        entityPaymentInfo.setValue("", forKey: "checkNo")
        entityPaymentInfo.setValue("", forKey: "licenseNo")
        entityPaymentInfo.setValue("", forKey: "expirationDate")
        entityPaymentInfo.setValue("", forKey: "specialInstructions")
        entityPaymentInfo.setValue("", forKey: "agreement")
        entityPaymentInfo.setValue("", forKey: "proposal")
        entityPaymentInfo.setValue("", forKey: "customerSignature")
        entityPaymentInfo.setValue("", forKey: "salesSignature")
        entityPaymentInfo.setValue("", forKey: "createdBy")
        entityPaymentInfo.setValue("", forKey: "createdDate")
        entityPaymentInfo.setValue("", forKey: "modifiedBy")
        entityPaymentInfo.setValue("", forKey: "modifiedDate")
        entityPaymentInfo.setValue(Global().getUserName(), forKey: "userName")
        entityPaymentInfo.setValue(Global().getCompanyKey(), forKey: "companyKey")
        entityPaymentInfo.setValue("", forKey: "checkBackImagePath")
        entityPaymentInfo.setValue("", forKey: "checkFrontImagePath")
        entityPaymentInfo.setValue("", forKey: "inspection")
        
        
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.fetchPaymentInfo()
        
    }
   
    // MARK: ----------------------------  Fetch from local DB ----------------------------
    
    func fetchLetterTemplateFromLocalDB()
    {
        let arrayLetterTemplate = getDataFromLocal(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        if(arrayLetterTemplate.count > 0)
        {
            let match = arrayLetterTemplate.firstObject as! NSManagedObject
            
             
            // cover letter
            if("\(match.value(forKey: "coverLetterSysName") ?? "")".count > 0)
            {
                for item in arrayCoverLetter
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "coverLetterSysName")!)")
                    {
                        
                        
                        dictCoverLetter = item as! NSDictionary
                        
                        self.strCoverLetter = dictCoverLetter.value(forKey: "TemplateName") as? String ?? ""
                        break
                    }
                }
            }
            else
            {
                
            }
            
            // Introduction letter
            
            if("\(match.value(forKey: "introSysName") ?? "")".count > 0)
            {
                for item in arrayIntroduction
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "introSysName")!)")
                    {
                        dictIntroduction = item as! NSDictionary
                        self.strIntroductionLetter = dictIntroduction.value(forKey: "TemplateName") as? String ?? ""
                        TemplateContent = "\(match.value(forKey: "introContent") ?? "")" //dictIntroduction.value(forKey: "TemplateContent") as? String ?? ""
                        
                        debugPrint("----------????")
                        debugPrint(TemplateContent)
                        
                        let dictInspector = global.getInspectorDetails(strInspectorId)! as NSDictionary
                        debugPrint("0000000---^^^^")
                        
                        debugPrint(dictInspector)
                        
                        for i in 0 ..< 4 {
                            
                           // strMutableTextIntroductionLetter = NSMutableString(string: strMutableTextIntroductionLetter.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName))
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyName]", with: CustomerCompanyName)
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceAddress]", with: BillingAddressName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Logo]", with: strCompanyLogoPath)
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BranchLogo]", with: strBranchLogoImagePath)
                            let strCustomTagBranchLogoImage = "<img src=\(strBranchLogoImagePath) style = max-width:170px  />"
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BranchLogoImage]", with: strCustomTagBranchLogoImage)

                            let strCustomTag = "<img src=\(strCustomerCompanyProfileImage) style = max-width:170px  />"
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyLogoImage]", with: strCustomTag)

                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CustomerCompanyLogo]", with: strCustomerCompanyProfileImage)

                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceDateTime]", with: global.getCurrentDate())
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CurrentDate]", with: global.getCurrentDate())
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[ServiceAddressName]", with: BillingAddressName)
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingAddress]", with: BillingAddressName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingAddressName]", with: BillingAddressName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountNo]", with: strAccountNo)

                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[LeadNo]", with: strLeadNumber)
                            
                            if(dictInspector.count > 0)
                            {
                                
                                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "\(dictInspector.value(forKey: "PrimaryPhone") ?? "")" as String)
                                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorEmail]", with: "\(dictInspector.value(forKey: "PrimaryEmail") ?? "")" as String)
                            
                            }
                            else
                            {
                                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorPrimaryPhone]", with: "")
                                TemplateContent = TemplateContent?.replacingOccurrences(of: "[InspectorEmail]", with:  "")
                            
                            }
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Technician]", with: strEmpName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerName]", with: strAccountManagerName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerEmail]", with: strAccountManagerEmail)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CellPhone]", with: CellPhone)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CellNumber]", with: strCellNo)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[OpportunityContactName]", with: strCustomerName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[Licenseno]", with: "")
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[PrimaryPhone]", with: PrimaryPhone)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerPrimaryPhone]", with: strAccountManagerPrimaryPhone)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CompanyAddress]", with: strCompanyAddress)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOC]", with: strBillingPocName)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCPrimaryPhone]", with: billingPrimaryPhone)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCSecondaryPhone]", with: self.billingSecondaryPhone)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCCell]", with: self.billingCellNo)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[BillingPOCEmailID]", with: self.billingPrimaryEmail)
                            
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[CompanyBusinessLicenseNo]", with: self.BusinessLicenceNo)
                            
                            TemplateContent = TemplateContent?.replacingOccurrences(of: "[AccountManagerEmai]", with: self.strAccountManagerEmail)
                            
                            
                            
                        }
                        
                    }
                }
            }
            else
            {
                
            }
            
            // Terms Of Service
            if("\(match.value(forKey: "termsOfServiceSysName") ?? "")".count > 0)
            {
                for item in arrayTermsOfService
                {
                    if("\((item as! NSDictionary).value(forKey: "SysName")!)" == "\(match.value(forKey: "termsOfServiceSysName")!)")
                    {
                        dictTermsOfService = item as! NSDictionary
                        
                        //btnTermsService.setTitle("\(dictTermsOfService.value(forKey: "Title")!)", for: .normal)
                        let i = dictTermsOfService.value(forKey: "Title")
                        let Description = dictTermsOfService.value(forKey: "Description")
                        
                        self.strTermsOfService = i as! String
                        
                        if Description as! String == "\(match.value(forKey: "termsOfServiceContent") ?? "")"{
                            self.strDescriptionTermsService = Description as! String
                        }
                        else{
                            self.strDescriptionTermsService = "\(match.value(forKey: "termsOfServiceContent") ?? "")"
                        }
                           
                    }
                }
            }
            else
            {
                
            }
            
            self.strIsAgreementValidForNotes = "\(match.value(forKey: "isAgreementValidFor") ?? "")"
            self.strValidFor = "\(match.value(forKey: "validFor") ?? "")"
            txtNoOfDays = self.strValidFor
            
            if strIsAgreementValidForNotes == "false"{
                self.strValidFor = ""
                txtNoOfDays = self.strValidFor
            }
        }
    }
    
    fileprivate func fetchMarketingContentFromLocalDB()
    {
        let arrayMatches = getDataFromLocal(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))

        if(arrayMatches.count > 0)
        {
           
            for match in arrayMatches
            {
                for item in arrayMarketingContent
                {
                   
                    if("\((match as! NSManagedObject).value(forKey: "contentSysName")!)" == "\((item as! NSDictionary).value(forKey: "SysName")!)")
                    {
                        arrayMarketingContentMultipleSelected.add(item as! NSDictionary)
                        break
                    }
                }
            }
            if(arrayMarketingContentMultipleSelected.count > 0)
            {
                var termsTitle = ""
                for item in arrayMarketingContentMultipleSelected
                {
                    termsTitle = termsTitle + "\((item as! NSDictionary).value(forKey: "Title")!)" + ","
                }
                termsTitle.removeLast()
            }
        }
    }
    
    //MARK: - CoreData Functions
    
    //==================TARGETS
    func getTargetFromCoreData(){
        arrOfTagests.removeAllObjects()
       
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialTargetExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfTagests.add(dict)
            }
        }
    }
    
    
    //==================Scope
    func getScopeFromCoreData(){
        arrOfScopes.removeAllObjects()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "LeadCommercialScopeExtDc", predicate: NSPredicate(format: "leadId == %@", strLeadId))
    
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                arrOfScopes.add(dict)
            }
        }
    }
    
    //=========================Stardard Service
    func fetchStandardServiceData(){
        self.arrStandardService.removeAll()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                self.arrStandardService.append(dict)

            }
            
            self.subtotalStandard()
        }
    }
    
    //=========================Non Stardard Service
    func fetchCustomServiceData(){
        self.arrCustomService.removeAll()
        
        let arryOfData = getDataFromCoreDataBaseArray(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@", strLeadId))
        if arryOfData.count > 0 {
            for item in arryOfData{
                let dict = item as! NSManagedObject
                self.arrCustomService.append(dict)
            }
            
            self.subtotalNonStandard()
        }
        
        //Getting Credit from API Call
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

        let strDetailUrl = "/api/CoreToSaleAuto/GetDiscountsForAccountExcludingLeadNo?"
        //let strType = "Credit"
        let strEmpBranchSysName = "\(dictLoginData.value(forKeyPath: "EmployeeBranchSysName") ?? "")"
        let strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        
        
        let strUrl = "\(strServiceUrlMain)\(strDetailUrl)companyKey=\(strCompanyKey)&branchSysName=\(strEmpBranchSysName)&AccountNo=\(strAccountNoGlobal)&&leadNoToExclude=\(self.strLeadNumberLead)"
        debugPrint(strUrl)
        self.presenter.requestToGetAppliedCredit(apiUrl: strUrl)
    }
    
    func fetchRenewalServiceFromCoreData(soldServiceId: String) -> String {
        debugPrint(soldServiceId)
        var strRenewalDesc = ""
        
        let arrAllObjRenewal = getDataFromCoreDataBaseArray(strEntity: "RenewalServiceExtDcs", predicate: NSPredicate(format: "leadId == %@ && serviceId = %@", strLeadId, soldServiceId))
        
        if arrAllObjRenewal.count > 0 {
            let matchesRenewal: NSManagedObject = arrAllObjRenewal.object(at: 0) as! NSManagedObject
            debugPrint(matchesRenewal)
            var strDesc = "\(matchesRenewal.value(forKey: "renewalDescription") ?? "")"
            if strDesc != "" {
                strDesc = strDesc + ":"
            }
            
            //let strAmount = "\(matchesRenewal.value(forKey: "renewalAmount") ?? "")"
            
            let strAmount =  "\(convertDoubleToCurrency(amount: Double("\(matchesRenewal.value(forKey: "renewalAmount") ?? "")") ?? 0.0))"
            
            let dict = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(matchesRenewal.value(forKey: "renewalFrequencySysName") ?? "")")
            let strFrequency = dict.value(forKey: "FrequencyName") ?? ""
            strRenewalDesc = "\(strDesc) \(strAmount) (\(strFrequency))"
        }
        
        return strRenewalDesc
    }
    func subtotalStandard() {
        debugPrint(self.arrStandardService)
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        self.arrStandardServiceSold.removeAll()
        self.arrStandardServiceNotSold.removeAll()
        
        for objServiceDetail in self.arrStandardService {
            
            if objServiceDetail.value(forKey: "isSold") as! String == "true" || objServiceDetail.value(forKey: "isSold") as! String == "1" {
                        
                //********Add Sold Standard Service********
                self.arrStandardServiceSold.append(objServiceDetail)

                                
                //********Calculations********
                //Initial Price
                let initialPrice: String = objServiceDetail.value(forKey: "initialPrice") as! String
                totalInitialPrice = totalInitialPrice + ((Double(initialPrice) ?? 0.0))
                if totalInitialPrice < 0
                {
                    totalInitialPrice = 0
                }
                
                
                //Discount Price
                let discountPrice: String = objServiceDetail.value(forKey: "discount") as! String
                totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
                
                //totalMaintPrice
                let maintPrice: String = objServiceDetail.value(forKey: "totalMaintPrice") as! String
                totalMaintPrice = totalMaintPrice + (Double(maintPrice) ?? 0.0)
                if totalMaintPrice < 0
                {
                    totalMaintPrice = 0
                }
                
                //Total  Billing Frequancy
                self.arrayOfFreqPrice.append(objServiceDetail.value(forKey: "billingFrequencyPrice") as! String)
                let billingFrequencySysName: String = objServiceDetail.value(forKey: "billingFrequencySysName") as! String
                self.arrayOfFreqSysName.append(billingFrequencySysName)
                
                //Tax Calculations
                if self.strIsServiceAddrTaxExempt == "false" {
                    if strServiceAddressSubType == "Commercial" {
                        let strTemp = objServiceDetail.value(forKey: "serviceSysName") ?? ""
                        if "\(self.dictCommercialStatus.value(forKey: "\(strTemp)") ?? "")" == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"

                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)

                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                            debugPrint(self.initialStanTax, self.initialMaintTax)
                        }
                    }else {
                        let strTemp = objServiceDetail.value(forKey: "serviceSysName") ?? ""
                        if "\(self.dictResidentialStatus.value(forKey: "\(strTemp)") ?? "")" == "1" {
                            let strInitial = "\(objServiceDetail.value(forKey: "totalInitialPrice") ?? "0")"

                            self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                            
                            self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)

                            debugPrint(self.initialStanTax, self.initialMaintTax)
                        }
                    }
                }
        
            }else {
                //********Add non Sold Standard Service********
                self.arrStandardServiceNotSold.append(objServiceDetail)
            }
        }
        
        self.subTotalInitialPrice       = self.subTotalInitialPrice + totalInitialPrice
        self.subTotalMaintenancePrice   = self.subTotalMaintenancePrice + totalMaintPrice
        
    }
    
    func subtotalNonStandard() {
        debugPrint(self.arrCustomService)
        var totalInitialPrice = 0.0
        var totalDiscountPrice = 0.0
        var totalMaintPrice = 0.0
        self.arrCustomServiceSold.removeAll()
        self.arrCustomServiceNotSold.removeAll()
        
        
        for objCustomServiceDetail in self.arrCustomService {
            
            if objCustomServiceDetail.value(forKey: "isSold") as! String == "true" || objCustomServiceDetail.value(forKey: "isSold") as! String == "1" {
                        
                //********Add Sold Custom Standard Service********
                self.arrCustomServiceSold.append(objCustomServiceDetail)

                                
                //********Calculations********
                //Initial Price
                let initialPrice: String = objCustomServiceDetail.value(forKey: "initialPrice") as! String
                totalInitialPrice = totalInitialPrice + ((Double(initialPrice) ?? 0.0))
                if totalInitialPrice < 0
                {
                    totalInitialPrice = 0
                }
                
                
                //Discount Price
                let discountPrice: String = objCustomServiceDetail.value(forKey: "discount") as! String
                totalDiscountPrice = totalDiscountPrice + (Double(discountPrice) ?? 0.0)
                
                //totalMaintPrice
                let maintPrice: String = objCustomServiceDetail.value(forKey: "maintenancePrice") as! String
                totalMaintPrice = totalMaintPrice + (Double(maintPrice) ?? 0.0)
                if totalMaintPrice < 0
                {
                    totalMaintPrice = 0
                }
        
                //Total  Billing Frequancy
                self.arrayOfFreqPrice.append(objCustomServiceDetail.value(forKey: "billingFrequencyPrice") as! String)

                let billingFrequencySysName: String = objCustomServiceDetail.value(forKey: "billingFrequencySysName") as! String
                self.arrayOfFreqSysName.append(billingFrequencySysName)
                
                //Tax Calculations
                print(objCustomServiceDetail.value(forKey: "isTaxable") as! String)
                if ("\(objCustomServiceDetail.value(forKey: "isTaxable") ?? "false")" == "true" || "\(objCustomServiceDetail.value(forKey: "isTaxable") ?? "0")" == "1") && (strIsServiceAddrTaxExempt.caseInsensitiveCompare("false") == .orderedSame) {
                    if strServiceAddressSubType == "Commercial" {
                        let strInitial = "\(objCustomServiceDetail.value(forKey: "initialPrice") ?? "0")"
                        
                        self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                        
                        self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)
                        
                        debugPrint(self.initialStanTax, self.initialMaintTax)
                    }else {
                        let strInitial = "\(objCustomServiceDetail.value(forKey: "initialPrice") ?? "0")"
                        
                        self.initialStanTax = self.initialStanTax + (Double(strInitial) ?? 0.0)
                        
                        self.initialMaintTax  = self.initialMaintTax + (Double(maintPrice) ?? 0.0)
                        
                        debugPrint(self.initialStanTax, self.initialMaintTax)
                    }
                }
                
            }else {
                //********Add non Sold Custom Standard Service********
                self.arrCustomServiceNotSold.append(objCustomServiceDetail)
            }
        }
        
        self.subTotalInitialPrice       = self.subTotalInitialPrice + totalInitialPrice
        self.subTotalMaintenancePrice   = self.subTotalMaintenancePrice + totalMaintPrice
        
    }
   
    func goToEmailHistory()
    {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.EmailHistory
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func goToServiceHistory(){
        let storyboardIpad = UIStoryboard.init(name: "MechanicaliPad", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "ServiceHistoryMechanical") as? ServiceHistoryMechanical
        testController?.strFromWhere = "NewSalesFlow"
        testController?.strAccNoSalesFlow =  "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        self.navigationController?.pushViewController(testController!, animated: false)
    }
    
    func goToUploadDocument(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "SalesMainiPad", bundle:nil)
        let objUploadDocumentSales = storyBoard.instantiateViewController(withIdentifier: "UploadDocumentSalesiPad") as! UploadDocumentSales

        objUploadDocumentSales.strWoId = strLeadId
        self.navigationController?.present(objUploadDocumentSales, animated: false, completion: nil)
        
    }
    
    func goToGlobalmage(strType: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "PestiPhone", bundle:nil)
        let objGlobalImageVC = storyBoard.instantiateViewController(withIdentifier: "GlobalImageVC") as! GlobalImageVC
        
        objGlobalImageVC.strHeaderTitle = strType as NSString;
        objGlobalImageVC.strWoId = strLeadId;
        objGlobalImageVC.strWoLeadId = strLeadId;
        objGlobalImageVC.strModuleType = "SalesFlow"
        
        if("\(dataGeneralInfo.value(forKey: "statusSysName") ?? "")".lowercased() == "complete" && "\(dataGeneralInfo.value(forKey: "stageSysName") ?? "")".lowercased() == "lost")
        {
            objGlobalImageVC.strWoStatus = "Complete"
        }
        else{
            objGlobalImageVC.strWoStatus = "InComplete"
        }
        objGlobalImageVC.strWoStatus = "InComplete"
        
        self.navigationController?.present(objGlobalImageVC, animated: false, completion: nil)
    }
    
    
    // MARK: Go to Appointment
    func goToAppointment()
    {
           
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            

            
            var isAppointmentFound = false
            
            var controllerVC = UIViewController()

//            do
//            {
//                sleep(1)
//            }
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is AppointmentVC {
                    
                    isAppointmentFound = true
                    controllerVC = aViewController
                    //break
                    //self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
            if isAppointmentFound
            {
                nsud.setValue(true, forKey: "RefreshAppointmentList")
                nsud.synchronize()
                
                self.navigationController?.popToViewController(controllerVC, animated: false)
                print("Pop to appointment")
            }
            else
            {
                
                print("Push to appointment")

                let defsAppointemnts = UserDefaults.standard
                let strAppointmentFlow = defsAppointemnts.value(forKey: "AppointmentFlow") as?
                    String
                if strAppointmentFlow == "New"
                {
                    if DeviceType.IS_IPAD
                    {
                        let storyBoard = UIStoryboard(name: "Appointment_iPAD", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                    else
                    {
                        let storyBoard = UIStoryboard(name: "Appointment", bundle: nil)
                        let objAppointmentView = storyBoard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentVC") as! AppointmentVC
                        self.navigationController?.pushViewController(objAppointmentView, animated: false)

                    }
                }
                else
                {
                    if DeviceType.IS_IPAD
                    {
                        let mainStoryboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentViewiPad") as! AppointmentViewiPad
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                    else
                    {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier:
                                                                                        "AppointmentView") as! AppointmentView
                        self.navigationController?.pushViewController(objByProductVC, animated: false)

                    }
                }
            }
            
        }
    }
    
    func goToSetupHistory()  {
        
        let storyboardIpad = UIStoryboard.init(name: "ServicePestNewFlow", bundle: nil)
              let testController = storyboardIpad.instantiateViewController(withIdentifier: "CurrentSetupVC") as? CurrentSetupVC
              testController?.modalPresentationStyle = .fullScreen
              testController?.strAccountNo = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
              testController?.strCompanyKey = Global().getCompanyKey()
              testController?.strUserName = Global().getUserName()

              self.present(testController!, animated: false, completion: nil)
    }
    
    
    func goToServiceNotesHistory()  {
        
        //ServiceNotesHistoryViewController
        let storyboardIpad = UIStoryboard.init(name: "NewSales_GeneralInfo", bundle: nil)
        let testController = storyboardIpad.instantiateViewController(withIdentifier: "SalesNew_NotesHistory") as? SalesNew_NotesHistory
        testController!.strIsFromSales = true;
        testController!.strAccccountId = "\(dataGeneralInfo.value(forKey: "accountNo") ?? "")"
        testController!.strLeadNo = "\(dataGeneralInfo.value(forKey: "leadNumber") ?? "")"
        testController?.modalPresentationStyle = .fullScreen
        self.present(testController!, animated: false, completion: nil)
        
    }
  
    // MARK: Save to local DB
    func saveLetterTemplateCoverLetterIntroductionLetter(dictCoverLetter:NSDictionary ,dictIntroLetter:NSDictionary ,dictTermsOfService :NSDictionary)
    {
        deleteAllRecordsFromDB(strEntity: "LeadCommercialDetailExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        arrOfKeys.add("companyKey")
        arrOfKeys.add("userName")
        arrOfKeys.add("leadId")
        arrOfKeys.add("coverLetterSysName")
        arrOfKeys.add("introSysName")
        arrOfKeys.add("introContent")
        arrOfKeys.add("termsOfServiceSysName")
        arrOfKeys.add("termsOfServiceContent")
        arrOfKeys.add("isAgreementValidFor")
        arrOfKeys.add("validFor")
        
        // values
        arrOfValues.add(Global().getCompanyKey())
        arrOfValues.add(Global().getUserName())
        arrOfValues.add(strLeadId)
        arrOfValues.add(dictCoverLetter.value(forKey: "SysName") ?? "")
        
        // Intro Letter
        if(dictIntroLetter.count > 0)
        {
            arrOfValues.add("\(dictIntroLetter.value(forKey: "SysName")!)")
            arrOfValues.add(TemplateContent ?? "")
        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        if(dictTermsOfService.count > 0)
        {
            debugPrint("termsOfServices")
            arrOfValues.add("\(dictTermsOfService.value(forKey: "SysName")!)")
            //arrOfValues.add("\(converHTML(html: dictTermsOfService.value(forKey: "Description")! as! String))")
            
            arrOfValues.add(strDescriptionTermsService)
        }
        else
        {
            arrOfValues.add("")
            arrOfValues.add("")
        }
        
        debugPrint("-------------------")
        debugPrint(strIsAgreementValidForNotes)
        
        if strIsAgreementValidForNotes == "" {
            
        }else{
            
            
                    if strIsAgreementValidForNotes == "true" || strIsAgreementValidForNotes == "1"
                    {
                        if txtNoOfDays == ""
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter days for agreement validity", viewcontrol: self)
            
                            return
            
                        }
                        else
                        {
                            //arrOfValues.add(strIsAgreementValidFor)
                            //arrOfValues.add(txtNoOfDays)
                        }
            
                    }
        }
        
        debugPrint("brake-----")
        
        arrOfValues.add((strIsAgreementValidForNotes == "1" || strIsAgreementValidForNotes == "true") ? "true" : "false")
        arrOfValues.add(txtNoOfDays)
       
        saveDataInDB(strEntity: "LeadCommercialDetailExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
         
    }
    
    // MARK: Save Marketing Content local DB
    func saveMarketingContent(arraySeletecMarketingContent:NSMutableArray)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        if(arraySeletecMarketingContent.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
            
            debugPrint("save1--------")
            debugPrint(arraySeletecMarketingContent.count)
            
            
            
            for item in arraySeletecMarketingContent
            {
                // keys
                arrOfKeys.add("companyKey")
                arrOfKeys.add("userName")
                arrOfKeys.add("leadId")
                arrOfKeys.add("contentSysName")
                
                
                // values
                arrOfValues.add(Global().getCompanyKey())
                arrOfValues.add(Global().getUserName())
                arrOfValues.add(strLeadId)
                arrOfValues.add("\((item as! NSDictionary).value(forKey: "SysName")!)")
                
                saveDataInDB(strEntity: "LeadMarketingContentExtDc", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
        }else{
            
            debugPrint("save2--------")
            deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strLeadId))
        }
        
        
    }
    
    // MARK: Save Agreement Checklist local DB
    func saveAgreementChekList(arrayAgreementChecklistContent:NSMutableArray)
    {
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        
        
        if(arrayAgreementChecklistContent.count > 0)
        {
            deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@",strLeadId))
            
            debugPrint("save1--------")
            debugPrint(arrayAgreementChecklistContent.count)
            
            for item in arrayAgreementChecklistContent
            {
                // keys
                
                let strActive = "\((item as! NSDictionary).value(forKey: "IsActive") ?? "")"
                
                arrOfKeys.add("leadId")
                arrOfKeys.add("leadAgreementChecklistSetupId")
                arrOfKeys.add("agreementChecklistId")
                arrOfKeys.add("agreementChecklistSysName")
                arrOfKeys.add("userName")
                arrOfKeys.add("companyKey")
                arrOfKeys.add("isActive")

                // values
                
                arrOfValues.add(strLeadId)
                arrOfValues.add("0")
                arrOfValues.add("\((item as! NSDictionary).value(forKey: "AgreementChecklistId")!)")
                arrOfValues.add("\((item as! NSDictionary).value(forKey: "SysName")!)")
                arrOfValues.add(strUserName)
                arrOfValues.add(strCompanyKey)
                arrOfValues.add(strActive == "1" ? "true" : "false")

                
                saveDataInDB(strEntity: "LeadAgreementChecklistSetups", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
            
            
        }else{
            
           // deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
            
        }
        
        
        
    }
    
    
    // MARK: Delete Agreement Checklist local DB
    func checkListAgreementDelete(arrayAgreementChecklistContent:NSMutableArray){
        
        let arrOfKeys = NSMutableArray()
        let arrOfValues = NSMutableArray()
        
        
        
        if(arrayAgreementChecklistContent.count > 0)
        {
           // deleteAllRecordsFromDB(strEntity: "LeadMarketingContentExtDc", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
            
            debugPrint("save1--------")
            debugPrint(arrayAgreementChecklistContent.count)
            
            
            
            for item in arrayAgreementChecklistContent
            {
              
                
                // keys
                
                arrOfKeys.add("leadId")
                arrOfKeys.add("leadAgreementChecklistSetupId")
                arrOfKeys.add("agreementChecklistId")
                arrOfKeys.add("agreementChecklistSysName")
                
                // values
                
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                arrOfValues.add("")
                
                saveDataInDB(strEntity: "LeadAgreementChecklistSetups", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
            }
            
            
            
        }else{
            
           // deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@",strWoLeadId))
            
        }
        
        
       
        
    }
    
    //MARK:- *CoreData Update For Credit & Coupon*
    /***************CoreData Update for Credit & Coupon********************/
    func saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: [String: Any], strType: String) {
        debugPrint(dictCouponDetail)
        let context = getContext()
        let strServiceName = "\(dictCouponDetail["ServiceSysName"] ?? "")"
        let strDiscountAmount = "\(dictCouponDetail["DiscountAmount"] ?? "")"
        var strDiscountPer = ""
        var strChkDiscountPer = ""

        if strType == "Coupon" {
            
            let appliedDiscountInfo = LeadCommercialDiscountExtDc(context: context)
            
            appliedDiscountInfo.discountSysName = "\(dictCouponDetail["SysName"] ?? "")"
            appliedDiscountInfo.discountType = "\(dictCouponDetail["Type"] ?? "")"
            appliedDiscountInfo.discountDescription = "\(dictCouponDetail["Description"] ?? "")"
            
            if "\(dictCouponDetail["IsDiscountPercent"] ?? "0")" == "1" {
                strChkDiscountPer = "true"
                strDiscountPer = "\(dictCouponDetail["DiscountPercent"] ?? "0")"
                self.appliedDiscountInitial = (self.totalPrice * (Double(strDiscountPer) ?? 0.0))/100
            }else {
                strChkDiscountPer = "false"
                strDiscountPer = "0"
            }
            appliedDiscountInfo.isDiscountPercent = strChkDiscountPer
            appliedDiscountInfo.discountPercent = strDiscountPer
            appliedDiscountInfo.discountCode = "\(dictCouponDetail["DiscountCode"] ?? "")"
            appliedDiscountInfo.leadId = self.strLeadId as String

            let strUserName = "\(self.dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            let strCompanyKey = "\(self.dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            appliedDiscountInfo.userName = strUserName
            appliedDiscountInfo.companyKey = strCompanyKey
            
            appliedDiscountInfo.appliedMaintDiscount = String(format: "%.2f", self.appliedDiscountMaint)
            appliedDiscountInfo.appliedInitialDiscount = String(format: "%.2f", self.appliedDiscountInitial);
            appliedDiscountInfo.accountNo = self.strAccountNoGlobal

            if "\(dictCouponDetail["ApplicableForInitial"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForInitial"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForInitial = "true"
            }else {
                appliedDiscountInfo.applicableForInitial = "false"
            }
            
            if "\(dictCouponDetail["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForMaintenance"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForMaintenance = "true"
            }else {
                appliedDiscountInfo.applicableForMaintenance = "false"
            }
            
        }else {
            //Credit
            let appliedDiscountInfo = LeadCommercialDiscountExtDc(context: context)
            
//            appliedDiscountInfo.leadAppliedDiscountId = "\(dictCouponDetail["DiscountSetupId"] ?? "")"
//            appliedDiscountInfo.serviceSysName = strServiceName
            appliedDiscountInfo.discountSysName = "\(dictCouponDetail["SysName"] ?? "")"
            appliedDiscountInfo.discountType = "\(dictCouponDetail["Type"] ?? "")"
//            appliedDiscountInfo.discountAmount = strDiscountAmount
            appliedDiscountInfo.discountDescription = "\(dictCouponDetail["Description"] ?? "")"
            //appliedDiscountInfo.applicableForInitial = "\(dictCouponDetail["ApplicableForInitial"] ?? "")"
            //appliedDiscountInfo.applicableForMaintenance = "\(dictCouponDetail["ApplicableForMaintenance"] ?? "")"
            
            if "\(dictCouponDetail["IsDiscountPercent"] ?? "0")" == "1" {
                strChkDiscountPer = "true"
                strDiscountPer = "\(dictCouponDetail["DiscountPercent"] ?? "0")"
            }else {
                strChkDiscountPer = "false"
                strDiscountPer = "0"
            }
            appliedDiscountInfo.isDiscountPercent = strChkDiscountPer
            appliedDiscountInfo.discountPercent = strDiscountPer
            appliedDiscountInfo.discountCode = "\(dictCouponDetail["DiscountCode"] ?? "")"
            appliedDiscountInfo.leadId = self.strLeadId as String
            
            let strUserName = "\(self.dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
            let strCompanyKey = "\(self.dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
            appliedDiscountInfo.userName = strUserName
            appliedDiscountInfo.companyKey = strCompanyKey
            appliedDiscountInfo.appliedMaintDiscount = String(format: "%.2f", self.appliedDiscountMaint)
            appliedDiscountInfo.appliedInitialDiscount = String(format: "%.2f", self.appliedDiscountInitial);
            appliedDiscountInfo.accountNo = self.strAccountNoGlobal
            if "\(dictCouponDetail["ApplicableForInitial"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForInitial"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForInitial = "true"
            }else {
                appliedDiscountInfo.applicableForInitial = "false"
            }
            
            if "\(dictCouponDetail["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dictCouponDetail["ApplicableForMaintenance"] ?? "false")" == "true" {
                appliedDiscountInfo.applicableForMaintenance = "true"
            }else {
                appliedDiscountInfo.applicableForMaintenance = "false"
            }
            
            appliedDiscountInfo.name = "\(dictCouponDetail["Name"] ?? "")"
            debugPrint(appliedDiscountInfo)
        }
        
        do { try context.save()
            debugPrint("Record Saved Successfully.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }

    func getTaxCodeDetailWithApi(strTaxCode: String) {
        //Getting Tax from API Call
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strNewTaxUrl = "/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey="
        let strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"

        
        let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&taxSysName=\(strTaxCode)"
        debugPrint(strUrl)
        
        loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(self.loader, animated: false, completion: nil)
        self.presenter.requestToGetTaxByTaxCode(apiUrl: strUrl)
    }
    
    
    // Sourabh From Builder
    
    func getTemplateKey(strURL: String)  {
        self.fetchServiceProposalFollowUp()
        let start1 = CFAbsoluteTimeGetCurrent()
        print("Sales Auto Get Leads API called --- %@",strURL)
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "SalesGetLead_AppointmentVC") { (response, status) in
            self.loader.dismiss(animated: false) {
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Sales Auto Appointments API Data From SQL Server.")
                    
                    print("Sales Auto Get Appointments API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if response.value(forKey: "data") is NSDictionary {
                            
                            let dictData = Global().convertNullArray(response as? [AnyHashable : Any])! as NSDictionary
                            
                            let dictSalesLeadResponseLocal = dictData.value(forKey: "data") as! NSDictionary
                            
                            print(dictSalesLeadResponseLocal)
                            
                            if "\(dictSalesLeadResponseLocal.value(forKey: "TemplateKey") ?? "")".count > 0
                            {
                                self.arrImageDetails.removeAll()
                                self.arrGraphDetails.removeAll()
                                
                                let post_paramsValue = dictSalesLeadResponseLocal as! Dictionary<String,Any>
                                
                                self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                                self.present(self.loader, animated: false, completion: nil)

                                self.fetchImageFromDB(strHeaderTitle: "Before") { success in
                                    if success {
                                        self.uploadCRMImages() { success in
                                            if success {
                                                self.fetchImageFromDB(strHeaderTitle: "Graph") { success in
                                                    self.loader.dismiss(animated: false) {
                                                        if success {
                                                            self.goToFormBuilder(dict: post_paramsValue)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                self.goToReview()
                            }
                            
                        }
                        else
                        {
                            self.goToReview()
                        }
                        
                    }
                    else
                    {
                        self.goToReview()
                    }
                }
                else
                {
                    self.goToReview()
                }
            }
            
        }
        
    }
    
    func uploadCRMImages(completion: @escaping(_ success: Bool) -> Void) {
        self.countCRMImage = 0
        
        let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", "\(self.matchesGeneralInfo.value(forKey: "leadId") ?? "")" , self.strUserName, "false", "false"))
        
        let aryCRMImagesList = NSMutableArray()
        
        for item in arrImageCRM
        {
            let match = item as! NSManagedObject
            aryCRMImagesList.add("\(match.value(forKey: "path") ?? "")")

        }
        
        if aryCRMImagesList.count != 0 {
            
            let arrImageData = NSMutableArray()
            let arrImageName = NSMutableArray()

            for item in aryCRMImagesList {
                let strImage = "\(item)"
                arrImageData.add(getImage(imagePath: strImage))
                arrImageName.add(strImage)
            }
           
            for item in aryCRMImagesList
            {
                //self.dispatchGroupSales.enter()
                
                let strImage = "\(item)"
                ServiceGalleryService().uploadImageSalesOld(strImageName: strImage, strUrll: "\(URL.Gallery.uploadCRMImagesSingle)") { (response, status) in
                    
                    //self.dispatchGroupSales.leave()
                    self.countCRMImage = self.countCRMImage + 1
                    print(response ?? "")
                    
                    if aryCRMImagesList.count == self.countCRMImage
                    {
                        
                        let arrOfKeys = NSMutableArray()
                        let arrOfValues = NSMutableArray()
                        var count = 0
                        for item in aryCRMImagesList
                        {
                            let imgPath = item as! String
        
                            arrOfKeys.add("path")
                            arrOfKeys.add("isImageSyncforMobile")

                            arrOfValues.add(imgPath)
                            arrOfValues.add("true")
                          
                            let isSuccess =  getDataFromDbToUpdate(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && path == %@", "\(self.matchesGeneralInfo.value(forKey: "leadId") ?? "")",imgPath), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess
                            {
                                // Image updated in DB.
                            }
                            
                            if count == aryCRMImagesList.count - 1 {
                                // loop completed go to fetch all images and redirect to form builder page
                                let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : "Crm")
                                completion(isTrue)
                            }
                            
                            count = count + 1
                        }
                    }else {
                        completion(true)
                    }
                }
            }
        }else {
            let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : "Crm")
            completion(isTrue)
        }

    }
    
    func getImage(imagePath : String)-> UIImage{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)!
        }else{
            return UIImage()
        }
    }
    
    
    func goToFormBuilder(dict: [String: Any]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = UIStoryboard.init(name: "FormBuilder", bundle: Bundle.main).instantiateViewController(withIdentifier: "FormBuilderViewController") as? FormBuilderViewController
            vc?.dictTemplateObj = dict
            vc!.matchesGeneralInfo = self.objLeadDetails!
            vc!.objPaymentInfo = self.objPaymentInfo
            vc?.arrImageDetails = self.arrImageDetails
            vc?.arrGraphDetails = self.arrGraphDetails
            vc!.dictPricingInfo = self.dictPricingInfo
            if self.arrStandardServiceSold.count > 0 {
                vc!.arrSoldService = self.arrStandardServiceSold
            }else {
                vc!.arrSoldService = self.arrCustomServiceSold
            }
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    func getUIImageFromString(arrOfImagesString: [String]) -> [UIImage]{
        var arrOfUIImages = [UIImage]()
        
        for i in 0..<arrOfImagesString.count{
            
            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: arrOfImagesString[i])
            
            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: arrOfImagesString[i])
            
            if isImageExists == true{
                arrOfUIImages.append(image!)
            }
            
        }
        
        return arrOfUIImages
    }
    
    
    func fetchImageFromDB(strHeaderTitle : String, completion: @escaping(_ success: Bool) -> Void) {
        // Do something
        let arrayOfImageData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isImageSyncforMobile == %@ && isInternalUsage == %@", self.strLeadId, strHeaderTitle, "false", "false"))
        
        
        
        print(arrayOfImageData)
        if arrayOfImageData.count > 0 {
            
            var arrImgName: [String] = []
            var arrImgCaption: [String] = []
            var arrImgDescription: [String] = []
            
            for imgName in arrayOfImageData {
                let objTemp: NSManagedObject = imgName as! NSManagedObject
                print(objTemp)
                
                let strImagePath = objTemp.value(forKey: "leadImagePath")
                arrImgName.append(strImagePath as! String)
                
                let strCap = objTemp.value(forKey: "leadImageCaption")
                let strDec = objTemp.value(forKey: "descriptionImageDetail")
                arrImgCaption.append(strCap as! String)
                arrImgDescription.append(strDec as! String)
            }
            print(arrImgName)
            
            let arrOfImages = self.getUIImageFromString(arrOfImagesString : arrImgName)
            
            ServiceGalleryService().uploadImageForGeneralInfo(_strServiceUrlMainServiceAutomation: self.strServiceUrlMain, arrOfImages, arrImgName) { (object, error) in
                if let object = object {
                    print(object)
                    if object.count > 0 {
                        for i in 0..<object.count {
                            let imgPath: String = (object[i].relativePath ?? "") + (object[i].name ?? "")
                            
                            var strUrl = String()
                            strUrl = self.strServiceUrlMain
                                      
                            if imgPath.contains("Documents"){
                                strUrl = strUrl + imgPath
                            }else{
                                strUrl = strUrl + "//Documents/" + imgPath
                            }
                            strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                            strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                            
                            // save back in DB after Syncing.
                            
                            let arrOfKeys = NSMutableArray()
                            let arrOfValues = NSMutableArray()
                            
                            arrOfKeys.add("leadImagePath")
                            arrOfKeys.add("isImageSyncforMobile")

                            arrOfValues.add(imgPath)
                            arrOfValues.add("true")
                          
                            let isSuccess =  getDataFromDbToUpdate(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImagePath == %@", self.strLeadId,"\(object[i].name ?? "")"), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
                            
                            if isSuccess
                            {
                                // Image updated in DB.
                            }
                            
                            if i == object.count - 1 {
                                // loop completed go to fetch all images and redirect to form builder page
                                let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : strHeaderTitle)
                                completion(isTrue)
                            }
                        }
                    }else {
                        completion(true)
                    }

                } else {
                    completion(true)
                }
            }
        }else{
            let isTrue = self.fetchAllImagesFromDBAfterSync(strHeaderTitle : strHeaderTitle)

            completion(isTrue)
        }
    }
    
    func fetchAllImagesFromDBAfterSync(strHeaderTitle : String)-> Bool {
        
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strServiceUrlMainL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
        let strServiceCRMUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl") ?? "")"

        var arrayOfImageData = NSArray()
        if strHeaderTitle == "Crm" {
            arrayOfImageData = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isInternalUsage == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName, "false"))
        }else  {
            arrayOfImageData = getDataFromCoreDataBaseArray(strEntity: "ImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && isInternalUsage == %@", self.strLeadId, strHeaderTitle, "false"))
        }
        
        
        //let arrImageCRM = getDataFromLocal(strEntity: "CRMImagesDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName))

              
        if arrayOfImageData.count > 0 {
            
            for i in 0..<arrayOfImageData.count {
                
                let objTemp: NSManagedObject = arrayOfImageData[i] as! NSManagedObject
                print(objTemp)
                if strHeaderTitle == "Crm" {
                    //CRM Image
                    let strImagePath = objTemp.value(forKey: "path")
                    
                    let strCap = objTemp.value(forKey: "cRMImageCaption")
                    let strDec = objTemp.value(forKey: "cRMImageDescription")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    
                    var strUrl = String()
                    strUrl = strServiceCRMUrlMain
//                    if imgPath.contains("Documents"){
//                        strUrl = strUrl + imgPath
//                    }else{
                        strUrl = strUrl + "//Documents/" + imgPath
//                    }
                    
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var order = 0
                    if self.arrImageDetails.count > 0 {
                        order = self.arrImageDetails.count
                    }else {
                        order = i
                    }
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = order + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    self.arrImageDetails.append(dict)
                    
                }else {
                    let strImagePath = objTemp.value(forKey: "leadImagePath")
                    
                    let strCap = objTemp.value(forKey: "leadImageCaption")
                    let strDec = objTemp.value(forKey: "descriptionImageDetail")
                    
                    let imgPath: String = strImagePath as! String//(object[i].relativePath ?? "") + (object[i].name ?? "")
                    
                    var strUrl = String()
                    strUrl = strServiceUrlMainL
                              
                    if imgPath.contains("Documents"){
                        strUrl = strUrl + imgPath
                    }else{
                        strUrl = strUrl + "//Documents/" + imgPath
                    }
                    
                    strUrl = strUrl.replacingOccurrences(of: "\\", with: "/")
                    strUrl = strUrl.replacingOccurrences(of: "///", with: "//")
                    
                    var dict: [String: Any] = [:]
                    dict.removeAll()
                    dict["image"]       = strUrl
                    dict["order"]       = i + 1
                    dict["caption"]     = strCap as! String
                    dict["description"] = strDec as! String
                    
                    if "\(objTemp.value(forKey: "leadImageType") ?? "")" == "Before" {
                        self.arrImageDetails.append(dict)
                    }else {
                        self.arrGraphDetails.append(dict)
                    }
                }
                
            }
        }
        return true
//        self.goToFormBuilder(dict: dict, strPdfPath: "")
    }
    
    //End
    
    //MARK:- Action
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancleBtn(_ sender: UIButton) {
        debugPrint("cancle")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        if isEditInSalesAuto
        {
            Global().updateSalesModifydate(strLeadId as String)

        }

        let serviceCount = (self.arrStandardServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount > 0 {
            if (nsud.bool(forKey: "TaxCodeReq")) && self.strTaxCode.isEmpty && !getOpportunityStatus()
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Tax code is required, Please select tax code.", viewcontrol: self)
                
            }else {
                if serviceCount > 1  {
                    self.goToReview()
                }else {
                    loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(self.loader, animated: false, completion: nil)
                    
                    //Getting TemplateKey For Form Builder
                    let dictLoginData = nsud.value(forKey: "LoginDetails") as? NSDictionary ?? [:]
                    
                    let strNewTaxUrl = "/api/MobileToSaleAuto/GetSalesReportTemplateByServiceForMobile?companyKey="
                    self.strServiceUrlMain = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl") ?? "")"
                    
                    let strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
                    
                    var categorySysName = ""
                    var serviceSysName = ""
                    var nonStdDepartmentSysName = ""
                    
                    if self.arrStandardServiceSold.count > 0 {
                        let objeStandardService: NSManagedObject = self.arrStandardServiceSold[0]
                        print(objeStandardService)
                        serviceSysName = objeStandardService.value(forKey: "serviceSysName") as? String ?? ""
                        let catObj = getCategoryObjectFromServiceSysName(strServiceSysName: serviceSysName)//objeStandardService.value(forKey: "categorySysName") as! String
                        if catObj.value(forKey: "SysName") != nil {
                            categorySysName = catObj.value(forKey: "SysName") as! String
                        }else {
                            categorySysName = ""
                        }
                        print("Standard")
                    }else {
                        let objeNonStandardService: NSManagedObject = self.arrCustomServiceSold[0]
                        print(objeNonStandardService)
                        nonStdDepartmentSysName = objeNonStandardService.value(forKey: "departmentSysname") as? String ?? ""
                        
                        print("Non Standard")
                        
                    }
                    
                   let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&serviceSysName=\(serviceSysName)&categorySysName=\(categorySysName)&nonStdDepartmentSysName=\(nonStdDepartmentSysName)&flowtype=Commercial"
                    
                   // let strUrl = "\(strServiceUrlMain)\(strNewTaxUrl)\(strCompanyKey)&serviceSysName=\(serviceSysName)&categorySysName=\(categorySysName)&nonStdDepartmentSysName=\(nonStdDepartmentSysName)"
                    
                    if getOpportunityStatus()
                    {
                        self.loader.dismiss(animated: false) {}
                        
                        if "\(objLeadDetails?.value(forKey: "pdfPath") ?? "")".count > 0 && "\(objLeadDetails?.value(forKey: "templateKey") ?? "")".count > 0
                        {
                            self.goToFormBuilder(dict: [:])
                        }
                        else
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                                self.goToReview()
                                
                            })
                        }
                        
                    }
                    else
                    {
                        if isInternetAvailable() {
                            print("Internet connection available")
                            //self.presenter.requestToGetTemplateKey(apiUrl: strUrl)
                           
                            
                            //loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            //self.present(self.loader, animated: false, completion: nil)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                                
                                self.getTemplateKey(strURL: strUrl)
                                
                            }
                            
                        }
                        else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No internet connection available", viewcontrol: self)
                        }
                    }
                }
            }
            
        }else {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "To proceed, please add atleast one service to the review.", viewcontrol: self)
        }

    }
    
    //MARK:- ------Footer--IBAction-----------
    @IBAction func action_Image(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Make Your Selection", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.AddImages, style: .default, handler: { action in
            
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.AddDocuments, style: .default, handler: { [self] action in
            
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
      //      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.down]
        }
        self.present(alert, animated: true)
    }
    
    
    @IBAction func btnAddImageAction(_ sender: Any)
    {
        
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Add Images", style: .default , handler:{ (UIAlertAction)in
            self.goToGlobalmage(strType: "Before")
        }))
        
        alert.addAction(UIAlertAction(title: "Add Documents", style: .default , handler:{ (UIAlertAction)in
            self.goToUploadDocument()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceView = self.btnAddImage
        self.present(alert, animated: true, completion: {
        })
        
    }
     
    @IBAction func btnAddGraphAction(_ sender: Any)
    {
       goToGlobalmage(strType: "Graph")
    }
    
    
    @IBAction func action_ChemicalSensitive(_ sender: Any) {
        
        self.view.endEditing(true)
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? SalesNewStoryBoardName.SalesFlowNewGeneralInfo : SalesNewStoryBoardName.SalesFlowNewGeneralInfo, bundle: Bundle.main).instantiateViewController(withIdentifier: "SalesNew_EmailHistoryVC") as! SalesNew_EmailHistoryVC
        vc.strTitle = SalesNewListName.ChemicalSensitivity
        vc.dataGeneralInfo = dataGeneralInfo
        self.navigationController?.pushViewController(vc, animated: false)
//        let storyboardIpad = UIStoryboard.init(name: "SalesMain", bundle: nil)
//        let objChemicalSen = storyboardIpad.instantiateViewController(withIdentifier: "ChemicalSensitivityList") as? ChemicalSensitivityList
//        objChemicalSen?.strFrom = "sales"
//        objChemicalSen?.strId = strLeadId as String
//        self.navigationController?.present(objChemicalSen!, animated: false, completion: nil)
        
    }
    
    @IBAction func btnCalendar(_ sender: Any) {
        self.goToAppointment()
        
    }
    
    @IBAction func btnHistory(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: SalesNewListName.EmailHistory, style: .default, handler: { [self] action in
            self.view.endEditing(true)
            
            self.goToEmailHistory()
            
        }))
        
        alert.addAction(UIAlertAction(title: SalesNewListName.SetupHistory, style: .default, handler: { action in
        
            self.goToSetupHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.NotesHistory, style: .default, handler: { action in
        
            self.goToServiceNotesHistory()
        }))
        alert.addAction(UIAlertAction(title: SalesNewListName.ServiceHistory, style: .default, handler: { action in
        
            self.goToServiceHistory()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
           // popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [.up]
        }
        self.present(alert, animated: true)
        
    }
    
    
    @IBAction func btnMarkAsLost(_ sender: UIButton) {
        
        let alert = UIAlertController(title: Alert, message: "Are you sure to mark Opportunity as lost?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "YES", style: .default , handler:{ [self] (UIAlertAction)in
                    self.updateLeadIdDetail()
                    
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .default , handler:{ (UIAlertAction)in
                }))
                alert.popoverPresentationController?.sourceView = self.view
                self.present(alert, animated: true, completion: {
                })
        
    }
    
    @objc func btnEditIntroductionLetterDescriptionAction(sender: UIButton!){

        let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
        vc.strfrom = "EditIntroductionLetterDecriptionConfig"
        vc.strLeadId = strLeadId as String
        vc.strHtml = TemplateContent ?? ""
        vc.delegateCommerical = self
        vc.modalPresentationStyle = .fullScreen
        self.isEditInSalesAuto = true
        self.present(vc, animated: false, completion: nil)
    }
    func goToReview()
    {
        debugPrint("123456")
        debugPrint(self.arrCustomServiceSold)
        
        self.updateLeadDetailWithTipOtherAndLeadInspectionFee()
        saveLetterTemplateCoverLetterIntroductionLetter(dictCoverLetter: dictCoverLetter, dictIntroLetter: dictIntroduction, dictTermsOfService: dictTermsOfService)
        saveMarketingContent(arraySeletecMarketingContent: arrayMarketingContentMultipleSelected)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommericialReview", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommericialReviewViewController) as! NewCommericialReviewViewController
        vc.modalPresentationStyle = .fullScreen
        
        vc.matchesGeneralInfo = self.objLeadDetails!
        vc.objPaymentInfo = self.objPaymentInfo
        vc.arrayOfMonth = self.arrayOfMonth
        
        vc.arrStandardServiceReview = self.arrStandardServiceSold
        vc.arrCustomServiceReview = self.arrCustomServiceSold
        
        vc.dictPricingInfo = self.dictPricingInfo
        vc.arrAppliedCoupon = self.arrAppliedCoupon
        vc.arrAppliedCredit = self.arrAppliedCredit
        
        
        vc.strLeadId = strLeadId
        vc.strLeadStatusGlobal =  strGlobalStatusSysName
        vc.strSelectedPropsedDate = strSelectedPropsedDate
        vc.strSelectedPropsedMonth = strSelectedPropsedMonth
        vc.strTitle = strTitle
        vc.strOppNumber = strOppNumber
        vc.strUserName = Global().getUserName()
        vc.arrOfTagests = arrOfTagests
        vc.arrOfScopes = arrOfScopes
        vc.arrayOfMonth = arrayOfMonth
        vc.arrSelectedAgreementChckList = arrSelectedAgreementChckList
        
        fetchServiceProposalFollowUp()
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    //MARK:  - Footer Funcs
    
    
    func updateLeadIdDetail()

    {
        Global().updateSalesModifydate(strLeadId as String)
        
        var strInitalServiceDate = strSelectedPropsedDate
        var strRecurringServiceMonth = strSelectedPropsedMonth
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        arrOfKeys = ["stageSysName",
                     "statusSysName",
                     "initialServiceDate",
                     "recurringServiceMonth"
        ]
        
        arrOfValues = ["Lost",
                       "Complete",
                       strInitalServiceDate,
                       strRecurringServiceMonth
        ]
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate:  NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        if isSuccess {
            
            debugPrint("suc")
            
            goToAppointment()
            
        }
        
    }
    
    //MARK:-------------------------------------- Nilind --------------------------------------
    
    //MARK:------------------ Service & Proposal Function -------------

    func basicFunction()  {
        
        strEmpName = "\(dictLoginData.value(forKey: "EmployeeName") ?? "")"
        strUserName = "\(dictLoginData.value(forKeyPath: "Company.Username") ?? "")"
        strCompanyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey") ?? "")"
        let strPreferdMonth: String = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPreferredMonth") ?? "")"
        if strPreferdMonth == "1" || strPreferdMonth == "true" {
            isPreferredMonths = true
        }else{
            isPreferredMonths = false
        }
        
        let strTermsOfService: String = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsTermsOfService") ?? "")"

        if strTermsOfService == "1" || strTermsOfService == "true" {
            isTermsOfService = true
        }else{
            isTermsOfService = false
        }
        
        matchesGeneralInfo = getLeadDetail(strLeadId: strLeadId as String, strUserName: strUserName)
            
        strGlobalStageSysName = "\(matchesGeneralInfo.value(forKey: "stageSysName") ?? "")"
        strGlobalStatusSysName = "\(matchesGeneralInfo.value(forKey: "statusSysName") ?? "")"
        strBranchSysName = ("\(matchesGeneralInfo.value(forKey: "branchSysName") ?? "")" as NSString) as String
        
    }
    
    func getServicePropsalBasicDetails() {
       
        if nsud.value(forKey: "companyDetail") is NSDictionary
        {
            let dictCompanyDetail = nsud.value(forKey: "companyDetail") as! NSDictionary
            
            isProposalFoolowUp = (dictCompanyDetail.value(forKey: "IsProposalFollowUp") ?? false) as! Bool
            isServiceFollowUp = (dictCompanyDetail.value(forKey: "IsServiceFollowUp") ?? false) as! Bool
            serviceFollowDays = "\(dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ServiceFollowUpDays") ?? 0) as! Int : 0
            proposalFollowDays = "\(dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? "")".count > 0 ? (dictCompanyDetail.value(forKey: "ProposalFollowUpDays") ?? 0) as! Int : 0
        }
        
        arySalesServiceFollowUpList = getSoldServiceDepartments()
        
        let object = getUnSoldServiceDepartmentsAndServices()
        arySalesProposalFollowUpList = object.arrUnSoldDept
        arySalesProposalServices = object.arrProposalService
    }
    
    func getSoldServiceDepartments() -> NSArray {
        
        let arrSoldDept = NSMutableArray()
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
        }

        let arrUniqueDept = arrSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        debugPrint("Unique array \(arrUniqueDept)")
        
        
        return arrUniqueDept
    }
    
    func getUnSoldServiceDepartmentsAndServices() -> (arrUnSoldDept : NSArray, arrProposalService: NSArray) {
        
        let arrUnSoldDept = NSMutableArray()
        let arrUnSoldServiceName = NSMutableArray()

        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
        
        
        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            
            arrUnSoldServiceName.add(getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName).value(forKey: "Name") ?? "")
        }
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            arrUnSoldDept.add(getDepartmentObjectFromDeptSysName(strDeptSysName: strDeptSysName))
            arrUnSoldServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
        }


        let arrUniqueDept = arrUnSoldDept.reduce([], { $0.contains($1) ? $0 : $0 + [$1] })
        debugPrint("Unique array \(arrUniqueDept)")
        
        
        return (arrUniqueDept, arrUnSoldServiceName)
    }
    
    
    func getServiceDetailFromDept(strDepartmentSysName : String) -> String  {
        
        let arrServiceName = NSMutableArray()
        
        //Standard
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))

        for item in arraySoldServiceStandard
        {
            let matches = item as! NSManagedObject
            let strServiceSysName = "\(matches.value(forKey: "serviceSysName") ?? "")"
            let strDeptSysName = getDeptSysNameFromServiceSysName(strServiceSysName: strServiceSysName)
            
            if strDeptSysName == strDepartmentSysName
            {
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: strServiceSysName)
                arrServiceName.add("\(dictService.value(forKey: "Name") ?? "")")
            }
        }
        
        //Custom
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"true"))
        for item in arraySoldServiceCustom
        {
            let matches = item as! NSManagedObject
            let strDeptSysName = "\(matches.value(forKey: "departmentSysname") ?? "")"
            
            if strDeptSysName == strDepartmentSysName
            {
                arrServiceName.add("\(matches.value(forKey: "serviceName") ?? "")")
            }
        }
        
    
        if arrServiceName.count > 0
        {
            return arrServiceName.componentsJoined(by: ", ")
        }
        else
        {
            return ""
        }
    }
    func saveServiceFollowUp()  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
        
        let strToday = changeStringDateToGivenFormat(strDate: Global().strCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = Calendar.current.date(byAdding: .day, value: serviceFollowDays, to: dateFormatter.date(from: strToday)!)!
        let strFollowUpDate = dateFormatter.string(from: modifiedDate)
        
        for item in arySalesServiceFollowUpList
        {
            let matches = item as! NSDictionary
            
            arrOfKeys = [
                "departmentId",
                "departmentName",
                "departmentSysName",
                "followupDate",
                "id",
                "leadId",
                "notes",
                "serviceId",
                "serviceName",
                "serviceSysName"
            ]
            
            arrOfValues = [
                "",
                "\(matches.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "SysName") ?? "")",
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                (Global().getReferenceNumberNew() ?? ""),
                strLeadId,
                "Follow up for: \(getServiceDetailFromDept(strDepartmentSysName: "\(matches.value(forKey: "SysName") ?? "")"))",
                "",
                "",
                ""
            ]
            
            saveDataInDB(strEntity: "ServiceFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        

    }
    func fetchServiceProposalFollowUp() {
        
        getServicePropsalBasicDetails()
        
        if getOpportunityStatus()
        {
            
        }
        else
        {
            if isServiceFollowUp
            {
                deleteServiceFollowup()
                saveServiceFollowUp()
            }
            if isProposalFoolowUp
            {
                deleteProposalFollowup()
                saveProposalFollowUp()
            }
            
        }
    }
    
    func deleteServiceFollowup()  {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ServiceFollowUpDcs", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))

        for item in arrayData
        {
            let objTempSales = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)

        }
    }
    func deleteProposalFollowup()  {
        
        let arrayData = getDataFromCoreDataBaseArray(strEntity: "ProposalFollowUpDcs", predicate: NSPredicate(format: "proposalLeadId == %@", self.strLeadId))
        
        for item in arrayData
        {
            let objTempSales = item as! NSManagedObject
            deleteDataFromDB(obj: objTempSales)

        }
    }
    func saveProposalFollowUp()  {
        
        var arrOfKeys = NSMutableArray()
        var arrOfValues = NSMutableArray()
    
        let strToday = changeStringDateToGivenFormat(strDate: Global().strCurrentDate(), strRequiredFormat: "MM/dd/yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let modifiedDate = Calendar.current.date(byAdding: .day, value: proposalFollowDays, to: dateFormatter.date(from: strToday)!)!
        let strFollowUpDate = dateFormatter.string(from: modifiedDate)
        
        
        let arraySoldServiceStandard = getDataFromLocal(strEntity: "SoldServiceStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))
    
        
        let arraySoldServiceCustom = getDataFromLocal(strEntity: "SoldServiceNonStandardDetail", predicate: NSPredicate(format: "leadId == %@ && userName == %@ && isSold == %@", "\(matchesGeneralInfo.value(forKey: "leadId") ?? "")" , strUserName,"false"))

        
        
        for item in arraySoldServiceStandard //For Standard
        {
            let matches = item as! NSManagedObject
            
            let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(matches.value(forKey: "serviceSysName") ?? "")")
            
            
            arrOfKeys = [
                "proposalFollowupDate",
                "proposalId",
                "proposalLeadId",
                "proposalNotes",
                "proposalServiceId",
                "proposalServiceName",
                "proposalServiceSysName"
            ]
            
            
            arrOfValues = [
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                "\(Global().getReferenceNumberNew() ?? "")",
                 strLeadId,
                "Follow up for: \(dictService.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "serviceId") ?? "")",
                "\(dictService.value(forKey: "Name") ?? "")",
                "\(matches.value(forKey: "serviceSysName") ?? "")"
            ]
            
            saveDataInDB(strEntity: "ProposalFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
        for item in arraySoldServiceCustom //For Custom
        {
            let matches = item as! NSManagedObject
            
            arrOfKeys = [
                "proposalFollowupDate",
                "proposalId",
                "proposalLeadId",
                "proposalNotes",
                "proposalServiceId",
                "proposalServiceName",
                "proposalServiceSysName"
            ]
            
            arrOfValues = [
                strFollowUpDate, //\(Global().strCurrentDate() ?? "")
                "\(Global().getReferenceNumberNew() ?? "")",
                 strLeadId,
                "Follow up for: \(matches.value(forKey: "serviceName") ?? "")", //Follow up for: \(getServiceDetailFromDept(strDepartmentSysName: "\(matches.value(forKey: "departmentSysname") ?? "")"))
                "",
                "\(matches.value(forKey: "serviceName") ?? "")",
                "\(matches.value(forKey: "serviceName") ?? "")"
            ]
            
            saveDataInDB(strEntity: "ProposalFollowUpDcs", arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        }
        
    }
    
    func updateLeadDetailWithTipOtherAndLeadInspectionFee()
    
    {
        
        let strOtherDiscount = (dictPricingInfo["otherDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        
        let strTipDiscount = (dictPricingInfo["tipDiscount"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        
        let strLeadInspectionFee = (dictPricingInfo["leadInspectionFee"] as! String).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
        
        
        
        var arrOfKeys = NSMutableArray()
        
        var arrOfValues = NSMutableArray()
        
        arrOfKeys = ["otherDiscount",
                     
                     "tipDiscount",
                     
                     "leadInspectionFee"
                     
        ]
        
        
        
        arrOfValues = [strOtherDiscount,
                       
                       strTipDiscount,
                       
                       strLeadInspectionFee
                       
        ]
        
        
        
        let isSuccess = getDataFromDbToUpdate(strEntity: "LeadDetail", predicate:  NSPredicate(format: "leadId == %@", strLeadId), arrayOfKey: arrOfKeys, arrayOfValue: arrOfValues)
        
        
        
        if isSuccess {
            
            debugPrint("True")
            
        }
        
    }
    
}

extension NewCommercialConfigureDashBoardVC : UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate , ConfigureStandardCellDelegate , ConfigureCustomeServicesCellDelegate , ConfigureMonthOfServiceCellDelegate,callBackAddtionalNotes , ConfigureAddtionalNotes{
    
    // MARK: ---------------------------- Addtional Notes   //LeadCommercialDetailExtDc
    func returnTxtValue(data: String) {
        debugPrint("value")
        txtNoOfDays = data
        strValidFor = data
    }
    
    
    func callBackMethodAddtional(cell: NewCommericialAddtionalNotesCell) {
        self.isEditInSalesAuto = true
        if cell.btnAgreement.isSelected {
            cell.txtNoOfDays.isHidden = false
            cell.lblDays.isHidden = false
            strIsAgreementValidForNotes = "true"
            
        }else {
            cell.txtNoOfDays.isHidden = true
            cell.lblDays.isHidden = true
            strIsAgreementValidForNotes = "false"
            cell.txtNoOfDays.text = ""
            txtNoOfDays = ""
            strValidFor = ""
        }
    }
    
    func callBackMethodAdditonalNotes(data: String) {
        self.isEditInSalesAuto = true
        if !getOpportunityStatus()
        {
            debugPrint("addtional Nptes")
            debugPrint(data)
            self.strAddtionalNotes = data
            objPaymentInfo?.setValue(self.strAddtionalNotes, forKey: "specialInstructions")

            let context = getContext()
            //save the object
            do { try context.save()
                debugPrint("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
        
    }
    
    
    // MARK: ---------------------------- Internal Notes
    func callBakMethodInternalNotes(data: String) {
        self.isEditInSalesAuto = true
        self.strInternalNotes = data
        debugPrint("strLise ")
        debugPrint(self.strInternalNotes)
        
        objLeadDetails?.setValue(self.strInternalNotes, forKey: "notes")
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    func getOpportunityStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Won") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func getLostStatus() -> Bool
    {
        if (strGlobalStatusSysName.caseInsensitiveCompare("Complete") == .orderedSame && strGlobalStageSysName.caseInsensitiveCompare("Lost") == .orderedSame)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    // MARK: ---------------------------- Month Of Services
    func callBackMonthMethod(cell: NewCommericialMonthOfServicesCell, month: String, tag: Int) {
        self.isEditInSalesAuto = true
        debugPrint("Month")
        debugPrint(month)
        
        if self.arrayOfMonth.contains(month) {
            if let index = arrayOfMonth.firstIndex(of: month) {
                arrayOfMonth.remove(at: index)
            }
        }else {
            self.arrayOfMonth.append(month)
        }
        
        debugPrint(self.arrayOfMonth)
        strPreferedMonth = arrayOfMonth.joined(separator: ", ")
        debugPrint(strPreferedMonth)
        self.updateLeadDetails()
        configureTableView.reloadData()
        
    }
    
    
    func updateLeadDetails() {
        
        objLeadDetails?.setValue(strPreferedMonth, forKey: "strPreferredMonth")
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    // MARK: ---------------------------- Agreement Custome Services
    func callBackAddCustomAgreementMethod(cell: NewCommercialConfigureCustomCell) {
        let indexPath = configureTableView.indexPath(for: cell)
        self.isEditInSalesAuto = true
        
        var strTaxable = "false"
        if cell.btnIsTaxable.isSelected {
            strTaxable = "true"
        }
        
        
        if cell.btnAddToAgreement.isSelected {
            self.updateAddToAgreementCustome(isAddAgreement: "true", isTaxable: strTaxable,  indexPath: indexPath!)
            self.isSoldCustome = "isSoldCustome"
        }else {
            self.updateAddToAgreementCustome(isAddAgreement: "false", isTaxable: strTaxable,  indexPath: indexPath!)
            self.isSoldCustome = ""
        }
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func callBackIsTaxableMethod(cell: NewCommercialConfigureCustomCell)
    {
        let indexPath = configureTableView.indexPath(for: cell)
        var strIsAgreement = "false"
        if cell.btnAddToAgreement.isSelected {
            strIsAgreement = "true"
        }
        
        if cell.btnIsTaxable.isSelected {
            self.updateAddToAgreementCustome(isAddAgreement: strIsAgreement, isTaxable: "true", indexPath: indexPath!)
        }else {
            self.updateAddToAgreementCustome(isAddAgreement: strIsAgreement, isTaxable: "false", indexPath: indexPath!)
        }
    }
    
    func updateAddToAgreementCustome(isAddAgreement: String, isTaxable: String, indexPath: IndexPath) {
                
        var objServiceDetail = NSManagedObject()
        
        if indexPath.section == 7 {
            
            objServiceDetail = self.arrCustomService[indexPath.row]
            objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
            objServiceDetail.setValue(isTaxable, forKey: "isTaxable")
        }
        if isAddAgreement == "false" {
            debugPrint(arrAppliedCoupon)
            debugPrint(arrAppliedCredit)
            let arrCreditAndCoupon = arrAppliedCoupon + arrAppliedCredit
            debugPrint(arrCreditAndCoupon)
            
            let soldService = arrStandardServiceSold + arrCustomServiceSold
            if soldService.count <= 1 {
                for couponOrCredit in arrCreditAndCoupon {
                    var objCouponCredit = NSManagedObject()
                    objCouponCredit = couponOrCredit
                    let discountType = "\((objCouponCredit as AnyObject).value(forKey: "discountType") ?? "")"
                    if discountType == "Coupon" {
                        //Delete Coupon
                        debugPrint("Delete Coupon")
                        self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                    }else {
                        //Delete Credit
                        debugPrint("Delete Credit")
                        self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                    }
                }
            }
        }
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    
    
    // MARK: ---------------------------- Add To Agreement Standard Services
    func updateAddToAgreement(isAddAgreement: String,indexPath: IndexPath) {
        
        var objServiceDetail = NSManagedObject()
        
        if indexPath.section == 6 {
            
            objServiceDetail = self.arrStandardService[indexPath.row]
            objServiceDetail.setValue(isAddAgreement, forKey: "isSold")
            
        }
        
        
        if isAddAgreement == "false" {
            debugPrint(arrAppliedCoupon)
            debugPrint(arrAppliedCredit)
            let arrCreditAndCoupon = arrAppliedCoupon + arrAppliedCredit
            debugPrint(arrCreditAndCoupon)
           
            let soldService = arrStandardServiceSold + arrCustomServiceSold
            if soldService.count <= 1 {
                for couponOrCredit in arrCreditAndCoupon {
                    var objCouponCredit = NSManagedObject()
                    objCouponCredit = couponOrCredit
                    let discountType = "\((objCouponCredit as AnyObject).value(forKey: "discountType") ?? "")"
                    if discountType == "Coupon" {
                        //Delete Coupon
                        debugPrint("Delete Coupon")
                        self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                    }else {
                        //Delete Credit
                        debugPrint("Delete Credit")
                        self.deleteCreditOrCoupon(objCouponDetails: objCouponCredit)
                    }
                }
            }
        }
        
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    
    func deleteCreditOrCoupon(objCouponDetails: NSManagedObject) {
        debugPrint(objCouponDetails)
        
        let context = getContext()
        context.delete(objCouponDetails)
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        self.initialSetup()
    }
    
    func callBackAddAgreementMethod(cell: NewCommercialConfigureStandardCell) {
        self.isEditInSalesAuto = true
        guard let indexPath = configureTableView.indexPath(for: cell) else { return }
        
        if cell.btnAddToAgreement.isSelected {
            self.updateAddToAgreement(isAddAgreement: "true",  indexPath: indexPath)
            self.isSoldStandard = "isSoldStandard"
        }else {
            self.updateAddToAgreement(isAddAgreement: "false",  indexPath: indexPath)
            self.isSoldStandard = ""
        }
        let context = getContext()
        //save the object
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
       
    }
    
    // MARK: ---------------------------- Number of Section
    func numberOfSections(in tableView: UITableView) -> Int {
        return 13
    }
    
    // MARK: ---------------------------- Number Of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        } else if section == 1{
            
            if TemplateContent == "" || TemplateContent == nil {
                return 0
            }else{
                return 1
            }
           
        }else if section == 2{
            return 1
        }else if section == 3{
            if isTermsOfService == false{
                return 0
            }else{
                return 1
            }
            
        }else if section == 4{
            
            if arrOfTagests.count == 0{
                return 0
            }else{
                return arrOfTagests.count
            }
            
        }else if section == 5{
            
            if arrOfScopes.count == 0{
                return 0
            }else{
                return arrOfScopes.count
            }
            
        }else if section == 6{
            
            if arrStandardService.count == 0{
                return 0
            }else{
                return self.arrStandardService.count
            }
            
            
        }else if section == 7{
            
            if arrCustomService.count == 0{
                return 0
            }else{
                return self.arrCustomService.count
            }
        }else if section == 8{
            return 1
        }else if section == 9{
            if isPreferredMonths == false {
                return 0
            }else{
                return 1
            }
        }else if section == 10 {
            return 1
        }else if section == 11 {
            return 1
        }
        else if section == 12{
            return arrAgreementCheckList.count
        }
        return 0
    }
    
    // MARK: ---------------------------- Cell for row at indexpath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialProposalCoverCell, for: indexPath) as? NewCommercialProposalCoverCell
            
 
                //Cover
            let tap = UITapGestureRecognizer(target: self, action: #selector(CoverTap))
            tap.delegate = self
            cell?.stackCoverLetter.isUserInteractionEnabled = true
            cell?.stackCoverLetter.addGestureRecognizer(tap)
            cell?.txtCoverLetter.text = self.strCoverLetter
            
            //Introduction
            let tapIntroduction = UITapGestureRecognizer(target: self, action: #selector(IntroductionTap))
            tapIntroduction.delegate = self
            cell?.stackIntroduction.isUserInteractionEnabled = true
            cell?.stackIntroduction.addGestureRecognizer(tapIntroduction)
            cell?.txtIntroductionLetter.text = self.strIntroductionLetter
           
            
            return cell ??  UITableViewCell()
            
        }else if indexPath.section == 1{
            //  Intoduction description
            
            if  TemplateContent == "" || TemplateContent == nil{
                
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialIntroductionLetterDiscriptionCell", for: indexPath) as? NewCommericialIntroductionLetterDiscriptionCell
                
                
               // cell?.lblIntroductionLetter.attributedText = TemplateContent?.htmlAttributedStringInGlobal()
                //cell?.tableView = configureTableView
                cell?.loadWebView(str: TemplateContent ?? "")
                
                
//                cell?.callback = { [self] (strSuccessUpdate: String) -> () in
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
//                        self.configureTableView.beginUpdates()
//                        self.configureTableView.endUpdates()
//
//                    }
//                }
                cell?.btnEditIntroductionLetter.tag = indexPath.row
                cell?.btnEditIntroductionLetter.addTarget(self, action: #selector(btnEditIntroductionLetterDescriptionAction), for: .touchUpInside)
                
                if getOpportunityStatus()
                {
                    cell?.btnEditIntroductionLetter.isEnabled = false
                }
                else
                {
                    cell?.btnEditIntroductionLetter.isEnabled = true
                }
                
                return  cell ?? UITableViewCell()
            }
            
          }else if indexPath.section == 2{
              //  markting content
              let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialMarketingContentCell", for: indexPath) as? NewCommericialMarketingContentCell
              
             
              let tapMarketing = UITapGestureRecognizer(target: self, action: #selector(marketingTap))
              tapMarketing.delegate = self
              cell?.stackMarktingContent.isUserInteractionEnabled = true
              cell?.stackMarktingContent.addGestureRecognizer(tapMarketing)
              cell?.opportunityStatus = getOpportunityStatus()
                  
             
              if arrayMarketingContentMultipleSelected.count == 0 {
                  cell?.heightCollectionVIew.constant = 0
              }else{
                  cell?.heightCollectionVIew.constant = 60
              }
              
              cell?.dictMarketContentCollectionView = arrayMarketingContentMultipleSelected
              cell?.delegateReturnData = self
              cell?.collMarketingContent.reloadData()
              cell?.PrefreshTable = self
              return  cell ?? UITableViewCell()
          }
        else if indexPath.section == 3{
            //  Terms Service
            
            if isTermsOfService == false {
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialTermsAndServiceCell, for: indexPath) as? NewCommercialTermsAndServiceCell
                let tap = UITapGestureRecognizer(target: self, action: #selector(termsAndservicesTap))
                tap.delegate = self
                cell?.stackTermsOfServices.isUserInteractionEnabled = true
                cell?.stackTermsOfServices.addGestureRecognizer(tap)
                
                cell?.txtTermsOfService.text = self.strTermsOfService
                cell?.lblTermsOfService.attributedText = strDescriptionTermsService.htmlAttributedStringInGlobal()

                cell?.btnEditTermsOfService.tag = indexPath.row
                cell?.btnEditTermsOfService.addTarget(self, action: #selector(btnEditTermsOfService), for: .touchUpInside)
                
                if getOpportunityStatus() == true
                {
                    cell?.btnEditTermsOfService.isHidden = true
                }
                else{
                    cell?.btnEditTermsOfService.isHidden = false
                }
                return cell ?? UITableViewCell()
            }
             
        }else if indexPath.section == 4{
            //  Addd Targert
            
            if arrOfTagests.count == 0{
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialConfigureAddTargetCell", for: indexPath) as? NewCommercialConfigureAddTargetCell
                let obj = arrOfTagests[indexPath.row]  as! NSManagedObject
                
                cell?.lblTarget.text = "\(obj.value(forKey: "name") ?? "")"
                cell?.lblDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(obj.value(forKey: "targetDescription") ?? "")")
                
                let arrayImageReturn = fetchImageFromDB(strLeadCommercialTargetId: obj.value(forKey: "leadCommercialTargetId") as? String ?? "", strMobileTargetId: obj.value(forKey: "mobileTargetId") as? String ?? "")
                
                cell?.arrayOfImages = arrayImageReturn
                cell?.tblTargetImage.reloadData()
                cell?.strLeadId = strLeadId
                if cell?.arrayOfImages.count == 0{
                    cell?.heightForTable.constant = 0
                }
                
//                // Do any additional setup after loading the view.
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
//
//                    configureTableView.beginUpdates()
//                    configureTableView.endUpdates()
//                }
                
                
                return cell ?? UITableViewCell()
                
            }
            
        }else if indexPath.section == 5{
            //  Add Scope
            
            if arrOfScopes.count == 0{
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialConfigureAddScopeCell", for: indexPath) as? NewCommercialConfigureAddScopeCell
                let obj = arrOfScopes[indexPath.row]  as! NSManagedObject
                cell?.lblScope.text = "\(obj.value(forKey: "title") ?? "")"
                let strDesc = "\(obj.value(forKey: "scopeDescription") ?? "")"
                
                if strDesc.count == 0
                {
                    cell?.lblScopeDescription.text = ""
                }
                else
                {
                    cell?.lblScopeDescription.attributedText = getAttributedHtmlStringUnicode(strText: strDesc)
                }
                
              
                return cell ?? UITableViewCell()
            }
            
        }else if indexPath.section == 6{
            //  standard Service
            if self.arrStandardService.count == 0 {
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialConfigureStandardCell", for: indexPath) as? NewCommercialConfigureStandardCell
                let objService = self.arrStandardService[indexPath.row]
                               
                let dictService = getServiceObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "serviceSysName") ?? "")")
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "billingFrequencySysName") ?? "")")

                cell?.lblServiceName.text = "\(dictService.value(forKey: "Name") ?? "")"
                cell?.lblServiceName.sizeToFit()
                cell?.lblIntialPrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
                cell?.lblMaintenancePrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
                cell?.lblFrequencyName.text = "\(objService.value(forKey: "serviceFrequency") ?? "")"
                //cell?.lblBillingFrequencyName.text = "Billed: $" + "\(objService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                let strBillingFrequncy = String(format: "%.2f", (Float("\(objService.value(forKey: "billingFrequencyPrice") ?? "")") ?? 0.0 ))
                cell?.lblBillingFrequencyName.text = "Billed: $" + strBillingFrequncy + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"
                
                let strRenewal = self.fetchRenewalServiceFromCoreData(soldServiceId: "\(objService.value(forKey: "serviceId") ?? "")")
                
                cell?.lblRenewalContent.isHidden = true
                cell?.lblRenewalSeprator.isHidden = true
                if strRenewal != "" {
                    cell?.lblRenewalContent.isHidden = false
                    cell?.lblRenewalSeprator.isHidden = false
                    var tempFontSize = CGFloat()
                    if DeviceType.IS_IPAD{
                        tempFontSize = 19
                    }
                    else{
                        tempFontSize = 14
                    }
                    let myString = NSAttributedString(string: "Renewal: ", attributes: [
                        NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: tempFontSize)
                    ])

                    let mutableAttString = NSMutableAttributedString()

                    mutableAttString.append(myString)
                    mutableAttString.append(NSAttributedString(string: strRenewal))

                    cell?.lblRenewalContent.attributedText = mutableAttString

                }
                
                if objService.value(forKey: "serviceDescription") as! String == "" {
                    cell?.viewForHeightDescription.constant = 0
                    cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(objService.value(forKey: "serviceDescription") ?? "")")
                    cell?.viewServiceDescriptionSeprator.isHidden = true
                   // cell?.topConstraintForBottomView.constant = -7
                }else{
                    cell?.viewForDescription.removeConstraint((cell?.viewForHeightDescription)!)
                    cell?.viewServiceDescriptionSeprator.isHidden = false
                    cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(objService.value(forKey: "serviceDescription") ?? "")")//htmlAttributedStringNewSales(strHtmlString: "\(objService.value(forKey: "serviceDescription") ?? "")")
                   // cell?.topConstraintForBottomView.constant = 0
                }
                
                if cell?.lblFrequencyName.text?.lowercased() == "OneTime".lowercased() || cell?.lblFrequencyName.text?.lowercased() == "One Time".lowercased()
                {
                    cell?.lblMaintenancePrice.isHidden = true
                    cell?.lblStrMaintenancePrice.isHidden = true
                    cell?.lblStrheightForMaintenancePrice.constant = 0
                    cell?.lblHeightForMaintenancePrice.constant = 0
                  //  cell?.viewForBottom.constant = 89
                }else {
                    cell?.lblMaintenancePrice.isHidden = false
                    cell?.lblStrMaintenancePrice.isHidden = false
                    cell?.lblStrheightForMaintenancePrice.constant = 24
                    cell?.lblHeightForMaintenancePrice.constant = 18
                   // cell?.viewForBottom.constant = 114
                }
                
                if objService.value(forKey: "isSold") as! String == "true" {
                    cell?.btnAddToAgreement.isSelected = true
                    isSoldStandard = "isSoldStandard"
                }else {
                    cell?.btnAddToAgreement.isSelected = false
                    isSoldStandard = ""
                }
                
                cell?.delegate = self
                
                if getOpportunityStatus() {
                    cell?.btnAddToAgreement.isUserInteractionEnabled = false
                }
                
                return cell ?? UITableViewCell()
                
            }
        }else if indexPath.section == 7{
            
            //  Custome Serice
            if self.arrCustomService.count == 0{
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommercialConfigureCustomCell", for: indexPath) as? NewCommercialConfigureCustomCell
                
                let objService = self.arrCustomService[indexPath.row]
                               
                let dictBillingFrequency = getFrequencyObjectFromIdAndSysName(strId: "", strSysName: "\(objService.value(forKey: "billingFrequencySysName") ?? "")")
                
                cell?.lblServiceName.text = "\(objService.value(forKey: "serviceName") ?? "")"
                cell?.lblServiceName.sizeToFit()
                cell?.lblIntialPrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "initialPrice") ?? "")" as NSString).doubleValue))
                cell?.lblMaintenancePrice.text = convertDoubleToCurrency(amount: Double(("\(objService.value(forKey: "maintenancePrice") ?? "")" as NSString).doubleValue))
                cell?.lblFrequency.text = "\(objService.value(forKey: "serviceFrequency") ?? "")"
                cell?.lblBillingFrequency.text = "Billed: $" + "\(objService.value(forKey: "billingFrequencyPrice") ?? "")" + "/ " + "\(dictBillingFrequency.value(forKey: "FrequencyName") ?? "")"//"\(objService.value(forKey: "serviceName") ?? "")"
                
                if objService.value(forKey: "serviceDescription") as! String == "" {
                    //cell?.heightForViewServiceDescription.constant = 0
                    cell?.heightForViewDescription.constant = 0
                }else{
                    cell?.viewDescription.removeConstraint((cell?.heightForViewDescription)!)
                    cell?.lblServiceDescription.attributedText = getAttributedHtmlStringUnicode(strText: "\(objService.value(forKey: "serviceDescription") ?? "")")//htmlAttributedStringNewSales(strHtmlString: "\(objService.value(forKey: "serviceDescription") ?? "")")

                }
                
                if objService.value(forKey: "isTaxable") as! String == "true" {
                    cell?.btnIsTaxable.isSelected = true
                }else {
                    cell?.btnIsTaxable.isSelected = false
                }
                
                if cell?.lblFrequency.text?.lowercased() == "OneTime".lowercased() || cell?.lblFrequency.text?.lowercased() == "One Time".lowercased()
                {
                    cell?.lblMaintenancePrice.isHidden = true
                    cell?.lblStrMaintenancePrice.isHidden = true
                    cell?.lblStrheightForMaintenancePrice.constant = 0
                    cell?.lblHeightForMaintenancePrice.constant = 0
                    cell?.viewForBottom.constant = 89
                }else {
                    cell?.lblMaintenancePrice.isHidden = false
                    cell?.lblStrMaintenancePrice.isHidden = false
                    cell?.lblStrheightForMaintenancePrice.constant = 24
                    cell?.lblHeightForMaintenancePrice.constant = 18
                    cell?.viewForBottom.constant = 114
                }
                
                if objService.value(forKey: "isSold") as! String == "true" {
                    cell?.btnAddToAgreement.isSelected = true
                    isSoldCustome = "isSoldCustome"
                }else {
                    cell?.btnAddToAgreement.isSelected = false
                    isSoldCustome = ""
                }
                
                cell?.delegate = self
                
                if getOpportunityStatus() {
                    cell?.btnAddToAgreement.isUserInteractionEnabled = false
                }
                
                return cell ?? UITableViewCell()
            }
            
        }
        else if indexPath.section == 8{
            //Pricing Info
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialConfigurePricingCell", for: indexPath) as? NewCommericialConfigurePricingCell
            
            cell?.delegate = self

            /*Coupon Details*/
            if self.arrAppliedCoupon.count > 0 {
                cell?.tableSetUp(arrAppliedCoupon : self.arrAppliedCoupon)
            }else {
                cell?.arrAppliedCoupon.removeAll()
                cell?.tblCouponHeight.constant = 20
                cell?.tblCoupon.reloadData()
            }
            
            /**********************/
            
            /*Credit Details*/
            if self.arrAppliedCredit.count > 0 {
//                cell?.txtCredit.text = dictSelectedCredit?["Name"] as? String ?? ""
                cell?.tableCreditSetUp(arrAppliedCredit : self.arrAppliedCredit)
            }else {
                cell?.arrAppliedCredit.removeAll()
                cell?.tblCreditHeight.constant = 20
                cell?.tblCredit.reloadData()
            }
            /**********************/
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
//                configureTableView.beginUpdates()
//                configureTableView.endUpdates()
//            }
            
            //****Sub Total****
            cell?.lblSubTotal.text = String(format: "$%.2f", self.subTotalInitialPrice)
            self.dictPricingInfo["subTotalInitial"] = cell?.lblSubTotal.text
            /**********************/

            //****Coupon Discount****
            cell?.lblCouponDiscount.text = String(format: "$%.2f", self.couponDiscountPrice)
            self.dictPricingInfo["couponDiscount"] = cell?.lblCouponDiscount.text
            /**********************/

            //****Credit****
            if self.creditAmount > self.subTotalInitialPrice {
                self.creditAmount = self.subTotalInitialPrice
            }

            cell?.lblCredit.text = String(format: "$%.2f", self.creditAmount)
            self.dictPricingInfo["credit"] = cell?.lblCredit.text
            /**********************/

            //****Other Discount****
            cell?.txtOtherDiscount.text = String(format: "$%.2f", (self.otherDiscount))
            self.dictPricingInfo["otherDiscount"] = cell?.txtOtherDiscount.text
            /**********************/

            //****Tip Discount****
            cell?.txtTipDiscount.text = String(format: "$%.2f", (self.tipDiscount))
            self.dictPricingInfo["tipDiscount"] = cell?.txtTipDiscount.text
            /**********************/
            
            //****Lead Inspection Fee****
            cell?.txtLeadInspectionFee.text = String(format: "$%.2f", (self.leadInspectionFee))
            self.dictPricingInfo["leadInspectionFee"] = cell?.txtLeadInspectionFee.text
            /**********************/
            
            //****Total Price****
            self.totalPrice = (self.subTotalInitialPrice
                                - (self.creditAmount + self.couponDiscountPrice)) + (leadInspectionFee - otherDiscount - tipDiscount)
            if self.totalPrice < 0 {
                self.totalPrice = 0.0
            }
            cell?.totalPrice = self.totalPrice
            let finalTotal = self.totalPrice// + (leadInspectionFee - otherDiscount - tipDiscount)
            
            cell?.lblTotalPrice.text = String(format: "$%.2f", finalTotal)
            self.dictPricingInfo["totalPrice"] = cell?.lblTotalPrice.text
            /**********************/

            //****Tax Amount****
            cell?.lblTaxAmount.text = String(format: "$%.2f", self.taxAmount)
            self.dictPricingInfo["taxAmount"] = cell?.lblTaxAmount.text
            /**********************/

            //****Billing Amount****
            self.billingAmount = finalTotal + self.taxAmount
            cell?.lblBillingAmount.text = String(format: "$%.2f", self.billingAmount)
            self.dictPricingInfo["billingAmount"] = cell?.lblBillingAmount.text
            /**********************/
            
            //*Maintenance Service Price*
            //****SubTotal Amount****
            cell?.lblSubTotalAmount.text = String(format: "$%.2f", self.subTotalMaintenancePrice)
            self.dictPricingInfo["subTotalMaintenance"] = cell?.lblSubTotalAmount.text
            /**********************/
            
            //****Credit Maint****
            if self.creditAmountMaint > self.subTotalMaintenancePrice {
                self.creditAmountMaint = self.subTotalMaintenancePrice
            }
            cell?.lblMaintenanceCredit.text = String(format: "$%.2f", self.creditAmountMaint)
            self.dictPricingInfo["creditMaint"] = cell?.lblMaintenanceCredit.text
            /**********************/
            
            //****Total Price****y
            var totalMaint = self.subTotalMaintenancePrice - self.creditAmountMaint
            if totalMaint < 0 {
                totalMaint = 0.0
            }
            cell?.lblMaintenanceTotalPrice.text = String(format: "$%.2f", totalMaint)
            self.dictPricingInfo["totalPriceMaintenance"] = cell?.lblMaintenanceTotalPrice.text
            /**********************/
            
            //****Tax Amount****
            cell?.lblMaintenanceTaxAmount.text = String(format: "$%.2f", taxMaintAmount)
            self.dictPricingInfo["taxAmountMaintenance"] = cell?.lblMaintenanceTaxAmount.text
            /**********************/
            
            //****Billing Amount****
            let dueAmount = self.subTotalMaintenancePrice - self.creditAmountMaint + taxMaintAmount
            cell?.lblTotalDueAmount.text = String(format: "$%.2f", dueAmount)
            self.dictPricingInfo["totalDueAmount"] = cell?.lblTotalDueAmount.text
            /**********************/
            
            /*Maint billing info*/
            if self.arrMaintBillingPrice.count > 0 {
                cell?.tableMaintSetUp(arrMaintBillingPrice : self.arrMaintBillingPrice)
                self.dictPricingInfo["arrMaintBillingPrice"] = self.arrMaintBillingPrice
            }else {
                self.dictPricingInfo["arrMaintBillingPrice"] = []
                cell?.arrMaintBillingPrice.removeAll()
                cell?.tblMaintFrqHeight.constant = 20
                cell?.tblMaintFrq.reloadData()
            }
            /**********************/
            let aryTaxCodeSysName = "\(objLeadDetails?.value(forKey: "taxSysName") ?? "")".split(separator: ",")
             SalesNew_SupportFile().GetNameBySysNameCommon(arysysNameList: aryTaxCodeSysName as NSArray, getmasterName: "MasterSalesAutomation", getkeyName: "TaxMaster", keyName: "Name", keySysName: "SysName") { [self] (result, name, sysname) in
                 if self.strTaxCode != "" {
                     cell?.txtTaxCode.text = name
                 }
                 else
                 {
                     cell?.txtTaxCode.text = ""
                 }
             }
            
            
            if getOpportunityStatus()
            {
                cell?.tblCoupon.isUserInteractionEnabled = false
                cell?.tblCredit.isUserInteractionEnabled = false
            }
            else
            {
                cell?.tblCoupon.isUserInteractionEnabled = true
                cell?.tblCredit.isUserInteractionEnabled = true

            }
            
            if getOpportunityStatus() {
                cell?.txtCoupon.isUserInteractionEnabled = false
                cell?.txtCredit.isUserInteractionEnabled = false
                cell?.txtTaxCode.isUserInteractionEnabled = false
                cell?.btnTaxCode.isUserInteractionEnabled = false
                cell?.btnApplyCoupon.isUserInteractionEnabled = false
//                cell?.btnCredit.isUserInteractionEnabled = false
                cell?.btnAddCredit.isUserInteractionEnabled = false
                cell?.txtOtherDiscount.isUserInteractionEnabled = false
                cell?.txtTipDiscount.isUserInteractionEnabled = false
                cell?.txtLeadInspectionFee.isUserInteractionEnabled = false
            }
            
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 12{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommercialAgreementChecklistTableViewCell, for: indexPath) as? NewCommercialAgreementChecklistTableViewCell
            let dictService = arrAgreementCheckList[indexPath.row]  as! NSDictionary
            cell?.lblTitle.text = "\(dictService.value(forKey: "Title") ?? "")"
            cell?.btnCheck.addTarget(self, action: #selector(btnAgreementCheckList(sender:)), for: .touchUpInside)
            
            if(arrSelectedAgreementChckList.contains(arrAgreementCheckList.object(at: indexPath.row))){ // True
                debugPrint("counting")
                debugPrint(arrSelectedAgreementChckList.count)
                cell?.btnCheck.setImage(UIImage(named: "check_box_2New.png"), for: .normal)
            }else{
                cell?.btnCheck.setImage(UIImage(named: "check_box_1New.png"), for: .normal)
            }
            
            if getOpportunityStatus()
            {
                cell?.btnCheck.isEnabled = false
            }
            else
            {
                cell?.btnCheck.isEnabled = true
            }
            
            return cell ?? UITableViewCell()
            
        }else if indexPath.section ==  9{
            
            
            if isPreferredMonths == false {
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommericialMonthOfServicesCell, for: indexPath) as? NewCommericialMonthOfServicesCell
                cell?.delegate = self
                cell?.setMonthValues(arrMonth: arrayOfMonth)
                
                if getOpportunityStatus()
                {
                    cell?.isUserInteractionEnabled = false
                }
                else
                {
                    cell?.isUserInteractionEnabled = true
                }
                
                return cell ?? UITableViewCell()
            }
            
        }
        else if indexPath.section == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommericialAddtionalNotesCell, for: indexPath) as? NewCommericialAddtionalNotesCell
            cell?.lblTxtDescription.text = strAddtionalNotes
            cell?.delegate = self
            cell?.setUp(textDayValue: strValidFor, isAgreementValid: strIsAgreementValidForNotes)
            
            if getOpportunityStatus()
            {
                cell?.isUserInteractionEnabled = false
            }
            else
            {
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ?? UITableViewCell()
            
        }else if indexPath.section == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableView.NewCommericialInternalNotesCell, for: indexPath) as? NewCommericialInternalNotesCell
            cell?.lblInternalDescription.text = strInternalNotes
            return cell ?? UITableViewCell()
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let introductionLatterCell_IndexPath = IndexPath(row: 0, section: 1)
        let targetDecCell_IndexPath = IndexPath(row: 0, section: 4)
        let pricingCell_IndexPath = IndexPath(row: 0, section: 8)

        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if lastVisibleIndexPath == introductionLatterCell_IndexPath || lastVisibleIndexPath == targetDecCell_IndexPath || lastVisibleIndexPath == pricingCell_IndexPath {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                        configureTableView.beginUpdates()
                        configureTableView.endUpdates()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // HEADER VIEW
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  50))
        viewheader.backgroundColor = UIColor.white
        
        // INSIDE LABEL
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height:50))
        
        // INSIDE BUTTON :-
        addButton = UIButton(frame: CGRect(x: viewheader.frame.size.width - 110, y: 10, width: 90, height: 30))
        addButton?.setImage(#imageLiteral(resourceName: "Edit2"), for: .normal)
        addButton?.layer.cornerRadius = 15
        addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        
       // addButton.center = viewheader.center
        
        // HEADER BOTTOM LINE
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 43.0, width: viewheader.frame.size.width, height:  1.5)
        bottomBorder.backgroundColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        
        if tableView == configureTableView
        {
            if(section == 0)
            {
                lbl.text = "Proposal Cover:"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.isHidden = true
                
            }
            if(section == 1)
            {
                viewheader.isHidden = true
                
            }
            if(section == 2)
            {
                viewheader.isHidden = true
                
            }
            else if(section == 3)
            {
                
                if isTermsOfService == false{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Terms of Service:"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    addButton?.isHidden = true
                }
            }
            
            else if(section == 4)
            {
                if arrOfTagests.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Target"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    // INSIDE BUTTON :-
                    addButton?.isHidden = true
 
                }
            }else if(section == 5)
            {
                
                if arrOfScopes.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Scope"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    // INSIDE BUTTON :-
                    addButton?.isHidden = true

                }
                
            }
            else if(section == 6)
            {
                
                if arrStandardService.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Standard Service"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    // INSIDE BUTTON :-
                    addButton?.isHidden = true

                }
               
            }
            else if(section == 7)
            {
                
                if arrCustomService.count == 0{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Custom Service"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    // INSIDE BUTTON :-
                    addButton?.isHidden = true
                    
                }
            }
            
            else if(section == 8)
            {
               // lbl.text = "Pricing"
               // lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.isHidden = true
                viewheader.isHidden = true
               
            }
            
            else if(section == 12)
            {
                lbl.text = "Agreement Checklist:"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.isHidden = true
      
            }
            else if(section == 9)
            {
                if isPreferredMonths == false{
                    viewheader.isHidden = true
                }else{
                    lbl.text = "Month Of Service:"
                    lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                    addButton?.isHidden = true
                }
            }
            else if(section == 10)
            {
                lbl.text = "Customer Notes"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                addButton?.isHidden = false
                addButton?.addTarget(self, action: #selector(tapOfAddtionalNotes), for: .touchUpInside)
                
                if getOpportunityStatus()
                {
                    addButton?.isEnabled = false
                }
                else
                {
                    addButton?.isEnabled = true
                }
            }
            else if(section == 11)
            {
                lbl.text = "Internal Notes"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
                // INSIDE BUTTON :-
                addButton?.isHidden = false
                addButton?.addTarget(self, action: #selector(tapOfInternalNotes), for: .touchUpInside)
                addButton?.isEnabled = true
               
            }
           
            if DeviceType.IS_IPAD {
                addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
                lbl.font = UIFont.boldSystemFont(ofSize: 21)

            }
            else{
                addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                lbl.font = UIFont.boldSystemFont(ofSize: 18)

            }
           
           // lbl.font = UIFont.boldSystemFont(ofSize: 18)
            viewheader.addSubview(lbl)
            viewheader.addSubview(addButton!)
            viewheader.layer.addSublayer(bottomBorder)
            return viewheader
        }
        else
        {
            return viewheader
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section  == 1{
            return 0
        }
        if section  == 2{
            return 0
        }
        
        if section == 3 {
            if isTermsOfService == false{
                return 0
            }
        }
        
        if section == 4 {
            if arrOfTagests.count == 0{
                return 0
            }
        }
        
        if section == 5 {
            if arrOfScopes.count == 0{
                return 0
            }
        }
        
        if section == 6 {
            if arrStandardService.count == 0{
                return 0
            }
        }
        
        if section == 7 {
            if arrCustomService.count == 0 {
                return 0
            }
        }
        
        if section  == 8{
            return 0
        }
        
        if section == 9 {
            if isPreferredMonths == false{
                return 0
            }
        }
        return 50
    }
    
    //MARK:- Addtional funtion
    
    @objc func CoverTap(){
        
        if !getOpportunityStatus()
        {
            isEditInSalesAuto = true

            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            nextViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nextViewController.modalTransitionStyle = .coverVertical
            nextViewController.aryCoverLetter = arrayCoverLetter
            nextViewController.delegateReturnData = self
            nextViewController.strCoverLetter = self.strCoverLetter
            nextViewController.strTag = 351
            self.present(nextViewController, animated: false, completion: {})
        }
    }
    
    @objc func IntroductionTap(){
        
        if !getOpportunityStatus()
        {
            isEditInSalesAuto = true
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            nextViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nextViewController.modalTransitionStyle = .coverVertical
            nextViewController.aryIntroductionLetter = arrayIntroduction
            nextViewController.delegateReturnData = self
            nextViewController.strTag = 352
            nextViewController.tbl = configureTableView
            nextViewController.strIntroductionLetter = strIntroductionLetter
            self.present(nextViewController, animated: false, completion: {})
        }
    }
    
    @objc func disctiption(){
        
        isEditInSalesAuto = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewCommercialInternalNotesViewController") as! NewCommercialInternalNotesViewController
        nextViewController.InternalNotesStr = self.discStr
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func marketingTap(){
        
        if !getOpportunityStatus()
        {
            isEditInSalesAuto = true
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            nextViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nextViewController.modalTransitionStyle = .coverVertical
            nextViewController.strTag = 350
            nextViewController.arySelectedItems = arrayMarketingContentMultipleSelected
            nextViewController.aryTBL = arrayMarketingContent
            nextViewController.delegateReturnData = self
            self.present(nextViewController, animated: false, completion: {})
        }
        
    }
    
    @objc func btnAgreementCheckList(sender: UIButton){
        
        isEditInSalesAuto = true

        let buttonPosition : CGPoint = sender.convert(CGPoint.zero , to: configureTableView)
        let indexPath = self.configureTableView.indexPathForRow(at: buttonPosition)
        
        if(self.arrSelectedAgreementChckList.contains(self.arrAgreementCheckList.object(at: indexPath!.row))){
            self.arrSelectedAgreementChckList.remove(self.arrAgreementCheckList.object(at: indexPath!.row))
           
            debugPrint(arrSelectedAgreementChckList.count)
            saveAgreementChekList(arrayAgreementChecklistContent:arrSelectedAgreementChckList)
            
            if arrSelectedAgreementChckList.count == 0 {
                deleteAllRecordsFromDB(strEntity: "LeadAgreementChecklistSetups", predicate: NSPredicate(format: "leadId == %@",strLeadId))
            }
            
        }else{
            
            self.arrSelectedAgreementChckList.add(self.arrAgreementCheckList.object(at: indexPath!.row))
            saveAgreementChekList(arrayAgreementChecklistContent:arrSelectedAgreementChckList)
        }
        configureTableView.reloadData()
    }
    
    @objc func termsAndservicesTap(){
        
        if !getOpportunityStatus()
        {
            isEditInSalesAuto = true
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            nextViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nextViewController.modalTransitionStyle = .coverVertical
            nextViewController.arrayTermsOfService = arrayTermsOfService
            nextViewController.strTag = 353
            nextViewController.strTermsOfService = self.strTermsOfService
            nextViewController.delegateReturnData = self
            self.present(nextViewController, animated: false, completion: {})
        }
    }
    
    @objc func btnAgreementCell(){
       
    }
    
    @objc func tapOfAddtionalNotes(){
        
        isEditInSalesAuto = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialConfigureAddtionalNote) as! NewCommercialConfigureAddtionalNote
        nextViewController.strValue = "Addtional Notes"
        nextViewController.delegate = self
        nextViewController.addtionalNote = strAddtionalNotes
        nextViewController.strLeadId = strLeadId
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func tapOfInternalNotes(){
        
        isEditInSalesAuto = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "NewCommercialServices", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierStoryboard.NewCommercialConfigureAddtionalNote) as! NewCommercialConfigureAddtionalNote
        nextViewController.strValue = "Internal Notes"
        nextViewController.delegate = self
        nextViewController.addtionalNote = strInternalNotes
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    //MARK: - Edit Terms Of Service
       @objc func btnEditTermsOfService(sender: UIButton!){
           if strTermsOfService.count > 0 {
           let storyBoard = UIStoryboard(name: DeviceType.IS_IPAD ? "WDOiPad" : "Appointment", bundle: nil)
           let vc = storyBoard.instantiateViewController(withIdentifier: "HTMLEditorVC") as! HTMLEditorVC
           vc.strfrom = "EditConfigureTermsOfService"
           vc.strLeadId = self.strLeadNumber as String
           vc.strHtml = strDescriptionTermsService
               vc.delegateCommerical = self
           vc.modalPresentationStyle = .fullScreen
           self.isEditInSalesAuto = true
           self.present(vc, animated: false, completion: nil)
           }
           else{
               showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please select terms of service first", viewcontrol: self)

           }
       }
    
}
extension NewCommercialConfigureDashBoardVC: SalesConfigureDelegate, NewCommericialConfigurePricingDelegate, CustomTableView ,refreshTable{
    
    func refreshTable(data: String) {
        configureTableView.reloadData()
    }
    
    
    func callBackSelectTaxCode(cell: NewCommericialConfigurePricingCell) {
        if (isInternetAvailable()){
            
            isEditInSalesAuto = true

            if self.arrTaxCode?.count ?? 0 > 0 {
                openTableViewPopUp(tag: 3333, ary: (self.arrTaxCode! as NSArray).mutableCopy() as! NSMutableArray)
            }else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No tax code available.", viewcontrol: self)
            }
            
            }
            
        else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Can't update taxcode offline", viewcontrol: self)
        }
    }
    
    func callBackAddApplyCoupon(cell: NewCommericialConfigurePricingCell, couponCode: String)
    {
       
        isEditInSalesAuto = true

        //Check Service Add to Agreement or not
        let serviceCount = (self.arrStandardServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add atleast one service to apply coupon.", viewcontrol: self)
            return
        }
        
        if couponCode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter coupon code.", viewcontrol: self)
        }else {
            
            debugPrint(couponCode)
            
            guard let dictMaster: [String: Any] = UserDefaults.standard.object(forKey: "MasterSalesAutomation") as? [String : Any] else
            {
                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
            }
            
            guard let arrDiscountCoupon: [[String: Any]] = dictMaster["DiscountSetupMasterCoupon"] as? [[String : Any]] else
            {
                return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
            }
            //***Check Applied Coupon Available or Not***
            var isValidCoupon = false
            var dictCouponDetail: [String: Any]?
            for couponDetail in arrDiscountCoupon {
                if couponDetail["DiscountCode"] as! String == couponCode {
                    isValidCoupon = true
                    dictCouponDetail = couponDetail
                    break
                }else {
                    isValidCoupon = false
                }
            }
            
            if !isValidCoupon {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Invalid coupon code.", viewcontrol: self)
            }else {
                //Coupon Availe
                
                debugPrint(dictCouponDetail)
                let isCouponApplied = self.checkForAppliedDiscount(strType: "Coupon", strDiscountCode: couponCode, couponUsage: dictCouponDetail?["Usage"] as? String ?? "")
                if isCouponApplied {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon already applied.", viewcontrol: self)
                }else {
                    
                    //Applied coupon Valid on Not
                    let isValidDiscount = self.checkCouponValidity(dictCouponDetail: dictCouponDetail!)
                    if isValidDiscount {
                        //Coupon Valid
                      
                        let arrTempCouponAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon")
                        let arrTempCreditAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Credit")
                        var totalDiscountAmoutAlreadyPresent = 0.0
                        
                        //Coupon discout already Applied
                        for objTemp in arrTempCouponAlreadyApplied {
                            debugPrint(objTemp)
                            let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "")"
                            let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")
                            
                            let discountAmountPresent: Double = Double(strDiscountAmountPresent) ?? 0.0
                            
                            let applicableForMaintenance = "\(objTemp.value(forKey: "applicableForMaintenance") ?? "")"
                            if applicableForMaintenance == "1" || applicableForMaintenance == "true" {
                                
                            }else {
                                totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresent;
                            }
                        }
                        //+ Credit discout already Applied
                        for objTemp in arrTempCreditAlreadyApplied {
                            debugPrint(objTemp)
                            let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "")"
                            let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")
                            
                            let discountAmountPresent: Double = Double(strDiscountAmountPresent) ?? 0.0
                            
                            let applicableForMaintenance = "\(objTemp.value(forKey: "applicableForMaintenance") ?? "")"
                            if applicableForMaintenance == "1" || applicableForMaintenance == "true" {
                                
                            }else {
                                totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresent;
                            }
                        }
                        
                        var isToSave = false
                        var strIsServiceFound = ""
                        let strDiscountAmountTemp = "\(dictCouponDetail?["DiscountAmount"] ?? "")"
                        
                        let discount: Double = Double(strDiscountAmountTemp) ?? 0.0
                        //20
                        //If Not Service Base
                        debugPrint(self.arrStandardService.count)
                        let arrStanAllSoldService = self.arrStandardServiceSold
                        
                        
                        if "\(dictCouponDetail?["IsServiceBased"] ?? "0")" == "1" || "\(dictCouponDetail?["IsServiceBased"] ?? "false")".caseInsensitiveCompare("true") == .orderedSame
                        {
//                            "ServiceSysName": SignaturePestControl
                            // Service Base

                            var objMatchesDiscoutUpdate : NSManagedObject?
                            for i in 0..<arrStanAllSoldService.count {
                                objMatchesDiscoutUpdate = (arrStanAllSoldService[i] as! NSManagedObject)
                                
                                if objMatchesDiscoutUpdate?.value(forKey: "serviceSysName") as! String == "\(dictCouponDetail?["ServiceSysName"] ?? "")" {
                                    
                                    //
                                    /************Initial  value to Coupon calculation**********/
                                    let valueInitial = (self.subTotalInitialPrice + (leadInspectionFee - otherDiscount - tipDiscount))//self.subTotalInitialPrice
//                                    let strInitial = "\((objMatchesDiscoutUpdate as AnyObject).value(forKey: "totalInitialPrice") ?? "0")"
//                                    valueInitial = Double(strInitial) ?? 0.0
                                    /***********************/
                                    if i == 0 {
                                        if (!(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount))) {
                                            
                                            if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                                
                                                let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                                
                                                var dictTempToSave = dictCouponDetail
                                                dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                                dictCouponDetail = dictTempToSave
                                                
                                                isToSave = true

                                                if (amountDiscountToApplyCredit > 0) {
                                                    
                                                    isToSave = true
                                                    appliedDiscountInitial = amountDiscountToApplyCredit
                                                    
                                                    
                                                }else{
                                                    
                                                    isToSave = false
                                                    appliedDiscountInitial = 0.0;
                                                    
                                                }
                                            }
                                            
                                        }else {
                                            appliedDiscountInitial = discount;
                                            isToSave = true
                                        }
                                    }else {
                                        if (!(valueInitial >= discount)) {
                                            
                                            if (valueInitial < discount) {
                                                
                                                let amountDiscountToApplyCredit = discount
                                                
                                                var dictTempToSave = dictCouponDetail
                                                dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                                dictCouponDetail = dictTempToSave
                                                
                                                isToSave = true

                                                if (amountDiscountToApplyCredit > 0) {
                                                    
                                                    isToSave = true
                                                    appliedDiscountInitial = amountDiscountToApplyCredit
                                                    
                                                    
                                                }else{
                                                    
                                                    isToSave = false
                                                    appliedDiscountInitial = 0.0;
                                                    
                                                }
                                            }
                                            
                                        }else {
                                            appliedDiscountInitial = discount;
                                            isToSave = true
                                        }
                                    }
                                    strIsServiceFound = "true"
                                    break
                                }else {
                                    strIsServiceFound = "false"
                                }
                            }
                        }
                        else
                        {
                            //Non Service Base
                            strIsServiceFound = ""
                            /************Initial  value to Coupon calculation**********/
                            let valueInitial = (self.subTotalInitialPrice + (leadInspectionFee - otherDiscount - tipDiscount))//self.subTotalInitialPrice
//                            let strInitial = "\((arrStanAllSoldService[0] as AnyObject).value(forKey: "totalInitialPrice") ?? "0")"
//                            valueInitial = Double(strInitial) ?? 0.0
                            /***********************/
                            
                            if (!(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount))) {
                                
                                if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                    
                                    let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                    
                                    debugPrint(dictCouponDetail)
                                    var dictTempToSave = dictCouponDetail
                                    
                                    dictTempToSave?["DiscountAmount"] = "\(amountDiscountToApplyCredit)"
                                    
                                    dictCouponDetail = dictTempToSave
                                    debugPrint(dictCouponDetail)
                                    
                                    isToSave = true

                                    if (amountDiscountToApplyCredit > 0) {
                                        
                                        isToSave = true
                                        appliedDiscountInitial = amountDiscountToApplyCredit
                                        
                                        
                                    }else{
                                        
                                        isToSave = false
                                        appliedDiscountInitial = 0.0;
                                        
                                    }
                                }
                                
                            }else {
                                appliedDiscountInitial = discount;
                                isToSave = true
                            }
                        }
                        
                        
                         
                        //Save data to DB
                        if isToSave {
                            self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dictCouponDetail!, strType: "Coupon")
                            
                        }else {
                            
                            if strIsServiceFound == "false" {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon applicable service is not available.", viewcontrol: self)
                            }else {
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Coupon can not be more then billing amount.", viewcontrol: self)
                            }
                        }
                    }else {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Invalid coupon code.", viewcontrol: self)
                    }
                    
                }
            }
        }
        cell.txtCoupon.text = ""
    }
    
    func callBackSelectCredit(cell: NewCommericialConfigurePricingCell) {
        
        
        if !getOpportunityStatus()
        {
            isEditInSalesAuto = true
            if nsud.value(forKey: "MasterSalesAutomation") is NSDictionary
            {
                let dictMaster = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
                //Getting Credit and Coupon Detail
                if(dictMaster.value(forKey: "DiscountSetupMasterCredit") is NSArray) {
                    self.arrDiscountCredit = dictMaster.value(forKey: "DiscountSetupMasterCredit") as! [Any]
                    debugPrint(self.arrDiscountCredit)
                    
                    let arrTempCredit = self.fetchForAppliedDiscountFromCoreData(strType: "Credit")
                    let arrOnlyAppliedCredit: NSMutableArray = NSMutableArray(array: self.arrDiscountCredit)
                    
                    
                    for object in arrTempCredit {
                        
                        let strSysNameDiscount = object.value(forKey: "discountSysName")
                        for dict in arrDiscountCredit {
                            let dict1: [String: Any] = dict as! [String : Any]
                            if strSysNameDiscount as! String == "\(dict1["SysName"] ?? "")" {
                                arrOnlyAppliedCredit.remove(dict)
                            }
                            
                        }
                    }
                    self.arrDiscountCredit.removeAll()
                    self.arrDiscountCredit = arrOnlyAppliedCredit as! [Any]
                    
                    
                    
                    if ReachabilityTest.isConnectedToNetwork() {
                        debugPrint("Internet connection available")
                        
                        let arrTemp: NSMutableArray = NSMutableArray()
                        let arrTempNew: NSMutableArray = NSMutableArray(array: self.arrDiscountCredit)
                        for dictCredit in arrDiscountCredit {
                            let dictCredit1: [String: Any] = dictCredit as! [String : Any]
                            
                            for dictAppliedCredit in self.arrAppliedCreditDetail {
                                let dictAppliedCredit1: [String: Any] = dictAppliedCredit as! [String : Any]
                                let strC1: String = dictCredit1["SysName"] as! String
                                let strC2: String = dictAppliedCredit1["SysName"] as! String
                                if strC1 == strC2 {
                                    arrTemp.add(dictCredit1)
                                    break
                                }
                            }
                        }
                        
                        arrTempNew.removeObjects(in: arrTemp as! [Any])
                        self.arrDiscountCredit.removeAll()
                        self.arrDiscountCredit = arrTempNew as! [Any]
                    }
                    else{
                        debugPrint("No internet connection available")
                    }
                    
                    if self.arrDiscountCredit.count > 0 {
                        openTableViewPopUp(tag: 3334, ary: (self.arrDiscountCredit as NSArray).mutableCopy() as! NSMutableArray)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No credit available.", viewcontrol: self)
                    }

                }
                
            }

        }
        
    }
    
    func callBackAddCredit(cell: NewCommericialConfigurePricingCell, creditCode: String){
        
        //Check Service Add to Agreement or not
        isEditInSalesAuto = true

        
        let serviceCount = (self.arrStandardServiceSold.count + self.arrCustomServiceSold.count)
        
        if serviceCount == 0 {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please add atleast one service to apply credit.", viewcontrol: self)
            return
        }
        
        if creditCode == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select credit code.", viewcontrol: self)

        }else {
            
            let arrStanAllSoldService = self.arrStandardServiceSold
            debugPrint(arrStanAllSoldService.count)
            
            if arrStanAllSoldService.count > 0 {
                
                guard let dictMaster: [String: Any] = UserDefaults.standard.object(forKey: "MasterSalesAutomation") as? [String : Any] else
                {
                    return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                }
                
                
                //can define Globle : arrDiscountCredit
                guard let arrDiscountCredit: [[String: Any]] = dictMaster["DiscountSetupMasterCredit"] as? [[String : Any]] else
                {
                    return showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailable, viewcontrol: self)
                }
                debugPrint(arrDiscountCredit)
                
                
                let strDiscountCredit = "\(self.dictSelectedCredit?["SysName"] ?? "")"
                let isCreditApplied = self.checkForAppliedDiscount(strType: "Credit", strDiscountCode: strDiscountCredit, couponUsage: "OneTime")
                
                debugPrint(self.dictSelectedCredit)
                if isCreditApplied {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit Coupon already applied.", viewcontrol: self)
                }else {
                    
                    for dictObj in arrDiscountCredit {
                        var dict: [String: Any] = dictObj
                        
                        /*Check In Applied Credit Available*/
                        if strDiscountCredit == "\(dict["SysName"] ?? "")" {
                            
                            /************Initial and Maint value to credite calculation**********/
                            let valueInitial = (self.subTotalInitialPrice + (leadInspectionFee - otherDiscount - tipDiscount))//self.subTotalInitialPrice
                            let valueMaint = self.subTotalMaintenancePrice
                            /***********************/
                            
                            if "\(dict["IsDiscountPercent"] ?? "")" == "1" || "\(dict["qq"] ?? "")" == "true" {

                                var isToSave = false
                                //Maint
                                if "\(dict["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dict["ApplicableForMaintenance"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon") + self.fetchForAppliedDiscountFromCoreData(strType: "Credit")

                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedMaintDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //**************
                                    appliedDiscountMaint = ((valueMaint - totalDiscountAmoutAlreadyPresent) * Double("\(dict["DiscountPercent"] ?? "0")")!) / 100

                                    //Check Maint Amount to update on DB
                                    if !(valueMaint >= (totalDiscountAmoutAlreadyPresent + appliedDiscountMaint)) {
                                        
                                        if (valueMaint < (totalDiscountAmoutAlreadyPresent + appliedDiscountMaint)) {
                                            
                                            let amountDiscountToApplyCredit = valueMaint - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountMaint = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountMaint = 0.0
                                            }
                                            
                                        }
                                        
                                    }else {
                                        if appliedDiscountMaint > valueMaint {
                                            appliedDiscountMaint = valueMaint
                                        }
                                        
                                        let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                        dictTempToSave.setValue("\(appliedDiscountMaint)", forKey: "DiscountAmount")
                                        dict = dictTempToSave as! [String : Any]
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountMaint = 0.0
                                }
                                
                                
                                //Initial
                                if "\(dict["ApplicableForInitial"] ?? "0")" == "1" || "\(dict["ApplicableForInitial"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon") + self.fetchForAppliedDiscountFromCoreData(strType: "Credit")

                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                        
                                        if !("\(objTemp.value(forKey: "discountType") ?? "0")" ==  "Credit") {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //***-Discount Percent Base-***
                                    
                                    appliedDiscountInitial = ((valueInitial - totalDiscountAmoutAlreadyPresent) * Double("\(dict["DiscountPercent"] ?? "0")")!) / 100
                                    
                                    //Check Initial Amount to update on DB
                                    if !(valueInitial >= (totalDiscountAmoutAlreadyPresent + appliedDiscountInitial)) {
                                        
                                        if (valueInitial < (totalDiscountAmoutAlreadyPresent + appliedDiscountInitial)) {
                                            
                                            let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountInitial = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountInitial = 0.0;
                                            }
                                            
                                        }
                                        
                                    }else {
                                        if appliedDiscountInitial > valueInitial {
                                            appliedDiscountInitial = valueInitial
                                        }
                                        
                                        let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                        dictTempToSave.setValue("\(appliedDiscountInitial)", forKey: "DiscountAmount")
                                        
                                        dict = dictTempToSave as! [String : Any]
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountInitial = 0.0
                                }
                                
                                
                                //Want to Save on DB
                                if isToSave {
                                    self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dict, strType: "Credit")

                                }else {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit can not be more then billing amount.", viewcontrol: self)
                                }
                                
                            }else {
                                
                                //***-Discount: Amount Base-***
                                
                                appliedDiscountMaint = 0.0
                                appliedDiscountInitial = 0.0
                                var discount = 0.0

                                
                                let strDiscount = "\(dict["DiscountAmount"] ?? "0")"
                                discount = Double(strDiscount) ?? 0.0
                                appliedDiscountInitial = discount
                                appliedDiscountMaint = discount

                                var isToSave = false
                                //Maint
                                if "\(dict["ApplicableForMaintenance"] ?? "0")" == "1" || "\(dict["ApplicableForMaintenance"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon") + self.fetchForAppliedDiscountFromCoreData(strType: "Credit")

                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    
                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedMaintDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        
                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //Check Maint Amount to update on DB
                                    if !(valueMaint >= (totalDiscountAmoutAlreadyPresent + discount)) {
                                        
                                        if (valueMaint < (totalDiscountAmoutAlreadyPresent + discount)) {
                                            
                                            let amountDiscountToApplyCredit = valueMaint - totalDiscountAmoutAlreadyPresent
                                            
                                            var dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountMaint = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountMaint = 0.0
                                            }
                                            
                                        }
                                        
                                    }else {
                                        let strMaint = self.subTotalMaintenancePrice
                                        if discount > Double(strMaint) ?? 0.0 {
                                            appliedDiscountMaint = Double(strMaint) ?? 0.0
                                        }
                                        
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountMaint = 0.0
                                }
                                
                                
                                //Initial
                                if "\(dict["ApplicableForInitial"] ?? "0")" == "1" || "\(dict["ApplicableForInitial"] ?? "0")" == "true" {
                                    
                                    let arrTempCreditAlreadyApplied = self.fetchForAppliedDiscountFromCoreData(strType: "Coupon") + self.fetchForAppliedDiscountFromCoreData(strType: "Credit")

                                    //***-Calculation To Total Already Exist Discount Amount-***
                                    var totalDiscountAmoutAlreadyPresent = 0.0
                                    for objTemp in arrTempCreditAlreadyApplied {
                                        
                                        let strDiscountAmountPresentTemp = "\(objTemp.value(forKey: "appliedInitialDiscount") ?? "0")"
                                        let strDiscountAmountPresent = strDiscountAmountPresentTemp.replacingOccurrences(of: "$", with: "")

                                        let discountAmountPresentt: Double = Double(strDiscountAmountPresent)!

                                        if "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objTemp.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                        
                                        if !("\(objTemp.value(forKey: "discountType") ?? "0")" ==  "Credit") {
                                           
                                            totalDiscountAmoutAlreadyPresent = totalDiscountAmoutAlreadyPresent + discountAmountPresentt
                                        }
                                    }
                                    //*******************
                                    
                                    //Check Initial Amount to update on DB
                                    if !(valueInitial >= (totalDiscountAmoutAlreadyPresent + discount)) {
                                        
                                        if (valueInitial < (totalDiscountAmoutAlreadyPresent + discount)) {
                                            
                                            let amountDiscountToApplyCredit = valueInitial - totalDiscountAmoutAlreadyPresent
                                            
                                            let dictTempToSave : NSMutableDictionary = NSMutableDictionary(dictionary: dict)
                                            dictTempToSave.setValue("\(amountDiscountToApplyCredit)", forKey: "DiscountAmount")
                                            
                                            dict = dictTempToSave as! [String : Any]
                                            isToSave = true
                                            if amountDiscountToApplyCredit > 0 {
                                                isToSave = true
                                                appliedDiscountInitial = amountDiscountToApplyCredit
                                            }else {
                                                isToSave = false
                                                appliedDiscountInitial = 0.0;
                                            }
                                            
                                        }
                                        
                                    }else {
                                        if discount > valueInitial {
                                            appliedDiscountInitial = valueInitial
                                        }
                                        isToSave = true
                                    }
                                }else {
                                    appliedDiscountInitial = 0.0
                                }
                                
                                
                                //Want to Save On DB
                                if isToSave {
                                    self.saveAppliedDiscountCouponAndCreditCoreData(dictCouponDetail: dict, strType: "Credit")

                                }else {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit can not be more then billing amount.", viewcontrol: self)
                                }
                                
                            }
                            
                            break
                        }
                    }
                    
                }
                
                
            }else {
                //Credit applicable service not available
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Credit applicable service not available.", viewcontrol: self)
            }
        }
        cell.txtCredit.text = ""

    }
    
    func callBackDeleteCouponOrCredit(cell: NewCommericialConfigurePricingCell, objCouponDetails : NSManagedObject)
    {
        isEditInSalesAuto = true

        debugPrint(objCouponDetails)
        
//        let strServiceName = "\(objCouponDetails.value(forKey: "serviceSysName") ?? "")"
//        let strDiscountAmount = "\(objCouponDetails.value(forKey: "appliedInitialDiscount") ?? "")"
//        let strDiscountPer = "\(objCouponDetails.value(forKey: "discountPercent") ?? "")"
//        let strChkDiscountPer = "\(objCouponDetails.value(forKey: "isDiscountPercent") ?? "")"
        let strDiscountCode = "\(objCouponDetails.value(forKey: "discountCode") ?? "")"

        let strDiscountType = "\(objCouponDetails.value(forKey: "discountType") ?? "")"

        
        let alert = UIAlertController(title: alertMessage, message: "Are you sure want to delete", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: { (nil) in
        }))
        
        alert.addAction(UIAlertAction (title: "OK", style: .cancel, handler: { (nil) in
            
            let context = getContext()
            context.delete(objCouponDetails)
            //save the object
            do { try context.save()
                debugPrint("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
            
          //  self.fetchForAppliedDiscountFromCoreData(strType: strDiscountType)
            self.initialSetup()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Tax
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        debugPrint(dictData)
        isEditInSalesAuto = true

        if tag == 3333 {
            //Tax
            self.dictSelectedTaxCode = (dictData as! [String : Any])
            
            let indexPath = IndexPath(row: 0, section: 8)
            let cell: NewCommericialConfigurePricingCell = self.configureTableView.cellForRow(at: indexPath)! as! NewCommericialConfigurePricingCell
            cell.txtTaxCode.text = dictSelectedTaxCode?["Name"] as? String ?? ""
            let strTaxCode = dictSelectedTaxCode?["SysName"] as? String ?? ""
            
            if cell.txtTaxCode.text != "" {
                self.getTaxCodeDetailWithApi(strTaxCode: strTaxCode)
            }else {
                self.strTaxCode = ""
                let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
                
                debugPrint(arrayLeadDetail)
                if(arrayLeadDetail.count > 0)
                {
                    let objLeadInfo : NSManagedObject? = (arrayLeadDetail[0] as! NSManagedObject)
                    objLeadInfo?.setValue("0", forKey: "tax")
                    objLeadInfo?.setValue("", forKey: "taxSysName")
                    
                    let context = getContext()
                    //save the object
                    do { try context.save()
                        debugPrint("Record Saved Updated.")
                    } catch let error as NSError {
                        debugPrint("Could not save. \(error), \(error.userInfo)")
                    }
                }
                self.initialSetup()
            }
        
        }else {
            //Credit 3334
            self.dictSelectedCredit = (dictData as! [String : Any])
            let indexPath = IndexPath(row: 0, section: 8)
            let cell: NewCommericialConfigurePricingCell = self.configureTableView.cellForRow(at: indexPath)! as! NewCommericialConfigurePricingCell
            cell.txtCredit.text = dictSelectedCredit?["Name"] as? String ?? ""
        }
        
    }
    
    
    func sucessfullyGetTaxFromTaxCode(_ output: Any) {
        //Tax Value
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.loader.dismiss(animated: false) {}
            let taxValue: Double = output as! Double
            debugPrint(taxValue)
            self.isEditInSalesAuto = true

            self.updateLeadDetailsWithTax(taxValue: taxValue)
        })
    }
    
    
    func updateLeadDetailsWithTax(taxValue: Double) {
        debugPrint(self.dictSelectedTaxCode)
        var strTaxCode = dictSelectedTaxCode?["SysName"] as? String ?? ""
        if strTaxCode == "" {
            strTaxCode = self.strTaxCode
        }
        
        let arrayLeadDetail = getDataFromLocal(strEntity: "LeadDetail", predicate: NSPredicate(format: "leadId == %@", self.strLeadId))
        
        debugPrint(arrayLeadDetail)
        if(arrayLeadDetail.count > 0)
        {
            let objLeadInfo : NSManagedObject? = (arrayLeadDetail[0] as! NSManagedObject)
            objLeadInfo?.setValue("\(taxValue)", forKey: "tax")
            objLeadInfo?.setValue(strTaxCode, forKey: "taxSysName")
            
            let context = getContext()
            //save the object
            do { try context.save()
                debugPrint("Record Saved Updated.")
            } catch let error as NSError {
                debugPrint("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        self.initialSetup()
    }
    
    
    //MARK: - Credit
    func sucessfullyGetCredits(_ output: [Any]) {
        debugPrint(output)
        self.arrAppliedCreditDetail = output
    }
    
    func sucessfullyGetTemplateKey(_ output: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: {
            self.fetchServiceProposalFollowUp()
            self.loader.dismiss(animated: false) {}

            if let dict: [String: Any] = output as? [String : Any] {
                if let msg = dict["Message"] {
                    print(msg)
                    //Template key not found
                    self.goToReview()
                }else {
                    
                    if let strTemplateKey = dict["TemplateKey"] as? String {
                        if strTemplateKey.count > 0 {
                            self.arrImageDetails.removeAll()
                            self.arrGraphDetails.removeAll()
                            
                            self.loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                            self.present(self.loader, animated: false, completion: nil)

                            self.fetchImageFromDB(strHeaderTitle: "Before") { success in
                                if success {
                                    
                                    self.uploadCRMImages() { success in
                                        if success {
                                            self.fetchImageFromDB(strHeaderTitle: "Graph") { success in
                                                self.loader.dismiss(animated: false) {
                                                    if success {
                                                        self.goToFormBuilder(dict: dict)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }else {
                            self.goToReview()
                        }
                    }
                    else {
                        print("Failure")
                        self.goToReview()
                    }
                }
            }else {
                print("Nulllllll")
                self.goToReview()
            }
        })
    }
    
    /*******************************************************************/
    
    func openTableViewPopUp(tag:Int, ary:NSMutableArray)
    {
        let vc: PopUpView = UIStoryboard.init(name: "CRMiPad", bundle: nil).instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
       // vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    
    func callBackOtherDiscount(value: Double) {
        debugPrint("---------")
        debugPrint(value)

        self.objLeadDetails?.setValue(String(format: "$%.2f", value), forKey: "otherDiscount")
        
        //save the object
        let context = getContext()
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    
    func callBackTipDiscount(value: Double) {
        debugPrint("--------->>>>>")
        debugPrint(value)

        self.objLeadDetails?.setValue(String(format: "$%.2f", value), forKey: "tipDiscount")
        //save the object
        let context = getContext()
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
    
    func callBackLeadInspection(value: Double) {
        debugPrint("---------")
        debugPrint(value)
        
        self.objLeadDetails?.setValue(String(format: "$%.2f", value), forKey: "leadInspectionFee")
        //save the object
        let context = getContext()
        do { try context.save()
            debugPrint("Record Saved Updated.")
        } catch let error as NSError {
            debugPrint("Could not save. \(error), \(error.userInfo)")
        }
        
        self.initialSetup()
    }
}
extension NewCommercialConfigureDashBoardVC : HTMLEditorForCommercialDelegate{
    
//    func didReceiveHTMLEditorText(strText: String, strFrom: String) {
//        if strFrom != ""{
//            isEditInSalesAuto = true
//            TemplateContent = strText
//            configureTableView.reloadData()
//        }
//    }
    
    func didReceiveHTMLEditorText(strText: String, strFrom: String) {
            if strFrom == "EditConfigureTermsOfService"{
                
                DispatchQueue.main.async {
                    
                    self.isEditInSalesAuto = true
                    self.strDescriptionTermsService = strText
                    self.configureTableView.reloadData()
                }
 
            }
            else if strFrom != ""{
                
                DispatchQueue.main.async {
                    
                    self.isEditInSalesAuto = true
                    self.TemplateContent = strText
                    self.configureTableView.reloadData()
                }
                
            }
        }
      
}
extension UITableView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: reloadData)
            { _ in completion() }
    }
}
