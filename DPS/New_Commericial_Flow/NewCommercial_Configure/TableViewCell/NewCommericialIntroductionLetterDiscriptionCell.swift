//
//  NewCommericialIntroductionLetterDiscriptionCell.swift
//  DPS
//
//  Created by Saavan Patidar on 26/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialIntroductionLetterDiscriptionCell: UITableViewCell ,UIWebViewDelegate, WKNavigationDelegate ,WKUIDelegate{
    var loadCount: Int = 0

    @IBOutlet var btnEditIntroductionLetter: UIButton!
    //@IBOutlet var lblIntroductionLetter: UILabel!
    @IBOutlet weak var webViewForIntroductionLetter: WKWebView!
    
    @IBOutlet var heightForWebVIew: NSLayoutConstraint!
    
    
    var callback: ((_ str: String) -> Void)?
    
    var tableView : UITableView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        webViewForIntroductionLetter.uiDelegate = self
        webViewForIntroductionLetter.navigationDelegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
            tableView?.reloadData()
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadWebView(str : String){
        webViewForIntroductionLetter.loadHTMLString((str) as String, baseURL: Bundle.main.bundleURL)
    }
    
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("121321321321321321")
//        webView.frame.size.height = 1
//        webView.frame.size = webView.sizeThatFits(.zero)
//        webView.scrollView.isScrollEnabled = false
//
//        heightForWebVIew.constant = webView.scrollView.contentSize.height
//
//        self.callback?("refresh")
//
//
//    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadCount += 1
    }

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadCount -= 1
        
        webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (height, error) in
//            self.webViewHeightConstraint?.constant = height as! CGFloat
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
                if self!.loadCount == 0 {
                    webView.frame.size.height = 1
                    webView.frame.size = webView.sizeThatFits(.zero)
                    webView.scrollView.isScrollEnabled = false
                    self?.heightForWebVIew.constant = 0

                    self?.heightForWebVIew.constant = webView.scrollView.contentSize.height
                    self?.callback?("refresh")
                }
            }
        })
    }

}

