//
//  NewCommercialConfigureAddScopeCell.swift
//  Commercial_Flow_Demo
//
//  Created by Saavan Patidar on 18/11/21.
//

import UIKit

class NewCommercialConfigureAddScopeCell: UITableViewCell {

    @IBOutlet var lblScope: UILabel!
    @IBOutlet var lblScopeDescription: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
