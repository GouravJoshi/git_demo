//
//  newCommericialMonthOfServicesCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 25/10/21.
//

import UIKit



protocol ConfigureMonthOfServiceCellDelegate : class {
   
    //ConfigureMonthTableViewCell Method
    func callBackMonthMethod(cell: NewCommericialMonthOfServicesCell, month: String, tag: Int)
}


class NewCommericialMonthOfServicesCell: UITableViewCell {
    
    
    weak var delegate : ConfigureMonthOfServiceCellDelegate?
    
    @IBOutlet var btnJanuary: UIButton!
    @IBOutlet var btnFeb: UIButton!
    @IBOutlet var btnMarch: UIButton!
    @IBOutlet var btnApril: UIButton!
    @IBOutlet var btnMay: UIButton!
    @IBOutlet var btnJune: UIButton!
    @IBOutlet var btnJuly: UIButton!
    @IBOutlet var btnAugust: UIButton!
    @IBOutlet var btnSeptember: UIButton!
    @IBOutlet var btnOctober: UIButton!
    @IBOutlet var btnNovember: UIButton!
    @IBOutlet var btnDecember: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMonthValues(arrMonth: [String]) {
        
        for month in arrMonth {
            if month == "January" {
                btnJanuary.isSelected = true
            }
            
            if month == "February" {
                btnFeb.isSelected = true
            }
            
            if month == "March" {
                btnMarch.isSelected = true
            }
            
            if month == "April" {
                btnApril.isSelected = true
            }
            
            if month == "May" {
                btnMay.isSelected = true
            }
            
            if month == "June" {
                btnJune.isSelected = true
            }
            
            if month == "July" {
                btnJuly.isSelected = true
            }
            
            if month == "August" {
                btnAugust.isSelected = true
            }
            
            if month == "September" {
                btnSeptember.isSelected = true
            }
            
            if month == "October" {
                btnOctober.isSelected = true
            }
            
            if month == "November" {
                btnNovember.isSelected = true
            }
            
            if month == "December" {
                btnDecember.isSelected = true
            }

        }
    }
    
    
    @IBAction func janAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "January", tag: 0)
    }
    
    @IBAction func febAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "February", tag: 1)
    }
    
    @IBAction func marchAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "March", tag: 2)
    }
    
    @IBAction func aprilAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "April", tag: 3)
    }
    
    @IBAction func mayAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "May", tag: 4)
    }
    
    @IBAction func juneAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "June", tag: 5)
    }
    
    @IBAction func julAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "July", tag: 6)
    }
    
    @IBAction func augAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "August", tag: 7)
    }
    
    @IBAction func septAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "September", tag: 8)
    }
    
    @IBAction func octAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "October", tag: 9)
    }
    
    @IBAction func novAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "November", tag: 10)
    }
    
    @IBAction func decAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMonthMethod(cell: self, month: "December", tag: 11)
    }
    
    
    
}
