//
//  NewCommericialMarketingContentCell.swift
//  DPS
//
//  Created by Saavan Patidar on 26/11/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

protocol refreshTable : class {
    
    func refreshTable(data : String)
}

class NewCommericialMarketingContentCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource{
   
    //MARK - outlets
    @IBOutlet var stackMarktingContent: UIStackView!
    @IBOutlet var lblMarketingContent: UILabel!
    @IBOutlet var collMarketingContent: UICollectionView!
    @IBOutlet var heightCollectionVIew: NSLayoutConstraint!
    
   //Mark - Variables
    var dictMarketContentCollectionView = NSMutableArray()
    weak var delegateReturnData : DataProtocol?
    var opportunityStatus : Bool?
    weak var PrefreshTable : refreshTable?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collMarketingContent.delegate = self
        collMarketingContent.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictMarketContentCollectionView.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "NewCommericialMarketContent", for: indexPath) as? NewCommericialMarketContent
        cell?.viewBackground.cornerRadiusV =  15
        cell?.lblMarketTitle.text = ((dictMarketContentCollectionView[indexPath.row] as AnyObject).value(forKey: "Title") as? String)
        cell?.btnClose.addTarget(self, action: #selector(actionBtn(sender:)), for: .touchUpInside)
        
        if self.opportunityStatus ?? false
        {
            cell?.btnClose.isHidden = true
        }
        else
        {
            cell?.btnClose.isHidden = false
        }
        
        return cell ?? UICollectionViewCell()
    }
    
    @objc func actionBtn(sender: UIButton){
        let buttonPosition : CGPoint = sender.convert(CGPoint.zero , to: collMarketingContent)
        let indexPath = collMarketingContent.indexPathForItem(at: buttonPosition)
        dictMarketContentCollectionView.removeObject(at: indexPath!.row)
        PrefreshTable?.refreshTable(data: "refresh")
        collMarketingContent.reloadData()
    }
    

}


