//
//  NewCommercialConfigureStandardCell.swift
//  Commercial_Flow_Demo
//
//  Created by Saavan Patidar on 18/11/21.
//

import UIKit

protocol ConfigureStandardCellDelegate : class {
    func callBackAddAgreementMethod(cell: NewCommercialConfigureStandardCell)
}

class NewCommercialConfigureStandardCell: UITableViewCell {
    
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceDescription: UILabel!
    
    @IBOutlet var lblIntialPrice: UILabel!
    @IBOutlet var lblDiscountPrice: UILabel!
    @IBOutlet var lblDIscount: UILabel!
    @IBOutlet var lblFinalIntialPrice: UILabel!
    @IBOutlet var lblMaintenancePrice: UILabel!
    @IBOutlet var btnAddToAgreement: UIButton!
    
    @IBOutlet var lblFrequencyName: UILabel!
    @IBOutlet var lblBillingFrequencyName: UILabel!
    
    @IBOutlet var btnAgreementChekList: UIButton!
    @IBOutlet var viewForHeightDescription: NSLayoutConstraint!
    
    @IBOutlet var viewForDescription: UIView!
    @IBOutlet var lblStrMaintenancePrice: UILabel!
    
    @IBOutlet var viewForBottom: NSLayoutConstraint!
    
    @IBOutlet var lblStrheightForMaintenancePrice: NSLayoutConstraint!
    @IBOutlet var lblHeightForMaintenancePrice: NSLayoutConstraint!
    
    @IBOutlet var lblRenewalContent: UILabel!
    @IBOutlet var lblRenewalSeprator: UILabel!
    
    @IBOutlet weak var topConstraintForBottomView: NSLayoutConstraint!
    @IBOutlet weak var viewServiceDescriptionSeprator: UIView!
    
    weak var delegate : ConfigureStandardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnActionOnAgreement(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        delegate?.callBackAddAgreementMethod(cell: self)
        
    }
    

}


    

