//
//  newCommercialAgreementCheckListCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 22/10/21.
//

import UIKit

class NewCommercialAgreementCheckListCell: UITableViewCell {
    
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
