//
//  NewCommercialConfigureAddTargetCell.swift
//  Commercial_Flow_Demo
//
//  Created by Saavan Patidar on 18/11/21.
//

import UIKit
import Foundation

class NewCommercialConfigureAddTargetCell: UITableViewCell {
    
    @IBOutlet var lblTarget: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var tblTargetImage: UITableView!
    @IBOutlet var heightForTable: NSLayoutConstraint!
    
    @objc var strLeadId = NSString()
    @objc var strHeaderTitle = NSString()
    var heightForAddTargetImages = CGFloat()
    
    //var arrTargetImage = [ #imageLiteral(resourceName: "attatch_2") , #imageLiteral(resourceName: "orange")]
    var arrayOfImages = NSArray ()
//    var CalcualetedHeight = CGFloat()
//    var tempCellHeight = CGFloat()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tblTargetImage.delegate = self
        self.tblTargetImage.dataSource = self
        self.tblTargetImage.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fetchImageFromDB(strLeadCommercialTargetId : String ,strMobileTargetId : String )  -> NSArray{
        var arrayOfImages = NSArray ()
        
        strHeaderTitle = "Target"
        print("strLeadId")
        print(strLeadId)
        if strLeadCommercialTargetId.count > 0
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && leadCommercialTargetId = %@", strLeadId, strHeaderTitle, strLeadCommercialTargetId))
        }
        else
        {
            arrayOfImages = getDataFromCoreDataBaseArray(strEntity: "TargetImageDetail", predicate: NSPredicate(format: "leadId == %@ && leadImageType == %@ && mobileTargetId == %@", strLeadId, strHeaderTitle, strMobileTargetId))
        }
        
        return arrayOfImages
    }

}

extension NewCommercialConfigureAddTargetCell : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCommericialConfigureAddTargetImageCell", for: indexPath) as?
        NewCommericialConfigureAddTargetImageCell
        
       
        let objTemp = arrayOfImages[indexPath.row] as! NSManagedObject
        
        // Caption Attributed String :
        
        let captionString = (objTemp.value(forKey: "leadImageCaption") as! String).count>0 ? "Caption :" + (objTemp.value(forKey: "leadImageCaption") as! String) : "Caption :"
        
        let stringToChange = "Caption :"
        
        let range = (captionString as NSString).range(of: stringToChange)

        let mutableAttributedString = NSMutableAttributedString.init(string: captionString)
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: range)
        
        if DeviceType.IS_IPAD {
            mutableAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: range)
        }else{
            mutableAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 12), range: range)
        }
        
        cell?.lblCaption.attributedText = mutableAttributedString
        
        // Description Attributed String :
        
        let descriptionString = (objTemp.value(forKey: "descriptionImageDetail") as! String).count>0 ? "Description :" + (objTemp.value(forKey: "descriptionImageDetail") as! String) : "Description :"
        
        let stringToDescription = "Description :"
        
        let rangeToDescription = (descriptionString as NSString).range(of: stringToDescription)

        let mutableDescriptionAttributedString = NSMutableAttributedString.init(string: descriptionString)
        mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: rangeToDescription)
        
        if DeviceType.IS_IPAD {
            mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: range)
        }else{
            mutableDescriptionAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 12), range: range)
        }
        
        cell?.lblDescription.attributedText = mutableDescriptionAttributedString
        
            let strImagePath = objTemp.value(forKey: "leadImagePath") as! String

            let image: UIImage?  = GetImageFromDocumentDirectory(strFileName: strImagePath)

            let isImageExists: Bool? = checkIfImageExistAtPath(strFileName: strImagePath)

            if isImageExists!  {

                cell?.imgTarget.image = image


            }else {

                let defsLogindDetail = UserDefaults.standard

                let dictLoginData = defsLogindDetail.value(forKey: "LoginDetails") as! NSDictionary

                var strURL = String()

                if let value = dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")
                {

                    strURL = "\(value)"

                }
                //strURL = strURL + "\(strImagePath)"

                strURL = strURL + "Documents/Targetimages/" + "\(strImagePath)"

                strURL = strURL.replacingOccurrences(of: "\\", with: "//", options: NSString.CompareOptions.literal, range: nil)

                let image: UIImage = UIImage(named: "NoImage.jpg")!

                cell?.imgTarget.image = image

                DispatchQueue.global(qos: .background).async {

                    let url = URL(string:strURL)
                    let data = try? Data(contentsOf: url!)

                    if data != nil && (data?.count)! > 0 {

                        let image: UIImage = UIImage(data: data!)!

                        DispatchQueue.main.async {

                            saveImageDocumentDirectory(strFileName: strImagePath , image: image)

                            cell?.imgTarget.image = image


                        }}

                }
            }
        
//        print("Caption height+++++++++++++")
//        print(Global().height(ofAttrbuitedText: cell?.lblCaption.attributedText, width: cell?.lblCaption.frame.size.width ?? 0))
//
//        print("Description height+++++++++++++")
//        print(Global().height(ofAttrbuitedText: cell?.lblDescription.attributedText, width: cell?.lblDescription.frame.size.width ?? 0))
//
//        //print(cell?.lblDescription.text?.heightForString(constraintedWidth: (cell?.lblDescription.frame.size.width)!, font: UIFont.systemFont(ofSize: 16)))
//
//        let h1 = Global().height(ofAttrbuitedText: cell?.lblCaption.attributedText, width: cell?.lblCaption.frame.size.width ?? 0)
//        let h2 = Global().height(ofAttrbuitedText: cell?.lblDescription.attributedText, width: cell?.lblDescription.frame.size.width ?? 0)
//
//        CalcualetedHeight = CGFloat(h1 + h2) + 130 + CalcualetedHeight
//        tempCellHeight = CGFloat(h1 + h2) + 130
//        print("Final Calucated height of innerrrrrrr cell _________ \(CalcualetedHeight)")
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if tableView.visibleCells.contains(cell) {
                self.heightForTable?.constant = self.tblTargetImage.contentSize.height
            }
        }
    }
    
}


class NewCommericialConfigureAddTargetImageCell : UITableViewCell{
    
    @IBOutlet var imgTarget: UIImageView!
    @IBOutlet var lblCaption: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
}
