//
//  NewCommericialDiscountCell.swift
//  DPS
//
//  Created by Saavan Patidar on 31/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class NewCommericialDiscountCell: UITableViewCell {
    
    
    @IBOutlet var lblSundayPaper: UILabel!
    @IBOutlet var lblFriends: UILabel!
    @IBOutlet var lblMilitary: UILabel!
    @IBOutlet var lblSundayPaperValue: UILabel!
    @IBOutlet var lblFriendsValue: UILabel!
    @IBOutlet var lblMilitaryValue: UILabel!
    @IBOutlet var lblCardOnFile: UILabel!
    @IBOutlet var lblBundelCredit: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
