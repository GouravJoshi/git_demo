//
//  ProposalCoverCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 21/10/21.
//

import UIKit

class NewCommercialProposalCoverCell: UITableViewCell {
    
    //MARK:- TABLE CELL OUTLET
    
    
    @IBOutlet var stackCoverLetter: UIStackView!
    @IBOutlet var btnDescription: UIButton!
    @IBOutlet var txtViewDescription: UITextView!
    @IBOutlet var stackMarketingContent: UIStackView!
    @IBOutlet var stackIntroduction: UIStackView!
    @IBOutlet var txtCoverLetter: ACFloatingTextField!
    
    @IBOutlet var txtIntroductionLetter: ACFloatingTextField!
    
   // @IBOutlet var heightForIntroductionLetter: NSLayoutConstraint!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        insideImageTextField(textField: txtCoverLetter)
        insideImageTextField(textField: txtIntroductionLetter)
        
    }
    
    func insideImageTextField(textField : ACFloatingTextField){
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "drop_down_ipad");
        imageView.image = image;
        textField.rightView = imageView
        textField.rightViewMode = UITextField.ViewMode.always
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
