//
//  NewCommericialConfigurePricingCell.swift
//  Commercial_Flow_Demo
//
//  Created by Saavan Patidar on 17/11/21.
//

import UIKit

protocol NewCommericialConfigurePricingDelegate : class {
    //NewCommericialConfigurePricingCell Method
    func callBackSelectTaxCode(cell: NewCommericialConfigurePricingCell)
    func callBackAddApplyCoupon(cell: NewCommericialConfigurePricingCell, couponCode: String)
    func callBackSelectCredit(cell: NewCommericialConfigurePricingCell)
    func callBackAddCredit(cell: NewCommericialConfigurePricingCell, creditCode: String)
    
    func callBackDeleteCouponOrCredit(cell: NewCommericialConfigurePricingCell, objCouponDetails : NSManagedObject)
    
    // By sourabh
    func callBackOtherDiscount(value : Double)
    func callBackTipDiscount(value : Double)
    func callBackLeadInspection(value : Double)
    
}

class NewCommericialConfigurePricingCell: UITableViewCell {
    weak var delegate : NewCommericialConfigurePricingDelegate?
    
    @IBOutlet var txtTaxCode: UITextField!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var txtCredit: UITextField!
    @IBOutlet var btnTaxCode: UIButton!
    @IBOutlet var lblSubTotal: UILabel!
    @IBOutlet var lblCouponDiscount: UILabel!
    @IBOutlet var lblCredit: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var lblTaxAmount: UILabel!
    @IBOutlet var lblBillingAmount: UILabel!
    @IBOutlet var lblSubTotalAmount: UILabel!
    @IBOutlet var lblMaintenanceCredit: UILabel!
    @IBOutlet var lblMaintenanceTotalPrice: UILabel!
    @IBOutlet var lblMaintenanceTaxAmount: UILabel!
    @IBOutlet var lblTotalDueAmount: UILabel!
    @IBOutlet var btnApplyCoupon: UIButton!
    @IBOutlet var btnAddCredit: UIButton!
    
    @IBOutlet weak var tblCouponHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCoupon: UITableView!
    var arrAppliedCoupon : [NSManagedObject] = []
    
    @IBOutlet weak var tblCreditHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCredit: UITableView!
    var arrAppliedCredit : [NSManagedObject] = []
    
    @IBOutlet weak var tblMaintFrq: UITableView!
    @IBOutlet weak var tblMaintFrqHeight: NSLayoutConstraint!
    var arrMaintBillingPrice  : [String] = []
    
    // by sourabh
    @IBOutlet var txtOtherDiscount: UITextField!
    @IBOutlet var txtTipDiscount: UITextField!
    @IBOutlet var txtLeadInspectionFee: UITextField!
    
    // VARIABLES
    var totalPrice              = 0.0
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnApplyCoupon.layer.cornerRadius = 15
        self.btnApplyCoupon.layer.borderWidth = 1
        self.btnApplyCoupon.layer.borderColor = UIColor.gray.cgColor
        
        self.btnAddCredit.layer.cornerRadius = 15
        self.btnAddCredit.layer.borderWidth = 1
        self.btnAddCredit.layer.borderColor = UIColor.gray.cgColor
        
        // by sourabh
        textFeildDelegateMethods()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func tableSetUp (arrAppliedCoupon : [NSManagedObject]) {
        //Coupon
        if arrAppliedCoupon.count > 0{
            self.arrAppliedCoupon.removeAll()
            
            self.tblCoupon.delegate = self
            self.tblCoupon.dataSource = self
            self.arrAppliedCoupon = arrAppliedCoupon
            self.tblCoupon.reloadData()
            
            self.tblCouponHeight.constant = CGFloat(arrAppliedCoupon.count * 50)
        }
    }
    
    func tableCreditSetUp (arrAppliedCredit : [NSManagedObject]) {
        //Credit
        if arrAppliedCredit.count > 0{
            self.arrAppliedCredit.removeAll()
            
            self.tblCredit.delegate = self
            self.tblCredit.dataSource = self
            self.arrAppliedCredit = arrAppliedCredit
            self.tblCredit.reloadData()
            
            self.tblCreditHeight.constant = CGFloat(arrAppliedCredit.count * 50)
        }
    }
    
    func tableMaintSetUp (arrMaintBillingPrice: [String]) {
        //Credit
        if arrMaintBillingPrice.count > 0{
            self.arrMaintBillingPrice.removeAll()
            
            self.tblMaintFrq.delegate = self
            self.tblMaintFrq.dataSource = self
            self.arrMaintBillingPrice = arrMaintBillingPrice
            self.tblMaintFrq.reloadData()
            
            self.tblMaintFrqHeight.constant = CGFloat(arrMaintBillingPrice.count * 35)
        }
    }
    
    // MARK: functions by sourabh
    // Text Feild Delegate
    func textFeildDelegateMethods(){
        self.txtOtherDiscount.delegate = self
        self.txtTipDiscount.delegate = self
        self.txtLeadInspectionFee.delegate = self
        
    }
    
    
    func checkIfDiscountGreater() -> Bool {
        var isDiscountGreater = false
        if( ((((txtOtherDiscount.text ?? "") as NSString).floatValue + (txtTipDiscount.text! as NSString).floatValue) > (lblSubTotal.text! as NSString).floatValue) )
        {
            print("OtherDisount")
            print(((txtOtherDiscount.text ?? "") as NSString).floatValue)
            
            print("TipDisount")
            print(((txtTipDiscount.text ?? "") as NSString).floatValue)
            
            print("SubTotal")
            print(((lblSubTotal.text ?? "") as NSString).floatValue)
            
            isDiscountGreater = true
        }
        return isDiscountGreater
    }
    
    // MARK: Action
    
    @IBAction func taxCodeAction(_ sender: Any) {
        print("11111222")
        self.delegate?.callBackSelectTaxCode(cell: self)
    }
    
    @IBAction func applyCouponAction(_ sender: Any) {
        print("111113333")
        self.delegate?.callBackAddApplyCoupon(cell: self, couponCode: self.txtCoupon.text ?? "")
    }
    
    @IBAction func selectCreditAction(_ sender: Any) {
        print("11111")
        self.delegate?.callBackSelectCredit(cell: self)
    }
    
    @IBAction func addCreditAction(_ sender: Any) {
        self.delegate?.callBackAddCredit(cell: self, creditCode: self.txtCredit.text ?? "")
    }
}

//MARK:- TablView Delegate for Coupon and Credit
extension NewCommericialConfigurePricingCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCoupon {
            return  self.arrAppliedCoupon.count
        }else if tableView == tblCredit {
            return  self.arrAppliedCredit.count
        }else {
            return self.arrMaintBillingPrice.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Service
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell", for: indexPath as IndexPath) as! ConfigCouponTableViewCell
        
        if tableView == tblCoupon {
            var objCoupon = NSManagedObject()
            objCoupon = self.arrAppliedCoupon[indexPath.row]
            
            
            var strCouponName = ""
            
            strCouponName = "\(objCoupon.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCoupon.value(forKey: "discountCode") ?? "")"
            }
            
            cell.lblCouponCode.text = strCouponName//objCoupon.value(forKey: "name") as? String //discountCode
            cell.lblCouponAmount.text = String(format: "$%.2f", Double("\(objCoupon.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            cell.lblCouponDescription.text = objCoupon.value(forKey: "discountDescription") as? String
            
        }else if tableView == tblCredit {
            var objCredit = NSManagedObject()
            objCredit = self.arrAppliedCredit[indexPath.row]
            
            let strInitialAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedInitialDiscount") ?? "0")") ?? 0.0)
            let strMaintAmount = String(format: "$%.2f", Double("\(objCredit.value(forKey: "appliedMaintDiscount") ?? "0")") ?? 0.0)
            var strForBoth = ""
            var isBoth = false
            if "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForInitial") ?? "0")" == "true" {
                strForBoth = strInitialAmount + " / " + "$0.00"
                isBoth = true
            }else {
                isBoth = false
            }
            if "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" ==  "1" || "\(objCredit.value(forKey: "applicableForMaintenance") ?? "0")" == "true" {
                strForBoth = "$0.00" + " / " + strMaintAmount
                isBoth = true
            }else {
                isBoth = false
            }
            
            if isBoth {
                strForBoth = strInitialAmount + " / " + strMaintAmount
            }
            
            cell.lblCouponAmount.text = strForBoth
            var strCouponName = ""
            
            strCouponName = "\(objCredit.value(forKey: "name") ?? "")"
            
            if strCouponName == ""
            {
                strCouponName = "\(objCredit.value(forKey: "discountCode") ?? "")"
            }
            if strCouponName == ""{
                strCouponName = getCreditNameBySysName(strSysName: "\(objCredit.value(forKey: "discountSysName") ?? "")")
            }
            cell.lblCouponCode.text = strCouponName//"\(objCredit.value(forKey: "name") ?? "")"
            cell.lblCouponDescription.text = objCredit.value(forKey: "discountDescription") as? String
        }else {
            //self.arrMaintBillingPrice
            cell.lblMaintPrice.text = self.arrMaintBillingPrice[indexPath.row]
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            if tableView == tblCoupon {
                //Coupon
                var objCoupon = NSManagedObject()
                objCoupon = self.arrAppliedCoupon[indexPath.row]
                delegate?.callBackDeleteCouponOrCredit(cell: self, objCouponDetails: objCoupon)
                
            }else if tableView == tblCredit {
                //Credit
                var objCredit = NSManagedObject()
                objCredit = self.arrAppliedCredit[indexPath.row]
                delegate?.callBackDeleteCouponOrCredit(cell: self, objCouponDetails: objCredit)
                
            }else {
                
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tblCoupon{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCouponHeight?.constant = self.tblCoupon.contentSize.height
                }
            }
        }
        if tableView == tblCredit{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if tableView.visibleCells.contains(cell) {
                    self.tblCreditHeight?.constant = self.tblCredit.contentSize.height
                }
            }
        }
    }
    
    func getCreditNameBySysName(strSysName : String) -> String{
        
        var strName = ""
        let dictMasters = nsud.value(forKey: "MasterSalesAutomation") as! NSDictionary
        
        if  dictMasters.value(forKey: "DiscountSetupMasterCredit") is [[String: Any]]{
            
            let arrDiscountCredit = dictMasters.value(forKey: "DiscountSetupMasterCredit") as? [[String:Any]]
            
            if (arrDiscountCredit?.count ?? 0) > 0
            {
                for i in 0..<arrDiscountCredit!.count {
                    let strSysNameDiscount = arrDiscountCredit![i]["SysName"] as? String ?? ""
                    if strSysNameDiscount.lowercased() == strSysName.lowercased(){
                        strName = arrDiscountCredit?[i]["Name"] as? String ?? ""
                    }
                    
                }
            }
        }
        
        return strName
    }
}

// By SOurabh
// Extension

// MARK: --------------------- Text Field Delegate --------------

extension NewCommericialConfigurePricingCell : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let amt = ((textField.text)!.replacingOccurrences(of: "$", with: "") as NSString).doubleValue
        if textField == txtLeadInspectionFee {
            totalPrice = (totalPrice - amt)
        }else {
            totalPrice = (totalPrice + amt)
        }
        
        let strTxt = textField.text ?? ""
        textField.text = strTxt.replacingOccurrences(of: "$", with: "")
        
        if textField.text == "0.00" {
            textField.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if string != numberFiltered {
            return false
        }else {
            let strTxt = textField.text ?? ""
            let countdots = strTxt.components(separatedBy: ".").count - 1
            if countdots > 0 && string == "."
            {
                return false
            }
        }
        
        if ( textField == txtOtherDiscount )
        {
            
            var strDiscount = "\(txtOtherDiscount.text ?? "")"
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    strDiscount.removeLast()
                }else {
//                    var ch :Character = "\(string)"
                    let chars = [Character](string)
                    let ch :Character = chars[0]

                    let i = strDiscount.index(strDiscount.startIndex, offsetBy: range.location)
                    strDiscount.insert(ch, at: i)
//                    strDiscount = strDiscount + string
                }
            }
            
            let  otherDiscount = (strDiscount.replacingOccurrences(of: "$", with: "") as NSString).doubleValue
            
            if otherDiscount > totalPrice {
                return false
            }else{
                print(otherDiscount)
                let finalAmt = (totalPrice - otherDiscount)
                print(finalAmt)
                
                self.lblTotalPrice.text = String(format: "$%.2f", finalAmt)
                return true
            }
            
        }
        
        
        else if ( textField == txtTipDiscount  )
        {
            var strTipDiscount = "\(txtTipDiscount.text ?? "")"
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    strTipDiscount.removeLast()
                }else {
                    let chars = [Character](string)
                    let ch :Character = chars[0]
                    
                    let i = strTipDiscount.index(strTipDiscount.startIndex, offsetBy: range.location)
                    strTipDiscount.insert(ch, at: i)

                   // strTipDiscount = strTipDiscount + string
                }
            }
            let tipDiscount = (strTipDiscount.replacingOccurrences(of: "$", with: "") as NSString).doubleValue
            
            if tipDiscount > totalPrice {
                return false
            }else{
                print(tipDiscount)
                let finalAmt = (totalPrice - tipDiscount)
                print(finalAmt)
                
                self.lblTotalPrice.text = String(format: "$%.2f", finalAmt)
                return true
            }
            
        }
        else
        {
            var strLeadInspectionFee = "\(txtLeadInspectionFee.text ?? "")"
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    strLeadInspectionFee.removeLast()
                }else {
                    let chars = [Character](string)
                    let ch :Character = chars[0]
                    
                    let i = strLeadInspectionFee.index(strLeadInspectionFee.startIndex, offsetBy: range.location)
                    strLeadInspectionFee.insert(ch, at: i)
//                    strLeadInspectionFee = strLeadInspectionFee + string
                }
            }
            
            let leadInspectionFee = (strLeadInspectionFee.replacingOccurrences(of: "$", with: "") as NSString).doubleValue
            
            
            //            if tipDiscount + otherDiscount > totalPrice {
            //                return false
            //            }else{
            print(leadInspectionFee)
            let finalAmt = (totalPrice + leadInspectionFee)
            print(finalAmt)
            
            self.lblTotalPrice.text = String(format: "$%.2f", finalAmt)
            //                return true
            //            }
            
            return true
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //textField code

        textField.resignFirstResponder()  //if desired
        performAction(textField: textField)
        return true
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        performAction(textField: textField)
    }
    
    func performAction(textField: UITextField) {
        
        if(textField == txtOtherDiscount)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = ((textField.text!.replacingOccurrences(of: "$", with: "") as NSString)).doubleValue
                delegate?.callBackOtherDiscount(value: amt)
            }
            else
            {
                textField.text = "$" + "0.00"
                delegate?.callBackOtherDiscount(value: 0.0)
            }
            
        }
        if(textField == txtTipDiscount)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = ((textField.text!.replacingOccurrences(of: "$", with: "") as NSString)).doubleValue
                delegate?.callBackTipDiscount(value: amt)
            }
            else
            {
                textField.text = "$" + "0.00"
                delegate?.callBackTipDiscount(value: 0.0)
            }
            
        }
        if(textField == txtLeadInspectionFee)
        {
            if((textField.text ?? "").count > 0)
            {
                let amt = ((textField.text!.replacingOccurrences(of: "$", with: "") as NSString)).doubleValue
                delegate?.callBackLeadInspection(value: amt)
            }
            else
            {
                textField.text = "$" + "0.00"
                delegate?.callBackLeadInspection(value: 0.0)
            }
        }
    }
}
