//
//  newCommercialAgreementChecklistTableViewCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 22/10/21.
//

import UIKit

protocol CheckAgreementCellDelegate : class {
    //ConfigureServiceTableViewCell Method
    func callBackCheckUncheckMethod(cell: NewCommercialAgreementChecklistTableViewCell)
}



class NewCommercialAgreementChecklistTableViewCell: UITableViewCell {
    
    
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    weak var delegate : CheckAgreementCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAgreeChecklist(_ sender: UIButton) {
        //delegate?.callBackCheckUncheckMethod(cell: self)
    }
    

}


