//
//  termsAndServiceCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 21/10/21.
//

import UIKit

class NewCommercialTermsAndServiceCell: UITableViewCell {
    
    @IBOutlet var stackTermsOfServices: UIStackView!
    @IBOutlet var txtTermsOfService: ACFloatingTextField!
    @IBOutlet var lblTermsOfService: UILabel!
    @IBOutlet var heightTermsService: NSLayoutConstraint!
    @IBOutlet var btnEditTermsOfService: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        insideImageTextField(textField: txtTermsOfService)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func insideImageTextField(textField : ACFloatingTextField){
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "drop_down_ipad");
        imageView.image = image;
        textField.rightView = imageView
        textField.rightViewMode = UITextField.ViewMode.always
        
    }

}
