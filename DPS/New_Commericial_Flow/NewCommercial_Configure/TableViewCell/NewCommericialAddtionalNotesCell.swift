//
//  newCommericialAddtionalNotesCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 25/10/21.
//

import UIKit
import Kingfisher

protocol ConfigureAddtionalNotes : class {
    func callBackMethodAddtional(cell : NewCommericialAddtionalNotesCell)
    func returnTxtValue(data:String)
    
}

class NewCommericialAddtionalNotesCell: UITableViewCell , UITextFieldDelegate {
    
    //@IBOutlet var txtDescription: UITextView!
    
    
    @IBOutlet var lblTxtDescription: UILabel!
    @IBOutlet var btnAgreement: UIButton!
    @IBOutlet var txtNoOfDays: UITextField!
    @IBOutlet var lblDays: UILabel!
    
    var delegate :  ConfigureAddtionalNotes?

    override func awakeFromNib() {
        super.awakeFromNib()
        txtNoOfDays.isHidden = true
        self.txtNoOfDays.delegate = self
        
    }
    
    func setUp(textDayValue : String , isAgreementValid : String){
        if isAgreementValid == "true" || isAgreementValid == "1"{
            btnAgreement.isSelected = true
            txtNoOfDays.text = textDayValue
            txtNoOfDays.isHidden = false
            lblDays.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnActionOnAgreementValid(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackMethodAddtional(cell: self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtNoOfDays {
            let value = txtNoOfDays.text ?? ""
            delegate?.returnTxtValue(data: value)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtNoOfDays {
            let allowedCharacters = "1234567890"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            let Range = range.length + range.location > (txtNoOfDays.text?.count)!
            
            if Range == false && alphabet == false {
                return false
            }
            
            let NewLength = (txtNoOfDays.text?.count)! + string.count - range.length
            return NewLength <= 2
              
        } else {
            return false
        }
    }

}
