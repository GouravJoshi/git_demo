//
//  NewCommercialConfigureCustomCell.swift
//  Commercial_Flow_Demo
//
//  Created by Saavan Patidar on 18/11/21.
//

import UIKit

protocol ConfigureCustomeServicesCellDelegate : class {
    func callBackAddCustomAgreementMethod(cell: NewCommercialConfigureCustomCell)
    func callBackIsTaxableMethod(cell: NewCommercialConfigureCustomCell)
}

class NewCommercialConfigureCustomCell: UITableViewCell {

    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblServiceDescription: UILabel!
    @IBOutlet var lblIntialPrice: UILabel!
    @IBOutlet weak var btnIsTaxable: UIButton!

    @IBOutlet var lblMaintenancePrice: UILabel!
    @IBOutlet var btnAddToAgreement: UIButton!
    @IBOutlet var lblFrequency: UILabel!
    @IBOutlet var lblBillingFrequency: UILabel!
    @IBOutlet var heightForViewServiceDescription: NSLayoutConstraint!
    @IBOutlet var viewDescription: UIView!
    @IBOutlet var heightForViewDescription: NSLayoutConstraint!
    
    @IBOutlet var lblStrMaintenancePrice: UILabel!
    
    @IBOutlet var viewForBottom: NSLayoutConstraint!
    
    @IBOutlet var lblStrheightForMaintenancePrice: NSLayoutConstraint!
    @IBOutlet var lblHeightForMaintenancePrice: NSLayoutConstraint!
    
    weak var delegate : ConfigureCustomeServicesCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func btnActionOnAgreement(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.callBackAddCustomAgreementMethod(cell: self)
    }
    
    @IBAction func isTaxableAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        delegate?.callBackIsTaxableMethod(cell: self)
    }
}


