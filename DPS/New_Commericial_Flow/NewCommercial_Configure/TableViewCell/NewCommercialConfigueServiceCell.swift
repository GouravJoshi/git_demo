//
//  newCommercialConfigueServiceCell.swift
//  Commercial_Flow_Demo
//
//  Created by Akshay Hastekar on 22/10/21.
//

import UIKit

class NewCommercialConfigueServiceCell: UITableViewCell {

    @IBOutlet weak var newCommericialConfigureTableView: UITableView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     //   newCommericialConfigureTableView.dataSource = self
      //  newCommericialConfigureTableView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension NewCommercialConfigueServiceCell : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        } else if section == 1{
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
            return cell 
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // INSIDE LABEL
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: 300, height:50))
        
        let viewheader = UIView.init(frame: CGRect(x: 0, y: 0, width: 250, height:  50))
        viewheader.backgroundColor = UIColor.white
   
        if tableView == newCommericialConfigureTableView
        {
            if(section == 0)
            {
                lbl.text = "Target"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
        
            }
            else if(section == 1)
            {
                lbl.text = "Scope"
                lbl.textColor = #colorLiteral(red: 0.3572379649, green: 0.5993420482, blue: 0.592584908, alpha: 1)
               
            }
           
            lbl.font = UIFont.boldSystemFont(ofSize: 18)
            viewheader.addSubview(lbl)
            
        }
        else
        {
            return viewheader
        }
        return viewheader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
}
