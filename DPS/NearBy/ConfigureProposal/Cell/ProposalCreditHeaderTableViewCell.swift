//
//  ProposalCreditHeaderTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ProposalCreditHeaderTableViewCell: UITableViewCell {
    // MARK: -
    @IBOutlet weak var txtCredit: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var viewBG: UIView! {
        didSet {
            viewBG.addBorders(edges: [UIRectEdge.top,UIRectEdge.right, UIRectEdge.left], color: UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0), thickness: 1)
            
        }
    }
    @IBOutlet weak var viewHeadings: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
