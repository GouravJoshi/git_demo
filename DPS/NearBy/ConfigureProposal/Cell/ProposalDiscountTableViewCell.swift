//
//  ProposalDiscountTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 29/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol ProposalDiscountDelegate: NSObjectProtocol {
    func actionRemove(_ cell: ProposalDiscountTableViewCell)
}
class ProposalDiscountTableViewCell: UITableViewCell {
    
    // MARK: - Outlet
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnRemove: UIButton!{
        didSet{
            AppUtility.buttonImageColor(btn: btnRemove, image: "Group 771", color: .appThemeColor)
        }
    }
    @IBOutlet weak var viewBG: UIView! {
        didSet {
            viewBG.addBorders(edges: [UIRectEdge.bottom, UIRectEdge.right, UIRectEdge.left], color: UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0), thickness: 1)
        }
    }
    
    // MARK: - Variables
    weak var delegate: ProposalDiscountDelegate?
    var discount: DiscountMaster?  {
        didSet {
            guard let discount = discount else { return }
            lblCouponCode.text = discount.name
            lblDescription.text = discount.description
        }
    }
    // MARK: -  Actions
    @IBAction func actionRemove(_ sender: Any) {
        delegate?.actionRemove(self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
