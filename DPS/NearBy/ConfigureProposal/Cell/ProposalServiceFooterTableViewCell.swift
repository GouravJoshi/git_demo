//
//  ProposalServiceFooterTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ProposalServiceFooterTableViewCell: UITableViewCell {
    
    // MARK: - Outlet
    @IBOutlet weak var lblTotalInitialPrice: UILabel!
    @IBOutlet weak var lblTotalMaintenancePrice: UILabel!
    @IBOutlet weak var viewTax: UIView!
    @IBOutlet weak var txtTax: UITextField!
    // MARK: -  Variables
    /*var services: [Service] = [] {
     didSet {
     lblTotalInitialPrice.text = services.compactMap({$0.manualInitialPrice}).sum().currency
     lblTotalMaintenancePrice.text = services.compactMap({$0.manualMaintenancePrice}).sum().currency
     }
     }*/
    var services: [SoldServiceStandardDetail] = [] {
        didSet {
            lblTotalInitialPrice.text = services.compactMap({$0.totalInitialPrice}).sum().currency
            lblTotalMaintenancePrice.text = services.compactMap({$0.totalMaintPrice}).sum().currency
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
