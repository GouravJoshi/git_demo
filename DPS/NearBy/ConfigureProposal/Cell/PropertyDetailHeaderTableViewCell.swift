//
//  PropertyDetailHeaderTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class PropertyDetailHeaderTableViewCell: UITableViewCell {
    
    // MARK: - Outlet
    @IBOutlet weak var imgViewProperty: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var stackName: UIStackView!
    @IBOutlet weak var stackAddress: UIStackView!
    @IBOutlet weak var stackPhone: UIStackView!
    @IBOutlet weak var stackEmail: UIStackView!
    @IBOutlet weak var imgaAddress: UIImageView!{
        didSet{
            AppUtility.imageColor(image: imgaAddress, color: .appDarkGreen)
        }
    }
    @IBOutlet weak var imgPhone: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
            
        }
    }
    @IBOutlet weak var imageEmail: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imageEmail, color: .appDarkGreen)
            
        }
    }
    @IBOutlet weak var imgName: UIImageView! {
        didSet {
            AppUtility.imageColor(image: imgName, color: .appDarkGreen)
            
        }
    }
    // MARK: - Variables
    var propertyInfo: PropertyInfo? {
        didSet {
            guard let propertyInfo = propertyInfo else { return }
            lblName.text = propertyInfo.name?.trimmed
            lblPhone.text = propertyInfo.phone?.trimmed
            lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
            lblEmail.text = propertyInfo.email?.trimmed
            lblAddress.text = propertyInfo.address?.trimmed
            stackName.isHidden = lblName.text?.trimmed == "" || lblName.text == nil ? true : false
            stackEmail.isHidden = lblEmail.text?.trimmed == "" || lblEmail.text == nil ? true : false
            stackPhone.isHidden = lblPhone.text?.trimmed == "" || lblPhone.text == nil ?  true : false
            stackAddress.isHidden = lblAddress.text?.trimmed == "" || lblAddress.text == nil ? true : false
            if let image = propertyInfo.imageManual {
                imgViewProperty.image = image
            } else {
                guard let imgUrl = propertyInfo.imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl) else {
                    return
                }
                imgViewProperty.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details")?.maskWithColor(color: .appThemeColor))
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
