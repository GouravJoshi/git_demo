//
//  BillingAddressTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 17/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import DropDown
import GooglePlaces

protocol BillingAddressDelegate: NSObjectProtocol {
    func actionBillingAddress(_ isSelected: Bool)
    func billingDetailEntered(_ billingAddress: SendAgreement)
    func addAddressPressed()
}

final class BillingAddressTableViewCell: UITableViewCell {
    var delegate: BillingAddressDelegate?
    var billingAddress: SendAgreement?
    private var places: [GMSAutocompletePrediction] = [] {
        didSet { openAddressDropDown(self.txtAddress) }
    }
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    let dropDown = DropDown()
    var entityDetail: EntityDetail?
    var propertyDetails: PropertyDetails?
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgLastName: UIImageView!
    @IBOutlet weak var imgFirstName: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    // MARK: Function to set UI
    func setUI() {
        viewAddress?.isHidden = btnBillingAddress.isSelected
        txtFirstName.setLeftPaddingPoints(30)
        txtLastName.setLeftPaddingPoints(30)
        txtEmail.setLeftPaddingPoints(30)
        txtPhone.setLeftPaddingPoints(30)
        AppUtility.imageColor(image: imgFirstName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgLastName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgEmail, color: .appDarkGreen)
        AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnAddress, image: "Group 771", color: .gray)
    }
    func setData() {
        if let billingAddress = billingAddress, let billingAddressSameAsService = billingAddress.billingAddressSameAsService {
            txtAddress.text = billingAddress.tempFullBillingAddress
            btnBillingAddress.isSelected = billingAddressSameAsService
            txtFirstName.text = billingAddress.billingFirstName
            txtLastName.text = billingAddress.billingLastName
            txtEmail.text = billingAddress.billingPrimaryEmail
            txtPhone.text = billingAddress.billingCellPhone1
        } else  if let entityDetail = entityDetail , let leadDetail = entityDetail.leadDetail {
            var billingAddress = SendAgreement()
            txtFirstName.text = leadDetail.billingFirstName
            txtLastName.text = leadDetail.billingLastName
            txtEmail.text = leadDetail.billingPrimaryEmail
            txtPhone.text = leadDetail.billingPrimaryPhone//leadDetail.billingCellNo
            btnBillingAddress.isSelected = entityDetail.leadDetail?.isBillingAddressSame ?? true
            var stateName = ""
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateSysName == leadDetail.billingState
            }) {
                stateName = state.stateSysName ?? ""
            }
            let address = "\(leadDetail.billingAddress1 ?? ""), \(leadDetail.billingCity ?? ""), \(stateName), \(leadDetail.billingZipcode ?? "")"
            txtAddress.text = address
            billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
            billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
            billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
            billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
            billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
            billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
            delegate?.billingDetailEntered(billingAddress)
        } else if let propertyDetails = propertyDetails {
            var billingAddress = SendAgreement()
            switch propertyDetails.propertyType {
            case .lead:
                if let lead = propertyDetails.lead {
                    btnBillingAddress.isSelected = true
                    billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
                    var stateName = ""
                    if let stateID = lead.stateID {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        
                        let address = "\(lead.address1 ?? ""), \(lead.cityName ?? ""), \(stateName), \(lead.zipcode ?? "")"
                        txtAddress.text = address
                }
                }
            case .empty:
                btnBillingAddress.isSelected = true
                if let propertyInfo = propertyDetails.propertyInfo {
                txtAddress.text = propertyInfo.address
                }
                billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
            case .opportunity:
                if let opportunity = propertyDetails.opportunity {
                    
                    btnBillingAddress.isSelected = opportunity.billingAddressSameAsService ?? true
                    var stateName = ""
                    if let billingAddressOld = opportunity.billingAddress {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == billingAddressOld.stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        
                        let address = "\(billingAddressOld.address1 ?? ""), \(billingAddressOld.cityName ?? ""), \(stateName), \(billingAddressOld.zipcode ?? "")"
                        txtAddress.text = address
                    }
                }
            case .account:
                if let account = propertyDetails.account {
                   // btnBillingAddress.isSelected = account.billingAddressSameAsService ?? true
                    var stateName = ""
                    if let billingAddressOld = account.billingAddress {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == billingAddressOld.stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        
                        let address = "\(billingAddressOld.address1 ?? ""), \(billingAddressOld.cityName ?? ""), \(stateName), \(billingAddressOld.zipcode ?? "")"
                        txtAddress.text = address
                    }
            }
          
            delegate?.billingDetailEntered(billingAddress)
        }
            billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
            billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
            billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
            billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
            billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
            billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
    }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func actionAddAdress(_ sender: Any) {
        delegate?.addAddressPressed()
        
    }
    @IBAction func actionCheckMark(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        var isSelected = true
        if sender.isSelected {
            viewAddress?.isHidden = true
            isSelected = true
        } else {
            viewAddress?.isHidden = false
            isSelected = false
        }
        delegate?.actionBillingAddress(isSelected)
    }
}
extension BillingAddressTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtPhone {
                let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
                if(isValidd){
                    var strTextt = txtPhone.text!
                    strTextt = String(strTextt.dropLast())
                    txtPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
                }
                return isValidd
                
            }
            else if textField == txtAddress {
                if  let text = txtAddress!.text, !text.isEmpty , !updatedText.isEmpty {
                    self.placeAutocomplete(text)
                } else {
                    dropDown.hide()
                }
            }
            var billingAddress = SendAgreement()
            billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
            billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
            billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
            billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
            billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
            billingAddress.billingAddressSameAsService = btnAddress.isSelected
            delegate?.billingDetailEntered(billingAddress)
        }
        return true
    }
}

extension  BillingAddressTableViewCell {
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    // MARK:- Auto Complete
    private func placeAutocomplete(_ text: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(text, bounds: nil, filter: nil) { (results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.places = results
            }
        }
    }
    // MARK:- To Get PlaceID From The Address
    private func getPlaceId(address:String) {
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        guard let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=false&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            // Spinner.shared.hide()
            if let data = data {
                do {
                    let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                    guard let loactionDetails = object.results.first else {
                        return
                    }
                    if let placeId = loactionDetails.placeId {
                        self.getPlaceDetails(id: placeId, apiKey: self.apiKey) { [unowned self] in
                            guard let value = $0 else { return }
                            self.txtAddress.text = self.addressFormattedByGoogle(value)
                            FTIndicator.dismissProgress()
                        }
                    }
                } catch  {
                    FTIndicator.dismissProgress()
                    self.txtAddress.text = ""
                }
            } else {
                FTIndicator.dismissProgress()
                self.txtAddress.text = ""
            }
        }.resume()
    }
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
    func openAddressDropDown(_ sender: UITextField) {
        
        if  self.places.count > 0 {
            var rows:[String] = []
            for (_,row) in places.enumerated() {
                rows.append(row.attributedFullText.string)
            }
            dropDown.dataSource = rows
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                guard let self = self else { return }
                let address = self.places[index].attributedFullText.string
                self.getPlaceId(address: address)
                // sender.setTitle(item, for: .normal)
            }
        } else {
            dropDown.hide()
        }
        
    }
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:]))
                FTIndicator.dismissProgress()
            }
        )
    }
    func addressFormattedByGoogle(_ value : PlaceDetails) -> String {
        var strAddress = ""
        if(value.name != nil) {
            strAddress = "\(value.name!)"
        }
        if(value.locality != nil){
            strAddress = strAddress + ",\(value.locality!)"
        }
        
        if(value.administrativeAreaCode != nil) {
            
            let administrativeAreaCode = "\(value.administrativeAreaCode!)"
            
            let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(administrativeAreaCode)")
            
            if dictStateDataTemp.count > 0 {
                strAddress = strAddress + ",\(dictStateDataTemp.value(forKey: "Name")!)"
            }
        }
        if(value.postalCode != nil) {
            strAddress = strAddress + ",\(value.postalCode!)"
        }
        return strAddress
    }
}
