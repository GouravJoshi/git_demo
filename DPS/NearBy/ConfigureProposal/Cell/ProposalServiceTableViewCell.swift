//
//  ProposalServiceTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ProposalServiceTableViewCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblInitialAmount: UILabel!
    @IBOutlet weak var lblMaintenancePrice: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    
    // MARK: - Variable
    var service: SoldServiceStandardDetail? {
        didSet {
            guard let service = service else { return }
            
            if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == service.serviceSysName}.first?.name
                lblServiceName.text = name
            } else {
                lblServiceName.text = service.serviceSysName
            }
            lblInitialAmount.text = service.totalInitialPrice?.currency ?? "$0.00"
            lblMaintenancePrice.text = service.totalMaintPrice?.currency ?? "$0.00"
            lblFrequency.text = service.serviceFrequency?.trimmed
        }
    }
    /*
    var service: Service? {
        didSet {
            guard let service = service else { return }
            lblServiceName.text = service.name
            lblInitialAmount.text = service.manualInitialPrice?.currency ?? "$0.00"
            lblMaintenancePrice.text = service.manualMaintenancePrice?.currency ?? "$0.00"
            
            if let objects = AppUserDefaults.shared.masterDetails?.frequencies, let object = objects.filter({$0.sysName == service.defaultServiceFrequencySysName}).first {
                lblFrequency.text = object.frequencyName
            } else {
                lblFrequency.text = nil
            }
        }
    }*/
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
