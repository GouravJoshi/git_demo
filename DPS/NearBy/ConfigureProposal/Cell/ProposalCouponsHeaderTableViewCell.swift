//
//  ProposalCouponsHeaderTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class ProposalCouponsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBG: UIView! {
        didSet {
            viewBG.clipsToBounds = false
            //viewBG.layer.cornerRadius = 10
            //viewBG.layer.addBorder(side: CALayer.BorderSide.notLeft, thickness: 1, color: UIColor.lightGray.cgColor, maskedCorners: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
            //viewBG.layer.addBorder(side: CALayer.BorderSide.notBottom, thickness: 1, color: UIColor.lightGray.cgColor, maskedCorners: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
//            viewBG.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            viewBG.addBorders(edges: [UIRectEdge.top,UIRectEdge.right, UIRectEdge.left], color: UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0), thickness: 1)
//            viewBG.addCorner(.topRight, thickness: 1, color: UIColor.lightGray.cgColor, cornerRadius: 10)
//            viewBG.addCorner(.topLeft, thickness: 1, color: UIColor.lightGray.cgColor, cornerRadius: 10)

        }
    }
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var viewHeadings: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension UIView {
    @discardableResult
    func addBorders(edges: [UIRectEdge],
                    color: UIColor,
                    inset: CGFloat = 0.0,
                    thickness: CGFloat = 1.0) -> [UIView] {
        
        var borders = [UIView]()
        
        @discardableResult
        func addBorder(formats: String...) -> UIView {
            let border = UIView(frame: .zero)
            border.backgroundColor = color
            border.clipsToBounds = true
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            addConstraints(formats.flatMap {
                            NSLayoutConstraint.constraints(withVisualFormat: $0,
                                                           options: [],
                                                           metrics: ["inset": inset, "thickness": thickness],
                                                           views: ["border": border]) })
            borders.append(border)
            return border
        }
        
        
        if edges.contains(.top) || edges.contains(.all) {
            addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]-inset-|")
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]-inset-|")
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:|-0-[border(==thickness)]")
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            addBorder(formats: "V:|-inset-[border]-inset-|", "H:[border(==thickness)]-0-|")
        }
        
        return borders
    }
    
    enum Corner {
        case topLeft
        case topRight
        case bottomLeft
        case bottomRight
    }
   func addCorner(_ corner: Corner, thickness: CGFloat, color: CGColor, cornerRadius: CGFloat) {
        // Set default to top left
        let width = frame.size.width; let height = frame.size.height
        var x = cornerRadius
        var startAngle: CGFloat = .pi; var endAngle: CGFloat = .pi*3/2
        
        switch corner {
        case .bottomLeft: startAngle = .pi/2; endAngle = .pi
            
        case .bottomRight:
            x = width - cornerRadius
            startAngle = 0; endAngle = .pi/2
            
        case .topRight:
            x = width - cornerRadius
            startAngle = .pi*3/2; endAngle = 0
            
        default: break
        }
        
        let cornerPath = UIBezierPath(arcCenter: CGPoint(x: x, y: height / 2),
                                      radius: cornerRadius - thickness,
                                      startAngle: startAngle,
                                      endAngle: endAngle,
                                      clockwise: true)
        
        let cornerShape = CAShapeLayer()
        cornerShape.path = cornerPath.cgPath
        cornerShape.lineWidth = thickness
        cornerShape.strokeColor = color
        cornerShape.fillColor = nil
        self.layer.addSublayer(cornerShape)
    }
}

