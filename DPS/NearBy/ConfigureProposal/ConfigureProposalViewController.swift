//
//  ConfigureProposalViewController.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import DropDown
import GooglePlaces
import Kingfisher
protocol ImagesUpdated {
    func imagesUpdates(_ images: [DTDLeadImagesDetailExtSerDc])
}

final class ConfigureProposalViewController: BaseViewController {
    
    private enum Sections: Int, CaseIterable {
        case header = 0
        case service
        // case coupons
        // case credit
        case billingAddress
    }
    
    // MARK: - Variable
    var isEditable = true
    var services: [SoldServiceStandardDetail] = []
    var entityDetail: EntityDetail?
    var customerNotes: String?
    var selectedTax: TaxDetail?
    var propertyDetails: PropertyDetails?
    lazy var couponEntered: String = ""
    var selectedCredit: DiscountMaster?
    lazy var discounts: [DiscountMaster] = []
    var delegateEdit: EditableStatusDelegate?
    var billingAddress = SendAgreement()
    private var isTaxable = false {
        didSet {
            self.reloadTableView()
        }
    }
    
    private lazy var presenter = {
        return ConfiguralProposalPresenter(self)
    }()
    private var taxPercentage: Double?
    var selectedLocation: CLLocation?

    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DropDown.startListeningToKeyboard()
        self.title = "Configure proposal"
        self.perform(#selector(taxValidation), with: nil, afterDelay: 0.0)
    }
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    @IBAction func actionHome(_ sender: Any) {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
    
    @objc func actionOpenEmail() {
        if let email = propertyDetails?.propertyInfo?.email?.trimmed {
            actionEmail(email: email)
        }
        
    }
    @objc func actionOpenCall() {
        if let number = propertyDetails?.propertyInfo?.phone?.trimmed {
            actionCall(number: number)
        }
    }
  
    // MARK: - Action
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionGallery(_ sender: Any) {
        guard let destination = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "GalleryListViewController") as? GalleryListViewController else {
            return
        }
        destination.delegate = self
        destination.images = propertyDetails?.galleryImages ?? []
        self.navigationController?.pushViewController(destination, animated: true)
    }
    @IBAction func actionDecline(_ sender: Any) {
        guard let propertyDetails = propertyDetails else {
            return
        }
        let storyboard =  UIStoryboard.init(name: "Proposal", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "DeclinePopupViewController") as! DeclinePopupViewController
        switch propertyDetails.propertyType {
        case .lead:
            destination.sendAgreement = getLeadAgreement()
            break
        case .account:
            destination.sendAgreement = getAccountAgreement()
            break
        case .opportunity:
            destination.sendAgreement = getOpportunityAgreement()
            break
        case .empty:
            destination.sendAgreement = getNewSendAggrement()
            break
        }
        if billingAddress.billingAddressSameAsService ?? true {
            billingAddress.billingAddressSameAsService = true
        }  else {
            guard let address = billingAddress.tempFullBillingAddress, !address.isEmpty else {
                showAlert(message: "Please enter billing Address")
                return
            }
            let addressComponents =  address.trimmed.replacingOccurrences(of: "  ", with: "").components(separatedBy:",")
            guard addressComponents.count == 4 ||  addressComponents.count == 5  else {
                showAlert(message: "Please enter valid Address")
                return
            }
            guard let states = AppUtility.fetchStates() else {
                showAlert(message: "Unable to find states")
                return
            }
            var administrativeArea = ""
            if addressComponents.count == 4 {
                administrativeArea = addressComponents[2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            } else {
                administrativeArea = addressComponents[3].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            }
            guard let _ = states.filter({ (stateT) -> Bool in
                return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
            }).first else {
                showAlert(message: "We are not serving in this state")
                return
            }
            if let address1 =  addressComponents.first?.trimmed {
                billingAddress.billingAddress1 = String(address1.prefix(100))
            }
            var position = 0
            if addressComponents.count == 4 {
                position = 0
            } else {
                position = 1
            }
            if addressComponents.count >= 2 {
                let city = addressComponents[position+1].trimmed
                billingAddress.billingCityName = city
            }
            if addressComponents.count >= 3 {
                let administrativeArea = addressComponents[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
                if let states = AppUtility.fetchStates()  {
                    if let state = states.filter({ (stateT) -> Bool in
                        return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                    }).first  {
                        billingAddress.billingStateId = state.stateID
                        billingAddress.billingState = state.name
                    } else {
                        showAlert(message: "We are not serving in this state")
                        return
                    }
                }
            }
            if addressComponents.count >= 4 {
                let zipCode = addressComponents[position+3].trimmed
                billingAddress.billingZipcode = zipCode
            }
        }
        destination.delegate = self
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
    }
    @IBAction func actionContinue(_ sender: Any) {
        /*
         guard  !isEditable || !isTaxable || (isTaxable && selectedTax != nil)  else {
         showAlert(message: "Please select a tax")
         return
         }
         if !isEditable {
         pushToAgreement()
         }else if isTaxable, let selectedTax = selectedTax, let sysyName = selectedTax.sysName {
         if (taxPercentage != nil) {
         pushToAgreement(taxPercentage)
         } else {
         presenter.apiCallForTaxtDetail(taxSysName: sysyName)
         }
         } else {
         pushToAgreement()
         }*/
        /*
         if let leadDetail = entityDetail?.leadDetail {
         
         if leadDetail.isServiceAddrTaxExempt == false {
         
         } else {
         
         }
         } */
        if AppUserDefaults.shared.isTaxRequired {
            requiredTax()
        } else {
            nonRequiredTax()
        }
    }
    private func requiredTax() {
        
        if let selectedTax = selectedTax, let sysyName = selectedTax.sysName {
            if !isTaxable {
                proceedValidation()
            } else if (taxPercentage != nil) {
                proceedValidation(taxPercentage)
            } else {
                presenter.apiCallForTaxtDetail(taxSysName: sysyName)
            }
        } else {
            showAlert(message: "Please select a tax")
        }
    }
    private func nonRequiredTax() {
        if !isTaxable {
            proceedValidation()
        } else if let selectedTax = selectedTax, let sysyName = selectedTax.sysName {
            if (taxPercentage != nil) {
                proceedValidation(taxPercentage)
            } else {
                presenter.apiCallForTaxtDetail(taxSysName: sysyName)
            }
        } else {
            proceedValidation()
        }
    }
    private func proceedValidation(_ tax: Double? = nil) {
        if billingAddress.billingAddressSameAsService ?? true {
            billingAddress.billingAddressSameAsService = true
            pushToEmail(tax)
        } else {
            guard let address = billingAddress.tempFullBillingAddress, !address.isEmpty else {
                showAlert(message: "Please enter billing Address")
                return
            }
            let addressComponents =  address.trimmed.replacingOccurrences(of: "  ", with: "").components(separatedBy:",")
            guard addressComponents.count == 4 ||  addressComponents.count == 5  else {
                showAlert(message: "Please enter valid Address")
                return
            }
            guard let states = AppUtility.fetchStates() else {
                showAlert(message: "Unable to find states")
                return
            }
            var administrativeArea = ""
            if addressComponents.count == 4 {
                administrativeArea = addressComponents[2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            } else {
                administrativeArea = addressComponents[3].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            }
            guard let _ = states.filter({ (stateT) -> Bool in
                return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
            }).first else {
                showAlert(message: "We are not serving in this state")
                return
            }
            if let address1 =  addressComponents.first?.trimmed {
                billingAddress.billingAddress1 = String(address1.prefix(100))
            }
            var position = 0
            if addressComponents.count == 4 {
                position = 0
            } else {
                position = 1
            }
            if addressComponents.count >= 2 {
                let city = addressComponents[position+1].trimmed
                billingAddress.billingCityName = city
            }
            if addressComponents.count >= 3 {
                let administrativeArea = addressComponents[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
                if let states = AppUtility.fetchStates()  {
                    if let state = states.filter({ (stateT) -> Bool in
                        return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                    }).first  {
                        billingAddress.billingStateId = state.stateID
                        billingAddress.billingState = state.name
                    } else {
                        showAlert(message: "We are not serving in this state")
                        return
                    }
                }
            }
            if addressComponents.count >= 4 {
                let zipCode = addressComponents[position+3].trimmed
                billingAddress.billingZipcode = zipCode
            }
            
            guard let firstName = billingAddress.billingFirstName, !firstName.isEmpty else {
                showAlert(message: "Please enter first name")
                return
            }
            guard let lastName = billingAddress.billingLastName, !lastName.isEmpty else {
                showAlert(message: "Please enter last name")
                return
            }
            if let email = billingAddress.billingPrimaryEmail {
                guard email.isValidEmail()  else {
                    showAlert(message: "Please enter valid Email address")
                    return
                }
            }
            /* guard let cellPhone = billingAddress.billingCellPhone1, !cellPhone.isEmpty else {
             showAlert(message: "Please enter mobile Number")
             return
             }*/
            
            pushToEmail(tax)
        }
        if propertyDetails?.propertyType == .opportunity {
            billingAddress.billingPOCId = propertyDetails?.opportunity?.billingAddress?.billingPOCId
        }
    }
    private func pushToEmail(_ tax: Double? = nil) {
        guard let destination = UIStoryboard(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "ServiceAgrementViewController") as? ServiceAgrementViewController else {
            return
        }
        destination.taxPercentage = tax
        destination.isEditable = isEditable
        destination.services = services
        destination.entityDetail = entityDetail
        destination.propertyDetails = propertyDetails
        destination.delegateEdit = self
       destination.billingAddress = billingAddress
        destination.customerNotes = customerNotes
        //destination.services = services
        destination.selectedTax = selectedTax
        destination.discounts = discounts
        destination.selectedLocation = self.selectedLocation
        self.navigationController?.pushViewController(destination, animated: true)
    }
    @objc private func taxValidation() {
        if  services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).first != nil {
            if let isServiceAddrTaxExempt = entityDetail?.leadDetail?.isServiceAddrTaxExempt {
                if isServiceAddrTaxExempt == false {
                    isTaxable = true
                } else {
                    isTaxable = false
                }
            } else {
                isTaxable = true
            }
        } else {
            isTaxable = false
        }
    }
}
// MARK: - Table view data source
extension ConfigureProposalViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allCases.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Sections(rawValue: section) {
        case .header, .billingAddress:
            return 1
        case .service:
            return services.count
        case .none:
            return 0
        /* case .some(.coupons):
         return discounts.filter({$0.type == .coupon}).count
         case .some(.credit):
         return discounts.filter({$0.type == .credit}).count */
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)
        switch section {
        case .header:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PropertyDetailHeaderTableViewCell.self), for: indexPath) as! PropertyDetailHeaderTableViewCell
            cell.propertyInfo = propertyDetails?.propertyInfo
            let tap = UITapGestureRecognizer(target: self, action: #selector(actionOpenCall))
            cell.stackPhone.addGestureRecognizer(tap)
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(actionOpenEmail))
            cell.stackEmail.addGestureRecognizer(emailTap)
            
            return cell
        case .service:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalServiceTableViewCell.self), for: indexPath) as! ProposalServiceTableViewCell
            cell.service = services[indexPath.row]
            
            return cell
       /* case .some(.coupons) , .some(.credit):
         let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalDiscountTableViewCell.self), for: indexPath) as! ProposalDiscountTableViewCell
         if section == .coupons {
         let discount = self.discounts.filter({$0.type == .coupon})[indexPath.row]
         cell.discount = discount
         cell.lblAmount.text = "\(calcualateIntialDiscount(discount).currency)"
         
         } else {
         let discount =  self.discounts.filter({$0.type == .credit})[indexPath.row]
         cell.discount = discount
         cell.lblAmount.text = "\(calcualateIntialDiscount(discount).currency)/\(calcualateMaintenanceDiscount(discount).currency)"
         }
         cell.delegate = self
         return cell*/
        case .billingAddress:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BillingAddressTableViewCell.self), for: indexPath) as! BillingAddressTableViewCell
            cell.delegate = self
            cell.propertyDetails = propertyDetails
            cell.entityDetail = entityDetail
            cell.billingAddress = billingAddress
            cell.setUI()
            cell.setData()
            cell.isUserInteractionEnabled = isEditable
            return cell
        case .none:
            return UITableViewCell()
        }
    }
    
}
// MARK: - Table view delegate
extension ConfigureProposalViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch Sections(rawValue: section) {
        case .service:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalServiceHeaderTableViewCell.self)) as! ProposalServiceHeaderTableViewCell
            return cell.contentView
        /* case .some(.coupons):
         let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalCouponsHeaderTableViewCell.self)) as! ProposalCouponsHeaderTableViewCell
         cell.txtCoupon.tag = section
         cell.txtCoupon.text = couponEntered
         cell.txtCoupon.delegate = self
         cell.viewHeadings.isHidden = self.discounts.filter({$0.type == .coupon}).isEmpty
         cell.btnApply.addTarget(self, action: #selector(actionApplyCoupon), for: .touchUpInside)
         return cell.contentView
         case .some(.credit):
         
         let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalCreditHeaderTableViewCell.self)) as! ProposalCreditHeaderTableViewCell
         cell.txtCredit.text = selectedCredit?.name
         cell.txtCredit.tag = section
         cell.txtCredit.delegate = self
         cell.viewHeadings.isHidden = self.discounts.filter({$0.type == .credit}).isEmpty
         cell.btnApply.addTarget(self, action: #selector(actionApplyCredit), for: .touchUpInside)
         return cell.contentView */
        default:
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch Sections(rawValue: section) {
        case .service:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProposalServiceFooterTableViewCell.self)) as! ProposalServiceFooterTableViewCell
            cell.services = services
            if let previousTaxType = entityDetail?.leadDetail?.taxSysName?.trimmed, !previousTaxType.isEmpty {
                selectedTax = AppUserDefaults.shared.masterDetails?.taxMaster.filter({$0.sysName == previousTaxType}).first
            }
            cell.txtTax.tag = section
            cell.txtTax.text = selectedTax?.name
            cell.txtTax.delegate = self
            //cell.viewTax.isHidden = !isTaxable
            cell.txtTax.isUserInteractionEnabled = isEditable
            
            return cell.contentView
        default:
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Sections(rawValue: section) {
        case .service://, .coupons, .credit:
            return UITableView.automaticDimension
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch Sections(rawValue: section) {
        case .service:
            return UITableView.automaticDimension
        /*case .credit, .coupons:
         return 10*/
        default:
            return 1
        }
    }
}

// MARK: - Convenience
extension ConfigureProposalViewController {
    
    private func calcualateIntialDiscount(_ discountRow: DiscountMaster) -> Double {
        
        let amount = services.compactMap({$0.totalInitialPrice}).sum()
        var discountAmount = amount
        guard let row = discounts.firstIndex(of: discountRow) else { return 0 }
        for i in 0...row  {
            let discount = discounts[i]
            if discount.isServiceBased == true {
                if let service = services.filter({$0.serviceSysName == discount.serviceSysName}).first {
                    if discount.isDiscountPercent {
                        discountAmount -= ((service.totalInitialPrice ?? 0)/100)*(discount.discountPercent ?? 0)
                    } else {
                        discountAmount -= (discount.discountAmount ?? 0)
                    }
                }
            } else {
                if discount.isDiscountPercent {
                    discountAmount -= (discountAmount/100)*(discount.discountPercent ?? 0)
                } else {
                    discountAmount -= (discount.discountAmount ?? 0)
                }
            }
        }
        if discountAmount < 0 {
            return amount
        } else {
            return amount - discountAmount
        }
    }
    private func calcualateMaintenanceDiscount(_ discountRow: DiscountMaster) -> Double {
        
        let amount = services.compactMap({$0.totalMaintPrice}).sum()
        var discountAmount = amount
        let credits = discounts.filter({$0.type == .credit})
        guard let row = credits.firstIndex(of: discountRow) else { return 0 }
        for i in 0...row {
            let discount = credits[i]
            if discount.isDiscountPercent {
                discountAmount -= (discountAmount/100)*(discount.discountPercent ?? 0)
            } else {
                discountAmount -= (discount.discountAmount ?? 0)
            }
        }
        if discountAmount < 0 {
            return amount
        } else {
            return amount - discountAmount
        }
    }
}
// MARK: - Action
extension ConfigureProposalViewController {
    
    @objc func actionApplyCredit() {
        
        guard let selectedCredits = selectedCredit else {
            showAlert(message: "Please select credit")
            return
        }
        if self.discounts.contains(selectedCredits) {
            showAlert(message: "This credit is already in list")
        } else {
            self.view.endEditing(true)
            self.discounts.append(selectedCredits)
            tableView.reloadData()
        }
    }
    
    @objc func actionApplyCoupon() {
        
        guard  let objects = AppUserDefaults.shared.masterDetails?.discountCoupons, !objects.isEmpty else {
            showAlert(message: "No record found !!")
            return
        }
        guard !couponEntered.isEmpty else {
            showAlert(message: "Please enter coupon")
            return
        }
        guard let coupon = objects.filter({ (discount) -> Bool in
            return discount.discountCode == couponEntered
        }).first else {
            showAlert(message: "Invalid coupon")
            return
        }
        if self.discounts.contains(coupon) {
            showAlert(message: "This coupon is already in list")
        } else {
            
            if coupon.isServiceBased == true {
                guard !objects.filter({$0.sysName == coupon.serviceSysName}).isEmpty else {
                    showAlert(message: "This coupon is not valid for any service in the list")
                    return
                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            if let date = coupon.validTo, !date.isEmpty {
                guard let validityDate = dateFormatter.date(from: date) else {
                    showAlert(message: "Something went wrong, please try again later")
                    return
                }
                guard Date() <= validityDate  else {
                    showAlert(message: "This coupon is expired !!")
                    return
                }
            }
            if let date = coupon.validFrom, !date.isEmpty {
                guard let validityDate = dateFormatter.date(from: date) else {
                    showAlert(message: "Something went wrong, please try again later")
                    return
                }
                guard Date() >= validityDate else {
                    showAlert(message: "This coupon is not started yet")
                    return
                }
            }
            /*
             if let dateTo = coupon.validTo, !dateTo.isEmpty, let dateFrom = coupon.validFrom, !dateFrom.isEmpty {
             
             guard let validityDateFrom = dateFormatter.date(from: dateFrom) else {
             showAlert(message: "Something went wrong, please try again later")
             return
             }
             guard let validityDateTo = dateFormatter.date(from: dateTo) else {
             showAlert(message: "Something went wrong, please try again later")
             return
             }
             guard validityDateFrom <= Date(), validityDateTo >= Date() else {
             if Date() > validityDateTo {
             showAlert(message: "This coupon is expired !!")
             } else {
             showAlert(message: "This coupon is not started yet")
             }
             return
             }
             }*/
            
            self.view.endEditing(true)
            self.discounts.append(coupon)
            tableView.reloadData()
        }
    }
    
}
// MARK: - Text field delegate
extension ConfigureProposalViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            var rows:[String] = []
            var selectedRow = 0
            self.view.endEditing(false)
            guard  let objects = AppUserDefaults.shared.masterDetails?.taxMaster, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            for (index,row) in objects.enumerated() {
                if selectedTax?.taxId == row.taxId{
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                let tax = objects[index]
                if tax.taxId != self.selectedTax?.taxId {
                    self.taxPercentage = nil
                }
                self.selectedTax = objects[index]
            }
            return false
        } else  if textField.tag == 3 {
            var rows:[String] = []
            var selectedRow = 0
            
            guard  let objects = AppUserDefaults.shared.masterDetails?.discountCredits, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            self.view.endEditing(false)
            for (index,row) in objects.enumerated() {
                if textField.text == row.name{
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                self.selectedCredit = objects[index]
            }
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            couponEntered = updatedText
        }
        return true
    }
}
// MARK: - ProposalDiscountDelegate
/* extension ConfigureProposalViewController: ProposalDiscountDelegate {
 func actionRemove(_ cell: ProposalDiscountTableViewCell) {
 guard let indexPath = tableView.indexPath(for: cell) else { return }
 
 if indexPath.section == Sections.credit.rawValue {
 if let index = self.discounts.firstIndex(of: cell.discount!) {
 self.discounts.remove(at: index)
 }
 self.tableView.deleteRows(at: [indexPath], with: .automatic)
 guard self.discounts.filter({$0.type == .credit}).isEmpty else {
 return
 }
 tableView.reloadData()
 } else {
 if let index = self.discounts.firstIndex(of: cell.discount!) {
 self.discounts.remove(at: index)
 }
 self.tableView.deleteRows(at: [indexPath], with: .automatic)
 guard self.discounts.filter({$0.type == .coupon}).isEmpty else {
 return
 }
 tableView.reloadData()
 
 }
 }
 } */
// MARK: - EditableStatusDelegate
extension ConfigureProposalViewController: EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?) {
        
        self.entityDetail = entityDetail
        self.propertyDetails = propertyDetail
        isEditable = AppUtility.setEditable(entityDetail)
        taxValidation()
        delegateEdit?.updateEditStatus(entityDetail, propertyDetail: propertyDetails)
    }
}
// MARK: - ProposalView
extension ConfigureProposalViewController: ProposalView {
    func gotTaxDetail(_ output: Double) {
        taxPercentage = output
        proceedValidation(taxPercentage)
    }
}
// MARK: - AgreementSignatureDelegate
extension ConfigureProposalViewController: BillingAddressDelegate {
    func billingDetailEntered(_ billingAddress: SendAgreement) {
        self.billingAddress = billingAddress
    }
    
    func actionBillingAddress(_ isSelected: Bool) {
        billingAddress.billingAddressSameAsService = isSelected
        self.tableView.reloadData()
    }
    func addAddressPressed() {
        /*let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
         let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
         vc.tag = 1
         vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
         vc.modalTransitionStyle = .coverVertical
         vc.delegate = self
         self.present(vc, animated: false, completion: {})*/
    }
}
extension ConfigureProposalViewController {
    
    private func getCommonAgreement() -> SendAgreement {
        var agreement = SendAgreement()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        agreement.companyKey = companyKey
        agreement.assignedToType = "Individual"
        agreement.addressSubType = "Residential"
        agreement.soldServiceStandardDetail  = services
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        agreement.scheduleDate = currentDate
        agreement.scheduleTime = currentTime
        if let source = AppUserDefaults.shared.leadDetailMaster?.sourceMaster.filter({$0.sourceSysName == "DoorToDoor"}).first {
            agreement.sourceIDS = [source.sourceId]
        }
        
        agreement.countryID = 1
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId") ?? "")"
        agreement.fieldSalesPerson = assignedToID
        agreement.assignedToID = assignedToID
        agreement.addressImagePath = propertyDetails?.propertyInfo?.imageUrl?.components(separatedBy: "\\").last
        agreement.billingAddress1 = billingAddress.billingAddress1
        agreement.billingCityName = billingAddress.billingCityName
        agreement.billingStateId = billingAddress.billingStateId
        agreement.billingZipcode = billingAddress.billingZipcode
        agreement.billingCellPhone1 = billingAddress.billingCellPhone1
        agreement.billingFirstName = billingAddress.billingFirstName
        agreement.billingLastName = billingAddress.billingLastName
        agreement.billingPrimaryEmail = billingAddress.billingPrimaryEmail
        agreement.billingAddressSameAsService = billingAddress.billingAddressSameAsService ?? false
        agreement.billingState = billingAddress.billingState
        agreement.billingCountry = billingAddress.billingCountry ?? "USA"
        agreement.billingCountryId =  billingAddress.billingCountryId ?? "1"
        agreement.billingPOCId = billingAddress.billingPOCId
        return agreement
    }
    private func getNewSendAggrement() -> SendAgreement {
        
        guard let  propertyDetails = propertyDetails, let propertyInfo = propertyDetails.propertyInfo else { return SendAgreement() }
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        
        var agreement = getCommonAgreement()
        agreement.flowType =  "Residential"
        agreement.companyKey = companyKey
        agreement.soldServiceStandardDetail  = services
        agreement.area = propertyInfo.area
        agreement.noofBathroom = propertyInfo.bathRoom
        agreement.noofBedroom = propertyInfo.bedRoom
        agreement.noofStory = propertyInfo.noOfFloor
        agreement.primaryEmail = propertyInfo.email ?? ""
        agreement.cellPhone1 = propertyInfo.phone ?? ""
        
        agreement.cityName = propertyInfo.city ?? propertyDetails.propertyInfo?.placeMark?.subLocality
        agreement.zipcode =  propertyInfo.zipCode ?? propertyDetails.propertyInfo?.placeMark?.postalCode
        agreement.firstName =  String(propertyInfo.firstName?.prefix(50) ?? "")
        agreement.lastName =  String(propertyInfo.lastName?.prefix(50) ?? "")
        agreement.address1 = String(propertyInfo.address?.prefix(100) ?? "")
        agreement.countryID = 1
        agreement.visitCount = 1
        agreement.stateID = propertyDetails.propertyInfo?.state?.stateID
        agreement.state = propertyDetails.propertyInfo?.state?.stateSysName
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        /*
         if let categorySysName = services.first?.categorySysName, let departmentSysName = AppUserDefaults.shared.masterDetails?.categories.filter({$0.sysName == categorySysName}).first?.departmentSysName {
         agreement.departmentSysName = departmentSysName
         }*/
        agreement.leadDetail = getCommonLead()
        agreement.paymentInfo = getPaymentInfo(nil)
        return agreement
    }
    private func getLeadAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        guard let leadDetail = propertyDetails?.lead else { return agreement }
        // agreement.assignedToID =  leadDetail.assignedToID?.description
        agreement.flowType =  leadDetail.flowType ?? ""
        agreement.companyKey = leadDetail.companyKey ?? ""
        agreement.area = leadDetail.area  ?? ""
        agreement.noofBathroom = leadDetail.noofBathroom ?? ""
        agreement.noofBedroom = leadDetail.noofBedroom ?? ""
        agreement.noofStory = leadDetail.noofStory  ?? ""
        agreement.primaryEmail = leadDetail.primaryEmail  ?? ""
        agreement.cellPhone1 = leadDetail.primaryPhone ?? ""
        agreement.cityName = leadDetail.cityName ?? ""
        agreement.zipcode =  leadDetail.zipcode ?? ""
        agreement.firstName =  String(leadDetail.firstName?.prefix(50) ?? "")
        agreement.lastName = String(leadDetail.lastName?.prefix(50) ?? "")
        agreement.address1 =  String(leadDetail.address1?.prefix(100) ?? "")
        agreement.address2 = String(leadDetail.address2?.prefix(100) ?? "")
        agreement.visitStatusID = leadDetail.visitStatusID
        agreement.emailDetail = entityDetail?.emailDetail
        agreement.documentsDetail = entityDetail?.documentDetail
        agreement.stateID = leadDetail.stateID
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            agreement.state = state.stateSysName
        }
        agreement.visitCount = leadDetail.visitCount
        agreement.objectionIds = leadDetail.objectionIds
        agreement.currentServicesProvider = leadDetail.currentServicesProvider
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        return agreement
    }
    
    
    private func getOpportunityAgreement() -> SendAgreement {
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        if let opportunityDetails = propertyDetails?.opportunity {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            //agreement.leadNumber = entityDetail?.leadDetail?.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            //  agreement.flowType =  leadDetail.flowType
            if let serviceAddress = opportunityDetails.serviceAddress {
                agreement.area = serviceAddress.area ?? ""
                agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
                agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
                agreement.noofStory = serviceAddress.noofStory ?? ""
                agreement.address1 = serviceAddress.address1 ?? ""
                agreement.address2 = serviceAddress.address2 ?? ""
                agreement.cityName = serviceAddress.cityName ?? ""
                agreement.zipcode =  serviceAddress.zipcode ?? ""
                agreement.stateID = serviceAddress.stateID
                if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                    return state.stateID == serviceAddress.stateID
                }) {
                    agreement.state = state.stateSysName
                }
            }
        } else if let opportunityDetails = entityDetail?.leadDetail {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            agreement.leadNumber = opportunityDetails.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            agreement.flowType =  opportunityDetails.flowType
            agreement.area = opportunityDetails.area ?? ""
            agreement.noofBathroom = opportunityDetails.noofBathroom ?? ""
            agreement.noofBedroom = opportunityDetails.noofBedroom ?? ""
            agreement.noofStory = opportunityDetails.noOfStory ?? ""
            agreement.address1 = opportunityDetails.servicesAddress1 ?? ""
            agreement.address2 = opportunityDetails.serviceAddress2 ?? ""
            agreement.cityName = opportunityDetails.serviceCity ?? ""
            agreement.zipcode =  opportunityDetails.serviceZipcode ?? ""
            //agreement.stateID = opportunityDetails.serviceState
        }
        return agreement
    }
    private func getAccountAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        guard let accountDetails = propertyDetails?.account else { return agreement }
        agreement.firstName =  String(accountDetails.firstName?.prefix(50) ?? "")
        agreement.lastName = String(accountDetails.lastName?.prefix(50) ?? "")
        agreement.primaryEmail = accountDetails.primaryEmail
        agreement.cellPhone1 = accountDetails.primaryPhone
        // agreement.flowType =  leadDetail.flowType
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        if let serviceAddress = accountDetails.serviceAddress {
            agreement.area = serviceAddress.area ?? ""
            agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
            agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
            agreement.noofStory = serviceAddress.noofStory ?? ""
            agreement.address1 = String(serviceAddress.address1?.prefix(100) ?? "")
            agreement.address2 = String(serviceAddress.address2?.prefix(100) ?? "")
            agreement.cityName = serviceAddress.cityName ?? ""
            agreement.zipcode =  serviceAddress.zipcode ?? ""
            agreement.stateID = serviceAddress.stateID
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateID == serviceAddress.stateID
            }) {
                agreement.state = state.stateSysName
            }
        }
        return agreement
    }
    private func getNewLeads() -> LeadDetail? {
        
        var leadDetail = LeadDetail()
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        leadDetail.flowType = "Residential"
        leadDetail.scheduleTime = currentTime
        leadDetail.scheduleDate = currentDate
        // leadDetail.isCustomerNotPresent =
        leadDetail.latitude = propertyDetails?.propertyInfo?.latitude
        leadDetail.longitude = propertyDetails?.propertyInfo?.longitude
        
        return leadDetail
    }
    private func getCommonLead(_ lead: LeadDetail? = nil) -> LeadDetail? {
        
        var leadDetail = lead
        if leadDetail == nil {
            leadDetail = getNewLeads()
        }
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        //leadDetail?.notes = signature.internalNotes
        leadDetail?.versionNumber = appVersion
        leadDetail?.statusSysName = "Complete"
        leadDetail?.stageSysName = "Lost"
        /*if !signature.customerNotPresent {
         leadDetail?.stageSysName = "Won"
         } else {
         leadDetail?.stageSysName = "CompletePending"
         }
         leadDetail?.collectedAmount = signature.amount
         leadDetail?.iAgreeTerms = signature.termsCondition
         leadDetail?.isAgreementSigned = !signature.customerNotPresent
         leadDetail?.isCustomerNotPresent = signature.customerNotPresent
         leadDetail?.smsReminders = signature.sms
         leadDetail?.phoneReminders = signature.phone
         leadDetail?.emailReminders = signature.email*/
        
        leadDetail?.subTotalAmount = services.compactMap({$0.totalInitialPrice}).sum()
        leadDetail?.subTotalMaintAmount = services.compactMap({$0.totalMaintPrice}).sum()
        leadDetail?.totalPrice = services.compactMap({$0.totalInitialPrice}).sum()
        leadDetail?.totalMaintPrice = services.compactMap({$0.totalMaintPrice}).sum()
        
        leadDetail?.tax = taxPercentage?.description ?? 0.description
        leadDetail?.taxSysName =  selectedTax?.sysName ?? ""
        
        /* let taxableServicesAmountMaint = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalMaintPrice}).sum()
         let discountMaint = calcualateMaintenanceDiscount(discount)
         
         let totalTaxableAmountMaint = taxableServicesAmountMaint - discountMaint
         var taxAmountMaint = 0.0
         if let taxPercentage = taxPercentage {
         taxAmountMaint = (totalTaxableAmountMaint/100)*(taxPercentage)
         }
         leadDetail?.taxableMaintAmount = totalTaxableAmountMaint
         leadDetail?.taxMaintAmount = taxAmountMaint
         
         var taxAmount = 0.0
         let taxableServicesAmount = services.filter({$0.isResidentialTaxable == true && propertyDetails?.propertyInfo?.flowType  == "Residential" || $0.isCommercialTaxable == true && propertyDetails?.propertyInfo?.flowType  != "Residential"}).compactMap({$0.totalInitialPrice}).sum()
         let discount = calcualateIntialDiscount()
         
         let totalTaxableAmount = taxableServicesAmount - discount
         
         if let taxPercentage = taxPercentage {
         taxAmount = (totalTaxableAmount/100)*(taxPercentage)
         }
         
         leadDetail?.taxableAmount = totalTaxableAmount
         leadDetail?.taxAmount = taxAmount*/
        
        leadDetail?.versionDate = VersionDate
        leadDetail?.versionNumber = VersionNumber
        let deviceVersion = UIDevice.current.systemVersion
        let deviceName = UIDevice.current.name
        leadDetail?.deviceName = deviceName
        leadDetail?.deviceVersion = deviceVersion
        
        if AppUserDefaults.shared.isPreSetSignSales, let serviceTechSignPath = AppUserDefaults.shared.serviceTechSignPath?.trimmed, !serviceTechSignPath.isEmpty {
            if  let urlString = serviceTechSignPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {
                leadDetail?.isEmployeePresetSignature = true
            }
        }
        return leadDetail
    }
    private func getPaymentInfo(_ paymentInfo: PaymentInfo?) -> PaymentInfo? {
        var paymentInfo = paymentInfo
        if paymentInfo == nil {
            paymentInfo = PaymentInfo()
        }
        /*  paymentInfo?.paymentMode = paymentMode.mode
         paymentInfo?.amount = signature.amount
         paymentInfo?.salesSignature = signature.salesSignature ?? ""
         paymentInfo?.customerSignature = signature.customerSignature ?? ""
         paymentInfo?.specialInstructions = signature.customerNotes ?? ""*/
        return paymentInfo
    }
}
extension ConfigureProposalViewController: ImagesUpdated {
    func imagesUpdates(_ images: [DTDLeadImagesDetailExtSerDc]) {
        self.propertyDetails?.galleryImages = images
    }
}
// MARK: - MapViewNavigationDelegate
extension ConfigureProposalViewController: MapViewNavigationDelegate {
    func actionBack() {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
    }
}
