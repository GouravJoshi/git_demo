//
//  ConfiguralProposalService.swift
//  DPS
//
//  Created by Vivek Patel on 28/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class ConfiguralProposalService {
    
    func getTaxDetail(taxSysName: String, _ callBack:@escaping (_ object: Double?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let url = URL.Tax.tax.replacingOccurrences(of: "(taxSysName)", with: taxSysName).replacingOccurrences(of: "(companyKey)", with: companyKey)
        AF.request(url, method: .get) .responseDecodable(of: Double.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                
                break
            }
            
        }
        
    }
}
