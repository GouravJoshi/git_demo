//
//  ConfiguralProposalPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 28/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol ProposalView: CommonView {
    func gotTaxDetail(_ output: Double)
}

class ConfiguralProposalPresenter: NSObject {
    weak private var delegate: ProposalView?
    private lazy var service = {
        return ConfiguralProposalService()
    }()
    
    init(_ delegate: ProposalView) {
        self.delegate = delegate
    }
    
    func apiCallForTaxtDetail(taxSysName: String) {
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        service.getTaxDetail(taxSysName: taxSysName) { (object, error) in
            FTIndicator.dismissProgress()
            if let object = object {
                self.delegate?.gotTaxDetail(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
}

