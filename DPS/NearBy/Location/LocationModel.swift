//
//  LocationModel.swift
//  DPS
//
//  Created by Vivek Patel on 11/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct SearchLocation: Codable {
    var results: [LocationResult]
}

struct LocationResult: Codable{
    var formattedAddress: String?
    var geometry: Geometry
    var placeId: String?
    enum CodingKeys: String, CodingKey  {
        case formattedAddress = "formatted_address"
        case geometry
        case placeId = "place_id"
    }
}

struct Geometry: Codable{
    var location: Location
}

struct Location: Codable{
    var latitude: Double
    var longitude: Double
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
struct RecentSearch: Codable {
    var attributedFullText: String
    var placeID: String
    var attributedPrimaryText: String
    
}
struct PlaceDetail: Codable {
    var result: PlaceComponets?
}

struct PlaceComponets: Codable {
    var formattedAddress: String?
    var formattedPhoneNumber: String?
    var international_phone_number: String?
    //  var rating: Int?
    var website: String?
    var icon: String?
    var name : String?
    var types: [String] = []
    var url: String?
    var photos:[PlaceImage]?
    
    enum CodingKeys: String, CodingKey  {
        case formattedAddress = "formatted_address"
        case formattedPhoneNumber = "formatted_phone_number"
        case website
        case icon
        case name
        case types
        case url
        case photos
    }
    init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        formattedAddress = try container.decodeIfPresent(String.self, forKey: .formattedAddress)
        formattedPhoneNumber = try container.decodeIfPresent(String.self, forKey: .formattedPhoneNumber)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        types = try container.decodeIfPresent([String].self, forKey: .types) ?? []
        photos = try container.decodeIfPresent([PlaceImage].self, forKey: .photos)
       
    }
}

struct PlaceImage: Codable {
    var photoRefrence:String?
    enum CodingKeys: String, CodingKey  {
        case photoRefrence = "photo_reference"
    }
}
