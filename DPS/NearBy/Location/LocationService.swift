//
//  LocationService.swift
//  DPS
//
//  Created by Vivek Patel on 11/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

class LocationService: NSObject {
    
    static let instance = LocationService()
    
    var lastKnownLocation: CLLocation?
    private let locationManager = CLLocationManager()
    private var locationCallback: ((CLLocation?) -> Void)?
    
    private override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Register to get notify if app move to foreground
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        stopUpdatingLocation()
    }
    
    @objc func appMovedToForeground() {
        getAuthorization { status in
            if status == .notDetermined {
                self.locationManager.requestWhenInUseAuthorization()
            } else if status == .authorized {
                if self.locationCallback != nil {
                    self.startUpdatingLocation()
                }
            } else {
                self.locationCallback?(nil)
                self.stopUpdatingLocation()
            }
        }
    }
    
    func startUpdatingLocation() {
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
        locationCallback = nil
    }
    
    func getAuthorization(authorization: @escaping AuthorizationHandler) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
                authorization(.authorized)
            } else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
                authorization(.denied)
            } else {
                authorization(.notDetermined)
            }
        } else {
            authorization(.disabled)
        }
    }
    
    func getLocation(completion: @escaping (CLLocation?) -> Void) {
        locationCallback = completion
        
        getAuthorization { status in
            if status == .notDetermined {
                self.locationManager.requestWhenInUseAuthorization()
            } else if status == .disabled || status == .denied {
                // self.showOpenSettingsAlert()
                self.locationCallback?(nil)
            } else {
                self.startUpdatingLocation()
            }
        }
    }
    
    //    func showOpenSettingsAlert()  {
    //        let alertController = UIAlertController (title: "Permission Needed", message: "Please enable Location Services in the \"Settings App\" on your device. When you allow Location Services, the App can find your location.", preferredStyle: .alert)
    //        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
    //            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
    //                return
    //            }
    //            if UIApplication.shared.canOpenURL(settingsUrl) {
    //                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
    //                })
    //            }
    //        }
    //        alertController.addAction(settingsAction)
    //
    //        self.navigationController?.present(alertController, animated: true, completion: nil)
    //    }
    private func openAppSettings() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString) {
            DispatchQueue.main.async {
                if UIApplication.shared.canOpenURL(appSettings) {
                    UIApplication.shared.open(appSettings)
                }
            }
        }
    }
}

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedWhenInUse || status == .authorizedAlways) && locationCallback != nil {
            startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastKnownLocation = locations.last
        locationCallback?(lastKnownLocation)
        stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
        locationCallback?(nil)
        stopUpdatingLocation()
    }
}
typealias AuthorizationHandler = (_ Status: AuthorizationStatus) -> ()

enum AuthorizationStatus {
    case notDetermined
    case denied
    case authorized
    case disabled
}
