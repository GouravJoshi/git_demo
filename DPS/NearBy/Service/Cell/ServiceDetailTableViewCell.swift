//
//  ServiceDetailTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol ServiceDetailCellDelegate {
    func actionAddOnService(_ cell: ServiceDetailTableViewCell)
    func actionEditService(_ cell: ServiceDetailTableViewCell)
    func actionSelectService(_ cell: ServiceDetailTableViewCell)
    func actionDeleteService(_ cell: ServiceDetailTableViewCell)
}

class ServiceDetailTableViewCell: UITableViewCell {
    // MARK: - Variables
    var service : Service? {
        didSet {
            guard  let service = service else { return }
            
            lblDescription.attributedText = service.description?.trimmed.html2AttributedString
            if let objects = AppUserDefaults.shared.masterDetails?.frequencies, let object = objects.filter({$0.sysName == service.defaultServiceFrequencySysName}).first {
                lblFrequency.text = object.frequencyName?.trimmed
            } else {
                lblFrequency.text = nil
            }
            /*
             if service.isParameterizedPriced == false, let selectedServicePackage = service.selectedServicePackage, let servicePackageDCS = service.servicePackageDCS?.filter({$0.servicePackageID == selectedServicePackage.servicePackageID}).first {
             if let packageName = servicePackageDCS.packageName {
             lblServiceName.text = "\(service.name?.trimmed ?? "")(\(packageName))"
             } else {
             lblServiceName.text = service.name?.trimmed
             }
             } else {
             lblServiceName.text = service.name?.trimmed
             }*/
            lblServiceName.text = service.name?.trimmed
            
            btnCheckBox.isSelected = service.isSelected
            lblDescription.attributedText = service.description?.trimmed.html2AttributedString
            lblSeprator.isHidden  =  lblDescription.attributedText?.string == "" || lblDescription.attributedText?.string == nil  ? true : false
        }
    }
    var soldService: SoldServiceStandardDetail? {
        didSet {
            guard  let soldService = soldService else { return }
            if let serviceDetails = AppUserDefaults.shared.masterDetails  {
                let name = serviceDetails.categories.flatMap { $0.services}.filter{$0.sysName == soldService.serviceSysName}.first?.name
                lblServiceName.text = name
                // setServiceName(name: name ?? "", soldService: soldService)
            } else {
                lblServiceName.text = soldService.serviceSysName
                // setServiceName(name: soldService.serviceSysName ?? "", soldService: soldService)
            }
            
            lblDescription.attributedText = soldService.serviceDescription?.trimmed.html2AttributedString
            lblFrequency.text = soldService.serviceFrequency
            
            btnCheckBox.isSelected = soldService.isSelected
            if lblDescription.attributedText?.string == "" || lblDescription.attributedText?.string == nil {
                lblSeprator.isHidden = true
            } else {
                lblSeprator.isHidden = false
            }
            
            
        }
    }
    private func setServiceName(name: String, soldService: SoldServiceStandardDetail) {
        if   let packageId = soldService.packageID, let category = AppUserDefaults.shared.masterDetails?.categories.filter({$0.sysName == soldService.categorySysName}).first, let service = category.services.filter({$0.sysName == soldService.serviceSysName}).first, service.isParameterizedPriced == false {
            let selectedPackage = service.servicePackageDCS?.filter({$0.servicePackageID == packageId}).first
            if let packageName = selectedPackage?.packageName {
                lblServiceName.text = "\(name)(\(packageName))"
            }
        }
    }
    
    var delegate: ServiceDetailCellDelegate?
    
    // MARK: - Outlets
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblMaintanence: UILabel!
    @IBOutlet weak var lblInitialPrice: UILabel!
    @IBOutlet weak var btnEditService: UIButton!{
        didSet {
            AppUtility.buttonImageColor(btn: btnEditService, image: "Group 779", color: .appDarkGreen)
        }
    }
    @IBOutlet weak var btnTrash: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnTrash, image: "deleteTemp.png", color: .red)
        }
    }
    @IBOutlet weak var lblSeprator: UILabel!
    @IBOutlet weak var btnAddOn: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblServiceName: UILabel!
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func actionAddOn(_ sender: Any) {
        delegate?.actionAddOnService(self)
    }
    @IBAction func actionEdit(_ sender: Any) {
        delegate?.actionEditService(self)
    }
    @IBAction func actionSelectService(_ sender: Any) {
        delegate?.actionSelectService(self)
    }
    
    @IBAction func actionDelete(_ sender: Any) {
        delegate?.actionDeleteService(self)
    }
}
