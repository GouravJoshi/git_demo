//
//  ServicePresenter.swift
//  DPS
//
//  Created by Vivek Patel on 09/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

protocol ServiceView: CommonView {
    func gotSeriveList(_ output: EntityDetail)
}

class ServicePresenter: NSObject {
    weak private var delegate: ServiceView?
    private lazy var service = {
        
        return ServicesService()
    }()
    
    init(_ delegate: ServiceView) {
        self.delegate = delegate
    }
    
    
    func apiCallForServiceList(_ leadId: String) {
        delegate?.showLoader()
        service.getServiceList(leadId: leadId) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object{
                self.delegate?.gotSeriveList(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}

