//
//  EditServiceViewController.swift
//  DPS
//
//  Created by Vivek Patel on 23/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
protocol EditServiceProtocol {
    func editService(service: Service?, soldService: SoldServiceStandardDetail?)
}
class EditServiceViewController: BaseViewController {
    
    // MARK: - Variable
    var initialPrice: Double?
    var maintenanacePirce: Double?
    var service: Service?
    var solidService: SoldServiceStandardDetail?
    var delegate: EditServiceProtocol?
    var selectedFrequency: Frequency?
    var selectedReason: PriceChangeReason?
    var propertyInfo: PropertyInfo?
    private var selectedPackage: ServicePackageDc?
    private var servicePackageDCS: [ServicePackageDc] = []
    private var servicesPricingDetailDcs: ServicePricingDetail?
    @IBOutlet weak var viewPackage: UIStackView!
    // MARK: Outlets
    @IBOutlet weak var txtPackage: UITextField!
    @IBOutlet weak var txtFrequency: UITextField!
    @IBOutlet weak var txtMaintenancePrice: UITextField!
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var txtInitialPrice: UITextField!
    @IBOutlet weak var viewMaintanencePriceError: UIView! {
        didSet {
            viewMaintanencePriceError.layer.cornerRadius = 6
            viewMaintanencePriceError.backgroundColor = UIColor.appThemeColor.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var viewIntialPriceError: UIView! {
        didSet {
            viewIntialPriceError.layer.cornerRadius = 6
            viewIntialPriceError.backgroundColor = UIColor.appThemeColor.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var lblinitialErorr: UILabel!{
        didSet   {
            lblinitialErorr.textColor = .black
        }
    }
    @IBOutlet weak var lblMaintenanceError: UILabel!{
        didSet   {
            lblMaintenanceError.textColor = .black
        }
    }
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var viewMain: UIView!  {
        didSet {
            viewMain.layer.cornerRadius = 6
        }
    }
    @IBOutlet weak var btnDissmiss: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.layer.cornerRadius = 20
        }
    }
    @IBOutlet weak var txtViewDescription: UITextView! {
        didSet {
            txtViewDescription.layer.borderWidth = 1
            txtViewDescription.layer.cornerRadius = 6
            txtViewDescription.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    // MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        
    }
    private func getMinPrice() -> (initialPrice: Double,maintanencePrice: Double) {
        var maintanencePrice: Double = 0
        var initialPrice: Double = 0
        if let selectedService = self.service {
           _ = self.settingPrice(selectedService)
        } else if let soldService = self.solidService {
            _ = self.settingSoldPrice(soldService)
        }
        if let selectedPackage = selectedPackage {
            if let servicePackageDetails = selectedPackage.servicePackageDetails, let selectedServicePackageDetails = servicePackageDetails.filter({$0.frequencyID == selectedFrequency?.frequencyId}).first {
                initialPrice = selectedServicePackageDetails.minPackageCost ?? 0
                maintanencePrice = selectedServicePackageDetails.minPackageMaintCost ?? 0
            }
        } else if let servicesPricingDetailDcs = servicesPricingDetailDcs  {
            initialPrice = servicesPricingDetailDcs.minInitialPrice ?? 0
            maintanencePrice = servicesPricingDetailDcs.minMaintenancePrice ?? 0
        }
        return (initialPrice: initialPrice , maintanencePrice: maintanencePrice)
    }
    
    private func minPriceValidation() -> Bool {
        let minPrice = getMinPrice()
        if let initialPriceString = txtInitialPrice.text {
            guard  let initialPrice = Double(initialPriceString), initialPrice >= minPrice.initialPrice  else {
                viewIntialPriceError.isHidden = false
                lblinitialErorr.text = "You can not reduce initial price of this service from minimun price of \(minPrice.initialPrice)"
                return false
            }
            viewIntialPriceError.isHidden = true
        }
        if let maintanencePriceString = txtMaintenancePrice.text {
            guard let maintanencePrice = Double(maintanencePriceString), maintanencePrice >= minPrice.maintanencePrice  else {
                viewMaintanencePriceError.isHidden = false
                lblMaintenanceError.text = "You can not reduce maintenance price of this service from minimun price of \(minPrice.maintanencePrice)"
                return false
            }
            viewMaintanencePriceError.isHidden = true
        }
        return true
    }
    private func setData() {
        viewMaintanencePriceError.isHidden = true
        viewIntialPriceError.isHidden = true
        if let service = service {
            lblServiceName.text = service.name
            if let objects = AppUserDefaults.shared.masterDetails?.frequencies, let object = objects.filter({$0.sysName == service.defaultServiceFrequencySysName}).first {
                txtFrequency.text = object.frequencyName
                selectedFrequency = object
            } else {
                txtFrequency.text = nil
            }
            if let isParameterizedPriced = service.isParameterizedPriced, isParameterizedPriced {
                viewPackage.isHidden = true
            } else {
                viewPackage.isHidden = false
            }
            self.servicePackageDCS = service.servicePackageDCS ?? []
            if let servicePackageDCS = service.servicePackageDCS?.filter({$0.servicePackageID == service.selectedServicePackage?.servicePackageID}).first {
                selectedPackage = servicePackageDCS
                
            }
        } else {
            viewPackage.isHidden = true
            lblServiceName.text = solidService?.serviceSysName
            txtFrequency.text = solidService?.serviceFrequency
            
            if   let packageId = solidService?.packageID, let category = AppUserDefaults.shared.masterDetails?.categories.filter({$0.sysName == solidService?.categorySysName}).first, let service = category.services.filter({$0.sysName == solidService?.serviceSysName}).first, service.isParameterizedPriced == false {
                viewPackage.isHidden = false
                self.servicePackageDCS = service.servicePackageDCS ?? []
                selectedPackage = service.servicePackageDCS?.filter({$0.servicePackageID == packageId}).first
            }
        }
        txtPackage.text = selectedPackage?.packageName
        txtInitialPrice.text = initialPrice?.description
        txtMaintenancePrice.text = maintenanacePirce?.description
    }
    // MARK: Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if !viewPackage.isHidden {
            guard  !txtPackage.text!.isEmpty else {
                showAlert(message: "Please select package")
                return
            }
        }
        guard  !txtFrequency.text!.isEmpty else {
            showAlert(message: "Please select frequency")
            return
        }
        guard !txtInitialPrice.text!.isEmpty  else {
            showAlert(message: "Please Enter initial Amount")
            return
        }
        
        guard  !txtMaintenancePrice.text!.isEmpty else {
            showAlert(message: "Please Enter maintenance Amount")
            return
        }
        guard minPriceValidation() else {
            return
        }
        /*guard  !txtReason.text!.isEmpty else {
            showAlert(message: "Please select Reason")
            return
        }
            guard  !txtViewDescription.text!.isEmpty else {
         showAlert(message: "Please enter description")
         return
         }*/
        self.dismiss(animated: true) {
            
            if var selectedService = self.service {
                if let intialPrice = self.txtInitialPrice.text, let price = Double(intialPrice) {
                    selectedService.manualInitialPrice = price
                }
                if let maintenancePrice = self.txtMaintenancePrice.text, let price = Double(maintenancePrice) {
                    selectedService.manualMaintenancePrice = price
                }
                if let selectedFrequency = self.selectedFrequency {
                    selectedService.defaultServiceFrequencySysName = selectedFrequency.sysName
                }
                selectedService.priceChangeReason = self.selectedReason
                selectedService.selectedServicePackage = self.selectedPackage
                selectedService.priceChangeDescription = self.txtViewDescription.text!
                self.delegate?.editService(service: selectedService, soldService: nil)
                
            } else if var soldService = self.solidService {
                if let intialPrice = self.txtInitialPrice.text, let price = Double(intialPrice) {
                    soldService.totalInitialPrice = price
                }
                if let maintenancePrice = self.txtMaintenancePrice.text, let price = Double(maintenancePrice) {
                    soldService.totalMaintPrice = price
                }
                if let selectedFrequency = self.selectedFrequency {
                    soldService.frequencySysName = selectedFrequency.sysName
                    soldService.serviceFrequency = selectedFrequency.frequencyName
                }
                soldService.packageID = self.selectedPackage?.servicePackageID
                soldService.priceModifiedDescription = self.txtViewDescription.text
                soldService.priceChangeReasonId = self.selectedReason?.priceChangeReasonId
                self.delegate?.editService(service: nil, soldService: soldService)
            }
        }
    }
}
// MARK: - UITextFieldDelegate
extension EditServiceViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPackage {
            
            var rows:[String] = []
            var selectedRow = 0
            if !viewPackage.isHidden, !servicePackageDCS.isEmpty {
                for (index,row) in servicePackageDCS.enumerated() {
                    if selectedPackage?.servicePackageID == row.servicePackageID {
                        selectedRow = index
                    }
                    rows.append(row.packageName ?? "")
                }
                showStringPicker(sender: txtPackage, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    self.txtPackage.text = value
                    self.selectedPackage = self.servicePackageDCS[index]
                    self.selectedFrequency = nil
                    self.txtFrequency.text = nil
                    self.txtInitialPrice.text = nil
                    self.txtMaintenancePrice.text = nil
                }
            } else {
                txtPackage.text = ""
                showAlert(message: "No Package found !!")
            }
            return false
        } else if textField == txtFrequency {
            if !viewPackage.isHidden, selectedPackage == nil {
                showAlert(message: "Please select a package first")
                return false
            }
            var rows:[String] = []
            var selectedRow = 0
            var frequencies: [Frequency] = []
            if viewPackage.isHidden
            {
                let servicesPrice = AppUtility.getServicePrices(service)
                if !servicesPrice.isEmpty {
                    frequencies = AppUtility.getFrquencies(servicesPrice)
                }
            }
            else
            {
                if let selectedPackage = selectedPackage, let servicePackageDetailsT = selectedPackage.servicePackageDetails , !servicePackageDetailsT.isEmpty {
                    frequencies = AppUtility.getFrquencies(servicePackageDc: selectedPackage.servicePackageDetails)
                }
            }
            guard !frequencies.isEmpty  else {
                showAlert(message: "No record found !!")
                return false
            }
            
            for (index,row) in frequencies.enumerated() {
                if selectedFrequency?.sysName == row.sysName {
                    selectedRow = index
                }
                rows.append(row.frequencyName ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                self.selectedFrequency = frequencies[index]
                self.service?.defaultServiceFrequencySysName = self.selectedFrequency?.sysName
                self.solidService?.frequencySysName = self.selectedFrequency?.sysName
                
                if self.service?.isParameterizedPriced == false
                {
                    var frequencies: [Frequency] = []
                    var servicePackageDetails: [ServicePackageDetail] = []
                    
                    if let selectedPackage = self.selectedPackage, let servicePackageDetailsT = selectedPackage.servicePackageDetails , !servicePackageDetailsT.isEmpty {
                        servicePackageDetails = servicePackageDetailsT
                        frequencies = AppUtility.getFrquencies(servicePackageDc: selectedPackage.servicePackageDetails)
                    }
                    
                    if let servicePackageDetail = servicePackageDetails.filter({$0.frequencyID == frequencies[index].frequencyId}).first {
                        self.txtInitialPrice.text = servicePackageDetail.packageCost?.description
                        self.txtMaintenancePrice.text = servicePackageDetail.packageMaintCost?.description
                    } else {
                        self.txtInitialPrice.text = nil
                        self.txtMaintenancePrice.text = nil
                    }
                    
                }
                else
                {
                    if let selectedService = self.service
                    {
                        let price =  self.settingPrice(selectedService)
                        self.txtInitialPrice.text = price.intialPrice.description
                        self.txtMaintenancePrice.text = price.maintaincePrice.description
                    }
                    else if let soldService = self.solidService
                    {
                        let price =  self.settingSoldPrice(soldService)
                        self.txtInitialPrice.text = price.intialPrice.description
                        self.txtMaintenancePrice.text = price.maintaincePrice.description
                    }
                }
                /*if let selectedService = self.service
                {
                    let price =  self.settingPrice(selectedService)
                    self.txtInitialPrice.text = price.intialPrice.description
                    self.txtMaintenancePrice.text = price.maintaincePrice.description
                }
                else if let soldService = self.solidService
                {
                    let price =  self.settingSoldPrice(soldService)
                    self.txtInitialPrice.text = price.intialPrice.description
                    self.txtMaintenancePrice.text = price.maintaincePrice.description
                }*/
            }
            return false
        }
        
        
        else if textField == txtReason {
            var rows:[String] = []
            var selectedRow = 0
            
            guard  let objects = AppUserDefaults.shared.masterDetails?.priceChangeReason, !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            for (index,row) in objects.enumerated() {
                if selectedReason?.priceChangeReasonId == row.priceChangeReasonId {
                    selectedRow = index
                }
                rows.append(row.title ?? "")
            }
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                self.selectedReason = objects[index]
            }
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
       /* if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            
            
            let characterSet = CharacterSet(charactersIn: string)
            
            if textField == txtInitialPrice || textField ==  txtMaintenancePrice {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
            }
        }*/
        
        if textField == txtInitialPrice || textField ==  txtMaintenancePrice
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            return chk
            
        }
        
        return true
    }
    
    private func settingPrice(_ service: Service) -> (intialPrice: Double, maintaincePrice: Double) {
        let price = AppUtility.getPrice(service, propertyDetails: propertyInfo)
        self.servicesPricingDetailDcs = price.servicesPricingDetailDcs
        return (service.manualInitialPrice ?? price.intialPrice,service.manualMaintenancePrice ?? price.maintaincePrice)
    }
    private func settingSoldPrice(_ service: SoldServiceStandardDetail) -> (intialPrice: Double, maintaincePrice: Double) {
        let price = AppUtility.getPrice(soldService: service, propertyDetails: propertyInfo)
        self.servicesPricingDetailDcs = price.servicesPricingDetailDcs
        return (service.totalInitialPrice ?? price.intialPrice,service.totalMaintPrice ?? price.maintaincePrice)
    }
}
