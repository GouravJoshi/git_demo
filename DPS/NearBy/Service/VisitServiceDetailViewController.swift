//
//  VisitServiceDetailViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class VisitServiceDetailViewController: BaseViewController {
    
    
    // MARK: - Variables
    var propertyDetails: PropertyDetails?
    lazy var services: [Service] = []
    lazy var soldServices: [SoldServiceStandardDetail] = []
    var servicesToSync: [SoldServiceStandardDetail] = []

    var entityDetail: EntityDetail? {
        didSet {
            setBillingAddressData()
        }
    }
    var customerNotes: String?
    lazy var plusServices: [Service] = []
    var delegate: EditLeadDelegate?
    var delegateEdit: EditableStatusDelegate?
    private lazy var presenter = {
        return ServicePresenter(self)
    }()
    var billingAddress: SendAgreement?
    var logType: LogType = .visit
    var offlineDelegate: OfllinePropertyInforUpdate?
    var selectedLocation: CLLocation?

    private var isEditable = true {
        didSet {
            tableView.reloadData()
            setUI()
        }
    }
    //Nilind
    var sendAgreement = SendAgreement()
    private lazy var presenterServiceReport = {
        return ServiceReportPresenter(self)
    }()
    // MARK: - Outlets
    @IBOutlet weak var btnLogVisit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        DispatchQueue.global(qos: .background).async {
//            self.getEmployeeBranchName()
//        }
        setUI()
        setFooterView()
        setBillingAddressData()
        
        self.title = "Select Services"
        // FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        if propertyDetails?.propertyType == .opportunity {
            
            if(propertyDetails?.opportunity?.opportunityStatus == "Complete" && (propertyDetails?.opportunity?.opportunityStage == "Won")){
                isEditable = false
            }else{
                isEditable = true
            }
            
            self.perform(#selector(apiCallForServiceList), with: nil, afterDelay: 0.0)
        } else {
            
            self.perform(#selector(self.fetchServices), with: nil, afterDelay: 0.0)
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
                
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    
    // MARK: - UI Setup
    private func setUI() {
        if propertyDetails?.propertyType == .empty || propertyDetails?.propertyType == .lead {
            if  let leadDetail = propertyDetails?.lead, let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == leadDetail.statusName}).first {
                if leadsStatus.name == "Converted" || leadsStatus.name == "Complete" {
                    btnLogVisit.isHidden = true
                } else {
                    btnLogVisit.isHidden = !isEditable
                }
            } else {
                btnLogVisit.isHidden = !isEditable
            }
            if propertyDetails?.propertyType == .empty {
                btnLogVisit.setTitle("Log Visit", for: .normal)
            } else {
                btnLogVisit.setTitle("Edit Visit", for: .normal)
            }
        } else {
            btnLogVisit.isHidden = true
        }
    }
    @objc private func apiCallForServiceList() {
        guard let propertyDetails = propertyDetails  else { return }
        let id = propertyDetails.opportunity?.opportunityNumber?.description
        if let id = id {
            presenter.apiCallForServiceList(id)
        }
    }
    
    private func getSoldServiecs(from services: [Service]) -> [SoldServiceStandardDetail] {
        
        var soldServices: [SoldServiceStandardDetail] = []
        for service in services {
            var soldService = SoldServiceStandardDetail()
            soldService.serviceSysName = service.sysName
            soldService.totalInitialPrice = service.manualInitialPrice ?? 0.0
            soldService.totalMaintPrice = service.manualMaintenancePrice ?? 0.0
            soldService.initialPrice = service.manualInitialPrice ?? 0.0
            soldService.maintenancePrice = service.manualMaintenancePrice ?? 0.0
            soldService.createdBy = service.createdBy
            soldService.createdDate = service.createdDate
            soldService.serviceID = service.serviceMasterID
            soldService.serviceTermsConditions = service.termsConditions
            soldService.modifiedDate = service.modifiedDate
            soldService.frequencySysName = service.defaultServiceFrequencySysName
            soldService.serviceDescription = service.description
            soldService.serviceID = service.serviceMasterID
            soldService.priceModifiedDescription = service.priceChangeDescription
            soldService.priceChangeReasonId = service.priceChangeReason?.priceChangeReasonId
            soldService.isCommercialTaxable = service.isCommercialTaxable
            soldService.isResidentialTaxable = service.isResidentialTaxable
            soldService.packageID =  service.selectedServicePackage?.servicePackageID
            soldService.servicePackageName = service.selectedServicePackage?.packageName
            let randomInt = Int.random(in: 1...100)
            soldService.soldServiceStandardID = randomInt
            var defaultBillingFreqSysName: String?
            if service.defaultBillingFrequency == "SameAsService" || service.defaultBillingFrequency == nil {
                defaultBillingFreqSysName = service.defaultServiceFrequencySysName
            } else {
                defaultBillingFreqSysName = service.defaultServiceFrequencySysName
            }
            soldService.billingFrequencySysName = defaultBillingFreqSysName
            
            soldService.billingFrequencyPrice = calculateBillingPrice(totalIntialPrice: soldService.totalInitialPrice ?? 0, totalMaintPrice: soldService.totalMaintPrice ?? 0, billingFrequencySysName: soldService.frequencySysName ?? "", defaultServiceFrequencySysName: soldService.billingFrequencySysName ?? "")
            soldService.categorySysName = service.categorySysName
            soldService.isSold = true
            if let objects = AppUserDefaults.shared.masterDetails?.frequencies, let object = objects.filter({$0.sysName == service.defaultServiceFrequencySysName}).first {
                soldService.serviceFrequency = object.frequencyName
            }
            soldServices.append(soldService)
        }
        return soldServices
    }
    private func getSoldServiecs(from services: [SoldServiceStandardDetail]) -> [SoldServiceStandardDetail] {
        
        var soldServices: [SoldServiceStandardDetail] = []
        for service in services {
            
            var soldService = service
            if soldService.soldServiceStandardID == nil {
                let randomInt = Int.random(in: 1...100)
                soldService.soldServiceStandardID = randomInt
            }
            var defaultBillingFreqSysName: String?
            if service.billingFrequencySysName == "SameAsService" || service.billingFrequencySysName == nil {
                defaultBillingFreqSysName = service.frequencySysName
            } else {
                defaultBillingFreqSysName = service.billingFrequencySysName
            }
            soldService.billingFrequencySysName = defaultBillingFreqSysName
            
            soldService.billingFrequencyPrice = calculateBillingPrice(totalIntialPrice: soldService.totalInitialPrice ?? 0, totalMaintPrice: soldService.totalMaintPrice ?? 0, billingFrequencySysName: soldService.frequencySysName ?? "", defaultServiceFrequencySysName: soldService.billingFrequencySysName ?? "")
            soldServices.append(soldService)
        }
        return soldServices
    }
    private func calculateBillingPrice(totalIntialPrice: Double, totalMaintPrice: Double, billingFrequencySysName: String, defaultServiceFrequencySysName: String) -> Double {
        
        guard let frequencies = AppUserDefaults.shared.masterDetails?.frequencies else { return 0 }
        let frequencyBilling = frequencies.first { (frequency) -> Bool in
            return frequency.sysName == billingFrequencySysName
        }
        guard let fBilling = frequencyBilling else {
            return 0
        }
        let frequencyService = frequencies.first { (frequency) -> Bool in
            return frequency.sysName == defaultServiceFrequencySysName
        }
        guard let fService = frequencyService else {
            return 0
        }
        if fService.sysName == "OneTime" || fService.sysName?.lowercased() == "One Time" {
            return totalIntialPrice * Double(fService.yearlyOccurrence ?? 0) / Double(fBilling.yearlyOccurrence ?? 0)
        } else {
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            if dictLoginData.value(forKeyPath: "Company.CompanyConfig.MaintPriceFreq_1") as? Bool ==  true &&  fService.sysName != "Yearly" {
                return totalMaintPrice * (Double(fService.yearlyOccurrence ?? 0) - 1) / Double(fBilling.yearlyOccurrence ?? 0)
            } else {
                return totalMaintPrice * Double(fService.yearlyOccurrence ?? 0) / Double(fBilling.yearlyOccurrence ?? 0)
            }
        }
    }
    
    private func getEmployeeBranchName()  {
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        //self.dispatchGroup.enter(); print("Dispatch Group Main Entered")
        let dictLoginDataL = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        let strURL = "\(dictLoginDataL.value(forKeyPath: "Company.CompanyConfig.CoreServiceModule.ServiceUrl")!)" + UrlD2DGetEmployeeBranch + "companyKey=" + "\(dictLoginDataL.value(forKeyPath: "Company.CompanyKey")!)" + "&employeeId=" + "\(dictLoginDataL.value(forKey: "EmployeeId")!)"
        
        print("Employee Branch API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "GetEmployeeBranchName") { (response, status) in
            
//            DispatchQueue.main.async {
//
//                self.dispatchGroup.leave(); print("Dispatch Group Main Left")
//
//            }
            if(status)
            {
                
                let diff1 = CFAbsoluteTimeGetCurrent() - start1
                print("Took \(diff1) seconds to get Employee Branch API Data From SQL Server.")
                
                print("Employee Branch API Response")
                
                let arrKeys = response.allKeys as NSArray
                
                if arrKeys.contains("data") {
                    
                    if response.value(forKey: "data") is NSString {
                        
                       //
                        let dictLoginDataL = nsud.value(forKey: "LoginDetails") as! NSDictionary
                        let empBranchL = "\(dictLoginDataL.value(forKey: "EmployeeBranchSysName")!)"
                        let empBranchFromServer = "\(response.value(forKey: "data")!)"

                        if empBranchL != empBranchFromServer {
                            
                            // Show Alert for Different Branch
                            
                            DispatchQueue.main.async {

                                self.showAlertForBranchChange()
                        
                            }
                            
                        }

                    }
                    
                }
                
            }
            
        }
        
    }
    
    private func showAlertForBranchChange() {
        
        let alert = UIAlertController(title: alertMessage, message: "You have been moved to a different branch. Please logout & login back to the app and 'Sync Master'.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Logout", style: .destructive, handler: { (nil) in
            
            // Clear all userdeafults
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(true, forKey: "AllAppointments")
            UserDefaults.standard.set(true, forKey: "SortByScheduleDate")
            UserDefaults.standard.set(true, forKey: "SortByScheduleDateAscending")
            UserDefaults.standard.set(true, forKey: "SortByModifiedDateAscending")
            UserDefaults.standard.set(false, forKey: "graphDebug")
            
            UserDefaults.standard.set(NSArray(), forKey: "AppointmentStatus")
            UserDefaults.standard.set(true, forKey: "fromAppDelegate")
            
            UserDefaults.standard.set(false, forKey: "firstInstall_4Dec2018")
            UserDefaults.standard.set(false, forKey: "firstInstall_13June2019")
            UserDefaults.standard.set("No", forKey: "YesFromShortcut")
            UserDefaults.standard.set(true, forKey: "isEnableAutoPropertyInformation")

            // go to login view again
            
            self.goToLoginVC()
            
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            self.goToDashBoardVC()
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func goToLoginVC() {
        
        if(DeviceType.IS_IPAD){
            
            let mainStoryboard =  UIStoryboard.init(name: "MainiPad", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewControlleriPad") as! LoginViewControlleriPad
            self.navigationController?.pushViewController(controller, animated: false)
        }else{
            let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(controller, animated: false)
        }

    }
    
    private func goToDashBoardVC() {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        self.navigationController?.pushViewController(controller, animated: false)
        
    }
    
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        if let propertyDetails = propertyDetails {
            offlineDelegate?.upadateOflinePropertyInfo(propertyDetails)
        }
    }
    @IBAction func actionHome(_ sender: Any) {
        self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
        
    }
    @IBAction func actionGallery(_ sender: Any) {
        
        guard let destination = UIStoryboard(name: "Gallery", bundle: nil).instantiateViewController(withIdentifier: "GalleryListViewController") as? GalleryListViewController else {
            return
        }
        destination.delegate = self
        destination.images = propertyDetails?.galleryImages ?? []
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    @objc private func actionSelectService(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        services[sender.tag].isSelected = !services[sender.tag].isSelected
    }
    @IBAction func actionContinue(_ sender: Any) {
        print(billingAddress)
        var services = self.services.filter{$0.isSelected == true}
        var soldServices = self.soldServices
        if isEditable {
            soldServices = self.soldServices.filter{$0.isSelected == true}
        }
        guard !(propertyDetails?.propertyInfo?.firstName?.isEmpty ?? true) else {
            showAlert(message: "Please enter first name to continue")
            return
        }
        guard !(propertyDetails?.propertyInfo?.phone?.isEmpty ?? true) else {
            showAlert(message: "Please enter mobile number")
            return
        }
        
        if billingAddress?.billingAddressSameAsService == false {
            
            guard !(billingAddress?.tempFullBillingAddress?.isEmpty ?? true) else {
                showAlert(message: "Please enter billing address to continue")
                return
            }
            
            guard !(billingAddress?.billingFirstName?.isEmpty ?? true) else {
                showAlert(message: "Please enter billing first name to continue")
                return
            }
            
//            guard !(billingAddress?.billingLastName?.isEmpty ?? true) else {
//                showAlert(message: "Please enter billing last name to continue")
//                return
//            }
            
        }
        
        guard !isEditable || !services.isEmpty || !soldServices.isEmpty else {
            showAlert(message: "Please select a service to continue")
            return
        }
        
        guard services.first(where: { (service) -> Bool in
            return (service.defaultServiceFrequencySysName == nil) || (service.defaultServiceFrequencySysName ?? "").trimmed.isEmpty
        }) == nil else {
            showAlert(message: "Please add frequency for all the selected services")
            return
        }
        guard soldServices.first(where: { (service) -> Bool in
            return (service.frequencySysName == nil) || (service.frequencySysName ?? "").trimmed.isEmpty
        }) == nil else {
            showAlert(message: "Please add frequency for all the selected services")
            return
        }
        for (index, service) in services.enumerated() {
            var serviceT = service
            let price = AppUtility.getPrice(service, propertyDetails: propertyDetails?.propertyInfo)
            if service.manualMaintenancePrice == nil {
                serviceT.manualMaintenancePrice = price.maintaincePrice
            }
            if service.manualInitialPrice == nil {
                serviceT.manualInitialPrice = price.intialPrice
            }
            services[index] = serviceT
        }
        let combineServices = getSoldServiecs(from: services) + getSoldServiecs(from: soldServices)
        servicesToSync = combineServices
        
      /*  guard let destination = UIStoryboard(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "ServiceAgrementViewController") as? ServiceAgrementViewController else { return }
        destination.isEditable = isEditable
        destination.propertyDetails = propertyDetails
        destination.services = combineServices
        destination.delegateEdit = self
        destination.entityDetail = entityDetail
        destination.customerNotes = customerNotes
        destination.billingAddress = billingAddress
        self.navigationController?.pushViewController(destination, animated: true)*/
        
        
        if let status = entityDetail?.leadDetail?.statusSysName, let stage = entityDetail?.leadDetail?.stageSysName
        {
            if status == "Complete" && stage == "Won"
            {
                goToServiceAgreement()
            }
            else
            {
                sendAgreementOnContinue()

            }
        }
        else
        {
            sendAgreementOnContinue()

        }
    }
    
    func goToServiceAgreement()
    {
        
        guard let destination = UIStoryboard(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "ServiceAgrementViewController") as? ServiceAgrementViewController else { return }
        destination.isEditable = isEditable
        destination.propertyDetails = propertyDetails
        destination.services = servicesToSync
        destination.delegateEdit = self
        destination.entityDetail = entityDetail
        destination.customerNotes = customerNotes
        destination.billingAddress = billingAddress
        destination.selectedLocation = self.selectedLocation
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func sendAgreementOnContinue()
    {
        showLoader()

        guard let propertyDetails = propertyDetails else {
            return
        }
        switch propertyDetails.propertyType {
        case .lead:
            
            sendAgreement = self.getLeadAgreement()
            presenterServiceReport.apiCallForSendMail(sendAgreement)

            break
        case .account:
            sendAgreement = self.getAccountAgreement()
            presenterServiceReport.apiCallForSendMail(sendAgreement)

            break
        case .opportunity:
            sendAgreement = self.getOpportunityAgreement()
            presenterServiceReport.apiCallForSendMail(sendAgreement)

            break
        case .empty:
            sendAgreement = self.getNewSendAggrement()
            presenterServiceReport.apiCallForSendMail(sendAgreement)

            break
        }
    }
    
    @IBAction func actionNoThanks(_ sender: Any) {
        editVisit(.visit)
        logType = .visit
    }
    @objc func actionEdit(_ sender: Any) {
        editVisit(.edit)
        logType = .edit
    }
    @objc func actionEditBillingAddress(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "BillingAddressD2DViewController") as? BillingAddressD2DViewController else { return }
        destination.modalPresentationStyle = .overFullScreen
        destination.delegate = self
        destination.propertyDetails = propertyDetails
        destination.entityDetail = entityDetail
        destination.billingAddress = billingAddress
        destination.isEditBillingAddress = true
        
        self.present(destination, animated: true, completion: nil)
        
    }
    
}

// MARK: - UITableViewDataSource
extension VisitServiceDetailViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return services.count
        } else if  section == 2 {
            return soldServices.count
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PropertyInfoTableViewCell.self), for: indexPath) as! PropertyInfoTableViewCell
            cell.btnAddService.tag = indexPath.row
            cell.btnAddService.addTarget(self, action: #selector(actionAddService), for: .touchUpInside)
            cell.btnBillingAddress.isUserInteractionEnabled = isEditable
            cell.btnBillingAddress.addTarget(self, action: #selector(actionBillingAddressChange), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(actionEdit), for: .touchUpInside)
            cell.btnEditBillingAddress.addTarget(self, action: #selector(actionEditBillingAddress), for: .touchUpInside)
            
            cell.lblBillingAddress.text = billingAddress?.tempFullBillingAddress
            cell.btnBillingAddress.isSelected = billingAddress?.billingAddressSameAsService ?? true
            cell.lblBillingAddress.isHidden = billingAddress?.billingAddressSameAsService ?? true
            if (propertyDetails?.propertyType == .lead ||  propertyDetails?.propertyType == .empty) && isEditable {
                if  let leadDetail = propertyDetails?.lead, let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == leadDetail.statusName}).first {
                    if leadsStatus.name == "Converted" {
                        cell.btnEdit.isHidden = true
                    } else {
                        cell.btnEdit.isHidden = false
                    }
                } else {
                    cell.btnEdit.isHidden = false
                }
                
            }else if (propertyDetails?.propertyType == .opportunity) && isEditable{
                cell.btnEdit.isHidden = false
            } else {
                cell.btnEdit.isHidden = true
            }
            if isEditable {
                cell.btnAddService.isHidden = false
            } else {
                cell.btnAddService.isHidden = true
            }
            
            if !cell.btnBillingAddress.isSelected {

                cell.btnEditBillingAddress.isHidden = false
                
                let statusL = "\(entityDetail?.leadDetail?.statusSysName ?? "")"
                let stageL = "\(entityDetail?.leadDetail?.stageSysName ?? "")"

                if statusL.caseInsensitiveCompare("complete") == .orderedSame  &&
                    stageL.caseInsensitiveCompare("Won") == .orderedSame{
                    
                    cell.btnEditBillingAddress.isHidden = true

                }
                
            } else {

                cell.btnEditBillingAddress.isHidden = true
                
            }
            
            cell.propertyInfo = propertyDetails?.propertyInfo
            
            //cell.lblName.text = "\(entityDetail?.leadDetail?.billingFirstName ?? "")" + " \(entityDetail?.leadDetail?.billingLastName ?? "")"
            
            return cell
        } else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServiceDetailTableViewCell.self), for: indexPath) as! ServiceDetailTableViewCell
            
            let solidService = soldServices[indexPath.row]
            cell.soldService = solidService
            
            cell.lblInitialPrice.text = solidService.totalInitialPrice?.currency
            cell.lblMaintanence.text = solidService.totalMaintPrice?.currency
            cell.delegate = self
            if !plusServices.filter({ (serviceT) -> Bool in
                return serviceT.parentID == solidService.serviceID
            }).isEmpty, isEditable {
                cell.btnAddOn.isHidden = false
            } else {
                cell.btnAddOn.isHidden = true
            }
            if isEditable {
                cell.btnTrash.isHidden = false
                cell.btnCheckBox.isHidden = false
                cell.btnEditService.isHidden = false
            } else {
                cell.btnTrash.isHidden = true
                cell.btnCheckBox.isHidden = true
                cell.btnEditService.isHidden = true
            }
            
            return cell
        } else  if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ServiceDetailTableViewCell.self), for: indexPath) as! ServiceDetailTableViewCell
            
            let service = services[indexPath.row]
            cell.service = service
            
            if service.isParameterizedPriced == true {
                let price = getParameterizedPrice(service)
                cell.lblInitialPrice.text = price.intialPrice.currency
                cell.lblMaintanence.text = price.maintaincePrice.currency
            } else {
                let price = getNonParameterizedPrice(service)
                cell.lblInitialPrice.text = price.intialPrice.currency
                cell.lblMaintanence.text = price.maintaincePrice.currency
            }
            cell.delegate = self
            if !plusServices.filter({ (serviceT) -> Bool in
                return serviceT.parentID == service.serviceMasterID
            }).isEmpty, service.isPlusService == false, isEditable {
                cell.btnAddOn.isHidden = false
            } else {
                cell.btnAddOn.isHidden = true
            }
            if isEditable {
                cell.btnTrash.isHidden = false
                cell.btnCheckBox.isHidden = false
                cell.btnEditService.isHidden = false
            } else {
                cell.btnTrash.isHidden = true
                cell.btnCheckBox.isHidden = true
                cell.btnEditService.isHidden = true
                
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AgreementNotesTableViewCell.self), for: indexPath) as! AgreementNotesTableViewCell
            cell.lblTitle.text = "Customer Notes"
            cell.notesType = .customerN
            cell.txtView.text =  entityDetail?.paymentInfo?.specialInstructions?.trimmed
            customerNotes = cell.txtView.text
            cell.delegate = self
            return cell
        }
    }
}
// MARK: - ServiceDetailCellDelegate
extension VisitServiceDetailViewController: ServiceDetailCellDelegate {
    
    func actionAddOnService(_ cell: ServiceDetailTableViewCell) {
        
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AddServiceViewController") as? AddServiceViewController else {
            return
        }
        destination.delegate = self
        if indexPath.section == 2 {
            let soldService = soldServices[indexPath.row]
            destination.soldService = soldService
        } else {
            let service = services[indexPath.row]
            destination.service = service
        }
        destination.services = services
        destination.soldServices = soldServices
        destination.propertyDetails = propertyDetails
        destination.serviceActionType = .adAddon
        self.present(destination, animated: true, completion: nil)
    }
    
    func actionEditService(_ cell: ServiceDetailTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "EditServiceViewController") as? EditServiceViewController else {
            return
        }
        if indexPath.section == 2 {
            let service = soldServices[indexPath.row]
            destination.solidService = service
            destination.initialPrice = service.totalInitialPrice
            destination.maintenanacePirce = service.totalMaintPrice
        } else {
            let service = services[indexPath.row]
            let price = getParameterizedPrice(service)
            destination.service = service
            destination.initialPrice = price.intialPrice
            destination.maintenanacePirce = price.maintaincePrice
        }
        destination.propertyInfo = propertyDetails?.propertyInfo
        destination.delegate = self
        self.present(destination, animated: true, completion: nil)
    }
    
    func actionSelectService(_ cell: ServiceDetailTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        cell.btnCheckBox.isSelected = !cell.btnCheckBox.isSelected
        
        if indexPath.section == 2 {
            soldServices[indexPath.row].isSelected = !soldServices[indexPath.row].isSelected
        } else {
            services[indexPath.row].isSelected = !services[indexPath.row].isSelected
        }
    }
    func actionDeleteService(_ cell: ServiceDetailTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        if indexPath.section == 2 {
            soldServices.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else {
            services.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
    }
}
// MARK: - UITableViewDelegate
extension VisitServiceDetailViewController: EditServiceProtocol {
    func editService(service: Service?, soldService: SoldServiceStandardDetail?) {
        if let selectedService = service {
            if let index = services.firstIndex(where: { (service) -> Bool in
                return selectedService.serviceMasterID == service.serviceMasterID
            }) {
                self.services[index] = selectedService
                //tableView.reloadRows(at: [IndexPath(row: index, section: 1)], with: .automatic)
                self.tableView.reloadData()
            }
        } else if let solidService = soldService {
            if let index = soldServices.firstIndex(where: { (service) -> Bool in
                return solidService.serviceID == service.serviceID
            }) {
                self.soldServices[index] = solidService
                //tableView.reloadRows(at: [IndexPath(row: index, section: 2)], with: .automatic)
                self.tableView.reloadData()
            }
        }
    }
}
// MARK: - UITableViewDelegate
extension VisitServiceDetailViewController: AddServiceProtocol {
    func addService(_ selectedService: Service) {
        services.insert(selectedService, at: 0)
        tableView.reloadData()
    }
    func addAddon(_ selectedService: Service, parentService: Service?, soldService: SoldServiceStandardDetail?) {
        if let parentService = parentService {
            if let index = services.firstIndex(where: { (service) -> Bool in
                return parentService.serviceMasterID == service.serviceMasterID
            }) {
                services.insert(selectedService, at: index+1)
                tableView.insertRows(at: [IndexPath(row: index+1, section: 1)], with: .automatic)
            }
        } else {
            if let index = self.soldServices.firstIndex(where: { (service) -> Bool in
                return soldService?.serviceID == service.serviceID
            }) {
                services.insert(selectedService, at: index+1)
                tableView.insertRows(at: [IndexPath(row: index+1, section: 2)], with: .automatic)
            }
        }
    }
}

// MARK: - Convenience
extension VisitServiceDetailViewController {
    private func getParameterizedPrice(_ service: Service) -> (intialPrice: Double, maintaincePrice: Double) {
        let price = AppUtility.getPrice(service, propertyDetails: propertyDetails?.propertyInfo)
        return (service.manualInitialPrice ?? price.intialPrice, service.manualMaintenancePrice ?? price.maintaincePrice)
    }
    private func getNonParameterizedPrice(_ service: Service) -> (intialPrice: Double, maintaincePrice: Double) {
        return (service.manualInitialPrice ?? 0, service.manualMaintenancePrice ?? 0)
    }
    private func editVisit(_ logType: LogType) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "VisitLogViewController") as? VisitLogViewController {
            destination.propertyDetails = propertyDetails
            destination.logType = logType
            destination.delegate = self
            destination.selectedLocation = self.selectedLocation
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    @objc private func actionEditService(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "EditServiceViewController") as? EditServiceViewController {
            let service = services[sender.tag]
            let price = getParameterizedPrice(service)
            destination.service = service
            destination.initialPrice = price.intialPrice
            destination.maintenanacePirce = price.maintaincePrice
            destination.delegate = self
            self.present(destination, animated: true, completion: nil)
        }
    }
    
    @objc private func actionAddService(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AddServiceViewController") as? AddServiceViewController else { return }
        
        destination.services = services
        destination.soldServices = soldServices
        destination.delegate = self
        destination.propertyDetails = propertyDetails
        self.present(destination, animated: true, completion: nil)
        
    }
    @objc private func actionBillingAddressChange(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.billingAddress?.billingAddressSameAsService = sender.isSelected
        if !sender.isSelected {
            let storyboard = UIStoryboard(name: "Service", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "BillingAddressD2DViewController") as? BillingAddressD2DViewController else { return }
            destination.modalPresentationStyle = .overFullScreen
            destination.delegate = self
            destination.propertyDetails = propertyDetails
            destination.entityDetail = entityDetail
            destination.billingAddress = billingAddress
            destination.isEditBillingAddress = true
            self.present(destination, animated: true, completion: nil)
        } else {
            setBillingAddressData()
            self.billingAddress?.billingAddressSameAsService = sender.isSelected
            tableView.reloadData()
        }
        
    }
    @objc private func actionAddOn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AddServiceViewController") as? AddServiceViewController else { return }
        destination.delegate = self
        destination.services = services
        destination.soldServices = soldServices
        
        let service = services[sender.tag]
        destination.service = service
        destination.propertyDetails = propertyDetails
        destination.serviceActionType = .adAddon
        self.present(destination, animated: true, completion: nil)
    }
    
    @objc private func fetchServices() {
        // FTIndicator.dismissProgress()
        guard let serviceDetails = AppUserDefaults.shared.masterDetails else { return }
        services = serviceDetails.categories.flatMap { $0.services}.filter{$0.isActive == true && ($0.isPlusService == false || $0.isPlusService == nil) && $0.isDoortodoor == true}
        plusServices = serviceDetails.categories.flatMap { $0.services}.filter{$0.isActive == true && $0.isPlusService == true}
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func setBillingAddressData() {
        if let entityDetail = entityDetail , let leadDetail = entityDetail.leadDetail {
            var billingAddress = SendAgreement()
            
            var stateName = ""
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateSysName == leadDetail.billingState
            }) {
                stateName = state.stateSysName ?? ""
            }
            let address = "\(leadDetail.billingAddress1 ?? ""), \(leadDetail.billingCity ?? ""), \(stateName), \(leadDetail.billingZipcode ?? "")"
            billingAddress.billingFirstName = leadDetail.billingFirstName
            billingAddress.billingLastName = leadDetail.billingLastName ?? ""
            billingAddress.billingPrimaryEmail =  leadDetail.billingPrimaryEmail
            billingAddress.billingCellPhone1 = leadDetail.billingPrimaryPhone//leadDetail.billingCellNo
            billingAddress.tempFullBillingAddress = address
            billingAddress.billingAddressSameAsService = entityDetail.leadDetail?.isBillingAddressSame ?? true
            self.billingAddress = billingAddress
         } else if let propertyDetails = propertyDetails {
            var billingAddress = SendAgreement()
            switch propertyDetails.propertyType {
            case .lead:
                if let lead = propertyDetails.lead {
                    billingAddress.billingAddressSameAsService = true
                    var stateName = ""
                    if let stateID = lead.stateID {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        let address = "\(lead.address1 ?? ""), \(lead.cityName ?? ""), \(stateName), \(lead.zipcode ?? "")"
                        billingAddress.tempFullBillingAddress = address
                    }
                }
            case .empty:
                if let propertyInfo = propertyDetails.propertyInfo {
                    billingAddress.tempFullBillingAddress = propertyInfo.address
                }
                billingAddress.billingAddressSameAsService = true
            case .opportunity:
                if let opportunity = propertyDetails.opportunity {
                    
                    billingAddress.billingAddressSameAsService  = opportunity.billingAddressSameAsService ?? true
                    var stateName = ""
                    if let billingAddressOld = opportunity.billingAddress {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == billingAddressOld.stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        
                        let address = "\(billingAddressOld.address1 ?? ""), \(billingAddressOld.cityName ?? ""), \(stateName), \(billingAddressOld.zipcode ?? "")"
                        billingAddress.tempFullBillingAddress = address
                    }
                }
            case .account:
                if let account = propertyDetails.account {
                    // btnBillingAddress.isSelected = account.billingAddressSameAsService ?? true
                    var stateName = ""
                    if let billingAddressOld = account.billingAddress {
                        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                            return state.stateID == billingAddressOld.stateID
                        }) {
                            stateName = state.stateSysName ?? ""
                            billingAddress.billingStateId = state.stateID
                        }
                        
                        let address = "\(billingAddressOld.address1 ?? ""), \(billingAddressOld.cityName ?? ""), \(stateName), \(billingAddressOld.zipcode ?? "")"
                        billingAddress.tempFullBillingAddress = address
                    }
                }
            }
            //            billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
            //            billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
            //            billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
            //            billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
            self.billingAddress = billingAddress
         }
    }
}


extension VisitServiceDetailViewController : ServiceView {
    func gotSeriveList(_ output: EntityDetail) {
        if let soldServiceStandardDetail = output.soldServiceStandardDetail, !soldServiceStandardDetail.isEmpty {
            self.soldServices = soldServiceStandardDetail
        } else {
            fetchServices()
        }
        entityDetail = output
                
        delegateEdit?.updateEditStatus(entityDetail, propertyDetail: propertyDetails)
        
        propertyDetails?.propertyInfo =  AppUtility.getPropertyDetailFromLeadDetail((entityDetail?.leadDetail)!)
                
        self.propertyDetails?.galleryImages = output.imagesDetail ?? []
        isEditable = AppUtility.setEditable(entityDetail)
    }
}
extension VisitServiceDetailViewController: EditLeadDelegate {
    func upadatePropertyInfo(_ propertyDetails: PropertyDetails) {
        self.propertyDetails = propertyDetails
        for (index, service) in services.enumerated() {
            var serviceT = service
            //Nilind
            if nsud.bool(forKey: "ChangeInParameter") == true && serviceT.isParameterizedPriced == true
            {
                serviceT.manualMaintenancePrice = nil
                serviceT.manualInitialPrice = nil
            }
            
            //serviceT.manualMaintenancePrice = nil
            //serviceT.manualInitialPrice = nil
            services[index] = serviceT
        }
        tableView.reloadData()
        if self.logType == .visit {
            delegate?.upadatePropertyInfo(propertyDetails)
        } else {
            offlineDelegate?.upadateOflinePropertyInfo(propertyDetails)
        }
    }
}

// MARK: - EditableStatusDelegate
extension VisitServiceDetailViewController: EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?) {
        self.entityDetail = entityDetail
        self.propertyDetails = propertyDetail
        isEditable = AppUtility.setEditable(entityDetail)
        delegateEdit?.updateEditStatus(entityDetail, propertyDetail: propertyDetails)
    }
}
extension VisitServiceDetailViewController: AgreementNotesCellDelegate {
    func updatedTextForNotes(_ notes: String, notesType: NotesType) {
        customerNotes = notes
    }
}
extension VisitServiceDetailViewController: ImagesUpdated {
    func imagesUpdates(_ images: [DTDLeadImagesDetailExtSerDc]) {
        self.propertyDetails?.galleryImages = images
    }
}
extension VisitServiceDetailViewController: BillingAddressDelegate {
    func actionBillingAddress(_ isSelected: Bool) {
        if var billingAddress = billingAddress {
          billingAddress.billingFirstName = ""
          billingAddress.billingLastName = ""
          billingAddress.billingPrimaryEmail =  ""
          billingAddress.billingCellPhone1 = ""
          billingAddress.billingAddressSameAsService = isSelected
          billingAddress.tempFullBillingAddress =  propertyDetails?.propertyInfo?.address
          self.billingAddress = billingAddress
        }
        tableView.reloadData()
    }
    
    func billingDetailEntered(_ billingAddress: SendAgreement) {
        self.billingAddress = billingAddress
        tableView.reloadData()
    }
    func addAddressPressed() {
        
    }
}

extension VisitServiceDetailViewController: ServiceReportView
{
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        
    }
    
    func mailSendSuccessfully(_ output: EntityDetail)
    {
        if output.leadDetail?.leadNumber != nil
        {
            self.hideLoader()
            
            self.entityDetail = output
            
            //delegateEdit?.updateEditStatus(entityDetail, propertyDetail: propertyDetails)
            
            //Nilind
//            self.entityDetail?.webLeadId = output.webLeadId
//            self.entityDetail?.leadNumber = output.leadNumber
//            self.entityDetail?.thirdPartyAccountId = output.thirdPartyAccountId
//            self.entityDetail?.billToLocationId = output.billToLocationId
            nsud.setValue(output.webLeadId, forKey: "webLeadIdD2D")
            nsud.setValue(output.leadNumber, forKey: "leadNumberD2D")
            nsud.synchronize()

            //NotificationCenter.default.post(name: .didUCreatedLead, object: propertyDetails)
            //self.performSegue(withIdentifier: "UnwindToAgreement", sender: self)
            
            goToServiceAgreement()
            
        }
        else
        {
            self.hideLoader()
           
//            nsud.setValue(output.webLeadId, forKey: "webLeadIdD2D")
//            nsud.setValue(output.leadNumber, forKey: "leadNumberD2D")
//            nsud.synchronize()
            //self.showAlert(message: "Unable to send the Agreement, please try again later")
        }
    }
    func resendAgreementSuccessfully(_ output: Bool) {
       
    }
}

extension VisitServiceDetailViewController {
    
    private func getCommonAgreement() -> SendAgreement {
        var agreement = SendAgreement()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        agreement.companyKey = companyKey
        agreement.assignedToType = "Individual"
        agreement.addressSubType = "Residential"
        agreement.soldServiceStandardDetail  = servicesToSync
        
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        agreement.scheduleDate = currentDate
        agreement.scheduleTime = currentTime
        if let source = AppUserDefaults.shared.leadDetailMaster?.sourceMaster.filter({$0.sourceSysName == "DoorToDoor"}).first {
            agreement.sourceIDS = [source.sourceId]
        }
        agreement.country = "USA"
        agreement.countryID = 1
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId") ?? "")"
        agreement.fieldSalesPerson = assignedToID
        agreement.assignedToID = assignedToID
        agreement.addressImagePath = propertyDetails?.propertyInfo?.imageUrl?.components(separatedBy: "\\").last
        if let billingAddress = billingAddress {
            //billingAddress.billingCellPhone1 == nil ? "" : billingAddress.billingCellPhone1!
            agreement.billingAddress1 = billingAddress.billingAddress1
            agreement.billingCityName = billingAddress.billingCityName
            agreement.billingStateId = billingAddress.billingStateId
            agreement.billingZipcode = billingAddress.billingZipcode
            agreement.billingCellPhone1 = getOnlyNumberFromString(number: billingAddress.billingCellPhone1 == nil ? "" : billingAddress.billingCellPhone1!)//billingAddress.billingCellPhone1
            agreement.billingFirstName = billingAddress.billingFirstName
            agreement.billingLastName = billingAddress.billingLastName
            agreement.billingPrimaryEmail = billingAddress.billingPrimaryEmail
            agreement.billingAddressSameAsService = billingAddress.billingAddressSameAsService ?? true
            agreement.billingState = billingAddress.billingState
            agreement.billingCountry = billingAddress.billingCountry ?? "USA"
            agreement.billingCountryId =  billingAddress.billingCountryId ?? "1"
            agreement.billingPOCId = billingAddress.billingPOCId
            
        }
        agreement.secondCall = false
        agreement.webLeadID = entityDetail?.webLeadId
        agreement.leadNumber = entityDetail?.leadNumber
        agreement.accountID = propertyDetails?.opportunity?.accountID
        agreement.latitude = selectedLocation!.coordinate.latitude
        agreement.longitude = selectedLocation!.coordinate.longitude
        return agreement
        
    }
    private func getNewSendAggrement() -> SendAgreement {
        
        guard let  propertyDetails = propertyDetails, let propertyInfo = propertyDetails.propertyInfo else { return SendAgreement() }
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        
        var agreement = getCommonAgreement()
        agreement.flowType =  "Residential"
        agreement.companyKey = companyKey
        //agreement.soldServiceStandardDetail  = services
        agreement.area = propertyInfo.area
        agreement.noofBathroom = propertyInfo.bathRoom
        agreement.noofBedroom = propertyInfo.bedRoom
        agreement.noofStory = propertyInfo.noOfFloor
        agreement.primaryEmail = propertyInfo.email ?? ""
        agreement.cellPhone1 = getOnlyNumberFromString(number: propertyInfo.phone ?? "")//propertyInfo.phone ?? ""
        
        agreement.cityName = propertyInfo.city ?? propertyDetails.propertyInfo?.placeMark?.subLocality
        agreement.zipcode =  propertyInfo.zipCode ?? propertyDetails.propertyInfo?.placeMark?.postalCode
        agreement.firstName =  String(propertyInfo.firstName?.prefix(50) ?? "")
        agreement.lastName =  String(propertyInfo.lastName?.prefix(50) ?? "")
        agreement.address1 = String(propertyInfo.address?.prefix(100) ?? "")
        let array = String(propertyInfo.address?.prefix(100) ?? "").components(separatedBy: ", ") as NSArray
        if array.count > 0
        {
            agreement.address1 = array[0] as? String
        }

        agreement.countryID = 1
        agreement.visitCount = 1
        agreement.stateID = propertyDetails.propertyInfo?.state?.stateID
        agreement.state = propertyDetails.propertyInfo?.state?.stateSysName
        agreement.branchSysName = barnchSysName
        
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == servicesToSync.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        /*
         if let categorySysName = services.first?.categorySysName, let departmentSysName = AppUserDefaults.shared.masterDetails?.categories.filter({$0.sysName == categorySysName}).first?.departmentSysName {
         agreement.departmentSysName = departmentSysName
         }*/
        agreement.leadDetail = getCommonLead()
        //agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        agreement.paymentInfo = getPaymentInfo(nil)
        agreement.leadAppliedDiscounts = getDiscountInfo(nil)
        return agreement
    }
    private func getLeadAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        guard let leadDetail = propertyDetails?.lead else { return agreement }
        // agreement.assignedToID =  leadDetail.assignedToID?.description
        agreement.flowType =  leadDetail.flowType ?? "Residential"
        agreement.companyKey = leadDetail.companyKey ?? ""
        agreement.area = leadDetail.area  ?? ""
        agreement.noofBathroom = leadDetail.noofBathroom ?? ""
        agreement.noofBedroom = leadDetail.noofBedroom ?? ""
        agreement.noofStory = leadDetail.noofStory  ?? ""
        agreement.primaryEmail = leadDetail.primaryEmail  ?? ""
        agreement.cellPhone1 = getOnlyNumberFromString(number: leadDetail.primaryPhone ?? "")//leadDetail.primaryPhone ?? ""
        agreement.cityName = leadDetail.cityName ?? ""
        agreement.zipcode =  leadDetail.zipcode ?? ""
        agreement.firstName =  String(leadDetail.firstName?.prefix(50) ?? "")
        agreement.lastName = String(leadDetail.lastName?.prefix(50) ?? "")
        agreement.address1 =  String(leadDetail.address1?.prefix(100) ?? "")
        agreement.address2 = String(leadDetail.address2?.prefix(100) ?? "")
        agreement.visitStatusID = leadDetail.visitStatusID
        agreement.emailDetail = entityDetail?.emailDetail
        agreement.documentsDetail = entityDetail?.documentDetail
        agreement.stateID = leadDetail.stateID
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            agreement.state = state.stateSysName
        }
        
        agreement.visitCount = leadDetail.visitCount
        agreement.objectionIds = leadDetail.objectionIds
        agreement.currentServicesProvider = leadDetail.currentServicesProvider
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
//        if let categories = AppUserDefaults.shared.masterDetails?.categories {
//            for category in categories {
//                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
//                    agreement.departmentSysName = category.departmentSysName
//                    break
//                }
//            }
//        }
//        agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        
        
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == servicesToSync.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        
        return agreement
    }
    
    
    private func getOpportunityAgreement() -> SendAgreement {
        var agreement = getCommonAgreement()
        
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        if let opportunityDetails = propertyDetails?.opportunity {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            //agreement.leadNumber = entityDetail?.leadDetail?.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = getOnlyNumberFromString(number: opportunityDetails.primaryPhone ?? "")//opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            agreement.flowType =  opportunityDetails.opportunityType  ?? "Residential"
            if let serviceAddress = opportunityDetails.serviceAddress {
                agreement.area = serviceAddress.area ?? ""
                agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
                agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
                agreement.noofStory = serviceAddress.noofStory ?? ""
                agreement.address1 = serviceAddress.address1 ?? ""
                agreement.address2 = serviceAddress.address2 ?? ""
                agreement.cityName = serviceAddress.cityName ?? ""
                agreement.zipcode =  serviceAddress.zipcode ?? ""
                agreement.stateID = serviceAddress.stateID
                if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                    return state.stateID == serviceAddress.stateID
                }) {
                    agreement.state = state.stateSysName
                }
            }
            
            agreement.leadNumber = propertyDetails?.opportunity?.opportunityNumber
            agreement.leadId = propertyDetails?.opportunity?.opportunityID
            
        } else if let opportunityDetails = entityDetail?.leadDetail {
            agreement.firstName =  String(opportunityDetails.firstName?.prefix(50) ?? "")
            agreement.lastName = String(opportunityDetails.lastName?.prefix(50) ?? "")
            agreement.leadNumber = opportunityDetails.leadNumber?.description
            agreement.primaryEmail = opportunityDetails.primaryEmail ?? ""
            agreement.cellPhone1 = getOnlyNumberFromString(number: opportunityDetails.primaryPhone ?? "")//opportunityDetails.primaryPhone ?? ""
            agreement.branchSysName = opportunityDetails.branchSysName ?? ""
            agreement.departmentSysName = opportunityDetails.departmentSysName ?? ""
            agreement.flowType =  opportunityDetails.flowType ?? "Residential"
            agreement.area = opportunityDetails.area ?? ""
            agreement.noofBathroom = opportunityDetails.noofBathroom ?? ""
            agreement.noofBedroom = opportunityDetails.noofBedroom ?? ""
            agreement.noofStory = opportunityDetails.noOfStory ?? ""
            agreement.address1 = opportunityDetails.servicesAddress1 ?? ""
            agreement.address2 = opportunityDetails.serviceAddress2 ?? ""
            agreement.cityName = opportunityDetails.serviceCity ?? ""
            agreement.zipcode =  opportunityDetails.serviceZipcode ?? ""
            agreement.state = opportunityDetails.serviceState
            agreement.webLeadID = propertyDetails?.lead?.leadID
        }
        agreement.electronicAuthorizationForm = entityDetail?.electronicAuthorizationForm
        return agreement
    }
    private func getAccountAgreement() -> SendAgreement {
        
        var agreement = getCommonAgreement()
        agreement.leadDetail = getCommonLead(entityDetail?.leadDetail)
        agreement.paymentInfo = getPaymentInfo(entityDetail?.paymentInfo)
        agreement.leadAppliedDiscounts = getDiscountInfo(entityDetail?.leadAppliedDiscounts)
        guard let accountDetails = propertyDetails?.account else { return agreement }
        agreement.firstName =  String(accountDetails.firstName?.prefix(50) ?? "")
        agreement.lastName = String(accountDetails.lastName?.prefix(50) ?? "")
        agreement.primaryEmail = accountDetails.primaryEmail
        agreement.cellPhone1 = getOnlyNumberFromString(number: accountDetails.primaryPhone == nil ? "" : accountDetails.primaryPhone!)//accountDetails.primaryPhone
        agreement.flowType =  "Residential"
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        agreement.branchSysName = barnchSysName
//        if let categories = AppUserDefaults.shared.masterDetails?.categories {
//            for category in categories {
//                if category.services.filter({$0.serviceMasterID == services.first?.serviceID}).first != nil {
//                    agreement.departmentSysName = category.departmentSysName
//                    break
//                }
//            }
//        }
        
        if let categories = AppUserDefaults.shared.masterDetails?.categories {
            for category in categories {
                if category.services.filter({$0.serviceMasterID == servicesToSync.first?.serviceID}).first != nil {
                    agreement.departmentSysName = category.departmentSysName
                    break
                }
            }
        }
        
        if let serviceAddress = accountDetails.serviceAddress {
            agreement.area = serviceAddress.area ?? ""
            agreement.noofBathroom = serviceAddress.noofBathroom ?? ""
            agreement.noofBedroom = serviceAddress.noofBedroom ?? ""
            agreement.noofStory = serviceAddress.noofStory ?? ""
            agreement.address1 = String(serviceAddress.address1?.prefix(100) ?? "")
            agreement.address2 = String(serviceAddress.address2?.prefix(100) ?? "")
            agreement.cityName = serviceAddress.cityName ?? ""
            agreement.zipcode =  serviceAddress.zipcode ?? ""
            agreement.stateID = serviceAddress.stateID
            agreement.flowType = "Residential"
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateID == serviceAddress.stateID
            }) {
                agreement.state = state.stateSysName
            }
        }
        //agreement.electronicAuthorizationForm = self.electronicAuthorizationForm
        return agreement
    }
    private func getNewLeads() -> LeadDetail? {
        
        var leadDetail = LeadDetail()
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "yyyy/MM/dd")
        let currentTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        leadDetail.flowType = "Residential"
        leadDetail.scheduleTime = currentTime
        leadDetail.scheduleDate = currentDate
        //leadDetail.isCustomerNotPresent = signature.customerNotPresent
        leadDetail.latitude = propertyDetails?.propertyInfo?.latitude
        leadDetail.longitude = propertyDetails?.propertyInfo?.longitude
        
        return leadDetail
    }
    private func getCommonLead(_ lead: LeadDetail? = nil) -> LeadDetail? {
        
        var leadDetail = lead
        
        if leadDetail == nil {
            leadDetail = getNewLeads()
        }
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String

        leadDetail?.versionDate = VersionDate
        leadDetail?.versionNumber = VersionNumber
        let deviceVersion = UIDevice.current.systemVersion
        let deviceName = UIDevice.current.name
        leadDetail?.deviceName = deviceName
        leadDetail?.deviceVersion = deviceVersion
        
        if AppUserDefaults.shared.isPreSetSignSales, let serviceTechSignPath = AppUserDefaults.shared.serviceTechSignPath?.trimmed, !serviceTechSignPath.isEmpty {
            if  let urlString = serviceTechSignPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {
                leadDetail?.isEmployeePresetSignature = true
            }
        }
        
        //Nilind 
        if propertyDetails?.propertyType == .lead
        {
            var strStage = leadDetail?.stageSysName
            var strStatus = leadDetail?.statusSysName
            
            if strStage?.count == 0 || strStage == nil
            {
                strStage = "Scheduled"
            }
            if strStatus?.count == 0 || strStatus == nil
            {
                strStatus = "Open"
            }

            leadDetail?.stageSysName = "\(strStage ?? "")"
            leadDetail?.statusSysName = "\(strStatus ?? "")"
        }
        else if propertyDetails?.propertyType == .account
        {
            var strStage = leadDetail?.stageSysName
            var strStatus = leadDetail?.statusSysName
            
            if strStage?.count == 0 || strStage == nil
            {
                strStage = "Scheduled"
            }
            if strStatus?.count == 0 || strStatus == nil
            {
                strStatus = "Open"
            }

            leadDetail?.stageSysName = "\(strStage ?? "")"
            leadDetail?.statusSysName = "\(strStatus ?? "")"
        }
        else if (propertyDetails?.propertyType == .opportunity)
        {
            var strStage = leadDetail?.stageSysName
            var strStatus = leadDetail?.statusSysName
            
            if strStage?.count == 0 || strStage == nil
            {
                strStage = "Scheduled"
            }
            if strStatus?.count == 0 || strStatus == nil
            {
                strStatus = "Open"
            }

            leadDetail?.stageSysName = "\(strStage ?? "")"
            leadDetail?.statusSysName = "\(strStatus ?? "")"
        }
        else if (propertyDetails?.propertyType == .empty)
        {
            leadDetail?.stageSysName = "Scheduled"
            leadDetail?.statusSysName = "Open"
        }
        else
        {
            leadDetail?.stageSysName = "Scheduled"
            leadDetail?.statusSysName = "Open"
        }
        
        
        return leadDetail
    }
    private func getPaymentInfo(_ paymentInfo: PaymentInfo?) -> PaymentInfo? {
        var paymentInfo = paymentInfo
        if paymentInfo == nil {
            paymentInfo = PaymentInfo()
        }
//        paymentInfo?.paymentMode = paymentMode.mode
//        paymentInfo?.amount = signature.amount
//        paymentInfo?.salesSignature = signature.salesSignature ?? ""
//        paymentInfo?.customerSignature = signature.customerSignature ?? ""
//        paymentInfo?.specialInstructions = signature.customerNotes ?? ""
        return paymentInfo
    }
    
    private func getDiscountInfo(_ discountsInfo: [LeadAppliedDiscount]?) -> [LeadAppliedDiscount]? {
        var discountsInfo: [LeadAppliedDiscount] = []
        return discountsInfo
    }
}
