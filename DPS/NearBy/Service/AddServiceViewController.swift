//
//  AddServiceViewController.swift
//  DPS
//
//  Created by Vivek Patel on 24/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol AddServiceProtocol: NSObjectProtocol {
    func addService(_ selectedService: Service)
    func addAddon(_ selectedService: Service, parentService: Service?, soldService: SoldServiceStandardDetail?)
}

enum ServiceAction {
    case addService
    case adAddon
}

final class AddServiceViewController: BaseViewController {
    
    // MARK: - Variables
    var service: Service?
    var services: [Service] = []
    var soldService: SoldServiceStandardDetail?
    var soldServices: [SoldServiceStandardDetail] = []
    var serviceActionType: ServiceAction = .addService
    weak var delegate:AddServiceProtocol?
    var propertyDetails: PropertyDetails?
    private var selectedService: Service?
    private var selectedFrequency: Frequency?
    private var selectedCategory: Category?
    private var selectedPackage: ServicePackageDc?
    private var servicesPricingDetailDcs: ServicePricingDetail?
    // MARK: - Outlets
    @IBOutlet weak var btnAdd: UIButton! {
        didSet {
            btnAdd.layer.cornerRadius = 20
        }
    }
    @IBOutlet weak var viemMain: UIView! {
        didSet {
            viemMain.layer.cornerRadius = 6
        }
    }
    
    @IBOutlet weak var btnDissmiss: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        }
    }
    @IBOutlet weak var viewInitialPriceError: UIView! {
        didSet {
            viewInitialPriceError.layer.cornerRadius = 6
            viewInitialPriceError.backgroundColor = UIColor.appThemeColor.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var viewMaintanecePriceError: UIView! {
        didSet {
            viewMaintanecePriceError.layer.cornerRadius = 6
            viewMaintanecePriceError.backgroundColor = UIColor.appThemeColor.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var lblMaintenenceErorr: UILabel!{
        didSet   {
            lblMaintenenceErorr.textColor = .black
        }
    }
    @IBOutlet weak var lblInitialError: UILabel!{
        didSet   {
            lblInitialError.textColor = .black
        }
    }
    
    @IBOutlet weak var viewPackage: UIStackView!
    @IBOutlet weak var txtPackage: UITextField!
    @IBOutlet weak var viewFrequency: UIStackView!
    @IBOutlet weak var viewCategory: UIStackView!
    @IBOutlet weak var viewService: UIStackView!
    @IBOutlet weak var txtMaintenancePrice: UITextField!
    @IBOutlet weak var txtService: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var lblAddService: UILabel!
    @IBOutlet weak var txtInitialPrice: UITextField!
    @IBOutlet weak var txtFrequency: UITextField!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtInitialPrice.keyboardType = .decimalPad
        txtMaintenancePrice.keyboardType = .decimalPad
        setUI()
    }
    private func getMinPrice() -> (initialPrice: Double,maintanencePrice: Double) {
        var maintanencePrice: Double = 0
        var initialPrice: Double = 0
        if let selectedService = self.selectedService {
           _ = self.settingPrice(selectedService)
        }
        if let selectedPackage = selectedPackage {
            if let servicePackageDetails = selectedPackage.servicePackageDetails, let selectedServicePackageDetails = servicePackageDetails.filter({$0.frequencyID == selectedFrequency?.frequencyId}).first {
                initialPrice = selectedServicePackageDetails.minPackageCost ?? 0
                maintanencePrice = selectedServicePackageDetails.minPackageMaintCost ?? 0
            }
        } else if let servicesPricingDetailDcs = servicesPricingDetailDcs  {
            initialPrice = servicesPricingDetailDcs.minInitialPrice ?? 0
            maintanencePrice = servicesPricingDetailDcs.minMaintenancePrice ?? 0
        }
        
        return (initialPrice: initialPrice, maintanencePrice: maintanencePrice)
    }
    
    private func minPriceValidation() -> Bool {
        let minPrice = getMinPrice()
        if let initialPriceString = txtInitialPrice.text {
            guard  let initialPrice = Double(initialPriceString), initialPrice >= minPrice.initialPrice  else {
                viewInitialPriceError.isHidden = false
                lblInitialError.text = "You can not reduce initial price of this service from minimun price of \(minPrice.initialPrice)"
                return false
            }
            viewInitialPriceError.isHidden = true
        }
        if let maintanencePriceString = txtMaintenancePrice.text {
            guard let maintanencePrice = Double(maintanencePriceString), maintanencePrice >= minPrice.maintanencePrice  else {
                viewMaintanecePriceError.isHidden = false
                lblMaintenenceErorr.text = "You can not reduce maintenance price of this service from minimun price of \(minPrice.maintanencePrice)"
                return false
            }
            viewMaintanecePriceError.isHidden = true
        }
        return true
    }
    // MARK: - Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        
        if serviceActionType == .addService {
            guard  !txtCategory.text!.isEmpty else {
                showAlert(message: "Please select category")
                return
            }
        }
        guard  !txtService.text!.isEmpty else {
            showAlert(message: "Please select service")
            return
        }
        if !viewPackage.isHidden {
            guard  !txtPackage.text!.isEmpty else {
                showAlert(message: "Please select package")
                return
            }
        }
        guard  !txtFrequency.text!.isEmpty else {
            showAlert(message: "Please select frequency")
            return
        }
        guard minPriceValidation() else {
            return
        }
        guard var selectedService = self.selectedService else {
            self.showAlert(message: "Please select a service")
            return
        }
        
        guard services.filter({$0.serviceMasterID == selectedService.serviceMasterID && $0.defaultServiceFrequencySysName == selectedFrequency?.sysName}).first == nil && soldServices.filter({$0.serviceID == selectedService.serviceMasterID &&  $0.frequencySysName == selectedFrequency?.sysName}).first == nil else  {
            self.showAlert(message: "You have already added this service with selected frequency!!")
            return
        }
        
        /*
         guard  !txtInitialPrice.text!.isEmpty else {
         showAlert(message: "Please Enter initial price")
         return
         }
         guard  !txtMaintenancePrice.text!.isEmpty else {
         showAlert(message: "Please Enter maintenance price")
         return
         }*/
        self.dismiss(animated: true) {
            
            selectedService.selectedServicePackage = self.selectedPackage
            if let intialPrice = self.txtInitialPrice.text, let price = Double(intialPrice) {
                selectedService.manualInitialPrice = price
            }
            if let maintenancePrice = self.txtMaintenancePrice.text, let price = Double(maintenancePrice) {
                selectedService.manualMaintenancePrice = price
            }
            if let selectedFrequency = self.selectedFrequency {
                selectedService.defaultServiceFrequencySysName = selectedFrequency.sysName
            }
            
            if self.serviceActionType == .addService {
                self.delegate?.addService(selectedService)
            } else {
                self.delegate?.addAddon(selectedService, parentService: self.service, soldService: self.soldService)
            }
        }
    }
    
    
}
// MARK: - Convenience
extension AddServiceViewController {
    private func setUI() {
        viewInitialPriceError.isHidden = true
        viewMaintanecePriceError.isHidden = true
        viewPackage.isHidden = true
        if serviceActionType == .adAddon {
            lblAddService.text = "Add Addon"
            viewCategory.isHidden = true
            viewFrequency.isHidden = false
        } else {
            lblAddService.text = "Add Service"
            viewCategory.isHidden = false
            viewFrequency.isHidden = false
        }
    }
    private func settingPrice(_ service: Service) -> (intialPrice: Double, maintaincePrice: Double) {
        let price = AppUtility.getPrice(service, propertyDetails: propertyDetails?.propertyInfo)
        self.servicesPricingDetailDcs = price.servicesPricingDetailDcs
        return (service.manualInitialPrice ?? price.intialPrice,service.manualMaintenancePrice ?? price.maintaincePrice)
    }
}

// MARK: - UITextFieldDelegate
extension AddServiceViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCategory {
            var rows:[String] = []
            var selectedRow = 0
            
            if let categories = AppUserDefaults.shared.masterDetails?.categories.filter({$0.isActive == true}), categories.count > 0 {
                let types = categories
                for (index,row) in types.enumerated() {
                    if textField.text == row.name {
                        selectedRow = index
                    }
                    rows.append(row.name ?? "")
                }
                showStringPicker(sender: txtCategory, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    self.txtCategory.text = value
                    self.selectedCategory = categories[index]
                    self.selectedService = nil
                    self.txtService.text = nil
                    self.selectedFrequency = nil
                    self.txtFrequency.text = nil
                    self.txtInitialPrice.text = nil
                    self.txtMaintenancePrice.text = nil
                }
            } else {
                txtCategory.text = ""
                showAlert(message: "No category found !!")
            }
            return false
        } else   if textField == txtService {
            var rows:[String] = []
            var selectedRow = 0
            var services: [Service] = []
            if serviceActionType == .adAddon {
                
                guard let serviceDetails = AppUserDefaults.shared.masterDetails else { return false }
                services = serviceDetails.categories.filter({$0.isActive == true}).flatMap { $0.services}.filter{$0.isActive == true && $0.isPlusService == true && $0.parentID == self.service?.serviceMasterID }
            } else if selectedCategory == nil {
                showAlert(message: "Please select a category first")
                return false
            } else {
                let categories = AppUserDefaults.shared.masterDetails?.categories.filter({ (category) -> Bool in
                    return category.categoryId == self.selectedCategory?.categoryId
                })
                if let servicesT = categories?.first?.services
                {
                    //services = servicesT
                    //Nilind
                    services = servicesT.filter({$0.isActive == true})
                } else { return false }
            }
            if services.count > 0 {
                let types = services
                for (index,row) in types.enumerated() {
                    if textField.text == row.name{
                        selectedRow = index
                    }
                    rows.append(row.name ?? "")
                }
                showStringPicker(sender: txtService, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    self.txtService.text = value
                    let service = services[index]
                    self.selectedService = service
                    if let selectedService = self.selectedService, let isParameterizedPriced = selectedService.isParameterizedPriced , isParameterizedPriced {
                        self.viewPackage.isHidden = true
                    } else {
                        self.viewPackage.isHidden = false
                    }
                    self.selectedPackage = nil
                    self.txtPackage.text = nil
                    self.selectedFrequency = nil
                    self.txtFrequency.text = nil
                    self.txtInitialPrice.text = nil
                    self.txtMaintenancePrice.text = nil

                }
            } else {
                txtService.text = ""
                if serviceActionType == .adAddon {
                    showAlert(message: "There is no Add on services")
                } else {
                    showAlert(message: "There is no service for selected category")
                }
            }
            return false
        } else if textField == txtPackage {
            
            if selectedService == nil {
                showAlert(message: "Please select a service first")
                return false
            }
            var rows:[String] = []
            var selectedRow = 0
            
            //if let selectedService = selectedService, let objects = selectedService.servicePackageDCS?.filter({$0.isDelete == false}), !objects.isEmpty {
            if let selectedService = selectedService, let objects = selectedService.servicePackageDCS, !objects.isEmpty {
                for (index,row) in objects.enumerated() {
                    if textField.text == row.packageName {
                        selectedRow = index
                    }
                    rows.append(row.packageName ?? "")
                }
                showStringPicker(sender: txtPackage, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    self.txtPackage.text = value
                    self.selectedPackage = objects[index]
                    self.selectedFrequency = nil
                    self.txtFrequency.text = nil
                    self.txtInitialPrice.text = nil
                    self.txtMaintenancePrice.text = nil
                }
            } else {
                txtPackage.text = ""
                showAlert(message: "No Package found !!")
            }
            return false
        } else if textField == txtFrequency {
            if selectedService == nil {
                showAlert(message: "Please select a service first")
                return false
            }
            
            if !viewPackage.isHidden, selectedPackage == nil {
                showAlert(message: "Please select a package first")
                return false
            }
            var rows:[String] = []
            var selectedRow = 0
            var frequencies: [Frequency] = []
            var servicePackageDetails: [ServicePackageDetail] = []
            
            if viewPackage.isHidden {
                let servicesPrice = AppUtility.getServicePrices(selectedService)
                if !servicesPrice.isEmpty {
                    frequencies = AppUtility.getFrquencies(servicesPrice)
                }
            } else {
                if let selectedPackage = selectedPackage, let servicePackageDetailsT = selectedPackage.servicePackageDetails , !servicePackageDetailsT.isEmpty {
                    servicePackageDetails = servicePackageDetailsT
                    frequencies = AppUtility.getFrquencies(servicePackageDc: selectedPackage.servicePackageDetails)
                }
            }
            if !frequencies.isEmpty {
                for (index,row) in frequencies.enumerated() {
                    if textField.text == row.frequencyName || textField.text == row.sysName {
                        selectedRow = index
                    }
                    rows.append(row.frequencyName ?? "")
                }
                showStringPicker(sender: txtFrequency, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    self.txtFrequency.text = value
                    self.selectedFrequency = frequencies[index]
                    self.selectedService?.defaultServiceFrequencySysName = self.selectedFrequency?.sysName
                    if let selectedService = self.selectedService, selectedService.isParameterizedPriced == true {
                        let price =  self.settingPrice(selectedService)
                        self.txtInitialPrice.text = price.intialPrice.description
                        self.txtMaintenancePrice.text = price.maintaincePrice.description
                    } else {
                        if let servicePackageDetail = servicePackageDetails.filter({$0.frequencyID == frequencies[index].frequencyId}).first {
                            self.txtInitialPrice.text = servicePackageDetail.packageCost?.description
                            self.txtMaintenancePrice.text = servicePackageDetail.packageMaintCost?.description
                        } else {
                            self.txtInitialPrice.text = nil
                            self.txtMaintenancePrice.text = nil
                        }
                    }
                   
                }
            } else {
                txtFrequency.text = ""
                showAlert(message: "No frequency found !!")
            }
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        /*if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            
            
            let characterSet = CharacterSet(charactersIn: string)
            
            if textField == txtInitialPrice || textField ==  txtMaintenancePrice {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
            }
        }*/
        
        if textField == txtInitialPrice || textField ==  txtMaintenancePrice
        {
            let chk = decimalValidation(textField: textField, range: range, string: string)
            return chk
            
        }
        return true
    }
}

