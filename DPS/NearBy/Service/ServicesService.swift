//
//  ServicesService.swift
//  DPS
//
//  Created by Vivek Patel on 09/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class ServicesService {
    
    func getServiceList(leadId: String, _ callBack:@escaping (_ object: EntityDetail?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let url = URL.Service.getServiceDetail.replacingOccurrences(of: "(LeadNumber)", with: leadId).replacingOccurrences(of: "(companyKey)", with: companyKey)
        AF.request(url, method: .get) .responseDecodable(of: EntityDetail.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                 do {
                 let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                 let object = try JSONDecoder().decode(EntityDetail.self, from: data)
                 print(object)
                 
                 } catch (let error) {
                 print("MasterSalesAutomation", error.localizedDescription)
                 }
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
