//
//  DiscountCreditModel.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation


struct DiscountMaster: Codable {
    
    var discountSetupId: Int?
    var companyId: Int?
    var sysName: String?
    var name: String?
    var discountPercent: Double?
    var discountAmount: Double?
    var isDiscountPercent: Bool
    var isActive: Bool
    var isDelete: Bool
    var qBDiscountMappingName: String?
    var branchSysName: String?
    var type: DiscountType?
    var applicableForInitial: Bool?
    var applicableForMaintenance: Bool?
    var discountCode: String?
    var isServiceBased: Bool?
    var serviceSysName: String?
    var validFrom: String?
    var validTo: String?
    var usage: String?
    var description: String?
    var createdDate: String?
    var appliedDiscount: Double = 0
    var appliedMaintanenceDiscount: Double = 0

    enum CodingKeys: String, CodingKey {
        
        case discountSetupId = "DiscountSetupId"
        case companyId = "CompanyId"
        case sysName = "SysName"
        case name = "Name"
        case discountPercent = "DiscountPercent"
        case discountAmount = "DiscountAmount"
        case isDiscountPercent = "IsDiscountPercent"
        case isActive = "IsActive"
        case isDelete = "IsDelete"
        case qBDiscountMappingName = "QBDiscountMappingName"
        case branchSysName = "BranchSysName"
        case type = "Type"
        case applicableForInitial = "ApplicableForInitial"
        case applicableForMaintenance = "ApplicableForMaintenance"
        case discountCode = "DiscountCode"
        case isServiceBased = "IsServiceBased"
        case serviceSysName = "ServiceSysName"
        case validFrom = "ValidFrom"
        case validTo = "ValidTo"
        case usage = "Usage"
        case description = "Description"
        case createdDate = "CreatedDate"
    }
    
}

extension DiscountMaster: Equatable {
    static func ==(lhs: DiscountMaster, rhs: DiscountMaster) -> Bool {
        return lhs.sysName == rhs.sysName
    }
}

enum DiscountType: String, Codable {
    case coupon = "Coupon"
    case credit = "Credit"
}
