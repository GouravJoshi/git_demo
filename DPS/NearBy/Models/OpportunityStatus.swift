//
//  WebLeadStatus.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation


struct WebLeadStatus: Codable, Equatable {
    
    var companyId: Int
    var CompanyMasterDc: String?
    var isActive: Bool?
    var name: String?
    var orderBy: Int
    var sysName: String?
    var statusId: Int?
    var colorCode: String?

    enum CodingKeys: String, CodingKey {
        case companyId = "CompanyId"
        case CompanyMasterDc = "CompanyMasterDc"
        case isActive = "IsActive"
        case name = "Name"
        case orderBy = "OrderBy"
        case statusId = "StatusId"
        case sysName = "SysName"
        case colorCode = "ColorCode"
    }
}

struct OpportunityStatus: Codable, Equatable {
    
    var isActive: Bool?
    var orderBy: Int
    var sysName: String?
    var statusId: Int?
    var statusName: String?
    var colorCode: String?
    enum CodingKeys: String, CodingKey {
        case isActive = "IsActive"
        case orderBy = "OrderBy"
        case statusId = "StatusId"
        case statusName = "StatusName"
        case sysName = "SysName"
        case colorCode = "ColorCode"

    }
}

struct VisitStatus: Codable , Equatable {
    
    var visitStatusId: Int
    var visitStatusName: String?
    var sysName: String?
    var isActive: Bool?
    var colorCode: String?
    
    enum CodingKeys: String, CodingKey{
        case visitStatusId = "VisitStatusId"
        case visitStatusName = "VisitStatusName"
        case sysName = "VisitStatusSysName"
        case isActive = "IsActive"
        case colorCode = "ColorCode"
    }
}
