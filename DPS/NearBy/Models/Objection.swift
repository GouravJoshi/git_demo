//
//  Objection.swift
//  DPS
//
//  Created by Vivek Patel on 18/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - Objection
struct Objection: Codable {
    var objectionMasterID: Int?
    var objection: String?
    var isActive: Bool?
    
    enum CodingKeys: String, CodingKey {
        case objectionMasterID = "ObjectionMasterId"
        case objection = "Objection"
        case isActive = "IsActive"
    }
}
