//
//  HelpModel.swift
//  DPS
//
//  Created by Vivek Patel on 28/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

struct EmployeeHelpDocument : Codable {
    let employeeTrainingMaterialID: Int?
    let type, path, welcomeDescription: String?
    let status: Bool?
    let title: String?
    
    enum CodingKeys: String, CodingKey {
        case employeeTrainingMaterialID = "EmployeeTrainingMaterialId"
        case type = "Type"
        case path = "Path"
        case welcomeDescription = "Description"
        case status = "Status"
       case title = "Title"
    }
}

