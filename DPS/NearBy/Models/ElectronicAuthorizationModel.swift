//
//  ElectronicAuthorizationModel.swift
//  DPS
//
//  Created by Vivek Patel on 04/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
// MARK: - ElectronicAuthorizatio
struct ElectronicAuthorizationForm: Codable {
    var electronicAuthorizationFormID: Int?
    var accountNo, leadNo, title, preNotes: String?
    var postNotes, terms: String?
    var termsSignUp: Bool?
    var frequency: String?
    var monthlyDate: Int?
    var paymentMethod, bankName, bankAccountNo, routingNo: String?
    var signaturePath, firstName, middleName, lastName: String?
    var address1, address2: String?
    var stateID: Int?
    var cityName, zipcode, phone, email: String?
    var date: String?
    var createdBy: Int?
    var createdDate, modifiedDate: String?
   // var modifiedBy: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case electronicAuthorizationFormID = "ElectronicAuthorizationFormId"
        case accountNo = "AccountNo"
        case leadNo = "LeadNo"
        case title = "Title"
        case preNotes = "PreNotes"
        case postNotes = "PostNotes"
        case terms = "Terms"
        case termsSignUp = "TermsSignUp"
        case frequency = "Frequency"
        case monthlyDate = "MonthlyDate"
        case paymentMethod = "PaymentMethod"
        case bankName = "BankName"
        case bankAccountNo = "BankAccountNo"
        case routingNo = "RoutingNo"
        case signaturePath = "SignaturePath"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case address1 = "Address1"
        case address2 = "Address2"
        case stateID = "StateId"
        case cityName = "CityName"
        case zipcode = "Zipcode"
        case phone = "Phone"
        case email = "Email"
        case date = "Date"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
      //  case modifiedBy = "ModifiedBy"
    }
    init() {}
}
// MARK: - ElectronicAuthorizationFormMaster
struct ElectronicAuthorizationFormMaster: Codable {
    var authorizationFormID, companyID: Int?
    var title, formSysName, preNotes, postNotes: String?
    var terms, createdDate: String?
    var createdBy: Int?
   // var modifiedDate, modifiedBy: JSONNull?
    enum CodingKeys: String, CodingKey {
        case authorizationFormID = "AuthorizationFormId"
        case companyID = "CompanyId"
        case title = "Title"
        case formSysName = "FormSysName"
        case preNotes = "PreNotes"
        case postNotes = "PostNotes"
        case terms = "Terms"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
     //   case modifiedDate = "ModifiedDate"
      //  case modifiedBy = "ModifiedBy"
    }
}
