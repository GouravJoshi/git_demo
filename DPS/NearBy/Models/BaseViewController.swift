//
//  BaseViewController.swift
//  DPS
//
//  Created by Vivek Patel on 14/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import GLKit

class BaseViewController: UIViewController {
    // MARK: - Variables
    
    lazy var spinner = UIAlertController()
    @IBOutlet weak var constraintFooterBottom: NSLayoutConstraint!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.appThemeColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        constraintFooterBottom?.constant = (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)
        
    }
    
    func setFooterView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            // self.setTopMenuOption()
            self.setFooterMenuOption()
        })
    }
    
    // MARK: Functions
    func setEmployeeListToUserDefault() {
        if let dictLeadDetailMaster =  UserDefaults.standard.value(forKey: "EmployeeList") as? NSArray  {
            print(dictLeadDetailMaster)
            do {
                let data = try JSONSerialization.data(withJSONObject: dictLeadDetailMaster, options: .prettyPrinted)
                let object = try JSONDecoder().decode([EmployeeListD2D].self, from: data)
                AppUserDefaults.shared.employeeList = object
            } catch (let error) {
                print("Data type MasterSalesAutomation", error.localizedDescription)
            }
        }
    }
    func getCenterCoordOfPolygon(_ LocationPoints: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        var x:Float = 0.0;
        var y:Float = 0.0;
        var z:Float = 0.0;
        for points in LocationPoints {
            let lat = GLKMathDegreesToRadians(Float(points.latitude));
            let long = GLKMathDegreesToRadians(Float(points.longitude));
            
            x += cos(lat) * cos(long);
            
            y += cos(lat) * sin(long);
            
            z += sin(lat);
        }
        x = x / Float(LocationPoints.count);
        y = y / Float(LocationPoints.count);
        z = z / Float(LocationPoints.count);
        let resultLong = atan2(y, x);
        let resultHyp = sqrt(x * x + y * y);
        let resultLat = atan2(z, resultHyp);
        let result = CLLocationCoordinate2D(latitude: CLLocationDegrees(GLKMathRadiansToDegrees(Float(resultLat))), longitude: CLLocationDegrees(GLKMathRadiansToDegrees(Float(resultLong))));
        return result;
    }
    
    func setFooterMenuOption() {
        
        nsud.setValue("Sales", forKey: "DPS_BottomMenuOptionTitle")
        nsud.synchronize()
        var footorView = FootorView()
        // footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - 90, width: self.view.frame.size.width, height:90))
        footorView = FootorView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))
        self.view.addSubview(footorView)
        footorView.onClickHomeButtonAction = {() -> Void in
            self.GotoDashboardViewController()
        }
        footorView.onClickScheduleButtonAction = {() -> Void in
            self.GotoScheduleViewController()
        }
        footorView.onClickAccountButtonAction = {() -> Void in
            self.GotoAccountViewController()
        }
        
    }
    
    func GotoDashboardViewController() {
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
        
        self.navigationController?.pushViewController(controller, animated: false)
        
        /*var status = Bool()
        
        if (self.navigationController != nil) {
            
            for vc in  self.navigationController!.viewControllers {
                
                if vc is DashBoardNew_iPhoneVC { //DashBoardView
                    
                    status = true
                    
                    self.navigationController?.popToViewController(vc, animated: false)
                    
                }
                
            }
            
        }
        
        if !(status){
            
            let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashBoardNew_iPhoneVC") as! DashBoardNew_iPhoneVC
            
            self.navigationController?.pushViewController(controller, animated: false)
            
        }*/
        
    }
    func GotoScheduleViewController() {
        
        nsud.set(true, forKey: "fromCompanyVC")
        nsud.synchronize()
        
        //let controller = storyboardMainiPhone.instantiateViewController(withIdentifier: "AppointmentView")
        
        //self.navigationController?.pushViewController(controller, animated: false)
        
        if(DeviceType.IS_IPAD){
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment_iPAD",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                
                let mainStoryboard = UIStoryboard(
                    name: "MainiPad",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentViewiPad") as? AppointmentViewiPad
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
            
        }else{
            
            let strAppointmentFlow = "\(nsud.value(forKey: "AppointmentFlow") ?? "New")"
            
            if strAppointmentFlow == "New" {
                
                let mainStoryboard = UIStoryboard(
                    name: "Appointment",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentVC") as? AppointmentVC
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            } else {
                let mainStoryboard = UIStoryboard(
                    name: "Main",
                    bundle: nil)
                let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentView") as? AppointmentView
                self.navigationController?.pushViewController(objByProductVC!, animated: false)
                
            }
        }
    }
    
    func GotoAddNewTaskViewController() {
        let dictTaskDetailsData = NSMutableDictionary()
        dictTaskDetailsData.setValue("DashBoardView", forKeyPath: "Pre_ViewComeFrom")
        dictTaskDetailsData.setValue(0, forKeyPath: "Pre_IsUpdate") //0 Add 1 for Edit 2 for followup
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_RefId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_leadNumber")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountNumber")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactId")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyId")
        
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_AccountName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_LeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_WebLeadName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmContactName")
        dictTaskDetailsData.setValue("", forKeyPath: "Pre_CrmCompanyName")
        
        
        
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "Task-Activity", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskVC_iPhoneVC") as! AddTaskVC_iPhoneVC
        controller.dictTaskDetailsData = dictTaskDetailsData
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func GotoMapViewController() {
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NearbyMeViewController") as? NearbyMeViewController
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    func GotoLeadViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadVC") as! WebLeadVC
        
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoOpportunityViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityVC") as! OpportunityVC
        self.navigationController?.pushViewController(controller, animated: false)
    }
    func GotoTaskViewController() {
        let testController = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskVC_CRMNew_iPhone") as! TaskVC_CRMNew_iPhone
        
        testController.strFromVC = "DashBoardView"//"WebLeadVC"
        self.navigationController?.pushViewController(testController, animated: false)
    }
    func GotoActivityViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ActivityVC_CRMNew_iPhone") as! ActivityVC_CRMNew_iPhone
        // vc.strFromVC = strFromVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func GotoSignAgreementViewController() {
        if(DeviceType.IS_IPAD){
            let mainStoryboard = UIStoryboard(
                name: "CRMiPad",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPadVC") as? SignedAgreementiPadVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }else{
            let mainStoryboard = UIStoryboard(
                name: "CRM",
                bundle: nil)
            let objByProductVC = mainStoryboard.instantiateViewController(withIdentifier: "SignedAgreementiPhoneVC") as? SignedAgreementiPhoneVC
            self.navigationController?.present(objByProductVC!, animated: true)
        }
    }
    func GotoViewScheduleViewController() {
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "ScheduleD2D" : "ScheduleD2D", bundle: Bundle.main).instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func GotoAccountViewController() {
        let controller = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRMContact_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactVC_CRMContactNew_iPhone") as! ContactVC_CRMContactNew_iPhone
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func showAlertWithCompletion(_ title: String? = "Message", message: String?, buttonTitle:String = "OK" ,completion: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default) { alert in
            completion?()
        })
        self.present(alert, animated: true)
    }
    func showAlertWithOptions(_ title: String? = "Message", message: String?,btnTitle1:String = "OK", cancelTitle:String = "Cancel" , completion: @escaping (_ isYes: Bool) -> Void) -> Void {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: btnTitle1, style: UIAlertAction.Style.default) { alert in
            completion(true)
        })
        alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.cancel) { alert in
            completion(false)
        })
        self.present(alert, animated: true)
    }
    func showStringPicker( sender:UIView,selectedRow:Int ,rows:[String], action: StringPickerActionHandler?) {
        let storyboard =  UIStoryboard.init(name: "Picker", bundle: nil)
        
        let destination = storyboard.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        destination.pickerType = .string
        destination.rows = rows
        destination.selectedRow = selectedRow
        destination.viewClicked = sender
        destination.doneAction =  {( selectedRow, selectedString, view) -> () in
            action?( selectedRow, selectedString, view)
        }
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
    }
    func showDatePicker( sender:UIView,selectedDate:Date, maximumDate:Date? = nil, minimumDate:Date? = nil, pickerType:PickerType, action: DatePickerActionHandler?) {
        let storyboard =  UIStoryboard.init(name: "Picker", bundle: nil)
        
        let destination = storyboard.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        destination.pickerType = pickerType
        destination.maximumDate = maximumDate
        destination.minimumDate = minimumDate
        destination.intialDate = selectedDate
        destination.viewClicked = sender
        destination.dateAction =  {(selectedDate, selectedView) -> () in
            action?(selectedDate, selectedView)
        }
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
    }
    func showMultiSelectionPicker(sender:UIView, rows:[MultiSelectRow], pickerListType:PickerListType, action: MultiSelectActionHandler?)  {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "FilterListViewController") as? FilterListViewController {
            destination.rows = rows
            destination.pickerType = pickerListType
            destination.viewClicked = sender
            destination.doneAction = {( selectedRow, view) -> () in
                action?( selectedRow, view)
            }
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    func showMultiPicker(sender:UIView, rows:[MultiSelectRow], action: MultiSelectActionHandler?)  {
        let storyboard =  UIStoryboard.init(name: "Picker", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "MultiSelectionViewController") as! MultiSelectionViewController
        destination.rows = rows
        destination.viewClicked = sender
        destination.doneAction = {( selectedRow, view) -> () in
            action?( selectedRow, view)
        }
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
    }
    func actionCall(number: String) {
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number)
    }
    func actionEmail(email: String) {
        guard MFMailComposeViewController.canSendMail() else{ return }
        let messageBody = ""
        let emails = email.components(separatedBy: ",")
        let toRecipents = emails
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setMessageBody(messageBody, isHTML: false)
        mail.setToRecipients(toRecipents)
        
        present(mail, animated: true)
    }
}
// MARK: - Common View
extension BaseViewController: CommonView {
    @objc func showAlert(message: String?) {
        guard let message = message else { return }
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: message, viewcontrol: self)
    }
    
    func showLoader() {
        spinner = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(spinner, animated: false, completion: nil)
    }
    func hideLoader() {
        self.spinner.dismiss(animated: false, completion: nil)
    }
}
extension BaseViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
