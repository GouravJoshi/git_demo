//
//  State.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct State: Codable {
    var stateID: Int
    var name, stateSysName, stateShortName: String?
    var countryID: Int?
    var countryMasterEXTDc: JSONNull?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate, modifiedBy: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case stateID = "StateId"
        case name = "Name"
        case stateSysName = "StateSysName"
        case stateShortName = "StateShortName"
        case countryID = "CountryId"
        case countryMasterEXTDc = "CountryMasterExtDc"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}
