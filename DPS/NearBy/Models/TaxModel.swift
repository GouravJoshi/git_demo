//
//  TaxModel.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct TaxDetail: Codable {
    var taxId: Int?
    var name: String?
    var sysName: String?
    var branchName: String?
    var branchSysName: String?
    enum CodingKeys: String, CodingKey {
        case taxId = "TaxId"
        case name = "Name"
        case sysName = "SysName"
        case branchName = "BranchName"
        case branchSysName = "BranchSysName"
    }
}
