//
//  AddLead.swift
//  DPS
//
//  Created by Vivek Patel on 10/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct AddLead: Codable {
    
    var firstName: String?
    var lastName: String?
    var primaryEmail: String?
    var cellPhone1: String?
    var visitStatusId: Int?
    var visitDescription: String?
    var visitCount: Int?
    var addressImagePath: String?
    var area: String?
    var noofBedroom: String?
    var noofBathroom: String?
    var noofStory: String?
    var companyKey, refType: String?
    var createNewAccount: Bool?
    var flowType: String?
    var primaryServiceID: Int?
    var additionalServiceIDS, sourceIDS: [Int]?
    var branchSysName: String?
    var accountID, crmContactID, crmCompanyID: Int?
    var title: String?
    var urgencyID: Int?
    var comment, assignedToID, scheduleDate, scheduleTime: String?
    var fieldSalesPerson: String?
    var customerAddressID: Int?
    var address1, cityName, zipcode: String?
    var stateID, countryID: Int?
    var assignedToType: String?
    var name: String?
    var industryId: Int?
    var webLeadId: Int?
    var address2: String?
    var webLeadServices: String?
    var latitude: Double?
    var longitude: Double?
    var currentServicesProvider: String?
    var objectionIds: [Int]?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case primaryEmail = "PrimaryEmail"
        case cellPhone1 = "CellPhone1"
        case visitStatusId = "VisitStatusId"
        case visitDescription = "VisitDescription"
        case visitCount = "VisitCount"
        case addressImagePath = "AddressImagePath"
        case area = "Area"
        case noofBedroom = "NoofBedroom"
        case noofBathroom = "NoofBathroom"
        case noofStory = "NoofStory"
        case companyKey = "CompanyKey"
        case refType = "RefType"
        case createNewAccount = "CreateNewAccount"
        case flowType = "FlowType"
        case primaryServiceID = "PrimaryServiceId"
        case additionalServiceIDS = "AdditionalServiceIds"
        case sourceIDS = "SourceIds"
        case branchSysName = "BranchSysName"
        case accountID = "AccountId"
        case crmContactID = "CrmContactId"
        case crmCompanyID = "CrmCompanyId"
        case title = "Title"
        case urgencyID = "UrgencyId"
        case comment = "Comment"
        case assignedToID = "AssignedToId"
        case scheduleDate = "ScheduleDate"
        case scheduleTime = "ScheduleTime"
        case fieldSalesPerson = "FieldSalesPerson"
        case customerAddressID = "CustomerAddressId"
        case address1 = "Address1"
        case cityName = "CityName"
        case zipcode = "Zipcode"
        case stateID = "StateId"
        case countryID = "CountryId"
        case assignedToType = "AssignedToType"
        case name = "Name"
        case industryId = "IndustryId"
        case webLeadId = "WebLeadId"
        case address2 = "Address2"
        case webLeadServices = "WebLeadServices"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case currentServicesProvider = "CurrentServicesProvider"
        case objectionIds = "ObjectionIds"
    }
}
