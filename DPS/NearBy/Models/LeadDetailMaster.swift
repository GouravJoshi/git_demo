//
//  LeadDetailMaster.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct LeadDetailMaster: Codable {
    
    var leadsStatus: [WebLeadStatus] = []
    var opportunitesLeadStatus: [OpportunityStatus] = []
    var visitsStatus: [VisitStatus] = []
    var sourceMaster: [SourceMaster] = []
    var objections: [Objection]?
    var textTemplateMasters: [TextTemplatesSelectListDCS] = []
    var leadStageMasters: [LeadStageMasters] = []
    var employeeHelpDocuments: [EmployeeHelpDocument] = []
    enum CodingKeys: String, CodingKey {
        case leadsStatus = "WebLeadStatusMasters"
        case opportunitesLeadStatus = "LeadStatusMasters"
        case visitsStatus = "VisitStatusMasters"
        case sourceMaster = "SourceMasters"
        case objections = "ObjectionMasters"
        case textTemplateMasters =  "TextTemplateMasters"
        case leadStageMasters = "LeadStageMasters"
        case employeeHelpDocuments = "EmployeeHelpDocuments"
    }
    init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        leadsStatus = try container.decodeIfPresent([WebLeadStatus].self, forKey: .leadsStatus) ?? []
        opportunitesLeadStatus = try container.decodeIfPresent([OpportunityStatus].self, forKey: .opportunitesLeadStatus) ?? []
        visitsStatus = try container.decodeIfPresent([VisitStatus].self, forKey: .visitsStatus) ?? []
        sourceMaster = try container.decodeIfPresent([SourceMaster].self, forKey: .sourceMaster) ?? []
        objections = try container.decodeIfPresent([Objection].self, forKey: .objections)
        textTemplateMasters = try container.decodeIfPresent([TextTemplatesSelectListDCS].self, forKey: .textTemplateMasters) ?? []
        leadStageMasters = try container.decodeIfPresent([LeadStageMasters].self, forKey: .leadStageMasters) ?? []
        employeeHelpDocuments = try container.decodeIfPresent([EmployeeHelpDocument].self, forKey: .employeeHelpDocuments) ?? []
    }
}












