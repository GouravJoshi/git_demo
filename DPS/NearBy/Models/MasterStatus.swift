//
//  MasterStatus.swift
//  DPS
//
//  Created by Vivek Patel on 11/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - MasterStatus
struct MasterStatus: Codable {
    var companyKey: String?
    var statusName: String?
    var statusSysName, leadStageMasterEXTSerDCS: String?
    
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case statusName = "StatusName"
        case statusSysName = "StatusSysName"
        case leadStageMasterEXTSerDCS = "LeadStageMasterExtSerDcs"
    }
}


// MARK: - MasterStage
struct MasterStage: Codable {
    var companyKey: String?
    var stageName, stageSysName: String?
    var statusSysName: String?
    
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case stageName = "StageName"
        case stageSysName = "StageSysName"
        case statusSysName = "StatusSysName"
    }
}
