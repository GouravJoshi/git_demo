//
//  SourceMaster.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct SourceMaster: Codable {
    var isActive: Bool?
    var name: String?
    var sourceId: Int
    var sourceCategoryId: Int?
    var sourceSysName: String
    var sourceFullName: String?
    enum CodingKeys: String, CodingKey {
        case isActive = "IsActive"
        case name = "Name"
        case sourceCategoryId = "SourceCategoryId"
        case sourceFullName = "SourceFullName"
        case sourceId = "SourceId"
        case sourceSysName = "SourceSysName"
    }
}

