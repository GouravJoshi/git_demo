//
//  ServiceModel.swift
//  DPS
//
//  Created by Vivek Patel on 22/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct Category: Codable {
    var categoryId:Int?
    var companyId:Int?
    var name:String?
    var sysName: String?
    var companyKey:String?
    var departmentId:Int?
    var departmentSysName:String?
    var isActive:Bool?
    var isSystem:Bool?
    //var serviceMasters:ServiceMaster?
    var services:[Service] = []
    var serviceMasterRenewalPriceDc:String?
    enum CodingKeys: String, CodingKey {
        case categoryId = "CategoryId"
        case companyId = "CompanyId"
        case name = "Name"
        case sysName = "SysName"
        case companyKey = "CompanyKey"
        case departmentId =  "DepartmentId"
        case departmentSysName  = "DepartmentSysName"
        case isActive = "IsActive"
        case isSystem = "IsSystem"
        // case serviceMasters = "ServiceMasters"
        case services = "Services"
        case serviceMasterRenewalPriceDc = "ServiceMasterRenewalPriceDc"
    }
    
}
struct Service: Codable {
    
    //var serviceMasterId:Int?
    var coreServiceMasterId:Int?
    var companyId:Int?
    var categorySysName:String?
    var subCategorySysName:String?
    var name:String?
    var description:String?
    var estimatedTimeReq:String?
    var termsConditions:String?
    var serviceTypeId:Int?
    var serviceHelpDoc:String?
    var isPlusService:Bool?
    var sysName:String?
    var parentSysName:String?
    var isActive:Bool?
    var initialServiceSysName:String?
    var isInitialService:Bool?
    //var initialPrice: Double?
    var initialEstimatedDuration:String?
    //var initialServiceMasterId: Int?
    // var price: Double?
    var isTaxable:Bool?
    var isResidentialTaxable:Bool?
    var isCommercialTaxable: Bool?
    var serviceTargets:String?
    var serviceAttributes:String?
    var isUnitBasedService:Bool?
    var isParameterizedPriced:Bool?
    var isRenewal:Bool?
    var defaultBillingFrequency:String?
    var defaultBillingFrequencySysName:String?
    // var serviceParametersDcs:[ServiceParametersDc] = []
    var serviceMasterRenewalPrices: [JSONAny]?
    var createdDate: String?
    var modifiedDate: String?
    var modifiedBy: String?
    var defaultServiceFrequencySysName: String?
    var manualInitialPrice: Double?
    var manualMaintenancePrice: Double?
    var isSelected = false
    var priceChangeDescription: String?
    var isDoortodoor: Bool?
    var priceChangeReason: PriceChangeReason?
    var createdBy: Int?
    var serviceMasterID, coreServiceMasterID, companyID: Int?
    var leadResponseDescription: String?
    var serviceTypeID: Int?
    var parentID: Int?
    var companyMasterDc, soldServiceStandardDCS: String?
    var serviceBundleDCS: String?
    var servicePackageDCS: [ServicePackageDc]?
    var selectedServicePackage: ServicePackageDc?
    
    enum CodingKeys: String, CodingKey {
        case serviceMasterID = "ServiceMasterId"
        case coreServiceMasterID = "CoreServiceMasterId"
        case companyID = "CompanyId"
        case categorySysName = "CategorySysName"
        case subCategorySysName = "SubCategorySysName"
        case name = "Name"
        case description = "Description"
        case estimatedTimeReq = "EstimatedTimeReq"
        case termsConditions = "TermsConditions"
        case serviceTypeID = "ServiceTypeId"
        case serviceHelpDoc = "ServiceHelpDoc"
        case isPlusService = "IsPlusService"
        case parentID = "ParentId"
        case sysName = "SysName"
        case parentSysName = "ParentSysName"
        case isActive = "IsActive"
        case initialServiceSysName = "InitialServiceSysName"
        case isInitialService = "IsInitialService"
        //case initialPrice = "InitialPrice"
        case initialEstimatedDuration = "InitialEstimatedDuration"
        // case initialServiceMasterID = "InitialServiceMasterId"
        // case price = "Price"
        case isTaxable = "IsTaxable"
        case isResidentialTaxable = "IsResidentialTaxable"
        case isCommercialTaxable = "IsCommercialTaxable"
        case serviceTargets = "ServiceTargets"
        case serviceAttributes = "ServiceAttributes"
        case isUnitBasedService = "IsUnitBasedService"
        case isParameterizedPriced = "IsParameterizedPriced"
        case isRenewal = "IsRenewal"
        case defaultBillingFrequency = "DefaultBillingFrequency"
        case defaultBillingFrequencySysName = "DefaultBillingFrequencySysName"
        case defaultServiceFrequencySysName = "DefaultServiceFrequencySysName"
        case isDoortodoor = "IsDoortodoor"
        // case serviceParametersDcs = "ServiceParametersDcs"
        case companyMasterDc = "CompanyMasterDc"
        case soldServiceStandardDCS = "SoldServiceStandardDcs"
        case servicePackageDCS = "ServicePackageDcs"
        case serviceBundleDCS = "ServiceBundleDcs"
        case serviceMasterRenewalPrices = "ServiceMasterRenewalPrices"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}

// MARK: - ServiceParametersDc
struct ServiceParametersDc: Codable {
    let serviceParameterID, serviceMasterID: Int?
    let name, sysName: String?
    let isDefault: Bool?
    let unitName: String?
    //let minimumPrice, initialUnitPrice, maintUnitPrice: Double?
    let serviceMaster: ServiceMaster?
    let createdDate: String?
    let createdBy: Int?
    let modifiedDate: String?
    let modifiedBy: String?
    
    enum CodingKeys: String, CodingKey {
        case serviceParameterID = "ServiceParameterId"
        case serviceMasterID = "ServiceMasterId"
        case name = "Name"
        case sysName = "SysName"
        case isDefault = "IsDefault"
        case unitName = "UnitName"
        // case minimumPrice = "MinimumPrice"
        // case initialUnitPrice = "InitialUnitPrice"
        // case maintUnitPrice = "MaintUnitPrice"
        case serviceMaster = "ServiceMaster"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}

// MARK: - ServiceMaster
struct ServiceMaster: Codable {
    let serviceMasterID: Int?
    let name, sysName: String?
    let companyID, categoryID: Int?
    let serviceMasterDescription, estimatedTimeReq: String?
    let termCondition: String?
    let serviceTypeID: Int?
    let serviceImage: String?
    let initialPrice: Int?
    let initialEstimatedDuration: String
    let initialServiceMasterID, price: Int?
    let isTaxable: Bool?
    let colorCode, estimatedTimeReqStr: String?
    let isActive: Bool?
    //isSystem
    //  let soldInitialPrice: Int?
    let isPlusService: Bool?
    let parentID: Int?
    let isUnitBasedService: Bool?
    let serviceName, serviceKey, combineValue: String?
    let isResidentialTaxable, isCommercialTaxable: Bool?
    let departmentSysName: String?
    let isParameterizedPriced: Bool?
    let orderType: String?
    let isRenewal: Bool?
    // let serviceCategoryMasterDc, skillMasterDCS, servicePackageMasterDCS, serviceDocuments: JSONNull?
    // let targetMasterDCS, soldServiceStandardDCS, attributeMasterDCS, parentServiceMasterDc: JSONNull?
    //   let invoiceChargesDCS, creditMemoReceivableDCS: JSONNull?
    // let serviceParameters: [JSONAny]
    //  let serviceMasterRenewalPriceDc: JSONNull?
    //  let serviceMasterRenewalPrices: [JSONAny]
    let createdDate: String?
    let createdBy: Int?
    let modifiedDate: String?
    let modifiedBy: String?
    
    enum CodingKeys: String, CodingKey {
        case serviceMasterID = "ServiceMasterId"
        case name = "Name"
        case sysName = "SysName"
        case companyID = "CompanyId"
        case categoryID = "CategoryId"
        case serviceMasterDescription = "Description"
        case estimatedTimeReq = "EstimatedTimeReq"
        case termCondition = "TermCondition"
        case serviceTypeID = "ServiceTypeId"
        case serviceImage = "ServiceImage"
        case initialPrice = "InitialPrice"
        case initialEstimatedDuration = "InitialEstimatedDuration"
        case initialServiceMasterID = "InitialServiceMasterId"
        case price = "Price"
        case isTaxable = "IsTaxable"
        case colorCode = "ColorCode"
        case estimatedTimeReqStr = "EstimatedTimeReqStr"
        case isActive = "IsActive"
        //case isSystem = "IsSystem"
        //case soldInitialPrice = "SoldInitialPrice"
        case isPlusService = "IsPlusService"
        case parentID = "ParentId"
        case isUnitBasedService = "IsUnitBasedService"
        case serviceName = "ServiceName"
        case serviceKey = "ServiceKey"
        case combineValue
        case isResidentialTaxable = "IsResidentialTaxable"
        case isCommercialTaxable = "IsCommercialTaxable"
        case departmentSysName = "DepartmentSysName"
        case isParameterizedPriced = "IsParameterizedPriced"
        case orderType = "OrderType"
        case isRenewal = "IsRenewal"
        /* case serviceCategoryMasterDc = "ServiceCategoryMasterDc"
         case skillMasterDCS = "SkillMasterDcs"
         case servicePackageMasterDCS = "ServicePackageMasterDcs"
         case serviceDocuments = "ServiceDocuments"
         case targetMasterDCS = "TargetMasterDcs"
         case soldServiceStandardDCS = "SoldServiceStandardDcs"
         case attributeMasterDCS = "AttributeMasterDcs"
         case parentServiceMasterDc = "ParentServiceMasterDc"
         case invoiceChargesDCS = "InvoiceChargesDcs"
         case creditMemoReceivableDCS = "CreditMemoReceivableDcs"
         case serviceParameters = "ServiceParameters"
         case serviceMasterRenewalPriceDc = "ServiceMasterRenewalPriceDc"
         case serviceMasterRenewalPrices = "ServiceMasterRenewalPrices"*/
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}

struct Frequency: Codable {
    var frequencyId: Int?
    var companyId: Int?
    var frequencyName: String?
    var frequencyValue: Int?
    var sysName: String?
    var yearlyOccurrence: Int?
    var frequencyType: String?
    var isActive: Bool?
    //FrequencyScheduleExtSerDcs:
    enum CodingKeys: String, CodingKey {
        
        case frequencyId = "FrequencyId"
        case companyId = "CompanyId"
        case frequencyName = "FrequencyName"
        case frequencyValue = "FrequencyValue"
        case sysName = "SysName"
        case yearlyOccurrence = "YearlyOccurrence"
        case frequencyType = "FrequencyType"
        case isActive  = "IsActive"
        //FrequencyScheduleExtSerDcs :
        
    }
    
}

struct AddressPropertyType: Codable {
    var addressPropertyTypeId:Int?
    var sysName: String?
    var name: String?
    var companyId: Int?
    var isActive:Bool?
    var isSystem: Bool?
    var createdDate: Bool?
    var modifiedDate: String?
    
    enum CodingKeys: String, CodingKey {
        case addressPropertyTypeId = "AddressPropertyTypeId"
        case sysName = "SysName"
        case name = "Name"
        case companyId = "CompanyId"
        case isActive = "IsActive"
        case isSystem = "IsSystem"
        case createdDate = "CreatedDate"
        case  modifiedDate = "ModifiedDate"
    }
}

// MARK: - ServicePackageDc
struct ServicePackageDc: Codable {
    var servicePackageID, companyID: Int?
    var categorySysName: String?
    var packageName: String?
    var frequencyID, serviceID: Int?
    // var packageCost:Double?
    var serviceSysName, frequencyName: String?
    var isTBD, isDelete: Bool?
    var branchSysName: String?
    // var serviceMaster: JSONNull?
    var servicePackageDetails: [ServicePackageDetail]?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate:String?
    var modifiedBy: String?
    
    enum CodingKeys: String, CodingKey {
        case servicePackageID = "ServicePackageId"
        case companyID = "CompanyId"
        //  case categorySysName = "CategorySysName"
        case packageName = "PackageName"
        case frequencyID = "FrequencyId"
        //case packageCost = "PackageCost"
        case serviceID = "ServiceId"
        case serviceSysName = "ServiceSysName"
        case frequencyName = "FrequencyName"
        case isTBD = "IsTBD"
        case isDelete = "IsDelete"
        case branchSysName = "BranchSysName"
        // case serviceMaster = "ServiceMaster"
        case servicePackageDetails = "ServicePackageDetails"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        // case modifiedBy = "ModifiedBy"
    }
}

// MARK: - ServicePackageDetail
struct ServicePackageDetail: Codable {
    var packageDetailID, servicePackageID, frequencyID: Int?
    var frequencyName: String?
    var packageCost, packageMaintCost: Double?
    // var servicePackageDc: JSONNull?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate, modifiedBy: String?
    var minPackageCost, minPackageMaintCost: Double?
    
    
    enum CodingKeys: String, CodingKey {
        case packageDetailID = "PackageDetailId"
        case servicePackageID = "ServicePackageId"
        case frequencyID = "FrequencyId"
        case frequencyName = "FrequencyName"
        case packageCost = "PackageCost"
        case packageMaintCost = "PackageMaintCost"
        //  case servicePackageDc = "ServicePackageDc"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        case minPackageCost = "MinPackageCost"
        case minPackageMaintCost = "MinPackageMaintCost"
    }
}
