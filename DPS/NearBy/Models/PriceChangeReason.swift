//
//  PriceChangeReason.swift
//  DPS
//
//  Created by Vivek Patel on 01/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct PriceChangeReason: Codable {
    
    var title: String?
    var companyId: Int?
    var description: String?
    var isActive: Bool
    var priceChangeReasonId: Int
    
    enum CodingKeys: String, CodingKey {
        case companyId = "CompanyId"
        case description = "Description"
        case isActive = "IsActive"
        case priceChangeReasonId = "PriceChangeReasonId"
        case title = "Title"
    }
}
