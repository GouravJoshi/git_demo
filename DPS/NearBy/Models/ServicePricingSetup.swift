//
//  ServicePricingSetup.swift
//  DPS
//
//  Created by Vivek Patel on 23/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct ServicePricing: Codable {
    
    var serviceId: Int
    var serivceSysName: String?
    var frequencySysName: String?
    var isActive: Bool
    var isTBD: Bool
    var servicesPricingDetailDcs: [ServicePricingDetail] = []
    
    enum CodingKeys: String, CodingKey {
        case serviceId = "ServiceId"
        case serivceSysName = "ServiceSysName"
        case frequencySysName = "FrequencySysName"
        case isActive = "IsActive"
        case isTBD = "IsTBD"
        case servicesPricingDetailDcs = "ServicePricingDetailDcs"
    }
}

struct ServicePricingDetail: Codable {
    
    var initialPrice: Double?
    var maintenancePrice: Double?
    var isActive: Bool
    var servicesPricingParameterDcs: [ServicePricingParameterDcs] = []
    var minInitialPrice :Double?
    var minMaintenancePrice: Double?
    enum CodingKeys: String, CodingKey {
        case initialPrice = "InitialPrice"
        case maintenancePrice = "MaintenancePrice"
        case isActive = "IsActive"
        case servicesPricingParameterDcs = "ServicePricingParameterDcs"
        case minInitialPrice = "MinInitialPrice"
        case minMaintenancePrice = "MinMaintenancePrice"
    }
}

struct ServicePricingParameterDcs: Codable {
    var paramterId: Int
    var detailId: Int
    var sysName: String
    var rangeTo: Double
    var rangeFrom: Double
    var isActive: Bool
    
    enum CodingKeys: String, CodingKey {
        case paramterId = "ServicePricingParameterId"
        case detailId = "ServicePricingDetailId"
        case sysName = "ParameterSysName"
        case rangeTo = "RangeTo"
        case rangeFrom = "RangeFrom"
        case isActive = "IsActive"
    }
}
