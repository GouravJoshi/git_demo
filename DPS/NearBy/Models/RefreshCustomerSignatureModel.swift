//
//  RefreshCustomerSignatureModel.swift
//  DPS
//
//  Created by Saavan Patidar on 08/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let refreshCustomerSignatureModel = try? newJSONDecoder().decode(RefreshCustomerSignatureModel.self, from: jsonData)

import Foundation

// MARK: - RefreshCustomerSignatureModel
struct RefreshCustomerSignatureModel: Codable {
    let leadID: Int?
    let isCustomerNotPresent: Bool?
    let stageSysName, statusSysName: String?
    let isInitialSetupCreated: JSONNull?
    let leadPaymentDetailDc: LeadPaymentDetailDc?

    enum CodingKeys: String, CodingKey {
        case leadID = "LeadId"
        case isCustomerNotPresent = "IsCustomerNotPresent"
        case stageSysName = "StageSysName"
        case statusSysName = "StatusSysName"
        case isInitialSetupCreated = "IsInitialSetupCreated"
        case leadPaymentDetailDc = "LeadPaymentDetailDc"
    }
}

// MARK: - LeadPaymentDetailDc
struct LeadPaymentDetailDc: Codable {
    let leadPaymentDetailID, leadID: Int?
    let paymentMode: String?
    let amount: Double?
    let checkNo, licenseNo, expirationDate: String?
    let specialInstructions: String?
    let unsignedAgreement: String?
    let agreement, proposal, inspection: String?
    let customerSignature, salesSignature, checkFrontImagePath, checkBackImagePath: String?
    let proposalAdditionalNotes, documentMACAddress, documentIPAddress, officeAgreement: String?
    let unsignedOfficeAgreement: String?
    //let leadDc: JSONNull?
    let createdDate: String?
    let createdBy: Int?
    let modifiedBy: Int?
    let modifiedDate : String?

    enum CodingKeys: String, CodingKey {
        case leadPaymentDetailID = "LeadPaymentDetailId"
        case leadID = "LeadId"
        case paymentMode = "PaymentMode"
        case amount = "Amount"
        case checkNo = "CheckNo"
        case licenseNo = "LicenseNo"
        case expirationDate = "ExpirationDate"
        case specialInstructions = "SpecialInstructions"
        case unsignedAgreement = "UnsignedAgreement"
        case agreement = "Agreement"
        case proposal = "Proposal"
        case inspection = "Inspection"
        case customerSignature = "CustomerSignature"
        case salesSignature = "SalesSignature"
        case checkFrontImagePath = "CheckFrontImagePath"
        case checkBackImagePath = "CheckBackImagePath"
        case proposalAdditionalNotes = "ProposalAdditionalNotes"
        case documentMACAddress = "DocumentMacAddress"
        case documentIPAddress = "DocumentIpAddress"
        case officeAgreement
        case unsignedOfficeAgreement = "UnsignedOfficeAgreement"
        //case leadDc = "LeadDc"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}

// MARK: - Encode/decode helpers

/*class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}*/
