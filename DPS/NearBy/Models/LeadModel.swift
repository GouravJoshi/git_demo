//
//  LeadModel.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct LeadResponse: Codable {
    var leads: [Lead]
    enum CodingKeys: String, CodingKey {
        case leads = "Leads"
    }
}
// MARK: - Lead
struct Lead: Codable {
    var leadID: Int?
    var leadNumber, leadName: String?
    var address1, address2, cityName: String?
    var stateID: Int?
    var countyName, schoolDistrict, zipcode: String?
    var latitude, longitude: Double?
    var countryID: Int?
    var area, noofBedroom, noofBathroom, noofStory: String?
    var addressImagePath: String?
    var visitStatusID: Int?
    var visitDescription, visitStatusName, visitStatusSysName: String?
    var assignedToID: Int?
    var assignedType: String?
    var createdDate, clientCreatedDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    var flowType: String?
    var firstName, middleName, lastName, primaryEmail: String?
    var secondaryEmail, cellPhone1, primaryPhone, primaryPhoneEXT: String?
    var secondaryPhone, secondaryPhoneEXT: String?
    var statusName: String?
    var branchesSysNames: [String]?
    var webLeadSourceID: [Int]?
    var assignedToName: String?
    var crmContactID: Int?
    var companyKey: String?
    var industryID: Int?
    var statusDate: String?
    var accountNo: String?
    var crmCompany: CRMCompany?
    var webLeadServices: JSONNull?
    var objectionIds: [Int]?
    var visitCount: Int?
    var currentServicesProvider: String?
    
    enum CodingKeys: String, CodingKey {
        case leadID = "LeadId"
        case leadNumber = "LeadNumber"
        case leadName = "LeadName"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case countyName = "CountyName"
        case schoolDistrict = "SchoolDistrict"
        case zipcode = "Zipcode"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case countryID = "CountryId"
        case area = "Area"
        case noofBedroom = "NoofBedroom"
        case noofBathroom = "NoofBathroom"
        case noofStory = "NoofStory"
        case addressImagePath = "AddressImagePath"
        case visitStatusID = "VisitStatusId"
        case visitDescription = "VisitDescription"
        case visitStatusName = "VisitStatusName"
        case visitStatusSysName = "VisitStatusSysName"
        case assignedToID = "AssignedToId"
        case assignedType = "AssignedType"
        case createdDate = "CreatedDate"
        case clientCreatedDate = "ClientCreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        case flowType = "FlowType"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case cellPhone1 = "CellPhone1"
        case primaryPhone = "PrimaryPhone"
        case primaryPhoneEXT = "PrimaryPhoneExt"
        case secondaryPhone = "SecondaryPhone"
        case secondaryPhoneEXT = "SecondaryPhoneExt"
        case statusName = "StatusName"
        case branchesSysNames = "BranchesSysNames"
        case webLeadSourceID = "WebLeadSourceId"
        case assignedToName = "AssignedToName"
        case crmContactID = "CrmContactId"
        case companyKey = "CompanyKey"
        case industryID = "IndustryId"
        case statusDate = "StatusDate"
        case accountNo = "AccountNo"
        case crmCompany = "CrmCompany"
        case webLeadServices = "WebLeadServices"
        case objectionIds = "ObjectionIds"
        case visitCount = "VisitCount"
        case currentServicesProvider = "CurrentServicesProvider"
    }
}


// MARK: - CRMCompany
struct CRMCompany: Codable {
    var crmCompanyID: Int?
    var name: String?
    var primaryEmail: String?
    var secondaryEmail: String?
    var primaryPhone: String?
    var primaryPhoneEXT: String?
    var secondaryPhone: String?
    var secondaryPhoneEXT: String?
    var cellPhone1: String?
    var address1, address2, cityName: String?
    var stateID, countryID: Int?
    var zipCode: String?
    var companyID: Int?
    var isActive: Bool?
    var profileImage: String?
    
    enum CodingKeys: String, CodingKey {
        case crmCompanyID = "CrmCompanyId"
        case name = "Name"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case primaryPhone = "PrimaryPhone"
        case primaryPhoneEXT = "PrimaryPhoneExt"
        case secondaryPhone = "SecondaryPhone"
        case secondaryPhoneEXT = "SecondaryPhoneExt"
        case cellPhone1 = "CellPhone1"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case countryID = "CountryId"
        case zipCode = "ZipCode"
        case companyID = "CompanyId"
        case isActive = "IsActive"
        case profileImage = "ProfileImage"
    }
}
