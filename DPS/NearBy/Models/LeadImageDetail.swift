//
//  LeadImageDetail.swift
//  DPS
//
//  Created by Vivek Patel on 02/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

struct DTDLeadImagesDetailExtSerDc: Codable {
    var leadImageId: Int?
    var leadImageCaption: String?
    var leadImageType: String?
    var leadImagePath: String?
    var description: String?
    var latitude: Float?
    var isSync: Bool?
    var isAddendum: Bool?
    var isNewGraph: Bool?
    var leadXmlPath: String?
    var tempImage: UIImage?
    enum CodingKeys: String, CodingKey {
        case leadImageId = "LeadImageId"
        case leadImageCaption = "LeadImageCaption"
        case leadImageType = "LeadImageType"
        case leadImagePath = "LeadImagePath"
        case description = "Description"
        case latitude = "Latitude"
        case isSync = "IsSync"
        case isAddendum = "IsAddendum"
        case isNewGraph = "IsNewGraph"
        case leadXmlPath = "LeadXmlPath"
    }
    init() {}
}
