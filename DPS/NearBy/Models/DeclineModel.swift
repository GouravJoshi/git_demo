//
//  DeclineModel.swift
//  DPS
//
//  Created by Vivek Patel on 04/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
// MARK: - CancelReasonDc
struct CancelReasonDc: Codable {
    var cancelReasonMasterID: Int?
    var cancelReason: String?
    var isControllable: Bool?
    var type: String?
    var isActive: Bool?
    var companyID: Int?
    var cancelReasonDcDescription, createdDate: String?
    var createdBy: Int?

    enum CodingKeys: String, CodingKey {
        case cancelReasonMasterID = "CancelReasonMasterId"
        case cancelReason = "CancelReason"
        case isControllable = "IsControllable"
        case type = "Type"
        case isActive = "IsActive"
        case companyID = "CompanyId"
        case cancelReasonDcDescription = "Description"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
    }
}
