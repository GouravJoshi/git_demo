//
//  SendAgreement.swift
//  DPS
//
//  Created by Vivek Patel on 09/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
// MARK: - LeadResponse
struct EntityDetail: Codable {
    var leadDetail: LeadDetail?
    var paymentInfo: PaymentInfo?
    var emailDetail: [EmailDetail]?
    var documentDetail: [DocumentDetail]?
    var soldServiceStandardDetail: [SoldServiceStandardDetail]?
    var leadAppliedDiscounts: [LeadAppliedDiscount]?
    var electronicAuthorizationForm: ElectronicAuthorizationForm?
    var imagesDetail:[DTDLeadImagesDetailExtSerDc]?
    var renewalServiceEXTDCS: [RenewalServiceEXTDc]?
    var leadContactDetailEXTSerDc: [LeadContactDetailEXTSerDc]?
    var clickHereUrl: String?
    
    //Nilind
    var webLeadId: Int?
    var leadNumber: String?
    var thirdPartyAccountId: String?
    var billToLocationId: String?

    
    enum CodingKeys: String, CodingKey {
        case leadDetail = "LeadDetail"
        case paymentInfo = "PaymentInfo"
        case documentDetail = "DocumentsDetail"
        case emailDetail = "EmailDetail"
        case soldServiceStandardDetail = "SoldServiceStandardDetail"
        case leadAppliedDiscounts = "LeadAppliedDiscounts"
        case electronicAuthorizationForm = "ElectronicAuthorizationForm"
        case imagesDetail = "ImagesDetail"
        case renewalServiceEXTDCS = "RenewalServiceExtDcs"
        case leadContactDetailEXTSerDc = "LeadContactDetailExtSerDc"
        case clickHereUrl = "clickHereUrl"
        //Nilind
        case webLeadId = "WebLeadId"
        case leadNumber = "LeadNumber"
        case thirdPartyAccountId = "ThirdPartyAccountId"
        case billToLocationId = "BillToLocationId"
    }
    init (from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: CodingKeys.self)
        leadDetail = try container.decodeIfPresent(LeadDetail.self, forKey: .leadDetail)
        paymentInfo = try container.decodeIfPresent(PaymentInfo.self, forKey: .paymentInfo)
        emailDetail = try container.decodeIfPresent([EmailDetail].self, forKey: .emailDetail)
        documentDetail = try container.decodeIfPresent([DocumentDetail].self, forKey: .documentDetail)
        soldServiceStandardDetail = try container.decodeIfPresent([SoldServiceStandardDetail].self, forKey: .soldServiceStandardDetail)
        leadAppliedDiscounts = try container.decodeIfPresent([LeadAppliedDiscount].self, forKey: .leadAppliedDiscounts)
        electronicAuthorizationForm = try container.decodeIfPresent(ElectronicAuthorizationForm.self, forKey: .electronicAuthorizationForm)
        imagesDetail = try container.decodeIfPresent([DTDLeadImagesDetailExtSerDc].self, forKey: .imagesDetail)
        renewalServiceEXTDCS = try container.decodeIfPresent([RenewalServiceEXTDc].self, forKey: .renewalServiceEXTDCS)
        leadContactDetailEXTSerDc = try container.decodeIfPresent( [LeadContactDetailEXTSerDc].self, forKey: .leadContactDetailEXTSerDc)
        clickHereUrl = try container.decodeIfPresent(String.self, forKey: .clickHereUrl)
        webLeadId = try container.decodeIfPresent(Int.self, forKey: .webLeadId)
        leadNumber = try container.decodeIfPresent(String.self, forKey: .leadNumber)
        thirdPartyAccountId = try container.decodeIfPresent(String.self, forKey: .thirdPartyAccountId)
        billToLocationId = try container.decodeIfPresent(String.self, forKey: .billToLocationId)
    }
}
// MARK: - LeadDetail
struct LeadDetail: Codable {
    var leadID: Int?
    var leadNumber: String?
    var companyID: Int?
    var companyMappingKey, accountNo, leadName, firstName: String?
    var middleName: String?
    var lastName: String?
    var companyName, primaryEmail, secondaryEmail, cellNo: String?
    var primaryPhone, secondaryPhone: String?
    var salesRepID: Int?
    var scheduleStartDate, scheduleEndDate, inspectionDate: String?
    var estimatedValue, closeDate, primaryServiceSysName: String?
    var stageSysName, statusSysName: String?
    var prioritySysName: String?
    var sourceSysName: String?
    var tags, leadDetailDescription, notes: String?
    var isInspectionCompleted: Bool?
    var inspectionCompletedTime: String?
    var branchSysName: String?
    var divisionSysName: String?
    var departmentSysName, billingAddress1, billingAddress2, billingCountry: String?
    var billingState, billingCity, billingZipcode: String?
    var billingMapcode, billingCounty, billingSchoolDistrict: String?
    var billingAddressName, servicesAddress1, serviceAddress2, serviceCountry: String?
    var serviceState, serviceCity, serviceZipcode: String?
    var serviceGateCode, serviceNotes, serviceDirection, serviceMapCode: String?
    var serviceCounty, serviceSchoolDistrict: String?
    var serviceAddressName: String?
    var isAgreementGenerated: Bool?
    var collectedAmount, latitude, longitude: Double?
    var customerName: String?
    var surveyID: Int?
    var surveyStatus, leadGeneralTermsConditions: String?
    var tax: String?
    var isAgreementSigned, isProposalFromMobile, isAgreementPosted: Bool?
    var onMyWaySentSMSTime: String?
    var employeeNoMobile: String?
    var isInitialSetupCreated: Bool?
    var  versionDate, versionNumber, deviceName: String?
    var deviceVersion: String?
    var iAgreeTerms, isCustomerNotPresent, isBillingAddressSame: Bool?
    var audioFilePath, area, noofBedroom, noofBathroom: String?
    var lotSizeSqFt, linearSqFt, yearBuilt, noOfStory: String?
    var turfArea, shrubArea, accountDescription: String?
    var isEmployeePresetSignature: Bool?
    var taxableAmount, billedAmount: Double?
    var taxAmount, taxableMaintAmount, taxMaintAmount, totalPrice: Double?
    var couponDiscount, subTotalAmount: Double?
    var isServiceAddrTaxExempt: Bool?
    // var serviceAddrTaxExemptionNo: JSONNull?
    var serviceAddressSubType: String?
    var isCouponApplied: Bool?
    var billingFirstName: String?
    var billingMiddleName: String?
    var billingLastName: String?
    var billingPrimaryEmail, billingSecondaryEmail, billingPrimaryPhone, billingSecondaryPhone: String?
    var isResendAgreementProposalMail: Bool?
    var billingCellNo, accountName: String?
    var serviceFirstName: String?
    var serviceMiddleName: String?
    var serviceLastName: String?
    var servicePrimaryEmail, serviceSecondaryEmail, servicePrimaryPhone, serviceSecondaryPhone: String?
    var serviceCellNo, onMyWaySentSMSTimeLatitude, onMyWaySentSMSTimeLongitude: String?
    var preferredMonths: Int?
    var strPreferredMonth: String?
    var problemDescription: String?
    var salesType: String?
    var accountElectronicAuthorizationFormSigned: Bool?
    var thirdPartyAccountNo: String?
    var  taxSysName: String?
    // var submittedBy
    var coverLetterSysName, introductionSysName: String?
    var flowType, initialServiceDate: String?
    var recurringServiceMonth, proposalNotes: String?
    var totalMaintPrice, subTotalMaintAmount: Double?
    var scheduleDate, scheduleTime: String?
    var earliestStartTime, latestStartTime, secondEarliestStartTime, secondLatestStartTime: String?
    var scheduleTimeType: String?
    var driveTime: String?
    var accountManagerID: Int?
    var accountManagerName, accountManagerNo, accountManagerEmail, accountManagerPrimaryPhone: String?
    var serverSignedDate, clientSignedDate: String?
    var serviceAddressPropertyTypeSysName, profileImage: String?
    var tipDiscount, otherDiscount: Double?
    var isSelectionAllowedForCustomer, isMailSend, isCommLeadUpdated, isCommTaxUpdated: Bool?
    var isInvisible: Bool?
    //var leadInspectionFee: String?
    var leadInspectionFee: Double?
    var isTipEligible: Bool?
    var needTipService: String?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    var smsReminders, phoneReminders, emailReminders: Bool?
   
    
    enum CodingKeys: String, CodingKey {
        case smsReminders = "SmsReminders"
        case phoneReminders = "PhoneReminders"
        case emailReminders = "EmailReminders"
        case leadID = "LeadId"
        case leadNumber = "LeadNumber"
        case companyID = "CompanyId"
        case companyMappingKey = "CompanyMappingKey"
        case accountNo = "AccountNo"
        case leadName = "LeadName"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case companyName = "CompanyName"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case cellNo = "CellNo"
        case primaryPhone = "PrimaryPhone"
        case secondaryPhone = "SecondaryPhone"
        case salesRepID = "SalesRepId"
        case scheduleStartDate = "ScheduleStartDate"
        case scheduleEndDate = "ScheduleEndDate"
        case inspectionDate = "InspectionDate"
        case estimatedValue = "EstimatedValue"
        case closeDate = "CloseDate"
        case primaryServiceSysName = "PrimaryServiceSysName"
        case stageSysName = "StageSysName"
        case statusSysName = "StatusSysName"
        case prioritySysName = "PrioritySysName"
        case sourceSysName = "SourceSysName"
        case tags = "Tags"
        case leadDetailDescription = "Description"
        case notes = "Notes"
        case isInspectionCompleted = "IsInspectionCompleted"
        case inspectionCompletedTime = "InspectionCompletedTime"
        case branchSysName = "BranchSysName"
        case divisionSysName = "DivisionSysName"
        case departmentSysName = "DepartmentSysName"
        case billingAddress1 = "BillingAddress1"
        case billingAddress2 = "BillingAddress2"
        case billingCountry = "BillingCountry"
        case billingState = "BillingState"
        case billingCity = "BillingCity"
        case billingZipcode = "BillingZipcode"
        case billingMapcode = "BillingMapcode"
        case billingCounty = "BillingCounty"
        case billingSchoolDistrict = "BillingSchoolDistrict"
        case billingAddressName = "BillingAddressName"
        case servicesAddress1 = "ServicesAddress1"
        case serviceAddress2 = "ServiceAddress2"
        case serviceCountry = "ServiceCountry"
        case serviceState = "ServiceState"
        case serviceCity = "ServiceCity"
        case serviceZipcode = "ServiceZipcode"
        case serviceGateCode = "ServiceGateCode"
        case serviceNotes = "ServiceNotes"
        case serviceDirection = "ServiceDirection"
        case serviceMapCode = "ServiceMapCode"
        case serviceCounty = "ServiceCounty"
        case serviceSchoolDistrict = "ServiceSchoolDistrict"
        case serviceAddressName = "ServiceAddressName"
        case isAgreementGenerated = "IsAgreementGenerated"
        case collectedAmount = "CollectedAmount"
        case billedAmount = "BilledAmount"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case customerName = "CustomerName"
        case surveyID = "SurveyID"
        case surveyStatus = "SurveyStatus"
        case leadGeneralTermsConditions = "LeadGeneralTermsConditions"
        case tax = "Tax"
        case isAgreementSigned = "IsAgreementSigned"
        case isProposalFromMobile = "IsProposalFromMobile"
        case isAgreementPosted = "IsAgreementPosted"
        case onMyWaySentSMSTime = "OnMyWaySentSMSTime"
        case employeeNoMobile = "EmployeeNo_Mobile"
        case isInitialSetupCreated = "IsInitialSetupCreated"
        case versionDate = "VersionDate"
        case versionNumber = "VersionNumber"
        case deviceName = "DeviceName"
        case deviceVersion = "DeviceVersion"
        case iAgreeTerms = "IAgreeTerms"
        case isCustomerNotPresent = "IsCustomerNotPresent"
        case isBillingAddressSame = "IsBillingAddressSame"
        case audioFilePath = "AudioFilePath"
        case area = "Area"
        case noofBedroom = "NoofBedroom"
        case noofBathroom = "NoofBathroom"
        case lotSizeSqFt, linearSqFt, yearBuilt
        case noOfStory = "NoOfStory"
        case turfArea, shrubArea
        case accountDescription = "AccountDescription"
        case isEmployeePresetSignature = "IsEmployeePresetSignature"
        case taxableAmount = "TaxableAmount"
        case taxAmount = "TaxAmount"
        case taxableMaintAmount = "TaxableMaintAmount"
        case taxMaintAmount = "TaxMaintAmount"
        case totalPrice = "TotalPrice"
        case couponDiscount = "CouponDiscount"
        case subTotalAmount = "SubTotalAmount"
        case isServiceAddrTaxExempt = "IsServiceAddrTaxExempt"
        // case serviceAddrTaxExemptionNo = "ServiceAddrTaxExemptionNo"
        case serviceAddressSubType = "ServiceAddressSubType"
        case isCouponApplied = "IsCouponApplied"
        case billingFirstName = "BillingFirstName"
        case billingMiddleName = "BillingMiddleName"
        case billingLastName = "BillingLastName"
        case billingPrimaryEmail = "BillingPrimaryEmail"
        case billingSecondaryEmail = "BillingSecondaryEmail"
        case billingPrimaryPhone = "BillingPrimaryPhone"
        case billingSecondaryPhone = "BillingSecondaryPhone"
        case isResendAgreementProposalMail = "IsResendAgreementProposalMail"
        case billingCellNo = "BillingCellNo"
        case accountName = "AccountName"
        case serviceFirstName = "ServiceFirstName"
        case serviceMiddleName = "ServiceMiddleName"
        case serviceLastName = "ServiceLastName"
        case servicePrimaryEmail = "ServicePrimaryEmail"
        case serviceSecondaryEmail = "ServiceSecondaryEmail"
        case servicePrimaryPhone = "ServicePrimaryPhone"
        case serviceSecondaryPhone = "ServiceSecondaryPhone"
        case serviceCellNo = "ServiceCellNo"
        case onMyWaySentSMSTimeLatitude = "OnMyWaySentSMSTimeLatitude"
        case onMyWaySentSMSTimeLongitude = "OnMyWaySentSMSTimeLongitude"
        case preferredMonths = "PreferredMonths"
        case strPreferredMonth
        case problemDescription = "ProblemDescription"
        case salesType = "SalesType"
        case accountElectronicAuthorizationFormSigned = "AccountElectronicAuthorizationFormSigned"
        case thirdPartyAccountNo = "ThirdPartyAccountNo"
        // case submittedBy = "SubmittedBy"
        case taxSysName = "TaxSysName"
        case coverLetterSysName = "CoverLetterSysName"
        case introductionSysName = "IntroductionSysName"
        case flowType = "FlowType"
        case initialServiceDate = "InitialServiceDate"
        case recurringServiceMonth = "RecurringServiceMonth"
        case subTotalMaintAmount = "SubTotalMaintAmount"
        case totalMaintPrice
        case proposalNotes = "ProposalNotes"
        case scheduleDate = "ScheduleDate"
        case scheduleTime = "ScheduleTime"
        case earliestStartTime = "EarliestStartTime"
        case latestStartTime = "LatestStartTime"
        case secondEarliestStartTime = "SecondEarliestStartTime"
        case secondLatestStartTime = "SecondLatestStartTime"
        case scheduleTimeType = "ScheduleTimeType"
        case driveTime = "DriveTime"
        case accountManagerID = "AccountManagerId"
        case accountManagerName = "AccountManagerName"
        case accountManagerNo = "AccountManagerNo"
        case accountManagerEmail = "AccountManagerEmail"
        case accountManagerPrimaryPhone = "AccountManagerPrimaryPhone"
        case serverSignedDate = "ServerSignedDate"
        case clientSignedDate = "ClientSignedDate"
        case serviceAddressPropertyTypeSysName = "ServiceAddressPropertyTypeSysName"
        case profileImage = "ProfileImage"
        case tipDiscount = "TipDiscount"
        case otherDiscount = "OtherDiscount"
        case isSelectionAllowedForCustomer = "IsSelectionAllowedForCustomer"
        case isMailSend = "IsMailSend"
        case isCommLeadUpdated = "IsCommLeadUpdated"
        case isCommTaxUpdated = "IsCommTaxUpdated"
        case isInvisible = "IsInvisible"
        case leadInspectionFee = "LeadInspectionFee"
        case isTipEligible = "IsTipEligible"
        case needTipService = "NeedTipService"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        
    }
}
struct SoldServiceStandardDetail: Codable {
    var soldServiceStandardID, serviceID, packageID: Int?
    var initialPrice, modifiedInitialPrice, discount, discountPercentage, maintenancePrice, modifiedMaintenancePrice: Double?
    var isSold: Bool?
    var serviceFrequency, serviceTermsConditions, servicePackageName: String?
    var isTBD: Bool?
    var serviceSysName, initialServiceSysName, frequencySysName: String?
    var unit, finalUnitBasedInitialPrice, finalUnitBasedMaintPrice, bundleID: Int?
    var bundleDetailID: Int?
    var billingFrequencySysName: String?
    var categorySysName, departmentSysName: String?
    var billingFrequencyPrice, totalInitialPrice, totalMaintPrice: Double?
    var serviceDescription: String?
    var isChangeServiceDesc, isResidentialTaxable, isCommercialTaxable, isBidOnRequest: Bool?
    var internalNotes: String?
    var isParameterizedPriced: Bool?
    //var mobileID, serviceMasterDc: JSONNull?
    // var additionalParameterPriceDCS, renewalServiceDCS: [JSONAny]?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    var isSelected = true
    var priceModifiedDescription: String?
    var priceChangeReasonId: Int?
    var serviceTempMaitenance = 0.0
    var serviceTempMaintDiscPercentage = 0.0
    enum CodingKeys: String, CodingKey {
        case isParameterizedPriced = "IsParameterizedPriced"
        case priceChangeReasonId = "PriceChangeReasonId"
        case priceModifiedDescription = "PriceModifiedDescription"
        case soldServiceStandardID = "SoldServiceStandardId"
        case serviceID = "ServiceId"
        case packageID = "PackageId"
        case initialPrice = "InitialPrice"
        case modifiedInitialPrice = "ModifiedInitialPrice"
        case discount = "Discount"
        case discountPercentage = "DiscountPercentage"
        case maintenancePrice = "MaintenancePrice"
        case modifiedMaintenancePrice = "ModifiedMaintenancePrice"
        case isSold = "IsSold"
        case serviceFrequency = "ServiceFrequency"
        case serviceTermsConditions = "ServiceTermsConditions"
        case servicePackageName = "ServicePackageName"
        case isTBD = "IsTBD"
        case serviceSysName = "ServiceSysName"
        case initialServiceSysName = "InitialServiceSysName"
        case frequencySysName = "FrequencySysName"
        case unit = "Unit"
        case finalUnitBasedInitialPrice = "FinalUnitBasedInitialPrice"
        case finalUnitBasedMaintPrice = "FinalUnitBasedMaintPrice"
        case bundleID = "BundleId"
        case bundleDetailID = "BundleDetailId"
        case billingFrequencySysName = "BillingFrequencySysName"
        case categorySysName = "CategorySysName"
        case departmentSysName = "DepartmentSysName"
        case billingFrequencyPrice = "BillingFrequencyPrice"
        case totalInitialPrice = "TotalInitialPrice"
        case totalMaintPrice = "TotalMaintPrice"
        case serviceDescription = "ServiceDescription"
        case isChangeServiceDesc = "IsChangeServiceDesc"
        case isResidentialTaxable = "IsResidentialTaxable"
        case isCommercialTaxable = "IsCommercialTaxable"
        case internalNotes = "InternalNotes"
        // case mobileID = "MobileId"
        case isBidOnRequest = "IsBidOnRequest"
        // case serviceMasterDc = "ServiceMasterDc"
        // case additionalParameterPriceDCS = "AdditionalParameterPriceDcs"
        // case renewalServiceDCS = "RenewalServiceDcs"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
    init() {}
}
extension SoldServiceStandardDetail: Comparable {
    static func < (lhs: SoldServiceStandardDetail, rhs: SoldServiceStandardDetail) -> Bool {
        lhs.serviceSysName == rhs.serviceSysName
    }
    
    
}
// MARK: - DocumentDetail
struct DocumentDetail: Codable {
    var leadDocumentID: Int?
    var accountNo, scheduleStartDate, docType, title: String?
    var fileName: String?
    var documentDetailDescription, otherDocSysName: String?
    var isSync, isAddendum: Bool?
    var docFormatType: String?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    
    enum CodingKeys: String, CodingKey {
        case leadDocumentID = "LeadDocumentId"
        case accountNo = "AccountNo"
        case scheduleStartDate = "ScheduleStartDate"
        case docType = "DocType"
        case title = "Title"
        case fileName = "FileName"
        case documentDetailDescription = "Description"
        case otherDocSysName = "OtherDocSysName"
        case isSync = "IsSync"
        case isAddendum = "IsAddendum"
        case docFormatType = "DocFormatType"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}
struct PaymentInfo: Codable {
    var leadPaymentDetailID: Int?
    var paymentMode: String?
    var amount: Double?
    var checkNo, licenseNo: String?
    var expirationDate: String?
    var specialInstructions: String?
    var agreement, proposal, customerSignature: String?
    var salesSignature: String?
    var inspection: String?
    var checkFrontImagePath, checkBackImagePath: String?
    var proposalAdditionalNotes: String?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case leadPaymentDetailID = "LeadPaymentDetailId"
        case paymentMode = "PaymentMode"
        case amount = "Amount"
        case checkNo = "CheckNo"
        case licenseNo = "LicenseNo"
        case expirationDate = "ExpirationDate"
        case specialInstructions = "SpecialInstructions"
        case agreement = "Agreement"
        case proposal = "Proposal"
        case customerSignature = "CustomerSignature"
        case salesSignature = "SalesSignature"
        case inspection = "Inspection"
        case checkFrontImagePath = "CheckFrontImagePath"
        case checkBackImagePath = "CheckBackImagePath"
        case proposalAdditionalNotes = "ProposalAdditionalNotes"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        
    }
}
struct EmailDetail: Codable, Comparable {
    
    static func < (lhs: EmailDetail, rhs: EmailDetail) -> Bool {
        return lhs.emailID?.trimmed == rhs.emailID?.trimmed
    }
    
    var leadMailID: Int?
    var emailID, subject: String?
    var isMailSent, isCustomerEmail, isDefaultEmail: Bool?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    var isAddToProfile: Bool?
    
    enum CodingKeys: String, CodingKey {
        case leadMailID = "LeadMailId"
        case emailID = "EmailId"
        case subject = "Subject"
        case isMailSent = "IsMailSent"
        case isCustomerEmail = "IsCustomerEmail"
        case isDefaultEmail = "IsDefaultEmail"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        case isAddToProfile = "IsAddToProfile"
    }
}
// MARK: - RenewalServiceEXTDc
struct RenewalServiceEXTDc: Codable {
    var renewalServiceID, leadID, soldServiceStandardID, serviceID: Int?
    var serviceName: String?
    var renewalAmount: Double?
    var renewalPercentage: Int?
    var renewalFrequencySysName, renewalDescription, createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    
    enum CodingKeys: String, CodingKey {
        case renewalServiceID = "RenewalServiceId"
        case leadID = "LeadId"
        case soldServiceStandardID = "SoldServiceStandardId"
        case serviceID = "ServiceId"
        case serviceName = "ServiceName"
        case renewalAmount = "RenewalAmount"
        case renewalPercentage = "RenewalPercentage"
        case renewalFrequencySysName = "RenewalFrequencySysName"
        case renewalDescription = "RenewalDescription"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}
// MARK: - LeadAppliedDiscount
struct LeadAppliedDiscount: Codable {
    
    var leadAppliedDiscountID, leadID: Int?
    var serviceSysName: String?
    var discountSysName, discountType: String?
    var discountCode: String?
    var discountAmount: Double?
    var discountDescription: String?
    var name: String?
    var isDiscountPercent: Bool?
    var discountPercent: Int?
    var isActive: Bool?
    var soldServiceID: Int?
    var serviceType: String?
    var appliedInitialDiscount, appliedMaintDiscount: Double?
    var applicableForInitial, applicableForMaintenance, isApplied: Bool?
    var accountNo, createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case leadAppliedDiscountID = "LeadAppliedDiscountId"
        case leadID = "LeadId"
        case serviceSysName = "ServiceSysName"
        case discountSysName = "DiscountSysName"
        case discountType = "DiscountType"
        case discountCode = "DiscountCode"
        case discountAmount = "DiscountAmount"
        case discountDescription = "DiscountDescription"
        case name = "Name"
        case isDiscountPercent = "IsDiscountPercent"
        case discountPercent = "DiscountPercent"
        case isActive = "IsActive"
        case soldServiceID = "SoldServiceId"
        case serviceType = "ServiceType"
        case appliedInitialDiscount = "AppliedInitialDiscount"
        case appliedMaintDiscount = "AppliedMaintDiscount"
        case applicableForInitial = "ApplicableForInitial"
        case applicableForMaintenance = "ApplicableForMaintenance"
        case isApplied = "IsApplied"
        case accountNo = "AccountNo"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
    }
}
