//
//  LeadStageMasters.swift
//  DPS
//
//  Created by Vivek Patel on 25/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

// MARK: - WelcomeElement
struct LeadStageMasters: Codable {
    let leadStageID, statusID: Int?
    let sysName, name: String?
    let isActive, isSystem: Bool?
    let colorCode: String?

    enum CodingKeys: String, CodingKey {
        case leadStageID = "LeadStageId"
        case statusID = "StatusId"
        case sysName = "SysName"
        case name = "Name"
        case isActive = "IsActive"
        case isSystem = "IsSystem"
        case colorCode = "ColorCode"
    }
}
