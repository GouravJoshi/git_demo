//
//  AccountModel.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct AccountsResponse: Codable {
    var accounts: [Account]
    enum CodingKeys: String, CodingKey {
        case accounts = "Accounts"
    }
}

/// MARK: - Account
struct Account: Codable {
    var accountName: String?
    var accountID: Int?
    var accountNumber: String?
    var crmContactID: Int?
    var firstName: String?
    var middleName: String?
    var lastName, primaryPhone, secondaryPhone: String?
    var primaryPhoneEXT, secondaryPhoneEXT: String?
    var cell, primaryEmail: String?
    var secondaryEmail: String?
    var serviceAddress, billingAddress: Address?
    var accountCompany: AccountCompany?
    var latitude, longitude: Double?
    var createdDate, clientCreatedDate: String?
    var accountLastActivity: LastActivity?
    
    enum CodingKeys: String, CodingKey {
        case accountName = "AccountName"
        case accountID = "AccountId"
        case accountNumber = "AccountNumber"
        case crmContactID = "CrmContactId"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case primaryPhone = "PrimaryPhone"
        case secondaryPhone = "SecondaryPhone"
        case primaryPhoneEXT = "PrimaryPhoneExt"
        case secondaryPhoneEXT = "SecondaryPhoneExt"
        case cell = "Cell"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case serviceAddress = "ServiceAddress"
        case billingAddress = "BillingAddress"
        case accountCompany = "AccountCompany"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case createdDate = "CreatedDate"
        case clientCreatedDate = "ClientCreatedDate"
       case accountLastActivity = "AccountLastActivity"
    }
}

