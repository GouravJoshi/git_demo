//
//  OpportunityModel.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct OpportunityResponse: Codable {
    var opportunities: [Opportunity]
    enum CodingKeys: String, CodingKey {
        case opportunities = "Opportunities"
    }
}

// MARK: - OpportunityElement
struct Opportunity: Codable {
    var opportunityName: String?
    var opportunityID: Int?
    var opportunityNumber: String?
    var opportunityValue: Double?
    var confidenceLevel, serviceID, serviceCategoryID: Int?
    var service: String?
    var opportunityType: String?
    var techNote, followUpDate: String?
    var proposedAmount: Double?
    var salesAppDate, grabbedDate: String?
    var submittedDate, expectedClosingDate: String?
    var branchSysName: String?
    var fieldSalesPersonID: Int?
    var scheduleDate, scheduleTime, totalEstimationTime: String?
    var driveTime: String?
    var departmentSysName: String?
    var submittedByID: Int?
    var industryID: Int?
    var insideSalesPersonID: Int?
    var opportunityDescription: String?
    var statusID, stageID, accountID: Int?
    var billingAddressSameAsService: Bool?
    var billingLocationID: Int?
    var serviceLocationID: Int?
    var taxSysName: String?
    var notes, direction: String?
    var isTaxExempt: Bool?
    var taxExemptionNo: String?
    var opportunityStatus: String?
    var opportunityStage: String?
    var urgencyID: Int?
    var urgency: String?
    var sizeID: Int?
    var companySize: String?
    var crmContactID: Int?
    var firstName: String?
    var middleName: String?
    var lastName, primaryPhone, secondaryPhone: String?
    var primaryPhoneEXT, secondaryPhoneEXT: String?
    var cell: String?
    var primaryEmail: String?
    var secondaryEmail: String?
    var opportunityContactProfileImage: String?
    var opportunityContactFullName, accountNo: String?
    var customerRating, feedbackRating: Double?
    var accountContactName: String?
    var sourceIDSList: [Int]?
    var serviceAddress: Address?
    var billingAddress: Address?
    var opportunityContactAddress: OpportunityContactAddress?
    var accountCompany: AccountCompany?
   // var submittedBy: Int?
    var insideSalesPerson: String?
    var fieldSalesPerson: String?
    var latitude, longitude: Double?
    var createdDate, clientCreatedDate: String?
    var opportunityLastActivity: LastActivity?
    
    enum CodingKeys: String, CodingKey {
        case opportunityName = "OpportunityName"
        case opportunityID = "OpportunityId"
        case opportunityNumber = "OpportunityNumber"
        case opportunityValue = "OpportunityValue"
        case confidenceLevel = "ConfidenceLevel"
        case serviceID = "ServiceId"
        case serviceCategoryID = "ServiceCategoryId"
        case service = "Service"
        case opportunityType = "OpportunityType"
        case techNote = "TechNote"
        case followUpDate = "FollowUpDate"
        case proposedAmount = "ProposedAmount"
        case salesAppDate = "SalesAppDate"
        case grabbedDate = "GrabbedDate"
        case submittedDate = "SubmittedDate"
        case expectedClosingDate = "ExpectedClosingDate"
        case branchSysName = "BranchSysName"
        case fieldSalesPersonID = "FieldSalesPersonId"
        case scheduleDate = "ScheduleDate"
        case scheduleTime = "ScheduleTime"
        case totalEstimationTime = "TotalEstimationTime"
        case driveTime = "DriveTime"
        case departmentSysName = "DepartmentSysName"
        case submittedByID = "SubmittedById"
        case industryID = "IndustryId"
        case insideSalesPersonID = "InsideSalesPersonId"
        case opportunityDescription = "OpportunityDescription"
        case statusID = "StatusId"
        case stageID = "StageId"
        case accountID = "AccountId"
        case billingAddressSameAsService = "BillingAddressSameAsService"
        case billingLocationID = "BillingLocationId"
        case serviceLocationID = "ServiceLocationId"
        case taxSysName = "TaxSysName"
        case notes = "Notes"
        case direction = "Direction"
        case isTaxExempt = "IsTaxExempt"
        case taxExemptionNo = "TaxExemptionNo"
        case opportunityStatus = "OpportunityStatus"
        case opportunityStage = "OpportunityStage"
        case urgencyID = "UrgencyId"
        case urgency = "Urgency"
        case sizeID = "SizeId"
        case companySize = "CompanySize"
        case crmContactID = "CrmContactId"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case primaryPhone = "PrimaryPhone"
        case secondaryPhone = "SecondaryPhone"
        case primaryPhoneEXT = "PrimaryPhoneExt"
        case secondaryPhoneEXT = "SecondaryPhoneExt"
        case cell = "Cell"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case opportunityContactProfileImage = "OpportunityContactProfileImage"
        case opportunityContactFullName = "OpportunityContactFullName"
        case accountNo = "AccountNo"
        case customerRating = "CustomerRating"
        case feedbackRating = "FeedbackRating"
        case accountContactName = "AccountContactName"
        case sourceIDSList = "SourceIdsList"
        case serviceAddress = "ServiceAddress"
        case billingAddress = "BillingAddress"
        case opportunityContactAddress = "OpportunityContactAddress"
        case accountCompany = "AccountCompany"
      //  case submittedBy = "SubmittedBy"
        case insideSalesPerson = "InsideSalesPerson"
        case fieldSalesPerson = "FieldSalesPerson"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case createdDate = "CreatedDate"
        case clientCreatedDate = "ClientCreatedDate"
        case opportunityLastActivity = "OpportunityLastActivity"
    }
}


// MARK: - AccountCompany
struct AccountCompany: Codable {
    var crmCompanyID: Int?
    var name: String?
    var primaryEmail: String?
    var secondaryEmail: String?
    var primaryPhone: String?
    var primaryPhoneEXT: String?
    var secondaryPhone: String?
    var secondaryPhoneEXT: String?
    var cellPhone1: String?
    var address1: String?
    var address2: String?
    var cityName: String?
    var stateID, countryID: Int?
    var zipCode: String?
    var companyID: Int?
    var isActive: Bool?
    var profileImage: String?
    
    enum CodingKeys: String, CodingKey {
        case crmCompanyID = "CrmCompanyId"
        case name = "Name"
        case primaryEmail = "PrimaryEmail"
        case secondaryEmail = "SecondaryEmail"
        case primaryPhone = "PrimaryPhone"
        case primaryPhoneEXT = "PrimaryPhoneExt"
        case secondaryPhone = "SecondaryPhone"
        case secondaryPhoneEXT = "SecondaryPhoneExt"
        case cellPhone1 = "CellPhone1"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case countryID = "CountryId"
        case zipCode = "ZipCode"
        case companyID = "CompanyId"
        case isActive = "IsActive"
        case profileImage = "ProfileImage"
    }
}


// MARK: - Address
struct Address: Codable {
    var mapCode, gateCode: String?
    var addressSubType: String?
    var area, noofBedroom, noofBathroom, noofStory: String?
    var imagePath: String?
    var customerAddressID: Int?
    var address1: String?
    var address2: String?
    var cityName: String?
    var stateID: Int?
    var zipcode: String?
    var county, schoolDistrict: String?
    var billingPOCId: Int?
    enum CodingKeys: String, CodingKey {
        case mapCode = "MapCode"
        case gateCode = "GateCode"
        case addressSubType = "AddressSubType"
        case area = "Area"
        case noofBedroom = "NoofBedroom"
        case noofBathroom = "NoofBathroom"
        case noofStory = "NoofStory"
        case imagePath = "ImagePath"
        case customerAddressID = "CustomerAddressId"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case zipcode = "Zipcode"
        case county = "County"
        case schoolDistrict = "SchoolDistrict"
        case billingPOCId = "BillingPOCId"
    }
}

// MARK: - OpportunityContactAddress
struct OpportunityContactAddress: Codable {
    var customerAddressID: Int?
    var address1: String?
    var address2: String?
    var cityName: String?
    var stateID: Int?
    var zipcode: String?
    var county, schoolDistrict: String?
    
    enum CodingKeys: String, CodingKey {
        case customerAddressID = "CustomerAddressId"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case zipcode = "Zipcode"
        case county = "County"
        case schoolDistrict = "SchoolDistrict"
    }
}

struct LastActivity: Codable {
    var accountId: Int?
    var activityId: Int?
    var activityTime:String?
    var agenda:String?
    var clientCreatedDate:String?
    var fromDate:String?
    var leadId: Int?
    var logTypeId: Int?
   // var Participants
    var toDate:String?
    enum CodingKeys: String, CodingKey {
        case accountId = "AccountId"
        case activityId = "ActivityId"
        case activityTime = "ActivityTime"
        case agenda = "Agenda"
        case fromDate = "FromDate"
        case leadId = "LeadId"
        case logTypeId = "LogTypeId"
      //  case Participants = "Participants"
        case toDate = "ToDate"
    }
}
