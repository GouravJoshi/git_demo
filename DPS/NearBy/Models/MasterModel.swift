//
//  MasterModel.swift
//  DPS
//
//  Created by Vivek Patel on 08/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct MasterDetails: Codable {
    var categories: [Category] = []
    var servicesPricing: [ServicePricing] = []
    var frequencies: [Frequency] = []
    var addressPropertyTypeMaster: [AddressPropertyType] = []
    var taxMaster: [TaxDetail] = []
    var discountCredits: [DiscountMaster] = []
    var discountCoupons: [DiscountMaster] = []
    var priceChangeReason: [PriceChangeReason] = []
    var generalTermsConditions: GeneralTermsConditions?
    var status: [MasterStatus] = []
    var stages: [MasterStage] = []
    var textTemplatesSelectListDcs:  [TextTemplatesSelectListDCS] = []
    var electronicAuthorizationFormMaster: ElectronicAuthorizationFormMaster?
    var cancelReasonDc: [CancelReasonDc] =  []
    
   
    enum CodingKeys: String, CodingKey {
        case categories  = "Categories"
        case servicesPricing = "ServicePricingSetup"
        case frequencies = "Frequencies"
        case taxMaster =  "TaxMaster"
        case discountCredits = "DiscountSetupMasterCredit"
        case discountCoupons = "DiscountSetupMasterCoupon"
        case priceChangeReason = "PriceChangeReasons"
        case generalTermsConditions = "GeneralTermsConditions"
        case status = "Status"
        case stages = "Stages"
        case textTemplatesSelectListDcs = "TextTemplatesSelectListDcs"
        case electronicAuthorizationFormMaster = "ElectronicAuthorizationFormMaster"
        case cancelReasonDc = "CancelReasonDc"
      
        
    }
}


// MARK: - TextTemplatesSelectListDCS
struct TextTemplatesSelectListDCS: Codable {
    var textTemplateID: Int?
    var templateName, templateContent: String?
   // var isActive, isSystem, isDefault: Bool?
    
    enum CodingKeys: String, CodingKey {
        case textTemplateID = "TextTemplateId"
        case templateName = "TemplateName"
        case templateContent = "TemplateContent"
       /* case isActive = "IsActive"
        case isSystem = "IsSystem"
        case isDefault = "IsDefault"*/
    }
}
