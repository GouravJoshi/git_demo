//
//  SendTextModel.swift
//  DPS
//
//  Created by Vivek Patel on 10/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct LeadContactDetailEXTSerDc: Codable, Comparable  {
    
    static func < (lhs: LeadContactDetailEXTSerDc, rhs: LeadContactDetailEXTSerDc) -> Bool {
        return lhs.contactNumber?.trimmed == rhs.contactNumber?.trimmed
    }
    
    var subject: String?
    var leadContactID, leadID: Int?
    var isMessageSent, isCustomerNumber, isDefaultNumber: Bool?
    var companyKey, createdDate: String?
    var userName, modifiedDate: String?
    var contactNumber: String?
    var modifiedBy, createdBy: Int?
    enum CodingKeys: String, CodingKey {
        case subject = "Subject"
        case leadContactID = "LeadContactId"
        case isMessageSent = "IsMessageSent"
        case leadID = "LeadId"
        case companyKey = "CompanyKey"
        case createdDate = "CreatedDate"
        case isCustomerNumber = "IsCustomerNumber"
        case isDefaultNumber = "IsDefaultNumber"
        case modifiedBy = "ModifiedBy"
        case userName = "UserName"
        case createdBy = "CreatedBy"
        case modifiedDate = "ModifiedDate"
        case contactNumber = "ContactNumber"
    }
}
