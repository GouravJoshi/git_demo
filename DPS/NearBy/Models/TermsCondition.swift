//
//  TermsCondition.swift
//  DPS
//
//  Created by Vivek Patel on 05/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation


struct GeneralTermsConditions: Codable {
    
    var id: Int?
    var companyId: Int?
    var termsTitle: String?
    var termsConditions: String?
    var termsType: String?
    var sysName: String?
    var isActive: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case companyId = "CompanyId"
        case termsTitle = "TermsTitle"
        case termsConditions = "TermsConditions"
        case termsType = "TermsType"
        case sysName = "BranchSysName"
        case isActive = "IsActive"
    }
}
