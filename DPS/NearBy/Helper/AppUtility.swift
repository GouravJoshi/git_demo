//
//  AppUtility.swift
//  DPS
//
//  Created by Vivek Patel on 16/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

class AppUtility: NSObject {
    
    class func imageColor(image: UIImageView, color:UIColor){
        
        let templateImage = image.image?.withRenderingMode(.alwaysTemplate)
        image.image = templateImage
        image.tintColor = color
    }
    class func buttonImageColor(btn:UIButton,image: String, color:UIColor){
        let origImage = UIImage(named: image)
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        // btn.setImage(tintedImage, for: .normal)
        btn.setBackgroundImage(tintedImage, for: .normal)
        btn.tintColor = color
    }
    class func fetchStates() -> [State]?  {
        guard let path = Bundle.main.path(forResource: "StateListNew_Production", ofType: "json") else {
            return nil
        }
        do {
            let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let  states = try JSONDecoder().decode([State].self, from: jsonData)
            return states
        }
        catch {
            print("States", error.localizedDescription)
        }
        return nil
    }
    class func getPropertyDetailFromLead(_ leadDetail: Lead) -> PropertyInfo {
        var propertyInfo = PropertyInfo()
        var name  = ""
        if let leadName = leadDetail.leadName, !leadName.isEmpty {
            name = leadName
        } else {
         name = "\(leadDetail.firstName ?? "") \(leadDetail.middleName ?? "") \(leadDetail.lastName ?? "")"
        }
        propertyInfo.name = name
        propertyInfo.firstName = leadDetail.firstName
        propertyInfo.lastName =  leadDetail.lastName
        propertyInfo.phone =  leadDetail.primaryPhone ?? leadDetail.cellPhone1
        propertyInfo.email =  leadDetail.primaryEmail
        if let state = fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(state.name ?? ""), \(leadDetail.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            propertyInfo.state = state
            propertyInfo.address =  address
        } else {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(leadDetail.zipcode ?? "")"
            propertyInfo.address =  address
            
        }
        
        if WebService().checkIfBlank(strString: "\(leadDetail.address1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.cityName ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.zipcode ?? "")").count == 0 {
            
            propertyInfo.address = ""
            
        }
        
        
        propertyInfo.zipCode =  leadDetail.zipcode
        propertyInfo.city =  leadDetail.cityName
        if let area = leadDetail.area , !area.isEmpty  {
            propertyInfo.area = area
            propertyInfo.lotSizeSqFt = area
        }
        if let noofStory = leadDetail.noofStory , !noofStory.isEmpty {
            propertyInfo.noOfFloor = noofStory
        }
        var type = "\(leadDetail.flowType ?? "")"
        propertyInfo.flowType = leadDetail.flowType ?? "Residential"
        if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
            type.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
            propertyInfo.bedRoom =  noofBedroom
            propertyInfo.bathRoom =  noofBathroom
        } else {
            if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty {
                type.append(" \(noofBedroom) Bed")
                propertyInfo.bedRoom =  noofBedroom
            }
            if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
                type.append(" \(noofBathroom) Bath.")
                propertyInfo.bathRoom =  noofBathroom
            }
        }
        propertyInfo.type = type.trimmed
        propertyInfo.latitude = leadDetail.latitude
        propertyInfo.longitude = leadDetail.longitude
        propertyInfo.imageUrl = leadDetail.addressImagePath
        return propertyInfo
        
    }
   class func getServicePrices(_ service: Service? = nil, soldService: SoldServiceStandardDetail? = nil) -> [ServicePricing] {
        guard let servicesPrice = AppUserDefaults.shared.masterDetails?.servicesPricing.filter({ (servicePrice) -> Bool in
            if let service = soldService {
                return servicePrice.serivceSysName == service.serviceSysName 
            } else {
                return servicePrice.serivceSysName == service?.sysName
            }
        }) else { return [] }
        return servicesPrice
    }
  class  func getFrquencies(_ servicesPrice: [ServicePricing]? = nil, servicePackageDc: [ServicePackageDetail]? = nil) -> [Frequency] {
        var frequencies: [Frequency] = []
        if let servicesPrice = servicesPrice {
            for service in servicesPrice {
                if let frequency = AppUserDefaults.shared.masterDetails?.frequencies.filter({$0.sysName == service.frequencySysName}).first {
                    frequencies.append(frequency)
                }
            }
        } else {
            if let servicePackageDc = servicePackageDc {
                for service in servicePackageDc {
                    if let frequency = AppUserDefaults.shared.masterDetails?.frequencies.filter({$0.frequencyId == service.frequencyID}).first {
                        frequencies.append(frequency)
                    }
                }
            }
        }
        return frequencies
    }
    class func getPrice(_ service: Service? = nil, soldService: SoldServiceStandardDetail? = nil ,propertyDetails: PropertyInfo?) -> (intialPrice: Double, maintaincePrice: Double, servicesPricingDetailDcs: ServicePricingDetail?) {
        
        guard let mapDetail = propertyDetails, let servicesPrice = AppUserDefaults.shared.masterDetails?.servicesPricing.filter({ (servicePrice) -> Bool in
            if let service = soldService {
                return servicePrice.serivceSysName == service.serviceSysName  && servicePrice.frequencySysName == service.frequencySysName && servicePrice.isActive == true //Nilind true
            } else {
                return servicePrice.serivceSysName == service?.sysName  && servicePrice.frequencySysName == service?.defaultServiceFrequencySysName && servicePrice.isActive == true
            }
        }), let servicePrice = servicesPrice.first else { return (0, 0, nil) }
        for servicePricingDetailDcs in servicePrice.servicesPricingDetailDcs
        {
            if servicePricingDetailDcs.isActive == true //Nilind
            {
                let servicesPricingParameterDcs = servicePricingDetailDcs.servicesPricingParameterDcs.filter { (servicePricingParameterDcs) -> Bool in
                    return (servicePricingParameterDcs.sysName  == "NoofBedroom" || servicePricingParameterDcs.sysName == "Area" ||  servicePricingParameterDcs.sysName == "LotSize" || servicePricingParameterDcs.sysName == "NoofBathroom") && servicePricingParameterDcs.isActive == true //Nilind true
                }
                
                
                let servicesPricingParameterDcsCount = servicesPricingParameterDcs.count
                var satisfiedRange = 0
                for servicePricingParameterDcs in servicesPricingParameterDcs
                {
                    if servicePricingParameterDcs.isActive == true //Nilind
                    {
                        if servicePricingParameterDcs.sysName == "NoofBedroom", let value = mapDetail.bedRoom, let count = Double(value) {
                            if count >= servicePricingParameterDcs.rangeFrom && count <= servicePricingParameterDcs.rangeTo  {
                                satisfiedRange += 1
                            } else { break }
                        } else if servicePricingParameterDcs.sysName == "Area" , let value = mapDetail.area, let count = Double(value) {
                            if count >= servicePricingParameterDcs.rangeFrom && count <= servicePricingParameterDcs.rangeTo  {
                                satisfiedRange += 1
                            } else { break }
                        } else if servicePricingParameterDcs.sysName == "LotSize", let value = mapDetail.lotSizeSqFt, let count = Double(value) {
                            if count >= servicePricingParameterDcs.rangeFrom && count <= servicePricingParameterDcs.rangeTo  {
                                satisfiedRange += 1
                            } else { break }
                        } else if servicePricingParameterDcs.sysName == "NoofBathroom", let value = mapDetail.bathRoom, let count = Double(value) {
                            if count >= servicePricingParameterDcs.rangeFrom && count <= servicePricingParameterDcs.rangeTo  {
                                satisfiedRange += 1
                            } else { break }
                        }
                    }
                    
                }
                if servicesPricingParameterDcsCount == satisfiedRange {
                    return (intialPrice: servicePricingDetailDcs.initialPrice ?? 0, maintaincePrice:servicePricingDetailDcs.maintenancePrice ?? 0, servicesPricingDetailDcs: servicePricingDetailDcs)
                }
            }
        }
        return (0, 0, nil)
    }
   
    class func convertStringToDate(_ strDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        
        for format in getAllDateFormats() {
            dateFormatter.dateFormat = format
            if let date = dateFormatter.date(from: strDate) {
                return date
            }
        }
        return nil
    }
    class func convertDateToString(_ date: Date, toFormat dateFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = NSTimeZone.local
        return  dateFormatter.string(from: date)
    }
    class func convertStringToFormat(fromString strDate: String, toFormat dateFormat: String) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        var date: Date?
        for format in getAllDateFormats() {
            dateFormatter.dateFormat = format
            if let dateT = dateFormatter.date(from: strDate) {
                date = dateT
            }
        }
        guard let dat = date else {
            return nil
        }
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: dat)
    }
    class func getAllDateFormats() -> [String] {
        return ["yyyy-MM-dd'T'HH:mm:ss", "MM/dd/yyyy hh:mm a", "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss.SS", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy", "MM/dd/yyyy HH:mm", "HH:mm:ss", "hh:mm a", "yyyy-MM-dd"]
    }
    class func setEditable(_ entityDetail: EntityDetail?) -> Bool {
        if  let stageSysName =  entityDetail?.leadDetail?.stageSysName, let statusSysName = entityDetail?.leadDetail?.statusSysName {
            if statusSysName == "Complete" {
                if stageSysName == "Won" {
                    return false
                } else {
                    return true
                }
             } else {
                return true
             }
        } else {
            return true
        }
    }
    class func isSetupService(_ leadDetail: LeadDetail) -> Bool {
        if  let stageSysName =  leadDetail.stageSysName, let statusSysName = leadDetail.statusSysName {
            if statusSysName == "Complete" {
                if stageSysName == "Won" || stageSysName == "CompletePending" {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    class func getPropertyDetailFromLeadDetail(_ leadDetail: LeadDetail) -> PropertyInfo {
        
        var propertyInfo = PropertyInfo()
        
        propertyInfo.name = "\(leadDetail.firstName ?? "") \(leadDetail.middleName ?? "") \(leadDetail.lastName ?? "")".trimmed
        propertyInfo.middleName = leadDetail.middleName
        
        propertyInfo.firstName = leadDetail.firstName
        
        propertyInfo.lastName =  leadDetail.lastName
        
        //if(leadDetail.primaryPhone != nil && leadDetail.primaryPhone == "")
        if (leadDetail.primaryPhone != nil && leadDetail.primaryPhone != "")
        {
            propertyInfo.phone =  leadDetail.primaryPhone
        }else{
            propertyInfo.phone =  leadDetail.cellNo

        }
    
        propertyInfo.email =  leadDetail.primaryEmail
        
        
        //if let serviceAddress = opportunity.serviceAddress {

        let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(leadDetail.serviceState ?? "")")
        
        if dictStateDataTemp.count > 0 {
                            
            propertyInfo.state?.stateID = Int("\(dictStateDataTemp.value(forKey: "StateId")!)")!
            propertyInfo.state?.name = "\(dictStateDataTemp.value(forKey: "Name")!)"
            propertyInfo.state?.stateShortName = "\(dictStateDataTemp.value(forKey: "StateShortName")!)"
            propertyInfo.state?.stateSysName = "\(dictStateDataTemp.value(forKey: "StateSysName")!)"

            let address = "\(leadDetail.servicesAddress1 ?? ""), \(leadDetail.serviceCity ?? ""), \(dictStateDataTemp.value(forKey: "Name")!), \(leadDetail.serviceZipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")

            propertyInfo.address =  address
            
            if WebService().checkIfBlank(strString: "\(leadDetail.servicesAddress1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.serviceCity ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.serviceZipcode ?? "")").count == 0 {
                
                propertyInfo.address = ""
                
            }
                        
        }

            propertyInfo.zipCode =  leadDetail.serviceZipcode
        

            propertyInfo.city =  leadDetail.serviceCity

            if let area = leadDetail.area , !area.isEmpty {

                propertyInfo.lotSizeSqFt =  area

                propertyInfo.area =  leadDetail.area

            }

            if let noofStory = leadDetail.noOfStory , !noofStory.isEmpty {

                propertyInfo.noOfFloor =  leadDetail.noOfStory

            } else {

            }

            if propertyInfo.type != nil {

            } else {

                var typeInfo = "\(leadDetail.flowType ?? "")"

                if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty, let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {

                    propertyInfo.bedRoom =  noofBedroom

                    propertyInfo.bathRoom =  noofBathroom

                } else {

                    if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty {

                        typeInfo.append(" \(noofBedroom) Bed")

                        propertyInfo.bedRoom =  noofBedroom

                        

                    }
                    

                    if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {

                        typeInfo.append(" \(noofBathroom) Bath.")

                        propertyInfo.bathRoom =  noofBathroom

                    }

                }

                propertyInfo.type = typeInfo.trimmed

            }

        if let urlString = leadDetail.profileImage , !urlString.isEmpty  {
            
            if  let urlString = leadDetail.profileImage!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {

                propertyInfo.imageUrl = leadDetail.profileImage!

                }
            
        }
        
        //}
        
        
        
        if let area = leadDetail.area , !area.isEmpty  {
            
            propertyInfo.area = area
            
            propertyInfo.lotSizeSqFt = area
            
        }
        
        if let noofStory = leadDetail.noOfStory , !noofStory.isEmpty {
            
            propertyInfo.noOfFloor = noofStory
            
        }
        
        var type = "\(leadDetail.flowType ?? "")"
        
        propertyInfo.flowType = leadDetail.flowType ?? "Residential"
        
        if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
            
            type.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
            
            propertyInfo.bedRoom =  noofBedroom
            
            propertyInfo.bathRoom =  noofBathroom
            
        } else {
            
            if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty {
                
                type.append(" \(noofBedroom) Bed")
                
                propertyInfo.bedRoom =  noofBedroom
                
            }
            
            if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
                
                type.append(" \(noofBathroom) Bath.")
                
                propertyInfo.bathRoom =  noofBathroom
                
            }
            
        }
        
        propertyInfo.type = type.trimmed
        
        propertyInfo.latitude = leadDetail.latitude
        
        propertyInfo.longitude = leadDetail.longitude
        
        // propertyInfo.imageUrl = leadDetail.addressImagePath
        
        return propertyInfo
        
        
        
    }
    
    class func  getPropertyDetailFormOpportunity(_ opportunity: Opportunity) -> PropertyInfo{
        
        var propertyInfo = PropertyInfo()

        propertyInfo.name =  "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")".trimmed

        propertyInfo.firstName = opportunity.firstName

        propertyInfo.lastName =  opportunity.lastName

        propertyInfo.phone =  opportunity.primaryPhone ?? opportunity.cell

        propertyInfo.email =  opportunity.primaryEmail

        propertyInfo.latitude = opportunity.latitude

        propertyInfo.longitude = opportunity.longitude

        propertyInfo.flowType = opportunity.opportunityType ?? "Residential"

        if let serviceAddress = opportunity.serviceAddress {

            if let state = fetchStates()?.first(where: { (state) -> Bool in

                return state.stateID == serviceAddress.stateID

            }) {

                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(state.name ?? ""), \(serviceAddress.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")

                propertyInfo.state = state

                propertyInfo.address =  address
                
                if WebService().checkIfBlank(strString: "\(serviceAddress.address1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.cityName ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.zipcode ?? "")").count == 0 {
                    
                    propertyInfo.address = ""
                    
                }
                

            } else {

                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.zipcode ?? "")"

                propertyInfo.address =  address
                
                if WebService().checkIfBlank(strString: "\(serviceAddress.address1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.cityName ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.zipcode ?? "")").count == 0 {
                    
                    propertyInfo.address = ""
                    
                }


            }
            
//            let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(serviceAddress.stat ?? "")")
//
//            if dictStateDataTemp.count > 0 {
//
//                propertyInfo.state?.stateID = Int("\(dictStateDataTemp.value(forKey: "StateId")!)")!
//
//            }
            
            propertyInfo.zipCode =  serviceAddress.zipcode

            propertyInfo.city =  serviceAddress.cityName

            if let area = serviceAddress.area , !area.isEmpty {

                propertyInfo.lotSizeSqFt =  area

                propertyInfo.area =  serviceAddress.area

            }

            if let noofStory = serviceAddress.noofStory , !noofStory.isEmpty {

                propertyInfo.noOfFloor =  serviceAddress.noofStory

            } else {

            }

            if propertyInfo.type != nil {

            } else {

                var typeInfo = "\(opportunity.opportunityType ?? "")"

                if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty, let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {

                    typeInfo.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )

                    propertyInfo.bedRoom =  noofBedroom

                    propertyInfo.bathRoom =  noofBathroom

                } else {

                    if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty {

                        typeInfo.append(" \(noofBedroom) Bed")

                        propertyInfo.bedRoom =  noofBedroom

                        

                    }

                    if let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {

                        typeInfo.append(" \(noofBathroom) Bath.")

                        propertyInfo.bathRoom =  noofBathroom

                    }

                }

                propertyInfo.type = typeInfo.trimmed

            }

            if  let urlString = serviceAddress.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {

                propertyInfo.imageUrl = serviceAddress.imagePath

            }

        }

        return propertyInfo

    }
    /*
    class func printJSON<T: Encodable>(of type: T.Type = T.self) {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let jsonData = try encoder.encode(T)
            print(String(data: jsonData, encoding: .utf8)!)
        } catch  {}
    }*/
}
