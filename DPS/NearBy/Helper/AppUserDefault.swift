//
//  AppUserDefault.swift
//  DPS
//
//  Created by Vivek Patel on 11/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

@objcMembers public class AppUserDefaults: NSObject {
    
    static let shared = AppUserDefaults()
    private let recentSearchesKey = "recentSearchesKey"
    private let masterDetailsKey = "masterDetailsKey"
    private let leadDetailMasterKey = "LeadDetailMasterKey"
    private let filterDataKey = "FilterDataKey"
    private let isPreSetSignSalesKey = "isPreSetSignSalesKey"
    private let serviceTechSignPathKey = "ServiceTechSignPathKey"
    private let taxCodeReqKey = "TaxCodeReq"//"TaxCodeReqKey" //
    private let loginDetailsKey = "LoginDetailsKey"
    private let employeeListKey = "employeeListKey"
    
    var recentSearches: [RecentSearch]? {
        get{
            do{
                guard let jsonData  = UserDefaults.standard.data(forKey: recentSearchesKey) else{
                    return nil
                }
                let userData = try JSONDecoder().decode([RecentSearch].self, from: jsonData)
                return userData
            }catch{
                return nil
            }
        }set{
            do{
                UserDefaults.standard.setValue(try JSONEncoder().encode(newValue), forKey: recentSearchesKey)
            }catch{
                UserDefaults.standard.setValue(nil, forKey: recentSearchesKey)
            }
        }
    }
    var masterDetails: MasterDetails? {
        get{
            do{
                guard let jsonData  = UserDefaults.standard.data(forKey: masterDetailsKey) else{
                    return nil
                }
                let userData = try JSONDecoder().decode(MasterDetails.self, from: jsonData)
                return userData
            }catch{
                return nil
            }
        }set{
            do{
                UserDefaults.standard.setValue(try JSONEncoder().encode(newValue), forKey: masterDetailsKey)
            }catch{
                UserDefaults.standard.setValue(nil, forKey: masterDetailsKey)
            }
        }
    }
    var leadDetailMaster: LeadDetailMaster? {
        get{
            do{
                guard let jsonData  = UserDefaults.standard.data(forKey: leadDetailMasterKey) else{
                    return nil
                }
                let userData = try JSONDecoder().decode(LeadDetailMaster.self, from: jsonData)
                return userData
            }catch{
                return nil
            }
        }set{
            do{
                UserDefaults.standard.setValue(try JSONEncoder().encode(newValue), forKey: leadDetailMasterKey)
            }catch{
                UserDefaults.standard.setValue(nil, forKey: leadDetailMasterKey)
            }
        }
    }
    var employeeList: [EmployeeListD2D]? {
        get{
            do{
                guard let jsonData  = UserDefaults.standard.data(forKey: employeeListKey) else{
                    return nil
                }
                let userData = try JSONDecoder().decode([EmployeeListD2D].self, from: jsonData)
                return userData
            }catch{
                return nil
            }
        }set{
            do{
                UserDefaults.standard.setValue(try JSONEncoder().encode(newValue), forKey: employeeListKey)
            }catch{
                UserDefaults.standard.setValue(nil, forKey: employeeListKey)
            }
        }
    }
    /*  var nearbyFilterData: NearbyFilterData {
     get{
     do{
     guard let jsonData  = UserDefaults.standard.data(forKey: filterDataKey) else{
     return NearbyFilterData()
     }
     let userData = try JSONDecoder().decode(NearbyFilterData.self, from: jsonData)
     return userData
     } catch { return NearbyFilterData() }
     }set{
     do{
     UserDefaults.standard.setValue(try JSONEncoder().encode(newValue), forKey: filterDataKey)
     }catch{
     UserDefaults.standard.setValue(NearbyFilterData(), forKey: filterDataKey)
     }
     }
     }*/
    var isShowSendText: Bool  {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        return dictLoginData.value(forKeyPath:"Company.CompanyConfig.ShowTextOnMobile") as? Bool ?? false
    }
    var isElectronicAuthorizationForm: Bool  {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        return dictLoginData.value(forKeyPath:"Company.CompanyConfig.IsElectronicAuthorizationForm") as? Bool ?? false
    }
    
    var isPreSetSignSales: Bool  {
        get {
            return UserDefaults.standard.bool(forKey: isPreSetSignSalesKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: isPreSetSignSalesKey)
        }
    }
    var isTaxRequired: Bool  {
        get {
            return UserDefaults.standard.bool(forKey: taxCodeReqKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: taxCodeReqKey)
        }
    }
    var loginDetails: [String: Any]  {
        get {
            return UserDefaults.standard.value(forKey: loginDetailsKey) as? [String: Any] ?? [:]
        } set {
            UserDefaults.standard.set(newValue, forKey: loginDetailsKey)
        }
    }
    var serviceTechSignPath:String?{
        get{
            return UserDefaults.standard.string(forKey: serviceTechSignPathKey)
        }set{
            UserDefaults.standard.set(newValue, forKey: serviceTechSignPathKey)
        }
    }
    
    public func setMasterDetails(_ data: Data) {
        do {
            // let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
            let object = try JSONDecoder().decode(MasterDetails.self, from: data)
            self.masterDetails = object
            print(object)
            
        } catch (let error) {
            print("MasterSalesAutomation", error.localizedDescription)
        }
    }
    public func setLeadDetails(_ data: Data) {
        do {
            // let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
            let object = try JSONDecoder().decode(LeadDetailMaster.self, from: data)
            self.leadDetailMaster = object
            print(object)
            
        } catch (let error) {
            print("Data type LeadDetailMaster", error.localizedDescription)
        }
    }
}
