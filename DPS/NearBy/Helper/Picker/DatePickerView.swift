//
//  DatePickerView.swift
//
//  Created by Vivek Patel on 09/02/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit

class DatePickerView: UIView {

    // MARK: - Variable
    var intialDate = Date()
    var minimumDate:Date?
    var maximumDate:Date?
    var minuteInterval:Int?

    var pickerType:PickerType = .date
    // MARK: - Outlet
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var blurView: UIVisualEffectView!

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        pickerView.date = intialDate
        if #available(iOS 13.4, *) {
            pickerView.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        if let minimumDate = minimumDate{
            pickerView.minimumDate = minimumDate
        }
        if let maximumDate = maximumDate{
            pickerView.maximumDate = maximumDate
        }
        if pickerType == .date {
            pickerView.datePickerMode = .date
        }else{
            pickerView.datePickerMode = .dateAndTime
            
        }
    }

}
