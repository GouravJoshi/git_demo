//
//  MultipleSelectionTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 23/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class MultiselectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageCheckUncheck: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
