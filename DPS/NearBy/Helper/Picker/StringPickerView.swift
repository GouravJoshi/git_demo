//
//  StringPickerView.swift
//
//  Created by Vivek Patel on 09/02/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit

class StringPickerView: UIView {

    lazy var rows:[String] = []
    var selectedRow = 0
    // MARK:- Outlets
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var blurView: UIVisualEffectView!

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(selectedRow, inComponent: 0, animated: false)
    }
   
}
// MARK: - UIPickerViewDataSource
extension StringPickerView:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return rows.count
    }
}
extension StringPickerView:UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rows[row]
    }
}
