//
//  PickerViewController.swift
//
//  Created by Vivek Patel on 09/02/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit

enum PickerType:String {
    case string
    case date
    case dateTime
}

public typealias StringPickerActionHandler = (_ selectedRow:Int, _ selectedString:String, _ view:UIView?) -> ()
public typealias DatePickerActionHandler = (_ selectedDate:Date, _ view:UIView?) -> Void
public typealias MultiSelectActionHandler = (_ selectedRows:[Int], _ view:UIView?) -> ()

class PickerViewController: UIViewController {
    
    // MARK: - Variable
    lazy var pickerType:PickerType = .string
    var viewClicked:UIView?
    lazy var selectedRow = 0
    var doneAction:StringPickerActionHandler?
    var dateAction:DatePickerActionHandler?
    lazy var intialDate = Date()
    var minimumDate:Date?
    var maximumDate:Date?
    lazy var rows:[String] = []
    
    // MARK: - Outlets
    @IBOutlet var stringPickerView: StringPickerView!
    @IBOutlet var datePickerView: DatePickerView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func loadView() {
        if pickerType == .string {
            self.view = stringPickerView
            stringPickerView.rows = rows
            stringPickerView.pickerView.reloadAllComponents()
            stringPickerView.selectedRow = selectedRow
            stringPickerView.blurView.addGestureRecognizer(tapGesture)
        }else{
            self.view = datePickerView
            datePickerView.intialDate = intialDate
            datePickerView.pickerType = pickerType
            datePickerView.minimumDate = minimumDate
            datePickerView.maximumDate = maximumDate
            datePickerView.blurView.addGestureRecognizer(tapGesture)
        }
    }
    // MARK: - Action
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionDone(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.pickerType == .string {
            
                let row = self.stringPickerView.pickerView.selectedRow(inComponent: 0)
                let string = self.rows[row]
                if let doneAction = self.doneAction{
                    doneAction(row,string,self.viewClicked)
            }
        } else {
            if let dateAction = self.dateAction{
                dateAction(self.datePickerView.pickerView.date,self.viewClicked!)
            }
        }
        }
    }
    
    
}
