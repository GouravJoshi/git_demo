//
//  MultiSelectionViewController.swift
//  DPS
//
//  Created by Vivek Patel on 23/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

import Foundation
import UIKit

class MultiSelectionViewController: UIViewController {
    
    //MARK: - Variable
    var viewClicked:UIView?
    var selectedRow = 0
    var doneAction: MultiSelectActionHandler?
    lazy var rows: [MultiSelectRow] = []
    
    //MARK: - Outlet
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - Action
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionDone(_ sender: Any) {
        
        var selectedRows:[Int] = []
        
        for (index,row) in rows.enumerated() {
            if row.isSelected{
                selectedRows.append(index)
            }
        }
        
        if let doneAction = doneAction{
            doneAction(selectedRows, viewClicked)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - TableView DataSource
extension MultiSelectionViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MultiselectionTableViewCell", for: indexPath) as? MultiselectionTableViewCell {
            
            let row =  rows[indexPath.row]
            cell.lblTitle?.text = row.title
            //cell.imageCheckUncheck.backgroundColor = row.isSelected ? UIColor.appThemeColor : .appLightGray
            cell.imageCheckUncheck.image =  row.isSelected ? UIImage(named: "checked.png") : UIImage(named: "check_box_1New.png") 
            // UIImage(named:"checkmark.square.fill") : UIImage(named: "square")
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
extension MultiSelectionViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var row =  rows[indexPath.row]
        row.isSelected = !row.isSelected
        rows[indexPath.row] = row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
