//
//  Typealiases.swift
//  DPS
//
//  Created by Vivek Patel on 30/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

typealias SignatureActionHandler = (_ signature:UIImage?) -> ()
