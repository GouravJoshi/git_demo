//
//  SchedulePresenter.swift
//  DPS
//
//  Created by Vivek Patel on 30/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

protocol ViewScheduleView: CommonView {
    func gotList(_ output: [Schedules])
}

class VisitServicePresenter: NSObject {
    weak private var delegate: ViewScheduleView?
    private lazy var service = {
        
        return VisitScheduleService()
    }()
    
    init(_ delegate: ViewScheduleView) {
        self.delegate = delegate
    }
    
    
    func apiCallForScheduleList() {
        delegate?.showLoader()
        service.getScheduleList() { (object, error) in
            self.delegate?.hideLoader()
            if let object = object{
                self.delegate?.gotList(object)
                //self.delegate?.gotList(object.reversed())
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}


