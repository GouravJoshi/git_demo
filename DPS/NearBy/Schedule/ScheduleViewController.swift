//
//  ScheduleViewController.swift
//  DPS
//
//  Created by Vivek Patel on 30/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ScheduleViewController: BaseViewController, FilterViewScheduleProtocol {
    //MARK:- Variables
    enum ScheduleTabs: Int, CaseIterable {
        case all = 0
        case schedule
        case incomplete
        case complete
        case cancelled
        case void
        var title: String {
            switch self {
            case .all:
                return  "All"
            case .schedule:
                return  "Schedule"
            case .incomplete:
                return "Incomplete"
            case .complete:
                return "Complete"
            case .cancelled:
                return "Cancelled"
            case .void:
                return "Void"
            }
        }
    }

    private lazy var servicePresenter = {
        return ServicePresenter(self)
    }()
    private lazy var presenter = {
        return VisitServicePresenter(self)
    }()
    private var selectedTab: ScheduleTabs = .all
    private var schedules: [Schedules] = [] {
        didSet {
              lblNoRecordFound.isHidden =  schedules.isEmpty ? false : true
            tableView.reloadData()
        }
    }
    private var allSchedules: [Schedules] = [] {
        didSet {
            filterData()
            tableView.reloadData()
            
        }
    }
    
    //Nilind
    
    // MARK: - Variables
    var propertyDetails: PropertyDetails?
    lazy var services: [Service] = []
    lazy var soldServices: [SoldServiceStandardDetail] = []
    var servicesToSync: [SoldServiceStandardDetail] = []

    var entityDetail: EntityDetail? {
        didSet {
            //setBillingAddressData()
        }
    }
    var customerNotes: String?
    lazy var plusServices: [Service] = []
    var delegate: EditLeadDelegate?
    var delegateEdit: EditableStatusDelegate?
   
    var billingAddress: SendAgreement?
    var logType: LogType = .visit
    var offlineDelegate: OfllinePropertyInforUpdate?
//    private var isEditable = true {
//        didSet {
//            tableView.reloadData()
//        }
//    }
    
    //End
    var strScheduleDateTime = String()
    var strReScheduleDate = String()
    var strReScheduleTime = String()
    var strWorkOrderId = Int()
    var timeRangeIdSelected = Int()
    var isReschedule = false

    //MARK:- Outlets
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!

    
    var aryTimeRange = NSArray()
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "View Schedule"
        setFooterView()
        CallTimeRangeAPI()
        presenter.apiCallForScheduleList()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        nsud.setValue(NSDictionary(), forKey: "ViewScheduleFilter")
        nsud.synchronize()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "AddedCreditCard_Notification"),
        object: nil,
        queue: nil,
        using:catchNotificationAddedCreditCard)
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"),
        object: nil,
        queue: nil,
        using:catchNotificationReloadSetupAppointments)
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "reloadonEditViewSchedule"),
        object: nil,
        queue: nil,
        using:catchNotificationReloadEdit)
        
        
        //---SearchBar------
               if #available(iOS 13.0, *) {
                   searchBar.searchTextField.font = DeviceType.IS_IPAD ? UIFont.systemFont(ofSize: 22) : UIFont.systemFont(ofSize: 16)
               }
               if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
                   txfSearchField.borderStyle = .none
                   txfSearchField.backgroundColor = .white
                   txfSearchField.layer.cornerRadius = 15
                   txfSearchField.layer.masksToBounds = true
                   txfSearchField.layer.borderWidth = 1
                   txfSearchField.layer.borderColor = UIColor.white.cgColor
               }
               searchBar.layer.borderWidth = 0
               searchBar.layer.opacity = 1.0
               
         //---End------

    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
                
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    @IBAction func actionFilter(_ sender: Any) {
        let storyboard =  UIStoryboard.init(name: "ScheduleD2D", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FilterViewScheduleViewController") as! FilterViewScheduleViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func actionHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnClickVoiceRecognizationButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.schedules.count != 0 {
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpeechRecognitionVC") as! SpeechRecognitionVC
             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true, completion: {})
        }
    }
    private func filterData() {
      
//        let sorted = allSchedules.sorted { first, second -> Bool in
//            let dateClientCreated = Global().convertDate("\(first.clientCreatedDate!)")
//            let dateClientCreated2 = Global().convertDate("\(second.clientCreatedDate!)")
//            let strfirst = Global().convertStringDateToDate(forCrmNewFlow: dateClientCreated, "MM/dd/yyyy" , "UTC")
//            let strsecond = Global().convertStringDateToDate(forCrmNewFlow: dateClientCreated2, "MM/dd/yyyy" , "UTC")
//
//            return strfirst! > strsecond!
//        }
//
//        allSchedules = sorted
   //     print(sorted)
        
        switch selectedTab {
        
        case .all:
            schedules = allSchedules
        case .schedule:
            schedules = allSchedules.filter({$0.scheduleStatus == .schedule})
        case .complete:
            schedules = allSchedules.filter({$0.scheduleStatus == .complete})
        case .incomplete:
            schedules = allSchedules.filter({$0.scheduleStatus == .inComplete})
        case .cancelled:
            schedules = allSchedules.filter({$0.scheduleStatus == .cancelled})
        case .void:
            schedules = allSchedules.filter({$0.scheduleStatus == .void})
        }
        
    }
    func CallTimeRangeAPI() {
        
        let strURL = MainUrl + UrlSalesRangeofTime + Global().getCompanyKey()
        WebService.callAPIBYGETWithoutKey(parameter: NSDictionary(), url: strURL) { [self] (responce, status) in
            print(responce)
            if(responce.value(forKey: "data") is NSArray){
               aryTimeRange = NSArray()
                let  aryTemp = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                aryTimeRange = aryTemp.filter { (task) -> Bool in
                    return ("\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("1") || "\((task as! NSDictionary).value(forKey: "IsActive")!)".lowercased().contains("true"))
                } as NSArray
            }else{
               // showAlert(message: alertSomeError)
            }
        }
        
    }
    func getDataFromFilterSchdule(dict: NSDictionary, tag: Int) {
        print(dict)
        print(tag)
    }
    
    func catchNotificationAddedCreditCard(notification:Notification) {
        
        tableView.reloadData()
        
    }
    func catchNotificationReloadSetupAppointments(notification:Notification) {
        
        presenter.apiCallForScheduleList()
        
    }
    func catchNotificationReloadEdit(notification:Notification) {
        
        presenter.apiCallForScheduleList()
        
    }
}

extension ScheduleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedules.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ViewScheduleUITableViewCell.self), for: indexPath) as! ViewScheduleUITableViewCell
        let schedule = schedules[indexPath.row]
        cell.schedule = schedule
        cell.btnAddCard.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        cell.btnReSchedule.tag = indexPath.row
//        if schedule.workOrderId == 650369
//        {
//            cell.backgroundColor = UIColor.red
//        }
        cell.btnAddCard.addTarget(self, action: #selector(btnAddCardClick), for: .touchUpInside)
        cell.btnEdit.addTarget(self, action: #selector(btnEditClick), for: .touchUpInside)
        cell.btnReSchedule.addTarget(self, action: #selector(btnReScheduleClick), for: .touchUpInside)
        
        cell.lblTimeRange.text = ""

        if let timeRangeId = schedule.timeRangeId  {
            let aryTemp = aryTimeRange.filter { (task) -> Bool in
                return "\((task as! NSDictionary).value(forKey: "RangeofTimeId")!)".lowercased().contains("\(timeRangeId)")
                
            }
            
            if(aryTemp.count != 0){
                var dict = NSDictionary()
                dict = aryTemp[0] as! NSDictionary
                let StartInterval = "\(dict.value(forKey: "StartInterval")!)"
                let EndInterval = "\(dict.value(forKey: "EndInterval")!)"
                let strrenge = StartInterval + " - " + EndInterval
                cell.lblTimeRange.text = "Time Range " + strrenge

            }else{
                cell.lblTimeRange.text = ""
            }
                
        }

        return cell
        
    }
    
    @objc func btnAddCardClick(sender: UIButton) {
        
        let scheduleL = schedules[sender.tag]
        let dictPaymentDetails = NSMutableDictionary()
        let firstNameL = "\(scheduleL.billingFirstName ?? "")"
        let lastNameL = "\(scheduleL.billingLastName ?? "")"
        let billingAddress1L = "\(scheduleL.billingAddressLine1 ?? "")"
        let billingZipcodeL = "\(scheduleL.billingZipcode ?? "")"
        let billToLocationIdL = "\(scheduleL.billToLocationId ?? "")"
        let webLeadIdL = "\(scheduleL.saLeadId ?? 0)"
        let contactNameToCheck = "\(firstNameL)" + "\(lastNameL)"

        let contactName = "\(firstNameL)" + " " + "\(lastNameL)"
        
        let ammountL = 10.0
                    
        if contactNameToCheck.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing contact name.", viewcontrol: self)
            
        }
        else if contactName.count > 30 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Billing contact name must be less than 30 characters", viewcontrol: self)
            
        }
        else if billingAddress1L.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing address.", viewcontrol: self)
            
        }else if billingZipcodeL.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billing zipcode.", viewcontrol: self)
            
        }else if billToLocationIdL.count == 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide billto location id.", viewcontrol: self)
            
        }else if webLeadIdL.count == 0 || webLeadIdL == "0" {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Lead does not exist.", viewcontrol: self)
            
        }else if ammountL <= 0 {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: "Please provide amount to charge.", viewcontrol: self)
            
        }else{
            
            dictPaymentDetails.setValue(webLeadIdL, forKey: "leadId")
            dictPaymentDetails.setValue(billToLocationIdL, forKey: "billToId")
            dictPaymentDetails.setValue(ammountL, forKey: "amount")
            dictPaymentDetails.setValue(contactName, forKey: "nameOnCard")
            dictPaymentDetails.setValue(billingAddress1L, forKey: "billingAddress")
            dictPaymentDetails.setValue(billingZipcodeL, forKey: "billingPostalCode")
            
            let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "PestPacPayment" : "PestPacPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "PestPacPaymentVC") as? PestPacPaymentVC
            vc?.dictPaymentDetails = dictPaymentDetails
            self.navigationController?.present(vc!, animated: false, completion: nil)

        }
        
    }
    @objc func btnEditClick(sender: UIButton) {
        
        let scheduleL = schedules[sender.tag]
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "Service" : "Service", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditViewControllerVC") as? EditViewControllerVC
        vc?.schedules = scheduleL
        //self.navigationController?.present(vc!, animated: false, completion: nil)
        self.navigationController?.pushViewController(vc!, animated: true)

    }

    @objc func btnReScheduleClick(sender: UIButton) {
        
        let tempData = schedules[sender.tag]
        
        switch tempData.scheduleStatus {
        case .schedule:
            isReschedule = true
            
        case .complete:
            isReschedule = false
            
        case .inComplete:
            isReschedule = false
            
        case .cancelled:
            isReschedule = false
            
        case .void:
            isReschedule = false
            
        case .none:
            isReschedule = false

        }
        
        if let timeRangeId = tempData.timeRangeId  {
            
            timeRangeIdSelected = timeRangeId

        }else{
            
            timeRangeIdSelected = 0

        }
        
        
//        let strDate = tempData.serviceDate
//        let strTime = tempData.scheduleTime

        if let strDate = tempData.serviceDate, let strTime = tempData.scheduleTime
        {
            let strDateNew = changeStringDateToGivenFormat(strDate: strDate, strRequiredFormat: "MM/dd/yyyy")
            let strTimeNew = changeStringDateToGivenFormat(strDate: strTime, strRequiredFormat: "hh:mm a")
            strScheduleDateTime = "\(strTimeNew) On \(strDateNew)"
            strReScheduleDate = strDateNew
            strReScheduleTime = strTimeNew
            strWorkOrderId = tempData.workOrderId!
            
        }
        getOpportunityData(strLeadNumber: tempData.leadNumber ?? "")

    }
    //Nilind
    func getOpportunityData(strLeadNumber : String)
    {
        //showLoader()
        servicePresenter.apiCallForServiceList(strLeadNumber)
    }
    
    @objc private func fetchServices() {
        // FTIndicator.dismissProgress()
        guard let serviceDetails = AppUserDefaults.shared.masterDetails else { return }
        services = serviceDetails.categories.flatMap { $0.services}.filter{$0.isActive == true && ($0.isPlusService == false || $0.isPlusService == nil) && $0.isDoortodoor == true}
        plusServices = serviceDetails.categories.flatMap { $0.services}.filter{$0.isActive == true && $0.isPlusService == true}
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func getPropertyDetailFromLeadDetailLocal(_ leadDetail: LeadDetail) -> PropertyInfo {
       
       var propertyInfo = PropertyInfo()
       
       propertyInfo.name = "\(leadDetail.firstName ?? "") \(leadDetail.middleName ?? "") \(leadDetail.lastName ?? "")".trimmed
       propertyInfo.middleName = leadDetail.middleName
       
       propertyInfo.firstName = leadDetail.firstName
       
       propertyInfo.lastName =  leadDetail.lastName
       
       //if(leadDetail.primaryPhone != nil && leadDetail.primaryPhone == "")
       if (leadDetail.primaryPhone != nil && leadDetail.primaryPhone != "")
       {
           propertyInfo.phone =  leadDetail.primaryPhone
       }else{
           propertyInfo.phone =  leadDetail.cellNo

       }
   
       propertyInfo.email =  leadDetail.primaryEmail
       
       
       //if let serviceAddress = opportunity.serviceAddress {

       let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(leadDetail.serviceState ?? "")")
       
       if dictStateDataTemp.count > 0 {
                           
           propertyInfo.state?.stateID = Int("\(dictStateDataTemp.value(forKey: "StateId")!)")!
           propertyInfo.state?.name = "\(dictStateDataTemp.value(forKey: "Name")!)"
           propertyInfo.state?.stateShortName = "\(dictStateDataTemp.value(forKey: "StateShortName")!)"
           propertyInfo.state?.stateSysName = "\(dictStateDataTemp.value(forKey: "StateSysName")!)"

           let address = "\(leadDetail.servicesAddress1 ?? ""), \(leadDetail.serviceCity ?? ""), \(dictStateDataTemp.value(forKey: "Name")!), \(leadDetail.serviceZipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")

           propertyInfo.address =  address
                       
       }

           propertyInfo.zipCode =  leadDetail.serviceZipcode
       

           propertyInfo.city =  leadDetail.serviceCity

           if let area = leadDetail.area , !area.isEmpty {

               propertyInfo.lotSizeSqFt =  area

               propertyInfo.area =  leadDetail.area

           }

           if let noofStory = leadDetail.noOfStory , !noofStory.isEmpty {

               propertyInfo.noOfFloor =  leadDetail.noOfStory

           } else {

           }

           if propertyInfo.type != nil {

           } else {

               var typeInfo = "\(leadDetail.flowType ?? "")"

               if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty, let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {

                   propertyInfo.bedRoom =  noofBedroom

                   propertyInfo.bathRoom =  noofBathroom

               } else {

                   if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty {

                       typeInfo.append(" \(noofBedroom) Bed")

                       propertyInfo.bedRoom =  noofBedroom

                       

                   }
                   

                   if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {

                       typeInfo.append(" \(noofBathroom) Bath.")

                       propertyInfo.bathRoom =  noofBathroom

                   }

               }

               propertyInfo.type = typeInfo.trimmed

           }

       if let urlString = leadDetail.profileImage , !urlString.isEmpty  {
           
           if  let urlString = leadDetail.profileImage!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {

               propertyInfo.imageUrl = leadDetail.profileImage!

               }
           
       }
       
       //}
       
       
       
       if let area = leadDetail.area , !area.isEmpty  {
           
           propertyInfo.area = area
           
           propertyInfo.lotSizeSqFt = area
           
       }
       
       if let noofStory = leadDetail.noOfStory , !noofStory.isEmpty {
           
           propertyInfo.noOfFloor = noofStory
           
       }
       
       var type = "\(leadDetail.flowType ?? "")"
       
       propertyInfo.flowType = leadDetail.flowType ?? "Residential"
       
       if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
           
           type.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
           
           propertyInfo.bedRoom =  noofBedroom
           
           propertyInfo.bathRoom =  noofBathroom
           
       } else {
           
           if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty {
               
               type.append(" \(noofBedroom) Bed")
               
               propertyInfo.bedRoom =  noofBedroom
               
           }
           
           if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty {
               
               type.append(" \(noofBathroom) Bath.")
               
               propertyInfo.bathRoom =  noofBathroom
               
           }
           
       }
       
       propertyInfo.type = type.trimmed
       
       propertyInfo.latitude = leadDetail.latitude
       
       propertyInfo.longitude = leadDetail.longitude
       
       // propertyInfo.imageUrl = leadDetail.addressImagePath
       
       return propertyInfo
       
       
       
   }
    
    
}
extension ScheduleViewController: UITableViewDelegate {
    
}
extension ScheduleViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ScheduleTabs.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ScheduleTabsCollectionViewCell.self), for: indexPath) as! ScheduleTabsCollectionViewCell
        cell.lblTitle.text  =  ScheduleTabs(rawValue: indexPath.row)?.title
        if selectedTab ==  ScheduleTabs(rawValue: indexPath.row) {
            cell.imgRadio.image = #imageLiteral(resourceName: "redio_button_2.png")
        } else {
            cell.imgRadio.image = UIImage(named: "redio_button_1.png")
        }
        return cell
    }
}
extension ScheduleViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTab = ScheduleTabs(rawValue: indexPath.row) ?? .all
        filterData()
        collectionView.reloadData()
        tableView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = ScheduleTabs(rawValue: indexPath.row)?.title ?? ""
        let cellWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 14)) + 15
        return CGSize(width: (cellWidth + 20), height: collectionView.frame.height)
    }
}
extension ScheduleViewController: ViewScheduleView {
    func gotList(_ output: [Schedules]) {
        
        self.allSchedules = output
        
    }
}
extension ScheduleViewController : ServiceView {
    
    private func showAlertForBranchChange(strAppoinntmentBranch : String, strEmpLoginBranchSysName : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "ScheduleView"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func gotSeriveList(_ output: EntityDetail) {
        if let soldServiceStandardDetail = output.soldServiceStandardDetail, !soldServiceStandardDetail.isEmpty {
            self.soldServices = soldServiceStandardDetail
        } else {
            fetchServices()
        }
        entityDetail = output
        
        guard !soldServices.isEmpty else {
            return showAlert(message: "you need to have atleast one selected service to proceed")
        }
        
        // Check if Logged In Branch is different
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"

        var proceedFurther = true
        
        if entityDetail?.leadDetail != nil {
            
            if entityDetail?.leadDetail?.branchSysName != strEmpLoginBranchSysName {
                
                proceedFurther = false
                // Show Alert of Branch
                                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(entityDetail?.leadDetail?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)
                
            }
            
        }
        
        if proceedFurther {
            
            let storyboard = UIStoryboard(name: "Agreement", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "ServiceSetupViewController") as? ServiceSetupViewController else {
                return
            }
            destination.entityDetail = entityDetail
            destination.services = soldServices
            destination.fromReschedule = isReschedule//true
            propertyDetails = PropertyDetails()
            propertyDetails?.account = nil
            propertyDetails?.lead = nil
            propertyDetails?.propertyType = .opportunity
            let propertInfo = getPropertyDetailFromLeadDetailLocal((entityDetail?.leadDetail)!)
            propertyDetails?.propertyInfo = propertInfo
            destination.propertyDetails = propertyDetails
            destination.strScheduleDateTime = strScheduleDateTime
            destination.strReScheduleTime = strReScheduleTime
            destination.strReScheduleDate = strReScheduleDate
            destination.strWorkOrderId = strWorkOrderId
            destination.aryTimeRange = self.aryTimeRange
            destination.timeRangeIdL = timeRangeIdSelected
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(destination, animated: true)
            
            nsud.setValue(true, forKey: "IsScheduleFromViewSchedule")
            nsud.synchronize()
            
        }
        
    }
    
}


// MARK: UISearchBar delegate

extension ScheduleViewController : UISearchBarDelegate{


func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchAutocomplete(searchText: searchText)
}

func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
    self.view.endEditing(true)
    
    if(searchBar.text?.count == 0)
    {
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter text to be searched.", viewcontrol: self)
    }
    
}

func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    self.view.endEditing(true)
    filterData()
}
    func searchAutocomplete(searchText: String) -> Void{
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.searchBar.resignFirstResponder()
            }
            filterData()
        }
        
        else{
            var schedulesTemp: [Schedules] = []
            switch selectedTab {
            case .all:
                let aryTemp = allSchedules.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                    
                }
                schedules = aryTemp

            case .schedule:
                schedulesTemp = allSchedules.filter({$0.scheduleStatus == .schedule})
                let aryTemp = schedulesTemp.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                }
                schedules = aryTemp
            case .complete:
                schedulesTemp = allSchedules.filter({$0.scheduleStatus == .complete})
                let aryTemp = schedulesTemp.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                }
                schedules = aryTemp
            case .incomplete:
                schedulesTemp = allSchedules.filter({$0.scheduleStatus == .inComplete})
                let aryTemp = schedulesTemp.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                }
                schedules = aryTemp
            case .cancelled:
                schedulesTemp = allSchedules.filter({$0.scheduleStatus == .cancelled})
                let aryTemp = schedulesTemp.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                }
                schedules = aryTemp
            case .void:
                schedulesTemp = allSchedules.filter({$0.scheduleStatus == .void})
                let aryTemp = schedulesTemp.filter { (task) -> Bool in
                    return (("\(task.firstName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.lastName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.leadNumber ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine1 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceAddressLine2 ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceCityName ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.serviceZipCode ?? "")".lowercased().contains("\(searchText)".lowercased())) || ("\(task.customerName ?? "")".lowercased().contains("\(searchText)".lowercased())))
                }
                schedules = aryTemp
            }


        }
        
    }
}
// MARK: - -----------------------------------SpeechRecognitionDelegate -----------------------------------
extension ScheduleViewController : SpeechRecognitionDelegate{
    func getDataFromSpeechRecognition(str: String) {
        print(str)
        if #available(iOS 13.0, *) {
            self.searchBar.searchTextField.text = str
        } else {
            self.searchBar.text = str
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.searchAutocomplete(searchText: str)
        }
    }
}
