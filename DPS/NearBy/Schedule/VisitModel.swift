//
//  VisitModel.swift
//  DPS
//
//  Created by Vivek Patel on 14/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
struct Schedules : Codable {
    var clientCreatedDate: String?
    var customerName: String?
    var leadNumber: String?
    var scheduleStatus: ScheduleStatus?
    var scheduleDateTime: String?
    var serviceAddrSubType: String?
    var serviceAddressLine1: String?
    var serviceAddressLine2: String?
    var serviceCityName: String?
    var serviceCountryId: Int?
    var serviceGateCode: String?
    var serviceMapCode: String?
    var serviceStateId: Int?
    var serviceZipCode: String?
    
    var accountId            : Int?
    var billToLocationId     : String?
    var billingFirstName     : String?
    var billingLastName      : String?
    var serviceAddressId     : Int?
    var billingAddressId     : Int?
    var billingAddressLine1  : String?
    var billingAddressLine2  : String?
    var billingCityName      : String?
    var billingStateId       : Int?
    var billingZipcode       : String?
    var billingCountryId     : Int?

    var crmContactId         : Int?
    var firstName            : String?
    var lastName             : String?
    var primaryEmail         : String?
    var primaryPhone         : String?

    var specialInstructions  : String?
    var creditCardType       : String?
    var cardAccountNumber    : String?
    var workOrderId          : Int?
    var serviceDate          : String?
    var scheduleTime         : String?
    var saLeadId          : Int?
    var timeRangeId          : Int?

    enum CodingKeys: String, CodingKey {
        
        case accountId           = "AccountId"
        case billToLocationId    = "BillToLocationId"
        case billingFirstName    = "BillingFirstName"
        case billingLastName     = "BillingLastName"
        case serviceAddressId    = "ServiceAddressId"
        case billingAddressId    = "BillingAddressId"
        case billingAddressLine1 = "BillingAddressLine1"
        case billingAddressLine2 = "BillingAddressLine2"
        case billingCityName     = "BillingCityName"
        case billingStateId      = "BillingStateId"
        case billingZipcode      = "BillingZipcode"
        case billingCountryId    = "BillingCountryId"
        case crmContactId        = "CrmContactId"
        case firstName           = "FirstName"
        case lastName            = "LastName"
        case primaryEmail        = "PrimaryEmail"
        case primaryPhone        = "PrimaryPhone"
        case specialInstructions = "SpecialInstructions"
        case creditCardType      = "CreditCardType"
        case cardAccountNumber   = "CardAccountNumber"
        case workOrderId         = "WorkOrderId"
        case serviceDate         = "ServiceDate"
        case scheduleTime        = "ScheduleTime"
        case saLeadId            = "SaLeadId"
        case timeRangeId            = "RangeofTimeId"

        case clientCreatedDate = "ClientCreatedDate"
        case customerName  = "CustomerName"
        case leadNumber = "LeadNumber"
        case scheduleStatus = "ScheduleStatus"
        case scheduleDateTime = "ScheduleDateTime"
        case serviceAddrSubType = "ServiceAddrSubType"
        case serviceAddressLine1 =  "ServiceAddressLine1"
        case serviceAddressLine2 = "ServiceAddressLine2"
        case serviceCityName = "ServiceCityName"
        case serviceCountryId = "ServiceCountryId"
        case serviceGateCode = "ServiceGateCode"
        case serviceMapCode = "ServiceMapCode"
        case serviceStateId = "ServiceStateId"
        case serviceZipCode = "ServiceZipCode"
        
    }
}
enum ScheduleStatus: String, Codable {
    case schedule = "Scheduled"
    case complete = "Completed"
    case inComplete = "Incomplete"
    case void = "Void"
    case cancelled = "Cancelled"
}
