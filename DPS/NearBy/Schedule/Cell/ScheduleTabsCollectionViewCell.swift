//
//  ScheduleTabsCollectionViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ScheduleTabsCollectionViewCell: UICollectionViewCell {
    //MARK: - Outlets
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
