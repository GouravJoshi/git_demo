//
//  ViewScheduleUITableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//


import UIKit

final class ViewScheduleUITableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTimeRange: UILabel!

    @IBOutlet weak var btnAddCard: UIButton!
    @IBOutlet weak var btnReSchedule: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblSavedCard: UILabel!

    
    @IBOutlet weak var heightbtnEdit: NSLayoutConstraint!
    @IBOutlet weak var heightbtnReSchedule: NSLayoutConstraint!
    @IBOutlet weak var heightbtnAddCard: NSLayoutConstraint!

    
    var schedule: Schedules? {
        didSet {
            btnReSchedule.isHidden = true // false
            heightbtnReSchedule.constant = 0.0 //95
            

            btnEdit.isHidden = false
            heightbtnEdit.constant = 40.0

            lblSavedCard.isHidden = true


            guard let schedule = schedule else { return }
            
            lblName.text = schedule.customerName
           //-----For Address-----
            let dictAddress = NSMutableDictionary()

            let address1 = "\(schedule.serviceAddressLine1 ?? "")"
            let address2 = "\(schedule.serviceAddressLine2 ?? "")"
            let city = "\(schedule.serviceCityName ?? "")"
            let StateId = "\(schedule.serviceStateId ?? 0)"
            let State = Global().strStatName(fromID: "\(StateId)")
            let ZipCode = "\(schedule.serviceZipCode ?? "")"
            
            dictAddress.setValue(address1, forKey: "Address1")
            dictAddress.setValue(address2, forKey: "Address2")
            dictAddress.setValue(city, forKey: "CityName")
            dictAddress.setValue(State, forKey: "State")
            dictAddress.setValue("\(StateId)", forKey: "StateId")
            dictAddress.setValue(ZipCode, forKey: "ZipCode")
            self.lblAddress.text = Global().strCombinedAddress(dictAddress as? [AnyHashable : Any])
      
  
            
            //-----End-----
         
            if let dateString = schedule.clientCreatedDate , let date = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy") {
                lblDays.text = date
            }
            
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            
            if (dictLoginData.value(forKeyPath: "Company.CompanyConfig.IsPestPacIntegration") as! Bool) == true
            {
                if (dictLoginData.value(forKeyPath: "Company.PestPacIntegrationConfig.IsPaymentIntegration") as! Bool) == true
                {
                    
                    btnAddCard.isHidden = false
                    heightbtnAddCard.constant = 70.0

                }
                else
                {
                    // Payment Integration is Off
                    btnAddCard.isHidden = true
                    heightbtnAddCard.constant = 0.0

                }
            }
            else
            {
                // Payment Integration is Off
                btnAddCard.isHidden = true
                heightbtnAddCard.constant = 0.0

            }
            
            let billToId = "\(schedule.billToLocationId ?? "")"
            
            if billToId.count == 0 {
                
                btnAddCard.isHidden = true
                heightbtnAddCard.constant = 0.0

            }
            
            let cardAccountNumber = "\(schedule.cardAccountNumber ?? "")"

            if cardAccountNumber.count == 0 {
                
                lblSavedCard.isHidden = true

            }else{
                lblSavedCard.text = "\(schedule.creditCardType ?? "") " + String("\(cardAccountNumber)".suffix(4))
                lblSavedCard.isHidden = false

            }
                        
            // For Local Check.
            if(nsud.value(forKey: billToId) != nil){
                
                if nsud.value(forKey: billToId) is NSDictionary {
                    
                    lblSavedCard.isHidden = false

                    let dictCreditCardInfo = nsud.value(forKey: billToId) as! NSDictionary
                    
                    let accountNumber = String("\(dictCreditCardInfo.value(forKey: "accountNumber") ?? "")".suffix(4))
                    lblSavedCard.text = "\(dictCreditCardInfo.value(forKey: "creditCardType") ?? "") " + accountNumber
                }
            }
            
            // check for WorkOrder Id
            
            let woID = "\(schedule.workOrderId ?? 0)"
            
            if woID.count == 0 || woID == "0" {
                
                //btnEdit.isHidden = true
                //heightbtnEdit.constant = 0.0
                
                //Nilind 19 May
                //heightbtnReSchedule.constant = 0.0
               // btnReSchedule.isHidden = true
                
            }
            
            let attrs = [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.underlineStyle : 1,
                NSAttributedString.Key.foregroundColor : UIColor.lightGray
            ] as [NSAttributedString.Key : Any]
            
            switch schedule.scheduleStatus {
            case .schedule:
                lblStatus.text = schedule.scheduleDateTime
                lblStatus.textColor = .appThemeColor
                
                //Nilind
                btnReSchedule.isHidden = false
                heightbtnReSchedule.constant = 95.0
                btnReSchedule.setAttributedTitle(NSMutableAttributedString(string:"Re-Schedule", attributes:attrs), for: .normal)


            case .complete:
                lblStatus.text = "Done"
                lblStatus.textColor = .appThemeColor
                btnAddCard.isHidden = true
                btnEdit.isHidden = true
                heightbtnEdit.constant = 0.0
                
                btnReSchedule.setAttributedTitle(NSMutableAttributedString(string:"Schedule", attributes:attrs), for: .normal)
                
                //In Done case Schedule and Reschedule will not show
            
                btnReSchedule.isHidden = true
                heightbtnReSchedule.constant = 0


            case .inComplete:
                lblStatus.text = "Incompleted"
                lblStatus.textColor = .appYellow
                
                btnReSchedule.setAttributedTitle(NSMutableAttributedString(string:"Schedule", attributes:attrs), for: .normal)
                
                btnReSchedule.isHidden = false
                heightbtnReSchedule.constant = 95.0
                
            case .cancelled:
                lblStatus.text = "Cancelled"
                lblStatus.textColor = .appThemeColor
                
                btnReSchedule.setAttributedTitle(NSMutableAttributedString(string:"Schedule", attributes:attrs), for: .normal)
                
                btnReSchedule.isHidden = true
                heightbtnReSchedule.constant = 0
                
            case .void:
                lblStatus.text = "Void"
                lblStatus.textColor = .appThemeColor
                
                btnReSchedule.setAttributedTitle(NSMutableAttributedString(string:"Schedule", attributes:attrs), for: .normal)
                
                btnReSchedule.isHidden = true
                heightbtnReSchedule.constant = 0


            case .none:
                break
            }
        }
        
    }
    // MARK: - View Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
