//
//  ScheduleService.swift
//  DPS
//
//  Created by Vivek Patel on 14/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class VisitScheduleService {
    
    func getScheduleList(_ callBack:@escaping (_ object: [Schedules]?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let url = URL.Schedule.viewSchedule.replacingOccurrences(of: "(employeeId)", with: employeeId).replacingOccurrences(of: "(companyKey)", with: companyKey)
        AF.request(url, method: .get) .responseDecodable(of: [Schedules].self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode([Schedules].self, from: data)
                    print(object)
                    
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
