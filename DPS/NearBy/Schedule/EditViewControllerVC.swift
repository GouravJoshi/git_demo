//
//  EditViewControllerVC.swift
//  DPS
//
//  Created by NavinPatidar on 4/12/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

class EditViewControllerVC: UIViewController {
    //MARK:
    //MARK: IBOutlet
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgLastName: UIImageView!
    @IBOutlet weak var imgFirstName: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var btnEdit: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnEdit, image: "Group 779", color: .black)
        }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!

    //MARK:
    //MARK: Other Variable
    var schedules:Schedules?
    
    // MARK: - ----------- Google Address Code ----------------
    
    
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    var txtAddressMaxY = CGFloat()
    var imgPoweredBy = UIImageView()
    var strStateId = ""
    var strCityName = ""
    var strAddressLine1 = ""
    var strZipCode = ""
    var strCustomerAddressId = ""
    var loader = UIAlertController()

    
    var arrAddress = NSMutableArray() , arrOfAccountAddress = NSMutableArray()
    var dictLoginData = NSDictionary()

    
    private var places = [Place]() {
        didSet { tableView.reloadData() }
    }
    var dictAddress = NSMutableDictionary()

    //MARK:
    //MARK: LifeCycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()

        self.txtAddress.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
        tableView.layer.cornerRadius = 4.0
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        // Set values to edit
        
        txtPhone.text = "\(schedules?.primaryPhone ?? "")"
        txtPhone.text = formattedNumber(number: txtPhone.text!)
        txtEmail.text = "\(schedules?.primaryEmail ?? "")"
        txtFirstName.text = "\(schedules?.firstName ?? "")"
        txtLastName.text = "\(schedules?.lastName ?? "")"
        txtAddress.text = ""// yaha pura bana lena yrr
        txtViewDescription.text = "\(schedules?.specialInstructions ?? "")"

        let address1 = "\(schedules?.serviceAddressLine1 ?? "")"
        let address2 = "\(schedules?.serviceAddressLine2 ?? "")"
        let city = "\(schedules?.serviceCityName ?? "")"
        let StateId = "\(schedules?.serviceStateId ?? 0)"
        let State = Global().strStatName(fromID: "\(StateId)")
        let ZipCode = "\(schedules?.serviceZipCode ?? "")"
        
        dictAddress.setValue(address1, forKey: "Address1")
        dictAddress.setValue(address2, forKey: "Address2")
        dictAddress.setValue(city, forKey: "CityName")
        dictAddress.setValue(State, forKey: "State")
        dictAddress.setValue("\(StateId)", forKey: "StateId")
        dictAddress.setValue(ZipCode, forKey: "ZipCode")
        self.txtAddress.text = Global().strCombinedAddress(dictAddress as? [AnyHashable : Any])

        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    //MARK:
    //MARK: IBAction
    
    @IBAction func actionSave(_ sender: Any) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        if(Validation()){
            if(isInternetAvailable()){
                self.CallAPIEdit()

            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionEditAdress(_ sender: Any) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.dictAddress = self.dictAddress
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
    }
    @IBAction func actionClearAddress(_ sender: Any) {
        txtAddress.text = ""
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
    }
    
    @IBAction func actionDropDown(_ sender: Any) {
        self.view.endEditing(true)
        self.imgPoweredBy.removeFromSuperview()
        self.tableView.removeFromSuperview()
        
        if arrAddress.count > 0  {
            self.openTableViewPopUp(tag: 63, ary: (self.arrAddress as NSArray).mutableCopy() as! NSMutableArray , aryselectedItem: NSMutableArray())
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: NoDataAvailableCustomerAddress, viewcontrol: self)
        }
        

    }
    
    
    //MARK:
    //MARK: Other Function
    
    private func setUI() {
        txtFirstName.setLeftPaddingPoints(30)
        txtLastName.setLeftPaddingPoints(30)
        txtEmail.setLeftPaddingPoints(30)
        txtPhone.setLeftPaddingPoints(30)
        AppUtility.imageColor(image: imgFirstName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgLastName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgEmail, color: .appDarkGreen)
        AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
    }

    func Validation() -> Bool {
        if(txtFirstName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "First name is required!", viewcontrol: self)
            return false
        }else if(txtEmail.text?.count != 0){
            
            let isValid = Global().checkIfCommaSepartedEmailsAreValid(txtEmail.text!)

            //if !(validateEmail(email: txtEmail.text!)){
            if !(isValid){

                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_EmailAddress, viewcontrol: self)
                return false
            }
            
        }
        else if (txtPhone.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Phone number is required!", viewcontrol: self)
            return false
        }
        else if (txtPhone.text!.count < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Phone number is invalid!", viewcontrol: self)
            return false
        }
        else if (txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Address is invalid!", viewcontrol: self)
            return false
        }
        if (txtAddress.text!.count > 0) {
            
            let dictOfEnteredAddress = formatEnteredAddress(value: txtAddress.text!)
            
            if dictOfEnteredAddress.count == 0 {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid address.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                
                return false
                
            } else {
                
                //test
                strCityName = "\(dictOfEnteredAddress.value(forKey: "CityName")!)"
                
                strZipCode = "\(dictOfEnteredAddress.value(forKey: "ZipCode")!)"
                
                let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(dictOfEnteredAddress.value(forKey: "StateName")!)")
                
                if dictStateDataTemp.count > 0 {
                    
                    strStateId = "\(dictStateDataTemp.value(forKey: "StateId")!)"
                    
                }else{
                    
                    strStateId = ""
                    
                }
                
                strAddressLine1 = "\(dictOfEnteredAddress.value(forKey: "AddresLine1")!)"
                
                if strAddressLine1.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address 1.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strCityName.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid city.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strStateId.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid state name.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count == 0 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                if strZipCode.count != 5 {
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter valid zipCode.\n\n Address Format (address 1, city, state, zipcode).", viewcontrol: self)
                    
                    return false
                    
                }
                
            }
            
            
        }

        return true
    }
    func openTableViewPopUp(tag:Int, ary:NSMutableArray, aryselectedItem: NSMutableArray)
    {
        let storyboardIpad = UIStoryboard.init(name: "CRMiPad", bundle: nil)
        
        let vc: PopUpView = storyboardIpad.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = "Select"
        vc.strTag = tag
        vc.arySelectedItems = aryselectedItem
        if ary.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handelDataSelectionTable = self
            vc.aryTBL = addSelectInArray(strName: "AddLeadProspect", array: ary)
            self.present(vc, animated: false, completion: {})
        }else{
            
            Global().displayAlertController(Alert, NoDataAvailableee, self)
        }
    }
    func scrollToTop(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            let topOffset = CGPoint(x: 0, y: 200)
            scrollView.setContentOffset(topOffset, animated: animated)
        }
     
       }
    func scrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            let topOffset = CGPoint(x: 0, y: 0)
            scrollView.setContentOffset(topOffset, animated: animated)
            self.imgPoweredBy.removeFromSuperview()
            self.tableView.removeFromSuperview()
         }
       }
    func CallAPIEdit() {
        let dictSendData = NSMutableDictionary()
        dictSendData.setValue("\(schedules?.leadNumber ?? "")", forKey: "LeadNo")
        dictSendData.setValue("\(String(describing: Global().getCompanyKey()!))", forKey: "CompanyKey")
        dictSendData.setValue("\(txtFirstName.text!)", forKey: "FirstName")
        dictSendData.setValue("\(txtLastName.text!)", forKey: "LastName")
        dictSendData.setValue("\(txtEmail.text!)", forKey: "PrimaryEmail")
        dictSendData.setValue("\(txtPhone.text!)", forKey: "CellPhone1")
        dictSendData.setValue("\(schedules?.crmContactId ?? 0)", forKey: "CrmContactId")
        dictSendData.setValue("\(schedules?.serviceAddressId ?? 0)", forKey: "CustomerAddressId")
        dictSendData.setValue("\(dictAddress.value(forKey:"Address1")!)", forKey: "Address1")
        dictSendData.setValue("\(dictAddress.value(forKey:"CityName")!)", forKey: "CityName")
        dictSendData.setValue("\(dictAddress.value(forKey:"StateId")!)", forKey: "StateId")
        dictSendData.setValue("1", forKey: "CountryId")
        dictSendData.setValue("\(dictAddress.value(forKey:"ZipCode")!)", forKey: "Zipcode")
//        dictSendData.setValue("", forKey: "Latitude")
//        dictSendData.setValue("", forKey: "Longitude")
        dictSendData.setValue("\(txtViewDescription.text!)", forKey: "CustomerNotes")
        dictSendData.setValue("\(schedules?.workOrderId ?? 0)", forKey: "WorkOrderId")
        self.EditContactAPI(dictToSend: dictSendData, strType: "")
    }
    func EditContactAPI(dictToSend : NSDictionary , strType : String) {
        
        if(isInternetAvailable()){
            
            var jsonString = String()
            
            if(JSONSerialization.isValidJSONObject(dictToSend) == true)
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: dictToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                print(jsonString)
                
            }
            
            let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlEditCustomerAddressNotes_DoorToDoor
            
            loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebService.postDataWithHeadersToServer(dictJson: dictToSend, url: strURL, strType: "bool"){ [self] (responce, status) in
                loader.dismiss(animated: false) {
                    print(responce)
                    if(status)
                    {
                        if responce.value(forKey: "Success") as! NSString == "true"
                        {
                            
                            schedules?.firstName = "\(txtFirstName.text!)"
                            schedules?.lastName = "\(txtLastName.text!)"
                            schedules?.primaryEmail = "\(txtEmail.text!)"
                            schedules?.primaryPhone = "\(txtPhone.text!)"
                            schedules?.serviceAddressLine1 = "\(dictAddress.value(forKey:"Address1")!)"
                            schedules?.serviceCityName = "\(dictAddress.value(forKey:"CityName")!)"
                            schedules?.serviceStateId = Int("\(dictAddress.value(forKey:"StateId")!)")
                            schedules?.serviceZipCode = "\(dictAddress.value(forKey:"ZipCode")!)"
                            schedules?.specialInstructions = "\(txtViewDescription.text!)"
                            let alert = UIAlertController(title: "Alert", message: "Updated successfully.", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                

                                self.navigationController?.popViewController(animated: false)
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadonEditViewSchedule"), object: nil, userInfo: NSDictionary() as? [AnyHashable : Any])

                                
                            }))
                            self.present(alert, animated: true, completion: nil)

                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                    
                }
             
            }
            
        }else{
            
            self.loader.dismiss(animated: false) {
                
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                
            }
            
        }
        
    }
    
}
// MARK: - UITextFieldDelegate
extension EditViewControllerVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == txtAddress){
            
            self.scrollView.isScrollEnabled = false
            scrollToTop(animated: false)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == txtAddress){
            scrollToBottom(animated: false)
            self.scrollView.isScrollEnabled = true
        }
        
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
            if(isValidd){
                var strTextt = txtPhone.text!
                strTextt = String(strTextt.dropLast())
                txtPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
            }
            return isValidd
            
        }
        
        else if(textField == txtAddress){
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    places = [];
                    self.imgPoweredBy.removeFromSuperview()
                    
                    self.tableView.removeFromSuperview()
                    return true
                    
                    //   print("Backspace was pressed")
                }
            }
            var txtAfterUpdate:NSString = txtAddress.text! as NSString
            
            txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
            
            if(txtAfterUpdate == ""){
                places = [];
                self.imgPoweredBy.removeFromSuperview()
                
                self.tableView.removeFromSuperview()
                return true
            }
            
            if((txtAfterUpdate as String).count % 3 == 0){
                let parameters = getParameters(for: txtAfterUpdate as String)
                self.getPlaces(with: parameters) {
                    self.places = $0
                }
            }
            return true

            
        }
        else{
            return txtFieldValidation(textField: textField, string: string, returnOnly: "", limitValue: 500)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if(textField == txtAddress){
            scrollToBottom(animated: false)
            self.scrollView.isScrollEnabled = true
        }

        return true
    }
    private func getParameters(for text: String) -> [String: String] {
        var params = [
            "input": text,
            "types": placeType.rawValue,
            "key": apiKey
        ]
        
        if CLLocationCoordinate2DIsValid(coordinate) {
            params["location"] = "\(coordinate.latitude),\(coordinate.longitude)"
            
            if radius > 0 {
                params["radius"] = "\(radius)"
            }
            
            if strictBounds {
                params["strictbounds"] = "true"
            }
        }
        
        return params
    }
}
// MARK: - AddNewAddressiPhoneDelegate

extension  EditViewControllerVC : AddNewAddressiPhoneDelegate {
    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        print(dictData)
        dictAddress = NSMutableDictionary()
        dictAddress = dictData
        self.txtAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        //self.strCustomerAddressId = ""
    }
}

// MARK: -  ---------------- UITableViewDelegate ----------------
// MARK: -

extension EditViewControllerVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "addressCell", for: indexPath as IndexPath) as! addressCell
        let place = places[indexPath.row]
        cell.lbl_Address?.text = place.mainAddress + "\n" + place.secondaryAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = places[indexPath.row]
        
        self.getPlaceDetails(id: place.id, apiKey: apiKey) { [unowned self] in
            guard let value = $0 else { return }
            //self.txtFldAddress.text = value.formattedAddress
            self.txtAddress.text = addressFormattedByGoogle(value: value)
            self.strCustomerAddressId = ""
            
            self.places = [];
        }
        self.imgPoweredBy.removeFromSuperview()
        
        self.tableView.removeFromSuperview()
        self.view.endEditing(true)
        
    }
    
}
extension EditViewControllerVC {
    
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    
    func getPlaces(with parameters: [String: String], completion: @escaping ([Place]) -> Void) {
        var parameters = parameters
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/autocomplete/json",
            params: parameters,
            completion: {
                guard let predictions = $0["predictions"] as? [[String: Any]] else { return }
                completion(predictions.map { Place(prediction: $0)
                    
                })
                if (self.places.count == 0){
                    self.imgPoweredBy.removeFromSuperview()
                    self.tableView.removeFromSuperview()
                }
                else
                {
                   
                    for item in self.view.subviews {
                        if(item is UITableView){
                            item.removeFromSuperview()
                        }
                    }
                    self.imgPoweredBy.removeFromSuperview()
                    
                    // self.tableView.frame = CGRect(x: self.txtFldAddress.frame.origin.x, y: self.txtFldAddress.frame.maxY, width: self.txtFldAddress.frame.width, height: DeviceType.IS_IPHONE_6 ? 200 : 250)
                    
                    if DeviceType.IS_IPHONE_6_OR_LESS {
                        
                        self.tableView.frame = CGRect(x: self.txtAddress.frame.origin.x, y: self.txtAddress.frame.maxY, width: self.txtPhone.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 170)
                        
                        
                    } else {
                        
                        self.tableView.frame = CGRect(x: self.txtAddress.frame.origin.x, y: self.txtAddress.frame.maxY, width: self.txtPhone.frame.width, height: DeviceType.IS_IPHONE_6 ? 130 : 250)
                        
                    }
                    
                    self.imgPoweredBy.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.maxY, width: self.tableView.frame.width, height: 25)
                    
                    self.scrollView.addSubview(self.tableView)
                    self.scrollView.addSubview(self.imgPoweredBy)
                }
            }
        )
        
        
    }
    
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:])) }
        )
    }
    
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
}
// MARK: -
// MARK: - Selection CustomTableView
extension EditViewControllerVC: CustomTableView
{
    func getDataOnSelection(dictData: NSDictionary, tag: Int) {
        
        if(tag == 63)
        {
            if(dictData.count == 0){
                txtAddress.text = ""
                self.txtAddress.isUserInteractionEnabled = true
                strCustomerAddressId = ""
            }else{
                
                txtAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                self.txtAddress.isUserInteractionEnabled = false
                let arrTemp = dictData.allKeys as NSArray
                
                if arrTemp.contains("CustomerAddressId") {
                    
                    strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId")!)"
                    
                }else{
                    
                    strCustomerAddressId = ""
                    
                }
                
            }
        }
}
}



//MARK: API Calling
extension EditViewControllerVC {
    func callApiToGetAddressViaAccountId(strAccountIdLocal : String , strType : String) {
        self.txtAddress.isUserInteractionEnabled = true
        let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlGetServiceAddressesByAccountId + strAccountIdLocal
        WebService.getRequestCRM(dictJson: NSDictionary(), url: strURL , responseStringComing: "") { (Response, Status) in
            self.loader.dismiss(animated: false) {
                if(Status)
                {
                    let arrKeys = Response.allKeys as NSArray
                    if arrKeys.contains("data") {
                        let dictData = Global().convertNullArray(Response as? [AnyHashable : Any])! as NSDictionary
                        self.arrOfAccountAddress = (dictData.value(forKey: "data") as! NSArray).mutableCopy()as! NSMutableArray
                        //self.arrOfAccountAddress = Response.value(forKey: "data") as! NSArray
                        if self.arrOfAccountAddress.count > 0 {
                            for k in 0 ..< self.arrOfAccountAddress.count {
                                let dictData = self.arrOfAccountAddress[k] as! NSDictionary
                                if (dictData.value(forKey: "IsPrimary") as! Bool) == true {
                                    if self.txtAddress.text?.count != 0 {
                                        let alertCOntroller = UIAlertController(title: Info, message: "Address already exist. Do you want to replace it?", preferredStyle: .alert)
                                        let alertAction = UIAlertAction(title: "Yes-Replace", style: .default, handler: { (action) in
                                            self.txtAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                            self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                            self.txtAddress.isUserInteractionEnabled = false
                                        })
                                        
                                        let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                                            
                                            
                                        })
                                        
                                        alertCOntroller.addAction(alertAction)
                                        alertCOntroller.addAction(alertActionCancel)
                                        
                                        self.present(alertCOntroller, animated: true, completion: nil)
                                        
                                    }else{
                                        
                                        self.txtAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
                                        self.strCustomerAddressId = "\(dictData.value(forKey: "CustomerAddressId") ?? "")"
                                        self.txtAddress.isUserInteractionEnabled = false
                                        
                                        
                                    }
                                    
                                    break
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }

}
