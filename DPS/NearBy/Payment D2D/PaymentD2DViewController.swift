//
//  PaymentD2DViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class PaymentD2DViewController: BaseViewController {
    // MARK: - Variable
    enum PaymentMode: Int, CaseIterable {
        case paymentMethod = 0
        case creditCard
        case cash
        case check
        case autoChargeCustomer
        case collectAtTime
        case invoice
        
        var title: String {
            switch self {
            case .paymentMethod:
                return "Payment Method"
            case .creditCard:
                return "Credit Card"
            case .cash:
                return "Cash"
            case .check:
                return "Check"
            case .autoChargeCustomer:
                return "Auto Charge Customer"
            case .collectAtTime:
                return "Collect at time of Scheduling"
            case .invoice:
                return "Invoice"
            }
        }
        var mode: String {
            switch self {
            case .paymentMethod:
                return "Payment Method"
            case .creditCard:
                return "CreditCard"
            case .cash:
                return "Cash"
            case .check:
                return "Check"
            case .autoChargeCustomer:
                return "AutoChargeCustomer"
            case .collectAtTime:
                return "CollectattimeofScheduling"
            case .invoice:
                return "Invoice"
            }
        }
    }
    var paymentMode: PaymentMode = .paymentMethod
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtPaymentMethod: UITextField!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
}
extension PaymentD2DViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtPaymentMethod {
            var rows:[String] = []
            var selectedRow = 0
            for (index,row) in PaymentMode.allCases.enumerated() {
                if index != 0 {
                    if paymentMode.title == row.title{
                        selectedRow = index - 1
                    }
                    rows.append(row.title)
                }
            }
            showStringPicker(sender: view, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                self.paymentMode = PaymentMode(rawValue: index + 1) ?? .paymentMethod
            }
            
            return false
            
        } else {
            return true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtAmount {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
                //  delegate?.amountEntered(updatedText)
            }
        }
        return true
    }
}
