//
//  UIImage+Compression.swift
//  DPS
//
//  Created by Vivek Patel on 09/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(expectedSizeInMb: Int? = nil, expectedSizeInKb: Int? = nil) -> UIImage? {
        var sizeInBytes = 0
        if let expectedSizeInMb = expectedSizeInMb {
         sizeInBytes = expectedSizeInMb * 1024 * 1024
        } else if let expectedSizeInKb = expectedSizeInKb {
             sizeInBytes = expectedSizeInKb * 1024
        }
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return UIImage(data: data)
            }
        }
        return nil
    }
}
