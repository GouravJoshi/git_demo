//
//  UIButton+Font.swift
//  DPS
//
//  Created by Vivek Patel on 15/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

extension UIButton {
    @IBInspectable var adjustFontSizeToWidth: Bool {
        get {
            return ((self.titleLabel?.adjustsFontSizeToFitWidth) != nil)
        }
        set {
            self.titleLabel?.numberOfLines = 1
            self.titleLabel?.adjustsFontSizeToFitWidth = newValue;
            self.titleLabel?.lineBreakMode = .byClipping;
            self.titleLabel?.baselineAdjustment = .alignCenters
        }
    }
}
