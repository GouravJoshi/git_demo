//
//  MKPlacemark+Extension.swift
//  DPS
//
//  Created by Vivek Patel on 16/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Contacts

extension MKPlacemark {
    var formattedAddress: String? {
        guard let postalAddress = postalAddress else { return nil }
        return CNPostalAddressFormatter.string(from: postalAddress, style: .mailingAddress).replacingOccurrences(of: "\n", with: " ")
    }
}
