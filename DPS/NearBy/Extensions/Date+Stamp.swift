//
//  Date+Stamp.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
