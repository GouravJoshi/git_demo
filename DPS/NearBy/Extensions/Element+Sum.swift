//
//  Element+Sum.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}
