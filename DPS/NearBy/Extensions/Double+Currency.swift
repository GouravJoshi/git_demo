//
//  Double+Currency.swift
//  DPS
//
//  Created by Vivek Patel on 24/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

extension Double {
    var currency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        return formatter.string(from: self as NSNumber) ?? "$0"
    }
    var percentage: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.multiplier     = 1.00
        return formatter.string(from: self as NSNumber) ?? "0%"
    }
    var currencyWithoutSign: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.maximumFractionDigits = 2
        return formatter.string(from: self as NSNumber) ?? "0"
    }
}
extension Double {
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

