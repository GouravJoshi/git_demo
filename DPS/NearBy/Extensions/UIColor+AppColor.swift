//
//  UIColor+AppColor.swift
//  DPS
//
//  Created by Vivek Patel on 28/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

extension UIColor{
    
    static var appThemeColor:UIColor {
        return UIColor(named: "AppThemeColor")!
    }
    static var appDarkGreen:UIColor {
        return UIColor(named: "AppDarkGreen")!
    }
    static var appYellow:UIColor {
        return UIColor(named: "AppYellow")!
    }
    static var appLightGray:UIColor {
        return UIColor(named: "AppLightGray")!
    }
    
}
