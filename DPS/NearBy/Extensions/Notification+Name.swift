//
//  Notification+Name.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let didUCreatedLead = Notification.Name("DidUCreatedLead")
    static let updatedGeoAreaDataD2D = Notification.Name("UpdatedGeoAreaDataD2D")
    static let refreshMapOnCancelD2D = Notification.Name("RefreshMapOnCancelD2D")
    static let refreshNewAreaD2D = Notification.Name("RefreshNewAreaD2D")

}
