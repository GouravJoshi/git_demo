//
//  HeaderTablsCollectionViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 07/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class HeaderTablsCollectionViewCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var lblSeprator: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
