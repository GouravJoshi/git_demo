//
//  NearByListViewController.swift
//  DPS
//
//  Created by Vivek Patel on 14/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit


final class NearByListViewController: BaseViewController {
    
    private enum EntityType: Int, CaseIterable {
        case all = 0
        case leads
        case opportunity
        case account
        var title: String{
            switch self {
            case .all:
                return "All Records"
            case .leads:
                return "All Leads"
            case .opportunity:
                return "All Opportunities"
            case .account:
                return "All Accounts"
            }
        }
    }
    // MARK: - Variables
    var leads: [Lead] = []
    var opportunities: [Opportunity] = []
    var accounts: [Account] = []
    
    var searchedText:String = ""
    lazy var allLeads: [Lead] = []
    lazy var allOpportunities: [Opportunity] = []
    lazy var allAccounts: [Account] = []
    private var entityType:EntityType = .all
    
    // MARK: - Outlets
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UISearchBar!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var viewColored: UIView!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.placeholder = "Search by contact or address"
        viewColored.layer.cornerRadius = 6
        txtStatus.text = entityType.title
        setNoDataFound()
    }
    
    func setData() {
        var filterLeads: [Lead] = []
        var filterOpportunities: [Opportunity] = []
        allAccounts = accounts
        for opportunity in opportunities {
            let lat =  opportunity.latitude ?? 0.0
            let lng = opportunity.longitude ?? 0.0
            if accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil {
                filterOpportunities.append(opportunity)
            }
        }
        opportunities = filterOpportunities
        allOpportunities = opportunities
        
        for  lead in leads {
            let lat =  lead.latitude ?? 0.0
            let lng = lead.longitude ?? 0.0
            if opportunities.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil && accounts.filter({ $0.latitude == lat && $0.longitude == lng}).first == nil  {
                filterLeads.append(lead)
            }
        }
        leads = filterLeads
        allLeads = leads
        tableView?.reloadData()
        setNoDataFound()
    }
    private func setNoDataFound() {
        if (entityType == .all && (!leads.isEmpty || !opportunities.isEmpty || !accounts.isEmpty)) || (entityType == .leads && !leads.isEmpty) || (entityType == .opportunity && !opportunities.isEmpty) || (entityType == .account && !accounts.isEmpty) {
            lblNoRecordFound?.isHidden = true
        } else {
            lblNoRecordFound?.isHidden = false
        }
    }
    // MARK: - Actions
    @IBAction func actionSelectStatus(_ sender: Any) {
        var rows:[String] = []
        for row in EntityType.allCases {
            rows.append(row.title)
        }
        var selectedRow = 0
        for (index,row) in rows.enumerated() {
            if txtStatus.text == row{
                selectedRow = index
            }
        }
        showStringPicker(sender: txtStatus, selectedRow: selectedRow, rows: rows) { (index, value, view) in
            self.txtStatus.text = value
            self.entityType = EntityType(rawValue: index) ?? .all
            self.setNoDataFound()
            self.tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource
extension NearByListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (entityType == .all || entityType == .account) ? accounts.count: 0
        } else if section == 1 {
            return (entityType == .all || entityType == .leads) ? leads.count: 0
        } else {
            return (entityType == .all || entityType == .opportunity) ? opportunities.count: 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NearByListTableViewCell.self), for: indexPath) as! NearByListTableViewCell
       
        if indexPath.section == 0 {
            cell.account = accounts[indexPath.row]
        } else if indexPath.section == 1 {
            cell.lead = leads[indexPath.row]
        } else {
            cell.opportunity = opportunities[indexPath.row]
        }
        return cell
    }
}
// MARK: - UITableViewDelegate
extension NearByListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            
            let accountL = accounts[indexPath.row]
            
            let locationL = CLLocation(latitude: accountL.latitude!, longitude: accountL.longitude!)

            openVisitDeatils(.account, account: accounts[indexPath.row], location: locationL)
        case 1:
            
            let leadL = leads[indexPath.row]

            let locationL = CLLocation(latitude: leadL.latitude!, longitude: leadL.longitude!)

            openVisitDeatils(.lead, lead: leads[indexPath.row], location: locationL)
        case 2:
            
            let opportunitiesL = opportunities[indexPath.row]

            let locationL = CLLocation(latitude: opportunitiesL.latitude!, longitude: opportunitiesL.longitude!)

            openVisitDeatils(.opportunity, opportunity: opportunities[indexPath.row], location: locationL)
        default:
            break
        }
    }
    
    private func showAlertForBranchChange(strAppoinntmentBranch : String, strEmpLoginBranchSysName : String) {
        
        var loggedBranchNameLocal = ""
        var appointmentBranchNameLocal = ""

        var arrBranch = NSMutableArray()
        arrBranch = getBranches()
        
        for item in arrBranch
        {
            
            let dict = item as! NSDictionary
            
            if strEmpLoginBranchSysName == "\(dict.value(forKey: "SysName") ?? "")"
            {
                loggedBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
            if strAppoinntmentBranch == "\(dict.value(forKey: "SysName") ?? "")"
            {
                appointmentBranchNameLocal = "\(dict.value(forKey: "Name") ?? "")"
            }
            
        }
        
        let strMsssggg = "You are currently in " + "\(loggedBranchNameLocal)" + " branch. Please switch branch to work on the " + "\(appointmentBranchNameLocal)" + " branch."
        
        //You are currently in Lodi branch. Please switch branch to work on the Fresno branch.
        
        let alert = UIAlertController(title: alertMessage, message: strMsssggg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Switch Branch", style: .destructive, handler: { (nil) in
            
            self.goToSwitchBranch()
            
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            // Do Nothing On This.
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getBranches() -> NSMutableArray {
        let temparyBranch = NSMutableArray()

        if let dict = nsud.value(forKey: "LeadDetailMaster") {
            if(dict is NSDictionary){
                if let aryBranchMaster = (dict as! NSDictionary).value(forKey: "BranchMastersAll"){
                    if(aryBranchMaster is NSArray){
                        for item  in aryBranchMaster as! NSArray{
                            if((item as! NSDictionary).value(forKey: "IsActive") as! Bool == true){
                                temparyBranch.add((item as! NSDictionary))
                            }
                        }
                    }
                }
            }
        }
        return temparyBranch
    }
    
    func goToSwitchBranch() {
        
        let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "DashBoard" : "DashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "SwitchBranchVC") as! SwitchBranchVC
         
        vc.reDirectWhere = "NearByMap"
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    private func openVisitDeatils(_ propertyType: PropertyType, lead: Lead? = nil, opportunity: Opportunity? = nil, account: Account? = nil, location: CLLocation? = nil)  {
        
        
        // Check if Logged In Branch is different
        
        var dictLoginData = NSDictionary()
        dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strEmpLoginBranchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"

        var proceedFurther = true
        
        if lead != nil {
            
            let arrOfBranchesSysName = lead?.branchesSysNames
            
            proceedFurther = false
            
            if arrOfBranchesSysName?.count ?? 0 > 0 {
                
                for k in 0 ..< arrOfBranchesSysName!.count {

                    let strBrancchL = "\(arrOfBranchesSysName![k])"
                    
                    if strBrancchL == strEmpLoginBranchSysName {
                        
                        proceedFurther = true
                        break
                        
                    }
                    
                }
                
            }
            

            if !proceedFurther {
                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)

            }
            
        }else if opportunity != nil {
            
            if opportunity?.branchSysName != strEmpLoginBranchSysName {
                
                proceedFurther = false
                // Show Alert of Branch
                                
                self.showAlertForBranchChange(strAppoinntmentBranch: "\(opportunity?.branchSysName ?? "")", strEmpLoginBranchSysName: strEmpLoginBranchSysName)
                
            }
            
        }
        
        if proceedFurther {
            
            let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "EntityDetailViewController") as? EntityDetailViewController else { return }
            controller.modalPresentationStyle = .overFullScreen
            var propertyDetails = PropertyDetails()
            propertyDetails.propertyType = propertyType
            propertyDetails.lead = lead
            propertyDetails.opportunity = opportunity
            propertyDetails.account = account
            controller.propertyDetails = propertyDetails
            controller.selectedLocation = location
            let navController = UINavigationController.init(rootViewController: controller)
            navController.isNavigationBarHidden = true
            navController.modalPresentationStyle = .overFullScreen
            navigationController?.present(navController, animated: true, completion: nil)
            
        }
        
    }
}

// MARK:- SearchBarDelegate
extension NearByListViewController:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchedText = searchText
        if searchText.isEmpty {
            leads = allLeads
            accounts = allAccounts
            opportunities = allOpportunities
            
        } else {
            leads = allLeads
            accounts = allAccounts
            opportunities = allOpportunities
            leads = leads.filter { (lead)   in
                let name = "\(lead.firstName ?? "") \(lead.middleName ?? "") \(lead.lastName ?? "")"
                let address = "\(lead.address1 ?? ""), \(lead.cityName ?? ""), \(lead.countyName ?? "") \(lead.zipcode ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText) 
            }
            opportunities = opportunities.filter { (opportunity)   in
                var address = ""
                
                if let serviceAddress = opportunity.serviceAddress {
                    address = "\(serviceAddress.address1 ?? "") \(serviceAddress.cityName ?? "") \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
                    
                }
                let name = "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText)
            }
            
            accounts = accounts.filter { (account)   in
                var address = ""
                if let serviceAddress = account.serviceAddress {
                    address = "\(serviceAddress.address1 ?? "") \(serviceAddress.cityName ?? "") \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
                    
                }
                let name = "\(account.firstName ?? "") \(account.middleName ?? "") \(account.lastName ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText)
            }
        }
        if (entityType == .all && (!leads.isEmpty || !opportunities.isEmpty || !accounts.isEmpty)) || (entityType == .leads && !leads.isEmpty) || (entityType == .opportunity && !opportunities.isEmpty) || (entityType == .account && !accounts.isEmpty) {
            lblNoRecordFound.isHidden = true
        } else {
            lblNoRecordFound.isHidden = false
        }
        
        tableView.reloadData()
    }
    func searchInList(_ searchText: String) {
        
        searchedText = searchText
        if searchText.isEmpty {
            leads = allLeads
            accounts = allAccounts
            opportunities = allOpportunities
            
        } else {
            leads = allLeads
            accounts = allAccounts
            opportunities = allOpportunities
            leads = leads.filter { (lead)   in
                let name = "\(lead.firstName ?? "") \(lead.middleName ?? "") \(lead.lastName ?? "")"
                let address = "\(lead.address1 ?? ""), \(lead.cityName ?? ""), \(lead.countyName ?? "") \(lead.zipcode ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText)
            }
            opportunities = opportunities.filter { (opportunity)   in
                var address = ""
                
                if let serviceAddress = opportunity.serviceAddress {
                    address = "\(serviceAddress.address1 ?? "") \(serviceAddress.cityName ?? "") \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
                    
                }
                let name = "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText)
            }
            
            accounts = accounts.filter { (account)   in
                var address = ""
                if let serviceAddress = account.serviceAddress {
                    address = "\(serviceAddress.address1 ?? "") \(serviceAddress.cityName ?? "") \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
                    
                }
                let name = "\(account.firstName ?? "") \(account.middleName ?? "") \(account.lastName ?? "")"
                return name.localizedCaseInsensitiveContains(searchText) || address.localizedCaseInsensitiveContains(searchText)
            }
        }
        if (entityType == .all && (!leads.isEmpty || !opportunities.isEmpty || !accounts.isEmpty)) || (entityType == .leads && !leads.isEmpty) || (entityType == .opportunity && !opportunities.isEmpty) || (entityType == .account && !accounts.isEmpty) {
            lblNoRecordFound?.isHidden = true
        } else {
            lblNoRecordFound?.isHidden = false
        }
        
        tableView?.reloadData()
    }
}




