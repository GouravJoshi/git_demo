//
//  NearByListTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 14/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class NearByListTableViewCell: UITableViewCell {
    
    // MARK: -  Outlets
    @IBOutlet weak var lblVisitCount: UILabel!
    @IBOutlet weak var lblVisitStatus: UILabel!
    @IBOutlet weak var lblLeadName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var visitCountView: UIStackView!
    @IBOutlet weak var lblDate: UILabel!
    // MARK: - Variables
    var lead: Lead? {
        didSet{
            guard let lead = lead else { return }
            if let date = lead.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblDate.text =  statusDate.trimmed
            } else {
                lblDate.text = nil
            }
            if let leadName = lead.leadName, !leadName.isEmpty {
                lblLeadName.text = leadName
            } else {
                lblLeadName.text = "\(lead.firstName ?? "") \(lead.middleName ?? "") \(lead.lastName ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            }
            let address = "\(lead.address1 ?? ""), \(lead.cityName ?? ""), \(lead.countyName ?? "") \(lead.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            lblAddress?.text = address
            lblVisitStatus.text = lead.visitStatusName?.trimmed
            if let visitCount = lead.visitCount {
                visitCountView.isHidden = false
                lblVisitCount.text = visitCount.description + " Visit"
            } else {
                visitCountView.isHidden = true
            }
            if let statusName = lead.visitStatusSysName, let colorCode = AppUserDefaults.shared.leadDetailMaster?.visitsStatus.first(where: { (status) -> Bool in
                return status.sysName == statusName
            })?.colorCode {
                if let tempColor = UIColor(hex:colorCode) {
                    lblVisitStatus.textColor = tempColor
                }
            } else {
                lblVisitStatus.textColor = .lightGray
            }
        }
    }
    
    var  opportunity: Opportunity? {
        didSet {
            guard let opportunity = opportunity else { return }
            
            if let date = opportunity.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblDate.text =  statusDate.trimmed
            } else {
                lblDate.text = nil
            }
            lblLeadName.text = "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            lblVisitStatus.text = opportunity.opportunityStatus
            if let serviceAddress = opportunity.serviceAddress {
                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
                lblAddress?.text = address.trimmed
            } else {
                lblAddress?.text = nil
            }
            visitCountView.isHidden = true
            
            if let sysName = opportunity.opportunityStage, let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadStageMasters.first(where: { (status) -> Bool in
                return status.sysName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                
                if let tempColor = UIColor(hex:colorCode) {
                    lblVisitStatus.textColor = tempColor
                }
            } else  if let sysName = opportunity.opportunityStatus, let colorCode = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.first(where: { (status) -> Bool in
                return status.statusName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                
                if let tempColor = UIColor(hex:colorCode) {
                    lblVisitStatus.textColor = tempColor
                }
            } else {
                lblVisitStatus.textColor = .lightGray
            }
        }
        
    }
    var  account: Account? {
        didSet {
            guard let accountDetail = account else { return }
            lblLeadName.text = "\(accountDetail.firstName ?? "") \(accountDetail.middleName ?? "") \(accountDetail.lastName ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            lblVisitStatus.text = nil
            if let date = accountDetail.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblDate.text =  statusDate.trimmed
            } else {
                lblDate.text = nil
            }
            if let serviceAddress = accountDetail.serviceAddress{
                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
                lblAddress?.text = address.trimmed
            } else {
                lblAddress?.text = nil
            }
            visitCountView.isHidden = true
            lblVisitStatus.textColor = .lightGray
        }
    }
    // MARK: -  View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
