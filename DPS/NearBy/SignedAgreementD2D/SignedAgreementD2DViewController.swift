//
//  SignedAgreementD2DViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class SignedAgreementD2DViewController: UIViewController {
   // var agreements:
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}
//MARK: - UITableViewDataSource
extension SignedAgreementD2DViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
       // return agreements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SignedAgreementD2DTableViewCell.self), for: indexPath) as! SignedAgreementD2DTableViewCell
        cell.setUI()
       // cell.agreement = agreements[indexPath.row]
        return  cell
    }
}
//MARK: - UITableViewDelegate
extension SignedAgreementD2DViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let destination = UIStoryboard(name: "Agreement", bundle: nil).instantiateViewController(withIdentifier: "ServiceAgrementViewController") as? ServiceAgrementViewController else {
            return
        }
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
