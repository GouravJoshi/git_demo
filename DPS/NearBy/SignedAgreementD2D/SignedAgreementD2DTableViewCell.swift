//
//  SignedAgreementTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class SignedAgreementD2DTableViewCell: UITableViewCell {
    //MARK: - Variables
    /* var agreement: SendAgreement? {
     didSet {
     setData()
     }
     }*/
    // MARK: - Outlets
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var lblTask: UILabel!
    @IBOutlet weak var lblName: UILabel!
    // MARK: - View Liufe cycle
    @IBOutlet weak var lblDate: UILabel!
    
    // MARK: - Convinience
    func setData() {
        #warning("HardCoded Data")
        lblName.text = ""
        lblTask.text = ""
        let dateString = "20/12/2021"
        if  let signedDate = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy") {
            lblDate.text = "Signed on \(signedDate)"
        }
    }
    func setUI() {
        AppUtility.buttonImageColor(btn: btnMessage, image: "Group 770", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnCall, image: "Group 760", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnChat, image: "Group 773", color: .appDarkGreen)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
