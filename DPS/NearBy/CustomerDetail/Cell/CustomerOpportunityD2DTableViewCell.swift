//
//  CustomerOpportunityD2DTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class CustomerOpportunityD2DTableViewCell: UITableViewCell {
    // MARK:- Outlets
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOpportunityNumber: UILabel!
    
    // MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
