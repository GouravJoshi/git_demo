//
//  CustomerContactsD2DTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class CustomerContactsD2DTableViewCell: UITableViewCell {
    // MARK:- Variables
   /* var contact: {
       didSet {
            
        }
    }*/
    // MARK:- Outlets
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var lblName: UILabel!
    
    func setUI() {
        AppUtility.buttonImageColor(btn: btnCall, image: "Group 760", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnCall, image: "Group 770", color: .appDarkGreen)
    }
    // MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
