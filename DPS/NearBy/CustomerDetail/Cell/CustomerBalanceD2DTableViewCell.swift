//
//  CustomerBalanceD2DTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class CustomerBalanceD2DTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTotalBalance: UILabel!
    @IBOutlet weak var lblCurrentBalance: UILabel!
    // MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }

}
