//
//  CustomerDetailD2DViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class CustomerDetailD2DViewController: BaseViewController {
    // MARK: - Variables
    enum DetailSections : Int, CaseIterable {
        case propertyInfo = 0
        case balance
        case contact
        case opportunity
        
        var title: String {
            switch self {
            case .propertyInfo:
                return ""
            case .balance:
                return ""
            case .contact:
                return "Contacts"
            case .opportunity:
                return "Oppertunities"
            }
        }
    }
    var propertyDetails: PropertyDetails?
    // MARK: - Outlets
    @IBOutlet weak var tableview: UITableView!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    // MARK: - Convienience
    @objc func actionOpenEmail() {
        if let email = propertyDetails?.propertyInfo?.email?.trimmed {
            actionEmail(email: email)
        }
    }
    @objc func actionOpenCall() {
        if let number = propertyDetails?.propertyInfo?.phone?.trimmed {
            actionCall(number: number)
        }
    }
    
}
// MARK: - UITableViewDataSource
extension CustomerDetailD2DViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return DetailSections.allCases.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch DetailSections(rawValue: section) {
        case .propertyInfo, .balance :
            return 1
        case .contact :
            return 3
        case .opportunity :
            return 4
        case .none:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch DetailSections(rawValue: indexPath.section) {
        case .propertyInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PropertyDetailHeaderTableViewCell.self), for: indexPath) as! PropertyDetailHeaderTableViewCell
            cell.propertyInfo = propertyDetails?.propertyInfo
            let tap = UITapGestureRecognizer(target: self, action: #selector(actionOpenCall))
            cell.stackPhone.addGestureRecognizer(tap)
            let emailTap = UITapGestureRecognizer(target: self, action: #selector(actionOpenEmail))
            cell.stackEmail.addGestureRecognizer(emailTap)
            return  cell
        case .some(.balance):
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerBalanceD2DTableViewCell.self), for: indexPath) as! CustomerBalanceD2DTableViewCell
            
            return cell
        case .some(.contact):
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerContactsD2DTableViewCell.self), for: indexPath) as! CustomerContactsD2DTableViewCell
            cell.setUI()
            return cell
            
        case .some(.opportunity):
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomerOpportunityD2DTableViewCell.self), for: indexPath) as! CustomerOpportunityD2DTableViewCell
            return cell
        case .none:
            return UITableViewCell()
        }
    }
}
// MARK: - UITableViewDelegate
extension CustomerDetailD2DViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderCustomerD2DTableViewCell.self)) as! HeaderCustomerD2DTableViewCell
        switch DetailSections(rawValue: section) {
        case .contact:
            cell.lblTitle.text = DetailSections(rawValue: section)?.title
            cell.btnAddOpportunity.isHidden = true
            return cell.contentView
        case .opportunity:
            cell.lblTitle.text = DetailSections(rawValue: section)?.title
            cell.btnAddOpportunity.isHidden = false
            return cell.contentView
        default:
            return UIView()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch DetailSections(rawValue: section) {
        case .contact, .opportunity:
            return 44
        default:
            return 1
        }
    }
}
