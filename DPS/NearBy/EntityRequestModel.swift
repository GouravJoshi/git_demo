//
//  EntityRequestModel.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct LocationInfo: Codable {
    var companyKey = ""
    var latitude = ""
    var opportunityStatus = ""
    var employeeID: String = ""
    var longitude = ""
    var aerialDistance = ""
    var leadStatus = ""
    var visitStatus = ""
    enum CodingKeys: String, CodingKey {
        case companyKey = "CompanyKey"
        case latitude = "Latitude"
        case opportunityStatus = "OpportunityStatus"
        case employeeID = "EmployeeId"
        case longitude = "Longitude"
        case aerialDistance = "AerialDistance"
        case leadStatus = "LeadStatus"
        case visitStatus = "VisitStatus"
    }
}
