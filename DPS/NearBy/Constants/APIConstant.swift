//
//  APIConstant.swift
//  DPS
//
//  Created by Vivek Patel on 07/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

extension URL {
    
    static var baseUrl: String {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let strUrlSaved = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        return strUrlSaved
    }
    static var headers: HTTPHeaders {
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let empNo = "\(dictLoginData.value(forKey: "EmployeeNumber")!)"
        let empId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let name = "\(dictLoginData.value(forKey: "EmployeeName")!)"
        let companyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId") ?? "")"
        let salesCompanyId = "\(dictLoginData.value(forKeyPath: "Company.SalesProcessCompanyId") ?? "" )"
        let hrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
        let emailId = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        let ipAddress = Global().getIPAddress()
        return ["BranchId":Global().strEmpBranchID(),
                "BranchSysName":Global().strEmpBranchSysName(),
                "IsCorporateUser":Global().strIsCorporateUser(), "ClientTimeZone":Global().strClientTimeZone(),
                "Browser": "IOS",
                "CompanyKey": companyKey,
                "VisitorIP": ipAddress ?? "",
                "Content-Type":"application/json",
                "Accept":"application/json",
                "HrmsCompanyId":hrmsCompanyId,
                "EmployeeNumber": empNo,
                "EmployeeId": empId,
                "CreatedBy": empId,
                "EmployeeName": name,
                "CoreCompanyId": companyId,
                "SalesProcessCompanyId": salesCompanyId,
                "CompanyId": companyId,
                "EmployeeEmail": emailId,
                "IpAddress": ipAddress ?? "",
        ]
    }
    static var salesAutoBaseUrl: String {
        if nsud.value(forKey: "LoginDetails") != nil
        {
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            return "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesAutoModule.ServiceUrl")!)"
        }
        else
        {
            return ""
        }
    }
    
    static var salesCRMBaseUrl: String {
        if nsud.value(forKey: "LoginDetails") != nil
        {
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            return "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)"
        }
        else
        {
            return ""
        }
    }
    
    
    enum Entities {
        static let getLeads = baseUrl + "api/LeadNowAppToSalesProcess/GetWebleadListByAerialDistanceV2"
        static let getAccounts = baseUrl + "api/LeadNowAppToSalesProcess/GetAccountListByAerialDistance"
        static let getOpportunity = baseUrl + "api/LeadNowAppToSalesProcess/GetOpportunityListByAerialDistanceV2"
    }
    enum Service {
        static let getServiceDetail = salesAutoBaseUrl + "api/mobiletosaleauto/GetDoorToDoorLeadByLeadId?companyKey=(companyKey)&LeadNumber=(LeadNumber)"
    }
    enum LogActivity {
        static let logActivity =  baseUrl + "/api/LeadNowAppToSalesProcess/AddUpdateActivity"
    }
    enum ServiceReport {
        //static let sendMail = salesAutoBaseUrl + "api/MobileToSaleAuto/UpdateMobileDoorToDoorLead"
        //static let sendMail = salesAutoBaseUrl + "api/MobileToSaleAuto/UpdateMobileDoorToDoorLead_V1"
        static let sendMail = salesAutoBaseUrl + "api/MobileToSaleAuto/UpdateMobileDoorToDoorLead_V2"

        static let resendMail = salesAutoBaseUrl  + "api/MobileToSaleAuto/MobileResendAgreementProposalMail"
        static let getBestFitEmployees = MainUrl + "api/Employee/GetBestFitEmployees"
        static let saveSetup = MainUrl + "api/MobileAppToCore/SaveSetupandGenerateWorkOrder"
        static let rescheduleSetup = MainUrl + "api/MobileAppToCore/RescheduleServiceOrder"
        //static let getRoute =  MainUrl + "api/MobileAppToCore/GetSuggestedRouteAndOtherRoutesAsync"
        static let getRoute =  MainUrl + "api/MobileAppToCore/GetSuggestedRouteAndOtherRoutesAsyncForDoorToDoor"
        static let updateSetup = salesAutoBaseUrl + "api/MobileToSaleAuto/UpdateLeadInitialSetup?leadId=(LeadId)&IsInitialSetup=(IsInitialSetup)"
        //  https://pcrsstaging.pestream.com/api/MobileAppToCore/GetSuggestedRouteAndOtherRoutesAsync
    }
    enum Tax {
        static let tax = salesAutoBaseUrl + "/api/MobileToSaleAuto/GetTaxByTaxCode?companyKey=(companyKey)&taxSysName=(taxSysName)"
        
    }
    enum LeadD2D {
        static let addLeadOpp = baseUrl + "api/LeadNowAppToSalesProcess/AddLeadOpportunity_DoorToDoor_DetailResult"
        static let editLeadOpp = baseUrl + "api/LeadNowAppToSalesProcess/EditWebLead_DoorToDoor_DetailResult"
        static let uploadImage = baseUrl + "api/File/UploadWebLeadAddressImagesAsync"
        static let createTask = baseUrl + "api/LeadNowAppToSalesProcess/AddUpdateTask"
    }
    enum Signature {
        static let upload = salesAutoBaseUrl +  "/api/File/UploadSignatureAsync"
        static let uploadEAF = baseUrl + "/api/File/UploadSignatureAsync"
        static let uploadCheque = salesAutoBaseUrl + "/api/File/UploadCheckImagesAsync"
    }
    enum Document {
        static let uploadDocument = salesAutoBaseUrl +  "/api/File/UploadOtherDocsAsync"
        static let uploadAudio = salesAutoBaseUrl +  "/api/File/UploadAudioAsync"
        static let uploadXML = salesAutoBaseUrl +  "/api/File/UploadGraphXMLFileAsync"

    }
    enum SendText {
        static let updateTextDetail = salesAutoBaseUrl +  "/api/MobileToSaleAuto/UpdateTextContactDetail"
    }
    
    enum TimeRange {
        
        static let updateTimeRange = salesAutoBaseUrl +  "/api/MobileToSaleAuto/UpdateScheduleTimeInformation"
    }
    enum LeadNowFlow{
        
        static let getleadNowData = "/api/LeadNowAppToSalesProcess/GetWebLeadListAndCounts"
        
    }
    
    enum Gallery {
        static let uploadImages = salesAutoBaseUrl +
            "/api/File/UploadMultipleImageAsync"
        static let uploadImagesSingle = salesAutoBaseUrl +
            "/api/File/UploadImageAsync"
        static let uploadServiceAddressImageSales = baseUrl +
            "api/File/UploadCustomerAddressImageAsync"
        
        static let uploadTargetImage = salesAutoBaseUrl +
            "/api/File/UploadTargetImageAsync"
        static let uploadCRMImagesSingle = salesCRMBaseUrl +
            "/api/File/UploadAsync"
    }
    enum Schedule {
        static let viewScheduleOld = baseUrl +
            "api/LeadNowAppToSalesProcess/GetLeadViewScheduleStatusByEmployee?companyKey=(companyKey)&employeeid=(employeeId)"
        
        static let viewSchedule = baseUrl +
            "api/LeadNowAppToSalesProcess/GetLeadViewScheduleStatusByEmployeeV1?companyKey=(companyKey)&employeeid=(employeeId)"
        
        static let notifyViaMail = MainUrl + "api/MobileAppToCore/SendWorkOrderScheduleConfirmationEmail"
    }
    enum GeoMapping
    {
        static let  getAllAssigneArea = MainUrl + "api/MobileAppToCore/GetAllEmployeeGeoFencingAreaByEmployeeId?companykey=(companykey)&employeeId=(employeeId)"
         static let addUpdateArea = MainUrl + "api/MobileAppToCore/AddUpdateEmployeeGeoFencingArea"
        static let geoHistoryList = MainUrl + "api/MobileAppToCore/GetEmployeeGeoFencingAreaHistoryByAreaId?companykey=Production&employeeGeoFencingAreaId=(employeeGeoFencingAreaId)"
        static let saveAreaName = MainUrl + "api/MobileAppToCore/UpdateEmployeeGeoFencingAreaNameById?companykey=(companykey)&employeeGeoFencingAreaId=(employeeGeoFencingAreaId)&area=(area)"
        static let unaasign = MainUrl + "api/MobileAppToCore/UnassignedEmployeeGeoFencingAreaById?companykey=(companykey)"
      
        
        static let getAssignArea = MainUrl + "api/MobileAppToCore/GetEmployeeGeoFencingAreaByEmployeeId?companykey=(companykey)&employeeId=(employeeId)"
        
        static let getAllEmployees = MainUrl + "api/MobileAppToCore/GetAllEmployeeGeoFencings?companyKey=(companyKey)"
        
        static let deleteArea = MainUrl + "api/MobileAppToCore/DeleteEmployeeGeoFencingAreaById?companykey=Production&employeeGeoFencingAreaId=(employeeGeoFencingAreaId)"
        
        static let checkAssignArea = MainUrl + "api/MobileAppToCore/IsGeoFencingAreaExists?companykey=(companykey)&employeeGeoFencingAreaId=(employeeGeoFencingAreaId)"//employeeIds

        
    }
    enum LiveTarcking {
        static let getLiveLocations = MainUrl +
            "api/MobileAppToCore/GetLiveTrackingMultiEmployeeCurrentLocation?companyKey=(companyKey)&CreatedDate=(CreatedDate)"
    }
    enum BillingAddress {
        static let getServiceAddress = baseUrl +
            "api/LeadNowAppToSalesProcess/GetServiceAddressesByAccountId?AccountId=(AccountId)"
    }
    enum RefreshSignature {
        static let refreshSignatureUrl = salesAutoBaseUrl +  "/api/MobileToSaleAuto/GetDoorToDoorRefreshLeadId?LeadId=(LeadId)&CompanyKey=(CompanyKey)"
    }
    enum SwitchBranch {
        static let switchBranchUrl = MainUrl +  "/api/mobileapptocore/GetBranchDetaildByBranchId?companykey="
        //static let companyKey = "&CompanyKey="
         //(LeadId)&CompanyKey=(CompanyKey)"
    }
    
    enum ServiceNewPestFlow{
        
           static let getNotesAlert = "api/LeadNowAppToSalesProcess/GetAlertNotes?RefType=%@&RefIdNo=%@&CompanyKey=%@"
           static let addTechComment = "Api/MobileToServiceAuto/AddUpdateServiceNoteFavoriteDetail"
           static let getListOfComment = "Api/MobileToServiceAuto/GetServiceNoteFavoriteDetailByEmployee?CompanyId=%@&EmployeeId=%@"
           static let markFavouiteComment = "/Api/MobileToServiceAuto/AddServiceNoteFavorite"
           static let getTimeRangeList = MainUrl +
    "api/MobileAppToCore/GetAllRangeofTimeAsync?CompanyKey=production"
           static let deleteTechComment = "/api/MobileToServiceAuto/DeleteTechCommentsByIds"
           static let getGenreralNotesByMaster =  "/Api/MobileToServiceAuto/GetByCompanyIdAsync?CompanyId=%@"
           
           static let markProdufctFav = "/Api/MobileToServiceAuto/AddUpdateServiceProductFavoriteDetail"
           static let getFavProductList = "/Api/MobileToServiceAuto/GetServiceProductFavoriteDetailByEmployee?CompanyId=%@&EmployeeId=%@"

           static let getChemicalCalculation = "Api/Workorder/GetUndilutedQuantity?ProductId=%@&Quantity=%@&UnitOfMeasure=%@&Concentration=%@&CompanyId=%@"
           
           static let getCurrentSetupList =  MainUrl + "api/MobileAppToCore/GetCurrentSetupByAccountNo?companyKey=%@&AccountNo=%@"
           
           static let getTheNonPreferabledateAndTime = MainUrl + "/Api/MobileAppToCore/GetNonPreferrableDayAndTime?CompanyKey=%@"
        
        static let uploadImagesInAsync = salesAutoBaseUrl +
         "/api/File/UploadMultipleImageAsync"
        
       }
    
    enum HelpTicket{
        static let getStatus = MainUrl +  "/api/HelpTicket/GetStatusList?CompanyKey=%@"
        static let getClientPriorityList = MainUrl + "api/HelpTicket/GetClientPriorityList"
        static let getTicketList = MainUrl + "api/HelpTicket/GetTicketList"
        static let getTicketDetails = MainUrl + "api/HelpTicket/GetTicketByTicketId?TicketId=%@&CompanyKey=%@"
        static let closeTicket = MainUrl + "api/HelpTicket/UpdateTicketQuickAction?TaskTimeEntryId=%@&ActionValue=%@&ActionName=%@&CompanyKey=%@&EmployeeId=%@"
        static let upadteTicketDetail = MainUrl + "api/HelpTicket/UpdateTicket"
        static let addComment = MainUrl + "/api/HelpTicket/AddTicketComment?CompanyKey=%@"
        static let addTicket = MainUrl + "/api/HelpTicket/AddTicket?CompanyKey=%@"
    }
    
}
