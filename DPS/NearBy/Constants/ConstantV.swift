//
//  Contants.swift
//  DPS
//
//  Created by Vivek Patel on 12/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

class ConstantV: NSObject {
    static let modifiedDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let latitude = 0.0//38.9072
    static let longitude = 0.0//-77.0369
}
