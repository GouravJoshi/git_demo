//
//  LeadsStatusCollectionViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 15/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class VisitStatusCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewRadio: UIImageView!
}
