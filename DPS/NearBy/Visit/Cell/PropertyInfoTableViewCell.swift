//
//  PropertyInfoTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 23/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class PropertyInfoTableViewCell: UITableViewCell {
    // MARK: - Variables
    var propertyInfo: PropertyInfo?   {
        didSet {
            guard let propertyInfo = propertyInfo else { return }
            lblName.text = "\(propertyInfo.firstName ?? "") \(propertyInfo.middleName ?? "") \(propertyInfo.lastName ?? "")"
            
            lblPhone.text = propertyInfo.phone
            lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
            var type = ""
            if let propertyType = propertyInfo.type, !propertyType.trimmed.isEmpty {
                type.append("\(propertyType)")
            }
            if let area = propertyInfo.area , !area.isEmpty {
                type.append(", \(area) Sq. Feet.")
            }
            lblType.text = type
            lblEmail.text = propertyInfo.email
            lblLeadAddress.text = propertyInfo.address
            
            if let floor = propertyInfo.noOfFloor , floor != "" {
                lblNoOfFloors.isHidden = false
                lblNoOfFloors.text = "No. of floors: \(floor)"
            } else {
                lblNoOfFloors.isHidden = true
            }
            lblName.isHidden = lblName.text?.trimmed == "" || lblName.text == nil ? true : false
            lblPhone.isHidden = lblPhone.text?.trimmed == "" || lblPhone.text == nil ? true : false
            lblType.isHidden = lblType.text?.trimmed == "" || lblType.text == nil ?  true : false
            lblEmail.isHidden = lblEmail.text?.trimmed == "" || lblEmail.text == nil ? true : false
            
            lblName.isHidden = false
            
            if lblName.text?.trimmed.count == 0 {
                
                lblName.text = " "
                
            }
            
//            if let image = propertyInfo.imageManual {
//                imgViewProperty.image = image
//            } else {
//                guard let imgUrl = propertyInfo.imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl) else {
//                    return
//                }
//                imgViewProperty.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details")?.maskWithColor(color: .appThemeColor))
//            }
            
            if let image = propertyInfo.imageManual {
                imgViewProperty.image = image
            } else {
                //
                let aString = propertyInfo.imageUrl ?? ""
                let newString = aString.replacingOccurrences(of: "WebLeadDocuments", with: "WebLeadAddressImages", options: .literal, range: nil)
                
                guard let imgUrl = newString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl) else {
                    return
                }
                imgViewProperty.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details")?.maskWithColor(color: .appThemeColor))
            }
            
            
            
        }
    }
    
    // MARK: - Outltes
    @IBOutlet weak var btnAddService: UIButton!
    @IBOutlet weak var imgViewProperty: UIImageView!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var lblLeadAddress: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblServiceDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnEdit, image: "Group 779", color: .black)
        }
    }
    @IBOutlet weak var btnEditBillingAddress: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnEditBillingAddress, image: "Group 779", color: .black)
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblNoOfFloors: UILabel!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
