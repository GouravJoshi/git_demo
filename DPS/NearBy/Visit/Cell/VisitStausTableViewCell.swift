//
//  VisitStausTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 12/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class VisitStausTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var imgViewRadio: UIImageView!
    @IBOutlet weak var viewColoured: UIView!
    @IBOutlet weak var lblName: UILabel!
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
