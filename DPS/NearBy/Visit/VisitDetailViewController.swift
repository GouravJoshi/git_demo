//
//  VisitDetailViewController.swift
//  DPS
//
//  Created by Vivek Patel on 12/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Kingfisher


protocol VisitActionDelegate {
    func actionGetQuote(_ propertyDetails: PropertyDetails)
}

final class VisitDetailViewController: BaseViewController {
    
    // MARK: - Variables
    var delegate: VisitActionDelegate?
    var propertyDetails: PropertyDetails?
    private var isEditable = true {
        didSet {
            setData()
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var lblVisitStatus: UILabel!
    @IBOutlet weak var lblLeadAddress: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnGetQuotes: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewPhone: UIStackView!
    @IBOutlet weak var viewEmail: UIStackView!
    @IBOutlet weak var btnDissmiss: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnAddVisit: UIButton!
    @IBOutlet weak var lblNoOfFloor: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewLeadStatus: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NearBy Me"
        setData()
    }
    
    // MARK: - Utility
    private func setData() {
        guard let propertyDetails = propertyDetails else { return}
        
        switch propertyDetails.propertyType {
        case .lead:
            if let lead = propertyDetails.lead {
                self.propertyDetails?.propertyInfo = AppUtility.getPropertyDetailFromLead(lead)
                setupDataFromLead(lead)
            }
        case .opportunity:
            if let opportunity = propertyDetails.opportunity {
                setupDataForOpportunity(opportunity)
            }
            break
        case .account:
            if let account = propertyDetails.account {
                setupDataForAccount(account)
            }
            break
        case .empty:
            setupDataForNewProperty(propertyDetails)
        }
        if propertyDetails.propertyType == .empty {
            btnAddVisit.setTitle("Visit", for: .normal)
        } else {
            btnAddVisit.setTitle("Edit", for: .normal)  
        }
        if propertyDetails.propertyType == .opportunity {
            btnGetQuotes.setTitle("Visit Again", for: .normal)
        } else {
            btnGetQuotes.setTitle("Get Quotes", for: .normal)
        }
        viewPhone.isHidden =  lblPhone.text == "" ||  lblPhone.text == nil ? true : false
        viewEmail.isHidden =  lblEmail.text == "" ||  lblEmail.text == nil ? true : false
        lblNoOfFloor.isHidden =  lblNoOfFloor.text == "" ||  lblNoOfFloor.text == nil ? true : false
        lblArea.isHidden =  lblArea.text == "" ||  lblArea.text == nil ? true : false
        setupCollectionViewDefaultPosition()
    }
    private func setupDataFromLead(_ leadDetail: Lead) {
        // btnAddVisit.isHidden = !isEditable
        viewLeadStatus.isHidden = false
        if  let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == leadDetail.statusName}).first {
            if leadsStatus.name == "Converted" || leadsStatus.name == "Complete" {
                btnAddVisit.isHidden = true
            } else {
                btnAddVisit.isHidden = !isEditable
            }
            if let date = leadDetail.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblStatus.text = (leadsStatus.name ?? "") + " - " + statusDate
            } else {
                lblStatus.text = leadsStatus.name
            }
        }
        if let leadName = leadDetail.leadName, !leadName.isEmpty {
            lblName.text = leadName
        } else {
            lblName.text = "\(leadDetail.firstName ?? "") \(leadDetail.middleName ?? "") \(leadDetail.lastName ?? "")"
        }
        lblPhone.text = leadDetail.primaryPhone ?? leadDetail.cellPhone1 ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = leadDetail.primaryEmail ?? ""
        viewPhone.isHidden =  lblPhone.text == "" ||  lblPhone.text == nil ? true : false
        viewEmail.isHidden =  lblEmail.text == "" ||  lblEmail.text == nil ? true : false
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(state.name ?? ""), \(leadDetail.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            lblLeadAddress?.text = address
        } else {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(leadDetail.zipcode ?? "")"
            lblLeadAddress?.text = address
        }
        if let area = leadDetail.area , !area.isEmpty, area != "0" {
            lblArea.text = "\(area) Sq. Feet"
            lblArea.isHidden = false
        } else {
            lblArea.isHidden = true
            lblArea.text = ""
        }
        if let noofStory = leadDetail.noofStory , !noofStory.isEmpty, noofStory != "0" {
            lblNoOfFloor.text = noofStory == "1" ? "\(noofStory) Floor" : "\(noofStory) Floors"
            lblNoOfFloor.isHidden = false
        } else {
            lblNoOfFloor.text = ""
            lblNoOfFloor.isHidden = true
        }
        lblVisitStatus.text = leadDetail.visitStatusName
        var type = "\(leadDetail.flowType ?? "")"
        if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty , noofBedroom != "0", noofBathroom != "0"{
            type.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
        } else {
            if let noofBedroom = leadDetail.noofBedroom , !noofBedroom.isEmpty, noofBedroom != "0" {
                type.append(" \(noofBedroom) Bed")
            }
            if let noofBathroom = leadDetail.noofBathroom , !noofBathroom.isEmpty, noofBathroom != "0" {
                type.append(" \(noofBathroom) Bath.")
            }
        }
        lblType.text = type.trimmed
        
        if let image = self.propertyDetails?.propertyInfo?.imageManual {
            imgView.image = image
        } else {
            guard let urlString = leadDetail.addressImagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
                return
            }
            imgView.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
        }
        collectionView.reloadData()
    }
    private func setupDataForOpportunity(_ opportunity: Opportunity) {
        btnAddVisit.isHidden = true
        var propertyInfo = PropertyInfo()
        viewLeadStatus.isHidden = false
        lblStatusTitle.text = "Opportunity Status:"
        if  let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == opportunity.opportunityStatus}).first {
            if let date = opportunity.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblStatus.text = (leadsStatus.name ?? "") + " - " + statusDate
            } else {
                lblStatus.text = leadsStatus.name
            }
        }
        lblName.text = "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")"
        propertyInfo.name = lblName.text?.trimmed
        propertyInfo.firstName = opportunity.firstName
        propertyInfo.lastName =  opportunity.lastName
        propertyInfo.phone =  opportunity.primaryPhone
        propertyInfo.email =  opportunity.primaryEmail
        lblPhone.text = opportunity.primaryPhone ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = opportunity.primaryEmail ?? ""
        lblVisitStatus.text = ""
        if let serviceAddress = opportunity.serviceAddress {
            let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
            lblLeadAddress?.text = address
            propertyInfo.zipCode =  serviceAddress.zipcode
            propertyInfo.city =  serviceAddress.cityName
            propertyInfo.address =  address
            if let area = serviceAddress.area , !area.isEmpty, area != "0" {
                lblArea.text = "\(area) Sq. Feet"
                propertyInfo.lotSizeSqFt =  area
                propertyInfo.area =  serviceAddress.area
            } else {
                lblArea.text = ""
            }
            if let noofStory = serviceAddress.noofStory , !noofStory.isEmpty , noofStory != "0" {
                lblNoOfFloor.text = noofStory == "1" ? "\(noofStory) Floor" : "\(noofStory) Floors"
                propertyInfo.noOfFloor =  serviceAddress.noofStory
            } else {
                lblNoOfFloor.text = ""
            }
            if propertyInfo.type != nil {
                lblType.text =  propertyInfo.type
            } else {
                var typeInfo = "\(opportunity.opportunityType ?? "")"
                if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty, noofBedroom != "0", let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty, noofBathroom != "0" {
                    typeInfo.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
                    propertyInfo.bedRoom =  noofBedroom
                    propertyInfo.bathRoom =  noofBathroom
                } else {
                    if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty, noofBedroom != "0" {
                        typeInfo.append(" \(noofBedroom) Bed")
                        propertyInfo.bedRoom =  noofBedroom
                        
                    }
                    if let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty, noofBathroom != "0" {
                        typeInfo.append(" \(noofBathroom) Bath.")
                        propertyInfo.bathRoom =  noofBathroom
                    }
                }
                lblType.text =  typeInfo.trimmed
                propertyInfo.type = typeInfo.trimmed
            }
            if  let urlString = serviceAddress.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                imgView.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
                
                propertyInfo.imageUrl = serviceAddress.imagePath
            }
            
        }
        propertyDetails?.propertyInfo = propertyInfo
    }
    private func setupDataForAccount(_ accountDetail: Account) {
        btnAddVisit.isHidden = true
        var propertyInfo = PropertyInfo()
        viewLeadStatus.isHidden = true
        lblName.text = "\(accountDetail.firstName ?? "") \(accountDetail.middleName ?? "") \(accountDetail.lastName ?? "")"
        propertyInfo.name = lblName.text?.trimmed
        propertyInfo.firstName = accountDetail.firstName
        propertyInfo.lastName =  accountDetail.lastName
        propertyInfo.phone =  accountDetail.primaryPhone
        propertyInfo.email =  accountDetail.primaryEmail
        lblPhone.text = accountDetail.primaryPhone ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = accountDetail.primaryEmail ?? ""
        lblVisitStatus.text = ""
        if let date = accountDetail.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
            lblStatus.text =  statusDate
        }
        if let serviceAddress = accountDetail.serviceAddress {
            let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
            propertyInfo.zipCode =  serviceAddress.zipcode
            propertyInfo.city =  serviceAddress.cityName
            propertyInfo.address =  address
            lblLeadAddress?.text = address
            if let area = serviceAddress.area , !area.isEmpty, area != "0" {
                lblArea.text = "\(area) Sq. Feet"
                propertyInfo.lotSizeSqFt =  area
                propertyInfo.area =  serviceAddress.area
            } else {
                lblArea.text = ""
            }
            if let noofStory = serviceAddress.noofStory , !noofStory.isEmpty, noofStory != "0" {
                lblNoOfFloor.text = noofStory == "1" ? "\(noofStory) Floor" : "\(noofStory) Floors"
                propertyInfo.noOfFloor =  serviceAddress.noofStory
            } else{
                lblNoOfFloor.text = ""
            }
            if propertyInfo.type != nil {
                lblType.text =  propertyInfo.type
            } else {
                var typeInfo = ""
                if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty , noofBedroom != "0" , noofBathroom != "0"{
                    typeInfo.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
                    propertyInfo.bedRoom =  noofBedroom
                    propertyInfo.bathRoom =  noofBathroom
                } else {
                    if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty, noofBedroom != "0" {
                        typeInfo.append(" \(noofBedroom) Bed")
                        propertyInfo.bedRoom =  noofBedroom
                    }
                    if let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty, noofBathroom != "0" {
                        typeInfo.append(" \(noofBathroom) Bath.")
                        propertyInfo.bathRoom =  noofBathroom
                    }
                }
                lblType.text =  typeInfo.trimmed
                propertyInfo.type = typeInfo.trimmed
            }
            if  let urlString = serviceAddress.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                imgView.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
                
                propertyInfo.imageUrl = serviceAddress.imagePath
            }
        }
        propertyDetails?.propertyInfo = propertyInfo
    }
    private func setupDataForNewProperty(_ propertyDetails: PropertyDetails) {
        btnAddVisit.isHidden = !isEditable
        viewLeadStatus.isHidden = false
        guard var propertyInfo =  propertyDetails.propertyInfo else { return }
        
        lblVisitStatus.text = ""
        lblName.text = propertyInfo.name
        lblLeadAddress.text = propertyInfo.address
        lblEmail.text = propertyInfo.email ?? ""
        lblPhone.text = propertyInfo.phone ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblNoOfFloor.text = propertyInfo.noOfFloor ?? ""
        lblStatus.text = "Grabbed"
        if let noofStory = propertyInfo.noOfFloor , !noofStory.isEmpty, noofStory != "0" {
            lblNoOfFloor.text = noofStory == "1" ? "\(noofStory) Floor" : "\(noofStory) Floors"
            lblNoOfFloor.isHidden = false
        } else {
            lblNoOfFloor.isHidden = true
        }
        if let finishedSqFt = propertyInfo.area, finishedSqFt != "0" {
            lblArea.text = "\(finishedSqFt) Sq. Feet"
        }
        if propertyInfo.type != nil {
            lblType.text =  propertyInfo.type
        } else {
            var typeInfo = "\(propertyInfo.flowType ?? "")"
            if let noofBedroom = propertyInfo.bedRoom , !noofBedroom.isEmpty , let noofBathroom = propertyInfo.bathRoom , !noofBathroom.isEmpty {
                if noofBedroom != "0" {
                    typeInfo.append(" \(noofBedroom) Bed" )
                }
                if noofBathroom != "0" {
                    typeInfo.append(" \(noofBathroom) Bath." )
                }
            } else {
                if let noofBedroom = propertyInfo.bedRoom , !noofBedroom.isEmpty {
                    if noofBedroom != "0" {
                        typeInfo.append(" \(noofBedroom) Bed")
                    }
                }
                if let noofBathroom =  propertyInfo.bathRoom , !noofBathroom.isEmpty {
                    if noofBathroom != "0"  {
                        typeInfo.append(" \(noofBathroom) Bath.")
                    }
                }
            }
            lblType.text =  typeInfo.trimmed
            propertyInfo.type = typeInfo
        }
        
        if let image = self.propertyDetails?.propertyInfo?.imageManual {
            imgView.image = image
        } else {
            if let urlString = propertyInfo.imageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString)  {
                imgView.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
                
            }
            
        }
        self.propertyDetails?.propertyInfo = propertyInfo
    }
    private func setupCollectionViewDefaultPosition() {
        guard let propertyDetails = propertyDetails else {
            return
        }
        if propertyDetails.propertyType == .lead || propertyDetails.propertyType == .empty {
            
            guard let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus else {
                return
            }
            if propertyDetails.propertyType == .lead {
                if let index = leadsStatus.firstIndex(where: { (status) -> Bool in
                    return status.sysName == propertyDetails.lead?.statusName
                }) {
                    self.collectionView.scrollToItem(at:IndexPath(item: index, section: 0), at: .right, animated: false)
                }
            } else {
                if let index = leadsStatus.firstIndex(where: { (status) -> Bool in
                    return status.sysName == "Grabbed"
                }) {
                    self.collectionView.scrollToItem(at:IndexPath(item: index, section: 0), at: .right, animated: false)
                }
            }
        } else if propertyDetails.propertyType == .opportunity {
            guard let opportunitesLeadStatus = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus else {
                return
            }
            if let index = opportunitesLeadStatus.firstIndex(where: { (status) -> Bool in
                return status.sysName == propertyDetails.opportunity?.opportunityStatus
            }) {
                self.collectionView.scrollToItem(at:IndexPath(item: index, section: 0), at: .right, animated: false)
            }
        }
    }
    // MARK: - Action
    @IBAction func actionCall(_ sender: Any) {
        if let number = lblPhone.text?.trimmed {
            actionCall(number: number)
        }
    }
    @IBAction func actionSendMail(_ sender: Any) {
        if let email = lblEmail.text?.trimmed {
            actionEmail(email: email)
        }
    }
    // MARK: - Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionAddVisit(_ sender: Any) {
        if propertyDetails?.propertyType == .empty {
            var rows:[String] = []
            let selectedRow = 0
            guard let status = AppUserDefaults.shared.leadDetailMaster?.visitsStatus, !status.isEmpty else {
                showAlert(message: "No record found !!")
                return  }
            
            for (_,row) in status.enumerated() {
                rows.append(row.visitStatusName ?? "")
            }
            showStringPicker(sender: btnAddVisit, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Service", bundle: nil)
                    guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitLogViewController") as? VisitLogViewController else {
                        return
                    }
                    destination.propertyDetails = self.propertyDetails
                    destination.logType = .visit
                    destination.selectedVisitResult = status[index]
                    destination.delegate = self
                    self.navigationController?.pushViewController(destination, animated: true)
                }
            }
        } else {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Service", bundle: nil)
                guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitLogViewController") as? VisitLogViewController else {
                    return
                }
                destination.propertyDetails = self.propertyDetails
                destination.logType = .visit
                destination.delegate = self
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
    }
    @IBAction func actionGetQuotes(_ sender: Any) {
        guard let propertyDetails = self.propertyDetails else {
            return
        }
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Service", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitServiceDetailViewController") as? VisitServiceDetailViewController else {
                return
            }
            destination.propertyDetails = propertyDetails
            destination.delegate = self
            destination.delegateEdit = self
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    @IBAction func unwindToPropertyDetail(_ sender: UIStoryboardSegue) {
        self.dismiss(animated: false, completion: nil)
    }
}
// MARK: - Collection view Datasource
extension VisitDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
            return AppUserDefaults.shared.leadDetailMaster?.leadsStatus.count ?? 0
        } else if propertyDetails?.propertyType == .opportunity {
            return AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.count ?? 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: VisitStatusCollectionViewCell.self), for: indexPath) as! VisitStatusCollectionViewCell
        
        var color = UIColor.blue
        
        if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
            
            guard let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus else {
                return cell
            }
            let lead = leadsStatus[indexPath.row]
            if let colorCode = lead.colorCode {
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
            }
            cell.lblTitle.text = lead.name
            cell.lblTitle.lineBreakMode = .byWordWrapping
            
            if propertyDetails?.propertyType == .empty, lead.sysName == "Grabbed" {
                
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            } else if lead.sysName == propertyDetails?.lead?.statusName {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            } else {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Unselected.png")
            }
            
        } else if propertyDetails?.propertyType == .opportunity {
            guard let opportunitesLeadStatus = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus else {
                return cell
            }
            let opportunity = opportunitesLeadStatus[indexPath.row]
            if let colorCode = opportunity.colorCode {
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
            }
            cell.lblTitle.text = opportunity.statusName
            cell.lblTitle.lineBreakMode = .byWordWrapping
            
            if opportunity.sysName?.lowercased() == propertyDetails?.opportunity?.opportunityStatus?.lowercased() {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            }else{
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Unselected.png")
            }
        }
        
        AppUtility.imageColor(image: cell.imgViewRadio, color: color)
        return cell
    }
}
extension VisitDetailViewController: EditLeadDelegate {
    func upadatePropertyInfo(_ propertyDetails: PropertyDetails) {
        self.propertyDetails = propertyDetails
        switch propertyDetails.propertyType {
        case .lead:
            guard let lead = propertyDetails.lead else { return }
            NotificationCenter.default.post(name: .didUCreatedLead, object: propertyDetails)
            setupDataFromLead(lead)
        //  self.performSegue(withIdentifier: "UnwindToAgreement", sender: self)
        case .empty,.opportunity:
            self.propertyDetails = propertyDetails
            setData()
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - EditableStatusDelegate
extension VisitDetailViewController: EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?) {
        isEditable = AppUtility.setEditable(entityDetail)
        self.propertyDetails = propertyDetail
    }
}
