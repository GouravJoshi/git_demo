//
//  TaskService.swift
//  DPS
//
//  Created by Vivek Patel on 19/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire
class TaskService {
    
    func createTask(parameters: CreateTask, _ callBack:@escaping (_ object: TaskResponse?,_ error:String?) -> Void) {
        let url = URL.LeadD2D.createTask
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: TaskResponse.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                
                break
            }
        }
        
    }
}
