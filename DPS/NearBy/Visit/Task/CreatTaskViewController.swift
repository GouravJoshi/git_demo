//
//  CreatTaskViewController.swift
//  DPS
//
//  Created by Vivek Patel on 23/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class CreatTaskViewController: BaseViewController {
    // MARK: - Variables
    private lazy var presenter = {
        return TaskPresenter(self)
    }()
    
    var propertyDetails: PropertyDetails?
    
    
    // MARK: - Outlets
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtViewNotes: UITextView!
    @IBOutlet weak var imgCalender: UIImageView!
    @IBOutlet weak var btnDissmiss: UIButton! {
        didSet {
            AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        }
    }
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.imageColor(image: imgCalender, color: .gray)
    }
    // MARK: - Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionCreateTask(_ sender: Any) {
        guard !txtDate.text!.isEmpty else {
            showAlert(message: "Please select date")
            return
        }
        guard !txtViewNotes.text!.isEmpty else {
            showAlert(message: "Please enter note")
            return
        }
        var parameter = CreateTask()
        // parameter.reminderDate = txtDate.text
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        parameter.dueDate = txtDate.text
        parameter.assignedTo = assignedToID
        parameter.description = ""
        parameter.taskName = txtViewNotes.text.trimmed
        parameter.priority = "8"
        parameter.status = "Open"
        parameter.createdBy = assignedToID
        
        if let propertyDetails = propertyDetails {
            if let lead = propertyDetails.lead {
                parameter.webLeadId = lead.leadID
                // parameter.crmCompanyId = lead.crmContactID
            } else if let lead = propertyDetails.opportunity {
                parameter.leadId = lead.opportunityID
                parameter.accountId = lead.accountID
                parameter.crmContactId = lead.crmContactID
                // parameter.crmCompanyId = lead.accountCompany?.crmCompanyID
            } else if let lead = propertyDetails.account {
                parameter.accountId = lead.accountID
                parameter.crmContactId = lead.crmContactID
                // parameter.crmCompanyId = lead.accountCompany?.crmCompanyID
            }
        }
        presenter.apicallToCreateTask(parameter)
    }
}
// MARK: - UITextFieldDelegate
extension CreatTaskViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            showDatePicker(sender: txtDate, selectedDate: Date(), minimumDate: Date(), pickerType: .dateTime) { (selectedDate, _) in
                let format = DateFormatter()
                format.dateFormat = "MM/dd/yyyy HH:mm"
                let formattedDate = format.string(from: selectedDate)
                self.txtDate.text = formattedDate
            }
            return false
        }
        return true
        
    }
}
extension CreatTaskViewController: TaskView {
    func taskCreated(_ output: TaskResponse) {
        if let _ = output.leadTaskId {
            showAlertWithCompletion(message: "Task created successfully") {
                self.dismiss(animated: true) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            showAlertWithCompletion(message: "Unable to create task") {
                self.dismiss(animated: true) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}
