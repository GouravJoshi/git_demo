//
//  TaskModel.swift
//  DPS
//
//  Created by Vivek Patel on 19/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

struct CreateTask : Codable {
    var id: String?
    var refId: String?
    var refType: String?
    var taskName: String?
    var dueDate: String?
    var reminderDate: String?
    var description: String?
    var assignedTo: String?
    var priority: String?
    var endDate: String?
    var status: String?
    var createdBy: String?
    var tags: String?
    var childActivities: String?
    var followUpFromTaskId: String?
    var accountId: Int?
    var leadId: Int?
    var webLeadId: Int?
    var crmContactId: Int?
    var crmCompanyId: Int?
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case refId = "RefId"
        case refType  = "RefType"
        case taskName  = "TaskName"
        case dueDate = "DueDate"
        case reminderDate = "ReminderDate"
        case description = "Description"
        case assignedTo = "AssignedTo"
        case priority = "Priority"
        case endDate = "EndDate"
        case status = "Status"
        case createdBy = "CreatedBy"
        case tags = "Tags"
        case childActivities = "ChildActivities"
        case followUpFromTaskId = "FollowUpFromTaskId"
        case accountId = "AccountId"
        case leadId = "LeadId"
        case webLeadId = "WebLeadId"
        case crmContactId = "CrmContactId"
        case crmCompanyId = "CrmCompanyId"
    }
    init() {}
}

struct TaskResponse : Codable {
    var leadTaskId: Int?
    enum CodingKeys: String, CodingKey {
        case leadTaskId = "LeadTaskId"
    }
    init() {}
}
