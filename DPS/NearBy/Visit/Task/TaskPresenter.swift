//
//  TaskPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 19/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol TaskView: CommonView {
    func taskCreated(_ output: TaskResponse)
}

class TaskPresenter: NSObject {
    weak private var delegate: TaskView?
    private lazy var service = {
        
        return TaskService()
    }()
    
    init(_ delegate: TaskView) {
        self.delegate = delegate
    }
    
    
    func apicallToCreateTask(_ parameters: CreateTask) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        FTIndicator.showProgress(withMessage: "Fetching...", userInteractionEnable: false)
        service.createTask(parameters: parameters) { (object, error) in
            FTIndicator.dismissProgress()
            if let object = object{
                self.delegate?.taskCreated(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}
