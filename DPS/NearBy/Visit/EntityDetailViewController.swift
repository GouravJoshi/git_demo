//
//  EntityDetailViewController.swift
//  DPS
//
//  Created by Vivek Patel on 24/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class EntityDetailViewController: LogActivityBaseViewController {
    // MARK: - Variables
    var delegate: VisitActionDelegate?
    var propertyDetails: PropertyDetails?
    var selectedLocation: CLLocation?
    private var placeDetail: PlaceComponets?
    
    var selectedVisitStatus: VisitStatus? {
        didSet{
            if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
                lblVisitStatus.isHidden = false
                lblVisitStatus.text = selectedVisitStatus?.visitStatusName
            }
        }
        
    }
    private var isEditable = true {
        didSet {
            setData()
        }
    }
    private lazy var presenter = {
        return LogVisitPresenter(self)
    }()
    private lazy var presenterLog = {
        return LogActivityPresenter(self)
    }()
    var isNotificationSend = false
    //MARK: - Outlets
    @IBOutlet weak var lblVisitStatus: UILabel!
    @IBOutlet weak var lblEntityStatus: UILabel!
    @IBOutlet weak var lblLeadStatusTitle: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewVisitStatus: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewPhone: UIStackView!
    @IBOutlet weak var viewEmail: UIStackView!
    @IBOutlet weak var btnLogVisit: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnFollowUP: UIButton!
    @IBOutlet weak var viewLeadStatus: UIView!
    @IBOutlet weak var lblLeadStatus: UILabel!
    @IBOutlet weak var btnLogActivity: UIButton!
    @IBOutlet weak var lblLeadStatusTittle: UILabel!
    @IBOutlet weak var btnSelectSubmit: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnDirection: UIButton!
    @IBOutlet weak var addressImgView: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        //setFooterView()
        setData()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    
    private func setUI() {
        guard let propertyDetails = propertyDetails else { return }
        let attrs = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0, weight: .semibold),
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        var buttonTitleStr = NSMutableAttributedString(string:"Edit", attributes:attrs)
        if propertyDetails.propertyType == .empty {
            buttonTitleStr = NSMutableAttributedString(string:"Edit", attributes:attrs)
        } else if propertyDetails.propertyType == .lead  {
            buttonTitleStr = NSMutableAttributedString(string:"Edit Log", attributes:attrs)
        }
        let attributedString = NSMutableAttributedString(string:"")
        attributedString.append(buttonTitleStr)
        btnLogVisit.setAttributedTitle(attributedString, for: .normal)
        let buttonTitleStrFollow = NSMutableAttributedString(string:"Follow up", attributes:attrs)
        let attributedStringFollow = NSMutableAttributedString(string:"")
        attributedStringFollow.append(buttonTitleStrFollow)
        btnFollowUP.setAttributedTitle(attributedStringFollow, for: .normal)
        
        let buttonTitleStrActivity = NSMutableAttributedString(string:"Log an activity", attributes:attrs)
        let attributedStringActivity = NSMutableAttributedString(string:"")
        attributedStringActivity.append(buttonTitleStrActivity)
        btnLogActivity.setAttributedTitle(attributedStringActivity, for: .normal)
        
        let buttonTitleStrDirection = NSMutableAttributedString(string:"Directions", attributes:attrs)
        let attributedStringDirection = NSMutableAttributedString(string:"")
        attributedStringDirection.append(buttonTitleStrDirection)
        btnDirection.setAttributedTitle(attributedStringDirection, for: .normal)
        
        
        let buttonTitleStrDetails = NSMutableAttributedString(string:"Details", attributes:attrs)
        let attributedStringDetails = NSMutableAttributedString(string:"")
        attributedStringDetails.append(buttonTitleStrDetails)
        btnDetails.setAttributedTitle(attributedStringDetails, for: .normal)
    }
    private func setData() {
        guard let propertyDetails = propertyDetails else { return}
        
        switch propertyDetails.propertyType {
        case .lead:
            if let lead = propertyDetails.lead {
                self.propertyDetails?.propertyInfo = AppUtility.getPropertyDetailFromLead(lead)
                setupDataFromLead(lead)
            }
            
        case .opportunity:
            if let opportunity = propertyDetails.opportunity {
                setupDataForOpportunity(opportunity)
            }
            break
        case .account:
            if let account = propertyDetails.account {
                setupDataForAccount(account)
            }
            break
        case .empty:
            setupDataForNewProperty(propertyDetails)
        }
        
        lblVisitStatus.isHidden =  lblVisitStatus.text == "" ||  lblVisitStatus.text == nil ? true : false
        viewPhone.isHidden =  lblPhone.text == "" ||  lblPhone.text == nil ? true : false
        viewEmail.isHidden =  lblEmail.text == "" ||  lblEmail.text == nil ? true : false
        setupCollectionViewDefaultPosition()
    }
    private func setupDataFromLead(_ leadDetail: Lead) {
        btnSubmit.isHidden = false
        btnDetails.isHidden = false // true
        btnDirection.isHidden = false // true
        viewVisitStatus.isHidden = false
        viewLeadStatus.isHidden = false
        btnLogActivity.isHidden = true
        btnLogVisit.isHidden = false
        lblLeadStatusTitle.text = "Lead:"
        lblLeadStatus.text = propertyDetails?.lead?.statusName
        
        if propertyDetails?.lead?.statusName == "Converted" || propertyDetails?.lead?.statusName == "converted"{
            
            btnDelete.isHidden = true
            
        } else {
            
            btnDelete.isHidden = false
            
        }
        
        if  let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == leadDetail.statusName}).first {
            if let date = leadDetail.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                lblEntityStatus.text = (leadsStatus.name ?? "") + " - " + statusDate
            } else {
                lblEntityStatus.text = leadsStatus.name
            }
        }  else {
            lblEntityStatus.text = ""
        }
        if let leadName = leadDetail.leadName, !leadName.isEmpty {
            lblName.text = leadName
        } else {
            lblName.text = "\(leadDetail.firstName ?? "") \(leadDetail.middleName ?? "") \(leadDetail.lastName ?? "")"
        }
        if let visitsStatus = AppUserDefaults.shared.leadDetailMaster?.visitsStatus {
            if propertyDetails?.propertyType == .lead {
                if let status = visitsStatus.filter({$0.visitStatusId == propertyDetails?.lead?.visitStatusID}).first {
                    selectedVisitStatus = status
                    propertyDetails?.propertyInfo?.visitStaus = status
                    
                }
            }
        }
        lblVisitStatus.text = selectedVisitStatus?.visitStatusName ?? ""
        /*  if let statusName = leadDetail.statusName, let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.first(where: { (status) -> Bool in
         return status.sysName == statusName
         })?.colorCode {
         if let tempColor = UIColor(hex:colorCode) {
         viewLeadStatus.backgroundColor = tempColor
         }
         }*/
        lblPhone.text = leadDetail.primaryPhone ?? leadDetail.cellPhone1 ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = leadDetail.primaryEmail ?? ""
        viewPhone.isHidden =  lblPhone.text == "" ||  lblPhone.text == nil ? true : false
        viewEmail.isHidden =  lblEmail.text == "" ||  lblEmail.text == nil ? true : false
        if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
            return state.stateID == leadDetail.stateID
        }) {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(state.name ?? ""), \(leadDetail.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
            lblAddress?.text = address
        } else {
            let address = "\(leadDetail.address1 ?? ""), \(leadDetail.cityName ?? ""), \(leadDetail.zipcode ?? "")"
            lblAddress?.text = address
        }
        // conndition for null address
        
        if WebService().checkIfBlank(strString: "\(leadDetail.address1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.cityName ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(leadDetail.zipcode ?? "")").count == 0 {
            
            lblAddress?.text = ""

        }
        
        collectionView.reloadData()
    }
    private func setupDataForOpportunity(_ opportunity: Opportunity) {
        
        var propertyInfo = PropertyInfo()
        
        btnDetails.isHidden = false // true
        
        btnDirection.isHidden = false // true
        
        lblName.text = "\(opportunity.firstName ?? "") \(opportunity.middleName ?? "") \(opportunity.lastName ?? "")"
        lblPhone.text = opportunity.primaryPhone
         if(lblPhone.text == ""){
            lblPhone.text = opportunity.cell
         }
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = opportunity.primaryEmail ?? ""
        
        propertyInfo.name = lblName.text?.trimmed
        
        propertyInfo.middleName = opportunity.middleName
        propertyInfo.firstName = opportunity.firstName
        
        propertyInfo.lastName =  opportunity.lastName
        
        propertyInfo.phone =  opportunity.primaryPhone ?? opportunity.cell
        
        propertyInfo.email =  opportunity.primaryEmail
        
        propertyInfo.latitude = opportunity.latitude
        
        propertyInfo.longitude = opportunity.longitude
        
        propertyInfo.flowType = opportunity.opportunityType ?? "Residential"
        
       
        
        if let serviceAddress = opportunity.serviceAddress {
            
            
            
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                
                return state.stateID == serviceAddress.stateID
                
            }) {
                
                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(state.name ?? ""), \(serviceAddress.zipcode ?? "")".trimmed.replacingOccurrences(of: "  ", with: " ")
                
                propertyInfo.state = state
                
                propertyInfo.address =  address
                
                lblAddress.text = address
                
            } else {
                
                let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.zipcode ?? "")"
                
                propertyInfo.address =  address
                
                lblAddress.text = address
                
            }
            
            // conndition for null address
            
            if WebService().checkIfBlank(strString: "\(serviceAddress.address1 ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.cityName ?? "")").count == 0 && WebService().checkIfBlank(strString: "\(serviceAddress.zipcode ?? "")").count == 0 {
                
                lblAddress?.text = ""

            }
            
            propertyInfo.zipCode =  serviceAddress.zipcode
            
            propertyInfo.city =  serviceAddress.cityName
            
            
            
            if let area = serviceAddress.area , !area.isEmpty {
                
                propertyInfo.lotSizeSqFt =  area
                
                propertyInfo.area =  serviceAddress.area
                
            }
            
            if let noofStory = serviceAddress.noofStory , !noofStory.isEmpty {
                
                propertyInfo.noOfFloor =  serviceAddress.noofStory
                
            } else {
                
            }
            
            if propertyInfo.type != nil {
                
            } else {
                
                var typeInfo = "\(opportunity.opportunityType ?? "")"
                
                if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty, let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {
                    
                    typeInfo.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
                    
                    propertyInfo.bedRoom =  noofBedroom
                    
                    propertyInfo.bathRoom =  noofBathroom
                    
                } else {
                    
                    if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty {
                        
                        typeInfo.append(" \(noofBedroom) Bed")
                        
                        propertyInfo.bedRoom =  noofBedroom
                        
                        
                        
                    }
                    
                    if let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {
                        
                        typeInfo.append(" \(noofBathroom) Bath.")
                        
                        propertyInfo.bathRoom =  noofBathroom
                        
                    }
                    
                }
                
                propertyInfo.type = typeInfo.trimmed
                
            }
            
            if  let urlString = serviceAddress.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {
                
                propertyInfo.imageUrl = serviceAddress.imagePath
                
            }
            
        }
        
        
        
        btnSubmit.isHidden = false
        
        viewVisitStatus.isHidden = false
        
        viewLeadStatus.isHidden = false
        
        btnLogActivity.isHidden = false
        
        btnLogVisit.isHidden = true
        
        if  let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({$0.sysName == opportunity.opportunityStatus}).first {
            
            if let date = opportunity.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
                
                lblEntityStatus.text = (leadsStatus.name ?? "") + " - " + statusDate
                
            } else {
                
                lblEntityStatus.text = leadsStatus.name
                
            }
            
        } else {
            
            lblEntityStatus.text = ""
            
        }
        
        lblLeadStatusTitle.text = "Opportunity:"
        btnDelete.isHidden = true

        if let opportunityStatus =  propertyDetails?.opportunity?.opportunityStatus ,  let opportunityStage = propertyDetails?.opportunity?.opportunityStage {
            
            lblLeadStatus.text = opportunityStatus + "/"  + opportunityStage
            
            if opportunityStage == "Won" || opportunityStage == "won" {
                
                btnDelete.isHidden = true
                
            } else {
                
                btnDelete.isHidden = false
                
            }
            
        }
        
        lblVisitStatus.text = opportunity.opportunityLastActivity?.agenda
        
        
        propertyDetails?.propertyInfo = propertyInfo
        
    }
    private func setupDataForAccount(_ accountDetail: Account) {
        btnDelete.isHidden = true
        btnDetails.isHidden = true // false
        btnDirection.isHidden = true // false
        var propertyInfo = PropertyInfo()
        viewVisitStatus.isHidden = false
        btnLogVisit.isHidden = true
        btnLogActivity.isHidden = false
        btnSubmit.isHidden = false
        viewLeadStatus.isHidden = false
        lblName.text = "\(accountDetail.firstName ?? "") \(accountDetail.middleName ?? "") \(accountDetail.lastName ?? "")"
        propertyInfo.name = lblName.text?.trimmed
        propertyInfo.firstName = accountDetail.firstName
        propertyInfo.lastName =  accountDetail.lastName
        propertyInfo.phone =  accountDetail.primaryPhone
        propertyInfo.email =  accountDetail.primaryEmail
        propertyInfo.latitude = accountDetail.latitude
        propertyInfo.longitude = accountDetail.longitude
        propertyInfo.flowType =  "Residential"
        lblPhone.text = accountDetail.primaryPhone ?? ""
        lblPhone.text =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
        lblEmail.text = accountDetail.primaryEmail ?? ""
        if let date = accountDetail.createdDate, let statusDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy hh:mm a") {
            lblEntityStatus.text =  statusDate
        } else {
            lblEntityStatus.text = ""
        }
        if let serviceAddress = accountDetail.serviceAddress {
            let address = "\(serviceAddress.address1 ?? ""), \(serviceAddress.cityName ?? ""), \(serviceAddress.county ?? "") \(serviceAddress.zipcode ?? "")"
            propertyInfo.zipCode =  serviceAddress.zipcode
            propertyInfo.city =  serviceAddress.cityName
            propertyInfo.address =  address
            lblAddress?.text = address
            if let area = serviceAddress.area , !area.isEmpty {
                propertyInfo.lotSizeSqFt =  area
                propertyInfo.area =  serviceAddress.area
            }
            if let noofStory = serviceAddress.noofStory , !noofStory.isEmpty {
                propertyInfo.noOfFloor =  serviceAddress.noofStory
            }
         
                var typeInfo = ""
                if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty , let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {
                    typeInfo.append(" \(noofBedroom) Bed / \(noofBathroom) Bath" )
                    propertyInfo.bedRoom =  noofBedroom
                    propertyInfo.bathRoom =  noofBathroom
                } else {
                    if let noofBedroom = serviceAddress.noofBedroom , !noofBedroom.isEmpty {
                        typeInfo.append(" \(noofBedroom) Bed")
                        propertyInfo.bedRoom =  noofBedroom
                    }
                    if let noofBathroom = serviceAddress.noofBathroom , !noofBathroom.isEmpty {
                        typeInfo.append(" \(noofBathroom) Bath.")
                        propertyInfo.bathRoom =  noofBathroom
                    }
                }
                propertyInfo.type = typeInfo.trimmed
            if  let urlString = serviceAddress.imagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let _ = URL(string: urlString)  {
                
                propertyInfo.imageUrl = serviceAddress.imagePath
            }
        }
        lblLeadStatusTitle.text = "Customer"
        lblLeadStatus.text = ""
        lblVisitStatus.text = accountDetail.accountLastActivity?.agenda
        propertyDetails?.propertyInfo = propertyInfo
    }
    private func setupDataForNewProperty(_ propertyDetails: PropertyDetails) {
        btnSubmit.isHidden = false
        viewVisitStatus.isHidden = false
        viewLeadStatus.isHidden = false
        btnLogActivity.isHidden = true
        btnDetails.isHidden = true
        btnDirection.isHidden = true
        btnLogVisit.isHidden = false
        lblLeadStatusTitle.text = "Lead Status:"
        lblEntityStatus.text = "Grabbed"
        lblLeadStatus.text = "Grabbed"
        btnDelete.isHidden = true

        /*  if let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.first(where: { (status) -> Bool in
         return status.sysName == "Grabbed"
         })?.colorCode {
         if let tempColor = UIColor(hex:colorCode) {
         viewLeadStatus.backgroundColor = tempColor
         }
         }*/
        lblVisitStatus.text = selectedVisitStatus?.visitStatusName ?? ""
        
        guard let propertyInfo =  propertyDetails.propertyInfo else {
            
//            lblName.text    = "UnKnown"
//            lblAddress.text = ""
//            lblEmail.text   = ""
//            lblPhone.text   = ""
//            lblPhone.text   = ""
//            addressImgView.isHidden = true

            return
            
        }
        
        addressImgView.isHidden = false
        lblName.text    = propertyInfo.name
        lblAddress.text = propertyInfo.address
        lblEmail.text   = propertyInfo.email ?? ""
        lblPhone.text   = propertyInfo.phone ?? ""
        lblPhone.text   =  formattedNumber(number:lblPhone.text == nil ? "" : lblPhone.text!)
    }
    private func setupCollectionViewDefaultPosition() {
        guard let propertyDetails = propertyDetails else {
            return
        }
        // if propertyDetails.propertyType == .lead {
        guard let visitStatus = AppUserDefaults.shared.leadDetailMaster?.visitsStatus else {
            return
        }
        if propertyDetails.propertyType == .lead {
            if let index = visitStatus.firstIndex(where: { (status) -> Bool in
                return status.visitStatusId == propertyDetails.lead?.visitStatusID
            }) {
                self.collectionView.scrollToItem(at:IndexPath(item: index, section: 0), at: .right, animated: false)
            }
        }
        // }
    }
    private func getCommonParameters() -> AddLead? {
        var parameter = AddLead()
        guard let propertyInfo = propertyDetails?.propertyInfo else { return nil}
        parameter.firstName = propertyInfo.firstName
        parameter.lastName = propertyInfo.lastName
        parameter.primaryEmail =  propertyInfo.email
        parameter.cellPhone1 =  propertyInfo.phone
        if let address = propertyInfo.address?.components(separatedBy: ",") {
            if let address1 =  address.first?.trimmed {
                parameter.address1 = String(address1.prefix(100))
            }
            var position = 0
            if address.count == 4 {
                position = 0
            } else {
                position = 1
            }
            if address.count >= 2 {
                let city = address[position+1].trimmed
                parameter.cityName = city
            }
            if address.count >= 3 {
                let administrativeArea = address[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
                if let states = AppUtility.fetchStates()  {
                    if let state = states.filter({ (stateT) -> Bool in
                        return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                    }).first  {
                        parameter.stateID = state.stateID
                    } else {
                        showAlert(message: "We are not serving in this state")
                        return nil
                    }
                }
            }
            if address.count >= 4 {
                let zipCode = address[position+3].trimmed
                parameter.zipcode = zipCode
            }
        }
        parameter.noofBedroom = propertyInfo.bedRoom
        parameter.noofBathroom =  propertyInfo.bathRoom
        parameter.noofStory =  propertyInfo.noOfFloor
        parameter.area =  propertyInfo.area
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        parameter.branchSysName = barnchSysName
        
        if let source = AppUserDefaults.shared.leadDetailMaster?.sourceMaster.filter({$0.sourceSysName == "DoorToDoor"}).first {
            parameter.sourceIDS = [source.sourceId]
        }
        let imageUrl = propertyInfo.imageUrl?.components(separatedBy: "\\").last
        parameter.addressImagePath = imageUrl
        return parameter
    }
    private func apiCallToAddNewLead() {
        guard var parameter = getCommonParameters() else { return }
        parameter.visitCount = 1
        parameter.countryID = 1
        parameter.refType = "WebLead"
        parameter.zipcode = propertyDetails?.propertyInfo?.zipCode
        parameter.stateID = propertyDetails?.propertyInfo?.state?.stateID
        parameter.cityName = propertyDetails?.propertyInfo?.city
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        parameter.fieldSalesPerson = assignedToID
        parameter.assignedToID = assignedToID
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        parameter.companyKey = companyKey
        parameter.visitStatusId = selectedVisitStatus?.visitStatusId
        parameter.flowType = propertyDetails?.propertyInfo?.flowType
        parameter.visitDescription = ""
        parameter.createNewAccount = false
        parameter.latitude = selectedLocation!.coordinate.latitude
        parameter.longitude = selectedLocation!.coordinate.longitude
        presenter.apiCallForAddNewLead(parameter)
    }
    func apiCallToEditLead() {
        guard let propertyDetail = propertyDetails else { return }
        guard var parameter = getCommonParameters() else { return }
        switch propertyDetails?.propertyType {
        case .lead:
            guard let lead = propertyDetail.lead else { return }
            parameter.visitCount = lead.visitCount
            parameter.assignedToID = lead.assignedToID?.description
            parameter.crmCompanyID = lead.crmCompany?.companyID
            parameter.companyKey = lead.companyKey
            parameter.countryID = lead.countryID
            parameter.fieldSalesPerson = lead.assignedToID?.description
            parameter.assignedToType = lead.assignedType
            parameter.industryId = lead.industryID
            parameter.crmContactID = lead.crmContactID
            parameter.webLeadId = lead.leadID
            parameter.comment = ""
            parameter.name = lead.leadName
            parameter.visitDescription = lead.visitDescription
            parameter.visitStatusId = selectedVisitStatus?.visitStatusId ?? lead.visitStatusID
            var selectedObjections: [Objection] = []
            if let objections = lead.objectionIds, let defaultObjections = AppUserDefaults.shared.leadDetailMaster?.objections {
                for objection in objections {
                    if let selectedObjection = defaultObjections.filter({$0.objectionMasterID == objection}).first {
                        selectedObjections.append(selectedObjection)
                    }
                }
            }
            var selectedRows: [Int] = []
            for object in selectedObjections {
                if let value = object.objectionMasterID {
                    selectedRows.append(value)
                }
            }
            parameter.objectionIds = selectedRows
            parameter.currentServicesProvider = lead.currentServicesProvider
            parameter.latitude = selectedLocation!.coordinate.latitude
            parameter.longitude = selectedLocation!.coordinate.longitude
            selectedObjections.removeAll()
        default:
            break
        }
        self.presenter.apiCallForEditLead(parameter)
        
    }
    private func deleteLeadWebLead(strType : String, iddd : String)  {
        
        let loaderL = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        self.present(loaderL, animated: false, completion: nil)
        
        let start1 = CFAbsoluteTimeGetCurrent()
        
        let dictLoginDataL = nsud.value(forKey: "LoginDetails") as! NSDictionary
        
        var strURL = ""
        
        if strType == "WebLead" {
            
            strURL = "\(dictLoginDataL.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlD2DDeleteWebLead + iddd
            
        } else {
            
            strURL = "\(dictLoginDataL.value(forKeyPath: "Company.CompanyConfig.SalesProcessModule.ServiceUrl")!)" + UrlD2DDeleteLead + "\(dictLoginDataL.value(forKeyPath: "Company.CompanyKey")!)" + "&leadNo=" + iddd
            
        }
        print("Delete Lead Web Lead API called --- %@",strURL)
        
        WebService.getRequestWithHeadersNew(dictJson: NSDictionary(), url: strURL, responseStringComing: "DeleteLeadWebLead") { (response, status) in

            loaderL.dismiss(animated: false) {
                
                if(status)
                {
                    
                    let diff1 = CFAbsoluteTimeGetCurrent() - start1
                    print("Took \(diff1) seconds to get Lead Web Lead API Data From SQL Server.")
                    
                    print("Lead Web Lead API Response")
                    
                    let arrKeys = response.allKeys as NSArray
                    
                    if arrKeys.contains("data") {
                        
                        if response.value(forKey: "data") is NSString {

                            let responseL = "\(response.value(forKey: "data")!)"
                            
                            if responseL == "Success" {
                                
                                //showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "Deleted Successfully", viewcontrol: self)
                                
                                let alert = UIAlertController(title: alertInfo, message: "Deleted Successfully", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction (title: "Ok", style: .default, handler: { (nil) in

                                   // Success Redirect
                                    self.dismiss(animated: true) {
                                        NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
                                    }
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            } else {
                                
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func actionDirection(_ sender: Any)
    {
        //                Global().redirect(onAppleMap: self, strAddress)
        
        guard let propertyDetails = propertyDetails else { return}
        
        switch propertyDetails.propertyType {
        case .lead:
            if let lead = propertyDetails.lead
            {
                guard let lat = lead.latitude else { return}
                
                //Global().redirectOnAppleMap(forD2d: self, lat: "\(lead.latitude!)", long: "\(lead.longitude!)", address: "\(lblAddress.text!)")
                getDirectionMap(strTitle: "\(lblName.text!)", strlat:  "\(lead.latitude!)", strLong: "\(lead.longitude!)")
                
            }
            break
            
        case .opportunity:
            if let opportunity = propertyDetails.opportunity
            {
                guard let lat = opportunity.latitude else { return}

                //Global().redirectOnAppleMap(forD2d: self, lat: "\(opportunity.latitude!)", long: "\(opportunity.longitude!)", address: "\(lblAddress.text!)")
                getDirectionMap(strTitle: "\(lblName.text!)", strlat:  "\(opportunity.latitude!)", strLong: "\(opportunity.longitude!)")


            }
            break
        default:
            break
        }

    }
    @IBAction func actionDetails(_ sender: Any)
    {
       /* let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "CustomerDetailD2DViewController") as? CustomerDetailD2DViewController else {
            return
        }
        destination.propertyDetails = propertyDetails
        self.navigationController?.pushViewController(destination, animated: true)*/
        

        guard let propertyDetails = propertyDetails else { return}
        
        switch propertyDetails.propertyType {
        case .lead:
            if let lead = propertyDetails.lead
            {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebLeadDetailsVC_CRMNew_iPhone") as! WebLeadDetailsVC_CRMNew_iPhone
                
                vc.strReftype = "weblead"
                
                vc.strRefIdNew = "\(lead.leadID!)"
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            break
            
        case .opportunity:
            if let opportunity = propertyDetails.opportunity
            {
                let vc = UIStoryboard.init(name: DeviceType.IS_IPAD ? "CRM_New_iPad" : "CRM_New_iPhone", bundle: Bundle.main).instantiateViewController(withIdentifier: "OpportunityDetailsVC_CRMNew_iPhone") as! OpportunityDetailsVC_CRMNew_iPhone
                
                vc.strRefIdNew = "\(opportunity.opportunityID!)"
                vc.strReftype = "Opportunity"
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            break
        default:
            break
        }
        
    }
    @IBAction func unwindToPropertyDetail(_ sender: UIStoryboardSegue) {
        self.dismiss(animated: false) {
            if let source = sender.source as? ServiceAgrementViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? ConfigureProposalViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? VisitServiceDetailViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? ServiceSetupViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? ConfirmAppointmentViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? VisitLogViewController {
                self.propertyDetails = source.propertyDetails
            } else  if let source = sender.source as? ServiceReportViewController {
                self.propertyDetails = source.propertyDetails
            } else if let source = sender.source as? SendTextViewController  {
                self.propertyDetails = source.propertyDetails
            }
            if self.isNotificationSend {
                NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
            }
        }
    }
    @IBAction func actionSelectService(_ sender: Any) {
        guard let propertyDetails = self.propertyDetails else { return }
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Service", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitServiceDetailViewController") as? VisitServiceDetailViewController else {
                return
            }
            destination.propertyDetails = propertyDetails
            destination.delegate = self
            destination.offlineDelegate = self
            destination.delegateEdit = self
            destination.selectedLocation = self.selectedLocation
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    @IBAction func actionLogActivity(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "LogActivityViewController") as? LogActivityViewController else { return }
        if propertyDetails?.propertyType == .opportunity {
            destination.selectedOpportunity = propertyDetails?.opportunity
        } else {
            destination.selectedAccount = propertyDetails?.account
        }
        destination.propertyType = propertyDetails?.propertyType
        destination.delegate = self
        destination.selectedVisitStatus = selectedVisitStatus
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    @IBAction func actionSubmit(_ sender: Any) {
        guard let propertyDetails = self.propertyDetails else { return }
        if propertyDetails.propertyType == .empty {
            apiCallToAddNewLead()
        } else if propertyDetails.propertyType == .lead {
            apiCallToEditLead()
        } else if propertyDetails.propertyType == .opportunity {
            if let opportunity = propertyDetails.opportunity {
                var parameters = getParameters()
                parameters.accountID = opportunity.accountID?.description
                let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
                // let createdBy =  dictLoginData.value(forKey: "CreatedBy") as! String
                parameters.participantsList = arrOfParticipantsList
                parameters.createdDate = opportunity.createdDate
                parameters.crmContactID = opportunity.crmContactID?.description
                // parameters.createdBy = createdBy
                // parameters.crmCompanyID = selectedOpportunity?.compa
                //parameters.webLeadID =  selectedOpportunity?.
                parameters.leadID =  opportunity.opportunityID?.description
                parameters.agenda = selectedVisitStatus?.visitStatusName
                presenterLog.apiCallToLogActivity(parameters)
            }
        } else if let account = propertyDetails.account {
            var parameters = getParameters()
            parameters.accountID = account.accountID?.description
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
            // let createdBy =  dictLoginData.value(forKey: "CreatedBy") as! String
            parameters.participantsList = arrOfParticipantsList
            parameters.createdDate = account.createdDate
            parameters.crmContactID = account.crmContactID?.description
            // parameters.createdBy = createdBy
            // parameters.crmCompanyID = selectedOpportunity?.compa
            //parameters.webLeadID =  selectedOpportunity?.
            //parameters.leadID =  account.accountID?.description
            parameters.agenda = selectedVisitStatus?.visitStatusName
            presenterLog.apiCallToLogActivity(parameters)
        }
    }
    @IBAction func actionLogVisit(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Service", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitLogViewController") as? VisitLogViewController else {
                return
            }
            destination.propertyDetails = self.propertyDetails
            destination.logType = .visit
            destination.selectedVisitResult = self.selectedVisitStatus
            destination.delegate = self
            destination.selectedLocation = self.selectedLocation

            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    @IBAction func actionDelete(_ sender: Any) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: alertMessage, message: "Are you sure want to delete", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Delete", style: .destructive, handler: { (nil) in

                //Delete Lead OR Opportunity.
                
                if self.propertyDetails?.propertyType == .lead{

                    guard let leadL = self.propertyDetails?.lead else { return }
                    
                    self.deleteLeadWebLead(strType: "WebLead", iddd: "\(leadL.leadID ?? 0)")

                }else if self.propertyDetails?.propertyType == .opportunity{
                    
                    guard let oppL = self.propertyDetails?.opportunity else { return }
                    
                    self.deleteLeadWebLead(strType: "Lead", iddd: "\(oppL.opportunityNumber ?? "")")

                }
                
            }))
            alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in

                //Cancel and stay on samme pasge
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func actionFollowup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "CreatTaskViewController") as? CreatTaskViewController {
            destination.propertyDetails = propertyDetails
            self.present(destination, animated: true, completion: nil)
        }
    }
    // MARK: - Action
    @IBAction func actionCall(_ sender: Any) {
        if let number = lblPhone.text?.trimmed {
            actionCall(number: number)
        }
    }
    @IBAction func actionSendMail(_ sender: Any) {
        if let email = lblEmail.text?.trimmed {
            actionEmail(email: email)
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any) {
        if isNotificationSend {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
// MARK: - Collection view Datasource
extension EntityDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AppUserDefaults.shared.leadDetailMaster?.visitsStatus.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var color:String?
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: VisitStatusCollectionViewCell.self), for: indexPath) as! VisitStatusCollectionViewCell
        guard let visitsStatus = AppUserDefaults.shared.leadDetailMaster?.visitsStatus else {
            return cell
        }
        let lead = visitsStatus[indexPath.row]
        cell.lblTitle.text = lead.visitStatusName
        cell.lblTitle.lineBreakMode = .byWordWrapping
        
        if lead == selectedVisitStatus {
            cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
        } else {
            cell.imgViewRadio.image = UIImage(named: "RadioButton-Unselected.png")
        }
        if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
            if let colorCode = lead.colorCode {
                color = colorCode
            }
        } else if propertyDetails?.propertyType == .opportunity {
            if let sysName = propertyDetails?.opportunity?.opportunityStage, let colorCode = AppUserDefaults.shared.leadDetailMaster?.leadStageMasters.first(where: { (status) -> Bool in
                return status.sysName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                color = colorCode
            } else if let sysName =  propertyDetails?.opportunity?.opportunityStatus, let colorCode = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.first(where: { (status) -> Bool in
                return status.statusName?.lowercased() == sysName.lowercased()
            })?.colorCode {
                color = colorCode
            }
        }
        if let color = color {
            if let tempColor = UIColor(hex:color) {
                AppUtility.imageColor(image: cell.imgViewRadio, color: tempColor)
            }
        }
        return cell
    }
}
// MARK: - UICollectionViewDelegate
extension EntityDetailViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedVisitStatus = AppUserDefaults.shared.leadDetailMaster?.visitsStatus[indexPath.row]
        collectionView.reloadData()
    }
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     let text = ScheduleTabs(rawValue: indexPath.row)?.title ?? ""
     let cellWidth = text.widthOfString(usingFont: UIFont.systemFont(ofSize: 14)) + 15
     return CGSize(width: (cellWidth + 20), height: collectionView.frame.height)
     }*/
}
// MARK: - EditLeadDelegate
extension EntityDetailViewController: EditLeadDelegate {
    func upadatePropertyInfo(_ propertyDetails: PropertyDetails) {
        self.propertyDetails = propertyDetails
        setUI()
        switch propertyDetails.propertyType {
        case .lead:
            guard let lead = propertyDetails.lead else { return }
            setupDataFromLead(lead)
            NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
        case .empty,.opportunity:
            self.propertyDetails = propertyDetails
            setData()
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
            }
        default:
            break
        }
    }
}
// MARK: - EditableStatusDelegate
extension EntityDetailViewController: EditableStatusDelegate {
    func updateEditStatus(_ entityDetail: EntityDetail?, propertyDetail: PropertyDetails?) {
        isEditable = AppUtility.setEditable(entityDetail)
        self.propertyDetails = propertyDetail
        isNotificationSend = true
        
    }
}
extension EntityDetailViewController: LogVisitView {
    func uploadImage(uploadResponse: UploadResponse) { }
    func leadAddedSuccessFully(_ object: Lead) {
        self.propertyDetails?.propertyType = .lead
        self.propertyDetails?.lead = object
        //setData()
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
        }
    }
    
    func leadEditedSuccessfully(_ object: Lead) {
        self.propertyDetails?.lead = object
        setData()
        setUI()
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
        }
    }
}
extension EntityDetailViewController: OfllinePropertyInforUpdate {
    func upadateOflinePropertyInfo(_ propertyDetails: PropertyDetails) {
        self.propertyDetails = propertyDetails
        setUI()
        switch propertyDetails.propertyType {
        case .lead:
            guard let lead = propertyDetails.lead else { return }
            setupDataFromLead(lead)
        case .empty,.opportunity:
            self.propertyDetails = propertyDetails
            setData()
        default:
            break
        }
    }
}

//MARK: - LogActivityView
extension EntityDetailViewController: LogActivityView {
    func loggedSuccessfully(_ output: LogActivityResponse) {
        showAlertWithCompletion(message: "Activity Added successfully") {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
            }
        }
    }
}
//MARK: - ActivityDelegate
extension EntityDetailViewController: ActivityDelegate {
    func  activityUpdated(_ activityStatus:String) {
        /*if propertyDetails?.propertyType == .opportunity {
         self.propertyDetails?.opportunity?.opportunityLastActivity?.agenda = activityStatus
         }  else {
         self.propertyDetails?.account?.accountLastActivity?.agenda = activityStatus
         }
         setData()*/
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: .didUCreatedLead, object: self.propertyDetails)
        }
    }
}
