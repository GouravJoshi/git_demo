//
//  VisitLogViewController.swift
//  DPS
//
//  Created by Vivek Patel on 12/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import Photos
import DropDown
import GooglePlaces
import Alamofire

enum LogType: Int, CaseIterable {
    case visit = 0
    case edit
    var title: String {
        switch self {
        case .visit:
            return  "Visit Log"
        case .edit:
            return  "Edit Leads Detail"
        }
    }
}

protocol EditLeadDelegate {
    func upadatePropertyInfo(_ propertyDetails: PropertyDetails)
}

protocol OfllinePropertyInforUpdate {
    func upadateOflinePropertyInfo(_ propertyDetails: PropertyDetails)
}

final class VisitLogViewController: BaseViewController {
    
    private lazy var mapPresenter = {
        return MapPresenter(self)
    }()
    var selectedLocation: CLLocation?
//    var fullAddress = ""
//    var address     = ""
//    var cityState   = ""
//    var location: CLLocation?
//    var placeMark: CLPlacemark?
    
    private let dispatchGroup = DispatchGroup()
    private var mapDetail: MapResult?
    private var placeDetail: PlaceComponets?
    private lazy var states: [State] = []

    // MARK: Constants
    enum LocationType: Int, CaseIterable {
        case residential = 0
        case commercial
        var title: String {
            switch self {
            case .residential:
                return "Residential"
            case .commercial:
                return "Commercial"
            }
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var segmentType: UISegmentedControl! {
        didSet {
            segmentType.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            segmentType.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)
        }
    }
    @IBOutlet weak var stepperVisitCount: UIStepper!
    @IBOutlet weak var btnDeleteVisitCount: UIButton!
    @IBOutlet weak var btnAddVisitCount: UIButton!
    @IBOutlet weak var lblVisitCount: UILabel!
    @IBOutlet weak var txtServiceProvider: UITextField!
    @IBOutlet weak var txtObjection: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewVisitDetails: UIStackView!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgLastName: UIImageView!
    @IBOutlet weak var imgFirstName: UIImageView!
    @IBOutlet weak var viewStatus: UIStackView!
    @IBOutlet weak var btnCreateTask: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtResultType: UITextField!
    @IBOutlet weak var txtImageName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var btnUploadImage: UIButton!
    @IBOutlet weak var imgVisitResult: UIImageView!
    @IBOutlet weak var viewVisitResult: UIView!
    @IBOutlet weak var viewUploadImage: UIView!
    @IBOutlet weak var txtBathRoom: UITextField!
    @IBOutlet weak var txtBedRoom: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtSize: UITextField!
    @IBOutlet weak var txtNoOfFloor: UITextField!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var btnAddress: UIButton!
    // MARK: - Variables
    @IBOutlet weak var btnAddAddress: UIButton!
    var propertyDetails: PropertyDetails?
    private var propertyImage: UIImage?
    private var selectedStatus: WebLeadStatus?
    private var addressImageUrl:String?
    private var locationType: LocationType = .residential
    var logType:LogType = .visit
    var delegate: EditLeadDelegate?
    var ofllineDelegate: OfllinePropertyInforUpdate?
    var selectedVisitResult: VisitStatus?
    private var visitCount = 1
    private var selectedObjectionId: [Int] = []
    var imgPoweredBy = UIImageView()
    private lazy var presenter = {
        return LogVisitPresenter(self)
    }()
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    private var places: [GMSAutocompletePrediction] = [] {
        didSet { openAddressDropDown(self.txtAddress) }
    }
    private var placeDetails = [PlaceDetails]()
    private var placeType: PlaceType = .all
    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var radius: Double = 0.0
    private var strictBounds: Bool = false
    private var selectedObjections: [Objection] = []
    let dropDown = DropDown()
    var loader = UIAlertController()

    var isChangePrameter = Bool()
    var strAreaOld = ""
    var strNoOfBedroom = ""
    var strNoOfBathroom = ""
    var strNoOfFloor = ""
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isChangePrameter = false
        states = AppUtility.fetchStates() ?? []

        setUI()
        setData()
        setFooterView()
        DropDown.startListeningToKeyboard()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        guard let propertyDetails = self.propertyDetails else { return }
         if logType == .visit {
            if propertyDetails.propertyType == .empty {
              // self.callGetPropertyInfoAPI()

            } else {
                self.title = "Edit Leads Detail"
            }
            self.viewVisitDetails.isHidden = false
            btnCreateTask.isHidden = false
        } else {
            self.title = "Update"
            self.viewVisitDetails.isHidden = true
            btnCreateTask.isHidden = true
        }
        self.navigationController?.isNavigationBarHidden = false
    }
    // MARK: Function to set UI
    private func setUI() {
        txtFirstName.setLeftPaddingPoints(30)
        txtLastName.setLeftPaddingPoints(30)
        txtEmail.setLeftPaddingPoints(30)
        txtPhone.setLeftPaddingPoints(30)
        AppUtility.imageColor(image: imgFirstName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgLastName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgEmail, color: .appDarkGreen)
        AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
        AppUtility.imageColor(image: imgDropDown, color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnAddress, image: "Group 771", color: .gray)
        AppUtility.buttonImageColor(btn: btnAddAddress, image: "add_icon_2" , color: .appThemeColor)
    }
    
    private func setData() {
        guard let propertyDetails = self.propertyDetails,
              let propertyInfo = propertyDetails.propertyInfo else { return }
         if logType == .visit {
            if propertyDetails.propertyType == .empty {
                self.title = "Visit Log"
            } else {
                self.title = "Edit Leads Detail"
            }
            self.viewVisitDetails.isHidden = false
            btnCreateTask.isHidden = false
        } else {
            self.title = "Update"
            self.viewVisitDetails.isHidden = true
            btnCreateTask.isHidden = true
        }
        
        switch propertyDetails.propertyType {
        case .lead:
            if let visitCount = propertyDetails.lead?.visitCount {
                lblVisitCount.text = visitCount.description
                self.visitCount = visitCount
                stepperVisitCount.value = Double(visitCount)
            }
            if let objections = propertyDetails.lead?.objectionIds, let defaultObjections = AppUserDefaults.shared.leadDetailMaster?.objections {
                
                for objection in objections {
                    if let selectedObjection = defaultObjections.filter({$0.objectionMasterID == objection}).first {
                        selectedObjections.append(selectedObjection)
                        var selectedRows:[String] = []
                        for object in selectedObjections {
                            let value = object.objection ?? ""
                            selectedRows.append(value)
                        }
                        self.txtObjection.text = selectedRows.joined(separator: ",")
                    }
                }
            }
            
            txtServiceProvider.text = propertyDetails.lead?.currentServicesProvider
            viewStatus.isHidden = false
            txtViewDescription.text = propertyDetails.lead?.visitDescription
            if let selectedVisitResult = selectedVisitResult {
                txtResultType.text = selectedVisitResult.visitStatusName
            } else {
                if let visitStates = AppUserDefaults.shared.leadDetailMaster?.visitsStatus  {
                    if let status = visitStates.filter({$0.visitStatusId == propertyDetails.lead?.visitStatusID}).first {
                        txtResultType.text = status.visitStatusName
                        selectedVisitResult = status
                    } else {
                        txtResultType.text = "How was the visit"
                    }
                    
                } else {
                    txtResultType.text = "No Status Found"
                }
                
            }
            if let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus , let status = leadsStatus.filter({$0.sysName == propertyDetails.lead?.statusName}).first {
                txtStatus.text = status.name
                selectedStatus = status
            } else {
                txtStatus.text = "No record Found"
            }
        case .empty:
            stepperVisitCount.value = 1
            lblVisitCount.text = "1"
            txtStatus.text = "Grabbed"
            viewStatus.isHidden = false
            if logType == .visit {
                if let selectedVisitResult = selectedVisitResult {
                    txtResultType.text = selectedVisitResult.visitStatusName
                } else {
                    if let visitStatus = AppUserDefaults.shared.leadDetailMaster?.visitsStatus , let status =  visitStatus.first  {
                        self.txtResultType.text = status.visitStatusName
                        self.selectedVisitResult = status
                    } else {
                        self.txtResultType.text = "No Status Found"
                    }
                }
            }
        default:
            break
        }
        
        txtFirstName.text =  String(propertyInfo.firstName?.prefix(50) ?? "")
        txtLastName.text = String(propertyInfo.lastName?.prefix(50) ?? "")
        txtPhone.text = propertyInfo.phone
        if let number = propertyInfo.phone
        {
            txtPhone.text = formattedNumber(number: number)
        }
        txtEmail.text = propertyInfo.email
        txtBedRoom.text = propertyInfo.bedRoom
        txtBathRoom.text =  propertyInfo.bathRoom
        txtSize.text = propertyInfo.area
        txtNoOfFloor.text = propertyInfo.noOfFloor
        txtAddress.text = propertyInfo.address
        if propertyInfo.flowType == "Residential" {
            segmentType.selectedSegmentIndex = 0
            locationType = .residential
        } else if propertyInfo.flowType == "Commercial" {
            segmentType.selectedSegmentIndex = 1
            locationType = .commercial
        }
        propertyImage = propertyInfo.imageManual
        let imageUrl = propertyInfo.imageUrl?.components(separatedBy: "\\").last
        txtImageName.text = imageUrl
        addressImageUrl = imageUrl
        addressImageUrl = propertyInfo.imageUrl

        strAreaOld = propertyInfo.area ?? ""
        strNoOfBedroom = propertyInfo.bedRoom ?? ""
        strNoOfBathroom = propertyInfo.bathRoom ?? ""
        strNoOfFloor = propertyInfo.noOfFloor ?? ""
    }
    
    
    // MARK: Function To Open Camera
    private func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.allowsEditing = true
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    // MARK: Function To Open Gallery
    private func openGallery() {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
    }
    private func openUploadPhotoAlert() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.view.addSubview(UIView())
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
    private func getCommonParameters() -> AddLead? {
        var parameter = AddLead()
        parameter.firstName = String(txtFirstName.text?.prefix(50) ?? "")
        parameter.lastName = String(txtLastName.text?.prefix(50) ?? "")
        parameter.name = "\(txtFirstName.text?.trimmed ?? "") \(txtLastName.text?.trimmed ?? "")"
        parameter.primaryEmail = String(txtEmail.text?.prefix(50) ?? "")
        parameter.cellPhone1 = String(txtPhone.text?.prefix(50) ?? "")
        let address = txtAddress.text!.components(separatedBy: ",")
        if let address1 =  address.first?.trimmed {
            parameter.address1 = String(address1.prefix(100))
        }
        var position = 0
        if address.count == 4 {
            position = 0
        } else {
            position = 1
        }
        if address.count >= 2 {
            let city = address[position+1].trimmed
            parameter.cityName = city
        }
        if address.count >= 3 {
            let administrativeArea = address[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            if let states = AppUtility.fetchStates()  {
                if let state = states.filter({ (stateT) -> Bool in
                    return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                }).first  {
                    parameter.stateID = state.stateID
                } else {
                    showAlert(message: "We are not serving in this state")
                    return nil
                }
            }
        }
        if address.count >= 4 {
            let zipCode = address[position+3].trimmed
            parameter.zipcode = zipCode
        }
        let bedroom = Int(txtBedRoom.text  ?? "")
        let bathroom = Int(txtBathRoom.text  ?? "")
        let floors = Int(txtNoOfFloor.text  ?? "")
        let area = Double(txtSize.text ?? "")?.clean
        parameter.noofBedroom = bedroom?.description
        parameter.noofBathroom = bathroom?.description
        parameter.noofStory = floors?.description
        parameter.area = area?.description
        parameter.visitDescription = txtViewDescription.text ?? ""
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let barnchSysName = "\(dictLoginData.value(forKey: "EmployeeBranchSysName")!)"
        parameter.branchSysName = barnchSysName
        parameter.flowType = locationType.title
        parameter.visitCount = visitCount
        parameter.currentServicesProvider = txtServiceProvider.text?.trimmed ?? ""
        var selectedRows: [Int] = []
        for object in selectedObjections {
            if let value = object.objectionMasterID {
                selectedRows.append(value)
            }
        }
        parameter.objectionIds = selectedRows
        parameter.latitude = selectedLocation!.coordinate.latitude
        parameter.longitude = selectedLocation!.coordinate.longitude
        selectedObjections.removeAll()
        if let source = AppUserDefaults.shared.leadDetailMaster?.sourceMaster.filter({$0.sourceSysName == "DoorToDoor"}).first {
            parameter.sourceIDS = [source.sourceId]
        }
        return parameter
    }
    private func apiCallToAddNewLead() {
        guard var parameter = getCommonParameters() else { return }
        
        parameter.countryID = 1
        parameter.refType = "WebLead"
        parameter.zipcode = propertyDetails?.propertyInfo?.zipCode
        parameter.stateID = propertyDetails?.propertyInfo?.state?.stateID
        parameter.cityName = propertyDetails?.propertyInfo?.city
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let assignedToID = "\(dictLoginData.value(forKeyPath: "EmployeeId")!)"
        parameter.fieldSalesPerson = assignedToID
        parameter.assignedToID = assignedToID
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        parameter.companyKey = companyKey
        parameter.visitStatusId = selectedVisitResult?.visitStatusId
        parameter.flowType = locationType.title
        parameter.addressImagePath = addressImageUrl
        parameter.createNewAccount = false
        parameter.latitude = selectedLocation!.coordinate.latitude
        parameter.longitude = selectedLocation!.coordinate.longitude
        presenter.apiCallForAddNewLead(parameter)
        
    }
    func apiCallToEditLead() {
        guard let propertyDetail = propertyDetails else { return }
        guard var parameter = getCommonParameters() else { return }
        switch propertyDetails?.propertyType {
        case .lead:
            guard let lead = propertyDetail.lead else { return }
            parameter.assignedToID = lead.assignedToID?.description
            parameter.crmCompanyID = lead.crmCompany?.companyID
            parameter.companyKey = lead.companyKey
            parameter.countryID = lead.countryID
            parameter.fieldSalesPerson = lead.assignedToID?.description
            parameter.assignedToType = lead.assignedType
            parameter.industryId = lead.industryID
            parameter.crmContactID = lead.crmContactID
            parameter.webLeadId = lead.leadID
            parameter.comment = ""
            
            parameter.addressImagePath = addressImageUrl
            // parameter.webLeadService = lead.webLeadServices
            parameter.visitStatusId = selectedVisitResult?.visitStatusId
            parameter.latitude = selectedLocation!.coordinate.latitude
            parameter.longitude = selectedLocation!.coordinate.longitude
        default:
            break
        }
        self.presenter.apiCallForEditLead(parameter)
        
    }
    private func updateDataForNewProperty()
    {
        guard var propertyDetails = propertyDetails else { return }
        
        if propertyDetails.propertyInfo == nil {
            var  propertyInfo = PropertyInfo()
            propertyInfo.firstName = txtFirstName.text?.trimmed
            propertyDetails.propertyInfo = propertyInfo
        }
        
        propertyDetails.propertyInfo?.firstName = txtFirstName.text?.trimmed
        propertyDetails.propertyInfo?.lastName = txtLastName.text?.trimmed
        propertyDetails.propertyInfo?.address = txtAddress.text?.trimmed
        propertyDetails.propertyInfo?.email = txtEmail.text?.trimmed
        propertyDetails.propertyInfo?.phone = txtPhone.text?.trimmed
        propertyDetails.propertyInfo?.area = Double(txtSize.text  ?? "")?.clean.description
        propertyDetails.propertyInfo?.lotSizeSqFt = Double(txtSize.text  ?? "")?.clean.description
        propertyDetails.propertyInfo?.bathRoom =  Int(txtBathRoom.text  ?? "")?.description
        propertyDetails.propertyInfo?.bedRoom = Int(txtBedRoom.text  ?? "")?.description
        propertyDetails.propertyInfo?.noOfFloor = Int(txtNoOfFloor.text  ?? "")?.description
        propertyDetails.propertyInfo?.name = "\(txtFirstName.text?.trimmed ?? "") \(txtLastName.text?.trimmed ?? "")"
        propertyDetails.propertyInfo?.flowType = locationType.title
        var typeInfo = "\(locationType.title)"
        
        if let noofBedroom = Int(txtBedRoom.text  ?? "")?.description , !noofBedroom.isEmpty , let noofBathroom =   Int(txtBathRoom.text  ?? "")?.description  , !noofBathroom.isEmpty {
            typeInfo.append(" \(noofBedroom) Bed" )
            
            typeInfo.append(" \(noofBathroom) Bath." )
        } else {
            if let noofBedroom = Int(txtBedRoom.text  ?? "")?.description , !noofBedroom.isEmpty {
                typeInfo.append(" \(noofBedroom) Bed")
            }
            if let noofBathroom =  Int(txtBathRoom.text  ?? "")?.description  , !noofBathroom.isEmpty {
                typeInfo.append(" \(noofBathroom) Bath.")
            }
        }
        propertyDetails.propertyInfo?.type = typeInfo
        propertyDetails.propertyInfo?.imageUrl = self.addressImageUrl
        propertyDetails.propertyInfo?.imageManual = propertyImage
        self.showAlertWithCompletion(message: "Updated successfully") {
            self.delegate?.upadatePropertyInfo(propertyDetails)
            self.navigationController?.popViewController(animated: true)
        }
    }
    private func updateInfoForLead(_ updatedLead: Lead? = nil) {
        guard var propertyDetails = self.propertyDetails  else { return }
        propertyDetails.propertyType = .lead
        if let updatedLead = updatedLead {
            propertyDetails.lead = updatedLead
            propertyDetails.propertyInfo = AppUtility.getPropertyDetailFromLead(updatedLead)
        } else {
            propertyDetails.lead?.firstName = txtFirstName.text?.trimmed
            propertyDetails.lead?.lastName = txtLastName.text?.trimmed
            let address = txtAddress.text!.components(separatedBy: ",")
            if let address1 =  address.first?.trimmed {
                propertyDetails.lead?.address1 = address1
            }
            propertyDetails.lead?.cityName = "\(address[1])"
            let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(address[2])")
            
            if dictStateDataTemp.count > 0 {
                                
                propertyDetails.lead?.stateID = Int("\(dictStateDataTemp.value(forKey: "StateId")!)")

            }
            propertyDetails.lead?.zipcode = "\(address[3])"
            
            propertyDetails.lead?.primaryEmail = txtEmail.text?.trimmed
            propertyDetails.lead?.primaryPhone = txtPhone.text?.trimmed
            propertyDetails.lead?.area = (Double(txtSize.text?.trimmed ?? "" ) ?? 0).clean.description
            propertyDetails.lead?.noofBathroom =  (Int(txtBathRoom.text?.trimmed ?? "" ) ?? 0).description
            propertyDetails.lead?.noofBedroom = (Int(txtBedRoom.text?.trimmed ?? "" ) ?? 0).description
            propertyDetails.lead?.noofStory = (Int(txtNoOfFloor.text?.trimmed ?? "" ) ?? 0).description
            propertyDetails.lead?.leadName = "\(txtFirstName.text?.trimmed ?? "") \(txtLastName.text?.trimmed ?? "")"
            propertyDetails.lead?.flowType = locationType.title
            propertyDetails.lead?.visitStatusName = selectedVisitResult?.visitStatusName
            propertyDetails.lead?.statusName = selectedStatus?.sysName
            var propertyInfo  = AppUtility.getPropertyDetailFromLead(propertyDetails.lead!)
            propertyInfo.imageManual = propertyImage
            propertyInfo.imageUrl =  self.addressImageUrl
            propertyDetails.propertyInfo = propertyInfo
            self.showAlertWithCompletion(message: "Updated successfully") {
                self.delegate?.upadatePropertyInfo(propertyDetails)
                self.navigationController?.popViewController(animated: true)
            }
        }
        delegate?.upadatePropertyInfo(propertyDetails)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func updateInfoForOpportunity(_ updatedOpportunity: Opportunity? = nil) {
        
        guard var propertyDetails = self.propertyDetails  else { return }
        
        propertyDetails.propertyType = .opportunity
        
        if let updatedopportunity  = updatedOpportunity {
            
            propertyDetails.opportunity = updatedopportunity
            
            propertyDetails.propertyInfo = AppUtility.getPropertyDetailFormOpportunity(updatedopportunity)
            
        } else {
            
            propertyDetails.opportunity?.firstName = txtFirstName.text?.trimmed
            
            propertyDetails.opportunity?.lastName = txtLastName.text?.trimmed
            
            let address = txtAddress.text!.components(separatedBy: ",")
            
            if let address1 =  address.first?.trimmed {
                
                propertyDetails.opportunity?.serviceAddress?.address1 = address1
                
            }
            propertyDetails.opportunity?.serviceAddress?.cityName = "\(address[1])"
            
            let dictStateDataTemp = getStateDataFromStateName(strStateName: "\(address[2])")
            
            if dictStateDataTemp.count > 0 {
                                
                propertyDetails.opportunity?.serviceAddress?.stateID = Int("\(dictStateDataTemp.value(forKey: "StateId")!)")

            }
            
            propertyDetails.opportunity?.serviceAddress?.zipcode = "\(address[3])"
            
            propertyDetails.opportunity?.primaryEmail = txtEmail.text?.trimmed
            
            propertyDetails.opportunity?.primaryPhone = txtPhone.text?.trimmed
            
            
            propertyDetails.opportunity?.serviceAddress?.area = (Double(txtSize.text?.trimmed ?? "" ) ?? 0).clean.description
            
            propertyDetails.opportunity?.serviceAddress?.noofBathroom =  (Int(txtBathRoom.text?.trimmed ?? "" ) ?? 0).description
            
            propertyDetails.opportunity?.serviceAddress?.noofBedroom = (Int(txtBedRoom.text?.trimmed ?? "" ) ?? 0).description
            
            propertyDetails.opportunity?.serviceAddress?.noofStory = (Int(txtNoOfFloor.text?.trimmed ?? "" ) ?? 0).description
            
            
//            propertyDetails.lead?.area = (Double(txtSize.text?.trimmed ?? "" ) ?? 0).clean.description
//            propertyDetails.lead?.noofBathroom =  (Int(txtBathRoom.text?.trimmed ?? "" ) ?? 0).description
//            propertyDetails.lead?.noofBedroom = (Int(txtBedRoom.text?.trimmed ?? "" ) ?? 0).description
//            propertyDetails.lead?.noofStory = (Int(txtNoOfFloor.text?.trimmed ?? "" ) ?? 0).description
            
            propertyDetails.opportunity?.opportunityName = "\(txtFirstName.text?.trimmed ?? "") \(txtLastName.text?.trimmed ?? "")"
            
            propertyDetails.opportunity?.opportunityType = locationType.title
            
            propertyDetails.opportunity?.opportunityStage = selectedVisitResult?.visitStatusName
                       
            propertyDetails.opportunity?.opportunityStatus = selectedStatus?.sysName
            
            var propertyInfo  = AppUtility.getPropertyDetailFormOpportunity(propertyDetails.opportunity!)
            
            
            propertyInfo.imageManual = propertyImage
            
            propertyInfo.imageUrl =  self.addressImageUrl
            
            propertyDetails.propertyInfo = propertyInfo
            
            if(self.view.tag == 99){
                self.delegate?.upadatePropertyInfo(propertyDetails)
                self.navigationController?.popViewController(animated: true)
            }else{
                self.showAlertWithCompletion(message: "Updated successfully") {
                    self.delegate?.upadatePropertyInfo(propertyDetails)
                    self.navigationController?.popViewController(animated: true)
                }
            }
           
            
        }
        
        delegate?.upadatePropertyInfo(propertyDetails)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    private func addressValidation() -> Bool {
        guard !txtAddress.text!.trimmed.isEmpty else {
            showAlert(message: "Please enter Address")
            return false
        }
        if txtAddress.text?.count == 0 {
            showAlert(message: "Please enter Address")
            return false
        }
        guard let address = txtAddress.text?.trimmed.replacingOccurrences(of: "  ", with: "").components(separatedBy:","),  address.count == 4 ||  address.count == 5  else {
            showAlert(message: "Please enter valid Address")
            return false
        }
        guard let states = AppUtility.fetchStates() else {
            showAlert(message: "Unable to find states")
            return false
        }
        var administrativeArea = ""
        if address.count == 4 {
            administrativeArea = address[2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
        } else {
            administrativeArea = address[3].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
        }
        guard let _ = states.filter({ (stateT) -> Bool in
            return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
        }).first else {
            showAlert(message: "We are not serving in this state")
            return false
        }
        if let text = txtEmail.text
        {
            guard txtEmail.text!.isEmpty else {
                let emails = txtEmail.text!.components(separatedBy: ",")
                for email in emails {
                    guard email.trimmed.isValidEmail() else {
                        showAlertWithCompletion(message: "Please enter valid email") {
                            self.txtEmail.becomeFirstResponder()
                        }
                        return false
                    }
                }
                return true
            }
            
        }
       
        return true
        
    }
    
    func callGetPropertyInfoAPI()  {
        
        guard let propertyDetails = self.propertyDetails,
              let propertyInfo = propertyDetails.propertyInfo else { return }
        
        let dictRecord = NSMutableDictionary()
        dictRecord.setValue("\(propertyInfo.address!)", forKey: "AddressLine1")
        dictRecord.setValue("\(propertyInfo.city!)", forKey: "City")
        dictRecord.setValue("\(propertyInfo.state!.stateShortName!)", forKey: "State")
        dictRecord.setValue("\(propertyInfo.zipCode!)", forKey: "PostalCode")
        
//        dictRecord.setValue("18715 Rogers Lk", forKey: "AddressLine1")
//            dictRecord.setValue("San Antonio", forKey: "City")
//            dictRecord.setValue("TX", forKey: "State")
//            dictRecord.setValue("70258", forKey: "PostalCode")
        
        let aryRecord = NSMutableArray()
        aryRecord.add(dictRecord)
        let dictData = NSMutableDictionary()
        
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKeyMellissa = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MelissaKey")!)"

        dictData.setValue(apiKeyMellissa.count == 0 ? MelissaKey : apiKeyMellissa, forKey: "CustomerId")
        dictData.setValue("Property V4 - LookupProperty", forKey: "TransmissionReference")
        dictData.setValue("1", forKey: "TotalRecords")
        dictData.setValue("GrpAll", forKey: "Columns")
        dictData.setValue(aryRecord, forKey: "Records")
        var jsonString = String()

        if(JSONSerialization.isValidJSONObject(dictData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            print(dictData)
        Global().getServerResponseForSendLead(toServer: "https://property.melissadata.net/v4/WEB/LookupProperty/", requestData, (NSDictionary() as! [AnyHashable : Any]), "", "") { (success, response, error) in
            self.loader.dismiss(animated: false) {}
                if(success)
                    {
                    if((response as NSDictionary?)?.value(forKey: "Records") is NSArray){
                        
                    var aryRecord = NSMutableArray()
                        aryRecord = ((response as NSDictionary?)?.value(forKey: "Records") as! NSArray).mutableCopy()as! NSMutableArray
                        
                        if(aryRecord.count != 0){
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") is NSDictionary){
                                var dictRecord = NSMutableDictionary()
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                                self.txtBathRoom.text = "\(dictIntRoomInfo.value(forKey: "BathCount")!)"
                                self.txtBedRoom.text = "\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)"
                                self.txtNoOfFloor.text = "\(dictIntRoomInfo.value(forKey: "StoriesCount")!)"

                            }else{
                                self.txtBathRoom.text = ""
                                self.txtBedRoom.text = ""
                                self.txtNoOfFloor.text = ""

                            }
                            
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "PropertySize") is NSDictionary){
                                var dictRecord = NSMutableDictionary()
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertySize") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                                self.txtSize.text = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                                //self.txtNoOfFloor.text = "\(dictPropertySize.value(forKey: "AreaLotSF")!)"
                            }else{
                                self.txtSize.text = ""
                                //self.txtNoOfFloor.text = ""
                            }
                            
                        }
                    }
                }
                else
                {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
          
            
        
        }
          
    }
    func getUpdateOnParameter()
    {
        if strAreaOld != txtSize.text
        {
            isChangePrameter = true
        }
        if strNoOfBedroom != txtBedRoom.text
        {
            isChangePrameter = true
        }
        if strNoOfBathroom != txtBathRoom.text
        {
            isChangePrameter = true
        }
        if strNoOfFloor != txtNoOfFloor.text
        {
            isChangePrameter = true
        }
    
        nsud.setValue(isChangePrameter, forKey: "ChangeInParameter")
        nsud.synchronize()
    }
    // MARK: - Actions
    
    @IBAction func actionStepper(_ sender: UIStepper) {
        visitCount = Int(sender.value)
        lblVisitCount.text = visitCount.description
    }
    
    @IBAction func actionCancel(_ sender: Any)
    {
       /* self.view.tag = 99  // for alert not show in case of Edit takka hai shi kreng eisko bad me
        
        guard let propertyDetails = propertyDetails else { return }

        switch propertyDetails.propertyType {
        case .empty:
            updateDataForNewProperty()
        case .lead:
            updateInfoForLead()
        case .opportunity:
            updateInfoForOpportunity()
        default:
            break
        }*/
        txtAddress.text = ""
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        dropDown.hide()
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionUploadImage(_ sender: Any) {
        openUploadPhotoAlert()
    }
    
    @IBAction func actionDone(_ sender: Any) {
        
        getUpdateOnParameter()
        
        guard addressValidation() else { return }
        guard locationType == .residential else {
            showAlert(message: "We are not supporting Commerical flow right now")
            return
        }
        if txtFirstName.text!.count == 0 {   //Uncomment on approval
            showAlert(message: "Please enter first name")
            return
        }
        /*if txtLastName.text!.count == 0 {
            showAlert(message: "Please enter last name")
            return
        }*/
        if txtPhone.text!.count == 0 {
            showAlert(message: "Please enter mobile number")
            return
        }
        let strNumber = "\(txtPhone.text!)".replacingOccurrences(of: "-", with: "")
        if Int("\(strNumber)".trimmed) != nil{
            if strNumber.count != 10 {
                showAlert(message: "Please enter valid mobile number")
                return
            }
            
        }else{
            showAlert(message: "Please enter valid mobile number")
            return
        }
        
        guard let propertyDetails = propertyDetails else { return }
        if  logType == .visit {
            switch propertyDetails.propertyType {
            case .empty:
                if let propertyImagename = propertyImage  {
                    self.presenter.apiCallForUploadPropertyImage(image: propertyImagename  , fileName: txtImageName.text!)
                } else {
                    self.apiCallToAddNewLead()
                }
            default:
                if let propertyImagename = propertyImage  {
                    self.presenter.apiCallForUploadPropertyImage(image: propertyImagename  , fileName: txtImageName.text!)
                } else {
                    self.apiCallToEditLead()
                }
            }
        } else {
            switch propertyDetails.propertyType {
            case .empty:
                updateDataForNewProperty()
            case .lead:
                updateInfoForLead()
            case .opportunity:
                updateInfoForOpportunity()
            default:
                break
            }
        }
    }
    @IBAction func actionClearAddress(_ sender: Any) {
        txtAddress.text = ""
    }
    
    @IBAction func actionAddAdress(_ sender: Any) {
        let storyboardIpad = UIStoryboard.init(name: "Lead-Prospect", bundle: nil)
        let vc: AddNewAddress_iPhoneVC = storyboardIpad.instantiateViewController(withIdentifier: "AddNewAddress_iPhoneVC") as! AddNewAddress_iPhoneVC
        vc.tag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: false, completion: {})
    }
    @IBAction func actionChanngeType(_ sender: UISegmentedControl) {
        if  sender.selectedSegmentIndex == 0 {
            self.locationType = .residential
        } else {
            self.locationType = .commercial
        }
    }
    @IBAction func actionCreateTask(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "CreatTaskViewController") as? CreatTaskViewController  else { return }
        destination.propertyDetails = propertyDetails
        self.present(destination, animated: true, completion: nil)
    }
    @IBAction func actionVisitResult(_ sender: Any) {
        var rows:[String] = []
        var selectedRow = 0
        guard let status = AppUserDefaults.shared.leadDetailMaster?.visitsStatus, !status.isEmpty else {
            showAlert(message: "No record found !!")
            return }
        
        for (index,row) in status.enumerated() {
            if self.txtResultType.text == row.visitStatusName {
                selectedRow = index
            }
            rows.append(row.visitStatusName ?? "")
        }
        showStringPicker(sender: viewVisitResult, selectedRow: selectedRow, rows: rows) { (index, value, view) in
            self.txtResultType.text = value
            self.selectedVisitResult = status[index]
        }
    }
    
    @IBAction func loadAddressAction(_ sender: Any) {
        mapPresenter.apiCallForAddress(selectedLocation!)
    }
    
    @IBAction func loadPropertyDataAction(_ sender: Any) {
        if txtAddress.text != "" {
            self.loadPropertyDetails()
        }else {
            showAlert(message: "Please load address first.")
            return
        }
    }
    
    
}
extension VisitLogViewController: MapView {
    func gotPlaceDetail(output: PlaceComponets) {
      //  self.hideLoader()
        print(output)
        self.placeDetail = output
        dispatchGroup.leave()
    }
    
    func gotPlaceDetailByZilliow(output: MapResult) {
        
    }
    
    func gotAddress(_ fullAddress: String, address: String, cityStateZip: String, location: CLLocation, placeMark: CLPlacemark) {
        let newAddress = "\(placeMark.name ?? ""), \( placeMark.locality ?? ""), \(placeMark.administrativeArea ?? ""), \(placeMark.postalCode ?? "")"
        txtAddress.text = newAddress
        
        // Change Address logics by Saavan
        
        guard let administrativeArea = placeMark.administrativeArea?.lowercased() else {
            self.hideLoader()
            showAlert(message: "Unable to find your state")
            return
        }
        guard let state = states.filter({ (stateT) -> Bool in
            return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
        }).first else {
            self.hideLoader()
            showAlert(message: "We are not serving in this state")
            return
        }
        
        var  propertyInfo = PropertyInfo()
        propertyInfo.firstName = self.placeDetail?.name
        propertyInfo.name = self.placeDetail?.name
        propertyInfo.phone = self.placeDetail?.formattedPhoneNumber?.trimmed
        propertyInfo.firstName = self.placeDetail?.name
        propertyInfo.flowType = "Residential"
        propertyInfo.latitude = location.coordinate.latitude
        propertyInfo.longitude = location.coordinate.longitude
        propertyInfo.state = state
        propertyInfo.placeMark = placeMark
        
        propertyInfo.address = address
        propertyInfo.city = placeMark.locality
        propertyInfo.country = placeMark.country
        propertyInfo.state?.name = placeMark.administrativeArea
        propertyInfo.zipCode = placeMark.postalCode
        propertyInfo.name = placeMark.name
        
        var propertyDetails = PropertyDetails()
        propertyDetails.propertyInfo = propertyInfo
        self.propertyDetails = propertyDetails
        
        self.hideLoader()
    }
    
    func alertInZilliow(_ message: String) {
        
    }
    
    func noGoogleLocation() {
        
    }
    
    func loadPropertyDetails () {
        var  propertyInfo = self.propertyDetails?.propertyInfo
        
        let arrAddress = txtAddress.text?.components(separatedBy: ",")
        
        propertyInfo?.address = txtAddress.text
        propertyInfo?.city = arrAddress?[1]
        propertyInfo?.state?.name = arrAddress?[2]
        propertyInfo?.zipCode = arrAddress?[3]
        
        self.apiCallMelissadataForPlaceDetail(address: arrAddress?[0] ?? "", propertyInfo: propertyInfo)
    }
    
    //MARK: Melissadata API Calling
    func apiCallMelissadataForPlaceDetail(address: String, propertyInfo: PropertyInfo? = nil)  {
        
        var  propertyInfoNew = PropertyInfo()
        propertyInfoNew = propertyInfo!
       let dictRecord = NSMutableDictionary()
        dictRecord.setValue(address, forKey: "AddressLine1")
        dictRecord.setValue(propertyInfo?.city, forKey: "City")
        dictRecord.setValue(propertyInfo?.state?.name, forKey: "State")
        dictRecord.setValue(propertyInfo?.zipCode, forKey: "PostalCode")

//        dictRecord.setValue("18715 Rogers Lk", forKey: "AddressLine1")
//            dictRecord.setValue("San Antonio", forKey: "City")
//            dictRecord.setValue("TX", forKey: "State")
//            dictRecord.setValue("70258", forKey: "PostalCode")
        
        let aryRecord = NSMutableArray()
        aryRecord.add(dictRecord)
        let dictData = NSMutableDictionary()

        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKeyMellissa = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.MelissaKey")!)"

        dictData.setValue(apiKeyMellissa.count == 0 ? MelissaKey : apiKeyMellissa, forKey: "CustomerId")
        dictData.setValue("Property V4 - LookupProperty", forKey: "TransmissionReference")
        dictData.setValue("1", forKey: "TotalRecords")
        dictData.setValue("GrpAll", forKey: "Columns")
        dictData.setValue(aryRecord, forKey: "Records")
        var jsonString = String()

        if(JSONSerialization.isValidJSONObject(dictData) == true)
        {
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dictData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print(jsonString)
            
        }
        
        let requestData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            print(dictData)
        Global().getServerResponseForSendLead(toServer: UrlMelissa, requestData, (NSDictionary() as! [AnyHashable : Any]), "", "") { (success, response, error) in
                if(success)
                {
                    if((response as NSDictionary?)?.value(forKey: "Records") is NSArray){
                        
                        var aryRecord = NSMutableArray()
                        
                        aryRecord = ((response as NSDictionary?)?.value(forKey: "Records") as! NSArray).mutableCopy()as! NSMutableArray
                        
                        if(aryRecord.count != 0){
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "IntRoomInfo") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                let dictIntRoomInfo =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.bathRoom = "\(dictIntRoomInfo.value(forKey: "BathCount")!)"
                                
                                propertyInfoNew.bedRoom = "\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)"
                                
                                propertyInfoNew.noOfFloor = "\(dictIntRoomInfo.value(forKey: "StoriesCount")!)"
                                
                                propertyInfoNew.flowType = "Residential"
                                
                                var typeInfo = "Residential"
                                
                                if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !"\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)".isEmpty , let noofBathroom =   Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !"\(dictIntRoomInfo.value(forKey: "BathCount")!)".isEmpty {
                                    
                                    typeInfo.append(" \(noofBedroom) Bed /" )
                                    
                                    typeInfo.append(" \(noofBathroom) Bath." )
                                    
                                } else {
                                    
                                    if let noofBedroom = Int("\(dictIntRoomInfo.value(forKey: "BedroomsCount")!)")?.description , !noofBedroom.isEmpty {
                                        
                                        typeInfo.append(" \(noofBedroom) Bed")
                                        
                                    }
                                    
                                    if let noofBathroom =  Int("\(dictIntRoomInfo.value(forKey: "BathCount")!)")?.description  , !noofBathroom.isEmpty {
                                        
                                        typeInfo.append(" \(noofBathroom) Bath.")
                                        
                                    }
                                    
                                }
                                
                                propertyInfoNew.type = typeInfo
                                
                            }else{
                                
                                propertyInfoNew.bathRoom = ""
                                
                                propertyInfoNew.bedRoom = ""
                                
                                propertyInfoNew.noOfFloor = ""
                                
                                propertyInfoNew.type = ""
                                
                                propertyInfoNew.flowType = ""
                                
                            }
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "PropertySize") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PropertySize") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                
                                let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.lotSizeSqFt = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                                
                                propertyInfoNew.area = "\(dictPropertySize.value(forKey: "AreaBuilding")!)"
                                
                            }else{
                                
                                propertyInfoNew.lotSizeSqFt = ""
                                
                                propertyInfoNew.area = ""
                                
                            }
                            
                            if((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") is NSDictionary){
                                
                                var dictRecord = NSMutableDictionary()
                                
                                dictRecord = ((aryRecord[0] as AnyObject).value(forKey: "PrimaryOwner") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                
                                
                                let dictPropertySize =  removeNullFromDict(dict: dictRecord)
                                
                                propertyInfoNew.firstName = "\(dictPropertySize.value(forKey: "Name1First")!)"
                                
                                propertyInfoNew.middleName = "\(dictPropertySize.value(forKey: "Name1Middle")!)"
                                
                                propertyInfoNew.lastName = "\(dictPropertySize.value(forKey: "Name1Last")!)"
                                
                                propertyInfoNew.name = "\(dictPropertySize.value(forKey: "Name1Full")!)"
                                
                                
                                
                            }else{
                                
                                propertyInfoNew.firstName = ""
                                
                                propertyInfoNew.middleName = ""
                                
                                propertyInfoNew.lastName = ""
                                
                                propertyInfoNew.name = ""
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else
                {
                    
                }
            
            print(propertyInfoNew)
            var propertyDetails = PropertyDetails()
            propertyDetails.propertyInfo = propertyInfoNew
            self.propertyDetails = propertyDetails
            
            self.setData()
            self.hideLoader()

        }
    }
}

// MARK: - UITextFieldDelegate
extension VisitLogViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtStatus {
            let storyboard = UIStoryboard(name: "NearbyMe", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "VisitStatusViewController") as? VisitStatusViewController  else { return false }
            destination.propertyDetails = propertyDetails
            destination.rowActionHandler  =  {(selectRow) -> () in
                if self.propertyDetails?.propertyType == .lead || self.propertyDetails?.propertyType == .empty {
                    guard let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus else { return }
                    self.txtStatus.text = leadsStatus[selectRow].name
                    self.selectedStatus =  leadsStatus[selectRow]
                }
            }
            self.navigationController?.present(destination, animated: true, completion: nil)
            
            return false
        } else if textField == txtType {
            var rows:[String] = []
            var selectedRow = 0
            guard let frequencies = AppUserDefaults.shared.masterDetails?.addressPropertyTypeMaster, !frequencies.isEmpty else {
                showAlert(message: "No record found !!")
                return false }
            let types = frequencies
            for (index,row) in types.enumerated() {
                if textField.text == row.name{
                    selectedRow = index
                }
                rows.append(row.name ?? "")
            }
            showStringPicker(sender: txtType, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                self.txtType.text = value
            }
            return false
        } else if textField == txtObjection {
//            guard let objections = AppUserDefaults.shared.leadDetailMaster?.objections, !objections.isEmpty else {
//                showAlert(message: "No record found !!")
//                return false }
            
            guard let objections = AppUserDefaults.shared.leadDetailMaster?.objections?.filter({$0.isActive == true}), !objections.isEmpty else {
                showAlert(message: "No record found !!")
                return false }
            
            
            var rows: [MultiSelectRow] = []
            let array = txtObjection.text!.trimmed.components(separatedBy: ",")
            for row in objections {
                var isSelected = false
                if array.contains(row.objection ?? ""){
                    isSelected = true
                }
                let row = MultiSelectRow(title: row.objection!, isSelected: isSelected)
                rows.append(row)
            }
            showMultiPicker(sender: txtObjection, rows: rows) { (indexs, view) in
                self.selectedObjections.removeAll()
                var selectedRows: [String] = []
                for index in indexs {
                    let value = objections[index].objection ?? ""
                    selectedRows.append(value)
                    self.selectedObjections.append(objections[index])
                    
                }
                self.txtObjection.text = selectedRows.joined(separator: ",")
            }
            return false
        }
        
        else if textField == txtAddress
        {
            if self.title == "Update"
            {
                var point = textField.frame.origin
                point.y = point.y + 60 + 50
                scrollView.setContentOffset(point, animated: true)
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtAddress
        {
            if self.title == "Update"
            {
                 scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                 dropDown.hide()
            }
           
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtBedRoom || textField == txtBathRoom || textField == txtSize {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else{
                    return false
                }
            }
            else if textField == txtNoOfFloor {
                let allowedCharacters = CharacterSet.decimalDigits
                guard allowedCharacters.isSuperset(of: characterSet) else {
                    return false
                }
            }
            else if textField == txtPhone {
                let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
                if(isValidd){
                    var strTextt = txtPhone.text!
                    strTextt = String(strTextt.dropLast())
                    txtPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
                }
              return isValidd
                
            }  else if textField == txtAddress {
                if  let text = txtAddress!.text, !text.isEmpty , !updatedText.isEmpty {
                    
                    if self.title == "Visit Log" || self.title == "Edit Leads Detail"
                    {
                        self.scrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
                    }
                    
                    self.placeAutocomplete(text)
                    /*self.getPlaces(with: parameters) {
                     self.places = $0
                     }*/
                } else {
                    dropDown.hide()
                }
            }
        }
        return true
    }
}

// MARK:- Image Picker Controller Delegate
extension VisitLogViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            
            guard let image = info[.editedImage] as? UIImage else { return }
            /*
             if let asset = info[.phAsset] as? PHAsset, let name = asset.value(forKey: "filename") as? String {
             fileName = name
             } else if let url = info[.imageURL] as? URL {
             fileName = url.lastPathComponent
             }*/
            let timeStamp  = Date().currentTimeMillis()
            let fileName = "\(String.random())-\(timeStamp).jpeg"
            self.txtImageName.text = fileName
            self.addressImageUrl = fileName
            self.propertyImage = image
        }
    }
}
extension VisitLogViewController: LogVisitView {
    func uploadImage(uploadResponse: UploadResponse) {
        self.addressImageUrl = uploadResponse.name
        switch propertyDetails?.propertyType {
        case .empty:
            self.apiCallToAddNewLead()
        case .lead:
            self.apiCallToEditLead()
        default:
            break
        }
    }
    func leadAddedSuccessFully(_ object: Lead) {
        updateInfoForLead(object)
    }
    
    func leadEditedSuccessfully(_ object: Lead) {
        updateInfoForLead(object)
    }
}

extension  VisitLogViewController : AddNewAddressiPhoneDelegate {
    func getDataAddNewAddressiPhone(dictData: NSMutableDictionary, tag: Int) {
        print(dictData)
        self.txtAddress.text = Global().strCombinedAddress(dictData as? [AnyHashable : Any])
        //self.strCustomerAddressId = ""
    }
}

extension  VisitLogViewController {
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    // MARK:- Auto Complete
    private func placeAutocomplete(_ text: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(text, bounds: nil, filter: nil) { (results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.places = results
            }
        }
    }
    // MARK:- To Get PlaceID From The Address
    private func getPlaceId(address:String) {
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        guard let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=false&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            // Spinner.shared.hide()
            if let data = data {
                do {
                    let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                    guard let loactionDetails = object.results.first else {
                        return
                    }
                    if let placeId = loactionDetails.placeId {
                        self.getPlaceDetails(id: placeId, apiKey: self.apiKey) { [unowned self] in
                            guard let value = $0 else { return }
                            self.txtAddress.text = self.addressFormattedByGoogle(value)
                            FTIndicator.dismissProgress()
                        }
                    }
                } catch  {
                    FTIndicator.dismissProgress()
                    self.txtAddress.text = ""
                }
            } else {
                FTIndicator.dismissProgress()
                self.txtAddress.text = ""
            }
        }.resume()
    }
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
    func openAddressDropDown(_ sender: UITextField) {
        
        if  self.places.count > 0 {
            var rows:[String] = []
            for (_,row) in places.enumerated() {
                rows.append(row.attributedFullText.string)
            }
            dropDown.dataSource = rows
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                guard let self = self else { return }
                let address = self.places[index].attributedFullText.string
                self.getPlaceId(address: address)
                // sender.setTitle(item, for: .normal)
            }
        } else {
            dropDown.hide()
        }
    }
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:]))
                FTIndicator.dismissProgress()
            }
        )
    }
    func addressFormattedByGoogle(_ value : PlaceDetails) -> String {
        var strAddress = ""
        if(value.name != nil) {
            strAddress = "\(value.name!)"
        }
        if(value.locality != nil){
            strAddress = strAddress + ",\(value.locality!)"
        }
        
        if(value.administrativeAreaCode != nil) {
            
            let administrativeAreaCode = "\(value.administrativeAreaCode!)"
            
            let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(administrativeAreaCode)")
            
            if dictStateDataTemp.count > 0 {
                strAddress = strAddress + ",\(dictStateDataTemp.value(forKey: "Name")!)"
            }
        }
        if(value.postalCode != nil) {
            strAddress = strAddress + ",\(value.postalCode!)"
        }
        return strAddress
    }
}
