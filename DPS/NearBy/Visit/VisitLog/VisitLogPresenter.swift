//
//  VisitLogPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 14/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation


protocol LogVisitView: CommonView {
    func uploadImage(uploadResponse: UploadResponse)
    func leadAddedSuccessFully(_ object: Lead)
    func leadEditedSuccessfully(_ object: Lead)
}

class LogVisitPresenter: NSObject {
    weak private var delegate: LogVisitView?
    private lazy var service = {
        return LogVisitService()
    }()
    
    init(_ delegate: LogVisitView) {
        self.delegate = delegate
    }
    
    func apiCallForUploadPropertyImage( image: UIImage, fileName: String) {
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        service.uploadImage(image: image, fileName: fileName) { (object, error) in
            if let object = object?.first{
                self.delegate?.uploadImage(uploadResponse: object)
            } else {
                FTIndicator.dismissProgress()
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallForAddNewLead(_ parameter: AddLead) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameter)
        print(String(data: orderJsonData, encoding: .utf8)!)
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        service.addLead(parameters: parameter){ (object, error) in
            FTIndicator.dismissProgress()
            if let object = object {
                self.delegate?.leadAddedSuccessFully(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallForEditLead(_ parameter: AddLead) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameter)
        print(String(data: orderJsonData, encoding: .utf8)!)
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        service.editLead(parameters: parameter){ (object, error) in
            FTIndicator.dismissProgress()
            if let object = object{
                self.delegate?.leadEditedSuccessfully(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
}
