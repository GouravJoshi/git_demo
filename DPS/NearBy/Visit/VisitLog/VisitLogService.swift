//
//  VisitLogService.swift
//  DPS
//
//  Created by Vivek Patel on 14/10/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class LogVisitService {
    
    func uploadImage( image: UIImage?, fileName: String?, callBack:@escaping (_ object: [UploadResponse]?,_ error:String?) -> Void) {
        let compressedImage = image?.compressTo(expectedSizeInKb: 100)
        AF.upload(multipartFormData: { multiPart in
            if let imageData = compressedImage?.jpegData(compressionQuality: 0.6) {
                multiPart.append(imageData, withName: "filename", fileName: fileName, mimeType: "image/jpeg")
            }
        }, to: URL.LeadD2D.uploadImage){ (urlRequest: inout URLRequest) in
            urlRequest.timeoutInterval = 180
        }
        .responseDecodable(of: [UploadResponse].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
    }
    func addLead(parameters: AddLead, _ callBack:@escaping (_ object: Lead?,_ error:String?) -> Void) {
        let url = URL.LeadD2D.addLeadOpp
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Lead.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                /* do {
                 let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                 let object = try JSONDecoder().decode(Lead.self, from: data)
                 print(object)
                 } catch (let error) {
                 print("MasterSalesAutomation", error.localizedDescription)
                 }*/
                
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func editLead(parameters: AddLead, _ callBack:@escaping (_ object: Lead?,_ error:String?) -> Void) {
        let url = URL.LeadD2D.editLeadOpp
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Lead.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
        .responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
