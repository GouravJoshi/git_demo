//
//  VisitFeedbackPopupViewController.swift
//  DPS
//
//  Created by Vivek Patel on 12/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class VisitFeedbackPopupViewController: UIViewController {
    // MARK:- Outlets
    @IBOutlet weak var btnNoAnswer: UIButton!
    @IBOutlet weak var btnContacted: UIButton!
    var delegate:VisitActionDelegate?
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK:- Actions
    @IBAction func actiondismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionFeedback(_ sender: UIButton) {
        //
        //        var type:VisitResults
        //        if sender.tag == 0 {
        //            type = .contacted
        //        }else {
        //            type = .noAnswer
        //        }
        //        self.delegate?.actionVisit(type)
        
    }
}
