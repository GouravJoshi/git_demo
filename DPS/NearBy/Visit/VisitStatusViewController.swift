//
//  VisitStatusViewController.swift
//  DPS
//
//  Created by Vivek Patel on 12/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class VisitStatusViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    typealias RowSelection = (_ selectedRow:Int) -> ()
    @IBOutlet weak var btnDissmiss: UIButton!
    var rowActionHandler:RowSelection?
    var rows:[String]=[]
    var propertyDetails: PropertyDetails?
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        
    }
    // MARK: - Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - UITableViewDataSource
extension VisitStatusViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
            return AppUserDefaults.shared.leadDetailMaster?.leadsStatus.count ?? 0
        } else if propertyDetails?.propertyType == .opportunity {
            return AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.count ?? 0
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: VisitStausTableViewCell.self), for: indexPath) as! VisitStausTableViewCell
        var color = UIColor.blue
        
        if propertyDetails?.propertyType == .lead || propertyDetails?.propertyType == .empty {
            
            guard let leadsStatus = AppUserDefaults.shared.leadDetailMaster?.leadsStatus else {
                return cell
            }
            let lead = leadsStatus[indexPath.row]
            if let colorCode = lead.colorCode {
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
            }
            cell.lblName.text = lead.name
            cell.lblName.lineBreakMode = .byWordWrapping
            
            if propertyDetails?.propertyType == .empty, lead.sysName == "Grabbed" {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            } else if lead.sysName == propertyDetails?.lead?.statusName {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            } else {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Unselected.png")
            }
            
        } else if propertyDetails?.propertyType == .opportunity {
            guard let opportunitesLeadStatus = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus else {
                return cell
            }
            let opportunity = opportunitesLeadStatus[indexPath.row]
            if let colorCode = opportunity.colorCode {
                if let tempColor = UIColor(hex:colorCode) {
                    color = tempColor
                }
            }
            cell.lblName.text = opportunity.statusName
            cell.lblName.lineBreakMode = .byWordWrapping
            
            if opportunity.sysName?.lowercased() == propertyDetails?.opportunity?.opportunityStatus?.lowercased() {
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Selected.png")
            }else{
                cell.imgViewRadio.image = UIImage(named: "RadioButton-Unselected.png")
            }
        }
        
        AppUtility.imageColor(image: cell.imgViewRadio, color: color)
        return cell
    }
}
// MARK: - UITableViewDelegate
extension VisitStatusViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            self.rowActionHandler!(indexPath.row)
        }
    }
}
