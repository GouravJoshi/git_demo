//
//  LogActivityService.swift
//  DPS
//
//  Created by Vivek Patel on 30/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class LogActivityService {
    
    func logActivity(parameters: LogActivityReuest, _ callBack:@escaping (_ object: LogActivityResponse?,_ error:String?) -> Void) {
        let url = URL.LogActivity.logActivity
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default) .responseDecodable(of: LogActivityResponse.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(LogActivityResponse.self, from: data)
                    print(object)
                    
                } catch (let error) {
                    print("LogActivityReuest", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
