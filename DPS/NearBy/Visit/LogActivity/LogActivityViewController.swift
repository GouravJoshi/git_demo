//
//  LogActivityViewController.swift
//  DPS
//
//  Created by Vivek Patel on 30/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol ActivityDelegate {
    func activityUpdated(_ activityStatus: String)
}

final class LogActivityViewController: LogActivityBaseViewController {
    //MARK: - Variables
    var selectedLogType: [String: Any]?
    var selectedOpportunity: Opportunity?
    var selectedAccount: Account?
    var selectedVisitStatus:VisitStatus?
    var delegate: ActivityDelegate?
    var propertyType:PropertyType?
    private lazy var presenter = {
        return LogActivityPresenter(self)
    }()
    //MARK: - Outlets
    @IBOutlet weak var txtLogType: UITextField!
    @IBOutlet weak var txtActivity: UITextField!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtLogType.delegate = self
        self.title = "Log as Activity"
        txtActivity.text = selectedVisitStatus?.visitStatusName ?? ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    //MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionContinue(_ sender: Any) {
        
        guard !txtLogType.text!.trimmed.isEmpty else {
            showAlert(message:  "Please select actvity log type")
            return
        }
        guard !txtActivity.text!.trimmed.isEmpty else {
            showAlert(message:  "Please enter activity")
            return
        }
        var parameters = getParameters()
        if let selectedOpportunity = selectedOpportunity {
            parameters.accountID = selectedOpportunity.accountID?.description
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
            // let createdBy =  dictLoginData.value(forKey: "CreatedBy") as! String
            parameters.participantsList = arrOfParticipantsList
            parameters.createdDate = selectedOpportunity.createdDate
            parameters.crmContactID = selectedOpportunity.crmContactID?.description
            // parameters.createdBy = createdBy
            // parameters.crmCompanyID = selectedOpportunity?.compa
            //parameters.webLeadID =  selectedOpportunity?.
            parameters.leadID =  selectedOpportunity.opportunityID?.description
        } else if let selectedAccount = selectedAccount {
            parameters.accountID = selectedAccount.accountID?.description
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let arrOfParticipantsList = "\(dictLoginData.value(forKey: "EmployeeId")!)".components(separatedBy: ",")
            // let createdBy =  dictLoginData.value(forKey: "CreatedBy") as! String
            parameters.participantsList = arrOfParticipantsList
            parameters.createdDate = selectedAccount.createdDate
            parameters.crmContactID = selectedAccount.crmContactID?.description
            // parameters.createdBy = createdBy
            // parameters.crmCompanyID = selectedOpportunity?.compa
            //parameters.webLeadID =  selectedOpportunity?.
            //parameters.leadID =  account.accountID?.description
        }
        parameters.agenda  =  txtActivity.text
        if let selectedLogType = selectedLogType {
            let id = selectedLogType["LogTypeId"] as? Int
            parameters.logTypeID = id?.description
        }
        presenter.apiCallToLogActivity(parameters)
    }
}

// MARK: - UITextFieldDelegate
extension LogActivityViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLogType {
            self.view.endEditing(true)
            var selectedRow = 0
            let dictTotalLeadCountResponse = (nsud.value(forKey: "TotalLeadCountResponse") as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            let aryTemp = dictTotalLeadCountResponse.value(forKey: "ActivityLogTypeMasters") as! [[String: Any]]
            let arrayfilter = aryTemp.filter { (object) -> Bool in
                return object["IsActive"] as? Bool == true
            }
            var rows:[String] = []
            print(arrayfilter)
            for (index,row) in arrayfilter.enumerated()  {
                if textField.text == row["Name"] as? String{
                    selectedRow = index
                }
                rows.append(row["Name"] as? String ?? "")
            }
            if(arrayfilter.count != 0) {
                showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                    textField.text = value
                    self.selectedLogType = arrayfilter[index]
                }
            } else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Data not available.", viewcontrol: self)
            }
            return false
        }
        return true
    }
}
//MARK: - LogActivityView
extension LogActivityViewController: LogActivityView {
    func loggedSuccessfully(_ output: LogActivityResponse) {
        showAlertWithCompletion(message: "Activity Added successfully") {
            self.delegate?.activityUpdated(output.agenda ?? "")
        }
    }
}

