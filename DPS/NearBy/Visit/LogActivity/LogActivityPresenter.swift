//
//  LogActivityPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 30/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

import Foundation
import UIKit

protocol LogActivityView: CommonView {
    func loggedSuccessfully(_ output: LogActivityResponse)
}

class LogActivityPresenter: NSObject {
    weak private var delegate: LogActivityView?
    private lazy var service = {
        
        return LogActivityService()
    }()
    
    init(_ delegate: LogActivityView) {
        self.delegate = delegate
    }
    
    func apiCallToLogActivity(_ parameters: LogActivityReuest) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        service.logActivity(parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object{
                self.delegate?.loggedSuccessfully(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}

