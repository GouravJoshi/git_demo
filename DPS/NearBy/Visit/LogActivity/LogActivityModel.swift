//
//  LogActivityModel.swift
//  DPS
//
//  Created by Vivek Patel on 30/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

// MARK: - LogActivity
struct LogActivityReuest: Codable {
    var toDate, logTypeID, address2: String?
    var employeeEXTDCS: [JSONAny]?
    var leadID, activityTime, childTasks: String?
    var activityCommentEXTDCS: [JSONAny]?
    var countryID, cityName, agenda, createdDate: String?
    var stateID, refID, createdBy, webLeadID: String?
    var zipCode, modifiedDate, crmContactID, id: String?
    var activityLogTypeMasterEXTDc: [JSONAny]?
    var crmCompanyID, accountID, activityID: String?
    var leadEXTDCS: [JSONAny]?
    var address1, modifiedBy: String?
    var participantsList: [String]?
    var fromDate, isSystem, refType: String?
    
    
    enum CodingKeys: String, CodingKey {
        case toDate = "ToDate"
        case logTypeID = "LogTypeId"
        case address2 = "Address2"
        case employeeEXTDCS = "EmployeeExtDcs"
        case leadID = "LeadId"
        case activityTime = "ActivityTime"
        case childTasks = "ChildTasks"
        case activityCommentEXTDCS = "ActivityCommentExtDcs"
        case countryID = "CountryId"
        case cityName = "CityName"
        case agenda = "Agenda"
        case createdDate = "CreatedDate"
        case stateID = "StateId"
        case refID = "RefId"
        case createdBy = "CreatedBy"
        case webLeadID = "WebLeadId"
        case zipCode = "ZipCode"
        case modifiedDate = "ModifiedDate"
        case crmContactID = "CrmContactId"
        case id = "Id"
        case activityLogTypeMasterEXTDc = "ActivityLogTypeMasterExtDc"
        case crmCompanyID = "CrmCompanyId"
        case accountID = "AccountId"
        case activityID = "ActivityId"
        case leadEXTDCS = "LeadExtDcs"
        case address1 = "Address1"
        case modifiedBy = "ModifiedBy"
        case participantsList = "ParticipantsList"
        case fromDate = "FromDate"
        case isSystem = "IsSystem"
        case refType = "RefType"
    }
}
struct LogActivityResponse : Codable {
    var AccountId:Int?
    var AccountNo:String?
    // ActivityCommentExtDcs
    var ActivityId:Int?
    // ActivityLogTypeMasterExtDc
    var ActivityTime:String?
    var Address1:String?
    var Address2:String?
    var AddressLine1:String?
    var AddressLine2:String?
    var agenda:String?
    var CellPhone1:String?
    var CellPhone2:String?
    var ChildTasks:String?
    var City:String?
    var CityName:String?
    var ClientCreatedDate:String?
    var ClientModifiedDate:String?
    var CompanyId:String?
    var CompanyName:String?
    var CountryId:String?
    //  CountryMasterExtDc
    var CountryName:String?
    var CreatedBy:Int?
    var CreatedDate:String?
    var CrmCompanyId:String?
    var CrmContactId:Int?
    // var EmployeeExtDcs:String?
    var FirstName:String?
    var  FromDate:String?
    //var  IsPrimary:String?
    // var IsSystem:String?
    var LastName:String?
    // var LeadExtDcs:String?
    var LeadId:Int?
    var LeadNo:String?
    //    LogTypeId
    //    LogTypeStr
    //    MiddleName
    //    ModifiedBy
    //    ModifiedDate
    //    ParentTask
    //    ParentTaskId
    //    Participants
    //    ParticipantsList
    var PrimaryEmail:String?
    var PrimaryPhone:String?
    var RefId:String?
    var RefType:String?
    //RelatedLeads
    var SecondaryEmail:String?
    var SecondaryPhone:String?
    var StateId:String?
    // StateMasterExtDc
    // StateName
    var ToDate:String?
    // WebLeadId
    // WebLeadNumber
    var ZipCode:String?
    //accountinfo_url
    // leadinfo_url
    
    enum CodingKeys: String, CodingKey {
        case AccountId
        case AccountNo
        
        case ActivityId
        case ActivityTime
        case Address2
        case AddressLine1
        case AddressLine2
        case agenda = "Agenda"
        case CellPhone1
        case CellPhone2
        case ChildTasks
        case City
        case CityName
        case ClientCreatedDate
        case ClientModifiedDate
        case CompanyId
        case CompanyName
        case CountryId
        case CountryName
        case CreatedBy
        case CreatedDate
        case CrmCompanyId
        case CrmContactId
        case FirstName
        case FromDate
        case LastName
        case LeadId
        case LeadNo
        case PrimaryEmail
        case PrimaryPhone
        case RefId
        case RefType
        case SecondaryEmail
        case SecondaryPhone
        case StateId
        case ToDate
        case ZipCode
        // ActivityCommentExtDcs
        // ActivityLogTypeMasterExtDc
        //  case EmployeeExtDcs
        // case  LeadExtDcs
        //  case  IsPrimary
        //  case IsSystem
        // case CountryMasterExtDc
        //    LogTypeId
        //    LogTypeStr
        //    MiddleName
        //    ModifiedBy
        //    ModifiedDate
        //    ParentTask
        //    ParentTaskId
        //    Participants
        //    ParticipantsList
        // StateMasterExtDc
        // StateName
        // WebLeadId
        // WebLeadNumber
        //accountinfo_url
        // leadinfo_url
        //RelatedLeads
        
        
    }
    
}

