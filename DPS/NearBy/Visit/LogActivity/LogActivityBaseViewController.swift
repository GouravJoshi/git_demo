//
//  LogActivityBaseViewController.swift
//  DPS
//
//  Created by Vivek Patel on 30/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class LogActivityBaseViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func getParameters() -> LogActivityReuest {
        var parameters = LogActivityReuest()
        let date = Date()
        let currentDate = AppUtility.convertDateToString(date, toFormat: "MM/dd/yyyy")
        let activityTime = AppUtility.convertDateToString(date, toFormat: "HH:mm")
        
        
        parameters.activityCommentEXTDCS = []
        parameters.activityLogTypeMasterEXTDc = []
        parameters.activityTime = activityTime
        parameters.activityID = ""
        parameters.address1 = ""
        parameters.address2 = ""
        parameters.childTasks = ""
        parameters.cityName = ""
        parameters.countryID = ""
        parameters.employeeEXTDCS = []
        parameters.fromDate = currentDate
        parameters.id = "0"
        parameters.isSystem = "true"
        parameters.modifiedBy = ""
        parameters.refType = ""
        parameters.refID = ""
        parameters.stateID = ""
        parameters.toDate = currentDate
        parameters.zipCode = ""
        parameters.leadEXTDCS = []
        
      
        return parameters
    }
}
