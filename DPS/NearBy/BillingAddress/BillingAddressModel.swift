//
//  BillingAddressModel.swift
//  DPS
//
//  Created by Vivek Patel on 17/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation


// MARK: - WelcomeElement
struct ServiceAddressD2D: Codable {
    let customerAddressID: Int?
    let name: String?
    let addressTypeID: Int?
    let address1, address2, cityName: String?
    let stateID: Int?
    let stateName: String?
    let countryID: Int
    let countryName, zipcode, county, schoolDistrict: String?
    let accountID: Int?
    let addressSubType: String?
    let mobileAddressID: JSONNull?
    let createdBy: Int?
    let isPrimary: Bool?
    let gateCode, mapCode, taxSysName: String?
    
    enum CodingKeys: String, CodingKey {
        case customerAddressID = "CustomerAddressId"
        case name = "Name"
        case addressTypeID = "AddressTypeId"
        case address1 = "Address1"
        case address2 = "Address2"
        case cityName = "CityName"
        case stateID = "StateId"
        case stateName = "StateName"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case zipcode = "Zipcode"
        case county = "County"
        case schoolDistrict = "SchoolDistrict"
        case accountID = "AccountId"
        case addressSubType = "AddressSubType"
        case mobileAddressID = "MobileAddressId"
        case createdBy = "CreatedBy"
        case isPrimary = "IsPrimary"
        case gateCode = "GateCode"
        case mapCode = "MapCode"
        case taxSysName = "TaxSysName"
    }
}
