//
//  BillingAddressService.swift
//  DPS
//
//  Created by Vivek Patel on 17/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class BillingAddressService {
    
    func getServiceAddressD2D(accountId: String, _ callBack:@escaping (_ object: [ServiceAddressD2D]?,_ error:String?) -> Void) {
        
        let url = URL.BillingAddress.getServiceAddress.replacingOccurrences(of: "(AccountId)", with: accountId)
        AF.request(url, method: .get) .responseDecodable(of: [ServiceAddressD2D].self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode([ServiceAddressD2D].self, from: data)
                    print(object)
                    
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
