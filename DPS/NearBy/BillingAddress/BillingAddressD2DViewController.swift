//
//  BillingAddressD2DViewController.swift
//  DPS
//
//  Created by Vivek Patel on 16/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
import DropDown
import GooglePlaces

final class BillingAddressD2DViewController: BaseViewController {
    var delegate: BillingAddressDelegate?
    var billingAddress: SendAgreement?
    private var places: [GMSAutocompletePrediction] = [] {
        didSet { openAddressDropDown(self.txtNewaddress) }
    }
    private var apiKey: String = "AIzaSyDop70kb1gJxqWoi5Bt3m2YjEI_FYycbsU"
    let dropDown = DropDown()
    var entityDetail: EntityDetail?
    var propertyDetails: PropertyDetails?
    private lazy var presenter = {
        return BillingAddressPresenter(self)
    }()
    var serviceAddress: [ServiceAddressD2D] = [] {
        didSet {
            setData()
        }
    }
    var selectedAddress: ServiceAddressD2D?
    var isEditBillingAddress = false

    @IBOutlet weak var viewNewAddress: UIStackView!
    @IBOutlet weak var txtNewaddress: UITextField!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var imgLastName: UIImageView!
    @IBOutlet weak var imgFirstName: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet var scrollView: UIScrollView!
    // MARK: - View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setData()
    }
    
    // MARK: Function to set UI
    func setUI() {
        //viewAddress?.isHidden = btnBillingAddress.isSelected
        btnBillingAddress.isSelected = false
        txtFirstName.setLeftPaddingPoints(30)
        txtLastName.setLeftPaddingPoints(30)
        txtEmail.setLeftPaddingPoints(30)
        txtPhone.setLeftPaddingPoints(30)
        AppUtility.imageColor(image: imgFirstName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgLastName, color: .appDarkGreen)
        AppUtility.imageColor(image: imgEmail, color: .appDarkGreen)
        AppUtility.imageColor(image: imgPhone, color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnAddress, image: "Group 771", color: .gray)
        var id = ""
        switch propertyDetails?.propertyType {
        case .lead:
            id = propertyDetails?.lead?.leadID?.description ?? ""
        case .empty:
            break
        case .opportunity:
            id = propertyDetails?.opportunity?.opportunityID?.description ?? ""
        case .account:
            id = propertyDetails?.account?.accountID?.description ?? ""
        case .none:
            break
        }
        if propertyDetails?.propertyType != .empty {
            presenter.apiCallForServiceAddressD2D(id)
        }
        
    }
    func setData() {
        if let billingAddress = billingAddress, let billingAddressSameAsService = billingAddress.billingAddressSameAsService {
            //  txtAddress.text = billingAddress.tempFullBillingAddress
            btnBillingAddress.isSelected = billingAddressSameAsService
            txtFirstName.text = billingAddress.billingFirstName
            txtLastName.text = billingAddress.billingLastName
            txtEmail.text = billingAddress.billingPrimaryEmail
            txtPhone.text = billingAddress.billingCellPhone1
            txtPhone.text = formattedNumber(number: txtPhone.text!)
            txtNewaddress.text = billingAddress.tempFullBillingAddress
        } else  if let entityDetail = entityDetail , let leadDetail = entityDetail.leadDetail {
            var billingAddress = SendAgreement()
            txtFirstName.text = leadDetail.billingFirstName
            txtLastName.text = leadDetail.billingLastName
            txtEmail.text = leadDetail.billingPrimaryEmail
            txtPhone.text = leadDetail.billingPrimaryPhone//leadDetail.billingCellNo
            txtPhone.text = formattedNumber(number: txtPhone.text!)
            btnBillingAddress.isSelected = entityDetail.leadDetail?.isBillingAddressSame ?? true
            var stateName = ""
            if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
                return state.stateSysName == leadDetail.billingState
            }) {
                stateName = state.stateSysName ?? ""
            }
            let address = "\(leadDetail.billingAddress1 ?? ""), \(leadDetail.billingCity ?? ""), \(stateName), \(leadDetail.billingZipcode ?? "")"
            // txtAddress.text = address
            billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
            billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
            billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
            billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
            billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
            billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
            delegate?.billingDetailEntered(billingAddress)
        }/*else if let propertyDetails = propertyDetails {
         var billingAddress = SendAgreement()
         switch propertyDetails.propertyType {
         case .lead:
         btnBillingAddress.isSelected = true
         billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
         case .empty:
         btnBillingAddress.isSelected = true
         billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
         case .opportunity:
         if let opportunity = propertyDetails.opportunity {
         
         btnBillingAddress.isSelected = opportunity.billingAddressSameAsService ?? true
         var stateName = ""
         if let billingAddressOld = opportunity.billingAddress {
         if let state = AppUtility.fetchStates()?.first(where: { (state) -> Bool in
         return state.stateID == billingAddressOld.stateID
         }) {
         stateName = state.stateSysName ?? ""
         billingAddress.billingStateId = state.stateID
         }
         
         let address = "\(billingAddressOld.address1 ?? ""), \(billingAddressOld.cityName ?? ""), \(stateName), \(billingAddressOld.zipcode ?? "")"
         txtAddress.text = address
         }
         billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
         billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
         billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
         billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
         billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
         billingAddress.billingAddressSameAsService = btnBillingAddress.isSelected
         }
         case .account:
         print("account")
         }
         delegate?.billingDetailEntered(billingAddress)
         }*/
        if propertyDetails?.propertyType != .empty {
            let objects = serviceAddress
            guard !objects.isEmpty else {
                txtAddress.isUserInteractionEnabled = false
                txtAddress.placeholder = "New Billing Address"
                viewNewAddress.isHidden = false
                return
            }
            txtAddress.isUserInteractionEnabled = true
            txtAddress.placeholder = "Select Billing Address"
            viewNewAddress.isHidden = true
        } else {
            viewNewAddress.isHidden = false
            txtAddress.isUserInteractionEnabled = false
            txtAddress.placeholder = "New Billing Address"
        }
        
        if isEditBillingAddress {
            
            viewNewAddress.isHidden = false
            
        }
        
    }
    
    @IBAction func actionCheckMark(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.actionBillingAddress(true)
        }
        
    }
    @IBAction func actionAddAdress(_ sender: Any) {
        delegate?.addAddressPressed()
    }
    
    @IBAction func actionClose(_ sender: Any) {
        
        self.delegate?.addAddressPressed()
        self.dismiss(animated: false, completion: nil)
        
    }
    @IBAction func actionSave(_ sender: Any) {
        var isProceed = proceedValidationBilling()
        guard isProceed else {
            isProceed = proceedValidationBilling()
            return
        }
        if let billingAddress = billingAddress {
            self.dismiss(animated: true) {
                self.delegate?.billingDetailEntered(billingAddress)
            }
        }
    }
    private func proceedValidationBilling() -> Bool {
        guard var billingAddress = billingAddress else  {
            return false
        }
        var enterAddress: String?
        if viewNewAddress.isHidden {
            enterAddress = txtAddress.text
        } else {
            enterAddress = txtNewaddress.text
        }
        guard let address = enterAddress, !address.isEmpty else {
            showAlert(message: "Please enter billing Address")
            return false
        }
        let addressComponents =  address.trimmed.replacingOccurrences(of: "  ", with: "").components(separatedBy:",")
        guard addressComponents.count == 4 ||  addressComponents.count == 5  else {
            showAlert(message: "Please enter valid Address")
            return false
        }
        guard let states = AppUtility.fetchStates() else {
            showAlert(message: "Unable to find states")
            return false
        }
        billingAddress.tempFullBillingAddress = address
        var administrativeArea = ""
        if addressComponents.count == 4 {
            administrativeArea = addressComponents[2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
        } else {
            administrativeArea = addressComponents[3].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
        }
        guard let _ = states.filter({ (stateT) -> Bool in
            return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
        }).first else {
            showAlert(message: "We are not serving in this state")
            return false
        }
        if let address1 =  addressComponents.first?.trimmed {
            billingAddress.billingAddress1 = String(address1.prefix(100))
        }
        var position = 0
        if addressComponents.count == 4 {
            position = 0
        } else {
            position = 1
        }
        if addressComponents.count >= 2 {
            let city = addressComponents[position+1].trimmed
            billingAddress.billingCityName = city
        }
        if addressComponents.count >= 3 {
            let administrativeArea = addressComponents[position+2].lowercased().trimmed.replacingOccurrences(of: "  ", with: " ")
            if let states = AppUtility.fetchStates()  {
                if let state = states.filter({ (stateT) -> Bool in
                    return stateT.stateShortName?.lowercased() == administrativeArea || stateT.stateSysName?.lowercased() == administrativeArea
                }).first  {
                    billingAddress.billingStateId = state.stateID
                    billingAddress.billingState = state.name
                } else {
                    showAlert(message: "We are not serving in this state")
                    return false
                }
            }
        }
        if addressComponents.count >= 4 {
            let zipCode = addressComponents[position+3].trimmed
            billingAddress.billingZipcode = zipCode
        }
        
        guard let firstName = txtFirstName.text, !firstName.isEmpty else {
            showAlert(message: "Please enter first name")
            return false
        }
        
        let lastName = txtLastName.text
//        guard let lastName = txtLastName.text, !lastName.isEmpty else {
//            showAlert(message: "Please enter last name")
//            return false
//        }
        if (txtEmail.text?.count ?? 0) > 2
        {
            if let email = txtEmail.text {
                guard email.isValidEmail()  else {
                    showAlert(message: "Please enter valid Email address")
                    return false
                }
            }
        }
        let strNumber = "\(txtPhone.text ?? "")".replacingOccurrences(of: "-", with: "")
        if Int("\(strNumber)".trimmed) != nil{
            if strNumber.count != 10 {
                showAlert(message: "Please enter valid mobile number")
                return false
            }
        }
//        else{
//            showAlert(message: "Please enter valid mobile number")
//            return false
//        }
        /*if let text = txtEmail.text
        {
            guard txtEmail.text!.isEmpty else {
                let emails = txtEmail.text!.components(separatedBy: ",")
                for email in emails {
                    guard email.trimmed.isValidEmail() else {
                        showAlertWithCompletion(message: "Please enter valid Email address") {
                            self.txtEmail.becomeFirstResponder()
                        }
                        return false
                    }
                }
                return true
            }
            
        }*/
        
        
        billingAddress.billingFirstName = firstName
        billingAddress.billingLastName = lastName
        billingAddress.billingPrimaryEmail = txtEmail.text ?? ""
        billingAddress.billingCellPhone1 = txtPhone.text ?? ""
        if propertyDetails?.propertyType == .opportunity {
            billingAddress.billingPOCId = propertyDetails?.opportunity?.billingAddress?.billingPOCId
        }
        self.billingAddress = billingAddress
        return true
    }
}

extension BillingAddressD2DViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            if textField == txtPhone {
                let isValidd = txtFieldValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
                if(isValidd){
                    var strTextt = txtPhone.text!
                    strTextt = String(strTextt.dropLast())
                    txtPhone.text = formattedNumber(number: isValidd ? textField.text! : strTextt)
                }
                return isValidd
                
            }
            else if textField == txtNewaddress  {
                if  let text = txtNewaddress!.text, !text.isEmpty , !updatedText.isEmpty {
                    self.placeAutocomplete(text)
                } else {
                    dropDown.hide()
                }
            }
            /* var billingAddress = SendAgreement()
             billingAddress.billingFirstName = txtFirstName.text?.trimmed ?? ""
             billingAddress.billingLastName = txtLastName.text?.trimmed ?? ""
             billingAddress.billingPrimaryEmail = txtEmail.text?.trimmed ?? ""
             billingAddress.billingCellPhone1 = txtPhone.text?.trimmed ?? ""
             billingAddress.tempFullBillingAddress = txtAddress.text?.trimmed ?? ""
             billingAddress.billingAddressSameAsService = btnAddress.isSelected
             delegate?.billingDetailEntered(billingAddress)*/
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtAddress
        {

            var rows:[String] = []
            var selectedRow = 0
            let objects = serviceAddress
            guard  !objects.isEmpty else {
                showAlert(message: "No record found !!")
                return false
            }
            for (index,row) in objects.enumerated() {
                if selectedAddress?.customerAddressID == row.customerAddressID {
                    selectedRow = index
                }
                let address = "\(row.address1 ?? ""), \(row.cityName ?? ""), \(row.stateName ?? ""), \(row.zipcode ?? "")"
                rows.append(address)
            }
            rows.append("New Billling Address")
            showStringPicker(sender: textField, selectedRow: selectedRow, rows: rows) { (index, value, view) in
                textField.text = value
                if value == "New Billling Address" {
                    self.txtAddress.text = value
                    self.viewNewAddress.isHidden = false
                } else {
                    self.txtAddress.text = value
                    self.viewNewAddress.isHidden = true
                    self.selectedAddress = objects[index]
                }
            }
            

            return false
        }
        if textField == txtNewaddress
        {
            var point = textField.frame.origin
            point.y = point.y + 100
            scrollView.setContentOffset(point, animated: true)
        }
        
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtNewaddress
        {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            dropDown.hide()
        }
    }
}

extension  BillingAddressD2DViewController {
    func doRequest(_ urlString: String, params: [String: String], completion: @escaping (NSDictionary) -> Void) {
        var components = URLComponents(string: urlString)
        components?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = components?.url else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("GooglePlaces Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("GooglePlaces Error: No response from API")
                return
            }
            
            guard response.statusCode == 200 else {
                print("GooglePlaces Error: Invalid status code \(response.statusCode) from API")
                return
            }
            
            let object: NSDictionary?
            do {
                object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            } catch {
                object = nil
                print("GooglePlaces Error")
                return
            }
            
            guard object?["status"] as? String == "OK" else {
                print("GooglePlaces API Error: \(object?["status"] ?? "")")
                return
            }
            
            guard let json = object else {
                print("GooglePlaces Parse Error")
                return
            }
            // Perform table updates on UI thread
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completion(json)
            }
        })
        
        task.resume()
    }
    // MARK:- Auto Complete
    private func placeAutocomplete(_ text: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(text, bounds: nil, filter: nil) { (results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.places = results
            }
        }
    }
    // MARK:- To Get PlaceID From The Address
    private func getPlaceId(address:String) {
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let apiKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey")!)"
        guard let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=false&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            // Spinner.shared.hide()
            if let data = data {
                do {
                    let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                    guard let loactionDetails = object.results.first else {
                        return
                    }
                    if let placeId = loactionDetails.placeId {
                        self.getPlaceDetails(id: placeId, apiKey: apiKey) { [unowned self] in
                            guard let value = $0 else { return }
                            self.txtNewaddress.text = self.addressFormattedByGoogle(value)
                            FTIndicator.dismissProgress()
                        }
                    }
                } catch  {
                    FTIndicator.dismissProgress()
                    self.txtNewaddress.text = ""
                }
            } else {
                FTIndicator.dismissProgress()
                self.txtNewaddress.text = ""
            }
        }.resume()
    }
    var deviceLanguage: String? {
        return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String
    }
    func openAddressDropDown(_ sender: UITextField) {
        
        if  self.places.count > 0 {
            var rows:[String] = []
            for (_,row) in places.enumerated() {
                rows.append(row.attributedFullText.string)
            }
            dropDown.dataSource = rows
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                guard let self = self else { return }
                let address = self.places[index].attributedFullText.string
                self.getPlaceId(address: address)
                // sender.setTitle(item, for: .normal)
            }
        } else {
            dropDown.hide()
        }
        
    }
    func getPlaceDetails(id: String, apiKey: String, completion: @escaping (PlaceDetails?) -> Void) {
        var parameters = [ "placeid": id, "key": apiKey ]
        if let deviceLanguage = deviceLanguage {
            parameters["language"] = deviceLanguage
        }
        doRequest(
            "https://maps.googleapis.com/maps/api/place/details/json",
            params: parameters,
            completion: { completion(PlaceDetails(json: $0 as? [String: Any] ?? [:]))
                FTIndicator.dismissProgress()
            }
        )
    }
    func addressFormattedByGoogle(_ value : PlaceDetails) -> String {
        var strAddress = ""
        if(value.name != nil) {
            strAddress = "\(value.name!)"
        }
        if(value.locality != nil){
            strAddress = strAddress + ",\(value.locality!)"
        }
        
        if(value.administrativeAreaCode != nil) {
            
            let administrativeAreaCode = "\(value.administrativeAreaCode!)"
            
            let dictStateDataTemp = getStateFromShortName(strStateShortName: "\(administrativeAreaCode)")
            
            if dictStateDataTemp.count > 0 {
                strAddress = strAddress + ",\(dictStateDataTemp.value(forKey: "Name")!)"
            }
        }
        if(value.postalCode != nil) {
            strAddress = strAddress + ",\(value.postalCode!)"
        }
        return strAddress
    }
}

extension BillingAddressD2DViewController: BillingAddressView {
    func gotSeriveList(_ output: [ServiceAddressD2D]) {
        serviceAddress = output
    }
}
