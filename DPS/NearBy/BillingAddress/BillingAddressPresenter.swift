//
//  BillingAddressPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 17/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import UIKit

protocol BillingAddressView: CommonView {
    func gotSeriveList(_ output: [ServiceAddressD2D])
}

class BillingAddressPresenter: NSObject {
    weak private var delegate: BillingAddressView?
    private lazy var service = {
        
        return BillingAddressService()
    }()
    
    init(_ delegate: BillingAddressView) {
        self.delegate = delegate
    }
    
    
    func apiCallForServiceAddressD2D(_ accountId: String) {
        delegate?.showLoader()
        service.getServiceAddressD2D(accountId: accountId) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.gotSeriveList(object)
            } else {
               // self.delegate?.showAlert(message: error)
            }
        }
        
    }
    
}

