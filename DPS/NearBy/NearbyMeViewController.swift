//
//  NearbyMeViewController.swift
//  DPS
//
//  Created by Vivek Patel on 10/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit


protocol NearbyDelegate: NSObjectProtocol {
    func tabChanged(_ selectedTab:Int)
    func gotEntitiesResponse( leads: [Lead], opportunities: [Opportunity], accounts: [Account], isLocationChange: Bool)
    func changeMapLoction( location: CLLocation, address:String)
    func gotSelectedLocation(_ locationDetails: LocationResult)
    func updateNearByFilter(_ nearbyFilterData: NearbyFilterData)
    func searchLocation(_ selectedTab: Int, searchText: String?, searchedArea: GeoFencingD2D?)
    func gotShapesData(_ output: [GeoFencingD2D]?)
}
enum HeaderViewTabs: Int, CaseIterable {
    case lead
    case opportunity
    case map
    case tasks
    case activity
    case viewSchedule
    case signedAgreements
    var title: String {
        switch self {
        case .lead:
            return "Lead"
        case .opportunity:
            return "Opportunity"
        case .map:
            return "Map"
        case .tasks:
            return "Tasks"
        case .activity:
            return "Activity"
        case .viewSchedule:
            return "View Schedule"
        case .signedAgreements:
            return "Signed Agreements"
            
        }
    }
}
final class NearbyMeViewController: BaseViewController {
    // MARK: - Variables
    private var isSelectedIndex = 0 {
        didSet{
            if isSelectedIndex == 2 {
                searchBar.placeholder = "Search by Name or Contact"
            } else {
                searchBar.placeholder = "Search"
            }
        }
    }
    private var selectedTab: HeaderViewTabs = .map
    weak var delegate: NearbyDelegate?
    
    private lazy var presenter = {
        return EntityPresenter(self)
    }()
    let dispatchGroup = DispatchGroup()
    private var leads: [Lead] = []
    private var opportunities: [Opportunity] = []
    private var accounts: [Account] = []
    var nearbyFilterData = NearbyFilterData()
    private lazy var presenterGeo = {
        return GeoMappingPresenter(self)
    }()
    // MARK: - Outlets
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var constraintCollectionHeight: NSLayoutConstraint!
    // MARK: - View life cycle
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnFilter: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(handleAgreement(_:)), name: .didUCreatedLead, object: nil)
        // title = "NearBy Me"
        setSegment()
        setFooterView()
        setNavigationBar()
        // setTopMenuOption()
        if DeviceType.IS_IPAD {
            constraintCollectionHeight.constant = 60
        } else {
            constraintCollectionHeight.constant = 55
        }
        
        // Show Alert By Saavvan if Location Services are Denied or disabled.
        
        //self.checkIfLocationEnabled()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .didUCreatedLead, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        selectedTab = .map
        self.collectionView.scrollToItem(at:IndexPath(item: 2, section: 0), at: .left, animated: false)
        collectionView.reloadData()
        self.navigationController?.isNavigationBarHidden = false
        
        // Show Alert By Saavvan if Location Services are Denied or disabled.
        
        self.checkIfLocationEnabled()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Check If Location Allowed

    func getAuthorization(authorization: @escaping AuthorizationHandler) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
                authorization(.authorized)
            } else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
                authorization(.denied)
            } else {
                authorization(.notDetermined)
            }
        } else {
            authorization(.disabled)
        }
    }
    
    func checkIfLocationEnabled() {
        
        getAuthorization { status in
            if status == .notDetermined {
                
                //
            } else if status == .disabled || status == .denied {
                // show alert for settings App.
                 self.showOpenSettingsAlert()
            } else {
                
                //
                
            }
        }
    }
    
    func showOpenSettingsAlert()  {
        
        let alert = UIAlertController(title: "Permission Needed", message: "Please enable Location Services in the \"Settings App\" on your device. When you allow Location Services, the App can find your location.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (_) -> Void in
            
            self.navigationController?.popViewController(animated: false)

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }))
        alert.addAction(UIAlertAction (title: "Cancel", style: .cancel, handler: { (nil) in
            
            self.navigationController?.popViewController(animated: false)
            
        }))
        self.navigationController?.present(alert, animated: true, completion: nil)
        
        /*let alertController = UIAlertController (title: "Permission Needed", message: "Please enable Location Services in the \"Settings App\" on your device. When you allow Location Services, the App can find your location.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }
        alertController.addAction(settingsAction)
        
        self.navigationController?.present(alertController, animated: true, completion: nil)*/
        
    }
    
    // MARK: - Utility
    @objc private func handleAgreement(_ notification: Notification) {
        
        
        /*if let object = notification.object as? PropertyDetails {
            
            if object.propertyType == .lead {
                
                self.nearbyFilterData.userLocation.latitude = object.lead?.latitude
                self.nearbyFilterData.userLocation.longitude = object.lead?.longitude
                
                if object.lead == nil {
                    
                    self.nearbyFilterData.userLocation.latitude = object.propertyInfo?.latitude
                    self.nearbyFilterData.userLocation.longitude = object.propertyInfo?.longitude
                    
                }
                
            }else if object.propertyType == .opportunity {
                
                self.nearbyFilterData.userLocation.latitude = object.opportunity?.latitude
                self.nearbyFilterData.userLocation.longitude = object.opportunity?.longitude
                
                if object.opportunity == nil {
                    
                    self.nearbyFilterData.userLocation.latitude = object.propertyInfo?.latitude
                    self.nearbyFilterData.userLocation.longitude = object.propertyInfo?.longitude
                    
                }
                
            } else if object.propertyType == .account {
                
                self.nearbyFilterData.userLocation.latitude = object.account?.latitude
                self.nearbyFilterData.userLocation.longitude = object.account?.longitude
                
                if object.account == nil {
                    
                    self.nearbyFilterData.userLocation.latitude = object.propertyInfo?.latitude
                    self.nearbyFilterData.userLocation.longitude = object.propertyInfo?.longitude
                    
                }
                
            } else{
                
                self.nearbyFilterData.userLocation.latitude = object.propertyInfo?.latitude
                self.nearbyFilterData.userLocation.longitude = object.propertyInfo?.longitude
                
            }

        }*/
        
        
        showLoader()
        apiCallForLead()
        apiCallForOpportunities()
        apiCallForAccounts()
        
        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            self.hideLoader()
            self.delegate?.gotEntitiesResponse(leads: self.leads, opportunities: self.opportunities, accounts: self.accounts, isLocationChange: false)
        })
    }
   
    private func apiCallForAssingedArea() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenterGeo.apiCallToGetAssignedArea(employeeID, companyKey: companyKey)
    }
    private func setNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //searchView.translatesAutoresizingMaskIntoConstraints = true
        searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width-100, height: 44)
        //searchView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width-100, height: 44)
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .clear
            txfSearchField.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
            txfSearchField.layer.masksToBounds = true
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
        searchBar.backgroundColor = .white
        searchBar.setPositionAdjustment(UIOffset(horizontal: 20, vertical: 0), for: .search)
        searchBar.layer.opacity = 1.0
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
    }
    
    private func setSegment() {
        // segment.translatesAutoresizingMaskIntoConstraints = false
        let newImage = UIImage(named: "GoogleMap")
        segment.setImage(newImage , forSegmentAt: 0)
        let appleMap = UIImage(named: "AppleMap")
        segment.setImage(appleMap , forSegmentAt: 1)
        let list = UIImage(named: "ListBlack")
        segment.setImage(list , forSegmentAt: 2)
        segment.layer.cornerRadius = 20
        segment.layer.borderWidth = 1
        segment.layer.borderColor = UIColor(red: 96/255, green: 178/255, blue: 175/255, alpha: 1).cgColor
        // segment.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)
        //  segment.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let desination as NearbyPageViewController:
            desination.delegatePage = self
            delegate = desination
            desination.nearbyFilterData = nearbyFilterData
            desination.delegateLocation = self
        default:
            break
        }
    }
    
    //Mark: Navigation Nilind
    
    private func apiCallForAssingedAreaInitial() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenterGeo.apiCallToGetAssignedArea(employeeID,companyKey: companyKey, isLoaderShow: true)
    }
    
    
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionSearch(_ sender: Any) {
        delegate?.searchLocation(isSelectedIndex, searchText: "", searchedArea: nil)
    }
    @IBAction func actionFilter(_ sender: Any) {
        
        if isInternetAvailable() {
            
            //NotificationCenter.default.post(name: Notification.Name("FilerNotify"), object: nil, userInfo: nil)
            
            let storyboard =  UIStoryboard.init(name: "Filter", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController else { return }
            controller.delegate = self
            controller.nearbyFilterData = nearbyFilterData
            self.navigationController?.pushViewController(controller, animated: true)
            
        } else {
            
            showAlertWithoutAnyAction(strtitle: Alert, strMessage: ErrorInternetMsg, viewcontrol: self)
            
        }
        

    }
    @IBAction func actionSegment(_ sender: Any) {
        searchBar.text = ""
        isSelectedIndex = segment.selectedSegmentIndex
        delegate?.tabChanged(isSelectedIndex)
    }
    // MARK: - Delegate
    override func showAlert(message: String?) {
        let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        if let first = count.first, let int = Int(first), int > 0  {
            dispatchGroup.leave()
        }
        guard let message = message else { return }
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: message, viewcontrol: self)
    }
}
// MARK: - Collection view Datasource
extension NearbyMeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HeaderViewTabs.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderTablsCollectionViewCell.self), for: indexPath) as! HeaderTablsCollectionViewCell
        cell.lblTitle.text = HeaderViewTabs(rawValue: indexPath.row)?.title
        cell.lblTitle.font = UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16)
        if selectedTab.rawValue == indexPath.row  {
            cell.lblSeprator.backgroundColor = .white
        } else {
            cell.lblSeprator.backgroundColor = .clear
        }
        return cell
    }
}
extension NearbyMeViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTab = HeaderViewTabs(rawValue: indexPath.row) ?? .map
        switch HeaderViewTabs(rawValue: indexPath.row)! {
        case .lead:
            GotoLeadViewController()
        case .opportunity:
            GotoOpportunityViewController()
        case .map:
            GotoMapViewController()
        case .tasks:
            GotoTaskViewController()
        case .activity:
            GotoActivityViewController()
        case .viewSchedule:
            GotoViewScheduleViewController()
        case .signedAgreements:
            GotoSignAgreementViewController()
        }
        
        collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = HeaderViewTabs(rawValue: indexPath.row)?.title ?? ""
        let cellWidth = text.widthOfString(usingFont: UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16))
        // let cellHeight = text.heightOfString(usingFont: UIFont.boldSystemFont(ofSize: DeviceType.IS_IPAD ? 22 : 16))
        return CGSize(width: (cellWidth + 30), height: collectionView.frame.height)
    }
}

// MARK: - Convenience
extension NearbyMeViewController {

    private func createCommonRequest() -> LocationInfo {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        var params = LocationInfo()
        params.aerialDistance = nearbyFilterData.radius.description
        params.companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        params.employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        params.longitude = nearbyFilterData.userLocation.longitude?.description ?? ConstantV.longitude.description
        params.latitude = nearbyFilterData.userLocation.latitude?.description ?? ConstantV.latitude.description
        params.visitStatus = nearbyFilterData.visits.compactMap({$0.sysName}).joined(separator: ",")
        return params
    }
    private func apiCallForLead() {
        guard nearbyFilterData.isLead else {
            leads.removeAll()
            return
        }
        var params = createCommonRequest()
        if  nearbyFilterData.leads.compactMap({$0.sysName}).joined(separator: ",").trimmed.isEmpty {
            if let leads = AppUserDefaults.shared.leadDetailMaster?.leadsStatus.filter({ (status) -> Bool in
                return status.sysName != "Converted"
            }) {
                params.leadStatus = leads.compactMap({$0.sysName}).joined(separator: ",")
            }
        } else {
            params.leadStatus = nearbyFilterData.leads.compactMap({$0.sysName}).joined(separator: ",")
        }
        dispatchGroup.enter()
        presenter.apiCallsForLeadsInfo(params)
    }
    private func apiCallForOpportunities() {
        guard  nearbyFilterData.isOpportunity else {
            opportunities.removeAll()
            return
        }
        var params = createCommonRequest()
        if  nearbyFilterData.opportunities.compactMap({$0.sysName}).joined(separator: ",").trimmed.isEmpty {
            if let leads = AppUserDefaults.shared.leadDetailMaster?.opportunitesLeadStatus.filter({ (status) -> Bool in
                return status.sysName?.lowercased() != "Completed".lowercased() && status.sysName?.lowercased() != "won".lowercased() && status.sysName?.lowercased() != "lost".lowercased() && status.sysName?.lowercased() != "void".lowercased()
            }) {
                params.opportunityStatus = leads.compactMap({$0.sysName}).joined(separator: ",")
            }
        } else {
            params.opportunityStatus = nearbyFilterData.opportunities.compactMap({$0.sysName}).joined(separator: ",")
        }
        dispatchGroup.enter()
        presenter.apiCallsForGettingOpportunities(params)
    }
    private func apiCallForAccounts() {
        guard nearbyFilterData.isAccount else {
            accounts.removeAll()
            return
        }
        let params = createCommonRequest()
        
        dispatchGroup.enter()
        presenter.apiCallsForGettingAccounts(params)
    }
   
}

// MARK: - ReportTabsDelegate
extension NearbyMeViewController: NearByTabsDelegate {
    
    func tabChanged(_ index: Int) {
        isSelectedIndex = index
        segment.selectedSegmentIndex = isSelectedIndex
    }
}
extension NearbyMeViewController: EntityView {
    func gotLeads(output: LeadResponse) {
        leads = output.leads
        let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        if let first = count.first, let int = Int(first), int > 0  {
            dispatchGroup.leave()
        }
    }
    
    func gotOppotunities(output: OpportunityResponse) {
        opportunities = output.opportunities
        let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        if let first = count.first, let int = Int(first), int > 0  {
            dispatchGroup.leave()
        }
    }
    
    func gotAccounts(output: AccountsResponse) {
        accounts = output.accounts
        let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        if let first = count.first, let int = Int(first), int > 0  {
            dispatchGroup.leave()
        }
    }
}
// MARK: - NearbyMapDelegate
extension NearbyMeViewController: NearbyMapDelegate {
    func updateNearByFilter(_ nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
    }
    func gotCurrentLocation(latitude: Double, longitude: Double) {
        gotCurrentLocation(latitude: latitude, longitude: longitude, isLocationChange: false)
    }
    func gotCurrentLocation(latitude: Double, longitude: Double, isLocationChange: Bool) {
        showLoader()
        nearbyFilterData.userLocation.latitude = latitude
        nearbyFilterData.userLocation.longitude = longitude
        apiCallForLead()
        apiCallForOpportunities()
        apiCallForAccounts()
       // apiCallForAssingedArea()
        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            self.hideLoader()
            self.delegate?.gotEntitiesResponse(leads: self.leads, opportunities: self.opportunities, accounts: self.accounts, isLocationChange: isLocationChange)
        })
    }
}
// MARK: - MapFilterDelegate
extension NearbyMeViewController: MapFilterDelegate {
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        delegate?.gotSelectedLocation(locationDetails)
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        self.nearbyFilterData.userLocation.address = locationDetails.formattedAddress
        delegate?.updateNearByFilter(nearbyFilterData)
    }
    func refreshData(_ isLocationChange: Bool, nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
        let latitude = self.nearbyFilterData.userLocation.latitude ?? ConstantV.latitude
        let longitude = self.nearbyFilterData.userLocation.longitude ?? ConstantV.longitude
        let address = self.nearbyFilterData.userLocation.address ?? ""
        self.nearbyFilterData = nearbyFilterData
        delegate?.changeMapLoction(location: CLLocation(latitude: latitude, longitude: longitude), address:  address)
        delegate?.updateNearByFilter(nearbyFilterData)
        gotCurrentLocation(latitude: latitude, longitude: longitude, isLocationChange: isLocationChange)
    }
}
// MARK: - UpdateMapDelegate
extension NearbyMeViewController: UpdateMapDelegate {
    /* func gotSelectedLocation(_ locationDetails: LocationResult) {
     self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
     self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
     self.nearbyFilterData.userLocation.address = locationDetails.formattedAddress
     // setMapMarker(locationDetails)
     }*/
}
// MARK:- SearchBarDelegate
extension NearbyMeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if isSelectedIndex == 2 {
            delegate?.searchLocation(isSelectedIndex, searchText: searchText, searchedArea: nil)
        }
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if isSelectedIndex != 2 {
            delegate?.searchLocation(isSelectedIndex, searchText: "", searchedArea: nil)
        }
        return true
    }
}

// MARK: - GeoMappingView
extension NearbyMeViewController: GeoMappingView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?) {
        if let output = output, !output.isEmpty {
        delegate?.gotShapesData(output)
        }
    }
    func gotEmployeesList(_ output: [GeoFencingD2D]?) {
        
    }
    func areaAssigned(_ output: GeoFencingD2D) {
        
    }
}
/*
 // MARK: - GeoMappingView
 extension GeoFencingViewController: GeoMappingView {
     func gotAssignedArea(_ output: [GeoFencingD2D]?) {
         self.shapesData = output
         delegate?.gotShapesData(output)
     }
     func gotEmployeesList(_ output: [GeoFencingD2D]?) { }
     func areaAssigned(_ output: GeoFencingD2D) { }
 }
 extension GeoFencingViewController: UpdateAreaD2D {
     func updateAssigneesDetail(_ output: GeoFencingD2D) { }
     func updateShapeAfterRedraw(_ output: GeoFencingD2D){
         segment.selectedSegmentIndex = 0
         isSelectedIndex = segment.selectedSegmentIndex
         delegate?.tabChanged(isSelectedIndex)
         drawShapesDelegate?.redrawSahpe(output)
     }
 }
 */
