//
//  LeadService.swift
//  DPS
//
//  Created by Vivek Patel on 22/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire


class EntityService {
    
    func leadsDetails(parameters: LocationInfo, callBack: @escaping (_ object: LeadResponse?, _ error: String?) -> Void) {
        
        AF.request(URL.Entities.getLeads, method: .post,parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers)
            .responseDecodable(of: LeadResponse.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil,error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result {
                case .success(let value):
                    print(value)
                    /*
                     do {
                     let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                     let object = try JSONDecoder().decode(LeadResponse.self, from: data)
                     print(object)
                     } catch (let error) {
                     print("MasterSalesAutomation", error.localizedDescription)
                     }*/
                    
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
    }
    func opportunitesDetails(parameters: LocationInfo, callBack: @escaping (_ object: OpportunityResponse?, _ error: String?) -> Void) {
        
        AF.request( URL.Entities.getOpportunity, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default,  headers: URL.headers )
            .responseDecodable(of: OpportunityResponse.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil,error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result {
                case .success(let value):
                    print(value)
                    do {
                        let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                        let object = try JSONDecoder().decode(OpportunityResponse.self, from: data)
                    } catch (let error) {
                        print("MasterSalesAutomation", error.localizedDescription)
                    }
                    
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
    }
    
    func accountsDetails(parameters: LocationInfo, callBack: @escaping (_ object: AccountsResponse?, _ error: String?) -> Void) {
        
        AF.request( URL.Entities.getAccounts, method: .post,parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers)
            .responseDecodable(of: AccountsResponse.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil,error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result {
                case .success(let value):
                    print(value)
                    do {
                        let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                        let object = try JSONDecoder().decode(AccountsResponse.self, from: data)
                        print(object)
                    } catch (let error) {
                        print("MasterSalesAutomation", error.localizedDescription)
                    }
                    
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
    }
}
