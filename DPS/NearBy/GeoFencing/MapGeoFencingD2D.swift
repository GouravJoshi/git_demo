//
//  MapGeoFencingD2D.swift
//  DPS
//
//  Created by APPLE on 08/03/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

// MARK: - MapGeoFencingD2D
struct MapGeoFencingD2D: Codable {
    var employeeGeoFencingAreaID: Int?
    var companyKey: String?
    var area: String?
    var companyID: Int?
    var employeeId: Int?
    var modeType: String?
    var circleRadius: String?
    var circleCenter: ShapeCordinates?
    var polygonLatLngArray: [ShapeCordinates]?
    var rectangleSouthWest: ShapeCordinates?
    var rectangleNorthEast: ShapeCordinates?
    var mapCenterLatLng: ShapeCordinates?
    var createdByName: String?
    /*
     var circleCenter: String?
     var polygonLatLngArray: String?
     var rectangleSouthWest: String?
     var rectangleNorthEast: String?*/
    var employeeExtDc: String?
    var createdDate: String?
    var createdBy: Int?
    var modifiedDate: String?
    var modifiedBy: Int?
    var mapZoom: Int?
    var isActive: Bool?
    //  var isDelete: Bool?
    var clientModifiedDate: String?
    var clientCreatedDate: String?
    var employeeGeoFencingAreaAssignmentDCS:  [EmployeeGeoFencingAreaAssignmentDc] = []
    
    enum CodingKeys: String, CodingKey {
        case employeeGeoFencingAreaID = "EmployeeGeoFencingAreaId"
        case companyKey = "CompanyKey"
        case area = "Area"
        case companyID = "CompanyId"
        case employeeId = "EmployeeId"
        case modeType = "ModeType"
        case circleCenter = "CircleCenter"
        case circleRadius = "CircleRadius"
        case polygonLatLngArray = "PolygonLatLngArray"
        case rectangleSouthWest = "RectangleSouthWest"
        case rectangleNorthEast = "RectangleNorthEast"
        case employeeExtDc = "EmployeeExtDc"
        case createdBy = "CreatedBy"
        case createdDate = "CreatedDate"
        case modifiedDate = "ModifiedDate"
        case modifiedBy = "ModifiedBy"
        case mapZoom = "MapZoom"
        case mapCenterLatLng = "MapCenterLatLng"
        case isActive = "IsActive"
        // case isDelete = "IsDelete"
        case createdByName = "CreatedByName"
        case employeeGeoFencingAreaAssignmentDCS = "EmployeeGeoFencingAreaAssignmentDcs"
        case clientCreatedDate = "ClientCreatedDate"
        case clientModifiedDate = "ClientModifiedDate"
        
    }
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        employeeGeoFencingAreaID = try (container.decodeIfPresent(Int.self, forKey: .employeeGeoFencingAreaID))
        companyKey = try (container.decodeIfPresent(String.self, forKey: .companyKey))
        companyID = try (container.decodeIfPresent(Int.self, forKey: .companyID))
        employeeId = try (container.decodeIfPresent(Int.self, forKey: .employeeId))
        area = try (container.decodeIfPresent(String.self, forKey: .area) ?? "")
        modeType = try (container.decodeIfPresent(String.self, forKey: .modeType) ?? "")
        circleRadius = try (container.decodeIfPresent(String.self, forKey: .circleRadius))
        createdByName =  try (container.decodeIfPresent(String.self, forKey: .createdByName))
        if let data =  try (container.decodeIfPresent(String.self, forKey: .polygonLatLngArray))?.data(using: .utf8) {
            do {
                polygonLatLngArray = try JSONDecoder().decode([ShapeCordinates].self, from: data)
            } catch {
                print(error)
            }
        }
        
        if let dataCircleCenter =  try (container.decodeIfPresent(String.self, forKey: .circleCenter))?.data(using: .utf8) {
            do {
                circleCenter = try JSONDecoder().decode(ShapeCordinates.self, from: dataCircleCenter)
            } catch {
                print(error)
            }
        }
        if let dataRectWest =  try (container.decodeIfPresent(String.self, forKey: .rectangleSouthWest))?.data(using: .utf8) {
            do {
                rectangleSouthWest = try JSONDecoder().decode(ShapeCordinates.self, from: dataRectWest)
            } catch {
                print(error)
            }
        }
        if let dataRectEast =  try (container.decodeIfPresent(String.self, forKey: .rectangleNorthEast))?.data(using: .utf8) {
            do {
                rectangleNorthEast = try JSONDecoder().decode(ShapeCordinates.self, from: dataRectEast)
            } catch {
                print(error)
            }
        }
        if let dataRectEast =  try (container.decodeIfPresent(String.self, forKey: .mapCenterLatLng))?.data(using: .utf8) {
            do {
                mapCenterLatLng = try JSONDecoder().decode(ShapeCordinates.self, from: dataRectEast)
            } catch {
                print(error)
            }
        }
        employeeExtDc = try (container.decodeIfPresent(String.self, forKey: .employeeExtDc))
        createdDate = try (container.decodeIfPresent(String.self, forKey: .createdDate))
        createdBy = try (container.decodeIfPresent(Int.self, forKey: .createdBy))
        modifiedDate = try (container.decodeIfPresent(String.self, forKey: .modifiedDate))
        modifiedBy = try (container.decodeIfPresent(Int.self, forKey: .modifiedBy))
        clientModifiedDate = try (container.decodeIfPresent(String.self, forKey: .clientModifiedDate))
        clientCreatedDate = try (container.decodeIfPresent(String.self, forKey: .clientCreatedDate))
        employeeGeoFencingAreaAssignmentDCS = try (container.decodeIfPresent([EmployeeGeoFencingAreaAssignmentDc].self, forKey: .employeeGeoFencingAreaAssignmentDCS) ?? [])
        isActive = try (container.decodeIfPresent(Bool.self, forKey: .isActive))
        //isDelete = try (container.decodeIfPresent(Bool.self, forKey: .isDelete))
        mapZoom = try (container.decodeIfPresent(Int.self, forKey: .mapZoom))
    }
    init() {}
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        /*try container.encodeIfPresent(polygonLatLngArray, forKey: .polygonLatLngArray)
        try container.encodeIfPresent(circleCenter, forKey: .circleCenter)
        try container.encodeIfPresent(rectangleSouthWest, forKey: .rectangleSouthWest)
        try container.encodeIfPresent(rectangleNorthEast, forKey: .rectangleNorthEast)*/
        
        try container.encodeIfPresent(employeeGeoFencingAreaID, forKey: .employeeGeoFencingAreaID)
        try container.encodeIfPresent(employeeId, forKey: .employeeId)
        try container.encodeIfPresent(modeType, forKey: .modeType)
        try container.encodeIfPresent(circleRadius, forKey: .circleRadius)
        
        try container.encodeIfPresent(employeeExtDc, forKey: .employeeExtDc)
        try container.encodeIfPresent(createdBy, forKey: .createdBy)
        try container.encodeIfPresent(createdDate, forKey: .createdDate)
        try container.encodeIfPresent(modifiedDate, forKey: .modifiedDate)
       // try container.encodeIfPresent(mapCenterLatLng, forKey: .mapCenterLatLng)
        
        try container.encodeIfPresent(mapZoom, forKey: .mapZoom)
        try container.encodeIfPresent(employeeGeoFencingAreaAssignmentDCS, forKey: .employeeGeoFencingAreaAssignmentDCS)
        try container.encodeIfPresent(companyKey, forKey: .companyKey)
        
        try container.encodeIfPresent(isActive, forKey: .isActive)
        // try container.encodeIfPresent(isDelete, forKey: .isDelete)
        try container.encodeIfPresent(modifiedBy, forKey: .modifiedBy)
        try container.encodeIfPresent(createdByName, forKey: .createdByName)
        
        try container.encodeIfPresent(companyID, forKey: .companyID)
        try container.encodeIfPresent(clientModifiedDate, forKey: .clientModifiedDate)
        try container.encodeIfPresent(clientCreatedDate, forKey: .clientCreatedDate)
        try container.encodeIfPresent(area, forKey: .area)
        
        
        do {
            let jsonData = try JSONEncoder().encode(circleCenter)
            if let circleCenterValue = String(data: jsonData, encoding: .utf8) {
                try container.encodeIfPresent(circleCenterValue, forKey: .circleCenter)
            }
            
        } catch { print(error) }
        do {
            let jsonData = try JSONEncoder().encode(polygonLatLngArray)
            if let circleCenter = String(data: jsonData, encoding: .utf8) {
                try container.encodeIfPresent(circleCenter, forKey: .polygonLatLngArray)
            }
            
        } catch { print(error) }
        do {
            let jsonData = try JSONEncoder().encode(rectangleNorthEast)
            if let circleCenter = String(data: jsonData, encoding: .utf8) {
                try container.encodeIfPresent(circleCenter, forKey: .rectangleNorthEast)
            }
            
        } catch { print(error) }
        do {
            let jsonData = try JSONEncoder().encode(rectangleSouthWest)
            if let circleCenter = String(data: jsonData, encoding: .utf8) {
                try container.encodeIfPresent(circleCenter, forKey: .rectangleSouthWest)
            }
            
        } catch { print(error) }
        do {
            let jsonData = try JSONEncoder().encode(mapCenterLatLng)
            if let circleCenter = String(data: jsonData, encoding: .utf8) {
                try container.encodeIfPresent(circleCenter, forKey: .mapCenterLatLng)
            }
            
        } catch { print(error) }
        try container.encodeIfPresent(area, forKey: .area)
    }
}
