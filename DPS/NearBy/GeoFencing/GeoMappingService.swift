//
//  GeoMappingService.swift
//  DPS
//
//  Created by Vivek Patel on 21/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class GeoMappingService {
    func getAssingArea(empId:String, companyKey: String, _ callBack:@escaping (_ object: [GeoFencingD2D]?,_ error:String?) -> Void) {
        let url = URL.GeoMapping.getAllAssigneArea.replacingOccurrences(of: "(employeeId)", with: empId).replacingOccurrences(of: "(companykey)", with: companyKey)
        print(url)
        AF.request(url, method: .get, headers: URL.headers) .responseDecodable(of: [GeoFencingD2D]?.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode([GeoFencingD2D]?.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    func assignArea(companyKey: String, parameters:GeoFencingD2D, _ callBack:@escaping (_ object: GeoFencingD2D?, _ error:String?) -> Void) {
        let url = URL.GeoMapping.addUpdateArea
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: GeoFencingD2D.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode(GeoFencingD2D?.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    func getAllEmployeesGeofencing(companyKey: String, parameters: [String?], _ callBack:@escaping (_ object: [GeoFencingD2D]?,_ error:String?) -> Void) {
        let url = URL.GeoMapping.getAllEmployees.replacingOccurrences(of: "(companyKey)", with: companyKey)
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: [GeoFencingD2D]?.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                    let object = try JSONDecoder().decode([GeoFencingD2D]?.self, from: data)
                    print(object)
                } catch (let error) {
                    print("MasterSalesAutomation", error.localizedDescription)
                }
                
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    
    func checkAssignArea(companyKey: String, employeeGeoFencingAreaId: String, parameters:[Int], _ callBack:@escaping (_ object: Bool?, _ error:String?) -> Void) {
        let url = URL.GeoMapping.checkAssignArea.replacingOccurrences(of: "(companykey)", with: companyKey).replacingOccurrences(of: "(employeeGeoFencingAreaId)", with: employeeGeoFencingAreaId)

        print(url)
        print(parameters)

        if #available(iOS 13.0, *) {

            AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Bool.self) { response in
                switch response.result {
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil, error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result{
                case .success(let value):
                    print(value)
                    do {
    //                    let data = try JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
    //                    let object = try JSONDecoder().decode(Bool?.self, from: data)
    //                    print(object)
                    } catch (let error) {
                        print("MasterSalesAutomation", error.localizedDescription)
                    }
                    
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
            
        } else {
            
            //  For iOS 12 and below devices
            AF.request(url, method: .post, parameters: parameters.dictionary, encoding: JSONEncoding.default, headers: URL.headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    callBack(true,nil)
                    break
                case .failure(_):
                    print(response.error ?? 0)
                    callBack(false, alertSomeError)
                    break
                }
            }
            
        }

    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }

}

