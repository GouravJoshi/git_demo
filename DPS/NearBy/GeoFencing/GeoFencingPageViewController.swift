//
//  GeoFencingPageViewController.swift
//  DPS
//
//  Created by Vivek Patel on 21/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class GeoFencingPageViewController: UIPageViewController {
    // MARK:- Variable
    private var pageViewControllers:[UIViewController] = []
    var delegatePage:NearByTabsDelegate?
    var index = 0
    weak var delegateLocation: NearbyMapDelegate?
    var nearbyFilterData = NearbyFilterData()
    var drawShapesDelegate: DrawingShapeDelegate?
    var delgateRedraw: UpdateAreaD2D?
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        self.view.subviews.compactMap({ $0 as? UIScrollView }).first?.delaysContentTouches = false
        self.view.subviews.compactMap({ $0 as? UIScrollView }).first?.canCancelContentTouches = false
        
        let storyboard =  UIStoryboard.init(name: "GeoFencingD2D", bundle: nil)
        
        //let page = storyboard.instantiateViewController(withIdentifier: "NearbyMapViewController") as! NearbyMapViewController
        let page =  storyboard.instantiateViewController(withIdentifier: "GeoFencingBaseViewController") as! GeoFencingBaseViewController
        
        page.delegate = self
        page.nearbyFilterData = nearbyFilterData
        pageViewControllers.append(page)
        
        let page2 = storyboard.instantiateViewController(withIdentifier: "GeoListViewController") as! GeoListViewController
        page2.delgateRedraw = self
        pageViewControllers.append(page2)
        
        guard let first  = pageViewControllers.first else{
            return
        }
        setViewControllers([first], direction: .forward, animated: false, completion: nil)
    }
}


// MARK: - UIPageViewControllerDataSource
extension GeoFencingPageViewController:UIPageViewControllerDataSource{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pageViewControllers.firstIndex(of: viewController) else { return nil }
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0  else { return nil }
        
        guard pageViewControllers.count > previousIndex else { return nil }
        
        return pageViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pageViewControllers.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pageViewControllers.count else { return nil }
        
        guard pageViewControllers.count > nextIndex else { return nil }
        
        return pageViewControllers[nextIndex]
    }
}
// MARK: - UIPageViewControllerDelegate
extension GeoFencingPageViewController:UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        if let _ = pendingViewControllers.first as? GeoFencingBaseViewController {
            //tab = source.tab
            index = 0
        }else if  let _ = pendingViewControllers.first as? GeoListViewController {
            //tab = source.tab
            index = 1
        }
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else{
            return
        }
        delegatePage?.tabChanged(index)
    }
}

// MARK:- Setting Protocol
extension GeoFencingPageViewController: NearbyDelegate {
    
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController{
            source.gotSelectedLocation(locationDetails)
        }
    }
    func changeMapLoction(location: CLLocation, address:String) {
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController{
            source.changeMapLocation(location, address: address)
        }
    }
    func gotEntitiesResponse(leads: [Lead], opportunities: [Opportunity], accounts: [Account], isLocationChange: Bool) {
        /*if let source = pageViewControllers[0] as? GeoFencingBaseViewController {
         source.isLocationChange = isLocationChange
         source.accounts = accounts
         source.opportunities = opportunities
         source.leads = leads
         }
         if let source = pageViewControllers[1] as? GeoListViewController {
         source.accounts = accounts
         source.opportunities = opportunities
         source.leads = leads
         }*/
    }
    func gotShapesData(_ output: [GeoFencingD2D]?) {
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController {
            source.shapesData = output
        }
        if let source = pageViewControllers[1] as? GeoListViewController {
            source.shapesData = output
        }
    }
    func tabChanged(_ selectedTab: Int) {
        if index < selectedTab {
            setViewControllers([pageViewControllers[1]], direction: .forward, animated: true, completion: nil)
        } else {
            setViewControllers([pageViewControllers[0]], direction: .reverse, animated: true, completion: nil)
        }
        index = selectedTab
    }
    func updateNearByFilter(_ nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController{
            source.nearbyFilterData = nearbyFilterData
        }
    }
    func searchLocation(_ selectedTab: Int, searchText: String?, searchedArea: GeoFencingD2D?) {
        if let source = pageViewControllers[selectedTab] as? GeoFencingBaseViewController {
            source.searchArea(searchedArea)
        } else  if let source = pageViewControllers[selectedTab] as? GeoListViewController {
            source.searchInList(searchText ?? "")
        }
    }
}

// MARK: - NearbyMapDelegate
extension GeoFencingPageViewController: NearbyMapDelegate {
    func gotCurrentLocation(latitude: Double, longitude: Double) {
        delegateLocation?.gotCurrentLocation(latitude: latitude, longitude: longitude)
    }
}
extension GeoFencingPageViewController: DrawingShapeDelegate {
    func drawShapes()
    {
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController
        {
            if nsud.bool(forKey: "ShowAreaButton") == true
            {
                source.isDrawShape = true
            }
            else
            {
                source.isDrawShape = false
            }
            //source.isDrawShape = true
        }
    }
    func redrawSahpe(_ shapeData: GeoFencingD2D) {
        if let source = pageViewControllers[0] as? GeoFencingBaseViewController
        {
            if nsud.bool(forKey: "ShowAreaButton") == true
            {
                source.isDrawShape = true
                source.actionRedrawShape(shapeData)
            }
            else
            {
                source.isDrawShape = false
                source.actionRedrawShape(shapeData)
            }
//            source.isDrawShape = true
//            source.actionRedrawShape(shapeData)
        }
    }
}
extension GeoFencingPageViewController: UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D) { }
    func updateShapeAfterRedraw(_ output: GeoFencingD2D){
        delgateRedraw?.updateShapeAfterRedraw(output)
    }
}

