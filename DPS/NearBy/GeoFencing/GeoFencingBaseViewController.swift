//
//  GeoFencingBaseViewController.swift
//  DPS
//
//  Created by Vivek Patel on 15/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import GLKit


enum ShapeType {
    case circle
    case rectangle
    case polygon
    var title: String {
        switch self {
        case .circle:
            return "circle"
        case .polygon:
            return "polygon"
        case .rectangle:
            return "rectangle"
        }
    }
}
final class GeoFencingBaseViewController: BaseViewController {
    // MARK: - Variables
    
    weak var delegate: NearbyMapDelegate?
    private var mapView: GMSMapView!
    private let locationService = LocationService.instance
    private var kCameraLatitude = 40.692167
    private var kCameraLongitude = -102.664972
    var nearbyFilterData = NearbyFilterData()
    var places: [CLLocation] = []
    var annotations: [GMSMarker] = []
    var ispolygonExist  = false
    var circleLocation: CLLocationCoordinate2D?
    var currentRadius = 30
    var rectangleSouthWest: CLLocation?
    var rectangleNorthEast: CLLocation?
    var shapeType:ShapeType = .rectangle
    var centerMapCoordinate:CLLocationCoordinate2D!
    var shapesData: [GeoFencingD2D]? {
        didSet {
            mapView?.clear()
            guard let shapesData = shapesData else { return }
            drawShape(shapesData)
        }
    }
    var redrawShapeData: GeoFencingD2D?
    var isDrawShape = false {
        didSet
        {
            if isDrawShape
            {
                mapView?.clear()
                drawShape(shapesData)
                viewShapes?.isHidden = false
            }
            else
            {
                viewShapes?.isHidden = true
            }
            viewRadiusStepper.isHidden = true
            viewButtons.isHidden = true
            mapView?.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var txtRadius: UITextField!
    @IBOutlet weak var radiusStepper: UIStepper!
    @IBOutlet weak var lblOpportunityCount: UILabel!
    @IBOutlet weak var lblLeadsCount: UILabel!
    @IBOutlet weak var btnRectangle: UIButton!
    @IBOutlet weak var btnCircle: UIButton!
    @IBOutlet weak var btnPolygone: UIButton!
    @IBOutlet weak var lblAccountCount: UILabel!
    @IBOutlet weak var searchBar: UIButton!
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnNavigation: UIButton!
    @IBOutlet weak var btnLayer: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var viewShapes: UIStackView!
    @IBOutlet weak var viewRadiusStepper: UIView!
    @IBOutlet weak var viewButtons: UIStackView!
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GeoMapping"
        setGoogleMap(typee: "firstTime")
        setUI()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshFromDetail), name: .refreshMapOnCancelD2D, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshNewArea), name: .refreshNewAreaD2D, object: nil)

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        // presenter.apiCallToGetAllEmployeeGeoFencings()
    }
    
    @objc private func refreshNewArea() {
        redrawShapeData = nil
    }
    
    @objc private func refreshFromDetail() {
  
        setGoogleMap(typee: "OnRefresh")
        
        if(nsud.value(forKey: "filterAssignArea") != nil){
            
            if(nsud.value(forKey: "filterAssignArea") is NSDictionary){
                
                let dictFilterData = nsud.value(forKey: "filterAssignArea") as! NSDictionary
                
                if dictFilterData.count > 0  {
                    
                    let lat = Double("\(dictFilterData.value(forKey: "latitude")!)")
                    let long = Double("\(dictFilterData.value(forKey: "longitude")!)")
                    let address = "\(dictFilterData.value(forKey: "address")!)"
                    
                    changeMapLocation(CLLocation(latitude: lat!, longitude: long!), address: address)
                    
                }
                                                
            }
            
        }
        
    }
    
    
    //MARK: - Actions
    @IBAction func actionFilter(_ sender: Any) {
        /*   let storyboard =  UIStoryboard.init(name: "Filter", bundle: nil)
         guard let controller = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController else { return }
         controller.delegate = self
         controller.nearbyFilterData = nearbyFilterData
         self.navigationController?.pushViewController(controller, animated: true)*/
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionSetting(_ sender: Any) {
        mapView.mapType = .satellite
    }
    
    @IBAction func actionLayer(_ sender: Any) {
        mapView.mapType = .hybrid
    }
    @IBAction func actionStepper(_ sender: UIStepper) {
        mapView.clear()
        drawShape(shapesData)
        txtRadius.text = sender.value.description
        currentRadius =  Int(sender.value)
        addCircle(circleLocation!, radius: currentRadius)
    }
    
    @IBAction func actioNavigation(_ sender: Any) {
        mapView.mapType = .terrain
    }
    @IBAction func actionRectangle(_ sender: Any) {
        
        btnRectangle.isSelected = true
        btnCircle.isSelected = false
        btnPolygone.isSelected = false
        clearMap()
        viewRadiusStepper.isHidden = true
        shapeType = .rectangle
    }
    @IBAction func actionCircle(_ sender: Any) {
        btnCircle.isSelected = true
        btnPolygone.isSelected = false
        btnRectangle.isSelected = false
        
        clearMap()
        shapeType = .circle
    }
    @IBAction func actionPolygon(_ sender: Any) {
        btnPolygone.isSelected = true
        btnRectangle.isSelected = false
        btnCircle.isSelected = false
        viewRadiusStepper.isHidden = true
        clearMap()
        shapeType = .polygon
    }
    @IBAction func actionSearch(_ sender: Any) {
        let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as? SearchPlaceViewController  else { return }
        //controller.modalPresentationStyle = .overFullScreen
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    @IBAction func actionSave(_ sender: Any) {
        // generateRequest()
    }
    @IBAction func actionAssign(_ sender: Any) {
        
        self.assignTap()
        
        /*  let storyboard =  UIStoryboard.init(name: "Sales", bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: "AssignSalesPersonViewController") as! AssignSalesPersonViewController
         
         controller.geofencingDetails = parameters
         controller.delegate = self
         self.navigationController?.pushViewController(controller, animated: true)*/
    }
    
    func assignTap() {
        let storyboard =  UIStoryboard.init(name: "GeoFencingD2D", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "AddAreaViewController") as? AddAreaViewController else { return }
        let parameters = generateRequest()
        controller.geofencingDetails = parameters
        controller.delegate = self
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func actionRedraw(_ sender: Any) {
        clearMap()
        viewShapes.isHidden = false
        viewButtons.isHidden = true
        mapView?.isUserInteractionEnabled = true
    }
    // MARK:- Convinienece
    func searchArea(_ searchedArea: GeoFencingD2D?) {
        guard let searchedArea = searchedArea else { return }
        if let mapLat = searchedArea.mapCenterLatLng?.lat ,  let mapLng = searchedArea.mapCenterLatLng?.lng {
            self.mapView?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(searchedArea.mapZoom ?? 18)))
        }
    }
    func actionRedrawShape(_ output: GeoFencingD2D) {
        isDrawShape = true
        guard var shapesData = shapesData else { return }
        self.redrawShapeData = output
        if let index = shapesData.firstIndex(where: {$0.employeeGeoFencingAreaID == output.employeeGeoFencingAreaID}) {
            shapesData.remove(at: index)
            self.shapesData = shapesData
            mapView?.clear()
            drawShape(shapesData)
        }
    }
    private func clearMap() {
        places.removeAll()
        annotations.removeAll()
        mapView.clear()
        drawShape(shapesData)
        ispolygonExist  = false
    }
    private func setMapMarker(_ locationDetails: LocationResult) {
        let marker = GMSMarker()
        self.mapView?.camera = GMSCameraPosition(latitude: locationDetails.geometry.location.latitude, longitude: locationDetails.geometry.location.longitude, zoom: 18)
        mapView.mapType = .hybrid
        marker.position = CLLocationCoordinate2D(latitude: locationDetails.geometry.location.latitude, longitude:  locationDetails.geometry.location.longitude)
        marker.title = locationDetails.formattedAddress
        marker.snippet = locationDetails.formattedAddress
        let image =  UIImage(named: "Group 758")
        marker.icon = image?.maskWithColor(color: .systemRed)
        marker.map = self.mapView
    }
    private func setUI() {
        searchBar.layer.cornerRadius = 0
        AppUtility.buttonImageColor(btn: btnSetting, image: "Group 772", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnNavigation, image: "Group 765", color: .appDarkGreen)
        AppUtility.buttonImageColor(btn: btnLayer, image: "Group 768", color: .appDarkGreen)
        viewShapes.isHidden = true
        mapView.delegate = self
        mapView.isUserInteractionEnabled = true
        radiusStepper.value = 30
        txtRadius.text = "30"
        txtRadius.delegate = self
        viewRadiusStepper.isHidden = true
        btnRectangle.isSelected = true
    }
    private func generateRequest() -> GeoFencingD2D {
        var parameters = GeoFencingD2D()
        switch shapeType {
        case .circle:
            var circleCenter = ShapeCordinates()
            circleCenter.lat = circleLocation?.latitude
            circleCenter.lng = circleLocation?.longitude
            parameters.circleCenter = circleCenter
            parameters.circleRadius = currentRadius.description
        case .polygon:
            print("polygone")
            var polygonLatLngArray = [ShapeCordinates()]
            print(polygonLatLngArray)
            polygonLatLngArray.removeAll()
            print(polygonLatLngArray)

            for place in places {
                var tempArray = ShapeCordinates()
                tempArray.lat = place.coordinate.latitude
                tempArray.lng = place.coordinate.longitude
                polygonLatLngArray.append(tempArray)
            }
            parameters.polygonLatLngArray = polygonLatLngArray
        case .rectangle:
            
            if let rectangleNorthEast = rectangleNorthEast, let rectangleSouthWest = rectangleSouthWest {
                var rectangleNorthEastPara = ShapeCordinates()
                rectangleNorthEastPara.lat = rectangleNorthEast.coordinate.latitude
                rectangleNorthEastPara.lng = rectangleNorthEast.coordinate.longitude
                parameters.rectangleNorthEast = rectangleNorthEastPara
                var rectangleSouthWestPara = ShapeCordinates()
                rectangleSouthWestPara.lat = rectangleSouthWest.coordinate.latitude
                rectangleSouthWestPara.lng = rectangleSouthWest.coordinate.longitude
                parameters.rectangleSouthWest = rectangleSouthWestPara
            }
        }
        parameters.modeType = shapeType.title
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let createdBy =  dictLoginData.value(forKey: "CreatedBy") as! Int
        let hrmsCompanyId = "\(dictLoginData.value(forKeyPath: "Company.HrmsCompanyId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let empId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        parameters.isActive = true
        // parameters.isDelete = false
        parameters.mapZoom = Int(self.mapView.camera.zoom)
        var mapCenterLatLng = ShapeCordinates()
        mapCenterLatLng.lat = centerMapCoordinate.latitude
        mapCenterLatLng.lng = centerMapCoordinate.longitude
        parameters.mapCenterLatLng = mapCenterLatLng
        parameters.companyID = Int(hrmsCompanyId)
        parameters.companyKey = companyKey
        // parameters.employeeExtDc =
        //  parameters.modifiedBy = ""
        parameters.modifiedDate = ""
        if let redrawShapeData = redrawShapeData {
            parameters.employeeGeoFencingAreaAssignmentDCS = redrawShapeData.employeeGeoFencingAreaAssignmentDCS
            parameters.area = redrawShapeData.area
            parameters.modifiedBy = Int(empId)
            parameters.modifiedDate = AppUtility.convertDateToString(Date(), toFormat:ConstantV.modifiedDateFormat)
            parameters.employeeGeoFencingAreaID = redrawShapeData.employeeGeoFencingAreaID
            parameters.createdBy = redrawShapeData.createdBy
            parameters.createdDate =  redrawShapeData.createdDate
        
        } else {
            parameters.createdBy = createdBy
            parameters.createdDate =  AppUtility.convertDateToString(Date(), toFormat:ConstantV.modifiedDateFormat)
            parameters.employeeGeoFencingAreaID = 0
        }
        return parameters
    }
    
    private func addCircle(_ location: CLLocationCoordinate2D, radius:Int = 30) {
        viewRadiusStepper.isHidden = false
        let circle = GMSCircle(position: location, radius: CLLocationDistance(radius))
        circle.strokeColor = .white
        circle.strokeWidth = 4
        circle.isTappable = false
        circle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
        circle.map = mapView
        viewShapes.isHidden = true
        viewButtons.isHidden = true
        mapView?.isUserInteractionEnabled = false
    }
    
    private func addRectangle() {
        
        if let southLat = places.first?.coordinate.latitude, let sounthLong = places.first?.coordinate.longitude, let northLat = places.last?.coordinate.latitude ,let northLong = places.last?.coordinate.longitude {
            
            let rectanglePath = GMSMutablePath()
            rectanglePath.add(CLLocationCoordinate2D(latitude: southLat, longitude: sounthLong))
            rectanglePath.add(CLLocationCoordinate2D(latitude: northLat, longitude: northLong))
            rectanglePath.add(CLLocationCoordinate2D(latitude: southLat, longitude: northLong))
            rectanglePath.add(CLLocationCoordinate2D(latitude: northLat, longitude: sounthLong))
            let location1 = CLLocation(latitude: southLat, longitude: sounthLong)
            let location2 = CLLocation(latitude: southLat, longitude: northLong)
            let location3 = CLLocation(latitude: northLat, longitude: northLong)
            let location4 = CLLocation(latitude: northLat, longitude: sounthLong)
            rectangleSouthWest = location1
            rectangleNorthEast = location3
            addAnnotation(location1)
            addAnnotation(location3)
            addAnnotation(location2)
            addAnnotation(location4)
            places.append(location1)
            places.append(location2)
            places.append(location3)
            places.append(location4)
            ispolygonExist = true
            self.assignTap()
            addPolygone()
            
        }
    }
    func addPolylines() {
        let locations = places.map { $0.coordinate }
        let rect = GMSMutablePath()
        for location in locations {
            rect.add(CLLocationCoordinate2D(latitude:location.latitude, longitude: location.longitude))
        }
        let ployLine = GMSPolyline(path: rect)
        ployLine.strokeColor = .white
        ployLine.strokeWidth = 4
        ployLine.map = mapView
        
        ispolygonExist = false
    }
    func addPolygone() {
        mapView.clear()
        drawShape(shapesData)
        addAllAnnotations()
        let locations = places.map { $0.coordinate }
        let rect = GMSMutablePath()
        for location in locations {
            rect.add(CLLocationCoordinate2D(latitude:location.latitude, longitude: location.longitude))
        }
        let polygon = GMSPolygon(path: rect)
        polygon.fillColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        polygon .strokeColor = .white
        polygon.strokeWidth = 4
        polygon.map = mapView
        polygon.isTappable = false
        viewShapes.isHidden = true
        viewButtons.isHidden = true
        mapView?.isUserInteractionEnabled = false
    }
    func addAnnotation(_ location: CLLocation) {
        let annotation = GMSMarker()
        let position = CLLocationCoordinate2D(latitude: (location.coordinate.latitude), longitude: location.coordinate.longitude)
        annotation.position = position
        annotation.icon = UIImage(named: "MappingDot")
        annotation.map = mapView
        annotations.append(annotation)
    }
    func addAllAnnotations() {
        for annotation in annotations {
            annotation.map = mapView
        }
    }
    func drawImageOnAnotation(shape: GeoFencingD2D)-> UIView{
           let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 80))
           view.backgroundColor = UIColor.clear
           let viewimage = UIView()

           var xpoint = 0
           for item in shape.employeeGeoFencingAreaAssignmentDCS {
             
               let image = UIImageView()
               image.layer.cornerRadius = 20
               image.contentMode = .scaleAspectFill
               image.layer.masksToBounds = false
               image.clipsToBounds = true
               image.layer.borderWidth=1.0
               image.layer.borderColor = UIColor.white.cgColor
               image.frame = CGRect(x: xpoint, y: 0, width: 40, height: 40)
               image.contentMode = .scaleAspectFit

               xpoint = xpoint + Int(image.frame.width)/2
             
               let employeeID = "\(item.employeeID!)"
               var employeePhoto = ""
               print("EmployeeID-----\(employeeID)")
               print("employeePhoto-----\(employeePhoto)")
               
               if(item.employeePhoto != nil){
                   let listItems = "\(item.employeePhoto!)".components(separatedBy: "EmployeeImagesEmployeeImages")
                   if(listItems.count != 0){
                       employeePhoto = "\(listItems[1])"
                       let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary

                       let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.HrmsServiceModule.ServiceUrl")!)Documents/EmployeeImages/" + employeePhoto

                       image.setImageWith(URL(string: strURL), placeholderImage: UIImage(named: "contact_person_details"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)

                   }
               }else{
                   image.image = UIImage(named: "contact_person_details")

               }
              
               viewimage.addSubview(image)
           }
           viewimage.frame =  CGRect(x: 0, y: Int(view.frame.maxY) - 40, width: ((shape.employeeGeoFencingAreaAssignmentDCS.count * 20)+20), height: 40)
           viewimage.center.x = view.center.x

           let label = UILabel(frame: CGRect(x: 0, y:Int(view.frame.maxY) - 70, width: 100 , height: 40))
           label.center.x = view.center.x
           label.font = UIFont.boldSystemFont(ofSize: 18)
           label.textAlignment = .center
           label.textColor = .white
           label.numberOfLines = 2
           label.text = shape.area
           view.addSubview(viewimage)
           view.addSubview(label)
           view.isUserInteractionEnabled = false
           return view
       }
}

// MARK: - GMUClusterManagerDelegate & GMSMapViewDelegate
extension GeoFencingBaseViewController: GMSMapViewDelegate, GMUClusterManagerDelegate {
    func mapView(_ mapViewL: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if shapeType == .polygon {
            if annotations.count > 2 {
                mapView.clear()
                drawShape(shapesData)
                ispolygonExist = true
                addPolygone()
                self.assignTap()
            }
        }
        return false
    }
    func mapView(_ mapViewL: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        if isDrawShape {
            switch shapeType {
            case .circle:
                mapView.clear()
                drawShape(shapesData)
                circleLocation = coordinate
                radiusStepper.value = 30
                txtRadius.text = "30"
                currentRadius = 30
                addCircle(coordinate)
            case .polygon:
                if places.contains(location) {
                    if places.count > 2 {
                        addPolygone()
                    }
                } else {
                    if ispolygonExist {
                        places.removeAll()
                        annotations.removeAll()
                        mapView.clear()
                        drawShape(shapesData)
                    }
                    places.append(location)
                    addAnnotation(location)
                    addPolylines()
                }
            case . rectangle:
                if ispolygonExist {
                    places.removeAll()
                    annotations.removeAll()
                    mapView.clear()
                    drawShape(shapesData)
                    ispolygonExist = false
                }
                addAnnotation(location)
                places.append(location)
                if places.count == 2 {
                    annotations.removeAll()
                    mapView.clear()
                    drawShape(shapesData)
                    addRectangle()
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        //   mapView.clear()
        //  drawShape(shapesData)
    }
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        if overlay is GMSCircle {
            print("crircle tapped")
            
        } else if overlay is GMSPolygon {
            print("polygon tapped")
        }
        else if overlay is GMSPolyline {
            print("Polyline tapped")
            
        }
        let storyboard = UIStoryboard(name: "GeoFencingD2D", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AssignAreatDetailViewController") as? AssignAreaDetailViewController else {
            return
        }
        
        let navController = UINavigationController.init(rootViewController: destination)
        navController.isNavigationBarHidden = true
        navController.modalPresentationStyle = .overFullScreen
        if let userData = overlay.userData as! GeoFencingD2D? {
            destination.shapeData =  userData
        }
        destination.delgateRedraw = self
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
}
// MARK: - Map Convenience
extension GeoFencingBaseViewController {
    private func setGoogleMap(typee : String) {
        annotations.removeAll()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        GMSServices.provideAPIKey(apiKey)
        let camera = GMSCameraPosition.camera(withLatitude: kCameraLatitude,
                                              longitude: kCameraLongitude, zoom: 18)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView?.delegate = self
        mapView?.mapType = .satellite
        mapView?.isMyLocationEnabled = true
        self.viewMap.addSubview(mapView ?? UIView())
        mapView?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: mapView as Any, attribute: $0, relatedBy: .equal, toItem: mapView?.superview, attribute: $0, multiplier: 1, constant: 0)
        })
        //  setUpCluster()
        
        if typee == "firstTime" {
            
            getCurrentLocation()
            
        }
                
    }
    private func drawShape(_ shapes: [GeoFencingD2D]?) {
        guard let shapes = shapes else {
            return
        }
        for shape in shapes {
            if shape.modeType == "polygon"
            {
                let path = GMSMutablePath()
                var polygonCoordinates: [CLLocationCoordinate2D] = []
                if let polygonLatLngArray = shape.polygonLatLngArray {
                    for cordiantes in polygonLatLngArray {
                        if let lat =  cordiantes.lat ,  let lng = cordiantes.lng {
                            path.add(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                            polygonCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lng))
                        }
                    }
                    let polygon = GMSPolygon(path: path)
                    polygon.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                    polygon.strokeColor = .white
                    polygon.strokeWidth = 3
                    polygon.map = mapView
                    polygon.isTappable = true
                    polygon.userData = shape
                    
                    /* if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                     polygon.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                     }*/
                    //    if Int(self.mapView.camera.zoom) > (shape.mapZoom ?? 18 - 2) {
                    let polygonCenter = getCenterCoordOfPolygon(polygonCoordinates)
                    
                    let annotation = GMSMarker()
                    let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
                    annotation.position = position
                    annotation.iconView = drawImageOnAnotation(shape: shape)
                    annotation.map = mapView
                    
                }
            }
            else if  shape.modeType == "circle"
            {
                if let lat =  shape.circleCenter?.lat, let lng = shape.circleCenter?.lng, let radiusString = shape.circleRadius, let radius = Double(radiusString)  {
                    let circleCenter = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    
                    let circle = GMSCircle(position: circleCenter, radius: radius)
                    circle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                    circle.strokeColor = .white
                    circle.strokeWidth = 3
                    circle.map = mapView
                    circle.userData = shape
                    circle.isTappable = true
                    /* if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                     circle.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                     }*/
                    let annotation = GMSMarker()
                    let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    annotation.position = position
                    annotation.iconView = drawImageOnAnotation(shape: shape)
                    annotation.map = mapView
                }
                
            }
            else if  shape.modeType == "rectangle"
            {
                let rectanglePath = GMSMutablePath()
                guard let rectangleSouthWest = shape.rectangleSouthWest, let rectangleNorthEast = shape.rectangleNorthEast, let latSouth =  rectangleSouthWest.lat, let lngSouth = rectangleSouthWest.lng, let latNorth =  rectangleNorthEast.lat, let lngNorth = rectangleNorthEast.lng  else { return }
                rectanglePath.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
                rectanglePath.add(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
                var rectangleCoordinates: [CLLocationCoordinate2D] = []
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngSouth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latSouth, longitude: lngNorth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngNorth))
                rectangleCoordinates.append(CLLocationCoordinate2D(latitude: latNorth, longitude: lngSouth))
                let rectangle = GMSPolygon(path: rectanglePath)
                /* if let mapLat = shape.mapCenterLatLng?.lat ,  let mapLng = shape.mapCenterLatLng?.lng {
                 rectangle.map?.animate(to: GMSCameraPosition(latitude: mapLat, longitude: mapLng, zoom: Float(shape.mapZoom ?? 18)))
                 }*/
                rectangle.fillColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
                rectangle.strokeColor = .white
                rectangle.strokeWidth = 3
                rectangle.map = mapView
                rectangle.userData = shape
                rectangle.isTappable = true
                let polygonCenter = getCenterCoordOfPolygon(rectangleCoordinates)
                let annotation = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: (polygonCenter.latitude), longitude: polygonCenter.longitude)
                annotation.position = position
                annotation.iconView = drawImageOnAnotation(shape: shape)
                annotation.map = mapView
                
            }
        }
    }
    
    private func getCurrentLocation() {
        locationService.getLocation { location in
            guard let location = location else {
                // self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
                return
            }
            self.kCameraLatitude = location.coordinate.latitude
            self.kCameraLongitude = location.coordinate.longitude
            self.nearbyFilterData.userLocation.latitude = location.coordinate.latitude
            self.nearbyFilterData.userLocation.longitude = location.coordinate.longitude
            //    self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
            self.mapView?.camera = GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
        }
    }
    func changeMapLocation(_ location: CLLocation, address:String) {
        let marker = GMSMarker()
        self.mapView?.camera = GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.title = address
        marker.snippet = address
        let image =  UIImage(named: "Group 758")
        marker.icon = image?.maskWithColor(color: .systemRed)
        marker.map = self.mapView
        mapView.animate(to: GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18))
    }
}
// MARK: - MapFilterDelegate
extension GeoFencingBaseViewController: MapFilterDelegate, UpdateMapDelegate {
    func refreshData(_ isLocationChange: Bool, nearbyFilterData: NearbyFilterData) { }
    
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        setMapMarker(locationDetails)
        // delegate?.gotSelectedLocation(locationDetails)
    }
    
}
// MARK: - UITextFieldDelegate
extension GeoFencingBaseViewController:  UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            
            
            let characterSet = CharacterSet(charactersIn: string)
            
            if textField == txtRadius  {
                let repeatingDecimalCount = updatedText.filter{$0 == "."}.count
                let allowedCharacters = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "."))
                
                guard allowedCharacters.isSuperset(of: characterSet) && repeatingDecimalCount <= 1 else {
                    return false
                }
                let radius = Int(updatedText) ?? 0
                mapView.clear()
                drawShape(shapesData)
                radiusStepper.value = Double(radius)
                self.addCircle(circleLocation!, radius: radius)
            }
        }
        return true
    }
}
extension GeoFencingBaseViewController: MapViewNavigationDelegate {
    func actionBack()  {
        btnRectangle.isSelected = true
        btnCircle.isSelected = false
        btnPolygone.isSelected = false
        clearMap()
        viewRadiusStepper.isHidden = true
        shapeType = .rectangle
        viewButtons.isHidden = true
        viewShapes.isHidden = false
        mapView?.isUserInteractionEnabled = true
    }
}
extension GeoFencingBaseViewController: OpenAreaDetailProtocol {
    func openAreaDeatil(_ shapeData: GeoFencingD2D) {
        let storyboard = UIStoryboard(name: "GeoFencingD2D", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AssignAreatDetailViewController") as? AssignAreaDetailViewController else {
            return
        }
        //let parameters = generateRequest()
        //controller.geofencingDetails = parameters
        // controller.delegate = self
        self.mapView.isUserInteractionEnabled = true
        isDrawShape = false
        NotificationCenter.default.post(name: .updatedGeoAreaDataD2D, object: nil)
        destination.shapeData = shapeData
        destination.delgateRedraw = self
        let navController = UINavigationController.init(rootViewController: destination)
        navController.isNavigationBarHidden = true
        navController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
}
extension GeoFencingBaseViewController: UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D) { }
    func updateShapeAfterRedraw(_ output: GeoFencingD2D){
        isDrawShape = true
        guard var shapesData = shapesData else { return }
        self.redrawShapeData = output
        if let index = shapesData.firstIndex(where: {$0.employeeGeoFencingAreaID == output.employeeGeoFencingAreaID}) {
            shapesData.remove(at: index)
            self.shapesData = shapesData
            mapView.clear()
            drawShape(shapesData)
        }
    }
}

