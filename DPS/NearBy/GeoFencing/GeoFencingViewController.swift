//
//  GeoFencingViewController.swift
//  DPS
//
//  Created by Vivek Patel on 21/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import DropDown

protocol DrawingShapeDelegate {
    func drawShapes()
    func redrawSahpe(_ shapeData: GeoFencingD2D)
}
final class GeoFencingViewController: BaseViewController {
    // MARK: - Variables
    private lazy var presenterGeo = {
        return GeoMappingPresenter(self)
    }()
    var shapesData: [GeoFencingD2D]?
    let dropDown = DropDown()
    private var isSelectedIndex = 0
    weak var delegate: NearbyDelegate?
    let dispatchGroup = DispatchGroup()
    private var leads: [Lead] = []
    private var opportunities: [Opportunity] = []
    private var accounts: [Account] = []
    var nearbyFilterData = NearbyFilterData()
    var drawShapesDelegate: DrawingShapeDelegate?
    var dictFilterData = NSMutableDictionary()

    // MARK: - Outlets
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnAddArea: UIBarButtonItem!
    @IBOutlet weak var btnFilter: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet var btnCancel: UIButton!

    @IBOutlet var lblAreaAssignment: UILabel!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nsud.set(NSDictionary(), forKey: "filterAssignArea")

        NotificationCenter.default.addObserver(self, selector: #selector(apiCallForAssingedArea), name: .updatedGeoAreaDataD2D, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshFromDetail), name: .refreshMapOnCancelD2D, object: nil)

        setSegment()
        setFooterView()
        setNavigationBar()
        apiCallForAssingedAreaInitial()
        DropDown.startListeningToKeyboard()
        btnCancel.isHidden = true
        lblAreaAssignment.text! = "Area Assignment"
        nsud.setValue(false, forKey: "ShowAreaButton")
        nsud.synchronize()

    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: .updatedGeoAreaDataD2D, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    // MARK: - Covinience
    @objc private func apiCallForAssingedArea() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenterGeo.apiCallToGetAssignedArea(employeeID,companyKey: companyKey, isLoaderShow: false)
    }
    
    @objc private func refreshFromDetail() {
        self.cancelTap()
    }
    
    private func apiCallForAssingedAreaInitial() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeID = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        presenterGeo.apiCallToGetAssignedArea(employeeID,companyKey: companyKey, isLoaderShow: true)
    }
    func openAddressDropDown(_ searchedText: String) {
        guard let shapesData = shapesData else { return }
        if  shapesData.count > 0 {
            var rows:[String] = []
            let sortedData = shapesData.filter({ (row) -> Bool in
                return (row.area?.localizedCaseInsensitiveContains(searchedText)) ?? false
            })
            for (_,row) in sortedData.enumerated() {
                if let area = row.area {
                    rows.append(area)
                }
            }
            dropDown.dataSource = rows
            dropDown.anchorView = searchBar
            dropDown.bottomOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
            dropDown.show()
            dropDown.selectionAction = { [weak self] (index: Int, item: String) in
                guard let self = self else { return }
                self.delegate?.searchLocation(self.isSelectedIndex, searchText:"" , searchedArea: sortedData[index])
            }
        } else {
            dropDown.hide()
        }
    }
    
    private func setSegment() {
        segment.layer.cornerRadius = 20
        segment.layer.borderWidth = 1
        segment.layer.borderColor = UIColor(red: 96/255, green: 178/255, blue: 175/255, alpha: 1).cgColor
        segment.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)
        segment.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
    }
    private func setNavigationBar()
    {
        self.navigationController?.navigationBar.tintColor = .appThemeColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width-140, height: 44)
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .clear
            txfSearchField.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
            txfSearchField.layer.masksToBounds = true
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
        searchBar.backgroundColor = .white
        searchBar.setPositionAdjustment(UIOffset(horizontal: 20, vertical: 0), for: .search)
        searchBar.layer.opacity = 1.0
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        //Nilind
        
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let desination as GeoFencingPageViewController:
            desination.delegatePage = self
            delegate = desination
            drawShapesDelegate = desination
            desination.delgateRedraw = self
            desination.nearbyFilterData = nearbyFilterData
            desination.delegateLocation = self
        default:
            break
        }
    }
    
    // MARK: - Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionAddArea(_ sender: Any)
    {
        //Nilind
        self.segment.isHidden = true
        lblAreaAssignment.text! = "Create Area"
        nsud.setValue(true, forKey: "ShowAreaButton")
        nsud.synchronize()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        btnCancel.isHidden = false
        //
        
        if isSelectedIndex == 1 {
            segment.selectedSegmentIndex = 0
            isSelectedIndex = segment.selectedSegmentIndex
            delegate?.tabChanged(isSelectedIndex)
        }
        drawShapesDelegate?.drawShapes()
        NotificationCenter.default.post(name: .refreshNewAreaD2D, object: nil)

    }
    @IBAction func actionFilter(_ sender: Any) {
        let storyboard =  UIStoryboard.init(name: "Filter", bundle: nil)
           guard let controller = storyboard.instantiateViewController(withIdentifier: "FIlterAssignArea") as? FIlterAssignArea else { return }
           controller.delegate = self
          if(dictFilterData.count != 0){
              controller.latitude = Double("\(dictFilterData.value(forKey: "latitude")!)")
              controller.longitude = Double("\(dictFilterData.value(forKey: "longitude")!)")
              controller.address = "\(dictFilterData.value(forKey: "address")!)"
          }
           self.navigationController?.pushViewController(controller, animated: true)
          
      }
    @IBAction func actionSegment(_ sender: Any) {
        isSelectedIndex = segment.selectedSegmentIndex
        delegate?.tabChanged(isSelectedIndex)
    }
    
    @IBAction func actionOnCancel(_ sender: Any) {
        self.cancelTap()
    }
    
    func cancelTap() {
        self.apiCallForAssingedArea()

        lblAreaAssignment.text! = "Area Assignment"
        nsud.setValue(false, forKey: "ShowAreaButton")
        nsud.synchronize()
        
        drawShapesDelegate?.drawShapes()

        self.navigationController?.setNavigationBarHidden(false, animated: false)
        btnCancel.isHidden = true
        self.segment.isHidden = false
    }
    
    // MARK: - Delegate
    override func showAlert(message: String?) {
        let count = self.dispatchGroup.debugDescription.components(separatedBy: ",").filter({$0.contains("count")}).first!.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({Int($0) != nil})
        if let first = count.first, let int = Int(first), int > 0  {
            dispatchGroup.leave()
        }
        guard let message = message else { return }
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: message, viewcontrol: self)
    }
}


// MARK: - NearByTabsDelegate
extension GeoFencingViewController: NearByTabsDelegate {
    
    func tabChanged(_ index: Int) {
        isSelectedIndex = index
        segment.selectedSegmentIndex = isSelectedIndex
    }
}

// MARK: - NearbyMapDelegate
extension GeoFencingViewController: NearbyMapDelegate {
    func updateNearByFilter(_ nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
    }
    func gotCurrentLocation(latitude: Double, longitude: Double) {
        gotCurrentLocation(latitude: latitude, longitude: longitude, isLocationChange: false)
    }
    func gotCurrentLocation(latitude: Double, longitude: Double, isLocationChange: Bool) {
        showLoader()
        nearbyFilterData.userLocation.latitude = latitude
        nearbyFilterData.userLocation.longitude = longitude
        
        self.dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            self.hideLoader()
            self.delegate?.gotEntitiesResponse(leads: self.leads, opportunities: self.opportunities, accounts: self.accounts, isLocationChange: isLocationChange)
        })
    }
}
// MARK: - MapFilterDelegate
extension GeoFencingViewController: MapFilterDelegate {
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        delegate?.gotSelectedLocation(locationDetails)
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        self.nearbyFilterData.userLocation.address = locationDetails.formattedAddress
        delegate?.updateNearByFilter(nearbyFilterData)
    }
    
    func refreshData(_ isLocationChange: Bool, nearbyFilterData: NearbyFilterData) {
        self.nearbyFilterData = nearbyFilterData
        let latitude = self.nearbyFilterData.userLocation.latitude ?? ConstantV.latitude
        let longitude = self.nearbyFilterData.userLocation.longitude ?? ConstantV.longitude
        let address = self.nearbyFilterData.userLocation.address ?? ""
        self.nearbyFilterData = nearbyFilterData
        delegate?.changeMapLoction(location: CLLocation(latitude: latitude, longitude: longitude), address:  address)
        delegate?.updateNearByFilter(nearbyFilterData)
        gotCurrentLocation(latitude: latitude, longitude: longitude, isLocationChange: isLocationChange)
    }
}
// MARK:- SearchBarDelegate
extension GeoFencingViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if isSelectedIndex == 0 {
            if !searchText.isEmpty  {
            openAddressDropDown(searchText)
            } else {
                dropDown.isHidden = true
            }
        } else {
            delegate?.searchLocation(isSelectedIndex, searchText: searchText, searchedArea: nil)
        }
    }
}

// MARK: - GeoMappingView
extension GeoFencingViewController: GeoMappingView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?) {
        self.shapesData = output
        delegate?.gotShapesData(output)
    }
    func gotEmployeesList(_ output: [GeoFencingD2D]?) { }
    func areaAssigned(_ output: GeoFencingD2D) { }
}
extension GeoFencingViewController: UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D) { }
    func updateShapeAfterRedraw(_ output: GeoFencingD2D){
        segment.selectedSegmentIndex = 0
        isSelectedIndex = segment.selectedSegmentIndex
        delegate?.tabChanged(isSelectedIndex)
        drawShapesDelegate?.redrawSahpe(output)
    }
}

extension GeoFencingViewController : AssignAreaFilter {
    func refreshData(dictData: NSDictionary, tag: Int) {
        print("Locvatin********\(dictData)" )
        self.dictFilterData = NSMutableDictionary()
        self.dictFilterData = dictData.mutableCopy()as! NSMutableDictionary
        
        if(dictFilterData.count != 0){
            
            nsud.set(dictFilterData, forKey: "filterAssignArea")
            
            let lat = Double("\(dictFilterData.value(forKey: "latitude")!)")
            let long = Double("\(dictFilterData.value(forKey: "longitude")!)")
            let address = "\(dictFilterData.value(forKey: "address")!)"
            
            delegate?.changeMapLoction(location: CLLocation(latitude: lat!, longitude: long!), address:  address)
            
        }

    }
    
}
