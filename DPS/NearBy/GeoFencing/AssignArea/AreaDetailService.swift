//
//  AreaDetailService.swift
//  DPS
//
//  Created by Vivek Patel on 19/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class AreaDetailService {
    
    func saveAreaName(areaName: String, employeeGeoFencingAreaId: String, _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        
        let url = URL.GeoMapping.saveAreaName.replacingOccurrences(of: "(companykey)", with: companyKey).replacingOccurrences(of: "(employeeGeoFencingAreaId)", with: employeeGeoFencingAreaId).replacingOccurrences(of: "(area)", with: areaName).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        print(url)
        AF.request(url, method: .get, headers: URL.headers) .responseDecodable(of: Bool?.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }
    }
    func deleteArea(employeeGeoFencingAreaId:String, _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let url = URL.GeoMapping.deleteArea.replacingOccurrences(of: "(employeeGeoFencingAreaId)", with: employeeGeoFencingAreaId)
        print(url)
        AF.request(url, method: .get, headers: URL.headers) .responseDecodable(of: Bool?.self) { response in
            switch response.result {
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                print(value)
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
    func unassign(parameter: [Int], _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let urlString = URL.GeoMapping.unaasign.replacingOccurrences(of: "(companykey)", with: companyKey).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            for header in URL.headers  {
                request.setValue(header.value, forHTTPHeaderField: header.name)
            }
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameter)
            AF.request(request).responseDecodable(of: Bool?.self) { response in
                switch response.result {
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil, error.localizedDescription)
                    break
                }
            }.responseJSON { (dataResponse) in
                switch dataResponse.result {
                case .success(let value):
                    print(value)
                    break
                case .failure(let error):
                    print(error)
                    break
                    
                }
            }
            
        }
    }
}

