//
//  AddAssigneeViewController.swift
//  DPS
//
//  Created by Vivek Patel on 10/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class AddAssigneeViewController: BaseViewController {
    // MARK: - VAriables
    var selectedAssignees: [EmployeeListD2D] = []
    var employeeList: [EmployeeListD2D]?
    var shapeData: GeoFencingD2D?
    private lazy var presenterGeo = {
        return GeoMappingPresenter(self)
    }()
    var delegate: UpdateAreaD2D?
    // MARK: - Outlets
    @IBOutlet weak var txtAssignee: UITextField!
    @IBOutlet weak var btnDismiss: UIButton!
    var arrRedrawEmpIds: [Int] = []

    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let list = AppUserDefaults.shared.employeeList {
            self.employeeList = list
        }
        AppUtility.buttonImageColor(btn: btnDismiss, image: "Group 771", color: .lightGray)
        self.setData()
    }
    private func setData() {
        var selectedRows:[String] = []
        let arrAssignee = shapeData!.employeeGeoFencingAreaAssignmentDCS
        if(arrAssignee.count != 0){
            for assignee in arrAssignee {
                let value = assignee.employeeName ?? ""
                selectedRows.append(value)
                self.arrRedrawEmpIds.append(assignee.employeeID ?? 0)
            }
            self.txtAssignee.text = selectedRows.joined(separator: ",")
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionSave(_ sender: Any) {
        var arrIds: [Int] = []

        for assignee in selectedAssignees {
            arrIds.append(assignee.employeeId ?? 0)
        }
        if arrIds.count == 0 {
            arrIds = self.arrRedrawEmpIds
        }
        
        let areaId = shapeData?.employeeGeoFencingAreaID
        print(arrIds)
        presenterGeo.apiCallToCheckAssignArea(arrIds, areaID: "\(areaId ?? 0)")

        /*
        guard !selectedAssignees.isEmpty else {
            showAlert(message: "Please select assignee")
            return
        }
        guard var geofencingDetails = shapeData  else { return }
    
        var employeeGeoFencingAreaAssignmentDCS  = geofencingDetails.employeeGeoFencingAreaAssignmentDCS

        for assignee in selectedAssignees {
            var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
            employeeGeo.employeeID = assignee.employeeId
            employeeGeo.employeeName = assignee.fullName
            employeeGeo.isActive = assignee.isActive
            employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
        }
        geofencingDetails.employeeGeoFencingAreaAssignmentDCS = employeeGeoFencingAreaAssignmentDCS
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let empId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
        geofencingDetails.modifiedBy = Int(empId)
        geofencingDetails.modifiedDate = AppUtility.convertDateToString(Date(), toFormat:ConstantV.modifiedDateFormat)
        presenterGeo.apiCallToAssignArea(geofencingDetails)*/
    }
}
// MARK: - UITextFieldDelegate
extension AddAssigneeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtAssignee {
            guard let employeeList = employeeList, !employeeList.isEmpty else { return false}
            var rows: [MultiSelectRow] = []
            let array = txtAssignee.text!.trimmed.components(separatedBy: ",")
            for row in employeeList {
                var isSelected = false
                if let employeeId = row.employeeId {
//                    if array.contains(employeeId.description){
//                        isSelected = true
//                    }
                    if self.arrRedrawEmpIds.count != 0 {
                        for empId in self.arrRedrawEmpIds {
                            print(empId)
                            if "\(empId)" == employeeId.description {
                                isSelected = true
                            }
                        }
                    }
                    if self.selectedAssignees.count != 0 {
                        for row1 in self.selectedAssignees {
                            print(row1)
                            if "\(row1.employeeId ?? 0)" == employeeId.description {
                                isSelected = true
                            }
                        }
                    }
                }
                let row = MultiSelectRow(title: row.fullName ?? "", isSelected: isSelected)
                rows.append(row)
            }
            self.selectedAssignees.removeAll()
            showMultiPicker(sender: txtAssignee, rows: rows) { (indexs, view) in
                var selectedRows:[String] = []
                for index in indexs{
                    let value = employeeList[index].fullName ?? ""
                    let selectedAssignee = employeeList[index]
                    selectedRows.append(value)
                    self.selectedAssignees.append(selectedAssignee)
                }
                self.txtAssignee.text = selectedRows.joined(separator: ",")
            }
            return false
        }
        return true
    }
}
// MARK: - GeoMappingView
extension AddAssigneeViewController: GeoMappingView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?) { }
    func gotEmployeesList(_ output: [GeoFencingD2D]?) {}
    func areaAssigned(_ output: GeoFencingD2D) {
        delegate?.updateAssigneesDetail(output)
        self.dismiss(animated: true)
    }
    
    func isAreaAssigned(_ output: Bool)
    {
        print(output)
        if output == true {
            //Show alert
            showAlert(message: "Area has been already assigned.")
            return
        }else {
            guard !selectedAssignees.isEmpty else {
                showAlert(message: "Please select assignee")
                return
            }
            guard var geofencingDetails = shapeData  else { return }
        
            var employeeGeoFencingAreaAssignmentDCS  = geofencingDetails.employeeGeoFencingAreaAssignmentDCS

            employeeGeoFencingAreaAssignmentDCS.removeAll()
            for assignee in selectedAssignees {
                var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
                employeeGeo.employeeID = assignee.employeeId
                employeeGeo.employeeName = assignee.fullName
                employeeGeo.isActive = assignee.isActive
                employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
            }
            if employeeGeoFencingAreaAssignmentDCS.count == 0 {
                let arrAssignee = geofencingDetails.employeeGeoFencingAreaAssignmentDCS
                if(arrAssignee.count != 0){
                    for assignee in arrAssignee {
                        var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
                        employeeGeo.employeeID = assignee.employeeID
                        employeeGeo.employeeName = assignee.employeeName
                        employeeGeo.isActive = assignee.isActive
                        employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
                    }
                }
            }
            geofencingDetails.employeeGeoFencingAreaAssignmentDCS = employeeGeoFencingAreaAssignmentDCS
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            let empId = "\(dictLoginData.value(forKey: "EmployeeId")!)"
            geofencingDetails.modifiedBy = Int(empId)
            geofencingDetails.modifiedDate = AppUtility.convertDateToString(Date(), toFormat:ConstantV.modifiedDateFormat)
            presenterGeo.apiCallToAssignArea(geofencingDetails)
        }
    }
}
