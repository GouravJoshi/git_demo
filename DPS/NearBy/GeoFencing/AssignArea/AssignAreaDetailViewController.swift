//
//  AssignAreatDetailViewController.swift
//  DPS
//
//  Created by Vivek Patel on 10/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit
protocol OpenAreaDetailProtocol {
    func openAreaDeatil(_ shapeData: GeoFencingD2D)
}
protocol UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D)
    func updateShapeAfterRedraw(_ output: GeoFencingD2D)
}
final class AssignAreaDetailViewController: BaseViewController {
    // MARK: - Variables
    var delgateRedraw: UpdateAreaD2D?
    var assignees: [EmployeeGeoFencingAreaAssignmentDc]? {
        didSet {
            tableView?.reloadData()
            if let assignees = assignees {
                if assignees.isEmpty {
                    btnUnassignAll.isHidden = true
                } else {
                    btnUnassignAll.isHidden = false
                }
            } else {
                btnUnassignAll.isHidden = true
            }
        }
    }
    private lazy var presenter = {
        return AreaDetailPresenter(self)
    }()
    var shapeData: GeoFencingD2D?
    // MARK: - Outlets
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblAreacreatedDate: UILabel!
    @IBOutlet weak var topColoredView: UIView!
    @IBOutlet weak var viewAreaNameActions: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewDelete: UIStackView!
    @IBOutlet weak var viewHistory: UIStackView!
    @IBOutlet weak var viewRedraw: UIStackView!
    @IBOutlet weak var btnDissmiss: UIButton!
    @IBOutlet weak var txtAreaName: UITextField!
    @IBOutlet weak var btnAddAssignee: UIButton!
    @IBOutlet weak var btnUnassignAll: UIButton!
    @IBOutlet weak var lblDelete: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblRedraw: UILabel!
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        updateUI()
        setData()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    private func setUI() {
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedStringDelete = NSAttributedString(string: "Delete Area", attributes: underlineAttribute)
        let underlineAttributedStringHistory = NSAttributedString(string: "History", attributes: underlineAttribute)
        let underlineAttributedStringRedraw = NSAttributedString(string: "Redraw", attributes: underlineAttribute)
        lblDelete.attributedText = underlineAttributedStringDelete
        lblRedraw.attributedText = underlineAttributedStringRedraw
        lblHistory.attributedText = underlineAttributedStringHistory
        topColoredView.layer.cornerRadius = 8
        outerView.layer.cornerRadius = 8
        topColoredView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        outerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .white)
    }
    private func setData() {
        guard let shapeData = shapeData else { return }
        txtAreaName.text = shapeData.area
        
        if  let dateString =  shapeData.createdDate, let createdDate = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy"), let createdByName = shapeData.createdByName {
            lblAreacreatedDate.text = "Created on \(createdDate) by \(createdByName)"
        }
        self.assignees = shapeData.employeeGeoFencingAreaAssignmentDCS
    }
    
    private func updateUI() {
        if let assignees = assignees {
            if assignees.isEmpty {
                btnUnassignAll.isHidden = true
            } else {
                btnUnassignAll.isHidden = false
            }
        } else {
            btnUnassignAll.isHidden = true
        }
        if btnEdit.isHidden {
            viewAreaNameActions.isHidden = false
            txtAreaName?.backgroundColor = .white
            txtAreaName?.textColor = .appDarkGreen
            txtAreaName?.borderStyle = .roundedRect
            txtAreaName?.isUserInteractionEnabled = true
            txtAreaName?.borderColorV = .black
        } else {
            viewAreaNameActions.isHidden = true
            txtAreaName?.backgroundColor = .clear
            txtAreaName?.textColor = .white
            txtAreaName?.borderStyle = .none
            txtAreaName?.isUserInteractionEnabled = false
            txtAreaName?.borderColorV = .clear
            
        }
    }
    private func apicallToUnAssignArea(_ id: [Int], selectedIndex: Int?) {
        presenter.apiCallToUnassign(id, selectedIndex: selectedIndex)
    }
    // MARK:- Actions
    @IBAction func actionRedraw(_ sender: Any) {
        guard let shapeData = shapeData else { return }
        delgateRedraw?.updateShapeAfterRedraw(shapeData)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionHistory(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "GeoFencingD2D", bundle: nil)
            guard let destination = storyboard.instantiateViewController(withIdentifier: "GeoHistoryViewController") as? GeoHistoryViewController else { return }
            destination.employeeGeoFencingAreaId = self.shapeData?.employeeGeoFencingAreaID
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    @IBAction func actionDelete(_ sender: UITapGestureRecognizer) {
        showAlertWithOptions(message: "Are you sure you want to delete this area?", btnTitle1: "Delete") { (yes) in
            if yes {
                guard let shapeData = self.shapeData, let employeeGeoFencingAreaID = shapeData.employeeGeoFencingAreaID else { return }
                self.presenter.apiCallToDeleteAreaDetails(employeeGeoFencingAreaID.description)
            }
        }
    }
    @IBAction func actionCancleSaveName(_ sender: Any) {
        txtAreaName.text =  shapeData?.area
        btnEdit.isHidden = false
        viewAreaNameActions.isHidden = true
        updateUI()
    }
    @IBAction func actionDismiss(_ sender: Any) {
        NotificationCenter.default.post(name: .refreshMapOnCancelD2D, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionSaveName(_ sender: Any) {
        guard let shapeData = shapeData else { return }
        guard !txtAreaName.text!.isEmpty else {
            showAlert(message: "Area name shouldn't be empty, please enter area name")
            return
        }
        if let employeeGeoFencingAreaId = shapeData.employeeGeoFencingAreaID {
            presenter.apiCallToSaveAreaName(txtAreaName.text ?? "", employeeGeoFencingAreaId: employeeGeoFencingAreaId.description)
        }
        
    }
    @IBAction func actionEdit(_ sender: UIButton) {
        viewAreaNameActions.isHidden = false
        btnEdit.isHidden = true
        updateUI()
       
    }
    @IBAction func actionUnassignedAll(_ sender: Any) {
        /*if let ids = (assignees?.map{($0.employeeGeoFencingAreaID?.description ?? "")})?.joined(separator: ",")  {
        let array = ids.components(separatedBy: ",")
        let intArray = array.map { Int($0)!}*/
        var arrayInt: [Int] = []
        guard let assignees = assignees else { return }
        for assignee in assignees {
            if let employeeGeoFencingAreaID = assignee.employeeGeoFencingAreaAssignmentID {
                arrayInt.append(employeeGeoFencingAreaID)
            }
        }
        presenter.apiCallToUnassign(arrayInt, selectedIndex: nil)
        
    }
    @IBAction func actionAddAssignee(_ sender: Any) {
        let storyboard = UIStoryboard(name: "GeoFencingD2D", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AddAssigneeViewController") as? AddAssigneeViewController else {
            return
        }
        //let parameters = generateRequest()
        //controller.geofencingDetails = parameters
        destination.delegate = self
        destination.shapeData = shapeData
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: true, completion: nil)
        
    }
    
}
//MARK: - UITableViewDataSource
extension AssignAreaDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let assignees = assignees  else { return 0 }
        return assignees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AssigneeTableViewCell.self), for: indexPath) as! AssigneeTableViewCell
        let assignee = assignees?[indexPath.row]
        cell.assignee = assignee
        return cell
    }
}
extension AssignAreaDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let unaasign = UIContextualAction(style: .normal, title: "Unassign") { (action, sourceView, completionHandler) in
            if let id = self.assignees?[indexPath.row].employeeGeoFencingAreaAssignmentID {
                var ids:[Int] = []
                ids.append(id)
                self.apicallToUnAssignArea(ids, selectedIndex: indexPath.row)
            }
        }
        unaasign.backgroundColor = hexStringToUIColor(hex: "A50C3B")
        
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [unaasign])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
}
extension AssignAreaDetailViewController: AreaDetailView {
    func successsfullySavedName(_ output: Bool) {
        if output {
            shapeData?.area = txtAreaName.text
            btnEdit.isHidden = false
            viewAreaNameActions.isHidden = true
            updateUI()
            NotificationCenter.default.post(name: .updatedGeoAreaDataD2D, object: nil)
        } else {
            showAlertWithCompletion(message: "Unable to update area name", completion: {
                self.txtAreaName.text = self.shapeData?.area
            })
        }
    }
    func successsfullyUnassigned(_ output: Bool, selectedIndex: Int?) {
        if output {
            if let selectedIndex = selectedIndex {
                assignees?.remove(at: selectedIndex)
            } else {
                shapeData?.employeeGeoFencingAreaAssignmentDCS.removeAll()
                assignees?.removeAll()
            }
            tableView.reloadData()
            NotificationCenter.default.post(name: .updatedGeoAreaDataD2D, object: nil)
        } else {
            showAlert(message: "Unable to unassign, please try again later")
        }
        
    }
    func successsfullyDelete(_ output: Bool) {
        if output {
            NotificationCenter.default.post(name: .updatedGeoAreaDataD2D, object: nil)
            self.dismiss(animated: true, completion: nil)
        } else {
            showAlert(message: "Unable to delete area, please try again later")
        }
        
    }
    
}
extension AssignAreaDetailViewController: UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D) {
        self.shapeData = output
        setData()
        tableView.reloadData()
        NotificationCenter.default.post(name: .updatedGeoAreaDataD2D, object: nil)
    }
    func updateShapeAfterRedraw(_ output: GeoFencingD2D){ }
}

