//
//  AssigneeTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 10/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit


final class AssigneeTableViewCell: UITableViewCell {
    // MARK: - Variables
    var assignee: EmployeeGeoFencingAreaAssignmentDc?{
        didSet {
            guard let assignee = assignee  else { return}
            lblName.text = assignee.employeeName
            lblPost.text = assignee.roleName
            imgView.cornerRadiusV = imgView.frame.height/2
            if(assignee.employeePhoto != nil)
            {
                let listItems = "\(assignee.employeePhoto!)".components(separatedBy: "EmployeeImagesEmployeeImages")
                if(listItems.count != 0)
                {
                    let stremployeePhoto = "\(listItems[1])"
                    let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
                    let strURL = "\(dictLoginData.value(forKeyPath: "Company.CompanyConfig.HrmsServiceModule.ServiceUrl")!)Documents/EmployeeImages/" + stremployeePhoto
                    imgView.setImageWith(URL(string: strURL), placeholderImage: UIImage(named: "contact_person_details"), usingActivityIndicatorStyle: UIActivityIndicatorView.Style.gray)
                }
            }
            else
            {
                imgView.image = UIImage(named: "contact_person_details")
            }
            lblDate.text = ""
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
