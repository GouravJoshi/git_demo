//
//  AddAreaViewController.swift
//  DPS
//
//  Created by Vivek Patel on 10/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class AddAreaViewController: BaseViewController {
    var delegate: OpenAreaDetailProtocol?
    var selectedAssignees: [EmployeeListD2D] = []
    var employeeList: [EmployeeListD2D]?
    var geofencingDetails: GeoFencingD2D?
    var arrRedrawEmpIds: [Int] = []

    private lazy var presenterGeo = {
        return GeoMappingPresenter(self)
    }()
    // MARK: - Outltes
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var txtAssignees: UITextField!
    @IBOutlet weak var txtAreaName: UITextField!
    @IBOutlet weak var btnDissmiss: UIButton!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtAreaName.delegate = self
        setEmployeeListToUserDefault()
        AppUtility.buttonImageColor(btn: btnDissmiss, image: "Group 771", color: .lightGray)
        if let list = AppUserDefaults.shared.employeeList {
            self.employeeList = list
        }
        setData()
    }
    private func setData() {
        guard let geofencingDetails = geofencingDetails  else { return }
        txtAreaName.text = geofencingDetails.area
        print(geofencingDetails.employeeGeoFencingAreaAssignmentDCS)
        
        var selectedRows:[String] = []
        let arrAssignee = geofencingDetails.employeeGeoFencingAreaAssignmentDCS
        if(arrAssignee.count != 0){
            for assignee in arrAssignee {
                let value = assignee.employeeName ?? ""
                selectedRows.append(value)
                self.arrRedrawEmpIds.append(assignee.employeeID ?? 0)
            }
            self.txtAssignees.text = selectedRows.joined(separator: ",")
        }
    }
    
    // MARK: - Actions
    @IBAction func actionSave(_ sender: Any) {
        var arrIds: [Int] = []
        for assignee in selectedAssignees {
            arrIds.append(assignee.employeeId ?? 0)
        }
        if arrIds.count == 0 {
            arrIds = self.arrRedrawEmpIds
        }
        let areaId = geofencingDetails?.employeeGeoFencingAreaID
        presenterGeo.apiCallToCheckAssignArea(arrIds, areaID: "\(areaId ?? 0)")

      /*  guard !txtAreaName.text!.isEmpty else {
            showAlert(message: "Please enter area name")
            return
        }
        guard var geofencingDetails = geofencingDetails  else { return }
        geofencingDetails.area = txtAreaName.text
        var employeeGeoFencingAreaAssignmentDCS  = geofencingDetails.employeeGeoFencingAreaAssignmentDCS

        for assignee in selectedAssignees {
            var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
            employeeGeo.employeeID = assignee.employeeId
            employeeGeo.employeeName = assignee.fullName
            employeeGeo.isActive = assignee.isActive
            employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
        }
        geofencingDetails.employeeGeoFencingAreaAssignmentDCS = employeeGeoFencingAreaAssignmentDCS
        presenterGeo.apiCallToAssignArea(geofencingDetails)*/
    }
    
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: .refreshMapOnCancelD2D, object: nil)
        }
    }
}
// MARK: - UITextFieldDelegate
extension AddAreaViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (range.location == 0) && (string == " ")
        {
            return false
        }
        return true

    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtAssignees {
            guard let employeeList = employeeList, !employeeList.isEmpty else {
                showAlert(message: "No Record Found")
                return false}
            var rows: [MultiSelectRow] = []
            let array = txtAssignees.text!.trimmed.components(separatedBy: ",")
            for row in employeeList {
                var isSelected = false
                if let employeeId = row.employeeId {
//                    if array.contains(employeeId.description){
//                        isSelected = true
//                    }
                    if self.arrRedrawEmpIds.count != 0 {
                        for empId in self.arrRedrawEmpIds {
                            print(empId)
                            if "\(empId)" == employeeId.description {
                                isSelected = true
                            }
                        }
                    }
                    if self.selectedAssignees.count != 0 {
                        for row1 in self.selectedAssignees {
                            print(row1)
                            if "\(row1.employeeId ?? 0)" == employeeId.description {
                                isSelected = true
                            }
                        }
                    }
                }
                let row = MultiSelectRow(title: row.fullName ?? "", isSelected: isSelected)
                rows.append(row)
            }
            self.selectedAssignees.removeAll()
            showMultiPicker(sender: txtAssignees, rows: rows) { (indexs, view) in
                var selectedRows:[String] = []
                for index in indexs{
                    let value = employeeList[index].fullName ?? ""
                    let selectedAssignee = employeeList[index]
                    selectedRows.append(value)
                    self.selectedAssignees.append(selectedAssignee)
                }
                self.txtAssignees.text = selectedRows.joined(separator: ",")
            }
            return false
        }
        return true
    }
}
// MARK: - GeoMappingView
extension AddAreaViewController: GeoMappingView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?) { }
    func gotEmployeesList(_ output: [GeoFencingD2D]?) {}
    func areaAssigned(_ output: GeoFencingD2D) {
        self.dismiss(animated: true) {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                // your code here
                self.delegate?.openAreaDeatil(output)

            }
            
        }
    }
    func isAreaAssigned(_ output: Bool)
    {
        print(output)
        if output == true {
            //Show alert
            showAlert(message: "Area has been already assigned.")
            return
        }else {
            guard !txtAreaName.text!.isEmpty else {
                  showAlert(message: "Please enter area name")
                  return
              }
              guard var geofencingDetails = geofencingDetails  else { return }
              geofencingDetails.area = txtAreaName.text
              var employeeGeoFencingAreaAssignmentDCS  = geofencingDetails.employeeGeoFencingAreaAssignmentDCS
              employeeGeoFencingAreaAssignmentDCS.removeAll()
              for assignee in selectedAssignees {
                  var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
                  employeeGeo.employeeID = assignee.employeeId
                  employeeGeo.employeeName = assignee.fullName
                  employeeGeo.isActive = assignee.isActive
                  employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
              }
            if employeeGeoFencingAreaAssignmentDCS.count == 0 {
                let arrAssignee = geofencingDetails.employeeGeoFencingAreaAssignmentDCS
                if(arrAssignee.count != 0){
                    for assignee in arrAssignee {
                        var employeeGeo = EmployeeGeoFencingAreaAssignmentDc()
                        employeeGeo.employeeID = assignee.employeeID
                        employeeGeo.employeeName = assignee.employeeName
                        employeeGeo.isActive = assignee.isActive
                        employeeGeoFencingAreaAssignmentDCS.append(employeeGeo)
                    }
                }
            }
              geofencingDetails.employeeGeoFencingAreaAssignmentDCS = employeeGeoFencingAreaAssignmentDCS
              presenterGeo.apiCallToAssignArea(geofencingDetails)
        }
    }
}
