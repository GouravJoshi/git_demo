//
//  AreaDetailPreseneter.swift
//  DPS
//
//  Created by Vivek Patel on 19/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

protocol AreaDetailView: CommonView {
    func successsfullySavedName(_ output: Bool)
    func successsfullyDelete(_ output: Bool)
    func successsfullyUnassigned(_ output: Bool, selectedIndex: Int?)
}

class AreaDetailPresenter: NSObject {
    weak private var delegate: AreaDetailView?
    private lazy var service = {
        
        return AreaDetailService()
    }()
    
    init(_ delegate: AreaDetailView) {
        self.delegate = delegate
    }
    
    func apiCallToSaveAreaName( _ areaName: String, employeeGeoFencingAreaId: String) {
       delegate?.showLoader()
        service.saveAreaName(areaName: areaName, employeeGeoFencingAreaId: employeeGeoFencingAreaId) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.successsfullySavedName(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallToDeleteAreaDetails( _ employeeGeoFencingAreaId: String) {
       delegate?.showLoader()
        service.deleteArea(employeeGeoFencingAreaId: employeeGeoFencingAreaId) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.successsfullyDelete(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallToUnassign( _ id: [Int], selectedIndex: Int?) {
        let parameter = id
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameter)
        print(String(data: orderJsonData, encoding: .utf8)!)
        
       delegate?.showLoader()
        service.unassign(parameter: parameter) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.successsfullyUnassigned(object,selectedIndex: selectedIndex)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
