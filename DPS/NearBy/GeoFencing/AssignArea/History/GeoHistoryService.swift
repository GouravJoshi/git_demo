//
//  GeoHistoryService.swift
//  DPS
//
//  Created by Vivek Patel on 19/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class GeoHistoryService {
    
    func getGeoAreaHistory(empId:String, _ callBack:@escaping (_ object: [AreaHistoryD2D]?,_ error:String?) -> Void) {
        let url = URL.GeoMapping.geoHistoryList.replacingOccurrences(of: "(employeeGeoFencingAreaId)", with: empId)
        print(url)
        AF.request(url, method: .get, headers: URL.headers) .responseDecodable(of: [AreaHistoryD2D]?.self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
