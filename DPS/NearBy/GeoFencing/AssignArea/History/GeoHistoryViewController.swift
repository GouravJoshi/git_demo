//
//  GeoHistoryViewController.swift
//  DPS
//
//  Created by Vivek Patel on 11/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class GeoHistoryViewController: BaseViewController {
    // MARK: - Variables
    var historyList: [AreaHistoryD2D] = [] {
        didSet {
            lblRecordNotFound.isHidden =  !historyList.isEmpty
            tableView.reloadData()
        }
    }
    var employeeGeoFencingAreaId: Int?
    private lazy var presenterGeo = {
        return GeoHistoryPresenter(self)
    }()
    private var allRows: [AreaHistoryD2D] = []
    // MARK: - Outlets
    @IBOutlet weak var lblRecordNotFound: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        guard let employeeGeoFencingAreaId = employeeGeoFencingAreaId else { return }
        presenterGeo.apiCallToGetAreaHistory("\(employeeGeoFencingAreaId)")
        allRows = historyList
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    // MARK: - Convienience
    private func setNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = .appThemeColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //searchView.translatesAutoresizingMaskIntoConstraints = true
        searchBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width-140, height: 44)
        //searchView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width-100, height: 44)
        if let txfSearchField = searchBar.value(forKey: "searchField") as? UITextField {
            txfSearchField.borderStyle = .roundedRect
            txfSearchField.backgroundColor = .clear
            txfSearchField.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
            txfSearchField.layer.masksToBounds = true
        }
        searchBar.layer.borderColor = UIColor(red: 95.0/255, green: 178.0/255, blue: 175/255, alpha: 1).cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = (searchBar.frame.size.height/2) - 1
        searchBar.backgroundColor = .white
        searchBar.setPositionAdjustment(UIOffset(horizontal: 20, vertical: 0), for: .search)
        searchBar.layer.opacity = 1.0
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        /* let btn1 = UIButton(type: .custom)
         btn1.setImage(UIImage(named: "Group 771"), for: .normal)
         btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
         btn1.addTarget(self, action: #selector(actionDismiss), for: .touchUpInside)
         let button1 = UIBarButtonItem(customView: btn1)
         
         let btn2 = UIButton(type: .custom)
         btn2.setImage(UIImage(named: "Group 755"), for: .normal)
         btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
         btn2.addTarget(self, action: #selector(actionDismiss), for: .touchUpInside)
         let button2 = UIBarButtonItem(customView: btn2)
         
         
         self.navigationItem.rightBarButtonItems  = [button1, button2]*/
        self.navigationItem.leftBarButtonItem = leftNavBarButton
    }
    @objc private func actionDismisss() {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Actions
    @IBAction func actionDismiss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFilter(_ sender: Any) {
        
    }
}
extension GeoHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GeoHistoryTableViewCell.self), for: indexPath) as! GeoHistoryTableViewCell
        cell.history = historyList[indexPath.row]
        return cell
    }
}

// MARK:- SearchBarDelegate
extension GeoHistoryViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            historyList = allRows
        } else {
            historyList = allRows.filter({ (row) -> Bool in
                return (row.historyDescription?.description.localizedCaseInsensitiveContains(searchText)) ?? false
            })
        }
        tableView.reloadData()
    }
}
// MARK:- SearchBarDelegate
extension GeoHistoryViewController: GeoHistoryView {
    func gotAreaHistory(_ output: [AreaHistoryD2D]?) {
        guard let output = output else { return }
        self.historyList = output
        allRows = historyList
    }
}
