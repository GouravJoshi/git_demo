//
//  GeoHistoryPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 19/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

protocol GeoHistoryView: CommonView {
    func gotAreaHistory(_ output: [AreaHistoryD2D]?)
}

class GeoHistoryPresenter: NSObject {
    weak private var delegate: GeoHistoryView?
    private lazy var service = {
        
        return GeoHistoryService()
    }()
    
    init(_ delegate: GeoHistoryView) {
        self.delegate = delegate
    }
    
    func apiCallToGetAreaHistory( _ empId: String) {
       delegate?.showLoader()
        service.getGeoAreaHistory(empId: empId) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.gotAreaHistory(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
