//
//  GeoHistoryTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 11/02/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class GeoHistoryTableViewCell: UITableViewCell {
    //MARK: - Variables
    var history: AreaHistoryD2D? {
        didSet {
            guard let history = history else {
                return
            }
            lblDescription.text = history.historyDescription
            if let dateString =  history.createdDate, let createdTime = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "HH:mm a"), let  createdDate = AppUtility.convertStringToFormat(fromString: dateString, toFormat: "MM/dd/yyyy")   {
                lblDate.text = "Created on \(createdDate) at \(createdTime)"
            }
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
