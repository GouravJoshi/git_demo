//
//  GeoListTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 27/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class GeoListTableViewCell: UITableViewCell {
    // MARK: - Variables
    var shape: GeoFencingD2D? {
        didSet {
            guard let shape = shape else { return }
            lblAreaName.text = shape.area
            let formattedArray = (shape.employeeGeoFencingAreaAssignmentDCS.map{($0.employeeName ?? "")}).joined(separator: ",")
           lblAssignee.text =  "Assignee(s): \(formattedArray)"
            if let date = shape.createdDate, let createdDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "MM/dd/yyyy"), let createdByName = shape.createdByName {
                lblCreateDate.text = "Created on \(createdDate) by \(createdByName)"
            } else {
                lblCreateDate.text = ""
            }
        }
    }
    // MARK: - Outlets
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblAssignee: UILabel!
    @IBOutlet weak var lblAreaName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
