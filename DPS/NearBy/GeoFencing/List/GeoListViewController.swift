//
//  GeoListViewController.swift
//  DPS
//
//  Created by Vivek Patel on 27/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import UIKit

final class GeoListViewController: UIViewController {
    // MARK: - Variables
    var shapesData: [GeoFencingD2D]? {
        didSet {
            if let shapesData = shapesData, !shapesData.isEmpty {
                lblNoRecordFound?.isHidden = true
            } else {
                lblNoRecordFound?.isHidden = false
            }
            tableView?.reloadData()
        }
    }
    var allRows: [GeoFencingD2D] = []
    var delgateRedraw: UpdateAreaD2D?
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    // MARK: - View Life Cycle
    @IBOutlet weak var lblNoRecordFound: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let shapesData = shapesData else { return }
        allRows  = shapesData
        setUI()
    }
    func setUI() {
        if let shapesData = shapesData, !shapesData.isEmpty {
            lblNoRecordFound?.isHidden = true
        } else {
            lblNoRecordFound?.isHidden = false
        }
    }
    // MARK: - Convienience
    func searchInList(_ searchText: String) {
        if searchText.isEmpty {
            shapesData = allRows
        } else {
            shapesData = allRows.filter({ (row) -> Bool in
                return (row.area?.localizedCaseInsensitiveContains(searchText)) ?? false
            })
        }
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension GeoListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let shapesData = shapesData else { return 0 }
        return shapesData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GeoListTableViewCell.self), for: indexPath) as! GeoListTableViewCell
        cell.shape = shapesData![indexPath.row]
        return cell
    }
    
}
// MARK: - UITableViewDelegate
extension GeoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "GeoFencingD2D", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "AssignAreatDetailViewController") as? AssignAreaDetailViewController else { return }
        destination.shapeData = shapesData?[indexPath.row]
        destination.delgateRedraw = self
        let navController = UINavigationController.init(rootViewController: destination)
        navController.isNavigationBarHidden = true
        navController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
}
extension GeoListViewController: UpdateAreaD2D {
    func updateAssigneesDetail(_ output: GeoFencingD2D) { }
    func updateShapeAfterRedraw(_ output: GeoFencingD2D){
        delgateRedraw?.updateShapeAfterRedraw(output)
    }
}

