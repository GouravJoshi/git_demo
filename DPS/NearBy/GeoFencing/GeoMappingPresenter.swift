//
//  GeoMappingPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 21/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

protocol GeoMappingView: CommonView {
    func gotAssignedArea(_ output: [GeoFencingD2D]?)
    func gotEmployeesList(_ output: [GeoFencingD2D]?)
    func areaAssigned(_ output: GeoFencingD2D)
    func isAreaAssigned(_ output: Bool)

}
extension GeoMappingView {
    func isAreaAssigned(_ output: Bool) {}
}

class GeoMappingPresenter: NSObject {
    weak private var delegate: GeoMappingView?
    private lazy var service = {
        
        return GeoMappingService()
    }()
    
    init(_ delegate: GeoMappingView) {
        self.delegate = delegate
    }
    
    func apiCallToGetAssignedArea( _ empId: String, companyKey: String, isLoaderShow: Bool? = false) {
        if let isLoaderShow = isLoaderShow, isLoaderShow {
        delegate?.showLoader()
        }
        service.getAssingArea(empId: empId, companyKey: companyKey) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.gotAssignedArea(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    func apiCallToAssignArea(_ parameters: GeoFencingD2D) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        service.assignArea(companyKey: companyKey, parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.areaAssigned(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
    func apiCallToCheckAssignArea(_ parameters: [Int], areaID: String) {
//        let encoder = JSONEncoder()
//        encoder.outputFormatting = .prettyPrinted
//        let orderJsonData = try! encoder.encode(parameters)
//        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        service.checkAssignArea(companyKey: companyKey, employeeGeoFencingAreaId: areaID, parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.isAreaAssigned(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
    
    func apiCallToGetAllEmployeeGeoFencings() {
        let parameters = [""]
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        delegate?.showLoader()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        service.getAllEmployeesGeofencing(companyKey: companyKey, parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.gotEmployeesList(object)
            } else {
                
                self.delegate?.showAlert(message: error)
            }
        }
        
    }
}
