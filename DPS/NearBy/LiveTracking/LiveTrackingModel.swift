//
//  LiveTrackingModel.swift
//  DPS
//
//  Created by Vivek Patel on 21/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
// MARK: - WelcomeElement
struct LiveLocationD2D: Codable {
    let employeeLocationID: Int?
    let employeeNo: String?
    let employeeNOS: JSONNull?
    let deviceID: String?
    let latitude, longitude, speed: Double?
    let ip, createdDate: String?
    let companyID: Int?
    let companyKey, employeeName: String?

    enum CodingKeys: String, CodingKey {
        case employeeLocationID = "EmployeeLocationId"
        case employeeNo = "EmployeeNo"
        case employeeNOS = "EmployeeNos"
        case deviceID = "DeviceId"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case speed = "Speed"
        case ip = "IP"
        case createdDate = "CreatedDate"
        case companyID = "CompanyId"
        case companyKey = "CompanyKey"
        case employeeName = "EmployeeName"
    }
}

