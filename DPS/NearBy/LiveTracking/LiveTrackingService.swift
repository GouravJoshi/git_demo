//
//  LiveTrackingService.swift
//  DPS
//
//  Created by Vivek Patel on 21/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class LiveTrackingService {
    func getLiveLocations(parameters: [String], _ callBack:@escaping (_ object: [LiveLocationD2D]?,_ error:String?) -> Void) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyKey = "\(dictLoginData.value(forKeyPath: "Company.CompanyKey")!)"
        let date = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        let url = URL.LiveTarcking.getLiveLocations.replacingOccurrences(of: "(companyKey)", with: companyKey).replacingOccurrences(of: "(CreatedDate)", with: "2021-01-21T00:00:00")
        print(url)
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: [LiveLocationD2D].self) { response in
            switch response.result{
            case let .success(result):
                callBack(result,nil)
                break
            case let .failure(error):
                callBack(nil, error.localizedDescription)
                break
            }
        }.responseJSON { (dataResponse) in
            switch dataResponse.result{
            case .success(let value):
                print(value)
                
                break
            case .failure(let error):
                print(error)
                break
                
            }
        }
    }
}
