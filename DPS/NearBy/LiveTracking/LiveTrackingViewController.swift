//
//  LiveTrackingViewController.swift
//  DPS
//
//  Created by Vivek Patel on 04/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire

final class LiveTrackingViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var lblOpportunityCount: UILabel!
    @IBOutlet weak var lblLeadsCount: UILabel!
    @IBOutlet weak var lblAccountCount: UILabel!
    @IBOutlet weak var searchBar: UIButton!
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var navigationView: UIStackView!
    private var clusterManager: GMUClusterManager?
    var timer: Timer?
    
    //MARK: - Variables
    private lazy var presenter = {
        return LiveTrackingPresenter(self)
    }()
    
    private var isInitialLocation: Bool = true
    var bounds: GMSCoordinateBounds?
    var nearbyFilterData = NearbyFilterData()
    private var mapView: GMSMapView!
    private let locationService = LocationService.instance
    private var kCameraLatitude = 40.692167
    private var kCameraLongitude = -102.664972
    lazy var items: [POIItem] = []
    var liveLocations: [LiveLocationD2D] = []
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setGoogleMap()
        setFooterView()
        self.navigationItem.titleView = navigationView
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.rightBarButtonItem = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .appThemeColor
        self.navigationController?.navigationBar.backgroundColor = .appThemeColor
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK: - Actions
    @IBAction func actionFilter(_ sender: Any) {
        /*let storyboard =  UIStoryboard.init(name: "Filter", bundle: nil)
         guard let controller = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController else { return }
         controller.delegate = self
         controller.nearbyFilterData = nearbyFilterData
         self.navigationController?.pushViewController(controller, animated: true)*/
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionSearch(_ sender: Any) {
        let storyboard =  UIStoryboard.init(name: "NearbyMe", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SearchPlaceViewController") as! SearchPlaceViewController
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    @objc private func callApiForLiveLocation(isShowLoader: Bool) {
        presenter.apiCallToGetLiveLocations(isShowLoader)
    }
    override func showAlert(message: String?) {
        presenter.apiCallToGetLiveLocations(false)
    }
}

// MARK: - GMUClusterManagerDelegate & GMSMapViewDelegate
extension LiveTrackingViewController: GMSMapViewDelegate, GMUClusterManagerDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
}
// MARK: - Map Convenience
extension LiveTrackingViewController {
    
    private func setGoogleMap() {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        GMSServices.provideAPIKey(apiKey)
        let camera = GMSCameraPosition.camera(withLatitude: kCameraLatitude,
                                              longitude: kCameraLongitude, zoom: 18)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView?.delegate = self
        mapView?.mapType = .satellite
        mapView?.isMyLocationEnabled = true
        self.viewMap.addSubview(mapView ?? UIView())
        mapView?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: mapView as Any, attribute: $0, relatedBy: .equal, toItem: mapView?.superview, attribute: $0, multiplier: 1, constant: 0)
        })
        setUpCluster()
        getCurrentLocation()
    }
    
    private func getCurrentLocation() {
        locationService.getLocation { location in
            guard let location = location else {
                // self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
                return
            }
            self.kCameraLatitude = location.coordinate.latitude
            self.kCameraLongitude = location.coordinate.longitude
            self.nearbyFilterData.userLocation.latitude = location.coordinate.latitude
            self.nearbyFilterData.userLocation.longitude = location.coordinate.longitude
            //    self.delegate?.gotCurrentLocation(latitude: self.kCameraLatitude, longitude: self.kCameraLongitude)
            self.mapView?.camera = GMSCameraPosition(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
            
        }
    }
    private func setMapMarker(_ locationDetails: LocationResult) {
        let marker = GMSMarker()
        self.mapView?.camera = GMSCameraPosition(latitude: locationDetails.geometry.location.latitude, longitude: locationDetails.geometry.location.longitude, zoom: 18)
        marker.position = CLLocationCoordinate2D(latitude: locationDetails.geometry.location.latitude, longitude:  locationDetails.geometry.location.longitude)
        marker.title = locationDetails.formattedAddress
        marker.snippet = locationDetails.formattedAddress
        let image =  UIImage(named: "Group 758")
        marker.icon = image?.maskWithColor(color: .systemRed)
        marker.map = self.mapView
    }
    private func setUpCluster() {
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
        clusterManager?.setDelegate(self, mapDelegate: self)
        clusterManager?.cluster()
        self.callApiForLiveLocation(isShowLoader: true)
    }
    private func generateClusterFromLocations() {
        self.items.removeAll()
        if bounds == nil {
            bounds = GMSCoordinateBounds()
        }
        for location in liveLocations {
            let lat =  location.latitude ?? 0.0
            let lng = location.longitude ?? 0.0
            if lat > -90.0 && lat < 90.0 , lng > -180.0 && lng < 180.0 {
                // lat =  lat +  extent * randomScale()
                // lng = lng + extent * randomScale()
                let item =  POIItem(position: CLLocationCoordinate2DMake(lat, lng), liveLocation: location)
                bounds = bounds?.includingCoordinate(item.position)
                //clusterManager?.add(item)
                items.append(item)
            }
        }
        self.clusterManager?.clearItems()
        self.clusterManager?.add(self.items)
        if isInitialLocation {
            guard let bounds = self.bounds else { return }
            self.mapView.animate(with: .fit(bounds, withPadding: 100.0))
        }
    }
}
// MARK: - MapFilterDelegate
extension LiveTrackingViewController: MapFilterDelegate, UpdateMapDelegate {
    func refreshData(_ isLocationChange: Bool, nearbyFilterData: NearbyFilterData) { }
    func gotSelectedLocation(_ locationDetails: LocationResult) {
        self.nearbyFilterData.userLocation.latitude = locationDetails.geometry.location.latitude
        self.nearbyFilterData.userLocation.longitude = locationDetails.geometry.location.longitude
        setMapMarker(locationDetails)
        // delegate?.gotSelectedLocation(locationDetails)
    }
}
extension LiveTrackingViewController: LiveTrackingView {
    func gotLiveLocations(_ output: [LiveLocationD2D]) {
        self.liveLocations.removeAll()
        self.liveLocations = output
        generateClusterFromLocations()
        isInitialLocation = false
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.callApiForLiveLocation), userInfo: nil, repeats: false)
           // self.perform(#selector(self.callApiForLiveLocation(isShowLoader:)), with: false, afterDelay: 20)
        }
    }
}
// MARK: - GMUClusterRendererDelegate
extension LiveTrackingViewController: GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if (marker.userData! is POIItem) {
            guard let customClusterItem = (marker.userData as? POIItem) else {
                return
            }
            let image =  UIImage(named: "LiveTruck")
            marker.icon = image
            marker.title = customClusterItem.liveLocation?.employeeName
           // marker.snippet = "Working on it"
        }
    }
}
