//
//  LiveTarckingPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 21/01/21.
//  Copyright © 2021 Saavan. All rights reserved.
//

import Foundation

protocol LiveTrackingView: CommonView {
    func gotLiveLocations(_ output: [LiveLocationD2D])
    
}

class LiveTrackingPresenter: NSObject {
    weak private var delegate: LiveTrackingView?
    private lazy var service = {
        
        return LiveTrackingService()
    }()
    
    init(_ delegate: LiveTrackingView) {
        self.delegate = delegate
    }
    
    func apiCallToGetLiveLocations(_ isShowLoader: Bool? = false) {
        let parameters = ["1480219430",
                          "1480219511"]
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        if isShowLoader ?? false {
            delegate?.showLoader()
        }
        service.getLiveLocations(parameters: parameters) { (object, error) in
            self.delegate?.hideLoader()
            if let object = object {
                self.delegate?.gotLiveLocations(object)
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
