//
//  LeadPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 22/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
protocol EntityView: CommonView {
    func gotLeads(output: LeadResponse)
    func gotOppotunities(output: OpportunityResponse)
    func gotAccounts(output: AccountsResponse)
}

class EntityPresenter: NSObject {
    
    weak private var delegate: EntityView?
    
    private lazy var service = {
        return EntityService()
    }()
    
    init(_  delegate: EntityView) {
        self.delegate = delegate
    }
    
    func apiCallsForLeadsInfo(_ parameters: LocationInfo) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print("Lead",String(data: orderJsonData, encoding: .utf8)!)
        let startDate = Date()
        service.leadsDetails(parameters: parameters) { (object, error) in
            let endDate = Date()

            if let object = object {
                self.delegate?.gotLeads(output: object)
            } else {
                self.delegate?.showAlert(message: error)
            }
            var interval = TimeInterval()
            interval = endDate.timeIntervalSince(startDate)
            
            print("Execution time for Get Lead D2D API:  \(URL.Entities.getLeads) ======== \(interval)")
        }
       

    }
    func apiCallsForGettingOpportunities(_ parameters: LocationInfo) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print("Opportunity",String(data: orderJsonData, encoding: .utf8)!)
        let startDate = Date()
        service.opportunitesDetails(parameters: parameters) { (object, error) in
            let endDate = Date()

            if let object = object {

                self.delegate?.gotOppotunities(output: object)
            } else {
                self.delegate?.showAlert(message: error)
            }
            var interval = TimeInterval()
            interval = endDate.timeIntervalSince(startDate)
            
            print("Execution time for Get Opportunity D2D API:   \(URL.Entities.getOpportunity) ======== \(interval)")
        }
    }
    func apiCallsForGettingAccounts(_ parameters: LocationInfo) {
       // print(parameters)
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print("Account",String(data: orderJsonData, encoding: .utf8)!)
    
        let startDate = Date()

        service.accountsDetails(parameters: parameters) { (object, error) in
            let endDate = Date()

            if let object = object {
                self.delegate?.gotAccounts(output: object)
            } else {
                self.delegate?.showAlert(message: error)
            }
            
            var interval = TimeInterval()
            interval = endDate.timeIntervalSince(startDate)
            
            print("Execution time for Get Accounts D2D API  \(URL.Entities.getAccounts)  ======== \(interval)")
        }
    }
}
