//
//  SeachPlaceTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 11/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class SearchPlaceTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
