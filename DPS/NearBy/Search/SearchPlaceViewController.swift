//
//  SearchPlaceViewController.swift
//  DPS
//
//  Created by Vivek Patel on 11/09/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import GooglePlaces
import Firebase
import MapKit

protocol UpdateMapDelegate {
    func gotSelectedLocation(_ locationDetails:LocationResult)
}

final class SearchPlaceViewController: UIViewController {
    
    // MARK:- Variables
    private lazy var recentSearches: [RecentSearch] = [] {
        didSet{
            setNoRecord()
        }
    }
    private lazy var results: [GMSAutocompletePrediction] = [] {
        didSet{
            setNoRecord()
        }
    }
    
    private var isRecentSearch = false
    var delegate: UpdateMapDelegate?
    var searchText = ""
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var lblNoSearch: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblRecentSearch: UILabel!
    // MARK:- View Life Cycle
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var recentSearchSeprator: UILabel!
    @IBOutlet weak var viewMain: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNoRecord()
        btnClear.isHidden = true
        lblRecentSearch.isHidden = true
        recentSearchSeprator.isHidden = true
        searchView.text = searchText
        isRecentSearch = searchText.isEmpty
        placeAutocomplete(searchText)
        if searchText.trimmed.isEmpty {
            guard let recentSearches = AppUserDefaults.shared.recentSearches, !recentSearches.isEmpty  else { return }
            isRecentSearch = true
            btnClear.isHidden = false
            lblRecentSearch.isHidden = false
            recentSearchSeprator.isHidden = false
            self.recentSearches = recentSearches.reversed()
        }
        viewMain.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
      //  mainStack.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.searchView.becomeFirstResponder()
    }
    // MARK:- Actions
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    private func setNoRecord() {
        if !recentSearches.isEmpty || !results.isEmpty {
            lblNoSearch.isHidden = true
        } else {
            lblNoSearch.isHidden = false
        }
        tableView?.reloadData()
    }
    // MARK:- To Get Array Of Recent Data
    private func appendDataIntoRecentData(_ recentData:GMSAutocompletePrediction) {
        
        let recentData = RecentSearch(attributedFullText: recentData.attributedFullText.string,
                                      placeID: recentData.attributedFullText.string,
                                      attributedPrimaryText: recentData.attributedPrimaryText.string)
        
        var recentSearches: [RecentSearch]  = AppUserDefaults.shared.recentSearches ?? []
        if let _ = recentSearches.first(where:{ $0.placeID == recentData.placeID }) { return }
        if recentSearches.count < 5 {
            recentSearches.append(recentData)
        } else {
            recentSearches.remove(at: 0)
            recentSearches.append(recentData)
        }
        AppUserDefaults.shared.recentSearches = recentSearches
        tableView.reloadData()
    }
    @IBAction func actionClearRecent(_ sender: Any) {
        AppUserDefaults.shared.recentSearches = nil
        recentSearches.removeAll()
        isRecentSearch = false
        tableView.reloadData()
    }
    private func getNearByPlaces(searchText: String) {
        let locationService = LocationService.instance
        locationService.getLocation { location in
            let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
            guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey"), let location = location else { return }
            let apiKey = "\(key)"
            guard let urlString =   "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(location.coordinate.latitude),\(location.coordinate.longitude)&radius=5000&keyword=\(searchText)&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
                return
            }
            // Spinner.shared.show(self.view)
            URLSession.shared.dataTask(with: url) { data, response, error in
                // Spinner.shared.hide()g
                if let data = data {
                    do {
                        let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                        guard let loactionDetails = object.results.first else {
                            return
                        }
                        DispatchQueue.main.async {
                            self.dismiss(animated: true) {
                                self.delegate?.gotSelectedLocation(loactionDetails)
                            }
                        }
                        
                    } catch let error {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: error.localizedDescription, viewcontrol: self)
                        
                    }
                } else {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "unknown Error", viewcontrol: self)
                }
            }.resume()
        }
    }
    // MARK:- Auto Complete
    private func placeAutocomplete(_ text: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        let placesClient = GMSPlacesClient()
        let token = GMSAutocompleteSessionToken.init()
        placesClient.findAutocompletePredictions(fromQuery: text,
                                                 bounds: nil,
                                                 boundsMode: GMSAutocompleteBoundsMode.bias,
                                                 filter: filter,
                                                 sessionToken: token,
                                                 callback: { (results, error) in
                                                    if let error = error {
                                                        print("Autocmplete error \(error)")
                                                        self.results.removeAll()
                                                        return
                                                    }
                                                    if let results = results {
                                                        print("results \(results.count)")
                                                        self.results = results
                                                    }
                                                    if self.isRecentSearch {
                                                        self.results.removeAll()
                                                    }
                                                 })
        /*  let request = MKLocalSearch.Request()
         request.naturalLanguageQuery = text
         if #available(iOS 13.0, *) {
         request.resultTypes = .pointOfInterest
         } else {
         // Fallback on earlier versions
         }
         // request.region = mapView.region
         let search = MKLocalSearch(request: request)
         search.start { response, _ in
         guard let response = response else {
         return
         }
         self.results = response.mapItems
         self.tableView.reloadData()
         }*/
    }
    
    // MARK:- To Get Coordinates From The Address
    private func getCoordinates(address:String) {
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        guard  let key = dictLoginData.value(forKeyPath: "Company.CompanyConfig.GoogleMapKey") else { return }
        let apiKey = "\(key)"
        guard let urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&sensor=false&key=\(apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlString) else {
            return
        }
       
        // Spinner.shared.show(self.view)
        URLSession.shared.dataTask(with: url) { data, response, error in
            // Spinner.shared.hide()
            if let data = data {
                do {
                    let object = try JSONDecoder().decode(SearchLocation.self, from: data)
                    guard let loactionDetails = object.results.first else {
                        return
                    }
                    DispatchQueue.main.async {
                        self.dismiss(animated: true) {
                            self.delegate?.gotSelectedLocation(loactionDetails)
                        }
                    }
                } catch let error {
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: error.localizedDescription, viewcontrol: self)
                    
                }
            } else {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "unknown Error", viewcontrol: self)
            }
        }.resume()
    }
    
}

// MARK:- Table view data source
extension SearchPlaceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return results.count
        } else if !isRecentSearch {
            lblRecentSearch.isHidden = true
            btnClear.isHidden = true
            recentSearchSeprator.isHidden = true
            return 0
        } else {
            if recentSearches.count > 0 {
                lblRecentSearch.isHidden = false
                btnClear.isHidden = false
                recentSearchSeprator.isHidden = false
            } else {
                lblRecentSearch.isHidden = true
                btnClear.isHidden = true
                recentSearchSeprator.isHidden = true
            }
            return recentSearches.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlaceTableViewCell", for: indexPath) as! SearchPlaceTableViewCell
        if indexPath.section == 0 {
            cell.lblTitle.text = results[indexPath.row].attributedFullText.string
            cell.lblDescription.text = results[indexPath.row].attributedPrimaryText.string
            return cell
        } else {
            cell.lblTitle.text = recentSearches[indexPath.row].attributedFullText
            cell.lblDescription.text = recentSearches[indexPath.row].attributedPrimaryText
            return cell
        }
    }
}

// MARK:- Table view delegate
extension SearchPlaceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 1,isRecentSearch else {
            return UIView()
        }
        let headerView = UIView(frame: CGRect.init(x: 37, y: 15, width: tableView.frame.width, height: 20))
        let label = UILabel(frame: CGRect.init(x: 37, y: 15, width: tableView.frame.width, height: 20))
        label.text = "Recent Searches"
        label.textColor = UIColor.green
        label.font = UIFont.systemFont(ofSize: 20)
        label.sizeToFit()
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            let recentString = results[indexPath.row]
            appendDataIntoRecentData(recentString)
            getCoordinates(address:recentString.attributedFullText.string)
        } else {
            let searchString = recentSearches[indexPath.row]
            getCoordinates(address:searchString.attributedFullText)
        }
    }
}

// MARK:- Search bar delegate
extension SearchPlaceViewController:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        isRecentSearch = searchText.isEmpty
        if searchText.isEmpty {
            results.removeAll()
        } else {
            tableView.reloadData()
            placeAutocomplete(searchText)
        }
        //getNearByPlaces(searchText: searchText)
    }
}
