//
//  ServiceGalleryViewController.swift
//  DPS
//
//  Created by Vivek Patel on 20/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

final class ServiceGalleryViewController: BaseViewController {
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setFooterView()
    }
    
    //MARK: - Actions
    @IBAction func actionUploadImage(_ sender: Any) {
        // openUploadPhotoAlert()
    }
    @IBAction func actionViewGallery(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Gallery", bundle: nil)
        guard let destination = storyboard.instantiateViewController(withIdentifier: "GalleryListViewController") as? GalleryListViewController else {
            return
        }
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

