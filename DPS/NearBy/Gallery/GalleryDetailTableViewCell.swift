//
//  GalleryDetailTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 02/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class GalleryDetailTableViewCell: UITableViewCell {
//MARK:- Outlets
    @IBOutlet weak var switchIsAbedum: UISwitch!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
