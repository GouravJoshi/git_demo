//
//  GalleryDescriptionViewController.swift
//  DPS
//
//  Created by Vivek Patel on 02/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

protocol GalleryContentUpdated {
    func contentUpdated(_  galleryContent: DTDLeadImagesDetailExtSerDc)
}

class GalleryDescriptionViewController: BaseViewController {
    var delegate: GalleryContentUpdated?
    var galleryContent: DTDLeadImagesDetailExtSerDc?
    
    //MARK:- View Life Cycle
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var txtViewCaption: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    private func setData() {
        guard let galleryContent = galleryContent else { return }
        txtViewCaption.text =   galleryContent.leadImageCaption ?? ""
        txtViewDescription.text =  galleryContent.description ?? ""
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSave(_ sender: Any) {
        guard var galleryContent = galleryContent else { return }
        guard !txtViewCaption.text.trimmed.isEmpty else {
            showAlert(message: "Please enter caption")
            return
        }
        galleryContent.description = txtViewDescription.text ?? ""
        galleryContent.leadImageCaption = txtViewCaption.text.trimmed
        self.dismiss(animated: true) {
            self.delegate?.contentUpdated(galleryContent)
        }
        /* let storyboard = UIStoryboard(name: "Gallery", bundle: nil)
         guard let destination = storyboard.instantiateViewController(withIdentifier: "GalleryListViewController") as? GalleryListViewController else {
         return
         }
         self.navigationController?.pushViewController(destination, animated: true)*/
    }
}

