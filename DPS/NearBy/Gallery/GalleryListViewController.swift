//
//  GalleryListViewController.swift
//  DPS
//
//  Created by Vivek Patel on 02/12/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Kingfisher
import UIKit
import Alamofire

class GalleryListViewController: BaseViewController {
    //MARK:- Variables
    var images: [DTDLeadImagesDetailExtSerDc]=[] {
        didSet {
            tableView?.reloadData()
            
        }
    }
    var delegate: ImagesUpdated?
    var tag = -1
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Gallery"
        setFooterView()
    }
    //MARK:- Actions
    @IBAction func actionBack(_ sender: Any) {
        
        delegate?.imagesUpdates(images)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionUploadImage(_ sender: Any) {
        
    }
    
    @IBAction func actionAddImage(_ sender: Any) {
        self.tag = -1
        openUploadPhotoAlert()
    }
    @objc private func actionIsAbbendum(_ sender: UISwitch) {
        images[sender.tag].isAddendum  = sender.isOn
    }
    @objc private func actionPreviewImage(_ sender: UITapGestureRecognizer) {
        
        guard let row = sender.view?.tag else { return }
        let storyboardIpad = UIStoryboard.init(name: "PestiPhone", bundle: nil)
        guard let testController = storyboardIpad.instantiateViewController(withIdentifier: "PreviewImageVC") as? PreviewImageVC else { return }
        let image =  images[row]
        if  let image = image.tempImage {
            testController.img = image
            testController.modalPresentationStyle = .fullScreen
            self.present(testController, animated: false, completion: nil)
        } else {
            
            if let imgUrl =  image.leadImagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl)  {
                self.showLoader()
                DispatchQueue.global().async { [weak self] in
                    if let data = try? Data(contentsOf: url) {
                        self?.hideLoader()
                        if let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                testController.img = image
                                testController.modalPresentationStyle = .fullScreen
                                self?.present(testController, animated: false, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
}
//MARK: - UITableViewDataSource
extension GalleryListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GalleryDetailTableViewCell.self), for: indexPath) as! GalleryDetailTableViewCell
        let row = images[indexPath.row]
        //Need to change
        cell.lblCaption.text = row.leadImageCaption
        cell.lblDescription.text = row.description
        if let image = row.tempImage {
            cell.imgView?.image = image
        } else {
            
            if let imgUrl =  row.leadImagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                let urlString = "\(URL.salesAutoBaseUrl + "/Documents/" + imgUrl)"
                if let url = URL(string: urlString)
                {
                    cell.imgView?.kf.setImage(with: url, placeholder: UIImage(named: "contact_person_details"))
                }else{
                    cell.imgView.image = UIImage(named: "contact_person_details")
                }
            }
        }
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(actionPreviewImage))
        cell.imgView.tag = indexPath.row
        cell.imgView.addGestureRecognizer(tapImage)
        cell.imgView.isUserInteractionEnabled = true
        cell.switchIsAbedum.isOn = row.isAddendum ?? false
        cell.switchIsAbedum.tag = indexPath.row
        cell.switchIsAbedum.addTarget(self, action: #selector(actionIsAbbendum), for: .touchUpInside)
        return cell
    }
}

extension GalleryListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
            self.openEditTypePopup(tag: indexPath.row)
        }
        rename.backgroundColor = .appThemeColor
        let delete = UIContextualAction(style: .destructive, title: "Remove") { (action, sourceView, completionHandler) in
            self.images.remove(at: indexPath.row)
            completionHandler(true)
        }
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename,delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
}
extension GalleryListViewController {
    // MARK: Function To Open Camera
    private func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.allowsEditing = true
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    // MARK: Function To Open Gallery
    private func openGallery() {
        DispatchQueue.main.async {
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
    }
    private func openEditTypePopup(tag: Int) {
        let alert = UIAlertController(title: "Please select an option to edit", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Edit Before Image", style: .default, handler: { _ in
            // self.tag = tag
            // self.openUploadPhotoAlert()
            let storyboardIpad = UIStoryboard.init(name: "Service_iPhone", bundle: nil)
            guard  let testController = storyboardIpad.instantiateViewController(withIdentifier: "DrawingBoardViewController") as? DrawingBoardViewController else { return }
            testController.indexClicked = tag
            testController.strModuleType = "D2D"
            testController.delegate = self
            let row =  self.images[tag]
            if  let row = row.tempImage {
                testController.backgroundImage = row
                testController.modalPresentationStyle = .fullScreen
                self.present(testController, animated: false, completion: nil)
            } else {
                if let imgUrl =  row.leadImagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),  let url = URL(string: imgUrl)  {
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: url) {
                            self?.hideLoader()
                            if let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    testController.backgroundImage = image
                                    testController.modalPresentationStyle = .fullScreen
                                    self?.present(testController, animated: false, completion: nil)
                                }
                            }
                        }
                    }
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Edit Caption/Description", style: .default, handler: { _ in
            let storyboard =  UIStoryboard.init(name: "Gallery", bundle: nil)
           guard let destination = storyboard.instantiateViewController(withIdentifier: "GalleryDescriptionViewController") as? GalleryDescriptionViewController  else { return }
            let galleryContent = self.images[tag]
            self.tag = tag
            destination.galleryContent = galleryContent
            destination.modalPresentationStyle = .overFullScreen
            destination.delegate = self
            self.present(destination, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.view.addSubview(UIView())
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
    private func openUploadPhotoAlert() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.view.addSubview(UIView())
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
}
// MARK:- Image Picker Controller Delegate
extension GalleryListViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            
            guard let image = info[.editedImage] as? UIImage else { return }
            /*
             if let asset = info[.phAsset] as? PHAsset, let name = asset.value(forKey: "filename") as? String {
             fileName = name
             } else if let url = info[.imageURL] as? URL {
             fileName = url.lastPathComponent
             }*/
            if self.tag == -1 {
                let storyboard =  UIStoryboard.init(name: "Gallery", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "GalleryDescriptionViewController") as! GalleryDescriptionViewController
                var galleryContent = DTDLeadImagesDetailExtSerDc()
                galleryContent.tempImage = image
                galleryContent.isAddendum = false
                galleryContent.leadImageType = "Before"
                destination.galleryContent = galleryContent
                destination.modalPresentationStyle = .overFullScreen
                destination.delegate = self
                self.present(destination, animated: true, completion: nil)
            } else {
                self.images[self.tag].tempImage = image
            }
        }
    }
}
extension GalleryListViewController: GalleryContentUpdated {
    func contentUpdated(_ galleryContent: DTDLeadImagesDetailExtSerDc) {
        if self.tag == -1 {
            self.images.append(galleryContent)
        } else {
            self.images[tag].description = galleryContent.description
            self.images[tag].leadImageCaption = galleryContent.leadImageCaption
        }
        tableView.reloadData()
    }
}
extension GalleryListViewController: EditImageViewDelegate {
    func refresImage(_ indexClicked: Int, with updatedImage: UIImage!) {
        self.images[indexClicked].tempImage = updatedImage
        tableView.reloadData()
    }
}
