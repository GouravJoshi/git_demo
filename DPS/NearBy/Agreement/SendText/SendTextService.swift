//
//  SendTextService.swift
//  DPS
//
//  Created by Vivek Patel on 11/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation
import Alamofire

class SendTextService {
    func updateText(parameters: SendAgreement, _ callBack:@escaping (_ object: Bool?,_ error:String?) -> Void) {
        let url = URL.SendText.updateTextDetail
        
        if #available(iOS 13.0, *) {

            AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: URL.headers) .responseDecodable(of: Bool.self) { response in
                switch response.result{
                case let .success(result):
                    callBack(result,nil)
                    break
                case let .failure(error):
                    callBack(nil, error.localizedDescription)
                    break
                }
                
            }.responseJSON { (dataResponse) in
                switch dataResponse.result{
                case .success(let value):
                    print(value)
                    break
                case .failure(let error):
                    print(error)
                    break
                    
                }
            }
            
        } else {
            
            //  For iOS 12 and below devices
            AF.request(url, method: .post, parameters: parameters.dictionary, encoding: JSONEncoding.default, headers: URL.headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    callBack(true,nil)
                    break
                case .failure(_):
                    print(response.error ?? 0)
                    callBack(false, alertSomeError)
                    break
                }
            }
            
        }

    }
}
