//
//  sendTextTableViewCell.swift
//  DPS
//
//  Created by Vivek Patel on 10/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit

class SendTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnSelectPhone: UIButton!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
