//
//  SendTextViewController.swift
//  DPS
//
//  Created by Vivek Patel on 10/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import UIKit
import MessageUI

final class SendTextViewController: BaseViewController {
    //MARK: - Variables
    var isFromWorkOrder = false
    var fromReschedule = Bool()
    var services: [SoldServiceStandardDetail] = []
    var propertyDetails: PropertyDetails?
    var entityDetail: EntityDetail?
    var sendAgreement = SendAgreement()
    var uploadResponseAddress: UploadResponse?
    var uploadResponseCustomer: UploadResponse?
    var uploadResponseTechician: UploadResponse?
    lazy var imagesToUpload: [ImageToUpload] = []
    var isResend = false
    var leadContactDetailEXTSerDc: [LeadContactDetailEXTSerDc] = [] {
        didSet {
            updateUI()
            tableView.reloadData()
        }
    }
    private lazy var sendTextPresenter = {
        return SendTextPresenter(self)
    }()
    var typeNumber = ""
    var isEditable = false
    private lazy var presenter = {
        return ServiceReportPresenter(self)
    }()
    var delegateEdit: EditableStatusDelegate?
    var scheduleResponse: AppoinmentResponse?
    var rescheduleResponse: RescheduleResponse?
    var loaderSendText = UIAlertController()


    //MARK: - Outlets
    @IBOutlet weak var lblSendTextTO: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtNumber: UITextField!{
        didSet {
            txtNumber.delegate = self
        }
    }
    @IBOutlet weak var lblSendtext: UILabel!
    
    @IBOutlet weak var btnSendText: UIButton!

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.btnSendText.isEnabled = true
        self.title = "Send Text"
        setUI()
        setData()
        updateUI()
        
    }
    
    private func setUI() {
        
        if let phoneDetails = entityDetail?.leadContactDetailEXTSerDc {
            self.leadContactDetailEXTSerDc = phoneDetails
        } else if let phone = propertyDetails?.propertyInfo?.phone, !phone.trimmed.isEmpty {
            let phone = createPhoneObject(phone)
            self.leadContactDetailEXTSerDc.insert(phone, at: 0)
        }
    }
    private func setData() {
        guard  let leadContactDetailEXTSerDc = entityDetail?.leadContactDetailEXTSerDc  else {
            return
        }
        self.leadContactDetailEXTSerDc = leadContactDetailEXTSerDc
    }
    private func updateUI() {
        if leadContactDetailEXTSerDc.isEmpty  {
            lblSendTextTO.isHidden = true
        } else {
            lblSendTextTO.isHidden = false
        }
    }
    // MARK: - Action
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionAdd(_ sender: Any) {
        guard !typeNumber.isEmpty  else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter number", viewcontrol: self)
            return
        }
        guard typeNumber.count == 10 else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please enter valid number", viewcontrol: self)
            return
        }
        let number = createPhoneObject()
        guard !leadContactDetailEXTSerDc.contains(number) else {
            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Number already exist", viewcontrol: self)
            return
        }
        leadContactDetailEXTSerDc.insert(number, at: 0)
        txtNumber.text = ""
        typeNumber = ""
        tableView.reloadData()
    }
    @IBAction func actionCreateFollowup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Service", bundle: nil)
        if let destination = storyboard.instantiateViewController(withIdentifier: "CreatTaskViewController") as? CreatTaskViewController {
            destination.propertyDetails = propertyDetails
            self.present(destination, animated: true, completion: nil)
        }
    }
    @IBAction func actionSendText(_ sender: Any) {

        self.btnSendText.isEnabled = false

        guard !leadContactDetailEXTSerDc.isEmpty else {
            
            self.btnSendText.isEnabled = true

            showAlertWithoutAnyAction(strtitle: "Alert", strMessage: "Please add a number", viewcontrol: self)
            return
        }
        if isFromWorkOrder {
            #if TARGET_OS_SIMULATOR
            #else
            openMessageComposer()
            #endif
        } else {
            
            if isEditable  {
                
                if !isInternetAvailable()
                {
                    
                    self.btnSendText.isEnabled = true
                    showAlertWithoutAnyAction(strtitle: Alert, strMessage: alertInternet, viewcontrol: self)

                }
                else
                {
                    
                    loaderSendText = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
                    self.present(loaderSendText, animated: false, completion: nil)
                    
                    if imagesToUpload.isEmpty {
                        if sendAgreement.leadDetail?.isCustomerNotPresent == true {
                            sendAgreement.paymentInfo?.customerSignature = ""
                        }
                        presenter.apiCallForSendMail(sendAgreement)
                    } else {
                        presenter.apiCallForUploadSignature(type: imagesToUpload.first!.type, image: imagesToUpload.first!.image, fileName: imagesToUpload.first!.fileName)
                    }
                    
                }

            } else {
                openMessageComposer()
            }
        }
    }
    @objc func actionSelectPhone(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        leadContactDetailEXTSerDc[sender.tag].isMessageSent = sender.isSelected
    }
    private func createPhoneObject(_ phoneUser: String? = nil) -> LeadContactDetailEXTSerDc {
        var email = LeadContactDetailEXTSerDc()
        if let mail = phoneUser , !mail.trimmed.isEmpty {
            email.contactNumber = mail
        } else {
            email.contactNumber = typeNumber
        }
        email.isMessageSent = true
        email.createdDate = AppUtility.convertDateToString(Date(), toFormat: ConstantV.modifiedDateFormat)
        return email
    }
    private func openMessageComposer()
    {
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            if isFromWorkOrder {
                controller.body = getSendTextMeassageForWorkOrder()
                
                /*var numberList = ""
                if let phoneDetails = entityDetail?.leadContactDetailEXTSerDc {
                    numberList =  phoneDetails.compactMap({$0.contactNumber}).joined(separator: ",").trimmed
                } else if let phone = propertyDetails?.propertyInfo?.phone, !phone.trimmed.isEmpty {
                    numberList = phone
                }
                let numbers = numberList.components(separatedBy: ",")
                controller.recipients = numbers*/
                
                let recipients = leadContactDetailEXTSerDc.compactMap({$0.contactNumber})
                controller.recipients = recipients
                
            } else {
                controller.body = getSendTextMeassage()
                let recipients = leadContactDetailEXTSerDc.compactMap({$0.contactNumber})
                controller.recipients = recipients
            }
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    private func getSendTextMeassageForWorkOrder() -> String
    {
        if fromReschedule
        {
            guard let scheduleResponse = rescheduleResponse else { return "" }
            var employeeList: [EmployeeListD2D] = []
            var leadTecnician: EmployeeListD2D?
            var instpector: EmployeeListD2D?
            var technicianName = ""
            var technicianEmail = ""
            var technicianPhone = ""
            var inspectorName = ""
            var inspectorEmail = ""
            var inspectorPhone = ""
            if let list = AppUserDefaults.shared.employeeList {
                employeeList = list
            }

            //Nilind
    //        var finalStirng = AppUserDefaults.shared.leadDetailMaster?.textTemplateMasters.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
    //            return text.templateName == enumSendTextTemplateWorkOrder
    //        })?.templateContent ?? ""
            
            var finalStirng = AppUserDefaults.shared.masterDetails?.textTemplatesSelectListDcs.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
                return text.templateName == enumSendTextTemplateWorkOrder
            })?.templateContent ?? ""
            print(finalStirng)
            
            //end
            
            if  !employeeList.isEmpty {
                leadTecnician = employeeList.filter({$0.employeeNo == scheduleResponse.employeeNo}).first
                instpector = employeeList.filter({$0.employeeId  == scheduleResponse.createdBy}).first
            }
            if let leadTecnician = leadTecnician {
                technicianName = leadTecnician.fullName ?? ""
                technicianEmail = leadTecnician.primaryEmail ?? ""
                technicianPhone = leadTecnician.primaryPhone ?? ""
            }
            if let instpector = instpector {
                inspectorName = instpector.fullName ?? ""
                inspectorEmail = instpector.primaryEmail ?? ""
                inspectorPhone = instpector.primaryPhone ?? ""
            }
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianName##", with: technicianName)
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianEmail##", with: technicianEmail)
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianPhone##", with: technicianPhone)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorName##", with: inspectorName)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorEmail##", with: inspectorEmail)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorPhone##", with: inspectorPhone)
            let date = scheduleResponse.rescheduleDate ?? ""
            let scheduleDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "dd/MM/yyyy") ?? ""
            let time = scheduleResponse.rescheduleTime ?? ""
            let scheduleTime = AppUtility.convertStringToFormat(fromString: time, toFormat: "HH:mm:ss a") ?? ""
            let customerName = entityDetail?.leadDetail?.firstName ?? ""
            finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleDate##", with: scheduleDate)
            finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleTime##", with: scheduleTime)
            finalStirng = finalStirng.replacingOccurrences(of: "##CustomerName##", with: customerName)
            
            return finalStirng


        }
        else
        {
            guard let scheduleResponse = scheduleResponse else { return "" }
            var employeeList: [EmployeeListD2D] = []
            var leadTecnician: EmployeeListD2D?
            var instpector: EmployeeListD2D?
            var technicianName = ""
            var technicianEmail = ""
            var technicianPhone = ""
            var inspectorName = ""
            var inspectorEmail = ""
            var inspectorPhone = ""
            if let list = AppUserDefaults.shared.employeeList {
                employeeList = list
            }

            //Nilind
    //        var finalStirng = AppUserDefaults.shared.leadDetailMaster?.textTemplateMasters.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
    //            return text.templateName == enumSendTextTemplateWorkOrder
    //        })?.templateContent ?? ""
            
            var finalStirng = AppUserDefaults.shared.masterDetails?.textTemplatesSelectListDcs.filter({$0.templateName == enumSendTextTemplateWorkOrder}).first(where: { (text) -> Bool in
                return text.templateName == enumSendTextTemplateWorkOrder
            })?.templateContent ?? ""
            print(finalStirng)
            
            //end
            
            if  !employeeList.isEmpty {
                leadTecnician = employeeList.filter({$0.employeeNo == scheduleResponse.employeeNo}).first
                instpector = employeeList.filter({$0.employeeId  == scheduleResponse.createdBy}).first
            }
            if let leadTecnician = leadTecnician {
                technicianName = leadTecnician.fullName ?? ""
                technicianEmail = leadTecnician.primaryEmail ?? ""
                technicianPhone = leadTecnician.primaryPhone ?? ""
            }
            if let instpector = instpector {
                inspectorName = instpector.fullName ?? ""
                inspectorEmail = instpector.primaryEmail ?? ""
                inspectorPhone = instpector.primaryPhone ?? ""
            }
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianName##", with: technicianName)
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianEmail##", with: technicianEmail)
            finalStirng = finalStirng.replacingOccurrences(of: "##TechnicianPhone##", with: technicianPhone)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorName##", with: inspectorName)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorEmail##", with: inspectorEmail)
            finalStirng = finalStirng.replacingOccurrences(of: "##InspectorPhone##", with: inspectorPhone)
            let date = scheduleResponse.initialServiceDate ?? ""
            let scheduleDate = AppUtility.convertStringToFormat(fromString: date, toFormat: "dd/MM/yyyy") ?? ""
            let time = scheduleResponse.initialServiceTime ?? ""
            let scheduleTime = AppUtility.convertStringToFormat(fromString: time, toFormat: "HH:mm:ss a") ?? ""
            let customerName = entityDetail?.leadDetail?.firstName ?? ""
            finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleDate##", with: scheduleDate)
            finalStirng = finalStirng.replacingOccurrences(of: "##ScheduleTime##", with: scheduleTime)
            finalStirng = finalStirng.replacingOccurrences(of: "##CustomerName##", with: customerName)
            
            return finalStirng


        }
        
    }
    private func getSendTextMeassage() -> String {
        var finalMeassage = ""
        var agreementUrl = ""
        var proposalUrl = ""
        var inspectionUrl = ""
        var customerNotPresentUrl = ""
        let commonUrl =  "\n" + URL.salesAutoBaseUrl + "/Documents/"
        agreementUrl =  commonUrl + (entityDetail?.paymentInfo?.agreement ?? "")
        proposalUrl = commonUrl + (entityDetail?.paymentInfo?.proposal ?? "")
        inspectionUrl = commonUrl + (entityDetail?.paymentInfo?.inspection ?? "")
        customerNotPresentUrl = "\n" + MainUrlWeb + (entityDetail?.clickHereUrl ?? "")
        
        if let isCustomerNotPresent = entityDetail?.leadDetail?.isCustomerNotPresent, isCustomerNotPresent {
            finalMeassage =  getFinalMessage("Customer Signature Link")
        } else {
            finalMeassage =  getFinalMessage("Signed Agreements")
        }
        finalMeassage = finalMeassage.replacingOccurrences(of: "##CustomerSignatureLink##", with: customerNotPresentUrl)
        finalMeassage = finalMeassage.replacingOccurrences(of: "##AgreementPDFLink##", with: agreementUrl)
        finalMeassage = finalMeassage.replacingOccurrences(of: "##ProposalPDFLink##", with: proposalUrl)
        finalMeassage = finalMeassage.replacingOccurrences(of: "##InspectionPDFLink##", with: inspectionUrl)
        
        finalMeassage = finalMeassage + "\n" + "."
        return finalMeassage
    }
    private func getFinalMessage(_ type: String) -> String {
        var message = ""
        if  let masterData = AppUserDefaults.shared.masterDetails?.textTemplatesSelectListDcs, let temMessage =  masterData.filter({ $0.templateName == type}).first?.templateContent {
            message = temMessage
        }
        message = handleTag(message)
        return message
    }
    private func handleTag(_ message: String) -> String {
        var finalStirng =  message
        if let accountNo = entityDetail?.leadDetail?.accountNo {
            finalStirng = finalStirng.replacingOccurrences(of: "##AccountNo##", with: accountNo)
        }
        if let companyName = entityDetail?.leadDetail?.companyName {
            finalStirng = finalStirng.replacingOccurrences(of: "##CustomerCompanyName##", with: companyName)
        }
        
        if let customerName = entityDetail?.leadDetail?.customerName {
            finalStirng = finalStirng.replacingOccurrences(of: "##CustomerName##", with: customerName)
        }
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let employeeName = "\(dictLoginData.value(forKeyPath: "EmployeeName")!)"
        finalStirng = finalStirng.replacingOccurrences(of: "##EmployeeName##", with: employeeName)
        let employeeEmail = "\(dictLoginData.value(forKeyPath: "EmployeeEmail")!)"
        finalStirng = finalStirng.replacingOccurrences(of: "##EmployeEmail##", with: employeeEmail)
        let employeePhone = "\(dictLoginData.value(forKeyPath: "EmployeePrimaryPhone")!)"
        finalStirng = finalStirng.replacingOccurrences(of: "##EmployeePhone##", with: employeePhone)
        let companyName = "\(dictLoginData.value(forKeyPath: "Company.Name")!)"
        finalStirng = finalStirng.replacingOccurrences(of: "##CompanyName##", with: companyName)
        
        return finalStirng
    }
    private func apiCallForUpdateText() {
        
        var parameter = SendAgreement()
        let dictLoginData = nsud.value(forKey: "LoginDetails") as! NSDictionary
        let companyId = "\(dictLoginData.value(forKeyPath: "Company.CoreCompanyId") ?? "")"
        if let entityDetail = entityDetail {
            parameter.leadId = entityDetail.leadDetail?.leadID
            parameter.leadNumber = entityDetail.leadDetail?.leadNumber
            parameter.isProposalFromMobile = entityDetail.leadDetail?.isProposalFromMobile
            parameter.documentsDetail = entityDetail.documentDetail
            parameter.leadContactDetailEXTSerDc = leadContactDetailEXTSerDc
            parameter.companyId = entityDetail.leadDetail?.companyID?.description ?? companyId
        }
        sendTextPresenter.apiCallForUpdateTextDetail(parameter)
        self.btnSendText.isEnabled = true

    }
}
// MARK: - UITableViewDataSource
extension SendTextViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leadContactDetailEXTSerDc.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SendTextTableViewCell.self), for: indexPath) as! SendTextTableViewCell
        
        cell.btnSelectPhone.tag = indexPath.row
        cell.btnSelectPhone.addTarget(self, action: #selector(actionSelectPhone), for: .touchUpInside)
        cell.lblNumber.text = leadContactDetailEXTSerDc[indexPath.row].contactNumber
        cell.btnSelectPhone.isSelected = leadContactDetailEXTSerDc[indexPath.row].isMessageSent ?? false
        return cell
    }
}
// MARK: - UITableViewDelegate
extension SendTextViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension SendTextViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            let characterSet = CharacterSet(charactersIn: string)
            let allowedCharacters = CharacterSet.decimalDigits
            guard allowedCharacters.isSuperset(of: characterSet) && updatedText.count <= 10 else {
                return false
            }
            typeNumber = updatedText
        }
        return true
    }
}

extension SendTextViewController: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        
        self.btnSendText.isEnabled = false

        self.dismiss(animated: true)
        {
            if self.isFromWorkOrder
            {
                if self.fromReschedule
                {
                    
                    self.btnSendText.isEnabled = true
                    
                    /*for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ScheduleViewController.self) {
                            self.navigationController!.popToViewController(controller, animated: false)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                            break
                        }
                    }*/
                    
                    
                    var isVCFound = false
                    var controllerVC = UIViewController()
                    for controller in self.navigationController!.viewControllers as Array { if controller.isKind(of: ScheduleViewController.self) {
                        
                        isVCFound = true
                        controllerVC = controller
                   
                    }
                    }
                    if isVCFound == true {
                        self.navigationController?.popToViewController(controllerVC, animated: false)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadViewSetup_Notification"), object: nil, userInfo: nil)
                    }
                    
                }
                else
                {

                    self.btnSendText.isEnabled = true

                    self.performSegue(withIdentifier: "UnWindToProprtyDetail", sender: self)
                }
            }
            else
            {
                self.apiCallForUpdateText()
            }
        }
    }
}
extension SendTextViewController: SendTextView {
    func successfullyUpdateTExtDetail(_ output: Bool)
    {
        entityDetail?.leadContactDetailEXTSerDc = leadContactDetailEXTSerDc
        delegateEdit?.updateEditStatus(self.entityDetail, propertyDetail: self.propertyDetails)
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension SendTextViewController: ServiceReportView {
    func uploadImage(type: UploadType, uploadResponse: UploadResponse) {
        if type == .address {
            uploadResponseAddress = uploadResponse
        } else if type == .customer {
            uploadResponseCustomer = uploadResponse
        } else if type == .technician {
            uploadResponseTechician = uploadResponse
        }
        if let index = imagesToUpload.firstIndex(where: { (image) -> Bool in
            return image.type == type
        }) {
            imagesToUpload.remove(at: index)
            if !imagesToUpload.isEmpty {
                presenter.apiCallForUploadSignature(type: imagesToUpload.first!.type, image: imagesToUpload.first!.image, fileName: imagesToUpload.first!.fileName)
            } else {
                if let uploadResponse = uploadResponseAddress {
                    sendAgreement.addressImagePath = uploadResponse.name
                }
                if let uploadResponse = uploadResponseCustomer {
                    sendAgreement.paymentInfo?.customerSignature = uploadResponse.name
                }
                if let uploadResponse = uploadResponseTechician {
                    sendAgreement.paymentInfo?.salesSignature = uploadResponse.name
                }
                if sendAgreement.leadDetail?.isCustomerNotPresent == true {
                    sendAgreement.paymentInfo?.customerSignature = ""
                } else if sendAgreement.leadDetail?.isCustomerNotPresent == false,  sendAgreement.paymentInfo?.customerSignature?.trimmed == "" {
                    showAlert(message: "Customer signature is not been uploaded, please try again later")
                    return
                }
                presenter.apiCallForSendMail(sendAgreement)
            }
        }
    }
    func mailSendSuccessfully(_ output: EntityDetail)
    {
        self.entityDetail = output
        //Nilind
//        self.entityDetail?.webLeadId = output.webLeadId
//        self.entityDetail?.leadNumber = output.leadNumber
//        self.entityDetail?.thirdPartyAccountId = output.thirdPartyAccountId
//        self.entityDetail?.billToLocationId = output.billToLocationId
        
        isEditable = false
        
        self.loaderSendText.dismiss(animated: false) {

            self.openMessageComposer()

        }
        //openMessageComposer()
        self.propertyDetails?.propertyType = .opportunity
        delegateEdit?.updateEditStatus(self.entityDetail, propertyDetail: self.propertyDetails)
    }
    func resendAgreementSuccessfully(_ output: Bool) { }
}
