//
//  SendTextPresenter.swift
//  DPS
//
//  Created by Vivek Patel on 11/11/20.
//  Copyright © 2020 Saavan. All rights reserved.
//

import Foundation

protocol SendTextView: CommonView {
    func successfullyUpdateTExtDetail(_ output: Bool)
}

class SendTextPresenter: NSObject {
    weak private var delegate: SendTextView?
    private lazy var service = {
        return SendTextService()
    }()
    
    init(_ delegate: SendTextView) {
        self.delegate = delegate
    }
    
    func apiCallForUpdateTextDetail( _ parameters: SendAgreement) {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let orderJsonData = try! encoder.encode(parameters)
        print(String(data: orderJsonData, encoding: .utf8)!)
        
        FTIndicator.showProgress(withMessage: "", userInteractionEnable: false)
        service.updateText(parameters: parameters) { (object, error) in
            FTIndicator.dismissProgress()
            if let object = object {
                self.delegate?.successfullyUpdateTExtDetail(object)
                
            } else {
                self.delegate?.showAlert(message: error)
            }
        }
    }
}
